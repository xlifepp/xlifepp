/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Geometry.cpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 10 dec 2007
  \date 17 oct 2014

  \brief Implementation of xlifepp::Geometry class members and related functions
*/

#include "Geometry.hpp"
#include "geometries1D.hpp"
#include "geometries2D.hpp"
#include "Geodesic.hpp"

namespace xlifepp
{
//!dimension of a shape type (return -1 if not defined)
int_t shapeDim(ShapeType sh)
{
  switch (sh)
  {
    case _point: case _setofpoints:
      return 0;
    case _segment:
    case _ellArc: case _circArc:
    case _splineArc: case _parametrizedArc:
      return 1;
    case _polygon: case _triangle: case _quadrangle: case _parallelogram: case _rectangle: case _square:
    case _ellipse: case _disk:
    case _ellipticSector: case _circularSector:
    case _splineSurface: case _parametrizedSurface:
    case _ellipsoidSidePart: case _sphereSidePart:
      return 2;
    case _polyhedron: case _tetrahedron: case _hexahedron: case _parallelepiped: case _cuboid: case _cube:
    case _ellipsoid: case _ball:
    case _trunk: case _cylinder: case _prism: case _cone: case _pyramid:
    case _revTrunk: case _revCylinder: case _revCone:
    case _trunkSidePart: case _cylinderSidePart: case _coneSidePart:
      return 3;
    default:
      break;
  }
  return -1;  //undefined or variable dim
}

void buildNameAndSuffixTransformParams(std::vector<Parameter>& ps, string_t& name, string_t& suffix)
{
  std::vector<Parameter> ps2;
  for (number_t i=0; i < ps.size(); ++i)
  {
    Parameter& p=ps[i];
    ParameterKey key=p.key();
    switch(key)
    {
      case _pk_name:
      {
        switch (p.type())
        {
          case _string:
            name = p.get_s();
            break;
          default:
            error("param_badtype", words("value", p.type()), words("param key", p.key()));
        }
        break;
      }
      case _pk_suffix:
      {
        switch (p.type())
        {
          case _string:
            suffix = p.get_s();
            break;
          default:
            error("param_badtype", words("value", p.type()), words("param key", p.key()));
        }
        break;
      }
      default:
        ps2.push_back(p);
        break;
    }
  }
  ps=ps2;
}
//---------------------------------------------------------------------------
// BoundingBox member functions and related external functions
//---------------------------------------------------------------------------

// constructors
BoundingBox::BoundingBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax, real_t zmin, real_t zmax)
{
  bounds_.resize(3);
  bounds_[0] = RealPair(xmin, xmax);
  bounds_[1] = RealPair(ymin, ymax);
  bounds_[2] = RealPair(zmin, zmax);
}

BoundingBox::BoundingBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax)
{
  bounds_.resize(2);
  bounds_[0] = RealPair(xmin, xmax);
  bounds_[1] = RealPair(ymin, ymax);
}

BoundingBox::BoundingBox(real_t xmin, real_t xmax)
{
  bounds_.resize(1);
  bounds_[0] = RealPair(xmin, xmax);
}

BoundingBox::BoundingBox(const Point& p0, const Point& p1, const Point& p2, const Point& p3)
{
  if (p0.size() != p1.size())
  { error("diff_pts_size", p0.size(), p1.size()); }
  if (p0.size() != p2.size())
  { error("diff_pts_size", p0.size(), p2.size()); }
  if (p0.size() != p3.size())
  { error("diff_pts_size", p0.size(), p3.size()); }
  if (p1.size() != p2.size())
  { error("diff_pts_size", p1.size(), p2.size()); }
  if (p1.size() != p3.size())
  { error("diff_pts_size", p1.size(), p3.size()); }
  if (p2.size() != p3.size())
  { error("diff_pts_size", p2.size(), p3.size()); }
  // we have to ensure that p0, p1, p2, p3 is an orthogonal trihedral
  Point pp0 = p0;
  Point pp1 = p1;

  // computing pp0, pp1 and pp2 from p0, p1 and p2
  Point p0p1 = p1 - p0;
  Point p0p2 = p2 - p0;
  Point pp2;
  if (dot(p0p1, p0p2) == 0.)
  { pp2 = p2; }
  else
  {
    real_t h;
    real_t d01 = p0.distance(p1);
    Point ph = projectionOnStraightLine(p2, p0, p1, h);
    Point p0ph = ph - p0;
    real_t d0h = p0.distance(ph);
    if (dot(p0ph, p0p1) > 0.)
    {
      if (d0h > d01)
      { pp1 = ph; }
      pp2 = p2 - p0ph;
    }
    else
    {
      pp0 = ph;
      pp2 = p2;
    }
  }
  // computing pp0, pp1, pp2 and pp3 from p0, p1, p2 and p3
  Point p0p3 = p3 - pp0;
  Point p1p3 = p3 - pp1;
  Point p2p3 = p3 - pp2;
  p0p1 = pp1 - pp0;
  p0p2 = pp2 - pp0;
  Point pp3;
  if (dot(p0p3, p1p3) == 0 && dot(p0p3, p2p3) == 0.)
  { pp3 = p3; }
  else
  {
    real_t h12;
    Point ph12 = projectionOfPointOnPlane(p3, pp0, pp1, pp2, h12);
    // updating pp0 and pp1 (and eventually pp2)
    real_t h1;
    Point ph1 = projectionOnStraightLine(ph12, pp0, pp1, h1);
    Point p0ph1 = ph1 - pp0;
    real_t d0h1 = pp0.distance(ph1);
    if (dot(p0p1, p0ph1) > 0.)
    {
      if (d0h1 > pp0.distance(pp1))
      { pp1 = ph1; }
    }
    else
    {
      pp2 = pp2 + ph1 - pp0;
      pp0 = ph1;
    }
    // updating pp0 and pp2 (and eventually pp1)
    real_t h2;
    Point ph2 = projectionOnStraightLine(ph12, pp0, pp2, h2);
    Point p0ph2 = ph2 - pp0;
    p0p2 = pp2 - pp0;
    real_t d0h2 = pp0.distance(ph2);
    if (dot(p0p2, p0ph2) > 0.)
    {
      if (d0h2 > pp0.distance(pp2))
      { pp2 = ph2; }
    }
    else
    {
      pp1 = pp1 + ph2 - pp0;
      pp0 = ph2;
    }

    p0p1 = pp1 - pp0;
    p0p2 = pp2 - pp0;
    Point cross12 = crossProduct(p0p1, p0p2);
    real_t h3;
    Point ph3 = projectionOnStraightLine(p3, p0, p0 + cross12, h3);
    Point p0ph3 = ph3 - pp0;
    if (dot(cross12, p0ph3) > 0.)
    { pp3 = ph3; }
    else
    {
      pp3 = pp0;
      pp0 = pp0 - p0ph3;
      pp1 = pp1 - p0ph3;
      pp2 = pp2 - p0ph3;
    }
  }

  Point pp4 = pp1 + pp2 - pp0;
  Point pp5 = pp1 + pp3 - pp0;
  Point pp6 = pp2 + pp3 - pp0;
  Point pp7 = pp4 + pp3 - pp0;
  // with 4 points, it is a 3D bounding box and points are 3D
  dimen_t n = 3;
  bounds_.resize(n);

  for (dimen_t i = 0; i < n; i++)
  {
    bounds_[i] = RealPair(std::min({pp0[i], pp1[i], pp2[i], pp3[i], pp4[i], pp5[i], pp6[i], pp7[i]}),
                          std::max({pp0[i], pp1[i], pp2[i], pp3[i], pp4[i], pp5[i], pp6[i], pp7[i]}));
  }
}

BoundingBox::BoundingBox(const Point& p0, const Point& p1, const Point& p2)
{
  if (p0.size() != p1.size())
  { error("diff_pts_size", p0.size(), p1.size()); }
  if (p0.size() != p2.size())
  { error("diff_pts_size", p0.size(), p2.size()); }
  if (p1.size() != p2.size())
  { error("diff_pts_size", p1.size(), p2.size()); }
  // we have to ensure that p0, p1, p2 is an orthogonal trihedral
  Point pp0 = p0;
  Point pp1 = p1;

  // computing pp0, pp1 and pp2
  Point p0p1 = p1 - p0;
  Point p0p2 = p2 - p0;
  Point pp2;
  if (std::abs(dot(p0p1, p0p2)) > theEpsilon)
  { pp2 = p2; }
  else
  {
    real_t h;
    real_t d01 = p0.distance(p1);
    Point ph = projectionOnStraightLine(p2, p0, p1, h);
    Point p0ph = ph - p0;
    real_t d0h = p0.distance(ph);
    if (dot(p0ph, p0p1) > 0.)
    {
      if (d0h > d01)
      { pp1 = ph; }
      pp2 = p2 - p0ph;
    }
    else
    {
      pp0 = ph;
      pp2 = p2;
    }
  }
  Point pp3 = pp1 + pp2 - pp0;
  // with 3 points, bounding box can be 2D or 3D and points can be 2D or 3D
  dimen_t n = 3;
  if (p0.size() == 2)
  { n = 2; }
  else
  {
    Point x = p1 - p0, y = p2 - p0;
    if (x[2] == 0. && y[2] == 0.)
    { n = 2; }
  }
  bounds_.resize(n);
  for (dimen_t i = 0; i < n; i++)
  {
    bounds_[i] = RealPair(std::min({pp0[i], pp1[i], pp2[i], pp3[i]}), std::max({pp0[i], pp1[i], pp2[i], pp3[i]}));
  }
}

BoundingBox::BoundingBox(const Point& p0, const Point& p1)
{

  if (p0.size() != p1.size())
  { error("diff_pts_size", p0.size(), p1.size()); }
  // with 2 points, bounding box can be 1D, 2D or 3D, and points can be 1D, 2D, or 3D
  dimen_t n = 2;
  if (p0.size() == 1)
  { n = 1; }
  else if (p0.size() == 2)
  {
    Point x = p1 - p0;
    if (x[1] == 0.)
    { n = 1; }
  }
  else
  {
    Point x = p1 - p0;
    if (x[2] == 0.)
    {
      if (x[1] == 0.)
      { n = 1; }
    }
    else
    { n = 3; }
  }
  bounds_.resize(n);
  for (dimen_t i = 0; i < n; i++)
  { bounds_[i] = RealPair(std::min(p0[i], p1[i]), std::max(p0[i], p1[i])); }
}

// Build a bounding box from a set of points
BoundingBox::BoundingBox(const std::vector<Point>& vp)
{
  if (vp.empty())
  { return; }
  std::vector<Point>::const_iterator vp_it(vp.begin());
  // Initialize with the first point
  const Point& p0(*vp_it);
  bounds_.resize(p0.size());
  for (dimen_t i = 0; i < p0.size(); i++)
  { bounds_[i] = RealPair(p0[i], p0[i]); }
  // Compare with the others points
  for (vp_it++; vp_it < vp.end(); vp_it++)
  {
    if (p0.size() != vp_it->size())
    { error("diff_pts_size", p0.size(), vp_it->size()); }
    for (dimen_t i = 0; i < p0.size(); i++)
    { bounds_[i] = RealPair(std::min(bounds_[i].first, (*vp_it)[i]), std::max(bounds_[i].second, (*vp_it)[i])); }
  }
}

RealPair BoundingBox::bounds(dimen_t i) const
{
  if (i <= 0 || i > bounds_.size())
  { return RealPair(0., 0.); }
  return bounds_[i - 1];
}

// extrem points of the bounding box
Point BoundingBox::minPoint() const   // min point (left,bottom,front)
{
  std::vector<real_t> P(bounds_.size());
  for (dimen_t i = 0; i < bounds_.size(); i++)
  { P[i] = bounds_[i].first; }
  return Point(P);
}

Point BoundingBox::maxPoint() const   // max point (right,top,back)
{
  std::vector<real_t> P(bounds_.size());
  for (dimen_t i = 0; i < bounds_.size(); i++)
  { P[i] = bounds_[i].second; }
  return Point(P);
}

std::vector<Point> BoundingBox::points()   // return the vertices of the bounding box
{
  if (bounds_.size() == 1)
  {
    std::vector<Point> pts(2);
    pts[0] = minPoint();
    pts[1] = maxPoint();
    return pts;
  }
  if (bounds_.size() == 2)
  {
    std::vector<Point> pts(4);
    pts[0] = minPoint();
    pts[2] = maxPoint();
    pts[1].push_back(pts[2][0]);
    pts[1].push_back(pts[0][1]);
    pts[3].push_back(pts[0][0]);
    pts[3].push_back(pts[2][1]);
    return pts;
  }
  if (bounds_.size() == 3)
  {
    std::vector<Point> pts(8);
    pts[0] = minPoint();
    pts[6] = maxPoint();
    pts[1].push_back(pts[6][0]);
    pts[1].push_back(pts[0][1]);
    pts[1].push_back(pts[0][2]);
    pts[2].push_back(pts[6][0]);
    pts[2].push_back(pts[6][1]);
    pts[2].push_back(pts[0][2]);
    pts[3].push_back(pts[0][0]);
    pts[3].push_back(pts[6][1]);
    pts[3].push_back(pts[0][2]);
    pts[4].push_back(pts[0][0]);
    pts[4].push_back(pts[0][1]);
    pts[4].push_back(pts[6][2]);
    pts[5].push_back(pts[6][0]);
    pts[5].push_back(pts[0][1]);
    pts[5].push_back(pts[6][2]);
    pts[7].push_back(pts[0][0]);
    pts[7].push_back(pts[6][1]);
    pts[7].push_back(pts[6][2]);
    return pts;
  }
  return std::vector<Point>(); // dummy return
}

real_t BoundingBox::diameter() const  // return the max of (xmax-xmin, ymax-ymin, zmax-zmin)
{
  real_t m = 0.;
  for (dimen_t i = 0; i < bounds_.size(); i++)
  { m = std::max(m, bounds_[i].second - bounds_[i].first); }
  return m;
}

real_t BoundingBox::diameter2() const  // return sqrt((xmax-xmin)^2 + (ymax-ymin)^2 + (zmax-zmin)^2)
{
  real_t d = 0.;
  for (dimen_t i = 0; i < bounds_.size(); i++)
  {
    real_t e = bounds_[i].second - bounds_[i].first;
    d += e * e;
  }
  return std::sqrt(d);
}

// merge a bounding box to the current one
BoundingBox& BoundingBox::operator+=(const BoundingBox& bb)
{
  // we assure that both bounding boxes have the same dim ( the max of both dims )
  std::vector<RealPair> bounds = bb.bounds_;
  for (number_t i = bounds_.size(); i < bb.bounds_.size(); ++i)
  { bounds_.push_back(RealPair(0., 0.)); }
  for (number_t i = bb.bounds_.size(); i < bounds_.size(); ++i)
  { bounds.push_back(RealPair(0., 0.)); }

  for (number_t i = 0; i < bounds_.size(); i++)
  {
    bounds_[i].first = std::min(bounds_[i].first, bounds[i].first);
    bounds_[i].second = std::max(bounds_[i].second, bounds[i].second);
  }
  return *this;
}

// intersect a bounding box to the current one
BoundingBox& BoundingBox::operator^=(const BoundingBox& bb)
{
  // we assure that both bounding boxes have the same dim ( the max of both dims )
  std::vector<RealPair> bounds = bb.bounds_;
  for (number_t i = bounds_.size(); i < bb.bounds_.size(); ++i)
  { bounds_.push_back(RealPair(0., 0.)); }
  for (number_t i = bb.bounds_.size(); i < bounds_.size(); ++i)
  { bounds.push_back(RealPair(0., 0.)); }

  for (number_t i = 0; i < bounds_.size(); i++)
  {
    bounds_[i].first = std::max(bounds_[i].first, bounds[i].first);
    bounds_[i].second = std::min(bounds_[i].second, bounds[i].second);
    if (bounds_[i].first > bounds_[i].second) { bounds[i] = RealPair(0., 0.); } // null bounding box
  }
  return *this;
}

//format boundingbox as string: [a,b]x[c,d]x[e,f]
string_t BoundingBox::asString() const
{
  string_t s;
  number_t i, bs = bounds_.size();
  if (bs > 0)
  {
    real_t eps = 100.*theEpsilon;
    for (i = 0; i < bs - 1; i++)
    {
      real_t start = bounds_[i].first;
      real_t end = bounds_[i].second;
      if (std::abs(start) < eps) { start = 0.; }
      if (std::abs(end) < eps) { end = 0.; }
      s += "[" + tostring(start) + "," + tostring(end) + "]x";
    }
    real_t start = bounds_[i].first;
    real_t end = bounds_[i].second;
    if (std::abs(start) < eps) { start = 0.; }
    if (std::abs(end) < eps) { end = 0.; }
    s += "[" + tostring(start) + "," + tostring(end) + "]";
  }
  return s;
}

//print facilities
void BoundingBox::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0)
  { return; }
  os << "BoundingBox ";
  os << asString();
}

std::ostream& operator<<(std::ostream& os, const BoundingBox& bb)
{
  bb.print(os);
  return os;
}

std::ostream& operator<<(std::ostream& os, const RealPair& rp)
{
  os << "(" << rp.first << "," << rp.second << ")";
  return os;
}

// distance from two bounding boxes
real_t dist(const BoundingBox& bb1, const BoundingBox& bb2)
{
  real_t d = 0.;
  for (number_t i = 1; i <= bb1.dim(); i++)
  {
    RealPair xs1 = bb1.bounds(i), xs2 = bb2.bounds(i);
    real_t xmin1 = xs1.first, xmax1 = xs1.second, xmin2 = xs2.first, xmax2 = xs2.second;
    if (xmax2 < xmin1)
    { d += (xmax2 - xmin1) * (xmax2 - xmin1); }
    else if (xmin2 > xmax1)
    { d += (xmin2 - xmax1) * (xmin2 - xmax1); }
  }
  return std::sqrt(d);
}

//---------------------------------------------------------------------------
// BoundingBox transformations facilities
//---------------------------------------------------------------------------

//! apply a geometrical transformation on a BoundingBox
BoundingBox& BoundingBox::transform(const Transformation& t)
{
  std::vector<Point> pts = points();

  if (t.transformType() == _composition)
  {
    for (number_t j = 0; j < t.components().size(); ++j)
    {
      if (t.component(j)->transformType() == _rotation2d || t.component(j)->transformType() == _reflection2d)
      {
        if (pts.size() == 8)
        { error("transform_not_3D", words("transform", t.component(j)->transformType())); }
      }
      for (number_t i = 0; i < pts.size(); ++i)
      { pts[i] = t.component(j)->apply(pts[i]); }
    }
  }
  else
  {
    for (number_t i = 0; i < pts.size(); ++i)
    { pts[i] = t.apply(pts[i]); }
  }

  *this = BoundingBox(pts);
  return *this;
}

//! apply a translation on a BoundingBox (1 key)
BoundingBox& BoundingBox::translate(const Parameter& p1)
{
  return this->transform(Translation(p1));
}

//! apply a translation on a BoundingBox (vector version)
BoundingBox& BoundingBox::translate(std::vector<real_t> u)
{
  warning("deprecated", "BoundingBox::translate(Reals)", "BoundingBox::translate(_direction=xxx)");
  return this->transform(Translation(_direction=u));
}

//! apply a translation on a BoundingBox (3 reals version)
BoundingBox& BoundingBox::translate(real_t ux, real_t uy, real_t uz)
{
  warning("deprecated", "BoundingBox::translate(Real, Real, Real)", "BoundingBox::translate(_direction={xxx, yyy, zzz})");
  return this->transform(Translation(_direction={ux, uy, uz}));
}

//! apply a rotation on a BoundingBox (1 key)
BoundingBox& BoundingBox::rotate2d(const Parameter& p1)
{
  std::vector<Point> pts = points();
  if (pts.size() == 6)
  { error("transform_not_3D", words("transform", _rotation2d)); }
  return this->transform(Rotation2d(p1));
}

//! apply a rotation on a BoundingBox (2 keys)
BoundingBox& BoundingBox::rotate2d(const Parameter& p1, const Parameter& p2)
{
  std::vector<Point> pts = points();
  if (pts.size() == 6)
  { error("transform_not_3D", words("transform", _rotation2d)); }
  return this->transform(Rotation2d(p1, p2));
}

//! apply a rotation on a BoundingBox
BoundingBox& BoundingBox::rotate2d(const Point& c, real_t angle)
{
  warning("deprecated", "BoundingBox::rotate2d(Point, Real)", "BoundingBox::rotate2d(_center=xxx, _angle=yyy)");
  std::vector<Point> pts = points();
  if (pts.size() == 6)
  { error("transform_not_3D", words("transform", _rotation2d)); }
  return this->transform(Rotation2d(_center=c, _angle=angle));
}

//! apply a rotation on a BoundingBox (1 key)
BoundingBox& BoundingBox::rotate3d(const Parameter& p1)
{
  return this->transform(Rotation3d(p1));
}

//! apply a rotation on a BoundingBox (2 keys)
BoundingBox& BoundingBox::rotate3d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Rotation3d(p1, p2));
}

//! apply a rotation on a BoundingBox (2 keys)
BoundingBox& BoundingBox::rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  return this->transform(Rotation3d(p1, p2, p3));
}

//! apply a rotation on a BoundingBox
BoundingBox& BoundingBox::rotate3d(const Point& c, std::vector<real_t> d, real_t angle)
{
  warning("deprecated", "BoundingBox::rotate3d(Point, Reals, Real)", "BoundingBox::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
  return this->transform(Rotation3d(_center=c, _axis=d, _angle=angle));
}

//! apply a rotation on a BoundingBox
BoundingBox& BoundingBox::rotate3d(real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "BoundingBox::rotate3d(Real, Real, Real)", "BoundingBox::rotate3d(_axis={dx, dy}, _angle=zzz)");
  return this->transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
}

//! apply a rotation on a BoundingBox
BoundingBox& BoundingBox::rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "BoundingBox::rotate3d(Real, Real, Real, Real)", "BoundingBox::rotate3d(_axis={xxx, yyy, zzz}, _angle=ttt)");
  return this->transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
}

//! apply a rotation on a BoundingBox
BoundingBox& BoundingBox::rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "BoundingBox::rotate3d(Point, Real, Real, Real)", "BoundingBox::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
  return this->transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
}

//! apply a rotation on a BoundingBox
BoundingBox& BoundingBox::rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "BoundingBox::rotate3d(Point, Real, Real, Real)", "BoundingBox::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
  return this->transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
}

//! apply a homothety on a BoundingBox (1 key)
BoundingBox& BoundingBox::homothetize(const Parameter& p1)
{
  return this->transform(Homothety(p1));
}

//! apply a homothety on a BoundingBox (2 keys)
BoundingBox& BoundingBox::homothetize(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Homothety(p1, p2));
}

//! apply a homothety on a BoundingBox
BoundingBox& BoundingBox::homothetize(const Point& c, real_t factor)
{
  warning("deprecated", "BoundingBox::homothetize(Point, Real)", "BoundingBox::homothetize(_center=xxx, _scale=yyy)");
  return this->transform(Homothety(_center=c, _scale=factor));
}

//! apply a homothety on a BoundingBox
BoundingBox& BoundingBox::homothetize(real_t factor)
{
  warning("deprecated", "BoundingBox::homothetize(Real)", "BoundingBox::homothetize(_scale=xxx)");
  return this->transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
}

//! apply a point reflection on a BoundingBox (1 key)
BoundingBox& BoundingBox::pointReflect(const Parameter& p1)
{
  return this->transform(PointReflection(p1));
}

//! apply a point reflection on a BoundingBox
BoundingBox& BoundingBox::pointReflect(const Point& c)
{
  warning("deprecated", "BoundingBox::pointReflect(Point)", "BoundingBox::pointReflect(_center=xxx)");
  return this->transform(PointReflection(_center=c));
}

//! apply a reflection2d on a BoundingBox (1 key)
BoundingBox& BoundingBox::reflect2d(const Parameter& p1)
{
  std::vector<Point> pts = points();
  if (pts.size() == 6)
  { error("transform_not_3D", words("transform", _reflection2d)); }
  return this->transform(Reflection2d(p1));
}

//! apply a reflection2d on a BoundingBox (2 keys)
BoundingBox& BoundingBox::reflect2d(const Parameter& p1, const Parameter& p2)
{
  std::vector<Point> pts = points();
  if (pts.size() == 6)
  { error("transform_not_3D", words("transform", _reflection2d)); }
  return this->transform(Reflection2d(p1, p2));
}

//! apply a reflection2d on a BoundingBox
BoundingBox& BoundingBox::reflect2d(const Point& c, std::vector<real_t> d)
{
  warning("deprecated", "BoundingBox::reflect2d(Point, Reals)", "BoundingBox::reflect2d(_center=xxx, _direction=d)");
  std::vector<Point> pts = points();
  if (pts.size() == 6)
  { error("transform_not_3D", words("transform", _reflection2d)); }
  return this->transform(Reflection2d(_center=c, _direction=d));
}

//! apply a reflection2d on a BoundingBox
BoundingBox& BoundingBox::reflect2d(const Point& c, real_t dx, real_t dy)
{
  warning("deprecated", "BoundingBox::reflect2d(Point, Real, Real)", "BoundingBox::reflect2d(_center=xxx, _direction={dx, dy})");
  std::vector<Point> pts = points();
  if (pts.size() == 6)
  { error("transform_not_3D", words("transform", _reflection2d)); }
  return this->transform(Reflection2d(_center=c, _direction={dx, dy}));
}

//! apply a reflection3d on a BoundingBox (1 key)
BoundingBox& BoundingBox::reflect3d(const Parameter& p1)
{
  return this->transform(Reflection3d(p1));
}

//! apply a reflection3d on a BoundingBox (2 keys)
BoundingBox& BoundingBox::reflect3d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Reflection3d(p1, p2));
}

//! apply a reflection3d on a BoundingBox
BoundingBox& BoundingBox::reflect3d(const Point& c, std::vector<real_t> n)
{
  warning("deprecated", "BoundingBox::reflect3d(Point, Reals)", "BoundingBox::reflect3d(_center=xxx, _normal=yyy)");
  return this->transform(Reflection3d(_center=c, _normal=n));
}

//! apply a reflection3d on a BoundingBox
BoundingBox& BoundingBox::reflect3d(const Point& c, real_t nx, real_t ny, real_t nz)
{
  warning("deprecated", "BoundingBox::reflect3d(Point, Real, Real, Real)", "BoundingBox::reflect3d(_center=xxx, _normal={nx, ny, nz})");
  return this->transform(Reflection3d(_center=c, _normal={nx, ny, nz}));
}

//---------------------------------------------------------------------------
// MinimalBox member functions and related external functions
//---------------------------------------------------------------------------

MinimalBox::MinimalBox(const std::vector<RealPair>& bds)
{
  if (bds.size() == 1)
  {
    bounds_.resize(2);
    bounds_[0] = Point(bds[0].first, 0., 0.);
    bounds_[1] = Point(bds[0].second, 0., 0.);
  }
  if (bds.size() == 2)
  {
    bounds_.resize(3);
    bounds_[0] = Point(bds[0].first, bds[1].first, 0.);
    bounds_[1] = Point(bds[0].second, bds[1].first, 0.);
    bounds_[2] = Point(bds[0].first, bds[1].second, 0.);
  }
  if (bds.size() == 3)
  {
    bounds_.resize(4);
    bounds_[0] = Point(bds[0].first, bds[1].first, bds[2].first);
    bounds_[1] = Point(bds[0].second, bds[1].first, bds[2].first);
    bounds_[2] = Point(bds[0].first, bds[1].second, bds[2].first);
    bounds_[3] = Point(bds[0].first, bds[1].first, bds[2].second);
  }
}

MinimalBox::MinimalBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax, real_t zmin, real_t zmax)
{
  bounds_.resize(4);
  bounds_[0] = Point(xmin, ymin, zmin);
  bounds_[1] = Point(xmax, ymin, zmin);
  bounds_[2] = Point(xmin, ymax, zmin);
  bounds_[3] = Point(xmin, ymin, zmax);
}

MinimalBox::MinimalBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax)
{
  bounds_.resize(3);
  bounds_[0] = Point(xmin, ymin, 0.);
  bounds_[1] = Point(xmax, ymin, 0.);
  bounds_[2] = Point(xmin, ymax, 0.);
}

MinimalBox::MinimalBox(real_t xmin, real_t xmax)
{
  bounds_.resize(2);
  bounds_[0] = Point(xmin, 0., 0.);
  bounds_[1] = Point(xmax, 0., 0.);
}

MinimalBox::MinimalBox(const Point& p0, const Point& p1, const Point& p2, const Point& p3)
{
  bounds_.resize(4);
  bounds_[0] = force3D(p0);
  bounds_[1] = force3D(p1);
  bounds_[2] = force3D(p2);
  bounds_[3] = force3D(p3);
}

MinimalBox::MinimalBox(const Point& p0, const Point& p1, const Point& p2)
{
  bounds_.resize(3);
  bounds_[0] = force3D(p0);
  bounds_[1] = force3D(p1);
  bounds_[2] = force3D(p2);
}

MinimalBox::MinimalBox(const Point& p0, const Point& p1)
{
  bounds_.resize(2);
  bounds_[0] = force3D(p0);
  bounds_[1] = force3D(p1);
}

// accessors
dimen_t MinimalBox::dim() const
{
  if (bounds_.size() > 0)
  { return (bounds_.size() - 1); }
  return 0;
}

std::vector<Point> MinimalBox::vertices() const
{
  std::vector<Point> vertices(number_t(std::pow(2., static_cast<int>(bounds_.size() - 1))));
  switch (bounds_.size())
  {
    case 4: // 3D
      vertices[0] = bounds_[0];
      vertices[1] = bounds_[1];
      vertices[2] = bounds_[1] + bounds_[2] - bounds_[0];
      vertices[3] = bounds_[2];
      vertices[4] = bounds_[3];
      vertices[5] = vertices[1] + bounds_[3] - bounds_[0];
      vertices[6] = vertices[2] + bounds_[3] - bounds_[0];
      vertices[7] = vertices[3] + bounds_[3] - bounds_[0];
      break;
    case 3: // 2D
      vertices[0] = bounds_[0];
      vertices[1] = bounds_[1];
      vertices[2] = bounds_[1] + bounds_[2] - bounds_[0];
      vertices[3] = bounds_[2];
      break;
    default: // 1D
      return bounds_;
  }
  return vertices;
}

Point MinimalBox::boundPt(dimen_t i) const
{
  if (i <= 0 || i > bounds_.size())
  { return Point(0., 0., 0.); }
  return bounds_[i - 1];
}

// extrem points of the bounding box
Point MinimalBox::maxPoint() const   // max point (right,top,back)
{
  number_t n = bounds_.size();
  if (n == 2)
  { return bounds_[1]; }
  else
  {
    Point P(0., 0., 0.);
    for (dimen_t i = 0; i < bounds_.size(); i++)
    { P += bounds_[i] - bounds_[0]; }
    return P;
  }
}

//format boundingbox as string: [a,b]x[c,d]x[e,f]
string_t MinimalBox::asString() const
{
  string_t s = "[";
  if (bounds_.size() > 0)
  {
    s += bounds_[0].roundToZero().toString();
    for (number_t i = 1; i < bounds_.size(); i++)
    { s += ", " + bounds_[i].roundToZero().toString(); }
  }
  s += "]";
  return s;
}

//print facilities
void MinimalBox::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0)
  { return; }
  os << "MinimalBox ";
  os << asString();
}

std::ostream& operator<<(std::ostream& os, const MinimalBox& mb)
{
  mb.print(os);
  return os;
}

//---------------------------------------------------------------------------
// MinimalBox transformations facilities
//---------------------------------------------------------------------------

//! apply a geometrical transformation on a MinimalBox
MinimalBox& MinimalBox::transform(const Transformation& t)
{
  if (t.transformType() == _composition)
  {
    for (number_t j = 0; j < t.components().size(); ++j)
    {
      for (number_t i = 0; i < bounds_.size(); ++i)
      { bounds_[i] = t.component(j)->apply(bounds_[i]); }
    }
  }
  else
  {
    for (number_t i = 0; i < bounds_.size(); ++i)
    { bounds_[i] = t.apply(bounds_[i]); }
  }
  return *this;
}

//! apply a translation on a MinimalBox (1 key)
MinimalBox& MinimalBox::translate(const Parameter& p1)
{
  return this->transform(Translation(p1));
}

//! apply a translation on a MinimalBox (vector version)
MinimalBox& MinimalBox::translate(std::vector<real_t> u)
{
  warning("deprecated", "MinimalBox::translate(Reals)", "MinimalBox::translate(_direction=xxx)");
  return this->transform(Translation(_direction=u));
}

//! apply a translation on a MinimalBox (3 reals version)
MinimalBox& MinimalBox::translate(real_t ux, real_t uy, real_t uz)
{
  warning("deprecated", "MinimalBox::translate(Real, Real, Real)", "MinimalBox::translate(_direction={xxx, yyy, zzz})");
  return this->transform(Translation(_direction={ux, uy, uz}));
}

//! apply a rotation on a MinimalBox (1 key)
MinimalBox& MinimalBox::rotate2d(const Parameter& p1)
{
  return this->transform(Rotation2d(p1));
}

//! apply a rotation on a MinimalBox (2 keys)
MinimalBox& MinimalBox::rotate2d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Rotation2d(p1, p2));
}

//! apply a rotation on a MinimalBox
MinimalBox& MinimalBox::rotate2d(const Point& c, real_t angle)
{
  warning("deprecated", "MinimalBox::rotate2d(Point, Real)", "MinimalBox::rotate2d(_center=xxx, _angle=yyy)");
  return this->transform(Rotation2d(_center=c, _angle=angle));
}

//! apply a rotation on a MinimalBox (1 key)
MinimalBox& MinimalBox::rotate3d(const Parameter& p1)
{
  return this->transform(Rotation3d(p1));
}

//! apply a rotation on a MinimalBox (2 keys)
MinimalBox& MinimalBox::rotate3d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Rotation3d(p1, p2));
}

//! apply a rotation on a MinimalBox (3 keys)
MinimalBox& MinimalBox::rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  return this->transform(Rotation3d(p1, p2, p3));
}

//! apply a rotation on a MinimalBox
MinimalBox& MinimalBox::rotate3d(const Point& c, std::vector<real_t> d, real_t angle)
{
  warning("deprecated", "MinimalBox::rotate3d(Point, Reals, Real)", "MinimalBox::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
  return this->transform(Rotation3d(_center=c, _axis=d, _angle=angle));
}

//! apply a rotation on a MinimalBox
MinimalBox& MinimalBox::rotate3d(real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "MinimalBox::rotate3d(Real, Real, Real)", "MinimalBox::rotate3d(_axis={dx, dy}, _angle=zzz)");
  return this->transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
}

//! apply a rotation on a MinimalBox
MinimalBox& MinimalBox::rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "MinimalBox::rotate3d(Real, Real, Real, Real)", "MinimalBox::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
  return this->transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
}

//! apply a rotation on a MinimalBox
MinimalBox& MinimalBox::rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "MinimalBox::rotate3d(Point, Real, Real, Real)", "MinimalBox::rotate3d(_center=xxx, _axis={dx, dy}, _angle=zzz)");
  return this->transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
}

//! apply a rotation on a MinimalBox
MinimalBox& MinimalBox::rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "MinimalBox::rotate3d(Point, Real, Real, Real, Real)", "MinimalBox::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=zzz)");
  return this->transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
}

//! apply a homothety on a MinimalBox (1 key)
MinimalBox& MinimalBox::homothetize(const Parameter& p1)
{
  return this->transform(Homothety(p1));
}

//! apply a homothety on a MinimalBox (2 keys)
MinimalBox& MinimalBox::homothetize(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Homothety(p1, p2));
}

//! apply a homothety on a MinimalBox
MinimalBox& MinimalBox::homothetize(const Point& c, real_t factor)
{
  warning("deprecated", "MinimalBox::homothetize(Point, Real)", "MinimalBox::homothetize(_center=xxx, _scale=yyy)");
  return this->transform(Homothety(_center=c, _scale=factor));
}

//! apply a homothety on a MinimalBox
MinimalBox& MinimalBox::homothetize(real_t factor)
{
  warning("deprecated", "MinimalBox::homothetize(Real)", "MinimalBox::homothetize(_scale=xxx)");
  return this->transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
}

//! apply a point reflection on a MinimalBox (1 key)
MinimalBox& MinimalBox::pointReflect(const Parameter& p1)
{
  return this->transform(PointReflection(p1));
}

//! apply a point reflection on a MinimalBox
MinimalBox& MinimalBox::pointReflect(const Point& c)
{
  warning("deprecated", "MinimalBox::pointReflect(Point)", "MinimalBox::pointReflect(_center=xxx)");
  return this->transform(PointReflection(_center=c));
}

//! apply a reflection 2D on a MinimalBox (1 key)
MinimalBox& MinimalBox::reflect2d(const Parameter& p1)
{
  return this->transform(Reflection2d(p1));
}

//! apply a reflection 2D on a MinimalBox (2 keys)
MinimalBox& MinimalBox::reflect2d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Reflection2d(p1, p2));
}

//! apply a reflection 2D on a MinimalBox
MinimalBox& MinimalBox::reflect2d(const Point& c, std::vector<real_t> d)
{
  warning("deprecated", "MinimalBox::reflect2d(Point, Reals)", "MinimalBox::reflect2d(_center=xxx, _direction=yyy)");
  return this->transform(Reflection2d(_center=c, _direction=d));
}

//! apply a reflection 2D on a MinimalBox
MinimalBox& MinimalBox::reflect2d(const Point& c, real_t dx, real_t dy)
{
  warning("deprecated", "MinimalBox::reflect2d(Point, Real, Real)", "MinimalBox::reflect2d(_center=xxx, _direction={dx, dy})");
  return this->transform(Reflection2d(_center=c, _direction={dx, dy}));
}

//! apply a reflection 3D on a MinimalBox (1 key)
MinimalBox& MinimalBox::reflect3d(const Parameter& p1)
{
  return this->transform(Reflection3d(p1));
}

//! apply a reflection 3D on a MinimalBox (2 keys)
MinimalBox& MinimalBox::reflect3d(const Parameter& p1, const Parameter& p2)
{
  return this->transform(Reflection3d(p1, p2));
}

//! apply a reflection 3D on a MinimalBox
MinimalBox& MinimalBox::reflect3d(const Point& c, std::vector<real_t> n)
{
  warning("deprecated", "MinimalBox::reflect3d(Point, Reals)", "MinimalBox::reflect2d(_center=xxx, _normal=yyy)");
  return this->transform(Reflection3d(_center=c, _normal=n));
}

//! apply a reflection 3D on a MinimalBox
MinimalBox& MinimalBox::reflect3d(const Point& c, real_t nx, real_t ny, real_t nz)
{
  warning("deprecated", "MinimalBox::reflect3d(Point, Real, Real, Real)", "MinimalBox::reflect2d(_center=xxx, _normal={nx, ny, nz})");
  return this->transform(Reflection3d(_center=c, _normal={nx, ny, nz}));
}

//---------------------------------------------------------------------------
// Geometry member functions and related external functions
//---------------------------------------------------------------------------

void Geometry::buildParam(const Parameter& p)
{
  trace_p->push("Geometry::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_domain_name:
      domName_ = p.get_s();
      break;
    case _pk_varnames:
      theNamesOfVariables_ = p.get_sv();
      break;
    default:
      error("geom_unexpected_param_key", words("param key", key), words("shape", shape_));
  }
  if (domName_.find("#") == 0)
  { error("domain_name_invalid"); }
  if (domName_.find(" ") == 0)
  { error("domain_name_invalid"); }
  trace_p->pop();
}

void Geometry::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Geometry::buildDefaultParam");
  switch (key)
  {
    case _pk_domain_name:
      domName_ = "";
      break;
    case _pk_varnames:
    {
      theNamesOfVariables_.resize(dim_);
      if (dim_ > 0)
      { theNamesOfVariables_[0] = "x"; }
      if (dim_ > 1)
      { theNamesOfVariables_[1] = "y"; }
      if (dim_ > 2)
      { theNamesOfVariables_[2] = "z"; }
      break;
    }
    default: error("geom_unexpected_param_key", words("param key", key), words("shape", shape_));
  }
  trace_p->pop();
}

std::set<ParameterKey> Geometry::getParamsKeys()
{
  std::set<ParameterKey> params;
  params.insert(_pk_domain_name);
  params.insert(_pk_varnames);
  return params;
}

// constructors
Geometry::Geometry(): force(false), crackable(false), crackType(_noCrack), crackDomNameToOpen(""), isPlaneSurface(true), domName_(""), dim_(0), shape_(_noShape), nbParts_(0), extrusionData_(nullptr), parametrization_(nullptr), boundaryParametrization_(nullptr), boundaryGeometry_(nullptr), auxiliaryGeometry_(nullptr), geoNode_(nullptr)
{
#ifdef XLIFEPP_WITH_OPENCASCADE
  initOC();
#endif
}

Geometry::Geometry(const BoundingBox& bb, const string_t& na, ShapeType sh, const string_t& nx, const string_t& ny, const string_t& nz)
  : boundingBox(bb), minimalBox(), force(false), crackable(false), crackType(_noCrack), crackDomNameToOpen(""), isPlaneSurface(true), domName_(na), dim_(bb.dim()), shape_(sh), nbParts_(0), extrusionData_(nullptr), parametrization_(nullptr), boundaryParametrization_(nullptr), boundaryGeometry_(nullptr), auxiliaryGeometry_(nullptr), geoNode_(nullptr)
{
  init(nx, ny, nz);
}

Geometry::Geometry(const BoundingBox& bb, dimen_t d, const string_t& na, ShapeType sh, const string_t& nx, const string_t& ny, const string_t& nz)
  : boundingBox(bb), minimalBox(), force(false), crackable(false), crackType(_noCrack), crackDomNameToOpen(""), isPlaneSurface(true), domName_(na), dim_(d), shape_(sh), nbParts_(0), extrusionData_(nullptr), parametrization_(nullptr), boundaryParametrization_(nullptr), boundaryGeometry_(nullptr), auxiliaryGeometry_(nullptr), geoNode_(nullptr)
{
  init(nx, ny, nz);
}

Geometry::Geometry(dimen_t d, const string_t& na, ShapeType sh, const string_t& nx, const string_t& ny, const string_t& nz) // create the unit box as default box
  : minimalBox(), force(false), crackable(false), crackType(_noCrack), crackDomNameToOpen(""), isPlaneSurface(true), domName_(na), dim_(d), shape_(sh), nbParts_(0), extrusionData_(nullptr), parametrization_(nullptr), boundaryParametrization_(nullptr), boundaryGeometry_(nullptr), auxiliaryGeometry_(nullptr), geoNode_(nullptr)
{
  if (d == 1) { boundingBox = BoundingBox(0., 1.); }
  if (d == 2) { boundingBox = BoundingBox(0., 1., 0., 1.); }
  if (d == 3) { boundingBox = BoundingBox(0., 1., 0., 1., 0., 1.); }
  init(nx, ny, nz);
}

//!< constructor from file (only Brep up to now)
Geometry::Geometry(const string_t& fn) : force(false), crackable(false), crackType(_noCrack), crackDomNameToOpen(""), isPlaneSurface(true), domName_(""), dim_(0), shape_(_fromFile), nbParts_(0), extrusionData_(nullptr), parametrization_(nullptr), boundaryParametrization_(nullptr), boundaryGeometry_(nullptr), auxiliaryGeometry_(nullptr), geoNode_(nullptr)
{
  if (lowercase(fileExtension(fn)) == "brep")
  {
#ifdef XLIFEPP_WITH_OPENCASCADE
    initOC();
    loadFromBrep(fn);
    initName();
#else
    error("free_error", "importing brep file as Geometry requires Open Cascade to be available");
#endif
  }
  else { error("free_error", "up to now, only brep file can be imported as Geometry"); }
}

//!< init variable names, check domName  (dim_ has to be already set !!!)
void Geometry::init(const string_t& nx, const string_t& ny, const string_t& nz)
{
  initName(nx, ny, nz);
#ifdef XLIFEPP_WITH_OPENCASCADE
  initOC();
#endif
}

//!< init variable names, check domName, set oc pointer null (dim_ has to be already set)
void Geometry::initName(const string_t& nx, const string_t& ny, const string_t& nz)
{
  if (theNamesOfVariables_.size() == 0 && dim_ > 0)
  {
    theNamesOfVariables_.resize(dim_);
    if (dim_ > 0) { theNamesOfVariables_[0] = nx; }
    if (dim_ > 1) { theNamesOfVariables_[1] = ny; }
    if (dim_ > 2) { theNamesOfVariables_[2] = nz; }
  }
  if (domName_.find("#") == 0) { error("domain_name_invalid"); }
  if (domName_.find(" ") == 0) { error("domain_name_invalid"); }
}

#ifdef XLIFEPP_WITH_OPENCASCADE
void Geometry::initOC()
{
  ocData_ = nullptr;
}
#endif

//destructor
Geometry::~Geometry()
{
  std::map<number_t, Geometry*>::iterator it_c;
  for (it_c = components_.begin(); it_c != components_.end(); ++it_c)
  {
    if (it_c->second != nullptr)
    { delete it_c->second; }
  }
  if (extrusionData_ != nullptr) { delete extrusionData_; }
  if (geoNode_ != nullptr) { delete geoNode_; }
  if (auxiliaryGeometry_ != nullptr)
  {
    if (auxiliaryGeometry_->parametrization_ == parametrization_) { parametrization_ = nullptr; } //to avoid double delete
    if (auxiliaryGeometry_->boundaryParametrization_ == boundaryParametrization_) { boundaryParametrization_ = nullptr; } //to avoid double delete
    delete auxiliaryGeometry_;
  }
  if (boundaryGeometry_ != nullptr)
  {
    if (boundaryGeometry_->parametrization_ == boundaryParametrization_) { boundaryParametrization_ = nullptr; } //to avoid double delete
    delete boundaryGeometry_;
  }
  if (parametrization_ != nullptr) { delete parametrization_; }
  if (boundaryParametrization_ != nullptr) { delete boundaryParametrization_; }

#ifdef XLIFEPP_WITH_OPENCASCADE
  clearOC();
#endif
}

Geometry::Geometry(const Geometry& g) : boundingBox(g.boundingBox), minimalBox(g.minimalBox), force(g.force), crackable(g.crackable),
  crackType(g.crackType), crackDomNameToOpen(g.crackDomNameToOpen), isPlaneSurface(g.isPlaneSurface), domName_(g.domName_),
  dim_(g.dim_), shape_(g.shape_), sideNames_(g.sideNames_),  theNamesOfVariables_(g.theNamesOfVariables_), teXFilename_(g.teXFilename_)
{
  //copy components (deep)
  std::map<Geometry*, Geometry*> newgeo;
  for (std::map<number_t, Geometry*>::const_iterator it_c = g.components_.begin(); it_c != g.components_.end(); ++it_c)
  {
    Geometry* newg = it_c->second->clone();
    newgeo[it_c->second] = newg;
    components_[it_c->first] = newg;
  }

  // copy loops and geometries
  geometries_ = g.geometries_;
  loops_ = g.loops_;

  //copy extrusion data
  if (g.extrusionData_ != nullptr)  { extrusionData_ = new ExtrusionData(*g.extrusionData_); }
  else  { extrusionData_ = nullptr; }

  //copy geonode data
  if (g.geoNode_ != nullptr)
  {
    geoNode_ = new GeoNode(*g.geoNode_);
    geoNode_->updateGeo(newgeo);
  }
  else { geoNode_ = nullptr; }

  //copy auxiliaryGeometry
  auxiliaryGeometry_ = nullptr;
  if (g.auxiliaryGeometry_ != nullptr) { auxiliaryGeometry_ = g.auxiliaryGeometry_->clone(); }

  //copy boundaryGeometry
  boundaryGeometry_ = nullptr;
  if (g.boundaryGeometry_ != nullptr) { boundaryGeometry_ = g.boundaryGeometry_->clone(); }

  //copy parametrization data
  if (g.parametrization_ != nullptr) //deep copy
  {
    parametrization_ = new Parametrization(*g.parametrization_);
    parametrization_->geomP() = this;
    if (parametrization_->params.contains("geometry")) { parametrization_->params("geometry") = reinterpret_cast<const void*>(this); }
    else { parametrization_->params << Parameter(reinterpret_cast<const void*>(this), "geometry"); }
  }
  else { parametrization_ = nullptr; }

  if (g.boundaryParametrization_ != nullptr)
  {
    boundaryParametrization_ = new Parametrization(*g.boundaryParametrization_);
    boundaryParametrization_->geomP() = this;
    if (boundaryParametrization_->params.contains("geometry")) { boundaryParametrization_->params("geometry") = reinterpret_cast<const void*>(this); }
    else { boundaryParametrization_->params << Parameter(reinterpret_cast<const void*>(this), "geometry"); }
  }
  else  { boundaryParametrization_ = nullptr; }
  nbParts_ = g.nbParts_;

#ifdef XLIFEPP_WITH_OPENCASCADE
  ocData_ = nullptr;
  cloneOC(g);
#endif
}

//! assign operator = (deep copy)
Geometry& Geometry::operator=(const Geometry& g)
{
  if (this == &g) { return *this; }

  boundingBox = g.boundingBox;
  minimalBox = g.minimalBox;

  force = g.force;
  crackable = g.crackable;
  crackDomNameToOpen = g.crackDomNameToOpen;
  isPlaneSurface = g.isPlaneSurface;

  domName_ = g.domName_;
  dim_ = g.dim_;
  shape_ = g.shape_;

  sideNames_ = g.sideNames_;
  theNamesOfVariables_ = g.theNamesOfVariables_;
  teXFilename_ = g.teXFilename_;
  for (std::map<number_t, Geometry*>::const_iterator it_c = components_.begin(); it_c != components_.end(); ++it_c)
  { delete it_c->second; }
  components_.clear();
  std::map<Geometry*, Geometry*> newgeo;
  for (std::map<number_t, Geometry*>::const_iterator it_c = g.components_.begin(); it_c != g.components_.end(); ++it_c)
  {
    Geometry* newg = it_c->second->clone();
    newgeo[it_c->second] = newg;
    components_[it_c->first] = newg;
  }

  geometries_ = g.geometries_;
  loops_ = g.loops_;

  //copy extrusion data
  if (extrusionData_ != nullptr) { delete extrusionData_; }
  if (g.extrusionData_ != nullptr) { extrusionData_ = new ExtrusionData(*g.extrusionData_); }
  else  { extrusionData_ = nullptr; }

  //copy geonode data
  if (geoNode_ != nullptr) { delete geoNode_; }
  if (g.geoNode_ != nullptr)
  {
    geoNode_ = new GeoNode(*g.geoNode_);
    geoNode_->updateGeo(newgeo);
  }
  else { geoNode_ = nullptr; }

  //copy auxiliaryGeometry
  auxiliaryGeometry_ = nullptr;
  if (g.auxiliaryGeometry_ != nullptr) { auxiliaryGeometry_ = g.auxiliaryGeometry_->clone(); }

  //copy auxiliaryGeometry
  boundaryGeometry_ = nullptr;
  if (g.boundaryGeometry_ != nullptr) { boundaryGeometry_ = g.boundaryGeometry_->clone(); }

  //copy parametrization
  if (parametrization_ != nullptr)  { delete parametrization_; } // pointer not shared
  if (g.parametrization_ != nullptr)
  { parametrization_  = new Parametrization(*g.parametrization_); }
  else  { parametrization_ = nullptr; }

  if (boundaryParametrization_ != nullptr) { delete boundaryParametrization_; } // pointer not shared
  if (g.boundaryParametrization_ != nullptr) { boundaryParametrization_ = new Parametrization(*g.boundaryParametrization_); }
  else  { boundaryParametrization_ = nullptr; }

#ifdef XLIFEPP_WITH_OPENCASCADE
  clearOC();
  cloneOC(g);
#endif

  return *this;
}

bool Geometry::operator==(const Geometry& g) const
{
  if (this == &g)  { return true; }

  // test shapes
  if (shape_ != g.shape_) { return false; }

  // if composite ignore
  if (shape_ == _composite)  { error("nosuchcase", "operator==", "composite"); }

  // if loop test each border
  if (shape_ == _loop)
  {
    number_t n = components_.size();
    if (n != g.components_.size()) { return false; }
    for (number_t i = 0; i < n; ++i)
    {
      bool found = false;
      for (number_t j = 0; j < n; ++j)
      {
        if (*(g.components_.at(j)) == *(components_.at(i)))
        {
          found = true;
          break;
        }
      }
      if (!found)  { return false; }
    }
    return true;
  }

  // shape_ is not _composite nor _loop: it is a canonical geometry
  std::vector<const Point*> tnodes = nodes();
  std::vector<const Point*> gnodes = g.nodes();
  for (number_t i = 0; i < tnodes.size(); ++i)
  {
    // we should test if the list of nodes is a cyclic permutation of the other list of nodes
    if (*tnodes[i] != *gnodes[i])
    { return false; }
  }

  return true;
}

Geometry& Geometry::boundary() const
{
  if (boundaryGeometry_ == nullptr) { return buildBoundary(); }
  return *boundaryGeometry_;
}

const Parametrization& Geometry::parametrization() const
{
  if (parametrization_ == nullptr) { buildParametrization(); }
  return *parametrization_;
}

const Parametrization& Geometry::boundaryParametrization() const
{
  if (boundaryParametrization_ == nullptr) //try to construct it
  {
    if (boundaryGeometry_ == nullptr) { buildBoundary(); }
    if (boundaryGeometry_ == nullptr) { error("free_error", "Geometry::boundaryParametrization: unable to build boundary geometry"); }
    boundaryParametrization_ = &boundaryGeometry_->buildParametrization();
  }
  return *boundaryParametrization_;
}

void Geometry::setParametrization(const Parametrization& par)
{
  if (parametrization_ != nullptr) { delete parametrization_; }
  parametrization_ = new Parametrization(par);
}

void Geometry::setBoundaryParametrization(const Parametrization& par)
{
  if (boundaryParametrization_ != nullptr) { delete boundaryParametrization_; }
  boundaryParametrization_ = new Parametrization(par);
}

std::vector<const Point*> Geometry::boundNodes() const
{
  std::vector<const Point*> nodes;
  return nodes;
}

std::vector<Point*> Geometry::nodes()
{
  return std::vector<Point*>();  //fake return, void vector
}

std::vector<const Point*> Geometry::nodes() const
{
  return std::vector<const Point*>();  //fake return, void vector
}

// return last node index (index starting from 1)
number_t Geometry::lastNodeIndex() const
{
  return nodes().size();   //0 means fake return
}

Point Geometry::firstNode() const
{
  std::vector<const Point*> nds = nodes();
  if (nds.size() > 0) { return *nds[0]; }
  std::map<number_t, Geometry*>::const_iterator itm = components_.begin();
  for (; itm != components_.end(); ++itm) //case of a composite/loop geometry
  {
    Point p = itm->second->firstNode();
    if (p.size() > 0) { return p; }
  }
  return Point();  //point of 0 dimension
}

dimen_t Geometry::dimPoint() const
{
  std::vector<const Point*> nds = nodes();
  if (nds.size() > 0) { return nds[0]->size(); }  //case of a canonical geometry
  if (components_.size() > 0) { return components_.begin()->second->dimPoint(); } //case of a composite/loop geometry
  where("Geometry::dimPoint()");
  error("not_handled_return", tostring(0));
  return 0;
}

GeometricGeodesic Geometry::geodesic(const Point& x, const Point& dx, bool wCA, bool wT)
{ error("free_error", "GeometricGeodesic not available for " + words("shape", shape_)); return GeometricGeodesic();}

// collect canonical geometry with name n, loop on components or test name if not a composite geometry
void Geometry::collect(const string_t& n, std::list<Geometry*>& geoms) const
{
  if (domName_ == n)
  { geoms.push_back(const_cast<Geometry*>(this)); } //canonical geometry
  else if (components_.size() > 0) // composite geometry
  {
    std::map<number_t, Geometry*>::iterator itc = components_.begin();
    for (; itc != components_.end(); ++itc) { itc->second->collect(n, geoms); }
  }
}

// return geometry with name n,
// geometry may be a canonical geometry or a composite one with only some canonical geometries
Geometry* Geometry::find(const string_t& n) const
{
  std::list<Geometry*> geoms;
  collect(n, geoms);
  if (geoms.size() == 0) { return 0; }             // no geometry found
  if (geoms.size() == 1) { return *geoms.begin(); } // return the basic geometry
  Geometry* g = new Geometry();               // create a new composite geometry
  g->shape(_composite);
  g->domName_ = n;
  std::list<Geometry*>::iterator itg = geoms.begin();
  number_t k = 0;
  for (; itg != geoms.end(); ++itg, ++k)
  {
    Geometry* gk = (*itg)->clone();
    g->dim_ = std::max(g->dim_, gk->dim_);
    g->components_[k] = gk;
    g->boundingBox += gk->boundingBox;
    g->minimalBox = MinimalBox(g->boundingBox.bounds());
    g->geometries_[k].push_back(k);
  }
  return g;
}

//! clear components_, geometries_, loops_ and connectedParts_ data
void Geometry::clearCompositeData()
{
  std::map<number_t, Geometry*>::iterator itc = components_.begin();
  for (; itc != components_.end(); ++itc)
    if (itc->second != this) { delete itc->second; }
  components_.clear();
  geometries_.clear();
  loops_.clear();
  connectedParts_.clear();
  sideNames_.clear();
}

//! clear extrusionData_
void Geometry::clearExtrusionData()
{
  if (extrusionData_ != nullptr) { delete extrusionData_; }
  extrusionData_ = nullptr;
}

//! reset parametrization pointers
void Geometry::clearParametrization()
{
  parametrization_ = nullptr; // NOT DELETED
  boundaryParametrization_ = nullptr; //NOT DELETED
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Geometry::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves;
  if (dim_ == 2)
  {
    if (shape_ == _loop)
    {
      for (std::map<number_t, Geometry*>::const_iterator it_c = components_.begin(); it_c != components_.end(); ++it_c)
      { curves.push_back(std::make_pair(it_c->second->shape(), it_c->second->boundNodes())); }
    }
    else if (shape_ == _composite)
    {
      for (std::map<number_t, Geometry*>::const_iterator it_c = components_.begin(); it_c != components_.end(); ++it_c)
      {
        if (it_c->second->dim() == 2)
        {
          std::vector<std::pair<ShapeType, std::vector<const Point*> > > ccurves = it_c->second->curves();
          for (number_t i = 0; i < ccurves.size(); ++i)
          { curves.push_back(ccurves[i]); }
        }
      }
    }
  }
  return curves;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Geometry::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs;
  if (dim_ == 3)
  {
    if (shape_ == _loop)
    {
      for (std::map<number_t, Geometry*>::const_iterator it_c = components_.begin(); it_c != components_.end(); ++it_c)
      {
        std::vector<std::pair<ShapeType, std::vector<const Point*> > > csurfs = it_c->second->surfs();
        for (number_t i = 0; i < csurfs.size(); ++i)
        { surfs.push_back(csurfs[i]); }
      }
    }
    else if (shape_ == _composite)
    {
      for (std::map<number_t, Geometry*>::const_iterator it_c = components_.begin(); it_c != components_.end(); ++it_c)
      {
        if (it_c->second->dim() == 3)
        {
          std::vector<std::pair<ShapeType, std::vector<const Point*> > > csurfs = it_c->second->surfs();
          for (number_t i = 0; i < csurfs.size(); ++i)
          { surfs.push_back(csurfs[i]); }
        }
      }
    }
  }
  return surfs;
}

number_t Geometry::nbSides() const
{
  number_t nbSidesOfBasis = 0, nbComponents = 0;
  if (shape() == _extrusion)
  {
    nbSidesOfBasis = components_[0]->nbSides();
    switch (components_[0]->shape())
    {
      case _composite:
        nbComponents = components_[0]->geometries_.size();
        break;
      case _loop:
      default:
        nbComponents = 1;
        break;
    }
    switch (extrusionData_->extrusion()->transformType())
    {
      case _translation:
        return nbSidesOfBasis + 2 * nbComponents;
      case _rotation2d:
      {
        real_t angle = extrusionData_->extrusion()->rotation2d()->angle();
        if (angle == 2.*pi_)
        { return 4 * nbSidesOfBasis; }
        else if (angle >= pi_)
        { return 2 * nbSidesOfBasis + 2 * nbComponents; }
        else
        { return nbSidesOfBasis + 2; }
        break;
      }
      case _rotation3d:
      {
        real_t angle = extrusionData_->extrusion()->rotation3d()->angle();
        if (angle == 2.*pi_)
        { return 4 * nbSidesOfBasis; }
        else if (angle >= pi_)
        { return 2 * nbSidesOfBasis + 2 * nbComponents; }
        else
        { return nbSidesOfBasis + 2 * nbComponents; }
        break;
      }
      default:
      {
        where("Geometry::nbSides");
        error("gmsh_extrusion_not_handled", words("transform", extrusionData_->extrusion()->transformType()));
        break;
      }
    }
  }
  else if (shape() == _loop)
  {
    // for a loop geometry, number of sides is the number of components
    return loops_.at(components_.size()).size();
  }
  else if (shape() == _composite)
  {
    // first, we have to determine which components have to be considered
    // in fact, every component that is not a border of a loop has to be considered
    std::set<number_t> nums;
    for (number_t i = 0; i < components_.size(); ++i)
    { nums.insert(i); }

    for (std::map<number_t, std::vector<number_t> >::const_iterator it_l = loops_.begin(); it_l != loops_.end(); ++it_l)
    {
      for (number_t i = 0; i < it_l->second.size(); ++i)
      { nums.erase(it_l->second[i]); }
    }

    // then, we add number of sides of each component that is considered
    for (std::set<number_t>::const_iterator it_n = nums.begin(); it_n != nums.end(); ++it_n)
    { nbSidesOfBasis += components_[*it_n]->nbSides(); }

    return nbSidesOfBasis;
  }
  else
  {
    where("Geometry::nbSides");
    error("not_canonical", domName());
  }
  return 0;
}

number_t Geometry::nbLateralSidesForExtrusion() const
{
  number_t nbSidesOfBasis = 0, nbCurvesOnAxis = 0;
  if (shape() == _extrusion)
  {
    nbSidesOfBasis = components_[0]->nbSides();
    switch (extrusionData_->extrusion()->transformType())
    {
      case _translation: return nbSidesOfBasis;
      case _rotation2d:
      {
        // we have to check if one border of the base geometry is not on the rotation axis to eventually update nbSidesOfBasis
        std::vector<std::pair<ShapeType, std::vector<const Point*> > > bcurves = components_[0]->curves();

        for (number_t i = 0; i < bcurves.size(); ++i)
        {
          if (bcurves[i].first == _segment)
          {
            Point axis = force3D(*bcurves[i].second[1] - *bcurves[i].second[0]);
            if (pointDistance(force3D(crossProduct(axis, Point(0., 0., 1.))), Point(0., 0., 0.)) < theEpsilon)
            {
              Point tmp = force3D(*bcurves[i].second[1] - extrusionData_->extrusion()->rotation2d()->center());
              if (pointDistance(crossProduct(tmp, axis), Point(0., 0., 0.)) < theEpsilon) { nbCurvesOnAxis++; }
            }
          }
        }
        nbSidesOfBasis -= nbCurvesOnAxis;

        real_t angle = extrusionData_->extrusion()->rotation2d()->angle();
        if (angle == 2.*pi_) { return 4 * nbSidesOfBasis; }
        else if (angle >= pi_) { return 2 * nbSidesOfBasis; }
        else { return nbSidesOfBasis; }
        break;
      }
      case _rotation3d:
      {
        // we have to check if one border of the base geometry is not on the rotation axis to eventually update nbSidesOfBasis
        std::vector<std::pair<ShapeType, std::vector<const Point*> > > bcurves = components_[0]->curves();

        for (number_t i = 0; i < bcurves.size(); ++i)
        {
          if (bcurves[i].first == _segment)
          {
            Point axis = force3D(*bcurves[i].second[1] - *bcurves[i].second[0]);
            if (pointDistance(force3D(crossProduct(axis, force3D(extrusionData_->extrusion()->rotation3d()->axisDirection()))), Point(0., 0., 0.)) < theEpsilon)
            {
              Point tmp = force3D(*bcurves[i].second[1] - extrusionData_->extrusion()->rotation3d()->axisPoint());
              if (pointDistance(crossProduct(tmp, axis), Point(0., 0., 0.)) < theEpsilon)
              { nbCurvesOnAxis++; }
            }
          }
        }
        nbSidesOfBasis -= nbCurvesOnAxis;

        real_t angle = extrusionData_->extrusion()->rotation3d()->angle();
        if (angle == 2.*pi_)
        { return 4 * nbSidesOfBasis; }
        else if (angle >= pi_)
        { return 2 * nbSidesOfBasis; }
        else
        { return nbSidesOfBasis; }
        break;
      }
      default:
      {
        where("Geometry::nbLateralSidesForExtrusion");
        error("gmsh_extrusion_not_handled", words("transform", extrusionData_->extrusion()->transformType()));
        break;
      }
    }
  }
  else
  {
    where("Geometry::nbLateralSidesForExtrusion");
    error("extrusion_only", domName());
  }
  return 0;
}

bool Geometry::isCoplanar(const Geometry& g) const
{
  if (shape() == _extrusion || shape() == _composite)
  {
    where("Geometry::isCoplanar");
    error("canonical_or_loop_only", domName());
  }
  if (g.shape() == _extrusion || g.shape() == _composite)
  {
    where("Geometry::isCoplanar");
    error("canonical_or_loop_only", g.domName());
  }

  std::vector<const Point*> tnodes = boundNodes(), gnodes = g.boundNodes();
  std::set<const Point*> tsnodes, gsnodes;

  if (shape() == _loop)
  {
    std::map<number_t, Geometry*>::const_iterator it_g;
    for (it_g = components_.begin(); it_g != components_.end(); ++it_g)
    {
      for (number_t i = 0; i < it_g->second->boundNodes().size(); ++ i)
      { tsnodes.insert(it_g->second->boundNodes()[i]); }
    }
    std::set<const Point*>::const_iterator it_s;
    for (it_s = tsnodes.begin(); it_s != tsnodes.end(); ++it_s)
    { tnodes.push_back(*it_s); }
  }
  if (g.shape() == _loop)
  {
    std::map<number_t, Geometry*>::const_iterator it_g;
    for (it_g = g.components_.begin(); it_g != g.components_.end(); ++it_g)
    {
      for (number_t i = 0; i < it_g->second->boundNodes().size(); ++ i)
      { gsnodes.insert(it_g->second->boundNodes()[i]); }
    }
    std::set<const Point*>::const_iterator it_s;
    for (it_s = gsnodes.begin(); it_s != gsnodes.end(); ++it_s)
    { gnodes.push_back(*it_s); }
  }

  if (g.minimalBox.dim() == 1)
  { return true; }
  else if (g.minimalBox.dim() == 2)
  {
    for (number_t i = 0; i < tnodes.size(); ++i)
    {
      if (!arePointsCoplanar(g.minimalBox.vertices()[0], g.minimalBox.vertices()[1], g.minimalBox.vertices()[2], force3D(*tnodes[i])))
      { return false; }
    }
  }
  else
  {
    // g is not a Plane !!!
    return false;
  }
  return true;
}

bool Geometry::isInside(const Geometry& g) const
{
  if (minimalBox.dim() > g.minimalBox.dim())
  { return false; }

  real_t x, y, z;
  Point p, q, q2, q3;
  std::vector<const Point*> tnodes = boundNodes(), gnodes = g.boundNodes();
  const std::vector<Point>& tmbnodes = minimalBox.vertices();
  const std::vector<Point>& gmbnodes = g.minimalBox.vertices();
  bool isInside = true;

  // general remark: minimal box points are necessarily 3D, so that we have to force points to be 3D to
  //                  add, substract, ... them. This is the reason of the function force3D when necessary
  switch (g.shape())
  {
    case _ellipsoid:
    case _ball:
    {
      // anything inside ellipsoid3d / ball ?
      //
      // Reminder:
      //   - tnodes are the bound nodes of this, the geometry we try to determine if it is included in g
      //   - gnodes are the bound nodes of g
      //
      // we test each vertex of the minimal box.
      // - changing coordinates:
      //       new origin = center of the ellipsoid
      //       new axes = axes of the minimal box
      // - if x^2+y^2+z^2 > 1 the point is outside the ellipsoid, so we check the geometry
      for (number_t i = 0; i < tmbnodes.size(); i++)
      {
        p = tmbnodes[i] - 0.5 * (*gnodes[0] + *gnodes[2]);
        q = *gnodes[0] - 0.5 * (*gnodes[0] + *gnodes[2]);
        x = dot(p, q) / dot(q, q);
        q = *gnodes[1] - 0.5 * (*gnodes[0] + *gnodes[2]);
        y = dot(p, q) / dot(q, q);
        q = *gnodes[5] - 0.5 * (*gnodes[0] + *gnodes[2]);
        z = dot(p, q) / dot(q, q);
        if (x * x + y * y + z * z >= 1)
        {
          isInside = false;
          break;
        }
      }

      if (!isInside)  // if one corner of the minimal box is outside, check geometry directly
      {
        isInside = true;
        for (number_t i = 0; i < tnodes.size(); i++)
        {
          p = force3D(*tnodes[i]) - 0.5 * (*gnodes[0] + *gnodes[2]);
          q = *gnodes[0] - 0.5 * (*gnodes[0] + *gnodes[2]);
          x = dot(p, q) / dot(q, q);
          q = *gnodes[1] - 0.5 * (*gnodes[0] + *gnodes[2]);
          y = dot(p, q) / dot(q, q);
          q = *gnodes[5] - 0.5 * (*gnodes[0] + *gnodes[2]);
          z = dot(p, q) / dot(q, q);
          if (x * x + y * y + z * z >= 1)
          {
            isInside = false;
            break;
          }
        }
      }
      break;
    }
    case _ellipse:
    case _disk:
    {
      // anything inside ellipse2d / disk ?
      //
      // Reminder:
      //   - tnodes are the bound nodes of this, the geometry we try to determine if it is included in g
      //   - gnodes are the bound nodes of g
      //
      // we test each vertex of the minimal box.
      // - changing coordinates:
      //       new origin = center of the ellipse
      //       new axes = axes of the minimal box
      // - if x^2+y^2 > 1 the point is outside the ellipse, so we check the geometry
      if (isCoplanar(g))
      {
        for (number_t i = 0; i < tmbnodes.size(); i++)
        {
          p = tmbnodes[i] - 0.5 * force3D(*gnodes[0] + *gnodes[2]);
          q = *gnodes[0] - 0.5 * (*gnodes[0] + *gnodes[2]);
          x = dot(p, force3D(q)) / dot(q, q);
          q = *gnodes[1] - 0.5 * (*gnodes[0] + *gnodes[2]);
          y = dot(p, force3D(q)) / dot(q, q);
          if (x * x + y * y >= 1)
          {
            isInside = false;
            break;
          }
        }

        if (!isInside)  // if one corner of the minimal box is outside, check geometry directly
        {
          isInside = true;
          for (number_t i = 0; i < tnodes.size(); i++)
          {
            p = force3D(*tnodes[i]) - 0.5 * force3D(*gnodes[0] + *gnodes[2]);
            q = *gnodes[0] - 0.5 * (*gnodes[0] + *gnodes[2]);
            x = dot(p, force3D(q)) / dot(q, q);
            q = *gnodes[1] - 0.5 * (*gnodes[0] + *gnodes[2]);
            y = dot(p, force3D(q)) / dot(q, q);
            if (x * x + y * y >= 1)
            {
              isInside = false;
              break;
            }
          }
        }
      }
      else
      { isInside = false; }
      break;
    }
    case _parallelepiped:
    case _cuboid:
    case _cube:
    {
      // anything inside parallelepiped / cube ?
      //
      // Reminder:
      //   - tnodes are the bound nodes of this, the geometry we try to determine if it is included in g
      //   - gnodes are the bound nodes of g
      //
      // we test each vertex of the minimal box.
      // - changing coordinates:
      //       new origin = origin of the minimal box
      //       new axes = axes of the minimal box
      // - if coordinates of vertex are between 0 and 1, it is inside
      real_t a, b, c, d, e, ap, bp, cp, dp, as, bs, cs, ds;
      for (number_t i = 0; i < tmbnodes.size(); i++)
      {
        p = tmbnodes[i] - *gnodes[0];
        q = *gnodes[1] - *gnodes[0];
        q2 = *gnodes[3] - *gnodes[0];
        q3 = *gnodes[4] - *gnodes[0];
        a = dot(q, q);
        b = dot(q, q2);
        c = dot(q, q3);
        d = dot(p, q);
        ap = dot(q, q2);
        bp = dot(q2, q2);
        cp = dot(q2, q3);
        dp = dot(p, q2);
        as = dot(q, q3);
        bs = dot(q2, q3);
        cs = dot(q3, q3);
        ds = dot(p, q3);
        e = a * bp * cs + ap * bs * c + as * b * cp - c * bp * as - cp * bs * a - cs * b * ap;
        x = (d * bp * cs + dp * bs * c + ds * b * cp - c * bp * ds - cp * bs * d - cs * b * dp) / e;
        y = (a * dp * cs + ap * ds * c + as * d * cp - c * dp * as - cp * ds * a - cs * d * ap) / e;
        z = (a * bp * ds + ap * bs * d + as * b * dp - d * bp * as - dp * bs * a - ds * b * ap) / e;
        if (x <= 0. || x >= 1. || y <= 0. || y >= 1. || z <= 0. || z >= 1.)
        {
          isInside = false;
          break;
        }
      }

      if (!isInside)  // if one corner of the minimal box is outside, check geometry directly
      {
        isInside = true;
        for (number_t i = 0; i < tnodes.size(); i++)
        {
          p = force3D(*tnodes[i]) - *gnodes[0];
          q = *gnodes[1] - *gnodes[0];
          q2 = *gnodes[3] - *gnodes[0];
          q3 = *gnodes[4] - *gnodes[0];
          a = dot(q, q);
          b = dot(q, q2);
          c = dot(q, q3);
          d = dot(p, q);
          ap = dot(q, q2);
          bp = dot(q2, q2);
          cp = dot(q2, q3);
          dp = dot(p, q2);
          as = dot(q, q3);
          bs = dot(q2, q3);
          cs = dot(q3, q3);
          ds = dot(p, q3);
          e = a * bp * cs + ap * bs * c + as * b * cp - c * bp * as - cp * bs * a - cs * b * ap;
          x = (d * bp * cs + dp * bs * c + ds * b * cp - c * bp * ds - cp * bs * d - cs * b * dp) / e;
          y = (a * dp * cs + ap * ds * c + as * d * cp - c * dp * as - cp * ds * a - cs * d * ap) / e;
          z = (a * bp * ds + ap * bs * d + as * b * dp - d * bp * as - dp * bs * a - ds * b * ap) / e;
          if (x <= 0. || x >= 1. || y <= 0. || y >= 1. || z <= 0. || z >= 1.)
          {
            isInside = false;
            break;
          }
        }
      }
      break;
    }
    case _parallelogram:
    case _rectangle:
    case _square:
    {
      // anything inside rectangle / square ?
      //
      // Reminder:
      //   - tnodes are the bound nodes of this, the geometry we try to determine if it is included in g
      //   - gnodes are the bound nodes of g
      //
      // we test each vertex of the minimal box.
      // - changing coordinates:
      //       new origin = origin of the minimal box
      //       new axes = axes of the minimal box
      // - if coordinates of vertex are between 0 and 1, it is inside
      if (isCoplanar(g))
      {
        real_t a, b, c, ap, bp, cp;
        for (number_t i = 0; i < tmbnodes.size(); i++)
        {
          p = tmbnodes[i] - force3D(*gnodes[0]);
          q = *gnodes[1] - *gnodes[0];
          q2 = *gnodes[3] - *gnodes[0];
          a = dot(q, q);
          b = dot(q, q2);
          c = dot(p, force3D(q));
          ap = dot(q, q2);
          bp = dot(q2, q2);
          cp = dot(p, force3D(q2));
          x = (c * bp - cp * b) / (a * bp - ap * b);
          y = (a * cp - ap * c) / (a * bp - ap * b);
          if (x <= 0. || x >= 1. || y <= 0. || y >= 1.)
          {
            isInside = false;
            break;
          }
        }
        if (!isInside)  // if one corner of the minimal box is outside, check geometry directly
        {
          isInside = true;
          for (number_t i = 0; i < tnodes.size(); i++)
          {
            p = force3D(*tnodes[i]) - force3D(*gnodes[0]);
            q = *gnodes[1] - *gnodes[0];
            q2 = *gnodes[3] - *gnodes[0];
            a = dot(q, q);
            b = dot(q, q2);
            c = dot(p, force3D(q));
            ap = dot(q, q2);
            bp = dot(q2, q2);
            cp = dot(p, force3D(q2));
            x = (c * bp - cp * b) / (a * bp - ap * b);
            y = (a * cp - ap * c) / (a * bp - ap * b);
            if (x <= 0. || x >= 1. || y <= 0. || y >= 1.)
            {
              isInside = false;
              break;
            }
          }
        }
      }
      else
      { isInside = false; }
      break;
    }
    case _revCylinder:
    {
      // anything inside revolution cylinder3d ?
      //
      // Reminder:
      //   - tnodes are the bound nodes of this, the geometry we try to determine if it is included in g
      //   - gnodes are the bound nodes of g
      //
      // We have to measure the distance to the revolution axis
      // and to check if the projection of the nodes of the geometry on the revolution axis are between basis centers
      number_t n = gnodes.size() / 2;
      Point C1 = 0.5 * (*gnodes[0] + *gnodes[2]);
      Point C2 = 0.5 * (*gnodes[n] + *gnodes[n + 2]);
      real_t r = pointDistance(C1, *gnodes[0]);
      for (number_t i = 0; i < tmbnodes.size(); i++)
      {
        real_t dist;
        Point H = projectionOnStraightLine(force3D(tmbnodes[i]), C1, C2, dist);
        p = H - C1;
        q = C2 - C1;
        x = dot(p, q) / dot(q, q);
        if (dist > r || x <= 0. || x >= 1.)
        {
          isInside = false;
          break;
        }
      }
      if (!isInside)  // if one corner of the minimal box is outside, check geometry directly
      {
        isInside = true;
        for (number_t i = 0; i < tnodes.size(); i++)
        {
          real_t dist;
          Point H = projectionOnStraightLine(force3D(*tnodes[i]), C1, C2, dist);
          p = H - C1;
          q = C2 - C1;
          x = dot(p, q) / dot(q, q);
          if (dist > r || x <= 0. || x >= 1.)
          {
            isInside = false;
            break;
          }
        }
      }
      break;
    }
    case _segment:
    {
      if (shape_ != _segment)
      { isInside = false; }
      else
      {
        // we test first if segments are not colinear and not parallel
        Point segNormal = crossProduct(force3D(*tnodes[1] - *tnodes[0]), force3D(*gnodes[1] - *gnodes[0]));
        if (norm2(segNormal) > theEpsilon)
        { isInside = false; }
        else
        {
          // segments are colinear or parallel
          // we first have to check is vertices are aligned
          segNormal = crossProduct(force3D(*tnodes[1] - *gnodes[0]), force3D(*tnodes[0] - *gnodes[0]));
          if (norm2(segNormal) > theEpsilon)
          { isInside = false; }
          else
          {
            // segments are colinear
            // we first have to check if segments are identical
            if (*tnodes[0] == *gnodes[0] && *tnodes[1] == *gnodes[1])
            { isInside = false; }
            else
            {
              // segments are not identical
              // we first have to check if segments are reverse
              if (*tnodes[0] == *gnodes[1] && *tnodes[1] == *gnodes[0])
              { isInside = false; }
              else
              {
                // segments are not reverse
                // we test each vertex
                // - changing coordinates along the first segment
                // - if coordinates of vertex are between 0 and 1, it is inside
                for (number_t i = 0; i < tnodes.size(); i++)
                {
                  p = *tnodes[i] - *gnodes[0];
                  q = *gnodes[1] - *gnodes[0];
                  x = dot(p, q) / dot(q, q);
                  if (x <= 0. || x >= 1.)
                  {
                    isInside = false;
                    break;
                  }
                }
              }
            }
          }
        }
      }
      break;
    }
    case _ellArc:
    {
      // anything inside elliptic arc ?
      //
      if (shape() != _ellArc) { isInside = false; }
      isInside = false;
      break;
    }
    case _circArc:
    {
      // anything inside circle arc ?
      //
      if (shape() != _circArc) { isInside = false; }
      isInside = false;
      break;
    }
    default:
    {
      isInside = true;

      if (g.minimalBox.dim() == 2)
      {
        if (isCoplanar(g))
        {
          real_t a, b, c, ap, bp, cp;
          for (number_t i = 0; i < tmbnodes.size(); i++)
          {
            p = tmbnodes[i] - force3D(gmbnodes[0]);
            q = gmbnodes[1] - gmbnodes[0];
            q2 = gmbnodes[3] - gmbnodes[0];
            a = dot(q, q);
            b = dot(q, q2);
            c = dot(p, force3D(q));
            ap = dot(q, q2);
            bp = dot(q2, q2);
            cp = dot(p, force3D(q2));
            x = (c * bp - cp * b) / (a * bp - ap * b);
            y = (a * cp - ap * c) / (a * bp - ap * b);
            if (x <= 0. || x >= 1. || y <= 0. || y >= 1.)
            {
              isInside = false;
              break;
            }
          }
          if (!isInside)  // if one corner of the minimal box is outside, check geometry directly
          {
            isInside = true;
            for (number_t i = 0; i < tnodes.size(); i++)
            {
              p = force3D(*tnodes[i]) - force3D(gmbnodes[0]);
              q = gmbnodes[1] - gmbnodes[0];
              q2 = gmbnodes[3] - gmbnodes[0];
              a = dot(q, q);
              b = dot(q, q2);
              c = dot(p, force3D(q));
              ap = dot(q, q2);
              bp = dot(q2, q2);
              cp = dot(p, force3D(q2));
              x = (c * bp - cp * b) / (a * bp - ap * b);
              y = (a * cp - ap * c) / (a * bp - ap * b);
              if (x <= 0. || x >= 1. || y <= 0. || y >= 1.)
              {
                return false;
              }
            }
          }
        }
        else
        { return false; }

        if (theVerboseLevel >= 2)
        { warning("undetermined_inclusion"); }
        // appel de pointInPolygon
      }

      if (g.minimalBox.dim() == 3)
      {
        isInside = true;

        real_t a, b, c, d, e, ap, bp, cp, dp, as, bs, cs, ds;
        for (number_t i = 0; i < tmbnodes.size(); i++)
        {
          p = tmbnodes[i] - gmbnodes[0];
          q = gmbnodes[1] - gmbnodes[0];
          q2 = gmbnodes[3] - gmbnodes[0];
          q3 = gmbnodes[4] - gmbnodes[0];
          a = dot(q, q);
          b = dot(q, q2);
          c = dot(q, q3);
          d = dot(p, q);
          ap = dot(q, q2);
          bp = dot(q2, q2);
          cp = dot(q2, q3);
          dp = dot(p, q2);
          as = dot(q, q3);
          bs = dot(q2, q3);
          cs = dot(q3, q3);
          ds = dot(p, q3);
          e = a * bp * cs + ap * bs * c + as * b * cp - c * bp * as - cp * bs * a - cs * b * ap;
          x = (d * bp * cs + dp * bs * c + ds * b * cp - c * bp * ds - cp * bs * d - cs * b * dp) / e;
          y = (a * dp * cs + ap * ds * c + as * d * cp - c * dp * as - cp * ds * a - cs * d * ap) / e;
          z = (a * bp * ds + ap * bs * d + as * b * dp - d * bp * as - dp * bs * a - ds * b * ap) / e;
          if (x <= 0. || x >= 1. || y <= 0. || y >= 1. || z <= 0. || z >= 1.)
          {
            isInside = false;
            break;
          }
        }

        if (!isInside)  // if one corner of the minimal box is outside, check geometry directly
        {
          isInside = true;
          for (number_t i = 0; i < tnodes.size(); i++)
          {
            p = force3D(*tnodes[i]) - gmbnodes[0];
            q = gmbnodes[1] - gmbnodes[0];
            q2 = gmbnodes[3] - gmbnodes[0];
            q3 = gmbnodes[4] - gmbnodes[0];
            a = dot(q, q);
            b = dot(q, q2);
            c = dot(q, q3);
            d = dot(p, q);
            ap = dot(q, q2);
            bp = dot(q2, q2);
            cp = dot(q2, q3);
            dp = dot(p, q2);
            as = dot(q, q3);
            bs = dot(q2, q3);
            cs = dot(q3, q3);
            ds = dot(p, q3);
            e = a * bp * cs + ap * bs * c + as * b * cp - c * bp * as - cp * bs * a - cs * b * ap;
            x = (d * bp * cs + dp * bs * c + ds * b * cp - c * bp * ds - cp * bs * d - cs * b * dp) / e;
            y = (a * dp * cs + ap * ds * c + as * d * cp - c * dp * as - cp * ds * a - cs * d * ap) / e;
            z = (a * bp * ds + ap * bs * d + as * b * dp - d * bp * as - dp * bs * a - ds * b * ap) / e;
            if (x <= 0. || x >= 1. || y <= 0. || y >= 1. || z <= 0. || z >= 1.)
            {
              return false;
              break;
            }
          }
        }

        if (theVerboseLevel >= 2)
        { warning("undetermined_inclusion"); }
        // appel de pointInPolyhedron
      }
      break;
    }
  }
  return isInside;
}

bool pointInPolygon(const Point& p, const Geometry& g)
{
  bool isInside = true;

  // déroulement de l'algorithme 2D (3D ?) si g est un polygone ( vector<Point> gPts):
  // je prend chaque sommet de this->minimalBox_p -> vector<Point> oriPts
  // je prend la minimal box de g -> g.mb
  // je boucle sur chaque point de oriPts
  // mon segment test: pair<Point,Point> s(oriPts[i], oriPts[i]+100*(g.mb.p2-g.mb.p1)
  //                    le facteur 100 devrait suffire ...
  //                    MIEUX: on prend 2 fois le max entre la distance entre le point et le centre de la minimal
  //                    box et la plus grande des dimensions de la minimal box
  // pour chaque i
  // je regarde si le sommet gPts[i] est sur s
  //         si oui: je ne fais rien
  //         si non, je regarde si l'arête gPts[i], gPts[i-1] coupe s
  //                        si oui, j'incrémente mon compteur
  //                        si non, je ne fais rien
  // je retourne la valeur du compteur modulo 2
  //     si c'est 0, alors oriPts[i] est à l'extérieur de g et ma geometrie n'est pas include dans g.
  //     si c'est 1, alors oriPts[i] à l'intérieur
  // comment regarder si un segment coupe un autre segment

  return isInside;
}

bool pointInPolyhedron(const Point& p, const Geometry& g)
{
  bool isInside = true;
  return isInside;
}

Geometry& Geometry::crack(CrackType ct, string_t domNameToOpen)
{
  crackable = true;
  crackType = ct;
  crackDomNameToOpen = domNameToOpen;
  return *this;
}

Geometry& Geometry::uncrack()
{
  crackable = false;
  crackType = _noCrack;
  crackDomNameToOpen = "";
  return *this;
}

//! build parametrization of a composite geometry if not built
Parametrization& Geometry::buildParametrization() const
{
  if (parametrization_ == nullptr)
  {
    if (!isCanonical())
    {
      // buildConnectedParts(); //build connected parts
      // parametrization_= new Parametrization(const_cast<Geometry*>(this)); // allocate parametrization of a composite geometry
      if (shape_ == _composite)
      {
        if (dim_ == 1) { buildC0PiecewiseParametrization(); } // try to build C0 parametrization
        else { buildPiecewiseParametrization(); }
      }
      else { parametrization_ = new Parametrization(const_cast<Geometry*>(this)); }
    }
    else { error("free_error", "in Geometry::buildParametrization, canonical geometry (" + words("shape", shape_) + ") should have a parametrization"); }
  }
  return *parametrization_;
}

/*! build connectedParts:
      - collect all the geometry components of same dimension that are connected
      - connected parts are sorted so that two geometries are adjacent to each other
    connectedParts is empty for canonical geometry
*/
void Geometry::buildConnectedParts() const
{
  if (connectedParts_.size() > 0) { return; } //already built
  trace_p->push("Geometry::buildConnectedParts");
  //first collect geometry by dimension (curve, surface, volume)
  std::list<number_t> curves, surfaces, volumes;
  std::map<number_t, Geometry*>::iterator itg = components_.begin();
  for (; itg != components_.end(); ++itg)
  {
    Geometry* g = itg->second;
    dimen_t d = g->dim_;
    switch (d)
    {
      case 1: curves.push_back(itg->first);   break;
      case 2: surfaces.push_back(itg->first); break;
      case 3: volumes.push_back(itg->first);  break;
      default: break; // d=0 (Point) not managed
    }
  }
  // deal with curves
  if (dim_ == 1)
  {
    number_t nbc = 0;
    nbParts_ = curves.size();
    while (curves.size() > 0)
    {
      std::list<number_t>::iterator itc = curves.begin();
      while (nbc > 0 && itc != curves.end()) // try to add curve to existing connected parts
      {
        number_t ic = *itc;
        Geometry* g = components_[ic];
        std::vector<const Point*> gnodes = g->boundNodes();
        bool notins = true;
        for (number_t c = 0; c < nbc && notins; c++) // try to add curve ic to a connected part
        {
          number_t nbcp = connectedParts_[c].size();
          if (nbcp > 0) // try to connect curve ic at current connectedPart
          {
            int n = connectedParts_[c][0]; //try to insert before the first curve of current part
            std::vector<const Point*> nodes = components_[std::abs(n)]->boundNodes();
            const Point* nd = nodes[0]; //first bound
            if (n < 0) { nd = nodes[1]; }   //reverse curve
            if (*gnodes[1] == *nd)// before no reverse
            {
              connectedParts_[c].insert(connectedParts_[c].begin(), ic);
              notins = false;
            }
            if (notins && *gnodes[0] == *nd) // before reverse
            {
              connectedParts_[c].insert(connectedParts_[c].begin(), -ic);
              notins = false;
            }
            if (notins) //try to add after the last curve of current part
            {
              n = connectedParts_[c][nbcp - 1];
              nodes = components_[std::abs(n)]->boundNodes();
              const Point* nd = nodes[1]; //last bound
              if (n < 0) { nd = nodes[0]; }   //reverse curve
              if (*gnodes[0] == *nd)// after no reverse
              {
                connectedParts_[c].insert(connectedParts_[c].end(), ic);
                notins = false;
              }
              if (notins && *gnodes[1] == *nd) // after reverse
              {
                connectedParts_[c].insert(connectedParts_[c].end(), -ic);
                notins = false;
              }
            }
          }
        }
        if (!notins)
        {
          curves.erase(itc);
          itc = curves.begin(); // retry with all curves
        }
        else { itc++; }
      }
      if (curves.size() > 0) // create new connected parts
      {
        connectedParts_.push_back(std::vector<int_t>(1, *curves.begin()));
        nbc++;
        curves.erase(curves.begin());
      }
    }

  }

  if (dim_ == 2) // deal with surfaces
  {
    // TO BE DONE
  }

  if (dim_ == 3) // deal with volumes
  {
    // TO BE DONE
  }

  trace_p->pop();
}

// describe a list of transformations (trs) to apply to parameters u,v,w
//   _uvw: no transformation, _pxy permutation of parameters x,y, _sx symmetry for parameter x -> 1-x
//   tx: starting values of parameter x
//   isfree_x: indicates that parameter x is free (no transformation involving x)
enum Trans_uvw {_uvw, _puv, _puw, _pvw, _su, _sv, _sw};
class Trgeo
{
  public:
    std::list<Trans_uvw> trs;
    int_t tu, tv, tw;
    bool isfree_u, isfree_v, isfree_w;
    Trgeo() : tu(0), tv(0), tw(0), isfree_u(true), isfree_v(true), isfree_w(true) {}
    void add(Trans_uvw t) { trs.push_back(t); }
    Transformation* buildTransformation(Trans_uvw tr)
    {
      switch (tr)
      {
        case _uvw: return new Transformation(1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0);
        case _su: return new Transformation(-1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0);
        case _sv: return new Transformation(1, 0, 0, 0, -1, 0, 0, 0, 1, 0, 1, 0);
        case _sw: return new Transformation(1, 0, 0, 0, 1, 0, 0, 0, -1, 0, 0, 1);
        case _puv: return new Transformation(0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0);
        case _puw: return new Transformation(0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0);
        case _pvw: return new Transformation(1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0);
      }
      return 0;
    }
};

void Geometry::buildPiecewiseError(const string_t& s) const
{
  warning("free_warning", "Geometry::buildPiecewiseParametrization() fails, both adjacent geometries not free - " + s);
}


//! build side maps used by buildPiecewiseParametrisation functions
/* geomsides:   map: Geometry*    -> vector of sides (list of node numbers)
   sidemap:     map: side  -> list of (Geometry*, side num)
*/
void Geometry::buildSideMaps(std::map<Geometry*, std::vector<std::set<number_t> > >& geomsides,
                             std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > >& sidemap) const
{
  number_t nbp = 2, nbs = 2, nbps = 1; // u-vertex numbering: (0) (1)
  if (dim_ == 2) {nbp = 4; nbs = 4; nbps = 2;} // uv-parameter vertex numbering: (0,0) (1,0) (1,1) (0,1) side numbering: 12 23 34 41
  if (dim_ == 3) {nbp = 8; nbs = 6; nbps = 4;} // uvw-parameter vertex numbering: (0,0,0) (1,0,0) (1,1,0) (0,1,0) (0,0,1) (1,0,1) (1,1,1) (0,1,1)
  std::vector<Point> pts(nbs);       //                 side numbering:  1234 1265 2376 3487 4185 5678
  std::vector<std::vector<number_t> > sides(nbs, std::vector<number_t>(nbps, 0));
  if (dim_ == 1)
  {pts[0] = Point(0.); pts[1] = Point(1.); sides[0][0] = 0; sides[1][0] = 1; }
  if (dim_ == 2)
  {
    pts[0] = Point(0., 0.); pts[1] = Point(1., 0.); pts[2] = Point(1., 1.); pts[3] = Point(0., 1.);
    sides[0][0] = 0; sides[0][1] = 1; sides[1][0] = 1; sides[1][1] = 2; sides[2][0] = 2; sides[2][1] = 3; sides[3][0] = 3; sides[3][1] = 0;
  }
  if (dim_ == 3)
  {
    pts[0] = Point(0., 0., 0.); pts[1] = Point(1., 0., 0.); pts[2] = Point(1., 1., 0.); pts[3] = Point(0., 1., 0.);
    pts[4] = Point(0., 0., 1.); pts[5] = Point(1., 0., 1.); pts[6] = Point(1., 1., 1.); pts[7] = Point(0., 1., 1.);
    sides[0][0] = 0; sides[0][1] = 1; sides[0][2] = 2; sides[0][3] = 3;
    sides[1][0] = 0; sides[1][1] = 1; sides[1][2] = 5; sides[1][3] = 4;
    sides[2][0] = 1; sides[2][1] = 2; sides[2][2] = 6; sides[2][3] = 5;
    sides[3][0] = 2; sides[3][1] = 3; sides[3][2] = 7; sides[3][3] = 6;
    sides[4][0] = 3; sides[4][1] = 0; sides[4][2] = 7; sides[4][3] = 4;
    sides[5][0] = 4; sides[5][1] = 5; sides[5][2] = 6; sides[5][3] = 7;
  }
  // vertex and side index, each component must have a parametrization
  std::vector<Point> nodes;
  std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > >::iterator itm;
  std::map<number_t, Geometry*>::iterator itg = components_.begin();
  std::vector<Point>::iterator itn;
  for (; itg != components_.end(); ++itg)
  {
    Geometry* g = itg->second;
    geomsides[g] = std::vector<std::set<number_t> >(nbs);
    if (g->parametrization_ == nullptr) { g->buildParametrization(); }
    Parametrization& par = *g->parametrization_;
    for (number_t s = 0; s < nbs; ++s) // loop on side
    {
      std::set<number_t>& sidenum = geomsides[g][s];
      for (number_t i = 0; i < nbps; ++i) // loop on side points
      {
        Point q = par(pts[sides[s][i]]);
        number_t k = 0;
        for (itn = nodes.begin(); itn != nodes.end(); ++itn, ++k)
          if (q == *itn) { break; }
        if (itn == nodes.end()) {nodes.push_back(q); k = nodes.size() - 1;}
        sidenum.insert(k);
      }
      if (sidenum.size() == nbps) //insert into sidemap only non degenerated side
      {
        itm = sidemap.find(sidenum);
        if (itm == sidemap.end()) { sidemap[sidenum] = std::list<std::pair<Geometry*, number_t> >(1, std::make_pair(g, s)); }
        else { itm->second.push_back(std::make_pair(g, s)); }
      }
    }
  }
}

//!< build non C0 piecewise parametrization (for composite geometry)
/* build a non C0 parametrization
   - neighborParsMap: to each parametrization pointer associates the neighbor parametrization pointers and their u,v,w localization
   with the storage convention: u=0->1, u=1->2, v=0->3, v=1->4, w=0->5, w=1->6
   1D example:
      u=1       u=0 u=0             u=1 u=0     u=1       neighborParMap[p1]=(<p2,1>,<0,0>)
      |----p1------|---------p2--------|----p3----|       neighborParMap[p2]=(<p1,1>,<p3,1>)
                                                          neighborParMap[p3]=(<p2,2>,<0,0>)
   2D example:
            u=1             v=1            u=0
      |------------|-------------------|----------|
      |            |                   |          |       neighborParMap[p1]=(<0,0>,<0,0>,<p2,1>,<0,0>)
   v=1|     p1  v=0|u=0      p2     u=1|v=0  p3   |v=1    neighborParMap[p2]=(<p1,3>,<p3,3>,<0,0>,<0,0>)
      |            |                   |          |       neighborParMap[p2]=(<0,0>,<0,0>,<p2,2>,<0,0>)
      |------------|-------------------|----------|
            u=0             v=0            u=1
  this map allows to travel accross the piecewise parametrization
  - vertices: vector of vertices
  - vertexIndex: for each parametrization associates its vertex indexes in the vertices vector
*/
void Geometry::buildPiecewiseParametrization() const
{
  //create side maps
  std::map<Geometry*, std::vector<std::set<number_t> > > geomsides;
  std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > > sidemap;    // map side -> list of (geo, side num)
  buildSideMaps(geomsides, sidemap);
  //create PieceWiseParametrization
  number_t nbc = components_.size();
  PiecewiseParametrization* ppar = nullptr;
  if (dim() == 1) { ppar = new PiecewiseParametrization(const_cast<Geometry*>(this), nbc); }
  if (dim() == 2) { ppar = new PiecewiseParametrization(const_cast<Geometry*>(this), nbc, 1); }
  if (dim() == 3) { ppar = new PiecewiseParametrization(const_cast<Geometry*>(this), nbc, 1, 1); }
  // create NeighborParsMap and vertices
  ppar->buildNeighborParsMap(sidemap);
  ppar->buildVertices();
  ppar->geomP() = const_cast<Geometry*>(this);
  parametrization_ = ppar;
}

//! build C0 piecewise parametrization of a composite geometry if not built
/* complex algorithm travelling geometries to match sides according to their own parametrization
   a) create geomsides: geo -> list of its sides and sidemap: map side -> list of (geo, side num)
   b) build the list of transformations to apply to parameters of each geometry
      list of maps: geo-> Trgeo, one map by connected part
      Trgeo a structure to handle parameters transformation and starting parameter values
   c) create the PiecewiseParametrization

   1D exemple:    geometries            x-----g1----x------g2-----x------g3-----x        x------g4-----x-----g5-----x
               local parametrization     0   --u->  1|1    <-u--  0|1   <-u--    0  void  1    <-u--   0|0  --u->    1
               global parametrization    0   --t->   1     --t->   2    --t->    3  cell  4    --t->    5   --t->    6
                  transformation              id          su+1          su+2                   su+4         id+5
               PiecewiseParametrization  n1=5 parsMap: (1,0,0)->(g1->par,id), (2,0,0)->(g2->par,su), (3,0,0)->(g3->par,su),
                                                        (4,0,0)->(g4->par,su), (5,0,0)->(g5->par,id)
   ### NOT ROBUST ENOUGH TO USE IT ###
*/
bool Geometry::buildC0PiecewiseParametrization() const
{
  if (parametrization_ != nullptr) { return true; }

  std::map<Geometry*, std::vector<std::set<number_t> > > geomsides;
  std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > > sidemap;    // map side -> list of (geo, side num)
  buildSideMaps(geomsides, sidemap);
  std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > > sidemap0 = sidemap; // to keep in memory
  //theCout<<"nodes: "<<nodes<<eol<<"geomsides: "<<geomsides<<eol<<"sidemap: "<<sidemap<<eol;

  //build list of set of connected patches with parmetrization matching
  std::list<std::map<Geometry*, Trgeo> > trgeoslist;
  std::map<Geometry*, Trgeo>::iterator it1, it2;
  std::set<std::set<number_t> > cursides;
  // main loop on sides
  while (!sidemap.empty())
  {
    trgeoslist.push_back(std::map<Geometry*, Trgeo>());
    std::map<Geometry*, Trgeo>& trgeos = trgeoslist.back();
    cursides.insert(sidemap.begin()->first);
    //theCout<<"sidemap: "<<sidemap<<eol<<"cursides: "<<cursides<<eol;
    const std::set<number_t>* side = nullptr, * nextside;
    std::vector<std::set<number_t> >::iterator itv;
    while (!cursides.empty())
    {
      bool islastside = (cursides.size() == 1);
      if (side == 0) { side = &*cursides.begin(); }
      nextside = nullptr;
      //theCout<<"  deal with side "<<(*side)<<std::flush;
      std::list<std::pair<Geometry*, number_t> >& parents = sidemap[*side]; //assume 1 or 2 parents
      Geometry* g1 = nullptr, * g2 = nullptr; number_t s1, s2;
      if (parents.size() == 1)
      {
        g1 = parents.front().first;
        it1 = trgeos.find(g1);
        if (it1 == trgeos.end()) // free element
        {
          for (itv = geomsides[g1].begin(); itv != geomsides[g1].end(); ++itv) //update cursides with g1 sides except sides already visited
          {
            if (sidemap.find(*itv) != sidemap.end() && *itv != *side) {cursides.insert(*itv); nextside = &*itv;}
          }
          trgeos.insert(std::make_pair(g1, Trgeo()));
        }
      }
      else if (trgeos.find(parents.front().first) == trgeos.end() || trgeos.find(parents.back().first) == trgeos.end() )
      {
        if (trgeos.find(parents.front().first) != trgeos.end())
        {
          g1 = parents.front().first; s1 = parents.front().second;
          g2 = parents.back().first; s2 = parents.back().second;
        }
        else
        {
          g1 = parents.back().first; s1 = parents.back().second;
          g2 = parents.front().first; s2 = parents.front().second;
        }

        //theCout<<" geo1 "<<g1<<" geo2 "<<g2<<std::flush;
        it1 = trgeos.find(g1);
        if (it1 == trgeos.end())
        {
          for (itv = geomsides[g1].begin(); itv != geomsides[g1].end(); ++itv) //update cursides with g2 sides except sides already visited
            if (sidemap.find(*itv) != sidemap.end() && *itv != *side) {cursides.insert(*itv);}
          it1 = trgeos.insert(std::make_pair(g1, Trgeo())).first;
        }
        for (itv = geomsides[g2].begin(); itv != geomsides[g2].end(); ++itv) //update cursides with g2 sides except sides already visited
          if (sidemap.find(*itv) != sidemap.end() && *itv != *side) {cursides.insert(*itv); nextside = &*itv;}
        it2 = trgeos.insert(std::make_pair(g2, Trgeo())).first;
        Trgeo& tr1 = it1->second;
        Trgeo& tr2 = it2->second;
        // analyze side matching
        switch (dim_)
        {
          case 1:
          {
            if (std::find(tr1.trs.begin(), tr1.trs.end(), _su) != tr1.trs.end()) { s1 = (s1 + 1) % 2; } // reverse side
            switch (10 * s1 + s2) // s1, s2 = 0 or 1
            {
              case 0: tr2.add(_su); tr2.tu = tr1.tu - 1; break;
              case 1: tr2.tu = tr1.tu - 1;              break;
              case 10: tr2.tu = tr1.tu + 1;              break;
              case 11: tr2.add(_su); tr2.tu = tr1.tu + 1; break;
            }
            break;
          }
          case 2:  // side coding 0->v=0, 1->u=1; 2->v=1, 3->u=0
          {
            if ((s1 == 1 || s1 == 3) && std::find(tr1.trs.begin(), tr1.trs.end(), _puv) != tr1.trs.end()) { s1 = (s1 == 1) ? 2 : 0; } // permute u,v
            else if ((s1 == 0 || s1 == 2) && std::find(tr1.trs.begin(), tr1.trs.end(), _puv) != tr1.trs.end()) { s1 = (s1 == 0) ? 3 : 1; } // permute u,v
            if ((s1 == 1 || s1 == 3) && std::find(tr1.trs.begin(), tr1.trs.end(), _su) != tr1.trs.end()) { s1 = (s1 == 1) ? 3 : 1; } // reverse side
            if ((s1 == 0 || s1 == 2) && std::find(tr1.trs.begin(), tr1.trs.end(), _sv) != tr1.trs.end()) { s1 = (s1 == 0) ? 2 : 0; } // reverse side
            switch (10 * s1 + s2) // s1, s2 = 0,1,2,3
            {
              case 0: // v=0 and v=0 -> reverse v and update tu and tv
                tr2.add(_sv); tr2.tv = tr2.tv - 1; tr2.tu = tr1.tu;
                break;
              case 1: // v=0 and u=1 -> permute u,v, update tu and tv
                tr2.add(_puv); tr2.tv = tr1.tv - 1; tr2.tu = tr1.tu;
                break;
              case 2: // v=0 and v=1 -> update tv
                tr2.tv = tr1.tv - 1; tr2.tu = tr1.tu;
                break;
              case 3:// v=0 and u=0 -> permute u,v, reverse and update tu and tv
                tr2.add(_puv); tr2.add(_sv); tr2.tv = tr1.tv - 1; tr2.tu = tr1.tu;
                break;
              case 10:// u=1 and v=0 -> permute u,v, update tu and tv
                tr2.add(_puv); tr2.tu = tr1.tu + 1; tr2.tv = tr1.tv;
                break;
              case 11:// u=1 and u=1 -> reverse u,  update tu and tv
                tr2.add(_su); tr2.tu = tr2.tu + 1; tr2.tv = tr2.tv;
                break;
              case 12:// u=1 and v=1 -> permute u,v reverse and  update tu and tv
                tr2.add(_puv); tr2.add(_su); tr2.tu = tr1.tu + 1; tr2.tv = tr1.tv;
                break;
              case 13:// u=1 and u=0 -> update tu
                tr2.tu = tr1.tu + 1; tr2.tv = tr1.tv;
                break;
              case 20:// v=1 and v=0 -> update tv
                tr2.tv = tr1.tv + 1; tr2.tu = tr2.tu;
                break;
              case 21:// v=1 and u=1 -> permute u,v reverse and  update tu or tv
                tr2.add(_puv); tr2.add(_sv); tr2.tv = tr1.tv + 1; tr2.tu = tr1.tu;
                break;
              case 22:// v=1 and v=1 -> reverse v and update tv
                tr2.add(_sv); tr2.tv = tr1.tv + 1; tr2.tu = tr1.tu;
                break;
              case 23:// v=1 and u=0 -> permute u,v and update tu or tv
                tr2.add(_puv); tr2.tv = tr1.tv + 1; tr2.tu = tr1.tu;
                break;
              case 30:// u=0 and v=0 -> permute u,v, reverse u and update tu or tv
                tr2.add(_puv); tr2.add(_su); tr2.tu = tr1.tu - 1; tr2.tv = tr1.tv;
                break;
              case 31:// u=0 and u=1 -> update tu
                tr2.tu = tr1.tu - 1; tr2.tv = tr1.tv;
                break;
              case 32:// u=0 and v=1 -> permute u,v and  update tu or tv
                tr2.add(_puv); tr2.add(_sv); tr2.tu = tr1.tu - 1; tr2.tv = tr1.tv;
                break;
              case 33:// u=0 and u=0 -> reverse u and update tu
                tr2.add(_su); tr2.tu = tr1.tu - 1; tr2.tv = tr1.tv;
                break;
            }
            break;
          }
          case 3:
          default:
            warning("free_warning", "Geometry::buildPiecewiseParametrization() not available in 3D case");
            return false;
        }
        //theCout<<"s1="<<s1<<" s2="<<s2<<"  tr1 : "<<tr1.tu<<" "<<tr1.tv<<" tr2 : "<<tr2.tu<<" "<<tr2.tv<<eol<<std::flush;
        itv = geomsides[g2].begin();
        for (; itv != geomsides[g2].end(); ++itv) //update cursides with g2 sides
        {
          if (sidemap.find(*itv) != sidemap.end() && *itv != *side) {cursides.insert(*itv); nextside = &*itv;}
        }
      } // if (parents.size()>1)
      sidemap.erase(*side);
      cursides.erase(*side);
      side = nextside;
      // theCout<<" cursides: "<<cursides<<eol<<std::flush;
    } // while (!cursides.empty())
  } // while (!sidemap.empty())

  // compute mintu,maxtu, ...
  int_t mintu = theIntMax, maxtu = theIntMin, mintv = theIntMax, maxtv = theIntMin, mintw = theIntMax, maxtw = theIntMin;
  std::list<std::map<Geometry*, Trgeo> >::iterator itl = trgeoslist.begin();
  for (; itl != trgeoslist.end(); ++itl)
  {
    std::map<Geometry*, Trgeo>& trgeos = *itl;
    for (it1 = trgeos.begin(); it1 != trgeos.end(); ++it1)
    {
      Trgeo& trg = it1->second;
      //theCout<<"   geo "<<it1->first<<" tu="<<trg.tu<<" tv="<<trg.tv<<eol<<std::flush;
      mintu = std::min(mintu, trg.tu); maxtu = std::max(maxtu, trg.tu);
      if (dim_ > 1) {mintv = std::min(mintv, trg.tv); maxtv = std::max(maxtv, trg.tv);}
      if (dim_ > 2) {mintw = std::min(mintw, trg.tw); maxtw = std::max(maxtw, trg.tw);}
    }
  }

  // create parametrization
  PiecewiseParametrization* ppar = nullptr;
  if (dim_ == 1) { ppar = new PiecewiseParametrization(const_cast<Geometry*>(this), maxtu - mintu + 1); }
  if (dim_ == 2) { ppar = new PiecewiseParametrization(const_cast<Geometry*>(this), maxtu - mintu + 1, maxtv - mintv + 1); }
  if (dim_ == 3) { ppar = new PiecewiseParametrization(const_cast<Geometry*>(this), maxtu - mintu + 1, maxtv - mintv + 1, maxtw - mintw + 1); }
  itl = trgeoslist.begin();
  bool success = true;
  for (; itl != trgeoslist.end(); ++itl)
  {
    std::map<Geometry*, Trgeo>& trgeos = *itl;
    for (it1 = trgeos.begin(); it1 != trgeos.end(); ++it1)
    {
      Trgeo& trg = it1->second;
      number_t i = number_t(trg.tu - mintu + 1);
      number_t j = 0, k = 0;
      if (dim_ >= 2) { j = number_t(trg.tv - mintv + 1); }
      if (dim_ >= 3) { k = number_t(trg.tw - mintw + 1); }
      if (trg.trs.size() == 0) { ppar->add(it1->first->parametrization_, 0, i, j, k); }
      else
      {
        std::list<Trans_uvw>::iterator itt = trg.trs.begin();
        Transformation* tr = nullptr;
        if (*itt != _uvw) { tr = trg.buildTransformation(*itt); }
        itt++;
        for (; itt != trg.trs.end(); ++itt)
        {
          if (tr == nullptr && *itt != _uvw) { tr = trg.buildTransformation(*itt); }
          else if (*itt != _uvw)
          {
            Transformation* tr2 = trg.buildTransformation(*itt);
            (*tr) *= *tr2;
            delete tr2;
          }
        }
        if (!ppar->add(it1->first->parametrization_, tr, i, j, k)) { success = false; }
      }
    }
  }
  if (success) // create NeighborParsMap and vertices
  {
    ppar->buildNeighborParsMap(sidemap0);
    ppar->buildVertices();
    ppar->contOrder = _regC0;
    ppar->geomP() = const_cast<Geometry*>(this);
    parametrization_ = ppar;
  }
  else //build a discontinuous parametrization
  {
    delete ppar;
    warning("free_warning", "building C0 parametrization fails, build a discontinuous one");
    buildPiecewiseParametrization();
  }
  return true;
}

/*
  Operators to define composite geometries
 */
Geometry& Geometry::operator+()
{
  warning("deprecated", words("unary") + " +", words("unary") + " !");
  force = true;
  return *this;
}

Geometry& Geometry::operator!()
{
  force = !force;
  return *this;
}

Geometry& Geometry::operator-()
{
  warning("deprecated", words("unary") + " -", words("unary") + " !");
  force = false;
  return *this;
}

void Geometry::cleanInclusions()
{
  if (shape_ == _composite)
  {
    // first, we have to convert geometries_ to map<number_t, std::set<number_t>> with only inclusions
    std::map<number_t, std::set<number_t> > inclusions, parents;
    std::map<number_t, std::vector<number_t> >::const_iterator it_g;
    std::map<number_t, std::vector<number_t> >::iterator it_g2;
    std::map<number_t, std::set<number_t> >::const_iterator it_p, it_p2;
    std::set<number_t>::const_iterator it_s, it_s2;
    for (it_g = geometries_.begin(); it_g != geometries_.end(); ++it_g)
    {
      for (number_t i = 1; i < it_g->second.size(); ++i)
      {
        inclusions[it_g->first].insert(it_g->second[i]);
        parents[it_g->second[i]].insert(it_g->first);
      }
    }

    for (it_p = parents.begin(); it_p != parents.end(); ++it_p)
    {
      if (it_p->second.size() > 1)
      {
        for (it_s = it_p->second.begin(); it_s != it_p->second.end(); ++it_s)
        {
          it_p2 = parents.find(*it_s);
          if (it_p2 != parents.end())
          {
            for (it_s2 = it_p2->second.begin(); it_s2 != it_p2->second.end(); ++it_s2)
            { inclusions[*it_s2].erase(it_p->first); }
          }
        }
      }
    }

    for (it_g2 = geometries_.begin(); it_g2 != geometries_.end(); ++it_g2)
    {
      it_g2->second.resize(inclusions[it_g2->first].size() + 1);
      it_g2->second[0] = it_g2->first;
      number_t i = 1;
      for (it_s = inclusions[it_g2->first].begin(); it_s != inclusions[it_g2->first].end(); ++it_s, ++i)
      { it_g2->second[i] = *it_s; }
    }
  }
}

//------------------------------------------------------------------------------------------
//                                  new add functions
//------------------------------------------------------------------------------------------
void addCompositeAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("addCompositeAndComposite(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(std::max(g1.dim(), g2.dim()));
  g.shape(_composite);
  g.boundingBox += g2.boundingBox;
  g.minimalBox = MinimalBox(g.boundingBox.bounds());
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c, it_c2;
  // test if one or more components of g2 are components of g1 or not
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < g1.components_.size(), then the component of g2 already exists in g1 and will not be added
  std::map<number_t, number_t> renumbering;
  number_t n = 0;

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    renumbering[it_c->first] = it_c->first;
    if (it_c->first > n)
    { n = it_c->first; }
  }
  number_t nn = n + 1;
  number_t shift = 0;
  // we have to check if each border of g2 is a border of g1 or not
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    bool found = false;
    for (it_c2 = g1.components_.begin(); it_c2 != g1.components_.end(); ++it_c2)
    {
      if (*it_c2->second == *it_c->second)
      {
        renumbering[it_c->first + n + 1] = it_c2->first;
        shift++;
        found = true;
        break;
      }
    }

    if (!found)
    { renumbering[it_c->first + n + 1] = it_c->first + n + 1 - shift; }
    if (it_c->first + n + 1 - shift > nn)
    { nn = it_c->first + n + 1 - shift; }
  }

  nn = nn + shift;

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  { g.components_[it_c->first] = it_c->second->clone(); }
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  { g.components_[renumbering[it_c->first + n + 1]] = it_c->second->clone(); }

  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  for (it_g = g2.geometries_.begin(); it_g != g2.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    {
      bool found = false;
      // we have to test if the number to add is not already in the list before adding it
      for (number_t j = 0; j < g.geometries_[renumbering[it_g->first + n + 1]].size() && !found; ++j)
      {
        if (g.geometries_[renumbering[it_g->first + n + 1]][j] == renumbering[it_g->second[i] + n + 1])
        { found = true; }
      }
      if (!found)
      { g.geometries_[renumbering[it_g->first + n + 1]].push_back(renumbering[it_g->second[i] + n + 1]); }
    }
  }

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    {
      bool found = false;
      // we have to test if the number to add is not already in the list before adding it
      for (number_t j = 0; j < g.loops_[renumbering[it_l->first + n + 1]].size() && !found; ++j)
      {
        if (g.loops_[renumbering[it_l->first + n + 1]][j] == renumbering[it_l->second[i] + n + 1])
        { found = true; }
      }
      if (!found)
      { g.loops_[renumbering[it_l->first + n + 1]].push_back(renumbering[it_l->second[i] + n + 1]); }
    }
  }

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); it_c++)
  {
    for (it_c2 = g2.components_.begin(); it_c2 != g2.components_.end(); it_c2++)
    {
      if (it_c2->second->isInside(*it_c->second) && g1.geometries_.count(it_c->first) > 0)
      { g.geometries_[it_c->first].push_back(renumbering[it_c2->first + n + 1]); }
      if (it_c->second->isInside(*it_c2->second) && g2.geometries_.count(it_c2->first) > 0)
      { g.geometries_[renumbering[it_c2->first + n + 1]].push_back(it_c->first); }
    }
  }
  if (g1.force)  { g1.force = false; }
  if (g2.force)  { g2.force = false; }
  trace_p->pop();
  return;
}

void addCompositeAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("addCompositeAndLoop(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(std::max(g1.dim(), g2.dim()));
  g.shape(_composite);
  g.boundingBox += g2.boundingBox;
  g.minimalBox = MinimalBox(g.boundingBox.bounds());
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c, it_c2;

  // test if the canonical geometry is not a border of the loop
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < g1.components_.size(), then the component of g2 already exists in g1 and will not be added
  std::map<number_t, number_t> renumbering;
  number_t n = 0;

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    renumbering[it_c->first] = it_c->first;
    if (it_c->first > n)
    { n = it_c->first; }
  }
  number_t last_new_n = n;
  number_t last_old_n = n; // new index of first component of g2
  number_t shift = 0;
  // we have to check if each border of g2 is a component of g1 or not
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    bool found = false;
    for (it_c2 = g1.components_.begin(); it_c2 != g1.components_.end(); ++it_c2)
    {
      if (*it_c2->second == *it_c->second)
      {
        renumbering[it_c->first + n + 1] = it_c2->first;
        if (it_c2->first > last_new_n)
        { last_new_n = it_c2->first; }
        shift++;
        found = true;
        break;
      }
    }
    if (!found)
    {
      renumbering[it_c->first + n + 1] = it_c->first + n + 1 - shift;
      if (it_c->first + n + 1 - shift > last_new_n)
      { last_new_n = it_c->first + n + 1 - shift; }
    }
  }
  last_old_n += g2.components_.size() + 1;

  // we also have to check if the loop geometry is a component of g1
  // even if each border of g2 is in g1, g2 can be no component of g1
  bool found = false;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    if (*it_c->second == g2)
    {
      found = true;
      break;
    }
  }
  if (found)
  {
    trace_p->pop();
    g = g1;
    return;
  }
  else
  {
    renumbering[last_old_n] = last_new_n + 1;
    last_new_n++;
  }

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  { g.components_[it_c->first] = it_c->second->clone(); }
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  { g.components_[renumbering[it_c->first + n + 1]] = it_c->second->clone(); }
  g.components_[last_new_n] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); ++it_g)
  {
    for (number_t i = 0; i < it_g->second.size(); ++i)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  for (it_g = g2.geometries_.begin(); it_g != g2.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    {
      bool found = false;
      // we have to test if the number to add is not already in the list before adding it
      for (number_t j = 0; j < g.geometries_[renumbering[it_g->first + n + 1]].size() && !found; ++j)
      {
        if (g.geometries_[renumbering[it_g->first + n + 1]][j] == renumbering[it_g->second[i] + n + 1])
        { found = true; }
      }
      if (!found)
      { g.geometries_[renumbering[it_g->first + n + 1]].push_back(renumbering[it_g->second[i] + n + 1]); }
    }
  }
  g.geometries_[last_new_n].push_back(last_new_n);
  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    {
      bool found = false;
      // we have to test if the number to add is not already in the list before adding it
      for (number_t j = 0; j < g.loops_[renumbering[it_l->first + n + 1]].size() && !found; ++j)
      {
        if (g.loops_[renumbering[it_l->first + n + 1]][j] == renumbering[it_l->second[i] + n + 1])
        { found = true; }
      }
      if (!found)
      { g.loops_[renumbering[it_l->first + n + 1]].push_back(renumbering[it_l->second[i] + n + 1]); }
    }
  }

  bool isHole = false;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); it_c++)
  {
    isHole = false;
    for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
    {
      bool isLocalHole = true;
      // if every border of the loop is inside a geometry, then the loop geometry is also inside
      for (number_t i = 0; i < it_l->second.size(); ++i)
      {
        if (!g2.components_.at(it_l->second[i])->isInside(*it_c->second))
        { isLocalHole = false; }
      }
      if (isLocalHole)
      {
        g.geometries_[it_c->first].push_back(last_new_n);
        isHole = true;
      }
    }
    if (!isHole && g1.force)
    {
      g.geometries_[last_new_n].push_back(it_c->first);
      isHole = true;
    }
    if (!isHole)
    {
      if (theVerboseLevel >= 2)
      { warning("loop_undetermined_inclusion", g1.domName(), g2.domName()); }
    }
  }

  if (g1.force)  { g1.force = false; }
  if (g2.force)  { g2.force = false; }
  trace_p->pop();
  return;
}

void addCompositeAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("addCompositeAndCanonical(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(std::max(g1.dim(), g2.dim()));
  g.shape(_composite);
  g.boundingBox += g2.boundingBox;
  g.minimalBox = MinimalBox(g.boundingBox.bounds());
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c;

  // test if the canonical geometry is not a border of the loop
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < i, then the component already exists and will not be added
  std::map<number_t, number_t> renumbering;
  number_t n = 0;

  // every component of a composite/loop geometry is unique
  // we just have to test the canonical geometry
  bool found = false;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    renumbering[it_c->first] = it_c->first;
    if (it_c->first > n)  { n = it_c->first; }
    if (g2 == *(it_c->second))
    {
      renumbering[n + 1] = it_c->first;
      found = true;
      break;
    }
  }
  if (found)
  {
    trace_p->pop();
    g = g1;
    return;
  }
  else { renumbering[n + 1] = n + 1; } // in the following, g2 is not a component of g1 so for all i, renumbering[i] = i

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); it_c++)
  { g.components_[it_c->first] = it_c->second->clone(); }
  g.components_[n + 1] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  g.geometries_[n + 1].push_back(n + 1);

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }

  bool isHole = false;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    if (g.geometries_.count(it_c->first) > 0)
    {
      isHole = false;
      if (g1.force)
      {
        g.geometries_[n + 1].push_back(it_c->first);
        isHole = true;
      }
      else
      {
        if (it_c->second->shape() == _loop)
        {
          // it_c->second is equivalent to g.components_.at(it_c->first)
          // it_c->second->loops_ is equivalent to g.loops_
          for (it_l = g.loops_.begin(); it_l != g.loops_.end(); ++it_l)
          {
            bool isLocalHole = true;
            // if every border of the loop is inside a geometry, then the loop geometry is also inside
            for (number_t i = 0; i < it_l->second.size(); ++i)
            {
              if (!g.components_.at(it_l->second[i])->isInside(g2))
              { isLocalHole = false; }
            }
            if (isLocalHole)
            {
              g.geometries_[n + 1].push_back(it_l->first);
              isHole = true;
            }
          }
          if (!isHole)
          {
            if (theVerboseLevel >= 2)
            { warning("loop_undetermined_inclusion", g2.domName(), it_c->second->domName()); }
          }
        }
        else
        {
          if (g2.isInside(*it_c->second)) { g.geometries_[it_c->first].push_back(n + 1); }
          if (it_c->second->isInside(g2))
          {
            g.geometries_[n + 1].push_back(it_c->first);
            isHole = true;
          }
        }
      }
    }
  }

  if (g1.force) { g1.force = false; }
  if (g2.force) { g2.force = false; }
  trace_p->pop();
  return ;
}

void addLoopAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("addLoopAndLoop(Geometry,Geometry)");
  if (g1 == g2)
  {
    trace_p->pop();
    g = g1;
    return;
  }

  g.boundingBox = g1.boundingBox;
  g.dim(std::max(g1.dim(), g2.dim()));
  g.shape(_composite);
  g.boundingBox += g2.boundingBox;
  g.minimalBox = MinimalBox(g.boundingBox.bounds());
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c, it_c2;

  // test if the canonical geometry is not a border of the loop
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < g1.components_.size(), then the component of g2 already exists in g1 and will not be added
  std::map<number_t, number_t> renumbering;
  number_t n = 0;

  // every component of a composite/loop geometry is unique
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    renumbering[it_c->first] = it_c->first;
    if (it_c->first > n)
    { n = it_c->first; }
  }
  renumbering[n + 1] = n + 1; // loop geometry g1 is unique
  number_t nn = n + 2;
  number_t shift = 0;
  // we have to check if each border of g2 is a border of g1 or not
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    bool found = false;
    for (it_c2 = g1.components_.begin(); it_c2 != g1.components_.end(); ++it_c2)
    {
      if (*it_c2->second == *it_c->second)
      {
        renumbering[it_c->first + n + 2] = it_c2->first;
        shift++;
        found = true;
        break;
      }
    }

    if (!found)
    { renumbering[it_c->first + n + 2] = it_c->first + n + 2 - shift; }
    if (it_c->first + n + 2 - shift > nn)
    { nn = it_c->first + n + 2 - shift; }
  }
  renumbering[nn + 1 + shift] = nn + 1; // loop geometry g2 is unique

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  { g.components_[it_c->first] = it_c->second->clone(); }
  g.components_[n + 1] = g1.clone();
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  { g.components_[renumbering[it_c->first + n + 2]] = it_c->second->clone(); }
  g.components_[nn + 1] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[n + 1].push_back(it_l->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[nn + 1].push_back(renumbering[it_l->second[i] + n + 2]); }
  }

  // management of geometries
  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  g.geometries_[n + 1].push_back(n + 1);
  for (it_g = g2.geometries_.begin(); it_g != g2.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    {
      bool found = false;
      // we have to test if the number to add is not already in the list before adding it
      for (number_t j = 0; j < g.geometries_[renumbering[it_g->first + n + 2]].size() && !found; ++j)
      {
        if (g.geometries_[renumbering[it_g->first + n + 2]][j] == renumbering[it_g->second[i] + n + 2])
        { found = true; }
      }
      if (!found)
      { g.geometries_[renumbering[it_g->first + n + 2]].push_back(renumbering[it_g->second[i] + n + 2]); }
    }
  }
  g.geometries_[nn + 1].push_back(nn + 1);
  if (g2.force)  { g.geometries_[n + 1].push_back(nn + 1); }
  else if (g1.force) { g.geometries_[nn + 1].push_back(n + 1); }
  else
  {
    if (theVerboseLevel >= 2)
    { warning("undetermined_inclusion"); }
  }

  if (g1.force) { g1.force = false; }
  if (g2.force) { g2.force = false; }
  trace_p->pop();
  return ;
}

void addLoopAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("addLoopAndCanonical(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(std::max(g1.dim(), g2.dim()));
  g.shape(_composite);
  g.boundingBox += g2.boundingBox;
  g.minimalBox = MinimalBox(g.boundingBox.bounds());
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c;

  // test if the canonical geometry is not a border of the loop
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < g1.components_.size(), then g2 already exists in g1 and will not be added
  std::map<number_t, number_t> renumbering;
  number_t n = 0;

  // every component of a composite/loop geometry is unique
  // we just have to test the canonical geometry
  bool found = false;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    renumbering[it_c->first] = it_c->first;
    if (it_c->first > n)
    { n = it_c->first; }
    if (g2 == *(it_c->second))
    {
      renumbering[n + 2] = it_c->first;
      found = true;
      break;
    }
  }
  renumbering[n + 1] = n + 1; // loop geometry g1 is necessarily unique
  if (found)
  {
    trace_p->pop();
    g = g1;
    return;
  }
  else
  { renumbering[n + 2] = n + 2; } // in the following, g2 is not a border of g1, so for all i, renumbering[i] = i

  // management of components
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  { g.components_[it_c->first] = it_c->second->clone(); }

  g.components_[n + 1] = g1.clone(); // adding the loop geometry after its borders
  g.components_[n + 2] = g2.clone(); // adding the canonical geometry

  // border is not managed here

  // management of loops
  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[n + 1].push_back(it_l->second[i]); }
  }

  // management of geometries
  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  g.geometries_[n + 1].push_back(n + 1);
  g.geometries_[n + 2].push_back(n + 2);

  bool isHole = false;
  if (g1.force)
  {
    g.geometries_[n + 2].push_back(n + 1);
    isHole = true;
  }
  else
  {
    for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
    {
      bool isLocalHole = true;
      // if every border of the loop is inside the canonical geometry, then the loop geometry is also inside
      for (number_t i = 0; i < it_l->second.size(); ++i)
      {
        if (!g1.components_.at(it_l->second[i])->isInside(g2))
        { isLocalHole = false; }
      }
      if (isLocalHole)
      {
        g.geometries_[n + 2].push_back(n + 1);
        isHole = true;
      }
    }
  }
  if (!isHole && g2.force)
  {
    g.geometries_[n + 1].push_back(n + 2);
    isHole = true;
  }
  if (!isHole)
  {
    if (theVerboseLevel >= 2)
    { warning("loop_undetermined_inclusion", g2.domName(), g1.domName()); }
  }

  // we have to unforce copies of g1 and g2 in components
  if (g1.force) { g1.force = false; }
  if (g2.force) { g2.force = false; }
  trace_p->pop();
  return ;
}

void addCanonicalAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("addCanonicalAndCanonical(Geometry,Geometry)");
  if (g1 == g2)
  {
    g = g1;
    trace_p->pop();
    return;
  }
  g.boundingBox = g1.boundingBox;
  g.dim(std::max(g1.dim(), g2.dim()));
  g.shape(_composite);
  g.boundingBox += g2.boundingBox;
  g.minimalBox = MinimalBox(g.boundingBox.bounds());
  g.init();
  g.components_[0] = g1.clone();
  g.components_[1] = g2.clone();
  g.geometries_[0].push_back(0);
  g.geometries_[1].push_back(1);
  if (g2.force || g2.isInside(g1)) { g.geometries_[0].push_back(1); }
  if (g1.force || g1.isInside(g2)) { g.geometries_[1].push_back(0); }
  if (g1.force) { g1.force = false; }
  if (g2.force) { g2.force = false; }
  trace_p->pop();
  return;
}

Geometry& Geometry::operator+=(const Geometry& g)
{
  trace_p->push("Geometry::operator+=");
  if (this == &g) { return *this; }
  if (shape() == _extrusion || g.shape() == _extrusion)
  { error("shape_not_handled", words("shape", _extrusion)); }
  if (shape() == _noShape)
  {
    *this = toComposite(g);
    trace_p->pop();
    return *this;
  }
  if (g.shape() == _noShape)
  {
    trace_p->pop();
    return *this;
  }

  Geometry res;
  switch (shape())
  {
    case _composite:
      switch (g.shape())
      {
        case _composite: addCompositeAndComposite(*this, g, res); break;
        case _loop: addCompositeAndLoop(*this, g, res); break;
        default: addCompositeAndCanonical(*this, g, res);
      }
      break;
    case _loop:
      switch (g.shape())
      {
        case _composite: addCompositeAndLoop(g, *this, res); break;
        case _loop: addLoopAndLoop(*this, g, res); break;
        default: addLoopAndCanonical(*this, g, res);
      }
      break;
    default: //this is canonical
      switch (g.shape())
      {
        case _composite: addCompositeAndCanonical(g, *this, res); break;
        case _loop: addLoopAndCanonical(g, *this, res); break;
        default: addCanonicalAndCanonical(*this, g, res);
      }
  }
  res.cleanInclusions();
  res.geoNode_ = new GeoNode(_plusGeOp, this->clone(), g.clone());
  *this = res;
  trace_p->pop();
  return *this;
}

Geometry operator+(const Geometry& g1, const Geometry& g2)
{
  trace_p->push("Geometry::operator+");
  if (&g1 == &g2) { return g1; }
  if (g1.shape() == _extrusion || g2.shape() == _extrusion)
  { error("shape_not_handled", words("shape", _extrusion)); }

  Geometry g;
  switch (g1.shape())
  {
    case _composite:
      switch (g2.shape())
      {
        case _composite: addCompositeAndComposite(g1, g2, g); break;
        case _loop: addCompositeAndLoop(g1, g2, g); break;
        default: addCompositeAndCanonical(g1, g2, g);
      }
      break;
    case _loop:
      switch (g2.shape())
      {
        case _composite: addCompositeAndLoop(g2, g1, g); break;
        case _loop: addLoopAndLoop(g1, g2, g); break;
        default: addLoopAndCanonical(g1, g2, g);
      }
      break;
    default: //g1 is canonical
      switch (g2.shape())
      {
        case _composite: addCompositeAndCanonical(g2, g1, g); break;
        case _loop: addLoopAndCanonical(g2, g1, g); break;
        default: addCanonicalAndCanonical(g1, g2, g);
      }
  }
  //finalization
  g.cleanInclusions();
  g.geoNode_ = new GeoNode(_plusGeOp, g1.clone(), g2.clone());
  trace_p->pop();
  return g;
}

//------------------------------------------------------------------------------------------
//    new substr functions
//------------------------------------------------------------------------------------------
void substrCompositeAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrCompositeAndComposite(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c, it_c2;
  number_t n = 0;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); it_c++)
  {
    g.components_[it_c->first] = it_c->second->clone();
    if (it_c->first > n)
    { n = it_c->first; }
  }
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); it_c++)
  { g.components_[it_c->first + n + 1] = it_c->second->clone(); }

  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first + n + 1].push_back(it_l->second[i] + n + 1); }
  }

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); it_c++)
  {
    for (it_c2 = g2.components_.begin(); it_c2 != g2.components_.end(); it_c2++)
    {
      if (it_c2->second->isInside(*it_c->second) && g1.geometries_.count(it_c->first) > 0)
      { g.geometries_[it_c->first].push_back(it_c2->first + n + 1); }
    }
  }
  trace_p->pop();
  return;
}

void substrCompositeAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrCompositeAndLoop(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();

  bool isHole = false;
  std::map<number_t, Geometry*>::const_iterator it_c;
  number_t n = 0, nn = 0;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    g.components_[it_c->first] = it_c->second->clone();
    if (it_c->first > n)
    { n = it_c->first; }
  }
  nn = n + 1;
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    g.components_[it_c->first + n + 1] = it_c->second->clone();
    if (it_c->first + n + 1 > nn)
    { nn = it_c->first + n + 1; }
  }
  g.components_[nn + 1] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); ++it_g)
  {
    for (number_t i = 0; i < it_g->second.size(); ++i)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  // we add borders of the loops but not the loop itself
  for (it_g = g2.geometries_.begin(); it_g != g2.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first + n + 1].push_back(it_g->second[i] + n + 1); }
  }

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first + n + 1].push_back(it_l->second[i] + n + 1); }
  }

  for (it_c = g1.components_.begin(); it_c != g1.components_.end() && !isHole; it_c++)
  {
    for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
    {
      bool isLocalHole = true;
      // if every border of the loop is inside a geometry, then the loop geometry is also inside
      for (number_t i = 0; i < it_l->second.size(); ++i)
      {
        if (!g2.components_.at(it_l->second[i])->isInside(*it_c->second))
        { isLocalHole = false; }
      }
      if (isLocalHole)
      {
        g.geometries_[it_c->first].push_back(nn + 1);
        isHole = true;
      }
    }
  }
  if (!isHole) { warning("hole_outside"); }
  trace_p->pop();
  return;
}

void substrCompositeAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrCompositeAndCanonical(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();

  bool isHole = false;
  std::map<number_t, Geometry*>::const_iterator it_c;
  number_t n = 0;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); it_c++)
  {
    g.components_[it_c->first] = it_c->second->clone();
    if (it_c->first > n)
    { n = it_c->first; }
  }
  g.components_[n + 1] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_g, it_l;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); it_c++)
  {
    if (g2.isInside(*it_c->second) && g1.geometries_.count(it_c->first) > 0)
    { g.geometries_[it_c->first].push_back(n + 1); isHole = true; }
  }
  if (!isHole) { warning("hole_outside"); }
  trace_p->pop();
  return;
}

void substrLoopAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrLoopAndComposite(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();

  bool isHole = false;
  std::map<number_t, Geometry*>::const_iterator it_c;
  number_t n = 0, nn = 0;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    g.components_[it_c->first] = it_c->second->clone();
    if (it_c->first > n)
    { n = it_c->first; }
  }
  g.components_[n + 1] = g1.clone();
  nn = n + 2;
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    g.components_[it_c->first + n + 2] = it_c->second->clone();
    if (it_c->first + n + 2 > nn)
    { nn = it_c->first + n + 2; }
  }

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first + n + 2].push_back(it_l->second[i] + n + 2); }
  }

  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); ++it_g)
  {
    for (number_t i = 0; i < it_g->second.size(); ++i)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  g.geometries_[n + 1].push_back(n + 1);
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    g.geometries_[n + 1].push_back(it_c->first + n + 2);
    if (it_c->second->isInside(g1))  { isHole = true; }
  }
  if (!isHole) { warning("hole_outside"); }
  trace_p->pop();
  return;
}

void substrLoopAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrLoopAndLoop(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();
  std::map<number_t, Geometry*>::const_iterator it_c, it_c2;

  // test if the loop geometry is not a border of the loop
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < g1.components_.size(), then the component of g2 already exists in g1 and will not be added
  std::map<number_t, number_t> renumbering;
  number_t n = 0;

  // every component of a composite/loop geometry is unique
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    renumbering[it_c->first] = it_c->first;
    if (it_c->first > n)
    { n = it_c->first; }
  }
  renumbering[n + 1] = n + 1; // loop geometry g1 is unique
  number_t nn = n + 2;
  number_t shift = 0;
  // we have to check if each border of g2 is a border of g1 or not
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    bool found = false;
    for (it_c2 = g1.components_.begin(); it_c2 != g1.components_.end(); ++it_c2)
    {
      if (*it_c2->second == *it_c->second)
      {
        renumbering[it_c->first + n + 2] = it_c2->first;
        shift++;
        found = true;
        break;
      }
    }
    if (!found)
    { renumbering[it_c->first + n + 2] = it_c->first + n + 2 - shift; }
    if (it_c->first + n + 2 - shift > nn)
    { nn = it_c->first + n + 2 - shift; }
  }
  renumbering[nn + 1 + shift] = nn + 1; // loop geometry g2 is unique

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  { g.components_[it_c->first] = it_c->second->clone(); }
  g.components_[n + 1] = g1.clone();
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  { g.components_[renumbering[it_c->first + n + 2]] = it_c->second->clone(); }
  g.components_[nn + 1] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first + n + 2].push_back(it_l->second[i] + n + 2); }
  }

  // management of geometries
  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  g.geometries_[n + 1].push_back(n + 1);
  for (it_g = g2.geometries_.begin(); it_g != g2.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[renumbering[it_g->first + n + 2]].push_back(renumbering[it_g->second[i] + n + 2]); }
  }
  g.geometries_[n + 1].push_back(nn + 1);
  if (theVerboseLevel >= 2) { warning("undetermined_inclusion"); }
  trace_p->pop();
  return ;
}

void substrLoopAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrLoopAndCanonical(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c;
  // test if the canonical geometry is not a border of the loop
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < g1.components_.size(), then g2 already exists in g1 and will not be added
  std::map<number_t, number_t> renumbering;
  number_t n = 0;

  // every component of a composite/loop geometry is unique
  // we just have to test the canonical geometry
  bool found = false;
  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  {
    renumbering[it_c->first] = it_c->first;
    if (it_c->first > n)
    { n = it_c->first; }
    if (g2 == *(it_c->second))
    {
      renumbering[n + 2] = it_c->first;
      found = true;
      break;
    }
  }
  renumbering[n + 1] = n + 1; // loop geometry g1 is necessarily unique
  if (found)
  {
    trace_p->pop();
    g = g1;
    return;
  }
  else
  { renumbering[n + 2] = n + 2; } // in the following, g2 is not a border of g1, so for all i, renumbering[i] = i

  for (it_c = g1.components_.begin(); it_c != g1.components_.end(); ++it_c)
  { g.components_[it_c->first] = it_c->second->clone(); }
  g.components_[n + 1] = g1.clone();
  g.components_[n + 2] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;

  for (it_l = g1.loops_.begin(); it_l != g1.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[n + 1].push_back(it_l->second[i]); }
  }

  // management of geometries
  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_g = g1.geometries_.begin(); it_g != g1.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  g.geometries_[n + 1].push_back(n + 1);
  g.geometries_[n + 1].push_back(n + 2);

  if (!g2.isInside(g1))  { warning("hole_outside"); }
  trace_p->pop();
  return;
}

void substrCanonicalAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrCanonicalAndComposite(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();

  bool isHole = false;
  std::map<number_t, Geometry*>::const_iterator it_c;
  number_t n = 0;
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); it_c++)
  {
    g.components_[it_c->first] = it_c->second->clone();
    if (it_c->first > n)
    { n = it_c->first; }
  }
  g.components_[n + 1] = g1.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }

  g.geometries_[n + 1].push_back(n + 1);
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); it_c++)
  { if (it_c->second->isInside(g1)) { g.geometries_[n + 1].push_back(it_c->first); isHole = true; } }
  if (!isHole)  { warning("hole_outside"); }
  trace_p->pop();
  return;
}

void substrCanonicalAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrCanonicalAndLoop(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();

  std::map<number_t, Geometry*>::const_iterator it_c;
  // test if the canonical geometry is not a border of the loop
  // to do so, we define a renumbering numerotation:
  // we always have renumbering[i] <= i
  // if renumbering[i] == i, then the component is unique
  // if renumbering[i] < g1.components_.size(), then g2 already exists in g1 and will not be added
  std::map<number_t, number_t> renumbering;
  number_t nn = g2.components_.size();

  // every component of a composite/loop geometry is unique
  // we just have to test the canonical geometry
  bool found = false;
  renumbering[0] = 0;
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    renumbering[it_c->first + 1] = it_c->first + 1;
    if (g1 == *(it_c->second))
    {
      renumbering[it_c->first + 1] = 0;
      found = true;
      break;
    }
  }
  renumbering[nn + 1] = nn + 1; // loop geometry g1 is necessarily unique
  if (found) { error("loop_hole_border"); }
  bool isHole = true;
  g.components_[0] = g1.clone();
  for (it_c = g2.components_.begin(); it_c != g2.components_.end(); ++it_c)
  {
    g.components_[renumbering[it_c->first + 1]] = it_c->second->clone();
    if (!it_c->second->isInside(g1))
    { isHole = false; }
  }
  g.components_[renumbering[nn + 1]] = g2.clone();

  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  for (it_l = g2.loops_.begin(); it_l != g2.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[renumbering[it_l->first + 1]].push_back(renumbering[it_l->second[i] + 1]); }
  }

  // management of geometries
  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  g.geometries_[0].push_back(renumbering[0]);
  g.geometries_[0].push_back(renumbering[nn + 1]);
  for (it_g = g2.geometries_.begin(); it_g != g2.geometries_.end(); it_g++)
  {
    for (number_t i = 0; i < it_g->second.size(); i++)
    { g.geometries_[renumbering[it_g->first + 1]].push_back(renumbering[it_g->second[i] + 1]); }
  }
  if (!isHole)  { warning("hole_outside"); }
  trace_p->pop();
  return;
}

void substrCanonicalAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g)
{
  trace_p->push("substrCanonicalAndCanonical(Geometry,Geometry)");
  g.boundingBox = g1.boundingBox;
  g.dim(g1.dim());
  g.shape(_composite);
  g.minimalBox = g1.minimalBox;
  g.init();
  g.components_[0] = g1.clone();
  g.components_[1] = g2.clone();
  g.geometries_[0].push_back(0);
  g.geometries_[0].push_back(1);
  if (!g2.isInside(g1)) { warning("hole_outside"); }
  trace_p->pop();
  return;
}

Geometry& Geometry::operator-=(const Geometry& g)
{
  trace_p->push("Geometry::operator-=");
  // as g is supposed to be a hole of *this, its dimension should be less of equal g1's dimension
  if (g.dim() > dim())  { error("bad_dim", g.dim(), dim()); }
  if (this == &g)  { error("same_args", "Geometry::operator-="); }
  if (shape() == _extrusion || g.shape() == _extrusion)
  {
    error("shape_not_handled", words("shape", _extrusion));
    trace_p->pop();
    return *this;
  }
  if (shape() == _noShape)
  {
    error("shape_undef");
    trace_p->pop();
    return *this;
  }
  if (g.shape() == _noShape)  { return *this; }

  // we forbid g having at least one hole
  // it means that one of g components will not be meshed, then not a key in geometries attribute
  // => a composite geometry with holes is such that components and geometries attributes have different sizes
  if ((g.shape() == _composite) && (g.geometries_.size() != g.components_.size()))
  {
    error("hole_not_allowed");
    trace_p->pop();
    return *this;
  }
  Geometry res;
  switch (shape())
  {
    case _composite:
      switch (g.shape())
      {
        case _composite: substrCompositeAndComposite(*this, g, res); break;
        case _loop: substrCompositeAndLoop(*this, g, res); break;
        default: substrCompositeAndCanonical(*this, g, res);
      }
      break;
    case _loop:
      switch (g.shape())
      {
        case _composite: substrLoopAndComposite(*this, g, res); break;
        case _loop: substrLoopAndLoop(*this, g, res); break;
        default: substrLoopAndCanonical(*this, g, res);
      }
      break;
    default: //g1 is canonical
      switch (g.shape())
      {
        case _composite: substrCanonicalAndComposite(*this, g, res); break;
        case _loop: substrCanonicalAndLoop(*this, g, res); break;
        default: substrCanonicalAndCanonical(*this, g, res);
      }
  }
  res.cleanInclusions();
  res.geoNode_ = new GeoNode(_minusGeOp, this->clone(), g.clone());
  *this = res;
  trace_p->pop();
  return *this;
}

Geometry operator-(const Geometry& g1, const Geometry& g2)
{
  trace_p->push("Geometry::operator-");
  // as g2 is supposed to be a hole of g1, its dimension should be less of equal g1's dimension
  if (g2.dim() > g1.dim()) { error("is_greater", g2.dim(), g1.dim()); }
  if (&g1 == &g2)  { error("same_args", "Geometry::operator-"); }
  if (g1.shape() == _extrusion || g2.shape() == _extrusion)   { error("shape_not_handled", words("shape", _extrusion)); }

  // we forbid g2 having at least one hole
  // it means that one of g2 components will not be meshed, then not a key in geometries attribute
  // => a composite geometry with holes is such that components and geometries attributes have different sizes
  if ((g2.shape() == _composite) && (g2.geometries_.size() != g2.components_.size()))
  { error("hole_not_allowed"); }

  Geometry g;
  switch (g1.shape())
  {
    case _composite:
      switch (g2.shape())
      {
        case _composite: substrCompositeAndComposite(g1, g2, g); break;
        case _loop: substrCompositeAndLoop(g1, g2, g); break;
        default: substrCompositeAndCanonical(g1, g2, g);
      }
      break;
    case _loop:
      switch (g2.shape())
      {
        case _composite: substrLoopAndComposite(g1, g2, g); break;
        case _loop: substrLoopAndLoop(g1, g2, g); break;
        default: substrLoopAndCanonical(g1, g2, g);
      }
      break;
    default: //g1 is canonical
      switch (g2.shape())
      {
        case _composite: substrCanonicalAndComposite(g1, g2, g); break;
        case _loop: substrCanonicalAndLoop(g1, g2, g); break;
        default: substrCanonicalAndCanonical(g1, g2, g);
      }
  }
  //finalization
  g.cleanInclusions();
  g.geoNode_ = new GeoNode(_minusGeOp, g1.clone(), g2.clone());
  trace_p->pop();
  return g;
}

//------------------------------------------------------------------------------------------------------------
//           surfaceFrom, volumFrom stuff
//------------------------------------------------------------------------------------------------------------
Geometry surfaceFrom(const Geometry& c, string_t domName, bool isPlaneSurface) //! definition of a geometry 2D from its boundary 1D
{
  trace_p->push("surfaceFrom");
  if (c.dim() != 1) { error("bad_dim", c.dim(), 1); }
  Geometry g(c.boundingBox, 2, domName);
  g.shape(_loop);
  g.minimalBox = c.minimalBox;
  g.isPlaneSurface = isPlaneSurface;
  if (c.shape() != _composite ) // only closed curve
  {
    if (c.isClosed())
    {
      g.components_[0] = c.clone();
      g.loops_[1].push_back(0);
      g.geometries_[0].push_back(0);
      g.sideNames_.push_back(c.domName_);
      trace_p->pop();
      return g;
    }
    error("composite_or_closed_arc_only", c.domName());
  }

  //case of a composite geometry
  number_t n = c.components_.size();
  std::map<number_t, Geometry*>::const_iterator it_c;
  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_c = c.components_.begin(); it_c != c.components_.end(); ++it_c)
  {
    g.components_[it_c->first] = it_c->second->clone();
    g.loops_[n].push_back(it_c->first);
    g.sideNames_.push_back(it_c->second->domName_);
  }
  for (it_g = c.geometries_.begin(); it_g != c.geometries_.end(); ++it_g)
  {
    for (number_t i = 0; i < it_g->second.size(); ++i)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }

  trace_p->pop();
  return g;
}

Geometry volumeFrom(const Geometry& s, string_t domName) //! definition of a geometry 3D from its boundary 2D
{
  trace_p->push("volumeFrom");
  if (s.dim() != 2)  { error("bad_dim", s.dim(), 2); }
  if (s.shape() != _composite)  { error("composite_only", s.domName()); }
  Geometry g(s.boundingBox, 3, domName);
  g.shape(_loop);
  g.minimalBox = s.minimalBox;

  number_t n = s.components_.size();
  std::map<number_t, Geometry*>::const_iterator it_c;
  std::map<number_t, std::vector<number_t> >::const_iterator it_l;
  std::map<number_t, std::vector<number_t> >::const_iterator it_g;
  for (it_c = s.components_.begin(); it_c != s.components_.end(); ++it_c)
  {
    g.components_[it_c->first] = it_c->second->clone();
    if (s.geometries_.count(it_c->first))
    {
      // only surfaces can be added in g.loops_[n]
      if (it_c->second->dim() == 2)
      { g.loops_[n].push_back(it_c->first); }
      g.sideNames_.push_back(it_c->second->domName_);
    }
  }
  // in 3D, borders can be composite geometries with holes so that geometries_ has to be filled with s.geometries_.
  for (it_g = s.geometries_.begin(); it_g != s.geometries_.end(); ++it_g)
  {
    for (number_t i = 0; i < it_g->second.size(); ++i)
    { g.geometries_[it_g->first].push_back(it_g->second[i]); }
  }
  for (it_l = s.loops_.begin(); it_l != s.loops_.end(); ++it_l)
  {
    for (number_t i = 0; i < it_l->second.size(); ++i)
    { g.loops_[it_l->first].push_back(it_l->second[i]); }
  }

  trace_p->pop();
  return g;
}
//------------------------------------------------------------------------------------------------------------
//           extrusion stuff
//------------------------------------------------------------------------------------------------------------
//! management of key/value parameters for extrude routines
void ExtrusionData::buildParam(const Parameter& p)
{
  trace_p->push("ExtrusionData::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_domain_name:
    {
      domName_ = p.get_s();
      break;
    }
    case _pk_layers:
    {
      layers_ = p.get_n();
      break;
    }
    case _pk_base_names:
    {
      if (p.type() == _string) { baseDomainNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { baseDomainNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    case _pk_side_names:
    {
      if (p.type() == _string) { lateralSideNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { lateralSideNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integerVector:
        {
          std::vector<number_t> n = p.get_nv();
          n_.resize(n.size());
          for (number_t i = 0; i < n.size(); ++i) { n_[i] = n[i] > 2 ? n[i] : 2; }
          break;
        }
        case _integer: n_ = std::vector<number_t>(1, p.get_n() > 2 ? p.get_n() : 2); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _realVector: h_ = p.get_rv(); break;
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_init_transformation:
    {
       if (p.type() == _pointerTransformation)
       {
           initialTransf_ = reinterpret_cast<const Transformation*>(p.get_p())->clone();
       }
       else { error("param_badtype", words("value", p.type()), words("param key", key)); }
       break;
    }
    case _pk_center:
    {
       switch (p.type())
      {
        case _pt: center_ = p.get_pt(); break;
        case _integer: center_ = Point(real_t(p.get_i())); break;
        case _real: center_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_naming_domain:
    {
       int_t n=0;
       if (p.type() == _integer) n=p.get_n();
       else { error("param_badtype", words("value", p.type()), words("param key", key)); }
       if(n<0) n=0;
       if(n>2) n=2;
       namingDomain_=dimen_t(n);
       break;
    }
    case _pk_naming_section:
    {
       int_t n=0;
       if (p.type() == _integer) n=p.get_n();
       else { error("param_badtype", words("value", p.type()), words("param key", key)); }
       if(n<0) n=0;
       if(n>2) n=2;
       namingSection_=dimen_t(n);
       break;
    }
    case _pk_naming_side:
    {
       int_t n=0;
       if (p.type() == _integer) n=p.get_n();
       else { error("param_badtype", words("value", p.type()), words("param key", key)); }
       if(n<0) n=0;
       if(n>2) n=2;
       namingSide_=dimen_t(n);
       break;
    }
    default:
      error("geom_unexpected_param_key", words("param key", key), words("shape", _extrusion));
  }
  trace_p->pop();
}

//! management of default values of key/value parameters for extrude routines
void ExtrusionData::buildDefaultParam(ParameterKey key)
{
  trace_p->push("ExtrusionData::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_ = std::vector<number_t>(1, 2); break;
    case _pk_layers: layers_ = 0; break;
    case _pk_domain_name: domName_ = ""; break;
    case _pk_base_names: break;
    case _pk_side_names: break;
    case _pk_init_transformation:  break; // default value, nullptr
    case _pk_center: break; // default value, void Point
    case _pk_naming_domain: namingDomain_=0;break;
    case _pk_naming_section: namingSection_=0;break;
    case _pk_naming_side: namingSide_=0;break;
    default: break;
  }
  trace_p->pop();
}

ExtrusionData::ExtrusionData(const Transformation& t, bool isGeoExtrusion, const std::vector<Parameter>& ps)
{
  trace_p->push("ExtrusionData(const Transformation&, bool , const std::vector<Parameter>&)");
  extrusion_ = t.clone();
  if (isGeoExtrusion) geoParams(ps);
  else meshParams(ps);
  trace_p->pop();
}

ExtrusionData::ExtrusionData(const std::vector<Transformation*>& ts, bool isGeoExtrusion, const std::vector<Parameter>& ps)
{
  trace_p->push("ExtrusionData(const std::vector<Transformation>&, bool , const std::vector<Parameter>&)");
  number_t n=ts.size();
  transformations_.resize(n,nullptr);
  for(auto i=0;i<n;i++)
    transformations_[i]=ts[i]->clone();
  if(isGeoExtrusion) geoParams(ps);
  else meshParams(ps);
  layers_=n;
  trace_p->pop();
}

ExtrusionData::ExtrusionData(par_fun f, bool isGeoExtrusion, const std::vector<Parameter>& ps)
{
  trace_p->push("ExtrusionData(parfun, bool , const std::vector<Parameter>&)");
  fun_=f;
  if(isGeoExtrusion) geoParams(ps);
  else meshParams(ps);
  trace_p->pop();
}

void ExtrusionData::geoParams(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params = getGeoParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parallelepiped)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
  }
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
}

void ExtrusionData::meshParams(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params = getMeshParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parallelepiped)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
  }
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
}

ExtrusionData::~ExtrusionData()
{ clear(); }

void ExtrusionData::clear()
{
  if (extrusion_ != nullptr)     delete extrusion_;
  if (initialTransf_!=nullptr)   delete initialTransf_;
  for(auto& e: transformations_) delete e;
}

ExtrusionData::ExtrusionData(const ExtrusionData& e)
  : layers_(e.layers_), domName_(e.domName_), baseDomainNames_(e.baseDomainNames_), lateralSideNames_(e.lateralSideNames_), n_(e.n_), h_(e.h_),
    namingDomain_(e.namingDomain_), namingSection_ (e.namingSection_), namingSide_(e.namingSide_)
{
  clear();
  if(e.extrusion_!=nullptr) extrusion_ = e.extrusion_->clone();
  if(e.initialTransf_!=nullptr) initialTransf_= e.initialTransf_->clone();
  number_t n=e.transformations_.size();
  if(n==0) return;
  transformations_.resize(n,nullptr);
  for(auto i=0;i<n;i++)
     transformations_[i]=e.transformations_[i]->clone();
}

ExtrusionData& ExtrusionData::operator=(const ExtrusionData& e)
{
  if (this == &e) return *this;
  if(e.extrusion_!=nullptr)
  {
    if(extrusion_!=nullptr && extrusion_!=e.extrusion_) delete extrusion_;
    extrusion_ = e.extrusion_->clone();
  }
  layers_ = e.layers_;
  domName_ = e.domName_;
  baseDomainNames_ = e.baseDomainNames_;
  lateralSideNames_ = e.lateralSideNames_;
  n_ = e.n_;
  h_ = e.h_;
  fun_ = e.fun_;
  if(e.initialTransf_!=nullptr)
  {
    if(initialTransf_!=nullptr && initialTransf_!=e.initialTransf_) delete initialTransf_;
    initialTransf_ = e.initialTransf_->clone();
  }
  for(auto& e: transformations_) delete e;
  transformations_.clear();
  number_t n=e.transformations_.size();
  if(n>0)
  {
    transformations_.resize(n,nullptr);
    for(auto i=0;i<n;i++)
      transformations_[i]=e.transformations_[i]->clone();
  }
  namingDomain_ = e.namingDomain_;
  namingSection_ = e.namingSection_;
  namingSide_ = e.namingSide_;
  center_ = e.center_;
  return *this;
}

std::set<ParameterKey> ExtrusionData::getGeoParamsKeys()
{
  std::set<ParameterKey> params;
  params.insert(_pk_domain_name);
  params.insert(_pk_layers);
  params.insert(_pk_base_names);
  params.insert(_pk_side_names);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

std::set<ParameterKey> ExtrusionData::getMeshParamsKeys()
{
  std::set<ParameterKey> params;
  params.insert(_pk_layers);
  params.insert(_pk_domain_name);
  params.insert(_pk_base_names);
  params.insert(_pk_side_names);
  params.insert(_pk_init_transformation);
  params.insert(_pk_naming_domain);
  params.insert(_pk_naming_section);
  params.insert(_pk_naming_side);
  params.insert(_pk_center);
  return params;
}

//! main routine for the definition of a geometry by extrusion of another geometry, with a list if parameters
Geometry extrude(const Geometry& g, const Transformation& t, const std::vector<Parameter>& ps)
{
  BoundingBox bbe = g.boundingBox;
  bbe.transform(t);
  bbe += g.boundingBox;
  Geometry e(bbe, g.dim_ + 1, "", _extrusion);
  e.minimalBox = MinimalBox(bbe.bounds());
  e.components_[0] = g.clone();
  e.extrusionData_ = new ExtrusionData(t, true, ps);
  e.domName() = e.extrusionData_->domName();
  e.sideNames_ = e.extrusionData_->lateralSideNames();
  return e;
}

//! Definition of a geometry by extrusion of another geometry, with 1 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p)
{
  std::vector<Parameter> ps = {p};
  return extrude(g, t, ps);
}
//! Definition of a geometry by extrusion of another geometry, with 2 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2)
{
  std::vector<Parameter> ps = {p1, p2};
  return extrude(g, t, ps);
}
//! Definition of a geometry by extrusion of another geometry, with 3 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3)
{
  std::vector<Parameter> ps = {p1, p2, p3};
  return extrude(g, t, ps);
}
//! Definition of a geometry by extrusion of another geometry, with 4 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3, Parameter p4)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4};
  return extrude(g, t, ps);
}
//! Definition of a geometry by extrusion of another geometry, with 5 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5};
  return extrude(g, t, ps);
}
//! Definition of a geometry by extrusion of another geometry, with 6 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6)
{
  std::vector<Parameter> ps = {p1, p2, p3, p4, p5, p6};
  return extrude(g, t, ps);
}
//------------------------------------------------------------------------------------------------------------
//                                  composite stuff
//------------------------------------------------------------------------------------------------------------
/*!
    conversion of a canonical or loop Geometry into a composite geometry with one component
    - if g is canonical, the components_ vector contains only g
    - if g is loop, the components_ vector contains every border of g and g itself
    - if g is composite, it does nothing

    this function is necessary when initializing composite Geometry before using += and -= operators
*/
Geometry toComposite(const Geometry& g)
{
  if (g.shape() == _composite) { return g; }

  Geometry g2(g.boundingBox, g.dim(), g.domName_);
  g2.shape(_composite);

  if (g.shape() == _loop)
  {
    std::map<number_t, Geometry*>::const_iterator it_c;
    number_t n = 0;
    for (it_c = g.components_.begin(); it_c != g.components_.end(); ++it_c)
    {
      g2.components_[it_c->first] = it_c->second->clone();
      if (it_c->first > n)
      { n = it_c->first; }
    }
    g2.components_[n + 1] = g.clone();

    // management of loops
    std::map<number_t, std::vector<number_t> >::const_iterator it_l;
    for (it_l = g.loops_.begin(); it_l != g.loops_.end(); ++it_l)
    {
      for (number_t i = 0; i < it_l->second.size(); ++i)
      { g2.loops_[n + 1].push_back(it_l->second[i]); }
    }

    // management of geometries
    g2.geometries_[n + 1].push_back(n + 1);
  }
  else
  {
    g2.components_[0] = g.clone();
    g2.geometries_[0].push_back(0);
    g2.geoNode_ = new GeoNode(_noneGeOp, g.clone());
  }
  return g2;
}

//! find if a curve/surf is in a list of curves/surfs
int_t findBorder(const std::pair<ShapeType, std::vector<const Point*> >& border, const std::vector<std::pair<ShapeType, std::vector<const Point*> > >& borders)
{
  if (borders.size() == 0) { return -1; }
  std::vector<std::pair<ShapeType, std::vector<const Point*> > >::const_iterator it;
  std::vector<const Point*>::const_iterator it2;
  int_t j = 0;
  for (it = borders.begin(); it != borders.end(); it++, j++)
  {
    if (border.first == it->first)
    {
      std::vector<bool> ptShared(border.second.size(), false);
      for (number_t i = 0; i < border.second.size(); ++i)
      {
        if (border.second.size() == it->second.size())
        {
          for (it2 = it->second.begin(); it2 != it->second.end() && ptShared[i] == false; it2++)
          {
            if (force3D(*(border.second[i])) == force3D(**it2))
            { ptShared[i] = true; }
          }
        }
      }
      bool isFound = ptShared[0];
      for (number_t i = 1; i < border.second.size(); i++)
      { isFound = isFound && ptShared[i]; }
      if (isFound)
      { return j; }
    }
  }
  return -1;
}

/*!
  determine if 2 lists of points ordered differently have the same orientation
  It is supposed that every point in each list is also in the other one
*/
bool sameOrientation(std::vector<const Point*> border1, std::vector<const Point*> border2)
{
  bool ptFound = false;
  number_t n = border1.size();

  // we find the location of the first point of border1 in border2
  number_t i;
  for (i = 0; i < n && ptFound == false; ++i)
  {
    if (force3D(*(border1[0])) == force3D(*(border2[i])))
    { ptFound = true; }
  }
  --i;

  // if the second point of border1 is next to the first one in border2 then both borders have the same orientation
  // if the second point of border1 is previous to the first one in border2 then both borders does not have the same orientation
  if (!ptFound)
  { return false; }
  if (i < n - 1)
  {
    if (force3D(*(border1[1])) == force3D(*(border2[i + 1])))
    { return true; }
    else
    { return false; }
  }
  else
  {
    if (n == 2)
    { return false; }  // specific case when the geometry is a segment
    if (force3D(*(border1[1])) == force3D(*(border2[0])))
    { return true; }
    else
    { return false; }
  }
  return false;
}

/*! check if points pts1 are related by a translation to points pts2
    if true update the translation vector T
    translation vector candidate is built from first construction point (may be not sufficient)
    any point of pts1 must be related by T to a point of pts2
*/
bool isTranslatedPoints(const std::vector<Point>& pts1, const std::vector<Point>& pts2, Point& T)
{
  T = pts2[0] - pts1[0];
  std::vector<Point>::const_iterator it1 = pts1.begin(), it2 = pts2.begin(), it2e = pts2.end();
  it1++;
  for (; it1 != pts1.end(); ++it1)
  {
    it2 = pts2.begin(); it2++;
    bool found = false;
    while (!found && it2 != it2e) // check all  points of pts2, algorithm may be improved
      if (norm(*it2 - *it1) > theEpsilon) { it2++; }
      else { found = true; }
    if (!found) { return false; }
  }
  return true;
}

void crack(Geometry& g1, CrackType ct, string_t domNameToOpen)
{
  g1.crack(ct, domNameToOpen);
}

void crack(Geometry& g1, Geometry& g2, CrackType ct, string_t domNameToOpen)
{
  g1.crack(ct, domNameToOpen);
  g2.crack(ct, domNameToOpen);
}

void crack(Geometry& g1, Geometry& g2, Geometry& g3, CrackType ct, string_t domNameToOpen)
{
  g1.crack(ct, domNameToOpen);
  g2.crack(ct, domNameToOpen);
  g3.crack(ct, domNameToOpen);
}

void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, CrackType ct, string_t domNameToOpen)
{
  g1.crack(ct, domNameToOpen);
  g2.crack(ct, domNameToOpen);
  g3.crack(ct, domNameToOpen);
  g4.crack(ct, domNameToOpen);
}

void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, CrackType ct, string_t domNameToOpen)
{
  g1.crack(ct, domNameToOpen);
  g2.crack(ct, domNameToOpen);
  g3.crack(ct, domNameToOpen);
  g4.crack(ct, domNameToOpen);
  g5.crack(ct, domNameToOpen);
}

void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6, CrackType ct, string_t domNameToOpen)
{
  g1.crack(ct, domNameToOpen);
  g2.crack(ct, domNameToOpen);
  g3.crack(ct, domNameToOpen);
  g4.crack(ct, domNameToOpen);
  g5.crack(ct, domNameToOpen);
  g6.crack(ct, domNameToOpen);
}

void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6, Geometry& g7, CrackType ct, string_t domNameToOpen)
{
  g1.crack(ct, domNameToOpen);
  g2.crack(ct, domNameToOpen);
  g3.crack(ct, domNameToOpen);
  g4.crack(ct, domNameToOpen);
  g5.crack(ct, domNameToOpen);
  g6.crack(ct, domNameToOpen);
  g7.crack(ct, domNameToOpen);
}

/*!
  We check if sideNames_ has size 0, 1, or n and return an equivalent Strings of size n
  n2 is an optional secondary possible size (case for some particular geometries)
 */
Strings Geometry::buildSideNamesAfterCheck(number_t n, number_t n2) const
{
  number_t snsize = sideNames_.size();
  if (n2 == 0) { n2 = n; }
  if (snsize == 0) { return Strings(n, ""); }
  else if (snsize == 1) { return Strings(n, sideNames_[0]); }
  else if (sideNames_.size() != n && sideNames_.size() != n2)
  { error("bad_size", words("shape", shape_) + " sideNames", n, snsize); }
  return sideNames_;
}

/*!
  We check if sideNames_ has size 0, 1, or n and update it with size n
  n2 is an optional secondary possible size (case for some particular geometries)
 */
void Geometry::checkSideNamesAndUpdate(number_t n, number_t n2)
{
  number_t snsize = sideNames_.size();
  if (n2 == 0) { n2 = n; }
  if (snsize == 0)
  { sideNames_.resize(n, ""); }
  else if (snsize == 1)
  { sideNames_.resize(n, sideNames_[0]); }
  else if (sideNames_.size() != n && sideNames_.size() != n2)
  { error("bad_size", words("shape", shape_) + " sideNames", n, snsize); }
}

// print facilities
//! print Geometry boundNodes() vector
void Geometry::printBoundNodes(std::ostream& os) const
{
  std::vector<const Point*> nodes = boundNodes();
  os << *nodes[0];
  for (number_t i = 1; i < nodes.size(); ++i)
  { os << " " << *nodes[i]; }
  os << std::endl;
}

//! print Geometry nodes() vector
void Geometry::printNodes(std::ostream& os) const
{
  std::vector<const Point*> nnodes = nodes();
  os << *nnodes[0];
  for (number_t i = 1; i < nnodes.size(); ++i)
  { os << " " << *nnodes[i]; }
  os << std::endl;
}

//! print Geometry curves() vector
void Geometry::printCurves(std::ostream& os) const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > cnodes = curves();
  os << words("shape", cnodes[0].first);
  for (number_t j = 0; j < cnodes[0].second.size(); ++j)
  { os << " " << *cnodes[0].second[j]; }
  os << std::endl;
  for (number_t i = 1; i < cnodes.size(); ++i)
  {
    os << words("shape", cnodes[i].first);
    for (number_t j = 0; j < cnodes[i].second.size(); ++j)
    { os << " " << *cnodes[i].second[j]; }
    os << std::endl;
  }
  os << std::endl;
}

//! print Geometry surfs() vector
void Geometry::printSurfs(std::ostream& os) const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > snodes = surfs();
  os << words("shape", snodes[0].first);
  for (number_t j = 0; j < snodes[0].second.size(); ++j)
  { os << " " << *snodes[0].second[j]; }
  os << std::endl;
  for (number_t i = 1; i < snodes.size(); ++i)
  {
    os << words("shape", snodes[i].first);
    for (number_t j = 0; j < snodes[i].second.size(); ++j)
    { os << " " << *snodes[i].second[j]; }
    os << std::endl;
  }
  os << std::endl;
}

void Geometry::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  string_t na = "";
  if (isCanonical()) { na = asString(); }
  if (na != "") { na = "\"" + na + "\" "; }
  os << "Geometry " << na << ", of shape type " << words("shape", shape_) << ", of dimension " << dim_;
  if (theVerboseLevel > 5)
  {
    os << eol;
    printDetail(os);
  }
  else { os << ", "; }
  os << eol << "  " << boundingBox;
  if (minimalBox.dim() > 0)  { os << "," << eol << "  " << minimalBox; }
  os << ",\n  Names of variable:";
  for (dimen_t i = 0; i < theNamesOfVariables_.size(); i++)
  {
    if (i != 0) { os << ","; }
    os << " " << theNamesOfVariables_[i];
  }

  if (domName_ != "") { os << ", domain name: " << domName_; }

  if (sideNames_.size() > 0)
  {
    bool isSideNames = false;
    for (dimen_t i = 0; i < sideNames_.size() && !isSideNames; ++i)
    { if (sideNames_[i] != "") isSideNames = true; }
    if (isSideNames)
    {
      os << eol << "   side names:";
      for (dimen_t i = 0; i < sideNames_.size(); i++)
      {
        if (i != 0 && sideNames_[i - 1] != "") { os << ","; }
        if (sideNames_[i] != "") { os << " " << sideNames_[i]; }
      }
    }
  }

  if (shape_ == _loop)
  {
    os << eol << "- borders:";
    std::map<number_t, Geometry*>::const_iterator it_b;
    for (it_b = components_.begin(); it_b != components_.end(); ++it_b)
    {
      string_t nna = it_b->second->asString();
      if (nna != "") { nna = "\"" + nna + "\" "; }
      os << eol << "[" << it_b->first << "] ";
      os << "Geometry " << nna << "of shape type " << words("shape", it_b->second->shape_);
      os << " of dimension " << it_b->second->dim_ << ", " << it_b->second->boundingBox;
      if (it_b->second->domName_ != "") { os << ", domain name: " << it_b->second->domName_; }
    }
    os << eol << "- loops:";
    std::map<number_t, std::vector<number_t> >::const_iterator it_l;
    for (it_l = loops_.begin(); it_l != loops_.end(); ++it_l)
    {
      number_t n = it_l->second.size();
      os << eol << "  geometry " << it_l->first;
      if (n == 0) { os << " (no borders)"; }
      else { os << " (" << it_l->second.size() << " borders):"; }
      for (number_t i = 0; i < n; i++) { os << " " << it_l->second[i]; }
    }
    os << eol << "- holes:";
    std::map<number_t, std::vector<number_t> >::const_iterator it_g;
    for (it_g = geometries_.begin(); it_g != geometries_.end(); ++it_g)
    {
      number_t n = it_g->second.size() - 1;
      os << eol << "  geometry " << it_g->first;
      if (n == 0) { os << " (no holes)"; }
      else { os << " (" << it_g->second.size() - 1 << " holes):"; }
      for (number_t i = 0; i < n; ++i) { os << " " << it_g->second[i + 1]; }
      os << "   [ data:";
      for (number_t i = 0; i < n + 1; ++i) { os << " " << it_g->second[i]; }
      os << " ]";
    }
  }
  if (shape_ == _composite)
  {
    os << eol << "   - components:";
    std::map<number_t, Geometry*>::const_iterator it_c, it_b;
    for (it_c = components_.begin(); it_c != components_.end(); ++it_c)
    {
      string_t nna = it_c->second->asString();
      if (nna != "") { nna = "\"" + nna + "\" "; }
      os << eol << "     [" << it_c->first << "] ";
      os << "Geometry " << nna << "of shape type " << words("shape", it_c->second->shape_);
      os << " of dimension " << it_c->second->dim_ << ", " << it_c->second->boundingBox;
      if (it_c->second->domName_ != "") { os << ", domain name: " << it_c->second->domName_; }
      if (it_c->second->sideNames_.size() > 0)
      {
        bool isSideNames = false;
        for (number_t i = 0; i < it_c->second->sideNames_.size() && isSideNames == false; ++i)
        { if (it_c->second->sideNames_[i] != "") isSideNames = true; }
        if (isSideNames) { os << ", side names: " << it_c->second->sideNames_; }
      }
    }
    os << eol << "   - borders:";
    std::map<number_t, std::vector<number_t> >::const_iterator it_l;
    for (it_l = loops_.begin(); it_l != loops_.end(); ++it_l)
    {
      number_t n = it_l->second.size();
      os << eol << "     geometry " << it_l->first;
      if (n == 0) { os << " (no borders)"; }
      else { os << " (" << it_l->second.size() << " borders):"; }
      for (number_t i = 0; i < n; ++i) { os << " " << it_l->second[i]; }
    }
    os << eol << "   - holes:";
    std::map<number_t, std::vector<number_t> >::const_iterator it_g;
    for (it_g = geometries_.begin(); it_g != geometries_.end(); ++it_g)
    {
      number_t n = it_g->second.size() - 1;
      os << eol << "     geometry " << it_g->first;
      if (n == 0) { os << " (no holes)"; }
      else { os << " (" << it_g->second.size() - 1 << " holes):"; }
      for (number_t i = 0; i < n; ++i) { os << " " << it_g->second[i + 1]; }
      os << "   [ data:";
      for (number_t i = 0; i < n + 1; ++i) { os << " " << it_g->second[i]; }
      os << " ]";
    }
  }

  if (shape_ == _extrusion)
  {
    os << eol << "   - section:";
    os << eol << *components_[0];
    os << eol << "   - layers: " << extrusionData_->layers();
    os << eol << "   - extrusion by:";
    os << eol << *(extrusionData_->extrusion());
  }

  if (geoNode_ != nullptr)
  {
    os << eol << "   - tree representation: " << expand(*geoNode_).asString();
  }
}

std::ostream& operator<<(std::ostream& os, const Geometry& g)
{
  g.print(os);
  return os;
}

void Geometry::addSuffix(const string_t& s)
{
  if (domName_ != "" && domName_.find("#") != 0)
  { domName_ += "_" + s; }
  for (number_t i = 0; i < sideNames_.size(); ++i)
  {
    if (sideNames_[i] != "" && sideNames_[i].find("#") != 0)
    { sideNames_[i] += "_" + s; }
  }
  for (number_t i = 0; i < sideOfSideNames_.size(); ++i)
  {
    if (sideOfSideNames_[i] != "" && sideOfSideNames_[i].find("#") != 0)
    { sideOfSideNames_[i] += "_" + s; }
  }
  for (number_t i = 0; i < sideOfSideOfSideNames_.size(); ++i)
  {
    if (sideOfSideOfSideNames_[i] != "" && sideOfSideOfSideNames_[i].find("#") != 0)
    { sideOfSideOfSideNames_[i] += "_" + s; }
  }
  if (shape_ == _composite || shape_ == _loop)
  {
    std::map<number_t, Geometry*>::const_iterator it_c;
    for (it_c = components_.begin(); it_c != components_.end(); ++it_c)
    { it_c->second->addSuffix(s); }
  }
}

//---------------------------------------------------------------------------
// Geometry transformations facilities
//---------------------------------------------------------------------------

//! apply a geometrical transformation on a Geometry
Geometry& Geometry::transform(const Transformation& t)
{
  if (shape_ != _composite && shape_ != _loop && shape_ != _fromFile)
  {
    where("Geometry::transform");
    error("shape_not_handled", words("shape", shape_));
  }

  if (shape_ != _fromFile)
  {
    std::map<number_t, Geometry*>::const_iterator it_c;
    for (it_c = components_.begin(); it_c != components_.end(); ++it_c)
    { it_c->second->transform(t); }
  }
  ocTransform(t);  //transform the OC object if available else do nothing
  boundingBox.transform(t);
  minimalBox.transform(t);
  return *this;
}

//! apply a geometrical transformation on a Geometry (external)
Geometry transform(const Geometry& g, const Transformation& t)
{
  if (g.shape() != _composite && g.shape() != _loop && g.shape() != _fromFile)
  {
    where("xlifepp::transform(const Geometry&, ...)");
    error("shape_not_handled", words("shape", g.shape()));
  }
  Geometry g2 = g;
  // we call the corresponding member function, as a Geometry object carries few data
  g2.transform(t);
  return g2;
}

//========================================================================================
//                            GeoNode class stuff
//========================================================================================
//! copy constructor, no deep copy of geometry pointers
GeoNode::GeoNode(const GeoNode& gn) //copy constructor (no deep copy of geometry pointers, see updateGeo)
{
  geop_ = gn.geop_;
  level_ = gn.level_;
  geom1_ = gn.geom1_;
  geom2_ = gn.geom2_;
  node1_ = nullptr; node2_ = nullptr;
  if (gn.node1_ != nullptr) { node1_ = new GeoNode(*gn.node1_); }
  if (gn.node2_ != nullptr) { node2_ = new GeoNode(*gn.node2_); }
}

//! assign operator, no deep copy of geometry pointers
GeoNode& GeoNode::operator=(const GeoNode& gn)
{
  if (&gn == this) { return *this; }
  geop_ = gn.geop_;
  level_ = gn.level_;
  geom1_ = gn.geom1_;
  geom2_ = gn.geom2_;
  if (node1_ != nullptr) { delete node1_; }
  if (node2_ != nullptr) { delete node2_; }
  node1_ = nullptr; node2_ = nullptr;
  if (gn.node1_ != nullptr) { node1_ = new GeoNode(*gn.node1_); }
  if (gn.node2_ != nullptr) { node1_ = new GeoNode(*gn.node2_); }
  return *this;
}

//! destructor, geometry pointeurs are not deleted
GeoNode::~GeoNode()
{
  if (node1_ != nullptr) { delete node1_; }
  if (node2_ != nullptr) { delete node2_; }
}

//!< increment by one level_ of each node from current node level
void GeoNode::updateLevel(number_t l)
{
  level_ = l;
  if (node1_ != nullptr) { node1_->updateLevel(l + 1); }
  if (node2_ != nullptr) { node1_->updateLevel(l + 1); }
}

//! interpret node as a string for printing purpose
string_t GeoNode::asString() const
{
  if (geop_ == _noneGeOp)
  {
    if (geom1_->domName() != "") { return geom1_->domName(); }
    else { return words("shape", geom1_->shape()); }
  }
  string_t res;
  if (geop_ > _noneGeOp && geop_ < _loopGeOp) // binary structure
  {
    res = "(";
    if (node1_ != nullptr) { res += node1_->asString(); }
    else
    {
      if (geom1_->domName() != "") { res += geom1_->domName(); }
      else { res += words("shape", geom1_->shape()); };
    }
    res += xlifepp::asString(geop_);
    if (node2_ != nullptr) { res += node2_->asString(); }
    else
    {
      if (geom2_->domName() != "")  { res += geom2_->domName(); }
      else { res += words("shape", geom2_->shape()); };
    }
    res += ")";
  }
  else
  {
    res = xlifepp::asString(geop_) + "(";
    if (node1_ != nullptr) { res += node1_->asString(); }
    else
    {
      if (geom1_->domName() != "") { res += geom1_->domName(); }
      else { res += words("shape", geom1_->shape()); };
    }
    res += ")";
  }
  return res;
}

/*! recursive update of geometry pointers using the map relating old to new geometry pointers
    be cautious as copy or assignment does not realize a deep copy of geometry pointers
    to maintain integrity, they have probably to be updated after a copy or an assign operation */

void GeoNode::updateGeo(std::map<Geometry*, Geometry*>& newgeo)
{
  if (geom1_ != nullptr && newgeo.find(geom1_) != newgeo.end()) { geom1_ = newgeo[geom1_]; }
  if (geom2_ != nullptr && newgeo.find(geom2_) != newgeo.end()) { geom2_ = newgeo[geom2_]; }
  if (node1_ != nullptr) { node1_->updateGeo(newgeo); }
  if (node2_ != nullptr) { node2_->updateGeo(newgeo); }
}

//! recursive expansion of a GeoNode (substitute geometry pointers by its Geonode if exists)
void GeoNode::expand()
{
  if (geom1_ != nullptr && geom1_->geoNode_ != nullptr)
  {
    node1_ = new GeoNode(*geom1_->geoNode_); //copy node
    geom1_ = nullptr; //forget geometry, it could be kept ?
  }
  if (geom2_ != nullptr && geom2_->geoNode_ != nullptr)
  {
    node2_ = new GeoNode(*geom2_->geoNode_); //copy node
    geom2_ = nullptr; //forget geometry, it could be kept ?
  }
  if (node1_ != nullptr) { node1_->expand(); }
  if (node2_ != nullptr) { node2_->expand(); }
}

//! create the expansion of a GeoNode (new object)
GeoNode expand(const GeoNode& gn)
{
  GeoNode egn(gn); // copy original
  egn.expand();    // recursive expansion
  return egn;
}

//! give a string representation of GeoOperation
string_t asString(GeoOperation op)
{
  switch (op)
  {
    case _plusGeOp: return "+";
    case _minusGeOp: return "-";
    case _commonGeOp: return "^";
    case _loopGeOp: return "loop";
    case _extrusionGeOp: return "extrude";
    case _noneGeOp:
    default: break;
  }
  return string_t();
}

} // end of namespace xlifepp
