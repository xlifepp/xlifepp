/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Mesh.cpp
  \authors D. Martin, Y. Lafranche, E. Lunéville, N. Kielbasiewicz
  \since 26 may 2010
  \date 5 mar 2014

  \brief Implementation of xlifepp::Mesh class members and related functions
*/

#include "Mesh.hpp"
#include "utils.h"

namespace xlifepp
{

bool CrackData::operator<(const CrackData& cd) const
{
  if (dim != cd.dim) { return dim < cd.dim; }
  else {return id < cd.id; }
}

std::vector<real_t> CrackData::computeNormal() const
{
  std::vector<real_t> normal;
  if (dim == 1)
  {
    Point p=minimalBox.boundPt(2)-minimalBox.boundPt(1);
    normal.resize(p.size());
    if (p.x() == 0)
    {
      normal[0]=0.;
      normal[1]=-p.z();
      normal[2]=p.y();
      return normal;
    }
    if (p.y() == 0)
    {
      normal[0]=p.z();
      normal[1]=0.;
      normal[2]=-p.x();
      return normal;
    }
    if (p.z() == 0)
    {
      normal[0]=-p.y();
      normal[1]=p.x();
      normal[2]=0.;
      return normal;
    }
    error("abnormal_failure");
    return normal;
  }
  if (dim == 2)
  {
    Point p1=minimalBox.boundPt(2)-minimalBox.boundPt(1);
    Point p2=minimalBox.boundPt(3)-minimalBox.boundPt(1);
    return crossProduct(p1,p2);
  }
  error("nosuchcase","CrackData::computeNormal","dim=3");
  return normal;
}

void CrackData::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << "Cracked domain: id " << id << ", name " << domainName << ", dim " << dim << std::endl;
}

std::ostream& operator<<(std::ostream& os, const CrackData& crackData)
{
  crackData.print(os);
  return os;
}

short int findId(std::vector<CrackData>::const_iterator it_b, std::vector<CrackData>::const_iterator it_e, number_t id)
{
  std::vector<CrackData>::const_iterator it_cd;
  number_t i=0;
  for (it_cd=it_b; it_cd != it_e; ++it_cd, ++i)
  {
    if (it_cd->id == id) { return i; }
  }
  return -1;
}

//! void constructor
Mesh::Mesh()
  : geometry_p(nullptr), lastIndex_(0), name_(""), comment_(""), isMadeOfSimplices_(true), order_(0), firstOrderMesh_p(nullptr)
{}

//! construct mesh from file
void Mesh::buildParam(const Parameter& p, IOFormat& iof, number_t& nodesDim)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_dim:
    {
      if (p.type()==_integer) { nodesDim=p.get_n(); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_format:
    {
      switch (p.type())
      {
        case _integer:
        case _enumIOFormat:
          iof=IOFormat(p.get_n());
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_name:
    {
      switch (p.type())
      {
        case _string:
          this->name_ = p.get_s();
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    default:
    {
      where("Mesh::Mesh(String, ...)");
      error("unexpected_param_key", words("param key", p.key()));
    }
  }
}

void Mesh::buildParam(const Parameter& p, ShapeType& shape, number_t& order, MeshGenerator& generator, MeshPattern& pattern, StructuredMeshSplitRule& splitDirection)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_shape:
    {
      switch (p.type())
      {
        case _integer:
        case _enumShapeType:
          shape=ShapeType(p.get_n());
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    case _pk_order:
    {
      switch (p.type())
      {
        case _integer:
          order=p.get_n();
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_generator:
    {
      switch (p.type())
      {
        case _integer:
        case _enumMeshGenerator:
          generator=MeshGenerator(p.get_n());
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_pattern:
    {
      switch (p.type())
      {
        case _integer:
        case _enumMeshGenerator:
          pattern=MeshGenerator(p.get_n());
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_name:
    {
      switch (p.type())
      {
        case _string:
          this->name_ = p.get_s();
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_split_direction:
    {
      switch (p.type())
      {
        case _integer:
        case _enumStructuredMeshSplitRule:
          splitDirection=StructuredMeshSplitRule(p.get_n());
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_default_hstep:
    {
      switch (p.type())
      {
        case _real:
        {
          real_t h = p.get_r();
          meshData.hmin()=h;
          meshData.hmax()=h;
          break;
        }
        case _realVector:
        {
          std::vector<real_t> h = p.get_rv();
          if (h.size() != 2) { error("bad_size", 2, h.size()); }
          meshData.hmin()=h[0];
          meshData.hmax()=h[1];
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    default:
    {
      where("Mesh::Mesh(Geometry, ...)");
      error("unexpected_param_key", words("param key", p.key()));
    }
  }
}

void Mesh::buildParam(const Parameter& p, number_t& refinementDepth, number_t& order)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_order:
    {
      switch (p.type())
      {
        case _integer:
          order=p.get_n();
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_refinement_depth:
    {
      switch (p.type())
      {
        case _integer:
          refinementDepth=p.get_n();
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_name:
    {
      switch (p.type())
      {
        case _string:
          this->name_ = p.get_s();
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_default_hstep:
    {
      switch (p.type())
      {
        case _real:
        {
          real_t h = p.get_r();
          meshData.hmin()=h;
          meshData.hmax()=h;
          break;
        }
        case _realVector:
        {
          std::vector<real_t> h = p.get_rv();
          if (h.size() != 2) { error("bad_size", 2, h.size()); }
          meshData.hmin()=h[0];
          meshData.hmax()=h[1];
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    default:
    {
      where("Mesh::Mesh(Mesh, ...)");
      error("unexpected_param_key", words("param key", p.key()));
    }
  }
}

void Mesh::build(const string_t& filename, const std::vector<Parameter>& ps)
{
  geometry_p=nullptr;
  lastIndex_=0;
  firstOrderMesh_p=nullptr;
  IOFormat iof=_undefFormat;
  number_t nodesDim=0;

  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_name);
  params.insert(_pk_format);
  params.insert(_pk_dim);

  // managing params
  for (number_t i=0; i < ps.size(); ++i)
  {
    ParameterKey key=ps[i].key(); 
    buildParam(ps[i], iof, nodesDim);

    if (params.find(key) != params.end()) params.erase(key);
    else
    {
      if (usedParams.find(key) == usedParams.end()) error("unexpected_parameter", words("param key", key));
      else error("param_already_used", words("param key", key));
    }
    usedParams.insert(key);
  }

  std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
  // file extension has priority to key _format
  if (rootext.second == "m")
  {
    if (iof!=_undefFormat && iof!=_matlab) { warning("file_extension_priority", "m", words("ioformat", iof), words("ioformat", _matlab)); }
    iof=_matlab;
  }
  else if (rootext.second == "vtk")
  {
    if (iof!=_undefFormat && iof!=_vtk) { warning("file_extension_priority", "vtk", words("ioformat", iof), words("ioformat", _vtk)); }
    iof=_vtk;
  }
  else if (rootext.second == "vtu")
  {
    if (iof!=_undefFormat && iof!=_vtu) { warning("file_extension_priority", "vtu", words("ioformat", iof), words("ioformat", _vtu)); }
    iof=_vtu;
  }
  else if (rootext.second == "mel")
  {
    if (iof!=_undefFormat && iof!=_mel) { warning("file_extension_priority", "vtu", words("ioformat", iof), words("ioformat", _mel)); }
    iof=_mel;
  }
  else if (rootext.second == "msh")
  {
    if (iof!=_undefFormat && iof!=_msh) { warning("file_extension_priority", "msh", words("ioformat", iof), words("ioformat", _msh)); }
    iof=_msh;
  }
  else if (rootext.second == "geo")
  {
    if (iof!=_undefFormat && iof!=_geo) { warning("file_extension_priority", "geo", words("ioformat", iof), words("ioformat", _geo)); }
    iof=_geo;
  }
  else if (rootext.second == "xyvz")
  {
    if (iof!=_undefFormat && iof!=_xyzv) { warning("file_extension_priority", "xyzv", words("ioformat", iof), words("ioformat", _xyzv)); }
    iof=_xyzv;
  }
  else if (rootext.second == "ply")
  {
    if (iof!=_undefFormat && iof!=_ply) { warning("file_extension_priority", "ply", words("ioformat", iof), words("ioformat", _ply)); }
    iof=_ply;
  }
  else if (rootext.second == "mesh")
  {
    if (iof!=_undefFormat && iof!=_medit && iof!=_vizir4 ) 
    { 
      warning("file_extension_priority", "txt", words("ioformat", iof), words("ioformat", _medit)); 
    }
    // else if (iof!=_undefFormat && iof!=_medit)
    else if ( iof!=_medit)
    {
      iof=_vizir4;
    }
    else if (iof!=_undefFormat && iof!=_vizir4)
    {
      iof=_medit;
    }
  }
  else
  {
    if (rootext.second != "") { warning("file_extension_ignored", rootext.second); }
    if (iof == _undefFormat)
    {
      warning("file_default_extension", words("ioformat", _msh));
      iof=_msh;
      rootext.second = "msh";
    }
  }

  string_t filename2=fileNameFromComponents(rootext.first, rootext.second);
  switch (iof)
  {
    case _msh:
      loadGmsh(filename2, nodesDim);
      break;
    case _mel:
      loadMelina(filename2, nodesDim);
      break;
    case _geo:
      loadGeo(filename2, nodesDim);
      break;
    case _ply:
      loadPly(filename2, nodesDim);
      break;
    case _vtk:
      loadVtk(filename2, nodesDim);
      break;
    case _vtu:
      loadVtu(filename2, nodesDim);
      break;
    case _medit:
      loadMedit(filename2, nodesDim);
      break;
    case _vizir4:
      loadVizir4(filename2, nodesDim);
      break;
    default:
      error("not_yet_implemented", "Mesh::Mesh(String[, _name=xxx[,_format=yyy[, _dim=zzz]]])");
      break;
  }
  if (geometry_p!=nullptr) { delete geometry_p; }
  geometry_p = new Geometry(BoundingBox(computeBB()), "read from file " + basenameWithExtension(filename), _fromFile);
}

void Mesh::build(const Geometry& g, std::vector<Parameter>& ps)
{
  geometry_p=nullptr;
  lastIndex_=0;
  firstOrderMesh_p=nullptr;

  ShapeType shape=_noShape;
  number_t order=1;
  MeshGenerator mg=_defaultGenerator;
  MeshPattern pattern=_defaultPattern;
  StructuredMeshSplitRule splitDirection=_noSplitRule;

  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_name);
  params.insert(_pk_order);
  params.insert(_pk_generator);
  params.insert(_pk_shape);
  params.insert(_pk_pattern);
  params.insert(_pk_split_direction);
  params.insert(_pk_default_hstep);

  // managing params
  for (number_t i=0; i < ps.size(); ++i)
  {
    ParameterKey key=ps[i].key();
    buildParam(ps[i], shape, order, mg, pattern, splitDirection);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) error("unexpected_parameter", words("param key", key));
      else error("param_already_used", words("param key", key));
    }
    usedParams.insert(key);
  }
  if (g.dim() == 1 && shape == _noShape) { shape=_segment; }
  if (g.dim() == 2 && shape == _noShape) { shape=_triangle; }
  if (g.dim() == 3 && shape == _noShape) { shape=_tetrahedron; }

  buildMesh(g, shape, order, mg, pattern, splitDirection);
}

// constructor from a mesh m to be subdivided refinementDepth times
// m is assumed to be of order 1 and made of elements all of the same shape.
// The resulting mesh is of order order.
void Mesh::build(const Mesh& m, const std::vector<Parameter>& ps)
{
  geometry_p=m.geometry_p->clone();
  name_=m.name_+"_subdivided";
  if (m.order_ > 1) { error("sub_mesh_order", m.name_, m.order_); }

  number_t refinementDepth=0;
  number_t order=1;
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_name);
  params.insert(_pk_order);
  params.insert(_pk_refinement_depth);

  // managing params
  for (number_t i=0; i < ps.size(); ++i)
  {
    ParameterKey key=ps[i].key();
    buildParam(ps[i], refinementDepth, order);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) error("unexpected_parameter", words("param key", key));
      else error("param_already_used", words("param key", key));
    }
    usedParams.insert(key);
  }

  // Fetch (unique) type of elements which must be the same for all the (non boundary) subdomains
  ShapeType shty=_noShape;
  for (std::vector<GeomDomain*>::const_iterator itdom=m.domains_.begin(); itdom!=m.domains_.end(); itdom++)
  {
    if ((*itdom)->meshDomain()->isSideDomain()) { continue; }
    else
    {
      const std::set<ShapeType>& sst=(*itdom)->meshDomain()->shapeTypes;
      if (sst.size() != 1) { error("sub_mesh_elts", (*itdom)->meshDomain()->name(), m.name_); }
      if (shty != _noShape && shty != *(sst.begin())) { error("sub_mesh_shap", m.name_); }
      else { shty = *(sst.begin()); }
    }
  }
  if (shty == _noShape) { error("sub_mesh_nodom", m.name_); }

  // Fetch boundaries
  // Note: interface domains, if any, cannot be detected and thus are treated as boundaries, which would
  //       lead to an incorrect domain definition (in function build3Delements) since each interface
  //       element is shared by two elements of the main domain.
  std::vector<std::vector<number_t> > bounds;
  for (std::vector<GeomDomain*>::const_iterator itdom=m.domains_.begin(); itdom!=m.domains_.end(); itdom++)
  {
    if ((*itdom)->meshDomain()->isSideDomain())
    {
      std::set<number_t> sn((*itdom)->meshDomain()->nodeNumbers());
      bounds.push_back(std::vector<number_t>(sn.begin(), sn.end()));
    }
  }
  // Fetch elements
  short indHex[] = {3,7,2,6, 0,4,1,5}; // Vertices 4,8,3,7, 1,5,2,6 in XLiFE++ (cf. Hexahedron.cpp)
  short indId[] = {0,1,2,3}; // Identity. OK for triangle, quadrangle and tetrahedron.
  short* xl2sub = (shty==_hexahedron) ? indHex: indId;
  std::vector<std::vector<number_t> > elems;
  elems.reserve(elements_.size());
  for (std::vector<GeomElement*>::const_iterator itel=m.elements_.begin(); itel!=m.elements_.end(); itel++)
  {
    std::vector<number_t> Vxl((*itel)->vertexNumbers()), Vsub(Vxl);
    // Reordering of the vertices to match subdivision vertex numbering convention
    short k=0;
    for (std::vector<number_t>::iterator itVsub=Vsub.begin(); itVsub!=Vsub.end(); itVsub++)
    {
      *itVsub = Vxl[xl2sub[k++]];
    }
    elems.push_back(Vsub);
  }
  subdvMesh(m.nodes, elems, bounds, shty, refinementDepth, order);
  // Retrieve original domain names. The order in which the domains are stored has not been changed.
  /* No more true: The order in which the domains are stored may have changed.
     Temporarily disabled. Everything is OK if the original domains have the same name as the new ones.
     Otherwise, the user must check for the new names (with the printInfo() function) and uses them.
     Fixing of this issue in progress.
    for (vector<GeomDomain*>::const_iterator itdom=m.domains_.begin(), itndom=domains_.begin(); itdom!=m.domains_.end(); itdom++, itndom++)
    {
     (*itndom)->rename((*itdom)->name());
     (*itndom)->setDescription((*itdom)->description());
    }*/
}

void Mesh::copy(const Mesh& m)
{
  nodes = m.nodes;
  copyAllButNodes(m);
}

void Mesh::copyAllButNodes(const Mesh& m)
{
  name_ = m.name_;
  comment_ = m.comment_ + " (copy of " + name_ + ")";
  // if (geometry_p!=nullptr) delete geometry_p;
  // geometry_p=0;
  // if (m.geometry_p != nullptr) { geometry_p = m.geometry_p->clone(); }
  geometry_p = m.geometry_p->clone();
  meshData = m.meshData;
  vertices_ = m.vertices_;
  order_ = m.order_;
  isMadeOfSimplices_ = m.isMadeOfSimplices_;
  lastIndex_ = m.lastIndex_;
  std::map<const GeomElement*,GeomElement*> oldnewElts; //map relating old GeomElement* to new GeomElement* (copy of old)

  // full copy of elements (pointers have to be updated)
  elements_.resize(m.elements_.size());
  std::vector<GeomElement*>::const_iterator ite;
  std::vector<GeomElement*>::iterator it = elements_.begin();
  number_t k = 0;
  for (ite = m.elements_.begin(); ite != m.elements_.end(); ite++, it++, k++)
  {
    const GeomElement& elt = **ite;
    GeomElement* nelt = new GeomElement(elt);
    nelt->meshP() = this;
    if (nelt->hasMeshElement())    // update MeshElement attributes (nodes, ...)
    {
      std::vector<Point*>::iterator itp;
      std::vector<number_t>::iterator itn = nelt->meshElement()->nodeNumbers.begin();
      for (itp = nelt->meshElement()->nodes.begin(); itp != nelt->meshElement()->nodes.end(); itp++, itn++)
        *itp = &nodes[*itn-1];
      nelt->meshElement()->geomMapData_p = nullptr;
    }
    else { error("geoelt_not_meshelement"); }
    *it = nelt; //store new element
    oldnewElts[&elt]=nelt;
  }

  // full copy of mesh domains
  dimen_t minDim=3, maxDim=0, d;
  for (number_t i = 0; i < m.domains_.size(); i++)
  {
      d=m.domains_[i]->dim();
      minDim=std::min(d,minDim);
      maxDim=std::max(d,maxDim);
  }
  domains_.resize(m.domains_.size());
  for(int d=maxDim; d>=minDim; d--) // loop on domains by decreasing dim
  {
    for (number_t i = 0; i < m.domains_.size(); i++) // loop on domains of dim d
    {
     GeomDomain& dom = *m.domains_[i];
     if(dom.dim()==d)
     {
       switch (dom.domType())
       {
         case _meshDomain:
         {
           GeomDomain* ndom = new GeomDomain(*this, dom.name(), dom.dim(), dom.description());
           //copy elements
           std::vector<GeomElement*>& elts = dom.meshDomain()->geomElements;
           std::vector<GeomElement*>& nelts = ndom->meshDomain()->geomElements;
           nelts.resize(elts.size());
           std::vector<GeomElement*>::const_iterator ite;
           std::vector<GeomElement*>::iterator itne = nelts.begin();
           for (ite = elts.begin(); ite != elts.end(); ite++, itne++)
           {
            if ((*ite)->isSideElement())
            {
              GeomElement* nelt = new GeomElement(**ite); //new side element
              nelt->meshP()=this;                         //update parent mesh
              std::vector<GeoNumPair>::iterator itg;
              for (itg = nelt->parentSides().begin(); itg != nelt->parentSides().end(); itg++)
                //itg->first = elements_[itg->first->number()-1]; //update parent pointer
                itg->first = oldnewElts[itg->first]; //update parent pointer
              *itne = nelt;
              oldnewElts[*ite]=nelt; // store old->newGeomElement
            }
            //else *itne = elements_[(*ite)->number()-1]; //copy of a mesh element
            else *itne =oldnewElts[*ite]; // mesh element
           }
           domains_[i] = ndom;
           break;
         }
         case _compositeDomain:
         case _analyticDomain:
         default:  error("domain_notmesh",dom.name(), words("domain type",dom.domType()));
       }
     }
    }
  }
  //  optional vectors are not updated, reset to void
  sides_.clear();
  sideOfSides_.clear();
  vertexElements_.clear();
  firstOrderMesh_p = nullptr;
  if (order_ == 1) firstOrderMesh_p = this;
  setShapeTypes();
}

//! clear all pointers that have been copied at creation or copy
void Mesh::clear()
{
  for (std::vector<GeomElement*>::iterator it = elements_.begin(); it != elements_.end(); it++)
    if (*it != nullptr) delete *it;
  for (std::vector<GeomElement*>::iterator it = sides_.begin(); it != sides_.end(); it++)
    if (*it != nullptr) delete *it;
  for (std::vector<GeomElement*>::iterator it = sideOfSides_.begin(); it != sideOfSides_.end(); it++)
    if (*it != nullptr) delete *it;
  if (firstOrderMesh_p != nullptr && firstOrderMesh_p != this)
  {
     delete firstOrderMesh_p;
     geometry_p=nullptr;      // deallocated by firstOrderMesh_p deletion
  }
  for (std::vector<GeomDomain*>::iterator itd = domains_.begin(); itd != domains_.end(); itd++)
    if (*itd != nullptr && GeomDomain::findDomain(*itd)!=nullptr) delete *itd; // before deleting, check if not already deleted
  if (geometry_p != nullptr) delete geometry_p;
  geometry_p=nullptr;
}

//! assign operator
Mesh& Mesh::operator=(const Mesh& m)
{
  if (&m != this)
  {
    clear();
    copy(m);
  }
  return *this;
}

//!< insert domain to mesh domain list (if not listed)
void Mesh::insertDomain(GeomDomain& dom) const
{
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]==&dom) return;  //already in list
  }
  domains_.insert(domains_.begin(), &dom);
}

//!< add domain to mesh domain list (if not listed)
void Mesh::addDomain(GeomDomain& dom) const
{
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]==&dom) return;  //already in list
  }
  domains_.push_back(&dom);
}

//! access to a domain by its name
const GeomDomain& Mesh::domain(const string_t& na) const
{
  string_t sna = trim(na);
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return *domains_[i]; }
  }
  if (sna == "Omega") { sna = "#Omega"; }
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return *domains_[i]; }
  }
  error("mesh_failfinddomain", na);
  return *domains_[0];   //fake return
}

//! access to a domain by its name
GeomDomain& Mesh::domain(const string_t& na)
{
  string_t sna = trim(na);
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return *domains_[i]; }
  }
  if (sna == "Omega") { sna = "#Omega"; }
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return *domains_[i]; }
  }
  error("mesh_failfinddomain", na);
  return *domains_[0];   //fake return
}

//! access to a domain pointer by its name, return 0 if not found (const)
const GeomDomain* Mesh::domainP(const string_t& na) const
{
  string_t sna = trim(na);
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return domains_[i]; }
  }
  if (sna == "Omega") { sna = "#Omega"; }
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return domains_[i]; }
  }
  return nullptr;   //not found
}

//! access to a domain pointer by its name, return 0 if not found
GeomDomain* Mesh::domainP(const string_t& na)
{
  string_t sna = trim(na);
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return domains_[i]; }
  }
  if (sna == "Omega") { sna = "#Omega"; }
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return domains_[i]; }
  }
  return nullptr;   //not found
}

//! access to a domain by its number
const GeomDomain& Mesh::domain(number_t n) const
{
  if (n >= domains_.size()) { error("mesh_finddomainout", n, domains_.size()); }
  return *domains_[n];
}

//! access to a domain by its number
GeomDomain& Mesh::domain(number_t n)
{
  if (n >= domains_.size()) { error("mesh_finddomainout", n, domains_.size()); }
  return *domains_[n];
}

//! access to a domain by its name
number_t Mesh::domainNumber(const string_t& na) const
{
  string_t sna = trim(na);
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return i; }
  }
  error("mesh_failfinddomain", na);
  return 0;   //fake return
}

//! access to a domain by its name
number_t Mesh::domainNumber(const string_t& na)
{
  string_t sna = trim(na);
  for (number_t i = 0; i < domains_.size(); i++)
  {
    if (domains_[i]->name() == sna) { return i; }
  }
  error("mesh_failfinddomain", na);
  return 0;   //fake return
}

//! access to a domain by its number
string_t Mesh::domainName(number_t n) const
{
  if (n >= domains_.size()) { error("mesh_finddomainout", n, domains_.size()); }
  return domains_[n]->name();
}

//! access to a domain by its number
string_t Mesh::domainName(number_t n)
{
  if (n >= domains_.size()) { error("mesh_finddomainout", n, domains_.size()); }
  return domains_[n]->name();
}

//! try to find a MeshGenerator for a geometry and an element shape
MeshGenerator Mesh::defaultMeshGenerator(const Geometry& g, ShapeType sh)
{
  switch (g.dim())
  {
    case 1:
      switch (g.shape())
      {
        case _segment:
        case _setofpoints:
          return _structured;
        default:
          return _gmsh;
      }
    case 2:
      switch (g.shape())
      {
        case _parallelogram:
        case _rectangle:
        case _square:
          return _structured;
        case _disk:
        case _setofelems:
          return _subdiv;
        default:
          return _gmsh;
      }
    case 3:
      switch (g.shape())
      {
        case _parallelepiped:
        case _cuboid:
        case _cube:
          if (sh == _hexahedron) { return _structured; }
          else                   { return _subdiv; }
        case _ball:
        case _revTrunk:
        case _revCylinder:
        case _revCone:
          return _subdiv;
        default:
          return _gmsh;
      }
  }
  return _gmsh;
}

/*---------------------------------------------------------------------------------------------
  create test mesh with only one reference element (order one)
  ---------------------------------------------------------------------------------------------*/
Mesh::Mesh(ShapeType shape)
  : name_("one reference "+words("shape",shape)+" element mesh")
{
  number_t nbpt=0;
  dimen_t dim=0;
  BoundingBox bb;

  switch (shape)
  {
    case _segment:
    {
      dim=1;
      nbpt=2;
      bb=BoundingBox(0,1);
      nodes.resize(nbpt);
      nodes[0] = Point(1.);
      nodes[1] = Point(0.);
      geometry_p = new Segment(_v1=nodes[0],_v2=nodes[1]);
      break;
    }
    case _triangle:
    {
      dim=2;
      nbpt=3;
      bb=BoundingBox(0,1,0,1);
      nodes.resize(nbpt);
      nodes[0] = Point(1.,0.);
      nodes[1] = Point(0.,1.);
      nodes[2] = Point(0.,0.);
      geometry_p = new Triangle(_v1=nodes[0],_v2=nodes[1],_v3=nodes[2]);
      break;
    }
    case _quadrangle:
    {
      dim=2;
      nbpt=4;
      bb=BoundingBox(0,1,0,1);
      nodes.resize(nbpt);
      nodes[0] = Point(1.,0.);
      nodes[1] = Point(1.,1.);
      nodes[2] = Point(0.,1.);
      nodes[3] = Point(0.,0.);
      geometry_p = new Rectangle(_v1=nodes[3],_v2=nodes[0],_v4=nodes[2]);
      break;
    }
    case _tetrahedron:
    {
      dim=3;
      nbpt=4;
      bb=BoundingBox(0,1,0,1,0,1);
      nodes.resize(nbpt);
      nodes[0] = Point(1.,0.,0.);
      nodes[1] = Point(0.,1.,0.);
      nodes[2] = Point(0.,0.,1.);
      nodes[3] = Point(0.,0.,0.);
      geometry_p = new Tetrahedron(_v1=nodes[0],_v2=nodes[1],_v3=nodes[2], _v4=nodes[3]);
      break;
    }
    case _hexahedron:
    {
      dim=3;
      nbpt=8;
      bb=BoundingBox(0,1,0,1,0,1);
      nodes.resize(nbpt);
      nodes[0] = Point(1.,0.,0.);
      nodes[1] = Point(1.,1.,0.);
      nodes[2] = Point(0.,1.,0.);
      nodes[3] = Point(0.,0.,0.);
      nodes[4] = Point(1.,0.,1.);
      nodes[5] = Point(1.,1.,1.);
      nodes[6] = Point(0.,1.,1.);
      nodes[7] = Point(0.,0.,1.);
      geometry_p = new Cube(_v1=nodes[3],_v2=nodes[0],_v4=nodes[2],_v5=nodes[7]);
      break;
    }
    case _prism:
    {
      dim=3;
      nbpt=4;
      bb=BoundingBox(0,1,0,1,0,1);
      nodes.resize(nbpt);
      nodes[0] = Point(1.,0.,0.);
      nodes[1] = Point(0.,1.,0.);
      nodes[2] = Point(0.,0.,0.);
      nodes[3] = Point(0.,0.,1.);
      geometry_p = new Prism(_v1=nodes[0],_v2=nodes[1],_v3=nodes[2],_direction=nodes[3]);
      break;
    }
    case _pyramid:
    {
      dim=3;
      nbpt=5;
      bb=BoundingBox(0,1,0,1,0,1);
      nodes.resize(nbpt);
      nodes[0] = Point(0.,0.,0.);
      nodes[1] = Point(1.,0.,0.);
      nodes[2] = Point(1.,1.,0.);
      nodes[3] = Point(0.,1.,0.);
      nodes[4] = Point(0.,0.,1.);
      geometry_p = new Pyramid(_v1=nodes[4],_v2=nodes[0],_v3=nodes[1],_v4=nodes[2],_apex=nodes[3]);
      break;
    }
    default:
    {
      where("Mesh::Mesh(ShapeType shape)");
      error("shape_not_handled",words("shape",shape));
    }
  }

  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  RefElement* ref_p = findRefElement(shape, interp_p);
  elements_.resize(1);
  elements_[0] = new GeomElement(this, ref_p, dim, 1);
  MeshElement* melt = elements_[0]->meshElement();
  for (number_t i=0; i<nbpt; i++)  melt->nodeNumbers[i]=i+1;
  melt->vertexNumbers = melt->nodeNumbers;
  melt->setNodes(nodes);
  vertices_.resize(nodes.size());
  for (number_t i = 0; i < nodes.size(); i++) { vertices_[i] = i + 1; }
  MeshDomain* meshdom_p = (new GeomDomain(*this, "Omega", 3, "ref. "+words("shape",shape)))->meshDomain();
  meshdom_p->geomElements = elements_;
  meshdom_p->setShapeTypes();
  domains_.push_back(meshdom_p);
  lastIndex_ = 2;
  buildGeomData();
  setShapeTypes();
  firstOrderMesh_p=this;
}

//! construction method from 1D geometry
void Mesh::build1DMesh(const Geometry& g, number_t order, MeshGenerator mg, MeshPattern pattern, StructuredMeshSplitRule splitDirection)
{
  trace_p->push("Mesh::build1DMesh(const Geometry&[, Number[, MeshGenerator[, MeshPattern[, StructuredMeshSplitRule]]]]);");
  if (g.dim() != 1) { error("geometry_only_1D"); }

  lastIndex_=0;
  firstOrderMesh_p=nullptr;
  geometry_p=g.clone();

  if (mg == _defaultGenerator) { mg = defaultMeshGenerator(g); }
  if (pattern != _unstructured && pattern != _structured) { pattern = _defaultPattern; }

  switch (mg)
  {
    case _gmsh:
    {
      string_t filename = "xlifepp_script.geo";
      saveToGeo(*geometry_p, meshData, order, filename);
      loadGeo(filename, geometry_p->dimPoint());
      break;
    }
    case _gmshOC: // use OpenCascade representation
    {
      #ifdef XLIFEPP_WITH_OPENCASCADE
        string_t filebrep="xlifepp_script.brep";
        string_t filename="xlifepp_script.geo";
        saveToBrepGeo(*geometry_p, meshData, _segment, order, pattern, splitDirection, filename, filebrep);
        loadGeo(filename, geometry_p->dimPoint(), 0);
      #else
        error("OC_not_available");
      #endif // XLIFEPP_WITH_OPENCASCADE
      break;
    }
    case _fromParametrization:
    {
      meshFromParametrization(g, _segment, order, pattern, splitDirection);
      break;
    }
    case _structured:
    {
      if (order != 1) { error("bad_order", order, 1); }
      switch (geometry_p->shape())
      {
        case _segment:
        {
          geometry_p->checkSideNamesAndUpdate(2);
          Segment& geom(*geometry_p->segment());
          number_t nx=geom.n()[0];
          if (geom.h().size()==2) { nx=geom.measure()/geom.h1(); }
          meshP1Segment(geom, nx, geom.sideNames());
          break;
        }
        case _setofpoints:
        {
          geometry_p->checkSideNamesAndUpdate(2);
          SetOfPoints& geom(*geometry_p->setofpoints());
          meshP1Line(geom);
          break;
        }
        default:
        {
          error("mesh_not_handled", geometry_p->domName(), words("shape",g.shape()));
          break;
        }
      }
      break;
    }
    default:
      error("generator_not_handled", words("mesh generator", mg));
      break;
  }
  trace_p->pop();
}

//! constructor from 2D and 3D geometries
void Mesh::buildMesh(const Geometry& g, ShapeType sh, number_t order, MeshGenerator mg, MeshPattern pattern, StructuredMeshSplitRule splitDirection)
{
  trace_p->push("Mesh::Mesh(const Geometry&, ShapeType[, Number[, MeshGenerator[, String]]]);");
  if (g.dim() < 2)
  {
    if (sh > _segment) warning("unused_shape_for_1D");
    if (g.isClosed() && sh > _segment) warning("use_surface_from_for_1D",words("shape",sh));
    build1DMesh(g, order, mg, pattern, splitDirection);
  }
  else
  {
    std::vector<string_t> sideNames;
    lastIndex_=0;
    firstOrderMesh_p=nullptr;
    geometry_p=g.clone();

    if (mg == _defaultGenerator) { mg = defaultMeshGenerator(g,sh); }
    if (pattern != _unstructured && pattern != _structured) { pattern = _defaultPattern; }

    switch (mg)
    {
      case _gmsh:
      {
        string_t filename="xlifepp_script.geo";
        std::set<CrackData> cracks;
        saveToGeo(*geometry_p, meshData, sh, order, pattern, splitDirection, filename, cracks);
        if (cracks.size() > 0) { loadGeo(filename, nodesDim(*geometry_p), &cracks); }
        else { loadGeo(filename, nodesDim(*geometry_p), 0); }
        break;
      }
      case _gmshOC: // use OpenCascade representation
      {
        #ifdef XLIFEPP_WITH_OPENCASCADE
          string_t filebrep="xlifepp_script.brep";
          string_t filename="xlifepp_script.geo";
          saveToBrepGeo(*geometry_p, meshData, sh, order, pattern, splitDirection, filename, filebrep);
          loadGeo(filename, nodesDim(*geometry_p), 0);
        #else
          error("OC_not_available");
        #endif // XLIFEPP_WITH_OPENCASCADE
        break;
      }
      case _fromParametrization:
      {
        meshFromParametrization(g, sh, order, pattern, splitDirection);
        break;
      }
      case _structured:
      {
        if (order != 1) { error("bad_order",order,1); }
        switch (geometry_p->shape())
        {
          case _parallelogram:
          {
            geometry_p->checkSideNamesAndUpdate(4);
            Parallelogram& para = *geometry_p->parallelogram();
            number_t n1 = para.n1(), n2=para.n2();
            if (para.h().size()==4) {n1=number_t(para.length1()/para.h(1)); n2=number_t(para.length2()/para.h(1));}
            switch (sh)
            {
              case _triangle: meshP1Parallelogram(para, n1, n2, para.sideNames(), para.sideOfSideNames(), splitDirection); break;
              case _quadrangle: meshQ1Parallelogram(para, n1, n2, para.sideNames(), para.sideOfSideNames()); break;
              case _segment : meshBoundaryStructured(g, sh); break;
              default:
                error("mesh_not_handled", geometry_p->domName(), words("shape",sh));
                break;
            }
            break;
          }
          case _rectangle:
          {
            geometry_p->checkSideNamesAndUpdate(4);
            Rectangle& rect = *geometry_p->rectangle();
            number_t nx = rect.nx(), ny=rect.ny();
            if (rect.h().size()==4) {nx=number_t(rect.xlength()/rect.h(1)); ny=number_t(rect.ylength()/rect.h(2));}
            switch (sh)
            {
              case _triangle: meshP1Parallelogram(rect, nx, ny, rect.sideNames(), rect.sideOfSideNames(), splitDirection); break;
              case _quadrangle: meshQ1Parallelogram(rect, nx, ny, rect.sideNames(), rect.sideOfSideNames()); break;
              case _segment : meshBoundaryStructured(g, sh); break;
              default:
                error("mesh_not_handled", geometry_p->domName(), words("shape",sh));
                break;
            }
            break;
          }
          case _square:
          {
            geometry_p->checkSideNamesAndUpdate(4);
            SquareGeo& squa = *geometry_p->square();
            number_t nx = squa.nx(), ny=squa.ny();
            if (squa.h().size()==4) {nx=number_t(squa.xlength()/squa.h(1)); ny=number_t(squa.xlength()/squa.h(2));}
            switch (sh)
            {
              case _triangle: meshP1Parallelogram(squa, nx, ny, squa.sideNames(), squa.sideOfSideNames(), splitDirection); break;
              case _quadrangle: meshQ1Parallelogram(squa,nx,ny,squa.sideNames(), squa.sideOfSideNames()); break;
              case _segment : meshBoundaryStructured(g, sh); break;
              default:
                error("mesh_not_handled", geometry_p->domName(), words("shape",sh));
                break;
            }
            break;
          }
          case _parallelepiped:
          {
            geometry_p->checkSideNamesAndUpdate(6);
            Parallelepiped& para = *geometry_p->parallelepiped();
            switch (sh)
            {
              case _tetrahedron:
                meshP1Parallelepiped(para, para.n1(), para.n2(), para.n3(), para.sideNames(), para.sideOfSideNames(), para.sideOfSideOfSideNames());
                break;
              case _hexahedron:
                meshQ1Parallelepiped(para, para.n1(), para.n2(), para.n3(), para.sideNames(), para.sideOfSideNames(), para.sideOfSideOfSideNames());
                break;
              case _prism:
                meshPr1Parallelepiped(para, para.n1(), para.n2(), para.n3(), para.sideNames(), para.sideOfSideNames(), para.sideOfSideOfSideNames(), splitDirection);
                break;
              case _pyramid:
                meshPy1Parallelepiped(para, para.n1(), para.n2(), para.n3(), para.sideNames(), para.sideOfSideNames(), para.sideOfSideOfSideNames());
                break;
              case _triangle: meshBoundaryStructured(g, sh, splitDirection); break;
              case _quadrangle: meshBoundaryStructured(g, sh); break;
              default:
                error("mesh_not_handled", geometry_p->domName(), words("shape",sh));
                break;
            }
            break;
          }
          case _cuboid:
          {
            geometry_p->checkSideNamesAndUpdate(6);
            Cuboid& cub = *geometry_p->cuboid();
            switch (sh)
            {
              case _tetrahedron:
                meshP1Parallelepiped(cub, cub.nx(), cub.ny(), cub.nz(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames());
                break;
              case _hexahedron:
                meshQ1Parallelepiped(cub, cub.nx(), cub.ny(), cub.nz(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames());
                break;
              case _prism:
                meshPr1Parallelepiped(cub, cub.n1(), cub.n2(), cub.n3(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames(), splitDirection);
                break;
              case _pyramid:
                meshPy1Parallelepiped(cub, cub.n1(), cub.n2(), cub.n3(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames());
                break;
              case _triangle: meshBoundaryStructured(g, sh, splitDirection); break;
              case _quadrangle: meshBoundaryStructured(g, sh); break;
              default:
                error("mesh_not_handled", geometry_p->domName(), words("shape",sh));
                break;
            }
            break;
          }
          case _cube:
          {
            geometry_p->checkSideNamesAndUpdate(6);
            Cube& cub = *geometry_p->cube();
            switch (sh)
            {
              case _tetrahedron:
                meshP1Parallelepiped(cub, cub.nx(), cub.ny(), cub.nz(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames());
                break;
              case _hexahedron:
                meshQ1Parallelepiped(cub, cub.nx(), cub.ny(), cub.nz(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames());
                break;
              case _prism:
                meshPr1Parallelepiped(cub, cub.n1(), cub.n2(), cub.n3(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames(), splitDirection);
                break;
              case _pyramid:
                meshPy1Parallelepiped(cub, cub.n1(), cub.n2(), cub.n3(), cub.sideNames(), cub.sideOfSideNames(), cub.sideOfSideOfSideNames());
                break;
              case _triangle: meshBoundaryStructured(g, sh, splitDirection); break;
              case _quadrangle: meshBoundaryStructured(g, sh); break;
              default:
                error("mesh_not_handled", geometry_p->domName(), words("shape", sh));
                break;
            }
            break;
          }
          default:
            error("mesh_not_handled", geometry_p->domName(), words("shape", sh));
            break;
        }
        break;
      }
      case _subdiv:
      {
        switch (g.shape())
        {
          case _ball:
          {
            Ball& geom(*geometry_p->ball());
            subdvMesh(geom, sh, geom.nbOctants(), geom.nbSubdiv(), order, geom.type(), geom.teXFilename());
            break;
          }
          case _cube:
          {
            Cube& geom(*geometry_p->cube());
            subdvMesh(geom, sh, geom.nbOctants(), geom.nbSubdiv(), order, geom.teXFilename());
            break;
          }
          case _disk:
          {
            Disk& geom(*geometry_p->disk());
            subdvMesh(geom, sh, geom.nbSubdiv(), order, geom.type(), geom.teXFilename());
            break;
          }
          case _revCone:
          {
            RevCone& geom(*geometry_p->revCone());
            subdvMesh(geom, sh, geom.nbSubdomains(), geom.nbSubdiv(), order, geom.type(), geom.teXFilename());
            break;
          }
          case _revCylinder:
          {
            RevCylinder& geom(*geometry_p->revCylinder());
            subdvMesh(geom, sh, geom.nbSubdomains(), geom.nbSubdiv(), order, geom.type(), geom.teXFilename());
            break;
          }
          case _revTrunk:
          {
            RevTrunk& geom(*geometry_p->revTrunk());
            subdvMesh(geom, sh, geom.nbSubdomains(), geom.nbSubdiv(), order, geom.type(), geom.teXFilename());
            break;
          }
          case _setofelems:
          {
            SetOfElems& geom(*geometry_p->setofelems());
            subdvMesh(geom.pts(), geom.elems(), geom.bounds(), geom.elemShape(), geom.nbSubdiv(), order, geom.teXFilename());
            break;
          }
          default:
            error("mesh_not_handled", geometry_p->domName(), words("shape", sh));
            break;
        }
        break;
      }
      default:
      {
        error("generator_not_handled", words("mesh generator", mg));
        break;
      }
    } // switch (mg)
  } // else

  //build additionnal structures
  // because in some cases the name of main domain may be "#Omega" whereas the mesh geometry has an other name
  // renaming "#Omega" in geometry name is enforced
  if (geometry_p!=nullptr && domainP("#Omega")!=nullptr && geometry_p->domName()!="") renameDomain("#Omega",geometry_p->domName());
  //update geometry pointers of domains and build implicit parametrization of composite geometries
  updateGeometryPointers();
  trace_p->pop();
}

// try to update geometry pointers of domains of the mesh and create paramerization if not exist
void Mesh::updateGeometryPointers()
{
  trace_p->push("Mesh::updateGeometryPointers");
  std::vector<GeomDomain*>::iterator itd=domains_.begin();
  for (;itd!=domains_.end(); ++itd)  // loop on mesh domains
  {
    Geometry* gn=(*itd)->geometry();
    if (gn==nullptr) //find or build a new geometry
    {
      string_t nd=(*itd)->name();
      Geometry* gn = geometry_p->find(nd); // return geometry if exists or create a new one (may return 0!)
      (*itd)->setGeometry(gn);
    }
    // if (gn!=nullptr && !gn->isCanonical() && gn->parametrizationP()==nullptr) gn->buildParametrization(); //if no parametrization for a non canonical geometry, build one
  }
  trace_p->pop();
}

/* -------------------------------------------------------------------------------------------
   create a new mesh from a given one by spliting elements to other ones
   can only split mesh having same elements
   only few conversions are enable for the moment
       hexahedron mesh -> pyramid mesh
  --------------------------------------------------------------------------------------------*/
void Mesh::buildSplittedMesh(const Mesh& m, ShapeType sh, const string_t name)
{
  trace_p->push("Mesh::buildSplittedMesh(Mesh, ShapeType, String)");
  this->name_=name;
  std::set<ShapeType> shapeTypes;   //!< list of element shape types in mesh domain (counted once)
  std::vector<GeomElement*>::const_iterator itg;
  for (itg = m.elements_.begin(); itg != m.elements_.end(); itg++) shapeTypes.insert((*itg)->shapeType());
  if (shapeTypes.size()!=1) error("mesh_multiple_shapes");
  ShapeType sh0=*shapeTypes.begin();
  switch (sh0)
  {
    case hexahedron:
      switch (sh)
      {
        case _pyramid: buildPyramidFromHexahedron(m);
          break;
        default: error("mesh_bad_shape_conversion", words("shape",_hexahedron), words("shape",sh));
      }
      break;
    default: error("mesh_bad_shape_conversion", words("shape",sh0), words("shape",sh));
  }
  trace_p->pop();
}

/*!
   -------------------------------------------------------------------------------------------
   Utility function used to complete a 1D mesh, after the nodes have been defined.
  --------------------------------------------------------------------------------------------
*/
void Mesh::complete1Dmesh(const string_t& domname, const std::vector<string_t>& sideNames)
{
  isMadeOfSimplices_ = true;
  order_ = 1;
  firstOrderMesh_p = this;

  // construct mesh elements
  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  RefElement* ref_p = findRefElement(_segment, interp_p);
  dimen_t dim=nodes[0].size();
  number_t nx = nodes.size();
  elements_.resize(nx-1);
  for (number_t k = 0; k < nx-1; k++)
  {
    elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
    MeshElement* melt = elements_[k]->meshElement();
    melt->nodeNumbers[0] = k + 1;
    melt->nodeNumbers[1] = k + 2;
    melt->vertexNumbers = melt->nodeNumbers;
    melt->setNodes(nodes);
  }

  // construct vertex indices (begins at 1)
  vertices_.resize(nodes.size());
  for (number_t i = 0; i < nodes.size(); i++) { vertices_[i] = i + 1; }

  // construct main domain
  string_t nmd=domname;
  if (nmd=="") nmd="#Omega";
  MeshDomain* meshdom_p = (new GeomDomain(*this, nmd, 1, "segment [a,b]"))->meshDomain();
  meshdom_p->geomElements = elements_;
  domains_.push_back(meshdom_p);

  // construct boundary domains
  std::vector<string_t> vertexdesc;
  vertexdesc.push_back("point a of segment [a,b]");
  vertexdesc.push_back("point b of segment [a,b]");

  number_t k = nx;
  if (sideNames[1] != "")    //create domain {b}
  {
    meshdom_p = (new GeomDomain(*this, sideNames[1], 0, vertexdesc[1]))->meshDomain();
    meshdom_p->geomElements.push_back(new GeomElement(elements_[k - 2], 1, k));
    k++;
    domains_.push_back(meshdom_p);
  }
  if (sideNames[0] != "")    //create domain {a}
  {
    meshdom_p = (new GeomDomain(*this, sideNames[0], 0, vertexdesc[0]))->meshDomain();
    meshdom_p->geomElements.push_back(new GeomElement(elements_[0], 2, k));
    domains_.push_back(meshdom_p);
  }
  lastIndex_ = k;

  //merge domains with same name
  mergeDomainsWithSameName();

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();
}

/*---------------------------------------------------------------------------------------------
  create regular mesh of segment [a,b]
  sideNames[0]-> a, sideNames[1]-> b
  if sideNames[i] is empty, the side is not a GeomDomain
  nx is the number of nodes wanted on the segment (nx > 2)
  ---------------------------------------------------------------------------------------------*/
void Mesh::meshP1Segment(Segment& seg, number_t nx, const std::vector<string_t>& sideNames)
{
  trace_p->push("Mesh::meshP1Segment");
  Point a = seg.p1();
  Point b = seg.p2();
  if (a == b) { error("void_elt", "segment"); }

  // construct nodes
  nodes.resize(nx);
  for (number_t i = 0; i < nx; i++) { nodes[i] = a + i * (b-a)/(nx-1); }

  complete1Dmesh(seg.domName(), sideNames);
  trace_p->pop();
}

/*---------------------------------------------------------------------------------------------
  Create a mesh of an opened polygonal line, made of consecutive segments whose end points are
  two consecutive points taken from the input vector pts.
  The dimension n of all the points should be the same and the mesh is created in Rn.
  sideNames[0] is the domain name corresponding to the first point,
  sideNames[1] is the domain name corresponding to the last point.
  If sideNames[i] is empty, the side domain is not created.
  ---------------------------------------------------------------------------------------------*/
void Mesh::meshP1Line(SetOfPoints& sp)
{
  trace_p->push("Mesh::meshP1Line");
  const std::vector<Point>& pts = sp.pts();
  nodes.assign(pts.begin(), pts.end());
  complete1Dmesh(sp.domName(),sp.sideNames());
  trace_p->pop();
}

/*---------------------------------------------------------------------------------------------
  create regular mesh of rectangle [a,b]x[c,d]
  sideNames[0]->[a,b]x{c} sideNames[1]->{b}x[c,d] sideNames[2]->[a,b]x{d} sideNames[3]->{a}x[c,d]
  if sideNames[i] is empty, the side is not a GeomDomain
  nx and ny are the number of nodes wanted on each edge of the rectangle (nx > 2, ny > 2)
  split elementary parallelogram in two triangles, in the following way
    splitDirection = _left   : along x+y=c diagonal line (_left) - default behaviour -
    splitDirection = _right  : along x-y=c diagonal line (_right)
    splitDirection = _alternate : alternate diagonal lines (_alternate)
    splitDirection = _random : randomly along both diagonal lines (_random)
  or
    splitDirection = _cross : split each elementary parallelogram in four triangles, adding central point
  ---------------------------------------------------------------------------------------------*/
void Mesh::meshP1Parallelogram(Parallelogram& para, number_t nx, number_t ny,
                               const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames, StructuredMeshSplitRule splitDirection)
{
  //initialisation
  trace_p->push("Mesh::meshP1Parallelogram");
  Point a = para.p(1), b = para.p(2), c = para.p(4);
  dimen_t dim=a.size();

  if (a == b || a == c) { error("degenerated_elt", "parallelogram"); }

  isMadeOfSimplices_ = true;
  order_ = 1;
  firstOrderMesh_p = this;

  //construct nodes
  number_t k = 0;
  Point dx=(b-a)/(nx-1), dy=(c-a)/(ny-1);
  if (splitDirection != _cross)
  {
    nodes.resize(nx*ny);
    for (number_t j = 0; j < ny; j++)
      for (number_t i = 0; i < nx; i++, k++)
        nodes[k] = a+i*dx+j*dy;
  }
  else  // cross splitting
  {
    nodes.resize(nx*ny+(nx-1)*(ny-1));
    dy*=0.5;
    Point x=a;
    number_t p=0;
    for (number_t j = 0; j < 2*ny-1; j++)
    {
      if(j%2==0) {p=0;x=a;}
      else       {p=1;x=a+dx*0.5;}
      for (number_t i = 0; i < nx-p; i++, k++)
         nodes[k] = x+i*dx+j*dy;
    }
  }

  std::vector<bool> splits(2*(nx-1)*(ny-1));  // if true elements split along x+y=c

  //construct mesh elements
  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  RefElement* ref_p = findRefElement(_triangle, interp_p);
  k = 0;
  if(splitDirection!=_cross)
  {
    elements_.resize(2 * (nx-1) * (ny-1));
    bool randsplit = (splitDirection == _random);  // has priority
    bool altSplit = !randsplit && (splitDirection == _alternate); // has second priority
    real_t s = 0, sj=0;
    if (splitDirection == _right) s=1;
    number_t e=0;
    for (number_t j = 0; j < ny-1 ; j++)
    {
      if (altSplit) {s=sj; sj=1-sj;}
      for (number_t i = 0; i < nx-1; i++)
      {
        if (randsplit) s=uniformDistribution();
        if (altSplit) s=1-s;
        elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
        MeshElement* melt = elements_[k]->meshElement();
        number_t n = j * nx + i + 1;
        if (s<0.5) // split x+y=c
        {
          melt->nodeNumbers[0] = n + 1;
          melt->nodeNumbers[1] = n + nx;
          melt->nodeNumbers[2] = n;
          splits[e]=true; e++;
        }
        else      // split x-y=c
        {
          melt->nodeNumbers[0] = n + 1 + nx;
          melt->nodeNumbers[1] = n ;
          melt->nodeNumbers[2] = n + 1;
          splits[e]=false; e++;
        }
        melt->vertexNumbers = melt->nodeNumbers;   //update vertex numbers, same as node
        melt->setNodes(nodes);                     //update node pointers
        k++;
        elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
        melt = elements_[k]->meshElement();
        if (s<0.5) // split x+y=c
        {
          melt->nodeNumbers[0] = n + nx;
          melt->nodeNumbers[1] = n + 1;
          melt->nodeNumbers[2] = n + nx + 1;
          splits[e]=true; e++;
        }
        else     // split x-y=c
        {
          melt->nodeNumbers[0] = n ;
          melt->nodeNumbers[1] = n + 1 + nx ;
          melt->nodeNumbers[2] = n + nx;
          splits[e]=false; e++;
        }
        melt->vertexNumbers = melt->nodeNumbers;  //update vertex numbers
        melt->setNodes(nodes);                    //update node pointers
        k++;
      }
    }
  }
  else // cross splitting
  {
    elements_.resize(4 * (nx-1) * (ny-1));
    MeshElement* melt=nullptr;
    number_t e=0;
    for (number_t j = 0; j < ny-1 ; j++)
    {
      for (number_t i = 0; i < nx-1; i++)
      {
        number_t n = j * (2 * nx -1) + i + 1;
        elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
        melt = elements_[k]->meshElement();
        melt->nodeNumbers = {n+1, n+nx, n};
        melt->vertexNumbers = melt->nodeNumbers;   //update vertex numbers, same as node
        melt->setNodes(nodes);                     //update node pointers
        k++;
        elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
        melt = elements_[k]->meshElement();
        melt->nodeNumbers = {n , n+nx, n+2*nx-1};
        melt->vertexNumbers = melt->nodeNumbers;   //update vertex numbers, same as node
        melt->setNodes(nodes);                     //update node pointers
        k++;
        elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
        melt = elements_[k]->meshElement();
        melt->nodeNumbers = {n+2*nx , n+nx, n+1};
        melt->vertexNumbers = melt->nodeNumbers;   //update vertex numbers, same as node
        melt->setNodes(nodes);                     //update node pointers
        k++;
        elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
        melt = elements_[k]->meshElement();
        melt->nodeNumbers = {n+2*nx-1 , n+nx, n+2*nx};
        melt->vertexNumbers = melt->nodeNumbers;   //update vertex numbers, same as node
        melt->setNodes(nodes);                     //update node pointers
        k++;
      }
    }
  }

  //construct vertex indices (begins at 1)
  vertices_.resize(nodes.size());
  for (number_t i = 0; i < nodes.size(); i++) { vertices_[i] = i + 1; }

  //construct main domain
  string_t nmd=para.domName();
  if (nmd=="") nmd="#Omega";
  MeshDomain* meshdom_p = (new GeomDomain(*this, nmd, 2, "rectangle [a,b]x[c,d]"))->meshDomain();
  meshdom_p->geomElements = elements_;
  domains_.push_back(meshdom_p);

  //construct boundary domains
  std::vector<string_t> edgedesc, vertexdesc;
  edgedesc.push_back("boundary side [a,b]x{c} of rectangle [a,b]x[c,d]");
  edgedesc.push_back("boundary side {b}x[c,d] of rectangle [a,b]x[c,d]");
  edgedesc.push_back("boundary side [a,b]x{d} of rectangle [a,b]x[c,d]");
  edgedesc.push_back("boundary side {a}x[c,d] of rectangle [a,b]x[c,d]");
  vertexdesc.push_back("vertex (a,c) of rectangle [a,b]x[c,d]");
  vertexdesc.push_back("vertex (a,d) of rectangle [a,b]x[c,d]");
  vertexdesc.push_back("vertex (b,d) of rectangle [a,b]x[c,d]");
  vertexdesc.push_back("vertex (b,c) of rectangle [a,b]x[c,d]");
  

  for (number_t d = 0; d < sideNames.size(); ++d)
  {
    if (sideNames[d] != "")   //create a domain
    {
      meshdom_p = (new GeomDomain(*this, sideNames[d], 1, edgedesc[d]))->meshDomain();
      if(splitDirection!=_cross)
      {
        number_t nbelt = nx-1, numFace = 3;
        if (d == 1 || d == 3) {nbelt = ny-1; numFace = 2;}
        meshdom_p->geomElements.resize(nbelt);
        for (number_t e = 0; e < nbelt; e++)
        {
          number_t ne = 0;
          switch (d)
          {
            case 0:
              ne = 2 * e;
              if (splits[ne]) numFace=3; else numFace=2;
              break;
            case 1:
              ne = 2 * (e + 1) * (nx - 1) - 1;
              if (splits[ne]) numFace=2; else {numFace=3;ne--;}
              break;
            case 2:
              ne = 2 * (nx - 1) * (ny - 2) + 2 * e + 1;
              if (splits[ne]) numFace=3; else numFace=2;
              break;
            case 3:
              ne = 2 * e * (nx - 1);
              if (splits[ne]) numFace=2; else {numFace=3;ne++;}
              break;
          }
          meshdom_p->geomElements[e] = new GeomElement(elements_[ne], numFace, k + 1);
          k++;
        }
      }
      else  // cross splitting
      {
        number_t nbelt = nx-1, ne, numFace = 3;
        if (d == 1 || d == 3) {nbelt = ny-1;}
        meshdom_p->geomElements.resize(nbelt);
        for (number_t e = 0; e < nbelt; e++)
        {
          switch (d)
          {
            case 0: ne=4*e; break;
            case 1: ne=4*(e+1)*(nx-1)-2;; break;
            case 2: ne=4*(nx-1)*(ny-2)+4*e+3; break;
            case 3: ne=4*e*(nx-1)+1;break;
          }
          meshdom_p->geomElements[e] = new GeomElement(elements_[ne], numFace, k + 1);
          k++;
        }
      }
      domains_.push_back(meshdom_p);
    } // end of if (sideNames
  } // end of for (number_t d

  for (number_t d = 0; d < sideOfSideNames.size(); ++d)
  {
    if (sideOfSideNames[d] != "")   //create a domain
    {
      meshdom_p = (new GeomDomain(*this, sideNames[d], 1, vertexdesc[d]))->meshDomain();
      meshdom_p->geomElements.resize(1);
      number_t nv = 0;
      if (splitDirection != _cross)
      {
        switch (d)
        {
          case 0: nv = 0; break;
          case 1: nv = nx - 1; break;
          case 2: nv = nx * ny - 1; break;
          case 3: nv = nx * (ny - 1) ; break;
        }
      }
      else
      {
        switch (d)
        {
          case 0: nv = 0; break;
          case 1: nv = nx - 1; break;
          case 2: nv = (2*nx - 1) * (ny - 1) + nx - 1; break;
          case 3: nv = (2*nx - 1) * (ny - 1); break;
        }
      }
      // now we search for an edge having vertex nv as a vertex
      
      // meshdom_p->geomElements[0] = new GeomElement(elements_[ne], numFace, k + 1);
      domains_.push_back(meshdom_p);
    }
  }
  lastIndex_ = k ;

  //merge domains with same name
  mergeDomainsWithSameName();

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  // sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)
  trace_p->pop();
}

//merge all domains declared in domains_ vector having the same name
void Mesh::mergeDomainsWithSameName()
{
  if (domains_.size()==0) return;    //no domain defined
  //list of domains by name
  std::map<string_t,std::vector<const GeomDomain*> > mapdom;
  std::map<string_t,std::vector<const GeomDomain*> >::iterator itm;
  std::vector<GeomDomain*>::iterator itd=domains_.begin();
  for (; itd!=domains_.end(); ++itd)
  {
    string_t dn=(*itd)->name();
    itm=mapdom.find(dn);
    if (itm==mapdom.end()) mapdom[dn]=std::vector<const GeomDomain*>(1,*itd);
    else itm->second.push_back(*itd);
  }
  //merge domains with same name
  domains_.clear();
  std::vector<const GeomDomain*>::iterator itc;
  for (itm=mapdom.begin(); itm!=mapdom.end(); ++itm)
  {
    if (itm->second.size()>1)  //more than one, merge it
    {
      GeomDomain* gd = &xlifepp::merge(itm->second,itm->first);
      //domains_.push_back(gd->meshDomain());  now done in merge
      for (itc=itm->second.begin(); itc!=itm->second.end(); ++itc) delete *itc;  //delete old domains
    }
    else domains_.push_back(const_cast<GeomDomain*>(*itm->second.begin()));
  }
}

void Mesh::meshQ1Parallelogram(Parallelogram& para, number_t nx, number_t ny, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames)
{
  //initialisation
  trace_p->push("Mesh::meshQ1Parallelogram");
  Point a = para.p(1), b = para.p(2), c = para.p(4);
  dimen_t dim=a.size();
  if (a == b || a == c) { error("degenerated_elt", "parallelogram"); }

  isMadeOfSimplices_ = false;
  order_ = 1;
  firstOrderMesh_p = this;

  //construct nodes
  nodes.resize(nx*ny);
  number_t k = 0;
  for (number_t j = 0; j < ny; j++)
    for (number_t i = 0; i < nx; i++, k++)
      nodes[k] = a+i*(b-a)/(nx-1)+j*(c-a)/(ny-1);

  //construct mesh elements
  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  RefElement* ref_p = findRefElement(_quadrangle, interp_p);
  elements_.resize((nx-1) * (ny-1));
  k = 0;
  for (number_t j = 0; j < ny-1 ; j++)
  {
    for (number_t i = 0; i < nx-1; i++, k++)
    {
      elements_[k] = new GeomElement(this, ref_p, dim, k + 1);
      MeshElement* melt = elements_[k]->meshElement();
      number_t n = j * nx + i + 1;
      melt->nodeNumbers[0] = n ;
      melt->nodeNumbers[1] = n + 1;
      melt->nodeNumbers[2] = n + nx + 1;
      melt->nodeNumbers[3] = n + nx;
      melt->vertexNumbers = melt->nodeNumbers;   //update vertex numbers, same as node
      melt->setNodes(nodes);                 //update node pointers
    }
  }

  //construct vertex indices (begins at 1)
  vertices_.resize(nodes.size());
  for (number_t i = 0; i < nodes.size(); i++) { vertices_[i] = i + 1; }

  //construct main domain
  string_t nmd=para.domName();
  if (nmd=="") nmd="#Omega";
  MeshDomain* meshdom_p = (new GeomDomain(*this, nmd, 2, "rectangle [a,b]x[c,d]"))->meshDomain();
  meshdom_p->geomElements = elements_;
  domains_.push_back(meshdom_p);

  //construct boundary domains
  std::vector<string_t> descrip;
  descrip.push_back("boundary side [a,b]x{c} of rectangle [a,b]x[c,d]");
  descrip.push_back("boundary side {b}x[c,d] of rectangle [a,b]x[c,d]");
  descrip.push_back("boundary side [a,b]x{d} of rectangle [a,b]x[c,d]");
  descrip.push_back("boundary side {a}x[c,d] of rectangle [a,b]x[c,d]");
  for (number_t d = 0; d < sideNames.size(); d++)
  {
    if (sideNames[d] != "")   //create a domain
    {
      meshdom_p = (new GeomDomain(*this, sideNames[d], 1, descrip[d]))->meshDomain();
      number_t nbelt = nx-1, numFace = 0;
      if (d == 1 || d == 3) nbelt = ny-1;
      switch (d)
      {
        case 0:
          numFace=1;
          break;
        case 1:
          numFace=2;
          break;
        case 2:
          numFace=3;
          break;
        case 3:
          numFace=4;
          break;
      }
      meshdom_p->geomElements.resize(nbelt);
      for (number_t e = 0; e < nbelt; e++)
      {
        number_t ne = 0;
        switch (d)
        {
          case 0:
            ne = e;
            break;
          case 1:
            ne = (e + 1) * (nx - 1) - 1;
            break;
          case 2:
            ne = (nx - 1) * (ny - 2) + e ;
            break;
          case 3:
            ne = e * (nx - 1);
            break;
        }
        meshdom_p->geomElements[e] = new GeomElement(elements_[ne], numFace, k + 1);
        k++;
      }
      domains_.push_back(meshdom_p);
    } // end of if (sideNames
  } // end of for (number_t d
  lastIndex_ = k ;

  //merge domains with same name
  mergeDomainsWithSameName();

  //compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  //sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)
  trace_p->pop();
}

/*---------------------------------------------------------------------------------------------
  create regular mesh of parallelepiped
  sideNames[0]-> [0,1]x[0,1]x{0} sideNames[1]-> [0,1]x[0,1]x{1}
  sideNames[2]-> [0,1]x{0}x[0,1] sideNames[3]-> [0,1]x{1}x[0,1]
  sideNames[4]-> {0}x[0,1]x[0,1] sideNames[5]-> {1}x[0,1]x[0,1]
  if sideNames[i] is empty, the side is not a identified as a GeomDomain
  nx, ny and nz are the number of intervals wanted on each edge of the parallelepiped (nx > 0, ny > 0, nz > 0)
  ---------------------------------------------------------------------------------------------*/
void Mesh::meshP1Parallelepiped(Parallelepiped& par, number_t nx, number_t ny, number_t nz, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames, const std::vector<string_t>& sideOfSideOfSideNames)
{ error("not_yet_implemented", "Mesh::meshP1Parallelepiped"); }

void Mesh::meshQ1Parallelepiped(Parallelepiped& par, number_t nx, number_t ny, number_t nz, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames, const std::vector<string_t>& sideOfSideOfSideNames)
{
  // initialisation
  trace_p->push("Mesh::meshQ1Parallelepiped");
  Point a = par.p(1), b = par.p(2), c = par.p(4), d = par.p(5);
  if (a == b || a == c || a == d) { error("degenerated_elt", "parallelepiped"); }

  isMadeOfSimplices_ = false;
  order_ = 1;
  firstOrderMesh_p = this;

  // construct nodes
  nodes.resize(nx*ny*nz);
  number_t l = 0;
  for (number_t k = 0; k < nz; k++)
    for (number_t j = 0; j < ny; j++)
      for (number_t i = 0; i < nx; i++, l++)
        nodes[l] = a+i*(b-a)/(nx-1)+j*(c-a)/(ny-1)+k*(d-a)/(nz-1);

  // construct mesh elements
  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  RefElement* ref_p = findRefElement(_hexahedron, interp_p);
  elements_.resize((nx-1) * (ny-1) * (nz-1));
  l = 0;
  for (number_t k = 0; k < nz-1 ; k++)
  {
    for (number_t j = 0; j < ny-1 ; j++)
    {
      for (number_t i = 0; i < nx-1; i++, l++)
      {
        elements_[l] = new GeomElement(this, ref_p, 3, l + 1);
        MeshElement* melt = elements_[l]->meshElement();
        number_t q = nx * ny, n = k * q + j * nx + i + 1;
        melt->nodeNumbers[0] = n ;
        melt->nodeNumbers[1] = n + 1;
        melt->nodeNumbers[2] = n + nx + 1;
        melt->nodeNumbers[3] = n + nx;
        melt->nodeNumbers[4] = n + q;
        melt->nodeNumbers[5] = n + q + 1;
        melt->nodeNumbers[6] = n + nx + 1 + q;
        melt->nodeNumbers[7] = n + nx + q;
        melt->vertexNumbers = melt->nodeNumbers;   // update vertex numbers, same as node
        melt->setNodes(nodes);                 // update node pointers
      }
    }
  }

  // construct vertex indices (begins at 1)
  vertices_.resize(nodes.size());
  for (number_t i = 0; i < nodes.size(); i++) { vertices_[i] = i + 1; }

  // construct main domain
  string_t nmd=par.domName();
  if (nmd=="") nmd="#Omega";
  MeshDomain* meshdom_p = (new GeomDomain(*this, nmd, 3, "parallelepiped [a,b]x[c,d]x[e,f]"))->meshDomain();
  meshdom_p->geomElements = elements_;
  domains_.push_back(meshdom_p);

  // construct boundary domains
  std::vector<string_t> descrip;
  descrip.push_back("boundary side [a,b]x[c,d]x{e} of parallelepiped [a,b]x[c,d]x[e,f]");
  descrip.push_back("boundary side [a,b]x[c,d]x{f} of parallelepiped [a,b]x[c,d]x[e,f]");
  descrip.push_back("boundary side [a,b]x{c}x[e,f] of parallelepiped [a,b]x[c,d]x[e,f]");
  descrip.push_back("boundary side [a,b]x{d}x[e,f] of parallelepiped [a,b]x[c,d]x[e,f]");
  descrip.push_back("boundary side {a}x[c,d]x[e,f] of parallelepiped [a,b]x[c,d]x[e,f]");
  descrip.push_back("boundary side {b}x[c,d]x[e,f] of parallelepiped [a,b]x[c,d]x[e,f]");

  for (number_t d = 0; d < std::min(number_t(6), sideNames.size()); d++)
  {
    if (sideNames[d] != "")  // create a domain
    {
      meshdom_p = (new GeomDomain(*this, sideNames[d], 2, descrip[d]))->meshDomain();
      number_t n1=0, n2=0, e = 0, numFace=0;
      switch (d)
      {
        case 0:
          n1=nx-1;
          n2=ny-1;
          numFace = 6;
          break;
        case 1:
          n1=nx-1;
          n2=ny-1;
          numFace = 3;
          break;
        case 2:
          n1=nx-1;
          n2=nz-1;
          numFace = 1;
          break;
        case 3:
          n1=nx-1;
          n2=nz-1;
          numFace = 4;
          break;
        case 4:
          n1=ny-1;
          n2=nz-1;
          numFace = 5;
          break;
        case 5:
          n1=ny-1;
          n2=nz-1;
          numFace = 2;
          break;
      }
      meshdom_p->geomElements.resize(n1 * n2);
      for (number_t j = 0; j < n2; j++)
      {
        for (number_t i = 0; i < n1; i++, e++)
        {
          number_t ne = 0;
          switch (d)
          {
            case 0:
              ne = e;
              break;
            case 1:
              ne = (nx - 1) * (ny - 1) * (nz - 2) + e ;
              break;
            case 2:
              ne = j * (nx - 1) * (ny - 1) + i;
              break;
            case 3:
              ne = (nx - 1) * (ny - 2) + j * (nx - 1) * (ny - 1) + i;
              break;
            case 4:
              ne = j * (nx - 1) * (ny - 1) + i * (nx - 1) ;
              break;
            case 5:
              ne = nx - 2 + j * (nx - 1) * (ny - 1) + i * (nx - 1) ;
              break;
          }
          meshdom_p->geomElements[e] = new GeomElement(elements_[ne], numFace, l + 1);
          l++;
        }
      }
      domains_.push_back(meshdom_p);
    } // end of if (sideNames
  } // end of for (number_t d
  lastIndex_ = l ;

  //merge domains with same name
  mergeDomainsWithSameName();

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  // sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)
  trace_p->pop();
}

//mesh parallelepiped with prism, use extrusion of section
void Mesh::meshPr1Parallelepiped(Parallelepiped& par, number_t nx, number_t ny, number_t nz, const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames, const std::vector<string_t>& sideOfSideOfSideNames, StructuredMeshSplitRule splitDirection)
{
  // initialisation
  trace_p->push("Mesh::meshPr1Parallelepiped");
  Point a = par.p(1), b = par.p(2), c = par.p(4), d = par.p(5);
  if (a == b || a == c || a == d) { error("degenerated_elt", "parallelepiped"); }

  //mesh 2d section (a,c,d)
  Parallelogram S(_v1 = a, _v2 = b, _v4 = c, _nnodes = Numbers(nx,ny),
                  _edge_names=Strings(sideNames[0],sideNames[3],sideNames[1],sideNames[2]), _domain_name="Omega") ;
  Mesh section(S, _shape=_triangle, _order=1, _generator=_structured, _split_direction=splitDirection);

  //extrusion
  number_t ns=0, nb=0;
  if (sideNames[4]!="")  ns=1;
  if (ns==0 && sideNames[5]!="")  ns=1;
  for (number_t k=0; k<4 && nb==0; k++)
    if (sideNames[k]!="") nb=1;
  ExtrusionData extdata;
  extdata.extrusion()=new Translation(_direction=(d-a)/nz);  extdata.layers()=nz;
  extdata.namingDomain() = 1; extdata.namingSection() = ns; extdata.namingSide()=nb;
  buildExtrusion(section, extdata);
  //rename domains
  if (sideNames[4] != "") renameDomain("Omega_0", sideNames[4]);
  if (sideNames[5] != "") renameDomain("Omega_"+tostring(nz), sideNames[5]);
  string_t ndom=par.domName();
  if (ndom=="") ndom="#Omega";
  renameDomain("Omega_e",ndom);
  std::set<string_t> sn;
  for (number_t k=0; k<4; k++) sn.insert(sideNames[k]);
  for (std::set<string_t>::iterator its =sn.begin(); its!=sn.end(); ++its)
    if (*its!="") renameDomain(*its+"_e",*its);

  //merge domains with same name
  mergeDomainsWithSameName();

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  trace_p->pop();
}

//mesh parallelepiped with pyramid
void Mesh::meshPy1Parallelepiped(Parallelepiped& par, number_t nx, number_t ny, number_t nz,
                                 const std::vector<string_t>& sideNames, const std::vector<string_t>& sideOfSideNames, const std::vector<string_t>& sideOfSideOfSideNames)
{
  Mesh meshQ1(par, _shape=_hexahedron);
  buildPyramidFromHexahedron(meshQ1);
}

// build the mesh of a geometry using its parametrization,
// i.e. the mesh nodes are the images P(Mi) of the mesh nodes Mi of the supporting geometry
// hsteps and nnodes of the supporting geometry are inherited from the original geometry
//    g    : the geometry to mesh
//    sh   : shape type of mesh element (_segment, _triangle, ...)
//    order: element order 1,2, ...
//    pattern  : mesh options set (_structured or _unstructured or _defaultPattern)
// build first the mesh of the supporting geometry (parameters geometry mesh)
// then copy this mesh to current mesh
// then modify nodes by applying parametrization
// finally update some data
void Mesh::meshFromParametrization(const Geometry& g, ShapeType sh, number_t order, MeshPattern pattern, StructuredMeshSplitRule splitDirection)
{
  warning("experimental","Mesh::meshFromParametrization");
  trace_p->push("Mesh::meshFromParametrization");
  Geometry* gp= g.clone();
  Parametrization* par=nullptr;
  int_t sd=shapeDim(sh);
  if (int_t(g.dim())==sd) par=const_cast<Parametrization*>(&gp->parametrization());
  else if (int_t(g.dim())==sd+1) par= const_cast<Parametrization*>(&gp->boundaryParametrization());
  else {error("free_error","Mesh::meshFromParametrization is only designed for plain mesh or boundary mesh");}
  //create mesh of the support geometry
  MeshGenerator mg=pattern;
  if (mg == _defaultPattern) { mg=_structured; }
  par->geomSupport().h()=g.h();
  par->geomSupport().n()=g.n();
  par->geomSupport().domName()=g.domName()+"_parameters";
  par->geomSupport().sideNames()=g.sideNames();
  for (auto its=par->geomSupport().sideNames().begin();its!=par->geomSupport().sideNames().end();++its)
    *its+="_parameters";
  if (mg == structured) par->meshP() = new Mesh(par->geomSupport(), _shape=sh, _order=order, _generator=mg, _split_direction=splitDirection, _name=gp->domName()+"_"+"parameters");
  else par->meshP() = new Mesh(par->geomSupport(), _shape=sh, _order=order, _generator=mg, _name=gp->domName()+"_"+"parameters");
  //new mesh using transportation
  copy(*par->meshP());
  delete geometry_p;   // delete the clone created by copy
  geometry_p = gp;
  // apply transformation to nodes
  for (auto itn=nodes.begin(); itn!=nodes.end();++itn) *itn=(*par)(*itn);
  // change names
  for (auto itd=domains_.begin(); itd!=domains_.end();++itd)
  {
    string_t s=(*itd)->name();
    replaceString(s,"parameters","parametrized");
    (*itd)->rename(s);
  }
  name_ = gp->domName()+"_"+"parametrized";
  comment_ = "built from parametrization";
  buildGeomData();
  setShapeTypes();
  trace_p->pop();
}

// build the mesh of a geometry boundary using structured mesh tools
void Mesh::meshBoundaryStructured(const Geometry& geo, ShapeType sh, StructuredMeshSplitRule splitDirection)
{
   trace_p->push("Mesh::meshBoundaryStructured");
   ShapeType sg=geo.shape();
   if( sg!=_polygon && sg!= _quadrangle && sg!=_parallelogram && sg!=_rectangle && sg!=_square &&
       sg!=_polyhedron && sg!= _parallelepiped && sg!=_cuboid && sg!=_cube)
   {
       error("free_error","Mesh::meshBoundaryStructured deals only with polygon or polyhedron geometry");
       trace_p->pop();
   }
   geometry_p = geo.boundary().clone();
   for(auto& e : geometry_p->components())
   {
      merge(Mesh(*e.second, _shape=sh, _generator=_structured, _split_direction=splitDirection),true);
   }
   trace_p->pop();
}


/*---------------------------------------------------------------------------------------------
  index all of side element of the mesh by travelling all sidedomains
  ---------------------------------------------------------------------------------------------*/
void Mesh::createSideEltIndex(std::map<string_t, GeomElement*>& sideEltIndex) const  //! index all the side elements of mesh
{
  sideEltIndex.clear();
  for (number_t d = 0; d < nbOfDomains(); d++)
  {
    const MeshDomain* mdom = domains_[d]->meshDomain();
    if (mdom!=nullptr && mdom->isSideDomain()) xlifepp::createSideEltIndex(mdom->geomElements, sideEltIndex);
  }
}

/*---------------------------------------------------------------------------------------------
  build the sides vector of all mesh elements (not built by default), side numbering not optimized
  side numberings of each mesh element are updated
  Be careful: this function has to be called after boundary domains construction
  ---------------------------------------------------------------------------------------------*/
void Mesh::buildSides()
{
  trace_p->push("Mesh::buildSides");
  std::map<string_t, number_t> sideIndex; //temporary index where the string key is the ordered list of the side vertex numbers
  std::vector<GeomElement*>::iterator itel;
  std::vector<number_t>::iterator itn;
  std::map<string_t, number_t>::iterator itsn;
  number_t nbside = 0;

  //first, initialize sideIndex map with sides from boundary domains
  for (number_t d = 1; d <= nbOfDomains(); d++)
  {
    const GeomDomain& dom = domains(d);
    if (dom.domType() == _meshDomain && dom.dim() == meshDim() - 1)   //boundary domain of the mesh
    {
      std::vector<GeomElement*>::const_iterator itel;
      for (itel = dom.meshDomain()->geomElements.begin(); itel != dom.meshDomain()->geomElements.end(); itel++)
      {
        string_t key = (*itel)->encodeElement();       //encode vertex numbers
        if (sideIndex.find(key) == sideIndex.end())      //add GeomElement in map
        {
          sides_.push_back(*itel);
          nbside++;
          sideIndex[key] = nbside;
        }
      } // end of for (itel=
    } // end of if (dom
  } // end of for (number_t

  // main loop on mesh elements
  for (itel = elements_.begin(); itel != elements_.end(); itel++)
  {
    MeshElement* melt = (*itel)->meshElement();
    for (number_t s = 0; s < (*itel)->numberOfSides(); s++)   // loop on element sides
    {
      string_t key = (*itel)->encodeElement(s + 1); // encode vertex numbers of side s
      itsn = sideIndex.find(key);              // search side in sideIndex map
      if (itsn == sideIndex.end())                // create a new side element
      {
        sides_.push_back(new GeomElement(*itel, s + 1, ++lastIndex_)); //update sides_
        nbside++;
        sideIndex[key] = nbside;            // update sideIndex
        melt->sideNumbers[s] = nbside;       // update sideNumbers
      }
      else // update side numbering of element
      {
        melt->sideNumbers[s] = itsn->second; //update sideNumbers
        std::vector<GeoNumPair>& parside = sides_[itsn->second - 1]->parentSides(); //update parentSides_
        if (std::find(parside.begin(), parside.end(), GeoNumPair(*itel, s + 1)) == parside.end())
          { parside.push_back(GeoNumPair(*itel, s + 1)); }
      } // end of else
    } // end for (number_t s
  } //end for (itel
  trace_p->pop();
}

/*---------------------------------------------------------------------------------------------
 build the sides of sides vector of all mesh elements (not built by default)
  note that side of side numbering of each mesh element is updated
  Important note: the parent element of a side of side (edge in 3D) is a side (face in 3D)
           thus the list of sides (sides_) has to be built before to built the list of sides of sides
  NOT CHECKED
  ---------------------------------------------------------------------------------------------*/
void Mesh::buildSideOfSides()
{
  trace_p->push("Mesh::buildSideOfSides");
  if (meshDim() < 3) { error("mesh_nosos"); }
  if (sides_.size() == 0) { buildSides(); }    //build list of sides if it is not built

  std::map<string_t, number_t> sideIndex;   //temporary index where the string key is the ordered list of the side of side vertex numbers
  std::vector<GeomElement*>::iterator itel;
  std::vector<number_t>::iterator itn;
  std::map<string_t, number_t>::iterator itsn;
  number_t nbside = 0;

  // first, initialize sideIndex map with sides of sides from boundary domains of n-2 dimension
  for (number_t d = 1; d <= nbOfDomains(); d++)
  {
    const GeomDomain& dom = domains(d);
    if (dom.domType() == _meshDomain && dom.dim() == meshDim() - 2)   //boundary domain of n-2 dimension
    {
      std::vector<GeomElement*>::const_iterator itel;
      for (itel = dom.meshDomain()->geomElements.begin(); itel != dom.meshDomain()->geomElements.end(); itel++)
      {
        string_t key = (*itel)->encodeElement();       //encode vertex numbers
        if (sideIndex.find(key) == sideIndex.end())      //add GeomElement in map
        {
          sides_.push_back(*itel);
          nbside++;
          sideIndex[key] = nbside;
        }
      } // end of for (itel=
    } // end of if (dom
  } // end of for (number_t

  // main loop on sides element
  for (itel = sides_.begin(); itel != sides_.end(); itel++)
  {
    MeshElement* melt = (*itel)->parentSides()[0].first->meshElement(); // parent of side element as a mesh element
    for (number_t s = 0; s < (*itel)->geomRefElement()->nbSides(); s++)     // loop on sides of side sit
    {
      string_t key = (*itel)->encodeElement(s); // encode vertex numbers of side s of side element itel
      itsn = sideIndex.find(key);             // search side in sideIndex map
      if (itsn == sideIndex.end())              // create a new side element
      {
        sideOfSides_.push_back(new GeomElement(*itel, s + 1, lastIndex_++)); // update sideOfSides_
        nbside++;
        sideIndex[key] = nbside;                   // update sideIndex
        melt->sideOfSideNumbers.push_back(nbside); //update sideNumbers
      }
      else //update side numbering of element
      {
        melt->sideOfSideNumbers.push_back(itsn->second); // update sideOfSideNumbers
        sideOfSides_[itsn->second]->parentSides().push_back(GeoNumPair(*itel, s + 1)); // update parentSides_
      }
    } // end for (number_t s
  } // end for (itel

  trace_p->pop();
}

/*---------------------------------------------------------------------------------------------
 build for each vertex v of the mesh, the list of elements having v as vertex with its local number
 (not built by default)
 ---------------------------------------------------------------------------------------------*/
void Mesh::buildVertexElements()
{
  trace_p->push("Mesh::buildVertexElements");
  //build the list of global vertex number -> list of (elements,local vertex number)
  vertexElements_.resize(vertices_.size());
  std::vector<GeomElement*>::iterator itel;  //mesh element iterator
  for (itel = elements_.begin(); itel != elements_.end(); itel++)   //loop on mesh elements
  {
    for (number_t i = 1; i <= (*itel)->numberOfVertices(); i++)
    {
      vertexElements_[(*itel)->vertexNumber(i) - 1].push_back(GeoNumPair(*itel, i));
    }
  }
  trace_p->pop();
}

/*! Update parent sides of elements involved in all side domains
*/
void Mesh::updateSideDomains()
{
  trace_p->push("Mesh::updateSideDomains");
  std::vector<GeomDomain*>::iterator itd=domains_.begin();          // iterator on list of geometric domains
  std::map<string_t,std::vector<GeoNumPair> > sideIndex;            //temporary side index map
  for (; itd!=domains_.end(); ++itd)  //loop on geometric domains
  {
    MeshDomain* mdom=(*itd)->meshDomain();
    if (mdom->isSideDomain())    //only side domains
    {
      if (sideIndex.size()==0)  createSideIndex(sideIndex);
      mdom->updateSides(sideIndex);
    }
  }
  trace_p->pop();
}

/*! update node pointers of meshelement from nodenumbers
    nodenumbers has to be up to date !!!
    withDomains = false ->  do not process the side domains (default)
*/
void Mesh::updateNodePointers(bool withDomains)
{
  trace_p->push("Mesh::updateNodePointers");
  std::vector<GeomElement*>::iterator ite;
  for (ite=elements_.begin(); ite!=elements_.end(); ++ite)
  {
    MeshElement* melt=(*ite)->meshElement();
    if (melt!=nullptr)
      for (number_t i = 0; i < melt->nodeNumbers.size(); i++) melt->nodes[i] = &(nodes[melt->nodeNumbers[i] - 1]);
  }

  if (!withDomains) {trace_p->pop(); return;}

  //process side domains that may have mesh elements allocated
  std::vector<GeomDomain*>::iterator itd;
  for (itd=domains_.begin(); itd!=domains_.end(); ++itd)
  {
    MeshDomain* mdom=(*itd)->meshDomain();
    if (mdom!=nullptr && (*itd)->isSideDomain())
    {
      for (ite=mdom->geomElements.begin(); ite!=mdom->geomElements.end(); ++ite)
      {
        MeshElement* melt=(*ite)->meshElement();
        if (melt!=nullptr)
          for (number_t i = 0; i < melt->nodeNumbers.size(); i++) melt->nodes[i] = &(nodes[melt->nodeNumbers[i] - 1]);
      }
    }
  }
  trace_p->pop();
}

/*---------------------------------------------------------------------------------------------
  build geometric data
 ---------------------------------------------------------------------------------------------*/
void Mesh::buildGeomData()
{
  trace_p->push("Mesh::buildGeomData()");
  if (geometry_p==nullptr) geometry_p= new Geometry(BoundingBox(computeBB()), "");    //no geometry exists, create one to store BoundingBox
  else if (geometry_p->boundingBox.dim()==0) geometry_p->boundingBox=BoundingBox(computeBB());   // geometry exists, but void BoundingBox
  for (std::vector<GeomElement*>::iterator itElt = elements_.begin(); itElt != elements_.end(); itElt++)
  {
    (*itElt)->meshElement()->computeMeasures();
  }
  trace_p->pop();
}

// set the shape types for all mesh domains
void Mesh::setShapeTypes()
{
  std::vector<GeomDomain*>::iterator itd=domains_.begin();
  for (; itd!=domains_.end(); itd++)
  {
    (*itd)->meshDomain()->setShapeTypes();
  }
}

/*---------------------------------------------------------------------------------------------
  mesh merging tool
 ---------------------------------------------------------------------------------------------*/
/*!
 Merge a mesh to the current mesh with the following rules:
 - the two meshes must have the same dimensions (spaceDim and meshDim)
 - duplicated point are merged (duplicated means same points at a given tolerance, see Point class)
 - duplicated element (same order and same nodes) are merged
 notes: if meshes are not compatible, they are merged in a stupid way ...
      algorithm is not optimized !
      sides_, sideOfSides_, vertexElements_ vectors are not copied (recall buildSides)

 mergeSharedBoundary: if true, same boundary domains are merged in a new one domain and original domains are kept
                with the naming rule: let na1 and na2 the first and second domain name
                               the merged domain name is "na1 or na2"
 nmd: the optional name of the main domain (the whole domain), by default "#Omega"
 */
void Mesh::merge(const Mesh& m, bool mergeSharedBoundary, const string_t& nmd)
{
  trace_p->push("Mesh::merge");
  if (lastIndex_==0)   //current mesh is presumed empty, do copy
  {
    copy(m);
    // renameDomain(0,nmd);
    trace_p->pop();
    return;
  }

  if (m.spaceDim() != spaceDim()) { error("mesh_mismatch_dims", spaceDim(), m.spaceDim()); }
  // if (m.meshDim() != meshDim()) { error("mesh_mismatch_dims", meshDim(), m.meshDim()); }

  //update general data
  comment_ = "merge of " + name() + " and " + m.name();
  name_ += " + " + m.name();
  isMadeOfSimplices_ = isMadeOfSimplices_ && m.isMadeOfSimplices_;
  order_ = std::max(order_, m.order_);
  if (order_ > 2 && firstOrderMesh_p != nullptr)   //destroy old underlying first order mesh
  {
    delete firstOrderMesh_p;
    firstOrderMesh_p = nullptr;
  }
  //clear sides, ... of current mesh (has to be rebuilt later)
  sides_.clear();
  sideOfSides_.clear();
  vertexElements_.clear();

  //update geometry attributes (bounding box)
  *(geometry_p)+=*(m.geometry_p);
  geometry_p->domName(geometry_p->domName() + " + " + m.geometry_p->domName());
  geometry_p->boundingBox += m.geometry_p->boundingBox;

  real_t mesm=0.;   //max of measure os elements
  std::vector<GeomElement*>::iterator it_elt1;
  for (it_elt1 = elements_.begin(); it_elt1 != elements_.end(); it_elt1++)   //loop on elements of current mesh
    mesm=std::max(mesm,(*it_elt1)->meshElement()->measures[0]);
  real_t oldtol=Point::tolerance;
  if (mesm > 0) Point::tolerance =std::sqrt(mesm)/1000; //change tolerance in Point comparison

  //merge nodes
  std::vector<Point>::iterator itnod1;
  std::vector<Point>::const_iterator itnod2;
  std::map<Point, number_t> newNodes;                  //new list of nodes with no duplicate node
  std::map<Point, number_t>::iterator itmpn;            //iterator on previous map
  std::vector<number_t> mapNumbers(m.nodes.size(), 0);  //numbering of nodes of mesh to add in new list of nodes
  number_t k = 1, k2 = 0;
  for (itnod1 = nodes.begin(); itnod1 != nodes.end(); itnod1++, k++)      //insert nodes of current mesh
    newNodes.insert(std::pair<Point, number_t>(*itnod1, k));
  for (itnod2 = m.nodes.begin(); itnod2 != m.nodes.end(); itnod2++, k2++)   //insert nodes of mesh to add
  {
    itmpn = newNodes.find(*itnod2);
    if (itmpn != newNodes.end())   // found
      mapNumbers[k2] = itmpn->second;
    else //not found, add in newNodes list
    {
      newNodes.insert(std::pair<Point, number_t>(*itnod2, k));
      mapNumbers[k2] = k;
      k++;
    }
  }
  Point::tolerance=oldtol;  //reset old point tolerance

  //update nodes of current mesh and clear newNodes map
  nodes.resize(newNodes.size());
  for (itmpn = newNodes.begin(); itmpn != newNodes.end(); itmpn++)
  { nodes[itmpn->second-1] = itmpn->first; }
  newNodes.clear();

  //update vertices number (number are relative to nodes numbering)
  std::vector<number_t>::const_iterator itv;
  std::set<number_t> vert(vertices_.begin(),vertices_.end());
  for (itv = m.vertices_.begin(); itv != m.vertices_.end(); itv++)    //loop on vertices of added mesh
  {
    number_t vn = mapNumbers[*itv-1];
    if (vert.find(vn)==vert.end()) vert.insert(vn);   //vertex not in list
  }
  vertices_.assign(vert.begin(),vert.end());
  vert.clear();

  //merge elements
  std::map<string_t, GeoNumPair> eltsIndex;            //element index list of ordered vertex numbers -> (element,rank)
  std::map<number_t, number_t> eltNumbers;               //numbering of elements of adding mesh in new list of elements
  std::vector<GeomElement*>::const_iterator it_elt2;
  std::map<string_t, GeoNumPair>::iterator it_msg;       //iterator on eltsIndex map

  k = 1;
  for (it_elt1 = elements_.begin(); it_elt1 != elements_.end(); it_elt1++, k++)    //insert elements of current mesh
  {
    MeshElement* melt = (*it_elt1)->meshElement();
    melt->sideNumbers.clear();     //optionnal side informations are forgotten
    melt->sideOfSideNumbers.clear();
    for (number_t i = 0; i < melt->nodeNumbers.size(); i++)    //update node pointers
      melt->nodes[i] = &(nodes[melt->nodeNumbers[i] - 1]);
    eltsIndex.insert(std::pair<string_t, GeoNumPair>((*it_elt1)->encodeElement(), GeoNumPair(*it_elt1, k)));
  }
  k2 = 1;
  for (it_elt2 = m.elements_.begin(); it_elt2 != m.elements_.end(); it_elt2++, k2++)      //insert elements of mesh to add
  {
    //copy mesh element and update node numbers
    GeomElement* elt = new GeomElement(**it_elt2);  //copy element
    elt->meshP() = this;
    MeshElement* melt = elt->meshElement();
    for (number_t i = 0; i < melt->nodeNumbers.size(); i++)
      melt->nodeNumbers[i] = mapNumbers[melt->nodeNumbers[i] - 1];
    for (number_t i = 0; i < melt->vertexNumbers.size(); i++)
      melt->vertexNumbers[i] = mapNumbers[melt->vertexNumbers[i] - 1];

    string_t eltkey = elt->encodeElement();
    it_msg = eltsIndex.find(eltkey);
    if (it_msg != eltsIndex.end() &&                                 //same vertices
        it_msg->second.first->geomRefElement() == elt->geomRefElement()) //same reference element
    {
      eltNumbers[(*it_elt2)->number()] = it_msg->second.second;
      delete elt;
    }
    else //add element in current mesh
    {
      elt->number() = k;            //update index of element
      melt->index() = k;
      for (number_t i = 0; i < melt->nodeNumbers.size(); i++)    //update node pointers
        melt->nodes[i] = &(nodes[melt->nodeNumbers[i] - 1]);
      melt->sideNumbers.clear();       //optional side informations are not copied
      melt->sideOfSideNumbers.clear(); //too intricate, recall buildSides after
      elements_.push_back(elt);
      eltsIndex.insert(std::pair<string_t, GeoNumPair>(eltkey, GeoNumPair(elt, k)));
      eltNumbers[(*it_elt2)->number()] = k;
      k++;
    }
  }

  //shift side element numbering of domains
  for (number_t d = 0; d < domains_.size(); d++)
  {
    MeshDomain* mdom = domains_[d]->meshDomain();
    for (it_elt2 = mdom->geomElements.begin(); it_elt2 != mdom->geomElements.end(); it_elt2++)
    {
      GeomElement* elt = *it_elt2;
      if (elt->isSideElement())
      {
        elt->number() = k;
        k++;
      }
    }
  }

  number_t nbdom=domains_.size();
  bool hasDomDefault = false;
  // remove internal domain if exists
  for (number_t d=0; d < nbdom; ++d)
  {
    if (domains_[d]->name().find("#") == 0)
    {
      hasDomDefault=true;
      removeDomain(domains_[d]->name());
    }
  }
  nbdom=domains_.size();

  //copy domains of adding mesh, domains may be renamed and pointers are updated but the domains are not merged !
  for (number_t d = 0; d < m.domains_.size(); d++)
  {
    MeshDomain* mdom = m.domains_[d]->meshDomain();
    //rename domain of adding mesh if its name already exists in current mesh
    string_t na = mdom->name();
    if (na.find("#") != 0)  // internal domain (name start with #) is ignored
    {
//      number_t n = 0;
//      while (n < domains_.size())
//      {
//        if (domains_[n]->name() != na) n++;
//        else {na += "_2"; n = 0;}
//      }
      GeomDomain* newdom = new GeomDomain(*this, na, mdom->dim(), mdom->description());
      MeshDomain* ndom = newdom->meshDomain();
      for (it_elt2 = mdom->geomElements.begin(); it_elt2 != mdom->geomElements.end(); it_elt2++)
      {
        GeomElement* elt = *it_elt2;
        if (elt->isSideElement())    //create new side element
        {
          GeomElement* selt = new GeomElement(*elt);
          selt->number() = k;
          selt->meshP() = this;
          k++;
          selt->deleteMeshElement();  //destroy meshElement pointer to be safe
          for (number_t j = 0; j < selt->parentSides().size(); j++)
          {
            GeomElement* parelt = selt->parentSides()[j].first;
            if (parelt->isSideElement())    //is a side of side
              error("domain_type_not_handled", words("side of side"));
            else //is a side
              selt->parentSides()[j].first = elements_[eltNumbers[parelt->number()] - 1];
          }
          ndom->geomElements.push_back(selt);
        }
        else  //not a side element
          ndom->geomElements.push_back(elements_[eltNumbers[elt->number()] - 1]);
      }
      domains_.push_back(ndom);
    }
    else hasDomDefault=true;
  }

  dimen_t d=meshDim();
  number_t nbds = domains_.size();
  std::set<number_t> domainsToRemove;
  //merge boundary domains if  mergeSharedBoundary is true or if two boundary domains have the same name
  //for each shared boundary domains pair (d1,d2), merge them in a new one with original name and rename the old one with n1 and n2
   for (number_t d1 = 0; d1 < nbdom; d1++)    //loop on current mesh domains
   {
     MeshDomain* mdom1 = domains_[d1]->meshDomain();
     if (mdom1->dim() == d-1)     //possible shared boundary
     {
       std::set<number_t> sn1=mdom1->nodeNumbers();
       for (number_t d2 = nbdom; d2 < nbds; d2++)    //loop on added domains
       {
         MeshDomain* mdom2 = domains_[d2]->meshDomain();
         if (mdom2->dim() == d-1)
         {
           //bool sameName = (mdom1->name()+"_2" == mdom2->name());
           bool sameName = (mdom1->name() == mdom2->name());
           std::set<number_t> sn2=mdom2->nodeNumbers();
           number_t nc=0;
           auto it2=sn2.begin();
           auto it1=sn1.begin();
           bool sameSet = true;
           for(; sameSet && it1!=sn1.end(); ++it1, ++it2)
              if(*it1!=*it2) {sameSet=false;} else nc++;
           if (sameSet && (mergeSharedBoundary || sameName))   //merge dom1 and dom2
           {
              std::vector<GeomElement*>::const_iterator itg;
              std::map<string_t,GeomElement*> mgeo;
              for (itg = mdom1->geomElements.begin(); itg != mdom1->geomElements.end(); itg++)
                mgeo.insert(std::make_pair((*itg)->encodeElementNodes(),*itg));
              string_t rname = mdom1->name();
              if(!sameName) rname+="_"+mdom2->name();
              GeomDomain* newdom = new GeomDomain(*this, rname, mdom1->dim(), mdom1->description());
              MeshDomain* ndom = newdom->meshDomain();
              for (itg = mdom2->geomElements.begin(); itg != mdom2->geomElements.end(); itg++)
              {
                std::map<string_t,GeomElement*>::iterator itm=mgeo.find((*itg)->encodeElementNodes());
                if (itm!=mgeo.end())
                {
                  GeomElement* selt = new GeomElement(*itm->second);
                  selt->number() = k;
                  k++;
                  selt->deleteMeshElement();  //destroy meshElement pointer to be safe
                  selt->parentSides().push_back((*itg)->parentSides()[0]);
                  ndom->geomElements.push_back(selt);
                }
              }
              if (ndom->geomElements.size()>0) domains_.push_back(ndom);
              else delete newdom;   //void domain !!!
              domainsToRemove.insert(d1);
              domainsToRemove.insert(d2);
           }
           else if(nc>1 && sameName)  // case of side domains with same name and defining a partial interface
            error("free_error","side domains with same name but not defining an interface is not allowed");
         }
       }
     }
   }

  //remove some domains
  std::list<GeomDomain*> doms;
  auto itf=domainsToRemove.end();
  for(number_t n=0;n<domains_.size();n++)
  {
      if(domainsToRemove.find(n)==itf) doms.push_back(domains_[n]);
      else delete domains_[n];
  }
  domains_.assign(doms.begin(),doms.end());

  //merge domains with same name
  mergeDomainsWithSameName();

  //construct main domain if several domains of max dim
  number_t p=0;
  for(number_t n=0;n<domains_.size();n++)
     if(domains_[n]->meshDomain()->dim()==d) p++;
  if(hasDomDefault || p>1) //had #Omega domains or several domains of max dim
  {
    MeshDomain* meshdom_p = (new GeomDomain(*this, nmd, meshDim(), comment_))->meshDomain();
    //keep in main domain only elements of maximal dimension
    dimen_t maxdim=0;
    for (it_elt1 = elements_.begin(); it_elt1 != elements_.end(); it_elt1++)
      maxdim=std::max(maxdim, (*it_elt1)->meshElement()->elementDim());
    for (it_elt1 = elements_.begin(); it_elt1 != elements_.end(); it_elt1++)
      if ((*it_elt1)->meshElement()->elementDim()==maxdim) meshdom_p->geomElements.push_back(*it_elt1);
    meshdom_p->setDescription(comment_+", made of "+tostring(meshdom_p->geomElements.size())+" elements");
    domains_.push_back(meshdom_p);
  }

  lastIndex_ = k;
  setShapeTypes();
  trace_p->pop();
}

/*!
 create a underlying first order mesh (P1) if there exists some elements of order greater than one
 elements of order greater than one are split in elements of order one as usual:
  - P2 triangle split in 4 P1 triangles, P3 triangle split in 9 P1 triangles, ...
  - P2 tetrahedron split in 6 P1 tetrahedra, ...
  - Q2 hexahedron split in 6 P1 tetrahedra, ...
 */
Mesh* Mesh::createFirstOrderMesh() const
{
  Mesh* firstOrderMesh=nullptr;
  if (order_==1 && isMadeOfSimplices_) {return firstOrderMesh;}   //nothing is created
  trace_p->push("Mesh::createFirstOrderMesh");  //create new mesh
  firstOrderMesh=new Mesh();
  firstOrderMesh->name_=name_+"_first_order";
  firstOrderMesh->comment_="generated by Mesh::createFirstOrderMesh";
  firstOrderMesh->isMadeOfSimplices_=true;
  firstOrderMesh->order_=1;
  firstOrderMesh->lastIndex_=0;
  firstOrderMesh->nodes=nodes;    //nodes are not modified when splitting mesh
  firstOrderMesh->geometry_p=geometry_p;

  // build list of elements: keep first order element and split element of order greater than 1
  std::map<GeomElement*,std::vector<GeomElement*> > listel;        //list of split elements
  typedef std::pair<GeomElement*,std::vector<GeomElement*> > mapair;
  std::vector<GeomElement*>::const_iterator itel;
  std::vector<GeomElement*>::iterator itel1;
  for (itel=elements_.begin(); itel!=elements_.end(); itel++)
  {
    GeomElement* elt=*itel;
    if (!elt->isSideElement())    //side element are not split
    {
      std::pair<std::map<GeomElement*,std::vector<GeomElement*> >::iterator,bool> ret;
      ret=listel.insert(mapair(elt,elt->splitP1()));       //split element
      for (itel1=ret.first->second.begin(); itel1!=ret.first->second.end(); itel1++)   //add to the list of elements of the first order mesh
      {
        firstOrderMesh->elements_.push_back(*itel1);
      }
    }
  }
  // update mesh pointer and number
  std::vector<GeomElement*>::iterator itel2;
  for (itel2=firstOrderMesh->elements_.begin(); itel2!=firstOrderMesh->elements_.end(); itel2++)
  {
    (*itel2)->meshP()=firstOrderMesh;
    (*itel2)->number()=firstOrderMesh->lastIndex_++;
  }

  // update vertices (all nodes are vertices)
  number_t nbnodes=nodes.size();
  firstOrderMesh->vertices_.resize(nbnodes);
  std::vector<number_t>::iterator itv=firstOrderMesh->vertices_.begin();
  for (number_t i=1; i<=nbnodes; i++,itv++) { *itv=i; }

  // update domains
  std::vector<GeomDomain*>::const_iterator itd;
  for (itd=domains_.begin(); itd!=domains_.end(); itd++)
  {
    GeomDomain* dom=*itd;
    if (dom->domType()==_meshDomain)
    {
      MeshDomain* meshdom = dom->meshDomain();
      MeshDomain* newdom = (new GeomDomain(*this, dom->name()+"_firstOrder", dom->dim(), dom->description()))->meshDomain();
      if (meshdom->isSideDomain())    //side domain, only side element
      {
        for (itel=meshdom->geomElements.begin(); itel!=meshdom->geomElements.end(); itel++)
        {
          GeomElement* elt=*itel;
          if (!elt->isSideElement())
          {
            where("Mesh::createFirstOrderMesh");
            error("side_elt_in_side_domain", meshdom->name(), elt->number());
          }
          std::vector<GeoNumPair>& parsides=elt->parentSides();
          std::vector<GeoNumPair>::iterator itgn;
          for (itgn=parsides.begin(); itgn!=parsides.end(); itgn++)
          {
            GeomElement* parelt=itgn->first;
            number_t side=itgn->second;
            if (parelt->isSideElement())
            {
              where("Mesh::createFirstOrderMesh");
              error("sos_elt_not_handled");
            }
            std::vector<std::pair<number_t,number_t> > sidenum=parelt->refElement()->splitP1Side(side);
            std::vector<std::pair<number_t,number_t> >::iterator its;
            for (its=sidenum.begin(); its!=sidenum.end(); its++)
            {
              GeomElement* eltP1=listel[parelt][its->first-1];
              GeomElement* sidelt=new GeomElement(eltP1,its->second,firstOrderMesh->lastIndex_++);
              newdom->geomElements.push_back(sidelt);
            }
          }
        }
      }
      else  //plain domain, copy element
      {
        for (itel=meshdom->geomElements.begin(); itel!=meshdom->geomElements.end(); itel++)
        {
          for (itel1=listel[*itel].begin(); itel1!=listel[*itel].end(); itel1++)
          {
            newdom->geomElements.push_back(*itel1);
          }
        }
      }
      newdom->setShapeTypes();
      firstOrderMesh->domains_.push_back(newdom);
    }
  }
  trace_p->pop();
  return firstOrderMesh;
}

/*--------------------------------------------------------------------------------------------
  remove a domain, be cautious when use it
 ---------------------------------------------------------------------------------------------*/
void Mesh::removeDomain(const string_t& na)
{
  string_t sna = trim(na);
  number_t nd = domains_.size(), ina = nd;
  for (number_t i = 0; i < nd && ina==nd; i++)
    if (domains_[i]->name() == sna) ina=i;
  if (ina==nd) error("mesh_failfinddomain", na);
  delete domains_[ina];
  for (number_t i=ina; i<nd-1; i++) domains_[i]=domains_[i+1];   //shift
  domains_.resize(nd-1);
}
//----------------------------------------------------------------------------------------------
/* utilities for crack function
//----------------------------------------------------------------------------------------------
 recursive function that adds elements adjacent to the current element by a side
 except if this side belongs to the crack domain
 or if the element to add does not touch the crack domain by a side and the previous one to
*/
void addElts(Node<GeomElement>* node, std::set<GeomElement*>& elts, const std::set<number_t>& vns,
             std::map<string_t,std::pair<GeomElement*,GeomElement*> >& sideIndex,
             std::set<number_t> vBoundaryIndex,
             std::map<string_t,GeomElement*>& cdomIndex, dimen_t d)
{
  //loop on side of current element
  Node<GeomElement>* prevnode=nullptr;
  GeomElement* elt = node->obj_, *eltn;
  std::vector<number_t> vn;
  std::vector<number_t>::iterator itn;
  for (number_t s=1; s<=elt->numberOfSides(); s++)
  {
    vn=elt->vertexNumbers(s);
    number_t vonCdom=0;
    for (itn=vn.begin(); itn!=vn.end(); ++itn)
      if (vns.find(*itn)!=vns.end()) vonCdom++;
    string_t sv=elt->encodeElement(s);
    if (vonCdom>0 && cdomIndex.find(sv)==cdomIndex.end()) //side touch cdom but not the entire side
    {
      std::pair<GeomElement*,GeomElement*>& felts= sideIndex[sv];
      eltn=felts.first;
      if (eltn==elt) eltn = felts.second;
      if (elts.find(eltn)!=elts.end()) //eltn is available
      {
        bool endElt=false;
        if (eltn->flag <=d) //does not touch by a side, may be an end element
        {
          vn=eltn->vertexNumbers();
          for (itn=vn.begin(); itn!=vn.end() && !endElt; ++itn)
            if (vBoundaryIndex.find(*itn)!=vBoundaryIndex.end()) endElt=true; //one vertex belongs to the boundary of crack
        }
        Node<GeomElement>* newnode = new Node<GeomElement>(eltn,node,node->level_+1);
        if (node->child_==nullptr) node->child_=newnode;
        else if (prevnode!=nullptr) prevnode->right_=newnode;
        prevnode=newnode;
        elts.erase(eltn);
        //thePrintStream<<string_t(2*node->level_,' ')<<"level "<<node->level_<<", add (endElt="<<endElt<<") " <<*eltn<<eol<<std::flush;
        if (!endElt) addElts(newnode, elts, vns, sideIndex, vBoundaryIndex, cdomIndex, d);
        else eltn->flag=-std::abs(eltn->flag);  //eltn is an end-element (no child)
      }
    }
  }
}

//move tree to set, removing end elements if closed crack
//std::set<GeomElement*> treeToSet(Node<GeomElement>& tree, CrackType ct)
std::set<GeomElement*> treeToSet(Node<GeomElement>& tree)
{
  std::set<GeomElement*> elts;
  Node<GeomElement>* curnode = &tree;
  while (curnode!=nullptr)
  {
    elts.insert(curnode->obj_);
    //if (ct ==_openCrack || curnode->obj_->flag >=0) elts.insert(curnode->obj_);
    if (curnode->child_!=nullptr) curnode=curnode->child_;     //take child, go down
    else
    {
      if (curnode->right_!=nullptr) curnode=curnode->right_; //take right, go to the right
      else // go up
      {
        Node<GeomElement>* next=nullptr;
        while (next==nullptr && curnode!=nullptr)
        {
          curnode=curnode->parent_;
          if (curnode!=nullptr) next=curnode->right_;
        }
        if (next!=nullptr) curnode=next;
      }
    }
  }
  return elts;
}

void removeElts(std::set<GeomElement*>& elts, const std::set<number_t>& vSideCrack)
{
  std::set<GeomElement*>::iterator its, it;
  if (vSideCrack.size()==0)  //remove all elements connected to crack boundary by one vertex or one edge in 3D (flag<0)
  {
    its=elts.begin();
    while (its!=elts.end())
    {
      if ((*its)->flag < 0) {it=its; ++it; elts.erase(its); its=it;}
      else ++its;
    }
  }
  else //restrict to sideCrack
  {
    std::vector<number_t> vn;
    std::vector<number_t>::iterator itn;
    its=elts.begin();
    while (its!=elts.end())
    {
      if ((*its)->flag < 0)
      {
        vn=(*its)->vertexNumbers();
        number_t von=0;
        for (itn=vn.begin(); itn!=vn.end(); ++itn)
          if (vSideCrack.find(*itn)!=vSideCrack.end()) von++;
        if (von>0) {it=its; ++it; elts.erase(its); its=it;}
        else ++its;
      }
      else ++its;
    }
  }
}

void addElts(std::set<GeomElement*>& elts, std::set<GeomElement*>& elts1, std::set<GeomElement*>& elts2, const std::set<number_t>& vSideCrack)
{
  // compute minimal distance between elements of elts and elts1/elts2
  std::vector<real_t> min1(elts.size(),theRealMax), min2=min1;
  std::set<GeomElement*>::iterator its, it;
  for (it=elts1.begin(); it!=elts1.end(); ++it)
  {
    Point P=(*it)->center();
    std::vector<real_t>::iterator itm=min1.begin();
    for (its=elts.begin(); its!=elts.end(); ++its, ++itm) *itm=std::min(*itm,P.distance((*its)->center()));
  }
  for (it=elts2.begin(); it!=elts2.end(); ++it)
  {
    Point P=(*it)->center();
    std::vector<real_t>::iterator itm=min2.begin();
    for (its=elts.begin(); its!=elts.end(); ++its, ++itm) *itm=std::min(*itm,P.distance((*its)->center()));
  }
  //compare distance and add elts in the right set
  std::vector<real_t>::iterator itm1=min1.begin(), itm2=min2.begin();
  bool testSide = (vSideCrack.size() > 0);
  std::vector<number_t> vn;
  std::vector<number_t>::iterator itn;
  for (its=elts.begin(); its!=elts.end(); ++its, ++itm1, ++itm2)
  {
    bool von=true;
    if (testSide)
    {
      vn=(*its)->vertexNumbers();
      von = false;
      for (itn=vn.begin(); itn!=vn.end() && !von; ++itn)
        if (vSideCrack.find(*itn)!=vSideCrack.end()) von=true;
    }
    if (von)
    {
      if (*itm1<*itm2) elts1.insert(*its);
      else elts2.insert(*its);
    }
  }
}

//to check crack algorithm
void pictureCrack(const std::set<GeomElement*>& elts1, const std::set<GeomElement*>& elts2, const string_t& fn)
{
  std::set<GeomElement*>::const_iterator its=elts1.begin();
  const Mesh* mp=(*its)->meshP();
  GeomDomain domext1(*mp, "crack_ext1", (*its)->elementDim(), "crack extension ");
  MeshDomain* mdom=domext1.meshDomain();
  mdom->geomElements.resize(elts1.size());
  std::vector<GeomElement*>::iterator it=mdom->geomElements.begin();
  for (its=elts1.begin(); its!=elts1.end(); ++its, ++it) *it = *its;
  mdom->setShapeTypes();
  std::ofstream out;
  out.open((fn+"_1.vtk").c_str());
  mp->vtkExport(domext1,out);
  out.close();

  its=elts2.begin();
  GeomDomain domext2(*mp, "crack_ext2", (*its)->elementDim(), "crack extension ");
  mdom=domext2.meshDomain();
  mdom->geomElements.resize(elts2.size());
  it=mdom->geomElements.begin();
  for (its=elts2.begin(); its!=elts2.end(); ++its, ++it) *it = *its;
  mdom->setShapeTypes();
  out.open((fn+"_2.vtk").c_str());
  mp->vtkExport(domext2,out);
  out.close();
}

//--------------------------------------------------------------------------------------------
/*! main function to crack a side domain cdom
    the crack process consists in
      - building the list of elements having a vertex on cdom
      - split this list in two set defining the sides of the crack (the first is cdom and the second is named cdom->name2)
      - adding new nodes (clone) and update the node numbering of parent element of side -
      - managing open and closed crack information
      - setting the name of crack sides

      The splitting algorithm differs from the case of a side domain in a same domain and an interface between two domains

      cdom: the domain (dim-1) to crack
      cracktype1 : a crack type (_openCrack or _closedcrack), default = _closedCrack
      sideCrack1 : the side domain of cdom where to apply the cracktype1, default = 0 mean all the side domain
      cracktype2 : a crack type (_openCrack or _closedCrack or _noCrack), default = _closedCrack
      sideCrack2 : the side domain of cdom where to apply the cracktype2, default = 0
      crackName1 : name of crack side 1, default="" -> crackName1 = "cdom name"_+
      crackName2 : name of crack side 2, default="" -> crackName1 = "cdom name"_-
      P1         : a point close to crack specifying which side of the crack is the first one, default = 0 means unset
                   if unset,  crack side 1 is chosen such that max x_E1 > max x_E2 if different , if equal max y_E1 > max y_E2, ...
                   where E1/E2 is the set of all vertex of all volume elements connected to  crack side 1/2
      withPicture: if true save list of crack elements as domain to pictureCrack1.vtk and pictureCrack2.vtk files

      NOTE: the original cdom becomes one of the crack side, renamed regarding arguments

      examples: m a mesh with Sigma (named Sigma) as side domain to crack
         - m.crack(Sigma)  closed crack of Sigma building the two side domains named Sigma+ and Sigma-
         - m.crack(Sigma, _openCrack)  open crack of Sigma building the two side domains named Sigma+ and Sigma-
         - m.crack(Sigma, _openCrack, dsig)  open crack on dsig and closed everywhere of Sigma building the two side domains named Sigma+ and Sigma-
         - m.crack(Sigma, _openCrack, 0, _noCrack, 0, "S+","S-")  open crack everywhere of Sigma building the two side domains named S+ and S-
         - m.crack(Sigma, _openCrack, 0, _noCrack, 0, "S+","S-", P)  open crack everywhere of Sigma building the two side domains named S+ and S-, P located at S+ side
*/
//---------------------------------------------------------------------------------------------
Mesh& Mesh::crack(GeomDomain& cdom, CrackType cracktype1, const GeomDomain* sideCrack1, CrackType cracktype2, const GeomDomain* sideCrack2,
                  const string_t & crackName1, const string_t &crackName2, const Point* P1, bool withPicture)
{
  trace_p->push("Mesh::crack(...)");
  MeshDomain* mdom=cdom.meshDomain();
  if (mdom==nullptr)
  {
    error("free_error",cdom.name()+" is not a mesh domain and cannot be cracked");
    trace_p->pop();
    return *this;
  }
  if (!cdom.isSideDomain())
  {
    error("free_error",cdom.name()+" is not a side domain and cannot be cracked");
    trace_p->pop();
    return *this;
  }
  //update parent side of cdom elements and find parent domains (1 or 2)
  cdom.updateParentOfSideElements();
  std::vector<GeoNumPair>& parelts = mdom->geomElements.front()->parentSides();
  GeomElement* parelt1=parelts[0].first, *parelt2=parelts[1].first;
  std::vector<GeomDomain*>::iterator itd=domains_.begin();
  std::vector<GeomElement*>::const_iterator itg;
  GeomDomain* dom1=nullptr, *dom2=nullptr;
  number_t d = cdom.dim();
  for (; itd!=domains_.end() && (dom1==nullptr || dom2==nullptr); ++itd)
  {
    if ((*itd)->dim()==d+1)
    {
      std::vector<GeomElement*> delts=(*itd)->meshDomain()->geomElements;
      // bool cont =true;
      for (itg=delts.begin(); itg!=delts.end()&& (dom1==nullptr || dom2==nullptr); ++itg)
      {
        if (dom1==nullptr && *itg==parelt1) dom1=*itd;
        if (dom2==nullptr && *itg==parelt2) dom2=*itd;
      }
    }
  }

  if (dom1==nullptr || dom2==nullptr) error("free_error","in crack, one or two domains not located");
  bool isinterface = (dom1!=dom2);

  //list of elements intersecting cdom by a face (3D), an edge (2D and 3D), a point (1D,2D and 3D)
  std::set<number_t> vns = mdom->vertexNumbers();  //side vertices
  std::set<GeomElement*> elts;
  std::map<string_t,std::pair<GeomElement*,GeomElement*> > sideIndex;
  std::map<string_t,std::pair<GeomElement*,GeomElement*> >::iterator itm;
  std::vector<number_t> vn ;
  std::vector<number_t>::iterator itn;
  std::set<number_t>::iterator itne=vns.end();
  std::set<GeomElement*>::iterator its, itse, it;
  for (itg=elements_.begin(); itg!=elements_.end(); ++itg) //loop on all mesh elements
  {
    vn =(*itg)->vertexNumbers();
    number_t k=0;
    for (itn=vn.begin(); itn!=vn.end() && k<d+1; ++itn)  //when k > d side on cdom is detected
      if (vns.find(*itn)!=itne) k++;
    if (k>0) //element intersects cdom
    {
      elts.insert(*itg);
      (*itg)->flag = k;

      for (number_t s=1; s<=(*itg)->numberOfSides(); s++)
      {
        string_t sv=(*itg)->encodeElement(s);
        itm=sideIndex.find(sv);
        if (itm==sideIndex.end()) sideIndex.insert(std::make_pair(sv, std::pair<GeomElement*,GeomElement*>(*itg,0)));
        else sideIndex[sv]=std::pair<GeomElement*,GeomElement*>(itm->second.first,*itg);
      }
    }
  }

  //Splitting
  std::set<GeomElement*> elts1, elts2;
  if (isinterface) //case of an interface between two domains
  {
    //build list 2 of elements on the other side of the crack
    itse=elts.end();
    std::vector<GeomElement*> delts=dom1->meshDomain()->geomElements;
    for (itg=delts.begin(); itg!=delts.end(); ++itg)
      if (elts.find(*itg)!=itse) elts1.insert(*itg);
    delts=dom2->meshDomain()->geomElements;
    for (itg=delts.begin(); itg!=delts.end(); ++itg)
      if (elts.find(*itg)!=itse) elts2.insert(*itg);
    elts.clear();
  }
  else //case of a side domain not an interface
  {
    //boundary of cdom
    std::map<string_t,GeomElement*> cdomIndex;
    std::map<string_t,GeoNumPair> boundaryIndex;
    std::map<string_t,GeoNumPair>::iterator itmb;
    for (itg=mdom->geomElements.begin(); itg!=mdom->geomElements.end(); ++itg) //loop on all cdom elements
    {
      cdomIndex[(*itg)->encodeElement()]= *itg;
      for (number_t s=1; s<=(*itg)->numberOfSides(); s++)
      {
        string_t sv=(*itg)->encodeElement(s);
        itmb=boundaryIndex.find(sv);
        if (itmb==boundaryIndex.end()) boundaryIndex[sv]=GeoNumPair(*itg,s);
        else boundaryIndex.erase(itmb);
      }
    }
    std::set<number_t> vBoundaryIndex;
    for (itmb=boundaryIndex.begin(); itmb!=boundaryIndex.end(); ++itmb)
    {
      std::vector<number_t> vns=itmb->second.first->vertexNumbers(itmb->second.second);
      for (itn=vns.begin(); itn!=vns.end(); ++itn) vBoundaryIndex.insert(*itn);
    }
    boundaryIndex.clear();

    //build list 1 of elements on one side of the crack
    GeomElement* elt0=nullptr;
    for (its=elts.begin(); its!=elts.end() && elt0==nullptr ; ++its)
      if ((*its)->flag > d) elt0=*its;
    if (elt0==nullptr) //no element intesects cdom by a side, degenerated crack
    {
      warning("free_warning","nothing to crack, check your domain");
      trace_p->pop();
      return *this;
    }
    elts.erase(elt0);
    Node<GeomElement> tree1(elt0,0,0);  //root node 1
    addElts(&tree1, elts, vns, sideIndex, vBoundaryIndex, cdomIndex, d); //see function for details

    //build list 2 of elements on the other side of the crack
    elt0=nullptr;
    for (its=elts.begin(); its!=elts.end() && elt0==nullptr ; ++its)
      if ((*its)->flag > d) elt0=*its;
    if (elt0==nullptr) //no element intesects cdom by a side, other side is "void"
    {
      warning("free_warning","second side of crack seems to be void, no crack generated");
      trace_p->pop();
      return *this;
    }
    elts.erase(elt0);
    Node<GeomElement> tree2(elt0,0,0);  //root node 2
    addElts(&tree2, elts, vns, sideIndex, vBoundaryIndex, cdomIndex, d); //see function for details

    //move tree1, tree2 to elts1, elts2 set
    elts1=treeToSet(tree1); elts2=treeToSet(tree2);
  }

  //deal with crack types
  std::set<number_t> vSideCrack1, vSideCrack2;
  if (sideCrack1!=nullptr) vSideCrack1=sideCrack1->meshDomain()->vertexNumbers();
  // if (sideCrack2!=nullptr) vSideCrack2=sideCrack2->meshDomain()->vertexNumbers();
  if (cracktype1==_closedCrack) {removeElts(elts1, vSideCrack1); removeElts(elts2, vSideCrack1);}
  // if (cracktype2==_closedCrack) {removeElts(elts1, vSideCrack2); removeElts(elts2, vSideCrack2);}
  if (cracktype1==_openCrack && sideCrack1!=nullptr && elts.size()>0) addElts(elts, elts1, elts2, vSideCrack1);
  // if (cracktype2==_openCrack && elts.size()>0) addElts(elts, elts1, elts2, vSideCrack2);

  if (withPicture) pictureCrack(elts1,elts2,"pictureCrack");

  //duplicating mesh nodes of cdom
  std::set<number_t> cnodes = mdom->nodeNumbers();
  std::map<number_t,number_t> renum;
  number_t nbn=nodes.size();
  nodes.resize(nbn+cnodes.size()); //invalidate node pointers in MeshElement, so update them
  std::set<number_t>::iterator itc=cnodes.begin();
  for (; itc!=cnodes.end(); ++itc, nbn++) {nodes[nbn]=nodes[*itc-1]; renum[*itc]=nbn+1;}
  std::set<number_t> cvertices = mdom->vertexNumbers();
  nbn=vertices_.size();
  vertices_.resize(nbn+cvertices.size());
  for (itc=cvertices.begin(); itc!=cvertices.end(); ++itc, nbn++) {vertices_[nbn]=renum[*itc-1];}

  //create new side domain 2
  string_t na=crackName2;
  if (na=="") na=cdom.name()+"-";
  GeomDomain* cdom2 =new GeomDomain(*this, na, cdom.dim(), "crack side of "+cdom.name(), cdom.geometry());
  domains_.push_back(cdom2);
  MeshDomain* mdom2 = cdom2->meshDomain();
  mdom2->shapeTypes=mdom->shapeTypes;
  mdom->dualCrackDomain_p=mdom2;  mdom2->dualCrackDomain_p=mdom;
  mdom2->geomElements.resize(mdom->geomElements.size());
  std::vector<GeomElement*>::iterator itg2=mdom2->geomElements.begin();
  std::set<GeomElement*>::iterator itgs = elts2.begin(), itgse=elts2.end();
  lastIndex_++;
  for (itg=mdom->geomElements.begin(); itg!=mdom->geomElements.end(); ++itg, ++itg2)
  {
    *itg2 = new GeomElement(**itg); //copy original
    (*itg2)->deleteMeshElement();
    (*itg2)->twin_p=*itg; (*itg)->twin_p=*itg2;
    (*itg2)->number()=lastIndex_++;
    std::vector<GeoNumPair>& pars=(*itg)->parentSides();
    string_t sv=(*itg)->encodeElement();
    itm=sideIndex.find(sv);
    GeomElement* elt1=itm->second.first, *elt2 = itm->second.second;
    if (elts2.find(elt2)==itgse) {elt2=elt1; elt1=itm->second.second;}
    number_t s1,s2;
    if (pars[0].first==elt1) {s1=pars[0].second; s2=pars[1].second;}
    else {s1=pars[1].second; s2=pars[0].second;}
    pars = std::vector<GeoNumPair>(1,GeoNumPair(elt1,s1));
    (*itg2)->parentSides()=std::vector<GeoNumPair>(1,GeoNumPair(elt2,s2));
  }

  //update nodenumbers, vertexnumbers and node pointers of elements of elts2
  itne=cnodes.end();
  std::set<number_t>::iterator itve=vns.end();
  for (itgs = elts2.begin(); itgs!=itgse; ++itgs)
  {
    MeshElement* melt = (*itgs)->meshElement();
    number_t k=0;
    for (itn=melt->nodeNumbers.begin(); itn!=melt->nodeNumbers.end(); ++itn, k++)
    {
      if (cnodes.find(*itn)!=itne)
      {
        *itn=renum[*itn];
        melt->nodes[k]=&nodes[*itn-1];
      }
    }
    for (itn=melt->vertexNumbers.begin(); itn!=melt->vertexNumbers.end(); ++itn)
      if (vns.find(*itn)!=itve) *itn=renum[*itn];
  }
  updateNodePointers(true);

  //change mesh name and rename cdom
  if (name_=="") name_="?";
  name_+= " with "+cdom.name()+" cracked";
  comment_+=" with "+cdom.name()+" cracked";
  na=crackName1;
  if (na=="") na=cdom.name()+"+";
  cdom.rename(na);
  // choose side +
  if (P1!=nullptr)
  {
    //locate element containing P1, find if it belongs to elts1
    bool found=false;
    for (it=elts1.begin(); it!=elts.end() && !found; ++it)
      if ((*it)->contains(*P1)) found = true;
    if (!found) //exchange crack side domains
    {
      na=cdom2->name();
      cdom2->rename(cdom.name());
      cdom.rename(na);
    }
  }

  trace_p->pop();
  return *this;
}

//! user shortcut to crack one domain
Mesh& Mesh::closedCrack(GeomDomain& cdom) { return this->crack(cdom); }

//! user shortcut to crack two domains
Mesh& Mesh::closedCrack(GeomDomain& cdom1, GeomDomain& cdom2)
{
  this->crack(cdom1);
  this->crack(cdom2);
  return *this;
}

//! user shortcut to crack three domains
Mesh& Mesh::closedCrack(GeomDomain& cdom1, GeomDomain& cdom2, GeomDomain& cdom3)
{
  this->crack(cdom1);
  this->crack(cdom2);
  this->crack(cdom3);
  return *this;
}

//! user shortcut to crack four domains
Mesh& Mesh::closedCrack(GeomDomain& cdom1, GeomDomain& cdom2, GeomDomain& cdom3, GeomDomain& cdom4)
{
  this->crack(cdom1);
  this->crack(cdom2);
  this->crack(cdom3);
  this->crack(cdom4);
  return *this;
}

//! user shortcut to crack five domains
Mesh& Mesh::closedCrack(GeomDomain& cdom1, GeomDomain& cdom2, GeomDomain& cdom3, GeomDomain& cdom4, GeomDomain& cdom5)
{
  this->crack(cdom1);
  this->crack(cdom2);
  this->crack(cdom3);
  this->crack(cdom4);
  this->crack(cdom5);
  return *this;
}

//! user shortcut to crack six domains
Mesh& Mesh::closedCrack(GeomDomain& cdom1, GeomDomain& cdom2, GeomDomain& cdom3, GeomDomain& cdom4, GeomDomain& cdom5, GeomDomain& cdom6)
{
  this->crack(cdom1);
  this->crack(cdom2);
  this->crack(cdom3);
  this->crack(cdom4);
  this->crack(cdom5);
  this->crack(cdom6);
  return *this;
}

//! user shortcut to crack seven domains
Mesh& Mesh::closedCrack(GeomDomain& cdom1, GeomDomain& cdom2, GeomDomain& cdom3, GeomDomain& cdom4, GeomDomain& cdom5, GeomDomain& cdom6,
                        GeomDomain& cdom7)
{
  this->crack(cdom1);
  this->crack(cdom2);
  this->crack(cdom3);
  this->crack(cdom4);
  this->crack(cdom5);
  this->crack(cdom6);
  this->crack(cdom7);
  return *this;
}

//! user shortcut to crack one domain with a point
Mesh& Mesh::closedCrack(GeomDomain& cdom, const Point& p) { return this->crack(cdom, p); }

//! user shortcut to crack two domains with a point
Mesh& Mesh::closedCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& cdom2, const Point& p2)
{
  this->crack(cdom1, p1);
  this->crack(cdom2, p2);
  return *this;
}

//! user shortcut to crack three domains with a point
Mesh& Mesh::closedCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& cdom2, const Point& p2, GeomDomain& cdom3, const Point& p3)
{
  this->crack(cdom1, p1);
  this->crack(cdom2, p2);
  this->crack(cdom3, p3);
  return *this;
}

//! user shortcut to crack four domains with a point
Mesh& Mesh::closedCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& cdom2, const Point& p2, GeomDomain& cdom3, const Point& p3,
                        GeomDomain& cdom4, const Point& p4)
{
  this->crack(cdom1, p1);
  this->crack(cdom2, p2);
  this->crack(cdom3, p3);
  this->crack(cdom4, p4);
  return *this;
}

//! user shortcut to crack five domains with a point
Mesh& Mesh::closedCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& cdom2, const Point& p2, GeomDomain& cdom3, const Point& p3,
                        GeomDomain& cdom4, const Point& p4, GeomDomain& cdom5, const Point& p5)
{
  this->crack(cdom1, p1);
  this->crack(cdom2, p2);
  this->crack(cdom3, p3);
  this->crack(cdom4, p4);
  this->crack(cdom5, p5);
  return *this;
}

//! user shortcut to crack six domains with a point
Mesh& Mesh::closedCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& cdom2, const Point& p2, GeomDomain& cdom3, const Point& p3,
                        GeomDomain& cdom4, const Point& p4, GeomDomain& cdom5, const Point& p5, GeomDomain& cdom6, const Point& p6)
{
  this->crack(cdom1, p1);
  this->crack(cdom2, p2);
  this->crack(cdom3, p3);
  this->crack(cdom4, p4);
  this->crack(cdom5, p5);
  this->crack(cdom6, p6);
  return *this;
}

//! user shortcut to crack seven domains with a point
Mesh& Mesh::closedCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& cdom2, const Point& p2, GeomDomain& cdom3, const Point& p3,
                        GeomDomain& cdom4, const Point& p4, GeomDomain& cdom5, const Point& p5, GeomDomain& cdom6, const Point& p6,
                        GeomDomain& cdom7, const Point& p7)
{
  this->crack(cdom1, p1);
  this->crack(cdom2, p2);
  this->crack(cdom3, p3);
  this->crack(cdom4, p4);
  this->crack(cdom5, p5);
  this->crack(cdom6, p6);
  this->crack(cdom7, p7);
  return *this;
}

//! user shortcut to crack one domain with an open boundary
Mesh& Mesh::openCrack(GeomDomain& cdom, GeomDomain& sideDom) { return this->crack(cdom, _openCrack, sideDom); }

//! user shortcut to crack two domains with an open boundary
Mesh& Mesh::openCrack(GeomDomain& cdom1, GeomDomain& sideDom1, GeomDomain& cdom2, GeomDomain& sideDom2)
{
  this->crack(cdom1, _openCrack, sideDom1);
  this->crack(cdom2, _openCrack, sideDom2);
  return *this;
}

//! user shortcut to crack three domains with an open boundary
Mesh& Mesh::openCrack(GeomDomain& cdom1, GeomDomain& sideDom1, GeomDomain& cdom2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, GeomDomain& sideDom3)
{
  this->crack(cdom1, _openCrack, sideDom1);
  this->crack(cdom2, _openCrack, sideDom2);
  this->crack(cdom3, _openCrack, sideDom3);
  return *this;
}

//! user shortcut to crack four domains with an open boundary
Mesh& Mesh::openCrack(GeomDomain& cdom1, GeomDomain& sideDom1, GeomDomain& cdom2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, GeomDomain& sideDom3, GeomDomain& cdom4, GeomDomain& sideDom4)
{
  this->crack(cdom1, _openCrack, sideDom1);
  this->crack(cdom2, _openCrack, sideDom2);
  this->crack(cdom3, _openCrack, sideDom3);
  this->crack(cdom4, _openCrack, sideDom4);
  return *this;
}

//! user shortcut to crack five domains with an open boundary
Mesh& Mesh::openCrack(GeomDomain& cdom1, GeomDomain& sideDom1, GeomDomain& cdom2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, GeomDomain& sideDom3, GeomDomain& cdom4, GeomDomain& sideDom4,
                      GeomDomain& cdom5, GeomDomain& sideDom5)
{
  this->crack(cdom1, _openCrack, sideDom1);
  this->crack(cdom2, _openCrack, sideDom2);
  this->crack(cdom3, _openCrack, sideDom3);
  this->crack(cdom4, _openCrack, sideDom4);
  this->crack(cdom5, _openCrack, sideDom5);
  return *this;
}

//! user shortcut to crack six domains with an open boundary
Mesh& Mesh::openCrack(GeomDomain& cdom1, GeomDomain& sideDom1, GeomDomain& cdom2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, GeomDomain& sideDom3, GeomDomain& cdom4, GeomDomain& sideDom4,
                      GeomDomain& cdom5, GeomDomain& sideDom5, GeomDomain& cdom6, GeomDomain& sideDom6)
{
  this->crack(cdom1, _openCrack, sideDom1);
  this->crack(cdom2, _openCrack, sideDom2);
  this->crack(cdom3, _openCrack, sideDom3);
  this->crack(cdom4, _openCrack, sideDom4);
  this->crack(cdom5, _openCrack, sideDom5);
  this->crack(cdom6, _openCrack, sideDom6);
  return *this;
}

//! user shortcut to crack seven domains with an open boundary
Mesh& Mesh::openCrack(GeomDomain& cdom1, GeomDomain& sideDom1, GeomDomain& cdom2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, GeomDomain& sideDom3, GeomDomain& cdom4, GeomDomain& sideDom4,
                      GeomDomain& cdom5, GeomDomain& sideDom5, GeomDomain& cdom6, GeomDomain& sideDom6,
                      GeomDomain& cdom7, GeomDomain& sideDom7)
{
  this->crack(cdom1, _openCrack, sideDom1);
  this->crack(cdom2, _openCrack, sideDom2);
  this->crack(cdom3, _openCrack, sideDom3);
  this->crack(cdom4, _openCrack, sideDom4);
  this->crack(cdom5, _openCrack, sideDom5);
  this->crack(cdom6, _openCrack, sideDom6);
  this->crack(cdom7, _openCrack, sideDom7);
  return *this;
}

//! user shortcut to crack one domain with an open boundary and a point
Mesh& Mesh::openCrack(GeomDomain& cdom, const Point& p, GeomDomain& sideDom) { return this->crack(cdom, _openCrack, sideDom, p); }

//! user shortcut to crack two domains with an open boundary and a point
Mesh& Mesh::openCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& sideDom1, GeomDomain& cdom2, const Point& p2, GeomDomain& sideDom2)
{
  this->crack(cdom1, _openCrack, sideDom1, p1);
  this->crack(cdom2, _openCrack, sideDom2, p2);
  return *this;
}

//! user shortcut to crack three domains with an open boundary and a point
Mesh& Mesh::openCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& sideDom1, GeomDomain& cdom2, const Point& p2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, const Point& p3, GeomDomain& sideDom3)
{
  this->crack(cdom1, _openCrack, sideDom1, p1);
  this->crack(cdom2, _openCrack, sideDom2, p2);
  this->crack(cdom3, _openCrack, sideDom3, p3);
  return *this;
}

//! user shortcut to crack four domains with an open boundary and a point
Mesh& Mesh::openCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& sideDom1, GeomDomain& cdom2, const Point& p2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, const Point& p3, GeomDomain& sideDom3, GeomDomain& cdom4, const Point& p4, GeomDomain& sideDom4)
{
  this->crack(cdom1, _openCrack, sideDom1, p1);
  this->crack(cdom2, _openCrack, sideDom2, p2);
  this->crack(cdom3, _openCrack, sideDom3, p3);
  this->crack(cdom4, _openCrack, sideDom4, p4);
  return *this;
}

//! user shortcut to crack five domains with an open boundary and a point
Mesh& Mesh::openCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& sideDom1, GeomDomain& cdom2, const Point& p2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, const Point& p3, GeomDomain& sideDom3, GeomDomain& cdom4, const Point& p4, GeomDomain& sideDom4,
                      GeomDomain& cdom5, const Point& p5, GeomDomain& sideDom5)
{
  this->crack(cdom1, _openCrack, sideDom1, p1);
  this->crack(cdom2, _openCrack, sideDom2, p2);
  this->crack(cdom3, _openCrack, sideDom3, p3);
  this->crack(cdom4, _openCrack, sideDom4, p4);
  this->crack(cdom5, _openCrack, sideDom5, p5);
  return *this;
}

//! user shortcut to crack six domains with an open boundary and a point
Mesh& Mesh::openCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& sideDom1, GeomDomain& cdom2, const Point& p2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, const Point& p3, GeomDomain& sideDom3, GeomDomain& cdom4, const Point& p4, GeomDomain& sideDom4,
                      GeomDomain& cdom5, const Point& p5, GeomDomain& sideDom5, GeomDomain& cdom6, const Point& p6, GeomDomain& sideDom6)
{
  this->crack(cdom1, _openCrack, sideDom1, p1);
  this->crack(cdom2, _openCrack, sideDom2, p2);
  this->crack(cdom3, _openCrack, sideDom3, p3);
  this->crack(cdom4, _openCrack, sideDom4, p4);
  this->crack(cdom5, _openCrack, sideDom5, p5);
  this->crack(cdom6, _openCrack, sideDom6, p6);
  return *this;
}

//! user shortcut to crack seven domains with an open boundary and a point
Mesh& Mesh::openCrack(GeomDomain& cdom1, const Point& p1, GeomDomain& sideDom1, GeomDomain& cdom2, const Point& p2, GeomDomain& sideDom2,
                      GeomDomain& cdom3, const Point& p3, GeomDomain& sideDom3, GeomDomain& cdom4, const Point& p4, GeomDomain& sideDom4,
                      GeomDomain& cdom5, const Point& p5, GeomDomain& sideDom5, GeomDomain& cdom6, const Point& p6, GeomDomain& sideDom6,
                      GeomDomain& cdom7, const Point& p7, GeomDomain& sideDom7)
{
  this->crack(cdom1, _openCrack, sideDom1, p1);
  this->crack(cdom2, _openCrack, sideDom2, p2);
  this->crack(cdom3, _openCrack, sideDom3, p3);
  this->crack(cdom4, _openCrack, sideDom4, p4);
  this->crack(cdom5, _openCrack, sideDom5, p5);
  this->crack(cdom6, _openCrack, sideDom6, p6);
  this->crack(cdom7, _openCrack, sideDom7, p7);
  return *this;
}

//------------------------------------------------------------------------------------------------------------
//                                  merge stuff
//------------------------------------------------------------------------------------------------------------
Mesh merge(const std::vector<const Mesh*>& ms, bool mergeSharedBoundary, const string_t& nam)
{
  if(ms.size()==0) // nothing to merge
  {
    error("free_error","merge(vector<const Mesh*>,...), nothing to merge");
    return Mesh();
  }
  trace_p->push("merge(vector<const Mesh*>,...)");
  Mesh mm;
  mm.name(nam);
  for (auto& m:ms)
      mm.merge(*m,mergeSharedBoundary);
  trace_p->pop();
  return mm;
}

/*--------------------------------------------------------------------------------------------
  print mesh data
 ---------------------------------------------------------------------------------------------*/
void Mesh::printInfo(std::ostream& os) const
{
  os << "Mesh" << " '" << name_ << "'";        //name
  if (comment_ != "") { os << " (" << comment_ << ")"; }   //comment
  os << "\n";
  if (nbOfElements() == 0) { os << " This mesh contains no element.\n"; return; }

  os << "  " << "space dimension" << ": " << spaceDim() << ", "
     << "element dimension" << ": " << meshDim() << "\n";        //global information
  os << "  " << *geometry_p;
  os << "  " << "number of elements" << ": " << nbOfElements() << ", "
     << "number of vertices" << ": " << nbOfVertices() << ", "
     << "number of nodes" << ": " << nbOfNodes() << ", "
     << "number of domains" << ": " << nbOfDomains();
  for (number_t i = 0; i < domains_.size(); i++)
  {
    os << "\n    domain number " << i << ": " <<  domains_[i]->name() << " of dimension " << domains_[i]->dim()
        << ", made of " << domains_[i]->numberOfElements() << " elements, ";
    if (domains_[i]->geometry()!=nullptr)
    {
      if (domains_[i]->geometry()->isCanonical()) os<<"canonical "; else os<<"composite ";
      os<<"geometry "<<domains_[i]->geometry()->domName();
      if (domains_[i]->geometry()->parametrizationP()==nullptr) os<<" (no parametrization) ";
      else os<<" (parametrized) ";
    }
    os << "(";
    if (domains_[i]->description().empty()) { os << "no description available"; }
    else                                    { os << domains_[i]->description(); }
    os << ")";
  }
  os << "\n";
}

void Mesh::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  printInfo(os);

  if (theVerboseLevel < 3) { return; }

  //list of elements, vertices, ... and nodes up to the verboselevel
  number_t s = elements_.size();
  os << words("list of") << " " << words("elements") << " (" << s << "):";
  number_t tvl = theVerboseLevel, n = std::min(tvl, s);
  verboseLevel(3);
  for (number_t i = 0; i < n; i++)
    { os << std::endl << (*elements_[i]); }
  if (n < s)      { os << "\n...\n" << (*elements_[s - 1]) ; }
  verboseLevel(tvl);

  s = vertices_.size();
  os << std::endl << words("list of") << " " << words("vertices") << " (" << s << "):";
  n = std::min(tvl, s);
  for (number_t i = 0; i < n; i++)
    { os << "\n  " << vertices_[i] << " -> " << nodes[vertices_[i] - 1]; }
  if (n < s) { os << "\n  ...\n  " << s << " -> " << nodes[vertices_[s - 1] - 1]; }
  verboseLevel(tvl);

  s = nodes.size();
  os << std::endl << words("list of") << " " << words("nodes") << " (" << s << "):";
  n = std::min(tvl, s);
  for (number_t i = 0; i < n; i++)  { os << "\n  " << i + 1 << " -> " << nodes[i]; }
  if (n < s) { os << "\n  ...\n  " << s << " -> " << nodes[s - 1]; }
  verboseLevel(tvl);

  s = sides_.size();
  os << std::endl << words("list of") << " " << words("sides") << " (" << s << "):";
  if (s == 0) { os << " unset"; }
  else
  {
    tvl = theVerboseLevel;
    n = std::min(tvl, s);
    verboseLevel(1);
    for (number_t i = 0; i < n; i++)  { os << "\n  " << words("side") << " " << i + 1 << " -> " << (*sides_[i]); }
    if (n < s) { os << "\n  ...\n  " << words("side") << " " << s << " -> " << (*sides_[s - 1]); }
    verboseLevel(tvl);
  }

  s = sideOfSides_.size();
  os << std::endl << words("list of") << " " << words("side of sides") << " (" << s << "):";
  if (s == 0) { os << " unset"; }
  else
  {
    n = std::min(tvl, s);
    for (number_t i = 0; i < n; i++)  { os << "\n  " << words("side of side") << " " << i + 1 << " -> " << (*sideOfSides_[i]); }
    if (n < s) { os << "\n  ...\n  " << words("side of side") << " " << s << " -> " << (*sideOfSides_[s - 1]); }
  }

  s = domains_.size();
  os << std::endl << words("list of") << " " << words("domains") << " (" << s << "):";
  for (number_t i = 0; i < s; i++)
    { os << std::endl << (*domains_[i]); }

  s = vertexElements_.size();
  os << std::endl << words("list of") << " " << words("elements") << " " << words("by vertex") << " (" << s << "):";
  if (s == 0) { os << " unset"; }
  else
  {
    n = std::min(tvl, s);
    for (number_t i = 0; i < n; i++)
    {
      os << "\n  " << words("vertex") << " " << vertices_[i] << ": ";
      for (number_t k = 0; k < vertexElements_[i].size(); k++)
        { os << " " << vertexElements_[i][k].first->number(); }
    }
    if (n < s)
    {
      os << "\n  ...\n" << "  " << words("vertex") << " " << vertices_[s - 1] << ": ";
      for (number_t k = 0; k < vertexElements_[s - 1].size(); k++)
        { os << " " << vertexElements_[s - 1][k].first->number(); }
    }
  }
}

std::ostream& operator<<(std::ostream& os, const Mesh& m)
{
  m.print(os);
  return os;
}

void Mesh::addSuffix(const string_t& s)
{
  name_+="_"+s;
  geometry_p->addSuffix(s);
  for (number_t i = 0; i < domains_.size(); ++i) { if (domains_[i]->name().find("#") != 0) {domains_[i]->addSuffix(s);} }
}

//!< compute mesh center (barycenter of  nodes)
Point Mesh::center() const
{
   auto itp=nodes.begin();
   Point P=*itp; itp++;
   for(;itp!=nodes.end();++itp) P+=*itp;
   P/=nodes.size();
   return P;
}

//! adaptative domains for levelset methods
void Mesh::adaptDomains(number_t mainDomNum, const std::vector<number_t>& elementsColorMap)
{
  // the domain number is the index in Mesh::domains_
  // nodesColorMap affects a domain number to each node of a mesh, even side domains

  // 1. we detect which domains are plain domains
  // In the meantime, we build list of element numbers
  std::vector<number_t> plainDomains, sideDomains;
  std::map<number_t, std::set<number_t> > eltNumsPerDomains;

  for (number_t d=0; d < domains_.size(); ++d)
  {
    if (domains_[d]->meshDomain()->isSideDomain()) { sideDomains.push_back(d); }
    else { plainDomains.push_back(d); }
    for (number_t e=0; e < domains_[d]->meshDomain()->numberOfElements(); ++e)
    {
      eltNumsPerDomains[d].insert(domains_[d]->meshDomain()->element(e+1)->number());
    }
  }

  // 2. we build additional data between domains

  // 2.a we build parent domains of side domains
  // it is enough to determine domains containing parents of the first element
  // so we have to update parents management
  updateSideDomains();
  std::map<number_t, std::set<number_t> > parentSideDomains;
  std::map<number_t, std::set<number_t> > boundaryPlainDomains;

  for (number_t sd=0; sd < sideDomains.size(); ++sd)
  {
    number_t sdomId=sideDomains[sd];
    GeomElement* gelt=domains_[sdomId]->meshDomain()->element(1);
    std::vector<GeoNumPair>& parents=gelt->parentSides();
    std::vector<number_t> parentEltNums;
    for (number_t p=0; p < parents.size(); ++p) { parentEltNums.push_back(parents[p].first->number()); }
    for (number_t pe=0; pe < parentEltNums.size(); ++pe)
    {
      for (number_t pd=0; pd < plainDomains.size(); ++pd)
      {
        number_t pdomId=plainDomains[pd];
        if (eltNumsPerDomains[pdomId].count(parentEltNums[pe])) { parentSideDomains[sdomId].insert(pdomId); }
      }
    }
  }

  // 2.b we build side domains of plain domains
  // we use previous data and reverse it
  std::map<number_t, std::set<number_t> >::const_iterator it_psd;
  for (it_psd=parentSideDomains.begin(); it_psd != parentSideDomains.end(); ++it_psd)
  {
    std::set<number_t>::const_iterator it;
    for (it=it_psd->second.begin(); it != it_psd->second.end(); ++it)
    {
      boundaryPlainDomains[*it].insert(it_psd->first);
    }
  }

  // 3. we update plain domains by regenerating elements lists
  std::map<number_t,std::vector<GeomElement*> > eltsByDom;

  // 3.a we first build list of elements per plain domain
  // as number of plain elements in mesh does not change,
  // plain elements numbering is left unchanged
  for (number_t e=0; e < elementsColorMap.size(); ++e)
  {
    eltsByDom[elementsColorMap[e]].push_back(elements_[e]);
  }

  // 3.b we update plain domains
  for (number_t d=0; d < plainDomains.size(); ++d)
  {
    number_t domId=plainDomains[d];
    std::vector<GeomElement*>& v_ge =domains_[domId]->meshDomain()->geomElements;
    v_ge.resize(eltsByDom[domId].size());
    for (number_t e=0; e < eltsByDom[d].size(); ++e) { v_ge[e]=eltsByDom[domId][e]; }
  }

  // 4 we update side domains by regenerating elements lists

  // 4.a first, we get available numbering for side elements
  // To do so, we need the max index used and to keep numbers for plain elements and
  // for side elements that are not on interfaces between plain domains
  std::map<number_t, std::pair<number_t, number_t> > eltRangePerDomains;
  std::map<number_t, std::set<number_t> >::const_iterator it_enpd;
  for (it_enpd=eltNumsPerDomains.begin(); it_enpd != eltNumsPerDomains.end(); ++it_enpd)
  {
    eltRangePerDomains[it_enpd->first]=std::make_pair(*it_enpd->second.begin(), *it_enpd->second.rbegin());
  }

  number_t maxNum=0;
  std::map<number_t, std::pair<number_t, number_t> >::const_iterator it_erpd;
  for (it_erpd=eltRangePerDomains.begin(); it_erpd != eltRangePerDomains.end(); ++it_erpd)
  {
    if (it_erpd->second.second > maxNum) { maxNum = it_erpd->second.second; }
  }

  std::set<number_t> availableNumberingSet;
  for (number_t i=0; i < maxNum; ++i)
  {
    availableNumberingSet.insert(i+1);
  }

  for (it_enpd=eltNumsPerDomains.begin(); it_enpd != eltNumsPerDomains.end(); ++it_enpd)
  {
    std::set<number_t>::const_iterator it_en;
    if (!(parentSideDomains.count(it_enpd->first) && parentSideDomains[it_enpd->first].size() > 1))
    {
      for (it_en=it_enpd->second.begin(); it_en != it_enpd->second.end(); ++it_en)
      {
        availableNumberingSet.erase(*it_en);
      }
    }
  }

  std::vector<number_t> availableNumbering(availableNumberingSet.begin(), availableNumberingSet.end());

  // 4.b then, we compute side domain of every internal plain domain
  // an internal plain domain is a domain that has only shared boundaries
  // in only one domain
  std::map<number_t, std::vector<GeoNumPair> > sideElts;
  for (number_t pd=0; pd < plainDomains.size(); ++pd)
  {
    number_t pdomNum=plainDomains[pd];
    if (pdomNum != mainDomNum)
    {
      // domain number of boundary
      // in this case boundaryPlainDomains[pdomNum].size() == 1
      number_t sDomNum=*boundaryPlainDomains[pdomNum].begin();
      std::map<string_t,std::vector<GeoNumPair> > sideIndex;
      std::map<string_t,std::vector<GeoNumPair> >::const_iterator it;
      xlifepp::createSideIndex(domains_[pdomNum]->meshDomain()->geomElements, sideIndex);
      for (it=sideIndex.begin(); it != sideIndex.end(); ++it)
      {
        if (it->second.size() == 1)   // side element is not shared => it is a boundary element
        {
          sideElts[sDomNum].push_back(it->second[0]);
        }
      }
    }
  }

  // 4.c we update side domains
  std::map<number_t, std::vector<GeoNumPair> >::const_iterator it_se;
  for (it_se=sideElts.begin(); it_se != sideElts.end(); ++it_se)
  {
    number_t sdomNum=it_se->first;
    // we have to clean list of elements of domain
    std::vector<GeomElement*>& v_ge =domains_[sdomNum]->meshDomain()->geomElements;
    for (std::vector<GeomElement*>::iterator it = v_ge.begin(); it != v_ge.end(); it++)
      if (*it != nullptr) delete *it;

    // we build the new list by using available numbering
    v_ge.resize(it_se->second.size());
    for (number_t e=0; e < it_se->second.size(); ++e)
    {
      number_t eltNum;
      if (e < availableNumbering.size()) { eltNum=availableNumbering[e]; }
      else
      {
        eltNum=maxNum++;
      }
      v_ge[e]=new GeomElement(it_se->second[e].first, it_se->second[e].second,eltNum);
    }
  }
}

//! construct a 2d/3d extruded mesh from a 1d/2d section mesh with quadrangle in 2D, with prism in 3D
/*
  ms: mesh of the section
  O: section origin
  D: section end       (direction of extrusion is OD)
  nbl: number of layers
  namingDomain, namingSection, namingSide:  0 means no naming, 1 means unique name, >1 means each intern domain/section/side is named

  domain names are inherited from section domain names

  for instance, assume the following mesh of a rectangle
                          Gamma
                    -----------------
                    |       |       |
            SigmaP  | Omg1  |  Omg2 | SigmaM
                    |       |       |
                    -----------------
                          Gamma

  namingDomain=0  will produce only the full 3d domain "Omega"
  namingDomain=1  will produce the volumic domains: "Omg1_e", "Omg2_e"
  namingDomain=2  will produce the volumic domains: "Omg1_e1","Omg1_e2", ..., "Omg1_en", "Omg2_e1","Omg2_e2",..., "Omg2_en"
  namingSection=0 will not produce any section domains
  namingSection=1 will produce the section domains: "Omg1_0", "Omg2_0" (section 0), "Omg1_n", "Omg2_n" (section n)
  namingSection=2 will produce the section domains: "Omg1_0", "Omg2_0" (section 0), "Omg1_2", "Omg2_2" (section 2),..., "Omg1_n", "Omg2_n" (section n)
  namingSide=0    will not produce any side domains
  namingSide=1    will produce the side domains: "Gamma_e", "SigmaP_e, "SigmaM_e"
  namingSide=2    will produce the side domains: "Gamma_e1","Gamma_e2", ..., "Gamma_en", "SigmaM_e1", "SigmaM_e2", ..., "SigmaM_en", "SigmaP_e1", "SigmaP_e2", ..., "SigmaP_en"

  Default name for volumic domain is "Omega", for section domain is "Section"
  If section has no side domain defined, the extruded mesh will not have any side domains
  SideOfSide domain in 3D are not yet managed

  Remarks:
    Only one order mesh are currently extruded
    nbelt_section * n extruded elements are ordered by layer
*/
void Mesh::buildExtrusion(const Mesh& ms, const ExtrusionData& extdata)
{
  trace_p->push("Mesh::buildExtrusion(Mesh, ExtrusionData)");
  dimen_t dim_mesh=ms.meshDim();
  if (dim_mesh!=1 && dim_mesh!=2) error("dim_not_in_range", 1, 2);
  if (name_=="") name_=ms.name_+"_extrusion";
  number_t nbl=extdata.layers();
  comment_="extrusion of "+ms.name_+" using "+tostring(nbl)+" layers";
  isMadeOfSimplices_=false;
  order_=1;
  lastIndex_=0;
  firstOrderMesh_p=this;
  geometry_p=nullptr;
  number_t nbelt_s=ms.nbOfElements();
  number_t nbver_s=ms.nbOfVertices();
  elements_.resize(nbelt_s*nbl);
  nodes.resize(nbver_s*(nbl+1));
  dimen_t dim=std::max(dimen_t(dim_mesh+1),ms.spaceDim());
  // build nodes using extrusion data
  number_t m=0;
  const Transformation* tr=extdata.initialTransf();
  //starting section
  if (tr==nullptr)
  {
    for (auto itv=ms.vertices_.begin(); itv!=ms.vertices_.end(); ++itv, m++)
    {
      nodes[m]=Point(ms.nodes[*itv-1],dim);
    }
  }
  else // move the first section
  {
    for (auto itv=ms.vertices_.begin(); itv!=ms.vertices_.end(); ++itv, m++)
    {
      nodes[m]=tr->apply(Point(ms.nodes[*itv-1],dim));
    }
  }
  if (extdata.transformations().size()>0) // use list of transformations
  {
    number_t n=0;
    auto it=extdata.transformations().begin();
    std::vector<number_t>::const_iterator itv;
    for (number_t k=1; k<=nbl; k++, ++it)
    {
      for (auto j=0;j<nbver_s; j++, m++, n++)
      {
        nodes[m]=(*it)->apply(nodes[n]);
      }
    }
  }
  else if (extdata.extrusion()!=nullptr) //use Transformation passing from one section to the next
  {
    number_t n=0;
    std::vector<number_t>::const_iterator itv;
    const Transformation& tr = *extdata.extrusion();
    for (number_t k=1; k<=nbl; k++)
    {
      for (auto j=0;j<nbver_s; j++, m++, n++)
      {
        nodes[m]=tr.apply(nodes[n]);
      }
    }
  }
  else if (extdata.fun()!=nullptr) //use a parametrization of the extrusion M+f(k)
  {
    Parameters pars;
    real_t d, dt=1./nbl, a;
    par_fun f=extdata.fun();
    number_t dimf=f(Point(0.),pars,_id).size();
    Point C0 = extdata.center(), Q;
    if (C0.size()==0) C0 = ms.center();
    const MeshDomain& mdom = *ms.domain(0).meshDomain();
    if (!mdom.orientationComputed) mdom.setNormalOrientation();
    GeomElement* gelt = mdom.nearest(C0,d);
    if (gelt==nullptr) error("free_error","abnormal failure in buildExtrusion, gelt not localized!");
    for (number_t i = C0.size(); i < dimf; ++i) { C0.push_back(0.);}  // align dimension
    std::vector<real_t> n0=gelt->normalVector(), dQ;
    if (tr!=nullptr)
    {
      C0=tr->apply(C0);
      n0=tr->mat()*Vector<real_t>(n0);
    }
    for (number_t k=1; k<=nbl; k++)
    {
      Q=f(Point(k*dt),pars,_id);
      dQ=f(Point(k*dt),pars,_dt);
      auto d=crossProduct(n0,dQ);
      a=std::acos(dot(n0,dQ)/(norm(n0)*norm(dQ))); // angle between [0,pi]
      if (std::abs(a)>theTolerance && norm(d)>theTolerance) // apply rotation
      {
        Rotation3d Rk(_center=C0+Q,_axis=d,_angle=a);
        for (auto j=0;j<nbver_s; j++, m++)
        {
          nodes[m]=Rk.apply(nodes[j]+Q);
        }
      }
      else // no rotation
      {
        for (auto j=0;j<nbver_s; j++, m++)
        {
          nodes[m]=nodes[j]+Q;
        }
      }
    }
  }
  else error("free_error","no extrusion transformation found in buildExtrusion");

  //construct vertex indices (begins at 1), assume first order mesh
  vertices_.resize(nodes.size());
  for (number_t i = 0; i < nodes.size(); i++) { vertices_[i] = i + 1; }

  //create elements (ordered by section)
  ShapeType sh=_segment;
  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  RefElement* ref_p = findRefElement(_quadrangle, interp_p);
  if (dim_mesh==2)   //2D case, assuming same element shapetype
  {
    sh=(*ms.elements_.begin())->shapeType();
    if (sh==_triangle) ref_p= findRefElement(_prism, interp_p);
    else if (sh==_quadrangle) ref_p= findRefElement(_hexahedron, interp_p);
    else error("shape_not_handled", words("shape", sh));
  }
  number_t e=0;
  for (number_t k=0; k<nbl; k++)  //loop on slices
  {
    std::vector<GeomElement*>::const_iterator ites=ms.elements_.begin();
    for (; ites!=ms.elements_.end(); ++ites, e++)
    {
      elements_[e]=new GeomElement(this, ref_p, dim, e+1);
      MeshElement* melt=elements_[e]->meshElement();
      number_t nv=(*ites)->numberOfVertices();
      melt->vertexNumbers.resize(2*nv);
      if (sh==_segment)  //1D case
      {
        melt->vertexNumbers[0]=(*ites)->vertexNumber(1)+k*nbver_s;
        melt->vertexNumbers[1]=(*ites)->vertexNumber(2)+k*nbver_s;
        melt->vertexNumbers[2]=(*ites)->vertexNumber(2)+(k+1)*nbver_s;
        melt->vertexNumbers[3]=(*ites)->vertexNumber(1)+(k+1)*nbver_s;
      }
      else
      {
        for (number_t i=0; i<nv; ++i)
        {
          melt->vertexNumbers[i]=(*ites)->vertexNumber(i+1)+k*nbver_s;
          melt->vertexNumbers[nv+i]=melt->vertexNumbers[i]+nbver_s;
        }
      }
      melt->nodeNumbers = melt->vertexNumbers;
      melt->setNodes(nodes);
    }
  }
  // remove duplicate nodes (closed extrusion)
  noDuplicateNode();

  //create domains
  std::vector<GeomDomain*>::const_iterator itd=ms.domains().begin();
  std::vector<GeomDomain*> doms;
  for (; itd!=ms.domains().end(); ++itd)
    if ((*itd)->dim()==dim_mesh && (*itd)->domType()==_meshDomain) doms.push_back(*itd);

  if (extdata.namingDomain() <2)  //unique domain
  {
    if (doms.size()==0 || extdata.namingDomain()==0)  //only the full domain named Omega
    {
      MeshDomain* meshdom_p = (new GeomDomain(*this, "#Omega", dim_mesh+1))->meshDomain();
      meshdom_p->geomElements = elements_;
      domains_.push_back(meshdom_p);
    }
    else
    {
      for (number_t d=0; d<doms.size(); d++)
      {
        number_t e=0;
        MeshDomain* meshdom_p = (new GeomDomain(*this, doms[d]->name()+"_e", dim_mesh+1))->meshDomain();
        domains_.push_back(meshdom_p);
        MeshDomain* dom=doms[d]->meshDomain();
        number_t nbe = dom->numberOfElements();
        meshdom_p->geomElements.resize(nbe*nbl);
        for (number_t k=0; k<nbl; k++)
          for (number_t i=0; i<nbe; i++, e++)
            meshdom_p->geomElements[e]=elements_[k*nbelt_s+dom->geomElements[i]->number()-1];
      }
    }
  }
  if (extdata.namingDomain() >= 2)  //one domain by slice
    {
      if (doms.size()==0)
      {
        number_t e=0;
        for (number_t k=0; k<nbl; k++)  //loop on slices
        {
          MeshDomain* meshdom_p = (new GeomDomain(*this, "Omega_e"+tostring(k+1), dim_mesh+1))->meshDomain();
          meshdom_p->geomElements.resize(nbelt_s);
          for (number_t i=0; i<nbelt_s; i++, e++) meshdom_p->geomElements[i]=elements_[e];
          domains_.push_back(meshdom_p);
        }
      }
      else
        for (number_t d=0; d<doms.size(); d++)
        {
          MeshDomain* dom=doms[d]->meshDomain();
          number_t nbe = dom->numberOfElements();
          string_t dnam = doms[d]->name();
          for (number_t k=0; k<nbl; k++)
          {
            number_t e=0;
            MeshDomain* meshdom_p = (new GeomDomain(*this, dnam+"_e"+tostring(k+1), dim_mesh+1))->meshDomain();
            domains_.push_back(meshdom_p);
            meshdom_p->geomElements.resize(nbe);
            for (number_t i=0; i<nbe; i++, e++)
              meshdom_p->geomElements[e]=elements_[k*nbe+dom->geomElements[i]->number()-1];
          }
        }
    }

  number_t last = elements_.size()+1;
  if (extdata.namingSection() >0)  //create section domain
    {
      number_t s1=1;
      if (sh==_quadrangle) s1=6;   //left side numbering
      number_t s2=3;
      if (sh==_triangle) s2=5;     //right side numbering
      if (extdata.namingSection()==1)
      {
        if (doms.size()==0)
        {
          MeshDomain* meshdom_p = (new GeomDomain(*this, "Section_0", dim_mesh))->meshDomain();
          domains_.push_back(meshdom_p);
          meshdom_p->geomElements.resize(nbelt_s);
          for (number_t i=0; i<nbelt_s; i++, last++)
            meshdom_p->geomElements[i]=new GeomElement(elements_[i],s1,last);
          meshdom_p = (new GeomDomain(*this, "Section_"+tostring(nbl), dim_mesh))->meshDomain();
          domains_.push_back(meshdom_p);
          meshdom_p->geomElements.resize(nbelt_s);
          for (number_t i=0; i<nbelt_s; i++, last++)
            meshdom_p->geomElements[i]=new GeomElement(elements_[(nbl-1)*nbelt_s+i],s2,last);
        }
        else
        {
          for (number_t d=0; d<doms.size(); d++)
          {
            MeshDomain* dom=doms[d]->meshDomain();
            number_t nbe = dom->numberOfElements();
            string_t dnam = doms[d]->name();
            MeshDomain* meshdom_p = (new GeomDomain(*this, dnam+"_0", dim_mesh))->meshDomain();
            domains_.push_back(meshdom_p);
            meshdom_p->geomElements.resize(nbe);
            for (number_t i=0; i<nbe; i++, last++)
              meshdom_p->geomElements[i]=new GeomElement(elements_[dom->geomElements[i]->number()-1],s1,last);
            meshdom_p = (new GeomDomain(*this, dnam+"_"+tostring(nbl), dim_mesh))->meshDomain();
            domains_.push_back(meshdom_p);
            meshdom_p->geomElements.resize(nbe);
            for (number_t i=0; i<nbe; i++, last++)
              meshdom_p->geomElements[i]=new GeomElement(elements_[(nbl-1)*nbelt_s+dom->geomElements[i]->number()-1],s2,last);
          }
        }
      }
      else
      {
        if (doms.size()==0)
        {
          for (number_t k=0; k<nbl+1; k++)
          {
            MeshDomain* meshdom_p = (new GeomDomain(*this, "Section_"+tostring(k), dim_mesh))->meshDomain();
            domains_.push_back(meshdom_p);
            meshdom_p->geomElements.resize(nbelt_s);
            for (number_t i=0; i<nbelt_s; i++, last++)
            {
              if (k<nbl) meshdom_p->geomElements[i]=new GeomElement(elements_[k*nbelt_s+i],s1,last);
              else meshdom_p->geomElements[i]=new GeomElement(elements_[(k-1)*nbelt_s+i],s2,last);
              if (k>0 && k<nbl) meshdom_p->geomElements[i]->parentSides().push_back(GeoNumPair(elements_[(k-1)*nbelt_s+i], s2));
            }
          }
        }
        else
        {
          for (number_t d=0; d<doms.size(); d++)
          {
            MeshDomain* dom=doms[d]->meshDomain();
            number_t nbe = dom->numberOfElements();
            string_t dnam = doms[d]->name();
            for (number_t k=0; k<nbl+1; k++)
            {
              MeshDomain* meshdom_p = (new GeomDomain(*this, dnam+"_"+tostring(k), dim_mesh))->meshDomain();
              domains_.push_back(meshdom_p);
              meshdom_p->geomElements.resize(nbe);
              for (number_t i=0; i<nbe; i++, last++)
              {
                number_t j= dom->geomElements[i]->number()-1;
                if (k<nbl) meshdom_p->geomElements[i]=new GeomElement(elements_[k*nbelt_s+j],s1,last);
                else meshdom_p->geomElements[i]=new GeomElement(elements_[(k-1)*nbelt_s+j],s2,last);
                if (k>0 && k<nbl) meshdom_p->geomElements[i]->parentSides().push_back(GeoNumPair(elements_[(k-1)*nbelt_s+j], s2));
              }
            }
          }
        }
      }
    }

  if (extdata.namingSide() >0)  //create side domain, propagates sidename of section mesh
  {
    itd=ms.domains().begin();
    doms.clear();
    for (; itd!=ms.domains().end(); ++itd)
      if ((*itd)->dim()==dim_mesh-1 && (*itd)->domType()==_meshDomain) doms.push_back(*itd);

    std::vector<number_t> sToS;   // side numbering of section element to side numbering of side of extruded element
    switch (sh)
    {
      case _segment: sToS.resize(3,0); sToS[1]=4; sToS[2]=2; break;
      case _triangle: sToS.resize(4,0); sToS[1]=2; sToS[2]=3; sToS[3]=4; break;
      case _quadrangle: sToS.resize(5,0); sToS[1]=1; sToS[2]=2; sToS[3]=4; sToS[4]=5; break;
      default: error("shape_not_handled", words("shape", sh));
    }
    if (doms.size()>0)
    {
      for (itd=doms.begin(); itd!=doms.end(); ++itd)
      {
        MeshDomain* sidom=(*itd)->meshDomain();
        number_t nbe = sidom->numberOfElements();
        if (extdata.namingSide()==1)   //only one boundary
        {
          MeshDomain* meshdom_p = (new GeomDomain(*this, (*itd)->name()+"_e", dim_mesh))->meshDomain();
          domains_.push_back(meshdom_p);
          meshdom_p->geomElements.resize(nbe*nbl);
          number_t e=0;
          for (number_t k=0; k<nbl; k++)  //loop on slice
          {
            for (number_t i=0; i<nbe; i++, last++, e++)
            {
              number_t ind = sidom->geomElements[i]->parentSides()[0].first->number();
              number_t sid = sToS[sidom->geomElements[i]->parentSides()[0].second];
              meshdom_p->geomElements[e]=new GeomElement(elements_[k*nbelt_s+ind-1],sid,last);
            }
          }
        }
        else //one boundary by layer
        {
          for (number_t k=0; k<nbl; k++)  //loop on slice
          {
            MeshDomain* meshdom_p = (new GeomDomain(*this, (*itd)->name()+"_e"+tostring(k), dim_mesh))->meshDomain();
            domains_.push_back(meshdom_p);
            meshdom_p->geomElements.resize(nbe);
            for (number_t i=0; i<nbe; i++, last++)
            {
              number_t ind = sidom->geomElements[i]->parentSides()[0].first->number();
              number_t sid = sToS[sidom->geomElements[i]->parentSides()[0].second];
              meshdom_p->geomElements[i]=new GeomElement(elements_[k*nbelt_s+ind-1],sid,last);
            }
          }
        }
      }
    }
  }
  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();
  trace_p->pop();
}

//! external routine to apply an extrusion on a Mesh using an elementary transformation
Mesh extrude(const Mesh& sectionMesh, const Transformation& tr, const std::vector<Parameter>& ps)
{
  Mesh m;
  ExtrusionData extdata(tr,false,ps);
  m.buildExtrusion(sectionMesh,extdata);
  return m;
}

//! external routine to apply an extrusion on a Mesh using a list of transformation
Mesh extrude(const Mesh& sectionMesh, const std::vector<Transformation*>& trs, const std::vector<Parameter>& ps)
{
  Mesh m;
  ExtrusionData extdata(trs,false,ps);
  m.buildExtrusion(sectionMesh,extdata);
  return m;
}

//! external routine to apply an extrusion on a Mesh using a parametrisation function
Mesh extrude(const Mesh& sectionMesh, par_fun f, const std::vector<Parameter>& ps)
{
  Mesh m;
  ExtrusionData extdata(f,false,ps);
  m.buildExtrusion(sectionMesh,extdata);
  return m;
}

/* remove duplicate nodes at a tolerance*/
void Mesh::noDuplicateNode(real_t tol)
{
   trace_p->push("Mesh::noDuplicateNode");
   //find re-numbering
   std::map<number_t,std::pair<number_t,number_t>> nums;  // old num -> (new num, 1 if duplicate)
   std::list<Point> newNodes;
   number_t n=0, m=0;
   bool uni=true;
   for(number_t i=0;i<nodes.size();i++)
   {
      uni=true;
      const Point& qi = nodes[i];
      for(auto& p:nums)
      {
        if(dist(nodes[p.second.first],qi) < tol)  // same node
        {
          nums[i]=std::make_pair(p.second.first,1);
          uni=false;
          break;
        }
      }
      if(uni) { nums[i] = std::make_pair(m++,0); newNodes.push_back(nodes[i]);}
   }
   if(m==nodes.size()) {trace_p->pop();return;}  // no duplicates
   //update nodes and vertices
   nodes.assign(newNodes.begin(),newNodes.end());
   std::list<number_t> newvs;
   for(auto& i:vertices_)
   {
       std::pair<number_t,number_t>& p = nums[i-1];
       if(p.second==0) newvs.push_back(p.first+1);
   }
   vertices_.assign(newvs.begin(),newvs.end());
   //update node index of elements_
   for(auto& e:elements_)
   {
     MeshElement* melt=e->meshElement();
     auto itn = melt->nodes.begin();
     for(auto& n:melt->nodeNumbers)
     {
         *itn = &nodes[nums[n-1].first];
         n = nums[n-1].first+1;
         itn++;
     }
     for(auto& n:melt->vertexNumbers)  n=nums[n-1].first+1;
   }
   // domains of mesh not updated, may be to be done in futur
   trace_p->pop();
}

/*create pyramid mesh from hexaedron mesh
  by spliting each hexaedron into six pyramids using the hexahedron centroids as new nodes c
    hexahedron face 1: 2 6 5 1 -> pyramid  2 6 5 1 c
    hexahedron face 2: 7 6 2 3 -> pyramid  7 6 2 3 c
    hexahedron face 3: 5 6 7 8 -> pyramid  5 6 7 8 c
    hexahedron face 4: 3 4 8 7 -> pyramid  3 4 8 7 c
    hexahedron face 5: 8 4 1 5 -> pyramid  8 4 1 5 c
    hexahedron face 6: 1 4 3 2 -> pyramid  1 4 3 2 c
  split only first order hexaedra
  assume current void mesh
*/
void Mesh::buildPyramidFromHexahedron(const Mesh& hexMesh)
{
  trace_p->push("Mesh::buildPyramidFromHexahedron");

  if (name_=="") name_=hexMesh.name_+"_toPyramids";
  comment_="mesh from spliting hexadron into six pyramids";
  isMadeOfSimplices_=false;
  order_=1;
  lastIndex_=0;
  firstOrderMesh_p=this;
  geometry_p=nullptr;
  number_t nbelt_h=hexMesh.nbOfElements();
  number_t nbver_h=hexMesh.nbOfVertices();
  elements_.resize(6*nbelt_h);
  nodes.resize(nbver_h+nbelt_h);

  //create nodes only from vertices
  std::vector<number_t>::const_iterator itv;
  std::vector<Point>::iterator itn=nodes.begin();
  for (itv=hexMesh.vertices_.begin(); itv!=hexMesh.vertices_.end(); ++itv, ++itn)  //copy original nodes
    *itn=hexMesh.nodes[*itv-1];
  std::vector<GeomElement*>::const_iterator iteh=hexMesh.elements_.begin();
  for (; iteh!=hexMesh.elements_.end(); ++iteh, ++itn)
  {
    if ((*iteh)->shapeType()!=_hexahedron)
    {
      where("Mesh::buildPyramidFromHexahedron()");
      error("shape_not_handled", words("shape", (*iteh)->shapeType()));
    }
    *itn = (*iteh)->meshElement()->centroid;
  }

  //construct vertex indices (begins at 1)
  vertices_.resize(nodes.size());
  for (number_t i = 0; i < nodes.size(); i++) { vertices_[i] = i + 1; }

  //create elements
  std::vector<GeomElement*>::iterator ite=elements_.begin();
  number_t nc = nbver_h+1, e=0;
  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  RefElement* ref_p = findRefElement(_pyramid, interp_p);
  const GeomRefElement* gref_h = findRefElement(_hexahedron, interp_p)->geomRefElement();
  for (iteh=hexMesh.elements_.begin(); iteh!=hexMesh.elements_.end(); ++iteh, nc++)
  {
    for (number_t s=1; s<=6; s++, e++, ++ite)  //loop on hexahedron faces
    {
      *ite = new GeomElement(this, ref_p, 3, e+1);
      MeshElement* melt=(*ite)->meshElement();
      melt->vertexNumbers.resize(5);
      for (number_t i=1; i<=4; ++i)
        melt->vertexNumbers[i-1]=(*iteh)->vertexNumber(gref_h->sideVertexNumber(i,s));
      melt->vertexNumbers[4] = nc;
      melt->nodeNumbers = melt->vertexNumbers;
      melt->setNodes(nodes);
    }
  }
  number_t last =elements_.size()+1;

  //create domains with same name
  std::vector<GeomDomain*>::const_iterator itdh=hexMesh.domains().begin();
  for (; itdh!=hexMesh.domains().end(); ++itdh)
  {
    MeshDomain* domh=(*itdh)->meshDomain();
    number_t nbe = domh->numberOfElements();
    if ((*itdh)->dim()==3)   //volumic domain
    {
      MeshDomain* domp = (new GeomDomain(*this, (*itdh)->name(), 3))->meshDomain();
      domains_.push_back(domp);
      domp->geomElements.resize(6*nbe);
      ite=domp->geomElements.begin();
      for (iteh = domh->geomElements.begin(); iteh!=domh->geomElements.end(); ++iteh)
      {
        number_t num=(*iteh)->number()-1;
        for (number_t k=0; k<6; ++k,++ite)  *ite = elements_[6*num+k];
      }
    }
    if ((*itdh)->dim()==2)   //surfacic domain
    {
      MeshDomain* domp = (new GeomDomain(*this, (*itdh)->name(), 2))->meshDomain();
      domains_.push_back(domp);
      domp->geomElements.resize(nbe);
      ite=domp->geomElements.begin();
      for (iteh = domh->geomElements.begin(); iteh!=domh->geomElements.end(); ++iteh, ++ite)
      {
        const std::vector<GeoNumPair>& psh= (*iteh)->parentSides();
        std::vector<GeoNumPair>::const_iterator itsh=psh.begin();
        GeomElement* parp = elements_[6*(itsh->first->number()-1)+itsh->second-1];
        GeomElement* geop = new GeomElement(parp,1,last++);
        *ite = geop;
        itsh++;
        for (; itsh!=psh.end(); ++itsh)
        {
          parp =elements_[6*(itsh->first->number()-1)+itsh->second-1];
          geop->parentSides().push_back(GeoNumPair(parp,1));
        }
      }
    }
  }

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();
  trace_p->pop();
}

} // end of namespace xlifepp
