/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Parametrization.hpp
  \authors E. Lunéville
  \since 10 jan 2017
  \date 10 jan 2017

  \brief Definition of the xlifepp::Parametrization class

  The Parametrization class handles a general parametrization
  that is an application p from a geometric domain D to the space R^n
  It mainly manages
      geometry_p: a pointer to the Geometry object D
      f_p: a pointer to the parametrization function of the form Vector<Real> f(const Point&, Parameters&, DiffOpType)
  and the data:
      name: the name of the parametrization (default "")
      dim: the dimension of the arrival space (initialized at construction by calling once the parametrization function)
      params: an additional list of parameters pass to functions

   besides some other pointer functions can be set
      invParametrization_p: pointer to the inverse function (may be 0)
      curabc_p: pointer to the curvilinear abcissa function (may be 0)
      curvature_p: pointer to the curvature function (may be 0)
      length_p: pointer to the length function (may be 0)
      normal_p: pointer to the normal function (may be 0)
      tangent_p: pointer to the tangent function (may be 0)

    to allow to deal with curves and surfaces, all this function returns a Vector<Real> even if the returned quantities is a scalar, a vector, some vectors, a matrix, ...

    the followong maps are managed:
      1D -> 1D   (segment mapping)        2D -> 2D (surface mapping)    3D -> 3D (volume mapping)
      1D -> 2D   (2D curve)               2D -> 3D (3D surface)
      1D -> 3D   (3D curve)
    Note: length, curvature, curabc, tangent and normal vectors are not managed for nD -> nD mapping

  Example: parametrization of the unit circle
    Vector<Real> myfun(const Point& P, Parameters& pars, DiffOpType dif)
    {
     Real t=P(1);
     if(dif == _id)  return Vector<Real>(cos(t),sin(t));
     if(dif == _dt)  return Vector<Real>(-sin(t),cos(t));
     if(dif == _dt2) return Vector<Real>(-cos(t),-sin(t));
     parfun_error("myfun",dif);
    }
    ...
    Parametrization pcircle(0,2*pi_,myfun);
    Vector<Real> q = pcircle(pi_);         // point at t=pi
    Vector<Real> t = pcircle(pi_,_dt);     // tangent vector at t=pi
    Vector<Real> t = pcircle.normal(pi_);  // unitary normal vector at t=pi
    Vector<Real> t = pcircle.tangent(pi_);  // unitary tangent vector at t=pi

    Besides, it is also possible to define the parametrization function as a symbolic function:
        Parametrization pc(-pi_, pi_, cos(x_1), sin(x_1), pars, "unit circle");
        Parametrization ps(-pi_/2, pi_/2, -pi_, pi_, cos(x_1)*cos(x_2), cos(x_1)*sin(x_2), sin(x_2), pars, "unit sphere");

    By default, length, curvature, curabc, tangent and normal vectors are computed from derivatives
    but it is possible to specify them as function or symbolic functions using setlength, setcurvature, ...

    If it is planned to compute quantitities at points in arrival space, the inverse function has to be set (see setinvParametrization)!

    This class supports also parametrization of piecewise geometry.
    In that case the parametrization functions use the geom_p pointer, more precisely the data geom_p->connectedParts
    that provides pieces of canonical geometry organized as multiple connected parts.
 */

#ifndef PARAMETRIZATION_HPP_INCLUDED
#define PARAMETRIZATION_HPP_INCLUDED

#include "config.h"
#include "utils.h"

namespace xlifepp
{
class Geometry; //forward declaration
class Mesh;     //forward declaration

typedef Vector<real_t> (*par_fun)(const Point&, Parameters&, DiffOpType);  //!< alias of real vector function

inline void parfun_error(const string_t& com, DiffOpType d)
{
  error("parfun_error",words("diffop",d),com);
}

inline void parmap_error(const string_t& com, number_t d)
{
  error("parmap_error",com,d);
}

Vector<real_t> symbolic_f(const Point&, Parameters&, DiffOpType);         //!< to deal with symbolic paramerization f
Vector<real_t> symbolic_invParametrization(const Point&, Parameters&, DiffOpType);      //!< to deal with symbolic paramerization invParametrization
Vector<real_t> symbolic_length(const Point&, Parameters&, DiffOpType);    //!< to deal with symbolic paramerization length
Vector<real_t> symbolic_curvature(const Point&, Parameters&, DiffOpType); //!< to deal with symbolic paramerization curvature
Vector<real_t> symbolic_curabc(const Point&, Parameters&, DiffOpType);    //!< to deal with symbolic paramerization curvilinear abcissa
Vector<real_t> symbolic_normal(const Point&, Parameters&, DiffOpType);    //!< to deal with symbolic paramerization normal
Vector<real_t> symbolic_tangent(const Point&, Parameters&, DiffOpType);   //!< to deal with symbolic paramerization tangent

enum DiffComputation{_IdComputation,_funComputation, _invComputation, _lengthComputation, _lengthsComputation, _curvatureComputation, _curvaturesComputation,
                     _curabcComputation, _curabcsComputation, _normalComputation, _tangentComputation};

//===========================================================================================
//                  Parametrization class handling any parametrization
//===========================================================================================
class Parametrization
{
  protected:
    Geometry* geom_p;           //!< pointer to the parametrized geometry (may be 0)
    Geometry* geomSupport_p;    //!< pointer to the geometry support of the parameter
    par_fun f_p;                //!< pointer to the parametrization function with parameters
    mutable Vector<Vector<real_t> > curabcs_; //!< internal vector to store curvilinear abcissas on a grid
    mutable Mesh* meshSupport_p;//!< to store an optional mesh of the geometry support (may be 0)

  public:
    par_fun curvature_p;  //!< pointer to a curvature function (may be 0)
    par_fun length_p;     //!< pointer to a length function (may be 0)
    par_fun invParametrization_p;      //!< pointer to the inverse function (may be 0)
    par_fun normal_p;     //!< pointer to the normal function (may be 0)
    par_fun tangent_p;    //!< pointer to the tangent function (may be 0)
    par_fun curabc_p;     //!< pointer to the curvilinear abcissa function (may be 0)
    par_fun christoffel_p;//!< pointer to the cristoffel symbols function (may be 0)
    string_t name;        //!< a parametrization name useful for printing
    dimen_t dimg;         //!< dimension of the geometry, set by init()
    dimen_t dim;          //!< dimension of the arrival space, set by init()
    number_t np;          //!< number of parameters used when approximating curvilinear abcissa (default=1000)
    mutable Parameters params; //!< optional parameter list
    real_t s0, s1;        //!< starting curvilinear abcissas (default 0)
    ContinuityOrder contOrder; //!< continuity order (by default _Cinf), to be set by user if not _Cinf
    Vector<real_t> periods;    //!< (u,v,w) period value (0 means non periodic)
    std::list<std::pair<number_t, real_t> > singularSide;  //!< singular side (parameter index, value); in 2D:(1,0-1)->u=0-1  or (2,0-1)->v=0-1 in 3D: two of (i,a) defining face parametrization
    virtual bool isPiecewise() const {return false;}
    bool hasSingularSide() const {return singularSide.size()>0;}
    bool onSingularSide(const Point& p) const;  //!< true if p is located on singular side
    bool isPeriodic() const;   //!< true if there exists at least one periodic direction
    number_t nbOfMaps=1;       //!< number of maps associated to the parametrization

    //constructors
    Parametrization(Geometry * g=nullptr);  //!< parametrization from a geometry (composite) and default constructor
    Parametrization(const Geometry& geo, par_fun, const Parameters&, const string_t& na="", dimen_t dimp=0); //!< parametrization from a geometry and a function
    Parametrization(real_t a, real_t b, par_fun, const Parameters&, const string_t& na="", dimen_t dimp=0);  //!< 1d parametrization from segment [a,b] (1D mapping or 2D/3D curve)
    Parametrization(real_t a, real_t b, real_t c, real_t d, par_fun, const Parameters&, const string_t& na="", dimen_t dimp=0);//!< 2d parametrization from rectangle [a,b]x[c,d] (2D mapping or 3D surface)
    Parametrization(real_t a, real_t b, par_fun, par_fun, const Parameters&, const string_t& na="", dimen_t dimp=0);  //!< 1d parametrization from segment [a,b] (1D mapping or 2D/3D curve)
    Parametrization(real_t a, real_t b, real_t c, real_t d, par_fun, par_fun, const Parameters&, const string_t& na="", dimen_t dimp=0);//!< 2d parametrization from rectangle [a,b]x[c,d] (2D mapping or 3D surface)

    Parametrization(const Geometry& g, const SymbolicFunction& f, const Parameters&, const string_t& na="");   //!< 1d parametrization from geometry (1D mapping)
    Parametrization(const Geometry& g, const SymbolicFunction& f1, const SymbolicFunction& f2,
                    const Parameters&, const string_t& na="");   //!< parametrization from geometry to 2D space (2D curve or 2D mapping)
    Parametrization(const Geometry& g, const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3,
                    const Parameters&, const string_t& na="");   //!< parametrization from geometry to 3D space (3D curve, 3D surface or 3D mapping)

    Parametrization(real_t a, real_t b, const SymbolicFunction& f, const Parameters&, const string_t& na="");  //!< 1d parametrization from segment [a,b] (1D mapping)
    Parametrization(real_t a, real_t b, const SymbolicFunction& f1, const SymbolicFunction& f2, const Parameters&, const string_t& na="");//!< 1d parametrization from segment [a,b] (2D curve)
    Parametrization(real_t a, real_t b, const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3,
                    const Parameters&, const string_t& na="");//!< 1d parametrization from segment [a,b] (3D curve)
    Parametrization(real_t a, real_t b, real_t c, real_t d, const SymbolicFunction& f1, const SymbolicFunction& f2,
                    const Parameters&, const string_t& na="");//!< 2d parametrization from rectangle [a,b]x[c,d] (2D mapping)
    Parametrization(real_t a, real_t b, real_t c, real_t d, const SymbolicFunction& f1, const SymbolicFunction& f2, const SymbolicFunction& f3,
                    const Parameters&, const string_t& na="");//!< 2d parametrization from rectangle [a,b]x[c,d] (2D surface)

    Parametrization(const Parametrization&);            //!< copy constructor
    Parametrization& operator=(const Parametrization&); //!< assign operator
    ~Parametrization();                                 //!< destructor (clear pointers)

    protected:
    void init();            //!< initialization, set dim
    void clearPointers();   //!< clear internal pointers (geometry and symbolic functions)
    void copy(const Parametrization&); //!< copy tool
    void buildSymbolic(const SymbolicFunction&);//!< bulid symbolic functions related to the parametrization
    void buildSymbolic(const SymbolicFunction&, const SymbolicFunction&);//!< bulid symbolic functions related to the parametrization
    void buildSymbolic(const SymbolicFunction&, const SymbolicFunction&, const SymbolicFunction&);//!< bulid symbolic functions related to the parametrization

    public:
    Geometry& geomSupport() const {return *geomSupport_p;}
    Geometry*& geomSupportP() {return geomSupport_p;}
    Geometry*& geomP() {return geom_p;}
    Geometry&  geom() const {return *geom_p;}
    Mesh* meshP() const {return meshSupport_p;}
    Mesh*& meshP() {return meshSupport_p;}

    void setinvParametrization(par_fun f)  {invParametrization_p=f;}
    void setLength(par_fun f)    {length_p=f;}
    void setCurvature(par_fun f) {curvature_p=f;}
    void setCurabc(par_fun f)    {curabc_p=f;}
    void setNormal(par_fun f)    {normal_p=f;}
    void setTangent(par_fun f)   {tangent_p=f;}

    void setinvParametrization(const SymbolicFunction&);                           //!< associate symbolic function to invParametrization (1D parametrization)
    void setinvParametrization(const SymbolicFunction&, const SymbolicFunction&);  //!< associate symbolic function to invParametrization (2D parametrization)
    void setLength(const SymbolicFunction&);                              //!< associate symbolic function to length/surface element
    void setCurvature(const SymbolicFunction&);                           //!< associate symbolic function to curvature (2D curve)
    void setCurvatures(const SymbolicFunction&, const SymbolicFunction&); //!< associate symbolic function to curvatures (3D curve/surface)
    void setCurabc(const SymbolicFunction&);                              //!< associate symbolic function to curvilinear abcissa (2D curve)
    void setCurabcs(const SymbolicFunction&, const SymbolicFunction&);    //!< associate symbolic function to curvilinear abcissa (3D surface)
    void setNormal(const SymbolicFunction&, const SymbolicFunction&);     //!< associate symbolic function to normal  (2D curve)
    void setNormal(const SymbolicFunction&, const SymbolicFunction&, const SymbolicFunction&);   //!< associate symbolic function to normal (3D surface)
    void setNormals(const SymbolicFunction&, const SymbolicFunction&, const SymbolicFunction&,
                    const SymbolicFunction&, const SymbolicFunction&, const SymbolicFunction&);  //!< associate symbolic function to normal (3D curve/surface)
    void setTangent(const SymbolicFunction&, const SymbolicFunction&);                           //!< associate symbolic function to tangent (2D curve)
    void setTangent(const SymbolicFunction&, const SymbolicFunction&,const SymbolicFunction&);   //!< associate symbolic function to tangent (3D curve)
    void setTangents(const SymbolicFunction&, const SymbolicFunction&,const SymbolicFunction&,
                     const SymbolicFunction&, const SymbolicFunction&,const SymbolicFunction&);  //!< associate symbolic function to tangent (3D surface)
    RealPair bounds(VariableName v=_x1) const; //!< return bounds of geometry in v direction (_x1,_x2,_x3)
    void createMesh(real_t hs=0., ShapeType sh=_noShape);  //!< create a mesh of the geometry

    //operations
    Geometry* locateGeometry(Point& t) const;  //!< locate canonical geometry when parametrization of a piecewise geometry
    Point operator()(const Point& t, DiffOpType d=_id) const;   //!< compute parametrization
    Point operator()(real_t t, DiffOpType d=_id) const  {return (*this)(Point(t),d);}
    Point operator()(real_t u, real_t v, DiffOpType d=_id) const  {return (*this)(Point(u,v),d);}

    virtual Point toParameter(const Point& p) const;      //!< inverse function of the parametrization
    real_t toRealParameter(const Point& p) const  //!< inverse function of the parametrization (restricted to curve)
    {return toParameter(p)[0];}
    Point toDerParameter(const Point& t,const Point &dp) const; //!< compute parameter derivatives dt related to dp
    void toParameters(const std::vector<Point*>& nodes, number_t& mapIndex, std::vector<Point*>& parnodes) const;//!< parameters associated to a set of points

    virtual Vector<real_t> lengths(const Point&, DiffOpType =_id) const;        //!< local lengths of a curve: sqrt(x1'(t)^2+x2'(t)^2+...)
    Vector<real_t> lengths(real_t t, DiffOpType d=_id) const            //!< shortcut for 1D parametrization
    {return lengths(Point(t),d);}
    Vector<real_t> lengths(real_t u, real_t v, DiffOpType d=_id) const  //!< shortcut for 2D parametrization
    {return lengths(Point(u,v),d);}
    real_t length(const Point& P, DiffOpType d =_id) const       //!< shortcut to the first length if few
    {return lengths(P,d)[0];}
    real_t length(real_t t, DiffOpType d =_id) const             //!< shortcut for 1D parametrization
    {return lengths(Point(t),d)[0];}
    real_t length(real_t u, real_t v, DiffOpType d =_id) const   //!< shortcut for 2D parametrization
    {return lengths(Point(u,v),d)[0];}
    real_t bilength(const Point& P, DiffOpType d =_id) const     //!< shortcut to the first length if few
    {return lengths(P,d)[1];}
    real_t bilength(real_t t, DiffOpType d =_id) const           //!< shortcut for 1D parametrization
    {return lengths(Point(t),d)[1];}
    real_t bilength(real_t u, real_t v, DiffOpType d =_id) const //!< shortcut for 2D parametrization
    {return lengths(Point(u,v),d)[1];}

    virtual Vector<real_t> curvatures(const Point&, DiffOpType =_id) const;//!< curvatures (1 or 2)
    Vector<real_t> curvatures(real_t t, DiffOpType d=_id) const    //!< shortcut for 1D parametrization
    {return curvatures(Point(t),d);}
    Vector<real_t> curvatures(real_t u, real_t v, DiffOpType d=_id) const    //!< shortcut for 1D parametrization
    {return curvatures(Point(u,v),d);}
    real_t curvature(const Point& P, DiffOpType d =_id) const       //!< shortcut to the first curvature if few
    {return curvatures(P,d)[0];}
    real_t curvature(real_t t, DiffOpType d =_id) const             //!< shortcut for 1D parametrization
    {return curvatures(Point(t),d)[0];}
    real_t curvature(real_t u, real_t v, DiffOpType d =_id) const   //!< shortcut for 2D parametrization
    {return curvatures(Point(u,v),d)[0];}
    real_t bicurvature(const Point& P, DiffOpType d =_id) const     //!< shortcut to the first curvature if few
    {return curvatures(P,d)[1];}
    real_t bicurvature(real_t t, DiffOpType d =_id) const           //!< shortcut for 1D parametrization
    {return curvatures(Point(t),d)[1];}
    real_t bicurvature(real_t u, real_t v, DiffOpType d =_id) const //!< shortcut for 2D parametrization
    {return curvatures(Point(u,v),d)[1];}
    real_t gaussCurvature(const Point& P, DiffOpType d =_id) const; //! Gauss curvature (3D surface)
    real_t gaussCurvature(real_t u, real_t v, DiffOpType d =_id) const
    {return gaussCurvature(Point(u,v),d);}
    real_t meanCurvature(const Point& P, DiffOpType d =_id) const; //! mean curvature (3D surface)
    real_t meanCurvature(real_t u, real_t v, DiffOpType d =_id) const
    {return meanCurvature(Point(u,v),d);}

    Vector<real_t> curabcs(const Point&, DiffOpType =_id) const;       //!< curvilinear abcissa (1 or 2)
    Vector<real_t> curabcs(real_t t, DiffOpType d=_id) const           //!< shortcut for 1D parametrization
    {return curabcs(Point(t),d);}
    Vector<real_t> curabcs(real_t u, real_t v, DiffOpType d=_id) const //!< shortcut for 2D parametrization
    {return curabcs(Point(u,v),d);}
    real_t curabc(const Point& P, DiffOpType d =_id) const             //!< shortcut to the first curabc if few
    {return curabcs(P,d)[0];}
    real_t curabc(real_t t, DiffOpType d =_id) const                   //!< shortcut for 1D parametrization
    {return curabcs(Point(t),d)[0];}
    real_t curabc(real_t u, real_t v, DiffOpType d =_id) const         //!< shortcut for 2D parametrization
    {return curabcs(Point(u,v),d)[0];}
    real_t bicurabc(const Point& P, DiffOpType d =_id) const           //!< shortcut to the second curabc if few
    {return curabcs(P,d)[1];}
    real_t bicurabc(real_t t, DiffOpType d =_id) const                 //!< shortcut for 1D parametrization
    {return curabcs(Point(t),d)[1];}
    real_t bicurabc(real_t u, real_t v, DiffOpType d =_id) const       //!< shortcut for 2D parametrization
    {return curabcs(Point(u,v),d)[1];}

    Vector<real_t> normals(const Point&, DiffOpType =_id) const;       //!< normal vectors (1 or 2)
    Vector<real_t> normals(real_t t, DiffOpType d=_id) const           //!< shortcut for 1D parametrization
    {return normals(Point(t),d);}
    Vector<real_t> normals(real_t u, real_t v, DiffOpType d=_id) const //!< shortcut for 2D parametrization
    {return normals(Point(u,v),d);}
    virtual Vector<real_t> normal(const Point& P, DiffOpType d=_id) const;     //!< shortcut to the first normal vector
    Vector<real_t> normal(real_t t, DiffOpType d=_id) const            //!< shortcut for 1D parametrization
    {return normal(Point(t),d);}
    Vector<real_t> normal(real_t u, real_t v, DiffOpType d=_id) const  //!< shortcut for 1D parametrization
    {return normal(Point(u,v),d);}
    virtual Vector<real_t> binormal(const Point& P, DiffOpType d=_id) const;   //!< shortcut to the second normal vector (if exists)
    Vector<real_t> binormal(real_t t, DiffOpType d=_id) const          //!< shortcut for 1D parametrization
    {return binormal(Point(t),d);}
    Vector<real_t> binormal(real_t u, real_t v, DiffOpType d=_id) const//!< shortcut for 2D parametrization
    {return binormal(Point(u,v),d);}

    Vector<real_t> tangents(const Point&, DiffOpType =_id) const;      //!< tangent vectors (1 or 2)
    Vector<real_t> tangents(real_t t, DiffOpType d=_id) const          //!< shortcut for 1D parametrization
    {return tangents(Point(t),d);}
    Vector<real_t> tangents(real_t u, real_t v,DiffOpType d=_id) const //!< shortcut for 2D parametrization
    {return tangents(Point(u,v),d);}
    virtual Vector<real_t> tangent(const Point& P, DiffOpType d=_id) const;    //!< shortcut to the first tangent vector
    Vector<real_t> tangent(real_t t, DiffOpType d=_id) const           //!< shortcut for 1D parametrization
    {return tangent(Point(t),d);}
    Vector<real_t> tangent(real_t u, real_t v, DiffOpType d=_id) const //!< shortcut for 2D parametrization
    {return tangent(Point(u,v),d);}
    virtual Vector<real_t> bitangent(const Point& P, DiffOpType d=_id) const;  //!< shortcut to the second tangent vector
    Vector<real_t> bitangent(real_t t, DiffOpType d=_id) const         //!< shortcut for 1D parametrization
    {return bitangent(Point(t),d);}
    Vector<real_t> bitangent(real_t u,real_t v, DiffOpType d=_id) const//!< shortcut for 1D parametrization
    {return bitangent(Point(u,v),d);}

    Matrix<real_t> jacobian(const Point& t, DiffOpType d=_id) const;    //!< jacobian matrix
    Matrix<real_t> jacobian(real_t u, DiffOpType d=_id) const           //!< shortcut for 1D parametrization
    {return jacobian(Point(u),d);}
    Matrix<real_t> jacobian(real_t u, real_t v, DiffOpType d=_id) const //!< shortcut for 2D parametrization
    {return jacobian(Point(u,v),d);}

    Vector<real_t> metricTensor(const Point& t, DiffOpType d=_id) const;    //!< metric tensor
    Vector<real_t> metricTensor(real_t u, DiffOpType d=_id) const           //!< shortcut for 1D parametrization
    {return metricTensor(Point(u),d);}
    Vector<real_t> metricTensor(real_t u, real_t v, DiffOpType d=_id) const //!< shortcut for 2D parametrization
    {return metricTensor(Point(u,v),d);}

    Vector<real_t> christoffel (const Point& t, DiffOpType d=_id) const;    //!< the Christoffel symbols
    Vector<real_t> christoffel (real_t u, DiffOpType d=_id) const           //!< shortcut for 1D parametrization
    {return christoffel(Point(u),d);}
    Vector<real_t> christoffel (real_t u, real_t v, DiffOpType d=_id) const //!< shortcut for 2D parametrization
    {return christoffel(Point(u,v),d);}

    Matrix<real_t> weingarten(const Point& t) const;                   //!< Weingarten matrix (only for 3D surface)
    Matrix<real_t> weingarten(real_t u, real_t v) const                //!< Weingarten matrix (only for 3D surface)
    {return weingarten(Point(u,v));}

    real_t normalCurvature(const Point& uv, const Vector<real_t>& d) const;    //!< normal curvature related to a given tangent direction (3D surface)
    Vector<real_t> curvatures(const Point& uv, const Vector<real_t>& d) const; //!< Gauss curvature, mean curvature, normal curvature related to a given tangent direction (3D surface)
    real_t torsion(const Point& t, DiffOpType d) const;                        //!< torsion (only for 3D curve, requires third derivatives)

    virtual void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}

}; // end of class Parametrization  ===================================================

inline std::ostream& operator<< (std::ostream& out, const Parametrization& par)
{par.print(out); return out;}

// inherited arc parametrizations of a surface parametrization (passed through parameters)
Vector<real_t> surfToArcParametrizationu0(const Point&, Parameters&, DiffOpType =_id);
Vector<real_t> surfToArcParametrizationu1(const Point&, Parameters&, DiffOpType =_id);
Vector<real_t> surfToArcParametrizationv0(const Point&, Parameters&, DiffOpType =_id);
Vector<real_t> surfToArcParametrizationv1(const Point&, Parameters&, DiffOpType =_id);

//===========================================================================================
//           PiecewiseParametrization class handling any piecewise parametrization
//===========================================================================================
/*!PiecewiseParametrization class handles a global parametrization of multiple patches: basically a list of standard parametrizations
   Main data members:
     map<Parametrization*, vector<pair<Parametrization*,number_t> > > neighborParsMap: map Parametrization* -> neighbor Parametrizations vector
     vector<Point> vertices: list of all vertices of parametrizations (stored once)
     map<Parametrization*, vector<number_t> > vertexIndexes: map Parametrization* -> vertex numbers
     with parameters correspondance index: u=0->1, u=1->2, v=0->3, v=1->4, w=0->5, w=1->6
     the neighbor parametrization and vertexIndexes vector are stored according to the parameters correspondance

  EXPERIMENTAL: global C0 parametrization, additional structures
    Vector <number_t> ns: the numbers n1, n2, n3
    map<Triplet, pair<Parametrization*, Transformation*>>> parsMap: map (i,j,k) -> (Parametrization*, Transformation*)  (i=1,n1, ,j=1,n2, k=1,n3)
    map<Parametrization*, Triplet> parsRMap: reverse map Parametrization* -> i,j,k
    u in [0,1] if curve, (u,v) in [0,1]x[0,1] if surface, (u,v,w) in [0,1]x[0,1]x[0,1] if volume
    u-interval [0,1] is split in n1 parts [0,1/n1],[1/n1,2/n1], ... [(n1-1)/n1,1]  defined if curve
    v-interval [0,1] is split in n2 parts [0,1/n2],[1/n2,2/n2], ... [(n2-1)/n2,1]  defined if surface
    w-interval [0,1] is split in n3 parts [0,1/n3],[1/n3,2/n3], ... [(n3-1)/n3,1]  defined if volume
    if defines n1 x n2 x n3 cells (Cijk)on which may be attached a parametrization Pijk or not and a tranfer fuction
    that maps (u,v,w) -> Tijk(n1*u-i+1, n2*v-j+1, n3*w-k+1) in [0,1]x[0,1]x[0,1]
         where T: [0,1]x[0,1]x[0,1] -> [0,1]x[0,1]x[0,1] is a linear transformation that maps parameters of Pijk
    the global parametrization may be discontinuous at interfaces except if the algorithm build continous parametrization
    by matching parameters on interfaces
    Note: Parametrization* involved here are shared pointers (no clone),
           null Transformation pointer means no transformation (say id)
           transformation pointers should be deleted as they are built on memory stack
*/
class PiecewiseParametrization : public Parametrization
{
 public:
    std::vector<Point> vertices;  //!< list of vertices
    std::map<Parametrization*,std::vector<number_t> > vertexIndexes; //!< for each Parametrization, list of vertex indices
    std::map<Parametrization*, std::vector<std::pair<Parametrization*,number_t> > > neighborParsMap; //!< nneighbor parametrizations for each parametrization
    ~PiecewiseParametrization();                                                 //! destructor
    //! build the neighborParsMap
    void buildNeighborParsMap(std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > >& sidemap);
    //! build vertices
    void buildVertices();
    //! locate parametrization related to a physical point, update parameter point q and distance d
    Parametrization* locateParametrization(const Point& p, const Point& dp, Parametrization* par0, Point& q, real_t& d) const;

    virtual bool isPiecewise() const {return true;}
    Vector<real_t> funParametrization(const Point& pt, Parameters& pars, DiffComputation dc, DiffOpType d=_id) const;
    Vector<real_t> invParametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;
    virtual Point toParameter(const Point& p) const;
    virtual Vector<real_t> lengths(const Point& P, DiffOpType d=_id) const //!< local lengths of a curve: sqrt(x1'(t)^2+x2'(t)^2+...)
    {return funParametrization(P,const_cast<Parameters&>(params),_lengthsComputation,d);}
    virtual Vector<real_t> curvatures(const Point& P, DiffOpType d =_id) const         //!< shortcut to the first length if few
    {return funParametrization(P,const_cast<Parameters&>(params),_curvaturesComputation,d);}
    void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}

    //for global C0 parametrization (EXPERIMENTAL)
    std::vector <number_t> ns;
    std::map<Triplet<>, std::pair<Parametrization*, Transformation*> > parsMap;      //!< map (i,j,k) -> Parametrization*
    std::map<Parametrization*, Triplet<> > parsRMap;                                 //!< reverse map Parametrization* -> (i,j,k)
    PiecewiseParametrization(Geometry* g, number_t n1, number_t n2=0, number_t n3=0);//!< basic constructor
    bool add(Parametrization* p, Transformation * tr, number_t i, number_t j=0, number_t k=0); //!< add parametrisation to map (return false if fails)
    void buildReverseMap();       //!< build the reverse map
    std::map<Triplet<>, std::pair<Parametrization*, Transformation*> >::const_iterator locateCell(const Point& pt) const;
    Vector<real_t> funC0Parametrization(const Point& pt, Parameters& pars, DiffComputation dc, DiffOpType d=_id) const;
    Vector<real_t> invC0Parametrization(const Point& pt, Parameters& pars, DiffOpType d=_id) const;
};

inline Vector<real_t> parametrization_Piecewise(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const PiecewiseParametrization*>(pars("piecewise parametrization").get_p())->funParametrization(pt,pars,_IdComputation,d);}

inline Vector<real_t> normal_Piecewise(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const PiecewiseParametrization*>(pars("piecewise parametrization").get_p())->funParametrization(pt,pars,_normalComputation,d);}

inline Vector<real_t> tangent_Piecewise(const Point& pt, Parameters& pars, DiffOpType d=_id)    //! extern parametrization call
{return reinterpret_cast<const PiecewiseParametrization*>(pars("piecewise parametrization").get_p())->funParametrization(pt,pars,_tangentComputation,d);}

inline Vector<real_t> invParametrization_Piecewise(const Point& pt, Parameters& pars, DiffOpType d=_id) //! extern invParametrization call
{return reinterpret_cast<const PiecewiseParametrization*>(pars("piecewise parametrization").get_p())->invParametrization(pt,pars,d);}

} // end of namespace xlifepp

#endif // PARAMETRIZATION_HPP_INCLUDED
