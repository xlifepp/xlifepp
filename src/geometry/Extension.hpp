/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Extension.hpp
  \author E. Lunéville
  \since 27 feb 2017
  \date  27 feb 2017

  \brief Definition of the xlifepp::Extension class

  Class xlifepp::Extension deals with extension of any "function" from a side domain (say sigma) to any domain (say omega) having sigma as side domain.

  Extension of a function f is  Ext(f)(x) = sum_{i on sigma} f(xi)*wi(x)  for any x in omega
  where wi are "volumic" Lagrange shape function of a given order k >0 (1 by default)
  By definition  Ext(f) on sigma is a Pk interpolation of f only approximating f on side !

                            |-----------------------------------
                            | support |     |     |       |
                            |-----------------------------------
                            |         |     |     |       |
                       sigma|   of    |     |     |       |
                            |         |     |     |       |
                            |-----------------------------------
                            |extension|     |     |       |
                            |-----------------------------------

  Extension is an object managed by OperatorOnFunction, OperatorOnKernel or OperatorOnUnknown allowing to take into account
  For instance, define E as an extension from sigma to omega, such syntaxes are allowed E*f, E*op(f), op(E(f)), E*k, E*op(k), op(E(k))

  Note that op(E*f)= sum_{i on sigma} f(xi)*op(wi) whereas  E*op(f)= sum_{i on sigma} op(f(xi))*wi
  that are identical if difop does not involve derivatives

  When sigma is a boundary domain, it is not required to specify the domain omega
  but when sigma is an interface, omega has to be specified , because if not, all elements having a vertex on sigma will be consider in the extension

*/

#ifndef EXTENSION_HPP_INCLUDED
#define EXTENSION_HPP_INCLUDED

#include "config.h"
#include "GeomDomain.hpp"
#include "utils.h"

namespace xlifepp
{

class OperatorOnFunction;
class OperatorOnKernel;
class TermVector;
class Unknown;

class Extension
{
  protected:
    const GeomDomain* sideDomain_, *domain_;   //!< domains involved in extension
    const GeomDomain* extendedDomain_;         //!< extended domain from vertices (built during construction)

  public:
    number_t order_;                           //!< order of Lagrange FE used in extension (1 by default)
    mutable VariableName var_;                 //!< variable (_x or _y) used in Kernel computation
    mutable std::map<GeomElement*,std::set<number_t> > domToSide; //!< map: domain element -> list of dofs on sideDomain (local number)

    Extension(const GeomDomain& sd, VariableName v=_x, number_t o=1)     //! extension constructor with no domain
      :sideDomain_(&sd), domain_(nullptr), order_(o), var_(v)
      {extendedDomain_= &sd.extendDomain(true);buildDomToSide();}
    Extension(const GeomDomain& sd, const GeomDomain& d, VariableName v=_x, number_t o=1) //! extension constructor with domain
      :sideDomain_(&sd), domain_(&d), order_(o), var_(v)
      {extendedDomain_= &sd.extendDomain(true, d); buildDomToSide();}

    string_t name() const;
    void buildDomToSide() const;                                       //!< build the map domToSide

    OperatorOnFunction& operator()(const Function&) const;             //!< apply Extension to Function
    OperatorOnFunction& operator()(OperatorOnFunction&) const;         //!< apply Extension to OperatorOnFunction
    OperatorOnKernel& operator()(const Kernel&) const;                 //!< apply Extension to Kernel
    OperatorOnKernel& operator()(OperatorOnKernel&) const;             //!< apply Extension to OperatorOnKernel

    real_t operator()(const Point &) const;                            //!< compute extension function at a point
    real_t operator()(const Point &, const GeomElement&) const;        //!< compute extension function at a point knowing element
    std::vector<real_t> grad(const Point &) const;                     //!< compute gradient of extension function at a point
    std::vector<real_t> grad(const Point &, const GeomElement&) const; //!< compute gradient of extension function at a point knowing element

    void print(std::ostream&) const;
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream& out, const Extension& ext)
    {ext.print(out); return out;}
};


// structure to handle some data related to an extension, useful in FE computation
class ExtensionData
{
    public:
    const Extension* extension;            //!< pointer to extension
    std::vector<real_t> w;                 //!< shape functions at a given point
    std::vector< std::vector<real_t> > dw; //!< derivatives of shape functions at a given point
    std::vector< std::vector<real_t> > d2w;//!< second derivatives of shape functions at a given point
    std::vector<Point> nodes;              //!< nodes of a Lagrange element

    bool compute(const Extension*, GeomElement*, std::vector<real_t>::const_iterator, bool der1=false, bool der2=false); //!< update w, dw and nodes, return false if not in extension
};

/*=======================================================================================
  Perform extension of a function f from a domain Ds to an other domain De, as follows
      f_ext(M) = f(g(M)) o h(M)
  where g : Rn->Rn a map linking De to Ds
        h : any function
        o : consistent operation between f and h, generally *

  A simple case is the constant cartesian extension :
            ^ x,y
            |----------------|
    (x,y,z0)|<--- (x,y,z)    |        g(x,y,z) = (x,y,z0)
            |                |        h(x,y,z) = 1.
          Ds|       De       |        o = *
            |----------------|-> z
           z0

  May be also a map from domains of same dimension :
            ^ x,y
            |                |
    (x,y,z0)|<---------------|(x,y,z)     g(x,y,z) = (x,y,z0)
            |                |            h(x,y,z) = 1.
          Ds|                |De          o = *
            |                |-> z
           z0
  =======================================================================================*/

//evaluate at a given point P
Value extension(const Function& f, const Function& g, const Point& P);
Value extension(const Function& f, const Function& g, const Point& P, const Function& h);
Value extension(const TermVector& f, const Function& g, const Point& P);
Value extension(const TermVector& f, const Function& g, const Point& P, const Function& h);
//evaluate at 'node' of domain dom related to Lagrange interpolation supported by u
TermVector extension(const Function& f, const Function& g, const GeomDomain& dom, const Unknown& u);
TermVector extension(const Function& f, const Function& g, const GeomDomain& dom, const Unknown& u, const Function& h);
TermVector extension(const TermVector& f, const Function& g, const GeomDomain& dom, const Unknown& u);
TermVector extension(const TermVector& f, const Function& g, const GeomDomain& dom, const Unknown& u, const Function& h);

} // end of namespace xlifepp

#endif // EXTENSION_HPP_INCLUDED
