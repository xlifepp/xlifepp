/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file iomesh.cpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 5 aug 2012
  \date 28 mar 2013

  \brief Implementation of xlifepp::Mesh class members and related functions about input / outputs

  The main supported formats are:
    - msh (mesh format produced by the mesher gmsh)
    - mel (format of melina library, mother of xlife++)
    - vtk (graphic format, dealt by paraview for instance)
    - vtu (graphic format, dealt by paraview for instance)
*/

#include "../Mesh.hpp"

namespace xlifepp
{
/*!
  save mesh to file in format _msh, _melina ,_vtk or _vtu
  when withDomains is true, domain informations are also exported
*/

void buildParamSaveToFile(const Parameter& p, IOFormat& iof, bool& aFilePerDomain)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_format:
    {
      switch (p.type())
      {
        case _integer:
        case _enumIOFormat:
          iof=IOFormat(p.get_n());
          break;
        default:
          error("param_badtype", words("value", p.type()), words("param key", p.key()));
      }
      break;
    }
    case _pk_aFilePerDomain:
    {
      aFilePerDomain=true;
      break;
    }
    case _pk_aUniqueFile:
    {
      aFilePerDomain=false;
      break;
    }
    default:
    {
      where("saveToFile(...)");
      error("unexpected_param_key", words("param key", p.key()));
    }
  }
}

void Mesh::saveToFile(const string_t& filename, const std::vector<Parameter>& ps) const
{
  trace_p->push("saveToFile(String filename, Mesh m, ...)");
  IOFormat iof=_undefFormat;
  bool withDomains=false;

  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_format);
  params.insert(_pk_aFilePerDomain);
  params.insert(_pk_aUniqueFile);

  for (number_t i=0; i < ps.size(); ++i)
  {
    buildParamSaveToFile(ps[i], iof, withDomains);
    ParameterKey key=ps[i].key();
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) error("unexpected_parameter", words("param key", key));
      else error("param_already_used", words("param key", key));
    }
    usedParams.insert(key);
    // user must use aFilePerDomain or aUniqueFile, not both
    if (key==_pk_aFilePerDomain && usedParams.find(_pk_aUniqueFile) != usedParams.end())
    { error("param_conflict", words("param key",key), words("param key",_pk_aUniqueFile)); }
    if (key==_pk_aUniqueFile && usedParams.find(_pk_aFilePerDomain) != usedParams.end())
    { error("param_conflict", words("param key",key), words("param key",_pk_aFilePerDomain)); }
  }

  // string_t filen = trim(filename);
  std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());

  // file extension has priority to key _format
  if (rootext.second == "vtk")
  {
    if (iof!=_undefFormat && iof!=_vtk) { warning("file_extension_priority", "vtk", words("ioformat", iof), words("ioformat", _vtk)); }
    iof=_vtk;
  }
  if (rootext.second == "vtu")
  {
    if (iof!=_undefFormat && iof!=_vtu) { warning("file_extension_priority", "vtu", words("ioformat", iof), words("ioformat", _vtu)); }
    iof=_vtu;
  }
  else if (rootext.second == "msh")
  {
    if (iof!=_undefFormat && iof!=_msh) { warning("file_extension_priority", "msh", words("ioformat", iof), words("ioformat", _msh)); }
    iof=_msh;
  }
  else if (rootext.second == "mel")
  {
    if (iof!=_undefFormat && iof!=_mel) { warning("file_extension_priority", "msh", words("ioformat", iof), words("ioformat", _mel)); }
    iof=_mel;
  }
  else
  {
    if (rootext.second == "") {
      #ifdef XLIFEPP_WITH_GMSH
        warning("file_default_extension", words("ioformat", _msh));
        iof=_msh;
        rootext.second = "msh";
      #else
        warning("file_default_extension", words("ioformat", _vtk));
        iof=_vtk;
        rootext.second = "vtk";
      #endif
    }
    else
    {
      if (iof == _undefFormat)
      {
        #ifdef XLIFEPP_WITH_GMSH
          warning("file_default_extension", words("ioformat", _msh));
          iof=_msh;
          rootext.second = "msh";
        #else
          warning("file_default_extension", words("ioformat", _vtk));
          iof=_vtk;
          rootext.second = "vtk";
        #endif
      }
      else
      {
        switch (iof)
        {
          case _matlab:
            rootext.second = "m";

          case _vtk:
            rootext.second = "vtk";
            break;
          case _vtu:
            rootext.second = "vtu";
            break;
          case _vizir4:
            rootext.second = "sol";
            break;
          case _xyzv:
            rootext.second = "xyzv";
            break;
          case _msh:
            rootext.second = "msh";
            break;
          default:
            break;
        }
      }
    }
  }
  string_t filename2=fileNameFromComponents(rootext.first, rootext.second);

  switch (iof)  // real export
  {
    case _vtk: saveToVtk(filename2, withDomains); break;
    case _vtu: saveToVtu(filename2, withDomains); break;
    case _msh: saveToMsh(filename2, withDomains); break;
    case _vizir4: saveToVizir4(filename2, withDomains); break;
    case _mel: saveToMel(filename2, withDomains); break;
    default:
      error("bad_format", words("ioformat",iof));
  }
  trace_p->pop();
}

void Mesh::saveToFile(const string_t& filename, IOFormat iof, bool withDomains) const
{
  trace_p->push("saveToFile(String filename, Mesh m, ...)");
  string_t filen = trim(filename);
  std::pair<string_t, string_t> rootext = fileRootExtension(filen, Environment::authorizedSaveToFileExtensions());
  string_t ext = rootext.second;

  if (ext == "" && iof == _undefFormat)
  {
    #ifdef XLIFEPP_WITH_GMSH
      iof=_msh;
    #else
      iof=_raw;
    #endif
  }

  // if given, file extension is taken first into account for the file format
  if (ext == "vtk") { iof = _vtk; }
  if (ext == "vtu") { iof = _vtu; }
  if (ext == "msh") { iof = _msh; }
  if (ext == "mesh") { iof = _vizir4; }
  if (ext == "mel") { iof = _mel; }

  if (ext != "") { filen = rootext.first; }

  switch (iof)  // real export
  {
    case _vtk: saveToVtk(filen, withDomains); break;
    case _vtu: saveToVtu(filen, withDomains); break;
    case _msh: saveToMsh(filen, withDomains); break;
    case _vizir4: saveToVizir4(filen, withDomains); break;
    case _mel: saveToMel(filen, withDomains); break;
    default:
      error("bad_format",words("ioformat",iof));
  }
  trace_p->pop();
}

/*!
   export nodes to a file as:
   x1 y1 z1
   ...
   xn yn zn
*/
void Mesh::exportNodes(const string_t& filename) const
{
  string_t filen = trim(filename);
  std::ofstream out(filen.c_str());

  for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++)
    for (number_t j=0;j<it_node[0].size();j++)  out << " " << (*it_node)[j];
  out.close();
}

//export domain to msh file
void saveToFile(const string_t& filename, const GeomDomain& dom, IOFormat iof)
{
  string_t filen = trim(filename);
  std::pair<string_t, string_t> rootext = fileRootExtension(filen, Environment::authorizedSaveToFileExtensions());
  string_t ext = rootext.second;
  if (ext == "") filen+=".msh";
  else if (ext != "msh")
  {
    where("saveToFile(String, Domain, IOFormat)");
    error("bad_format",ext);
  }
  const Mesh* mp=dom.mesh();
  std::ofstream fout(filen.c_str());
  mp->mshExport(dom,fout);
  fout.close();
}

//! test if a domain is to be exported in a file
bool Mesh::isDomainToBeExported(const GeomDomain& dom) const
{
  dimen_t maxdim=0, nmaxdim=0;
  for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
  {
    dimen_t d=(*it_dom)->dim();
    if (d == maxdim) {nmaxdim++;}
    else if (d>maxdim) {maxdim=d; nmaxdim=1;}
  }
  // if a domain name begins with a sharp, it is an internal domain (supposed to be the largest domain, with the maximal dimension)
  // it is saved only if there is not another domain with the maximal dimension
  if (dom.name().find("#") != 0) return true;
  else return (dom.dim() == maxdim && nmaxdim == 1);
}

} // end of namespace xlifepp
