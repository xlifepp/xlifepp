/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ElementBlock.cpp
  \authors D. Martin, Y. Lafranche
  \since 21 may 2009
  \date 18 sep 2013

  \brief Implementation of xlifepp::iomel::ElementBlock class members and related functions
*/

#include "ElementBlock.hpp"
#include "StringInput.hpp"

namespace xlifepp {
namespace iomel {
using std::vector;

/*
--------------------------------------------------------------------------------
 Constructor
--------------------------------------------------------------------------------
*/
//! default constructor
ElementBlock::ElementBlock() :  nb_elts_(0)
{
  #ifdef ME_DEBUG_ON
    theLogStream << " +ElementBlock constructor() @ this=" << this << eol;
  #endif        
}

//! Strings corresponding to the ShapeType identifiers (defined in config.hpp) taken into account by Melina format.
std::pair<string_t, ShapeType> shapeList[] = {
  std::make_pair("se", _segment    ),
  std::make_pair("tr", _triangle   ),
  std::make_pair("qu", _quadrangle ),
  std::make_pair("te", _tetrahedron),
  std::make_pair("he", _hexahedron ),
  std::make_pair("pr", _prism      )
  };

std::map<string_t, ShapeType> ElementBlock::shapesOfElements_(shapeList,shapeList + sizeof(shapeList)/sizeof(std::pair<string_t, ShapeType>));

// return number of string shape s (i.e. rank of s in shapesOfElements_)
ShapeType ElementBlock::getShapeNum(const string_t& s) {
  string_t begs(s.substr(0,2)); // 2 first characters
  if (shapesOfElements_.count(begs) > 0) { return shapesOfElements_.at(begs); }
  else                                   { return _noShape; }
}

/*
--------------------------------------------------------------------------------
 Reads type of element and number of element of an element block
 Syntax:
    Bloc 
    <
      < Segme[nt[s]] | Trian[gle[s]] | Quadr[angle[s]] 
      | Tetra[edre[s]|edr[a|on]] | Prism[e][s] | Hexae[dre[s]|dr[a|on]] >
      [de] Lagrange [ [aux] Points de Gauss-Lobatto ]
      < < P[1|2|3] ^ Q[1|..|10] > ^ < Ord[re|er] | Degre[e] > K >
    ^ [Type] [Geometrique] <Se|Tr|Qu|Te|Pr|He><01|02>
    > : N Elements
--------------------------------------------------------------------------------
*/
dimen_t ElementBlock::read(StringInput& mi)
{
  trace_p->push("ElementBlock::read");
  vector<string_t> expect_ordre_degre, expect_points_abscisses;
  expect_ordre_degre.push_back("ordre");
  expect_ordre_degre.push_back("degre");
  expect_points_abscisses.push_back("points");
  expect_points_abscisses.push_back("abscisses");
  FEType i_t = Lagrange;
  FESubType i_st = standard;
  dimen_t i_n=0;

  // read word following "bloc"
  mi.nextWord();

  if ( ( shape_num_ = getShapeNum( (mi.value()).substr(0,4)) ) != _noShape )
  {
    /*
        Element given by shape name then interpolation
     
        default Interpolation Type is "Lagrange"
     */
    i_t = Lagrange;
    mi.nextWord();
    if ( mi.value() == "lagrange" ) { mi.nextWord();}
    if ( findString((mi.value()).substr(0,3), expect_ordre_degre) != -1 )
    {  //reading "ordre k" to define interpolation number (k)
      mi.notWord('i');
      i_n = mi.integerValue();
    }
    else
    {
      if ( findString(mi.value(), expect_points_abscisses) != -1 )
      {
        mi.nextWord();
        if ( mi.value() == "gauss-lobatto" ) 
        {
          i_st = GaussLobattoPoints;
          mi.nextWord();
        }
        else if ( mi.value().substr(0,11) == "equidistant" )
          mi.nextWord();
      }
      // reading Pk or Qk to define interpolation number (k)
      if ( (mi.value()).find_first_of("pq") == 0 )
        i_n = stringto<int>((mi.value()).substr(1));
    }
  }
  else
  {
    /*
        Element given by [Type] [Geometrique] A where A is a 4 char string 
        [se | tr | qu | te | pr | he ] concatenated with a 2-digit number
      
        In this case Interpolation Type is "Lagrange"
     */
    i_t = Lagrange;
    if ( mi.value()== "type" ) mi.nextWord();
    if ( mi.value()== "geometrique" ) mi.nextWord();
    if ( ( shape_num_ = getShapeNum( (mi.value()).substr(0,2)) ) != _noShape )
      i_n = stringto<dimen_t>((mi.value()).substr(2));
  }
  // define interpolation for Block
  interpolation_p = findInterpolation(i_t, i_st, i_n, H1);
    
  // Number of elements of block
  mi.notWord('i');
  nb_elts_ = mi.integerValue();
  mi.nextWord();

  trace_p->pop();
  return i_n;
}

} // end of namespace iomel

} // end of namespace xlifepp
