/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ElementBlock.hpp
  \authors D. Martin, Y. Lafranche
  \since 26 may 2011
  \date 18 sep 2013

  \brief Definition of the xlifepp::iomel::ElementBlock class

  This small class is a auxiliary class only used in load mesh files in Melina format
  and to create output mesh files acording to Melina format 
  It is used to make a partition of the (geometric) elements of a mesh each part 
  containing consecutive (geometric) element of the same type.
  A homogeneous mesh in which all gemetric elements share the same type
  (such as a mesh containing only P_k triangles or a mesh containing only Q_k hexehedra)
  has only 1 element block containing all the elements.
 
  Examples:
  ----------
  - A homogeneous mesh in which all gemetric elements share the same type
    (such as a mesh containing only P_k triangles or a mesh containing only Q_k hexahedra)
    has only 1 element block containing all the elements.
  - A mesh where first 20 elements are P_1 triangles, the next 30 elements  are
    Q_1 quadrangles and the last 40 elements are P_1 triangles has 3 element block's
    containing 20, 30 and 40 elements
 
 member functions
 ----------------
  - read   reads type of element and number of element of an element block
*/

#ifndef ELEMENT_BLOCK_HPP
#define ELEMENT_BLOCK_HPP

#include "finiteElements.h"

namespace xlifepp {
class Interpolation;

//! namespace dedicated to management of Melina mesh format
namespace iomel {
class StringInput;

/*!
  \class ElementBlock
*/
struct ElementBlock
{
   ShapeType shape_num_; //!< shape number of block elements
   Interpolation* interpolation_p; //!< interpolation characteristics (type, number and conforming map number)
   number_t nb_elts_; //!< number of elements in block

   static std::map<string_t, ShapeType> shapesOfElements_; //!< element shapes managed by MELINA format: string keywords and corresponding enum items
/*
--------------------------------------------------------------------------------
 Constructor, Destructor
--------------------------------------------------------------------------------
*/
   ElementBlock(); //!< default constructor

   ShapeType getShapeNum(const string_t& s); //!< return number of string shape

   dimen_t read(StringInput&); //!< read element block definition with StringInput class from a mesh input file (Melina format)
   
}; // end of class ElementBlock =================================================

} // end of namespace iomel

} // end of namespace xlifepp

#endif /* ELEMENT_BLOCK_HPP */
