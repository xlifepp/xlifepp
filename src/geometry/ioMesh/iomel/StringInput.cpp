/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file StringInput.cpp
  \authors D. Martin, Y. Lafranche
  \since 28 feb 2005
  \date 10 sep 2013

  \brief Implementation of xlifepp::iomel::StringInput class members and related functions
*/

#include "StringInput.hpp"
#include "utils.h"
#include <cctype>   // to use isalpha(), isdigit() ... C-functions

namespace xlifepp {
namespace iomel {
using std::string;

/*
--------------------------------------------------------------------------------
  Constructor, destructor
--------------------------------------------------------------------------------
*/
//! constructor by filename (opens stream)
StringInput::StringInput(const string& filename)
: si_file_(filename), si_string_(""), si_value_(" "), si_code_('?'),
  si_pos1_(1), si_pos2_(0), si_prev_string_(""), verbose_level_(0),
  separators_("=#:;"), left_delimiters_("([{<"), right_delimiters_(")]}>"),
  eol_chars_("!%@"), stop_words_(":arret:abort:stop:"), end_words_(":fin:bye:end:"),
  ignored_words_(":au:aux:d:de:des:du:en:et:l:le:la:les:par:sur:un:une:and:at:by:from:in:of:off:on:onto:out:over:the:to:upon:")
{
  si_stream_.open(filename.c_str(), std::ios::in);
  if ( !si_stream_ ) { error("file_failopen","StringInput::StringInput",filename); }
}

//! destructor (closes stream)
StringInput::~StringInput()
{ 
  if ( si_stream_ ) si_stream_.close();
}

/*
--------------------------------------------------------------------------------
Reads next (blank separated) string from current file input stream
Returns void pointer "ifstream" si_stream_.
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::get()
{
  if ( si_stream_ >> si_string_ )
  { 
    si_pos1_=0;
    si_pos2_=si_string_.size()-1;
  }
  return si_stream_;
}

/*
--------------------------------------------------------------------------------
 Advance to next string either as a (blank separated) string from file stream 
 or as a substring (starting at si_pos1_) in previouly read string
 Returns void pointer "ifstream" si_stream_.
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::next()
{
  si_prev_string_=si_value_;
  if ( si_pos1_ > si_pos2_ ) get();
  si_code_='?';
  return si_stream_;
}

/*
--------------------------------------------------------------------------------
Search for an embedded expression (char delimited version)
--------------------------------------------------------------------------------
*/
void StringInput::embeddedExpr(const char l_delim, const char r_delim)
{
  int si_c_lvl(1);
  number_t pos_ld(0),pos_rd;
  number_t l_delim_size(1), r_delim_size(1);
  si_value_=("");
  
  // looking forward for a terminal delimiter
  while (si_c_lvl != 0)
  {
    if ( (pos_rd=si_string_.find(r_delim,si_pos1_+l_delim_size)) != string::npos )
    { // a r_delim was found in current string
      si_value_=si_value_+si_string_.substr(si_pos1_,pos_rd-si_pos1_+r_delim_size);
      // next string starts after right delimiter
      si_pos1_=pos_rd+r_delim_size;
      si_c_lvl--;
    }
    else
    {
      si_value_=si_value_ + si_string_.substr(si_pos1_,si_pos2_-si_pos1_+1);
      // looking for a r_delim in subsequent input strings
      while ( get() && (pos_rd=si_string_.find(r_delim)) == string::npos )
        si_value_ += " " + si_string_ ;
      si_value_ += " " + si_string_.substr(si_pos1_,pos_rd-si_pos1_+r_delim_size);
      // next string starts after right delimiter
      si_pos1_=pos_rd+r_delim_size;
      si_c_lvl--;
    }
    // multiply embedded expression (e.g. like this (or that))
    if ( (pos_ld=si_value_.find(l_delim,pos_ld+l_delim_size)) != string::npos
    && (pos_ld < si_value_.size()-l_delim_size) ) si_c_lvl++;
  }
}

/*
--------------------------------------------------------------------------------
Search for an embedded expression (string delimited version)
--------------------------------------------------------------------------------
*/
void StringInput::embeddedExpr(const string l_delim, const string r_delim)
{
  int si_c_lvl(1);
  number_t pos_ld=0, pos_rd;
  number_t l_delim_size(l_delim.size()),r_delim_size(r_delim.size());
  //
  si_value_="";
  // lookin forward for a terminal delimiter
  while ( si_c_lvl != 0 )
  {
    if ( (pos_rd = si_string_.find(r_delim, si_pos1_+l_delim_size)) != string::npos )
    {   // a r_delim was found in current string
      si_value_ = si_value_ + si_string_.substr(si_pos1_, pos_rd-si_pos1_+r_delim_size);
      // next string starts after right delimiter
      si_pos1_ = pos_rd+r_delim_size;
      si_c_lvl--;
    }
    else
    {
      si_value_ = si_value_ + si_string_.substr(si_pos1_, si_pos2_-si_pos1_+1);
      // looking for a r_delim in subsequent strings
      while ( get() && (pos_rd=si_string_.find(r_delim)) == string::npos )
        si_value_ += " " + si_string_ ;
      si_value_ += " " + si_string_.substr(si_pos1_, pos_rd-si_pos1_+r_delim_size);
      // next string starts after right delimiter
      si_pos1_ = pos_rd+r_delim_size;
      si_c_lvl--;
    }
    // multiply embedded expression (e.g. like this (or that))
    if ( (pos_ld = si_value_.find(l_delim, pos_ld+l_delim_size)) != string::npos
    && (pos_ld < si_value_.size()-l_delim_size) ) si_c_lvl++;
  }
}//@end StringInput::embeddedExpr

/*
--------------------------------------------------------------------------------
 Computes current input string code (si_code_) and value
--------------------------------------------------------------------------------
*/
void StringInput::eval()
{
  const char char_eq('=');
  char the_char, char1;
  bool is_in_number(1);
  number_t pos_cur, pos_init;
//
  char1 = si_string_.at(si_pos1_);
//
  switch ( char2code(char1) )
  {
  case 'l':     // a word (initial is a letter or _)
    si_code_ = 'w';
    pos_cur = si_pos1_;
    //si_value_ = char1;
    // only letters, _ , digits and + - . , are allowed in words
    // skipping till end of string or first non-word character
    while ( pos_cur++ < si_pos2_ 
    && ( (the_char = char2code(si_string_.at(pos_cur))) == 'l' || the_char == 'd') ) ;
    // word value are always lowercase
    si_value_ = lowercase(si_string_.substr(si_pos1_,pos_cur-si_pos1_));
    si_pos1_ = pos_cur;
    // some words are treated as separators ( ignored_words_ )
    // (To Do: d' or l' at beginning of word should be ignored)
    if ( ignored_words_.find(":"+si_value_+":") != string::npos ) 
      si_code_ = char_eq;
    break;
    
  case 'd':     // a number (starting with +, -, . or a digit)
    si_code_ = 'i';
    pos_cur = pos_init = si_pos1_;
    while ( is_in_number && pos_cur <= si_pos2_ )
    {
      the_char = si_string_.at(pos_cur);
      switch (the_char)
      {
      case ',': // a decimal comma
        si_string_.at(pos_cur) = '.';
      case '.': // a decimal dot
        si_code_ = 'r';
        si_pos1_ = ++pos_cur;
        break;
      case 'd':case 'D':case 'E': // a scientific notation (exponent)
        si_string_.at(pos_cur) = 'e';
      case 'e': // a scientific notation (exponent)
        si_code_ = 'r';
      case '+':case '-': // a sign
      case '0':case '1':case '2':case '3':case '4':
      case '5':case '6':case '7':case '8':case '9': // or digits !
        si_pos1_ = ++pos_cur;
        break;
      default: // giving up on any other char ...
        is_in_number = 0;
        pos_cur++;
        break;
      }; // end switch (the_char)
    }; // end while
    // number is now in the following string
    si_value_ = si_string_.substr(pos_init,si_pos1_-pos_init);
    if ( si_pos1_-pos_init == 1 && !isdigit(si_value_.at(0)) ) si_code_ = '?';
    break;
    
  case '"':     // a string (delimited by quotes);
    embeddedExpr(char1,char1);
    // erasing opening and closing quotes
    si_value_.erase(0,1); si_value_.erase(si_value_.size()-1,1);
    si_code_ = 's';
    // void string is a separator
    if ( si_value_.empty() ) si_code_ = char_eq;
    break;
    
  case '$':    // a mathematical expression embedded between $
    embeddedExpr(char1,char1);
    // erasing opening and closing $
    si_value_.erase(0,1); si_value_.erase(si_value_.size()-1,1);
    si_code_ = '$';
    // triming blanks in mathematical expressions
    while ( (pos_cur = si_value_.find(' ')) != string::npos) si_value_.erase(pos_cur,1);
    if ( si_value_.empty() ) si_code_ = char_eq;
    break;
    
  case '!':    //! a comment up to the end of line
    si_code_ = '!';
    si_value_ = si_string_.substr(si_pos1_,si_pos2_-si_pos1_+1);
    // concatenating chars up to the end of line
    while ( (the_char = si_stream_.get()) != '\n') si_value_ += the_char;
    // force next input string to get read
    si_pos1_ = si_pos2_+1;
    break;
    
  case '(':    // left (opening) delimiters
    // comment embedded in delimiters (see left_delimiters_, right_delimiters_)
    embeddedExpr(char1,right_delimiters_.at(left_delimiters_.find(char1)));
    si_code_ = '(';
    break;
    
  case ')':    // right (closing) delimiters (see right_delimiters_)
    si_code_ = ')';
    si_value_ = char1;
    si_pos1_++;
    break;
    
  case '/': // '/' was found, looking for a following '/' or '*'
    if ( si_pos2_ > si_pos1_ )
    {
      if ( (the_char = si_string_.at(si_pos1_+1)) == '/' )
      {  // was found: bulding string as a comment up to the end of line
        si_value_ = si_string_.substr(si_pos1_,si_pos2_-si_pos1_+1);
        // concatenating chars up to the end of line
        while ( (the_char = si_stream_.get()) != '\n') si_value_ += the_char;
        // force next input string to read
        si_pos1_ = si_pos2_+1;
        si_code_ = '!';
      }
      else if ( the_char == '*' )
      {  /* was found: building string as a comment up to next ocurrence of */
        embeddedExpr("/*","*/");
        si_code_ = '(';
      }
    }
    if ( si_code_ == '?' )
    { // no '/' or '*' was found after an initial '/'
      si_code_ = '/';
      si_value_ = '/';
      si_pos1_++;
    }
    break;
    
  case '=':    // a separator
    si_code_ = char_eq;
    si_value_ = char1;
    si_pos1_++;
    break;
    
  default: // (si_code_ = '?')
    si_value_ = char1;
    si_pos1_++;
    break;
  }
}

/*
--------------------------------------------------------------------------------
 Reads given number of lines into string (inclusive of eol chars)
--------------------------------------------------------------------------------
*/
void StringInput::readLines(const number_t nbl)
{
  char the_char;
  number_t nl(0);
  si_prev_string_ = si_value_;
  si_value_="";
  do
  {
    char eol('\n');
    while ( (the_char = si_stream_.get()) != eol) si_value_ += the_char;
    si_value_ += eol;
  } while ( nl++ < nbl );
  // strip blanks, tabs and eol chars at beginning end of string
  si_pos1_ = si_value_.find_first_not_of(" \t\n");
  si_pos2_ = si_value_.find_last_not_of(" \t\n");
  si_value_ = si_value_.substr(si_pos1_,si_pos2_-si_pos1_+1);
  si_pos1_ = 1; 
  si_pos2_ = 0;
  si_code_='?';
}
/*
--------------------------------------------------------------------------------
  Reads up to any next non-comment or non-separator
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::notComment()
{
  do 
  { si_code_='!';
    if ( next() ) { eval(); if ( verbose_level_ > 1 ) print(); }
  } while ( si_code_ == '!' || si_code_ == '(' || si_code_ == '=' );
  return si_stream_;
}
/*
--------------------------------------------------------------------------------
  Reads up to any next word-valued string
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::nextWord()
{
  do { si_code_='w'; notComment(); } while ( si_code_ != 'w' );
  if ( stop_words_.find(":"+si_value_+":") != string::npos ) stop('w');
  return si_stream_;
}
/*
--------------------------------------------------------------------------------
 Reads up to any word-valued string (current word included)
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::word()
{
  while (si_code_ != 'w') nextWord();
  return si_stream_;
}
// Reads up to a specified word-valued string (as a constant char*)
std::ifstream& StringInput::word (char* wd)
{
  while ( !(si_code_ =='w' && si_value_ == string(wd)) ) nextWord();
  return si_stream_;
}
// Reads up to a specified word-valued string (as a string)
std::ifstream& StringInput::word (const string& wd)
{
  while ( !(si_code_ =='w' && si_value_ == wd) ) nextWord();
  return si_stream_;
}
/*
--------------------------------------------------------------------------------
  Reads up to a word starting with a given string (as a constant char* or string)
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::wordBegin (const string& wd)
{
  while ( !(si_code_ =='w' && si_value_.find(wd) == 0) ) nextWord();
  return si_stream_;
}
std::ifstream& StringInput::wordBegin (char* wd)
{
  while ( !(si_code_ =='w' && si_value_.find(string(wd)) == 0) ) nextWord();
  return si_stream_;
}
/*
--------------------------------------------------------------------------------
  Reads up to a word containing a given part of word (as a constant char* or string)
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::wordContain (const string& wd)
{
  while ( !(si_code_ =='w' && si_value_.find(wd) != string::npos) ) nextWord();
  return si_stream_;
}
std::ifstream& StringInput::wordContain (char* wd)
{
  while ( !(si_code_ =='w' && si_value_.find(string(wd)) != string::npos) ) nextWord(); 
  return si_stream_;
}
/*
--------------------------------------------------------------------------------
  Reads up to expected next non-word, stops on reading a word (use with caution!)
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::notWord (const char cc)
{
  do { notComment(); if ( si_code_ == 'w') stop(si_code_); } while (si_code_ != cc);
  return si_stream_;
}
/*
--------------------------------------------------------------------------------
  Reads up to any next string-valued input string
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::nextString()
{
  do { si_code_='s'; notComment(); } while ( si_code_ != 's' );
  return si_stream_;
}
/*
--------------------------------------------------------------------------------
  Reads up to expected next string, stops on reading a word (use with caution!)
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::String() { return notWord('s'); }
/*
--------------------------------------------------------------------------------
  Reads up to expected next integer, stops on reading a word (use with caution!)
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::nextInteger() { return notWord('i'); }
/*
--------------------------------------------------------------------------------
  Reads up to expected next real, stops on reading a word (use with caution!)
--------------------------------------------------------------------------------
*/
std::ifstream& StringInput::nextReal() { return notWord('r'); }
/*
--------------------------------------------------------------------------------
  stops reading from file and stops program
--------------------------------------------------------------------------------
*/
void StringInput::stop(const char cc)
{
  string what;
  switch (cc) {
    case 'w': what = string("word");     break;
    case 's': what = string("string");   break;
    case 'i': what = string("integer");  break;
    case 'r': what = string("real");     break;
    case '$': what = string("math expr");break;
    default: what = string("value");    break;
  }
  std::cout <<  "Error while reading expected " << what << " (" << si_value_ << ") from file " << si_file_ << std::endl;
  error("free_error","StringInput::stop: unexpected read", what, si_value_, si_file_);
}
/*
-------------------------------------------------------------------------------
  public inline Access functions
-------------------------------------------------------------------------------
*/
string StringInput::value() const //! current string value
{ return si_value_; } 

string StringInput::previousString() const //! previous string value
{ return si_prev_string_; }

int StringInput::integerValue() const //! current string integer value
{ return stringto<int>(si_value_); }

real_t StringInput::realValue() const //! current string real value
{ return stringto<real_t>(si_value_); }

/*
--------------------------------------------------------------------------------
  prints input string to log file according to code
--------------------------------------------------------------------------------
*/
void StringInput::print() { print(theLogFile); }

//! print input string to filename according to code
void StringInput::print(const string& filename)
{
  using std::endl;
  std::ofstream fs;
  fs.open(filename.c_str(),std::ios::app);
  if ( fs )
  {
    switch (si_code_)
    {
    case 'w': fs <<"\t\tWord "<<si_value_<<endl; break;
    case 's': fs <<"\t\tString "<<si_value_<<endl; break;
    case 'i': fs <<"\t\tInteger = "<< stringto<int>(si_value_)<<endl; break;
    case 'r': fs <<"\t\tReal = "<<stringto<real_t>(si_value_)<<endl; break;
    case '$': fs <<"\t\tMath exp = "<<si_value_<<endl; break;
    case '=': if (verbose_level_>5) fs <<"\t\tSeparator ("<<si_value_<<")"<<endl; break;
    case '(': if (verbose_level_>5) fs <<"\t\tEmbedded comment "<<si_value_<<endl; break;
    case '!': if (verbose_level_>5) fs <<"\t\tComment up to eol"<<si_value_<<endl; break;
    case '/': if (verbose_level_>5) fs <<"\t\tSlash"<<si_value_<<endl; break;
    default: if (verbose_level_>5) fs <<"\t\tUndefined ("<<si_value_<<")"<<endl; break;
    }
  }
}

/*
--------------------------------------------------------------------------------
  char2code returns melina++'s code of a char
--------------------------------------------------------------------------------
*/
char StringInput::char2code (const char c)
{
  dimen_t ic=c;
  if ( isalpha(c) || ic == 95 ) 
    return 'l'; // uppercase, lowercase letters or underscore
  else if ( isdigit(c) || (ic > 42 && ic < 47 ) )
    return 'd'; // digits, dot, comma, or signs
  else if ( isascii(c) )
  {
    if ( separators_.find(c) != string::npos ) 
      return separators_.at(0);      // --> separators
    if ( left_delimiters_.find(c) != string::npos ) 
      return left_delimiters_.at(0); // --> comment openers
    if ( right_delimiters_.find(c) != string::npos ) 
      return right_delimiters_.at(0);// --> comment closers
    if ( eol_chars_.find(c) != string::npos ) 
      return eol_chars_.at(0);       // --> comment to end of line
    switch (c) 
    {
      case 34:case 39:case 96:// " ' ` --> strings delimiters
        return '"'; break;
      case '$':case 47:case 92:  // --> $-dollar sign, /-slash, \-backslash
        return c; break;
      default:                 // --> any other ascii 0-127 char
        return ' '; break;
    }
  }
  else
  {   // accentuated characters
    switch (c) 
    { // uppercase accentuated letters
      /*
       case 'À':case 'Á':case 'Â':case 'Ã':case 'Ä':case 'Å':case 'Æ':
      case 'Ç':
      case 'È':case 'É':case 'Ê':case 'Ë':
      case 'Ì':case 'Î':case 'Ï':
      case 'Ñ':
      case 'Ò':case 'Ó':case 'Ô':case 'Õ':case 'Ö':case 'Œ'://:case 'Ø'
      case 'Ù':case 'Ú':case 'Û':case 'Ü':
        return 'l';
      case 'à':case 'á':case 'â':case 'ã':case 'ä':case 'å':case 'æ':
      case 'ç':
      case 'è':case 'é':case 'ê':case 'ë':
      case 'ì':case 'í':case 'î':case 'ï':
      case 'ñ':
      case 'ò':case 'ó':case 'ô':case 'õ':case 'ö':case 'œ':// case:'ø':
      case 'ù':case 'ú':case 'û':case 'ü':
        return 'l';
      */
      default:    // --> any other
        return '0'; break;
    }
  }
}

} // end of namespace iomel

} // end of namespace xlifepp
