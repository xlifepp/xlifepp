/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file StringInput.hpp
  \authors D. Martin, Y. Lafranche
  \since 28 feb 2005
  \date 10 sep 2013

  \brief Definition of the xlifepp::iomel::StringInput class

  This class is used by the function xlifepp::Mesh::loadMelina to read data from a file
  containing a mesh in Melina format.

  member functions
  ----------------
    - embeddedExpr search for an embbedded expression (char or string version)
    - eval         compute current input string code (si_code_) and value
    - get          read next string from current file input stream
    - notComment   read up to any next non-comment or non-separator
    - next         advance to next string either as a string from file stream
                   or as a substring of current input string
    - nextInteger  read up to expected next integer, stops on reading a word
    - nextReal     read up to expected next real, stops on reading a word
    - nextString   read up to any next string-valued input string
    - nextWord     read up to any next word-valued string
    - notWord      read up to expected next non-word, stops on reading a word
    - print        print input string to log file according to code 
    - readLines    read given number of lines into string
    - stop         stop reading from file and stops program
    - String       read up to expected next string, stops on reading a word
    - word         read up to any word-valued string (current word included)
    - wordBegin    read up to a word starting with a given string
    - wordContain  read up to a word containing with given part of word
*/

#ifndef STRING_INPUT_HPP
#define STRING_INPUT_HPP

#include "config.h"

#include <string>   // Class string of C++ Standard Library
#include <iostream> // input/output stream
#include <fstream>  // input/output stream from/to file

namespace xlifepp {

namespace iomel {

/*!
  \class StringInput
  This class is used by the function Mesh::loadMelina to read data from a file
  containing a mesh in Melina format.
*/
class StringInput
{
  public:
    std::ifstream si_stream_; //!< input file stream
  private:
    std::string si_file_; //!< input file name
    std::string si_string_; //!< current input string
    std::string si_value_; //!< current active part of input string
    char si_code_; //!< w word (initial is a letter or _) always output as lowercase string
        // s string (delimited by quotes)
        // i integral value (only sign & digits)
        // r float value i.e a string reading as [@][ddd].[ddd][e[@]ddd] 
        //   where @ is + or -, 
        //         d is any digit, 
        //         [ddd] any sequence of digits
        // $ mathematical expression, delimited by $
        // = separator, blank string or $$
        // ( embedded comment, delimited by (), {}, [], <> or C++ delimiters
        // ! comment up to the end of line
        // / slash(/)
        // ? undefined
    size_t si_pos1_; //!< delimiting positions in current input string
    size_t si_pos2_; //!< delimiting positions in current input string
    std::string si_prev_string_; //!< previous string
    mutable number_t verbose_level_; //!< verbose level

  /*
    Constant string declarations for Melina istream input
  */
  //! separators will be ignored on istream input
  std::string separators_;        // =#:;
  //@{
  //! strings from l_delimiter up to the corresponding r_delimiter will be ignored   
  std::string left_delimiters_;   // ([{<
  std::string right_delimiters_;  // )]}>
  //@}
  //! strings up to the end of line will be ignored
  std::string eol_chars_;         // !%@
  //! imply stop (exit(1)) on istream input
  std::string stop_words_;        // :arret:stop:
  //! end istream input
  std::string end_words_;         // :fin:bye:end:
  //! ignored on istream input
  std::string ignored_words_; // :au:aux:d:de:des:du:en:et:l:le:la:les:par:sur:un:une
                              // :and:at:by:from:in:of:off:on:onto:out:over:the:to:upon:
   
public:
/*
--------------------------------------------------------------------------------
 Constructors, Destructors
--------------------------------------------------------------------------------
*/
  StringInput(); //!< default constructor
  StringInput(const std::string& filename); //!< constructor by file name (opens stream)
  ~StringInput(); //!< destructor (closes stream)

/*
--------------------------------------------------------------------------------
   public inline Access functions
--------------------------------------------------------------------------------
*/
  std::string endWords() //! return end words
    { return end_words_; }
  char code() const //! current string code
    { return si_code_; }
  std::string value() const; //!< current string value
  std::string previousString() const; //!< previous string value
  int integerValue() const; //!< current string integer value
  real_t realValue() const; //!< current string real value
  std::string File() const //! return file name
    { return si_file_; }
  number_t verboseLevel() const //! returns verbose level
    { return verbose_level_; }
  void setVerboseLevel(const number_t vb) const //! sets verbose level
    { verbose_level_ = vb; }

/*
--------------------------------------------------------------------------------
   public Member Utility functions
--------------------------------------------------------------------------------
*/
   std::ifstream& next(); //!< get next value of string
   void eval(); //!< computes current input string code (si_code_) and value
   void readLines(const number_t n); //!< read next n lines from input file
   std::ifstream& notComment(); //!< find next non-comment string in input file
   std::ifstream& nextWord(); //!< find next word in input file
   std::ifstream& word(); //!< find word in input file
   std::ifstream& word(char*); //!< find given (char*) word in input file
   std::ifstream& word(const std::string&); //!< find given (string) word in input file
   std::ifstream& wordBegin(char*); //!< find (char*)word as the beginning of a string in input file
   std::ifstream& wordBegin(const std::string&); //!< find (string)word as the beginning of a string
   std::ifstream& wordContain(char*); //!< find (char*)word as substring of a string in input file
   std::ifstream& wordContain(const std::string&); //!< find (string)word as substring of a string
   std::ifstream& notWord(const char); //!< find next input string of type 'char'
   std::ifstream& String(); //!< get next string-valued string
   std::ifstream& nextString(); //!< find next string-valued string
   std::ifstream& nextInteger(); //!< find next integer-valued string
   std::ifstream& nextReal(); //!< find next real-valued string
   void  stop(const char); //!< stop on reading (type 'char' string found)
   void  print(const std::string&); //!< print input string to filename according to code
   void  print(); //!< main routine to print

   char char2code(const char); //!< char2code returns melina++'s code of a char
   /*  Output
      l for a letter (lowercase, uppercase or an underscore)
      d for a digit or + - . ,
      = for a separator, i.e. # : ; =
      " for a quote delimiting string
      ( for an opening char i.e. ( < [ {
      ) for a terminating char, i.e.  ) > ] }
      ! for a up-to-eol comment, i.e. ! # % or //
      $ for $
      \ for \
      / for /
      0 for all others chars
    */
   
  private:
  /*
  --------------------------------------------------------------------------------
    private member functions
  --------------------------------------------------------------------------------
  */
   std::ifstream& get(); //!< get next string from input file
   void  embeddedExpr(const char, const char); //!< find expressions enclosed between chars
   void  embeddedExpr(const std::string, const std::string); //!< find expressions enclosed between strings
    
}; // end of class StringInput

} // end of namespace iomel

} // end of namespace xlifepp

#endif /* STRING_INPUT_HPP */
