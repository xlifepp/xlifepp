/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file vizir4.cpp
  \author N. Kielbasiewicz C. Chambeyron
  \since 31 mai 2024
  \date 31 mai 2024

  \brief Implementation of xlifepp::Mesh class members related to input and output mesh file format (vizir4)

  \verbatim
  ============================================================================
                               MSH 2.0 format
  ============================================================================
  $MeshFormat
  MeshVersionFormatted 3
  $EndMeshFormat
  Dimension
  $Dimension
  $Vertices
  number-of-vertices
   x-coord y-coord z-coord ref
  ...
  
  $Elements
  number-of-elements
  num-vertices-list ref
  ...
  
  $End
  \endverbatim

  ============================================================================
                               MSH 2.1 format
  ============================================================================
  the only change concerns the physical names definition:

  \verbatim
  $PhysicalNames
  number-of-physical-names
  physical-dimension physical-number "physical-name"
  ...
  $EndPhysicalNames
  \endverbatim

*/

#include "../Mesh.hpp"

namespace xlifepp
{
//save mesh and related domains to files
void Mesh::saveToVizir4(const string_t& filename, bool withDomains) const
{
  std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
  string_t froot = rootext.first;
  if(rootext.second!="msh") froot=filename;
  string_t f=froot+".mesh"; 
  std::ofstream out(f.c_str());
  out.precision(fullPrec);
  out << "MeshVersionFormatted 3"<<eol<<eol;
  out <<"Dimension " << eol << meshDim()<<eol << eol;
  vizir4Export(out,withDomains);
  out.close();
  if (withDomains && meshDim() > 1) //save domains in files filename_domainname.msh
  {
    for (number_t d = 0; d < domains_.size(); d++) 
    {
      string_t dn = domains_[d]->nameWithoutSharp();
      if (dn == "") { dn = "domain_" + tostring(d); }
      f = froot + "_" + dn + ".mesh";
      out.open(f.c_str());
      out.precision(fullPrec);
      out << "MeshVersionFormatted 3"<<eol<<eol;
      out <<"Dimension" << eol << meshDim()<<eol << eol;
      vizir4Export(*domains_[d],d, out);
      out.close();
    }
  }
}

void Mesh::vizir4Export(std::ostream& out, bool withDomains) const
{
  // save vizir4 header
  number_t i=1, nbDoms=0;
  std::map<number_t, string_t> elemInfo;
  std::map<string_t, number_t> sideIndex;
  std::map<string_t, number_t>::iterator itsn;
  string_t MeshElemNature="", Men0=MeshElemNature;
  string_t  Men1;
  string_t SideElemNature="", Sen0=SideElemNature;
  string_t  Sen1;
  dimen_t d;

  std::vector<string_t> MeshNature;
  MeshNature.push_back("");
  std::vector<number_t> NbNature;
  number_t nbEN=0;
  number_t pl=0;
  NbNature.push_back(0);
  number_t nbside=0;
  MeshElement* melt;
  GeomElement* sideT;

  dimen_t maxdim=0, nmaxdim=0;
  for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
  {
    d=(*it_dom)->dim();
    if(d== maxdim) {nmaxdim++;}
    else if (d>maxdim) {maxdim=d; nmaxdim=1;}
    nbDoms++;
  }
  nbDoms=0;
  for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
  {
    d=(*it_dom)->dim();
    if ((*it_dom)->domType()==_meshDomain && isDomainToBeExported(*(*it_dom)))
    {
      nbDoms++;
      // to write elements info in an ordered way, we prepare each output line in elemInfo
      for (std::vector<GeomElement*>::iterator it_elem=(*it_dom)->meshDomain()->geomElements.begin(); it_elem!=(*it_dom)->meshDomain()->geomElements.end(); ++it_elem)
      {
        std::stringstream ss(std::stringstream::out);
        // shape info and interpolation order are available trough parent element if it is not a MeshElement
        if((*it_elem)->hasMeshElement())
        { 
          Men1=vizir4Nature(vizir4Type((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype));
        }
        else
        { 
          std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
          Men1=vizir4Nature(vizir4Type(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype));
        }
        if (Men1 != Men0 ) 
        { 
          MeshNature.push_back(Men1);
          nbEN=1;
          pl++;
          NbNature.push_back(1);
        }
        else
        {
          nbEN++;
          NbNature[pl]=nbEN;
        }
        Men0=Men1;
        // if (d==3)
        if (d >1)
        { 
            melt = (*it_elem)->meshElement();
            for (number_t s = 0; s < (*it_elem)->numberOfSides(); s++)   // loop on element sides
            {
              string_t key = (*it_elem)->encodeElement(s + 1); // encode vertex numbers of side s
              itsn = sideIndex.find(key);                // search side in sideIndex map
              if (itsn == sideIndex.end())               // create a new side element
              {
                // sideT=new GeomElement(*it_elem, s + 1, ++(*it_elem)->meshP()->lastIndex_);
                nbside++;
                sideIndex[key] = nbside;             // update sideIndex
                melt->sideNumbers[s] = nbside;       // update sideNumbers
              }
              else // here, side already exists so it is not on the boundary side so it is erased.
              {
                nbside--;
                sideIndex.erase (key);                  // erasing by key
              } // end of else
            } // end for (number_t s
            if(Men1=="Tetrahedra")
            {
              Sen1="Triangles";
            }
            else if(Men1=="Hexahedra")
            {
              Sen1="Quadrilaterals";
            } 
            else if(Men1=="Triangles")
            {
              Sen1="Edges";
            }
            else if(Men1=="Quadrilaterals")
            {
              Sen1="Edges";
            } 
            // Sen1= vizir4Nature(vizir4Type(sideT->shapeType(),(*it_elem)->refElement()->interpolation().numtype));
            // Sen1="Triangles";
            if (Sen1 != Sen0 ) SideElemNature=SideElemNature+Sen1;
            Sen0=Sen1;
        }
        if((*it_elem)->hasMeshElement())
        { 
            for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); i++) 
            {
              ss <<  (*it_elem)->meshElement()->nodeNumbers[i]<<" " ; 
            } 
            if(withDomains) 
            {
              ss<<" "<<nbDoms<<eol;
            }
            else
            {
              ss<<" "<<domains().size()<<eol;
            }
            // ss << eol;
        }
        else
        {
          std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
          if (it_gnp->first->hasMeshElement())
          {
            MeshElement* meshE=it_gnp->first->meshElement();
            for (number_t i=0; i<meshE->refElement()->sideDofNumbers_[it_gnp->second-1].size(); i++)
            {
              ss << meshE->nodeNumbers[meshE->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1]<<" " ;
            }
            ss<<" "<<nbDoms<<eol;
          }
          else
          {
            // case of side of side
            warning("sos_elt_not_handled");
          }
        }
        if (elemInfo.find((*it_elem)->number()-1) == elemInfo.end())
        {
          elemInfo[(*it_elem)->number()-1]=ss.str();
        }
      }
      ++i;
    }
  }

  // nodes list
  out << "Vertices" << std::endl << nbOfNodes() << std::endl;
  i=1;

  for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
  {
    for (number_t j=0; j<d; ++j) { out << (*it_node)[j]<< " " ; }
    out <<1 << std::endl;
  }// list of sides of element if dimMesh>2
  // if (d>2)
  // {
    out <<" "<<eol<<SideElemNature << eol << nbside << eol;
    for (std::map<string_t , number_t>::const_iterator it=sideIndex.begin(); it != sideIndex.end(); ++it) 
    {
       out << it->first << " " <<nbDoms<<eol;
    }
  // }
  sideIndex.clear();
  //
  if(withDomains)
  {
  // std::ofstream outBord;
  // outBord << out;
  // outBord.open(fBord.c_str());
  // outBord.precision(fullPrec);
  // outBord << out;
  }
//
  number_t jj=1, ii=1;
  out <<eol<<MeshNature[ii]<<eol<<NbNature[ii]<<eol;
  for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) 
  {
    if (jj==NbNature[ii]+1)
    {
      out << eol<<MeshNature[ii+1]<<eol<<NbNature[ii+1]<<eol;
    }
    out<< it->second;
    jj++;
  }

  out << std::endl << "End" << std::endl;
}

void Mesh::vizir4Export(const GeomDomain& dom, number_t nbDoms, std::ostream& out) const
{
  std::map<number_t, string_t> elemInfo;
  std::map<string_t, number_t> sideIndex;
  std::map<string_t, number_t>::iterator itsn;

  string_t MeshElemNature;  
  string_t SideElemNature="", Sen0=SideElemNature;
  string_t Sen1;
  dimen_t  d=dom.dim();
  number_t nbSe=0;
  number_t nbside=0;
  MeshElement* melt;
  GeomElement* sideT;
  std::vector< GeomElement * >   VgeomElements=dom.meshDomain()->geomElements;
  std::stringstream st(std::stringstream::out);

  if (dom.domType()==_meshDomain)
  {
    // shape type of elements.
    std::vector<GeomElement*>::const_iterator it_elem0=VgeomElements.begin();
    if ((*it_elem0)->hasMeshElement())
    {
      MeshElemNature=vizir4Nature(vizir4Type((*it_elem0)->meshElement()->shapeType(),(*it_elem0)->refElement()->interpolation().numtype));
    }
    else
    { 
      std::vector<GeoNumPair>::iterator it_gnp=(*it_elem0)->parentSides().begin();
      MeshElemNature=vizir4Nature(vizir4Type(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype));
    }
    // to write elements info in an ordered way, we prepare each output line in elemInfo
    for (std::vector<GeomElement*>::const_iterator it_elem=VgeomElements.begin(); it_elem!=VgeomElements.end(); ++it_elem)
    {
      std::stringstream sev(std::stringstream::out);
      if (d>2)
      { 
        melt = (*it_elem)->meshElement();
        for (number_t s = 0; s < (*it_elem)->numberOfSides(); s++)   // loop on element sides
        {
          string_t key = (*it_elem)->encodeElement(s + 1); // encode vertex numbers of side s
          itsn = sideIndex.find(key);                // search side in sideIndex map
          if (itsn == sideIndex.end())               // create a new side element
          {
            // sideT=new GeomElement(*it_elem, s + 1, ++(*it_elem)->meshP()->lastIndex_);
            nbside++;
            sideIndex[key] = nbside;             // update sideIndex
            melt->sideNumbers[s] = nbside;       // update sideNumbers
          }
          else // here, side already exists so it is not on the boundary side so it is erased.
          {
            nbside--;
            sideIndex.erase (key);                  // erasing by key
          } // end of else
        } // end for (number_t s
        // Sen1= vizir4Nature(vizir4Type(sideT->shapeType(),(*it_elem)->refElement()->interpolation().numtype));
        if(MeshElemNature=="Tetrahedra")
        {
            Sen1="Triangles";
        }
        else if(MeshElemNature=="Hexahedra")
        {
          Sen1="Quadrilaterals";
        } 
        if (Sen1 != Sen0 ) SideElemNature=SideElemNature+Sen1;
        Sen0=Sen1;
      }
      std::stringstream ss(std::stringstream::out);
      // node numbers are available through refElement for non MeshElement side
      if ((*it_elem)->hasMeshElement())
      { 
        for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); ++i) 
        { 
          ss << (*it_elem)->meshElement()->nodeNumbers[i] << " ";
        } 
        // ss<<eol;
      }
      else
      { 
        std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
        for (number_t i=0; i<it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1].size(); ++i)
        { 
          ss << it_gnp->first->meshElement()->nodeNumbers[it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1]<< " " ; 
        }
      }
      ss << nbDoms;
      elemInfo[(*it_elem)->number()-1]=ss.str();
    }
  }
  // nodes list
  dimen_t nodeDim=meshDim();
  out << "Vertices" <<std::endl<<nbOfNodes()<<std::endl;
  number_t i=1;
  for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
  {
    for (number_t j=0; j<it_node[0].size(); ++j) { out  << (*it_node)[j]<< " "; }
    out <<" "<<1 << std::endl;
  }
  
// list of sides of element if dimMesh>2
  if (d>2)
  {
    out << SideElemNature << std::endl << nbside << eol;
    std::stringstream ss(std::stringstream::out);
    number_t nbSides=0;
    for ( std::map<string_t, number_t>::const_iterator itMap = sideIndex.begin() ; itMap != sideIndex.end() ; ++itMap )
    {
      out << itMap->first <<" "<<itMap->second<<eol;
    }
    sideIndex.clear();
  }
  out << "" << std::endl;
  // elements list
  out << MeshElemNature << std::endl << elemInfo.size() << std::endl;
  for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) 
  {
     out << it->second << std::endl;
  }
  out <<std::endl << "End" << std::endl;
}

void vizir4Export(const GeomDomain& dom, const std::vector<Point>& coords, const splitvec_t& elementsInfo, std::ostream& out)
{
  // save vizir4 header
  out << "MeshVersionFormatted 3"<<eol<<eol;
  std::vector<string_t> elemInfo(elementsInfo.size());
  string_t MeshElemNature;

  if (dom.domType()==_meshDomain)
  {
    number_t i=1;
    // to write elements info in an ordered way, we prepare each output line in elemInfo
    for (splitvec_t::const_iterator it_elem=elementsInfo.begin(); it_elem != elementsInfo.end(); ++it_elem, ++i)
    {
         MeshElemNature=vizir4Nature(vizir4Type(it_elem->first,1));
    }
  }

  out << "\n Dimension" << eol << coords.size()<<eol << eol;

  // nodes list
  out << "Vertices" << std::endl << coords.size() << std::endl;
  number_t i=1;

  for (std::vector<Point>::const_iterator it_node=coords.begin(); it_node!=coords.end(); it_node++, i++)
  {
    out << i;
    for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
    out << " " << 1 << std::endl;
  }
  // elements list
  out << MeshElemNature << std::endl << elemInfo.size() << std::endl;
  for (number_t i=0; i< elemInfo.size(); ++i) { out << elemInfo[i] << " "<< i+1 <<std::endl; }

  out << std::endl << "End" << std::endl;
}

number_t vizir4Type(ShapeType st, number_t order) {
  switch (st) {
    case _point: return 15;
    case _segment:
      switch (order) {
        case 1: return 1;
        case 2: return 8;
        case 3: return 26;
        case 4: return 27;
        case 5: return 28;
        default:
          error("vizir4_elem_type", st, order);
      }
    case _triangle:
      switch (order) {
        case 1: return 2;
        case 2: return 9;
        case 3: return 21;
        case 4: return 23;
        case 5: return 25;
        // P3inc -> 20
        // P4inc -> 22
        // P5inc -> 24
        default:
          error("vizir4_elem_type",st,order);
      }
    case _quadrangle:
      switch (order) {
        case 1: return 3;
        case 2: return 10;
        // Q2Ser -> 16
        default:
          error("vizir4_elem_type",st,order);
      }
    case _tetrahedron:
      switch (order) {
        case 1: return 4;
        case 2: return 11;
        case 3: return 29;
        case 4: return 30;
        case 5: return 31;
        default:
          error("vizir4_elem_type",st,order);
      }
    case _hexahedron:
      switch (order) {
        case 1: return 5;
        case 2: return 12;
        case 3: return 92;
        case 4: return 93;
        // Q2Ser -> 17
        default:
          error("vizir4_elem_type",st,order);
      }
    case _prism:
      switch (order) {
        case 1: return 6;
        case 2: return 13;
        // 02Ser -> 18
        default:
          error("vizir4_elem_type",st,order);
      }
    case _pyramid:
      switch (order) {
        case 1: return 7;
        case 2: return 14;
        // O2Ser -> 19
        default:
          error("vizir4_elem_type",st,order);
      }
    default:
      error("vizir4_elem_type",st,order);
  }
  return 0;
}


string_t vizir4Nature( number_t st) {
  switch (st) {
    case 1: return "Edges";
    case 2: return "Triangles";
    case 3: return "Quadrilaterals";
    case 4: return "Tetrahedra";
    case 5: return "Hexahedra";
    case 6: return "Prisms";
    case 7: return "Pyramid";
    case 8: return "EdgesP2";
    case 9: return "TrianglesP2";
    case 10: return "QuadrilateralsQ2";
    case 11: return "TetrahedraP2";
    case 12: return "HexahedraQ2";
    case 13: return "PrismsP2";
    case 14: return "PyramidP2";
    case 15: return "Point";
    case 21: return "TrianglesP3";
    case 23: return "TrianglesP4";
    case 25: return "TrianglesP5";
    case 26: return "EdgesP3";
    case 27: return "EdgesP4";
    case 28: return "EdgesP5";
    case 29: return "TetrahedraP3";
    case 30: return "TetrahedraP4";
    case 31: return "TetrahedraP5";
    case 92: return "HexahedraP3";
    case 93: return "HexahedraP4";
    default:
      error("vizir4_elem_type",st,99999);
  }
  return 0;
}

} // end of namespace xlifepp


// *****************************************************************************


// //save mesh and related domains to files
// void Mesh::saveTovizir4(const string_t& filename, bool withDomains) const
// {
//   std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
//   string_t froot = rootext.first;
//   if(rootext.second!="msh") froot=filename;
//   string_t f=froot+".mesh"; 
//   std::ofstream out(f.c_str());
//   out.precision(fullPrec);
//   vizir4Export(out,withDomains);
//   out.close();
//   if (withDomains && meshDim() > 1) //save domains in files filename_domainname.msh
//   {
//     for (number_t d = 0; d < domains_.size(); d++) {
//       string_t dn = domains_[d]->nameWithoutSharp();
//       if (dn == "") { dn = "domain_" + tostring(d); }
//       f = froot + "_" + dn + ".mesh";
//       out.open(f.c_str());
//       out.precision(fullPrec);
//   std::cout <<" export en vizir4 "<<domains_[d]->name()<<eol;
//       vizir4Export(*domains_[d], out);
//       out.close();
//     }
//   }
// }

// void Mesh::vizir4Export(std::ostream& out, bool withDomains) const
// {
//   // save vizir4 header
//   out << "MeshVersionFormatted 3"<<eol<<eol;

//   number_t i=1, nbDoms=0;
//   std::map<number_t, string_t> elemInfo;
//   std::stringstream domOut;

//   dimen_t maxdim=0, nmaxdim=0;
//   for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
//   {
//       dimen_t d=(*it_dom)->dim();
//       if(d== maxdim) {nmaxdim++;}
//       else if (d>maxdim) {maxdim=d; nmaxdim=1;}
//   }

//   for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
//   {
//     if ((*it_dom)->domType()==_meshDomain && isDomainToBeExported(*(*it_dom)))
//     {
//       domOut << (*it_dom)->dim() << " " << i << " \"" << (*it_dom)->nameWithoutSharp() << "\"" << std::endl;
//       nbDoms++;
//       // to write elements info in an ordered way, we prepare each output line in elemInfo
//       for (std::vector<GeomElement*>::iterator it_elem=(*it_dom)->meshDomain()->geomElements.begin(); it_elem!=(*it_dom)->meshDomain()->geomElements.end(); ++it_elem)
//       {
//         std::stringstream ss(std::stringstream::out);
//         //  ss << (*it_elem)->number() << " ";
//         // // shape info and interpolation order are available trough parent element if it is not a MeshElement
//         //  if((*it_elem)->hasMeshElement())
//         //  { ss << vizir4Type((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype); 
//         //        std::cout << ss.str()<<eol;
//         //   std::cout << vizir4Nature(vizir4Type((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype))<<eol;}
//         // else
//         // {
//         //  std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//         //   ss << vizir4Type(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype);
//         //        std::cout << ss.str()<<eol;
//         //   std::cout << vizir4Nature(vizir4Type(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype))<<eol;
//         // }
//         // ss << " 2 " << i << " " << i;
//         // node numbers are available through refElement for non MeshElement side

//         if((*it_elem)->hasMeshElement())
//         { 
//           for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); i++) 
//           {
//              ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; 
//           } 
//         }
//         else
//         {
//           std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//           if (it_gnp->first->hasMeshElement())
//           {
//             MeshElement* meshE=it_gnp->first->meshElement();
//             for (number_t i=0; i<meshE->refElement()->sideDofNumbers_[it_gnp->second-1].size(); i++)
//             {
//               ss << " " << meshE->nodeNumbers[meshE->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1];
//             }
//           }
//           else
//           {
//             // case of side of side
//             warning("sos_elt_not_handled");
//           }
//         }
//         ss << " " << i;

//         // Management of this part is tricky
//         // Actually, if a element has already been created, it is not updated (case of several domains recovering same elements)
//         if (elemInfo.find((*it_elem)->number()-1) == elemInfo.end())
//         {
//           elemInfo[(*it_elem)->number()-1]=ss.str();
//         }
//       }
//       ++i;
//     }
//   }

//   // save physical names
//   if (withDomains) 
//   {
//   out << "\n$PhysicalNames" << std::endl << nbDoms << std::endl;
//   out << domOut.str(); // domOut ends with a std::endl
//   out << "$EndPhysicalNames" << std::endl << std::endl;
//   }

//   out << eol <<"Dimension" << eol << meshDim()<<eol << eol;

//   // nodes list
//   out << "Vertices" << std::endl << nbOfNodes() << std::endl;
//   i=1;

//   for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
//   {
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     out << " " << 1 << std::endl;
//   }

//   out << "" << std::endl;

//   // elements list
//   number_t nbelt=elemInfo.size();
//   out <<"Elements" << std::endl << nbelt << std::endl;
//   for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) 
//   {
//      out << it->second << std::endl;
//   }

//   out << std::endl << "End" << std::endl;
// }

// void Mesh::vizir4Export(const GeomDomain& dom, std::ostream& out) const
// {
//   // save vizir4 header
//   out << "MeshVersionFormatted 3"<<eol<<eol;
//   std::map<number_t, string_t> elemInfo;
  
//   string_t MeshElemNature;

//   if (dom.domType()==_meshDomain)
//   {
//     std::vector<GeomElement*>::const_iterator it_elem0=dom.meshDomain()->geomElements.begin();
//     if ((*it_elem0)->hasMeshElement())
//       {
//         MeshElemNature=vizir4Nature(vizir4Type((*it_elem0)->meshElement()->shapeType(),(*it_elem0)->refElement()->interpolation().numtype));
//       }
//       else
//       { 
//         std::vector<GeoNumPair>::iterator it_gnp=(*it_elem0)->parentSides().begin();
//         MeshElemNature=vizir4Nature(vizir4Type(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype));
//       }

//     // to write elements info in an ordered way, we prepare each output line in elemInfo
//     for (std::vector<GeomElement*>::const_iterator it_elem=dom.meshDomain()->geomElements.begin(); it_elem!=dom.meshDomain()->geomElements.end(); ++it_elem)
//     {
//       std::stringstream ss(std::stringstream::out);
//       // node numbers are available through refElement for non MeshElement side
//       if ((*it_elem)->hasMeshElement())
//         { for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); ++i) { ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; } }
//       else
//       {
//         std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//         for (number_t i=0; i<it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1].size(); ++i)
//           { ss << " " << it_gnp->first->meshElement()->nodeNumbers[it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1]; }
//       }
//       elemInfo[(*it_elem)->number()-1]=ss.str();
//     }
//   }
//   out << "Dimension" << eol << meshDim()<<eol << eol;

//   // nodes list
//   out << "Vertices" << std::endl << nbOfNodes() << std::endl;
//   number_t i=1;

//   for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
//   {
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     out << " " << 1 << std::endl;
//   }

//   out << "" << std::endl;

//   // elements list
//   out << MeshElemNature << std::endl << elemInfo.size() << std::endl;
//   for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) 
//   {
//      out << it->second << " "<< it->first +1<< std::endl;
//   }

//   out <<std::endl << "End" << std::endl;
// }

// void vizir4Export(const GeomDomain& dom, const std::vector<Point>& coords, const splitvec_t& elementsInfo, std::ostream& out)
// {
//   // save vizir4 header
//   out << "MeshVersionFormatted 3"<<eol<<eol;
//   std::vector<string_t> elemInfo(elementsInfo.size());
//   string_t MeshElemNature;

//   if (dom.domType()==_meshDomain)
//   {
//     number_t i=1;
//     // to write elements info in an ordered way, we prepare each output line in elemInfo
//     for (splitvec_t::const_iterator it_elem=elementsInfo.begin(); it_elem != elementsInfo.end(); ++it_elem, ++i)
//     {
//          MeshElemNature=vizir4Nature(vizir4Type(it_elem->first,1));
//     }
//   }

//   out << "\n Dimension" << eol << coords.size()<<eol << eol;

//   // nodes list
//   out << "Vertices" << std::endl << coords.size() << std::endl;
//   number_t i=1;

//   for (std::vector<Point>::const_iterator it_node=coords.begin(); it_node!=coords.end(); it_node++, i++)
//   {
//     out << i;
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     out << " " << 1 << std::endl;
//   }
//   // elements list
//   out << MeshElemNature << std::endl << elemInfo.size() << std::endl;
//   for (number_t i=0; i< elemInfo.size(); ++i) { out << elemInfo[i] << " "<< i+1 <<std::endl; }

//   out << std::endl << "End" << std::endl;
// }

// number_t vizir4Type(ShapeType st, number_t order) {
//   switch (st) {
//     case _point: return 15;
//     case _segment:
//       switch (order) {
//         case 1: return 1;
//         case 2: return 8;
//         case 3: return 26;
//         case 4: return 27;
//         case 5: return 28;
//         default:
//           error("vizir4_elem_type", st, order);
//       }
//     case _triangle:
//       switch (order) {
//         case 1: return 2;
//         case 2: return 9;
//         case 3: return 21;
//         case 4: return 23;
//         case 5: return 25;
//         // P3inc -> 20
//         // P4inc -> 22
//         // P5inc -> 24
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _quadrangle:
//       switch (order) {
//         case 1: return 3;
//         case 2: return 10;
//         // Q2Ser -> 16
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _tetrahedron:
//       switch (order) {
//         case 1: return 4;
//         case 2: return 11;
//         case 3: return 29;
//         case 4: return 30;
//         case 5: return 31;
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _hexahedron:
//       switch (order) {
//         case 1: return 5;
//         case 2: return 12;
//         case 3: return 92;
//         case 4: return 93;
//         // Q2Ser -> 17
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _prism:
//       switch (order) {
//         case 1: return 6;
//         case 2: return 13;
//         // 02Ser -> 18
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _pyramid:
//       switch (order) {
//         case 1: return 7;
//         case 2: return 14;
//         // O2Ser -> 19
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     default:
//       error("vizir4_elem_type",st,order);
//   }
//   return 0;
// }


// string_t vizir4Nature( number_t st) {
//   switch (st) {
//     case 1: return "Edges";
//     case 2: return "Triangles";
//     case 3: return "Quadrilaterals";
//     case 4: return "Tetrahedra";
//     case 5: return "Hexahedra";
//     case 6: return "Prisms";
//     case 7: return "Pyramid";
//     case 8: return "EdgesP2";
//     case 9: return "TrianglesP2";
//     case 10: return "QuadrilateralsQ2";
//     case 11: return "TetrahedraP2";
//     case 12: return "HexahedraQ2";
//     case 13: return "PrismsP2";
//     case 14: return "PyramidP2";
//     case 15: return "Point";
//     case 21: return "TrianglesP3";
//     case 23: return "TrianglesP4";
//     case 25: return "TrianglesP5";
//     case 26: return "EdgesP3";
//     case 27: return "EdgesP4";
//     case 28: return "EdgesP5";
//     case 29: return "TetrahedraP3";
//     case 30: return "TetrahedraP4";
//     case 31: return "TetrahedraP5";
//     case 92: return "HexahedraP3";
//     case 93: return "HexahedraP4";
//     default:
//       error("vizir4_elem_type",st,99999);
//   }
//   return 0;
// }






























// //save mesh and related domains to files
// void Mesh::saveTovizir4(const string_t& filename, bool withDomains) const
// {
//   std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
//   string_t froot = rootext.first;
//   if(rootext.second!="msh") froot=filename;
//   string_t f=froot+".mesh";
//   std::ofstream out(f.c_str());
//   out.precision(fullPrec);
//   vizir4Export(out);
//   out.close();
//   if (withDomains && meshDim() > 1) //save domains in files filename_domainname.msh
//   {
//     for (number_t d = 0; d < domains_.size(); d++) {
//       string_t dn = domains_[d]->nameWithoutSharp();
//       if (dn == "") { dn = "domain_" + tostring(d); }
//       f = froot + "_" + dn + ".msh";
//       out.open(f.c_str());
//       out.precision(fullPrec);
//       vizir4Export(*domains_[d], out);
//       out.close();
//     }
//   }
// }

// void Mesh::vizir4Export(std::ostream& out) const
// {
//   // save vizir4 header
//   out << "MeshVersionFormatted 3"<<eol<<eol;

//   number_t i=1, nbDoms=0;
//   std::map<number_t, string_t> elemInfo;
//   std::stringstream domOut;

//   dimen_t maxdim=0, nmaxdim=0;
//   for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
//   {
//       dimen_t d=(*it_dom)->dim();
//       if(d== maxdim) {nmaxdim++;}
//       else if (d>maxdim) {maxdim=d; nmaxdim=1;}
//   }

//   for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
//   {
//     if ((*it_dom)->domType()==_meshDomain && isDomainToBeExported(*(*it_dom)))
//     {
//       domOut << (*it_dom)->dim() << " " << i << " \"" << (*it_dom)->nameWithoutSharp() << "\"" << std::endl;
//       nbDoms++;
//       // to write elements info in an ordered way, we prepare each output line in elemInfo
//       for (std::vector<GeomElement*>::iterator it_elem=(*it_dom)->meshDomain()->geomElements.begin(); it_elem!=(*it_dom)->meshDomain()->geomElements.end(); ++it_elem)
//       {
//         std::stringstream ss(std::stringstream::out);
//         ss << (*it_elem)->number() << " ";
//         // shape info and interpolation order are available trough parent element if it is not a MeshElement
//         if((*it_elem)->hasMeshElement())
//           { ss << vizir4Type((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype); }
//         else
//         {
//           std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//           ss << vizir4Type(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype);
//         }
//         ss << " 2 " << i << " " << i;
//         // node numbers are available through refElement for non MeshElement side
//         if((*it_elem)->hasMeshElement())
//           { for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); i++) { ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; } }
//         else
//         {
//           std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//           if (it_gnp->first->hasMeshElement())
//           {
//             MeshElement* meshE=it_gnp->first->meshElement();
//             for (number_t i=0; i<meshE->refElement()->sideDofNumbers_[it_gnp->second-1].size(); i++)
//             {
//               ss << " " << meshE->nodeNumbers[meshE->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1];
//             }
//           }
//           else
//           {
//             // case of side of side
//             warning("sos_elt_not_handled");
//           }
//         }

//         // Management of this part is tricky
//         // Actually, if a element has already been created, it is not updated (case of several domains recovering same elements)
//         if (elemInfo.find((*it_elem)->number()-1) == elemInfo.end())
//         {
//           elemInfo[(*it_elem)->number()-1]=ss.str();
//         }
//       }
//       ++i;
//     }
//   }

//   // save physical names
//   out << "\n$PhysicalNames" << std::endl << nbDoms << std::endl;
//   out << domOut.str(); // domOut ends with a std::endl
//   out << "$EndPhysicalNames" << std::endl;

//   // nodes list
//   out << "$Nodes" << std::endl << nbOfNodes() << std::endl;
//   i=1;

//   for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
//   {
//     out << i;
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     for (number_t j=it_node[0].size(); j<3; ++j) { out << " 0"; }
//     out << std::endl;
//   }

//   out << "$EndNodes" << std::endl;

//   // elements list
//   number_t nbelt=elemInfo.size();
//   out <<"$Elements" << std::endl << nbelt << std::endl;
//   for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) { out << it->second << std::endl; }

//   out << "$EndElements" << std::endl;
// }

// void Mesh::vizir4Export(const GeomDomain& dom, std::ostream& out) const
// {
//   // save vizir4 header
//   out << "MeshVersionFormatted 3"<<eol<<eol;
//   // save physical names
//   out << "\n$PhysicalNames"<< std::endl << "1" << std::endl;
//   std::map<number_t, string_t> elemInfo;

//   if (dom.domType()==_meshDomain)
//   {
//     out << dom.dim() << " 1 \"" << dom.name() << "\"" << std::endl;
//     // to write elements info in an ordered way, we prepare each output line in elemInfo
//     for (std::vector<GeomElement*>::const_iterator it_elem=dom.meshDomain()->geomElements.begin(); it_elem!=dom.meshDomain()->geomElements.end(); ++it_elem)
//     {
//       std::stringstream ss(std::stringstream::out);
//       ss << (*it_elem)->number() << " ";
//       // shape info and interpolation order are available trough parent element if it is not a MeshElement
//       if ((*it_elem)->hasMeshElement()) { ss << vizir4Type((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype); }
//       else
//       {
//         std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//         ss << vizir4Type(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype);
//       }
//       ss << " 3 1 0 0";
//       // node numbers are available through refElement for non MeshElement side
//       if ((*it_elem)->hasMeshElement())
//         { for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); ++i) { ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; } }
//       else
//       {
//         std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//         for (number_t i=0; i<it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1].size(); ++i)
//           { ss << " " << it_gnp->first->meshElement()->nodeNumbers[it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1]; }
//       }
//       elemInfo[(*it_elem)->number()-1]=ss.str();
//     }
//   }

//   out << "$EndPhysicalNames" << std::endl;
//   // nodes list
//   out << "$Nodes" << std::endl << nbOfNodes() << std::endl;
//   number_t i=1;

//   for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
//   {
//     out << i;
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     for (number_t j=it_node[0].size(); j<3; ++j) { out << " 0"; }
//     out << std::endl;
//   }

//   out << "$EndNodes" << std::endl;

//   // elements list
//   out << "$Elements" << std::endl << elemInfo.size() << std::endl;
//   for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) { out << it->second << std::endl; }

//   out << "$EndElements" << std::endl;
// }

// void vizir4Export(const GeomDomain& dom, const std::vector<Point>& coords, const splitvec_t& elementsInfo, std::ostream& out)
// {
//   // save vizir4 header
//   out << "MeshVersionFormatted 3"<<eol;
//   // save physical names
//   out << "\n$PhysicalNames"<< std::endl << "1" << std::endl;
//   std::vector<string_t> elemInfo(elementsInfo.size());

//   if (dom.domType()==_meshDomain)
//   {
//     out << dom.dim() << " 1 \"" << dom.name() << "\"" << std::endl;

//     number_t i=1;
//     // to write elements info in an ordered way, we prepare each output line in elemInfo
//     for (splitvec_t::const_iterator it_elem=elementsInfo.begin(); it_elem != elementsInfo.end(); ++it_elem, ++i)
//     {
//       std::stringstream ss(std::stringstream::out);
//       ss << i << " " << vizir4Type(it_elem->first,1) << " 3 1 0 0";
//       for (number_t j=0; j < it_elem->second.size(); ++j) { ss << " " << it_elem->second[j]; }
//       elemInfo[i-1]=ss.str();
//     }
//   }

//   out << "$EndPhysicalNames" << std::endl;
//   // nodes list
//   out << "$Nodes" << std::endl << coords.size() << std::endl;
//   number_t i=1;

//   for (std::vector<Point>::const_iterator it_node=coords.begin(); it_node!=coords.end(); it_node++, i++)
//   {
//     out << i;
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     for (number_t j=it_node[0].size(); j<3; ++j) { out << " 0"; }
//     out << std::endl;
//   }

//   out << "$EndNodes" << std::endl;

//   // elements list
//   out << "$Elements" << std::endl << elemInfo.size() << std::endl;
//   for (number_t i=0; i< elemInfo.size(); ++i) { out << elemInfo[i] << std::endl; }

//   out << "$EndElements" << std::endl;
// }

// number_t vizir4Type(ShapeType st, number_t order) {
//   switch (st) {
//     case _point: return 15;
//     case _segment:
//       switch (order) {
//         case 1: return 1;
//         case 2: return 8;
//         case 3: return 26;
//         case 4: return 27;
//         case 5: return 28;
//         default:
//           error("vizir4_elem_type", st, order);
//       }
//     case _triangle:
//       switch (order) {
//         case 1: return 2;
//         case 2: return 9;
//         case 3: return 21;
//         case 4: return 23;
//         case 5: return 25;
//         // P3inc -> 20
//         // P4inc -> 22
//         // P5inc -> 24
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _quadrangle:
//       switch (order) {
//         case 1: return 3;
//         case 2: return 10;
//         // Q2Ser -> 16
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _tetrahedron:
//       switch (order) {
//         case 1: return 4;
//         case 2: return 11;
//         case 3: return 29;
//         case 4: return 30;
//         case 5: return 31;
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _hexahedron:
//       switch (order) {
//         case 1: return 5;
//         case 2: return 12;
//         case 3: return 92;
//         case 4: return 93;
//         // Q2Ser -> 17
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _prism:
//       switch (order) {
//         case 1: return 6;
//         case 2: return 13;
//         // 02Ser -> 18
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     case _pyramid:
//       switch (order) {
//         case 1: return 7;
//         case 2: return 14;
//         // O2Ser -> 19
//         default:
//           error("vizir4_elem_type",st,order);
//       }
//     default:
//       error("vizir4_elem_type",st,order);
//   }
//   return 0;
// }
