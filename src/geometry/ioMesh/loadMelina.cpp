/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file loadMelina.cpp
  \authors D. Martin, Y. Lafranche
  \since 26 may 2011
  \date  10 sep 2013

  \brief Read a file containing a mesh in "Melina" format

  The useful function is loadMelina, which is a member function of the class Mesh.
  It uses some utilitary extern functions gathered in this file, as long as the two
  utilitary classes xlifepp::iomel::ElementBlock and xlifepp::iomel::StringInput.
*/

#include "../Mesh.hpp"
#include "iomel/StringInput.hpp"
#include "iomel/ElementBlock.hpp"

namespace xlifepp {
using std::pair;
using std::string;
using std::ifstream;
using std::vector;

namespace iomel {
/*!
  loadTitle loads mesh Title from input file.

  Syntax:
    Titre/Title "title as a string"
    or
    Titre/Title &I (followed by) "title as &I lines of input"
*/
string loadTitle(StringInput& mi)
{
  trace_p->push("loadTitle");
  mi.notComment();
  if ( mi.code() == 'i' ) mi.readLines(mi.integerValue());
//  else if ( mi.code() == 's');
  else { error("mel_load_title"); }
  trace_p->pop();
  return mi.value();
}

//! Reads variable names from input mesh file and returns the space dimension
dimen_t readVarNames(StringInput& mi)
{
  dimen_t spaceDim = 0; // space dimension

  mi.nextString();
  // mi.value()) is 'X'
  spaceDim++;
  while ( mi.next() ) {
    mi.eval();
    if ( mi.code() == 's' ) { spaceDim++; }
    else break;
  }

  return spaceDim;
}

/*!
  loads mesh description from input file

  Syntax:
    < Description du maillage ^ Mesh Description >
    < Variables d''espace ^ Space variables > : &S1 [&S2 [&S3]]
    < Nombre d''elements ^ Number of elements > : &I
*/
number_t loadDescription(StringInput& mi, dimen_t& spaceDim)
{
  trace_p->push("loadDescription");
  number_t nb_elts(0);
  // main words
  bool r_elem = false, r_vari = false;
  vector<string> expected_words;
  expected_words.push_back("elements");
  expected_words.push_back("variables");

  while ( !(r_elem && r_vari) && mi.word() )
  {
    switch ( findString(mi.value(), expected_words) )
    {
      case 0: // reading the number of elements
        mi.nextInteger(); nb_elts+= mi.integerValue(); r_elem = true;
        break;
      case 1: // space variable names (their number defines mesh dimension)
        spaceDim = readVarNames(mi); r_vari = true;
        break;
      default: // ignore word and get next
        mi.nextWord();
        break;
    }
  }
  trace_p->pop();
  return nb_elts;
}

//! loads a Geom Element of mesh from input file
GeomElement* loadGeomElement(ifstream& ifs, const Mesh* msh, const RefElement* r_e, dimen_t spaceDim,
                     vector<real_t>& elt_coords, vector<number_t>& elt_numbers,
                     const number_t no_elt, pair<number_t, number_t>& minmax)
{
  // minmax carries a the pair of min and max of point numbers in previous elements

  // create geometric element
  GeomElement* g_e = new GeomElement(msh, r_e, spaceDim, no_elt+1);

  // element point coordinates
  for ( vector<real_t>::iterator c_i = elt_coords.begin(); c_i != elt_coords.end(); c_i++ )
    ifs >> *c_i;

  // element point global numbers
  for ( vector<number_t>::iterator n_i = elt_numbers.begin(); n_i != elt_numbers.end(); n_i++ )
  {
    ifs >> *n_i;
    if ( *n_i < minmax.first ) minmax.first = *n_i;
    else if ( *n_i > minmax.second ) minmax.second = *n_i;
  }
  // return the new element to be appended to vector of geometric elements of mesh
  return g_e;
}

//! checkPointNumbers
number_t checkPointNumbers(const vector< vector<number_t> >& elt_wise_numbers, const pair<number_t,number_t>& mM)
{
  number_t min_no(mM.first);
  number_t nb_points(mM.second - min_no + 1);

  // The set of all element point numbers is an increasing sequence of consecutive numbers
  vector<bool> unFoundNos(nb_points, true);
  for ( size_t no_elt = 0; no_elt < elt_wise_numbers.size(); no_elt++ )
  {
    for ( vector<number_t>::const_iterator n_i = elt_wise_numbers[no_elt].begin(); n_i != elt_wise_numbers[no_elt].end(); n_i++)
    {
      unFoundNos[*n_i - min_no] = false;
    }
  }
  bool holes_in(false);
  for ( vector<bool>::const_iterator b_i = unFoundNos.begin(); b_i != unFoundNos.end(); b_i++)
  {
    if ( *b_i ) holes_in = true;
  }
  if ( holes_in ) { error("mel_pts_outofrange", mM.first, mM.second); }

  // Lowest point number is 1
  if ( min_no != 1 ) { error("mel_pts_nopt1", min_no); }

  return static_cast<number_t>(nb_points);
}

//! Utilitary function to update MeshElement part of the GeomElement ge_p
void updateGeomElement(GeomElement* ge_p, vector<number_t>::const_iterator gnum_el, const number_t nbPts,
                       const RefElement* re_p, vector<Point>& nodes){
  MeshElement* melt = ge_p->meshElement();
  for (number_t i=0; i<nbPts; i++) { melt->nodeNumbers[i] = *gnum_el++; }
// update vertex numbers: the vertexNumbers are the first of nodeNumbers
  number_t nbVert = re_p->geomRefElement()->nbVertices();
  for (number_t i=0; i<nbVert; i++) {
    melt->vertexNumbers[i] = melt->nodeNumbers[i];
  }
  melt->setNodes(nodes); //update node pointers
}

/*!
  defines mesh nodes and elements_ members:
  create Nodes vector and set Points in Elements
  from the number of points in Mesh and element-wise arrays coords and numbers
*/
void Points_and_Elements(const number_t nb_points, const vector< vector<real_t> >& elt_wise_coords,
                         const vector< vector<number_t> >& elt_wise_numbers,
                         dimen_t spaceDim, vector<Point>& Nodes, vector<GeomElement*>& Elements)
{
  trace_p->push("Points_and_Elements");

  // temporary array to store first occurrence of a mesh point in mesh elements
  vector<bool> met_1st(nb_points+1, false);
  Nodes.resize(nb_points);

  // geometric element list loop
  number_t el = 0;
  for ( vector<GeomElement*>::iterator ge_it = Elements.begin(); ge_it != Elements.end(); ge_it++, el++ ) {
    vector<real_t>::const_iterator e_w_coords_p = elt_wise_coords[el].begin();
    vector<number_t>::const_iterator e_w_numbers_p = elt_wise_numbers[el].begin();
    number_t nbpts_el = (*ge_it)->refElement()->nbPts();
    const RefElement* re_p = (*ge_it)->refElement();

    // update internal members of current element
    updateGeomElement(*ge_it, e_w_numbers_p, nbpts_el, re_p, Nodes);

    // Check points of current element and create new points if needed:
    for ( number_t po = 0; po < nbpts_el; po++ ) {
       // global point number of point po of current element
       number_t global_num = *e_w_numbers_p++;
       if ( !met_1st[global_num] ) {
          // point is not found in any previous element
          met_1st[global_num] = true;

          // create new point
          Nodes[global_num-1] = Point(e_w_coords_p, spaceDim);
       }
       // advance coordinates pointer to next point
       e_w_coords_p += spaceDim;
    } // go next point po of element el
  } // go next element

  trace_p->pop();
}

/*!
  find domain kind from input stream: list of elements (E) or components (C)
  Also returns the number of the first element e1, and the domain dimension
  in the components case (unset otherwise).
*/
char domainReadKind(StringInput& mi, number_t& e1, char& code, dimen_t& dimdom) {
  trace_p->push("domainReadKind");

  char kindom(' '); // funny isn't it ?
  string inpstring;

   // lookin first for first component
  mi.nextWord();
  inpstring=mi.value();
  if ( inpstring.at(0) == 'e' )
  {
    mi.nextInteger(); e1=mi.integerValue();  // first element number
    mi.notComment();     // next non-comment chunk after first element
    if ( (code = mi.code()) == 'i' || code == '/')
      kindom = 'E';
    else if ( code == 'w' )
    {
      inpstring = mi.value();
      code = inpstring.at(0);
      if ( code == 'e' && inpstring != "edge" )
        kindom = 'E';
      else
      {
        // domain dimension
        switch (code)
        {
          case 's': case 'f':
            dimdom = 2; kindom = 'C'; break;
          case 'e': case 'a':
            dimdom = 1; kindom = 'C'; break;
          case 'p':
            dimdom = 0; kindom = 'C'; break;
          default:
            break;
        }
      }
    }
  }
  trace_p->pop();
  return kindom;
}

//! reads the element list defining an element domain
void domainReadElements(StringInput& mi, const number_t e1, const char nxc,
                        const vector<GeomElement*>& elements, vector<GeomElement*>& domainElements)
{
  trace_p->push("domainReadElements");

  number_t e2(0), elt, prev_elt;
  domainElements.push_back(elements[e1-1]);

  // Starting block: first two elements of element GeomDomain list
  if ( nxc == 'i' )
  {
    e2 = mi.integerValue();
    domainElements.push_back(elements[e2-1]);
  }
  else if ( nxc == 'e' )
  {
    mi.nextInteger(); e2 = mi.integerValue();
    domainElements.push_back(elements[e2-1]);
  }
  else if ( nxc == '/' )
  {
    mi.nextInteger(); e2 = mi.integerValue();
    for ( elt = e1+1; elt <= e2; elt++ ) domainElements.push_back(elements[elt-1]);
  }

  prev_elt = e2;
  // next to 2nd element
  while ( mi.notComment() )
  {
    if ( mi.code() == 'i' ) // next element number
    {
      elt = mi.integerValue();
      domainElements.push_back(elements[elt-1]);
    }
    else if ( mi.code() == '/' )  // next elements as a pair separated with a '/'
    {
      mi.nextInteger(); elt = mi.integerValue(); // last element number of pair
      for (number_t el = prev_elt; el < elt; el++ )
        domainElements.push_back(elements[el-1]);
    }
    else if ( mi.code() == 'w' && mi.value().at(0) == 'e' && mi.value() != "end" )
    {
      // reading "e[lement]"
      mi.nextInteger(); elt = mi.integerValue();
      domainElements.push_back(elements[elt-1]);
    }
    else break;
    prev_elt = elt;
  }
  // sort element list ascendingly ( according to overloaded <(less) operator )
/*INUTILE ?   GeomElementLess less_ge_star;
  std::sort(domainElements.begin(), domainElements.end(), less_ge_star);*/

  trace_p->pop();
}

//! reads the component list defining a boundary domain or point-wise domain
void domainReadComponents(StringInput& mi, const number_t e1, const char f,
                          const vector<GeomElement*>& elements,
                          vector<GeomElement*>& domainElements, number_t& no_sideElt) {
  trace_p->push("domainReadComponents");
  number_t elt;

  // first component of domain
  mi.nextInteger();
  domainElements.push_back(new GeomElement(elements[e1-1], mi.integerValue(), no_sideElt++));

  while ( mi.nextWord() && mi.value().at(0) == 'e' && mi.value() != "end" )
  {
    // next element number
    mi.nextInteger();
    elt = mi.integerValue();
    // next word 'face', 'edge' or 'point' (might be needed for a test)
    mi.notComment();
    // next element face, element edge or element point number
    if ( mi.code() == 'w' ) mi.nextInteger();
    domainElements.push_back(new GeomElement(elements[elt-1], mi.integerValue(), no_sideElt++));
  }
  trace_p->pop();
}

/*!
  build domain component list from input stream
  (reads domain list of elements/components)
*/
void domainRead(StringInput& mi, const char kindom, const number_t e1, const char code, MeshDomain* meshdom_p,
               const vector<GeomElement*>& elements, number_t& no_sideElt) {
  trace_p->push("domainRead");
  switch ( kindom ) {
    case 'E': domainReadElements  (mi, e1, code, elements, meshdom_p->geomElements);
              break;
    case 'C': domainReadComponents(mi, e1, code, elements, meshdom_p->geomElements, no_sideElt);
              break;
    default:
              error("mel_noelindom",meshdom_p->name(), meshdom_p->mesh()->name());
              break;
  }
  trace_p->pop();
}

//! loads a domain description from input file
void loadDomain(StringInput& mi, const Mesh& msh, number_t& no_sideElt, vector<GeomDomain*>& domains_) {
// domains_ is equal to msh.domains() but is passed here in order to be modified
  trace_p->push("loadDomain");

  // MeshDomain name (as a string)
  mi.String();
  string dom_name = mi.value();

  // Is this a new domain (name) ?
  // (iterate over vector<GeomDomain*> member of `this' object msh)

  GeomDomain* dom_p(nullptr);
  for ( vector<GeomDomain*>::const_iterator dom_it = msh.domains().begin(); dom_it != msh.domains().end(); dom_it++ )
  {
    if ( dom_name == (*dom_it)->name() && (*dom_it)->domType() == _meshDomain ) { dom_p = *dom_it; }
  }

  // If   MeshDomain name is new:
  //         1. create MeshDomain,
  //         2. read domain component list from mesh file (domainRead)
  // else MeshDomain already exists (component list is split in input mesh file):
  //         we just have to read domain additional components from mesh file (domainRead)

  // lookin first for first component in domain
  // get domain dimension in the components case.
  char code;
  number_t e1;
  dimen_t dimdom(0);
  char kindom=domainReadKind(mi, e1, code, dimdom);

  if ( dom_p == nullptr ) {
    if ( dimdom == 0 ) {
       // get domain dimension from first element read (elements have been stored previoulsy)
       dimdom = (msh.elements()[e1-1])->refElement()->geomRefElem_p->dim();
    }
    // MeshDomain name is new: create MeshDomain
    std::ostringstream ss;   ss << "domain of dimension " << dimdom;
    dom_p = (new GeomDomain(msh, dom_name, dimdom, ss.str()))->meshDomain();
    // append MeshDomain to vector of domain pointers
    domains_.push_back(dom_p);
    info("mel_domain_info",dom_name,dimdom);
  }
  // read domain component list from mesh file
  domainRead(mi, kindom, e1, code, dom_p->meshDomain(), msh.elements(), no_sideElt);

  trace_p->pop();
}

} // end of namespace iomel

//! loads mesh from input file filename according to melina mesh format
void Mesh::loadMelina(const string_t& filename, number_t nodesDim)
{
  using namespace iomel;
  trace_p->push("Mesh::loadMelina");

  // define object of Class String_input
  StringInput mi(filename);
  mi.setVerboseLevel(theVerboseLevel);

  comment_ = string("Melina mesh format read from file ") + basenameWithExtension(filename);
  info("loadFile_info", "Melina", filename);

/*
   Mesh characteristics
*/
  // initialize number of elements --sum of number of elements in all blocks--
  number_t nb_elts_blockwise(0), nb_points(0), nb_elts(0);
  vector<ElementBlock> elementBlocks_;
  dimen_t spaceDim = 0; // space dimension

  // main words to describe mesh
  vector<string> expected_words;
  expected_words.push_back("nom");   expected_words.push_back("name");
  expected_words.push_back("titre"); expected_words.push_back("title");
  expected_words.push_back("format");
  expected_words.push_back("description");
  expected_words.push_back("bloc");  expected_words.push_back("block");

  order_ = 0;
  while ( mi.word() && mi.endWords().find(":"+mi.value()+":") == string::npos )
  {
    switch ( findString(mi.value(), expected_words) )
    {
      case 0: case 1: // reading "Name" (as a string)
        mi.String();
        name_ = mi.value();
      case 2: case 3: // reading "Title"
        comment_ += " "+loadTitle(mi);
        break;
      case 4:         //  skipping "Format" description list keywords
                      // (compatibility with melina f77 mesh files)
        mi.nextWord();
        break;
      case 5:         // reading Mesh global "Description"
        nb_elts = loadDescription(mi,spaceDim);
        break;
      case 6: case 7: // reading next Element_block characteristics
        while ( nb_elts_blockwise < nb_elts)
        {
          mi.wordBegin(string("bloc"));
          // next block of consecutive elements of same type
          ElementBlock el_b;
          // read domain characteristics from input mesh file
          order_ = std::max(order_, el_b.read(mi));
          // append new block to list of element blocks
          elementBlocks_.push_back(el_b);
          // advance number of mesh elements
          nb_elts_blockwise += el_b.nb_elts_;
        }
        break;
      default:        // skip to next word
        mi.nextWord();
        break;
      }
    // all blocks have been loaded
    if ( !(elementBlocks_.empty()) && nb_elts_blockwise == nb_elts ) break;
  }
/*
   Mesh geometric element list (by element blocks)
*/
  pair<number_t,number_t> minmax_Nu(theNumberMax,0);
  number_t no_elt(0);
  elements_.reserve(nb_elts);
  vector< vector<real_t> > elt_wise_coords(nb_elts);
  vector< vector<number_t> > elt_wise_numbers(nb_elts);

  RefElement* r_e;
  // GeomRefElement* g_r_e;
  // iterator pointer on blocks of elements
  vector<ElementBlock>::iterator e_b_i;
  isMadeOfSimplices_ = true;
  for ( e_b_i = elementBlocks_.begin(); e_b_i != elementBlocks_.end(); e_b_i++)
  {
    // Find or create reference element

    if ( (*e_b_i).shape_num_ != _triangle && (*e_b_i).shape_num_ != _tetrahedron ) isMadeOfSimplices_ = false;
    r_e = findRefElement((*e_b_i).shape_num_, (*e_b_i).interpolation_p);
    // g_r_e = r_e->geomRefElem_p;
    // Number of points of reference element
    number_t nb_pts = r_e->nbPts();
    // Dimension of elements of mesh (can be less than space dimension)
    // Commente: if ( g_r_e->dim() > element_dim_ ) element_dim_ = g_r_e->dim();

    for ( number_t nelt = 0; nelt < (*e_b_i).nb_elts_; nelt++, no_elt++ )
    {
      // read next geometric element data
      elt_wise_coords[no_elt] = vector<real_t>(spaceDim*nb_pts);
      elt_wise_numbers[no_elt] = vector<number_t>(nb_pts);
      // the number of the element created is no_elt+1
      elements_.push_back(loadGeomElement(mi.si_stream_, this, r_e, spaceDim, elt_wise_coords[no_elt],
                                          elt_wise_numbers[no_elt], no_elt, minmax_Nu));
    }
  }
  // Check element point numbers in mesh
  nb_points = checkPointNumbers(elt_wise_numbers, minmax_Nu);

/*
 Mesh Points & Elements: fill nodes vector and terminate the construction of the GeomElements
*/
  Points_and_Elements(nb_points, elt_wise_coords, elt_wise_numbers, spaceDim, nodes, elements_);

/*
 Mesh Geometric Domain list
*/
  // Increment no_elt because it should now be the next number to be used for the side elements
  no_elt++;
  // reading next string (expecting "domain" as first word)
  mi.notComment();
  while ( mi.word() && mi.endWords().find(":"+mi.value()+":") == string::npos )
  {
    if ( (mi.value()).find("domain") != string::npos ) loadDomain(mi,*this, no_elt, domains_);
  }
  lastIndex_ = no_elt-1;
/*---*/

  // Create list of vertices (vector<number_t> vertices_ member) from elements vertices
  std::set<number_t> vertset; // to store each vertex once
  for ( vector<GeomElement*>::iterator ge_it = elements_.begin(); ge_it != elements_.end(); ge_it++ ) {
    MeshElement* melt = (*ge_it)->meshElement();
    for ( vector<number_t>::const_iterator itv=melt->vertexNumbers.begin(); itv < melt->vertexNumbers.end(); itv++ ) {
      vertset.insert(*itv);
    }
  }
  vertices_.assign(vertset.begin(), vertset.end());

  //compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  //sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)

  // to generate a blank line in the print file
  thePrintStream << std::endl;
  trace_p->pop();
}

} // end of namespace xlifepp
