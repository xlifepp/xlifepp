/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file loadVtk.cpp
  \author N. Kielbasiewicz
  \since 5 may 2015
  \date 6 may 2015

  \brief Read a file containing a mesh in VTK/VTU format

  The useful function is loadPly, which is a member function of the class Mesh.
*/

#include "../Mesh.hpp"

namespace xlifepp {

//! loads mesh from input stream according to POLYDATA dataset in vtk mesh format
dimen_t Mesh::loadPolyDataVtk(std::istream& data, number_t nodesDim)
{
  string_t strVal;
  number_t numVal;

  data >> strVal;
  if (strVal != "POINTS") { error("vtk_bad_block", words("First"), "POLYDATA", "POINTS"); }
  number_t nbPoints;
  data >> nbPoints;
  data >> strVal; // data type of points (ignored)
  // Definition of nodes
  std::vector<std::vector<real_t> > coords(nbPoints,std::vector<real_t>(3));
  nodes.resize(nbPoints);
  vertices_.resize(nbPoints);
  for (number_t i=0; i < nbPoints; ++i)
  {
    data >> coords[i][0] >> coords[i][1] >> coords[i][2];
    vertices_[i]=i+1; // every node is a vertex
  }
  data >> strVal;
  if (strVal != "POLYGONS") { error("vtk_bad_block", words("Second"), "POLYDATA", "POLYGONS"); }
  number_t nbElems;
  data >> nbElems;
  data >> numVal; // total number of integers to read
  std::vector<std::vector<number_t> > cells(nbElems);

  for (number_t i =0; i < nbElems; ++i)
  {
    number_t nbVertices;
    data >> nbVertices;
    cells[i].resize(nbVertices);
    for (number_t j=0; j < nbVertices; ++j)
    {
      data >> numVal;
      cells[i][j]=numVal+1;
    }
  }
  dimen_t spaceDim=2;
  if (nbPoints > 4)
  {
    // we take 4 vertices randomly and check their coplanarity
    number_t v1=0, v2=(nbPoints-1)/3, v3=(2*(nbPoints-1))/3, v4=nbPoints-1;
    if ( std::abs(dot(coords[v2]-coords[v1],crossProduct(coords[v3]-coords[v1],coords[v4]-coords[v1]))) > theEpsilon ) { spaceDim=3; }
  }
  if (nodesDim != 0 && nodesDim > spaceDim) { spaceDim=nodesDim; }

  nodes.resize(nbPoints,Point(std::vector<real_t>(spaceDim, 0.)));
  for (number_t i=0; i < nbPoints; ++i)
  {
    if (spaceDim == 2) { nodes[i]=Point(coords[i][0], coords[i][1]); }
    else { nodes[i]=Point(coords[i][0], coords[i][1], coords[i][2]); }
  }

  elements_.resize(nbElems);
  for (number_t i =0; i < nbElems; ++i)
  {
    number_t nbVertices = cells[i].size();
    Interpolation* interp_p;
    RefElement* re_p=nullptr;
    if (nbVertices == 3) // element is a triangle
    {
      interp_p=findInterpolation(_Lagrange, _standard, _P1, _H1);
      re_p= findRefElement(_triangle, interp_p);
    }
    else if (nbVertices == 4) // element is a quadrangle
    {
      interp_p=findInterpolation(_Lagrange, _standard, _Q1, _H1);
      re_p= findRefElement(_quadrangle, interp_p);
    }
    else { error("index_out_of_range", "nbVertices", 3, 4); }
    // create GeomElement
    elements_[i]=new GeomElement(this, re_p, spaceDim, i+1);
    MeshElement * melt=elements_[i]->meshElement();
    melt->vertexNumbers.resize(nbVertices);
    melt->nodeNumbers.resize(nbVertices);
    for (number_t j=0; j < nbVertices; ++j)
    {
      melt->vertexNumbers[j] = melt->nodeNumbers[j] = cells[i][j];
    }
    melt->setNodes(nodes);
  }

  return 2;
}

//! loads mesh from input stream according to RECTILINEAR_GRID dataset in vtk mesh format
dimen_t Mesh::loadRectilinearVtk(std::istream& data, number_t nodesDim)
{
  string_t strVal;
  number_t numVal;

  data >> strVal;
  if (strVal != "DIMENSIONS") { error("vtk_bad_block", words("First"), "RECTILINEAR_GRID", "DIMENSIONS"); }
  number_t nx, ny, nz;
  data >> nx >> ny >> nz;
  std::vector<std::vector<real_t> > coords(nx*ny*nz,std::vector<real_t>(3));

  data >> strVal;
  if (strVal != "X_COORDINATES") { error("vtk_bad_block", words("Second"), "RECTILINEAR_GRID", "X_COORDINATES"); }
  data >> numVal;
  if (numVal != nx) { error("bad_size", "", nx, numVal); }
  data >> strVal; // data type of coordinates (ignored)
  for (number_t i=0; i < nx; ++i) { data >> coords[i][0]; }

  data >> strVal;
  if (strVal != "Y_COORDINATES") { error("vtk_bad_block", words("Third"), "RECTILINEAR_GRID", "Y_COORDINATES"); }
  data >> numVal;
  if (numVal != ny) { error("bad_size", "", ny, numVal); }
  data >> strVal; // data type of coordinates (ignored)
  for (number_t i=0; i < ny; ++i) { data >> coords[i][1]; }

  data >> strVal;
  if (strVal != "Z_COORDINATES") { error("vtk_bad_block", words("Fourth"), "RECTILINEAR_GRID", "Z_COORDINATES"); }
  data >> numVal;
  if (numVal != nz) { error("bad_size", "", nz, numVal); }
  data >> strVal; // data type of coordinates (ignored)
  for (number_t i=0; i < nz; ++i) { data >> coords[i][2]; }

  // building vertices
  vertices_.resize(nx*ny*nz);
  for (number_t k=0; k < nz; ++k)
  {
    for (number_t j=0; j < ny; ++j)
    {
      for (number_t i=0; i < nx; ++i)
      {
        number_t l=k*nx*ny+j*nx+i;
        vertices_[l]=l+1;
      }
    }
  }
  // if nx=1 and ny=1 -> segments along the z axis
  // if nx=1 and nz=1 -> segments along the y axis
  // if ny=1 and nz=1 -> segments along the x axis
  // if nx=1 -> quadrangles along the yz plane
  // if ny=1 -> quadrangles along the xz plane
  // if nz=1 -> quadrangles along the xy plane
  // else hexahedra

  Interpolation* interp_p;
  if (ny*nz==1 || nx*nz==1 || nx*ny==1)
  {
    // segments are P1
    interp_p=findInterpolation(_Lagrange, _standard, _P1, _H1);
  }
  else
  {
    // quadrangles and hexahedra are Q1
    interp_p=findInterpolation(_Lagrange, _standard, _Q1, _H1);
  }

  dimen_t spaceDim=3;
  if (nx == 1 && ny == 1) { spaceDim=1; } // segments along the z axis
  else if (nx == 1 && nz == 1) { spaceDim=1; } // segments along the y axis
  else if (ny == 1 && nz == 1) { spaceDim=1; } // segments along the x axis
  else if (nx == 1) { spaceDim=2; } // quadrangles along the yz plane
  else if (ny == 1) { spaceDim=2; } // quadrangles along the xz plane
  else if (nz == 1) { spaceDim=2; } // quadrangles along the xy plane
  if (nodesDim != 0 && nodesDim > spaceDim) { spaceDim=nodesDim; }

  nodes.resize(nx*ny*nz,Point(std::vector<real_t>(spaceDim, 0.)));
  for (number_t i=0; i < nx*ny*nz; ++i)
  {
    if (spaceDim == 1) { nodes[i]=Point(coords[i][0]); }
    else if (spaceDim == 2) { nodes[i]=Point(coords[i][0], coords[i][1]); }
    else { nodes[i]=Point(coords[i][0], coords[i][1], coords[i][2]); }
  }

  if (nx == 1 && ny == 1) // segments along the z axis
  {
    RefElement* re_p = findRefElement(_segment, interp_p);
    elements_.resize(nz-1);
    for (number_t k=0; k < nz-1; ++k)
    {
      elements_[k]=new GeomElement(this, re_p, 1, k+1);
      MeshElement* melt=elements_[k]->meshElement();
      melt->nodeNumbers.resize(2);
      melt->vertexNumbers.resize(2);
      melt->vertexNumbers[0] = melt->nodeNumbers[0] = k+1;
      melt->vertexNumbers[1] = melt->nodeNumbers[1] = k+2;
      melt->setNodes(nodes);
    }
  }
  else if (nx == 1 && nz == 1) // segments along the y axis
  {
    RefElement* re_p = findRefElement(_segment, interp_p);
    elements_.resize(ny-1);
    for (number_t j=0; j < ny-1; ++j)
    {
      elements_[j]=new GeomElement(this, re_p, 1, j+1);
      MeshElement* melt=elements_[j]->meshElement();
      melt->nodeNumbers.resize(2);
      melt->vertexNumbers.resize(2);
      melt->vertexNumbers[0] = melt->nodeNumbers[0] = j+1;
      melt->vertexNumbers[1] = melt->nodeNumbers[1] = j+2;
      melt->setNodes(nodes);
    }
  }
  else if (ny == 1 && nz == 1) // segments along the x axis
  {
    RefElement* re_p = findRefElement(_segment, interp_p);
    elements_.resize(nx-1);
    for (number_t i=0; i < nx-1; ++i)
    {
      elements_[i]=new GeomElement(this, re_p, 1, i+1);
      MeshElement* melt=elements_[i]->meshElement();
      melt->nodeNumbers.resize(2);
      melt->vertexNumbers.resize(2);
      melt->vertexNumbers[0] = melt->nodeNumbers[0] = i+1;
      melt->vertexNumbers[1] = melt->nodeNumbers[1] = i+2;
      melt->setNodes(nodes);
    }
  }
  else if (nx == 1) // quadrangles along the yz plane
  {
    RefElement* re_p = findRefElement(_quadrangle, interp_p);
    elements_.resize((ny-1)*(nz-1));
    for (number_t k=0; k < nz-1; ++k)
    {
      for (number_t j=0; j < ny-1; ++j)
      {
        number_t l=k*ny+j;
        elements_[l]=new GeomElement(this, re_p, 2, l+1);
        MeshElement* melt=elements_[l]->meshElement();
        melt->nodeNumbers.resize(4);
        melt->vertexNumbers.resize(4);
        melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
        melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
        melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+ny+1;
        melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+ny+2;
        melt->setNodes(nodes);
      }
    }
  }
  else if (ny == 1) // quadrangles along the xz plane
  {
    RefElement* re_p = findRefElement(_quadrangle, interp_p);
    elements_.resize((nx-1)*(nz-1));
    for (number_t k=0; k < nz-1; ++k)
    {
      for (number_t i=0; i < nx-1; ++i)
      {
        number_t l=k*nx+i;
        elements_[l]=new GeomElement(this, re_p, 2, l+1);
        MeshElement* melt=elements_[l]->meshElement();
        melt->nodeNumbers.resize(4);
        melt->vertexNumbers.resize(4);
        melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
        melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
        melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+nx+1;
        melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+nx+2;
        melt->setNodes(nodes);
      }
    }
  }
  else if (nz == 1) // quadrangles along the xy plane
  {
    RefElement* re_p = findRefElement(_quadrangle, interp_p);
    elements_.resize((nx-1)*(ny-1));
    for (number_t j=0; j < ny-1; ++j)
    {
      for (number_t i=0; i < nx-1; ++i)
      {
        number_t l=j*nx+i;
        elements_[l]=new GeomElement(this, re_p, 2, l+1);
        MeshElement* melt=elements_[l]->meshElement();
        melt->nodeNumbers.resize(4);
        melt->vertexNumbers.resize(4);
        melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
        melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
        melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+nx+1;
        melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+nx+2;
        melt->setNodes(nodes);
      }
    }
  }
  else // hexahedra
  {
    RefElement* re_p = findRefElement(_hexahedron, interp_p);
    elements_.resize((nx-1)*(ny-1)*(nz-1));
    for (number_t k=0; k < nz-1; ++k)
    {
      for (number_t j=0; j < ny-1; ++j)
      {
        for (number_t i=0; i < nx-1; ++i)
        {
          number_t l=k*nx+ny+j*ny+i;
          elements_[l]=new GeomElement(this, re_p, 3, l+1);
          MeshElement* melt=elements_[l]->meshElement();
          melt->nodeNumbers.resize(8);
          melt->vertexNumbers.resize(8);
          melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
          melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
          melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+nx+1;
          melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+nx+2;
          melt->vertexNumbers[4] = melt->nodeNumbers[4] = l+nx*ny+1;
          melt->vertexNumbers[5] = melt->nodeNumbers[5] = l+nx*ny+2;
          melt->vertexNumbers[6] = melt->nodeNumbers[6] = l+nx*ny+nx+1;
          melt->vertexNumbers[7] = melt->nodeNumbers[7] = l+nx*ny+nx+2;
          melt->setNodes(nodes);
        }
      }
    }
  }
  return spaceDim;
}

//! loads mesh from input stream according to STRUCTURED_GRID dataset in vtk mesh format
dimen_t Mesh::loadStructuredGridVtk(std::istream& data, number_t nodesDim)
{
  string_t strVal;

  data >> strVal;
  if (strVal != "DIMENSIONS") { error("vtk_bad_block", words("First"), "STRUCTURED_GRID", "DIMENSIONS"); }
  number_t nx, ny, nz;
  data >> nx >> ny >> nz;

  data >> strVal;
  if (strVal != "POINTS") { error("vtk_bad_block", words("Second"), "STRUCTURED_GRID", "POINTS"); }
  number_t nbPoints;
  data >> nbPoints;
  if (nbPoints != nx*ny*nz) { error("bad_size", "", nx*ny*nz, nbPoints); }
  data >> strVal; // data type of coordinates (ignored)

  std::vector<std::vector<real_t> > coords(nbPoints,std::vector<real_t>(3));
  vertices_.resize(nbPoints);
  for (number_t i=0; i < nbPoints; ++i)
  {
    data >> coords[i][0] >> coords[i][1] >> coords[i][2];
    vertices_[i]=i+1;
  }

  // if nx=1 and ny=1 -> segments along the z axis
  // if nx=1 and nz=1 -> segments along the y axis
  // if ny=1 and nz=1 -> segments along the x axis
  // if nx=1 -> quadrangles along the yz plane
  // if ny=1 -> quadrangles along the xz plane
  // if nz=1 -> quadrangles along the xy plane
  // else hexahedra

  Interpolation* interp_p;
  if (ny*nz==1 || nx*nz==1 || nx*ny==1)
  {
    // segments are P1
    interp_p=findInterpolation(_Lagrange, _standard, _P1, _H1);
  }
  else
  {
    // quadrangles and hexahedra are Q1
    interp_p=findInterpolation(_Lagrange, _standard, _Q1, _H1);
  }

  dimen_t spaceDim=3;
  if (nx == 1 && ny == 1) { spaceDim=1; } // segments along the z axis
  else if (nx == 1 && nz == 1) { spaceDim=1; } // segments along the y axis
  else if (ny == 1 && nz == 1) { spaceDim=1; } // segments along the x axis
  else if (nx == 1) { spaceDim=2; } // quadrangles along the yz plane
  else if (ny == 1) { spaceDim=2; } // quadrangles along the xz plane
  else if (nz == 1) { spaceDim=2; } // quadrangles along the xy plane
  if (nodesDim != 0 && nodesDim > spaceDim) { spaceDim=nodesDim; }

  nodes.resize(nbPoints,Point(std::vector<real_t>(spaceDim, 0.)));
  for (number_t i=0; i < nbPoints; ++i)
  {
    if (spaceDim == 1) { nodes[i]=Point(coords[i][0]); }
    else if (spaceDim == 2) { nodes[i]=Point(coords[i][0], coords[i][1]); }
    else { nodes[i]=Point(coords[i][0], coords[i][1], coords[i][2]); }
  }

  if (nx == 1 && ny == 1) // segments along the z axis
  {
    RefElement* re_p = findRefElement(_segment, interp_p);
    elements_.resize(nz-1);
    for (number_t k=0; k < nz-1; ++k)
    {
      elements_[k]=new GeomElement(this, re_p, 1, k+1);
      MeshElement* melt=elements_[k]->meshElement();
      melt->nodeNumbers.resize(2);
      melt->vertexNumbers.resize(2);
      melt->vertexNumbers[0] = melt->nodeNumbers[0] = k+1;
      melt->vertexNumbers[1] = melt->nodeNumbers[1] = k+2;
      melt->setNodes(nodes);
    }
  }
  else if (nx == 1 && nz == 1) // segments along the y axis
  {
    RefElement* re_p = findRefElement(_segment, interp_p);
    elements_.resize(ny-1);
    for (number_t j=0; j < ny-1; ++j)
    {
      elements_[j]=new GeomElement(this, re_p, 1, j+1);
      MeshElement* melt=elements_[j]->meshElement();
      melt->nodeNumbers.resize(2);
      melt->vertexNumbers.resize(2);
      melt->vertexNumbers[0] = melt->nodeNumbers[0] = j+1;
      melt->vertexNumbers[1] = melt->nodeNumbers[1] = j+2;
      melt->setNodes(nodes);
    }
  }
  else if (ny == 1 && nz == 1) // segments along the x axis
  {
    RefElement* re_p = findRefElement(_segment, interp_p);
    elements_.resize(nx-1);
    for (number_t i=0; i < nx-1; ++i)
    {
      elements_[i]=new GeomElement(this, re_p, 1, i+1);
      MeshElement* melt=elements_[i]->meshElement();
      melt->nodeNumbers.resize(2);
      melt->vertexNumbers.resize(2);
      melt->vertexNumbers[0] = melt->nodeNumbers[0] = i+1;
      melt->vertexNumbers[1] = melt->nodeNumbers[1] = i+2;
      melt->setNodes(nodes);
    }
  }
  else if (nx == 1) // quadrangles along the yz plane
  {
    RefElement* re_p = findRefElement(_quadrangle, interp_p);
    elements_.resize((ny-1)*(nz-1));
    for (number_t k=0; k < nz-1; ++k)
    {
      for (number_t j=0; j < ny-1; ++j)
      {
        number_t l=k*ny+j;
        elements_[l]=new GeomElement(this, re_p, 2, l+1);
        MeshElement* melt=elements_[l]->meshElement();
        melt->nodeNumbers.resize(4);
        melt->vertexNumbers.resize(4);
        melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
        melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
        melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+ny+1;
        melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+ny+2;
        melt->setNodes(nodes);
      }
    }
  }
  else if (ny == 1) // quadrangles along the xz plane
  {
    RefElement* re_p = findRefElement(_quadrangle, interp_p);
    elements_.resize((nx-1)*(nz-1));
    for (number_t k=0; k < nz-1; ++k)
    {
      for (number_t i=0; i < nx-1; ++i)
      {
        number_t l=k*nx+i;
        elements_[l]=new GeomElement(this, re_p, 2, l+1);
        MeshElement* melt=elements_[l]->meshElement();
        melt->nodeNumbers.resize(4);
        melt->vertexNumbers.resize(4);
        melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
        melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
        melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+nx+1;
        melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+nx+2;
        melt->setNodes(nodes);
      }
    }
  }
  else if (nz == 1) // quadrangles along the xy plane
  {
    RefElement* re_p = findRefElement(_quadrangle, interp_p);
    elements_.resize((nx-1)*(ny-1));
    for (number_t j=0; j < ny-1; ++j)
    {
      for (number_t i=0; i < nx-1; ++i)
      {
        number_t l=j*nx+i;
        elements_[l]=new GeomElement(this, re_p, 2, l+1);
        MeshElement* melt=elements_[l]->meshElement();
        melt->nodeNumbers.resize(4);
        melt->vertexNumbers.resize(4);
        melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
        melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
        melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+nx+1;
        melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+nx+2;
        melt->setNodes(nodes);
      }
    }
  }
  else // hexahedra
  {
    RefElement* re_p = findRefElement(_hexahedron, interp_p);
    elements_.resize((nx-1)*(ny-1)*(nz-1));
    for (number_t k=0; k < nz-1; ++k)
    {
      for (number_t j=0; j < ny-1; ++j)
      {
        for (number_t i=0; i < nx-1; ++i)
        {
          number_t l=k*nx+ny+j*ny+i;
          elements_[l]=new GeomElement(this, re_p, 3, l+1);
          MeshElement* melt=elements_[l]->meshElement();
          melt->nodeNumbers.resize(8);
          melt->vertexNumbers.resize(8);
          melt->vertexNumbers[0] = melt->nodeNumbers[0] = l+1;
          melt->vertexNumbers[1] = melt->nodeNumbers[1] = l+2;
          melt->vertexNumbers[2] = melt->nodeNumbers[2] = l+nx+1;
          melt->vertexNumbers[3] = melt->nodeNumbers[3] = l+nx+2;
          melt->vertexNumbers[4] = melt->nodeNumbers[4] = l+nx*ny+1;
          melt->vertexNumbers[5] = melt->nodeNumbers[5] = l+nx*ny+2;
          melt->vertexNumbers[6] = melt->nodeNumbers[6] = l+nx*ny+nx+1;
          melt->vertexNumbers[7] = melt->nodeNumbers[7] = l+nx*ny+nx+2;
          melt->setNodes(nodes);
        }
      }
    }
  }
  return spaceDim;
}

//! loads mesh from input stream according to UNSTRUCTURED_GRID dataset in vtk mesh format
dimen_t Mesh::loadUnstructuredGridVtk(std::istream& data, number_t nodesDim)
{
  string_t strVal;
  number_t numVal;

  data >> strVal;
  if (strVal != "POINTS") { error("vtk_bad_block", words("First"), "UNSTRUCTURED_GRID", "POINTS"); }
  number_t nbPoints;
  data >> nbPoints;
  data >> strVal; // data type of points (ignored)

  // Definition of nodes
  std::vector<std::vector<real_t> > coords(nbPoints,std::vector<real_t>(3));
  vertices_.resize(nbPoints);
  for (number_t i=0; i < nbPoints; ++i)
  {
    data >> coords[i][0] >> coords[i][1] >> coords[i][2];
    vertices_[i]=i+1;
  }

  data >> strVal;
  if (strVal != "CELLS") { error("vtk_bad_block", words("Second"), "UNSTRUCTURED_GRID", "CELLS"); }
  number_t nbElems;
  data >> nbElems; // number of cells = number of elements
  data >> numVal; // number of data to be read (useless)
  std::vector<std::vector<number_t> > cells(nbElems,std::vector<number_t>(8));
  std::vector<ShapeType> celltypes(nbElems);

  for (number_t c=0; c < nbElems; ++c)
  {
    number_t cellSize;
    data >> cellSize;
    cells[c].resize(cellSize);
    for (number_t i=0; i < cellSize; ++i)
    {
      data >> numVal;
      cells[c][i]=numVal+1;
    }
  }

  data >> strVal;
  if (strVal != "CELL_TYPES") { error("vtk_bad_block", words("Third"), "UNSTRUCTURED_GRID", "CELL_TYPES"); }
  data >> numVal; // number of cell types = number of cells
  if (numVal != nbElems) { error("bad_size", "", nbElems, numVal); }
  data >> numVal; // number of data to be read (useless)

  std::map<number_t, ShapeType> vtkShape;
  std::map<number_t, number_t> vtkDim;
  vtkShape[1]=_point;         vtkDim[1]=0;
  vtkShape[3]=_segment;       vtkDim[3]=1;
  vtkShape[5]=_triangle;      vtkDim[5]=2;
  vtkShape[9]=_quadrangle;    vtkDim[9]=2;
  vtkShape[10]=_tetrahedron;  vtkDim[10]=3;
  vtkShape[12]=_hexahedron;   vtkDim[12]=3;
  vtkShape[13]=_prism;        vtkDim[13]=3;
  vtkShape[14]=_pyramid;      vtkDim[14]=3;

  dimen_t spaceDim=1;
  for (number_t c=0; c < nbElems; ++c)
  {
    data >> numVal;
    celltypes[c]=vtkShape[numVal];
    if (vtkDim[numVal] > spaceDim) { spaceDim = vtkDim[numVal]; }
  }
  if (nodesDim != 0 && nodesDim > spaceDim) { spaceDim=nodesDim; }

  if (spaceDim == 2)
  {
    // we have to check if the geometry is 2D or 3D
    if (coords.size() > 4)
    {
      // we take 4 vertices randomly and check their coplanarity
      number_t v1=0, v2=(nbPoints-1)/3, v3=(2*(nbPoints-1))/3, v4=nbPoints-1;
      if ( std::abs(dot(coords[v2]-coords[v1],crossProduct(coords[v3]-coords[v1],coords[v4]-coords[v1]))) > theEpsilon ) { spaceDim=3; }
    }
  }

  nodes.resize(nbPoints,Point(std::vector<real_t>(spaceDim, 0.)));
  for (number_t i=0; i < nbPoints; ++i)
  {
    if (spaceDim == 1) { nodes[i]=Point(coords[i][0]); }
    else if (spaceDim == 2) { nodes[i]=Point(coords[i][0], coords[i][1]); }
    else { nodes[i]=Point(coords[i][0], coords[i][1], coords[i][2]); }
  }

  elements_.resize(nbElems);
  for (number_t c =0; c < nbElems; ++c)
  {
    Interpolation* interp_p;
    if (celltypes[c] == _quadrangle || celltypes[c] == _hexahedron)
    {
      interp_p=findInterpolation(_Lagrange, _standard, _Q1, _H1);
    }
    else
    {
      interp_p=findInterpolation(_Lagrange, _standard, _P1, _H1);
    }
    RefElement* re_p=findRefElement(celltypes[c], interp_p);

    // create GeomElement
    elements_[c]=new GeomElement(this, re_p, spaceDim, c+1);
    MeshElement * melt=elements_[c]->meshElement();
    number_t nbVertices = cells[c].size();
    melt->vertexNumbers.resize(nbVertices);
    melt->nodeNumbers.resize(nbVertices);
    for (number_t i=0; i < nbVertices; ++i)
    {
      melt->vertexNumbers[i] = melt->nodeNumbers[i] = cells[c][i];
    }
    melt->setNodes(nodes);
  }

  return spaceDim;
}

//! loads mesh from input file filename according to vtk mesh format
void Mesh::loadVtk(const string_t& filename, number_t nodesDim)
{
  trace_p->push("Mesh::loadVtk");
  // error("not_yet_implemented", "Mesh::loadVtk(String filename)");

  std::ifstream data(filename.c_str());
  if ( !data ) { error("file_failopen","Mesh::loadVtk",filename); }

  comment_ = string_t("VTK mesh format read from file ") + basenameWithExtension(filename);
  if ( theVerboseLevel > 1) { info("loadFile_info", "VTK", filename); }

  string_t strVal;

  data >> strVal;
  bool firstLineError = false;

  if (strVal != "#") { firstLineError=true; }
  if (!firstLineError) { data >> strVal; }
  if (!firstLineError && strVal != "vtk") { firstLineError=true; }
  if (!firstLineError) { data >> strVal; }
  if (!firstLineError && strVal != "DataFile") { firstLineError=true; }
  if (!firstLineError) { data >> strVal; }
  if (!firstLineError && strVal != "Version") { firstLineError=true; }
  if (firstLineError) { error("vtk_format_block_first_line"); }
  real_t version;
  data >> version;

  // getting until end of line (normally it is empty)
  getline(data,strVal);
  // next line is a comment line
  getline(data,strVal);

  data >> strVal;
  if (strVal != "ASCII") { error("non_ascii"); }
  data >> strVal;
  if (strVal != "DATASET") { error("vtk_bad_format"); }
  string_t dataset;
  data >> dataset;
  dimen_t spaceDim=0;
  if (dataset == "POLYDATA") { spaceDim=loadPolyDataVtk(data, nodesDim); }
  else if (dataset == "RECTILINEAR_GRID") { spaceDim=loadRectilinearVtk(data, nodesDim); }
  else if (dataset == "STRUCTURED_GRID") { spaceDim=loadStructuredGridVtk(data, nodesDim); }
  else if (dataset == "UNSTRUCTURED_GRID") { spaceDim=loadUnstructuredGridVtk(data, nodesDim); }
  else { error("vtk_bad_format"); }

  // management of domain
  MeshDomain* meshdom_p = (new GeomDomain(*this, "Omega", spaceDim))->meshDomain();
  meshdom_p->geomElements.assign(elements_.begin(), elements_.end());
  domains_.push_back(meshdom_p);
  order_=1;
  lastIndex_ = elements_.size();

  //compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  //sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)

  firstOrderMesh_p = this;

  trace_p->pop();
}

//! loads mesh from input file filename according to vtp mesh format
void Mesh::loadVtp(const string_t& filename, number_t nodesDim)
{
  trace_p->push("Mesh::loadVtp");
  error("not_yet_implemented", "Mesh::loadVtp(String filename)");

  comment_ = string_t("VTP mesh format read from file ") + basenameWithExtension(filename);
  if ( theVerboseLevel > 1) { info("loadFile_info", "VTP", filename); }

  trace_p->pop();
}

//! loads mesh from input file filename according to vtr mesh format
void Mesh::loadVtr(const string_t& filename, number_t nodesDim)
{
  trace_p->push("Mesh::loadVtr");
  error("not_yet_implemented", "Mesh::loadVtr(String filename)");

  comment_ = string_t("VTR mesh format read from file ") + basenameWithExtension(filename);
  if ( theVerboseLevel > 1) { info("loadFile_info", "VTR", filename); }

  trace_p->pop();
}

//! loads mesh from input file filename according to vts mesh format
void Mesh::loadVts(const string_t& filename, number_t nodesDim)
{
  trace_p->push("Mesh::loadVts");
  error("not_yet_implemented", "Mesh::loadVts(String filename)");

  comment_ = string_t("VTS mesh format read from file ") + basenameWithExtension(filename);
  if ( theVerboseLevel > 1) { info("loadFile_info", "VTS", filename); }

  trace_p->pop();
}

//! loads mesh from input file filename according to vtu mesh format
void Mesh::loadVtu(const string_t& filename, number_t nodesDim)
{
  trace_p->push("Mesh::loadVtu");
  error("not_yet_implemented", "Mesh::loadVtu(String filename)");

  comment_ = string_t("VTU mesh format read from file ") + basenameWithExtension(filename);
  if ( theVerboseLevel > 1) { info("loadFile_info", "VTU", filename); }

  trace_p->pop();
}

} // end of namespace xlifepp
