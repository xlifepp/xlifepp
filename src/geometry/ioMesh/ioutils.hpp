/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ioutils.hpp
  \authors N. Kielbasiewicz, Y. Lafranche
  \since 13 jul 2016
  \date 13 jul 2016

  \brief Definition of utility functions to read data from mesh files of various formats
*/

#include "../Mesh.hpp"
#include <cstdio>

namespace xlifepp {

//----------------------------------------------------------------------------------------
// using fscanf
inline void readInt(FILE * data, number_t& Val)
{
#ifdef COMPILER_IS_32_BITS
  fscanf(data, "%u", &Val);
#else
  #ifdef OS_IS_WIN
    fscanf(data, "%Iu", &Val);
  #else
    fscanf(data, "%zu", &Val);
  #endif
#endif
}
inline void readRea(FILE * data, real_t& Val)
{
#ifdef STD_TYPES
  fscanf(data, "%f", &Val);
#endif
#ifdef LONG_TYPES
  fscanf(data, "%lf", &Val);
#endif
#ifdef LONGLONG_TYPES
  fscanf(data, "%Lf", &Val);
#endif
}
inline bool readStr(FILE * data, string_t& Val)
{
  char str[100];
  fscanf(data, "%s", str);
  Val = str; return (! feof(data));
}
inline void closeFile(FILE * data) { fclose(data); }

// using iostream
inline void readInt(std::ifstream& data, number_t& Val) { data >> Val; }
inline void readRea(std::ifstream& data, real_t&   Val) { data >> Val; }
inline bool readStr(std::ifstream& data, string_t& Val) { data >> Val; return (! data.eof()); }
inline void closeFile(std::ifstream& data) { data.close(); }

//----------------------------------------------------------------------------------------
// Look for string BeginSection in the Gmsh data file connected to the input stream data.
// Returns true on success, false otherwise.
bool lookfor(const string_t BeginSection, FILE *data);
bool lookfor(const string_t BeginSection, std::ifstream &data);

//----------------------------------------------------------------------------------------
// Records the sides of the element el_p in the map parentEl.
// noelt is the rank of this element in the vector allElts declared in loadGmsh or loadMedit
// The map associates each side Sk of the element to a pair made of the two integers
// (element, side number). Sk is a unique key, whose type is vector<number_t>, built from
// its nodes numbers, read in the mesh data file.
// In short, the map is: Sk ---> (noelt, k)
typedef std::pair<number_t, number_t> pairNN;
typedef std::multimap<std::vector<number_t>, pairNN > SIDELTMAP;
void storeElSides(const GeomElement* el_p, number_t noelt, SIDELTMAP &parentEl);

//----------------------------------------------------------------------------------------
// Retrieve the parent element of the side element defined by its nodes numbers nums,
// given in any order, from the map parentEl declared in loadGmsh.
// If the side is found, the function returns true and sets itpar to the pair of iterators
// of the form [begin, end[ defining the range of elements in parentEl that own the key nums,
// with itpar.first = begin and itpar.second = end.
// Indeed, a side may have 1 parent, pointed to by begin in which case end = begin+1:
//     begin    --> pair< nums, (element, side number) >
//     end      -->
// or 2 parents, pointed to by begin and begin+1 in which case end = begin+2:
//     begin    --> pair< nums, (element1, side number1) >
//     begin+1  --> pair< nums, (element2, side number2) >
//     end      -->
// More than 2 parents is a erroneous configuration.
// If the side is not found in the map, the function returns false and itpar should be considered
// as undefined ; as such these iterators should not be dereferenced.
// The requested information is the mapped value, which is itself a pair (element, side number)
// that can be accessed by the following example code:
// for (SIDELTMAP::const_iterator it=itpar.first; it != itpar.second; it++) { mapped_value = it->second; }
typedef std::pair<SIDELTMAP::const_iterator, SIDELTMAP::const_iterator> ITPARENTS;
bool foundParent(const std::vector<number_t>& nums, const SIDELTMAP &parentEl, ITPARENTS &itpar);
// Returns the number of parents found in the map parentEl
int nbPar(const ITPARENTS &itpar);

//----------------------------------------------------------------------------------------
// Utilitary structure to store data related to the element types found in the Gmsh data file
struct ELTDEF {
  number_t nbPts,   // number of points defining the element
           elmDim;  // dimension of the element
  RefElement* refElt_p;  // associated reference element in XLiFE++

  ELTDEF() {}
  ELTDEF(number_t& nP, number_t& eD, RefElement*& rE)
    :nbPts(nP), elmDim(eD), refElt_p(rE) {}
};

} // end of namespace xlifepp
