/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file mtlb.cpp
  \author Y. Lafranche
  \since 22 feb 2016
  \date 22 feb 2016

  \brief Implementation of xlifepp::Mesh class members related to input and output Matlab file format

*/

#include "../Mesh.hpp"

namespace xlifepp {
using std::endl;
using std::pair;
using std::vector;

void mtlbExport(const GeomDomain& dom, const vector<Point>& coords,
                const vector<pair<ShapeType, vector<number_t> > >& elementsInfo, std::ostream& out) {

  if (dom.domType()==_meshDomain) {
    out << "spacedim = " << coords[0].size() << "; % = size(coord,2)" << endl;
    out << "domaindim =" << dom.dim() << "; domainname = '" << dom.name() << "';" << endl;

  // nodes list
    out << "% List of interpolation nodes, implicitly numbered from 1 to " << coords.size() << endl;
    out << "coord=[" << endl;
    for (vector<Point>::const_iterator it_node=coords.begin(); it_node!=coords.end(); it_node++) {
      for (number_t j=0; j<it_node->size(); ++j) { out << " " << (*it_node)[j]; }
      out << endl;
    }
    out << "];" << endl;

  // elements list
    out << "% Type of each element present in the mesh." << endl;
    out << "% Each of them is a code number in XLiFE++'s internal codification:" << endl;
    out << "%   2 = point, 3 = segment, 4 = triangle, 5 = quadrangle," << endl;
    out << "%   6 = tetrahedron, 7 = hexahedron, 8 = prism, 9 = pyramid." << endl;
    out << "elemtype=[" << endl;
  //  1. write element types and detect max number of vertices by element, irrespective of element type
    number_t nbvx = 0;
    for (vector<pair<ShapeType, vector<number_t> > >::const_iterator it_elem=elementsInfo.begin();
         it_elem != elementsInfo.end(); it_elem++) {
         out << it_elem->first << endl;
         number_t n = it_elem->second.size();
         if (n > nbvx) { nbvx = n; }
    }
    out << "];" << endl;
  //  2. write elements, padding with NaNs if necessary
    out << "% List of " << elementsInfo.size() << " elements" << endl;
    out << "% Format of the array: one element per row, column i holds the i-th" << endl;
    out << "% interpolation node, given by its number in the array coord above." << endl;
    out << "% Shortest lists of vertices are padded with NaNs to use the patch function."<< endl;
    out << "elem=[" << endl;
    for (vector<pair<ShapeType, vector<number_t> > >::const_iterator it_elem=elementsInfo.begin();
         it_elem != elementsInfo.end(); it_elem++) {
      for (vector<number_t>::const_iterator it=it_elem->second.begin(); it != it_elem->second.end(); it++) { out << " " << *it ; }
      for (number_t in=it_elem->second.size(); in < nbvx; in++) { out << " NaN" ; }
      out << endl;
    }
    out << "];" << endl;
  }
  else { error("domain_notmesh",dom.name(),words("domain type",dom.domType())); }
}

} // end of namespace xlifepp
