/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file loadPly.cpp
  \author N. Kielbasiewicz
  \since 5 may 2015
  \date 6 may 2015

  \brief Read a file containing a mesh in PLY format

  The useful function is loadPly, which is a member function of the class Mesh.
*/

#include "../Mesh.hpp"

namespace xlifepp {

/*!
  \struct PlyProperty
  Definition of a Property according to PLY format
 */
struct PlyProperty
{
  string_t name; //!< name of the property (x, y, z, vertex_index, ...)
  string_t type; //!< type of the property (char, uchar, short, ushort, int, uint, float, double)
  string_t subtype; //!< sub type of the property when is a list
  bool isList; //!< true if it is a list
};

/*!
  \struct PlyElement
  Definition of an Element according to PLY format
 */
struct PlyElement
{
  number_t n; //!< number of elements
  std::vector<PlyProperty> properties; //!< properties of this element
};

//! read element and its properties
string_t readPlyElement(PlyElement& e, std::istream& data)
{
  string_t strVal;
  data >> e.n;
  data >> strVal;
  while (strVal == "property")
  {
    PlyProperty prop;
    data >> strVal;
    if (strVal != "list")
    {
      prop.type=strVal;
      prop.subtype="";
      prop.isList=false;
      data >> prop.name;
    }
    else // strVal == "list"
    {
      // a property list is a line with the following syntax
      //        property list <type> <type> <name>
      data >> prop.type >> prop.subtype >> prop.name;
      prop.isList=true;
    }
    e.properties.push_back(prop);
    data >> strVal;
  }
  return strVal;
}

//! loads mesh from input file filename according to ply mesh format
void Mesh::loadPly(const string_t& filename, number_t nodesDim)
{
  trace_p->push("Mesh::loadPly");

  std::ifstream data(filename.c_str());
  if ( !data ) { error("file_failopen","Mesh::loadPly",filename); }

  comment_ = string_t("PLY mesh format read from file ") + basenameWithExtension(filename);
  if ( theVerboseLevel > 1) { info("loadFile_info", "PLY", filename); }

  string_t strVal;
  real_t rVal;
  int_t iVal;
  /*
    Mesh characteristics
    the header files begins by the keyword ply on the first line and ends with the keyword end_header
    each line begins by one of the following keyword:
   */
  data >> strVal;
  if (strVal != "ply") { error("ply_first_line"); }
  data >> strVal;
  if (strVal != "format") { error("ply_second_line"); }
  data >> strVal;
  if (strVal != "ascii") { error("non_ascii"); }
  real_t version;
  data >> version;
  data >> strVal;

  PlyElement vertex, edge, face;
  std::vector<string_t> elements_order;
  std::vector<std::pair<number_t, number_t> > edges; // temporary array containing edge elements
  std::vector<std::vector<number_t> > faces; // temporary array containing face elements

  while(strVal != "end_header")
  {
    if (strVal == "comment") { getline(data, strVal); data >> strVal; }
    else if (strVal == "element")
    {
      data >> strVal;
      if (strVal == "vertex") { strVal=readPlyElement(vertex,data); elements_order.push_back("vertex"); }
      if (strVal == "face") { strVal=readPlyElement(face,data); elements_order.push_back("face"); }
      // if (strVal == "edge") { strVal=readPlyElement(edge,data); elements_order.push_back("edge"); }
    }
    else
    {
      data >> strVal;
    }
  }

  /*
    Definition of elements
  */
  number_t i=0;
  while (i < elements_order.size())
  {
    if (elements_order[i] == "vertex")
    {
      nodes.resize(vertex.n);
      vertices_.resize(vertex.n);
      // tester si x, y et z sont dans les proprietes
      for (number_t v=0; v < vertex.n; ++v)
      {
        // every node is a vertex
        vertices_[v]=v+1;
        for (number_t j=0; j < vertex.properties.size(); ++j)
        {
          if (vertex.properties[j].type == "float" || vertex.properties[j].type == "double")
          {
            data >> rVal;
            if (vertex.properties[j].name == "x" || vertex.properties[j].name == "y" || vertex.properties[j].name == "z")
            {
              // coordinates of new vertex
              nodes[v].push_back(rVal);
            }
          }
          else
          {
            data >> iVal;
          }
        }
      }
    }
    if (elements_order[i] == "face")
    {
      // "faces" are plain elements, on a unique domain
      elements_.resize(face.n);
      //! check if the property vertex_index exists
      bool isVertexIndex=false;
      for (number_t j=0; j < face.properties.size(); ++j)
      {
        if (face.properties[j].isList)
        {
          if (face.properties[j].name == "vertex_index" || face.properties[j].name == "vertex_indices")
          { isVertexIndex=true; }
        }
      }
      if (!isVertexIndex) { error("ply_bad_nbvert", "face"); }
      std::vector<std::vector<number_t> > cells(face.n);

      for (number_t f=0; f < face.n; ++f)
      {
        number_t nbVertices;
        data >> nbVertices;
        cells[f].resize(nbVertices);
        for (number_t j=0; j < nbVertices; ++j)
        {
          data >> iVal;
          cells[f][j]=iVal+1;
        }
      }

      // we have to detect properly if spaceDim is 2 or 3
      // perhaps the good way to do it is to take 4 points randomly and check if they are coplanar or not
      number_t spaceDim=2;
      number_t nnodes=nodes.size();
      if (nodes.size() > 4)
      {
        // we take 4 vertices randomly and check their coplanarity
        number_t v1=0, v2=(nnodes-1)/3, v3=(2*(nnodes-1))/3, v4=nnodes-1;
        if ( std::abs(dot(nodes[v2]-nodes[v1],crossProduct(nodes[v3]-nodes[v1],nodes[v4]-nodes[v1]))) > theEpsilon ) { spaceDim=3; }
      }
      for (number_t f=0; f < face.n; ++f)
      {
        number_t nbVertices = cells[f].size();
        Interpolation* interp_p;
        RefElement* re_p=nullptr;
        if (nbVertices == 3) // face elements are triangles
        {
          interp_p=findInterpolation(_Lagrange, _standard, _P1, _H1);
          re_p= findRefElement(_triangle, interp_p);
        }
        else if (nbVertices == 4) // face elements are quadrangle
        {
          interp_p=findInterpolation(_Lagrange, _standard, _Q1, _H1);
          re_p= findRefElement(_quadrangle, interp_p);
        }
        else { error("index_out_of_range", "nbVertices", 3, 4); }
        // create GeomElement
        elements_[f]=new GeomElement(this, re_p, spaceDim, f+1);
        MeshElement* melt=elements_[f]->meshElement();
        melt->vertexNumbers.resize(nbVertices);
        melt->nodeNumbers.resize(nbVertices);
        for (number_t j=0; j < nbVertices; ++j)
        {
          melt->vertexNumbers[j] = melt->nodeNumbers[j] = cells[f][j];
        }
        melt->setNodes(nodes);
      }
    }
    // if (elements_order[i] == "edge")
    // {
    //   // "edges" are side elements
    //   // we have to find their parent element
    // }

    ++i;
  }

  // management of domain
  MeshDomain* meshdom_p = (new GeomDomain(*this, "Omega", 2))->meshDomain();
  meshdom_p->geomElements.assign(elements_.begin(), elements_.end());
  domains_.push_back(meshdom_p);
  order_=1;
  lastIndex_ = elements_.size();

  //compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  //sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)

  firstOrderMesh_p = this;

  trace_p->pop();
}

} // end of namespace xlifepp
