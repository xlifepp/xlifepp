/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ioutils.cpp
  \authors N. Kielbasiewicz, Y. Lafranche
  \since 13 jul 2016
  \date 13 jul 2016

  \brief Implementation of utility functions to read data from mesh files of various formats
*/

#include "ioutils.hpp"

namespace xlifepp {
using std::ifstream;
using std::make_pair;
using std::multimap;
using std::ostringstream;
using std::pair;
using std::vector;

//----------------------------------------------------------------------------------------
// Look for string BeginSection in the Gmsh data file connected to the input stream data.
// Returns true on success, false otherwise.
bool lookfor(const string_t BeginSection, FILE *data) {
 // Version for Windows only (three times as fast as the ifstream method)
  string_t strVal;
  char str[100];
  int i=fscanf(data, "%s", str);
  while (! feof(data)) {
    strVal = str;
    if (strVal == BeginSection) { return true; }
    i=fscanf(data, "%s", str);
  }
  // End of file reached without detecting the expected string.
  // Restore stream state for possible proper later use.
  clearerr(data);
  return false;
}
bool lookfor(const string_t BeginSection, ifstream &data) {
  string_t strVal;
  data >> strVal;
  while (data.good()) {
    if (strVal == BeginSection) { return true; }
    data >> strVal;
  }
  // End of file reached without detecting the expected string.
  // Restore stream state for possible proper later use.
  data.clear();
  return false;
}

//----------------------------------------------------------------------------------------
// Records the sides of the element el_p in the map parentEl.
// noelt is the rank of this element in the vector allElts declared in loadGmsh.
// The map associates each side Sk of the element to a pair made of the two integers
// (element, side number). Sk is a unique key, whose type is vector<number_t>, built from
// its nodes numbers, read in the mesh data file.
// In short, the map is: Sk ---> (noelt, k)
void storeElSides(const GeomElement* el_p, number_t noelt, SIDELTMAP &parentEl) {
  for (number_t side_no=1; side_no <= el_p->numberOfSides(); side_no++) {
    vector<number_t> vns = el_p->nodeNumbers(side_no);
    sort(vns.begin(),vns.end());
    parentEl.insert(make_pair(vns, make_pair(noelt, side_no)));
  }
}

//----------------------------------------------------------------------------------------
// Retrieve the parent element of the side element defined by its nodes numbers nums,
// given in any order, from the map parentEl declared in loadGmsh.
// If the side is found, the function returns true and sets itpar to the pair of iterators
// of the form [begin, end[ defining the range of elements in parentEl that own the key nums,
// with itpar.first = begin and itpar.second = end.
// Indeed, a side may have 1 parent, pointed to by begin in which case end = begin+1:
//     begin    --> pair< nums, (element, side number) >
//     end      -->
// or 2 parents, pointed to by begin and begin+1 in which case end = begin+2:
//     begin    --> pair< nums, (element1, side number1) >
//     begin+1  --> pair< nums, (element2, side number2) >
//     end      -->
// More than 2 parents is a erroneous configuration.
// If the side is not found in the map, the function returns false and itpar should be considered
// as undefined ; as such these iterators should not be dereferenced.
// The requested information is the mapped value, which is itself a pair (element, side number)
// that can be accessed by the following example code:
// for (SIDELTMAP::const_iterator it=itpar.first; it != itpar.second; it++) { mapped_value = it->second; }
bool foundParent(const vector<number_t>& nums, const SIDELTMAP &parentEl, ITPARENTS &itpar) {
  vector<number_t> vns(nums);
  sort(vns.begin(),vns.end());
  itpar = parentEl.equal_range(vns);
  return itpar.first != itpar.second;
}
// Returns the number of parents found in the map parentEl
int nbPar(const ITPARENTS &itpar) {
  int nb=0;
  for (SIDELTMAP::const_iterator it=itpar.first; it != itpar.second; it++) { nb++; }
  return nb;
}

} // end of namespace xlifepp
