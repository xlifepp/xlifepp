/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file msh.cpp
  \author N. Kielbasiewicz
  \since 25 mar 2013
  \date 28 mar 2013

  \brief Implementation of xlifepp::Mesh class members related to input and output msh file format (gmsh)

  \verbatim
  ============================================================================
                               MSH 2.0 format
  ============================================================================
  $MeshFormat
  version-number file-type data-size
  $EndMeshFormat
  $Nodes
  number-of-nodes
  node-number x-coord y-coord z-coord
  ...
  $EndNodes
  $Elements
  number-of-elements
  elm-number elm-type number-of-tags < tag > ... node-number-list
  ...
  $EndElements
  $PhysicalNames
  number-of-physical-names
  physical-number "physical-name"
  ...
  $EndPhysicalNames
  $NodeData
  number-of-string-tags
  < "string-tag" >
  ...
  number-of-real-tags
  < real-tag >
  ...
  number-of-integer-tags
  < integer-tag >
  ...
  node-number value ...
  ...
  $EndNodeData
  $ElementData
  number-of-string-tags
  < "string-tag" >
  ...
  number-of-real-tags
  < real-tag >
  ...
  number-of-integer-tags
  < integer-tag >
  ...
  elm-number value ...
  ...
  $EndElementData
  $ElementNodeData
  number-of-string-tags
  < "string-tag" >
  ...
  number-of-real-tags
  < real-tag >
  ...
  number-of-integer-tags
  < integer-tag >
  ...
  elm-number number-of-nodes-per-element value ...
  ...
  $ElementEndNodeData
  \endverbatim

  ============================================================================
                               MSH 2.1 format
  ============================================================================
  the only change concerns the physical names definition:

  \verbatim
  $PhysicalNames
  number-of-physical-names
  physical-dimension physical-number "physical-name"
  ...
  $EndPhysicalNames
  \endverbatim

  ============================================================================
                               MSH 2.2 format
  ============================================================================
  the only change concerns the tag list in $Element block with introduction of
  number and id list of potential partition

  elm-type values in MSH:
  1=P1 segment, 2=P1 triangle, 3=Q1 quadrangle, 4=P1 tetrahedron, 5=Q1 hexahedron, 6=01 prism, 7=01 pyramid,
  8=P2 segment, 9=P2 triangle, 10=Q2 quadrangle, 11=P2 tetrahedron, 12=Q2 hexahedron, 13=02 prism, 14=O2 pyramid
  15=vertex, 16=Q2Ser quadrangle, 17=Q2Ser hexahedron, 18=02Ser prism, 19=02Ser pyramid,
  20=P3inc triangle, 21=P3 triangle, 22=P4inc triangle, 23=P4 triangle, 24=P5inc triangle, 25=P5 triangle,
  26=P3 segment, 27=P4 segment, 28=P5 segment,
  29=P3 tetrahedron, 30=P4 tetrahedron, 31=P5 tetrahedron
  92=Q3 hexahedron, 93=Q4 hexahedron
*/

#include "../Mesh.hpp"

namespace xlifepp
{

//save mesh and related domains to files
void Mesh::saveToMsh(const string_t& filename, bool withDomains) const
{
  std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
  string_t froot = rootext.first;
  if(rootext.second!="msh") froot=filename;
  string_t f=froot+".msh";
  std::ofstream out(f.c_str());
  out.precision(fullPrec);
  mshExport(out);
  out.close();
  if (withDomains && meshDim() > 1) //save domains in files filename_domainname.msh
  {
    for (number_t d = 0; d < domains_.size(); d++) {
      string_t dn = domains_[d]->nameWithoutSharp();
      if (dn == "") { dn = "domain_" + tostring(d); }
      f = froot + "_" + dn + ".msh";
      out.open(f.c_str());
      out.precision(fullPrec);
      mshExport(*domains_[d], out);
      out.close();
    }
  }
}

void Mesh::mshExport(std::ostream& out) const
{
  // save msh header
  out << "$MeshFormat\n2.2 0 8\n$EndMeshFormat";

  number_t i=1, nbDoms=0;
  std::map<number_t, string_t> elemInfo;
  std::stringstream domOut;

  dimen_t maxdim=0, nmaxdim=0;
  for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
  {
      dimen_t d=(*it_dom)->dim();
      if(d== maxdim) {nmaxdim++;}
      else if (d>maxdim) {maxdim=d; nmaxdim=1;}
  }

  for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
  {
    if ((*it_dom)->domType()==_meshDomain && isDomainToBeExported(*(*it_dom)))
    {
      domOut << (*it_dom)->dim() << " " << i << " \"" << (*it_dom)->nameWithoutSharp() << "\"" << std::endl;
      nbDoms++;
      // to write elements info in an ordered way, we prepare each output line in elemInfo
      for (std::vector<GeomElement*>::iterator it_elem=(*it_dom)->meshDomain()->geomElements.begin(); it_elem!=(*it_dom)->meshDomain()->geomElements.end(); ++it_elem)
      {
        std::stringstream ss(std::stringstream::out);
        ss << (*it_elem)->number() << " ";
        // shape info and interpolation order are available trough parent element if it is not a MeshElement
        if((*it_elem)->hasMeshElement())
          { ss << mshType((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype); }
        else
        {
          std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
          ss << mshType(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype);
        }
        ss << " 2 " << i << " " << i;
        // node numbers are available through refElement for non MeshElement side
        if((*it_elem)->hasMeshElement())
          { for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); i++) { ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; } }
        else
        {
          std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
          if (it_gnp->first->hasMeshElement())
          {
            MeshElement* meshE=it_gnp->first->meshElement();
            for (number_t i=0; i<meshE->refElement()->sideDofNumbers_[it_gnp->second-1].size(); i++)
            {
              ss << " " << meshE->nodeNumbers[meshE->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1];
            }
          }
          else
          {
            // case of side of side
            warning("sos_elt_not_handled");
          }
        }

        // Management of this part is tricky
        // Actually, if a element has already been created, it is not updated (case of several domains recovering same elements)
        if (elemInfo.find((*it_elem)->number()-1) == elemInfo.end())
        {
          elemInfo[(*it_elem)->number()-1]=ss.str();
        }
      }
      ++i;
    }
  }

  // save physical names
  out << "\n$PhysicalNames" << std::endl << nbDoms << std::endl;
  out << domOut.str(); // domOut ends with a std::endl
  out << "$EndPhysicalNames" << std::endl;

  // nodes list
  out << "$Nodes" << std::endl << nbOfNodes() << std::endl;
  i=1;

  for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
  {
    out << i;
    for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
    for (number_t j=it_node[0].size(); j<3; ++j) { out << " 0"; }
    out << std::endl;
  }

  out << "$EndNodes" << std::endl;

  // elements list
  number_t nbelt=elemInfo.size();
  out <<"$Elements" << std::endl << nbelt << std::endl;
  for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) { out << it->second << std::endl; }

  out << "$EndElements" << std::endl;
}

void Mesh::mshExport(const GeomDomain& dom, std::ostream& out) const
{
  // save msh header
  out << "$MeshFormat\n2.2 0 8\n$EndMeshFormat";
  // save physical names
  out << "\n$PhysicalNames"<< std::endl << "1" << std::endl;
  std::map<number_t, string_t> elemInfo;

  if (dom.domType()==_meshDomain)
  {
    out << dom.dim() << " 1 \"" << dom.name() << "\"" << std::endl;
    // to write elements info in an ordered way, we prepare each output line in elemInfo
    for (std::vector<GeomElement*>::const_iterator it_elem=dom.meshDomain()->geomElements.begin(); it_elem!=dom.meshDomain()->geomElements.end(); ++it_elem)
    {
      std::stringstream ss(std::stringstream::out);
      ss << (*it_elem)->number() << " ";
      // shape info and interpolation order are available trough parent element if it is not a MeshElement
      if ((*it_elem)->hasMeshElement()) { ss << mshType((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype); }
      else
      {
        std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
        ss << mshType(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype);
      }
      ss << " 3 1 0 0";
      // node numbers are available through refElement for non MeshElement side
      if ((*it_elem)->hasMeshElement())
        { for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); ++i) { ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; } }
      else
      {
        std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
        for (number_t i=0; i<it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1].size(); ++i)
          { ss << " " << it_gnp->first->meshElement()->nodeNumbers[it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1]; }
      }
      elemInfo[(*it_elem)->number()-1]=ss.str();
    }
  }

  out << "$EndPhysicalNames" << std::endl;
  // nodes list
  out << "$Nodes" << std::endl << nbOfNodes() << std::endl;
  number_t i=1;

  for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
  {
    out << i;
    for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
    for (number_t j=it_node[0].size(); j<3; ++j) { out << " 0"; }
    out << std::endl;
  }

  out << "$EndNodes" << std::endl;

  // elements list
  out << "$Elements" << std::endl << elemInfo.size() << std::endl;
  for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) { out << it->second << std::endl; }

  out << "$EndElements" << std::endl;
}

void mshExport(const GeomDomain& dom, const std::vector<Point>& coords, const splitvec_t& elementsInfo, std::ostream& out)
{
  // save msh header
  out << "$MeshFormat\n2.2 0 8\n$EndMeshFormat";
  // save physical names
  out << "\n$PhysicalNames"<< std::endl << "1" << std::endl;
  std::vector<string_t> elemInfo(elementsInfo.size());

  if (dom.domType()==_meshDomain)
  {
    out << dom.dim() << " 1 \"" << dom.name() << "\"" << std::endl;

    number_t i=1;
    // to write elements info in an ordered way, we prepare each output line in elemInfo
    for (splitvec_t::const_iterator it_elem=elementsInfo.begin(); it_elem != elementsInfo.end(); ++it_elem, ++i)
    {
      std::stringstream ss(std::stringstream::out);
      ss << i << " " << mshType(it_elem->first,1) << " 3 1 0 0";
      for (number_t j=0; j < it_elem->second.size(); ++j) { ss << " " << it_elem->second[j]; }
      elemInfo[i-1]=ss.str();
    }
  }

  out << "$EndPhysicalNames" << std::endl;
  // nodes list
  out << "$Nodes" << std::endl << coords.size() << std::endl;
  number_t i=1;

  for (std::vector<Point>::const_iterator it_node=coords.begin(); it_node!=coords.end(); it_node++, i++)
  {
    out << i;
    for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
    for (number_t j=it_node[0].size(); j<3; ++j) { out << " 0"; }
    out << std::endl;
  }

  out << "$EndNodes" << std::endl;

  // elements list
  out << "$Elements" << std::endl << elemInfo.size() << std::endl;
  for (number_t i=0; i< elemInfo.size(); ++i) { out << elemInfo[i] << std::endl; }

  out << "$EndElements" << std::endl;
}

number_t mshType(ShapeType st, number_t order) {
  switch (st) {
    case _point: return 15;
    case _segment:
      switch (order) {
        case 1: return 1;
        case 2: return 8;
        case 3: return 26;
        case 4: return 27;
        case 5: return 28;
        default:
          error("msh_elem_type", st, order);
      }
    case _triangle:
      switch (order) {
        case 1: return 2;
        case 2: return 9;
        case 3: return 21;
        case 4: return 23;
        case 5: return 25;
        // P3inc -> 20
        // P4inc -> 22
        // P5inc -> 24
        default:
          error("msh_elem_type",st,order);
      }
    case _quadrangle:
      switch (order) {
        case 1: return 3;
        case 2: return 10;
        // Q2Ser -> 16
        default:
          error("msh_elem_type",st,order);
      }
    case _tetrahedron:
      switch (order) {
        case 1: return 4;
        case 2: return 11;
        case 3: return 29;
        case 4: return 30;
        case 5: return 31;
        default:
          error("msh_elem_type",st,order);
      }
    case _hexahedron:
      switch (order) {
        case 1: return 5;
        case 2: return 12;
        case 3: return 92;
        case 4: return 93;
        // Q2Ser -> 17
        default:
          error("msh_elem_type",st,order);
      }
    case _prism:
      switch (order) {
        case 1: return 6;
        case 2: return 13;
        // 02Ser -> 18
        default:
          error("msh_elem_type",st,order);
      }
    case _pyramid:
      switch (order) {
        case 1: return 7;
        case 2: return 14;
        // O2Ser -> 19
        default:
          error("msh_elem_type",st,order);
      }
    default:
      error("msh_elem_type",st,order);
  }
  return 0;
}

} // end of namespace xlifepp


// *****************************************************************************


// //save mesh and related domains to files
// void Mesh::saveToVizir(const string_t& filename, bool withDomains) const
// {
//   std::pair<string_t, string_t> rootext = fileRootExtension(trim(filename), Environment::authorizedSaveToFileExtensions());
//   string_t froot = rootext.first;
//   if(rootext.second!="msh") froot=filename;
//   string_t f=froot+".mesh"; 
//   std::ofstream out(f.c_str());
//   out.precision(fullPrec);
//   vizirExport(out,withDomains);
//   out.close();
//   if (withDomains && meshDim() > 1) //save domains in files filename_domainname.msh
//   {
//     for (number_t d = 0; d < domains_.size(); d++) {
//       string_t dn = domains_[d]->nameWithoutSharp();
//       if (dn == "") { dn = "domain_" + tostring(d); }
//       f = froot + "_" + dn + ".mesh";
//       out.open(f.c_str());
//       out.precision(fullPrec);
//   std::cout <<" export en vizir "<<domains_[d]->name()<<eol;
//       vizirExport(*domains_[d], out);
//       out.close();
//     }
//   }
// }

// void Mesh::vizirExport(std::ostream& out, bool withDomains) const
// {
//   // save vizir header
//   out << "MeshVersionFormatted 3"<<eol<<eol;

//   number_t i=1, nbDoms=0;
//   std::map<number_t, string_t> elemInfo;
//   std::map<number_t, string_t> elemSides;
//   std::map<number_t, string_t> elemSides2;
//   std::stringstream domOut;
//   string_t MeshElemNature="", Men0=MeshElemNature;
//   string_t  Men1;
//   string_t SideElemNature="", Sen0=SideElemNature;
//   string_t  Sen1;
//   dimen_t d;
//   number_t nbSe=0;

//   dimen_t maxdim=0, nmaxdim=0;
//   for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
//   {
//       d=(*it_dom)->dim();
//       if(d== maxdim) {nmaxdim++;}
//       else if (d>maxdim) {maxdim=d; nmaxdim=1;}
//   }

//   for (std::vector<GeomDomain *>::const_iterator it_dom=domains().begin(); it_dom!=domains().end(); ++it_dom)
//   {
//     d=(*it_dom)->dim();
//     if ((*it_dom)->domType()==_meshDomain && isDomainToBeExported(*(*it_dom)))
//     {
//       domOut << (*it_dom)->dim() << " " << i << " \"" << (*it_dom)->nameWithoutSharp() << "\"" << std::endl;
//       nbDoms++;
//       // to write elements info in an ordered way, we prepare each output line in elemInfo
//       for (std::vector<GeomElement*>::iterator it_elem=(*it_dom)->meshDomain()->geomElements.begin(); it_elem!=(*it_dom)->meshDomain()->geomElements.end(); ++it_elem)
//       {
//         //  ss << (*it_elem)->number() << " ";
//         // std::cout << "**"<<eol<< "Nombre de faces: " << (*it_elem)->meshElement()->numberOfSides()<<eol;
//         // std::cout << "**"<<eol<< "Numeros des faces: " << (*it_elem)->meshElement()->sideNumbers<<eol;
//         // std::cout << "**"<<eol<< "faces 0: " << (*it_elem)->meshElement()->sideNumbers[0]<<eol;
//         std::stringstream ss(std::stringstream::out);
//         std::stringstream se(std::stringstream::out);
//         std::stringstream sev(std::stringstream::out);
//         if (d>2)
//         { 
//           // se << (*it_elem)->meshElement()->sideNumbers;
//           for(number_t i=1;i<(*it_elem)->numberOfSides()+1;i++)
//           {
//             nbSe++;
//             for(number_t j=1;j<(*it_elem)->sideElement(i)->numberOfSides()+1;j++)
//             {
//               sev << (*it_elem)->sideElement(i)->vertexNumber(j)<<" "; 
//             }
//             sev << " " <<1 << eol;
//           }
//           Sen1=vizirNature(vizirType((*it_elem)->sideElement(1)->shapeType(),(*it_elem)->refElement()->interpolation().numtype));
//           if (Sen1 != Sen0 ) SideElemNature=SideElemNature+Sen1;
//           Sen0=Sen1;
//         }
//         // shape info and interpolation order are available trough parent element if it is not a MeshElement
//          if((*it_elem)->hasMeshElement())
//          {
//            // ss << vizirType((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype); 
//            Men1=vizirNature(vizirType((*it_elem)->meshElement()->shapeType(),(*it_elem)->refElement()->interpolation().numtype));
//          }
//         else
//         { 
//           std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//           // ss << vizirType(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype);
//           Men1=vizirNature(vizirType(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype));
//         }
//         if (Men1 != Men0 ) MeshElemNature=MeshElemNature+Men1;
//         Men0=Men1;
//         // ss << " 2 " << i << " " << i;
//         // node numbers are available through refElement for non MeshElement side

//         if((*it_elem)->hasMeshElement())
//         { 
//           for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); i++) 
//           {
//             //  ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; 
//             ss <<" "<< (*it_elem)->meshElement()->sideNumbers[i];
//           } 
//         }
//         else
//         {
//           std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//           if (it_gnp->first->hasMeshElement())
//           {
//             MeshElement* meshE=it_gnp->first->meshElement();
//             for (number_t i=0; i<meshE->refElement()->sideDofNumbers_[it_gnp->second-1].size(); i++)
//             {
//               ss << " " << meshE->nodeNumbers[meshE->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1];
//             }
//           }
//           else
//           {
//             // case of side of side
//             warning("sos_elt_not_handled");
//           }
//         }
//         ss << " " << i;

//         if (d>2)
//         {
//            elemSides[(*it_elem)->number()-1]=sev.str();
//         }
//         // Management of this part is tricky
//         // Actually, if a element has already been created, it is not updated (case of several domains recovering same elements)
//         if (elemInfo.find((*it_elem)->number()-1) == elemInfo.end())
//         {
//           elemInfo[(*it_elem)->number()-1]=ss.str();
//         }
//       }
//       ++i;
//     }
//   }

//   // save physical names
//   if (withDomains) 
//   {
//     out << "\n$PhysicalNames" << std::endl << nbDoms << std::endl;
//     out << domOut.str(); // domOut ends with a std::endl
//     out << "$EndPhysicalNames" << std::endl << std::endl;
//   }

//   out << eol <<"Dimension" << eol << meshDim()<<eol << eol;

//   // nodes list
//   out << "Vertices" << std::endl << nbOfNodes() << std::endl;
//   i=1;

//   for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
//   {
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     out << 1 << std::endl;
//   }

// // list of sides of element if dimMesh>2
//   if (d>2)
//   {
//     out << "" << std::endl;
//     out << SideElemNature << std::endl << nbSe << eol;
//     for (std::map<number_t,string_t>::const_iterator it=elemSides.begin(); it != elemSides.end(); ++it) 
//     {
//        out << it->second ;
//     }
//   }
  
//   out << "" << std::endl;

//   // elements list
//   number_t nbelt=elemInfo.size();
//   out <<MeshElemNature << std::endl << nbelt << std::endl;
  
//   for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) 
//   {
//      out << it->second << std::endl;
//   }

//   out << std::endl << "End" << std::endl;
// }

// void Mesh::vizirExport(const GeomDomain& dom, std::ostream& out) const
// {
//   // save vizir header
//   out << "MeshVersionFormatted 3"<<eol<<eol;
//   std::map<number_t, string_t> elemInfo;
  
//   string_t MeshElemNature;

//   if (dom.domType()==_meshDomain)
//   {
//         // if (d>2)
//         // { 
//         //   // se << (*it_elem)->meshElement()->sideNumbers;
//         //   for(number_t i=1;i<(*it_elem)->numberOfSides()+1;i++)
//         //   {
//         //     nbSe++;
//         //     for(number_t j=1;j<(*it_elem)->sideElement(i)->numberOfSides()+1;j++)
//         //     {
//         //       sev << (*it_elem)->sideElement(i)->vertexNumber(j)<<" "; 
//         //     }
//         //     sev << " " <<eol;
//         //   }
//         //   Sen1=vizirNature(vizirType((*it_elem)->sideElement(1)->shapeType(),(*it_elem)->refElement()->interpolation().numtype));
//         //   if (Sen1 != Sen0 ) SideElemNature=SideElemNature+Sen1;
//         //   Sen0=Sen1;
//         // }
//     std::vector<GeomElement*>::const_iterator it_elem0=dom.meshDomain()->geomElements.begin();
//     if ((*it_elem0)->hasMeshElement())
//       {
//         MeshElemNature=vizirNature(vizirType((*it_elem0)->meshElement()->shapeType(),(*it_elem0)->refElement()->interpolation().numtype));
//       }
//       else
//       { 
//         std::vector<GeoNumPair>::iterator it_gnp=(*it_elem0)->parentSides().begin();
//         MeshElemNature=vizirNature(vizirType(it_gnp->first->shapeType(it_gnp->second),it_gnp->first->refElement()->interpolation().numtype));
//       }

//     // to write elements info in an ordered way, we prepare each output line in elemInfo
//     for (std::vector<GeomElement*>::const_iterator it_elem=dom.meshDomain()->geomElements.begin(); it_elem!=dom.meshDomain()->geomElements.end(); ++it_elem)
//     {
//       std::stringstream ss(std::stringstream::out);
//       // node numbers are available through refElement for non MeshElement side
//       if ((*it_elem)->hasMeshElement())
//         { for (number_t i=0; i<(*it_elem)->meshElement()->numberOfNodes(); ++i) { ss << " " << (*it_elem)->meshElement()->nodeNumbers[i]; } }
//       else
//       {
//         std::vector<GeoNumPair>::iterator it_gnp=(*it_elem)->parentSides().begin();
//         for (number_t i=0; i<it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1].size(); ++i)
//           { ss << " " << it_gnp->first->meshElement()->nodeNumbers[it_gnp->first->meshElement()->refElement()->sideDofNumbers_[it_gnp->second-1][i]-1]; }
//       }
//       elemInfo[(*it_elem)->number()-1]=ss.str();
//     }
//   }
//   out << "Dimension" << eol << meshDim()<<eol << eol;

//   // nodes list
//   out << "Vertices" << std::endl << nbOfNodes() << std::endl;
//   number_t i=1;

//   for (std::vector<Point>::const_iterator it_node=nodes.begin(); it_node!=nodes.end(); it_node++, i++)
//   {
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     out << " " << 1 << std::endl;
//   }

//   out << "" << std::endl;

//   // elements list
//   out << MeshElemNature << std::endl << elemInfo.size() << std::endl;
//   for (std::map<number_t,string_t>::const_iterator it=elemInfo.begin(); it != elemInfo.end(); ++it) 
//   {
//      out << it->second << " "<< it->first +1<< std::endl;
//   }

//   out <<std::endl << "End" << std::endl;
// }

// void vizirExport(const GeomDomain& dom, const std::vector<Point>& coords, const splitvec_t& elementsInfo, std::ostream& out)
// {
//   // save vizir header
//   out << "MeshVersionFormatted 3"<<eol<<eol;
//   std::vector<string_t> elemInfo(elementsInfo.size());
//   string_t MeshElemNature;

//   if (dom.domType()==_meshDomain)
//   {
//     number_t i=1;
//     // to write elements info in an ordered way, we prepare each output line in elemInfo
//     for (splitvec_t::const_iterator it_elem=elementsInfo.begin(); it_elem != elementsInfo.end(); ++it_elem, ++i)
//     {
//          MeshElemNature=vizirNature(vizirType(it_elem->first,1));
//     }
//   }

//   out << "\n Dimension" << eol << coords.size()<<eol << eol;

//   // nodes list
//   out << "Vertices" << std::endl << coords.size() << std::endl;
//   number_t i=1;

//   for (std::vector<Point>::const_iterator it_node=coords.begin(); it_node!=coords.end(); it_node++, i++)
//   {
//     out << i;
//     for (number_t j=0; j<it_node[0].size(); ++j) { out << " " << (*it_node)[j]; }
//     out << " " << 1 << std::endl;
//   }
//   // elements list
//   out << MeshElemNature << std::endl << elemInfo.size() << std::endl;
//   for (number_t i=0; i< elemInfo.size(); ++i) { out << elemInfo[i] << " "<< i+1 <<std::endl; }

//   out << std::endl << "End" << std::endl;
// }

// number_t vizirType(ShapeType st, number_t order) {
//   switch (st) {
//     case _point: return 15;
//     case _segment:
//       switch (order) {
//         case 1: return 1;
//         case 2: return 8;
//         case 3: return 26;
//         case 4: return 27;
//         case 5: return 28;
//         default:
//           error("vizir_elem_type", st, order);
//       }
//     case _triangle:
//       switch (order) {
//         case 1: return 2;
//         case 2: return 9;
//         case 3: return 21;
//         case 4: return 23;
//         case 5: return 25;
//         // P3inc -> 20
//         // P4inc -> 22
//         // P5inc -> 24
//         default:
//           error("vizir_elem_type",st,order);
//       }
//     case _quadrangle:
//       switch (order) {
//         case 1: return 3;
//         case 2: return 10;
//         // Q2Ser -> 16
//         default:
//           error("vizir_elem_type",st,order);
//       }
//     case _tetrahedron:
//       switch (order) {
//         case 1: return 4;
//         case 2: return 11;
//         case 3: return 29;
//         case 4: return 30;
//         case 5: return 31;
//         default:
//           error("vizir_elem_type",st,order);
//       }
//     case _hexahedron:
//       switch (order) {
//         case 1: return 5;
//         case 2: return 12;
//         case 3: return 92;
//         case 4: return 93;
//         // Q2Ser -> 17
//         default:
//           error("vizir_elem_type",st,order);
//       }
//     case _prism:
//       switch (order) {
//         case 1: return 6;
//         case 2: return 13;
//         // 02Ser -> 18
//         default:
//           error("vizir_elem_type",st,order);
//       }
//     case _pyramid:
//       switch (order) {
//         case 1: return 7;
//         case 2: return 14;
//         // O2Ser -> 19
//         default:
//           error("vizir_elem_type",st,order);
//       }
//     default:
//       error("vizir_elem_type",st,order);
//   }
//   return 0;
// }


// string_t vizirNature( number_t st) {
//   switch (st) {
//     case 1: return "Edges";
//     case 2: return "Triangles";
//     case 3: return "Quadrilaterals";
//     case 4: return "Tetrahedra";
//     case 5: return "Hexahedra";
//     case 6: return "Prisms";
//     case 7: return "Pyramid";
//     case 8: return "EdgesP2";
//     case 9: return "TrianglesP2";
//     case 10: return "QuadrilateralsQ2";
//     case 11: return "TetrahedraP2";
//     case 12: return "HexahedraQ2";
//     case 13: return "PrismsP2";
//     case 14: return "PyramidP2";
//     case 15: return "Point";
//     case 21: return "TrianglesP3";
//     case 23: return "TrianglesP4";
//     case 25: return "TrianglesP5";
//     case 26: return "EdgesP3";
//     case 27: return "EdgesP4";
//     case 28: return "EdgesP5";
//     case 29: return "TetrahedraP3";
//     case 30: return "TetrahedraP4";
//     case 31: return "TetrahedraP5";
//     case 92: return "HexahedraP3";
//     case 93: return "HexahedraP4";
//     default:
//       error("vizir_elem_type",st,99999);
//   }
//   return 0;
// }

// } // end of namespace xlifepp
