/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file loadGmsh.cpp
  \authors N. Kielbasiewicz, Y. Lafranche
  \since 20 mar 2013
  \date 12 dec 2013

  \brief Read a file containing a mesh in Gmsh format
*/

#include "ioutils.hpp"
#include <cstdio>

namespace xlifepp {
using std::ifstream;
using std::make_pair;
using std::map;
using std::multimap;
using std::ostringstream;
using std::pair;
using std::set;
using std::vector;

//----------------------------------------------------------------------------------------
// Correspondence between Gmsh and XLiFE++ element types:
static number_t Idty[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13}; // Identity
static number_t Qua3[] = {1,2,3,0,6,8,10,4,7,9,11,5,13,14,15,12}; // Gmsh code 36
static number_t Qua4[] = {1,2,3,0,7,10,13,4,8,11,14,5,9,12,15,6,17,18,19,16,21,22,23,20,24}; // Gmsh code 37
static number_t Qua5[] = {1,2,3,0,8,12,16,4,9,13,17,5,10,14,
                          18,6,11,15,19,7,21,22,23,20,26,28,30,24,27,29,31,25,33,34,35,32}; // Gmsh code 38
static number_t Tri3[] = {0,1,2,3,5,7,4,6,8,9};
static number_t Tri4[] = {0,1,2,3,6,9,4,7,10,5,8,11,12,13,14};
static number_t Tri5[] = {0,1,2,3,7,11,4,8,12,5,9,13,6,10,14,15,16,17,18,19,20};
static number_t Tet1[] = {3,1,2,0};
static number_t Tet2[] = {3,1,2,0,7,4,6,5,8,9};
static number_t Tet3[] = {3,1,2,0,10,5,8,6,12,14,11,4,9,7,13,15,16,18,17,19};
static number_t Tet4[] = {3,1,2,0,13,6,10,7,16,19,14,5,11,8,17,20,15,4,12,9,
                          18,21,22,23,24,28,29,30,25,26,27,31,32,33,34};
static number_t Tet5[] = {3,1,2,0,16,7,12,8,20,24,17,6,13,9,21,25,18,5,14,10,22,26,19,4,15,11,23,27,28,29,
                          30,31,32,33,40,41,42,43,44,45,34,35,36,37,38,39,46,47,48,49,50,51,55,53,54,52};
static number_t Hex2[] = {1,2,3,0,5,6,7,4,14,19,18,10,8,9,12,13,17,15,16,11,23,24,25,22,21,20,26};
//static number_t Hex2[] = {0,1,2,3,4,5,6,7,12,18,16,15,9,13,10,11,19,14,17,8,21,23,25,24,22,20,26};
//         Second Hex2 equivalent to previous one after a rotation of pi/2 around Oz
static number_t Hex3[] = {1,2,3,0,5,6,7,4,20,31,28,13,9,11,16,18,27,22,25,14,21,30,29,12,8,10,17,
                          19,26,23,24,15,45,46,47,44,50,51,48,49,53,54,55,52,43,40,41,42,39,36,37,
                          38,35,32,33,34,57,58,59,56,61,62,63,60};
static number_t Hex4[] = {1,2,3,0,5,6,7,4,26,43,38,16,10,13,20,23,37,29,34,17,27,42,39,15,9,12,21,24,36,
                          30,33,18,28,41,40,14,8,11,22,25,35,31,32,19,72,73,74,71,76,77,78,75,79,82,83,
                          80,81,86,87,84,85,88,90,91,92,89,94,95,96,93,97,65,62,63,64,69,66,67,68,70,56,
                          53,54,55,60,57,58,59,61,47,44,45,46,51,48,49,50,52,99,100,101,98,103,104,105,
                          102,112,117,116,108,106,107,110,111,115,113,114,109,121,122,123,120,119,118,124};
static number_t Pri2[] = {0,1,2,3,4,5,6,9,7,12,14,13,8,10,11,15,17,16};
struct GMXL {
  ShapeType shpt;
  InterpolationType polt;
  number_t *perm_p;  // permutation to be applied to the list of points defining the elements
                     // read in the mesh to cope with XLiFE++ own numbering order of points
                     // If i is the index in XLiFE++'s numbering, perm_p[i] is the corresponding
                     // index in Gmsh.
  GMXL(){}
  GMXL(ShapeType sh, InterpolationType po, number_t *pe_p)
   : shpt(sh), polt(po), perm_p(pe_p) {}
};
typedef map <number_t, GMXL > GMSHMAP;
void initGmshMap(GMSHMAP& gmMap) {
  // The numbering conventions are equivalent in Gmsh and XLiFE++ when they match
  // after a rotation of the element. In this case, the permutation is the identity.
  gmMap[ 1] = GMXL(_segment,     P1, Idty); //  2-node line
  gmMap[ 2] = GMXL(_triangle,    P1, Idty); //  3-node triangle
  gmMap[ 3] = GMXL(_quadrangle,  P1, Idty); //  4-node quadrangle
  gmMap[ 4] = GMXL(_tetrahedron, P1, Tet1); //  4-node tetrahedron
  gmMap[ 5] = GMXL(_hexahedron,  P1, Idty); //  8-node hexahedron
  gmMap[ 6] = GMXL(_prism,       P1, Idty); //  6-node prism
  gmMap[ 7] = GMXL(_pyramid,     P1, Idty); //  5-node pyramid
  gmMap[ 8] = GMXL(_segment,     P2, Idty); //  3-node 2nd order line (2 vertices and 1 edge)
  gmMap[ 9] = GMXL(_triangle,    P2, Idty); //  6-node 2nd order triangle (3 vertices and 3 edges)
  gmMap[10] = GMXL(_quadrangle,  P2, Idty); //  9-node 2nd order quadrangle (4 vertices, 4 edges and 1 face)
  gmMap[11] = GMXL(_tetrahedron, P2, Tet2); // 10-node 2nd order tetrahedron (4 vertices and 6 edges)
  gmMap[12] = GMXL(_hexahedron,  P2, Hex2); // 27-node 2nd order hexahedron (8 vertices, 12 edges, 6 faces and 1 volume)
  gmMap[13] = GMXL(_prism,       P2, Pri2); // 18-node 2nd order prism (6 vertices, 9 edges and 3 quadrangular faces)
  gmMap[14] = GMXL(_pyramid,     P2, Idty); // 14-node 2nd order pyramid (5 vertices, 8 edges and 1 quadrangular face)
  gmMap[15] = GMXL(_point,       P0, Idty); //  1-node point
  // 16-20 : no correspondence in XLiFE++
  gmMap[21] = GMXL(_triangle,    P3, Tri3); // 10-node 3rd order triangle (3 vertices, 6 edges, 1 face)
  // 22 : no correspondence in XLiFE++
  gmMap[23] = GMXL(_triangle,    P4, Tri4); // 15-node 4th order triangle (3 vertices, 9 edges, 3 face)
  // 24 : no correspondence in XLiFE++
  gmMap[25] = GMXL(_triangle,    P5, Tri5); // 21-node 5th order  triangle (3 vertices, 12 edges, 6 face)
  gmMap[26] = GMXL(_segment,     P3, Idty); //  4-node 3rd order edge (2 vertices, 2 internal to the edge)
  gmMap[27] = GMXL(_segment,     P4, Idty); //  5-node 4th order edge (2 vertices, 3 internal to the edge)
  gmMap[28] = GMXL(_segment,     P5, Idty); //  6-node 5th order edge (2 vertices, 4 internal to the edge)
  gmMap[29] = GMXL(_tetrahedron, P3, Tet3); // 20-node 3rd order tetrahedron (4 vertices, 12 edges, 4 faces)
  gmMap[30] = GMXL(_tetrahedron, P4, Tet4); // 35-node 4th order tetrahedron (4 vertices, 18 edges, 12 faces, 1 in the volume)
  gmMap[31] = GMXL(_tetrahedron, P5, Tet5); // 56-node 5th order tetrahedron (4 vertices, 24 edges, 24 faces, 4 in the volume)
  //
  gmMap[36] = GMXL(_quadrangle,  P3, Qua3); //  16-node 3rd order quadrangle (4 vertices, 8 edges and 4 face)
  gmMap[37] = GMXL(_quadrangle,  P4, Qua4); //  25-node 4th order quadrangle (4 vertices, 12 edges and 9 face)
  gmMap[38] = GMXL(_quadrangle,  P5, Qua5); //  36-node 5th order quadrangle (4 vertices, 16 edges and 16 face)
  //
  gmMap[92] = GMXL(_hexahedron,  P3, Hex3); //  64-node 3rd order hexahedron (8 vertices, 24 edges, 24 faces, 8 in the volume)
  gmMap[93] = GMXL(_hexahedron,  P4, Hex4); // 125-node 4th order hexahedron (8 vertices, 36 edges, 54 faces, 27 in the volume)
}

//----------------------------------------------------------------------------------------
/*!
  \struct GELT
  Gmsh element
*/
struct GELT {
  number_t elty,      //!< element type number in Gmsh
           ndom,      //!< number of the physical entity
           ndom2,     //!< number of the elementary geometrical entity (only used for cracked domains)
           readOrder; //!< only used to make the sort function give the same result on different systems
  vector<number_t> nums,  //!< node numbers defining the element
                   parts; //!< partition numbers related to the element (the first one is the partition containing the element)
  GeomElement* ge_p; //!< address of the corresponding GeomElement
  bool plain; //!< true if this GeomElement is created as a plain element, false if it is a side element

  GELT():ge_p(nullptr), plain(true) {} //!< plain element by default
  void side() { plain = false; }
};

/*!
  Comparison function of two Gmsh elements according to type and domain numbers:
  a < b if its type number is smaller, then if its domain number is smaller.
*/
bool compareGELTs(const GELT &a, const GELT &b) {
  if (a.elty < b.elty) { return true; }
  else {
    if (a.elty == b.elty) {
      if (a.ndom < b.ndom) { return true; }
      else {
        if (a.ndom == b.ndom) {
          if  (a.ndom2 < b.ndom2) { return true; }
          else {
            if (a.ndom2 == b.ndom2) {
              if (a.readOrder < b.readOrder) { return true; }
              else { return false; }
            }
            else { return false; }
          }
        }
        else { return false; }
      }
    }
    else { return false; }
  }
}
//----------------------------------------------------------------------------------------
// Get reference element in XLiFE++ tables corresponding to Gmsh element type elmType.
// Get associated data such as the number of points of the element, its dimension and
// whether it is a simplex or not.
RefElement* getRefElt(number_t elmType, const GMSHMAP &gmMap, number_t *nb_pts,
               number_t *elmDim, bool *isSimplex) {
  if (gmMap.count(elmType) > 0) {
    if (elmType != 15) { // All elements except point
      // Find or create reference element
      Interpolation* interp_p = findInterpolation(Lagrange, standard, gmMap.at(elmType).polt, H1);
      ShapeType eltShape = gmMap.at(elmType).shpt;
      RefElement* re_p = findRefElement(eltShape, interp_p);
      *isSimplex = re_p->isSimplex();

      // Number of points of reference element
      *nb_pts = re_p->nbPts();
      // Dimension of elements of mesh (can be less than space dimension)
      *elmDim = re_p->geomRefElem_p->dim();
      // N      // compute shape functions at centroid (used later to compute elements orientation)
      // N      re_p->computeShapeValues(re_p->geomRefElem_p->centroid());
      return re_p;
    }
    else { // Case of a point
      // a point is a 0-simplex
      *isSimplex = true;
      *nb_pts = 1;
      *elmDim = 0;
      return nullptr;
    }
  }
  else {
    error("gmsh_elt_not_managed",elmType);
    return nullptr;
  }
}

//----------------------------------------------------------------------------------------
// Returns the name of the domain ndom: it is the name found in the data file if any,
// or it is a generated one whose form is "Omega"ndom.
string_t genDomName(number_t ndom, const map <number_t, string_t>& domNameMap) {
  if (domNameMap.count(ndom) > 0) {
    return domNameMap.at(ndom);
  }
  else {
    ostringstream domnum; domnum << ndom;
    return "Omega"+domnum.str();
  }
}

//----------------------------------------------------------------------------------------
// Returns the name of the sub-domain nsdom: it is the name found in the data file if any,
// or it is a generated one whose form is "dOmega"ndom_nsdom.
string_t genSDomName(number_t ndom, number_t nsdom, const map <number_t, string_t>& domNameMap) {
  if (domNameMap.count(nsdom) > 0) {
    return domNameMap.at(nsdom);
  }
  else {
    ostringstream domnum; domnum << nsdom;
    string_t dDN("d"+genDomName(ndom,domNameMap)+"_");
    return dDN+domnum.str();
  }
}

//----------------------------------------------------------------------------------------
// Read the $PhysicalNames section of a .msh file (associated to input stream data).
// Below, physical-tag corresponds to the tag of a physical entity, to which a name
// is given (physical-name).
template<class T_>
void readPhysNames(T_& data, map<number_t, string_t>& domNameMap) {
  number_t nb_physn_sctn; // number of physical names in this section
  readInt(data, nb_physn_sctn); // data >> nb_physn_sctn;
  for (number_t physn=0; physn<nb_physn_sctn; physn++)
  {
    number_t intVal;
    string_t domn;
    // physical-dimension physical-tag "physical-name"
    readInt(data, intVal); readInt(data, intVal); readStr(data, domn); // data >> intVal >> intVal >> domn;
    removeChar(domn, '"');    //remove " in gmsh name
    if (domn.find("#") == 0) { error("domain_name_invalid"); }
    if (domn.find(" ") == 0) { error("domain_name_invalid"); }
    domNameMap[intVal] = domn;
  }
}

typedef map <number_t, ELTDEF > ELTDEFMAP;
const dimen_t R3Dim = 3; // Dimension of 3D space (used in following functions)
//----------------------------------------------------------------------------------------
// Read Gmsh file that conforms to format version number 2.2.
// Sections taken into account are $Nodes, $Elements and $PhysicalNames ; all others are
// discarded. The mandatory $MeshFormat section has been read previously.
// This function accepts several sections of the same type in any order, although this
// format requires that there is only one $Nodes section followed by one $Elements section.
template<class T_>
void readfmt2(T_& data, const string_t& filename, const vector<CrackData>& crackData,
              number_t& nb_geom_Pts, vector<real_t>& PointCoords, vector<number_t>& nodeNum,
              bool& planeDomain, bool& curveDomain, number_t& nb_elts, vector<GELT>& gelts,
              ELTDEFMAP& elMap, dimen_t& elementDimx, const GMSHMAP& gmMap,
              set<number_t>& domSet, map<number_t, set<number_t> >& crackedDomSet,
              map<number_t, number_t >& it_inCrackData, map<number_t, string_t>& domNameMap,
              bool& isMadeOfSimplices_) {
  nb_geom_Pts = 0;
  planeDomain = curveDomain = true;
  nb_elts = 0;
  elementDimx = 0;
  isMadeOfSimplices_ = true;

  number_t gpt_n = nb_geom_Pts;
  number_t gel_n = nb_elts;
  number_t intVal;
  string_t strVal;
  while (readStr(data, strVal)) { //while (data >> strVal) {// read delimiters of sections.
    // Do something only for $Nodes, $Elements and $PhysicalNames sections ; everything else is discarded.
    if (strVal == "$Nodes") { // Read $Nodes section
      number_t nb_pts_sctn; // number of points in this section
      readInt(data, nb_pts_sctn); // data >> nb_pts_sctn;
      number_t prevnb_geom_Pts = nb_geom_Pts;
      nb_geom_Pts += nb_pts_sctn;
      PointCoords.resize(R3Dim*nb_geom_Pts);
      nodeNum.resize(nb_geom_Pts);
      real_t* pt_coords_p = &PointCoords[R3Dim*prevnb_geom_Pts];// location may have changed
                                                                // (in case there were several $Nodes sections)
      for (number_t pt_n=0; pt_n<nb_pts_sctn; pt_n++, pt_coords_p += R3Dim) {
        readInt(data, nodeNum[gpt_n++]); //data >> nodeNum[gpt_n++];
        for (int i=0; i<R3Dim; i++) { readRea(data, pt_coords_p[i]); } // { data >> pt_coords_p[i]; }
        // if third coordinate non zero, this is a 3D mesh
        if (pt_coords_p[2] != 0) {
          planeDomain = curveDomain = false;
        }
        else {
          // third coordinate is zero
          // if second coordinate is non zero, this is a 2D mesh
          if (pt_coords_p[1] != 0) { curveDomain = false; }
        }
      }
      // $EndNodes (unused)
    }
    else if (strVal == "$Elements") { // Read $Elements section
      number_t nb_pts=0;
      number_t defndom=0;     // initialized to a default domain number
      number_t prevelmType=0; // initialized to a non existing element type
      number_t nb_elts_sctn;  // number of elements in this section
      readInt(data, nb_elts_sctn); // data >> nb_elts_sctn;
      nb_elts += nb_elts_sctn;
      gelts.resize(nb_elts);
      // Read following data whose format is:
      // elm-number elm-type number-of-tags < tag > ... node-number-list
      for (number_t el_n=0; el_n<nb_elts_sctn; el_n++) {
        gelts[gel_n].readOrder=gel_n;
        readInt(data, intVal);  // data >> intVal; // elm-number (unused, use gel_n instead)
        number_t elmType;
        readInt(data, elmType); // data >> elmType;
        gelts[gel_n].elty = elmType; // elm-type
        if (elmType != prevelmType) {
          number_t elmDim=0;
          bool isSimplex;
          RefElement* re_p = getRefElt(elmType, gmMap, &nb_pts, &elmDim, &isSimplex);
          if (elementDimx < elmDim) { elementDimx = elmDim; }
          if (! isSimplex) { isMadeOfSimplices_ = false; }
          prevelmType = elmType;
          // Store data related to this element type for future use:
          elMap[elmType] = ELTDEF(nb_pts, elmDim, re_p);
        }
        number_t nb_tag;
        readInt(data, nb_tag); // data >> nb_tag; // by construction, cannot be greater than 3
        if (nb_tag > 0) {
          // First tag: number of the physical entity to which the element belongs
          //             (one of the physical-tags in the $PhysicalNames section if this section is present)
          readInt(data, gelts[gel_n].ndom); // data >> gelts[gel_n].ndom;
        }
        else {
          // No tag at all: set an arbitrary domain number (the same for all elements for which this data is missing)
          // This situation should never occur according to Gmsh 2.10 documentation.
          gelts[gel_n].ndom = defndom;
        }
        number_t curndom = gelts[gel_n].ndom;
        domSet.insert(curndom); // store the number of the physical entity (domain number). Each one is unique.
        if (nb_tag > 1) {
          // Second tag if any: number of the elementary geometrical entity to which
          // the element belongs (used only if it is a cracked domain)
          readInt(data, gelts[gel_n].ndom2); // data >> gelts[gel_n].ndom2;
        }
        short int i_cd = findId(crackData.begin(), crackData.end(), curndom);
        if (i_cd > -1) {
          crackedDomSet[curndom].insert(gelts[gel_n].ndom2); // store the number of the geometrical entity. Each one is unique.
          it_inCrackData[curndom]=i_cd;
        }
        if (nb_tag > 2) { // there are mesh partitionning informations:
          number_t nb_partitions;
          readInt(data, nb_partitions); // data >> nb_partitions; // third tag: number of mesh partitions to which the element belongs
          /*
            Read the partition ids
            The first one is the number of the partition containing the element (it is positive)
            The following ones are the numbers of partitions adjacent to the element (they are negatives but stored positives)
          */
          vector<number_t>& gnum_part = gelts[gel_n].parts;
          readInt(data, intVal); // data >> intVal;
          gnum_part.push_back(intVal);
          for (number_t i=1; i<nb_partitions; i++) {
            readInt(data, intVal); // data >> intVal;
            gnum_part.push_back(-intVal);
          }
        }
        // Now, read the list of the nb_pts node numbers of the current element
        vector<number_t>& gnum_el = gelts[gel_n].nums;
        gnum_el.resize(nb_pts);
        for (number_t i=0; i<nb_pts; i++) { readInt(data, gnum_el[i]); } // { data >> gnum_el[i]; }
        gel_n++;
      }
      // $EndElements (unused)
    }

    else if (strVal == "$PhysicalNames") {
      readPhysNames(data, domNameMap);
      // $EndPhysicalNames (unused)
    }
    else { /* strVal is an end of section delimiter or unused data */}
  } // end of while (data >> strVal)

  if (gpt_n != nb_geom_Pts) { error("gmsh_wrong_nbnodes",gpt_n,filename,nb_geom_Pts); }
  if (gel_n != nb_elts)     { error("gmsh_wrong_nbelts",gel_n,filename,nb_elts); }
}

typedef map<number_t, set<number_t> > Mn_Sn;
typedef map<number_t, pair<number_t, number_t> > Mn_Pnn;
//----------------------------------------------------------------------------------------
template<class T_>
void readEntities(T_& data, size_t numEnt, Mn_Sn& phystag) {
  number_t intVal;
  for (size_t nb=0; nb<numEnt; nb++) {
    number_t entityTag; readInt(data, entityTag); // data >> entityTag;

    real_t xyz; // unused here (read minX minY minZ maxX maxY maxZ)
    for (int i=0; i<2*R3Dim; i++) { readRea(data, xyz); } // { data >> xyz; }

    size_t numPhysicalTags;  readInt(data, numPhysicalTags); // data >> numPhysicalTags;
    // Read the physicalTags
    set<number_t> sphtag;
    for (number_t i=0; i<numPhysicalTags; i++) {
      readInt(data, intVal); // data >> intVal;
      sphtag.insert(intVal);
    }
    phystag[entityTag] = sphtag;

    size_t numBoundingEnts; // unused here
    readInt(data, numBoundingEnts); // data >> numBoundingEnts;
    for (number_t i=0; i<numBoundingEnts; i++) { readInt(data, intVal); } // { data >> intVal; }
  }
}
//----------------------------------------------------------------------------------------
template<class T_>
void readPartitionedEntities(T_& data, size_t numEnt, Mn_Pnn& parentDT,
                             Mn_Sn& partparttag, Mn_Sn& partphystag) {
  number_t intVal;
  for (size_t nb=0; nb<numEnt; nb++) {
    number_t entityTag; readInt(data, entityTag); // data >> entityTag;
    number_t parentDim; readInt(data, parentDim); // data >> parentDim;
    number_t parentTag; readInt(data, parentTag); // data >> parentTag;
    parentDT[entityTag] = make_pair(parentDim, parentTag);

    size_t numPartitions;  readInt(data, numPartitions); // data >> numPartitions;
    // Read the partitionTags
    set<number_t> spartag;
    for (number_t i=0; i<numPartitions; i++) {
      readInt(data, intVal); // data >> intVal;
      spartag.insert(intVal);
    }
    partparttag[entityTag] = spartag;

    real_t xyz; // unused here (read minX minY minZ maxX maxY maxZ)
    for (int i=0; i<2*R3Dim; i++) { readRea(data, xyz); } // { data >> xyz; }

    size_t numPhysicalTags;  readInt(data, numPhysicalTags); // data >> numPhysicalTags;
    // Read the physicalTags
    set<number_t> sphtag;
    for (number_t i=0; i<numPhysicalTags; i++) {
      readInt(data, intVal); // data >> intVal;
      sphtag.insert(intVal);
    }
    partphystag[entityTag] = sphtag;

    size_t numBoundingEnts; // unused here
    readInt(data, numBoundingEnts); // data >> numBoundingEnts;
    for (number_t i=0; i<numBoundingEnts; i++) { readInt(data, intVal); } // { data >> intVal; }
  }
}
//----------------------------------------------------------------------------------------
// Read Gmsh file that conforms to format version number 4.1.
// Sections taken into account are $Entities, $PartitionedEntities, $Nodes, $Elements and
// $PhysicalNames ; all others are discarded. The mandatory $MeshFormat section has been
// read previously.
// At least one $Nodes section, followed by one $Elements section is required. If present,
// an $Entities section should appear before the $Nodes section. Other sections may appear
// in any order.
template<class T_>
void readfmt4(T_& data, const string_t& filename, const vector<CrackData>& crackData,
              number_t& nb_geom_Pts, vector<real_t>& PointCoords, vector<number_t>& nodeNum,
              bool& planeDomain, bool& curveDomain, number_t& nb_elts, vector<GELT>& gelts,
              ELTDEFMAP& elMap, dimen_t& elementDimx, const GMSHMAP& gmMap,
              set<number_t>& domSet, map<number_t, set<number_t> >& crackedDomSet,
              map<number_t, number_t >& it_inCrackData, map<number_t, string_t>& domNameMap,
              bool& isMadeOfSimplices_) {
  nb_geom_Pts = 0;
  planeDomain = curveDomain = true;
  nb_elts = 0;
  elementDimx = 0;
  isMadeOfSimplices_ = true;

  number_t gpt_n = nb_geom_Pts;
  number_t gel_n = nb_elts;
  number_t intVal;
  string_t strVal;
  Mn_Sn phystag; // Filled by $Entities section if any, used later during $Elements section processing

  while (readStr(data, strVal)) { //while (data >> strVal) {// read delimiters of sections.
    // Do something only for $Entities, $PartitionedEntities, $Nodes, $Elements and $PhysicalNames sections ;
    // everything else is discarded.

    if (strVal == "$Entities") {
      size_t numPoints,  numCurves, numSurfaces, numVolumes;
      readInt(data, numPoints);   readInt(data, numCurves);  // data >> numPoints >> numCurves;
      readInt(data, numSurfaces); readInt(data, numVolumes); // data >> numSurfaces >> numVolumes;

      for (size_t nb=0; nb<numPoints; nb++) {
        number_t pointTag; readInt(data, pointTag); // data >> pointTag;

        real_t xyz; // unused here
        for (int i=0; i<R3Dim; i++) { readRea(data, xyz); } // { data >> xyz; }

        size_t numPhysicalTags;  readInt(data, numPhysicalTags); // data >> numPhysicalTags;
        // Read the physicalTags
        set<number_t> sphtag;
        for (number_t i=0; i<numPhysicalTags; i++) {
          readInt(data, intVal); // data >> intVal;
          sphtag.insert(intVal);
        }
        phystag[pointTag] = sphtag;
      }
      readEntities(data, numCurves,   phystag);
      readEntities(data, numSurfaces, phystag);
      readEntities(data, numVolumes,  phystag);
      // $EndEntities (unused)
    }
    else if (strVal == "$PartitionedEntities") {   // Not used nor tested on May 2019
    /* Link with "mesh partitionning informations" in Gmsh format version 2.2 ?
       (which is unused anyway)
    */
      // containers to store relevant information (intended to be used some day)
      Mn_Pnn parentDT;
      Mn_Sn partparttag, partphystag;
      vector<pair<number_t, number_t> > ghostpart; // couples (ghostEntityTag, partition)

      size_t numPartitions, numGhostEntities;
      readInt(data, numPartitions);   readInt(data, numGhostEntities);  // data >> numPartitions >> numGhostEntities;
      for (size_t nb=0; nb<numGhostEntities; nb++) {
        number_t ghostEntityTag; readInt(data, ghostEntityTag); // data >> ghostEntityTag;
        number_t partition;      readInt(data, partition);      // data >> partition;
        ghostpart.push_back(make_pair(ghostEntityTag, partition));
      }

      size_t numPoints,  numCurves, numSurfaces, numVolumes;
      readInt(data, numPoints);   readInt(data, numCurves);  // data >> numPoints >> numCurves;
      readInt(data, numSurfaces); readInt(data, numVolumes); // data >> numSurfaces >> numVolumes;

      for (size_t nb=0; nb<numPoints; nb++) {
        number_t pointTag;  readInt(data, pointTag);  // data >> pointTag;
        number_t parentDim; readInt(data, parentDim); // data >> parentDim;
        number_t parentTag; readInt(data, parentTag); // data >> parentTag;
        parentDT[pointTag] = make_pair(parentDim, parentTag);

        size_t numPartitions;  readInt(data, numPartitions); // data >> numPartitions;
        // Read the partitionTags
        set<number_t> spartag;
        for (number_t i=0; i<numPartitions; i++) {
          readInt(data, intVal); // data >> intVal;
          spartag.insert(intVal);
        }
        partparttag[pointTag] = spartag;

        real_t xyz; // unused here
        for (int i=0; i<R3Dim; i++) { readRea(data, xyz); } // { data >> xyz; }

        size_t numPhysicalTags;  readInt(data, numPhysicalTags); // data >> numPhysicalTags;
        // Read the physicalTags
        set<number_t> sphtag;
        for (number_t i=0; i<numPhysicalTags; i++) {
          readInt(data, intVal); // data >> intVal;
          sphtag.insert(intVal);
        }
        partphystag[pointTag] = sphtag;
      }
      readPartitionedEntities(data, numCurves,   parentDT, partparttag, partphystag);
      readPartitionedEntities(data, numSurfaces, parentDT, partparttag, partphystag);
      readPartitionedEntities(data, numVolumes,  parentDT, partparttag, partphystag);
      // $EndPartitionedEntities (unused)
    }
    else if (strVal == "$Nodes") { // Read $Nodes section
      size_t numEntityBlocks; readInt(data, numEntityBlocks); // data >> numEntityBlocks;
      size_t numNodes;        readInt(data, numNodes); // data >> numNodes;
      size_t minNodeTag; readInt(data, minNodeTag); // data >> minNodeTag;
      size_t maxNodeTag; readInt(data, maxNodeTag); // data >> maxNodeTag;

      number_t& nb_pts_sctn=numNodes; // number of points in this section
      number_t prevnb_geom_Pts = nb_geom_Pts;
      nb_geom_Pts += nb_pts_sctn;
      PointCoords.resize(R3Dim*nb_geom_Pts);
      nodeNum.resize(nb_geom_Pts);
      real_t* pt_coords_p = &PointCoords[R3Dim*prevnb_geom_Pts];// location may have changed
                                                                // (in case there were several $Nodes sections)
      // Read consecutive EntityBlocks
      for (size_t nbEntBl=0; nbEntBl<numEntityBlocks; nbEntBl++) {
        number_t entityDim, entityTag, parametric; // parametric = 0 or 1
        readInt(data, entityDim); readInt(data, entityTag); // data >> entityDim >> entityTag; (unused here)
        // Nota: entityTag is the "elementary geometrical entity tag" the point belongs to
        readInt(data, parametric);  // data >> parametric;
        size_t numNodesInBlock; readInt(data, numNodesInBlock); // data >> numNodesInBlock;
        // Read the nodeTag(s)
        for (size_t pt_n=0; pt_n<numNodesInBlock; pt_n++) {
          readInt(data, nodeNum[gpt_n++]); //data >> nodeNum[gpt_n++];
        }
        // Read the coordinates (x,y,z), and parameters if any
        for (size_t pt_n=0; pt_n<numNodesInBlock; pt_n++, pt_coords_p += R3Dim) {
          for (int i=0; i<R3Dim; i++) { readRea(data, pt_coords_p[i]); } // { data >> pt_coords_p[i]; }
          // if third coordinate non zero, this is a 3D mesh
          if (pt_coords_p[2] != 0) {
            planeDomain = curveDomain = false;
          }
          else {
            // third coordinate is zero
            // if second coordinate is non zero, this is a 2D mesh
            if (pt_coords_p[1] != 0) { curveDomain = false; }
          }
          if (parametric > 0) {
            real_t uvw;
            for (number_t i=0; i<entityDim; i++) { readRea(data, uvw); } // data >> u;   or v or w
          }
        }
      }// for (size_t nbEntBl=0;
      // $EndNodes (unused)
    }
    else if (strVal == "$Elements") { // Read $Elements section
      size_t numEntityBlocks; readInt(data, numEntityBlocks); // data >> numEntityBlocks;
      size_t numElements;     readInt(data, numElements); // data >> numElements;
      size_t minElementTag; readInt(data, minElementTag); // data >> minElementTag;
      size_t maxElementTag; readInt(data, maxElementTag); // data >> maxElementTag;

      number_t nb_pts=0;
      number_t defndom=0;     // initialized to a default domain number
      number_t prevelmType=0; // initialized to a non existing element type
      number_t& nb_elts_sctn=numElements;  // number of elements in this section
      nb_elts += nb_elts_sctn;
      gelts.resize(nb_elts);
      typedef map<number_t, vector<number_t> > MELM;
      MELM elems; // additional elements to be created in case an elementary
                  // geometrical entity s to several physical entities.

      // Read consecutive EntityBlocks
      for (size_t nbEntBl=0; nbEntBl<numEntityBlocks; nbEntBl++) {
        number_t entityDim; readInt(data, entityDim); // data >> entityDim; (unused here)
        number_t entityTag; readInt(data, entityTag); // data >> entityTag;
        // Nota: entityTag is the "elementary geometrical entity tag" the element belongs to
        number_t elmType;   readInt(data, elmType);   // data >> elmType; (elementType)
        if (elmType != prevelmType) {
          number_t elmDim=0;
          bool isSimplex;
          RefElement* re_p = getRefElt(elmType, gmMap, &nb_pts, &elmDim, &isSimplex);
          if (elementDimx < elmDim) { elementDimx = elmDim; }
          if (! isSimplex) { isMadeOfSimplices_ = false; }
          prevelmType = elmType;
          // Store data related to this element type for future use:
          elMap[elmType] = ELTDEF(nb_pts, elmDim, re_p);
        }

        bool storeElt = false;
        number_t curndom = defndom;
        if (! phystag[entityTag].empty()) {
          curndom = *(phystag[entityTag].begin());
          storeElt = (phystag[entityTag].size() > 1);
        }
        domSet.insert(curndom); // store the number of the physical entity (domain number). Each one is unique.

        size_t numElementsInBlock; readInt(data, numElementsInBlock); // data >> numElementsInBlock;

        // Read the elements in this block
        for (size_t el_n=0; el_n<numElementsInBlock; el_n++) {
          gelts[gel_n].readOrder=gel_n;
          readInt(data, intVal);  // data >> intVal; // elementTag (unused, use gel_n instead)
          gelts[gel_n].elty = elmType; // elm-type
          // Record the number of the physical entity to which the element belongs: if the elementary
          // geometrical entity (whose tag is entityTag) belongs to several physical entities (cf.
          // $Entities section), the physical entity of smallest tag (the first in the set) is first used.
          // It is one of the physical-tags in the $PhysicalNames section if this section is present.
          // Other physical entities are taken into account later, when all the elements have been read.
          // If no physical entity is defined, a default one is provided.
          if (storeElt) { elems[entityTag].push_back(gel_n); }
          gelts[gel_n].ndom = curndom;
          // Record the number of the elementary geometrical entity to which
          // the element belongs (used only if it is a cracked domain)
          gelts[gel_n].ndom2 = entityTag;
          short int i_cd = findId(crackData.begin(), crackData.end(), curndom);
          if (i_cd > -1) {
            crackedDomSet[curndom].insert(gelts[gel_n].ndom2); // store the number of the geometrical entity. Each one is unique.
            it_inCrackData[curndom]=i_cd;
          }
          // Now, read the list of the nb_pts node numbers (nodeTags) of the current element
          vector<number_t>& gnum_el = gelts[gel_n].nums;
          gnum_el.resize(nb_pts);
          for (number_t i=0; i<nb_pts; i++) { readInt(data, gnum_el[i]); } // { data >> gnum_el[i]; }
          gel_n++;
        }
      }// for (size_t nbEntBl=0;

      // Create additional elements to take into account the fact that some elementary
      // geometrical entities may belong to several physical entities. Each new element
      // differ from its original copy by its own tag and the associated physical entity tag
      // (a physical entity corresponds to (possibly part of) a computational domain).
      for (MELM::const_iterator it_elems=elems.begin(); it_elems!=elems.end(); it_elems++) {
        number_t entityTag = it_elems->first;
        // Take into account all physical entities, containing this elementary geometrical entity,
        // except the first one previoulsy treated.
        set<number_t>::const_iterator it_s=phystag[entityTag].begin();
        for (++it_s; it_s!=phystag[entityTag].end(); it_s++) {
          number_t curndom = *it_s;
          domSet.insert(curndom); // store the number of the physical entity (domain number). Each one is unique.
          const vector<number_t>& velts=it_elems->second;
          nb_elts += velts.size();
          gelts.resize(nb_elts);
          // Add new elements with their associated physical entity curndom
          for (vector<number_t>::const_iterator it_v=velts.begin(); it_v!=velts.end(); it_v++) {
            gelts[gel_n] = gelts[*it_v];
            gelts[gel_n].readOrder=gel_n;
            gelts[gel_n].ndom = curndom;
            gel_n++;
          }// elements
        }// physical entities
      }
      // $EndElements (unused)
    }// else if (strVal == "$Elements")

    else if (strVal == "$PhysicalNames") {
      readPhysNames(data, domNameMap);
      // $EndPhysicalNames (unused)
    }

    else { /* strVal is an end of section delimiter or unused data */}
  } // end of while (data >> strVal)

  if (gpt_n != nb_geom_Pts) { error("gmsh_wrong_nbnodes",gpt_n,filename,nb_geom_Pts); }
  if (gel_n != nb_elts)     { error("gmsh_wrong_nbelts",gel_n,filename,nb_elts); }
}


//----------------------------------------------------------------------------------------
// Conversion of the Gmsh file inputfilename to the corresponding 2.2 format file, created
// in the local directory. Its name begins with "xlifepp_converted_" and is returned
// through the outputfilename argument.
// The conversion is done only if Gmsh is available, in which case the function returns 2.
// The function returns 0 otherwise.
int convertmeshfile(const string_t& inputfilename, string_t& outputfilename) {
  #ifdef XLIFEPP_WITH_GMSH
    outputfilename = "xlifepp_converted_" + basename(inputfilename) + ".msh";
    string_t opts(" -0 "+inputfilename+" -format msh2 -o "+outputfilename);
    #ifdef OS_IS_WIN
      string_t cmd = "\""+theEnvironment_p->gmshExe() +"\""+opts+" -v 0 1> "+theLogGmshFile+" 2>&1";
    #elif defined(OS_IS_UNIX)
      string_t cmd = theEnvironment_p->gmshExe() +opts+" >> "+theLogGmshFile;
    #endif
    int ierr=system(cmd.c_str());
    if (theVerboseLevel > 1) { info("cmd_exec",cmd, ierr); }
    info("free_info", "Conversion of " + inputfilename + " to 2.2 format file " + outputfilename);
    return 2;
  #else
    outputfilename = inputfilename;
    return 0;
  #endif
}

/*
 --------------------------------------------------------------------------------
 Mesh::loadGmsh loads a mesh from the input file filename according to Gmsh mesh
 format version 2.2 or 4.1, whose definition can be found at
  http://geuz.org/gmsh/doc/texinfo/gmsh.html#MSH-ASCII-file-format
 or in the documentation file
  http://geuz.org/gmsh/doc/texinfo/gmsh.pdf

 An input file containing data with another format is converted into a 2.2 format
 local file which is then processed, unless Gmsh is not available. The name of this
 local file begins with "xlifepp_converted_".

 Nota: An input file, in.msh, may be converted to another format using Gmsh.
       For instance, the commands:
         gmsh in.msh -0 -format msh2  -o out.msh
         gmsh in.msh -0 -format msh41 -o out.msh
       create the output file out.msh using format 2.2 and 4.1 respectively.
       The last command works only with recent enough versions of Gmsh, e.g. 4.3.0.

 The mesh contained in the file may contain several domains (physical entities
 according to Gmsh terminology). As many domains as there are in the file are
 created, with their associated names if any (specified in the $PhysicalNames section),
 either as plain domains or as subdomains. Each domain found to be a subdomain of
 another domain is created as such ; it is not created as a plain domain.
 If no name is found in the data file, a name is provided which is of the form:
  - Omegai for a plain domain,
  - dPlainDomainName_i for a subdomain, e.g. dOmegaj_i,
 where i is the number of the physical entity associated to the set of elements.

 Let N be the total number of elements found in the file, gathering all domains and
 subdomains. The elements are numbered as a continuous sequence from 1 to N.

 Restrictions:
  1. This function treats only the text format (ASCII format), not the binary one.
  2. Only complete Lagrange elements are taken into account ; incomplete elements
     proposed by Gmsh are rejected (e.g. the 8-node second order quadrangle,
     bearing the the type number 16).
  3. Data sections present in the input file that are taken into account are the
     following: $MeshFormat, $Nodes, $Elements and $PhysicalNames.
     In format 4.1, the optional data section $Entities is also read.
     All other data sections are ignored.
 --------------------------------------------------------------------------------
 */
void Mesh::loadGmsh(const string_t& inputfilename, number_t nodesDim, std::set<CrackData>* cracks) {
  trace_p->push("Mesh::loadGmsh");
  // getting cracks data from gmsh mesh generator
  vector<CrackData> crackData;
  if (cracks != nullptr)
  {
    set<CrackData>::const_iterator it;
    for (it=cracks->begin(); it != cracks->end(); ++it) { crackData.push_back(*it); }
  }

  #ifdef OS_IS_WIN
    // using C style to read file (for Windows only)
    FILE * data = fopen(inputfilename.c_str(),"r");
    if (data==nullptr) { error("file_failopen", "Mesh::loadGmsh", inputfilename); }
  #else
    ifstream data(inputfilename.c_str());
    if ( !data ) { error("file_failopen", "Mesh::loadGmsh", inputfilename); }
  #endif

  comment_ = string_t("Gmsh mesh format read from file ") + basenameWithExtension(inputfilename);
  if ( theVerboseLevel > 1) { info("loadFile_info", "Gmsh", inputfilename); }

  // --------------------------- Start of STEP 1 ---------------------------
  number_t intVal;
  real_t versnum;
  /*
    Mesh characteristics
  */
  // 1. Read $MeshFormat mandatory section, skipping any other preceding section if any,
  //    e.g. a comment section.
  if (! lookfor("$MeshFormat", data)) { error("gmsh_bad_format","$MeshFormat",inputfilename); }
  readRea(data, versnum); //data >> versnum; // version-number
  readInt(data, intVal); //data >> intVal; // file-type
  if (intVal != 0) // the file is not a text file
  { error("gmsh_binary_file",inputfilename); }
  // data-size (unused)
  // $EndMeshFormat (unused)

  string_t filename(inputfilename);
  int fmtver = 0;
  if      (std::abs(versnum - 2.2) < theTolerance) { fmtver = 2; }
  else if (std::abs(versnum - 4.1) < theTolerance) { fmtver = 4; }
  else // The mesh file is not in a recognized version. Try to convert it and go on with the new mesh file.
  {
    closeFile(data); // data.close();
    fmtver = convertmeshfile(inputfilename, filename);
    #ifdef OS_IS_WIN
      // using C style to read file (for Windows only)
      data = fopen(filename.c_str(),"r");
      if (data==nullptr) { error("file_failopen", "Mesh::loadGmsh", filename); }
    #else
      data.open(filename.c_str());
      if ( !data ) { error("file_failopen", "Mesh::loadGmsh", filename); }
    #endif
  }

  // 2. Go on reading the file. First prepare some containers for data related to:
  // A. nodes
  //  Read and store points numbers and coordinates assuming the dimension of space is 3,
  //  since each point is defined by 3 coordinates, according to file format.
  //  Determine the space dimension which is 2 if all third coordinates are 0, 3 otherwise.
  bool planeDomain, curveDomain;
  number_t nb_geom_Pts;      // total number of points in the data file
  vector<real_t> PointCoords;
  vector<number_t> nodeNum;

  // B. elements
  GMSHMAP gmMap;
  initGmshMap(gmMap);
  ELTDEFMAP elMap;        // stores data for each element type found in the Gmsh data file
  dimen_t elementDimx=0;  // maximum dimension of elements in mesh (can be less than space dimension)
  number_t nb_elts=0;     // total number of elements in the data file
  vector<GELT> gelts;     // for temporary storage of elements
  set<number_t> domSet;                        // to gather the different domain numbers    found in the data file
  map<number_t, set<number_t> > crackedDomSet; // to gather the different domain subnumbers found in the data file
  map<number_t, number_t > it_inCrackData;

  // C. domains
  map<number_t, string_t> domNameMap;

  /*
    Mesh geometric point list and element list, and domain names if there are any
  */
  switch (fmtver)
  {
    case 2:
      readfmt2(data, filename, crackData, nb_geom_Pts, PointCoords, nodeNum, planeDomain, curveDomain, nb_elts, gelts,
               elMap, elementDimx, gmMap, domSet, crackedDomSet, it_inCrackData, domNameMap, isMadeOfSimplices_);
      break;
    case 4:
      readfmt4(data, filename, crackData, nb_geom_Pts, PointCoords, nodeNum, planeDomain, curveDomain, nb_elts, gelts,
               elMap, elementDimx, gmMap, domSet, crackedDomSet, it_inCrackData, domNameMap, isMadeOfSimplices_);
      break;
    default:  // version-number of file format not taken into account
      error("gmsh_bad_format_version",versnum,filename);
      break;
  }
  closeFile(data); // data.close();
  // In order to enable automatic name generation for domains, just add here:
  // domNameMap.clear();

  // Reading the data file is now finished. Now, make some consistency checks,
  // set space dimension and create nodes.
  number_t minnNum = *std::min_element(nodeNum.begin(), nodeNum.end());
  number_t maxnNum = *std::max_element(nodeNum.begin(), nodeNum.end());
  if (minnNum < 1) { error("gmsh_node_outofrange",minnNum,filename,maxnNum); }
  for (number_t el_n=0; el_n<nb_elts; el_n++)
  {
    vector<number_t>& gnum_el = gelts[el_n].nums;
    for (number_t i=0; i<gnum_el.size(); i++)
    {
      if (gnum_el[i] < 1 || gnum_el[i] > maxnNum) { error("gmsh_node_outofrange",gnum_el[i],filename,maxnNum); }
    }
  }
  if (maxnNum != nb_geom_Pts) // nodes numbers are not in a continuous range
  {
    // renumber nodes in the intervall [1,nb_geom_Pts]
    map<number_t, number_t> toInter;
    for (number_t pt_n=0; pt_n<nb_geom_Pts; pt_n++) { nodeNum[pt_n] = toInter[nodeNum[pt_n]] = pt_n+1; }
    // reflect this change in the elements definition
    for (number_t el_n=0; el_n<nb_elts; el_n++)
    {
      vector<number_t>& gnum_el = gelts[el_n].nums;
      for (number_t i=0; i<gnum_el.size(); i++) { gnum_el[i] = toInter[gnum_el[i]]; }
    }
  }
  dimen_t spaceDim = 3; // space dim
  if (nodesDim != 0) { spaceDim=nodesDim; }
  else
  {
    if (planeDomain) { spaceDim = 2; }
    if (curveDomain) { spaceDim = 1; }
  }
  if (elementDimx > spaceDim) { error("gmsh_bad_dim",elementDimx,filename,spaceDim); }

  //  Create nodes
  nodes.resize(nb_geom_Pts);
  real_t* pt_coords_p = &PointCoords[0];
  for (number_t pt_n=0; pt_n<nb_geom_Pts; pt_n++, pt_coords_p += R3Dim)
  {
    if (nodeNum[pt_n] < 1 || nodeNum[pt_n] > nb_geom_Pts)
    { error("gmsh_node_outofrange",nodeNum[pt_n],filename,nb_geom_Pts); }
    nodes[nodeNum[pt_n]-1] = Point(spaceDim, pt_coords_p);
  }

  number_t ndomx=1000; // A SUPPRIMER

  // Sort elements by type, then by domain number. The following actions rely on this.
  sort(gelts.begin(), gelts.end(), compareGELTs);

  // --------------------------- Start of STEP 2 ---------------------------
  map<GeomElement*, number_t> elements2gelt;// std::vector<number_t> elements2gelt(nb_elts,0);

  //  elMap.size() different elements have been encountered.
  //  First, store elements of maximum dimension elementDimx, create the associated domains, and
  //  eventually the sub-domains if it happens that other domains made of elements of dimension
  //  elementDimx-1 found in the file are sub-domains of these domains (boundaries or interfaces).
  //  Then, do the same for lower dimensions.
  //  This is done in the following "decreasing loop on elements dimension".

  //  a. Determine how many such elements (of maximum dimension) there are:
  /* unused ; kept here for memory purpose
  number_t nb_elts_maxdim(0);
  for (ELTDEFMAP::iterator it=elMap.begin(); it != elMap.end(); it++) {
    if (it->second.elmDim == elementDimx) {
      number_t elmType = it->first;
      for (number_t el_n=0; el_n<nb_elts; el_n++) {
       if (elmType == gelts[el_n].elty) { nb_elts_maxdim++; }
      }
    }
  }*/
  number_t crackedDomSetSize=0;
  for (map<number_t, set<number_t> >::const_iterator it_ms=crackedDomSet.begin();
       it_ms != crackedDomSet.end(); ++it_ms)
  {
    for (set<number_t>::const_iterator it_s=it_ms->second.begin(); it_s != it_ms->second.end(); ++ it_s)
    { crackedDomSetSize++; }
  }
  crackedDomSetSize-=crackedDomSet.size();
  number_t no_elt(0);             // the number associated to a plain element, updated when a new GeomElement is created
  number_t no_sideElt(nb_elts+1); // the number associated to a side  element, updated when a new GeomElement is created
  // ==> the numbers of the plain elements are in [1, nb_elts]
  //     the numbers of the side elements start from nb_elts+1
  // This is a temporary situation, true only during the construction phase of the elements. At the end,
  // the nb_elts elements are renumbered so that their numbers are consecutive and lying in [1, nb_elts],
  // following the order of creation of the elements.
  vector<GeomElement*> allElts(nb_elts); // Used to temporary store adresses of all elements present in the file.
  // If they are created as side elements during one iteration, they are not created as plain elements during
  // next iteration, in the following "decreasing loop on elements dimension".
  elements_.clear();
  domains_.reserve(domSet.size()+crackedDomSetSize);// at least domSet.size()+crackedDomSet.size() domains will be created
  //   vector<number_t> nbOfEltsInDim(elementDimx+1,0);// unused
  vector<GeomElement*>::const_iterator itb_Elts = allElts.begin(), // corresponds to no_elt(0)
      itb_EltsCurDim, ite_EltsCurDim = itb_Elts;
  vector<number_t> markvert(nb_geom_Pts+1,0); // to mark those nodes that are vertices
  // The following vector records the numbers of the subdomains created in order to
  // not also create them as plain domains:
  vector<number_t> sdNum;
  const number_t unexDom = *(domSet.rbegin()) + 1;// non existing domain number (= max domain number + 1)

  for (int_t curEltDim=elementDimx; curEltDim>=0; curEltDim--) // decreasing loop on elements dimension
  {
    //  b. Create elements of dimension curEltDim
    SIDELTMAP parentEl;
    itb_EltsCurDim = ite_EltsCurDim;
    number_t curEltDimM1 = curEltDim - 1;
    // startDom holds the rank (in allElts) of the first element created (of dimension curEltDim)
    // in a domain and the associated domain number.
    // -> a domain may be made of elements of different types
    vector<pairNN > startDom;
    number_t prevelmType = 0;    // initialized to a non existing element type
    number_t prevndom = unexDom; // initialized to a non existing domain number
    number_t elmDim=0;
    RefElement* re_p=nullptr;
    for (number_t el_n=0; el_n<nb_elts; el_n++)
    {
      number_t elmType = gelts[el_n].elty;
      // Detect change of element type in the list:
      if (elmType != prevelmType)
      {
        elmDim = elMap.at(elmType).elmDim;
        re_p   = elMap.at(elmType).refElt_p;
        prevelmType = elmType;
      }
      // Create element only if the dimension is the current one.
      if (elmDim == static_cast<number_t>(curEltDim))
      {
        // Detect change of domain number and record it for future use
        if (gelts[el_n].ndom != prevndom)
        {
          prevndom = gelts[el_n].ndom;
          // no_elt is the first element of domain number prevndom
          startDom.push_back(make_pair(no_elt,prevndom));
        }

        if (gelts[el_n].plain) // Create new plain element.
        {
          allElts[no_elt] = gelts[el_n].ge_p = new GeomElement(this, re_p, spaceDim, no_elt + 1);
          // Store the node numbers inside the GeomElement.
          // Apply a permutation to Gmsh numbering in order to get the corresponding one in XLiFE++.
          MeshElement* melt = allElts[no_elt]->meshElement();
          number_t *gnum_el = &gelts[el_n].nums[0], *permut = gmMap.at(elmType).perm_p;
          for (number_t i=0; i<elMap.at(elmType).nbPts; i++) { melt->nodeNumbers[i] = gnum_el[permut[i]];}
          melt->setNodes(nodes); // update node pointers

          // Update vertex numbers: the vertexNumbers are the first of nodeNumbers
          // Mark them in the global list markvert, formerly initialized to 0 (vertex numbers are > 0)
          number_t nbVert = re_p->geomRefElement()->nbVertices();
          for (number_t i=0; i<nbVert; i++)
          {
            number_t k = melt->vertexNumbers[i] = melt->nodeNumbers[i];
            markvert[k] = k;
          }
        }
        else
        {
          // gelts[el_n] has already been created as a side element in the previous loop iteration:
          // get the address of the corresponding (side) GeomElement
          allElts[no_elt] = gelts[el_n].ge_p;
        }
        // Store sides of this new GeomElement in a map: side ----> (parent element, side number)
        storeElSides(allElts[no_elt], no_elt, parentEl);

        elements2gelt.insert(make_pair(allElts[no_elt], el_n));
        no_elt++;
      }
    }

    // Record position of fictive first element of fictive next domain:
    startDom.push_back(make_pair(no_elt,0));

    ite_EltsCurDim = vector<GeomElement*>::const_iterator(allElts.begin()+no_elt);

    // From now on, itb_EltsCurDim and ite_EltsCurDim define the range in allElts
    // of the elements of current dimension just created.

    //  c. Create domains made of elements of dimension curEltDim, except if they have
    //     already been created as subdomains.
    //     Store the elements of the created domains in vector<GeomElement*> elements_ of mesh.
    //     By construction, each domain is made of elements whose numbers are consecutive.
    vector<pairNN >::iterator it=startDom.begin();
    intVal = it->second;
    number_t first_no_elt(it->first);
    for (it++; it < startDom.end(); it++)
    {
      if (find(sdNum.begin(),sdNum.end(),intVal) == sdNum.end())
      {
        // intVal has not been created as a subdomain ; create it as a plain domain.
        string_t domName = genDomName(intVal,domNameMap);
        vector<GeomElement*>::const_iterator begin_elt(allElts.begin()+first_no_elt),
                                               end_elt(allElts.begin()+it->first);
        number_t nelts = end_elt-begin_elt;
        ostringstream ss;   ss << "num. ref. " << intVal << ", " << words("made of") << " " << nelts << " " << words("elements");
        MeshDomain* meshdom_p = (new GeomDomain(*this, domName, curEltDim, ss.str()))->meshDomain();
        meshdom_p->geomElements.assign(begin_elt, end_elt);
        domains_.push_back(meshdom_p);
        // Store plain elements addresses related to this domain in elements_ vector.
        for (vector<GeomElement*>::const_iterator it=begin_elt; it < end_elt; it++) { elements_.push_back(*it); }
        if (theVerboseLevel > 2) { info("gmsh_domain_info",domName,intVal,nelts); }
        // nbOfEltsInDim[curEltDim] += end_elt-begin_elt;// unused
      }
      first_no_elt = it->first;
      intVal = it->second;
    }

    //  d. Create domains made of elements of dimension curEltDimM1 if they are sub-domains of domains
    //     just created, i.e. if they are boundary or interface domains.
    //     Among the elements stored in gelts, only those of dimension curEltDim have been taken into
    //     account. The remaining ones (of dimension curEltDimM1) may belong to sub-domains of the
    //     domains just created. If this occur, they should be part of the boundary of some of the
    //     elements of dimension curEltDim created before.
    //

    //  d.1. First, for each of the remaining elements R of dimension curEltDimM1, identify which element
    //       E of dimension curEltDim it belongs to (if any), and which boundary part F of E it corresponds to.
    //       Each of these elements R is thus defined as a couple (E,F) where E is the rank of an element
    //       in allElts, and F is a local number in XLiFE++ numbering convention. It is an edge in 2D,
    //       a face in 3D. Theses couples (E,F) are stored in a vector used later to create the domains.
    //       In the case of an interface domain, there exist 2 elements E for each R. The one considered
    //       is the first found, and due to the ordering of the elements, the elements E corresponding to
    //       an interface all belong to the same domain.

    number_t rkxel=0;
    vector<vector<GeomElement*> > vv_ge;
    vector<GeomElement*> v_ge;
    vector<vector<number_t> > vv_id, vv_ln, vv_ndom2;
    vector<number_t> v_id, v_ndom, v_ndom2, v_ln, v_rkxel;
    // vv_ge and vv_ln contain, for each sub-domain, the list of elements E and the corresponding
    // side number F. They are used during the construction of subdomains (d.2).
    // v_ndom contains these sub-domain numbers. For each of them, v_rkxel contains the rank of
    // maximum value in allElts among all elements found, when searching which element a sub-domain
    // element belongs to. This information is used in conjunction with startDom to recover the
    // domain number corresponding to each sub-domain.
    // vv_id contains, for each sub-domain, the indices in gelts corresponding to the elements that
    // are found to be side of the elements E stored in vv_ge, in the same order. These indices are
    // used to manage the creation of the plain and side elements.

    // vv_ndom2 that contains for each subdomain, the list of secondary ref num of elements. It is
    // used during the construction of cracked subdomains (d.2).
    prevelmType = 0;    // initialized to a non existing element type
    prevndom = unexDom; // initialized to a non existing domain number
    v_ndom.push_back(prevndom); // Initialization to make vv_ge, vv_ln, v_rkxel and v_ndom to have
                                // the same length at the end
    for (number_t el_n=0; el_n<nb_elts; el_n++)
    {
      number_t elmType = gelts[el_n].elty;
      // Detect change of element type in the list:
      if (elmType != prevelmType)
      {
        elmDim = elMap.at(elmType).elmDim;
        prevelmType = elmType;
      }
      // Select elements whose dimension is equal to curEltDimM1 and find which element of dimension
      // curEltDim it belongs to, among those created and stored in allElts at step b. above.
      if (elmDim == curEltDimM1)
      {
        // Detect change of domain number and record it for future use
        if (gelts[el_n].ndom != prevndom)
        {
          prevndom = gelts[el_n].ndom;
          v_ndom.push_back(prevndom);
          v_rkxel.push_back(rkxel); rkxel = 0;
          // push then clear previous vectors which are empty at the first time
          vv_id.push_back(v_id); v_id.clear();
          vv_ge.push_back(v_ge); v_ge.clear();
          vv_ln.push_back(v_ln); v_ln.clear();
          vv_ndom2.push_back(v_ndom2); v_ndom2.clear();
          // vv_geInd.push_back(v_geInd); v_geInd.clear();
        }
        ITPARENTS itpar; // pair of iterators on the map parentEl
        if (foundParent(gelts[el_n].nums, parentEl, itpar))
        {
          // Note: For now, we use the first parent found, given by itpar.first.
          // If itpar.second equals itpar.first + 1, then there is only one parent. Otherwise,
          // there are 2 parents (since more than 2 parents is an erroneous situation ! ).
          // This may happen in the case of an interface domain: this element is the side of 2
          // parent elements and in this case, the second parent is given by itpar.first + 1.
          number_t rkel = itpar.first->second.first;
          if (rkel > rkxel) {rkxel = rkel;}
          v_id.push_back(el_n);
          v_ge.push_back(allElts[rkel]);              // parent element
          v_ln.push_back(itpar.first->second.second); // local side number in parent element
          v_ndom2.push_back(gelts[el_n].ndom2);
          // v_geInd.push_back(rkel);
        }
        // else this element is not part of any sub-domain of previously created domains.
      }
    }
    // Store last information built:
    v_rkxel.push_back(rkxel);
    vv_id.push_back(v_id); v_id.clear();
    vv_ge.push_back(v_ge); v_ge.clear();
    vv_ln.push_back(v_ln); v_ln.clear();
    vv_ndom2.push_back(v_ndom2); v_ndom2.clear();
    // vv_geInd.push_back(v_geInd); v_geInd.clear();

    //  d.2. Second, create sub-domains (boundary or interface domains) made of elements
    //       whose dimension is equal to curEltDimM1.
    for (number_t i=1; i<v_ndom.size(); ++i)
    {
      if (vv_ge[i].size() > 0) // this one is a sub-domain of one of the domains previously created
      {
        number_t numDom=0;
        for (vector<pairNN >::iterator itsd=startDom.begin(); itsd<startDom.end(); itsd++)
        {
          if (v_rkxel[i] >= itsd->first) { numDom = itsd->second; }
        }
        if (crackedDomSet.count(v_ndom[i]) == 0) // the sub-domain was not cracked
        {
          string_t domName = genSDomName(numDom,v_ndom[i],domNameMap);
          ostringstream ss;   ss << "num. ref. " << v_ndom[i] << ", subdomain of domain "
                                 << domNameMap[numDom] << ", made of " << vv_ge[i].size() << " elements";
          MeshDomain* meshdom_p = (new GeomDomain(*this, domName, curEltDimM1, ss.str()))->meshDomain();
          vector<GeomElement*>& v_ge = meshdom_p->geomElements;
          vector<GeomElement*>::const_iterator it_ge;
          vector<number_t>::const_iterator it_ln, it_id;
          for (it_id=vv_id[i].begin(), it_ln=vv_ln[i].begin(), it_ge=vv_ge[i].begin();
               it_ge < vv_ge[i].end();   it_id++, it_ge++, it_ln++)
          {
            v_ge.push_back( gelts[*it_id].ge_p = new GeomElement(*it_ge,*it_ln, no_sideElt++) );
            gelts[*it_id].side();
          }
          domains_.push_back(meshdom_p);
          if (theVerboseLevel > 2) { info("gmsh_subdomain_info",domName,v_ndom[i],domNameMap[numDom],vv_ge[i].size()); }
          // record the creation of this subdomain:
          sdNum.push_back(v_ndom[i]);
        }
        else
        { // crackedDomSet.count(v_ndom[i]) == 1 because the cracked domain is listed once
          // we need to store geometrical references of parent domains
          // std::vector<number_t> parentInGelt(crackedDomSet[v_ndom[i]].size(),elements2gelt[vv_ge[i][0]->number()-1]);
          vector<number_t> parentInGelt(crackedDomSet[v_ndom[i]].size(),elements2gelt.at(vv_ge[i][0]));
          number_t i_s=0;
          set<number_t> crackedDomParentGeomNumbers;
          for (set<number_t>::const_iterator it_s=crackedDomSet[v_ndom[i]].begin();
               it_s != crackedDomSet[v_ndom[i]].end();    ++it_s, ++i_s)
          {
            crackedDomParentGeomNumbers.insert(gelts[parentInGelt[i_s]].ndom2);
            vector<GeomElement*>::const_iterator it_ge;
            vector<number_t>::const_iterator it_ln;
            number_t nbelts=0,elInd=0;
            // the parentInGelt vector is updated only if
            //     - elements having vv_ndom2[i][elInd] == *it_s have been selected previously
            //     - vv_ndom2[i][elInd] changes
            for (it_ln=vv_ln[i].begin(), it_ge=vv_ge[i].begin(); it_ge < vv_ge[i].end()  ; ++it_ge, ++it_ln, ++elInd)
            {
              if (*it_s == vv_ndom2[i][elInd]) { nbelts++; }
              else
              {
                if (nbelts > 0) { parentInGelt[i_s+1]=elements2gelt.at(vv_ge[i][elInd]); break; }
              }
            }
          }
          if (crackedDomParentGeomNumbers.size() > 1 )
          {
            // parent domains around the crack are defined with different geometrical objects
            // we use the geometrical number of each parent domain to name sides of the crack
            map<number_t,number_t> crackedDomParentDomRefNumbers;
            i_s=0;
            for (set<number_t>::const_iterator it_s=crackedDomSet[v_ndom[i]].begin();
                 it_s != crackedDomSet[v_ndom[i]].end(); ++it_s, ++i_s)
            {
              crackedDomParentDomRefNumbers[gelts[parentInGelt[i_s]].ndom2]=gelts[parentInGelt[i_s]].ndom;
            }

            set<string_t> crackedDomParentNames;
            for (map<number_t,number_t>::const_iterator it_cdpdrf=crackedDomParentDomRefNumbers.begin();
                 it_cdpdrf != crackedDomParentDomRefNumbers.end(); ++it_cdpdrf)
            {
              if (domNameMap.find(it_cdpdrf->second) != domNameMap.end()) crackedDomParentNames.insert(domNameMap[it_cdpdrf->second]);
            }

            map<number_t,string_t> crackedDomParentDomNames;
            if (crackedDomParentNames.size() > 1)
            {
              // there are several names
              // it is assumed that there are as many names as parent domains
              for (map<number_t,number_t>::const_iterator it_cdpdrf=crackedDomParentDomRefNumbers.begin();
                 it_cdpdrf != crackedDomParentDomRefNumbers.end(); ++it_cdpdrf)
              {
                crackedDomParentDomNames[it_cdpdrf->first]=domNameMap[it_cdpdrf->second];
              }
            }
            else
            {
              // all parent domains have the same name
              // as we manage only 1D cracks in 2D geometries or 2F cracks in 3D geometries,
              // the crack has only two sides, so we can call them with _+ and _- suffixes
              string_t suffix="+";
              for (map<number_t,number_t>::const_iterator it_cdpdrf=crackedDomParentDomRefNumbers.begin();
                 it_cdpdrf != crackedDomParentDomRefNumbers.end(); ++it_cdpdrf)
              {
                crackedDomParentDomNames[it_cdpdrf->first]=suffix;
                suffix="-";
              }
            }
            i_s=0;
            MeshDomain* prev_meshdom_p=nullptr;
            bool updateDualCrackDomain=true;
            for (set<number_t>::const_iterator it_s=crackedDomSet[v_ndom[i]].begin();
                 it_s != crackedDomSet[v_ndom[i]].end(); ++it_s, ++i_s)
            {
              ostringstream ss;
              ss << genSDomName(numDom, v_ndom[i], domNameMap) << "_" << crackedDomParentDomNames[gelts[parentInGelt[i_s]].ndom2];
              string_t domName = ss.str();
              ss.str(string_t());
              ss.clear();
              ss << "num. ref. " << v_ndom[i] << ", 2nd num. ref. " << *it_s << ", subdomain of domain "
                 << domNameMap[crackedDomParentDomRefNumbers[gelts[parentInGelt[i_s]].ndom2]] << ", made of " << vv_ge[i].size() << " elements";
              MeshDomain* meshdom_p = (new GeomDomain(*this, domName, curEltDimM1, ss.str()))->meshDomain();
              if (updateDualCrackDomain)  // update of dualCrackDomain pointers, works only for crack with two parents
              {
                if (prev_meshdom_p==nullptr) prev_meshdom_p=meshdom_p;
                else
                {
                  prev_meshdom_p->dualCrackDomain_p=meshdom_p;
                  meshdom_p->dualCrackDomain_p=prev_meshdom_p;
                  updateDualCrackDomain=false;
                }
              }
              vector<GeomElement*>& v_ge = meshdom_p->geomElements;
              vector<GeomElement*>::const_iterator it_ge;
              vector<number_t>::const_iterator it_ln, it_id;
              number_t nbelts=0,elInd=0;
              // select only those where vv_ndom2[i][elInd] == *it_s
              for (it_id=vv_id[i].begin(), it_ln=vv_ln[i].begin(), it_ge=vv_ge[i].begin();
                   it_ge < vv_ge[i].end() ;   it_id++, ++it_ge, ++it_ln, ++elInd)
              {
                // geo ref of parent element
                if (*it_s == vv_ndom2[i][elInd])
                {
                  v_ge.push_back( gelts[*it_id].ge_p = new GeomElement(*it_ge,*it_ln, no_sideElt++) );
                  gelts[*it_id].side();
                  nbelts++;
                }
                else { if (nbelts > 0) { break; } }
              }
              domains_.push_back(meshdom_p);
              if (theVerboseLevel > 2)
              { info("gmsh_cracked_subdomain_info","  ",domName,ndomx++,gelts[parentInGelt[i_s]].ndom,nbelts,v_ndom[i]); }
              // record the creation of this subdomain:
              sdNum.push_back(v_ndom[i]); // we give the origin domain number for better skip at next iteration
            }
          }
          else
          { // crackedDomParentGeomNumbers.size() == 1 : the crack is surrounded by one parent domain
            // we get the minimal box of the geometry supporting the crack domain it_inCrackData[v_ndom[i]]
            // the minimal box gives orientation of the crack through the determination of normal vector
            vector<real_t> normal=crackData[it_inCrackData[v_ndom[i]]].computeNormal();
            map<string_t,vector<GeomElement*> > crackedDomains;
            // we build names of both sides
            string_t domPlusName = genSDomName(numDom, v_ndom[i], domNameMap) + "_+";
            string_t domMinusName = genSDomName(numDom, v_ndom[i], domNameMap) + "_-";
            number_t domPlusId=0, domMinusId=0;
            vector<GeomElement*>::const_iterator it_ge;
            vector<number_t>::const_iterator it_ln;
            number_t nbeltsPlus=0, nbeltsMinus=0,elInd;

            // we scan each crack side:
            i_s=0;
            for (set<number_t>::const_iterator it_s=crackedDomSet[v_ndom[i]].begin();
                 it_s != crackedDomSet[v_ndom[i]].end(); ++it_s, ++i_s)
            {
              // scanning the first element of the side is enough to determine which side it is:
              //   - we get its parent element: vv_ge[i][0] and vv_ln[i][0]
              bool firstParentFound=false;
              elInd=0;
              for (it_ln=vv_ln[i].begin(), it_ge=vv_ge[i].begin(); it_ge < vv_ge[i].end() ; ++it_ge, ++it_ln, ++elInd)
              {
                if (*it_s == vv_ndom2[i][elInd] && !firstParentFound)
                {
                  vector<number_t> verticesNumbers = vv_ge[i][elInd]->meshElement()->verticesNumbers();
                  vector<number_t> verticesNumbersOnCrack = vv_ge[i][elInd]->meshElement()->verticesNumbers(vv_ln[i][elInd]);
                  vector<number_t> verticesIndicesNotOnCrack, verticesIndicesOnCrack;
                  //   - we find one vertex of the parent element that is not on the crack
                  for (number_t k=0; k < verticesNumbers.size(); ++k)
                  {
                    if (find(verticesNumbersOnCrack.begin(),verticesNumbersOnCrack.end(),verticesNumbers[k]) == verticesNumbersOnCrack.end())
                    { verticesIndicesNotOnCrack.push_back(k); }
                    else { verticesIndicesOnCrack.push_back(k); }
                  }
                  if (verticesIndicesOnCrack.size() == 2) // 2 vertices <=> segment edge <=> 1D
                  {
                    number_t k1=verticesIndicesOnCrack[0];
                    number_t k2=verticesIndicesOnCrack[1];
                    number_t kk=verticesIndicesNotOnCrack[0];
                    Point p1=vv_ge[i][elInd]->meshElement()->node(k1+1);
                    Point p2=vv_ge[i][elInd]->meshElement()->node(k2+1);
                    Point pp=vv_ge[i][elInd]->meshElement()->node(kk+1);

                    //   - we compute its projection on the crack, defined by the other 2 vertices
                    real_t hdist;
                    Point h=projectionOnStraightLine(pp,p1,p2,hdist);
                    vector<real_t> height = pp-h;
                    if (height.size() == 2) { height.push_back(0.); }
                    //   - we link the sign of the projection to the side of the crack
                    if (dot(normal,height) > 0) { domPlusId = *it_s;}
                    else { domMinusId = *it_s;}
                  }
                  else // verticesIndicesOnCrack.size() == 3 or 4
                  {
                    number_t k1=verticesIndicesOnCrack[0];
                    number_t k2=verticesIndicesOnCrack[1];
                    number_t k3=verticesIndicesOnCrack[2];
                    number_t kk=verticesIndicesNotOnCrack[0];
                    Point p1=vv_ge[i][elInd]->meshElement()->node(k1+1);
                    Point p2=vv_ge[i][elInd]->meshElement()->node(k2+1);
                    Point p3=vv_ge[i][elInd]->meshElement()->node(k3+1);
                    Point pp=vv_ge[i][elInd]->meshElement()->node(kk+1);

                    //   - we compute its projection on the crack, defined by 3 of the other vertices
                    real_t hdist;
                    Point h=projectionOfPointOnPlane(pp,p1,p2,p3,hdist);
                    vector<real_t> height = pp-h;
                    if (height.size() == 2) { height.push_back(0.); }
                    //   - we link the sign of the projection to the side of the crack
                    if (dot(normal,height) > 0) { domPlusId = *it_s;}
                    else { domMinusId = *it_s;}
                  }
                  firstParentFound=true;
                }
              }
            }

            // definition of domains
            std::stringstream ss;
            ss << "num. ref. " << v_ndom[i] << ", 2nd num. ref. " << domPlusId << ", subdomain of domain "
               << domNameMap[numDom] << ", made of " << vv_ge[i].size() << " elements";
            MeshDomain* meshdomPlus_p = (new GeomDomain(*this, domPlusName, curEltDimM1, ss.str()))->meshDomain();
            ss.str(string_t());
            ss.clear();
            vector<GeomElement*>& v_gePlus = meshdomPlus_p->geomElements;

            ss << "num. ref. " << v_ndom[i] << ", 2nd num. ref. " << domMinusId << ", subdomain of domain "
               << domNameMap[numDom] << ", made of " << vv_ge[i].size() << " elements";
            MeshDomain* meshdomMinus_p = (new GeomDomain(*this, domMinusName, curEltDimM1, ss.str()))->meshDomain();
            meshdomPlus_p->dualCrackDomain_p=meshdomMinus_p;
            meshdomMinus_p->dualCrackDomain_p=meshdomPlus_p;
            ss.str(string_t());
            ss.clear();
            vector<GeomElement*>& v_geMinus = meshdomMinus_p->geomElements;

            vector<number_t>::const_iterator it_id;
            elInd=0;
            for (it_id=vv_id[i].begin(), it_ln=vv_ln[i].begin(), it_ge=vv_ge[i].begin();
                 it_ge < vv_ge[i].end() ;  it_id++, ++it_ge, ++it_ln, ++elInd)
            {
              // dispatch elements to the right domain
              if (domPlusId == vv_ndom2[i][elInd])
              {
                v_gePlus.push_back( gelts[*it_id].ge_p = new GeomElement(*it_ge,*it_ln, no_sideElt++) );
                gelts[*it_id].side();
                nbeltsPlus++;
              }
              else if (domMinusId == vv_ndom2[i][elInd])
              {
                v_geMinus.push_back( gelts[*it_id].ge_p = new GeomElement(*it_ge,*it_ln, no_sideElt++) );
                gelts[*it_id].side();
                nbeltsMinus++;
              }
              else { if (nbeltsPlus > 0 && nbeltsMinus > 0) { break; } }
            }

            domains_.push_back(meshdomPlus_p);
            if (theVerboseLevel > 2) {
              info("gmsh_cracked_subdomain_info2","  ",domPlusName,ndomx++,domPlusId,gelts[parentInGelt[0]].ndom,nbeltsPlus,v_ndom[i]);
            }
            domains_.push_back(meshdomMinus_p);
            if (theVerboseLevel > 2) {
              info("gmsh_cracked_subdomain_info2","  ",domMinusName,ndomx++,domMinusId,gelts[parentInGelt[1]].ndom,nbeltsMinus,v_ndom[i]);
            }
            // record the creation of this subdomain:
            sdNum.push_back(v_ndom[i]); // we give the origin domain number for better skip at next iteration
          } // end of if crackedDomParentGeomNumbers.size() > 1
        } // end of if crackedDomSet.count(v_ndom[i]) == 0
      } // end of if vv_ge[i].size() > 0
    } // for number_t i=1;...
  } // for (int_t curEltDim=elementDimx...
  // lastIndex_ = no_sideElt - 1;
  // Assign numbers to the elements according to creation order, as is done in other constructors.
  number_t numEl = 0;
  for (vector<GeomElement*>::const_iterator it=allElts.begin(); it < allElts.end(); it++)
  {
    (*it)->number() = ++numEl;
  }
  lastIndex_ = numEl;

  // Create list of vertices (vector<number_t> vertices_ member) from marked node numbers
  for (number_t k=1; k<=nb_geom_Pts; k++)
  {
    if (markvert[k]>0) { vertices_.push_back(k); }
  }
  order_=1;
  ELTDEFMAP::iterator it=elMap.begin();
  while(it!=elMap.end() && it->second.refElt_p == nullptr) it++;
  order_=it->second.refElt_p->order();
  for (it=elMap.begin(); it != elMap.end(); it++)
  {
    if (it->second.refElt_p != nullptr)
    {
      if (it->second.refElt_p->order() != order_) // all elements in the mesh should have the same approximation order
      {
        error("gmsh_multiple_orders",filename);
      }
    }
  }
  // Determine the number (nbDomdx) of plain domains of maximum dimension created so far
  int nbDomdx=0;
  for (vector<GeomDomain*>::const_iterator itdom = domains_.begin(); itdom != domains_.end(); itdom++)
  {
    if ((*itdom)->dim() == elementDimx) { nbDomdx++; }
  }
  if (nbDomdx>1) {
    // There are several plain domains of maximum dimension: create a single domain, union of these domains.
    // The name of this domain is #Omega, or the name of the geometry if it exists
    // As domain names beginning with a # are forbidden to the user, this name is necessarily available.
    string_t Uname("#Omega");
    if (geometry_p != nullptr)
    {
      if (geometry_p->domName() != "") Uname = geometry_p->domName();
    }
    string_t sep(" ");
    vector<string_t> dnames; // to store the list of the names of all the domains created so far
    std::vector<const GeomDomain*> domDimx; // to store the list of domains of maximum dimension
    for (vector<GeomDomain*>::const_iterator itdom = domains_.begin(); itdom != domains_.end(); itdom++)
    {
      MeshDomain* md = (*itdom)->meshDomain();
      string_t domname(md->name());
      dnames.push_back(domname);
      if (md->dim() == elementDimx)
      {
        domDimx.push_back(md);
        sep = string_t(", ");
      }
    }
    GeomDomain& meshdom = xlifepp::merge(domDimx, Uname);
    //domains_.push_back(&meshdom);  //done by merge
    // Nota: plain elements addresses related to this new domain are already in elements_ vector.
    if (theVerboseLevel > 2) info("gmsh_global_domain_info", Uname.c_str(), meshdom.description().c_str(), meshdom.numberOfElements());
  }

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  //sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)

  // to generate a blank line in the print file
  thePrintStream << std::endl;

  if (order_ == 1) firstOrderMesh_p = this;

  trace_p->pop();
}

//! load a geo script, generate the associated gmsh file and load it
void Mesh::loadGeo(const string_t& filename, number_t nodesDim, std::set<CrackData>* cracks)
{
  trace_p->push("Mesh::loadGeo");
  int ierr;
#ifndef XLIFEPP_WITH_GMSH
  error("xlifepp_without_gmsh");
#endif

  ifstream data(filename.c_str());
  if ( !data ) { error("file_failopen","Mesh::loadGeo",filename); }
  data.close();

  string_t bname = basename(filename);
  string_t meshfilename = bname+".msh";
  string_t srcfilename=filename;
  #ifdef OS_IS_WIN
    string_t cmd = "\""+theEnvironment_p->gmshExe() +"\" -3 -format msh2 "+srcfilename+ " -o "+meshfilename+" -v 0 1> "+theLogGmshFile+" 2>&1";
  #elif defined(OS_IS_UNIX)
    string_t cmd = theEnvironment_p->gmshExe() +" -3 -format msh2 "+srcfilename+ " -o "+meshfilename+" >> "+theLogGmshFile;
  #endif
  ierr=system(cmd.c_str());
  if (theVerboseLevel > 1) { info("cmd_exec",cmd, ierr); }

  if (cracks != nullptr)
  {
    if (cracks->size() > 0)
    {
      string_t gmshVersion = GMSH_VERSION;
      if (gmshVersion[0] == '3') error("gmsh_crack_bad_version",gmshVersion);
      string_t optmeshfilename, srcmeshfilename, crackedmeshfilename;
      set<CrackData>::const_iterator it;
      number_t i=0;
      for (it=cracks->begin(); it != cracks->end(); ++it, ++i)
      {
        if (i == 0) { srcmeshfilename=meshfilename; }
        else { srcmeshfilename=crackedmeshfilename; }
        optmeshfilename="crack.pos";
        std::ofstream fout(optmeshfilename.c_str());
        fout << "Plugin(Crack).Dimension=" << it->dim << ";" << std::endl;
        fout << "Plugin(Crack).PhysicalGroup=" << it->id << ";" << std::endl;
        fout << "Plugin(Crack).OpenBoundaryPhysicalGroup=" << it->domIdToOpen << ";" << std::endl;
        fout << "Plugin(Crack).Run;" << std::endl;
        fout.close();
        std::stringstream ss;
        ss << bname << "_cracked_" << i+1 << ".msh";
        crackedmeshfilename=ss.str();
        #ifdef OS_IS_WIN
          cmd = "\""+theEnvironment_p->gmshExe() +"\" -3 -format msh2 "+srcmeshfilename+" "+optmeshfilename+" -o "+crackedmeshfilename+"  -v 0 1> "+theLogGmshFile+" 2>&1";
        #elif defined(OS_IS_UNIX)
          cmd = theEnvironment_p->gmshExe() +" -3 -format msh2 "+srcmeshfilename+" "+optmeshfilename+" -o "+crackedmeshfilename+" >> "+theLogGmshFile;
        #endif
        ierr=system(cmd.c_str());
        if (theVerboseLevel > 1) { info("cmd_exec",cmd, ierr); }
      }
      // loadGmsh will verify if the msh file exists and read it
      loadGmsh(crackedmeshfilename, nodesDim, cracks);
    }
    else
    {
      // loadGmsh will verify if the msh file exists and read it
      loadGmsh(meshfilename, nodesDim);
    }
  }
  else
  {
    // loadGmsh will verify if the msh file exists and read it
    loadGmsh(meshfilename, nodesDim);
  }
  trace_p->pop();
}

} // end of namespace xlifepp
