/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Geodesic.hpp
  \authors E. Lunéville
  \since 30 aug 2021
  \date 30 aug 2021

  \brief Definition of the xlifepp::Geodesic class

  The Geodesic class handles a geodesic curve on a 3d surface, it manages mainly
    GeomDomain* dom_p: surface where lives the geodesic (if given)
    vector<Point> xs_: list of geodesic points in 3D space
	vector<Point> dxs_: list of related geodesic tangent vectors in 3D space

  Geodesics can be constructed either from a parametrization, from a mesh description, from a geometry:

    ParametrizedGeodesic inherited class, managing additional data
       Parametrization* par_p: pointer to the surface parametrization
       vector<Point> us_: list of geodesic points in 2D parametrization space
	   vector<Point> dus_: list of geodesic tangent vectors in 2D parametrization space

    MeshedGeodesic inherited class, managing additional data
       list<GeoNumPair> elts_: list of crossed elements

    GeometricGeodesic inherited class (geodesic as a geometry), managing additional data
       Geometry* geom_p: pointer to the geometry of geodesic
       Point sx_: starting point
       Point sdx_: starting direction

    AnalyticGeodesic inherited class (geodesic as an explicit parametrization), managing additional data
       Parametrization parg_: 1D-3D parametrization object related to geodesic curve

   the main purposes of these classes is to compute a geodesic starting at a given point in a given direction
   additional quantities may be computed (curvilinear abcissa, ...)
 */

#ifndef GEODESIC_HPP_INCLUDED
#define GEODESIC_HPP_INCLUDED

#include "config.h"
#include "utils.h"
#include "GeomElement.hpp"
#include "Parametrization.hpp"

namespace xlifepp
{
class Geometry;   //forward declaration
class GeomDomain; //forward declaration

enum GeodesicType{_geodesic=0, _parametrizedGeodesic, _meshedGeodesic,_analyticGeodesic, _geometricGeodesic, _compositeGeodesic};
enum FieldType{_noFieldType,_terminator,_lightOP,_lightFock,_shadowFock,_creeping,_focal};

class MeshedGeodesic;

//===========================================================================================
/*! Geodesic class handling any geodesic (abstract)
    Base class geodesic handles
     - dom_p: pointer to a geometric domain
     - geo_p: pointer to related geometry
     - xs_: vector of successive geodesic points (fill by compute)
     - dxs_: vector of successive geodesic derivatives (fill by compute)
     - curAbcs_: vector of successive geodesic curvilinear abcissa (fill by compute)
     - normals_: vector of successive normal surfaces at geodesics point
	 - curvatures_: vector of curvatures (Gauss, mean and normal curvature)
     - sx_, sdx_: starting point an direction
     - ex_, edx_: ending point an direction
     - length_: geodesic length
     - meshgeod_: pointer to a MeshedGeodesic handling the projection of the geodesic on a mesh
     - field_: to store a field on geodesic (only scalar complex up to now)
     - fieldTypes_: to store field types on geodesic (specific to HF computation)
     - fcur_p, fnor_p: additional pointer to functions giving the principal curvatures and surface normals
                        cannot be deduced from geodesic! 0 if not required
                        automatic assignment if geo_p is assigned and has a parametrization
     - params_: usefull parameters (first Parameter is always this, set by init())

    dom_p or geo_p or both may be not allocated, regarding the type of Geodesic:
     - ParametrizedGeodesic: inherited class using parametrization and a RK4 scheme to compute geodesics
     - MeshedGeodesic: inherited class using mesh to compute geodesics
     - GeometricGeodesic: inherited class using a 1D geometry to describe geodesics
     - AnalyticGeodesic: inherited class using an analytic function to describe geodesics
     - CompositeGeodesic: inherited class using a list of chained geodesics of any type
    all inherited class must provide the computing function
      Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t nmax, real_t dt=0)
    where - (x,dx) is the starting point/dervative of the geodesic
          - l, lmax: the current and max length of the geodesic
          - nmax: maximum number of points to compute
          - dt: step of computation if required (may be dt or ds)
    GeometricGeodesic and AnalyticalGeodesic are available only for a few simple geometries

    When geometry is composed with patches that not match in a C1 way (different tangent planes)
    the new geodesic direction d2 from the previous one d1 at transition point x is computed as follows
        d2 = d1-(d1|n1)(n1+n2)
    where n1 is the outward normal (in tangent plane in x) to the original patch
      and n2 the outward normal (in tangent plane in x) to the new patch
    This rule is effective only when using a ParametrizedGeodesic, a MeshedGeodesic or a CompositeGeodesic
*/
//===========================================================================================
class Geodesic
{
 protected:
    GeomDomain* dom_p;          //!< geom domain where lives the geodesic
    Geometry* geo_p;            //!< geometry where lives the geodesic
    Parametrization* par_p;     //!< pointer to parametrization related to geo_p (if exists)
 public:
    GeodesicType type_;         //!< type of Geodesic (see enum)
	std::vector<Point> xs_;	    //!< list of geodesic points in 3D space
	std::vector<Point> dxs_;      //!< list of geodesic tangent vectors in 3D space
	std::vector<Point> normals_;  //!< list of normal surfaces at geodesics point
	std::vector<real_t> curAbcs_; //!< list of geodesic curvilinear abcissa in 3D space
	std::vector<Vector<real_t> > curvatures_;//!< list of curvatures (Gauss, mean and normal curvature)
	bool withCurAbc;            //!< true if curvilinear abcissa are computed
	bool withTangent;           //!< true if tangent and normal vectors are computed
	bool withCurvatures;        //!< true if manifold principal curvatures and geodesic normal curvatures are computed
	Point sx_;                  //!< starting point (allocated if computed)
    Point sdx_;                 //!< starting direction (allocated if computed)
    Point ex_;                  //!< ending point (allocated if computed)
    Point edx_;                 //!< ending direction (allocated if computed)
	real_t length_;             //!< length of geodesic if computed (else 0)
	MeshedGeodesic* meshgeod_;  //!< projection of geodesic on mesh if required (see buildMeshedGeodesic function)
	mutable Vector<complex_t> field_;            //!< to store a field on geodesic (only scalar complex up to now)
	mutable std::vector<FieldType> fieldTypes_;  //!< to store field types on geodesic (specific to HF computation)
	Vector<real_t> (*fcur_p)(const Point& p, const Point& d, bool fromParameters, Parameters& pars); //!< function pointer to curvatures computation at p (parameters or physical), may be 0
	Vector<real_t> (*fnor_p)(const Point& p, bool fromParameters, Parameters& pars); //!< function pointer to surface normal computation at p (parameters or physical), may be 0
    Parameters params_;         //!< fcur_p anf fnor_p parameters to store additionnal data requires by these functions

	Geodesic() : dom_p(nullptr), geo_p(nullptr), par_p(nullptr), type_(_geodesic), withCurAbc(false), withTangent(false), withCurvatures(false),
	             length_(0), meshgeod_(nullptr), fcur_p(nullptr), fnor_p(nullptr) {init();}
	Geodesic(GeomDomain& dp, bool wCA, bool wT, bool wC) : dom_p(&dp), geo_p(nullptr), par_p(nullptr), type_(_geodesic), withCurAbc(wCA), withTangent(wT), withCurvatures(wC),
	                                                                  length_(0), meshgeod_(nullptr), fcur_p(nullptr), fnor_p(nullptr) {init();}
	Geodesic(Geometry& gp, bool wCA, bool wT, bool wC) : dom_p(nullptr), geo_p(&gp), par_p(nullptr), type_(_geodesic), withCurAbc(wCA), withTangent(wT), withCurvatures(wC),
	                                                                length_(0), meshgeod_(nullptr), fcur_p(nullptr), fnor_p(nullptr) {init();}
	Geodesic(const Geodesic& geod);    //!< copy constructor
	~Geodesic();
	void copy(const Geodesic& geod);   //!< copy tool
	Geodesic& operator=(const Geodesic& geod);
	virtual Geodesic* clone() const {return new Geodesic(*this);}
	virtual void clear();              //! clear xs_,dxs_,curAbcs
	void init();                       //! initialize par_p pointer
	virtual Geodesic& compute(Point& u, Point& du, real_t& l, real_t lmax, number_t n, real_t dt=0){return *this;} //!< compute Geodesics
	virtual Vector<real_t> curvatures(const Point& p, const Point& d, bool fromParameters=false) const; //!< return curvatures (Gauss, mean, normal) if available
	Vector<real_t> normal(const Point& p, bool fromParameters) const; //!< return surface normal if available
    void buildMeshedGeodesic(const GeomDomain& dom, bool trace=false);  //!< build mesh projection of geodesic and attach Geodesic information to each GeomElement
	virtual string_t strtype() const {return "Geodesic";}
	void virtual print(std::ostream& os) const;
	void printData(std::ostream& os) const;   //!< print data
	void virtual saveToFile(const string_t& file) const{saveToFileG(file);};
	void virtual saveToFileG(const string_t& file) const;
};

inline std::ostream& operator<<(std::ostream&os,const Geodesic& g) {g.print(os);return os;}
inline Vector<real_t> zeroCurvatures(const Point& p, const Point& d, bool fromParameters, Parameters& pars) {return Vector<real_t>(3,0.);}
inline Vector<real_t> unCurvatures(const Point& p, bool fromParameters, Parameters& pars) {return Vector<real_t>(3,1.);}
Vector<real_t> planeNormal(const Point& p, bool fromParameters, Parameters& pars);

//===========================================================================================
/*! ParametrizedGeodesic class handling any geodesic got using surface parametrization
    require a GeomDomain or a Geometry or a Parametrization
    in any case, it constructs geodesic using a RK4 scheme to solve the geodesic EDO
         dt(x(t),y(t))=F(x(t),y(t))  x(t)=x0, y(t)=y(0)
    F function (involving Cristoffel symbols) is based on a Parametrization
    When the Parametrization is a PiecewiseParametrization, the computation is done using
    an iterative process to go from one parametrization to the other
*/
//===========================================================================================
class ParametrizedGeodesic : public Geodesic
{
  public:
	std::vector<Point> us_;   //! vector of geodesic points in 2D parametrization space
	std::vector<Point> dus_;  //! vector of geodesic tangent vectors in 2D parametrization space

	ParametrizedGeodesic(): Geodesic() {type_=_parametrizedGeodesic;}
    ParametrizedGeodesic(GeomDomain& dp, bool wCA=false, bool wT=false, bool wC=false);
    ParametrizedGeodesic(Geometry& gp, bool wCA=false, bool wT=false, bool wC=false);
	ParametrizedGeodesic(const Parametrization& pa, bool wCA=false, bool wT=false, bool wC=false);
	ParametrizedGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax,
                         number_t nmax, real_t dt=0, bool wCA=false, bool wT=false, bool wC=false);
	virtual ParametrizedGeodesic* clone() const {return new ParametrizedGeodesic(*this);}
	virtual void clear();               //! clear xs_,dxs_,curAbcs
    Point F(const Point& u, const Point& du, const Parametrization* par=nullptr) const; //! function involved when solving geodesic equation x" = F(x,x')
    bool checkBound(Point &p, const Point& o, const Parametrization& par, real_t tol=0) const;        //! tool to check if p in [0,1]x[0,1], project p along op
    bool checkBound(Point &p, const Parametrization& par, real_t tol=0) const;                        //! tool to check if p in [0,1]x[0,1], project p along axis
    virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0);  //! compute geodesic from x, dx (entry)
    Geodesic& compute(const Parametrization& par, Point& uv, Point& duv,
                      real_t& l, real_t lmax, number_t& k, number_t n, real_t dt);                    //! compute geodesic from x, dx (global parametrization)
    Geodesic& computePiecewise(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0); //! compute geodesic from x, dx (piecewise parametrization)
    virtual string_t strtype() const {return "Parametrized geodesic";}
    virtual void print(std::ostream&) const;
    virtual void saveToFile(const string_t& file) const;
};

//===========================================================================================
/*! MeshedGeodesic class handling any geodesic got using surface mesh
    Mesh is assumed to be linear
    dom_p must be allocated. In addition to xs_,dxs_, curAbcs_, compute function fills in
    the vector of crossed elements (GeomElement*,outward side number)  */
//===========================================================================================
class MeshedGeodesic : public Geodesic
{
 public:
	std::vector<GeoNumPair> elts_;   // list of crossed elements GeomElement* and outward side number

	MeshedGeodesic() : Geodesic() {type_=_meshedGeodesic; meshgeod_=this;}
	MeshedGeodesic(GeomDomain& dp, bool wCA=false, bool wT=false, bool wC=false)
	        : Geodesic(dp, wCA, wT, wC) {type_=_meshedGeodesic; meshgeod_=this;}
	virtual MeshedGeodesic* clone() const {return new MeshedGeodesic(*this);}
	virtual void clear();               //! clear xs_,dxs_,curAbcs
	virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0); //! compute geodesic from x, dx with curvilinear length lmax
	Geodesic& compute(Point& x, Point& d, GeomElement* gelt, real_t& l, real_t lmax, number_t& n, number_t nbp, real_t dt=0); //! effective computation
	GeomElement* locateStartingElement(Point& x, Point& dx, number_t& s) const;               //!< locate starting element for any starting pair (x,dx)
	GeomElement* locateElementFromVertex(Point& x, Point& dx, number_t& s, GeomElement* gelt0=nullptr) const; //!< locate next element when x is a vertex
	virtual string_t strtype() const {return "Meshed geodesic";}
	virtual void print(std::ostream&) const;
	virtual void saveToFile(const string_t& file, bool withElts=false) const;
};

//===========================================================================================
//! GeometricGeodesic class handling any geodesic described by a 1D geometry
//  geometry must have a 1D-parametrisation
//===========================================================================================
class GeometricGeodesic : public Geodesic
{
 protected:
    Geometry * geom_p;  //!< 1D geometry describing the geodesic
 public:
    GeometricGeodesic() : Geodesic(), geom_p(nullptr)  {type_=_geometricGeodesic;}  //! void constructor
    GeometricGeodesic(GeomDomain& dom, Geometry& geom, const Point& x, const Point& dx, real_t lmax, bool wCA=false, bool wT=false, bool wC=false); //!< explicit geodesic geometry given
    GeometricGeodesic(GeomDomain& dom, const Point& x, const Point& dx, real_t lmax, bool wCA=false, bool wT=false, bool wC=false); //!< build geodesic geometry from domain
	GeometricGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax, bool wCA=false, bool wT=false, bool wC=false);  //!< build geodesic geometry from geometry
	GeometricGeodesic(Geometry& geom, const Point& x, const Point& dx, real_t lmax, number_t nmax, real_t dl=0, bool wCA=false, bool wT=false, bool wC=false); //!< build geodesic geometry from geometry and compute
	~GeometricGeodesic();                                   //!< destructor
	GeometricGeodesic(const GeometricGeodesic&);            //!< copy constructor
    GeometricGeodesic& operator=(const GeometricGeodesic&); //!< assign operator
    void build(real_t lmax);                                //!< effective construction regarding shape
    void copy(const GeometricGeodesic&);                    //!< copy function
    virtual GeometricGeodesic* clone() const {return new GeometricGeodesic(*this);}
    virtual Geodesic& compute(Point& x, Point& dx,real_t& l,real_t lmax, number_t n, real_t dt=0); //! compute geodesic with curvilinear length lmax
    virtual string_t strtype() const {return "Geometric geodesic";}
    virtual void print(std::ostream&) const;
    virtual void saveToFile(const string_t& file) const
    {Geodesic::saveToFileG(file);}
};

bool hasGeometricGeodesic(const Geometry& geo);

//===========================================================================================
/*! AnalyticGeodesic class handling any geodesic described by a 1D->3D parametrization
      par_g  : Parametrization of the analytic geodesic
      init_p: function pointer to init function, must be called before using parametrization
      smin_, smax_ : bounds of parametrization regarding starting point
    the set() function set the starting point and starting derivative
    the init_p() function computes parameter bounds and additional data useful for computation
    the useful data must are transmitted to the parametrization function using its Parameters object
*/
//===========================================================================================
class AnalyticGeodesic : public Geodesic
{
 protected:
    Parametrization parg_;               //!< 1D->3D parametrization
    void (*init_p)(Parameters&);         //!< function pointer to init function called before using parametrization
 public:
    mutable real_t smin_,smax_; //!< bounds of parametrization
    AnalyticGeodesic() : Geodesic() {type_=_analyticGeodesic;}
	AnalyticGeodesic(GeomDomain& dp, const Parametrization& par, bool wCA=false, bool wT=false, bool wC=false)
      : Geodesic(dp,wCA,wT,wC), parg_(par), init_p(nullptr), smin_(0.), smax_(1.) {type_=_analyticGeodesic;}
	AnalyticGeodesic(const Parametrization& par, bool wCA=false, bool wT=false, bool wC=false);
	AnalyticGeodesic(const GeomDomain& dom,bool wCA=false, bool wT=false, bool wC=false);
	AnalyticGeodesic(const Geometry& geo,bool wCA=false, bool wT=false, bool wC=false);
	AnalyticGeodesic(const Geometry& geom, const Point& x, const Point& dx, real_t lmax,
                     number_t nbp, real_t dt=0, bool wCA=false, bool wT=false, bool wC=false);
	virtual AnalyticGeodesic* clone() const {return new AnalyticGeodesic(*this);}
	void build(const Geometry& geo,bool wCA=false, bool wT=false, bool wC=false);
    void set(const Point& x, const Point& dx);
    virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0); //! compute geodesic with curvilinear length lmax
    virtual string_t strtype() const {return "Analytic geodesic";}
    virtual void print(std::ostream&) const;
    virtual void saveToFile(const string_t& file) const
    {Geodesic::saveToFileG(file);}
};

bool hasAnalyticGeodesic(const Geometry& geo);
//cylinder side part stuff
Vector<real_t> cylinderSidePartGeodesic(const Point& P, Parameters& params, DiffOpType dif=_id);
Vector<real_t> invCylinderSidePartGeodesic(const Point& pt, Parameters& params, DiffOpType d=_id);
void initCylinderSidePartGeodesic(Parameters& params);
Vector<real_t> cylinderSidePartGeodesicCurvatures(const Point& p, const Point& d, bool fromParameters, Parameters& params);
Vector<real_t> cylinderSidePartGeodesicNormal(const Point& p, bool fromParameters, Parameters& params);

//===========================================================================================
/*! CompositeGeodesic class handling any geodesic described by a collection of Geodesics
      geodesics_   : list of pointers to Geodesic (each Geodesic may be of any type)
    Such structure can describe a geodesic by mixing different kind of geodesic,
    leading to a faster computation if GeometricGeodesic or AnalyticGeodesic are used
    NOTE: hard copy of Geodesics
*/
//===========================================================================================
class CompositeGeodesic : public Geodesic
{
 protected:
   std::list<Geodesic*> geodesics_;                  //!< list of pointers to Geodesic
 public:
   CompositeGeodesic(GeomDomain& dom,bool wCA=false, bool wT=false, bool wC=false)  //! constructor from a domain
   : Geodesic(dom,wCA,wT,wC){type_=_compositeGeodesic;}
   CompositeGeodesic(Geometry& geo,bool wCA=false, bool wT=false, bool wC=false)    //! constructor from a geometry
   : Geodesic(geo,wCA,wT,wC){type_=_compositeGeodesic;}
   CompositeGeodesic(bool wCA=false, bool wT=false, bool wC=false)                  //! "void" constructor
    {dom_p=nullptr; geo_p=nullptr;withTangent=wT; withCurAbc=wCA; withCurvatures=wC; type_=_compositeGeodesic;}
   CompositeGeodesic(const CompositeGeodesic& cg);                   //!< copy constructor
   ~CompositeGeodesic();
   CompositeGeodesic& operator=(const CompositeGeodesic& cg);        //!< assign operator
   void copy(const CompositeGeodesic& cg);                           //!< copy tool
   void clear();                                                     //!< clear tool
   virtual CompositeGeodesic* clone() const {return new CompositeGeodesic(*this);}  //! clone tool
   void add(const Geodesic& geod);                                   //!< add a Geodesic to the list
   virtual Geodesic& compute(Point& x, Point& dx, real_t& l, real_t lmax, number_t n, real_t dt=0); //!< compute geodesic at points
   virtual Vector<real_t> curvatures(const Point& p, bool fromParameters=false); //!< return principal curvatures if available
   virtual string_t strtype() const {return "Composite geodesic";}
   void virtual print(std::ostream&) const;
   void toGlobal();  //!< fill xs_, dxs_, curAbcs_ from local xs_, dxs_, curAbcs_
   void virtual saveToFile(const string_t& file) const{saveToFileG(file);}
};

//===========================================================================================
/*! EltSeg class handles a segment separating a geometric element gelt1 in 2 parts (interior segment)
    or separating element gelt1 and gelt2 (edge)
    P1_,P2_         : vertices of segment
    gelt1_, gelt2_  : pointers to the geometric elements concerned (gelt2=0 if interior ssegment)
    s1_,s2_         : side numbers where P1 (resp. P2) belongs to or side numbers of edge
*/
//===========================================================================================
class SegElt
{
  public:
    Point P1_, P2_;                  //!< vertices of segment
    GeomElement * gelt1_, *gelt2_;   //!< pointer to the geometric element
    number_t s1_, s2_ ;              //!< side numbers
    SegElt(const Point& P1, const Point& P2, GeomElement * g1, GeomElement * g2=nullptr, number_t s1=0, number_t s2=0)
    : P1_(P1), P2_(P2), gelt1_(g1), gelt2_(g2), s1_(s1), s2_(s2) {}
    SegElt& reverse();  // reverse point order
    void print(std::ostream&) const;
    friend std::ostream& operator<<(std::ostream& out, const SegElt& sg)
    {sg.print(out); return out;}
};


} // end of namespace xlifepp

#endif // GEODESIC_HPP_INCLUDED
