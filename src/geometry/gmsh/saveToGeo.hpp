/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file saveToGeo.hpp
  \authors N. Kielbasiewicz
  \since 12 oct 2015
  \date 12 oct 2015

  \brief Definition of utilities used for gmsh mesh generator of Mesh class

*/

#ifndef GMSH_UTILS_HPP
#define GMSH_UTILS_HPP

#include "config.h"
#include "../Geometry.hpp"
#include "../geometries1D.hpp"
#include "../geometries2D.hpp"
#include "../geometries3D.hpp"
#include "utils.h"

namespace xlifepp
{

/*!
  \class PhysicalData
  store data related to a physical group: domain name, domain id and dimension
 */

class PhysicalData
{
  public:
    string_t domName;      //!< name of the physical group
    number_t id;           //!< id of the physical group
    dimen_t dim;           //!< dimension of the physical group
    //! default constructor
    PhysicalData() : domName("Omega"), id(1), dim(3) {}
    //! constructor
    PhysicalData(string_t name, number_t i, dimen_t d) : domName(name), id(i), dim(d) {}
    bool operator<(const PhysicalData& cd) const { return id < cd.id; } //!< comparison operator
    void print(std::ostream&) const;                                    //!< print mesh data
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const PhysicalData&);
};

std::ostream& operator<<(std::ostream&, const PhysicalData&); //!< print operator

//! finds a physical data in a list
short int findId(std::vector<PhysicalData>::const_iterator it_b, std::vector<PhysicalData>::const_iterator it_e, number_t id);
//! finds a physical data in a list
short int findString(std::vector<PhysicalData>::const_iterator it_b, std::vector<PhysicalData>::const_iterator it_e, string_t name);

//! create geo Physical domain string from sidenames and type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomain(const std::vector<string_t>& sidenames, const string_t& sid, std::vector<PhysicalData>& pids);
//! create geo Physical domain string from sidenames and type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomain(const std::vector<string_t>& sidenames, const Strings& sids, std::vector<PhysicalData>& pids);
//! create geo Physical domain string from domain name, surface id, and dimension
string_t physicalDomain(const string_t& domName, const string_t& sid, dimen_t dim, std::vector<PhysicalData>& pids);
//! create geo Physical domain string from domain name, surface ids, and dimension
string_t physicalDomain(const string_t& domName, const Strings& sids, dimen_t dim, std::vector<PhysicalData>& pids);
//! create geo Physical domain string from sidenames and the type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomains(const std::vector<PhysicalData>& pids);
//! create geo Physical domain string from sidenames and the type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomainForExtrusion(const Geometry& g, const std::vector<string_t>& sidenames,
                                    const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent,
                                    const string_t& sd, const std::map<string_t,Strings>& inputs, std::vector<PhysicalData>& pids,
                                    real_t angle = 0.);
//! writing a component of composite geometry in a geo file whatever the dimension
void saveComponentToGeo(Geometry& g, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                        bool withLoopsStorage = true, bool withSideNames = true);
//! writing the surface/volume definition (Plane Surface or Volume command in geo)
void saveComponentExtraDataToGeo(const Geometry& g, number_t nloops, std::ofstream& fout);
//! writing the borders numbers of a canonicl geometry in a geo file
Strings getComponentBordersToGeo(const Geometry& g);
//! writing an extrusion geometry in a geo file whatever the dimension
std::vector<std::pair<number_t, number_t> > saveExtrusionGeometryToGeo(const Geometry& g, ShapeType sh, real_t angle,
                                                                       std::ofstream& fout, std::vector<PhysicalData>& pids,
                                                                       bool withLoopsStorage = true, bool withSideNames = true);
//! writing extrusion inputs as string
string_t saveExtrusionInputsAsString(const std::map<string_t,Strings>& inputs);
//! writing an extrusion in a geo file
void saveExtrusionComponentToGeo(const Geometry& g, const Transformation& t, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs);
//! writing an extrusion by Translation in a geo file
void saveExtByTranslationToGeo(const Geometry& g, const Translation& t, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs);
//! writing an extrusion by Rotation2d in a geo file
void saveExtByRotation2dToGeo(const Geometry& g, const Rotation2d& r, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs);
//! writing an extrusion by Rotation3d in a geo file
void saveExtByRotation3dToGeo(const Geometry& g, const Rotation3d& r, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs);
//! writing an extrusion by a composition of translations and rotations in a geo file
void saveExtByCompositionToGeo(const Geometry& g, const Transformation& t, std::ofstream& fout, const std::map<string_t,Strings>& inputs);
//! writing sidenames of an extrusion in a geo file
void saveExtrusionSideNamesToGeo(const Geometry& g, const std::vector<string_t>& sidenames,
                                 const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent,
                                 const Transformation& t, std::ofstream& fout, const std::map<string_t,Strings>& inputs,
                                 std::vector<PhysicalData>& pids);
//! writing sidenames of an extrusion by Translation in a geo file
void saveExtByTranslationSideNamesToGeo(const Geometry& g, const std::vector<string_t>& sidenames,
                                        const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent, const Translation& t, std::ofstream& fout, const std::map<string_t,Strings>& inputs,
                                        std::vector<PhysicalData>& pids);
//! writing sidenames of an extrusion by Rotation2d or Rotation3d in a geo file
void saveExtByRotationSideNamesToGeo(const Geometry& g, const std::vector<string_t>& sidenames,
                                     const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent, real_t angle,
                                     std::ofstream& fout, const std::map<string_t,Strings>& inputs, std::vector<PhysicalData>& pids);
//! writing a segment in a geo file
void saveSegmentToGeo(const Segment& s, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage = true, bool withSideNames = true);
//! writing an elliptic arc in a geo file
void saveEllArcToGeo(const EllArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                     bool withLoopsStorage = true, bool withSideNames = true);
//! writing a circle arc in a geo file
void saveCircArcToGeo(const CircArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage = true, bool withSideNames = true);
//! writing a parametrized arc in a geo file
void saveParametrizedArcToGeo(const ParametrizedArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage = true, bool withSideNames = true);
//! writing a parametrized arc in a geo file
void saveSplineArcToGeo(const SplineArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage = true, bool withSideNames = true);
//! writing a polygon in a geo file
void savePolygonToGeo(const Polygon& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage = true, bool withSideNames = true);
//! writing a triangle in a geo file
void saveTriangleToGeo(const Triangle& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                       bool withLoopsStorage = true, bool withSideNames = true);
//! writing a quadrangle, a rectangle or a square in a geo file
void saveQuadrangleToGeo(const Quadrangle& q, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage = true, bool withSideNames = true);
//! writing an elliptic surface or a disk in a geo file
void saveEllipseToGeo(const Ellipse& e, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage = true, bool withSideNames = true);
//! writing a parametrized surface in a geo file
void saveParametrizedSurfaceToGeo(const ParametrizedSurface& s, ShapeType sh, std::ofstream& fout,
                                  std::vector<PhysicalData>& pids, bool withLoopsStorage=true, bool withSideNames=true);
//! writing a polyhedron in a geo file
void savePolyhedronToGeo(const Polyhedron& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage = true, bool withSideNames = true);
//! writing a tetrahedron in a geo file
void saveTetrahedronToGeo(const Tetrahedron& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                          bool withLoopsStorage = true, bool withSideNames = true);
//! writing a hexahedron in a geo file
void saveHexahedronToGeo(const Hexahedron& h, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage = true, bool withSideNames = true);
//! writing a parallelepiped, a cuboid or a cube in a geo file
void saveParallelepipedToGeo(const Parallelepiped& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage = true, bool withSideNames = true);
//! writing an ellipsoidal volume or a ball in a geo file
void saveEllipsoidToGeo(const Ellipsoid& e, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                        bool withLoopsStorage = true, bool withSideNames = true);
//! writing a trunk in a geo file
void saveTrunkToGeo(const Trunk& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                    bool withLoopsStorage = true, bool withSideNames = true);
//! writing a cylindrical volume in a geo file
void saveCylinderToGeo(const Cylinder& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                       bool withLoopsStorage = true, bool withSideNames = true);
//! writing a cone in a geo file
void saveConeToGeo(const Cone& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                   bool withLoopsStorage = true, bool withSideNames = true);
//! writing a revolution trunk in a geo file
void saveRevTrunkToGeo(const RevTrunk& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                       bool withLoopsStorage = true, bool withSideNames = true);
//! writing a revolution cylindrical volume in a geo file
void saveRevCylinderToGeo(const RevCylinder& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                          bool withLoopsStorage = true, bool withSideNames = true);
//! writing a revolution cone in a geo file
void saveRevConeToGeo(const RevCone& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage = true, bool withSideNames = true);

} // end of namespace xlifepp

#endif // GMSH_UTILS_HPP
