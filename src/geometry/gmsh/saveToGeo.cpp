/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file saveToGeo.cpp
  \authors N. Kielbasiewicz
  \since 16 jun 2014
  \date 23 jun 2018

  \brief Implementation of utilities used for gmsh mesh generator of Mesh class
*/

#include "../Mesh.hpp"
#include "saveToGeo.hpp"
#include <sstream>

namespace xlifepp
{

void PhysicalData::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) { return; }
  os << "Physical domain: id " << id << ", name " << domName << ", dim " << dim << std::endl;
}

std::ostream& operator<<(std::ostream& os, const PhysicalData& pid)
{
  pid.print(os);
  return os;
}

short int findId(std::vector<PhysicalData>::const_iterator it_b, std::vector<PhysicalData>::const_iterator it_e, number_t id)
{
  std::vector<PhysicalData>::const_iterator it_cd;
  number_t i=0;
  for (it_cd=it_b; it_cd != it_e; ++it_cd, ++i)
  {
    if (it_cd->id == id) { return i; }
  }
  return -1;
}

short int findString(std::vector<PhysicalData>::const_iterator it_b, std::vector<PhysicalData>::const_iterator it_e, string_t name)
{
  std::vector<PhysicalData>::const_iterator it_cd;
  number_t i=0;
  for (it_cd=it_b; it_cd != it_e; ++it_cd, ++i)
  {
    if (it_cd->domName == name) { return i; }
  }
  return -1;
}

//! create geo Physical domain string from sidenames and the type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomain(const std::vector<string_t>& sidenames, const string_t& sid, std::vector<PhysicalData>& pids)
{
  string_t ps;
  std::map<string_t, std::vector<string_t> > sn;
  std::map<string_t, std::vector<string_t> >::iterator itm;
  short int itpd;

  for (number_t i=0; i < sidenames.size(); ++i)
  {
    string_t s = sidenames[i];
    if (s != "")
    {
      // itm=sn.find(s);
      sn[s].push_back(sid + "_" + tostring(i+1));
    }
  }
  string_t dom = "Point";
  dimen_t dim=0;
  if ( sid == "L" || sid == "E" ) { dom = "Line"; dim=1; }
  if ( sid == "S" ) { dom = "Surface"; dim=2; }
  for (itm=sn.begin(); itm!=sn.end(); ++itm)
  {
    itpd=findString(pids.begin(), pids.end(), itm->first);
    if (itpd == -1) // side domain is not already defined
    {
      number_t phyIdSize=pids.size();
      pids.push_back(PhysicalData(itm->first,phyIdSize+1,dim));
      ps += "domain_" + tostring(phyIdSize+1) + "={" + itm->second[0] + "[]";
      for (number_t i=1; i < itm->second.size(); ++i)
      { ps += "," + itm->second[i] + "[]"; }
      ps += "};\n";
    }
    else // side domain is already defined
    {
      for (number_t i=0; i < itm->second.size(); ++i)
      {
        ps += "If (built" + itm->second[i] + " == undefLoop)\n";
        ps += "  domain_" + tostring(pids[itpd].id) + "[]={domain_" + tostring(pids[itpd].id) + "[], " + itm->second[i]  + "[]};\n";
        ps += "EndIf\n";
      }
    }
  }
  return ps;
}

//! create geo Physical domain string from sidenames and the type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomain(const std::vector<string_t>& sidenames, const Strings& sids, std::vector<PhysicalData>& pids)
{
  string_t ps;
  std::map<string_t, std::vector<string_t> > sn;
  std::map<string_t, std::vector<string_t> >::iterator itm;
  short int itpd;

  for (number_t i=0; i < sidenames.size(); ++i)
  {
    string_t s = sidenames[i];
    if (s != "")
    {
      // itm=sn.find(s);
      sn[s].push_back(sids[i] + "_" + tostring(i+1));
    }
  }
  number_t i=0;
  for (itm=sn.begin(); itm!=sn.end(); ++itm, ++i)
  {
    string_t dom = "Point";
    dimen_t dim=0;
    if ( sids[i] == "L" || sids[i] == "E" ) { dom = "Line"; dim=1; }
    if ( sids[i] == "S" ) { dom = "Surface"; dim=2; }
    itpd=findString(pids.begin(), pids.end(), itm->first);
    if (itpd == -1) // side domain is not already defined
    {
      number_t phyIdSize=pids.size();
      pids.push_back(PhysicalData(itm->first,phyIdSize+1,dim));
      ps += "domain_" + tostring(phyIdSize+1) + "={" + itm->second[0] + "[]";
      for (number_t i=1; i < itm->second.size(); ++i)
      { ps += "," + itm->second[i] + "[]"; }
      ps += "};\n";
    }
    else // side domain is already defined
    {
      for (number_t i=0; i < itm->second.size(); ++i)
      {
        ps += "If (built" + itm->second[i] + " == undefLoop)\n";
        ps += "  domain_" + tostring(pids[itpd].id) + "[]={domain_" + tostring(pids[itpd].id) + "[], " + itm->second[i]  + "[]};\n";
        ps += "EndIf\n";
      }
    }
  }
  return ps;
}

//! create geo Physical domain string from sidenames and the type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomain(const string_t& domName, const string_t& sid, dimen_t dim, std::vector<PhysicalData>& pids)
{
  string_t ps;
  std::map<string_t, std::vector<string_t> >::iterator itm;
  short int itpd;
  string_t dom = "Point";

  if (dim == 1) { dom = "Line"; }
  if (dim == 2) { dom = "Surface"; }
  if (dim == 3) { dom = "Volume"; }

  itpd=findString(pids.begin(), pids.end(), domName);
  if (itpd == -1) // domain domain is not already defined
  {
    number_t phyIdSize=pids.size();
    pids.push_back(PhysicalData(domName,phyIdSize+1,dim));
    ps += "domain_" + tostring(phyIdSize+1) + "={" + sid + "};\n";
  }
  else // domain domain is already defined
  {
    ps += "If (built" + sid +" == undefLoop)\n";
    ps += "  domain_" + tostring(pids[itpd].id) + "={domain_" + tostring(pids[itpd].id) + "[]," + sid + "};\n";
    ps += "EndIf\n";
  }
  return ps;
}

//! create geo Physical domain string from sidenames and the type of side domain sd = "P" or "L" or "E" or "S"
string_t physicalDomain(const string_t& domName, const Strings& sids, dimen_t dim, std::vector<PhysicalData>& pids)
{
  string_t ps;
  short int itpd;
  string_t dom = "Point";

  if (dim == 1) { dom = "Line"; }
  if (dim == 2) { dom = "Surface"; }
  if (dim == 3) { dom = "Volume"; }

  itpd=findString(pids.begin(), pids.end(), domName);
  if (itpd == -1)  // domain domain is not already defined
  {
    number_t phyIdSize=pids.size();
    pids.push_back(PhysicalData(domName,phyIdSize+1,dim));
    ps += "domain_" + tostring(phyIdSize+1) + "={" + sids[0];
    for (number_t i=1; i < sids.size(); ++i) { ps += "," + sids[i]; }
    ps += "};\n";
  }
  else // domain domain is already defined
  {
    for (number_t i=0; i < sids.size(); ++i)
    {
      ps += "If (built" + sids[i] + " == undefLoop)\n";
      ps += "  domain_" + tostring(pids[itpd].id) + "={domain_" + tostring(pids[itpd].id) + "[]," + sids[i] + "[]};\n";
      ps += "EndIf\n";
    }
  }
  return ps;
}

string_t physicalDomains(const std::vector<PhysicalData>& pids)
{
  string_t ps;
  for (number_t i=0; i < pids.size(); ++i)
  {
    string_t dom="Point";
    dimen_t dim=pids[i].dim;
    if (dim == 1) { dom = "Line"; }
    if (dim == 2) { dom = "Surface"; }
    if (dim == 3) { dom = "Volume"; }
    ps += "Physical " + dom + "(\"" + pids[i].domName + "\")= domain_" + tostring(pids[i].id) + "[];\n";
  }
  return ps;
}

//! create geo Physical domain string from sidenames and the type of side domain sd = "P" or "L" or "E" or "S" for extrusion geometries
string_t physicalDomainForExtrusion(const Geometry& g, const std::vector<string_t>& sidenames,
                                    const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent, const string_t& sd,
                                    const std::map<string_t,Strings>& inputs, std::vector<PhysicalData>& pids, real_t angle)
{
  string_t ps;
  std::map<string_t, std::vector<string_t> > sn;
  std::map<string_t, std::vector<string_t> >::iterator itm;
  short int itpd;
  number_t dim=0;
  string_t dom = "Point";
  if ( sd == "L" || sd == "E" ) { dom = "Line"; dim=1; }
  if ( sd == "S" ) { dom = "Surface"; dim=2; }
  if (angle < pi_ - theEpsilon)
  {
    // we have to build the numbering map between sidenames and out[] arrays in geo scripts
    number_t nbSurfs=nbSidesPerComponent.size();
    number_t nbLateralSurfs=g.nbLateralSidesForExtrusion();
    std::vector<number_t> renumbering(sidenames.size());
    for (number_t i=0; i < nbSurfs; ++i)
    {
      renumbering[i]=i;
      if (i==0) { renumbering[i+nbSurfs]=0; }
      else { renumbering[i+nbSurfs]=renumbering[i-1+nbSurfs]+2+nbSidesPerComponent[i-1].second; }
    }
    for (number_t i=2*nbSurfs; i < sidenames.size(); ++i) { renumbering[i]=i-2*nbSurfs+2; }

    for (number_t i=0; i < sidenames.size(); ++i)
    {
      string_t s = sidenames[i];
      if (s != "")
      {
        string_t keyword;
        // we have to switch according to dimension and rotation angle
        if (dim == 2) { keyword="Surface"; }
        else { keyword="Line"; }

        if (i < nbSidesPerComponent.size())
        {
          sn[s].push_back(inputs.at(keyword)[renumbering[i]]);
        }
        else if (i < 2*nbSidesPerComponent.size())
        {
          sn[s].push_back(string_t("out[")+tostring(renumbering[i])+string_t("]"));
        }
        else
        {
          if (angle >= pi_) { sn[s].push_back(string_t("outa[") + tostring(renumbering[i]) + string_t("]"));}
          sn[s].push_back(string_t("out[") + tostring(renumbering[i]) + string_t("]"));
        }
      }
    }
  }
  else
  {
    // we have to build the numbering map between sidenames and out[] arrays in geo scripts
    number_t nbSurfs=nbSidesPerComponent.size();
    number_t nbLateralSurfs=g.nbLateralSidesForExtrusion();
    if (std::abs(angle - 2.*pi_) < theEpsilon) { nbLateralSurfs/=4; }
    else { nbLateralSurfs/=2; }
    std::vector<number_t> renumbering(sidenames.size());
    for (number_t i=0; i < nbSurfs; ++i)
    {
      renumbering[i]=i;
      if (i==0) { renumbering[i+nbSurfs]=0; }
      else { renumbering[i+nbSurfs]=renumbering[i-1+nbSurfs]+2+nbSidesPerComponent[i-1].second; }
    }
    if (angle < 2.*pi_ - theEpsilon)
    {
      for (number_t i=0; i < nbLateralSurfs; ++i)
      {
        renumbering[i+2*nbSurfs]=i+2;
        renumbering[i+2*nbSurfs+nbLateralSurfs]=i+2;
      }
    }
    else
    {
      for (number_t i=0; i < nbLateralSurfs; ++i)
      {
        renumbering[i]=i+2;
        renumbering[i+nbLateralSurfs]=i+2;
        renumbering[i+2*nbLateralSurfs]=i+2;
        renumbering[i+3*nbLateralSurfs]=i+2;
      }
    }

    for (number_t i=0; i < sidenames.size(); ++i)
    {
      string_t s = sidenames[i];
      if (s != "")
      {
        string_t keyword;
        // we have to switch according to dimension and rotation angle
        if (dim == 2) { keyword="Surface"; }
        else { keyword="Line"; }

        if (std::abs(angle - 2.*pi_) < theEpsilon)
        {
          if (i < nbLateralSurfs)
          {
            sn[s].push_back(string_t("out[") + tostring(renumbering[i]) + string_t("]"));
          }
          else if (i < 2*nbLateralSurfs)
          {
            sn[s].push_back(string_t("outa[") + tostring(renumbering[i]) + string_t("]"));
          }
          else if (i < 3*nbLateralSurfs)
          {
            sn[s].push_back(string_t("outb[") + tostring(renumbering[i]) + string_t("]"));
          }
          else
          {
            sn[s].push_back(string_t("outc[") + tostring(renumbering[i]) + string_t("]"));
          }
        }
        else
        {
          if (i < nbSurfs)
          {
            sn[s].push_back(inputs.at(keyword)[i]);
          }
          else if (i < 2*nbSurfs+nbLateralSurfs)
          {
            sn[s].push_back(string_t("out[") + tostring(renumbering[i]) + string_t("]"));
          }
          else
          {
            sn[s].push_back(string_t("outa[") + tostring(renumbering[i]) + string_t("]"));
          }
        }
      }
    }
  }

  for (itm=sn.begin(); itm!=sn.end(); ++itm)
  {
    itpd=findString(pids.begin(), pids.end(), itm->first);
    if (itpd == -1)  // side domain is not already defined
    {
      number_t phyIdSize=pids.size();
      pids.push_back(PhysicalData(itm->first,phyIdSize+1,dim));
      ps += "domain_" + tostring(phyIdSize+1) + "={" + itm->second[0];
      for (number_t i=1; i < itm->second.size(); ++i)
      { ps += "," + itm->second[i]; }
      ps += "};\n";
    }
    else // side domain is already defined,
    {
      for (number_t i=0; i < itm->second.size(); ++i)
      {
        ps += "If (built" + itm->second[i] + " == undefLoop)\n";
        ps += "  domain_" + tostring(pids[itpd].id) + "={domain_ " + tostring(pids[itpd].id) + "[]," + itm->second[i]  + "[]};\n";
      }
      ps += "EndIf\n";
    }
  }
  return ps;
}

//! writing a 1D geometry in a geo file
void saveToGeo(Geometry& g, MeshData& meshData, number_t order, const string_t& filename)
{
  std::ofstream fout(filename.c_str());
  if (isTestMode) { fout.precision(testPrec); }
  else { fout.precision(fullPrec); }

  number_t nloops=0;
  fout << "Include \""+theEnvironment_p->geoMacroFilePath()+"xlifepp_macros.geo\";" << std::endl << std::endl;
  fout << "Mesh.MeshSizeMin = " << meshData.hmin() << ";" << eol;
  fout << "Mesh.MeshSizeMax = " << meshData.hmax() << ";" << eol;
  fout << "h0=" << theDefaultCharacteristicLength << ";" << std::endl;

  std::vector<PhysicalData> pids;

  string_t gname = g.domName();
  switch (g.shape())
  {
    case _segment:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveSegmentToGeo(*g.segment(), _segment, fout, pids);
      if (gname == "")
      {
        string_t spd = physicalDomain("Omega", "L_1", 1, pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      else
      {
        string_t spd = physicalDomain(gname, "L_1", 1, pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      break;
    }
    case _ellArc:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveEllArcToGeo(*g.ellArc(), _segment, fout, pids);
      if (gname == "")
      {
        string_t spd = physicalDomain("Omega", "E_1", 1, pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      else
      {
        string_t spd = physicalDomain(gname, "E_1", 1, pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      break;
    }
    case _circArc:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveCircArcToGeo(*g.circArc(), _segment, fout, pids);
      if (gname == "")
      {
        string_t spd = physicalDomain("Omega", "E_1", 1, pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      else
      {
        string_t spd = physicalDomain(gname, "E_1", 1, pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      break;
    }
    case _parametrizedArc:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveParametrizedArcToGeo(*g.parametrizedArc(), _parametrizedArc, fout, pids);
      string_t spd;
      if(g.parametrizedArc()->partitioning()==_splinePartition)
      {
        if (gname == "") spd=physicalDomain("Omega", "S_1", 1, pids);
        else spd = physicalDomain(gname, "S_1", 1, pids);
        if (spd != "")  { fout << spd << std::endl; }
      }
      else
      {
        Strings sL(g.parametrizedArc()->nbParts());
        for (number_t i=0; i < sL.size(); ++i)  sL[i]="L_" + tostring(i+1);
        if (gname == "") spd = physicalDomain("Omega", sL, 1, pids);
        else spd = physicalDomain(gname, sL, 1, pids);
        if (spd != "")  { fout << spd << std::endl; }
      }
      break;
    }
    case _splineArc:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveSplineArcToGeo(*g.splineArc(), _splineArc, fout, pids);
      if (gname == "")
      {
        string_t spd = physicalDomain("Omega", "S_1", 1, pids);
        if (spd != "")  { fout << spd << std::endl; }
      }
      else
      {
        string_t spd = physicalDomain(gname, "S_1", 1, pids);
        if (spd != "")  { fout << spd << std::endl; }
      }
      break;
    }
    case _composite:
    {
      std::map<number_t, std::vector<number_t> >::const_iterator it_g;
      std::map<number_t, Geometry*> gc=g.components();              //why not a reference ?
      std::map<number_t, Geometry*>::const_iterator it,it2;
      std::map<number_t, std::vector<number_t> > gg=g.geometries(); //why not a reference ?
      std::map<string_t, std::vector<number_t> > gnames;
      std::map<string_t, std::vector<number_t> >::const_iterator it_n;
      std::vector<const Point*> geoPoints;
      std::vector<const Point*>::const_iterator it3;
      std::vector<number_t> geoPtPointers;
      std::vector<number_t> geoPtIndices;
      std::vector<std::pair<ShapeType,std::vector<const Point*> > > geoCurves;
      std::vector<std::pair<ShapeType,std::vector<const Point*> > >::iterator it4;
      std::vector<int_t> geoCurvePointers, geoCurveIndices;
      bool physicalGroupDefined=false;
      number_t ptrp=0, ptrc=0;
      int_t i;
      for (it=gc.begin(); it!=gc.end(); ++it)
      {
        geoPtPointers.push_back(ptrp);
        geoCurvePointers.push_back(ptrc);
        bool ptExists = false;

        std::vector<const Point*> nodes = it->second->boundNodes();
        std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves;
        curves=it->second->curves();

        ptrp+=nodes.size();
        ptrc+=curves.size();

        for (number_t k=0; k<nodes.size(); k++)
        {
          i=0;
          for (it3=geoPoints.begin(); it3 != geoPoints.end() && ptExists == false; it3++,i++)
          {
            if (force3D(*(nodes[k])) == force3D(**it3))
            {
              ptExists=true;
              geoPtIndices.push_back(i);
            }
          }
          if (!ptExists) { geoPtIndices.push_back(geoPtIndices.size()); }
          geoPoints.push_back(nodes[k]);
          ptExists=false;
        }

        for (number_t j=0; j< curves.size(); j++)
        {
          i=findBorder(curves[j],geoCurves);
          if (i != -1)
          {
            if (sameOrientation(curves[j].second,geoCurves[i].second) == false) { geoCurveIndices.push_back(-i); }
            else { geoCurveIndices.push_back(i); }
          }
          else { geoCurveIndices.push_back(geoCurveIndices.size()); }
          geoCurves.push_back(curves[j]);
        }
      }

      geoPtPointers.push_back(ptrp);
      geoCurvePointers.push_back(ptrc);

      for (it=gc.begin(); it!= gc.end(); it++)
      {
        fout << "Call xlifepp_init;" << std::endl;
        fout << "l=" << it->first << ";" << std::endl;
        i=1;
        bool addSpace=false;
        for (number_t j=geoPtPointers[it->first]; j<geoPtPointers[it->first+1]; j++,i++)
        {
          switch (it->second->shape())
          {
            case _ellArc:
            {
              if (i==2) i=4;
              break;
            }
            case _circArc:
            {
              if (i==2) i=3;
              break;
            }
            case _splineArc:
            {
              if(i==2) i=it->second->lastNodeIndex();
              break;
            }
            default: break;
          }
          if (geoPtIndices[j] < j)
          {
            if (addSpace) { fout << " "; }
            fout << "builtP_" << i << "=points_" << geoPtIndices[j] << ";";
            addSpace=true;
          }
        }

        if (addSpace) { fout << std::endl; }

        saveComponentToGeo(*it->second, _segment, fout, pids);
      }

      fout << std::endl;

      // management of domain names
      for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
      {
        if (gc[it_g->first]->domName() != "" && gc[it_g->first]->shape() != _loop)
          { gnames[gc[it_g->first]->domName()].push_back(it_g->first); physicalGroupDefined=true; }
      }

      for (it_n=gnames.begin(); it_n != gnames.end(); ++it_n)
      {
        if (it_n->second.size() == 1)
        {
          std::stringstream ssargs;
          ssargs << "loops_";
          number_t i=0;
          for ( ; i < it_n->second.size()-1; ++i) { ssargs << it_n->second[i] << "[],loops_"; }
          ssargs << it_n->second[i] << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(it_n->first, args, 1, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          Strings args(it_n->second.size());
          for (number_t i=0; i < it_n->second.size(); ++i) { args[i] = "loops_"+tostring(it_n->second[i])+"[]"; }
          string_t spd = physicalDomain(it_n->first, args, 1, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }

      if (!physicalGroupDefined)
      {
        Strings args;
        for (it_g=gg.begin(); it_g != gg.end(); it_g++)
        {
          std::stringstream ssargs;
          ssargs << "loops_" << it_g->first << "[]";
          args.push_back(ssargs.str());
        }
        string_t spd = physicalDomain("Omega", args, 1, pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      break;
    }
    default:
    {
      error("gmsh_shape_not_handled", words("shape",g.shape()), gname);
      break;
    }
  }

  // management of domain names
  string_t spd = physicalDomains(pids);
  if (spd != "") { fout << spd << std::endl; }

  fout << "Mesh.ElementOrder=" << order << ";" << std::endl;

  std::vector<string_t> gmshVersionNumbers=split(GMSH_VERSION,'.');
  if (stringto<int>(gmshVersionNumbers[0]) >= 4)
  {
    fout << "Mesh.MshFileVersion = 2.2;" << std::endl;
  }
  fout.close();
}

//! writing a 2D/3D geometry in a geo file
void saveToGeo(Geometry& g, MeshData& meshData, ShapeType sh, number_t order, MeshPattern pattern, StructuredMeshSplitRule splitDirection, const string_t& filename, std::set<CrackData>& cracks)
{
  std::ofstream fout(filename.c_str());
  if (isTestMode) { fout.precision(testPrec); }
  else { fout.precision(fullPrec); }

  fout << "Include \""+theEnvironment_p->geoMacroFilePath()+"xlifepp_macros.geo\";" << std::endl << std::endl;
  fout << "Mesh.MeshSizeMin = " << meshData.hmin() << ";" << eol;
  fout << "Mesh.MeshSizeMax = " << meshData.hmax() << ";" << eol;
  fout << "h0=" << theDefaultCharacteristicLength << ";" << std::endl;

  number_t nloops=0;
  string_t gname = g.domName();
  std::vector<PhysicalData> pids;

  bool buildSurfaces=true, buildVolumes=true;
  // check element shape type
  switch (sh)
  {
    case _segment: buildSurfaces=false;
    case _triangle:
    case _quadrangle: buildVolumes=false;
    case _tetrahedron:
    case _hexahedron: break;
    default: error("shape_not_handled", words("shape",sh));
  }

  switch (g.shape())
  {
    case _polygon:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      savePolygonToGeo(*g.polygon(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.polygon()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.polygon()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _triangle:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveTriangleToGeo(*g.triangle(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.triangle()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.triangle()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _quadrangle:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveQuadrangleToGeo(*g.quadrangle(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.quadrangle()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.quadrangle()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _parallelogram:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveQuadrangleToGeo(*g.parallelogram(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.parallelogram()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.parallelogram()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _rectangle:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveQuadrangleToGeo(*g.rectangle(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.rectangle()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.rectangle()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _square:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveQuadrangleToGeo(*g.square(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.square()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.square()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _ellipse:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveEllipseToGeo(*g.ellipse(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.ellipse()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.ellipse()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _disk:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveEllipseToGeo(*g.disk(), sh, fout, pids);
      if (buildSurfaces)
      {
        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 2, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.disk()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.disk()), 1, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _parametrizedSurface:
    {
      fout << "Call xlifepp_init;" << std::endl;
      saveParametrizedSurfaceToGeo(*g.parametrizedSurface(), sh, fout, pids);
      // string_t args="loops_" +tostring(nloops)+"[]", spd;
      // if (gname == "") spd = physicalDomain("Omega", args, 2, pids);
      // else spd = physicalDomain(gname, args, 2, pids);
      // if (spd != "") { fout << spd << std::endl;}
      break;
    }
    case _polyhedron:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;

      savePolyhedronToGeo(*g.polyhedron(), sh, fout, pids);

      std::vector<Polygon*> gc=g.polyhedron()->faces();
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.polyhedron()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.polyhedron()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _tetrahedron:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveTetrahedronToGeo(*g.tetrahedron(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.tetrahedron()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.tetrahedron()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _hexahedron:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveHexahedronToGeo(*g.hexahedron(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.hexahedron()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.hexahedron()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _parallelepiped:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveParallelepipedToGeo(*g.parallelepiped(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.parallelepiped()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.parallelepiped()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _cuboid:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveParallelepipedToGeo(*g.cuboid(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.cuboid()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.cuboid()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _cube:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveParallelepipedToGeo(*g.cube(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.cube()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.cube()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _ellipsoid:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveEllipsoidToGeo(*g.ellipsoid(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.ellipsoid()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.ellipsoid()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _ball:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveEllipsoidToGeo(*g.ball(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.ball()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.ball()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _trunk:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveTrunkToGeo(*g.trunk(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.trunk()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.trunk()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _cylinder:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveCylinderToGeo(*g.cylinder(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.cylinder()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.cylinder()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _prism:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveCylinderToGeo(*g.prism(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.prism()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.prism()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _cone:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveConeToGeo(*g.cone(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.cone()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.cone()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _pyramid:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveConeToGeo(*g.pyramid(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.pyramid()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.pyramid()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _revTrunk:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveRevTrunkToGeo(*g.revTrunk(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.revTrunk()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.revTrunk()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _revCylinder:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveRevCylinderToGeo(*g.revCylinder(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.revCylinder()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.revCylinder()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _revCone:
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << nloops << ";" << std::endl;
      saveRevConeToGeo(*g.revCone(), sh, fout, pids);
      if (buildVolumes)
      {
        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl;
        if (gname == "")
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain("Omega", args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
        else
        {
          std::stringstream ssargs;
          ssargs << "loops_" << nloops << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(gname, args, 3, pids);
          if (spd != "") { fout << spd << std::endl; }
        }
      }
      else
      {
        if (pids.size() == 0) //! sidenames were not defined
        {
          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", getComponentBordersToGeo(*g.revCone()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
          else
          {
            string_t spd = physicalDomain(gname, getComponentBordersToGeo(*g.revCone()), 2, pids);
            if (spd != "") { fout << spd << std::endl; }
          }
        }
      }
      break;
    }
    case _extrusion:
    {
      // we have to get the angle when extrusion by rotation
      real_t angle=0.;
      const ExtrusionData* extrData=g.extrusionData();
      switch (extrData->extrusion()->transformType())
      {
        case _translation: break;
        case _rotation2d: angle=extrData->extrusion()->rotation2d()->angle(); break;
        case _rotation3d: angle=extrData->extrusion()->rotation3d()->angle(); break;
        default:
          where("saveToGeo(...)");
          error("gmsh_extrusion_not_handled", words("transform",extrData->extrusion()->transformType()));
      }

      std::vector<std::pair<number_t, number_t> > nbSidesPerComponent = saveExtrusionGeometryToGeo(g, sh, angle, fout, pids, true, true);

      // we have to determine indices of volumes
      number_t nbDomains=nbSidesPerComponent.size();
      std::vector<number_t> domainNumbers(nbDomains);
      domainNumbers[0]=1;
      for (number_t i=0; i < nbDomains-1; ++i) { domainNumbers[i+1]=domainNumbers[i]+nbSidesPerComponent[i].second+2; }

      if (g.dim() == 2)
      {
        if (buildSurfaces)
        {
          if (gname == "")
          {
            if (domainNumbers.size() == 1)
            {
              if (angle >= pi_)
              {
                string_t spd = physicalDomain("Omega", "outa[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              if (angle == 2.*pi_)
              {
                string_t spd = physicalDomain("Omega", "outb[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
                spd = physicalDomain("Omega", "outc[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              string_t spd = physicalDomain("Omega", "out[1]", 2, pids);
              if (spd != "") { fout << spd << std::endl; }
            }
            else
            {
              for (number_t i=0; i < domainNumbers.size(); ++i)
              {
                if (angle >= pi_)
                {
                  string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outa[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                if (angle == 2.*pi_)
                {
                  string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outb[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                  spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outc[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
            }
          }
          else
          {
            if (domainNumbers.size() == 1)
            {
              if (angle >= pi_)
              {
                string_t spd = physicalDomain(gname, "outa[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              if (angle == 2.*pi_)
              {
                string_t spd = physicalDomain(gname, "outb[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
                spd = physicalDomain(gname, "outc[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              string_t spd = physicalDomain(gname, "out[1]", 2, pids);
              if (spd != "") { fout << spd << std::endl; }
            }
            else
            {
              for (number_t i=0; i < domainNumbers.size(); ++i)
              {
                if (angle >= pi_)
                {
                  string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outa[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                if (angle == 2.*pi_)
                {
                  string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outb[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                  spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outc[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
            }
          }
        }
        else
        {
          if (pids.size() == 0) //! sidenames were not defined
          {
            if (gname == "")
            {
              if (domainNumbers.size() == 1)
              {
                if (angle >= pi_)
                {
                  string_t spd = physicalDomain("Omega", "outa[1]", 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                if (angle == 2.*pi_)
                {
                  string_t spd = physicalDomain("Omega", "outa[1]", 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                  spd = physicalDomain("Omega", "outa[1]", 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                string_t spd = physicalDomain("Omega", "out[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              else
              {
                for (number_t i=0; i < domainNumbers.size(); ++i)
                {
                  if (angle >= pi_)
                  {
                    string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outa[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                    if (spd != "") { fout << spd << std::endl; }
                  }
                  if (angle == 2.*pi_)
                  {
                    string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outb[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                    if (spd != "") { fout << spd << std::endl; }
                    spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outc[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                    if (spd != "") { fout << spd << std::endl; }
                  }
                  string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
              }
            }
            else
            {
              if (domainNumbers.size() == 1)
              {
                if (angle >= pi_)
                {
                  string_t spd = physicalDomain(gname, "outa[1]", 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                if (angle == 2.*pi_)
                {
                  string_t spd = physicalDomain(gname, "outb[1]", 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                  spd = physicalDomain(gname, "outc[1]", 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                string_t spd = physicalDomain(gname, "out[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              else
              {
                for (number_t i=0; i < domainNumbers.size(); ++i)
                {
                  if (angle >= pi_)
                  {
                    string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outa[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                    if (spd != "") { fout << spd << std::endl; }
                  }
                  if (angle == 2.*pi_)
                  {
                    string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outb[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                    if (spd != "") { fout << spd << std::endl; }
                    spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outc[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                    if (spd != "") { fout << spd << std::endl; }
                  }
                  string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
              }
            }
          }
        }
      }

      if (g.dim() == 3)
      {
        if (buildVolumes)
        {
          if (gname == "")
          {
            if (domainNumbers.size() == 1)
            {
              if (angle >= pi_)
              {
                string_t spd = physicalDomain("Omega", "outa[1]", 3, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              if (angle == 2.*pi_)
              {
                string_t spd = physicalDomain("Omega", "outb[1]", 3, pids);
                if (spd != "") { fout << spd << std::endl; }
                spd = physicalDomain("Omega", "outc[1]", 3, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              string_t spd = physicalDomain("Omega", "out[1]", 3, pids);
              if (spd != "") { fout << spd << std::endl; }
            }
            else
            {
              for (number_t i=0; i < domainNumbers.size(); ++i)
              {
                if (angle >= pi_)
                {
                  string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outa[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                if (angle == 2.*pi_)
                {
                  string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outb[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                  spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outc[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
            }
          }
          else
          {
            if (domainNumbers.size() == 1)
            {
              if (angle >= pi_)
              {
                string_t spd = physicalDomain(gname, "outa[1]", 3, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              if (angle == 2.*pi_)
              {
                string_t spd = physicalDomain(gname, "outb[1]", 3, pids);
                if (spd != "") { fout << spd << std::endl; }
                spd = physicalDomain(gname, "outc[1]", 3, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              string_t spd = physicalDomain(gname, "out[1]", 3, pids);
              if (spd != "") { fout << spd << std::endl; }
            }
            else
            {
              for (number_t i=0; i < domainNumbers.size(); ++i)
              {
                if (angle >= pi_)
                {
                  string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outa[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                if (angle == 2.*pi_)
                {
                  string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outb[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                  spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("outc[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
            }
          }
        }
        else
        {
          if (pids.size() == 0) //! sidenames were not defined
          {
            if (gname == "")
            {
              if (domainNumbers.size() == 1)
              {
                if (angle >= pi_)
                {
                  string_t spd = physicalDomain("Omega", "outa[1]", 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                if (angle == 2.*pi_)
                {
                  string_t spd = physicalDomain("Omega", "outb[1]", 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                  spd = physicalDomain("Omega", "outc[1]", 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
                string_t spd = physicalDomain("Omega", "out[1]", 3, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              else
              {
                for (number_t i=0; i < domainNumbers.size(); ++i)
                {
                  if (angle >= pi_)
                  {
                    string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outa[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                    if (spd != "") { fout << spd << std::endl; }
                  }
                  if (angle == 2.*pi_)
                  {
                    string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outb[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                    if (spd != "") { fout << spd << std::endl; }
                    spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("outc[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                    if (spd != "") { fout << spd << std::endl; }
                  }
                  string_t spd = physicalDomain(string_t("Omega_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 3, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
              }
              // string_t spd = physicalDomain("Omega", getComponentBordersToGeo(g), 2, pids);
              // if (spd != "") { fout << spd << std::endl; }
            }
            else
            {
              if (domainNumbers.size() == 1)
              {
                string_t spd = physicalDomain(gname, "out[1]", 2, pids);
                if (spd != "") { fout << spd << std::endl; }
              }
              else
              {
                for (number_t i=0; i < domainNumbers.size(); ++i)
                {
                  string_t spd = physicalDomain(gname+string_t("_")+tostring(i+1), string_t("out[")+tostring(domainNumbers[i])+string_t("]"), 2, pids);
                  if (spd != "") { fout << spd << std::endl; }
                }
              }
              // string_t spd = physicalDomain(gname, getComponentBordersToGeo(g), 2, pids);
              // if (spd != "") { fout << spd << std::endl; }
            }
          }
        }
      }
      break;
    }
    case _loop:
    {
      std::map<number_t, Geometry*> gc=g.components();
      std::map<number_t, std::vector<number_t> > gg=g.geometries();
      std::map<number_t, std::vector<number_t> >::const_iterator it_g;
      std::map<number_t, Geometry*>::const_iterator it=gc.begin(),it2;
      std::vector<const Point*> geoPoints;
      std::vector<const Point*>::const_iterator it3;
      std::map<number_t, std::vector<number_t> > gl=g.loops();
      std::map<string_t, std::vector<number_t> > gnames;
      std::map<string_t, std::vector<number_t> >::const_iterator it_n;
      std::vector<number_t> geoPtPointers, geoPtIndices;
      number_t ptrp=0;

      if (g.dim() == 2)
      {
        if (gc.size()==1)
        {
            fout << "Call xlifepp_init;" << std::endl;
            fout << "l=0;" << std::endl;
            saveComponentToGeo(*it->second, sh, fout, pids);

        }
        else // building additional information, the aim is to detect shared points (bounds) of each line
        {

          for (it=gc.begin(); it!= gc.end(); it++)
          {
            geoPtPointers.push_back(ptrp);
            bool startptExists = false, endptExists = false;
            std::vector<const Point*> nodes = it->second->boundNodes();
            ptrp+=nodes.size();
            number_t i=0;
            for (it3=geoPoints.begin(); it3 != geoPoints.end(); it3++,i++)
            {
              if (force3D(*(nodes[0])) == force3D(**it3))
              {
                startptExists=true;
                geoPtIndices.push_back(i);
              }
            }
            if (!startptExists) { geoPtIndices.push_back(2*it->first); }
            geoPoints.push_back(nodes[0]);

            i=0;
            for (it3=geoPoints.begin(); it3 != geoPoints.end(); it3++,i++)
            {
              if (force3D(*(nodes[1])) == force3D(**it3))
              {
                // if the end point is the same as the start point (that can occur only for closed arcs)
                // we disable detection, it will be done in dedicated saveXXXToGeo routines
                if (force3D(*(nodes[0])) != force3D(**it3))
                {
                  endptExists=true;
                  geoPtIndices.push_back(i);
                }
              }
            }
            if (!endptExists) { geoPtIndices.push_back(2*it->first+1); }
            geoPoints.push_back(nodes[1]);
          }
          geoPtPointers.push_back(ptrp);

          // main loop
          for (it=gc.begin(); it!= gc.end(); it++)
          {
            fout << "Call xlifepp_init;" << std::endl;
            fout << "l=" << it->first << ";" << std::endl;
            number_t i=1;
            bool addSpace=false;
            for (number_t j=geoPtPointers[it->first]; j<geoPtPointers[it->first+1]; j++,i++)
            {
              switch (it->second->shape())
              {
                case _ellArc:
                {
                  if (i==2) { i=4; }
                  break;
                }
                case _circArc:
                {
                  if (i==2) { i=3; }
                  break;
                }
                case _parametrizedArc:
                {
                  if (i==2) i=it->second->parametrizedArc()->nbParts()+1;
                  break;
                }
                case _splineArc:
                {
                if(i==2) i=it->second->lastNodeIndex();
                break;
                }
                default: break;
              }
              if (geoPtIndices[j] < j)
              {
                if (addSpace) { fout << " "; }
                fout << "builtP_" << i << "=points_" << geoPtIndices[j] << ";";
                addSpace=true;
              }
            }

            if (addSpace) { fout << std::endl; }
            saveComponentToGeo(*it->second, sh, fout, pids);
            fout << std::endl;
          }
        }

        if (buildSurfaces)
        {
          it_g=gg.begin();
          number_t llnum=gg.size();
          std::stringstream ss;
          ss << "loops_" << llnum;
          fout << ss.str() << "=newll;" << std::endl;
          fout << "Line Loop(" << ss.str() << ")={loops_" << it_g->first << "[]";
          ++it_g;
          for (; it_g != gg.end(); ++it_g) { fout << ",loops_" << it_g->first << "[]"; }
          fout << "};" << std::endl;

          if (g.isPlaneSurface) { fout << "Plane Surface("; }
          else { fout << "Ruled Surface("; }

          fout << ss.str() << ")={" << ss.str() << "};" << std::endl;

          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", ss.str()+"[]", 2, pids);
            if (spd != "") { fout << spd; }
          }
          else
          {
            string_t spd = physicalDomain(gname, ss.str()+"[]", 2, pids);
            if (spd != "") { fout << spd; }
          }
        }
      }

      if (g.dim() == 3)
      {
        std::vector<std::pair<ShapeType,std::vector<const Point*> > > geoCurves;
        std::vector<std::pair<ShapeType,std::vector<const Point*> > >::iterator it4;
        std::vector<number_t> geoCurvePointers, geoCurveIndices;
        std::vector<bool> geoCurveReverse;
        number_t ptrc=0;

        // building additional information
        // the aim is to detect shared points (bounds) of each line and shared lines of each surface
        for (it=gc.begin(); it!= gc.end(); it++)
        {
          geoPtPointers.push_back(ptrp);
          geoCurvePointers.push_back(ptrc);
          bool ptExists = false;
          std::vector<const Point*> nodes = it->second->boundNodes();
          std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves;

          if (it->second->shape() != _loop) { curves=it->second->curves(); }

          ptrp+=nodes.size();
          ptrc+=std::max(number_t(1),curves.size());
          int_t i;

          for (number_t k=0; k<nodes.size(); k++)
          {
            i=0;
            for (it3=geoPoints.begin(); it3 != geoPoints.end() && ptExists == false; it3++,i++)
            {
              if (force3D(*(nodes[k])) == force3D(**it3))
              {
                ptExists=true;
                geoPtIndices.push_back(i);
              }
            }
            if (!ptExists) { geoPtIndices.push_back(geoPtIndices.size()); }
            geoPoints.push_back(nodes[k]);
            ptExists=false;
          }

          for (number_t j=0; j< curves.size(); j++)
          {
            i=findBorder(curves[j],geoCurves);
            if (i != -1)
            {
              // we have to find the geometry to test if both are circArcs or ellArcs so that we have to test centers and apogees
              // to confirm if curves are identical or not
              if (it->second->shape() == _circArc || it->second->shape() == _ellArc)
              {
                // to find the geometry concerned by geoCurves[i], we have to find the geometry index thanks to geoCurvePointers
                it2=gc.begin();
                for (number_t gid=0; geoCurvePointers[gid] < i; ++gid, ++it2) {}
                if (it->second->shape() == _circArc)
                {
                  if (it2->second->shape() != _circArc)
                  { error("bad_geometry", it2->second->asString(), words("shape", it2->second->shape()), words("shape", _circArc)); }

                  if (it->second->circArc()->center().distance(it2->second->circArc()->center()) >= theEpsilon) { i=-1;}
                }
                if (it->second->shape() == _ellArc)
                {
                  if (it2->second->shape() != _ellArc)
                  { error("bad_geometry", it2->second->asString(), words("shape", it2->second->shape()), words("shape", _ellArc)); }
                  if (it->second->ellArc()->center().distance(it2->second->ellArc()->center()) >= theEpsilon ||
                      it->second->ellArc()->apogee().distance(it2->second->ellArc()->apogee()) >= theEpsilon) { i=-1;}
                }
              }
              if (i != -1)
              {
                geoCurveIndices.push_back(i);
                if (sameOrientation(curves[j].second,geoCurves[i].second) == false) { geoCurveReverse.push_back(true); }
                else { geoCurveReverse.push_back(false); }
              }
              else
              {
                geoCurveIndices.push_back(geoCurveIndices.size());
                geoCurveReverse.push_back(false);
              }
            }
            else
            {
              geoCurveIndices.push_back(geoCurveIndices.size());
              geoCurveReverse.push_back(false);
            }
            geoCurves.push_back(curves[j]);
          }
          if (it->second->shape() ==_loop)
          {
            geoCurves.push_back(std::make_pair(_loop,std::vector<const Point*>()));
            geoCurveIndices.push_back(geoCurveIndices.size());
            geoCurveReverse.push_back(false);
          }
        }
        geoPtPointers.push_back(ptrp);
        geoCurvePointers.push_back(ptrc);

        // main loop
        for (it=gc.begin(); it!= gc.end(); it++)
        {
          fout << "Call xlifepp_init;" << std::endl;
          fout << "l=" << it->first << ";" << std::endl;
          number_t i=1;
          bool addSpace=false;
          for (number_t j=geoPtPointers[it->first]; j<geoPtPointers[it->first+1]; j++,i++)
          {
            switch (it->second->shape())
            {
              case _ellArc:
              {
                if (i==2) { i=4; }
                break;
              }
              case _circArc:
              {
                if (i==2) { i=3; }
                break;
              }
              case _parametrizedArc:
              {
                if (i==2) i=it->second->parametrizedArc()->nbParts()+1;
                break;
              }
              case _ellipse:
              {
                if (!it->second->ellipse()->isSector()) i++;
                else
                {
                  if (i==2) { i=3; }
                }
                break;
              }
              case _disk:
              {
                if (!it->second->disk()->isSector()) i++;
                else
                {
                  if (i==2) { i=3; }
                }
                break;
              }
              default:
              {
                break;
              }
            }
            if (geoPtIndices[j] < j)
            {
              if (addSpace) { fout << " "; }
              fout << "builtP_" << i << "=points_" << geoPtIndices[j] << ";";
              addSpace=true;
            }
          }
          if (addSpace) { fout << std::endl; }

          addSpace=false;
          i=1;
          for (number_t j=geoCurvePointers[it->first]; j<geoCurvePointers[it->first+1]; j++,i++)
          {
            if (geoCurveIndices[j] != j)
            {
              if (addSpace) { fout << " "; }
              if (geoCurveReverse[j]) { fout << "builtL_" << i << "=-curves_" << geoCurveIndices[j] << ";"; }
              else                        { fout << "builtL_" << i << "=curves_" << geoCurveIndices[j] << ";"; }
              addSpace=true;
            }
          }
          if (addSpace) { fout << std::endl; }

          if (it->second->shape() == _loop)
          {
            addSpace=false;
            std::vector<number_t> gloops=gl[it->first];
            for (number_t j=0; j<gloops.size(); j++)
            {
              number_t i=gloops[j];
              if (addSpace) { fout << " "; }
              if (geoCurveReverse[i]) { fout << "builtC_" << j+1 << "=-curves_" << geoCurveIndices[i] << ";"; }
              else                    { fout << "builtC_" << j+1 << "=curves_" <<  geoCurveIndices[i] << ";"; }
              addSpace=true;
            }
            if (addSpace) { fout << std::endl; }
          }

          if (it->second->shape() == _loop)
          {
            if (it->second->dim() == 2)
            {
              fout << "loops_" << it->first << "=newll;" << std::endl;
              fout << "Line Loop(loops_" << it->first << ")={builtC_";
              std::vector<number_t> gloops = gl[it->first];
              for (number_t i=0; i < gloops.size()-1; ++i) { fout << i+1 << ",builtC_"; }
              fout << gloops.size() << "};" << std::endl;
              fout << "curves~{itc}=loops_" << it->first << "[];" << std::endl;
              fout << "itc++;" << std::endl;
              fout << std::endl;
            }
          }
          else { saveComponentToGeo(*it->second, sh,fout, pids); }
        } // end main loop

        for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
        {
          if (gc[it_g->first]->dim() == g.dim()-1 && buildSurfaces)
          {
            if (gc[it_g->first]->isPlaneSurface) { fout << "Plane Surface(loops_"; }
            else { fout << "Ruled Surface(loops_"; }
            fout << it_g->first << ")={loops_" << it_g->first << "[]";
            for (number_t i=1; i < it_g->second.size(); ++i) { fout << ",loops_" << it_g->second[i] << "[]"; }
            fout << "};" << std::endl;
          }
        }
        fout << std::endl;

        if (buildVolumes)
        {
          it_g=gg.begin();
          number_t slnum=gg.size();
          std::stringstream ss;
          ss << "loops_" << slnum;
          fout << ss.str() << "=newsl;" << std::endl;
          bool firstDone=false;
          fout << "Surface Loop(" << ss.str() << ")={";
          for (; it_g != gg.end(); ++it_g)
          {
            if (gc[it_g->first]->dim() == 2)
            {
              if (firstDone) { fout << ","; }
              fout << "loops_" << it_g->first << "[]";
              firstDone=true;
            }
          }
          fout << "};" << std::endl;
          fout << "Volume(" << ss.str() << ")={" << ss.str() << "};" << std::endl;
          fout << std::endl;

          if (gname == "")
          {
            string_t spd = physicalDomain("Omega", ss.str(), 3, pids);
            if (spd != "") { fout << spd; }
          }
          else
          {
            string_t spd = physicalDomain(gname, ss.str(), 3, pids);
            if (spd != "") { fout << spd; }
          }
        }
      } // end g.dim()==3

      // management of domain names
      for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
      {
        string_t na=gc[it_g->first]->domName();
        if (na != "") { gnames[na].push_back(it_g->first); }
      }

      // we manage domain names for geometries that are not holes of other borders
      for (it_n=gnames.begin(); it_n != gnames.end(); ++it_n)
      {
        dimen_t dim = gc[it_n->second[0]]->dim();
        Strings args;
        for (number_t i=0; i < it_n->second.size(); ++i)
        {
          std::stringstream ssargs;
          ssargs << "loops_" << it_n->second[i];
          args.push_back(ssargs.str()+"[]");
        }
        string_t spd = physicalDomain(it_n->first, args, dim, pids);
        if (spd != "") { fout << spd; }
      }
      fout << std::endl;
      break;
    }
    case _composite:
    {
      std::map<number_t, std::vector<number_t> >::const_iterator it_g;
      std::map<number_t, Geometry*> gc=g.components();
      std::map<number_t, Geometry*>::const_iterator it,it2;
      std::map<number_t, std::vector<number_t> > gg=g.geometries();
      std::map<number_t, std::vector<number_t> > gl=g.loops();
      std::map<string_t, std::vector<number_t> > gnames;
      std::map<string_t, std::vector<number_t> >::const_iterator it_n;
      std::vector<const Point*> geoPoints;
      std::vector<const Point*>::const_iterator it3;
      std::vector<number_t> geoPtPointers;
      std::vector<number_t> geoPtIndices;
      std::vector<std::pair<ShapeType,std::vector<const Point*> > > geoCurves, geoSurfaces;
      std::vector<std::pair<ShapeType,std::vector<const Point*> > >::iterator it4, it5;
      std::vector<number_t> geoCurvePointers, geoCurveIndices;
      std::vector<bool> geoCurveReverse;
      std::vector<number_t> geoSurfPointers, geoSurfIndices;
      bool physicalGroupDefined=false;
      number_t ptrp=0, ptrc=0, ptrs=0;
      int_t i;

      for (it=gc.begin(); it!=gc.end(); ++it)
      {
        geoPtPointers.push_back(ptrp);
        geoCurvePointers.push_back(ptrc);
        if (g.dim() >= 2) { geoSurfPointers.push_back(ptrs); }
        bool ptExists = false;

        std::vector<const Point*> nodes = it->second->boundNodes();
        std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves;
        if (it->second->shape() != _loop) { curves=it->second->curves(); }
        std::vector<std::pair<ShapeType,std::vector<const Point*> > > surfs;
        if (g.dim() >= 2)
        {
          if (it->second->shape() != _loop) { surfs=it->second->surfs(); }
        }

        ptrp+=nodes.size();
        ptrc+=curves.size();
        if (g.dim() >= 2) { ptrs+=surfs.size(); }

        for (number_t k=0; k<nodes.size(); k++)
        {
          i=0;
          for (it3=geoPoints.begin(); it3 != geoPoints.end() && ptExists == false; it3++,i++)
          {
            if (force3D(*(nodes[k])) == force3D(**it3))
            {
              ptExists=true;
              geoPtIndices.push_back(i);
            }
          }
          if (!ptExists) { geoPtIndices.push_back(geoPtIndices.size()); }
          geoPoints.push_back(nodes[k]);
          ptExists=false;
        }
        for (number_t j=0; j< curves.size(); j++)
        {
          i=findBorder(curves[j],geoCurves);
          if (i != -1)
          {
            // we have to find the geometry to test if both are circArcs or ellArcs so that we have to test centers and apogees
            // to confirm if curves are identical or not
            if (it->second->shape() == _circArc || it->second->shape() == _ellArc)
            {
              // to find the geometry concerned by geoCurves[i], we have to find the geometry index thanks to geoCurvePointers
              it2=gc.begin();
              for (number_t gid=0; geoCurvePointers[gid] < i; ++gid, ++it2) {}
              if (it->second->shape() == _circArc)
              {
                if (it2->second->shape() != _circArc)
                { error("bad_geometry", it2->second->asString(), words("shape", it2->second->shape()), words("shape", _circArc)); }

                if (it->second->circArc()->center().distance(it2->second->circArc()->center()) >= theEpsilon) { i=-1;}
              }
              if (it->second->shape() == _ellArc)
              {
                if (it2->second->shape() != _ellArc)
                { error("bad_geometry", it2->second->asString(), words("shape", it2->second->shape()), words("shape", _ellArc)); }
                if (it->second->ellArc()->center().distance(it2->second->ellArc()->center()) >= theEpsilon ||
                    it->second->ellArc()->apogee().distance(it2->second->ellArc()->apogee()) >= theEpsilon) { i=-1;}
              }
            }
            if (i != -1)
            {
              geoCurveIndices.push_back(i);
              if (sameOrientation(curves[j].second,geoCurves[i].second) == false) { geoCurveReverse.push_back(true); }
              else { geoCurveReverse.push_back(false); }
            }
            else
            {
              geoCurveIndices.push_back(geoCurveIndices.size());
              geoCurveReverse.push_back(false);
            }
          }
          else
          {
            geoCurveIndices.push_back(geoCurveIndices.size());
            geoCurveReverse.push_back(false);
          }
          geoCurves.push_back(curves[j]);
        }
        if (it->second->shape() ==_loop && it->second->dim() == 2)
        {
          geoCurves.push_back(std::make_pair(_loop,std::vector<const Point*>()));
          geoCurveIndices.push_back(ptrc);
          geoCurveReverse.push_back(false);
          ptrc+=1;
        }

        if (it->second->dim() >= 2)
        {
          for (number_t j=0; j< surfs.size(); j++)
          {
            i=findBorder(surfs[j],geoSurfaces);
            if (i != -1) { geoSurfIndices.push_back(i); }
            else { geoSurfIndices.push_back(geoSurfIndices.size()); }
            geoSurfaces.push_back(surfs[j]);
          }
        }
        if (it->second->shape() ==_loop && it->second->dim() == 3)
        {
          geoSurfaces.push_back(std::make_pair(_loop,std::vector<const Point*>()));
          geoSurfIndices.push_back(ptrs);
          ptrs+=1;
        }
      }

      geoPtPointers.push_back(ptrp);
      geoCurvePointers.push_back(ptrc);
      if (g.dim() >= 2) { geoSurfPointers.push_back(ptrs); }
      for (it=gc.begin(); it!= gc.end(); it++)
      {
        fout << "Call xlifepp_init;" << std::endl;
        fout << "l=" << it->first << ";" << std::endl;
        i=1;
        bool addSpace=false;
        for (number_t j=geoPtPointers[it->first]; j<geoPtPointers[it->first+1]; j++,i++)
        {
          switch (it->second->shape())
          {
            case _ellArc:
            {
              if (i==2) { i=4; }
              break;
            }
            case _circArc:
            {
              if (i==2) { i=3; }
              break;
            }
            case _parametrizedArc:
            {
              if (i==2) i=it->second->parametrizedArc()->nbParts()+1;
              break;
            }
            case _ellipse:
            {
              if (!it->second->ellipse()->isSector()) i++;
              else
              {
                if (i==2) { i=3; }
              }
              break;
            }
            case _disk:
            {
              if (!it->second->disk()->isSector()) i++;
              else
              {
                if (i==2) { i=3; }
              }
              break;
            }
            case _revCylinder:
            {
              // we ignore centers of each basis surface
              if (i==1) { i=2; }
              if (i==6) { i=7; }
              break;
            }
            case _revCone:
            {
              // we ignore centers of each basis surface
              if (i==1) { i=2; }
              break;
            }
            default:
            {
              break;
            }
          }
          if (geoPtIndices[j] < geoPtPointers[it->first])
          {
            if (addSpace) { fout << " "; }
            fout << "builtP_" << i << "=points_" << geoPtIndices[j] << ";";
            addSpace=true;
          }
        }
        if (addSpace) { fout << std::endl; }

        if (g.dim() >= 2 )
        {
          i=1;
          string_t letter="L";
          addSpace=false;
          if (it->second->shape() != _loop)
          {
            for (int_t j=geoCurvePointers[it->first]; j<geoCurvePointers[it->first+1]; j++,i++)
            {
              switch (it->second->shape())
              {
                case _ellArc:
                case _circArc:
                {
                  letter="E";
                  break;
                }
                case _ellipse:
                {
                  if (!it->second->ellipse()->isSector()) { letter="E"; }
                  else
                  {
                    number_t nv=it->second->ellipse()->v().size();
                    if (nv == 4) // obtuse sector
                    {
                      if (i==2 || i==3) { letter="E"; }
                      else { letter="L"; }
                    }
                    else // acute sector
                    {
                      if (i==2) { letter="E"; }
                      else { letter="L"; }
                    }
                  }
                  break;
                }
                case _disk:
                {
                  if (!it->second->disk()->isSector()) { letter="E"; }
                  else
                  {
                    number_t nv=it->second->disk()->v().size();
                    if (nv == 4) // obtuse sector
                    {
                      if (i==2 || i==3) { letter="E"; }
                      else { letter="L"; }
                    }
                    else // acute sector
                    {
                      if (i==2) { letter="E"; }
                      else { letter="L"; }
                    }
                  }
                  break;
                }
                case _revCylinder:
                {
                  if (i<7) { letter="E"; }
                  break;
                }
                case _revCone:
                {
                  if (i<4) { letter="E"; }
                  break;
                }
                default:
                  break;
              }

              if (geoCurveIndices[j] != j)
              {
                if (addSpace) { fout << " "; }
                if (geoCurveReverse[j]) { fout << "built" << letter << "_" << i << "=-curves_" << geoCurveIndices[j] << ";"; }
                else                    { fout << "built" << letter << "_" << i << "=curves_" <<  geoCurveIndices[j] << ";"; }
                addSpace=true;
              }
            }
            if (addSpace) { fout << std::endl; }
          }

          if (it->second->shape() == _loop && it->second->dim() == 2)
          {
            addSpace=false;
            std::vector<number_t> gloops=gl[it->first];
            for (number_t j=0; j<gloops.size(); j++)
            {
              int_t i=gloops[j];
              int_t k=geoCurvePointers[i];
              if (addSpace) { fout << " "; }
              if (geoCurveReverse[k]) { fout << "builtC_" << j+1 << "=-curves_" << k << "[];"; }
              else                    { fout << "builtC_" << j+1 << "=curves_" <<  k << "[];"; }
              addSpace=true;
            }
            if (addSpace) { fout << std::endl; }
          }
        }

        if (g.dim() >= 3)
        {
          i=1;
          addSpace=false;
          if (it->second->shape() != _loop)
          {
            for (number_t j=geoSurfPointers[it->first]; j<geoSurfPointers[it->first+1]; j++,i++)
            {
              if (geoSurfIndices[j] != j)
              {
                if (addSpace) { fout << " "; }
                fout << "builtS_" << i << "=surfs_" << geoSurfIndices[j] << "[];";
                addSpace=true;
              }
            }
            if (addSpace) { fout << std::endl; }
          }
          if (it->second->shape() == _loop && it->second->dim() == 3)
          {
            addSpace=false;
            std::vector<number_t> gloops=gl[it->first];
            for (number_t j=0; j<gloops.size(); j++)
            {
              int_t i=gloops[j];
              int_t k=geoSurfPointers[i];
              if (addSpace) { fout << " "; }
              fout << "builtS_" << j+1 << "=surfs_" << k << "[];";
              addSpace=true;
            }
            if (addSpace) { fout << std::endl; }
          }
        }

        if (it->second->shape() == _loop)
        {
          if (it->second->dim() == 2)
          {
            fout << "loops_" << it->first << "=newll;" << std::endl;
            fout << "Line Loop(loops_" << it->first << ")={builtC_";
            std::vector<number_t> gloops = gl[it->first];
            for (number_t i=0; i < gloops.size()-1; ++i) { fout << i+1 << "[],builtC_"; }
            fout << gloops.size() << "[]};" << std::endl;
            fout << "curves~{itc}=loops_" << it->first << "[];" << std::endl;
            fout << "itc++;" << std::endl;
          }
          if (it->second->dim() == 3)
          {
            fout << "loops_" << it->first << "=newsl;" << std::endl;
            fout << "Surface Loop(loops_" << it->first << ")={builtS_";
            std::vector<number_t> gloops = gl[it->first];
            for (number_t i=0; i < gloops.size()-1; ++i) { fout << i+1 << ",builtS_"; }
            fout << gloops.size() << "};" << std::endl;
            fout << "surfs~{its}=loops_" << it->first << "[];" << std::endl;
            fout << "its++;" << std::endl;
          }
          fout << std::endl;
        }
        else { saveComponentToGeo(*it->second, sh, fout, pids); }
      }

      std::stringstream buf;
      for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
      {
        if (gc[it_g->first]->dim() == 2 && buildSurfaces)
        {
          if (gc[it_g->first]->isPlaneSurface) { fout << "Plane Surface(loops_"; }
          else { fout << "Ruled Surface(loops_"; }
          fout << it_g->first << ")={";
          fout << "loops_" << it_g->second[0] << "[]";
          for (number_t i=1; i < it_g->second.size(); ++i)
          {
            if (gc[it_g->second[i]]->dim() == 2) { fout << ",loops_" << it_g->second[i] << "[]"; }
            else { buf << "Line {loops_" << it_g->second[i] << "[]} In Surface {loops_" << it_g->first << "};" << std::endl; }
          }
          fout << "};" << std::endl;
        }
      }
      fout << buf.str();
      buf.clear();
      buf.str(string_t());

      for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
      {
        if (gc[it_g->first]->dim() == 3 && buildVolumes)
        {
          fout << "Volume(loops_" << it_g->first << ")={";
          fout << "loops_" << it_g->second[0] << "[]";
          for (number_t i=1; i < it_g->second.size(); ++i)
          {
            if (gc[it_g->second[i]]->dim() == 3) { fout << ",loops_" << it_g->second[i] << "[]"; }
            else if (gc[it_g->second[i]]->dim() == 2) { buf << "Surface {loops_" << it_g->second[i] << "[]} In Volume {loops_" << it_g->first << "};" << std::endl; }
          }
          fout << "};" << std::endl;
        }
      }
      fout << buf.str();
      buf.clear();
      buf.str(string_t());

      fout << std::endl;

      // management of domain names
      for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
      {
        if (gc[it_g->first]->domName() != "")
          { gnames[gc[it_g->first]->domName()].push_back(it_g->first); physicalGroupDefined=true; }
      }

      for (it_n=gnames.begin(); it_n != gnames.end(); ++it_n)
      {
        dimen_t dim = gc[it_n->second[0]]->dim();
        if (it_n->second.size() == 1)
        {
          std::stringstream ssargs;
          ssargs << "loops_" << it_n->second[0] << "[]";
          string_t args=ssargs.str();
          string_t spd = physicalDomain(it_n->first,args,dim, pids);
          if (spd != "") { fout << spd; }
        }
        else
        {
          Strings args(it_n->second.size());
          for (number_t i=0; i < it_n->second.size(); ++i) { args[i] = "loops_"+tostring(it_n->second[i])+"[]"; }
          string_t spd = physicalDomain(it_n->first,args,dim, pids);
          if (spd != "") { fout << spd; }
        }

      }
      fout << std::endl;

      // management of cracks
      for (it_n=gnames.begin(); it_n != gnames.end(); ++it_n)
      {
        for (number_t i=0; i < it_n->second.size(); ++i)
        {
          if (gc[it_n->second[i]]->crackable)
          {
            number_t itpd=findString(pids.begin(), pids.end(), it_n->first);
            if (gc[it_n->second[i]]->crackType == _openCrack)
            {
              // getting id of side domain of crack to open
              number_t domId=findString(pids.begin(), pids.end(), gc[it_n->second[i]]->crackDomNameToOpen);
              cracks.insert(CrackData(it_n->first, pids[itpd].id, gc[it_n->second[i]]->dim(), gc[it_n->second[i]]->minimalBox, pids[domId].id));
            }
            else
            {
              cracks.insert(CrackData(it_n->first, pids[itpd].id, gc[it_n->second[i]]->dim(), gc[it_n->second[i]]->minimalBox));
            }
          }
        }
      }

      if (!physicalGroupDefined)
      {
        Strings args;
        for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
        {
          std::stringstream ssargs;
          ssargs << "loops_" << it_g->first << "[]";
          args.push_back(ssargs.str());
        }
        string_t spd = physicalDomain("Omega",args,g.dim(), pids);
        if (spd != "") { fout << spd << std::endl; }
      }
      break;
    }
    default:
    {
      error("gmsh_shape_not_handled", words("shape",g.shape()), gname);
      break;
    }
  }

  // management of domain names
  string_t spd = physicalDomains(pids);
  if (spd != "") { fout << spd << std::endl; }

  if (sh == _triangle && pattern == _structured)
  {
    string_t splitQualifier = "";
    switch (splitDirection)
    {
      case _left: splitQualifier = " Left"; break;
      case _right: splitQualifier = " Right"; break;
      case _alternate: splitQualifier = " Alternate"; break;
      default:
        break;
    }
    fout << "Transfinite Surface \"*\""+splitQualifier+";" << std::endl;
  }
  if (sh == _quadrangle)
  {
    fout << "Transfinite Surface \"*\";" << std::endl;
    fout << "Recombine Surface \"*\";" << std::endl;
  }
  if (sh == _hexahedron)
  {
    fout << "Transfinite Surface \"*\";" << std::endl;
    fout << "Recombine Surface \"*\";" << std::endl;
    fout << "Transfinite Volume \"*\";" << std::endl;
  }
  fout << std::endl;

  fout << "Mesh.ElementOrder=" << order << ";" << std::endl;

  std::vector<string_t> gmshVersionNumbers=split(GMSH_VERSION,'.');
  if (stringto<int>(gmshVersionNumbers[0]) >= 4)
  {
    fout << "Mesh.MshFileVersion = 2.2;" << std::endl;
  }
  fout.close();
}

//! writing an extrusion geometry in a geo file whatever the dimension
std::vector<std::pair<number_t, number_t> > saveExtrusionGeometryToGeo(const Geometry& g, ShapeType sh, real_t angle, std::ofstream& fout,
                                                                       std::vector<PhysicalData>& pids,
                                                                       bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=g.buildSideNamesAfterCheck(g.nbLateralSidesForExtrusion());
  Geometry* sect = g.components().at(0);
  std::vector<int_t> nnodesPerLineForExtrusion;

  if (sect->shape() == _extrusion)
  {
    where("saveExtrusionGeometryToGeo");
    error("shape_not_handled",words("shape",_extrusion));
  }
  const ExtrusionData* extrData=g.extrusionData();
  const Transformation* extr = extrData->extrusion();

  if (extr->transformType() != _translation && extr->transformType() != _rotation2d && extr->transformType() != _rotation3d )
  {
    where("saveExtrusionGeometryToGeo(...)");
    error("gmsh_extrusion_not_handled", words("transform",extr->transformType()));
  }

  // printing the first section
  // there are three cases: canonical geometry, loop geometry or composite geometry
  // std::ostringstream oss;
  std::map<string_t, Strings > inputs;
  if (extrData->layers() > 0) { inputs["Layers"].push_back(tostring(extrData->layers())); }
  else { inputs["Layers"] = Strings(); }
  if (sect->shape() != _composite && sect->shape() != _loop)
  {
    fout << "Call xlifepp_init;" << std::endl;
    fout << "l=0;" << std::endl;
    saveComponentToGeo(*sect, sh, fout, pids, withLoopsStorage, withSideNames);
    // definition of the surface of the section
    saveComponentExtraDataToGeo(*sect, 0, fout);
    // we have to tell borders of the section to get their extruded equivalent
    // It depends on the fact that section is a canonical geometry or a composite geometry
    Strings borders=getComponentBordersToGeo(*sect);

    if (g.dim() == 2)
    {
      inputs["Line"].push_back("loops_0[]");
      inputs["Point"]=borders;
    }
    if (g.dim() == 3)
    {
      nnodesPerLineForExtrusion=sect->nnodesPerBorder();
      inputs["Surface"].push_back("loops_0[]");
      inputs["Line"]=borders;
    }
  }

  if (sect->shape() == _loop)
  {
    const std::map<number_t, Geometry*>& gc=sect->components();
    std::map<number_t, Geometry*>::const_iterator it,it2;
    std::vector<const Point*> geoPoints;
    std::vector<const Point*>::const_iterator it3;
    const std::map<number_t, std::vector<number_t> >& gl=sect->loops();
    std::map<number_t, std::vector<number_t> >::const_iterator it_l;
    std::map<string_t, std::vector<number_t> > gnames;
    std::map<string_t, std::vector<number_t> >::const_iterator it_n;
    std::vector<number_t> geoPtPointers;
    std::vector<number_t> geoPtIndices;
    number_t ptrp=0;

    Strings borders;
    for (it_l=gl.begin(); it_l != gl.end(); ++it_l)
    {
      borders.push_back("loops_"+tostring(it_l->second[0])+"[]");
      for (number_t i=1; i < gl.find(it_l->first)->second.size(); ++i) { borders.push_back("loops_"+tostring(it_l->second[i])+"[]"); }
    }

    if (g.dim() == 3)
    {
      // building additional information
      // the aim is to detect shared points (bounds) of each line
      for (it=gc.begin(); it!= gc.end(); it++)
      {
        geoPtPointers.push_back(ptrp);
        bool startptExists = false, endptExists = false;
        std::vector<const Point*> nodes = it->second->boundNodes();
        ptrp+=nodes.size();
        number_t i=0;
        for (it3=geoPoints.begin(); it3 != geoPoints.end(); it3++,i++)
        {
          if (*(nodes[0]) == **it3)
          {
            startptExists=true;
            geoPtIndices.push_back(i);
          }
        }
        if (!startptExists) { geoPtIndices.push_back(2*it->first); }
        geoPoints.push_back(nodes[0]);

        i=0;
        for (it3=geoPoints.begin(); it3 != geoPoints.end(); it3++,i++)
        {
          if (*(nodes[1]) == **it3)
          {
            endptExists=true;
            geoPtIndices.push_back(i);
          }
        }
        if (!endptExists) { geoPtIndices.push_back(2*it->first+1); }
        geoPoints.push_back(nodes[1]);
      }
      geoPtPointers.push_back(ptrp);

      // main loop
      for (it=gc.begin(); it!= gc.end(); it++)
      {
        fout << "Call xlifepp_init;" << std::endl;
        fout << "l=" << it->first << ";" << std::endl;
        if (it->second->withNnodes()) { nnodesPerLineForExtrusion.push_back(it->second->n()[0]); }
        else { nnodesPerLineForExtrusion.push_back(-1); }

        number_t i=1;
        bool addSpace=false;
        for (number_t j=geoPtPointers[it->first]; j<geoPtPointers[it->first+1]; j++,i++)
        {
          switch (it->second->shape())
          {
            case _ellArc:
            {
              if (i==2) { i=4; }
              break;
            }
            case _circArc:
            {
              if (i==2) { i=3; }
              break;
            }
            default:
            {
              break;
            }
          }
          if (geoPtIndices[j] < j)
          {
            if (addSpace) { fout << " "; }
            fout << "builtP_" << i << "=points_" << geoPtIndices[j] << ";";
            addSpace=true;
          }
        }

        if (addSpace) { fout << std::endl; }
        saveComponentToGeo(*it->second, sh, fout, pids, withSideNames);

        fout << std::endl;
      }

      fout << "Line Loop(loops_0)={" << join(borders,",") << "};" << std::endl;
      if (g.isPlaneSurface) { fout << "Plane Surface(loops_0)={loops_0[]};" << std::endl; }
      else { fout << "Ruled Surface(loops_0)={loops_0[]};" << std::endl; }

      inputs["Surface"].push_back("loops_0[]");
      inputs["Line"]=borders;
    }
    else
    {
      where("saveExtrusionGeometryToGeo");
      error("2D_loop_section_only");
    }
  }

  if (sect->shape() == _composite)
  {
    // only a composite geometry whose components are all holes is authorized here
    // it means that the geometry contains only one element in its geometries map
    // where key numbers in not a value in the others elements of the map
    std::map<number_t, std::vector<number_t> >::const_iterator it_g;
    std::map<number_t, Geometry*> gc=sect->components();
    std::map<number_t, Geometry*>::const_iterator it,it2;
    std::map<number_t, std::vector<number_t> > gg=sect->geometries();
    std::map<number_t, std::vector<number_t> > gl=sect->loops();
    std::map<string_t, std::vector<number_t> > gnames;
    std::map<string_t, std::vector<number_t> >::const_iterator it_n;
    std::vector<const Point*> geoPoints;
    std::vector<const Point*>::const_iterator it3;
    std::vector<number_t> geoPtPointers;
    std::vector<number_t> geoPtIndices;
    std::vector<std::pair<ShapeType,std::vector<const Point*> > > geoCurves, geoSurfaces;
    std::vector<std::pair<ShapeType,std::vector<const Point*> > >::iterator it4, it5;
    std::vector<number_t> geoCurvePointers, geoCurveIndices;
    std::vector<bool> geoCurveReverse;
    std::vector<number_t> geoSurfPointers, geoSurfIndices;
    number_t ptrp=0, ptrc=0, ptrs=0;
    int_t i;

    for (it=gc.begin(); it!=gc.end(); ++it)
    {
      geoPtPointers.push_back(ptrp);
      if (sect->dim() >= 1) { geoCurvePointers.push_back(ptrc); }
      if (sect->dim() >= 2) { geoSurfPointers.push_back(ptrs); }
      bool ptExists = false;

      std::vector<const Point*> nodes = it->second->boundNodes();
      std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves;
      if (sect->dim() >= 1) { curves=it->second->curves(); }
      std::vector<std::pair<ShapeType,std::vector<const Point*> > > surfs;
      if (sect->dim() >= 2) { surfs=it->second->surfs(); }

      ptrp+=nodes.size();

      if (sect->dim() >= 1) { ptrc+=curves.size(); }
      if (sect->dim() >= 2) { ptrs+=surfs.size(); }

      for (number_t k=0; k<nodes.size(); k++)
      {
        i=0;
        for (it3=geoPoints.begin(); it3 != geoPoints.end() && ptExists == false; it3++,i++)
        {
          if (*(nodes[k]) == **it3)
          {
            ptExists=true;
            geoPtIndices.push_back(i);
          }
        }
        if (!ptExists) { geoPtIndices.push_back(geoPtIndices.size()); }
        geoPoints.push_back(nodes[k]);
        ptExists=false;
      }
      for (number_t j=0; j< curves.size(); j++)
      {
        i=findBorder(curves[j],geoCurves);
        if (i != -1)
        {
          geoCurveIndices.push_back(i);
          if (sameOrientation(curves[j].second,geoCurves[i].second) == false) { geoCurveReverse.push_back(true); }
          else { geoCurveReverse.push_back(false); }
        }
        else
        {
          geoCurveIndices.push_back(geoCurveIndices.size());
          geoCurveReverse.push_back(false);
        }
        geoCurves.push_back(curves[j]);
      }

      for (number_t j=0; j< surfs.size(); j++)
      {
        i=findBorder(surfs[j],geoSurfaces);
        if (i != -1) { geoSurfIndices.push_back(i); }
        else { geoSurfIndices.push_back(geoSurfIndices.size()); }
        geoSurfaces.push_back(surfs[j]);
      }
    }

    geoPtPointers.push_back(ptrp);

    if (sect->dim() >= 1) { geoCurvePointers.push_back(ptrc); }
    if (sect->dim() >= 2) { geoSurfPointers.push_back(ptrs); }

    for (it=gc.begin(); it!= gc.end(); it++)
    {
      fout << "Call xlifepp_init;" << std::endl;
      fout << "l=" << it->first << ";" << std::endl;
      i=1;
      bool addSpace=false;
      for (number_t j=geoPtPointers[it->first]; j<geoPtPointers[it->first+1]; j++,i++)
      {
        switch (it->second->shape())
        {
          case _ellArc:
          {
            if (i==2) { i=4; }
            break;
          }
          case _circArc:
          {
            if (i==2) { i=3; }
            break;
          }
          case _ellipse:
          case _disk:
          {
            i++;
            break;
          }
          default:
          {
            break;
          }
        }
        if (geoPtIndices[j] < j)
        {
          if (addSpace) { fout << " "; }
          fout << "builtP_" << i << "=points_" << geoPtIndices[j] << ";";
          addSpace=true;
        }
      }

      if (addSpace) { fout << std::endl; }

      if (sect->dim() >= 2 )
      {
        i=1;
        string_t letter="L";
        addSpace=false;
        for (int_t j=geoCurvePointers[it->first]; j<geoCurvePointers[it->first+1]; j++,i++)
        {
          switch (it->second->shape())
          {
            case _ellipse:
            case _disk:
            {
              letter="E";
              break;
            }
            default:
            {
              break;
            }
          }
          if (geoCurveIndices[j] != j)
          {
            if (addSpace) { fout << " "; }
            if (geoCurveReverse[j]) { fout << "built" << letter << "_" << i << "=-curves_" << geoCurveIndices[j] << ";"; }
            else                    { fout << "built" << letter << "_" << i << "=curves_" <<  geoCurveIndices[j] << ";"; }
            addSpace=true;
          }
        }
        if (addSpace) { fout << std::endl; }
      }

      if (sect->dim() >= 3)
      {
        i=1;
        addSpace=false;
        for (number_t j=geoSurfPointers[it->first]; j<geoSurfPointers[it->first+1]; j++,i++)
        {
          if (geoSurfIndices[j] != j)
          {
            if (addSpace) { fout << " "; }
            fout << "builtS_" << i << "=surfs_" << geoSurfIndices[j] << "[];";
            addSpace=true;
          }
        }
        if (addSpace) { fout << std::endl; }
      }

      if (it->second->shape() == _loop)
      {
        if (it->second->dim() == 2)
        {
          fout << "loops_" << it->first << "=newll;" << std::endl;
          fout << "Line Loop(loops_" << it->first << ")={loops_";
          std::vector<number_t> gloops = gl[it->first];
          for (number_t i=0; i < gloops.size()-1; ++i) { fout << gloops[i] << "[],loops_"; }
          fout << gloops[gloops.size()-1] << "[]};" << std::endl;
        }
        if (it->second->dim() == 3)
        {
          fout << "loops_" << it->first << "=newsl;" << std::endl;
          fout << "Surface Loop(loops_" << it->first << ")={loops_";
          std::vector<number_t> gloops = gl[it->first];
          for (number_t i=0; i < gloops.size()-1; ++i) { fout << gloops[i] << "[],loops_"; }
          fout << gloops[gloops.size()-1] << "]};" << std::endl;
        }
        fout << std::endl;
      }
      else { saveComponentToGeo(*it->second, sh, fout, pids, withSideNames); }
    }

    std::stringstream buf;
    for (it_g=gg.begin(); it_g != gg.end(); ++it_g)
    {
      if (gc[it_g->first]->dim() == 2)
      {
        if (gc[it_g->first]->isPlaneSurface) { fout << "Plane Surface(loops_"; }
        else { fout << "Ruled Surface(loops_"; }

        inputs["Surface"].push_back("loops_"+tostring(it_g->first)+"[]");
        fout << it_g->first << ")={";
        fout << "loops_" << it_g->second[0] << "[]";
        for (number_t i=1; i < it_g->second.size(); ++i)
        {
          if (gc[it_g->second[i]]->dim() == 2) { fout << ",loops_" << it_g->second[i] << "[]"; }
          else { buf << "Line {loops_" << it_g->second[i] << "[]} In Surface {loops_" << it_g->first << "};" << std::endl; }
        }
        fout << "};" << std::endl;
      }
    }
    fout << buf.str();
    buf.clear();
    buf.str(string_t());

    fout << std::endl;

    // getting borders is very tricky
    // not handled yet

  }

  // update of sidenames
  std::vector<string_t> sidenames2;
  std::vector<std::pair<number_t, number_t> > nbSidesPerComponent;
  number_t sid=0;
  
  if (sect->shape() == _composite)
  {
    const std::map<number_t, Geometry*>& components = sect->components();
    for (std::map<number_t, Geometry*>::const_iterator it_c=components.begin(); it_c != components.end(); ++it_c)
    {
      if (it_c->second->dim() == sect->dim())
      {
        // if it is a hole, its number won't be a key of geometries_ map
        // but we have to take holes into account for the computation of the number fo sides
        if (sect->geometries().count(it_c->first))
        {
          if (angle < 2.*pi_)
          {
            number_t bdnsize=extrData->baseDomainNames().size();
            if (bdnsize > 0)
            {
              if (sid >= bdnsize) { error("bad_size_inf", "side_names", sid, bdnsize); }
              if (extrData->baseDomainName(sid) != "") sidenames2.push_back(extrData->baseDomainName(sid));
              else sidenames2.push_back(it_c->second->domName());
              sid++;
            }
            else sidenames2.push_back(it_c->second->domName());
          }
          std::vector<number_t> nums = sect->geometries().at(it_c->first);
          number_t nbSides=0, nbCurvesOnAxis=0;
          for (number_t i=0; i < nums.size(); ++i) { nbSides+=components.at(nums[i])->nbSides(); }

          switch(extr->transformType())
          {
            case _translation: break;
            case _rotation2d:
            {
              // we have to check if one border of the base geometry is not on the rotation axis to eventually update nbSidesOfBasis
              std::vector<std::pair<ShapeType,std::vector<const Point*> > > bcurves=sect->curves();

              for (number_t i=0; i < bcurves.size(); ++i)
              {
                if (bcurves[i].first == _segment)
                {
                  Point axis=force3D(*bcurves[i].second[1]-*bcurves[i].second[0]);
                  if (pointDistance(force3D(crossProduct(axis,Point(0.,0.,1.))),Point(0.,0.,0.)) < theEpsilon)
                  {
                    Point tmp=force3D(*bcurves[i].second[1]-extr->rotation2d()->center());
                    if (pointDistance(crossProduct(tmp,axis),Point(0.,0.,0.)) < theEpsilon) { nbCurvesOnAxis++; }
                  }
                }
              }
              nbSides-=nbCurvesOnAxis;
              break;
            }
            case _rotation3d:
            {
              // we have to check if one border of the base geometry is not on the rotation axis to eventually update nbSidesOfBasis
              std::vector<std::pair<ShapeType,std::vector<const Point*> > > bcurves=sect->curves();

              for (number_t i=0; i < bcurves.size(); ++i)
              {
                if (bcurves[i].first == _segment)
                {
                  Point axis=force3D(*bcurves[i].second[1]-*bcurves[i].second[0]);
                  if (pointDistance(force3D(crossProduct(axis,force3D(extr->rotation3d()->axisDirection()))),Point(0.,0.,0.)) < theEpsilon)
                  {
                    Point tmp=force3D(*bcurves[i].second[1]-extr->rotation3d()->axisPoint());
                    if (pointDistance(crossProduct(tmp,axis),Point(0.,0.,0.)) < theEpsilon) { nbCurvesOnAxis++; }
                  }
                }
              }
              nbSides-=nbCurvesOnAxis;
              break;
            }
            default:
            {
              where("saveExtrusionGeometryToGeo(...)");
              error("gmsh_extrusion_not_handled", words("transform",extr->transformType()));
              break;
            }
          }
          nbSidesPerComponent.push_back(std::make_pair(it_c->first, nbSides));
        }
      }
    }
  }
  else
  {
    if (angle < 2.*pi_)
    {
      number_t bdnsize=extrData->baseDomainNames().size();
      if (bdnsize > 0)
      {
        if (sid >= bdnsize) { error("bad_size_inf", "side_names", sid, bdnsize); }
        if (extrData->baseDomainName(sid) != "") sidenames2.push_back(extrData->baseDomainName(sid));
        else sidenames2.push_back(sect->domName());
        sid++;
      }
      else sidenames2.push_back(sect->domName());
    }
    nbSidesPerComponent.push_back(std::make_pair(0, sect->nbSides()));
  }

  if (angle < 2.*pi_)
  {
    number_t n=sidenames2.size();
    for (number_t i=0; i < n; ++i)
    {
      number_t bdnsize=extrData->baseDomainNames().size();
      if (bdnsize > 0)
      {
        if (i+n >= bdnsize) { error("bad_size_inf", "side_names", i+n, bdnsize); }
        if (extrData->baseDomainName(i+n) != "") sidenames2.push_back(extrData->baseDomainName(i+n));
        else
        {
          if (sidenames2[i] == "") { sidenames2.push_back(sidenames2[i]); }
          else { sidenames2.push_back(sidenames2[i]+string_t("_2"));}
        }
        sid++;
      }
      else
      {
        if (sidenames2[i] == "") { sidenames2.push_back(sidenames2[i]); }
        else { sidenames2.push_back(sidenames2[i]+string_t("_2"));}
      }
      
    }
  }
  for (number_t i=0; i < sidenames.size(); ++i) { sidenames2.push_back(sidenames[i]); }
  sidenames=sidenames2;

  saveExtrusionComponentToGeo(*sect, *extr, nnodesPerLineForExtrusion, fout, inputs);

  // management of sidenames
  // their numbers are loops_nloops[], out[0], out[2..n-1] where n is the number f sides of the section curve
  if (withSideNames) { saveExtrusionSideNamesToGeo(g, sidenames, nbSidesPerComponent, *extr, fout, inputs, pids); }

  return nbSidesPerComponent;
}

//! writing extrusion inputs as string
string_t saveExtrusionInputsAsString(const std::map<string_t,Strings>& inputs)
{
  std::ostringstream oss;
  if (inputs.count("Surface"))
  {
    if (inputs.at("Surface").size() > 0)
      oss << " Surface{" << join(inputs.at("Surface"),", ") << "};";
  }
  if (inputs.count("Line"))
  {
    if (inputs.at("Line").size() > 0)
      oss << " Line{" << join(inputs.at("Line"),", ") << "};";
  }
  if (inputs.count("Point"))
  {
    if (inputs.at("Point").size() > 0)
      oss << " Point{" << join(inputs.at("Point"),", ") << "};";
  }
  if (inputs.count("Layers"))
  {
    if (inputs.at("Layers").size() > 0)
      oss << " Layers{" << join(inputs.at("Layers"),", ") << "};";
  }
  return oss.str();
}

//! writing an extrusion in a geo file
void saveExtrusionComponentToGeo(const Geometry& g, const Transformation& t, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs)
{
  switch (t.transformType())
  {
    case _translation: saveExtByTranslationToGeo(g, *t.translation(), nnodesPerLine, fout, inputs); break;
    case _rotation2d: saveExtByRotation2dToGeo(g, *t.rotation2d(), nnodesPerLine, fout, inputs); break;
    case _rotation3d: saveExtByRotation3dToGeo(g, *t.rotation3d(), nnodesPerLine, fout, inputs); break;
    default:
    {
      where("saveExtrusionComponentToGeo(...)");
      error("gmsh_extrusion_not_handled", words("transform",t.transformType()));
      break;
    }
  }
}

//! writing an extrusion by Translation in a geo file
void saveExtByTranslationToGeo(const Geometry& g, const Translation& t, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs)
{
  std::vector<real_t> u=t.u();
  if (u.size() == 2) { u.push_back(0.); }

  fout << "out[] = Extrude{" << u[0] << ", " << u[1] << ", " << u[2] << "}{"
       << saveExtrusionInputsAsString(inputs) << "};" << std::endl;
  fout << "n=#out[];" << std::endl;

  if (g.dim() > 1)
  {
    number_t nbLineElements=0;
    if (inputs.find("Line") != inputs.end()) nbLineElements=inputs.at("Line").size();

    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {out[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
  }
}

//! writing an extrusion by Rotation2d in a geo file
void saveExtByRotation2dToGeo(const Geometry& g, const Rotation2d& r, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs)
{
  if (g.dim() > 1)
  {
    where("saveExtByRotation2dToGeo");
    error("1D_only", "saveExtByRotation2dToGeo");
  }

  Point c=r.center();
  string_t inputsString=saveExtrusionInputsAsString(inputs);

  if (r.angle() < pi_)
  {
    fout << "out[] = Extrude{{0, 0, 1}, {" << c[0] << ", " << c[1] << ", 0}, " << r.angle() << "}{"
         << saveExtrusionInputsAsString(inputs) << " };" << std::endl;
  }
  else if (r.angle() < 2.*pi_)
  {
    real_t semiangle=r.angle()/2.;
    fout << "outa[] = Extrude{{0, 0, 1}, {" << c[0] << ", " << c[1] << ", 0}, " << semiangle << "}{"
         << saveExtrusionInputsAsString(inputs) << " };" << std::endl;
    std::map<string_t, Strings> inputs2=inputs;
    inputs2["Line"][0]="outa[0]";
    fout << "out[] = Extrude{{0, 0, 1}, {" << c[0] << ", " << c[1] << ", 0}, " << semiangle << "}{"
         << saveExtrusionInputsAsString(inputs2) << " };" << std::endl;
  }
  else
  {
    fout << "outa[] = Extrude{{0, 0, 1}, {" << c[0] << ", " << c[1] << ", 0}, " << pi_/2. << "}{"
         << saveExtrusionInputsAsString(inputs) << " };" << std::endl;
    std::map<string_t, Strings> inputs2=inputs;
    inputs2["Line"][0]="outa[0]";
    fout << "outb[] = Extrude{{0, 0, 1}, {" << c[0] << ", " << c[1] << ", 0}, " << pi_/2. << "}{"
         << saveExtrusionInputsAsString(inputs2) << " };" << std::endl;
    std::map<string_t, Strings> inputs3=inputs2;
    inputs3["Line"][0]="outb[0]";
    fout << "outc[] = Extrude{{0, 0, 1}, {" << c[0] << ", " << c[1] << ", 0}, " << pi_/2. << "}{"
         << saveExtrusionInputsAsString(inputs3) << " };" << std::endl;
    std::map<string_t, Strings> inputs4=inputs3;
    inputs4["Line"][0]="outc[0]";
    fout << "out[] = Extrude{{0, 0, 1}, {" << c[0] << ", " << c[1] << ", 0}, " << pi_/2. << "}{"
         << saveExtrusionInputsAsString(inputs4) << " };" << std::endl;
  }
}

//! writing an extrusion by Rotation3d in a geo file
void saveExtByRotation3dToGeo(const Geometry& g, const Rotation3d& r, std::vector<int_t> nnodesPerLine, std::ofstream& fout, const std::map<string_t,Strings>& inputs)
{
  if (g.dim() > 2)
  {
    where("saveExtByRotation3dToGeo");
    error("1D_or_2D_only", "saveExtByRotation3dToGeo", g.dim(), 2);
  }

  Point c=r.axisPoint();
  std::vector<real_t> d=r.axisDirection();
  string_t inputsString=saveExtrusionInputsAsString(inputs);
  number_t nbLineElements=0;
  if (inputs.find("Line") != inputs.end()) nbLineElements=inputs.at("Line").size();

  if (r.angle() < pi_)
  {
    fout << "out[] = Extrude{{" << d[0] << ", " << d[1] << ", " << d[2] << "}, {" << c[0] << ", " << c[1] << ", " << c[2] << "}, "
         << r.angle() << "}{" << saveExtrusionInputsAsString(inputs) << "};" << std::endl;
    fout << "n=#out[];" << std::endl;
    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {out[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
  }
  else if (r.angle() < 2.*pi_)
  {
    real_t semiangle=r.angle()/2.;
    fout << "outa[] = Extrude{{" << d[0] << ", " << d[1] << ", " << d[2] << "}, {" << c[0] << ", " << c[1] << ", " << c[2] << "}, "
         << semiangle << "}{" << saveExtrusionInputsAsString(inputs) << "};" << std::endl;
    fout << "n=#outa[];" << std::endl;
    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {outa[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
    std::map<string_t, Strings> inputs2=inputs;
    inputs2["Surface"]=Strings("outa[0]");
    for (number_t i=0; i < nbLineElements; ++i)
    {
      std::stringstream ss;
      ss << "outa[n-" << nbLineElements - i << "]";
      inputs2["Line"][i]=ss.str();
    }
    fout << "out[] = Extrude{{" << d[0] << ", " << d[1] << ", " << d[2] << "}, {" << c[0] << ", " << c[1] << ", " << c[2] << "}, "
         << semiangle << "}{" << saveExtrusionInputsAsString(inputs2) << "};" << std::endl;
    fout << "n=#out[];" << std::endl;
    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {out[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
  }
  else
  {
    fout << "outa[] = Extrude{{" << d[0] << ", " << d[1] << ", " << d[2] << "}, {" << c[0] << ", " << c[1] << ", " << c[2] << "}, "
         << pi_/2. << "}{" << saveExtrusionInputsAsString(inputs) << "};" << std::endl;
    fout << "n=#outa[];" << std::endl;
    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {outa[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
    std::map<string_t, Strings> inputs2=inputs;
    inputs2["Surface"]=Strings("outa[0]");
    for (number_t i=0; i < nbLineElements; ++i)
    {
      std::stringstream ss;
      ss << "outa[n-" << nbLineElements - i << "]";
      inputs2["Line"][i]=ss.str();
    }

    fout << "outb[] = Extrude{{" << d[0] << ", " << d[1] << ", " << d[2] << "}, {" << c[0] << ", " << c[1] << ", " << c[2] << "}, "
         << pi_/2. << "}{" << saveExtrusionInputsAsString(inputs2) << "};" << std::endl;
    fout << "n=#outb[];" << std::endl;
    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {outb[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
    std::map<string_t, Strings> inputs3=inputs2;
    inputs3["Surface"][0]="outb[0]";
    for (number_t i=0; i < nbLineElements; ++i)
    {
      std::stringstream ss;
      ss << "outb[n-" << nbLineElements - i << "]";
      inputs3["Line"][i]=ss.str();
    }
    fout << "outc[] = Extrude{{" << d[0] << ", " << d[1] << ", " << d[2] << "}, {" << c[0] << ", " << c[1] << ", " << c[2] << "}, "
         << pi_/2. << "}{" << saveExtrusionInputsAsString(inputs3) << "};" << std::endl;
    fout << "n=#outc[];" << std::endl;
    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {outc[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
    std::map<string_t, Strings> inputs4=inputs3;
    inputs4["Surface"][0]="outc[0]";
    for (number_t i=0; i < nbLineElements; ++i)
    {
      std::stringstream ss;
      ss << "outc[n-" << nbLineElements - i << "]";
      inputs4["Line"][i]=ss.str();
    }
    fout << "out[] = Extrude{{" << d[0] << ", " << d[1] << ", " << d[2] << "}, {" << c[0] << ", " << c[1] << ", " << c[2] << "}, "
         << pi_/2. << "}{" << saveExtrusionInputsAsString(inputs4) << "};" << std::endl;
    fout << "n=#out[];" << std::endl;
    for (number_t i=0; i < nbLineElements; ++i)
    {
      if (nnodesPerLine[i] != -1)
      {
        fout << "Transfinite Line {out[n-" << nbLineElements -i << "]} = " << nnodesPerLine[i] << ";" << std::endl;
      }
    }
  }
}

//! writing sidenames of an extrusion in a geo file
void saveExtrusionSideNamesToGeo(const Geometry& g, const std::vector<string_t>& sidenames,
                                 const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent, const Transformation& t,
                                 std::ofstream& fout, const std::map<string_t,Strings>& inputs, std::vector<PhysicalData>& pids)
{
  switch (t.transformType())
  {
    case _translation:
      saveExtByTranslationSideNamesToGeo(g, sidenames, nbSidesPerComponent, *t.translation(), fout, inputs, pids); break;
    case _rotation2d:
      saveExtByRotationSideNamesToGeo(g, sidenames, nbSidesPerComponent, t.rotation2d()->angle(), fout, inputs, pids); break;
    case _rotation3d:
      saveExtByRotationSideNamesToGeo(g, sidenames, nbSidesPerComponent, t.rotation3d()->angle(), fout, inputs, pids); break;
    default:
    {
      where("saveExtrusionSideNamesToGeo(...)");
      error("gmsh_extrusion_not_handled", words("transform",t.transformType()));
      break;
    }
  }
}

//! writing sidenames of an extrusion by Translation in a geo file
void saveExtByTranslationSideNamesToGeo(const Geometry& g, const std::vector<string_t>& sidenames, const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent, const Translation& t, std::ofstream& fout, const std::map<string_t,Strings>& inputs, std::vector<PhysicalData>& pids)
{
  // their numbers are loops_nloops[], out[0], out[2..n-1] where n is the number of sides of the section curve
  if (g.dim() == 2)
  {
    string_t spd = physicalDomainForExtrusion(g, sidenames, nbSidesPerComponent, "L", inputs, pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  if (g.dim() == 3)
  {
    string_t spd = physicalDomainForExtrusion(g, sidenames, nbSidesPerComponent, "S", inputs, pids);
    if (spd != "") { fout << spd << std::endl; }
  }
}

//! writing sidenames of an extrusion by Rotation2d in a geo file
void saveExtByRotationSideNamesToGeo(const Geometry& g, const std::vector<string_t>& sidenames, const std::vector<std::pair<number_t, number_t> >& nbSidesPerComponent, const real_t angle, std::ofstream& fout, const std::map<string_t,Strings>& inputs, std::vector<PhysicalData>& pids)
{
  // their numbers are loops_nloops[], out[0], out[2..n-1] where n is the number f sides of the section curve
  if (g.dim() == 2)
  {
    string_t spd = physicalDomainForExtrusion(g, sidenames, nbSidesPerComponent, "L", inputs, pids, angle);
    if (spd != "") { fout << spd << std::endl; }
  }
  if (g.dim() == 3)
  {
    string_t spd = physicalDomainForExtrusion(g, sidenames, nbSidesPerComponent, "S", inputs, pids, angle);
    if (spd != "") { fout << spd << std::endl; }
  }
}

void saveComponentExtraDataToGeo(const Geometry& g, number_t nloops, std::ofstream& fout)
{
  switch (g.shape())
  {
    case _segment:         break;
    case _ellArc:          break;
    case _circArc:         break;
    case _splineArc:       break;
    case _parametrizedArc: break;
    case _polygon:         fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _triangle:        fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _quadrangle:      fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _parallelogram:   fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _rectangle:       fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _square:          fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _ellipse:         fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _disk:            fout << "Plane Surface(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _polyhedron:      fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _tetrahedron:     fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _hexahedron:      fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _parallelepiped:  fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _cube:            fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _ellipsoid:       fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _ball:            fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _trunk:           fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _revTrunk:        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _cylinder:        fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _revCylinder:     fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _prism:           fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _revCone:         fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    case _pyramid:         fout << "Volume(loops_" << nloops << ")={loops_" << nloops << "[]};" << std::endl; break;
    default:
    {
      where("saveComponentExtraDataToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",g.shape()), g.domName());
      break;
    }
  }
}

//! returns the borders numbers of a canonical geometry in a geo file
Strings getComponentBordersToGeo(const Geometry& g)
{
  switch (g.shape())
  {
    case _segment: return Strings("P_1","P_2");
    case _ellArc: return Strings("P_1","P_4");
    case _circArc: return Strings("P_1","P_3");
    case _polygon:
    {
      Strings s(g.polygon()->p().size());
      for (number_t i=0; i < s.size(); ++i)
      {
        s[i]="L" + tostring(i+1);
      }
      return s;
    }
    case _triangle: return Strings("L_1","L_2","L_3");
    case _quadrangle:
    case _parallelogram:
    case _rectangle:
    case _square: return Strings("L_1","L_2","L_3","L_4");
    case _ellipse:
    {
      bool isSector=g.ellipse()->isSector();
      if (!isSector) { return Strings("E_1","E_2","E_3","E_4"); }
      else
      {
        number_t vsize=g.ellipse()->boundNodes().size();
        if (vsize==3) { return Strings("L_1", "E_2", "L_3"); }
        else { return Strings("L_1", "E_2", "E_3", "L_4"); }
      }
    }
    case _disk:
    {
      bool isSector=g.ellipse()->isSector();
      if (!isSector) { return Strings("E_1","E_2","E_3","E_4"); }
      else
      {
        number_t vsize=g.ellipse()->boundNodes().size();
        if (vsize==3) { return Strings("L_1", "E_2", "L_3"); }
        else { return Strings("L_1", "E_2", "E_3", "L_4"); }
      }
    }
    case _polyhedron:
    {
      Strings s(g.polyhedron()->nbFaces());
      for (number_t i=0; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _parallelepiped:
    {
      Strings s(g.parallelepiped()->nbFaces());
      for (number_t i=0; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _cuboid:
    {
      Strings s(g.cuboid()->nbFaces());
      for (number_t i=0; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _cube:
    {
      Strings s(g.cube()->nbFaces());
      for (number_t i=0; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _tetrahedron: return Strings("S_1","S_2","S_3","S_4");
    case _hexahedron: return Strings("S_1","S_2","S_3","S_4","S_5","S_6");
    case _ellipsoid:
    {
      number_t nbocts= g.ellipsoid()->nbOctants();
      if (nbocts == 8 || nbocts == 0) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6","S_7","S_8"); }
      else if (nbocts > 4) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6","S_7","S_8","S_9","S_10"); }
      else if (nbocts > 2) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6","S_7","S_8"); }
      else if (nbocts == 2) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6"); }
      else { return Strings("S_1","S_2","S_3","S_4"); }
    }
    case _ball:
    {
      number_t nbocts= g.ball()->nbOctants();
      if (nbocts == 8 || nbocts == 0) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6","S_7","S_8"); }
      else if (nbocts > 4) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6","S_7","S_8","S_9","S_10"); }
      else if (nbocts > 2) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6","S_7","S_8"); }
      else if (nbocts == 2) { return Strings("S_1","S_2","S_3","S_4","S_5","S_6"); }
      else { return Strings("S_1","S_2","S_3","S_4"); }
    }
    case _trunk:
    {
      Strings s(g.trunk()->basis()->boundNodes().size()+2);
      s[0]="S_1"; s[1]="S_2";
      for (number_t i=2; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _cylinder:
    {
      Strings s(g.cylinder()->basis()->boundNodes().size()+2);
      s[0]="S_1"; s[1]="S_2";
      for (number_t i=2; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _prism:
    {
      Strings s(g.prism()->basis()->boundNodes().size()+2);
       s[0]="S_1"; s[1]="S_2";
      for (number_t i=2; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _cone:
    {
      Strings s(g.cone()->basis()->boundNodes().size()+1);
      for (number_t i=0; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _pyramid:
    {
      Strings s(g.pyramid()->basis()->boundNodes().size()+1);
      for (number_t i=0; i < s.size(); ++i) { s[i]="S_" + tostring(i+1); }
      return s;
    }
    case _revTrunk:
    case _revCylinder: return Strings("S_1","S_2","S_3","S_4","S_5","S_6");
    case _revCone: return Strings("S_1[]","S_2","S_3","S_4","S_5");
    default:
    {
      where("getComponentBordersToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",g.shape()), g.domName());
      break;
    }
  }
  return Strings(); // dummy return
}

//! writing a component of composite geometry in a geo file whatever the dimension
void saveComponentToGeo(Geometry& g, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                        bool withLoopsStorage, bool withSideNames)
{
  switch (g.shape())
  {
    case _segment:         saveSegmentToGeo(*g.segment(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _ellArc:          saveEllArcToGeo(*g.ellArc(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _circArc:         saveCircArcToGeo(*g.circArc(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _parametrizedArc: saveParametrizedArcToGeo(*g.parametrizedArc(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _splineArc:       saveSplineArcToGeo(*g.splineArc(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _polygon:         savePolygonToGeo(*g.polygon(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _triangle:        saveTriangleToGeo(*g.triangle(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _quadrangle:      saveQuadrangleToGeo(*g.quadrangle(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _parallelogram:   saveQuadrangleToGeo(*g.parallelogram(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _rectangle:       saveQuadrangleToGeo(*g.rectangle(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _square:          saveQuadrangleToGeo(*g.square(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _ellipse:         saveEllipseToGeo(*g.ellipse(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _disk:            saveEllipseToGeo(*g.disk(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _polyhedron:      savePolyhedronToGeo(*g.polyhedron(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _tetrahedron:     saveTetrahedronToGeo(*g.tetrahedron(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _hexahedron:      saveHexahedronToGeo(*g.hexahedron(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _parallelepiped:  saveParallelepipedToGeo(*g.parallelepiped(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _cuboid:          saveParallelepipedToGeo(*g.cuboid(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _cube:            saveParallelepipedToGeo(*g.cube(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _ellipsoid:       saveEllipsoidToGeo(*g.ellipsoid(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _ball:            saveEllipsoidToGeo(*g.ball(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _revCylinder:     saveRevCylinderToGeo(*g.revCylinder(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _prism:           saveCylinderToGeo(*g.prism(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _revCone:         saveRevConeToGeo(*g.revCone(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    case _pyramid:         saveConeToGeo(*g.pyramid(), sh, fout, pids, withLoopsStorage, withSideNames); break;
    default:
    {
      where("saveComponentToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",g.shape()), g.domName());
      break;
    }
  }
}

//! writing a segment in a geo file
void saveSegmentToGeo(const Segment& s, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=s.buildSideNamesAfterCheck(2);
  bool isTransfinite=true;
  if (s.h().size() == 2) { isTransfinite=false; }

  fout << "x1=" << s.p1()[0] << "; y1=";
  if (s.p1().size() >= 2) { fout << s.p1()[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (s.p1().size() >= 3) { fout << s.p1()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << s.p2()[0]<< "; y2=" ;
  if (s.p2().size() >= 2) { fout << s.p2()[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (s.p2().size() >= 3) { fout << s.p2()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite) { fout << "h1=h0; h2=h0;" << std::endl << std::endl; }
  else { fout << "h1=" << s.h1() << "; h2=" << s.h2() << ";" << std::endl << std::endl; }

  fout << "Call xlifepp_Segment;" << std::endl << std::endl;

  if (isTransfinite) { fout << "Transfinite Line {L_1} = " << s.n(0) << ";" << std::endl; }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"P", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing an elliptic arc in a geo file
void saveEllArcToGeo(const EllArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                     bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=a.buildSideNamesAfterCheck(2);
  bool isTransfinite=true;
  if (a.h().size() == 2) { isTransfinite=false; }

  fout << "x1=" << a.p1()[0] << "; y1=";
  if (a.p1().size() >= 2) { fout << a.p1()[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (a.p1().size() >= 3) { fout << a.p1()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << a.center()[0]<< "; y2=" ;
  if (a.center().size() >= 2) { fout << a.center()[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (a.center().size() >= 3) { fout << a.center()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << a.apogee()[0]<< "; y3=" ;
  if (a.apogee().size() >= 2) { fout << a.apogee()[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (a.apogee().size() >= 3) { fout << a.apogee()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;
  if (a.apogee() == a.p1()) { fout << "apogee=1;" << std::endl;}

  fout << "x4=" << a.p2()[0]<< "; y4=" ;
  if (a.p2().size() >= 2) { fout << a.p2()[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (a.p2().size() >= 3) { fout << a.p2()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;
  if (a.p2() == a.apogee()) { fout << "apogee=2;" << std::endl;}

  if (isTransfinite) { fout << "h1=h0; h2=h0; h3=h0; h4=h0;" << std::endl << std::endl; }
  else
  {
    fout << "h1=" << a.h1() << "; h2=h0; ";
    if (a.p1() == a.apogee()) { fout << "h3=" << a.h1() << "; "; }
    else if (a.p2() == a.apogee()) { fout << "h3=" << a.h2() << "; "; }
    else { fout << "h3=h0; "; }
    fout << "h4=" << a.h2() << ";" << std::endl << std::endl;
  }

  fout << "Call xlifepp_EllArc;" << std::endl << std::endl;

  if (isTransfinite) { fout << "Transfinite Line {E_1} = " << a.n(0) << ";" << std::endl; }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"P", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a circle arc in a geo file
void saveCircArcToGeo(const CircArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=a.buildSideNamesAfterCheck(2);
  bool isTransfinite=true;
  if (a.h().size() == 2) { isTransfinite=false; }

  fout << "x1=" << a.p1()[0] << "; y1=";
  if (a.p1().size() >= 2) { fout << a.p1()[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (a.p1().size() >= 3) { fout << a.p1()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << a.center()[0]<< "; y2=" ;
  if (a.center().size() >= 2) { fout << a.center()[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (a.center().size() >= 3) { fout << a.center()[2] ; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << a.p2()[0]<< "; y3=" ;
  if (a.p2().size() >= 2) { fout << a.p2()[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (a.p2().size() >= 3) { fout << a.p2()[2] ; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite) { fout << "h1=h0; h2=h0; h3=h0;" << std::endl << std::endl; }
  else { fout << "h1=" << a.h1() << "; h2=h0; h3=" << a.h2() << ";" << std::endl << std::endl; }

  fout << "Call xlifepp_CircArc;" << std::endl << std::endl;

  if (isTransfinite) { fout << "Transfinite Line {E_1} = " << a.n(0) << ";" << std::endl; }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"P", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a parametrized arc in a geo file
void saveParametrizedArcToGeo(const ParametrizedArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                              bool withLoopsStorage, bool withSideNames)
{
  number_t nbSides=2;
  if (a.isClosed()) { nbSides=1; }
  Strings sidenames=a.buildSideNamesAfterCheck(nbSides);
  bool isTransfinite = (a.h().size() == 0);   //true if nnodes given
  number_t np=a.nbParts(), nbn=0;
  if(isTransfinite)
  {
      nbn=std::max(number_t(2),a.n()[0]/np); //should be > 1
  }
  // real_t dt=(a.tmax()-a.tmin())/np, t=a.tmin();
  real_t dt=1./np, t=0;

  if (a.partitioning()==_splinePartition)  // use spline partition (Catmull-Rom spline)
  {
      std::vector<Point> pts(np+1);
      for (number_t i=0; i <= np; ++i, t+=dt) pts[i]=a.parametrization()(t);
      if(a.h().size()==0)
      {
          SplineArc spa(_splineType=_CatmullRomSpline,_vertices=pts,_nnodes=a.n());
          saveSplineArcToGeo(spa,sh,fout,pids,withLoopsStorage,withSideNames);
      }
      else
      {
          SplineArc spa(_splineType=_CatmullRomSpline,_vertices=pts,_hsteps=a.h());
          saveSplineArcToGeo(spa,sh,fout,pids,withLoopsStorage,withSideNames);
      }
      return;
  }
  else // use linear partition
  {
    for (number_t i=1; i <= np+1; ++i, t+=dt)
    {
      //if (i==np+1) t=a.tmax(); // to be exactly equal to p2
      if (i==np+1) t=1.; // to be exactly equal to p2
      if (i==np+1 && a.isClosed()) { fout << "builtP_" << i << "=P_1;" << std::endl; }
      Point p=a.parametrization()(t);
      fout << "If (builtP_" << i << " == undefPoint)" << std::endl;
      fout << "  P_" << i << "=newp;" << std::endl;
      fout << "  Point(P_" << i << ")={" << p[0];
      if (p.size() >= 2)      fout << "," << p[1];
      else   fout << ",0";
      if (p.size() >= 3)      fout << "," << p[2];
      else  fout << ",0";
      if (isTransfinite) fout << ",h0};" << std::endl;
      else fout << "," << a.h()[i-1] << "};" << std::endl;
      fout << "EndIf" << std::endl;
      fout << "If (builtP_" << i << " != undefPoint)" << std::endl;
      fout << "  P_" << i << "=builtP_" << i << ";" << std::endl;
      fout << "EndIf" << std::endl;
    }
    fout << std::endl;
    fout << "points~{itp}=P_1;" << std::endl;
    fout << "itp++;" << std::endl;
    fout << "points~{itp}=P_" << np+1 << ";" << std::endl;
    fout << "itp++;" << std::endl << std::endl;

    string_t sl;
    for (number_t i=1; i <= np; ++i)
    {
      fout << "If (builtL_" << i << " == undefCurve)" << std::endl;
      fout << "  L_" << i << "=newl;" << std::endl;
      fout << "  Line(L_" << i << ")={P_" << i << ",P_" << i+1 << "};" << std::endl;
      fout << "EndIf" << std::endl;
      fout << "If (builtL_" << i << " != undefCurve)" << std::endl;
      fout << "  L_" << i << "=builtL_" << i << ";" << std::endl;
      fout << "EndIf" << std::endl;
      sl+="L_"+tostring(i);
      if (i!=np) sl+=",";
    }
    fout<<"loops~{l}={"<<sl<<"};"<<std::endl;
    fout << "curves~{itc}=loops~{l}[];" << std::endl;
    fout << "itc++;" << std::endl << std::endl;

    if (isTransfinite)
    {
      for (number_t i=1; i <= np; ++i)
        fout << "Transfinite Line {L_" << i << "} = " << nbn << ";" << std::endl;
    }
    if (withSideNames)
    {
      string_t spd = physicalDomain(sidenames,"L", pids);
      if (spd != "")  fout << spd << std::endl;
    }
    fout << std::endl;
  }
}

//! writing a spline arc in a geo file
//!   spline in standard gmsh factory corresponds to Catmull-Rom spline
//!   spline in occ gmsh factory corresponds to C2 spline
void saveSplineArcToGeo(const SplineArc& a, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                        bool withLoopsStorage, bool withSideNames)
{
  const Spline& sp=a.spline();
  SplineType st=sp.type();
  const std::vector<Point>& cpts=sp.controlPoints();
  number_t nbv=cpts.size();
  string_t nasp="";
  number_t s=1, ne=nbv;
  switch (st)
  {
    case _CatmullRomSpline: nasp="Spline"; s=0; ne=nbv-2;break;
    case _BezierSpline:
    {
      //in GMSH, a Bezier curve is an union of Bezier curves of degree 3 : B3(P1,P2,P3,P4) B3(P4,P5,P6,P7) B3(P7,P8,P9,P10) ....
      //it is only C0 except if P3P4 x P4P5 = 0, P6P7 x P7P8 = 0, ...  in that case it is a C1 curve
      //because this curves can be constructed by an union of SplineArc (Bezier) and its management of parametrization is more intricate
      //we decide to allow in GMSH only  Bezier curves defined from 4 points!
      if (nbv!=4) error("free_error"," Only Bezier curve defined from 4 points are regular in gmsh");
      nasp="Bezier";
      break;
    }
    case _BSpline:
    {
      if (sp.degree()!=3) error("free_error"," gmsh supports only cubic BSpline");
      if (sp.subtype()==_SplineInterpolation) error("free_error"," gmsh supports only approximate BSpline, use _gmshOC mesh option if OC available");
      nasp="BSpline";
      break;
    }
    default: error("free_error","convert "+words("spline type",st)+" to gmsh is not yet supported");
 }
  number_t nbSides=2;
  if (a.isClosed()) { nbSides=1; }
  bool isTransfinite = (a.h().size() == 0);
  //create gmsh Points
  for (number_t i=1; i <= ne; ++i)
  {
    const Point& p=cpts[i-s];
    fout << "If (builtP_" << i << " == undefPoint)" << std::endl;
    fout << "  P_" << i << "=newp;" << std::endl;
    fout << "  Point(P_" << i << ")={" << p[0];
    if (p.size() >= 2) fout << "," << p[1];
    else   fout << ",0";
    if (p.size() >= 3) fout << "," << p[2];
    else  fout << ",0";
    if (!isTransfinite) fout << "," << a.h()[i-s] << "};" << std::endl;
    else fout << ",h0};" << std::endl;
    fout << "EndIf" << std::endl;
    fout << "If (builtP_" << i << " != undefPoint)" << std::endl;
    fout << "  P_" << i << "=builtP_" << i << ";" << std::endl;
    fout << "EndIf" << std::endl;
    if (i==1 || i==ne)
    {
      fout << "points~{itp}=P_" << i << "; " << std::endl;
      fout << "itp++;" << std::endl << std::endl;
    }
  }
  fout << "S_1=newl;" << std::endl;
  fout << nasp << "(S_1)={P_1";
  for (number_t i=2; i <= ne-1; ++i)  fout << ",P_" << i;
  if (sp.isClosed()) fout<<",P_1"; else fout << ",P_" << ne;
  fout << "};" << std::endl;
  fout << "loops~{l}=S_1;" << std::endl;
  fout << "curves~{itc}=S_1;" << std::endl;
  fout << "itc++;" << std::endl << std::endl;
  if (isTransfinite)
    fout << "Transfinite Line {S_1} = " << a.n() << ";" << std::endl;
}

//! writing a triangle in a geo file
void savePolygonToGeo(const Polygon& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage, bool withSideNames)
{
  number_t nbVertices=p.p().size();
  Strings sidenames=p.buildSideNamesAfterCheck(nbVertices);
  bool isTransfinite=true;
  if (p.h().size() == nbVertices) { isTransfinite=false; }

  for (number_t i=1; i < nbVertices+1; ++i)
  {
    fout << "If (builtP_" << i << " == undefPoint)" << std::endl;
    fout << "  P_" << i << "=newp;" << std::endl;
    fout << "  Point(P_" << i << ")={" << p.p(i)[0];
    if (p.p(i).size() >= 2) { fout << "," << p.p(i)[1]; }
    else { fout << ",0"; }
    if (p.p(i).size() >= 3) { fout << "," << p.p(i)[2]; }
    else { fout << ",0"; }
    if (isTransfinite) { fout << ",h0};" << std::endl; }
    else { fout << "," << p.h(i) << "};" << std::endl; }
    fout << "EndIf" << std::endl;
    fout << "If (builtP_" << i << " != undefPoint)" << std::endl;
    fout << "  P_" << i << "=builtP_" << i << ";" << std::endl;
    fout << "EndIf" << std::endl;
    fout << "points~{itp}=P_" << i << ";" << std::endl;
    fout << "itp++;" << std::endl << std::endl;
  }

  for (number_t i=1; i < nbVertices+1; ++i)
  {
    fout << "If (builtL_" << i << " == undefCurve)" << std::endl;
    fout << "  L_" << i << "=newl;" << std::endl;
    if (i == nbVertices) { fout << "  Line(L_" << i << ")={P_" << i << ",P_1};" << std::endl; }
    else { fout << "  Line(L_" << i << ")={P_" << i << ",P_" << i+1 << "};" << std::endl; }
    fout << "EndIf" << std::endl;
    fout << "If (builtL_" << i << " != undefCurve)" << std::endl;
    fout << "  L_" << i << "=builtL_" << i << ";" << std::endl;
    fout << "EndIf" << std::endl;
    fout << "curves~{itc}=L_" << i << ";" << std::endl;
    fout << "itc++;" << std::endl << std::endl;
  }

  fout << "LL_1=newll;" << std::endl;
  if (withLoopsStorage) { fout << "loops~{l}=LL_1;" << std::endl; }
  fout << "Line Loop(LL_1)={L_1";
  for (number_t i=2; i < nbVertices+1; ++i) { fout << ",L_" << i; }
  fout << "};" << std::endl << std::endl;
  fout << "surfs~{its}=LL_1;" << std::endl;
  fout << "its++;" << std::endl;
  if (isTransfinite)
  {
    for (number_t i=1; i < nbVertices+1; ++i) { fout << "Transfinite Line {L_" << i << "} = " << p.n(i) << ";" << std::endl; }
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"L", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a triangle in a geo file
void saveTriangleToGeo(const Triangle& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                       bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=t.buildSideNamesAfterCheck(3);
  bool isTransfinite=true;
  if (t.h().size() == 3) { isTransfinite=false; }

  fout << "x1=" << t.p(1)[0] << "; y1=";
  if (t.p(1).size() >= 2) { fout << t.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (t.p(1).size() >= 3) { fout << t.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << t.p(2)[0] << "; y2=";
  if (t.p(2).size() >= 2) { fout << t.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (t.p(2).size() >= 3) { fout << t.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << t.p(3)[0] << "; y3=";
  if (t.p(3).size() >= 2) { fout << t.p(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (t.p(3).size() >= 3) { fout << t.p(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite) { fout << "h1=h0; h2=h0; h3=h0;" << std::endl << std::endl; }
  else { fout << "h1=" << t.h(1) << "; h2=" << t.h(2) << "; h3=" << t.h(3) << ";" << std::endl<< std::endl;}

  fout << "Call xlifepp_Triangle;" << std::endl << std::endl;

  if (isTransfinite)
  {
    fout << "Transfinite Line {L_1} = " << t.n(1) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << t.n(2) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << t.n(3) << ";" << std::endl;
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"L", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a quadrangle, a rectangle or a square in a geo file
void saveQuadrangleToGeo(const Quadrangle& q, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=q.buildSideNamesAfterCheck(4);
  bool isTransfinite=true;
  if (q.h().size() == 4) { isTransfinite=false; }

  fout << "x1=" << q.p(1)[0] << "; y1=";
  if (q.p(1).size() >= 2) { fout << q.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (q.p(1).size() >= 3) { fout << q.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << q.p(2)[0] << "; y2=";
  if (q.p(2).size() >= 2) { fout << q.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (q.p(2).size() >= 3) { fout << q.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << q.p(3)[0] << "; y3=";
  if (q.p(3).size() >= 2) { fout << q.p(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (q.p(3).size() >= 3) { fout << q.p(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x4=" << q.p(4)[0] << "; y4=";
  if (q.p(4).size() >= 2) { fout << q.p(4)[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (q.p(4).size() >= 3) { fout << q.p(4)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite) { fout << "h1=h0; h2=h0; h3=h0; h4=h0;" << std::endl << std::endl; }
  else { fout << "h1=" << q.h(1) << "; h2=" << q.h(2) << "; h3=" << q.h(3) << ";h4=" << q.h(4) << ";" << std::endl << std::endl; }

  fout << "Call xlifepp_Quadrangle;" << std::endl << std::endl;

  if (isTransfinite)
  {
    fout << "Transfinite Line {L_1} = " << q.n(1) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << q.n(2) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << q.n(3) << ";" << std::endl;
    fout << "Transfinite Line {L_4} = " << q.n(4) << ";" << std::endl;
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"L", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing an elliptic surface or a disk in a geo file
void saveEllipseToGeo(const Ellipse& e, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage, bool withSideNames)
{
  if (!e.isSector())
  {
    Strings sidenames=e.buildSideNamesAfterCheck(4);
    bool isTransfinite=true;
    if (e.h().size() == 4) { isTransfinite=false; }

    fout << "x1=" << e.p(1)[0] << "; y1=";
    if (e.p(1).size() >= 2) { fout << e.p(1)[1]; }
    else { fout << "0"; }
    fout << "; z1=";
    if (e.p(1).size() >= 3) { fout << e.p(1)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x2=" << e.p(2)[0] << "; y2=";
    if (e.p(2).size() >= 2) { fout << e.p(2)[1]; }
    else { fout << "0"; }
    fout << "; z2=";
    if (e.p(2).size() >= 3) { fout << e.p(2)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x3=" << e.p(3)[0] << "; y3=";
    if (e.p(3).size() >= 2) { fout << e.p(3)[1]; }
    else { fout << "0"; }
    fout << "; z3=";
    if (e.p(3).size() >= 3) { fout << e.p(3)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x4=" << e.p(4)[0] << "; y4=";
    if (e.p(4).size() >= 2) { fout << e.p(4)[1]; }
    else { fout << "0"; }
    fout << "; z4=";
    if (e.p(4).size() >= 3) { fout << e.p(4)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x5=" << e.p(5)[0] << "; y5=";
    if (e.p(5).size() >= 2) { fout << e.p(5)[1]; }
    else { fout << "0"; }
    fout << "; z5=";
    if (e.p(5).size() >= 3) { fout << e.p(5)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    if (isTransfinite) { fout << "h1=h0; h2=h0; h3=h0; h4=h0; h5=h0;" << std::endl << std::endl; }
    else { fout << "h1=h0; h2=" << e.h(1) << "; h3=" << e.h(2) << "; h4=" << e.h(3) << "; h5=" << e.h(4) << ";" << std::endl << std::endl; }

    fout << "Call xlifepp_Ellipse;" << std::endl << std::endl;

    if (isTransfinite)
    {
      fout << "Transfinite Line {E_1} = " << e.n(1) << ";" << std::endl;
      fout << "Transfinite Line {E_2} = " << e.n(2) << ";" << std::endl;
      fout << "Transfinite Line {E_3} = " << e.n(3) << ";" << std::endl;
      fout << "Transfinite Line {E_4} = " << e.n(4) << ";" << std::endl;
    }

    if (withSideNames)
    {
      string_t spd = physicalDomain(sidenames,"E", pids);
      if (spd != "") { fout << spd << std::endl; }
    }
    fout << std::endl;
  }
  else
  {
    number_t nv=e.v().size();
    Strings sidenames=e.buildSideNamesAfterCheck(nv);
    bool isTransfinite=true;
    if (e.h().size() == nv) { isTransfinite=false; }

    // center
    fout << "x1=" << e.v(1)[0] << "; y1=";
    if (e.v(1).size() >= 2) { fout << e.v(1)[1]; }
    else { fout << "0"; }
    fout << "; z1=";
    if (e.v(1).size() >= 3) { fout << e.v(1)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    // apogee
    fout << "x2=" << e.p(2)[0] << "; y2=";
    if (e.p(2).size() >= 2) { fout << e.p(2)[1]; }
    else { fout << "0"; }
    fout << "; z2=";
    if (e.p(2).size() >= 3) { fout << e.p(2)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x3=" << e.v(2)[0] << "; y3=";
    if (e.v(2).size() >= 2) { fout << e.v(2)[1]; }
    else { fout << "0"; }
    fout << "; z3=";
    if (e.v(2).size() >= 3) { fout << e.v(2)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x4=" << e.v(3)[0] << "; y4=";
    if (e.v(3).size() >= 2) { fout << e.v(3)[1]; }
    else { fout << "0"; }
    fout << "; z4=";
    if (e.v(3).size() >= 3) { fout << e.v(3)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    if (nv == 4)
    {
      // elliptical sector is obtuse
      fout << "x5=" << e.v(4)[0] << "; y5=";
      if (e.v(4).size() >= 2) { fout << e.v(4)[1]; }
      else { fout << "0"; }
      fout << "; z5=";
      if (e.v(4).size() >= 3) { fout << e.v(4)[2]; }
      else { fout << "0"; }
      fout << ";" << std::endl;
    }

    Strings sids("L","E","L");
    if (isTransfinite)
    {
      fout << "h1=h0; h2=h0; h3=h0; h4=h0;";
      if (nv == 4)
      {
        fout << " h5=h0;";
        sids=Strings("L","E","E","L");
      }
      fout << std::endl << std::endl;
    }
    else
    {
      fout << "h1=" << e.h(1) << "; h2=h0; h3=" << e.h(2) << "; h4=" << e.h(3) << ";";
      if (nv == 4)
      {
        fout << " h5=" << e.h(4) << ";";
        sids=Strings("L","E","E","L");
      } 
      fout << std::endl << std::endl;
    }
    if (nv==4) { fout << "Call xlifepp_OutwardEllipticSector;" << std::endl << std::endl; }
    else { fout << "Call xlifepp_InwardEllipticSector;" << std::endl << std::endl; }

    if (isTransfinite)
    {
      for (number_t i=0; i < sids.size(); ++i)
      {
        fout << "Transfinite Line {" << sids[i] << "_" << i+1 << "} = " << e.n(i+1) << ";" << std::endl;
      }
    }

    if (withSideNames)
    {
      string_t spd = physicalDomain(sidenames, sids, pids);
      if (spd != "") { fout << spd << std::endl; }
    }
    fout << std::endl;
  }
}

/*! writing a parametrized surface (split in triangle or nurbs) in a geo file
    split the parametrized surface s in s.nbParts elementary surface
    either quadrangle or triangle or nurbs (s.shapePart)
*/
void saveParametrizedSurfaceToGeo(const ParametrizedSurface& s, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                                  bool withLoopsStorage, bool withSideNames)
{
  number_t nbSides=4;
  if (s.isClosed()) { nbSides=1; }
  bool isTransfinite = (s.h().size() == 0);   //true if nnodes given
  number_t np=s.nbParts(), nbn=s.n().size();
  if (isTransfinite)
  {
    if (nbn==1) nbn=s.n()[0]*s.n()[0]/np;
    else if(nbn>1) nbn=s.n()[0]*s.n()[1]/np;
    nbn=std::max(number_t(2),nbn);
  }
  string_t dname=s.domName();
  if (dname=="") dname="OMEGA";
  const Parametrization& par=s.parametrization();
  if (s.partitioning()==_splinePartition)  // use nurbs partition
  {
    error("free_error","saveParametrizedSurfaceToGeo does not yet handle spline partition");
    return;
  }
  else // use linear partition (triangle)
  {
    Mesh& ms=*s.parametrization().meshP();
    //create points
    for (number_t i=1; i <= s.p().size(); ++i)
    {
      const Point& p=s.p()[i-1];
      fout << "If (builtP_" << i << " == undefPoint)" << std::endl;
      fout << "  P_" << i << "=newp;" << std::endl;
      fout << "  Point(P_" << i << ")={" << p[0];
      if (p.size() >= 2) fout << "," << p[1]; else  fout << ",0";
      if (p.size() >= 3) fout << "," << p[2]; else  fout << ",0";
      if (isTransfinite) fout << ",h0};" << std::endl;
      else fout << "," << s.h()[i-1] << "};" << std::endl;
      fout << "EndIf" << std::endl;
      fout << "If (builtP_" << i << " != undefPoint)" << std::endl;
      fout << "  P_" << i << "=builtP_" << i << ";" << std::endl;
      fout << "EndIf" << std::endl;
    }
    //create edge map
    std::map<string_t,std::vector<GeoNumPair> > sideIndex;
    createSideIndex(ms.elements(),sideIndex);
    std::map<std::pair<number_t,number_t>, number_t> sideNumbers;
    std::map<string_t,std::vector<GeoNumPair> >::iterator itm=sideIndex.begin();
    //create lines
    number_t l=1;
    for (;itm!=sideIndex.end();++itm, l++)
    {
      std::vector<number_t> vs=itm->second[0].first->vertexNumbers(itm->second[0].second);
      if (vs[0]>vs[1]) { number_t t=vs[1];vs[1]=vs[0];vs[0]=t; }  //swap vertex numbers
      fout << "  L_" << l << "=newl;" << std::endl;
      fout << "  Line(L_" << l << ")={P_" << vs[0] << ",P_" << vs[1] << "};" << std::endl;
      sideNumbers.insert(std::make_pair(std::make_pair(vs[0],vs[1]),l));
      if (isTransfinite) fout << "Transfinite Line {L_"<<l<<"} = " << nbn << ";" << std::endl;
    }
    // create line loops and surfaces
    string_t phys="Physical Surface(\""+dname+"\")= {";
    for (number_t i=1; i <= ms.nbOfElements(); ++i)
    {
      const GeomElement& gelt=ms.element(i);
      fout << "  LL_" << i << "=newll;" << std::endl;
      fout << "  Line Loop(LL_" << i << ")={";
      for (number_t side=1;side<=gelt.numberOfSides();side++)
      {
        std::vector<number_t> vs=gelt.vertexNumbers(side);
        if (side>1) fout << ",";
        if (vs[0]<vs[1]) fout << "L_" << sideNumbers[std::make_pair(vs[0],vs[1])];
        else fout << "-L_" << sideNumbers[std::make_pair(vs[1],vs[0])];
      }
      fout << "};" << std::endl;
      fout << "  S_" << i << "=news;" << std::endl;
      fout << "  Plane Surface(S_" << i << ") = {LL_" << i << "};" << std::endl;
      if (i>1) phys+=",S_"+tostring(i); else phys+="S_"+tostring(i);
    }
    phys+="};";
    fout << phys << std::endl;
  }
  fout << std::endl;
}

//! writing a polyhedral volume in a geo file
void savePolyhedronToGeo(const Polyhedron& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage, bool withSideNames)
{
  number_t nbFaces=p.nbFaces();
  std::vector<Polygon*> gc=p.faces();

  std::vector<const Point*> geoPoints;
  std::vector<const Point*>::const_iterator it3;
  std::map<string_t, std::vector<number_t> > gnames;
  std::map<string_t, std::vector<number_t> >::const_iterator it_n;
  std::vector<number_t> geoPtPointers, geoPtIndices;
  number_t ptrp=0;

  std::vector<std::pair<ShapeType,std::vector<const Point*> > > geoCurves;
  std::vector<std::pair<ShapeType,std::vector<const Point*> > >::iterator it4;
  std::vector<int_t> geoCurvePointers, geoCurveIndices;
  number_t ptrc=0;

  // building additional information
  // the aim is to detect shared points (bounds) of each line and shared lines of each surface
  for (number_t f=0; f < nbFaces; ++f)
  {
    geoPtPointers.push_back(ptrp);
    geoCurvePointers.push_back(ptrc);
    bool ptExists = false;
    std::vector<const Point*> nodes = gc[f]->boundNodes();
    std::vector<std::pair<ShapeType,std::vector<const Point*> > > curves=gc[f]->curves();
    ptrp+=nodes.size();
    ptrc+=curves.size();
    int_t i;

    for (number_t k=0; k<nodes.size(); k++)
    {
      i=0;
      for (it3=geoPoints.begin(); it3 != geoPoints.end() && ptExists == false; it3++,i++)
      {
        if (force3D(*(nodes[k])) == force3D(**it3))
        {
          ptExists=true;
          geoPtIndices.push_back(i);
        }
      }
      if (!ptExists) { geoPtIndices.push_back(geoPtIndices.size()); }
      geoPoints.push_back(nodes[k]);
      ptExists=false;
    }

    for (number_t j=0; j< curves.size(); j++)
    {
      i=findBorder(curves[j],geoCurves);
      if (i != -1)
      {
        if (sameOrientation(curves[j].second,geoCurves[i].second) == false) { geoCurveIndices.push_back(-i); }
        else { geoCurveIndices.push_back(i); }
      }
      else { geoCurveIndices.push_back(geoCurveIndices.size()); }
      geoCurves.push_back(curves[j]);
    }
  }
  geoPtPointers.push_back(ptrp);
  geoCurvePointers.push_back(ptrc);

  // main loop
  for (number_t f=0; f < nbFaces; ++f)
  {
    fout << "Call xlifepp_init;" << std::endl;
    number_t i=1;
    bool addSpace=false;
    for (number_t j=geoPtPointers[f]; j<geoPtPointers[f+1]; j++,i++)
    {
      if (geoPtIndices[j] < j)
      {
        if (addSpace) { fout << " "; }
        fout << "builtP_" << i << "=points_" << geoPtIndices[j] << ";";
        addSpace=true;
      }
    }
    if (addSpace) { fout << std::endl; }

    addSpace=false;
    i=1;
    string_t letter="L";
    for (int_t j=geoCurvePointers[f]; j<geoCurvePointers[f+1]; j++,i++)
    {
      if (std::abs(geoCurveIndices[j]) != j)
      {
        if (addSpace) { fout << " "; }
        if (geoCurveIndices[j] < 0) { fout << "built" << letter << "_" << i << "=-curves_" << -geoCurveIndices[j] << ";"; }
        else                        { fout << "built" << letter << "_" << i << "=curves_" <<  geoCurveIndices[j] << ";"; }
        addSpace=true;
      }
    }

    if (addSpace) { fout << std::endl; }

    saveComponentToGeo(*gc[f], sh, fout, pids, false);
    fout << "S_" << f+1 << "=news;" << std::endl;

    fout << "Plane Surface(S_" << f+1 << ")={LL_1};" << std::endl;

    fout << std::endl;
  } // end main loop

  fout << "SL_1=news;" << std::endl;
  if (withLoopsStorage) { fout << "loops~{l}=SL_1;" << std::endl;}
  fout << "Surface Loop(SL_1)={S_1";
  for (number_t i=2; i < nbFaces+1; ++i) { fout << ",S_" << i; }
  fout << "};" << std::endl;

  if (withSideNames)
  {
    std::vector<string_t> sidenames(nbFaces);
    for (number_t f=0; f < nbFaces; ++f) { sidenames[f]=gc[f]->domName(); }
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
}

//! writing a tetrahedron in a geo file
void saveTetrahedronToGeo(const Tetrahedron& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                          bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=t.buildSideNamesAfterCheck(4);
  bool isTransfinite=true;
  if (t.h().size() == 4) { isTransfinite=false; }

  fout << "x1=" << t.p(1)[0] << "; y1=";
  if (t.p(1).size() >= 2) { fout << t.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (t.p(1).size() >= 3) { fout << t.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << t.p(2)[0] << "; y2=";
  if (t.p(2).size() >= 2) { fout << t.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (t.p(2).size() >= 3) { fout << t.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << t.p(3)[0] << "; y3=";
  if (t.p(3).size() >= 2) { fout << t.p(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (t.p(3).size() >= 3) { fout << t.p(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x4=" << t.p(4)[0] << "; y4=";
  if (t.p(4).size() >= 2) { fout << t.p(4)[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (t.p(4).size() >= 3) { fout << t.p(4)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite) { fout << "h1=h0; h2=h0; h3=h0; h4=h0;" << std::endl << std::endl; }
  else { fout << "h1=" << t.h(1) << "; h2=" << t.h(2) << "; h3=" << t.h(3) << "; h4=" << t.h(4) << ";" << std::endl << std::endl; }

  fout << "Call xlifepp_Tetrahedron;" << std::endl << std::endl;

  if (isTransfinite)
  {
    fout << "Transfinite Line {L_1} = " << t.n(1) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << t.n(2) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << t.n(3) << ";" << std::endl;
    fout << "Transfinite Line {L_4} = " << t.n(4) << ";" << std::endl;
    fout << "Transfinite Line {L_5} = " << t.n(5) << ";" << std::endl;
    fout << "Transfinite Line {L_6} = " << t.n(6) << ";" << std::endl;
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a hexahedron in a geo file
void saveHexahedronToGeo(const Hexahedron& h, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=h.buildSideNamesAfterCheck(6);
  bool isTransfinite=true;
  if (h.h().size() == 8) { isTransfinite=false; }

  fout << "x1=" << h.p(1)[0] << "; y1=";
  if (h.p(1).size() >= 2) { fout << h.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (h.p(1).size() >= 3) { fout << h.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << h.p(2)[0] << "; y2=";
  if (h.p(2).size() >= 2) { fout << h.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (h.p(2).size() >= 3) { fout << h.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << h.p(3)[0] << "; y3=";
  if (h.p(3).size() >= 2) { fout << h.p(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (h.p(3).size() >= 3) { fout << h.p(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x4=" << h.p(4)[0] << "; y4=";
  if (h.p(4).size() >= 2) { fout << h.p(4)[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (h.p(4).size() >= 3) { fout << h.p(4)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x5=" << h.p(5)[0] << "; y5=";
  if (h.p(5).size() >= 2) { fout << h.p(5)[1]; }
  else { fout << "0"; }
  fout << "; z5=";
  if (h.p(5).size() >= 3) { fout << h.p(5)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x6=" << h.p(6)[0] << "; y6=";
  if (h.p(6).size() >= 2) { fout << h.p(6)[1]; }
  else { fout << "0"; }
  fout << "; z6=";
  if (h.p(6).size() >= 3) { fout << h.p(6)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x7=" << h.p(7)[0] << "; y7=";
  if (h.p(7).size() >= 2) { fout << h.p(7)[1]; }
  else { fout << "0"; }
  fout << "; z7=";
  if (h.p(7).size() >= 3) { fout << h.p(7)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x8=" << h.p(8)[0] << "; y8=";
  if (h.p(8).size() >= 2) { fout << h.p(8)[1]; }
  else { fout << "0"; }
  fout << "; z8=";
  if (h.p(8).size() >= 3) { fout << h.p(8)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite) { fout << "h1=h0; h2=h0; h3=h0; h4=h0; h5=h0; h6=h0; h7=h0; h8=h0;" << std::endl << std::endl; }
  else
  {
    fout << "h1=" << h.h(1) << "; h2=" << h.h(2) << "; h3=" << h.h(3) << "; h4=" << h.h(4) << "; ";
    fout << "h5=" << h.h(5) << "; h6=" << h.h(6) << "; h7=" << h.h(7) << "; h8=" << h.h(8) << ";" << std::endl << std::endl;
  }

  fout << "Call xlifepp_Hexahedron;" << std::endl << std::endl;

  if (isTransfinite)
  {
    fout << "Transfinite Line {L_1} = " << h.n(1) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << h.n(2) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << h.n(3) << ";" << std::endl;
    fout << "Transfinite Line {L_4} = " << h.n(4) << ";" << std::endl;
    fout << "Transfinite Line {L_5} = " << h.n(5) << ";" << std::endl;
    fout << "Transfinite Line {L_6} = " << h.n(6) << ";" << std::endl;
    fout << "Transfinite Line {L_7} = " << h.n(7) << ";" << std::endl;
    fout << "Transfinite Line {L_8} = " << h.n(8) << ";" << std::endl;
    fout << "Transfinite Line {L_9} = " << h.n(9) << ";" << std::endl;
    fout << "Transfinite Line {L_10} = " << h.n(10) << ";" << std::endl;
    fout << "Transfinite Line {L_11} = " << h.n(11) << ";" << std::endl;
    fout << "Transfinite Line {L_12} = " << h.n(12) << ";" << std::endl;
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a parallelepiped, a cuboid or a cube in a geo file
void saveParallelepipedToGeo(const Parallelepiped& p, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                         bool withLoopsStorage, bool withSideNames)
{
  number_t nbSides=6;
  number_t nbOctants=p.nbOctants();
  bool octants36=false, octants57=false;
  switch (nbOctants)
  {
    case 7:
    case 5:
      nbSides=9;
      octants57=true;
      break;
    case 6:
    case 3:
      nbSides=8;
      octants36=true;
      break;
    default:
      break;
  }
  Strings sidenames=p.buildSideNamesAfterCheck(nbSides);
  bool isTransfinite=true;
  if (p.h().size() == 8) { isTransfinite=false; }

  fout << "x1=" << p.p(1)[0] << "; y1=";
  if (p.p(1).size() >= 2) { fout << p.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (p.p(1).size() >= 3) { fout << p.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << p.p(2)[0] << "; y2=";
  if (p.p(2).size() >= 2) { fout << p.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (p.p(2).size() >= 3) { fout << p.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << p.p(3)[0] << "; y3=";
  if (p.p(3).size() >= 2) { fout << p.p(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (p.p(3).size() >= 3) { fout << p.p(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x4=" << p.p(4)[0] << "; y4=";
  if (p.p(4).size() >= 2) { fout << p.p(4)[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (p.p(4).size() >= 3) { fout << p.p(4)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x5=" << p.p(5)[0] << "; y5=";
  if (p.p(5).size() >= 2) { fout << p.p(5)[1]; }
  else { fout << "0"; }
  fout << "; z5=";
  if (p.p(5).size() >= 3) { fout << p.p(5)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x6=" << p.p(6)[0] << "; y6=";
  if (p.p(6).size() >= 2) { fout << p.p(6)[1]; }
  else { fout << "0"; }
  fout << "; z6=";
  if (p.p(6).size() >= 3) { fout << p.p(6)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x7=" << p.p(7)[0] << "; y7=";
  if (p.p(7).size() >= 2) { fout << p.p(7)[1]; }
  else { fout << "0"; }
  fout << "; z7=";
  if (p.p(7).size() >= 3) { fout << p.p(7)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x8=" << p.p(8)[0] << "; y8=";
  if (p.p(8).size() >= 2) { fout << p.p(8)[1]; }
  else { fout << "0"; }
  fout << "; z8=";
  if (p.p(8).size() >= 3) { fout << p.p(8)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (octants36 || octants57)
  {
    fout << "x9=" << p.p(9)[0] << "; y9=";
    if (p.p(9).size() >= 2) { fout << p.p(9)[1]; }
    else { fout << "0"; }
    fout << "; z9=";
    if (p.p(9).size() >= 3) { fout << p.p(9)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x10=" << p.p(10)[0] << "; y10=";
    if (p.p(10).size() >= 2) { fout << p.p(10)[1]; }
    else { fout << "0"; }
    fout << "; z10=";
    if (p.p(10).size() >= 3) { fout << p.p(10)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x11=" << p.p(11)[0] << "; y11=";
    if (p.p(11).size() >= 2) { fout << p.p(11)[1]; }
    else { fout << "0"; }
    fout << "; z11=";
    if (p.p(11).size() >= 3) { fout << p.p(11)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x12=" << p.p(12)[0] << "; y12=";
    if (p.p(12).size() >= 2) { fout << p.p(12)[1]; }
    else { fout << "0"; }
    fout << "; z12=";
    if (p.p(12).size() >= 3) { fout << p.p(12)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;
  }

  if (octants57)
  {
    fout << "x13=" << p.p(13)[0] << "; y13=";
    if (p.p(13).size() >= 2) { fout << p.p(13)[1]; }
    else { fout << "0"; }
    fout << "; z13=";
    if (p.p(13).size() >= 3) { fout << p.p(13)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;

    fout << "x14=" << p.p(14)[0] << "; y14=";
    if (p.p(14).size() >= 2) { fout << p.p(14)[1]; }
    else { fout << "0"; }
    fout << "; z14=";
    if (p.p(14).size() >= 3) { fout << p.p(14)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;
  }

  if (isTransfinite)
  {
    fout << "h1=h0; h2=h0; h3=h0; h4=h0; h5=h0; h6=h0; h7=h0; h8=h0;";
    if (octants36 || octants57)
    {
      fout << " h9=h0; h10=h0; h11=h0; h12=h0;";
    }
    if (octants57)
    {
      fout << " h13=h0; h14=h0;";
    }
    fout << std::endl << std::endl;
  }
  else
  {
    fout << "h1=" << p.h(1) << "; h2=" << p.h(2) << "; h3=" << p.h(3) << "; h4=" << p.h(4) << "; ";
    fout << "h5=" << p.h(5) << "; h6=" << p.h(6) << "; h7=" << p.h(7) << "; h8=" << p.h(8) << ";";
    if (octants36 || octants57)
    {
      fout << "h9=" << p.h(9) << "; h10=" << p.h(10) << "; h11=" << p.h(11) << "; h12=" << p.h(12) << ";";
    }
    if (octants57)
    {
      fout << "h13=" << p.h(13) << "; h14=" << p.h(14) << ";";
    }
    fout << std::endl << std::endl;
  }

  switch (nbOctants)
  {
    case 7:
      fout << "Call xlifepp_Parallelepiped7;" << std::endl << std::endl;
      break;
    case 6:
      fout << "Call xlifepp_Parallelepiped6;" << std::endl << std::endl;
      break;
    case 5:
      fout << "Call xlifepp_Parallelepiped5;" << std::endl << std::endl;
      break;
    case 3:
      fout << "Call xlifepp_Parallelepiped3;" << std::endl << std::endl;
      break;
    default:
      fout << "Call xlifepp_Hexahedron;" << std::endl << std::endl;
      break;
  }

  if (isTransfinite)
  {
    fout << "Transfinite Line {L_1} = " << p.n(1) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << p.n(2) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << p.n(3) << ";" << std::endl;
    fout << "Transfinite Line {L_4} = " << p.n(4) << ";" << std::endl;
    fout << "Transfinite Line {L_5} = " << p.n(5) << ";" << std::endl;
    fout << "Transfinite Line {L_6} = " << p.n(6) << ";" << std::endl;
    fout << "Transfinite Line {L_7} = " << p.n(7) << ";" << std::endl;
    fout << "Transfinite Line {L_8} = " << p.n(8) << ";" << std::endl;
    fout << "Transfinite Line {L_9} = " << p.n(9) << ";" << std::endl;
    fout << "Transfinite Line {L_10} = " << p.n(10) << ";" << std::endl;
    fout << "Transfinite Line {L_11} = " << p.n(11) << ";" << std::endl;
    fout << "Transfinite Line {L_12} = " << p.n(12) << ";" << std::endl;
    if (octants36 || octants57)
    {
      fout << "Transfinite Line {L_13} = " << p.n(13) << ";" << std::endl;
      fout << "Transfinite Line {L_14} = " << p.n(14) << ";" << std::endl;
      fout << "Transfinite Line {L_15} = " << p.n(15) << ";" << std::endl;
      fout << "Transfinite Line {L_16} = " << p.n(16) << ";" << std::endl;
      fout << "Transfinite Line {L_17} = " << p.n(17) << ";" << std::endl;
      fout << "Transfinite Line {L_18} = " << p.n(18) << ";" << std::endl;
    }
    if (octants57)
    {
      fout << "Transfinite Line {L_19} = " << p.n(19) << ";" << std::endl;
      fout << "Transfinite Line {L_20} = " << p.n(20) << ";" << std::endl;
      fout << "Transfinite Line {L_21} = " << p.n(21) << ";" << std::endl;
    }
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing an ellipsoidal volume or a ball in a geo file
void saveEllipsoidToGeo(const Ellipsoid& e, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                        bool withLoopsStorage, bool withSideNames)
{
  number_t nbSides=8;
  number_t nbOctants=e.nbOctants();
  if (nbOctants == 0) { nbOctants=8; }
  switch (nbOctants)
  {
    case 7:
    case 6:
    case 5:
      nbSides=10;
      break;
    case 2:
      nbSides=6;
      break;
    case 1:
      nbSides=4;
      break;
    default:
      break;
  }
  Strings sidenames=e.buildSideNamesAfterCheck(nbSides);
  bool isTransfinite=true;
  if (nbOctants == 8)
  {
    if (e.h().size() == 6) { isTransfinite=false; }
  }
  else
  {
    if (e.h().size() == e.v().size()) { isTransfinite=false; }
  }

  fout << "x1=" << e.v(1)[0] << "; y1=";
  if (e.v(1).size() >= 2) { fout << e.v(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (e.v(1).size() >= 3) { fout << e.v(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << e.v(2)[0] << "; y2=";
  if (e.v(2).size() >= 2) { fout << e.v(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (e.v(2).size() >= 3) { fout << e.v(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << e.v(3)[0] << "; y3=";
  if (e.v(3).size() >= 2) { fout << e.v(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (e.v(3).size() >= 3) { fout << e.v(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x4=" << e.v(4)[0] << "; y4=";
  if (e.v(4).size() >= 2) { fout << e.v(4)[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (e.v(4).size() >= 3) { fout << e.v(4)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (nbOctants >= 2)
  {
    fout << "x5=" << e.v(5)[0] << "; y5=";
    if (e.v(5).size() >= 2) { fout << e.v(5)[1]; }
    else { fout << "0"; }
    fout << "; z5=";
    if (e.v(5).size() >= 3) { fout << e.v(5)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;
  }

  if (nbOctants >= 3)
  {
    fout << "x6=" << e.v(6)[0] << "; y6=";
    if (e.v(6).size() >= 2) { fout << e.v(6)[1]; }
    else { fout << "0"; }
    fout << "; z6=";
    if (e.v(6).size() >= 3) { fout << e.v(6)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;
  }

  if (nbOctants >= 5)
  {
    fout << "x7=" << e.v(7)[0] << "; y7=";
    if (e.v(7).size() >= 2) { fout << e.v(7)[1]; }
    else { fout << "0"; }
    fout << "; z7=";
    if (e.v(7).size() >= 3) { fout << e.v(7)[2]; }
    else { fout << "0"; }
    fout << ";" << std::endl;
  }

  if (isTransfinite)
  {
    fout << "h1=h0; h2=h0; h3=h0; h4=h0;";
    if (nbOctants >= 2) { fout << " h5=h0;"; }
    if (nbOctants >= 3) { fout << " h6=h0;"; }
    if (nbOctants >= 5) { fout << " h7=h0;";}
    fout << std::endl << std::endl;
  }
  else
  {
    if (nbOctants == 8)
    {
      fout << "h1=h0; h2=" << e.h(1) << "; h3=" << e.h(2) << "; h4=" << e.h(3) << "; h5=" << e.h(4) << "; ";
      fout << "h6=" << e.h(5) << "; h7=" << e.h(6) << ";" << std::endl << std::endl;
    }
    else
    {
      fout << "h1=" << e.h(1) << "; h2=" << e.h(2) << "; h3=" << e.h(3) << "; h4=" << e.h(4) << ";";
      if (nbOctants >= 2) { fout << " h5=" << e.h(5) << ";"; }
      if (nbOctants >= 3) { fout << " h6=" << e.h(6) << ";"; }
      if (nbOctants >= 5) { fout << " h7=" << e.h(7) << ";"; }
      fout << std::endl << std::endl;
    }
  }

  switch (nbOctants)
  {
    case 7:
      fout << "Call xlifepp_Ellipsoid7;" << std::endl << std::endl;
      break;
    case 6:
      fout << "Call xlifepp_Ellipsoid6;" << std::endl << std::endl;
      break;
    case 5:
      fout << "Call xlifepp_Ellipsoid5;" << std::endl << std::endl;
      break;
    case 4:
      fout << "Call xlifepp_Ellipsoid4;" << std::endl << std::endl;
      break;
    case 3:
      fout << "Call xlifepp_Ellipsoid3;" << std::endl << std::endl;
      break;
    case 2:
      fout << "Call xlifepp_Ellipsoid2;" << std::endl << std::endl;
      break;
    case 1:
      fout << "Call xlifepp_Ellipsoid1;" << std::endl << std::endl;
      break;
    default:
      fout << "Call xlifepp_Ellipsoid;" << std::endl << std::endl;
      break;
  }

  if (isTransfinite)
  {
    fout << "Transfinite Line {E_1} = " << e.n(1) << ";" << std::endl;
    fout << "Transfinite Line {E_2} = " << e.n(2) << ";" << std::endl;
    fout << "Transfinite Line {E_3} = " << e.n(3) << ";" << std::endl;
    if (nbOctants >= 2)
    {
      fout << "Transfinite Line {E_4} = " << e.n(4) << ";" << std::endl;
      fout << "Transfinite Line {E_5} = " << e.n(5) << ";" << std::endl;
    }
    if (nbOctants >= 3)
    {
      fout << "Transfinite Line {E_6} = " << e.n(6) << ";" << std::endl;
      fout << "Transfinite Line {E_7} = " << e.n(7) << ";" << std::endl;
    }
    if (nbOctants >= 4)
    {
      fout << "Transfinite Line {E_8} = " << e.n(8) << ";" << std::endl;
    }
    if (nbOctants >= 5)
    {
      fout << "Transfinite Line {E_9} = " << e.n(9) << ";" << std::endl;
      fout << "Transfinite Line {E_10} = " << e.n(10) << ";" << std::endl;
    }
    if (nbOctants >= 6)
    {
      fout << "Transfinite Line {E_11} = " << e.n(11) << ";" << std::endl;
    }
    if (nbOctants >= 7)
    {
      fout << "Transfinite Line {E_12} = " << e.n(12) << ";" << std::endl;
    }
    switch(nbOctants)
    {
      case 7:
        fout << "Transfinite Line {L_1} = " << e.n(13) << ";" << std::endl;
        fout << "Transfinite Line {L_2} = " << e.n(14) << ";" << std::endl;
        fout << "Transfinite Line {L_3} = " << e.n(15) << ";" << std::endl;
        break;
      case 6:
        fout << "Transfinite Line {L_1} = " << e.n(12) << ";" << std::endl;
        fout << "Transfinite Line {L_2} = " << e.n(13) << ";" << std::endl;
        fout << "Transfinite Line {L_3} = " << e.n(14) << ";" << std::endl;
        fout << "Transfinite Line {L_4} = " << e.n(15) << ";" << std::endl;
        break;
      case 5:
        fout << "Transfinite Line {L_1} = " << e.n(11) << ";" << std::endl;
        fout << "Transfinite Line {L_2} = " << e.n(12) << ";" << std::endl;
        fout << "Transfinite Line {L_3} = " << e.n(13) << ";" << std::endl;
        fout << "Transfinite Line {L_4} = " << e.n(14) << ";" << std::endl;
        fout << "Transfinite Line {L_5} = " << e.n(15) << ";" << std::endl;
        break;
      case 4:
        fout << "Transfinite Line {L_1} = " << e.n(9) << ";" << std::endl;
        fout << "Transfinite Line {L_2} = " << e.n(10) << ";" << std::endl;
        fout << "Transfinite Line {L_3} = " << e.n(11) << ";" << std::endl;
        fout << "Transfinite Line {L_4} = " << e.n(12) << ";" << std::endl;
        break;
      case 3:
        fout << "Transfinite Line {L_1} = " << e.n(8) << ";" << std::endl;
        fout << "Transfinite Line {L_2} = " << e.n(9) << ";" << std::endl;
        fout << "Transfinite Line {L_3} = " << e.n(10) << ";" << std::endl;
        fout << "Transfinite Line {L_4} = " << e.n(11) << ";" << std::endl;
        fout << "Transfinite Line {L_5} = " << e.n(12) << ";" << std::endl;
        break;
      case 2:
        fout << "Transfinite Line {L_1} = " << e.n(6) << ";" << std::endl;
        fout << "Transfinite Line {L_2} = " << e.n(7) << ";" << std::endl;
        fout << "Transfinite Line {L_3} = " << e.n(8) << ";" << std::endl;
        fout << "Transfinite Line {L_4} = " << e.n(9) << ";" << std::endl;
        break;
      case 1:
        fout << "Transfinite Line {L_1} = " << e.n(4) << ";" << std::endl;
        fout << "Transfinite Line {L_2} = " << e.n(5) << ";" << std::endl;
        fout << "Transfinite Line {L_3} = " << e.n(6) << ";" << std::endl;
        break;
      default:
        break;
    }
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a cylindrical volume in a geo file
void saveTrunkToGeo(const Trunk& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                    bool withLoopsStorage, bool withSideNames)
{
  // print first basis according to shape
  switch (t.basis()->shape())
  {
    case _polygon:
    case _triangle: saveTriangleToGeo(*t.basis()->triangle(), sh, fout, pids); break;
    case _quadrangle: saveQuadrangleToGeo(*t.basis()->quadrangle(), sh, fout, pids); break;
    case _parallelogram: saveQuadrangleToGeo(*t.basis()->parallelogram(), sh, fout, pids); break;
    case _rectangle: saveQuadrangleToGeo(*t.basis()->rectangle(), sh, fout, pids); break;
    case _square: saveQuadrangleToGeo(*t.basis()->square(), sh, fout, pids); break;
    case _ellipse: saveEllipseToGeo(*t.basis()->ellipse(), sh, fout, pids); break;
    case _disk: saveEllipseToGeo(*t.basis()->disk(), sh, fout, pids); break;
    default:
    {
      where("saveTrunkToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",t.basis()->shape()), t.basis()->domName());
      break;
    }
  }
  fout << "S_1=news;" << std::endl;
  fout << "Plane Surface(S_1)={LL_1};" << std::endl;

  // number of curve elements of basis boundary = number of boundary vertices (polygon-like)
  number_t nblc=t.basis()->boundNodes().size();
  Strings sidenames=t.buildSideNamesAfterCheck(nblc+2);

  // print second basis according to shape (second basis is generated by translation of first basis)
  Surface* basis2=t.basis()->cloneS();
  basis2->translate(_direction=std::vector<real_t>(t.origin()-t.basis()->p(1)));
  basis2->homothetize(_center=t.origin(), _scale=t.scale());

  if (t.basis()->h().size() == 0) { for (number_t i=0; i < nblc; ++i) { basis2->n(i+1)=t.n(i+1+nblc); } }
  else { for (number_t i=0; i < nblc; ++i) { basis2->h(i+1)=t.h(i+1+nblc); } }

  switch (t.basis()->shape())
  {
    case _polygon:
    case _triangle: saveTriangleToGeo(*basis2->triangle(), sh, fout, pids); break;
    case _quadrangle: saveQuadrangleToGeo(*basis2->quadrangle(), sh, fout, pids); break;
    case _parallelogram: saveQuadrangleToGeo(*basis2->parallelogram(), sh, fout, pids); break;
    case _rectangle: saveQuadrangleToGeo(*basis2->rectangle(), sh, fout, pids); break;
    case _square: saveQuadrangleToGeo(*basis2->square(), sh, fout, pids); break;
    case _ellipse: saveEllipseToGeo(*basis2->ellipse(), sh, fout, pids); break;
    case _disk: saveEllipseToGeo(*basis2->disk(), sh, fout, pids); break;
    default:
    {
      where("saveTrunkToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",t.basis()->shape()), t.basis()->domName());
      break;
    }
  }
  fout << "S_2=news;" << std::endl;
  fout << "Plane Surface(S_2)={LL_1};" << std::endl;

  // print lateral surfaces
  // as basis is a closed planed surface, its boundary behaves like a polygon
  // that is to say number of boundary curves is equal to number of boundary vertices
  for (number_t i=0; i < nblc; ++i)
  {
    fout << "L_" << i+1 << "=newl;" << std::endl;
    fout << "Line(L_" << i+1 << ")={points_" << i << ", points_" << i+nblc << "};" << std::endl;
  }
  // we build boundaries of lateral surfaces
  for (number_t i=0; i < nblc; ++i)
  {
    fout << "LL_" << i+1 << "=newll;" << std::endl;
    if (i==nblc-1) { fout << "Line Loop(LL_" << i+1 << ")={curves_" << i << ", L_1, -curves_" << i+nblc << ", -L_" << i+1 << "};" << std::endl; }
    else { fout << "Line Loop(LL_" << i+1 << ")={curves_" << i << ", L_" << i+2 << ", -curves_" << i+nblc << ", -L_" << i+1 << "};" << std::endl; }
    fout << "S_" << i+3 << "=news;" << std::endl;
    fout << "Ruled Surface(S_" << i+3 << ")={LL_" << i+1 << "};" << std::endl;
    fout << "surfs~{its}=S_" << i+3 << ";" << std::endl;
    fout << "its++;" << std::endl;
  }
  fout << "SL_1=newsl;" << std::endl;
  fout << "loops~{l}=SL_1;" << std::endl;
  fout << "Surface Loop(SL_1)={S_1";
  for (number_t i=1; i < nblc+2; ++i)
  {
    fout << ",S_" << i+1;
  }
  fout << "};" << std::endl;
  for (number_t i=0; i < nblc; ++i) { fout << "Transfinite Line {L_"<< i+1 << "} = " << t.n(i+2*nblc+1) << ";" << std::endl; }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a cylindrical volume in a geo file
void saveCylinderToGeo(const Cylinder& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                       bool withLoopsStorage, bool withSideNames)
{
  // print first basis according to shape
  switch (c.basis()->shape())
  {
    case _polygon: savePolygonToGeo(*c.basis()->polygon(), sh, fout, pids); break;
    case _triangle: saveTriangleToGeo(*c.basis()->triangle(), sh, fout, pids); break;
    case _quadrangle: saveQuadrangleToGeo(*c.basis()->quadrangle(), sh, fout, pids); break;
    case _parallelogram: saveQuadrangleToGeo(*c.basis()->parallelogram(), sh, fout, pids); break;
    case _rectangle: saveQuadrangleToGeo(*c.basis()->rectangle(), sh, fout, pids); break;
    case _square: saveQuadrangleToGeo(*c.basis()->square(), sh, fout, pids); break;
    case _ellipse: saveEllipseToGeo(*c.basis()->ellipse(), sh, fout, pids); break;
    case _disk: saveEllipseToGeo(*c.basis()->disk(), sh, fout, pids); break;
    default:
    {
      where("saveCylinderToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",c.basis()->shape()), c.basis()->domName());
      break;
    }
  }
  fout << "S_1=news;" << std::endl;
  fout << "Plane Surface(S_1)={LL_1};" << std::endl;

  // number of curve elements of basis boundary = number of boundary vertices (polygon-like)
  number_t nblc=c.basis()->boundNodes().size();
  Strings sidenames=c.buildSideNamesAfterCheck(nblc+2);

  // print second basis according to shape (second basis is generated by translation of first basis)
  Surface* basis2=c.basis()->cloneS();
  basis2->translate(_direction=c.dir());

  if (c.basis()->h().size() == 0) { for (number_t i=0; i < nblc; ++i) { basis2->n(i+1)=c.n(i+1+nblc); } }
  else { for (number_t i=0; i < nblc; ++i) { basis2->h(i+1)=c.h(i+1+nblc); } }

  switch (c.basis()->shape())
  {
    case _polygon: savePolygonToGeo(*basis2->polygon(), sh, fout, pids); break;
    case _triangle: saveTriangleToGeo(*basis2->triangle(), sh, fout, pids); break;
    case _quadrangle: saveQuadrangleToGeo(*basis2->quadrangle(), sh, fout, pids); break;
    case _parallelogram: saveQuadrangleToGeo(*basis2->parallelogram(), sh, fout, pids); break;
    case _rectangle: saveQuadrangleToGeo(*basis2->rectangle(), sh, fout, pids); break;
    case _square: saveQuadrangleToGeo(*basis2->square(), sh, fout, pids); break;
    case _ellipse: saveEllipseToGeo(*basis2->ellipse(), sh, fout, pids); break;
    case _disk: saveEllipseToGeo(*basis2->disk(), sh, fout, pids); break;
    default:
    {
      where("saveCylinderToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",c.basis()->shape()), c.basis()->domName());
      break;
    }
  }
  fout << "S_2=news;" << std::endl;
  fout << "Plane Surface(S_2)={LL_1};" << std::endl;

  // print lateral surfaces
  // as basis is a closed planed surface, its boundary behaves like a polygon
  // that is to say number of boundary curves is equal to number of boundary vertices
  for (number_t i=0; i < nblc; ++i)
  {
    fout << "L_" << i+1 << "=newl;" << std::endl;
    fout << "Line(L_" << i+1 << ")={points_" << i << ", points_" << i+nblc << "};" << std::endl;
  }
  // we build boundaries of lateral surfaces
  for (number_t i=0; i < nblc; ++i)
  {
    fout << "LL_" << i+1 << "=newll;" << std::endl;
    if (i==nblc-1) { fout << "Line Loop(LL_" << i+1 << ")={curves_" << i << ", L_1, -curves_" << i+nblc << ", -L_" << i+1 << "};" << std::endl; }
    else { fout << "Line Loop(LL_" << i+1 << ")={curves_" << i << ", L_" << i+2 << ", -curves_" << i+nblc << ", -L_" << i+1 << "};" << std::endl; }
    fout << "S_" << i+3 << "=news;" << std::endl;
    fout << "Ruled Surface(S_" << i+3 << ")={LL_" << i+1 << "};" << std::endl;
    fout << "surfs~{its}=S_" << i+3 << ";" << std::endl;
    fout << "its++;" << std::endl;
  }
  fout << "SL_1=newsl;" << std::endl;
  fout << "loops~{l}=SL_1;" << std::endl;
  fout << "Surface Loop(SL_1)={S_1";
  for (number_t i=1; i < nblc+2; ++i)
  {
    fout << ",S_" << i+1;
  }
  fout << "};" << std::endl;
  for (number_t i=0; i < nblc; ++i) { fout << "Transfinite Line {L_"<< i+1 << "} = " << c.n(i+2*nblc+1) << ";" << std::endl; }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a conical volume in a geo file
void saveConeToGeo(const Cone& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                   bool withLoopsStorage, bool withSideNames)
{
  // print first basis according to shape:
  // ellipse and disk are excluded as gmsh is not able to define lateral surfaces of a cone properly
  switch (c.basis()->shape())
  {
    case _polygon:
    case _triangle: saveTriangleToGeo(*c.basis()->triangle(), sh, fout, pids); break;
    case _quadrangle: saveQuadrangleToGeo(*c.basis()->quadrangle(), sh, fout, pids); break;
    case _parallelogram: saveQuadrangleToGeo(*c.basis()->parallelogram(), sh, fout, pids); break;
    case _rectangle: saveQuadrangleToGeo(*c.basis()->rectangle(), sh, fout, pids); break;
    case _square: saveQuadrangleToGeo(*c.basis()->square(), sh, fout, pids); break;
    case _ellipse:
    case _disk: where("saveConeToGeo(Cone,...)"); error("shape_not_handled",words("shape",c.basis()->shape()));
    default:
    {
      where("saveConeToGeo(...)");
      error("gmsh_shape_not_handled", words("shape",c.basis()->shape()), c.basis()->domName());
      break;
    }
  }
  fout << "S_1=news;" << std::endl;
  fout << "Plane Surface(S_1)={LL_1};" << std::endl;

  // number of curve elements of basis boundary = number of boundary vertices (polygon-like)
  number_t nblc=c.basis()->boundNodes().size();
  Strings sidenames=c.buildSideNamesAfterCheck(nblc+1);

  // print apex
  fout << "P_5=newp;" << std::endl;
  fout << "x5=" << c.apex()[0] << "; y5=";
  if (c.apex().size() >= 2) { fout << c.apex()[1]; }
  else { fout << "0"; }
  fout << "; z5=";
  if (c.apex().size() >= 3) { fout << c.apex()[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  bool isTransfinite = c.basis()->h().size() == 0;
  if (isTransfinite) { fout << "h5=h0;" << std::endl; }
  else { fout << "h5=" << c.h(c.basis()->h().size()+1) << ";" << std::endl; }

  fout << "Point(P_5)={x5,y5,z5,h5};" << std::endl;
  fout << "points~{itp}=P_5;" << std::endl;
  fout << "itp++;" << std::endl;

  // print lateral surfaces
  // as basis is a closed planed surface, its boundary behaves like a polygon
  // that is to say number of boundary curves is equal to number of boundary vertices
  for (number_t i=0; i < nblc; ++i)
  {
    fout << "L_" << i+1 << "=newl;" << std::endl;
    fout << "Line(L_" << i+1 << ")={points_" << i << ", P_5};" << std::endl;
  }
  // we build boundaries of lateral surfaces
  for (number_t i=0; i < nblc; ++i)
  {
    fout << "LL_" << i+1 << "=newll;" << std::endl;
    if (i==nblc-1) { fout << "Line Loop(LL_" << i+1 << ")={curves_" << i << ", L_1, -L_" << i+1 << "};" << std::endl; }
    else { fout << "Line Loop(LL_" << i+1 << ")={curves_" << i << ", L_" << i+2 << ", -L_" << i+1 << "};" << std::endl; }
    fout << "S_" << i+2 << "=news;" << std::endl;
    fout << "Ruled Surface(S_" << i+2 << ")={LL_" << i+1 << "};" << std::endl;
    fout << "surfs~{its}=S_" << i+2 << ";" << std::endl;
    fout << "its++;" << std::endl;
  }
  fout << "SL_1=newsl;" << std::endl;
  fout << "loops~{l}=SL_1;" << std::endl;
  fout << "Surface Loop(SL_1)={S_1";
  for (number_t i=1; i < nblc+1; ++i)
  {
    fout << ",S_" << i+1;
  }
  fout << "};" << std::endl;
  if (isTransfinite)
  { for (number_t i=0; i < nblc; ++i) { fout << "Transfinite Line {L_"<< i+1 << "} = " << c.n(i+nblc+1) << ";" << std::endl; } }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a prism in a geo file
void saveRevTrunkToGeo(const RevTrunk& t, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                       bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=t.buildSideNamesAfterCheck(6);
  bool isTransfinite=true;
  if (t.h().size() == 8) { isTransfinite=false; }

  fout << "x1=" << t.p(1)[0] << "; y1=";
  if (t.p(1).size() >= 2) { fout << t.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (t.p(1).size() >= 3) { fout << t.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << t.p(2)[0] << "; y2=";
  if (t.p(2).size() >= 2) { fout << t.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (t.p(2).size() >= 3) { fout << t.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << t.p(3)[0] << "; y3=";
  if (t.p(3).size() >= 2) { fout << t.p(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (t.p(3).size() >= 3) { fout << t.p(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x4=" << t.p(4)[0] << "; y4=";
  if (t.p(4).size() >= 2) { fout << t.p(4)[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (t.p(4).size() >= 3) { fout << t.p(4)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x5=" << t.p(5)[0] << "; y5=";
  if (t.p(5).size() >= 2) { fout << t.p(5)[1]; }
  else { fout << "0"; }
  fout << "; z5=";
  if (t.p(5).size() >= 3) { fout << t.p(5)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x6=" << t.p(6)[0] << "; y6=";
  if (t.p(6).size() >= 2) { fout << t.p(6)[1]; }
  else { fout << "0"; }
  fout << "; z6=";
  if (t.p(6).size() >= 3) { fout << t.p(6)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x7=" << t.p(7)[0] << "; y7=";
  if (t.p(7).size() >= 2) { fout << t.p(7)[1]; }
  else { fout << "0"; }
  fout << "; z7=";
  if (t.p(7).size() >= 3) { fout << t.p(7)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x8=" << t.p(8)[0] << "; y8=";
  if (t.p(8).size() >= 2) { fout << t.p(8)[1]; }
  else { fout << "0"; }
  fout << "; z8=";
  if (t.p(8).size() >= 3) { fout << t.p(8)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x9=" << t.p(9)[0] << "; y9=";
  if (t.p(9).size() >= 2) { fout << t.p(9)[1]; }
  else { fout << "0"; }
  fout << "; z9=";
  if (t.p(9).size() >= 3) { fout << t.p(9)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x10=" << t.p(10)[0] << "; y10=";
  if (t.p(10).size() >= 2) { fout << t.p(10)[1]; }
  else { fout << "0"; }
  fout << "; z10=";
  if (t.p(10).size() >= 3) { fout << t.p(10)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite)
  { fout << "h1=h0; h2=h0; h3=h0; h4=h0; h5=h0; h6=h0; h7=h0; h8=h0; h9=h0; h10=h0;" << std::endl << std::endl; }
  else
  {
    fout << "h1=h0; h2=" << t.h(1) << "; h3=" << t.h(2) << "; h4=" << t.h(3) << "; h5=" << t.h(4) << "; ";
    fout << "h6=h0; h7=" << t.h(5) << "; h8=" << t.h(6) << "; h9=" << t.h(7) << "; h10=" << t.h(8) << ";" << std::endl << std::endl;
  }

  fout << "Call xlifepp_RevTrunk;" << std::endl << std::endl;

  if (isTransfinite)
  {
    fout << "Transfinite Line {E_1} = " << t.n(1) << ";" << std::endl;
    fout << "Transfinite Line {E_2} = " << t.n(2) << ";" << std::endl;
    fout << "Transfinite Line {E_3} = " << t.n(3) << ";" << std::endl;
    fout << "Transfinite Line {E_4} = " << t.n(4) << ";" << std::endl;
    fout << "Transfinite Line {E_5} = " << t.n(5) << ";" << std::endl;
    fout << "Transfinite Line {E_6} = " << t.n(6) << ";" << std::endl;
    fout << "Transfinite Line {E_7} = " << t.n(7) << ";" << std::endl;
    fout << "Transfinite Line {E_8} = " << t.n(8) << ";" << std::endl;
    fout << "Transfinite Line {L_1} = " << t.n(9) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << t.n(10) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << t.n(11) << ";" << std::endl;
    fout << "Transfinite Line {L_4} = " << t.n(12) << ";" << std::endl;
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a prism in a geo file
void saveRevCylinderToGeo(const RevCylinder& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                          bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=c.buildSideNamesAfterCheck(6);
  bool isTransfinite=true;
  if (c.h().size() == 8) { isTransfinite=false; }

  fout << "x1=" << c.p(1)[0] << "; y1=";
  if (c.p(1).size() >= 2) { fout << c.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (c.p(1).size() >= 3) { fout << c.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << c.p(2)[0] << "; y2=";
  if (c.p(2).size() >= 2) { fout << c.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (c.p(2).size() >= 3) { fout << c.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x3=" << c.p(3)[0] << "; y3=";
  if (c.p(3).size() >= 2) { fout << c.p(3)[1]; }
  else { fout << "0"; }
  fout << "; z3=";
  if (c.p(3).size() >= 3) { fout << c.p(3)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x4=" << c.p(4)[0] << "; y4=";
  if (c.p(4).size() >= 2) { fout << c.p(4)[1]; }
  else { fout << "0"; }
  fout << "; z4=";
  if (c.p(4).size() >= 3) { fout << c.p(4)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x5=" << c.p(5)[0] << "; y5=";
  if (c.p(5).size() >= 2) { fout << c.p(5)[1]; }
  else { fout << "0"; }
  fout << "; z5=";
  if (c.p(5).size() >= 3) { fout << c.p(5)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x6=" << c.p(6)[0] << "; y6=";
  if (c.p(6).size() >= 2) { fout << c.p(6)[1]; }
  else { fout << "0"; }
  fout << "; z6=";
  if (c.p(6).size() >= 3) { fout << c.p(6)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x7=" << c.p(7)[0] << "; y7=";
  if (c.p(7).size() >= 2) { fout << c.p(7)[1]; }
  else { fout << "0"; }
  fout << "; z7=";
  if (c.p(7).size() >= 3) { fout << c.p(7)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x8=" << c.p(8)[0] << "; y8=";
  if (c.p(8).size() >= 2) { fout << c.p(8)[1]; }
  else { fout << "0"; }
  fout << "; z8=";
  if (c.p(8).size() >= 3) { fout << c.p(8)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x9=" << c.p(9)[0] << "; y9=";
  if (c.p(9).size() >= 2) { fout << c.p(9)[1]; }
  else { fout << "0"; }
  fout << "; z9=";
  if (c.p(9).size() >= 3) { fout << c.p(9)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x10=" << c.p(10)[0] << "; y10=";
  if (c.p(10).size() >= 2) { fout << c.p(10)[1]; }
  else { fout << "0"; }
  fout << "; z10=";
  if (c.p(10).size() >= 3) { fout << c.p(10)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  if (isTransfinite)
  { fout << "h1=h0; h2=h0; h3=h0; h4=h0; h5=h0; h6=h0; h7=h0; h8=h0; h9=h0; h10=h0;" << std::endl << std::endl; }
  else
  {
    fout << "h1=h0; h2=" << c.h(1) << "; h3=" << c.h(2) << "; h4=" << c.h(3) << "; h5=" << c.h(4) << "; ";
    fout << "h6=h0; h7=" << c.h(5) << "; h8=" << c.h(6) << "; h9=" << c.h(7) << "; h10=" << c.h(8) << ";" << std::endl << std::endl;
  }

  fout << "Call xlifepp_RevCylinder;" << std::endl << std::endl;

  if (isTransfinite)
  {
    fout << "Transfinite Line {E_1} = " << c.n(1) << ";" << std::endl;
    fout << "Transfinite Line {E_2} = " << c.n(2) << ";" << std::endl;
    fout << "Transfinite Line {E_3} = " << c.n(3) << ";" << std::endl;
    fout << "Transfinite Line {E_4} = " << c.n(4) << ";" << std::endl;
    fout << "Transfinite Line {E_5} = " << c.n(5) << ";" << std::endl;
    fout << "Transfinite Line {E_6} = " << c.n(6) << ";" << std::endl;
    fout << "Transfinite Line {E_7} = " << c.n(7) << ";" << std::endl;
    fout << "Transfinite Line {E_8} = " << c.n(8) << ";" << std::endl;
    fout << "Transfinite Line {L_1} = " << c.n(9) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << c.n(10) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << c.n(11) << ";" << std::endl;
    fout << "Transfinite Line {L_4} = " << c.n(12) << ";" << std::endl;
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

//! writing a revolution conical volume in a geo file
void saveRevConeToGeo(const RevCone& c, ShapeType sh, std::ofstream& fout, std::vector<PhysicalData>& pids,
                      bool withLoopsStorage, bool withSideNames)
{
  Strings sidenames=c.buildSideNamesAfterCheck(5);
  bool isTransfinite=true;
  if (c.h().size() == 5) { isTransfinite=false; }

  // to define a revCone properly for gmsh, we have to use extrusion by rotation of a triangle
  // such that one of its edge is along the revolution axis.

  fout << "x1=" << c.p(1)[0] << "; y1=";
  if (c.p(1).size() >= 2) { fout << c.p(1)[1]; }
  else { fout << "0"; }
  fout << "; z1=";
  if (c.p(1).size() >= 3) { fout << c.p(1)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x2=" << c.p(2)[0] << "; y2=";
  if (c.p(2).size() >= 2) { fout << c.p(2)[1]; }
  else { fout << "0"; }
  fout << "; z2=";
  if (c.p(2).size() >= 3) { fout << c.p(2)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "x6=" << c.p(6)[0] << "; y6=";
  if (c.p(6).size() >= 2) { fout << c.p(6)[1]; }
  else { fout << "0"; }
  fout << "; z6=";
  if (c.p(6).size() >= 3) { fout << c.p(6)[2]; }
  else { fout << "0"; }
  fout << ";" << std::endl;

  fout << "ux=x6-x1; uy=y6-y1; uz=z6-z1;" << std::endl;

  if (isTransfinite) { fout << "h1=h0; h2=h0; h6=h0;" << std::endl << std::endl; }
  else { fout << "h1=h0; h2=" << c.h(1) << "; h6=" << c.h(5) << ";" << std::endl << std::endl; }

  fout << "Call xlifepp_RevCone;" << std::endl << std::endl;

  if(isTransfinite)
  {
    fout << "Transfinite Line {E_1} = " << c.n(1) << ";" << std::endl;
    fout << "Transfinite Line {E_2} = " << c.n(2) << ";" << std::endl;
    fout << "Transfinite Line {E_3} = " << c.n(3) << ";" << std::endl;
    fout << "Transfinite Line {E_4} = " << c.n(4) << ";" << std::endl;
    fout << "Transfinite Line {L_1} = " << c.n(5) << ";" << std::endl;
    fout << "Transfinite Line {L_2} = " << c.n(6) << ";" << std::endl;
    fout << "Transfinite Line {L_3} = " << c.n(7) << ";" << std::endl;
    fout << "Transfinite Line {L_4} = " << c.n(8) << ";" << std::endl;
  }

  if (withSideNames)
  {
    string_t spd = physicalDomain(sidenames,"S", pids);
    if (spd != "") { fout << spd << std::endl; }
  }
  fout << std::endl;
}

} // end of namespace xlifepp
