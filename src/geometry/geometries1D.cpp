/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries1D.cpp
  \authors N. Kielbasiewicz, Y. Lafranche
  \since 18 oct 2012
  \date 29 jul 2015

  \brief This file contains the implementation of methods of classes defined in geometries1D.hpp
*/

#include "geometries1D.hpp"

namespace xlifepp
{

//==========================================================
// Curve and child classes member functions
//===========================================================

//! default curve is inside the bounding box [0,1]
Curve::Curve()
  : Geometry(BoundingBox(0., 1.), 1), isClosed_(false)
{
  n_.resize(1, 2);
}

void Curve::buildParam(const Parameter& p)
{
  trace_p->push("Curve::buildParam");
  ParameterKey key = p.key();

  switch (key)
  {
    case _pk_vertex_names:
    case _pk_side_names:
    {
      if (p.type() == _string) { sideNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { sideNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    default: Geometry::buildParam(p); break;
  }
  for (number_t i = 0; i < sideNames_.size(); ++i)
  {
    if (sideNames_[i].find("#") == 0) { error("domain_name_invalid"); }
    if (sideNames_[i].find(" ") == 0) { error("domain_name_invalid"); }
  }
  trace_p->pop();
}

void Curve::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Curve::buildDefaultParam");
  switch (key)
  {
    case _pk_vertex_names:
    case _pk_side_names: sideNames_.resize(0); break;
    default: Geometry::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Curve::getParamsKeys()
{
  std::set<ParameterKey> params = Geometry::getParamsKeys();
  params.insert(_pk_side_names);
  params.insert(_pk_vertex_names);
  return params;
}

//! compute approximated boundingBox by sampling n values, requires parametrization
void Curve::computeBB()
{
  if (parametrization_ == nullptr) { return; } //no parametrization -> no computation
  number_t n = 1000;
  RealPair ts = parametrization_->bounds(_x1);
  real_t dt = (ts.second - ts.first) / n, t = ts.first;
  number_t d = parametrization_->dim;
  std::vector<RealPair> rps(d, RealPair(theRealMax, -theRealMax));
  for (number_t k = 0; k <= n; k++, t += dt)
  {
    Point P = (*parametrization_)(t);
    for (number_t i = 0; i < d; i++)
    {
      if (P[i] < rps[i].first) { rps[i].first = P[i]; }
      if (P[i] > rps[i].second) { rps[i].second = P[i]; }
    }
  }
  boundingBox = BoundingBox(rps);
}

//! return the list of vertices
std::vector<const Point*> Curve::boundNodes() const
{
  std::vector<const Point*> vs(2);
  vs[0] = &p1_; vs[1] = &p2_;
  return vs;
}

//! return the list of curves
std::vector<std::pair<ShapeType, std::vector<const Point*> > > Curve::curves() const
{
  std::vector<const Point*> vs(2);
  vs[0] = &p1_; vs[1] = &p2_;
  return std::vector<std::pair<ShapeType, std::vector<const Point*> > >(1, std::make_pair(shape_, vs));
}


//==========================================================
// Segment class member functions
//===========================================================

//! default is segment [0,1]
Segment::Segment() : Curve()
{
  p1_ = Point(0.);
  p2_ = Point(1.);
  n_.resize(1, 2);
  shape_ = _segment;
  computeMB();
}

void Segment::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Segment::build");
  shape_ = _segment;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _segment)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must use side_names or vertex_names, not both
    if (key == _pk_vertex_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_vertex_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_vertex_names)); }
    // if user sets v1 or v2, and xmin or xmax, and vice versa it is an error
    if ((key == _pk_xmin || key == _pk_xmax) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_xmin || key == _pk_xmax) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_v1 || key == _pk_v2) && usedParams.find(_pk_xmin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmin)); }
    if ((key == _pk_v1 || key == _pk_v2) && usedParams.find(_pk_xmax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmax)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _vertex_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_vertex_names); }
  if (usedParams.find(_pk_vertex_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // (v1,v2) or (xmin,xmax) has to be set (no default values)
  if (params.find(_pk_v1) != params.end() && params.find(_pk_v2) != params.end() &&
      params.find(_pk_xmin) != params.end() && params.find(_pk_xmax) != params.end())
  { error("param_missing", "v1, v2, xmin, xmax"); }
  if (params.find(_pk_v1) != params.end() && params.find(_pk_v2) == params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end() && params.find(_pk_v1) == params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_xmin) != params.end() && params.find(_pk_xmax) == params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_xmax) != params.end() && params.find(_pk_xmin) == params.end()) { error("param_missing", "xmax"); }

  if (params.find(_pk_xmin) != params.end()) { params.erase(_pk_xmin); params.erase(_pk_xmax); }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); }
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  boundingBox = BoundingBox(p1_, p2_);
  computeMB();
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(Segment(), parametrization_Segment, pars, "segment parametrization");
  parametrization_->setinvParametrization(invParametrization_Segment);
  trace_p->pop();
}

void Segment::buildParam(const Parameter& p)
{
  trace_p->push("Segment::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p1_ = p.get_pt(); break;
        case _integer: p1_ = Point(real_t(p.get_i())); break;
        case _real: p1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p2_ = p.get_pt(); break;
        case _integer: p2_ = Point(real_t(p.get_i())); break;
        case _real: p2_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xmin:
    {
      switch (p.type())
      {
        case _integer: p1_ = Point(real_t(p.get_i())); break;
        case _real: p1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xmax:
    {
      switch (p.type())
      {
        case _integer: p2_ = Point(real_t(p.get_i())); break;
        case _real: p2_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integer:
        {
          number_t n = p.get_n();
          n_[0] = n > 2 ? n : 2;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _integer: h_ = std::vector<real_t>(2, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(2, p.get_r()); break;
        case _realVector:
        {
          h_ = p.get_rv();
          if (h_.size() != 2) { error("bad_size", 2, h_.size()); }
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Curve::buildParam(p); break;
  }
  trace_p->pop();
}

void Segment::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Segment::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_[0] = 2; break;
    default: Curve::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Segment::getParamsKeys()
{
  std::set<ParameterKey> params = Curve::getParamsKeys();
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_xmin);
  params.insert(_pk_xmax);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

Segment::Segment(Parameter p1, Parameter p2) : Curve()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Segment::Segment(Parameter p1, Parameter p2, Parameter p3) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Segment::Segment(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Segment::Segment(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Segment::Segment(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Segment::Segment(const Segment& s) : Curve(s)
{
  p1_ = s.p1_;
  p2_ = s.p2_;
  n_ = s.n_;
  h_ = s.h_;
}

string_t Segment::asString() const
{
  string_t s("Segment (");
  s += p1_.roundToZero().toString() + ", " + p2_.roundToZero().toString() + ")";
  return s;
}

Segment& Segment::reverse()
{
  std::swap(p1_, p2_);
  if (h_.size() == 2) { std::swap(h_[0], h_[1]); }
  return *this;
}

Segment operator~(const Segment& s)
{
  Segment s2 = s;
  s2.reverse();
  return s2;
}

std::vector<Point*> Segment::nodes()
{
  std::vector<Point*> nodes(2);
  nodes[0] = &p1_; nodes[1] = &p2_;
  return nodes;
}

std::vector<const Point*> Segment::nodes() const
{
  std::vector<const Point*> nodes(2);
  nodes[0] = &p1_; nodes[1] = &p2_;
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Segment::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(1);
  curves[0] = std::make_pair(_segment, boundNodes());
  return curves;
}

//! true if current is translated from g, T the translation vector
bool Segment::isTranslated(const Geometry& g, Point& T)
{
  if (g.shape() != _segment) { return false; }
  if (g.dimPoint() != dimPoint()) { return false; }
  T = g.segment()->p1() - p1();
  return (norm(g.segment()->p2() - p2() + T) < theEpsilon);
}

//! parametrization (1-t)*p1+t*p2
Vector<real_t> Segment::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  real_t t = pt[0];
  number_t dim = p1_.size();
  Vector<real_t> res(dim, 0.);
  if (t < -theTolerance || t > 1 + theTolerance) //return void vector
  {
    res.clear();
    return res;
    //error("free_error", "in SegmentArc, parameter " + tostring(t)+" is outside [0,1]");
  }
  t = std::max(0., std::min(1., t)); //to be safe
  switch (d)
  {
    case _id:
      for (number_t k = 0; k < dim; k++) { res[k] = (1 - t) * p1_[k] + t * p2_[k]; }
      break;
    case _d1:
      for (number_t k = 0; k < dim; k++) { res[k] = p2_[k] - p1_[k]; }
      break;
    case _d11:
      break;
    default:
      parfun_error("SegmentArc parametrization", d);
  }
  return res;
}

//!< inverse of parametrization t=(p-p1|p2-p1)/|p2-p1|^2
Vector<real_t> Segment::invParametrization(const Point& p, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("SegmentArc invParametrization", d); }
  Point p1p2 = p2_ - p1_, p1p = p - p1_;
  real_t l2 = dot(p1p2, p1p2), eps = std::max(std::sqrt(l2) * 1.E-6, theTolerance);
  real_t a = norm(crossProduct(p1p2, p1p)), t = dot(p1p, p1p2) / l2;
  if (std::abs(a) > eps || t < -eps || t > 1 + eps) { return Vector<real_t>(0); } // fails, return void vector
  return Vector<real_t>(1, t);
}

//==========================================================
// EllArc class member functions
//==========================================================

//! default ellipse arc is quarter of circle of center (0,0,0) and radius 1
EllArc::EllArc() : Curve(), c_(0., 0.), a_(1., 0.)
{
  p1_ = Point(1., 0.);
  p2_ = Point(0., 1.);
  n_[0] = 2;
  shape_ = _ellArc;
  computeMB();
  computeBB();
}

void EllArc::build(const std::vector<Parameter>& ps)
{
  trace_p->push("EllArc::build");
  shape_ = _ellArc;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _ellArc)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must use side_names or vertex_names, not both
    if (key == _pk_vertex_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_vertex_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_vertex_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _vertex_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_vertex_names); }
  if (usedParams.find(_pk_vertex_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // center, v1 and v2 have to be set
  // apogee is optional (default value is v1)
  if (params.find(_pk_center) != params.end()) { error("param_missing", "center"); }
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  computeBAndAngles();
  computeBB();
  computeMB();
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(0., 1., parametrization_EllArc, pars, "EllArc parametrization");
  parametrization_->setinvParametrization(invParametrization_EllArc);
  parametrization_->geomP() = this;
  trace_p->pop();
}

void EllArc::buildParam(const Parameter& p)
{
  trace_p->push("EllArc::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: c_ = p.get_pt(); break;
        case _integer: c_ = Point(real_t(p.get_i())); break;
        case _real: c_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_apogee:
    {
      switch (p.type())
      {
        case _pt: a_ = p.get_pt(); break;
        case _integer: a_ = Point(real_t(p.get_i())); break;
        case _real: a_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p1_ = p.get_pt(); break;
        case _integer: p1_ = Point(real_t(p.get_i())); break;
        case _real: p1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p2_ = p.get_pt(); break;
        case _integer: p2_ = Point(real_t(p.get_i())); break;
        case _real: p2_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integer:
        {
          number_t n = p.get_n();
          n_[0] = n > 2 ? n : 2;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _integer: h_ = std::vector<real_t>(2, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(2, p.get_r()); break;
        case _realVector:
        {
          h_ = p.get_rv();
          if (h_.size() != 2) { error("bad_size", 2, h_.size()); }
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Curve::buildParam(p); break;
  }
  trace_p->pop();
}

void EllArc::buildDefaultParam(ParameterKey key)
{
  trace_p->push("EllArc::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_[0] = 2; break;
    case _pk_apogee: a_ = p1_; break;
    default: Curve::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> EllArc::getParamsKeys()
{
  std::set<ParameterKey> params = Curve::getParamsKeys();
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_center);
  params.insert(_pk_apogee);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

EllArc::EllArc(Parameter p1, Parameter p2, Parameter p3) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

EllArc::EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

EllArc::EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

EllArc::EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

EllArc::EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

EllArc::EllArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

EllArc::EllArc(const EllArc& e) : Curve(e)
{
  p1_ = e.p1_; p2_ = e.p2_; n_ = e.n_; h_ = e.h_;
  c_ = e.c_; a_ = e.a_; b_ = e.b_;
  thetamin_ = e.thetamin_; thetamax_ = e.thetamax_;
}

string_t EllArc::asString() const
{
  string_t s("Elliptic arc (bounds = {");
  s += p1_.roundToZero().toString() + ", " + p2_.roundToZero().toString() + "}, center = " + c_.roundToZero().toString() + ", apogee = " + a_.roundToZero().toString() + ")";
  return s;
}

EllArc& EllArc::reverse()
{
  std::swap(p1_, p2_);
  if (h_.size() == 2) { std::swap(h_[0], h_[1]); }
  return *this;
}

EllArc operator~(const EllArc& e)
{
  EllArc e2 = e;
  e2.reverse();
  return e2;
}

//!< parametrization c+(a-c)cos(s)+(b-c)sin(s) with s = thetamin+ t*(thetamax-thetamin), t in [0,1]
Vector<real_t> EllArc::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  real_t t = pt[0], dt = thetamax_ - thetamin_;
  real_t s = thetamin_ + t * dt;
  real_t tmin = std::min(thetamin_, thetamax_);
  real_t tmax = std::max(thetamin_, thetamax_);
  if (s < tmin - theTolerance || s > tmax + theTolerance)
  { error("free_error", "in EllArc, parameter " + tostring(s) + " is outside [" + tostring(tmin) + "," + tostring(tmax) + "]"); }
  switch (d)
  {
    case _id: return Vector<real_t>(c_ + (a_ - c_) * std::cos(s) + (b_ - c_) * std::sin(s));
    case _d1: return Vector<real_t>(dt * ((b_ - c_) * std::cos(s) - (a_ - c_) * std::sin(s)));
    case _d11: return Vector<real_t>((dt * dt) * ((c_ - a_) * std::cos(s) + (c_ - b_) * std::sin(s)));
    default: parfun_error("EllArc parametrization", d);
  }
  return Vector<real_t>();  //dummy return
}

//!< inverse of parametrization of p = c+(a-c)cos(s)+(b-c)sin(s) with s = thetamin+ t*(thetamax-thetamin), t in [0,1]
Vector<real_t> EllArc::invParametrization(const Point& p, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("EllArc invParametrization", d); }
  Point ca = a_ - c_, cb = b_ - c_, cp = p - c_;
  real_t a = dot(ca, ca), b = dot(cb, cb), c = dot(ca, cb), e = dot(cp, ca), f = dot(cp, cb);
  real_t g = a * b - c * c; //g>=0 and g=0 <=> c,a,b aligned
  real_t s = std::atan2((a * f - c * e) / g, (b * e - c * f) / g); // in ]-pi,pi]
  if (std::abs(s + pi_) < theTolerance) { s = pi_; }
  if (s < -theTolerance) { s += 2 * pi_; } // move to [0,2*pi]
  if (s < 0) { s = 0; } // force to be >=0
  real_t t = (s - thetamin_) / (thetamax_ - thetamin_);
  //theCout<<" EllArc::invParametrization: s="<<s<<" t="<<t;
  if (t < -theTolerance || t > 1 + theTolerance) { return Vector<real_t>(0); } // fails, return void vector
  t = std::max(0., std::min(1., t)); // to force the parameter t to belong to [0,1]
  // check on ellipse
  cp -= std::cos(s) * ca; cp -= std::sin(s) * cb;
  //theCout<<" norm(cp)="<<norm(cp);
  if (norm(cp) > theTolerance) { return Vector<real_t>(0); }
  return Vector<real_t>(1, t);
}

void EllArc::computeBAndAngles()
{
  /*
    computation of b_
    b_ is defined so that in frame (C, \f$\overrightarrow{CA}, \overrigtarrowCB\f$),
    \f$ \sin \left( \overrightarrow{C A}, \overrightarrow{C P_1}\right) \geq 0. \f$
  */
  real_t casq = a_.squareDistance(c_);
  real_t cbsq = 0.;
  real_t cp1ca = dot(p1_ - c_, a_ - c_);
  real_t cp2ca = dot(p2_ - c_, a_ - c_);
  Vector<real_t> normalvector = crossProduct(a_ - c_, p1_ - c_);
  if (norm2(normalvector) < theEpsilon) { normalvector = crossProduct(a_ - c_, p2_ - c_); }
  // normalvector is not null because p1_ and p2_ cannot be opposite while one of them is equal to a_
  Point aprime = xlifepp::rotate3d(a_, _center=c_, _axis=normalvector, _angle=pi_ / 2.);
  if (c_.size() == 2)
  {
    Point atmp(0., 0.);
    atmp[0] = aprime[0];
    atmp[1] = aprime[1];
    aprime = atmp;
  }
  if (std::abs(cp1ca) < theTolerance)
  {
    // B=P1 or C middle of [BP1]
    cbsq = p1_.squareDistance(c_);
    if (dot(p1_ - c_, aprime - c_) > theEpsilon) { b_ = p1_; }
    else { b_ = 2.*c_ - p1_;}
    b_ = p1_;
  }
  else if (std::abs(cp2ca) < theTolerance)
  {
    // B=P1 or C middle of [BP1]
    cbsq = p2_.squareDistance(c_);
    if (dot(p2_ - c_, aprime - c_) > theEpsilon) { b_ = p2_; }
    else { b_ = 2.*c_ - p2_;}
  }
  else
  {
    real_t cp1sq = p1_.squareDistance(c_);
    real_t cp2sq = p2_.squareDistance(c_);
    real_t cos2t1 = cp1ca * cp1ca / (casq * casq);
    real_t cos2t2 = cp2ca * cp2ca / (casq * casq);
    // formula comes from the use of the polar equation of the ellipse from its center_ and major axis
    // applied on p1_ and p2_, resulting in an system of 2 equations where unknowns are semi minor axis length and excentricity
    // real_t bsq=cp1sq*cp2sq*deltacos2/(cp2sq*deltacos2+cp2sq-cp1sq);
    cbsq = cp1sq * cp2sq * (cos2t1 - cos2t2) / (cp1sq * cos2t1 + cp2sq * cos2t2);
    b_ = c_ + std::sqrt(cbsq) / aprime.distance(c_) * (aprime - c_);
  }

  /*
    computation of thetamin_ and thetamax_
  */
  Point ap1 = (a_ + p1_) / 2.;
  if (ap1 == c_) { thetamin_ = pi_; }
  else
  {
    real_t cp1cb = dot(p1_ - c_, b_ - c_);
    thetamin_ = 2.*std::atan(casq * cp1cb / (cbsq * (casq + cp1ca)));
  }
  Point ap2 = (a_ + p2_) / 2.;
  if (ap2 == c_) { thetamax_ = pi_; }
  else
  {
    real_t cp2cb = dot(p2_ - c_, b_ - c_);
    thetamax_ = 2.*std::atan(casq * cp2cb / (cbsq * (casq + cp2ca)));
  }
  real_t thetarange = thetamax_ - thetamin_;
  if (thetarange > 2 * pi_ + theEpsilon) { error("sector_bad_angles_range", 2 * pi_, thetarange); }
}

void EllArc::computeMB()
{
  // intersection of the bissectrix of (CP1,CP2) and the arc
  Point i = c_ + std::cos((thetamin_ + thetamax_) / 2.) * (a_ - c_) + std::sin((thetamin_ + thetamax_) / 2.) * (b_ - c_);
  // intersection of the bissectrix of (CP1,CP2) and (P1P2)
  bool intExists;
  Point j = intersectionOfStraightLines(c_, i, p1_, p2_, intExists);
  // compute the minimal box
  Point k = p1_ + i - j;
  Point l = p2_ + i - j;
  real_t h;
  Point hk = projectionOnStraightLine(k, p1_, p2_, h);
  Point hl = projectionOnStraightLine(l, p1_, p2_, h);
  Point v1 = p2_, v4 = l;
  if (dot(hl - p2_, p1_ - p2_) < 0.) { v1 = hl; }
  else { v4 = p2_ + l - hl; }
  Point v2 = p1_;
  if (dot(hk - p1_, p2_ - p1_) > 0.) { v2 = hk; }
  minimalBox = MinimalBox(v1, v2, v4);
}

void EllArc::computeBB()
{
  // intersection of the bissectrix of (CP1,CP2) and the arc
  Point i = c_ + std::cos((thetamin_ + thetamax_) / 2.) * (a_ - c_) + std::sin((thetamin_ + thetamax_) / 2.) * (b_ - c_);
  // intersection of the bissectrix of (CP1,CP2) and (P1P2)
  bool intExists;
  Point j = intersectionOfStraightLines(c_, i, p1_, p2_, intExists);
  // compute the minimal box
  Point k = p1_ + i - j;
  Point l = p2_ + i - j;
  real_t h;
  Point hk = projectionOnStraightLine(k, p1_, p2_, h);
  Point hl = projectionOnStraightLine(l, p1_, p2_, h);
  Point v1 = p2_, v4 = l;
  if (dot(hl - p2_, p1_ - p2_) < 0.) { v1 = hl; }
  else { v4 = p2_ + l - hl; }
  Point v2 = p1_;
  if (dot(hk - p1_, p2_ - p1_) > 0.) { v2 = hk; }
  boundingBox = BoundingBox(v1, v2, v4);
}

std::vector<Point*> EllArc::nodes()
{
  std::vector<Point*> nodes(4);
  nodes[0] = &c_; nodes[1] = &a_; nodes[2] = &p1_; nodes[3] = &p2_;
  return nodes;
}

std::vector<const Point*> EllArc::nodes() const
{
  std::vector<const Point*> nodes(4);
  nodes[0] = &c_; nodes[1] = &a_; nodes[2] = &p1_; nodes[3] = &p2_;
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > EllArc::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(1);
  curves[0] = std::make_pair(_ellArc, boundNodes());
  return curves;
}

//==========================================================
// CircArc class member functions
//==========================================================

//! default circular arc is quarter of circle of center (0,0,0) and radius 1
CircArc::CircArc() : Curve(), c_(0., 0.)
{
  p1_ = Point(1., 0.);
  p2_ = Point(0., 1.);
  n_[0] = 2;
  shape_ = _circArc;
  computeMB();
}

void CircArc::build(const std::vector<Parameter>& ps)
{
  trace_p->push("CircArc::build");
  shape_ = _circArc;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _circArc)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must use side_names or vertex_names, not both
    if (key == _pk_vertex_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_vertex_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_vertex_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _vertex_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_vertex_names); }
  if (usedParams.find(_pk_vertex_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // center, v1 and v2 have to be set
  if (params.find(_pk_center) != params.end()) { error("param_missing", "center"); }
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }

  if (std::abs(c_.distance(p1_) - c_.distance(p2_)) > theTolerance)
  { error("geometry_incoherent_points", words("shape", _circArc)); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  computeBAndAngle();
  computeBB();
  computeMB();
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(0., 1., parametrization_CircArc, pars, "CircArc parametrization");
  parametrization_->setinvParametrization(invParametrization_CircArc);
  trace_p->pop();
}

void CircArc::buildParam(const Parameter& p)
{
  trace_p->push("CircArc::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: c_ = p.get_pt(); break;
        case _integer: c_ = Point(real_t(p.get_i())); break;
        case _real: c_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p1_ = p.get_pt(); break;
        case _integer: p1_ = Point(real_t(p.get_i())); break;
        case _real: p1_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p2_ = p.get_pt(); break;
        case _integer: p2_ = Point(real_t(p.get_i())); break;
        case _real: p2_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integer:
        {
          number_t n = p.get_n();
          n_[0] = n > 2 ? n : 2;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _integer: h_ = std::vector<real_t>(2, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(2, p.get_r()); break;
        case _realVector:
        {
          h_ = p.get_rv();
          if (h_.size() != 2) { error("bad_size", 2, h_.size()); }
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Curve::buildParam(p); break;
  }
  trace_p->pop();
}

void CircArc::buildDefaultParam(ParameterKey key)
{
  trace_p->push("CircArc::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_[0] = 2; break;
    default: Curve::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> CircArc::getParamsKeys()
{
  std::set<ParameterKey> params = Curve::getParamsKeys();
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_center);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

CircArc::CircArc(Parameter p1, Parameter p2, Parameter p3) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

CircArc::CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

CircArc::CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

CircArc::CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

CircArc::CircArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

CircArc::CircArc(const CircArc& s) : Curve(s)
{
  p1_ = s.p1_; p2_ = s.p2_; n_ = s.n_; h_ = s.h_;
  c_ = s.c_; a_ = s.a_; b_ = s.b_;
  thetamin_ = s.thetamin_; thetamax_ = s.thetamax_;
  radius_ = s.radius_;
}

void CircArc::computeBAndAngle()
{
  a_ = p1_;

  // computation of b_
  Vector<real_t> normalvector = crossProduct(a_ - c_, p2_ - c_);
  if (norm2(normalvector) < theEpsilon) { error("sector_bad_angles_range", 360., 360.); }
  b_ = xlifepp::rotate3d(p1_, _center=c_, _axis=crossProduct(p1_ - c_, p2_ - c_), _angle=pi_ / 2.);

  if (c_.size() == 2)
  {
    Point btmp(0., 0.);
    btmp[0] = b_[0];
    btmp[1] = b_[1];
    b_ = btmp;
  }
  real_t radius2 = a_.squareDistance(c_);
  radius_ = std::sqrt(radius2);
  /*
    computation of thetamin_ and thetamax_
    it is already checked that angle is strictly less than pi_
  */
  thetamin_ = 0.;
  real_t cp2ca = dot(p2_ - c_, a_ - c_);
  real_t cp2cb = dot(p2_ - c_, b_ - c_);
  thetamax_ = 2.*std::atan(cp2cb / (radius2 + cp2ca));
}

void CircArc::computeMB()
{
  // intersection of the bissectrix of (CP1,CP2) and the arc
  Point i = c_ + std::cos(thetamax_ / 2.) * (a_ - c_) + std::sin(thetamax_ / 2.) * (b_ - c_);
  // intersection of the bissectrix of (CP1,CP2) and (P1P2)
  bool intExists;
  Point j = intersectionOfStraightLines(c_, i, p1_, p2_, intExists);
  // compute the minimal box
  Point k = p1_ + i - j;
  Point l = p2_ + i - j;
  real_t h;
  Point hk = projectionOnStraightLine(k, p1_, p2_, h);
  Point hl = projectionOnStraightLine(l, p1_, p2_, h);
  Point v1 = p2_, v4 = l;
  if (dot(hl - p2_, p1_ - p2_) < 0.) { v1 = hl; }
  else { v4 = p2_ + l - hl; }
  Point v2 = p1_;
  if (dot(hk - p1_, p2_ - p1_) > 0.) { v2 = hk; }
  minimalBox = MinimalBox(v1, v2, v4);
}

void CircArc::computeBB()
{
  // intersection of the bissectrix of (CP1,CP2) and the arc
  Point i = c_ + std::cos(thetamax_ / 2.) * (a_ - c_) + std::sin(thetamax_ / 2.) * (b_ - c_);
  // intersection of the bissectrix of (CP1,CP2) and (P1P2)
  bool intExists;
  Point j = intersectionOfStraightLines(c_, i, p1_, p2_, intExists);
  // compute the minimal box
  Point k = p1_ + i - j;
  Point l = p2_ + i - j;
  real_t h;
  Point hk = projectionOnStraightLine(k, p1_, p2_, h);
  Point hl = projectionOnStraightLine(l, p1_, p2_, h);
  Point v1 = p2_, v4 = l;
  if (dot(hl - p2_, p1_ - p2_) < 0.) { v1 = hl; }
  else { v4 = p2_ + l - hl; }
  Point v2 = p1_;
  if (dot(hk - p1_, p2_ - p1_) > 0.) { v2 = hk; }
  boundingBox = BoundingBox(v1, v2, v4);
}

string_t CircArc::asString() const
{
  string_t s("Circular arc (bounds = {");
  s += p1_.roundToZero().toString() + ", " + p2_.roundToZero().toString() + "}, center = " + c_.roundToZero().toString() + ")";
  return s;
}

CircArc& CircArc::reverse()
{
  std::swap(p1_, p2_);
  if (h_.size() == 2) { std::swap(h_[0], h_[1]); }
  return *this;
}

CircArc operator~(const CircArc& c)
{
  CircArc c2 = c;
  c2.reverse();
  return c2;
}

//!< parametrization c+(a-c)cos(t)+(b-c)sin(t)  t=(1-x)*thetamin + x*thetamax with x=pt[0] in [0,1]
Vector<real_t> CircArc::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  real_t t = pt[0];
  real_t dt = thetamax_ - thetamin_;
  t = thetamin_ + t * dt;
  if (t < thetamin_ - theTolerance || t > thetamax_ + theTolerance)
  { error("free_error", "in CircArc, parameter " + tostring(t) + " is outside [" + tostring(thetamin_) + "," + tostring(thetamax_) + "]"); }
  switch (d)
  {
    case _id: return Vector<real_t>(c_ + (a_ - c_) * std::cos(t) + (b_ - c_) * std::sin(t));
    case _d1: return dt * Vector<real_t>((b_ - c_) * std::cos(t) - (a_ - c_) * std::sin(t));
    case _d11: return (dt * dt) * Vector<real_t>((c_ - a_) * std::cos(t) + (c_ - b_) * std::sin(t));
    default: parfun_error("CircArc parametrization", d);
  }
  return Vector<real_t>();  //dummy return
}

//!< inverse of parametrization c+(a-c)cos(t)+(b-c)sin(t)
Vector<real_t> CircArc::invParametrization(const Point& p, Parameters& pars, DiffOpType d) const
{
  if (d != _id) { parfun_error("CircArc invParametrization", d); }
  Point ca = a_ - c_, cb = b_ - c_, cp = p - c_;
  real_t a = dot(ca, ca), b = dot(cb, cb), c = dot(ca, cb), e = dot(cp, ca), f = dot(cp, cb);
  real_t g = a * b - c * c;
  real_t t = std::atan2((a * f - c * e) / g, (b * e - c * f) / g);
  t = (t - thetamin_) / (thetamax_ - thetamin_);
  if (t < -theTolerance || t > 1 + theTolerance) { return Vector<real_t>(0); } // fails, return void vector
  return Vector<real_t>(1, t);
}

std::vector<Point*> CircArc::nodes()
{
  std::vector<Point*> nodes(3);
  nodes[0] = &c_; nodes[1] = &p1_; nodes[2] = &p2_;
  return nodes;
}

std::vector<const Point*> CircArc::nodes() const
{
  std::vector<const Point*> nodes(3);
  nodes[0] = &c_; nodes[1] = &p1_; nodes[2] = &p2_;
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > CircArc::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(1);
  curves[0] = std::make_pair(_circArc, boundNodes());
  return curves;
}

real_t CircArc::measure() const
{
  real_t theta;
  real_t radius = c_.distance(p1_);
  real_t scal = dot(p1_ - c_, p2_ - c_);
  if (std::abs(scal) < theEpsilon) { return 0.5 * pi_ * radius; }
  theta = std::atan(norm2(crossProduct(p1_ - c_, p2_ - c_)) / scal);
  if (theta < 0) { theta += pi_; }
  return theta;
}

//==========================================================
// SplineArc class member functions
//==========================================================

//! default is a void spline (spline_=0)
SplineArc::SplineArc() : Curve(), spline_(nullptr)
{
  shape_ = _splineArc;
}

void SplineArc::build(const std::vector<Parameter>& ps)
{
  trace_p->push("SplineArc::build");
  shape_ = _splineArc;
  SplineBC bc = _undefBC;
  SplineParametrization par = _undefParametrization;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  std::vector<real_t> weights;
  real_t tau = theRealMax;
  std::vector<real_t> ts, te;
  // managing params
  std::vector<Point> controlPoints;
  SplineType type = _noSpline;
  SplineSubtype subtype = _noSplineSubtype;
  number_t deg = 3;
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    const Parameter& p = ps[i];
    switch (key)
    {
      case _pk_spline:
      {
        if (p.type() == _pointerSpline) { spline_ = (reinterpret_cast<const Spline*>(p.get_p()))->clone(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_spline_type:
      {
        switch (p.type())
        {
          case _integer:
          case _enumSplineType:
            type = SplineType(p.get_n());
            break;
          default:
            error("param_badtype", words("value", p.type()), words("param key", key));
        }
        break;
      }
      case _pk_spline_subtype:
      {
        switch (p.type())
        {
          case _integer:
          case _enumSplineSubtype:
            subtype = SplineSubtype(p.get_n());
            break;
          default:
            error("param_badtype", words("value", p.type()), words("param key", key));
        }
        break;
      }
      case _pk_spline_BC:
      {
        switch (p.type())
        {
          case _integer:
          case _enumSplineBC:
            bc = SplineBC(p.get_n());
            break;
          default:
            error("param_badtype", words("value", p.type()), words("param key", key));
        }
        break;
      }
      case _pk_degree:
      {
        if (p.type() == _integer) { deg = p.get_n(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_tension:
      {
        if (p.type() == _integer) { tau = real_t(p.get_n()); }
        else if (p.type() == _real) { tau = p.get_r(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_spline_parametrization:
      {
        switch (p.type())
        {
          case _integer:
          case _enumSplineParametrization:
            par = SplineParametrization(p.get_n());
            break;
          default:
            error("param_badtype", words("value", p.type()), words("param key", key));
        }
        break;
      }
      case _pk_weights:
      {
        if (p.type() == _realVector) { weights = p.get_rv(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_tangent_0:
      {
        if (p.type() == _realVector) { ts = p.get_rv(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_tangent_1:
      {
        if (p.type() == _realVector) { te = p.get_rv(); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_vertices:
      {
        if (p.type() == _ptVector) { controlPoints = *reinterpret_cast<const std::vector<Point>*>(p.get_p()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_nnodes:
      {
        if (p.type() == _integer) { n_[0] = std::max(number_t(2), p.get_n()); }
        else { error("param_badtype", words("value", p.type()), words("param key", key)); }
        break;
      }
      case _pk_hsteps:
      {
        switch (p.type())
        {
          case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
          case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
          case _realVector: h_ = p.get_rv(); break;
          default: error("param_badtype", words("value", p.type()), words("param key", key));
        }
        break;
      }
      default: Curve::buildParam(p); break;
    }
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parametrizedArc)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must use side_names or vertex_names, not both
    if (key == _pk_vertex_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_vertex_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_vertex_names)); }
  }

  // if spline type is BSpline, set subtype to _SplineInterpolation if it is not defined (subtype=_noSplineSubtype)
  if (subtype == _noSplineSubtype) { subtype = _SplineInterpolation; }

  // check and set spline parametrization is not set
  if (type == _C2Spline)
  {
    if (par == _undefParametrization)  { par = _xParametrization; }
  }
  else
  {
    if (subtype == _SplineInterpolation)
    {
      if (par == _centripetalParametrization || par == _chordalParametrization)
      { warning("free_warning", " centripetal or chordal parametrizations are not allowed for spline interpolation, uniform parametrization will be used"); }
      par = _uniformParametrization; //force uniformParametrisation
    }
    else if (par == _undefParametrization)  { par = _centripetalParametrization; }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _vertex_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_vertex_names); }
  if (usedParams.find(_pk_vertex_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // check undef parameter
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  if (ts.size() == 0)
  {
    ts.resize(3, 0.);
    if (type != _C2Spline) { ts[0] = 1.; }
  }
  if (te.size() == 0)
  {
    te.resize(3, 0.);
    if (type != _C2Spline) { te[0] = 1.; }
  }

  if (spline_ == nullptr) // build spline object
  {
    switch (type)
    {
      case _C2Spline:
      {
        if (controlPoints.size() < 4) { error("free_error", "SplineArc requires at least 4 points"); }
        spline_ = new C2Spline(controlPoints, par, bc, bc, ts, te);
        break;
      }
      case _CatmullRomSpline:
      {
        if (controlPoints.size() < 4) { error("free_error", "SplineArc requires at least 4 points"); }
        spline_ = new CatmullRomSpline(controlPoints, par, tau, bc, bc, ts, te);
        break;
      }
      case _BSpline:
      {
        if (deg < 1)   { error("free_error", "spline degree may greater than 0"); }
        if (controlPoints.size() < deg + 1) { error("free_error", "SplineArc requires at least " + tostring(deg + 1) + " points"); }
        //if (subtype==_SplineApproximation) spline_= new BSpline(controlPoints,deg,bc,bc,weights);
        if (subtype == _SplineApproximation) { spline_ = new BSpline(_SplineApproximation, controlPoints, deg, par, bc, bc, ts, te, weights); }
        else { spline_ = new BSpline(_SplineInterpolation, controlPoints, deg, par, bc, bc, ts, te, weights); }
        break;
      }
      case _BezierSpline:
      {
        if (controlPoints.size() < 2) { error("free_error", "SplineArc requires at least 2 points"); }
        spline_ = new BezierSpline(controlPoints);
        break;
      }
      default: error("free_error", "unknown type of spline in SplineArc::build");
    }
  }

  //check controlPoints size
  number_t nc = spline_->controlPoints().size();
  if (nc < 2) { error("bad_size_inf", "control points", 2, nc); }

  // set p1_, p2_, isClosed_
  std::vector<Point> bounds = spline_->boundNodes();
  p1_ = bounds[0]; p2_ = bounds[1];
  isClosed_ = spline_->isClosed();

  // check h size and correct it in order to have size(h)=nc
  // (one or two endpoints may be added by spline construction)
  number_t nh = h_.size();
  if (nh != 0)
  {
    if (nh == 1) { h_ = std::vector<real_t>(nc, h_[0]); }
    else
    {
      int_t m = nc - nh;
      switch (m)
      {
        case 0: break;
        case 1: h_.insert(h_.begin(), h_[0]); break;
        case 2: h_.insert(h_.begin(), h_[0]); h_.push_back(h_[nh]); break;
        default: error("bad_size", "hsteps", nc, nh);
      }
    }
  }
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(0., 1., parametrization_SplineArc, pars, "splineArc parametrization");
  parametrization_->setinvParametrization(invParametrization_SplineArc);
  computeBB();
  computeMB();
  trace_p->pop();
}

void SplineArc::buildParam(const Parameter& p)
{
  trace_p->push("SplineArc::buildParam");
  ParameterKey key = p.key();
  trace_p->pop();
}

void SplineArc::buildDefaultParam(ParameterKey key)
{
  trace_p->push("SplineArc::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_[0] = 2; break;
    case _pk_spline: spline_ = 0; break;
    case _pk_vertices:         //optional parameters not managed by the class
    case _pk_degree:         //local management
    case _pk_spline_BC:
    case _pk_tension:
    case _pk_weights:
    case _pk_spline_parametrization:
    case _pk_tangent_0:
    case _pk_tangent_1:
    case _pk_spline_type:
    case _pk_spline_subtype: break;
    default: Curve::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> SplineArc::getParamsKeys()
{
  std::set<ParameterKey> params = Curve::getParamsKeys();
  params.insert(_pk_vertices);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  params.insert(_pk_spline);
  params.insert(_pk_spline_type);
  params.insert(_pk_spline_subtype);
  params.insert(_pk_degree);
  params.insert(_pk_spline_BC);
  params.insert(_pk_tension);
  params.insert(_pk_weights);
  params.insert(_pk_spline_parametrization);
  params.insert(_pk_tangent_0);
  params.insert(_pk_tangent_1);
  return params;
}

SplineArc::SplineArc(Parameter p1) : Curve()
{
  std::vector<Parameter> ps={p1};
  build(ps);
}

SplineArc::SplineArc(Parameter p1, Parameter p2) : Curve()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

SplineArc::SplineArc(Parameter p1, Parameter p2, Parameter p3) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

SplineArc::SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

SplineArc::SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

SplineArc::SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

SplineArc::SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

SplineArc::SplineArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

//! copy constructor
SplineArc::SplineArc(const SplineArc& spa) : Curve(spa)
{
  spline_ = nullptr;
  copy(spa);
}

//! assign operator
SplineArc& SplineArc::operator=(const SplineArc& spa)
{
  if (&spa == this) { return *this; }
  if (spline_ != nullptr) { delete spline_; }
  spline_ = nullptr;
  copy(spa);
  return *this;
}

//! copy tool (hard copy)
void SplineArc::copy(const SplineArc& spa)
{
  if (spa.spline_ != nullptr) { spline_ = spa.spline_->clone(); }
  p1_ = spa.p1_;
  p2_ = spa.p2_;
  n_ = spa.n_;
  h_ = spa.h_;
  isClosed_ = spa.isClosed_;
}

//! destructor
SplineArc::~SplineArc()
{
  if (spline_ != nullptr) { delete spline_; }
  parametrization_ = nullptr; //shared with spline_ and deleted by delete spline !
}

//! return bound nodes
std::vector<const Point*> SplineArc::boundNodes() const
{
  if (spline_ != nullptr)
  {
    std::vector<const Point*> res(2);
    res[0] = &p1_; res[1] = &p2_;
    return res;
  }
  return std::vector<const Point*>();
}

//! return last node index (index starting from 1)
number_t SplineArc::lastNodeIndex() const
{
  number_t ind = 0;
  if (spline_ != nullptr)
  {
    ind = spline_->controlPoints().size();
    if (spline_->type() == _CatmullRomSpline) { ind -= 2; }
  }
  return ind;
}

//! return nodes (controlPoints)
std::vector<const Point*> SplineArc::nodes() const
{
  std::vector<const Point*> nodes;
  if (spline_ != nullptr)
  {
    std::vector<Point>& cpts = spline_->controlPoints();
    number_t nbp = cpts.size(), s = 0;
    if (spline_->type() == _CatmullRomSpline) {nbp -= 2; s = 1;}
    nodes.resize(nbp);
    for (number_t n = 0; n < nbp; n++) { nodes[n] = const_cast<const Point*>(&cpts[n + s]); }
  }
  return nodes;
}

//! return curves, here only one
std::vector<std::pair<ShapeType, std::vector<const Point*> > > SplineArc::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(1);
  curves[0] = std::make_pair(_splineArc, boundNodes());
  return curves;
}

string_t SplineArc::asString() const
{
  string_t s("Spline arc of type ");
  s += words("spline type", type());
  return s;
}

//! print additional information
void SplineArc::printDetail(std::ostream& os) const
{
  if (spline_ != nullptr) { spline_->print(os); }
}

//!< interface to spline parametrization (t in [0,1])
Vector<real_t> SplineArc::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  const Parametrization& par_spline = spline_->parametrization();
  if (pt[0] < -theTolerance || pt[0] > 1 + theTolerance)
  { error("free_error", "in SplineArc, parameter " + tostring(pt[0]) + " is outside parameter intervall [0,1]"); }
  Vector<real_t> res = par_spline(pt, d);
  return res;
}

//!< inverse of parametrization
Vector<real_t> SplineArc::invParametrization(const Point& p, Parameters& pars, DiffOpType d) const
{
  error("free_error", "in SplineArc, no inverse");
  return Vector<real_t>();
}

//==========================================================
// ParametrizedArc class member functions
//===========================================================

//! no default is the ParametrizedArc [0,1]
ParametrizedArc::ParametrizedArc() : Curve(), tmin_(0.), tmax_(1.), transformation_(nullptr), arc_parametrization_(nullptr), partitioning_(_nonePartition), nbParts_(1)
{
  n_[0] = 2;
  shape_ = _parametrizedArc;
  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(tmin_, tmax_, parametrization_ParametrizedArc, pars, "parametrizedArc parametrization");
  parametrization_->setinvParametrization(invParametrization_ParametrizedArc);
  p_.resize(2);
  p_[0] = Point(0., 0.); p_[1] = Point(1., 0.);
  p1_ = p_[0]; p2_ = p_[1] ;
  computeBB();
  computeMB();
}

ParametrizedArc::ParametrizedArc(const ParametrizedArc& pa)
  : Curve(pa), p_(pa.p_), tmin_(pa.tmin_), tmax_(pa.tmax_), partitioning_(pa.partitioning_), nbParts_(pa.nbParts_)
{
  p1_ = pa.p1_;
  p2_ = pa.p2_;
  n_ = pa.n_;
  h_ = pa.h_;
  transformation_ = nullptr;
  if (pa.transformation_ != nullptr) { transformation_ = new Transformation(*pa.transformation_); }
  arc_parametrization_ = pa.arc_parametrization_; //shared pointer !!!!!
}

void ParametrizedArc::build(const std::vector<Parameter>& ps)
{
  trace_p->push("ParametrizedArc::build");
  shape_ = _parametrizedArc;
  transformation_ = nullptr;
  arc_parametrization_ = nullptr;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parametrizedArc)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must use side_names or vertex_names, not both
    if (key == _pk_vertex_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_vertex_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_vertex_names)); }
  }
  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _vertex_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_vertex_names); }
  if (usedParams.find(_pk_vertex_names) != usedParams.end()) { params.erase(_pk_side_names); }

  // parametrization has to be set (no default values)
  if (params.find(_pk_parametrization) != params.end())  { error("param_missing", "parametrization"); }
  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }
  if (arc_parametrization_->dimg != 1)
  { error("free_error", "building parametrizedArc requires a 1D->2D/3D parametrization"); }
  RealPair bx = arc_parametrization_->bounds(_x1);
  tmin_ = bx.first;  tmax_ = bx.second;
  p_.resize(nbParts_ + 1);
  std::vector<Point>::iterator itp = p_.begin();
  real_t dt = (tmax_ - tmin_) / nbParts_, t = tmin_;
  for (number_t i = 1; i <= nbParts_ + 1; ++i, t += dt, ++itp) { *itp = (*arc_parametrization_)(t); }
  p1_ = p_[0];
  p2_ = p_[nbParts_] ;
  number_t nbSides = 2;
  if (p1_ == p2_)
  {
    isClosed_ = true;
    nbSides = 1;
  }
  // h may be void, of size 1 or of size nbParts+1
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(nbParts_ + 1, h0); }
    else if (h_.size() != nbParts_ + 1) { error("bad_size", "hsteps", nbParts_ + 1, h_.size()); }
  }

  Parameters pars(reinterpret_cast<const void*>(this), "geometry");
  parametrization_ = new Parametrization(0., 1., parametrization_ParametrizedArc, pars, "parametrizedArc parametrization");
  parametrization_->setinvParametrization(invParametrization_ParametrizedArc);
  parametrization_->periods = arc_parametrization_->periods;
  parametrization_->nbOfMaps = arc_parametrization_->nbOfMaps;
  parametrization_->singularSide = arc_parametrization_->singularSide;

  computeBB();
  computeMB();
  trace_p->pop();
}

void ParametrizedArc::buildParam(const Parameter& p)
{
  trace_p->push("ParametrizedArc::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_tmin:
    {
      switch (p.type())
      {
        case _integer: tmin_ = real_t(p.get_i()); break;
        case _real: tmin_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_tmax:
    {
      switch (p.type())
      {
        case _integer: tmax_ = real_t(p.get_i()); break;
        case _real: tmax_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_parametrization:
    {
      switch (p.type())
      {
        case _pointerParametrization:
          arc_parametrization_ = new Parametrization(*reinterpret_cast<const Parametrization*>(p.get_p()));
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integer:
        {
          number_t n = p.get_n();
          n_[0] = n > 2 ? n : 2;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        case _realVector: h_ = p.get_rv(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_partitioning:
    {
      switch (p.type())
      {
        case _integer:
        case _enumPartitioning:
          partitioning_ = Partitioning(p.get_n());
          break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nbParts:
    {
      switch (p.type())
      {
        case _integer:
        {
          number_t n = p.get_n();
          nbParts_ = n > 1 ? n : 1;
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Curve::buildParam(p); break;
  }
  trace_p->pop();
}

void ParametrizedArc::buildDefaultParam(ParameterKey key)
{
  trace_p->push("ParametrizedArc::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_[0] = 2; break;
    case _pk_tmin: tmin_ = 0.; break;
    case _pk_tmax: tmax_ = 1.; break;
    case _pk_partitioning: partitioning_ = _nonePartition; break;
    case _pk_nbParts: nbParts_ = 1; break;
    default: Curve::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> ParametrizedArc::getParamsKeys()
{
  std::set<ParameterKey> params = Curve::getParamsKeys();
  params.insert(_pk_tmin);
  params.insert(_pk_tmax);
  params.insert(_pk_parametrization);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  params.insert(_pk_partitioning);
  params.insert(_pk_nbParts);
  return params;
}

ParametrizedArc::ParametrizedArc(Parameter p1, Parameter p2) : Curve()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

ParametrizedArc::ParametrizedArc(Parameter p1, Parameter p2, Parameter p3) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

ParametrizedArc::ParametrizedArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

ParametrizedArc::ParametrizedArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

ParametrizedArc::ParametrizedArc(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Curve()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

string_t ParametrizedArc::asString() const
{
  string_t s("ParametrizedArc (");
  s += parametrization_->name + ")";
  return s;
}

ParametrizedArc& ParametrizedArc::reverse()
{
  error("free_error", "ParametrizedArc::reverse not available");
  return *this;
}

ParametrizedArc operator~(const ParametrizedArc& s)
{
  ParametrizedArc s2 = s;
  s2.reverse();
  return s2;
}

std::vector<Point*> ParametrizedArc::nodes()
{
  std::vector<Point*> nodes(2);
  nodes[0] = &p1_; nodes[1] = &p2_;
  return nodes;
}

std::vector<const Point*> ParametrizedArc::nodes() const
{
  std::vector<const Point*> nodes(2);
  nodes[0] = &p1_; nodes[1] = &p2_;
  return nodes;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > ParametrizedArc::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(1);
  curves[0] = std::make_pair(_parametrizedArc, boundNodes());
  return curves;
}

//!< parametrization: [transformation_ * ] arc_parametrization
Vector<real_t> ParametrizedArc::funParametrization(const Point& pt, Parameters& pars, DiffOpType d) const
{
  real_t t = pt[0];
  t = (1 - t) * tmin_ + t * tmax_;
//  if (t < tmin_ - theTolerance || t > tmax_ + theTolerance)
//  { error("free_error", "in ParametrizedArc, parameter " + tostring(t) + " is outside [" + tostring(tmin_) + "," + tostring(tmax_) + "]"); }
  // evaluate parametrization
  Point qt = pt;
  qt[0] = t;
  Vector<real_t> res;
  if (arc_parametrization_ == nullptr) { res = pt; } //default segment [0,1]
  else { res = (*arc_parametrization_)(qt, d); }
  if (transformation_ == nullptr) { return res; }
  std::cout << "ParametrizedArc::funParametrization pt=" << pt << "res=" << res;
  //apply additional transformation
  const Vector<real_t>& b = transformation_->vec();
  number_t dim = res.size();
  std::cout << " dim=" << dim;
  if (dim < 3) { res.resize(3, 0.); } //move 3D
  res = transformation_->mat() * res;
  if (d == _id)
    for (number_t i = 0; i < b.size(); i++) { res[i] += b[i]; }
  std::cout << " transf res=" << res;
  if (dim < 3 && !transformation_->is3D()) { res.resize(2); } // move back 2D
  std::cout << " out res=" << res << eol << std::flush;
  return res;
}

//!< inverse of parametrization
Vector<real_t> ParametrizedArc::invParametrization(const Point& p, Parameters& pars, DiffOpType d) const
{
  return arc_parametrization_->toParameter(p);
}

//==========================================================
// SetOfPoints class member functions
//===========================================================
string_t SetOfPoints::asString() const
{
  string_t s("SetOfPoints (");
  s += tostring(pts_.size()) + " points)";
  return s;
}

std::vector<Point*> SetOfPoints::nodes()
{
  std::vector<Point*> nodes(pts_.size());
  for (size_t i = 0; i < pts_.size(); i++) { nodes[i] = &pts_[i]; }
  return nodes;
}

std::vector<const Point*> SetOfPoints::nodes() const
{
  std::vector<const Point*> nodes(pts_.size());
  for (size_t i = 0; i < pts_.size(); i++) { nodes[i] = &pts_[i]; }
  return nodes;
}

real_t SetOfPoints::measure() const
{
  real_t length = 0;
  for (number_t i = 1; i < pts_.size(); ++i)
  {
    length += pts_[i - 1].distance(pts_[i]);
  }
  return length;
}

} // end of namespace xlifepp
