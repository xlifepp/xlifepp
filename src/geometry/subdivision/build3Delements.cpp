/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file build3Delements.cpp
  \author Y. Lafranche
  \since 3 Fev 2014
  \date  16 Apr 2014

  \brief Build a XLiFE++ mesh of 3D elements from a mesh computed using a subdivision algorithm.

  The elements taken into account are the tetrahedron and the hexahedron.
*/

#include "../Mesh.hpp"
#include "finiteElements.h"

#include <algorithm>
using namespace std;

namespace xlifepp {
//-------------------------------------------------------------------------------
// Protected member functions
//-------------------------------------------------------------------------------
/*!
 Termination of the construction of a mesh object of 3D elements by transferring
 data from a subdivision::xxxMesh object into XLiFE++ objects.
 */
template <class ST_> number_t* faceNumbering();

template <class ST_>
void Mesh::build3Delements(subdivision::GeomFigureMesh<ST_> *TM_p,
                           const ShapeType elShape, number_t nbsubdom) {
  enum {_element_dim_m1=2, _element_dim=3};

  vector<string_t> boundaryNames,interfaceNames, subdomainNames;
  string_t domName;
  manageDomains(TM_p,nbsubdom,_element_dim, boundaryNames,interfaceNames,subdomainNames, domName);

  number_t el_k=0;// for numbering of elements
  copyPtsEltsDoms(TM_p, elShape, _element_dim, geometry_p->dim(), el_k,subdomainNames, domName);

  //   3b. boundary domains (made of triangles or quadrangles)
  // TMf2XLf is an interface array to convert subdivision::xxxMesh numbering of faces
  // into XLiFE++ numbering. If i, 1 <= i <= number of faces per element, denotes the number of a face
  // of an element in class xxxMesh, then the corresponding face is TMf2XLf[i] in XLiFE++.
  number_t *TMf2XLf = faceNumbering<ST_>();

  number_t minEltnum = TM_p->minElementNumber();
  number_t nbBound = TM_p->numberOfBoundaries();
  number_t nb_Boundary_Faces = 0;
  for (number_t numBoundary=1; numBoundary<=nbBound; numBoundary++) {
    // boundaryNames[numBoundary-1] replaces TM_p->boundaryName(numBoundary)
    if (! boundaryNames[numBoundary-1].empty()) {
      MeshDomain* meshdom_p = (new GeomDomain(*this, boundaryNames[numBoundary-1], _element_dim_m1,
                                              TM_p->boundaryDescription(numBoundary)))->meshDomain();
      // For the current boundary, get the list of boundary faces as pairs
      // (element number, local face number).
      // Numbering convention for the local faces is the same as the one used in XLiFE++ for the tetrahedron ;
      // it is different for the hexahedron. This is why TMf2XLf is needed.
      pair< vector<number_t>,vector<number_t> > TF = TM_p->faceElementsIn(subdivision::boundaryArea,numBoundary);
      vector<GeomElement*>& v_ge = meshdom_p->geomElements;
      v_ge.reserve(TF.first.size());
      for (vector<number_t>::iterator itT=TF.first.begin(), itF=TF.second.begin();
           itT != TF.first.end(); itT++,itF++) {
        v_ge.push_back(new GeomElement(elements_[*itT - minEltnum],TMf2XLf[*itF], el_k + 1));
                                       // element N is at rank N-minEltnum in the vector elements_
        el_k++;
      }
      domains_.push_back(meshdom_p);
      nb_Boundary_Faces += v_ge.size();
    }
  }
  // test the coherence of the mesh
/* --> commented since it is valid only if all the boundary domains are created
   if (nb_Boundary_Faces != TM_p->numberOfFacesIn(subdivision::boundaryArea)) {
      error("bad_nbsides",words("boundary faces"),nb_Boundary_Faces);
   }
*/
  //   3c. interface domains (made of triangles or quadrangles)
  number_t nbIntrf = TM_p->numberOfInterfaces();
  number_t nb_Interface_Faces = 0;
  for (number_t numInterface=1; numInterface<=nbIntrf; numInterface++) {
    // interfaceNames[numInterface-1] replaces TM_p->interfaceName(numInterface)
    if (! interfaceNames[numInterface-1].empty()) {
      MeshDomain* meshdom_p = (new GeomDomain(*this, interfaceNames[numInterface-1], _element_dim_m1,
                                              TM_p->interfaceDescription(numInterface)))->meshDomain();
      // For the current interface, get the list of interface faces as pairs
      // (element number, local face number).
      // Numbering convention for the local faces is the same as the one used in XLiFE++ for the tetrahedron ;
      // it is different for the hexahedron. This is why TMf2XLf is needed.
      pair< vector<number_t>,vector<number_t> > TF = TM_p->faceElementsIn(subdivision::interfaceArea,numInterface);
      // Here, we get two lists (cf. GeomFigureMesh<T_>::faceElementsIn): we need to describe the interface by
      // one single set of faces, by selecting the elements lying on the same side of the interface.
      // This is done in the following loop by computing a sign criterion, based on the normal to the face
      // with respect to the element it owns to.
      // Warning: this criterion only holds for a plane interface, which is always the case in the context
      //          of the subdivision algorithm.
      // Remark: Alternatively, if the elements on both sides of the interface lie in two different subdomains,
      // one can use the localization criterion, which is independent of any geometrical configuration. Since
      // the condition on the subdomains cannot be assumed in any case, this criterion has not been choosed.
      // The corresponding code below is commented and marked with //+.
      number_t lglif(TF.first.size()/2);
      vector<GeomElement*>& v_ge = meshdom_p->geomElements;
      v_ge.reserve(lglif);
      // Nff = exterior normal to the first face in the list:
      Vector<real_t> Nff(TM_p->faceExtNormalVec(TF.first[0],TF.second[0]));
      //+ Localization code of the first element
      //+ subdivision::refnum_t lcfe = TM_p->locCode(TF.first[0]);
      for (vector<number_t>::iterator itT=TF.first.begin(), itF=TF.second.begin();
           itT != TF.first.end(); itT++,itF++) {
        // Ncf = exterior normal to the current face:
        Vector<real_t> Ncf(TM_p->faceExtNormalVec(*itT,*itF));
        //+ Localization code of the current element
        //+ subdivision::refnum_t lcce = TM_p->locCode(*itT); if (lcce == lcfe) {
        if (dot(Nff,Ncf) > 0) {
          v_ge.push_back(new GeomElement(elements_[*itT - minEltnum],TMf2XLf[*itF], el_k + 1));
                                      // element N is at rank N-minEltnum in the vector elements_
          el_k++;
        }
      }
      domains_.push_back(meshdom_p);
      nb_Interface_Faces += v_ge.size();
    }
  }
  // test the coherence of the mesh
/* --> commented since it is valid only if all the interface domains are created
   if (nb_Interface_Faces*2 != TM_p->numberOfFacesIn(subdivision::interfaceArea)) {
    error("bad_nbsides",words("interface faces"),nb_Interface_Faces);
   }
*/
  // initialization of protected data members of class Mesh
  comment_ = TM_p->title();
  lastIndex_ = el_k ;
}

//-------------------------------------------------------------------------------
// Extern functions
//-------------------------------------------------------------------------------
template <class ST_>
void tensorNumbering(const int interpNum, number_t**& s2h);

/*!
 The following function returns a vector giving the correspondence between the two
 numberings of the points of the Lagrange mesh of order k over the reference element
 (tetrahedron or hexahedron).
 Provided V is the returned vector, if i denotes the rank of a point in XLiFE++
 then the corresponding point is V[i] in class subdivision::TetrahedronMesh or
 subdivision::HexahedronMesh, referred to as xxxMesh in the following.
 */
template <class ST_>
const vector<number_t> numberingConversion(const number_t order) {
//  Mapping between geometrical position and local numbering in class xxxMesh
//  and in XLiFE++ :
  vector< pair < vector<number_t>, number_t > > PTM, PMe;

//  1. Build the mapping for class xxxMesh
  vector< vector<number_t> > numTM = ST_::numberingOfVertices(order);
  vector< vector<number_t> >::const_iterator itnumTM;
  number_t rank = 0;
  for (itnumTM=numTM.begin(); itnumTM != numTM.end(); itnumTM++) {
    PTM.push_back(make_pair(*itnumTM,rank++));
  }

//  2. Build the mapping for XLiFE++
  const number_t nbPoints = numTM.size();
  const number_t nbCoord = 3;
  number_t** s2t = new number_t*[nbCoord];
  for ( dimen_t d = 0; d < nbCoord; d++ ) { s2t[d] = new number_t[nbPoints]; }
  tensorNumbering<ST_>(order,s2t);
  // The array s2t contains indices following the numbering rule of XLiFE++ on the
  // 1D reference element. In order to make the correspondence with the numbering
  // of class xxxMesh based on barycentric coordinates for the tetrahedron and
  // natural tensor product for the hexahedron, we need a conversion function
  // implemented via the array num2BC:
  //             0                                  1      1D reference element
  //             |----|----|----|----|----|----|----|
  // i: 1    n   n-1  ...   ...  3    2    0      XLiFE++ numbering
  // num2BC[i] : 0    1    2             n-2  n-1   n      class xxxMesh numbering

  vector<number_t> num2BC(order+1);
  num2BC[0] = order;
  num2BC[1] = 0;
  for (number_t i=2; i < order+1; i++ ) { num2BC[i] = order+1-i; }

  vector<number_t> V(nbCoord);
  for ( rank=0; rank < nbPoints; rank++ ) {
    for ( dimen_t d = 0; d < nbCoord; d++ ) { V[d] = num2BC[s2t[d][rank]]; }
      PMe.push_back(make_pair(V,rank));
  }
  for ( dimen_t d = 0; d < nbCoord; d++ ) { delete[] s2t[d]; }
  delete[] s2t;

//  3. Build the correspondence between the two numberings
  sort(PTM.begin(),PTM.end());
  sort(PMe.begin(),PMe.end());
  vector< pair < vector<number_t>, number_t > >::iterator itPTM, itPMe;
  vector<number_t> TM2Me(nbPoints);
  for ( itPTM=PTM.begin(), itPMe=PMe.begin(); itPTM != PTM.end(); itPTM++, itPMe++ ) {
    TM2Me[itPMe->second] = itPTM->second;
  }

  return TM2Me;
}

// HMf2XLf and TMf2XLf are interface arrays to convert subdivision::HexahedronMesh and
// subdivision::TetrahedronMesh numbering of faces into XLiFE++ numbering.
// The first element is unused.

// If i, 1 <= i <= 6, denotes the number of a face of a hexahedron
// in class HexahedronMesh, then the corresponding face is HMf2XLf[i] in XLiFE++.
number_t HMf2XLf[] = {0, 4,1,5,2,6,3};
template <> number_t* faceNumbering<subdivision::Hexahedron>() { return HMf2XLf; }

// If i, 1 <= i <= 4, denotes the number of a face of a tetrahedron
// in class TetrahedronMesh, then the corresponding face is TMf2XLf[i] in XLiFE++.
number_t TMf2XLf[] = {0, 1,2,3,4}; // Identity
template <> number_t* faceNumbering<subdivision::Tetrahedron>() { return TMf2XLf; }

// Instanciations for needed types:
template void Mesh::build3Delements(subdivision::GeomFigureMesh<subdivision::Hexahedron> *TM_p,
                                    const ShapeType elShape, number_t nbsubdom);
template void Mesh::build3Delements(subdivision::GeomFigureMesh<subdivision::Tetrahedron> *TM_p,
                                    const ShapeType elShape, number_t nbsubdom);

#include "copyPtsEltsDoms.hpp"
template void Mesh::copyPtsEltsDoms(subdivision::GeomFigureMesh<subdivision::Hexahedron> *TM_p,
                                    const ShapeType elShape, const dimen_t elementDim,
                                    const dimen_t spaceDim, number_t& el_k,
                                    const vector<string_t>& subdomainNames, const string_t& domName);
template void Mesh::copyPtsEltsDoms(subdivision::GeomFigureMesh<subdivision::Tetrahedron> *TM_p,
                                    const ShapeType elShape, const dimen_t elementDim,
                                    const dimen_t spaceDim, number_t& el_k,
                                    const vector<string_t>& subdomainNames, const string_t& domName);

// Specializations to call the adequate function of XLiFE++:
template <>
void tensorNumbering<subdivision::Hexahedron>(const int interpNum, number_t**& s2h) {
  tensorNumberingHexahedron(interpNum,s2h);
}
template <>
void tensorNumbering<subdivision::Tetrahedron>(const int interpNum, number_t**& s2h) {
  tensorNumberingTetrahedron(interpNum,s2h);
}

} // end of namespace xlifepp
