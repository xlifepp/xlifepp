/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file build2Delements.cpp
  \author Y. Lafranche
  \since 08 Feb 2010
  \date  16 Apr 2014

  \brief Build a XLiFE++ mesh of 2D elements from a mesh computed using a subdivision algorithm.

  The elements taken into account are the triangle and the quadrangle.
*/

#include "../Mesh.hpp"
#include "finiteElements.h"

#include <algorithm>
using namespace std;

namespace xlifepp {

//-------------------------------------------------------------------------------
//   Protected member functions
//-------------------------------------------------------------------------------

/*!
  Termination of the construction of a mesh object of 2D elements by transferring
  data from a subdivision::xxxMesh object into XLiFE++ objects.
 */
template <class ST_> number_t* edgeNumbering();

template <class ST_>
void Mesh::build2Delements(subdivision::GeomFigureMesh<ST_> *TM_p,
                           const ShapeType elShape, number_t nbsubdom) {
  enum {_element_dim_m1=1, _element_dim=2};

  vector<string_t> boundaryNames,interfaceNames, subdomainNames;
  string_t domName;
  manageDomains(TM_p,nbsubdom,_element_dim, boundaryNames,interfaceNames,subdomainNames, domName);

  number_t el_k=0;// for numbering of elements
  copyPtsEltsDoms(TM_p, elShape, _element_dim, geometry_p->dim(), el_k,subdomainNames, domName);

  //   3b. boundary domains (made of segments, edges of triangles or quadrangles)
  // TMe2XLe is an interface array to convert subdivision::xxxMesh numbering of edges
  // into XLiFE++ numbering. If i, 1 <= i <= number of edges per element, denotes the number of an edge
  // of an element in class xxxMesh, then the corresponding edge is TMe2XLe[i] in XLiFE++.
  number_t *TMe2XLe = edgeNumbering<ST_>();

  number_t minEltnum = TM_p->minElementNumber();
  number_t nbBound = TM_p->numberOfBoundaries();
  number_t nb_Boundary_Edges = 0;
  for (number_t numBoundary=1; numBoundary<=nbBound; numBoundary++) {
  // boundaryNames[numBoundary-1] replaces TM_p->boundaryName(numBoundary)
    if (! boundaryNames[numBoundary-1].empty()) {
      MeshDomain* meshdom_p = (new GeomDomain(*this, boundaryNames[numBoundary-1], _element_dim_m1,
                                              TM_p->boundaryDescription(numBoundary)))->meshDomain();
      // For the current boundary, get the list of boundary edges as pairs
      // (element number, local edge number).
      // Numbering convention for the local edges is the same as the one used in XLiFE++ for the triangle ;
      // it is different for the quadrangle. This is why TMe2XLe is needed.
      pair< vector<number_t>,vector<number_t> > TF = TM_p->edgeElementsIn(subdivision::boundaryArea,numBoundary);
      vector<GeomElement*>& v_ge = meshdom_p->geomElements;
      v_ge.reserve(TF.first.size());
      for (vector<number_t>::iterator itT=TF.first.begin(), itF=TF.second.begin();
           itT != TF.first.end(); itT++,itF++) {
         v_ge.push_back(new GeomElement(elements_[*itT - minEltnum],TMe2XLe[*itF], el_k + 1));
                                        // element N is at rank N-minEltnum in the vector elements_
         el_k++;
      }
      domains_.push_back(meshdom_p);
      nb_Boundary_Edges += v_ge.size();
    }
  }
  // test the coherence of the mesh
/* --> commented since it is valid only if all the boundary domains are created
   if (nb_Boundary_Edges != TM_p->numberOfEdgesIn(subdivision::boundaryArea)) {
    error("bad_nbsides",words("boundary edges"),nb_Boundary_Edges);
   }
*/

  // initialization of protected data members of class Mesh
  comment_ = TM_p->title();
  lastIndex_ = el_k ;
}
//-------------------------------------------------------------------------------
// Extern functions
//-------------------------------------------------------------------------------
template <class ST_>
void tensorNumbering(const int interpNum, std::vector<number_t>& s2t);

/*!
 The following function returns a vector giving the correspondence between the two
 numberings of the points of the Lagrange mesh of order k over the reference element
 (triangle or quadrangle).
 Provided V is the returned vector, if i denotes the rank of a point in XLiFE++,
 then the corresponding point is V[i] in class subdivision::TriangleMesh or
 subdivision::QuadrangleMesh, referred to as xxxMesh in the following.
 */
template <class ST_>
const vector<number_t> numberingConversion(const number_t order) {
//  Mapping between geometrical position and local numbering in class xxxMesh
//  and in XLiFE++ :
  vector< pair < vector<number_t>, number_t > > PTM, PMe;

//  1. Build the mapping for class xxxMesh
  vector< vector<number_t> > numTM = ST_::numberingOfVertices(order);
  vector< vector<number_t> >::const_iterator itnumTM;
  number_t rank = 0;
  for (itnumTM=numTM.begin(); itnumTM != numTM.end(); itnumTM++) {
    PTM.push_back(make_pair(*itnumTM,rank++));
  }

//  2. Build the mapping for XLiFE++
  const number_t nbPoints = numTM.size();
  const number_t nbCoord = 2;
  vector<number_t> s2t(nbCoord*nbPoints);
  tensorNumbering<ST_>(order,s2t);
  // The array s2t contains indices following the numbering rule of XLiFE++ on the
  // 1D reference element. In order to make the correspondence with the numbering
  // of class xxxMesh based on barycentric coordinates for the triangle and
  // natural tensor product for the quadrangle, we need a conversion function
  // implemented via the array num2BC:
  //             0                                  1      1D reference element
  //             |----|----|----|----|----|----|----|
  // i: 1    n   n-1  ...   ...  3    2    0      XLiFE++ numbering
  // num2BC[i] : 0    1    2             n-2  n-1   n      class xxxMesh numbering

  vector<number_t> num2BC(order+1);
  num2BC[0] = order;
  num2BC[1] = 0;
  for (number_t i=2; i < order+1; i++ ) { num2BC[i] = order+1-i; }

  vector<number_t> V(nbCoord);
  for ( number_t i=rank=0; rank < nbPoints; rank++ ) {
    for ( dimen_t d = 0; d < nbCoord; d++ ) { V[d] = num2BC[s2t[i++]]; }
    PMe.push_back(make_pair(V,rank));
  }

//  3. Build the correspondence between the two numberings
  sort(PTM.begin(),PTM.end());
  sort(PMe.begin(),PMe.end());
  vector< pair < vector<number_t>, number_t > >::iterator itPTM, itPMe;
  vector<number_t> TM2Me(nbPoints);
  for ( itPTM=PTM.begin(), itPMe=PMe.begin(); itPTM != PTM.end(); itPTM++, itPMe++ ) {
    TM2Me[itPMe->second] = itPTM->second;
  }

  return TM2Me;
}

// QMe2XLe and TMe2XLe are interface arrays to convert subdivision::QuadrangleMesh and
// subdivision::TriangleMesh numbering of edges into XLiFE++ numbering.
// The first element is unused.

// If i, 1 <= i <= 4, denotes the number of a edge of a quadrangle
// in class QuadrangleMesh, then the corresponding edge is QMe2XLe[i] in XLiFE++.
number_t QMe2XLe[] = {0, 4,1,2,3};
template <> number_t* edgeNumbering<subdivision::Quadrangle>() { return QMe2XLe; }

// If i, 1 <= i <= 3, denotes the number of a edge of a triangle
// in class TriangleMesh, then the corresponding edge is TMe2XLe[i] in XLiFE++.
number_t TMe2XLe[] = {0, 1,2,3}; // Identity
template <> number_t* edgeNumbering<subdivision::Triangle>() { return TMe2XLe; }

// Instanciations for needed types:
template void Mesh::build2Delements(subdivision::GeomFigureMesh<subdivision::Triangle> *TM_p,
                                    const ShapeType elShape, number_t nbsubdom);
template void Mesh::build2Delements(subdivision::GeomFigureMesh<subdivision::Quadrangle> *TM_p,
                                    const ShapeType elShape, number_t nbsubdom);

#include "copyPtsEltsDoms.hpp"
template void Mesh::copyPtsEltsDoms(subdivision::GeomFigureMesh<subdivision::Triangle> *TM_p,
                                    const ShapeType elShape, const dimen_t elementDim,
                                    const dimen_t spaceDim, number_t& el_k,
                                    const vector<string_t>& subdomainNames, const string_t& domName);
template void Mesh::copyPtsEltsDoms(subdivision::GeomFigureMesh<subdivision::Quadrangle> *TM_p,
                                    const ShapeType elShape, const dimen_t elementDim,
                                    const dimen_t spaceDim, number_t& el_k,
                                    const vector<string_t>& subdomainNames, const string_t& domName);

// Specializations to call the adequate function of XLiFE++:
template <>
void tensorNumbering<subdivision::Triangle>(const int interpNum, std::vector<number_t>& s2t) {
   tensorNumberingTriangle(interpNum,s2t);
}
template <>
void tensorNumbering<subdivision::Quadrangle>(const int interpNum, std::vector<number_t>& s2t) {
   tensorNumberingQuadrangle(interpNum,s2t);
}

} // end of namespace xlifepp
