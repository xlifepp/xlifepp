/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file copyPtsEltsDoms.hpp
  \author Y. Lafranche
  \since 16 Apr 2014
  \date  16 Apr 2014

  \brief Definition of function copyPtsEltsDoms (same code used in 2D and 3D).

  This file is intended to be included as is in build2Delements.cpp and build3Delements.cpp
  in order to create the function for the right types.
*/

//! utility for numbering conversion
template <class ST_> const vector<number_t> numberingConversion(const number_t order);

//! Utilitary function to copy information from subdivision mesh to XLiFE++
template <class ST_>
void Mesh::copyPtsEltsDoms(subdivision::GeomFigureMesh<ST_> *TM_p, const ShapeType elShape,
                           const dimen_t elementDim, const dimen_t spaceDim, number_t& el_k,
                           const vector<string_t>& subdomainNames, const string_t& domName) {
  order_ = TM_p->order();
  if ( order_ == 1 ) { firstOrderMesh_p = this; }
  else               { firstOrderMesh_p = nullptr;    }
  // definition of interpolation and reference elements
  Interpolation* interp_p = findInterpolation(Lagrange,standard,order_,H1);
  RefElement* ref_p = findRefElement(elShape,interp_p);

  // initialization of public data members of class Mesh
  //
  //  1. copy points coordinates
  number_t nb_geom_Pts = TM_p->numberOfVertices();
  nodes.resize(nb_geom_Pts);
  for (number_t pt_n=1; pt_n<=nb_geom_Pts; pt_n++) {
    nodes[pt_n-1] = Point(TM_p->vertexCoord(pt_n));
  }
  //
  //  2. copy elements (define vector<GeomElement*> elements_ of mesh)
  // TM2Me is an interface array to convert subdivision::xxxMesh numbering of points
  // into XLiFE++ mesh numbering. For order 1 and 2, and for the tetrahedron, the triangle
  // and the quadrangle, the conversion function is the identity.
  vector<number_t> TM2Me = numberingConversion<ST_>(order_);
//----------
/*   cout << endl << "TM2Me a l'ordre " << order_ << endl;
   for (vector<number_t>::iterator i=TM2Me.begin(); i != TM2Me.end(); i++) {
    cout << *i+1 << ", ";   }
   cout << endl;*/
//----------

  number_t nVBE = TM_p->numberOfVerticesByElement();
  if (nVBE != ref_p->nbPts()) {
    error("bad_nbpts_per_elt",nVBE, ref_p->nbPts());
  }
  number_t offsetVnum = 1 - TM_p->minVertexNumber();  // to make smallest vertex number = 1
  number_t minEltnum = TM_p->minElementNumber();
  number_t nb_Elements = TM_p->numberOfElements();
  elements_.resize(nb_Elements);
  number_t nbVert = ST_::numberOfMainVertices();
  el_k=0;// for numbering of elements
  for (number_t el_n=minEltnum; el_n<nb_Elements+minEltnum; el_n++,el_k++) {
    elements_[el_k] = new GeomElement(this, ref_p, spaceDim, el_k + 1);
    MeshElement* melt = elements_[el_k]->meshElement();
    vector<number_t> Tet = TM_p->element(el_n);
    for (number_t i=0; i<nVBE; i++) { melt->nodeNumbers[i] = Tet[ TM2Me[i] ] + offsetVnum ; }
    // update vertex numbers: the vertexNumbers are the first of nodeNumbers
    for (number_t i=0; i<nbVert; i++) { melt->vertexNumbers[i] = melt->nodeNumbers[i]; }
    melt->setNodes(nodes); //update node pointers
  }
  //construct vertex indices (begins at 1)
  vertices_= TM_p->verticesOfOrder1();
  //
  //  3. copy domains information
  number_t nbBound = TM_p->numberOfBoundaries();
  number_t nbIntrf = TM_p->numberOfInterfaces();
  number_t nbSbDom = TM_p->numberOfSubdomains();
  number_t nbTotDom(nbSbDom);
  if (nbSbDom > 1) { nbTotDom++; }
  domains_.reserve(nbBound+nbIntrf+nbTotDom);
  //   3a. internal domain (made of elements such as triangles, quadrangles, tetrahedra or hexahedra)
  //       Unique domain or union of all the subdomains defined in the mesh
  {
    string domdescr;
    if (nbSbDom == 1) { domdescr = TM_p->subdomainDescription(nbSbDom); }
    else { domdescr = "whole domain, union of all the subdomains"; }
    MeshDomain* meshdom_p = (new GeomDomain(*this, domName, elementDim, domdescr))->meshDomain();
    meshdom_p->geomElements = elements_;
    domains_.push_back(meshdom_p);
  }
  if (nbSbDom > 1) {// several subdomains are defined in the mesh
    for (number_t numSbDom=1; numSbDom<=nbSbDom; numSbDom++) {
      // subdomainNames[numSbDom-1] replaces TM_p->subdomainName(numSbDom)
      MeshDomain* meshdom_p = (new GeomDomain(*this, TM_p->subdomainName(numSbDom), elementDim,
                                              TM_p->subdomainDescription(numSbDom)))->meshDomain();
      // For the current subdomain, get the list of elements given by their number.
      vector<number_t> LE = TM_p->elementsIn(subdivision::subdomainArea,numSbDom);
      vector<GeomElement*>& v_ge = meshdom_p->geomElements;
      v_ge.reserve(LE.size());
      for (vector<number_t>::iterator itT=LE.begin(); itT != LE.end(); itT++) {
        v_ge.push_back(elements_[*itT - minEltnum]); // element N is at rank N-minEltnum in the vector elements_
      }
      domains_.push_back(meshdom_p);
    }
  }
}
