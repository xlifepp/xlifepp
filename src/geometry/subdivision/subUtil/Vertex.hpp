/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Vertex.hpp
  \author Y. Lafranche
  \since 07 Dec 2007
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::Vertex class

 This class holds the definition of the vertex of a polyhedron.
 Each vertex is associated to a geometrical point defined by the class Point.
*/

#ifndef VERTEX_HPP
#define VERTEX_HPP

#include "TopoGeom.hpp"
#include "PointUtils.hpp"

#include <iostream>

namespace xlifepp {
namespace subdivision {

/*!
   \class Vertex
*/
class Vertex
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! default constructor
   Vertex(){}
   //! full constructor
   Vertex(const number_t num, const refnum_t localcod, const Point& P)
   : num_(num), localcod_(localcod), P_(P){}

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! returns localization code
   refnum_t locCode() const {return localcod_;}

   //! returns vertex number
   number_t number() const {return num_;}

   //! returns geometrical point associated to the vertex
   Point geomPt() const {return P_;}

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   //! prints, on stream os, the attributes of the vertex
   void print(std::ostream& os, const TopoGeom& B) const ;
   void print(PrintStream& os, const TopoGeom& B) const {print(os.currentStream(),B);}

   //! prints, on stream os, the coordinates of the vertex
   void print(std::ostream& os) const ;
   void print(PrintStream& os) const {print(os.currentStream());}

   //! prints, on stream os, the coordinates of the vertex for TeX exploitation
   void printTeX(std::ostream& os) const ;
   void printTeX(PrintStream& os) const {printTeX(os.currentStream());}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! square of the distance between the current vertex and vertex V
   real_t squareDistance(const Vertex& V) const ;

private:
   //! number of the vertex in the global numbering
   number_t num_;
   //! internal localization code with respect to the areas
   refnum_t localcod_;
   //! geometrical point associated to the vertex
   Point P_;


}; // end of Class Vertex

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* VERTEX_HPP */
