/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HexahedronMesh.hpp
  \author Y. Lafranche
  \since 07 Fev 2014
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::HexahedronMesh class

  This class is an abstract class that holds a mesh of hexahedra built by
  successive subdivisions of an initial elementary mesh consisting of several hexahedra.

  The numbering convention used to denote the vertices, faces and edges of the
  hexahedrons is described on top of the header file of the class xlifepp::subdivision::Hexahedron,
  namely Hexahedron.hpp.

  inheriting classes
  ------------------
    - xlifepp::subdivision::VolMeshHexCube
    - xlifepp::subdivision::VolMeshHexCylinder
*/

#ifndef HEXAHEDRON_MESH_HPP
#define HEXAHEDRON_MESH_HPP

#include "Hexahedron.hpp"
#include "CartesianMesh.hpp"

#include <iostream>
#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class HexahedronMesh
*/
class HexahedronMesh : public CartesianMesh<Hexahedron>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   HexahedronMesh(const number_t nbsubdiv, const number_t order, const number_t type,
                  const number_t minVertexNum, const number_t minElementNum);

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   void printTeXNumberConvention(std::ostream& ftex) const{}
     //! prints, on stream ftex, Fig4TeX instructions to draw the numbering
     //! convention for a hexahedron mesh of order k
   virtual void printTeXNumberConvention(PrintStream& ftex) const {printTeXNumberConvention(ftex.currentStream());}

protected:
//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! subdivision of a hexahedron into 8 hexahedra
   void algoSubdivH(const Hexahedron& T, number_t& ElementNum, number_t& VertexNum,
                    std::vector<Hexahedron>& listT, map_pair_num& SeenEdges,
                    map_set_num& SeenFaces);

   //! build the mesh
   void buildMesh(number_t& VertexNum);// override

   //! create high order vertices inside the face numFace of element Elem
   virtual void createHOfV(Hexahedron& Elem, const number_t order, number_t& VertexNum,
                           const number_t numFace, map_set_pair_in& SeenFaces);

   //! create high order vertices inside the hexahedron
   virtual void createHOiV(Hexahedron& T, const number_t order, number_t& VertexNum);

   //! create initial mesh for a geometry of revolution
   void initMesh(const number_t nbslices,
                 const real_t radius1, const real_t radius2, const std::vector<Point>& CharacPts,
                 number_t& VertexNum, number_t& ElementNum, const std::vector<ShapeInfo>& vSI);

//--- Fig4TeX printing functions

   //! prints, on stream ftex, definition of some TeX macros
   virtual void printTeXHeader(std::ostream& ftex) const;
   virtual void printTeXHeader(PrintStream& ftex) const {printTeXHeader(ftex.currentStream());}

private:
   /*! tables holding the original and sorted barycentric coordinates
       Initialized in constructor and only used in createHOfV. */
   std::vector< std::vector<number_t> > tobc, stobc;

}; // end of Class HexahedronMesh

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* HEXAHEDRON_MESH_HPP */
