/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomFigureMesh.hpp
  \author Y. Lafranche
  \since 22 May 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::GeomFigureMesh class

  This class is a template abstract base class used as a frame for
  different kinds of meshes consisting of the same geometric figures, such as
  triangles, tetrahedrons, quadrangles and hexahedrons. These meshes are built
  by a subdivision process, starting from an initial configuration of elements.

  inheriting classes
  ------------------
    - xlifepp::subdivision::SimplexMesh
    - xlifepp::subdivision::HexahedronMesh
*/

#ifndef GEOM_FIGURE_MESH_HPP
#define GEOM_FIGURE_MESH_HPP

#include "SubdivisionMesh.hpp"

namespace xlifepp {
namespace subdivision {

/*!
   \class GeomFigureMesh
*/
template<class T_>
class GeomFigureMesh : public SubdivisionMesh {
public:

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   GeomFigureMesh(const number_t nbsubdiv, const number_t order, const number_t type,
                  const number_t minVertexNum, const number_t minElementNum, const number_t tnbVpEl,
                  const number_t nbVpEl, const number_t nbEpEl, const number_t nbFpEl, const number_t subF)
: SubdivisionMesh(nbsubdiv, order, type, minVertexNum, minElementNum),
                  total_nb_vertices_by_element_(tnbVpEl), nb_main_vertices_by_element_(nbVpEl),
                  nb_edges_by_element_(nbEpEl), nb_faces_by_element_(nbFpEl), subdivisionFactor_(subF)
{}

//-------------------------------------------------------------------------------
//  Public member functions
//-------------------------------------------------------------------------------
   /*!
     returns the list of all the vertices of element num,
     each given by its number, with num such that
     minElementNum_ <= num <= numberOfElements+minElementNum_-1
    */
   virtual std::vector<number_t> element(const number_t num) const;

   /*!
     returns the localization code of the element num, with num such that
     minElementNum_ <= num <= numberOfElements+minElementNum_-1
   */
   refnum_t locCode(const number_t num) const;

   /*!
     returns a normal vector to the face number i >= 1 of element num,
     with num such that minElementNum_ <= num <= numberOfElements+minElementNum_-1
     The vector is not normalized and is oriented towards the exterior of the element.
    */
   virtual std::vector<real_t> faceExtNormalVec(const number_t num, const number_t i) const;

   /*!
     returns the orientation of the face number i >= 1 of element num, with num such that
     minElementNum_ <= num <= numberOfElements+minElementNum_-1. The function:
     - returns 1 if the first three vertices (i,j,k) of the face define a vector ij x ik
       oriented towards the exterior of the element,
     - returns -1 otherwise.
    */
   virtual int faceOrientation(const number_t num, const number_t i) const;

   /*!
     returns the list of elements of the mesh belonging to an area of kind TA
     or belonging to any area of kind TA if num=0 (given by their number)
    */
   virtual std::vector<number_t> elementsIn(const topologicalArea TA, const number_t num = 0) const;

   //! returns the number of elements in the mesh
   virtual number_t numberOfElements() const { return listT_.size(); }

   //! returns the number of vertices of order 1 per element
   number_t numberOfMainVerticesByElement() const { return nb_main_vertices_by_element_; }

   /*!
     returns the total number of vertices of each element (equals listT_[0].numberOfVertices())
     This value is checked in the constructor for each element in listT_.
     For a simplex, equals C_{k+n}^n with k=order_ and n="number of main vertices by element minus 1"
     For a triangle,    this value equals C_{k+2}^2 = (k+1)(k+2)/2.
     For a tetrahedron, this value equals C_{k+3}^3 = (k+1)(k+2)(k+3)/6.
     For a hexahedron,  this value equals (k+1)^3.
    */
   virtual number_t numberOfVerticesByElement() const { return total_nb_vertices_by_element_; }


   /*!
     returns the number of edges belonging to an area of kind TA
     or belonging to any area of this kind if num=0.
    */
   number_t numberOfEdgesIn(const topologicalArea TA, const number_t num = 0) const ;

   /*!
     returns the list of edges of the elements of the mesh belonging to an area of kind TA
     or belonging to any area of this kind if num=0.
     Each edge is defined by a pair of the form (element number, local edge number)
    */
   std::pair< std::vector<number_t>,std::vector<number_t> > edgeElementsIn
                            (const topologicalArea TA, const number_t num = 0) const ;

   /*!
     returns the list of edges of the mesh belonging to an area of kind TA
     or belonging to any area of this kind if num=0 (given by their number)
    */
   std::vector< pair_nn > edgesIn(const topologicalArea TA, const number_t num = 0) const ;

//  functions for 3D geometries
   /*!
     returns the number of faces belonging to an area of kind TA
     or belonging to any area of this kind if num=0
    */
   number_t numberOfFacesIn(const topologicalArea TA, const number_t num = 0) const ;

   //! returns the number of the internal faces of the mesh
   number_t numberOfFacesInside() const ;

   /*!
     returns the list of faces of the elements of the mesh belonging to an area of kind TA
     or belonging to any area of this kind if num=0.
     Each face is defined by a pair of the form (element number, local face number)
    */
   std::pair< std::vector<number_t>,std::vector<number_t> > faceElementsIn
                                     (const topologicalArea TA, const number_t num = 0) const ;

   /*!
     returns the list of faces of the mesh belonging to a boundary
     or belonging to any boundary if numBoundary=0 (given by their number)
    */
   std::vector< std::vector<number_t> > facesIn(const topologicalArea TA, const number_t num = 0) const ;

   //! returns the list of the internal faces of the mesh (given by their number)
   map_set_vec facesInside() const ;

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   //! prints, on stream os, the mesh with vertices coordinates
   virtual void printall(std::ostream& os) const ;
   virtual void printall(PrintStream& os) const {printall(os.currentStream());}

   //! prints, on stream os, the mesh with vertices numbers
   virtual void print(std::ostream& os) const ;
   virtual void print(PrintStream& os) const {print(os.currentStream());}

   /*!
     prints, on stream ftex, fig4tex instructions to draw a mesh with
     observation direction defined by longitude psi and latitude theta
    */
   virtual void printTeX(std::ostream& ftex, const float psi=-30, const float theta=25,
                         const number_t nbviews=1, const std::string& DimProj="4cm,orthogonal",
                         const bool withInterface=true, const bool withElems=false) const ;
   virtual void printTeX(PrintStream& os, const float psi=-30, const float theta=25,
                         const number_t nbviews=1, const std::string& dimProj="4cm,orthogonal",
                         const bool withInterface=true, const bool withElems=false) const
        {printTeX(os.currentStream(), psi, theta, nbviews, dimProj, withInterface, withElems);}
   /*!
     prints, on stream ftex, Fig4TeX instructions to draw the numbering
     convention for a element mesh of order k
    */
   virtual void printTeXNumberConvention(std::ostream&) const = 0;
   virtual void printTeXNumberConvention(PrintStream&) const = 0;

protected:
   /*!
     list of all the elements in the final mesh
     the number assigned to the element listT_[k] is equal to k+minElementNum_
    */
   std::vector<T_> listT_;
   //! total number of vertices of each element for the current order of approximation
   const number_t total_nb_vertices_by_element_;
   //! number of main vertices of each element, i.e. number of vertices of order 1
   const number_t nb_main_vertices_by_element_;
   //! number of edges of each element
   const number_t nb_edges_by_element_;
   //! number of faces of each element
   const number_t nb_faces_by_element_;
   //! number of pieces an element is split into
   const number_t subdivisionFactor_;
//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! subdivision of an element into subdivisionFactor_ smaller ones
   virtual void algoSubdiv(const T_& T, number_t& ElementNum, number_t& VertexNum,
                           std::vector<T_>& listT, map_pair_num& SeenEdges);

   //! build the mesh, compute high order vertices and check consistency
   void buildNcheck(number_t& VertexNum);

   //! build the mesh
   virtual void buildMesh(number_t& VertexNum);

   //! to check consistency of the elements of the mesh
   void checkElements();

   //! create high order vertices
   void createHOV(number_t& VertexNum);

   //! create or retrieve high order vertices on the edge numEdge of element Elem
   virtual void createHOeV(T_& Elem, const number_t order, number_t& VertexNum,
                           const number_t numEdge, map_set_pair& SeenEdges);

   //! create or retrieve high order vertices inside the face numFace of element Elem
   virtual void createHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                           const number_t numFace, map_set_pair_in& SeenFaces) = 0;

   //! create high order vertices inside the element Elem
   virtual void createHOiV(T_& Elem, const number_t order, number_t& VertexNum) = 0;

   //--- Fig4TeX printing functions

   //! prints, on stream ftex, definition of some TeX macros
   virtual void printTeXHeader(std::ostream& ftex) const;

   /*!
     prints, on stream ftex, Fig4TeX instructions to define points to be used
     for the display of the mesh
    */
   virtual void printTeXPoints(std::ostream& ftex, const bool withInterface) const;

   /*!
     prints, on stream ftex, Fig4TeX instructions to draw faces or edges of a mesh
     belonging to areas of kind TA, sorted along a direction defined by psi and theta.
    */
   virtual void printTeXSortedAreaFoE(std::ostream& ftex, const topologicalArea TA,
                                      const float psi, const float theta) const;

   //--- Low level utilitary functions

   /*!
     returns the list of edges of the mesh belonging to an area of kind TA
     or belonging to any area of this kind if num=0 (given by their rank)
    */
   std::vector< pair_nn > rk_edgesIn(const topologicalArea TA, const number_t num = 0) const ;

   /*!
     returns the list of faces of the mesh belonging to an area of kind TA
     or belonging to any area of this kind if num=0 (given by their rank)
    */
   std::vector< std::vector<number_t> > rk_facesIn(const topologicalArea TA, const number_t num = 0) const ;

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
private:
   /*!
     write on stream ftex Fig4TeX instructions to create the graphical file and
     the associated TeX box. Selected faces and edges belong to areas of kind TA.
     Longitude psi and latitude theta define the observation direction.
     boxName is the name of the TeX box created and caption is the caption
     written beneath.
    */
   void createFileNBox(std::ostream& ftex, const topologicalArea TA,
                       const float psi, const float theta, const char* boxName,
                       const std::string& caption) const;

}; // end of Class GeomFigureMesh

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* GEOM_FIGURE_MESH_HPP */
