/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshTriRev.cpp
  \author Y. Lafranche
  \since 6 Jul 2015
  \date 6 Jul 2015

  \brief Implementation of xlifepp::subdivision::SurfMeshTriCone and xlifepp::subdivision::SurfMeshTriCylinder classes members and related functions
*/

#include "SurfMeshTriRev.hpp"
#include <sstream>

namespace xlifepp {
namespace subdivision {
using std::vector;
using std::string;
using std::ostringstream;

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of triangles by successive subdivisions in a cone or a truncated cone.
 The cylinder is a particular kind of truncated cone which has a special constructor below.

 \param nbslices: number of slices of elements orthogonal to the axis of the cone
            (0 by default, which means that the number of slices is automatically
            computed ; if nbslices>0, the given value has precedence and is taken
            into account)

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each triangle is subdivided into 4 triangles with the
   same orientation as the original one.

 \param order: order of the triangles in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   triangle is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, the vertices
   belonging to the appropriate boundary lie on the surface of the cone.

 \param type: type of the subdivision (1 by default)
   type equal to 0 leads to a simple (flat) subdivision of the initial
        mesh where new vertices are all the midpoints of the edges.
   type equal to 1 leads to a subdivision where the boundary vertices are computed
        so that they lie on the corresponding surface

 \param radius1       : radius of the basis of the cone containing P1 (1. by default)
 \param radius2       : radius of the other basis of the cone containing P2 (0.5 by default)
 \param P1, P2        : the two end points of the axis of the cone
 \param endShape1     : shape of the end surface of the cone on P1 or P2 side
 \param endShape2     : (Flat by default). The possible values are of GeomEndShape type.
 \param distance1     : distance between P1 (resp. P2) and the apex of the surface when the
 \param distance2     : previous parameter is non Flat (default=0.)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)

 Nota: If none of the radii is null, this defines a truncated cone. The parameters endShape1
        ans endShape2 are both applicable.
        If one of the radii is null, the corresponding point, P1 or P2, is the apex of the cone
        and the corresponding endShape is not applicable.
 */
SurfMeshTriCone::SurfMeshTriCone(const number_t nbslices,
                                 const number_t nbsubdiv, const number_t order,
                                 const number_t type, const real_t radius1, const real_t radius2,
                                 const Point P1, const Point P2,
                                 const GeomEndShape endShape1, const GeomEndShape endShape2,
                                 const real_t distance1, const real_t distance2,
                                 const number_t minVertexNum, const number_t minElementNum)
:TriangleMesh(nbsubdiv, order,type, minVertexNum, minElementNum) {
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   vector<Point> CP;   CP.push_back(P1); CP.push_back(P2);
   vector<ShapeInfo> vSI;
    vSI.push_back(ShapeInfo(endShape1,distance1));
    vSI.push_back(ShapeInfo(endShape2,distance2));
   if (radius1 > 0. && radius2 > 0.) {// truncated cone
      initMesh(nbslices,radius1,radius2,CP,VertexNum,ElementNum,vSI);
   }
   else {// "true" cone: one of the radii is null
      initMeshCone(nbslices,radius1,radius2,CP,VertexNum,ElementNum,vSI);
   }
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
/*!
 Create the initial mesh (or "seed" of the mesh) to be subdivided for a true cone,
 i.e. defined by an axis of revolution and one circular section.

 In the following, the word "object" is used to designate the cone.

 nbslices: number of slices of elements orthogonal to the axis of the object
               If this input value is equal to 0, a number of slices is computed from
               the geometrical data.
 radius: radius of the section of the basis of the object
 CharacPts: the two end points of the axis of the object ; the first one is the center
               of the basis, the second one is the apex of the cone.
 swappedPts: false if the points in CharacPts are the original data points (in the order
               given by the user), true if they have been swapped, since here we need P2
               to be the apex of the cone.
 VertexNum: number of the last vertex created (modified on output)
 ElementNum: number of the last element created (modified on output)
 SI: shape information at the end of the object (on the basis side)
 */
void SurfMeshTriCone::initMeshCone(const number_t nbslices, const real_t radius1, const real_t radius2,
                                   const vector<Point>& CharacPts,
                                   number_t& VertexNum, number_t& ElementNum,
                                   const vector<ShapeInfo>& vSI) {
   real_t R1, R2, slice_height;
   Point P1, P2;
   vector<ShapeInfo> cvSI;
   string bottomPt, topPt, shape;
   bool iscone;
   DefaultGeometry *DG;
   SurfPlane *SP;
   SurfCone *SC;
   vector <PatchGeometry *> EBS;
   int nb_sl;
   initRevMesh(nbslices, radius1,radius2, CharacPts, vSI,
               R1,R2, P1,P2, cvSI, bottomPt, topPt, iscone, shape, DG, SP, SC, EBS, nb_sl, slice_height);

   delete EBS[1];// discard this unused end shape
   number_t iPh = 0, iPhSD;
   title_ = shape + " - Triangle mesh";
   { // Definition of 0 or 1 boundary...
      number_t nbBound = 0;
      if ( cvSI[0].shapeCode_ == None ) { nbBound++; }
      vector<number_t> B_patch(nbBound), B_dimen(nbBound);
      for (size_t i=0; i<nbBound; i++) { B_patch[i] = i+1; B_dimen[i] = 1; }
     // ... nbIntrf interfaces...
     // (a plane between two successive slices, plus eventually one plane between the end slice
     //  and the corresponding "hat" end shape)
      number_t nbIntrf = nb_sl-1, nbSbDom = nb_sl;
      if (EBS[0]->curvedShape()) {nbIntrf++; nbSbDom++;} // if non Flat
      else if ( cvSI[0].shapeCode_ != None ) { nbSbDom++; }   // Flat end shape
      vector<number_t> I_patch(nbIntrf), I_dimen(nbIntrf);
      number_t numPa = nbBound;
      for (size_t i=0; i<nbIntrf; i++) { I_patch[i] = ++numPa; I_dimen[i] = 1; }
     // ...and nbSbDom subdomains
     // (each slice plus eventually one "hat" end shape)
      vector<number_t> D_patch(nbSbDom), D_dimen(nbSbDom);
      for (size_t i=0; i<nbSbDom; i++) { D_patch[i] = ++numPa; D_dimen[i] = 2; }

      number_t nbBI =  nbBound + nbIntrf;
      number_t nbPatches =  nbBI + nbSbDom;
      vector<PatchGeometry *> P_geom(nbPatches);
      for (size_t i=1; i<nbBI; i++) { P_geom[i] = SP; }

      if (type_ == 0) {
         // P_geom = {SP,      SP, SP,...   SP,  EBS[0],DG,...   DG}
         //           \ nbBound /   \_nbIntrf_/  \__   nbSbDom  __/   (nbBound may be 0)
         delete SC; // not needed
         for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = DG; }
     }
      else {
         // P_geom = {SP,      SP, SP,...   SP,  EBS[0],SC,...   SC}
         //           \ nbBound /   \_nbIntrf_/  \__   nbSbDom  __/   (nbBound may be 0)
         delete DG; // not needed
         for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = SC; }
      }
      if ( cvSI[0].shapeCode_ != None ) { P_geom[nbBI] = EBS[0]; }
      TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
      iPhSD = nbBound+nbIntrf;
   }
   refnum_t sBEs1=0; // localization code of the following patches
//       Description of the boundary patches
   if ( cvSI[0].shapeCode_ == None ) {
    TG_.setDescription(++iPh) = "Boundary: End curve on the side of end point " + bottomPt; sBEs1 = TG_.sigma(iPh); }

// Steps 1 and 2 of the construction
   vector<Point> Pt;
   real_t prevRitrf;
   number_t rVaFirst;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
   startConeTrunk(R1, P1, cvSI[0].shapeCode_, bottomPt, EBS, SC, slice_height, nb_sl, iscone, sBEs1,
                  iPh, iPhSD, VertexNum, ElementNum, Pt, prevRitrf, rVaFirst);

// 3. Add triangles on the top
      // Insert last vertex associated to top point in the global list
         refnum_t sDOM = TG_.sigma(iPhSD+1); // Top end subdomain
         listV_.push_back(Vertex(++VertexNum,sDOM,P2)); // rank rVd
      // Insert last 4 triangles in the global list
      number_t rVa = rVaFirst, rVb = rVa+1, rVd = rVaFirst+4;
/*!
 \verbatim
                 d
                 |
             a+2 |                  d is the top point (P2)
              \  |                  a, b  are in a circular section of the object (basis or last interface)
               \ |
     a+3________\|_________b=a+1
                 \
                  \
                   \
                    a
 \endverbatim
*/
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd)); rVa++; rVb++;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd)); rVa++; rVb++;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd)); rVa++; rVb=rVaFirst;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd));

//       Description of the subdomain patches
   if (EBS[0]->curvedShape()) { TG_.setDescription(++iPh) = "End subdomain on the side of end point " + bottomPt; }
   for (int isl=1; isl<=nb_sl; isl++) {
      ostringstream ss;
      ss << "Slice " << isl;
      TG_.setDescription(++iPh) = ss.str();
   }
}

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of triangles by successive subdivisions in a cylinder.

 \param nbslices: number of slices of elements orthogonal to the axis of the cone
            (0 by default, which means that the number of slices is automatically
            computed ; if nbslices>0, the given value has precedence and is taken
            into account)

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each triangle is subdivided into 4 triangles with the
   same orientation as the original one.

 \param order: order of the triangles in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   triangle is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, the vertices
   belonging to the appropriate boundary lie on the surface of the cone.

 \param type: type of the subdivision (1 by default)
   type equal to 0 leads to a simple (flat) subdivision of the initial
        mesh where new vertices are all the midpoints of the edges.
   type equal to 1 leads to a subdivision where the boundary vertices are computed
        so that they lie on the corresponding surface

 \param radius: radius of the cylinder (1. by default)
 \param P1, P2        : the two end points of the axis of the cone
 \param endShape1     : shape of the end surface of the cone on P1 or P2 side
 \param endShape2     : (Flat by default). The possible values are of GeomEndShape type.
 \param distance1     : distance between P1 (resp. P2) and the apex of the surface when the
 \param distance2     : previous parameter is non Flat (default=0.)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
SurfMeshTriCylinder::SurfMeshTriCylinder(const number_t nbslices,
                                         const number_t nbsubdiv, const number_t order,
                                         const number_t type, const real_t radius,
                                         const Point P1, const Point P2,
                                         const GeomEndShape endShape1, const GeomEndShape endShape2,
                                         const real_t distance1, const real_t distance2,
                                         const number_t minVertexNum, const number_t minElementNum)
:TriangleMesh(nbsubdiv, order,type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   vector<Point> CP;   CP.push_back(P1); CP.push_back(P2);
   vector<ShapeInfo> vSI;
   vSI.push_back(ShapeInfo(endShape1,distance1));
   vSI.push_back(ShapeInfo(endShape2,distance2));
   initMesh(nbslices,radius,radius,CP,VertexNum,ElementNum,vSI);
   buildNcheck(VertexNum);
}

} // end of namespace subdivision
} // end of namespace xlifepp
