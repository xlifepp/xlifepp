/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshQuaRev.cpp
  \author Y. Lafranche
  \since 1 Jul 2015
  \date 1 Jul 2015

  \brief Implementation of xlifepp::subdivision::SurfMeshQuaCone and xlifepp::subdivision::SurfMeshQuaCylinder class members and related functions
*/

#include "SurfMeshQuaRev.hpp"
#include <sstream>

namespace xlifepp {
namespace subdivision {
using std::vector;

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of quadrangles by successive subdivisions in a cone or a truncated cone.
 The cylinder is a particular kind of truncated cone which has a special constructor below.

 \param nbslices: number of slices of elements orthogonal to the axis of the cone
            (0 by default, which means that the number of slices is automatically
            computed ; if nbslices>0, the given value has precedence and is taken
            into account)

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each quadrangle is subdivided into 4 quadrangles with the
   same orientation as the original one.

 \param order: order of the quadrangles in the final mesh (1 by default)
   The default value is 1, which leads to a Q1 mesh, in which case each
   quadrangle is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, the vertices
   belonging to the appropriate boundary lie on the surface of the cone.

 \param type: type of the subdivision (1 by default)
   type equal to 0 leads to a simple (flat) subdivision of the initial
        mesh where new vertices are all the midpoints of the edges.
   type equal to 1 leads to a subdivision where the boundary vertices are computed
        so that they lie on the corresponding surface

 \param radius1       : radius of the basis of the cone containing P1 (1. by default)
 \param radius2       : radius of the other basis of the cone containing P2 (0.5 by default)
 \param P1, P2        : the two end points of the axis of the cone
 \param endShape1     : shape of the end surface of the cone on P1 and P2 side
 \param endShape2     : (Flat by default). The possible values are of GeomEndShape type.
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)

 Nota: If none of the radii is null, this defines a truncated cone.
        If one of the radii is null, the corresponding point, P1 or P2, is the apex of the cone.
        But indeed, NONE of the radii should be null, because this leads to degenerated quadrangles
        (several points collapse to the apex of the cone) ; this brings an error during the
        check stage of the mesh thus obtained (wrong number of interfaces).
        However, one can obtain nearly a "true" cone by setting one radius as small as 1.e-15.
 */
SurfMeshQuaCone::SurfMeshQuaCone(const number_t nbslices,
                                 const number_t nbsubdiv, const number_t order,
                                 const number_t type, const real_t radius1, const real_t radius2,
                                 const Point P1, const Point P2,
                                 const GeomEndShape endShape1, const GeomEndShape endShape2,
                                 const number_t minVertexNum, const number_t minElementNum)
: QuadrangleMesh(nbsubdiv, order, type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   vector<Point> CP;   CP.push_back(P1); CP.push_back(P2);
   vector<ShapeInfo> vSI;
   vSI.push_back(ShapeInfo(endShape1,0.));
   vSI.push_back(ShapeInfo(endShape2,0.));
   initMesh(nbslices,radius1,radius2,CP,VertexNum,ElementNum,vSI);
   buildNcheck(VertexNum);
}

/*!
 Build a mesh of quadrangles by successive subdivisions on the surfacce of a cylinder.

 \param nbslices: number of slices of elements orthogonal to the axis of the cone
            (0 by default, which means that the number of slices is automatically
            computed ; if nbslices>0, the given value has precedence and is taken
            into account)

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each quadrangle is subdivided into 4 quadrangles with the
   same orientation as the original one.

 \param order: order of the quadrangles in the final mesh (1 by default)
   The default value is 1, which leads to a Q1 mesh, in which case each
   quadrangle is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, the vertices
   belonging to the appropriate boundary lie on the surface of the cone.

 \param type: type of the subdivision (1 by default)
   type equal to 0 leads to a simple (flat) subdivision of the initial
        mesh where new vertices are all the midpoints of the edges.
   type equal to 1 leads to a subdivision where the boundary vertices are computed
        so that they lie on the corresponding surface

 \param radius: radius of the cylinder (1. by default)
 \param P1, P2        : the two end points of the axis of the cone
 \param endShape1     : shape of the end surface of the cone on P1 and P2 side
 \param endShape2     : (Flat by default). The possible values are of GeomEndShape type.
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 
 See above the other arguments, which are the same as for the previous constructor.
 */
SurfMeshQuaCylinder::SurfMeshQuaCylinder(const number_t nbslices,
                                         const number_t nbsubdiv, const number_t order,
                                         const number_t type, const real_t radius,
                                         const Point P1, const Point P2,
                                         const GeomEndShape endShape1, const GeomEndShape endShape2,
                                         const number_t minVertexNum, const number_t minElementNum)
: QuadrangleMesh(nbsubdiv, order, type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   vector<Point> CP;   CP.push_back(P1); CP.push_back(P2);
   vector<ShapeInfo> vSI;
   vSI.push_back(ShapeInfo(endShape1,0.));
   vSI.push_back(ShapeInfo(endShape2,0.));
   initMesh(nbslices,radius,radius,CP,VertexNum,ElementNum,vSI);
   buildNcheck(VertexNum);
}

} // end of namespace subdivision
} // end of namespace xlifepp
