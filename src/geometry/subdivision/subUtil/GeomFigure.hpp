/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomFigure.hpp
  \author Y. Lafranche
  \since 05 Apr 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::GeomFigure class

  Class xlifepp::subdivision::GeomFigure is an abstract base class that defines a geometric figure as
  a list of vertices.

  inheriting classes
  ------------------
    - xlifepp::subdivision::CartesianFig
    - xlifepp::subdivision::Simplex
*/

#ifndef GEOM_FIGURE_HPP
#define GEOM_FIGURE_HPP

#include "Vertex.hpp"
#include "types.hpp"

#include <iostream>
#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
  \struct HOV_error
  Structure used to transmit information in case an exception is thrown.
  Used in GeomFigure. Type referenced by GeomFigureMesh.
*/
struct HOV_error {
   number_t elemNum, vertexNum;
   number_t rV1, rV2, rV3, rV4;
   std::string mes;
};

/*!
   \class GeomFigure
*/
class GeomFigure
{
template<class T_> friend class GeomFigureMesh; // for createHOeV
template<class T_> friend class SimplexMesh;    // for createHOfV
template<class T_> friend class CartesianMesh;  // for createHOfV
friend class TetrahedronMesh;                   // for createHOiV
friend class HexahedronMesh;                    // for createHOfV and createHOiV

public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   /*!
     constructor by number, number of vertices (initial size of vector) and number
     of boundary side if it lies on a curved patch
    */
   GeomFigure(const number_t num, const number_t nelem, const number_t bdSideNum)
   :num_(num), vertices_(nelem), bdonCP_(bdSideNum){}

   //! destructor
   virtual ~GeomFigure(){}
//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! returns figure number
   number_t number() const { return num_; }

   //! returns the number of all the vertices of the figure
   number_t numberOfVertices() const { return vertices_.size(); }

   //! returns the number of main vertices (of order 1) of the figure
   virtual number_t numberOfO1Vertices() const = 0;

   //! returns the number of edges of the figure
   virtual number_t numberOfEdgesinFig() const = 0;

   //! returns rank of vertex number i in the general list of vertices of class SubdivisionMesh, 1 <= i <= numberOfVertices()
   number_t rankOfVertex(const number_t i) const { return(vertices_[i-1]); }

   //! returns the list of all the vertices (given by their rank in the general list of vertices of class SubdivisionMesh)
   std::vector<number_t> rankOfVertices() const { return vertices_; }

   //! returns the number of the side of the figure lying on a boundary curved patch
   unsigned short bdSideOnCP() const { return bdonCP_; }

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   //! prints, on stream os, the definition of an element
   void print(std::ostream& os) const;
   void print(PrintStream& os) const {print(os.currentStream());}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! returns edge number i >= 1 (given by rank of order 1 vertices) called in createHOeV
   pair_nn rkOfO1VeOnEdge(const number_t i) const;

   //! returns face number i >= 1 (given by rank of order 1 vertices) called in createHOfV
   std::vector<number_t> rkOfO1VeOnFace(const number_t i) const;

   //! returns the high order vertices (given by their rank) of edge number i >= 1
   std::vector<number_t> rkOfHOVeOnEdge(const number_t Order, const number_t i) const;

   //! returns the high order vertices (given by their rank) of face number i >= 1
   virtual std::vector<number_t> rkOfHOVeOnFace(const number_t Order, const number_t i) const = 0;

   /*!
     returns a normal vector to the face number i >= 1
     The vector is not normalized and is oriented towards the exterior of the element.
    */
   std::vector<real_t> extNormVec(const number_t i, const std::vector<Vertex>& listV) const;

protected:
   //! number of the figure
   number_t num_;
   //! rank in the vertex list of the main vertices eventually followed with the high order vertices
   std::vector<number_t> vertices_;
   /*!
     number (> 1) of the edge or face of the figure lying on a boundary curved patch ;
     equals 0 if the figure do not lie on a boundary or if the boundary is not curved.
    */
   unsigned short bdonCP_;

   //! get back the orientation of a face. Returns 1 if positive, -1 if negative.
   virtual short faceOrientation(short indFace) const = 0;

   //! get back the rank of a vertex of an edge
   virtual short getrkEdge(short indEdge, short indVert) const = 0;

   //! get back the rank of a vertex of a face
   virtual short getrkFace(short indFace, short indVert) const = 0;

   //! returns the number of vertices by face of the figure
   virtual number_t numberOfVertByFace() const = 0;

}; // end of Class GeomFigure

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* GEOM_FIGURE_HPP */
