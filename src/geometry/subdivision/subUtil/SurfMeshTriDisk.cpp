/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshTriDisk.cpp
  \author Y. Lafranche
  \since 28 Apr 2014
  \date 28 Apr 2014

  \brief Implementation of xlifepp::subdivision::SurfMeshTriDisk class members and related functions
*/

#include "SurfMeshTriDisk.hpp"
#include <sstream>

namespace xlifepp {
namespace subdivision {
using std::vector;

//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

/*!
 Build a mesh of triangles by successive subdivisions from an initial mesh of triangles.
 Works in 2D and 3D.

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each triangle is subdivided into 4 triangles with the
   same orientation as the original one: if the first triangle is (1,2,3), the
   vector 12x13 defines the exterior normal to the face.

 \param order: order of the triangles in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   triangle is defined by its 3 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh.

 \param type: type of the subdivision (1 by default)
   . if type = 0, the algorithm leads to a simple (flat) subdivision of the initial
   mesh where new vertices are all the midpoints of the edges.
   . if type = 1, the algorithm leads to a subdivision where the vertices belonging to
   a prescribed boundary are computed so that they lie on the circle ; they are computed
   as the radial projections of the vertices of the Lagrange mesh of order k over the
   chord segment.

 \param radius: radius of the circle (1. by default)
 \param Center: center of the circle ((0,0,0) by default)
 \param angmin: angles defining the sector to be meshed ;
 \param angmax: they are measured counterclockwise and given in degrees.

 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
SurfMeshTriDisk::SurfMeshTriDisk(const number_t nbsubdiv, const number_t order, const number_t type,
                                 const real_t radius, const Point Center,
                                 const real_t angmin, const real_t angmax,
                                 const number_t minVertexNum, const number_t minElementNum)
: TriangleMesh(nbsubdiv, order, type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   initMesh(radius,Center,angmin,angmax,VertexNum,ElementNum);
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
// Private member functions
//-------------------------------------------------------------------------------

/*!
  Create the initial mesh (or "seed" of the mesh) to be subdivided.
 */
void SurfMeshTriDisk::initMesh(const real_t radius, const Point& Center,
                               real_t angmin, real_t angmax,
                               number_t& VertexNum, number_t& ElementNum){

   SurfSphere *SS = new SurfSphere(Center,radius);
   DefaultGeometry *DG = new DefaultGeometry();

   title_ = "Triangle mesh of a portion of disk";
   real_t dang = angmax-angmin;
   if (dang < 0.) {
      real_t t = angmin;
      angmin = angmax;
      angmax = t;
      dang = -dang;
   }
   if (dang > 360.) { dang = 360.; angmin = 0.; angmax = 360.; }
   number_t nbBound = 3;
   if (std::abs(dang-360.) < theTolerance) { nbBound = 1; } // closed circle
   {// Definition of nbBound boundaries and 1 subdomain
        vector<number_t> B_patch(nbBound), B_dimen(nbBound);
        vector<number_t> I_patch, I_dimen;// both should be empty here
        vector<number_t> D_patch(1, nbBound+1), D_dimen(1, 2);//D_patch{nbBound+1}, D_dimen{2}
        vector<PatchGeometry *> P_geom(nbBound+1); // patch # 1,...,nbBound, nbBound+1
        for (size_t i=0; i<nbBound; i++) { B_patch[i] = i+1; B_dimen[i] = 1; P_geom[i] = DG;}
        if (type_ != 0) { P_geom[nbBound-1] = SS; }// fit the boundary circle
        P_geom[nbBound] = DG;// the last patch is the main domain
        TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
   }
//       Description of the boundary patches (# 1,...,nbBound)
   int iPh = 0;
   for (size_t i=0; i<nbBound; i++) {
      std::stringstream ss; ss << ++iPh;
      TG_.setDescription(iPh) = "Boundary: " + ss.str();
   }
   if (nbBound != 1) {
      TG_.setDescription(1) += ", first side of the sector, counterclockwise";
      TG_.setDescription(2) += ", second side of the sector, counterclockwise";
   }
   TG_.setDescription(iPh) += ", boundary of the sector"; number_t sCIR = TG_.sigma(iPh);
//       Description of the subdomain patch (# nbBound+1)
   TG_.setDescription(++iPh) = "main domain"; number_t sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries
//       and subdomain they belong to.
   number_t VN = minVertexNum_, EN = minElementNum_;
   real_t alpha = angmin*pi_/180.;
   Point Pt(Center + Point(radius*std::cos(alpha), radius*std::sin(alpha)));
   if (nbBound == 1) { // closed circle
      listV_.push_back(Vertex(VN++,sDOM,Center));  // the center is inside the domain
      listV_.push_back(Vertex(VN++,sCIR|sDOM,Pt)); // the first vertex is on the circle
   }
   else { // arc of circle
      // the center is on the first two boundaries
      listV_.push_back(Vertex(VN++,TG_.sigma(1)|TG_.sigma(2)|sDOM,Center));
      // the first vertex is on the first boundary
      listV_.push_back(Vertex(VN++,TG_.sigma(1)|sCIR|sDOM,Pt));
   }
//       Definition of the remaining vertices and the nbTr associated triangles.
   number_t nbTr = 1 + (dang-1.) / 90.;
   real_t dt = (dang/nbTr)*pi_/180.;
   number_t rV1=1, rV2=2;
   for (number_t k=0; k<nbTr-1; k++, rV1=rV2, rV2++) {
      alpha += dt;
      Point Pt(Center + Point(radius*std::cos(alpha), radius*std::sin(alpha)));
      listV_.push_back(Vertex(VN++,sCIR|sDOM,Pt));
      listT_.push_back(Triangle(EN++, 0, rV1, rV2, 2)); // ranks are needed here
                                           // edge 2 is (rV1, rV2) and lies on the curved boundary
   }
//       Definition of the last triangle, and eventually the last vertex
   alpha += dt;
   Pt = Point(Center + Point(radius*std::cos(alpha), radius*std::sin(alpha)));
   if (nbBound == 1) { // closed circle
      rV2 = 1;
   }
   else { // arc of circle
      listV_.push_back(Vertex(VN++,TG_.sigma(2)|sCIR|sDOM,Pt));
   }
   listT_.push_back(Triangle(EN++, 0, rV1, rV2, 2)); // ranks are needed here
                                        // edge 2 is (rV1, rV2) and lies on the curved boundary
   VertexNum = VN - 1;
   ElementNum = EN - 1;
}

} // end of namespace subdivision
} // end of namespace xlifepp
