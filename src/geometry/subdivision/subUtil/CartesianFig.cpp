/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CartesianFig.cpp
  \author Y. Lafranche
  \since 08 Apr 2014
  \date 08 Apr 2014

  \brief Implementation of xlifepp::subdivision::CartesianFig class members and related functions
*/

#include "CartesianFig.hpp"
#include "Quadrangle.hpp"
#include "Hexahedron.hpp"

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Static variables
//-------------------------------------------------------------------------------
//@{
//! Definition of static variables of CartesianFig class
template<> const number_t CartesianFig<Quadrangle>::nb_main_vertices_ = 4;
template<> const number_t CartesianFig<Quadrangle>::nb_edges_ = 4;
template<> const number_t CartesianFig<Quadrangle>::nb_faces_ = 1;

template<> const number_t CartesianFig<Hexahedron>::nb_main_vertices_ = 8;
template<> const number_t CartesianFig<Hexahedron>::nb_edges_ = 12;
template<> const number_t CartesianFig<Hexahedron>::nb_faces_ = 6;
//@}

/*!
 Access function returning the high order vertices (given by their rank) of face number i >= 1.
 The order is not stored in this class, so it needs to be passed by argument ; it should be
 the correct one, otherwise the result is false.
 For the quadrangle, the face is the element itself and the input number i should be equal to 1.
  The vertices in the array vertices_ are grouped according to the following:
      nb_main_vertices_, (k-1)*nb_edges_, (k-1)^2*nb_faces_
 */
template<class T_>
std::vector<number_t> CartesianFig<T_>::rkOfHOVeOnFace(const number_t Order, const number_t i) const{
   number_t km1=Order-1, km1sq=km1*km1;
   std::vector<number_t> V(km1sq);
   for (int j=0, k=nb_main_vertices_+km1*nb_edges_+(i-1)*km1sq; j<km1sq; j++,k++)
   { V[j] = vertices_[k]; }
   return V;
}

// class instantiations :
template class CartesianFig<Quadrangle>;
template class CartesianFig<Hexahedron>;

} // end of namespace subdivision
} // end of namespace xlifepp
