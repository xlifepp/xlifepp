/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VolMeshTetCube.hpp
  \author Y. Lafranche
  \since 16 Oct 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::VolMeshTetCube class

  This class creates a volumic mesh of a cube or part of cube.
  The mesh is made of tetrahedra and is built by successive subdivisions of an
  initial elementary mesh consisting of n groups of tetrahedra, n=1,...8. Each
  group of tetrahedra fills a cube which is lying in one octant of the 3D space
  around the origin.

  The mesh of the "big" cube is a special case. This big cube can be considered
  as the juxtaposition of 8 small cubes or as itself, leading to two meshes
  obtained by subdividing the 8 small cubes or by subdividing directly this big
  cube.
*/

#ifndef VOL_MESH_TET_CUBE_HPP
#define VOL_MESH_TET_CUBE_HPP

#include "TetrahedronMesh.hpp"

#include <map>
#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class VolMeshTetCube
*/
class VolMeshTetCube : public TetrahedronMesh
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   VolMeshTetCube(const std::vector<std::pair<real_t, dimen_t> >& rots, const int nboctants,
                  const number_t nbsubdiv=0, const number_t order=1,
                  const real_t edgeLength=1., const Point Center=Point(0,0,0),
                  const number_t minVertexNum=1, const number_t minElementNum=1);

private:

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
   //! create initial mesh
   void initMesh(const std::vector<std::pair<real_t, dimen_t> >& rots, const int nboctants,
                 const real_t edgeLength, const Point& Center,
                 number_t& VertexNum, number_t& ElementNum);

   //! create tetrahedrons that subdivide a cube according to type of subdivision
   void subdivCube(const int type,    const number_t U1, const number_t U2,
                   const number_t U3, const number_t U4, const number_t U5,
                   const number_t U6, const number_t U7, const number_t U8,
                   number_t& ElementNum);

}; // end of Class VolMeshTetCube

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* VOL_MESH_TET_CUBE_HPP */
