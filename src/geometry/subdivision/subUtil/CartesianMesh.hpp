/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CartesianMesh.hpp
  \author Y. Lafranche
  \since 08 Apr 2014
  \date 08 Apr 2014

  \brief Definition of the xlifepp::subdivision::CartesianMesh class

  Class xlifepp::subdivision::CartesianMesh is a template abstract class used as a frame for meshes made
  of quadrangles and hexahedrons.

  inheriting classes
  ------------------
    - xlifepp::subdivision::HexahedronMesh
    - xlifepp::subdivision::QuadrangleMesh
*/

#ifndef CARTESIAN_MESH_HPP
#define CARTESIAN_MESH_HPP

#include "CartesianFig.hpp"
#include "GeomFigureMesh.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class CartesianMesh
*/
template<class T_>
class CartesianMesh : public GeomFigureMesh<T_>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   CartesianMesh(const number_t nbsubdiv, const number_t order, const number_t type,
                 const number_t minVertexNum, const number_t minElementNum,
                 const number_t totNbV, const number_t subF)
: GeomFigureMesh<T_>(nbsubdiv, order, type, minVertexNum, minElementNum, totNbV,
                     CartesianFig<T_>::numberOfMainVertices(),
                     CartesianFig<T_>::numberOfEdges(), CartesianFig<T_>::numberOfFaces(), subF)
{}

protected:
//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! compute high order vertices inside the face of element Elem defined by vrV
   void computeHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                    const std::vector<number_t>& vrV);

   //! compute high order vertices inside the face of element Elem in the case of a curved patch
   void computeHOfV4CP(T_& Elem, const number_t order, number_t& VertexNum,
                       const std::vector<number_t>& vrV, const std::vector<short>& vnE);

   //! create or retrieve high order vertices inside the face numFace of element Elem
   virtual void createHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                           const number_t numFace, map_set_pair_in& SeenFaces) = 0;

   //! create high order vertices inside the element Elem
   virtual void createHOiV(T_& Elem, const number_t order, number_t& VertexNum) = 0;

}; // end of Class CartesianMesh

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* CARTESIAN_MESH_HPP */
