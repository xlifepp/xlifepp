/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DefaultGeometry.hpp
  \author Y. Lafranche
  \since 10 Nov 2008
  \date 08 Mar 2014

  \brief Definition of xlifepp::subdivision::DefaultGeometry class

  Class xlifepp::subdivision::DefaultGeometry is a class that defines the default geometry of a patch.
  It is used for patches defining the interior of a 3D domain.
  It is also used for boundary or interface patches of a surfacic domain that
  follow the shape of the surface.

  Inheriting classes
  ------------------
    - xlifepp::subdivision::SurfPlane
*/

#ifndef DEFAULT_GEOMETRY_HPP
#define DEFAULT_GEOMETRY_HPP

#include "PatchGeometry.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class DefaultGeometry
*/
class DefaultGeometry : public PatchGeometry
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   DefaultGeometry():PatchGeometry(0,"none"){}    //!< main constructor

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! projection onto the plane of the barycenter of the points in VP with coefficients in coef
   virtual Point projOnBound(const real_t *coef, const std::vector<Point>& VP) const;

}; // end of Class DefaultGeometry


/*!
   \class SurfPlane
   class that defines the shape of a plane surface on the boundary
   or inside (interface) of a 3D domain.
*/
class SurfPlane : public DefaultGeometry
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   SurfPlane(){ description_="plane"; }    //!< main constructor

}; // end of Class SurfPlane

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* DEFAULT_GEOMETRY_HPP */
