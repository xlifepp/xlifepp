/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshQuaDisk.hpp
  \author Y. Lafranche
  \since 28 Apr 2014
  \date 28 Apr 2014

  \brief Definition of the xlifepp::subdivision::SurfMeshQuaDisk class

  This class creates a surfacic mesh of a disk or a portion of a disk made of
  quadrangles and is built by successive subdivisions of an initial elementary mesh.
*/

#ifndef SURF_MESH_QUA_DISK_HPP
#define SURF_MESH_QUA_DISK_HPP

#include "QuadrangleMesh.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class SurfMeshQuaDisk
*/
class SurfMeshQuaDisk : public QuadrangleMesh
{
public:
//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

   //! main constructor
   SurfMeshQuaDisk(const number_t nbsubdiv=0, const number_t order=1, const number_t type=1,
                   const real_t radius=1., const Point Center=Point(0.,0.),
                   real_t angmin=0., real_t angmax=360.,
                   const number_t minVertexNum=1, const number_t minElementNum=1);
private:
//-------------------------------------------------------------------------------
// Private member functions
//-------------------------------------------------------------------------------

   //! create initial mesh
   void initMesh(const real_t radius, const Point& Center,
                 const real_t angmin, const real_t angmax,
                 number_t& VertexNum, number_t& ElementNum);

}; // end of Class SurfMeshQuaDisk

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* SURF_MESH_QUA_DISK_HPP */
