/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file DefaultGeometry.cpp
  \author Y. Lafranche
  \since 10 Nov 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::DefaultGeometry class members and related functions
*/

#include "DefaultGeometry.hpp"

#include <vector>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
/*!
 Projection onto the plane of the barycenter of the points in VP with coefficients
 in coef. The points in VP are assumed to lie on the plane (this is not checked).
 Thus, the new point is simply this barycenter.
 */
Point DefaultGeometry::projOnBound(const real_t *coef, const vector<Point>& VP) const {
   return barycenter(coef,VP);
}

} // end of namespace subdivision
} // end of namespace xlifepp
