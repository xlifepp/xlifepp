/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Triangle.cpp
  \author Y. Lafranche
  \since 05 Apr 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::Triangle class members and related functions
*/

#include "Triangle.hpp"

using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Static variables
//-------------------------------------------------------------------------------
// Index array defining the edges according to the following numering convention:
// edge #1=(1,2), edge #2=(2,3), edge #3=(1,3)
short Triangle::rkEdge[/* nb_edges_ */][2] = {{0,1}, {1,2}, {0,2}};

// Index array defining the face according to the following numering convention:
// face #1=(1,2,3)
short Triangle::rkFace[/* nb_faces_ */][3] = {{0,1,2}};

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Conversion constructor. Constructor by vertices, given by their rank in the list
 */
Triangle::Triangle(const number_t num,
                   const number_t rV1, const number_t rV2, const number_t rV3,
                   const number_t bdSideNum)
:Simplex<Triangle>(num,bdSideNum){
   vertices_[0] = rV1;
   vertices_[1] = rV2;
   vertices_[2] = rV3;
}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
/*!
 returns the numbers (>= 1) of the edges defining the face, i.e. the triangle itself
 */
vector<short> Triangle::numEdgesOfFace(const number_t i) {
   vector<short> V(3);
   for (short j=0; j<3; j++) { V[j] = j+1; }
   return V;
}
/*!
 returns the ranks (>= 0) of the vertices defining the edges of the triangle
 */
vector<pair<short,short> > Triangle::rkEdgeVertices() {
   vector<pair<short,short> > V(nb_edges_);
   for (number_t i=0; i<nb_edges_; i++) { V[i] = make_pair(rkEdge[i][0],rkEdge[i][1]); }
   return V;
}

/*!
 This function gives the local position of the vertices of any triangle of
 the mesh correspondig to the numbering convention explained on top of the
 Triangle class header file (Triangle.hpp).

 The entry BC[i] in the returned vector BC contains a vector whose components
 BC[i][j] are equal to k*lambda_j, where k is the order of the mesh and lambda_j
 is the jth barycentric coordinate of the vertex number i+1 with respect to the
 3 main vertices of the triangle(*). Thus, the returned vector gives the
 location of the vertices according to the way they are encountered in the
 numbering process implemented by the functions GeomFigureMesh<T_>::createHOV,
 GeomFigureMesh<T_>::createHOeV and TriangleMesh::createHOiV.

 (*) In fact, the last value is not stored since it is equal to k - sum of the
     previous values.
 */
vector< vector<number_t> > Triangle::numberingOfVertices(const number_t Order) {
   vector< vector<number_t> > BC;
   const number_t nbVert = nb_main_vertices_; // = 3
   vector<number_t> V(nbVert,0);
   number_t order = Order;

   if (order < 1) {order = 1;}

//    main vertices (order 1)
   for (number_t indV=0; indV<nbVert; indV++) {
      V[indV] = order;
      BC.push_back(vector<number_t>(V.begin(),V.end()-1));
      V[indV] = 0;
   }
//    edge vertices (cf. GeomFigureMesh<T_>::createHOeV)
   for (number_t indEdge=0; indEdge<nb_edges_; indEdge++) {
      V = vector<number_t> (nbVert,0);
      for (number_t i1=1; i1<order; i1++) {
         V[rkEdge[indEdge][0]] = i1;
         V[rkEdge[indEdge][1]] = order-i1;
         BC.push_back(vector<number_t>(V.begin(),V.end()-1));
      }
   }
//    internal vertices (cf. TriangleMesh::createHOiV)
   for (number_t indFace=0; indFace<nb_faces_; indFace++) {
      V = vector<number_t> (nbVert,0);
      for (number_t i1=1; i1<order; i1++) {
         V[rkFace[indFace][0]] = i1;
         for (number_t i2=1; i2<order-i1; i2++) {
            V[rkFace[indFace][1]] = i2;
            V[rkFace[indFace][2]] = order-i1-i2;
            BC.push_back(vector<number_t>(V.begin(),V.end()-1));
            }
         }

   }
   return BC;
}

} // end of namespace subdivision
} // end of namespace xlifepp
