/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TeXPolygon.hpp
  \author Y. Lafranche
  \since 17 May 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::TeXPolygon class

  This class is an utilitary class that holds the description of a polygon,
  typically a triangle face or an edge of a simplex mesh. These informations are
  used to sort such polygons for graphical purpose using Fig4TeX in the function
  subdivision::GeomFigureMesh::printTeXSortedAreaFoE.
*/

#ifndef TEX_POLYGON_HPP
#define TEX_POLYGON_HPP

#include "Vertex.hpp"
#include "types.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class TeXPolygon
*/
class TeXPolygon
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   TeXPolygon(const std::vector<number_t>& vrV, const number_t numBoundary,
              const std::vector<Vertex>& listV);

   //! constructor for edges
   TeXPolygon(const pair_nn& prV, const number_t numBoundary,
              const std::vector<Vertex>& listV);

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! returns the integer attribute
   number_t attrib() const { return numBoundary_; }

   //! returns a reference to this vector member
   const std::vector<number_t>& Vrank() const { return vrV_; }

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! initialize the observation direction (static vector OD)
   static void initObsDir(const Vect& V) { OD = V; }

   /*!
     tells whether the exterior of the face is visible
     with respect to the observation direction
    */
   bool isExtVisible() const;

   //! comparison function of two faces
   bool operator<(const TeXPolygon& f) const;

//-------------------------------------------------------------------------------
//  Public data members
//-------------------------------------------------------------------------------
private:
   //! observation direction (should be initialized before using operator<)
   static Vect OD;

   //! indices defining the vertices of the face (ranks in the general vertex list)
   std::vector<number_t> vrV_;

   //! attribute associated to the face (e.g. patch number)
   number_t numBoundary_;

   //! geometric points corresponding to the vertices
   std::vector<Point> geomPts_;

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
   //! default constructor
   TeXPolygon();


}; // end of Class TeXPolygon

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* TEX_POLYGON_HPP */
