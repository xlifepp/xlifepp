/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshTriDisk.hpp
  \author Y. Lafranche
  \since 28 Apr 2014
  \date 28 Apr 2014

  \brief Definition of the xlifepp::subdivision::SurfMeshTriDisk class

  This class allows the construction of a surfacic mesh of a disk or a portion of a disk made
  of triangles. It is built by successive subdivisions of an initial elementary mesh consisting
  of triangles given by the user.
*/

#ifndef SURF_MESH_TRI_DISK_HPP
#define SURF_MESH_TRI_DISK_HPP

#include "TriangleMesh.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class SurfMeshTriDisk
*/
class SurfMeshTriDisk : public TriangleMesh
{
public:
//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

   //! main constructor
   SurfMeshTriDisk(const number_t nbsubdiv=0, const number_t order=1, const number_t type=1,
                   const real_t radius=1., const Point Center=Point(0.,0.),
                   real_t angmin=0., real_t angmax=360.,
                   const number_t minVertexNum=1, const number_t minElementNum=1);
private:
//-------------------------------------------------------------------------------
// Private member functions
//-------------------------------------------------------------------------------

   //! create initial mesh
   void initMesh(const real_t radius, const Point& center,
                 const real_t angmin, const real_t angmax,
                 number_t& VertexNum, number_t& ElementNum);

}; // end of Class SurfMeshTriDisk

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* SURF_MESH_TRI_DISK_HPP */
