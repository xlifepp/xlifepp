/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Quadrangle.hpp
  \author Y. Lafranche
  \since 3 Fev 2014
  \date 3 Fev 2014

  \brief Definition of the xlifepp::subdivision::Quadrangle class

 Class Quadrangle defines a quadrangle as a list of vertices.
 This class also holds the so-called "high order vertices" involved in the
 Lagrange quadrangle of order k, k>1. The 4 "main" vertices correspond to k=1.
 High order vertices allow a better approximation of the geometry.

 Terminology: Strictly speaking, a quadrangle has 4 vertices. But to make short,
              the additionnal points involved in the Lagrange quadrangle of order
              greater than 1 are also called "vertices" although they are not.

 Local numbering convention
 --------------------------
 Assuming the current quadrangle is defined by the sequence of vertices (1,2,3,4),
 the edges are numbered according to the following convention:
 \verbatim
           4 +--------+ 3      Edges: Number Vertices
             |        |                 1      1,2
             |        |                 2      2,3
             |        |                 3      4,3
           1 +--------+ 2               4      1,4
 \endverbatim
 The face is oriented so that the cross-product 12 x 13 defines a normal vector
 to the face supposed to be oriented towards the exterior of the surface (positive orientation).
 The edge (i,j) is oriented from i towards j.

 In the implementation, the 4 main vertices (of order 1) are stored at the
 beginning of the internal vector vertices_ ; high order vertices, if any,
 are stored in the following positions according the numbering convention.
 The relation between the position of each vertex and its local number is given
 by the function numberingOfVertices.
*/

#ifndef QUADRANGLE_HPP
#define QUADRANGLE_HPP

#include "CartesianFig.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class Quadrangle
*/
class Quadrangle : public CartesianFig<Quadrangle>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! constructor by vertices, given by their rank in the list
   Quadrangle(const number_t num,
              const number_t rV1, const number_t rV2, const number_t rV3, const number_t rV4,
              const number_t bdSideNum=0);

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! get back the rank of a vertex of an edge
   virtual short getrkEdge(short indEdge, short indVert) const { return rkEdge[indEdge][indVert]; }

   //! get back the rank of a vertex of a face
   virtual short getrkFace(short indFace, short indVert) const { return rkFace[indFace][indVert]; }

   //! returns the numbers (>= 1) of the edges defining the face, i.e. the quadrangle itself
   static std::vector<short> numEdgesOfFace(const number_t i);

   //! returns the ranks (>= 0) of the vertices defining the edges of the quadrangle
   static std::vector<std::pair<short,short> > rkEdgeVertices();
//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! position of the vertices in relation with the numbering convention
   static std::vector< std::vector<number_t> > numberingOfVertices(const number_t Order) ;

private:
   static short rkEdge[/* nb_edges_ */][2];   //!< Index array defining the edges
   static short rkFace[/* nb_faces_ */][4];   //!< Index array defining the faces

}; // end of Class Quadrangle

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* QUADRANGLE_HPP */
