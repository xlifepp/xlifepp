/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SubdivisionMesh.hpp
  \author Y. Lafranche
  \since 22 May 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::SubdivisionMesh class

  This class is an abstract base class used as a frame for different
  kind of meshes consisting of the same geometric figures, such as triangles and
  tetrahedrons. These meshes are built by a subdivision process, starting from
  an initial configuration of elements.

  inheriting classes
  ------------------
    - xlifepp::subdivision::GeomFigureMesh
*/

#ifndef SUBDIVISION_MESH_HPP
#define SUBDIVISION_MESH_HPP

#include "Vertex.hpp"
#include "types.hpp"
#include "GeomFigure.hpp"

#include <iostream>
#include <map>
#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class SubdivisionMesh
*/
class SubdivisionMesh
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   SubdivisionMesh(const number_t nbsubdiv, const number_t order, const number_t type,
                   const number_t minVertexNum, const number_t minElementNum);

   //! destructor
   virtual ~SubdivisionMesh();
//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! returns the description of boundary num, num >=1
   std::string boundaryDescription(const number_t num) const { return TG_.descriptionOf(boundaryArea,num); }

   //! returns the description of interface num, num >=1
   std::string interfaceDescription(const number_t num) const { return TG_.descriptionOf(interfaceArea,num); }

   //! returns the description of subdomain num, num >=1
   std::string subdomainDescription(const number_t num) const { return TG_.descriptionOf(subdomainArea,num); }


   //! returns the name of boundary num, num >=1
   std::string boundaryName(const number_t num) const { return TG_.nameOf(boundaryArea,num); }

   //! returns the name of interface num, num >=1
   std::string interfaceName(const number_t num) const { return TG_.nameOf(interfaceArea,num); }

   //! returns the name of subdomain num, num >=1
   std::string subdomainName(const number_t num) const { return TG_.nameOf(subdomainArea,num); }

   /*!
     returns the list of all the vertices of element num,
     each given by its number, with num such that
     minElementNum_ <= num <= numberOfElements+minElementNum_-1
    */
   virtual std::vector<number_t> element(const number_t num) const = 0;

   /*!
     returns a normal vector to the face number i >= 1 of element num,
     with num such that minElementNum_ <= num <= numberOfElements+minElementNum_-1
     The vector is not normalized and is oriented towards the exterior of the element.
    */
   virtual std::vector<real_t> faceExtNormalVec(const number_t num, const number_t i) const = 0;

   /*!
     returns the orientation of the face number i >= 1 of element num, with num such that
     minElementNum_ <= num <= numberOfElements+minElementNum_-1. The function:
      - returns 1 if the first three vertices (i,j,k) of the face define a vector ij x ik
        oriented towards the exterior of the element,
      - returns -1 otherwise.
    */
   virtual int faceOrientation(const number_t num, const number_t i) const = 0;

   //! returns the number of boundaries of the mesh
   number_t numberOfBoundaries() const { return TG_.numberOf(boundaryArea); }

   //! returns the number of interfaces of the mesh
   number_t numberOfInterfaces() const { return TG_.numberOf(interfaceArea); }

   //! returns the number of subdomains of the mesh
   number_t numberOfSubdomains() const { return TG_.numberOf(subdomainArea); }

   //! number of boundary patches
   number_t numberOfBndryPatches() const {return TG_.numberOfBndryPatches(); }
   //! number of interface patches
   number_t numberOfIntrfPatches() const {return TG_.numberOfIntrfPatches(); }
   //! number of subdomain patches
   number_t numberOfSbDomPatches() const {return TG_.numberOfSbDomPatches(); }

   //! returns the number of elements in the mesh
   virtual number_t numberOfElements() const = 0;

   //! returns the total number of vertices in the mesh
   number_t numberOfVertices() const { return listV_.size(); }

   /*!
     returns the number of vertices of order 1 in the mesh
     (equals numberOfVertices() if order=1)
    */
   number_t numberOfMainVertices() const { return nb_main_vertices_; }

   //! returns the number of vertices of order 1 per element
   number_t numberOfMainVerticesByElement() const;

   /*!
     returns the smallest number associated to an element
     among all the elements in the mesh
    */
   number_t minElementNumber() const { return minElementNum_; }

   /*!
     returns the smallest number associated to a vertex among
     all the vertices in the mesh
    */
   number_t minVertexNumber() const { return minVertexNum_; }

   //! returns the total number of vertices of each element
   virtual number_t numberOfVerticesByElement() const = 0;

   //! returns the title of the mesh
   std::string title() const { return title_; }

   //! returns the order of the mesh
   number_t order() const { return order_; }

   //! returns the coordinates of a vertex, given its number num, num >= minVertexNum_
   std::vector<real_t> vertexCoord(const number_t num) const ;

   //! returns the coordinates of a vertex, given its rank rk>=0 in the global list of vertices
   std::vector<real_t> rkvertexCoord(const number_t rk) const ;

   std::vector<Point> unifMesh(const std::vector< std::vector<number_t> >& genmesh,
                               const std::vector<std::pair<short,short> >& rkEV,
                               const std::vector<number_t>& rkUnk, const std::vector<number_t>& rkData);

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   //! prints, on stream os, the general informations about the mesh
   void printInfo(std::ostream& os, const bool forTeX=false) const ;
   void printInfo(PrintStream& os, const bool forTeX=false) const {printInfo(os.currentStream());}

   //! prints, on stream os, the mesh with vertices coordinates
   virtual void printall(std::ostream& os) const = 0;
   virtual void printall(PrintStream& os) const = 0;


   //! prints, on stream os, the mesh with vertices numbers
   virtual void print(std::ostream& os) const = 0;
   virtual void print(PrintStream& os) const = 0;

   /*!
     prints, on stream ftex, fig4tex instructions to draw a mesh with
     observation direction defined by longitude psi and latitude theta
    */
   virtual void printTeX(std::ostream& ftex, const float psi=-30, const float theta=25,
                         const number_t nbviews=1, const std::string& DimProj="4cm,orthogonal",
                         const bool withInterface=true, const bool withElems=false) const = 0;
   virtual void printTeX(PrintStream& ftex, const float psi=-30, const float theta=25,
                         const number_t nbviews=1, const std::string& DimProj="4cm,orthogonal",
                         const bool withInterface=true, const bool withElems=false) const = 0;

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   /*!
     initializes the user attribute of the geometrical areas of the mesh,
     boundaries, interfaces and subdomains, given in this order
    */
   void initUserAttribute(const std::vector<std::string>& attribute);

   //! returns the number of the internal vertices of any order in the mesh
   number_t numberOfVerticesInside() const ;

   /*!
     returns the number of vertices of any order in the mesh, belonging to
     an area of kind TA or belonging to any area of kind TA if num=0
    */
   number_t numberOfVerticesIn(const topologicalArea TA, const number_t num = 0) const ;

   //! redefines boundaries
   bool setBoundaries(const std::vector< std::vector<number_t> >& group) {
                                         return TG_.setBoundaries(group); }
   //! redefines subdomains
   bool setSubdomains(const std::vector< std::vector<number_t> >& group) {
                                         return TG_.setSubdomains(group); }
   /*!
     returns the list of the internal vertices of any order in the mesh
     (given by their number)
    */
   std::vector<number_t> verticesInside() const ;

   /*!
     returns the list of vertices of any order in the mesh, belonging to an area
     of kind TA or belonging to any area of kind TA if num=0 (given by their number)
    */
   std::vector<number_t> verticesIn(const topologicalArea TA, const number_t num = 0) const ;

   //! returns the list of vertices of order 1 in the mesh (given by their number)
   std::vector<number_t> verticesOfOrder1() const ;

protected:
   //! short description of the mesh
   std::string title_;
   //! subdivision level (0 = no subdivision)
   number_t subdiv_level_;
   //! order of the tetrahedra in the final mesh (order > 0)
   number_t order_;
   //! type of the subdivision (flat or taking into account an underlying surface)
   number_t type_;
   //! number of main vertices in the final mesh, i.e. number of vertices of order 1
   number_t nb_main_vertices_;
   //! smallest vertex number
   number_t minVertexNum_;
   //! smallest element number
   number_t minElementNum_;
   //! definition of the topological and geometrical informations
   TopoGeom TG_;
   /*!
     list of all the vertices in the final mesh
     the number assigned to the vertex listV_[k] is equal to k+1
    */
   std::vector<Vertex> listV_;
   //! pointer to function that computes a point associated to a new vertex
   PtCompFun newVertexPt_;
   //! Exception handling
//   static HOV_error PB;

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! detects or creates a new vertex starting from a list of vertices and returns vertex rank
   number_t createVertex(number_t& VertexNum, const std::vector<number_t>& rkVert);

   //! detects or creates a new vertex starting from the two endpoints of an edge and returns vertex rank
   number_t createVertex(number_t& VertexNum, const pair_nn Edge, map_pair_num& SeenEdges);

   //! detects or creates a new vertex starting from the vertices of a face and returns vertex rank
   number_t createVertex(number_t& VertexNum, const std::vector<number_t>& rkVFace, map_set_num& SeenFaces);


   //--- Utilitary functions

   /*!
     returns reference number (localization code) of an area
     or of all the areas of kind TA if num=0
    */
   refnum_t lCodeOf(const topologicalArea TA, const number_t num) const ;

   //! get color associated to the area num, num>=1, of kind TA
   std::string colorOf(const topologicalArea TA, const number_t num) const ;

   /*!
     initializes the user attribute of the geometrical areas of the mesh,
     boundaries, interfaces and subdomains, given in this order, by a color.
    */
   void initDefaultUserAttribute(void);

   //! rotate and translate points
   void rotNtrans(const std::vector<std::pair<real_t, dimen_t> >& rots, const Vect& U, std::vector<Point>& Pt);

   //! create the boundary shapes to be used at both ends of a truncated cone
   std::vector<PatchGeometry *> endBoundaryShapes(const std::vector<ShapeInfo>& vSI,
                                                  const SurfCone& SC);

   //! Defines a set of points used to build the initial mesh in a cube.
   std::vector<Point> cubePoints(const real_t edLen, const std::vector<std::pair<real_t, dimen_t> >& rots, const Point& Center);

  //!  Start mesh building process for revolution objects.

   void initRevMesh(const number_t nbslices, const real_t radius1, const real_t radius2,
                    const std::vector<Point>& CharacPts, const std::vector<ShapeInfo>& vSI,
                    real_t& R1, real_t& R2, Point& P1, Point& P2, std::vector<ShapeInfo>& cvSI,
                    std::string& bottomPt, std::string& topPt, bool& iscone, std::string& shape,
                    DefaultGeometry *& DG, SurfPlane *& SP, SurfCone *& SC,
                    std::vector <PatchGeometry *>& EBS, int& nb_sl, real_t& slice_height);

   //--- Fig4TeX printing functions

   /*!
     prints, on stream ftex, fig4tex instructions to define vertices (action = 0)
     or to display vertices (action = 1) of any order in a mesh.
     The vertices belong to an area of kind TA, or to any area of kind TA if num=0.
    */
   void printTeXInArea(std::ostream& ftex, const number_t action,
                       const topologicalArea TA = boundaryArea, const number_t num = 0) const ;

   //! prints, on stream ftex, fig4tex instructions to define vertices of V
   void printTeXfigpt(std::ostream& ftex, const std::vector<number_t>& V) const ;

   //! prints, on stream ftex, fig4tex instructions to display vertices of V
   void printTeXfigwrite(std::ostream& ftex, const std::vector<number_t>& V) const ;

   /*!
     prints, on stream ftex, Fig4TeX instructions to draw faces of a mesh
     belonging to an area ok kind TA
    */
   void printTeXFacesInArea(std::ostream& ftex, const topologicalArea TA, const number_t num) const ;

   //--- Low level utilitary functions

   //! replace the rank in the internal list by the vertex number
   void rankToNum(std::vector<number_t>& V) const ;

   //! replace the rank in the internal list by the vertex number
   void rankToNum(pair_nn& V) const ;

   /*!
     returns the list of the internal vertices of any order in the mesh
     (given by their rank)
    */
   std::vector<number_t> rk_verticesInside() const ;

   /*!
     returns the list of vertices of any order in the mesh, belonging to an area of kind TA
     or belonging to any area of kind TA if num=0 (given by their rank)
    */
   std::vector<number_t> rk_verticesIn(const topologicalArea TA, const number_t num = 0) const ;

   /*!
     returns the list of faces of the mesh belonging to an area of kind TA
     or belonging to any area of kind TA if num=0 (given by their rank)
    */
   virtual std::vector< std::vector<number_t> > rk_facesIn(const topologicalArea TA, const number_t num = 0) const = 0;

   //--- For high order vertices generation

   /*!
     comparison of two vectors (used in createHOfV)
     (this function could be external and defined before its use in the .cpp file)
    */
   static bool cmpvect(const std::vector<number_t>& U, const std::vector<number_t>& V);

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
private:
   //! default function that computes the geometrical point associated to a new vertex
   Point newVertexPtDef(const refnum_t localcod, const real_t *coef, const std::vector<Point>& VP) const;
   //! general function that computes the geometrical point associated to a new vertex
   Point newVertexPtGen(const refnum_t localcod, const real_t *coef, const std::vector<Point>& VP) const;

   //! initialization of attributes of areas of kind TA
   void initAttribute_(const topologicalArea TA, const std::vector<std::string>& attribute, size_t& k);

}; // end of Class SubdivisionMesh

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* SUBDIVISION_MESH_HPP */
