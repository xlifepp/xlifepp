/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshQuaDisk.cpp
  \author Y. Lafranche
  \since 28 Apr 2014
  \date 28 Apr 2014

  \brief Implementation of xlifepp::subdivision::SurfMeshQuaDisk class members and related functions
*/

#include "SurfMeshQuaDisk.hpp"
#include <sstream>

namespace xlifepp {
namespace subdivision {
using std::vector;

//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

/*!
 Build a mesh of quadrangles by successive subdivisions from an initial mesh of quadrangles.
 Works in 2D and 3D.

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each quadrangle is subdivided into 4 quadrangles with the
   same orientation as the original one: if the first quadrangle is (1,2,3,4), the
   vector 12x13 defines the exterior normal to the face.

 \param order: order of the quadrangles in the final mesh (1 by default)
   The default value is 1, which leads to a Q1 mesh, in which case each
   quadrangle is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh.

 \param type: type of the subdivision (1 by default)
   . if type = 0, the algorithm leads to a simple (flat) subdivision of the initial
   mesh where new vertices are all the midpoints of the edges.
   . if type = 1, the algorithm leads to a subdivision where the vertices belonging to
   a prescribed boundary are computed so that they lie on the circle ; they are computed
   as the radial projections of the vertices of the Lagrange mesh of order k over the
   chord segment.

 \param radius: radius of the circle (1. by default)
 \param Center: center of the circle ((0,0) by default)
 \param angmin: angles defining the sector to be meshed ;
 \param angmax: they are measured counterclockwise and given in degrees.

 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
SurfMeshQuaDisk::SurfMeshQuaDisk(const number_t nbsubdiv, const number_t order, const number_t type,
                                 const real_t radius, const Point Center,
                                 const real_t angmin, const real_t angmax,
                                 const number_t minVertexNum, const number_t minElementNum)
: QuadrangleMesh(nbsubdiv, order, type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   initMesh(radius,Center,angmin,angmax,VertexNum,ElementNum);
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
// Private member functions
//-------------------------------------------------------------------------------

/*!
  Create the initial mesh (or "seed" of the mesh) to be subdivided.
 */
void SurfMeshQuaDisk::initMesh(const real_t radius, const Point& Center,
                               real_t angmin, real_t angmax,
                               number_t& VertexNum, number_t& ElementNum){

   SurfSphere *SS = new SurfSphere(Center,radius);
   DefaultGeometry *DG = new DefaultGeometry();

   title_ = "Quadrangle mesh of a portion of disk";
   real_t dang = angmax-angmin;
   if (dang < 0.) {
      real_t t = angmin;
      angmin = angmax;
      angmax = t;
      dang = -dang;
   }
   if (dang > 360.) { dang = 360.; angmin = 0.; angmax = 360.; }
   number_t nbBound = 3;
   if (std::abs(dang-360.) < theTolerance) { nbBound = 1; } // closed circle
   {// Definition of nbBound boundaries and 1 subdomain
        vector<number_t> B_patch(nbBound), B_dimen(nbBound);
        vector<number_t> I_patch, I_dimen;// both should be empty here
        vector<number_t> D_patch(1, nbBound+1), D_dimen(1,2);//D_patch{nbBound+1}, D_dimen{2}
        vector<PatchGeometry *> P_geom(nbBound+1); // patch # 1,...,nbBound, nbBound+1
        for (size_t i=0; i<nbBound; i++) { B_patch[i] = i+1; B_dimen[i] = 1; P_geom[i] = DG;}
        if (type_ != 0) { P_geom[nbBound-1] = SS; }// fit the boundary circle
        P_geom[nbBound] = DG;// the last patch is the main domain
        TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
   }
//       Description of the boundary patches (# 1,...,nbBound)
   int iPh = 0;
   for (size_t i=0; i<nbBound; i++) {
      std::stringstream ss; ss << ++iPh;
      TG_.setDescription(iPh) = "Boundary: " + ss.str();
   }
   if (nbBound != 1) {
      TG_.setDescription(1) += ", first side of the sector, counterclockwise";
      TG_.setDescription(2) += ", second side of the sector, counterclockwise";
   }
   TG_.setDescription(iPh) += ", boundary of the sector"; number_t sCIR = TG_.sigma(iPh);
//       Description of the subdomain patch (# nbBound+1)
   TG_.setDescription(++iPh) = "main domain"; number_t sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries
//       and subdomain they belong to.
   number_t VN = minVertexNum_, EN = minElementNum_;
   if (nbBound == 1) { // closed circle: initial mesh made of 5 quadrangles
/*!
 \verbatim
                  5              ^ Y
                  |              |
                  1              |
           6 -- 2   0 -- 4       +-----> X
                  3
                  |
                  7
 \endverbatim
*/
// Define 4 "reference" points on the circle and 4 points on the inner-circle of radius R.
      real_t R(radius/3.);
      vector<Point> Pt;
      Pt.push_back(Center + Point(R,0.));  // Pt[0]
      Pt.push_back(Center + Point(0.,R));  // Pt[1]
      Pt.push_back(Center + Point(-R,0.)); // Pt[2]
      Pt.push_back(Center + Point(0.,-R)); // Pt[3]
      Pt.push_back(Center + Point(radius,0.));  // Pt[4]
      Pt.push_back(Center + Point(0.,radius));  // Pt[5]
      Pt.push_back(Center + Point(-radius,0.)); // Pt[6]
      Pt.push_back(Center + Point(0.,-radius)); // Pt[7]
      for (size_t np=0; np<4; np++) {// vertices inside the domain
         listV_.push_back(Vertex(VN++,     sDOM,Pt[np])); // rank 0 to 3
      }
      for (size_t np=4; np<Pt.size(); np++) {// vertices on the boundary
         listV_.push_back(Vertex(VN++,sCIR|sDOM,Pt[np])); // rank 4 to 7
      }
// Define the 5 initial quadrangles
      // 1. internal quadrangle: (a,b,c,d) = (0,1,2,3)
      number_t rVa = 0, rVb = rVa+1, rVc = rVb+1, rVd = rVc+1;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd)); // ranks are needed here
      // 2. external quadrangles: (a,b,c,d) = rotations of (0,4,5,1)
      //    edge 2 is (b,c) and is on the curved boundary.
     rVd = rVb; rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
     rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
     rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
     rVa++; rVd=0;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
   }
   else { // arc of circle
      // the center is on the first two boundaries
      listV_.push_back(Vertex(VN++,TG_.sigma(1)|TG_.sigma(2)|sDOM,Center)); // rank 0

      real_t R(radius/2.);
      real_t alpha = angmin*pi_/180., csa=std::cos(alpha), sna=std::sin(alpha);
      // first two vertices: the first vertex is on the first boundary only
      Point Pt1(Center + Point(R*csa, R*sna)), Pt2(Center + Point(radius*csa, radius*sna));
      listV_.push_back(Vertex(VN++,TG_.sigma(1)     |sDOM,Pt1)); // rank rV1
      listV_.push_back(Vertex(VN++,TG_.sigma(1)|sCIR|sDOM,Pt2)); // rank rV2
//       Definition of the remaining vertices and the 3*nbPart associated quadrangles.
      number_t nbPart = 1 + (dang-1.) / 120.;
      real_t dt = (dang/nbPart)*pi_/360.;
      number_t rV1=1, rV2=2;
      for (number_t k=0; k<nbPart-1; k++, rV1+=2, rV2+=2) {
         for (number_t i=0; i<2; i++) {
            alpha += dt;
            csa=std::cos(alpha), sna=std::sin(alpha);
            Pt1=Point(Center + Point(R*csa, R*sna)), Pt2=Point(Center + Point(radius*csa, radius*sna));
            listV_.push_back(Vertex(VN++,     sDOM,Pt1));
            listV_.push_back(Vertex(VN++,sCIR|sDOM,Pt2));
         }
         // 1. internal quadrangle
         listT_.push_back(Quadrangle(EN++, 0, rV1, rV1+2, rV1+4)); // ranks are needed here
         // 2. external quadrangles ; edge 2 is (rV2, rV2+2) and lies on the curved boundary
         listT_.push_back(Quadrangle(EN++, rV1, rV2, rV2+2, rV1+2,2));
         rV1+=2, rV2+=2;
         listT_.push_back(Quadrangle(EN++, rV1, rV2, rV2+2, rV1+2,2));
      }
//       Definition of the last quadrangles, and the last vertices
      alpha += dt;
      csa=std::cos(alpha), sna=std::sin(alpha);
      Pt1=Point(Center + Point(R*csa, R*sna)), Pt2=Point(Center + Point(radius*csa, radius*sna));
      listV_.push_back(Vertex(VN++,     sDOM,Pt1));
      listV_.push_back(Vertex(VN++,sCIR|sDOM,Pt2));
      alpha += dt;
      csa=std::cos(alpha), sna=std::sin(alpha);
      Pt1=Point(Center + Point(R*csa, R*sna)), Pt2=Point(Center + Point(radius*csa, radius*sna));
      listV_.push_back(Vertex(VN++,TG_.sigma(2)     |sDOM,Pt1));
      listV_.push_back(Vertex(VN++,TG_.sigma(2)|sCIR|sDOM,Pt2));

      // 1. internal quadrangle
      listT_.push_back(Quadrangle(EN++, 0, rV1, rV1+2, rV1+4)); // ranks are needed here
      // 2. external quadrangles ; edge 2 is (rV2, rV2+2) and lies on the curved boundary
      listT_.push_back(Quadrangle(EN++, rV1, rV2, rV2+2, rV1+2,2));
      rV1+=2, rV2+=2;
      listT_.push_back(Quadrangle(EN++, rV1, rV2, rV2+2, rV1+2,2));
   }
   VertexNum = VN - 1;
   ElementNum = EN - 1;
}

} // end of namespace subdivision
} // end of namespace xlifepp
