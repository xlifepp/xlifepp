/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshQuaSet.cpp
  \author Y. Lafranche
  \since 13 Mar 2014
  \date 13 Mar 2014

  \brief Implementation of xlifepp::subdivision::SurfMeshQuaSet class members and related functions
*/

#include "SurfMeshQuaSet.hpp"

#include <algorithm>
#include <sstream>

namespace xlifepp {
namespace subdivision {
using std::vector;

//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

/*!
 Build a mesh of quadrangles by successive subdivisions from an initial set of quadrangles.
 Works in 2D and 3D.

 \param pts: set of vertices of the initial mesh. This vector should contain
   at least three points. They are implicitly numbered starting from 1.

 \param tri: list of quadrangles of the initial mesh. Each element of the vector
   is a triplet of numbers, each of them being the number of a vertex in the vector pts
   according to the implicit numbering starting from 1.
   Some points in the vector pts may not be referenced ; thus, only a subset of them may
   be used to define the initial mesh.

 \param bound: list of the boundaries of the domain. A boundary is defined by the list of
   the vertex numbers lying on it, in the initial mesh ; no order is required.
   Several boundaries must be defined if, in the initial mesh tri, there are internal
   edges whose vertices are both boundary vertices. In this case, the two vertices must belong
   to two different boundaries, otherwise the (internal) points created by the subdivision
   process along this edge would be considered as boundary vertices.

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each quadrangle is subdivided into 4 quadrangles with the
   same orientation as the original one: if the first quadrangle is (1,2,3,4), the
   vectors 12x13 define the exterior normal to the face.

 \param order: order of the quadrangles in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   quadrangle is defined by its 3 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh.

 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
SurfMeshQuaSet::SurfMeshQuaSet(const vector<Point>& pts, const vector<vector<number_t> >& tri,
                               const vector<vector<number_t> >& bound,
                               const number_t nbsubdiv, const number_t order,
                               const number_t minElementNum)
: QuadrangleMesh(nbsubdiv, order, 0, 1, minElementNum)
  {
    if (pts.size() < Quadrangle::numberOfMainVertices())
    {
      where("SurfMeshQuaSet::SurfMeshQuaSet");
      error("not_enough_values", pts.size(), Quadrangle::numberOfMainVertices());
    }
    if (tri.size() < 1)
    {
      where("SurfMeshQuaSet::SurfMeshQuaSet");
      error("not_enough_values", tri.size(), 1);
    }

//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   initMesh(pts,tri,bound,VertexNum,ElementNum);
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
// Private member functions
//-------------------------------------------------------------------------------

/*!
  Create the initial mesh (or "seed" of the mesh) to be subdivided.
 */
void SurfMeshQuaSet::initMesh(const vector<Point>& pts, const vector<vector<number_t> >& tri,
                              const vector<vector<number_t> >& bound,
                              number_t& VertexNum, number_t& ElementNum){

// Find the maximum point number nmax used in the initial mesh. Thus, only the first
// nmax points of the pts vector will be used.
   number_t nmax = 0, nbVert = tri[0].size();
   for (vector<vector<number_t> >::const_iterator it=tri.begin(); it<tri.end(); it++) {
      for (size_t k=0; k<nbVert; k++) {
         number_t n = (*it)[k];
         if (n > nmax) { nmax = n; }
      }
   }
   if (nmax>pts.size())
   {
     where("SurfMeshQuaSet::initMesh");
     error("not_enough_values", pts.size(), nmax);
   }

   DefaultGeometry *DG = new DefaultGeometry();

   title_ = "Quadrangle mesh from an initial set";
   number_t nbBound(bound.size());
   {// Definition of nbBound boundaries and 1 subdomain
        vector<number_t> B_patch(nbBound), B_dimen(nbBound);
        vector<number_t> I_patch, I_dimen;// both should be empty here
        vector<number_t> D_patch(1, nbBound+1), D_dimen(1, 2);//D_patch{nbBound+1}, D_dimen{2}
        vector<PatchGeometry *> P_geom(nbBound+1); // patch # 1,...,nbBound, nbBound+1
        for (size_t i=0; i<nbBound; i++) { B_patch[i] = i+1; B_dimen[i] = 1; P_geom[i] = DG;}
        P_geom[nbBound] = DG;
        TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
   }
//       Description of the boundary patches
   int iPh = 0;
   for (size_t i=0; i<nbBound; i++) {
      std::stringstream ss; ss << ++iPh;
      TG_.setDescription(iPh) = "boundary " + ss.str();
   }
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "main domain";
//       Definition of the initial vertices, taking into account the boundaries
//       and subdomain they belong to.
   number_t VN = minVertexNum_;
   for (size_t k=0; k<nmax; k++) {
      refnum_t bdsig = getSig(k+1,bound); // k is the rank, k+1 is the point number
      listV_.push_back(Vertex(VN++,bdsig|TG_.sigma(nbBound+1),pts[k]));
   }
   VertexNum = minVertexNum_ - 1 + nmax;
   ElementNum = minElementNum_ - 1;
   for (vector<vector<number_t> >::const_iterator it=tri.begin(); it<tri.end(); it++) {
      listT_.push_back(Quadrangle(++ElementNum, (*it)[0]-1, (*it)[1]-1, (*it)[2]-1, (*it)[3]-1)); // ranks are needed here
   }
}
/*!
  Build the localisation code of point num from boundaries definition
 */
refnum_t SurfMeshQuaSet::getSig(number_t num, const vector<vector<number_t> >& bound){
   refnum_t sig(0);
   for (size_t k=0; k<bound.size(); k++) {// for each boundary
      if (find(bound[k].begin(), bound[k].end(), num) != bound[k].end()) {
         // point num lies on this boundary
         sig |= TG_.sigma(k+1);
      }
   }
   return sig;
}

} // end of namespace subdivision
} // end of namespace xlifepp
