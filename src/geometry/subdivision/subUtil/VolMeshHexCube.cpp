/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VolMeshHexCube.cpp
  \author Y. Lafranche
  \since 24 Feb 2014
  \date 24 Feb 2014

  \brief Implementation of xlifepp::subdivision::VolMeshHexCube class members and related functions
*/

#include "VolMeshHexCube.hpp"

using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of hexahedrons by successive subdivisions.

 \param rots: rotations to be applied to the cube to get its final position.
   Each rotation, if any, is defined by an angle in degrees and the number of the
   absolute axis (1, 2 or 3) around which the rotation is made.
   Each rotation is applied in turn to the cube starting from the canonical initial
   position where the cube is centered at the origin and its edges are parallel to the axes.

 \param nboctants: number of octants to be filled
   nboctants may take a value in [1, 8].
   For the big cube, the initial mesh may consist of 1 hexahedron instead of
   8 when this big cube is the juxtatposition of 8 small cubes, which reduces
   the number of hexahedrons of the final mesh by a factor 8.
   In order to activate this behaviour, nboctants must take the value -8.

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each hexahedron is subdivided into 8 hexahedrons with the
   same orientation as the original one.

 \param order: order of the hexahedra in the final mesh (1 by default)
   The default value is 1, which leads to a Q1 mesh, in which case each
   hexahedron is defined by its 8 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh.

 \param edgeLength: edge length of the cube (1. by default)
 \param Center: center of the cube ((0,0,0) by default)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
VolMeshHexCube::VolMeshHexCube(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                               const number_t nbsubdiv, const number_t order,
                               const real_t edgeLength, const Point Center,
                               const number_t minVertexNum, const number_t minElementNum)
:HexahedronMesh(nbsubdiv, order, 0, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   initMesh(rots,nboctants,edgeLength,Center,VertexNum,ElementNum);
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
/*!
  Create the initial mesh (or "seed" of the mesh) to be subdivided.
  In each octant, one cube is subdivided into 8 hexahedrons.
 */
void VolMeshHexCube::initMesh(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                              const real_t edLen, const Point& Center,
                              number_t& VertexNum, number_t& ElementNum){
// Define the 27 characteristic points of the cube near the origin Pt[4] (corners,
// middle of the edges and faces) in order to define the selected octants.
   vector<Point> Pt(cubePoints(edLen, rots, Center));

   // to help numbering: i = index i vector Pt, V[i] = rank in the global list of vertices
   //                0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27
   const int V[] = {-1,0,1,2,3,4,5,6,7,8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
   int iPh = 0, iPhI;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
   number_t mVN = minVertexNum_, nbBound, nbIntrf, nbSbDom, sITF, sDOM;
   switch (int(std::abs(float(nboctants)))) {
   default:
//      1 octant (1/8 cube)  = = = = = = = = = = = = = = = = = = = = = = = = =
   case 1:
      title_ = "Cube - Hexahedron mesh over 1 octant X>0, Y>0, Z>0";
       nbBound = 6; nbIntrf = 0; nbSbDom = 1;
      TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, containing the center point (vertex 4)";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)|sDOM,Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)|sDOM,Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(6)|sDOM,Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)|sDOM,Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)|sDOM,Pt[5]));
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sDOM,Pt[6]));
      listV_.push_back(Vertex(V[7]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)|sDOM,Pt[7]));
      listV_.push_back(Vertex(V[8]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)|sDOM,Pt[8]));
      VertexNum += 8;
      listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      break;
//      2 octants (1/4 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 2:
      title_ = "Cube - Hexahedron mesh over 2 octants Y>0, Z>0";
      nbBound = 6, nbIntrf = 1, nbSbDom = 1;
      TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X>0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X<0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, containing the center point (vertex 4)";
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 7
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 1]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)     |sDOM,Pt[1]));
      listV_.push_back(Vertex(V[ 2]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)     |sDOM,Pt[2]));
      listV_.push_back(Vertex(V[ 3]+mVN,TG_.sigma(2)|             TG_.sigma(6)|sITF|sDOM,Pt[3]));
      listV_.push_back(Vertex(V[ 4]+mVN,             TG_.sigma(5)|TG_.sigma(6)|sITF|sDOM,Pt[4]));
      listV_.push_back(Vertex(V[ 5]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)     |sDOM,Pt[5]));
      listV_.push_back(Vertex(V[ 6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[6]));
      listV_.push_back(Vertex(V[ 7]+mVN,TG_.sigma(2)|             TG_.sigma(3)|sITF|sDOM,Pt[7]));
      listV_.push_back(Vertex(V[ 8]+mVN,             TG_.sigma(5)|TG_.sigma(3)|sITF|sDOM,Pt[8]));
      listV_.push_back(Vertex(V[ 9]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)     |sDOM,Pt[9]));
      listV_.push_back(Vertex(V[10]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(6)     |sDOM,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)     |sDOM,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)     |sDOM,Pt[12]));
      VertexNum += 12;
      listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      listT_.push_back(Hexahedron(++ElementNum, V[9],V[12],V[10],V[11],V[4],V[8],V[3],V[7]));
      break;
//      3 octants (3/8 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 3:
      title_ = "Cube - Hexahedron mesh over 3 octants Z>0, minus the octant X>0, Y<0";
      nbBound = 8, nbIntrf = 2, nbSbDom = 1;
      TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X>0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y>0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X<0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y<0";
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 9
      TG_.setDescription(++iPh) = "Interface: XZ plane";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(V[ 1]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)     |sDOM,Pt[1]));
      listV_.push_back(Vertex(V[ 2]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)     |sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 3]+mVN,TG_.sigma(2)|             TG_.sigma(6)|sITF|sDOM,Pt[3]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 4]+mVN,TG_.sigma(7)|TG_.sigma(5)|TG_.sigma(6)|sITF|sDOM,Pt[4]));
      listV_.push_back(Vertex(V[ 5]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)     |sDOM,Pt[5]));
      listV_.push_back(Vertex(V[ 6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[6]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 7]+mVN,TG_.sigma(2)|             TG_.sigma(3)|sITF|sDOM,Pt[7]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 8]+mVN,TG_.sigma(7)|TG_.sigma(5)|TG_.sigma(3)|sITF|sDOM,Pt[8]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 9]+mVN,TG_.sigma(4)|             TG_.sigma(6)|sITF|sDOM,Pt[9]));
      listV_.push_back(Vertex(V[10]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(6)     |sDOM,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)     |sDOM,Pt[11]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[12]+mVN,TG_.sigma(4)|             TG_.sigma(3)|sITF|sDOM,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,TG_.sigma(4)|TG_.sigma(8)|TG_.sigma(3)     |sDOM,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,TG_.sigma(4)|TG_.sigma(8)|TG_.sigma(6)     |sDOM,Pt[14]));
      listV_.push_back(Vertex(V[15]+mVN,TG_.sigma(8)|TG_.sigma(7)|TG_.sigma(6)     |sDOM,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,TG_.sigma(8)|TG_.sigma(7)|TG_.sigma(3)     |sDOM,Pt[16]));
      VertexNum += 16;
      listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      listT_.push_back(Hexahedron(++ElementNum, V[9],V[12],V[10],V[11],V[4],V[8],V[3],V[7]));
      listT_.push_back(Hexahedron(++ElementNum, V[14],V[13],V[9],V[12],V[15],V[16],V[4],V[8]));
      break;
//      4 octants (1/2 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 4:
      title_ = "Cube - Hexahedron mesh over 4 octants Z>0";
      nbBound = 6, nbIntrf = 2; nbSbDom = 1;
      TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X>0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y>0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X<0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y<0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, containing the center point (vertex 4)";
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 7
      TG_.setDescription(++iPh) = "Interface: XZ plane";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 1]+mVN,             TG_.sigma(1)|TG_.sigma(6)|sITF|sDOM,Pt[1]));
      listV_.push_back(Vertex(V[ 2]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)     |sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 3]+mVN,TG_.sigma(2)|             TG_.sigma(6)|sITF|sDOM,Pt[3]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 4]+mVN,                          TG_.sigma(6)|sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 5]+mVN,             TG_.sigma(1)|TG_.sigma(3)|sITF|sDOM,Pt[5]));
      listV_.push_back(Vertex(V[ 6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[6]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 7]+mVN,TG_.sigma(2)|             TG_.sigma(3)|sITF|sDOM,Pt[7]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 8]+mVN,                          TG_.sigma(3)|sITF|sDOM,Pt[8]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 9]+mVN,TG_.sigma(4)|             TG_.sigma(6)|sITF|sDOM,Pt[9]));
      listV_.push_back(Vertex(V[10]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(6)     |sDOM,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)     |sDOM,Pt[11]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[12]+mVN,TG_.sigma(4)|             TG_.sigma(3)|sITF|sDOM,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)     |sDOM,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)     |sDOM,Pt[14]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[15]+mVN,TG_.sigma(5)|             TG_.sigma(6)|sITF|sDOM,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,TG_.sigma(5)|             TG_.sigma(3)|sITF|sDOM,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)     |sDOM,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)     |sDOM,Pt[18]));
      VertexNum += 18;
      listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      listT_.push_back(Hexahedron(++ElementNum, V[9],V[12],V[10],V[11],V[4],V[8],V[3],V[7]));
      listT_.push_back(Hexahedron(++ElementNum, V[14],V[13],V[9],V[12],V[15],V[16],V[4],V[8]));
      listT_.push_back(Hexahedron(++ElementNum, V[15],V[16],V[4],V[8],V[18],V[17],V[1],V[5]));
      break;
//      5 octants (5/8 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 5:
      title_ = "Cube - Hexahedron mesh over 5 octants, 4 with Z>0, plus the octant X>0, Y>0, Z<0";
      nbBound = 9, nbIntrf = 3, nbSbDom = 1;
      TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X>0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y>0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z>0";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X<0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y<0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z<0";
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 10
      TG_.setDescription(++iPh) = "Interface: XZ plane";
      TG_.setDescription(++iPh) = "Interface: XY plane";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 1]+mVN,TG_.sigma(8)|TG_.sigma(1)|TG_.sigma(6)|sITF|sDOM,Pt[1]));
        sITF = TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 2]+mVN,TG_.sigma(1)|TG_.sigma(2)             |sITF|sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 3]+mVN,TG_.sigma(2)|TG_.sigma(7)|TG_.sigma(6)|sITF|sDOM,Pt[3]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 4]+mVN,TG_.sigma(8)|TG_.sigma(7)|TG_.sigma(6)|sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 5]+mVN,             TG_.sigma(1)|TG_.sigma(3)|sITF|sDOM,Pt[5]));
      listV_.push_back(Vertex(V[ 6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[6]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 7]+mVN,TG_.sigma(2)|             TG_.sigma(3)|sITF|sDOM,Pt[7]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 8]+mVN,                          TG_.sigma(3)|sITF|sDOM,Pt[8]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 9]+mVN,TG_.sigma(4)|             TG_.sigma(6)|sITF|sDOM,Pt[9]));
      listV_.push_back(Vertex(V[10]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(6)     |sDOM,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)     |sDOM,Pt[11]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[12]+mVN,TG_.sigma(4)|             TG_.sigma(3)|sITF|sDOM,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)     |sDOM,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)     |sDOM,Pt[14]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[15]+mVN,TG_.sigma(5)|             TG_.sigma(6)|sITF|sDOM,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,TG_.sigma(5)|             TG_.sigma(3)|sITF|sDOM,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)     |sDOM,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)     |sDOM,Pt[18]));
      listV_.push_back(Vertex(V[19]+mVN,TG_.sigma(8)|TG_.sigma(1)|TG_.sigma(9)     |sDOM,Pt[19]));
      listV_.push_back(Vertex(V[20]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(9)     |sDOM,Pt[20]));
      listV_.push_back(Vertex(V[21]+mVN,TG_.sigma(2)|TG_.sigma(7)|TG_.sigma(9)     |sDOM,Pt[21]));
      listV_.push_back(Vertex(V[22]+mVN,TG_.sigma(8)|TG_.sigma(7)|TG_.sigma(9)     |sDOM,Pt[22]));
      VertexNum += 22;
      // Cubes with Z>0
      listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      listT_.push_back(Hexahedron(++ElementNum, V[9],V[12],V[10],V[11],V[4],V[8],V[3],V[7]));
      listT_.push_back(Hexahedron(++ElementNum, V[14],V[13],V[9],V[12],V[15],V[16],V[4],V[8]));
      listT_.push_back(Hexahedron(++ElementNum, V[15],V[16],V[4],V[8],V[18],V[17],V[1],V[5]));
      // Cube with Z<0
      listT_.push_back(Hexahedron(++ElementNum, V[22],V[4],V[21],V[3],V[19],V[1],V[20],V[2]));
      break;
//      6 octants (3/4 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 6:
      title_ = "Cube - Hexahedron mesh over 6 octants, 4 with Z>0, plus 2 octants Y>0, Z<0";
      nbBound = 8, nbIntrf = 3, nbSbDom = 1;
      TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X>0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y>0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z>0";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X<0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y<0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z<0";
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 9
      TG_.setDescription(++iPh) = "Interface: XZ plane";
      TG_.setDescription(++iPh) = "Interface: XY plane";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 1]+mVN,TG_.sigma(7)|TG_.sigma(1)|TG_.sigma(6)|sITF|sDOM,Pt[1]));
        sITF = TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 2]+mVN,TG_.sigma(1)|TG_.sigma(2)             |sITF|sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 3]+mVN,TG_.sigma(2)                          |sITF|sDOM,Pt[3]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 4]+mVN,TG_.sigma(7)|             TG_.sigma(6)|sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 5]+mVN,             TG_.sigma(1)|TG_.sigma(3)|sITF|sDOM,Pt[5]));
      listV_.push_back(Vertex(V[ 6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[6]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 7]+mVN,TG_.sigma(2)|             TG_.sigma(3)|sITF|sDOM,Pt[7]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 8]+mVN,                          TG_.sigma(3)|sITF|sDOM,Pt[8]));
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 9]+mVN,TG_.sigma(4)|TG_.sigma(7)|TG_.sigma(6)|sITF|sDOM,Pt[9]));
        sITF = TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[10]+mVN,TG_.sigma(2)|TG_.sigma(4)             |sITF|sDOM,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)     |sDOM,Pt[11]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[12]+mVN,TG_.sigma(4)|             TG_.sigma(3)|sITF|sDOM,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)     |sDOM,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)     |sDOM,Pt[14]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[15]+mVN,TG_.sigma(5)|             TG_.sigma(6)|sITF|sDOM,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,TG_.sigma(5)|             TG_.sigma(3)|sITF|sDOM,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)     |sDOM,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)     |sDOM,Pt[18]));
      listV_.push_back(Vertex(V[19]+mVN,TG_.sigma(7)|TG_.sigma(1)|TG_.sigma(8)     |sDOM,Pt[19]));
      listV_.push_back(Vertex(V[20]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(8)     |sDOM,Pt[20]));
      listV_.push_back(Vertex(V[21]+mVN,TG_.sigma(2)|TG_.sigma(8)             |sITF|sDOM,Pt[21]));
      listV_.push_back(Vertex(V[22]+mVN,TG_.sigma(7)|TG_.sigma(8)             |sITF|sDOM,Pt[22]));
      listV_.push_back(Vertex(V[23]+mVN,TG_.sigma(4)|TG_.sigma(7)|TG_.sigma(8)     |sDOM,Pt[23]));
      listV_.push_back(Vertex(V[24]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(8)     |sDOM,Pt[24]));
      VertexNum += 24;
      // Cubes with Z>0
      listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      listT_.push_back(Hexahedron(++ElementNum, V[9],V[12],V[10],V[11],V[4],V[8],V[3],V[7]));
      listT_.push_back(Hexahedron(++ElementNum, V[14],V[13],V[9],V[12],V[15],V[16],V[4],V[8]));
      listT_.push_back(Hexahedron(++ElementNum, V[15],V[16],V[4],V[8],V[18],V[17],V[1],V[5]));
      // Cubes with Z<0
      listT_.push_back(Hexahedron(++ElementNum, V[22],V[4],V[21],V[3],V[19],V[1],V[20],V[2]));
      listT_.push_back(Hexahedron(++ElementNum, V[23],V[9],V[24],V[10],V[22],V[4],V[21],V[3]));
      break;
//      7 octants (7/8 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 7:
      title_ = "Cube - Hexahedron mesh over 7 octants, 8 octants minus the octant X>0, Y<0, Z<0";
      nbBound = 9, nbIntrf = 6, nbSbDom = 1;
      TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X>0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y>0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z>0";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X<0";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y<0";
      TG_.setDescription(++iPh) = "Boundary: XY plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: YZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, containing the center point (vertex 4)";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z<0";
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane, Z>=0"; iPhI = iPh; // patch iPhI (10)
      TG_.setDescription(++iPh) = "Interface: XZ plane, Z>=0";        // patch iPhI+1
      TG_.setDescription(++iPh) = "Interface: XY plane, Y>=0";        // patch iPhI+2
      TG_.setDescription(++iPh) = "Interface: XY plane, Y<=0";        // patch iPhI+3
      TG_.setDescription(++iPh) = "Interface: YZ plane, Z<=0";        // patch iPhI+4
      TG_.setDescription(++iPh) = "Interface: XZ plane, Z<=0";        // patch iPhI+5
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 1]+mVN,TG_.sigma(8)|TG_.sigma(1)|TG_.sigma(6)|sITF|sDOM,Pt[1]));
        sITF = TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[ 2]+mVN,TG_.sigma(1)|TG_.sigma(2)             |sITF|sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2)|TG_.sigma(iPhI+4);
      listV_.push_back(Vertex(V[ 3]+mVN,TG_.sigma(2)                          |sITF|sDOM,Pt[3]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2)|TG_.sigma(iPhI+3)|TG_.sigma(iPhI+4)|TG_.sigma(iPhI+5);
      listV_.push_back(Vertex(V[ 4]+mVN,TG_.sigma(7)|TG_.sigma(8)|TG_.sigma(6)|sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 5]+mVN,             TG_.sigma(1)|TG_.sigma(3)|sITF|sDOM,Pt[5]));
      listV_.push_back(Vertex(V[ 6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[6]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[ 7]+mVN,TG_.sigma(2)|             TG_.sigma(3)|sITF|sDOM,Pt[7]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[ 8]+mVN,                          TG_.sigma(3)|sITF|sDOM,Pt[8]));
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2)|TG_.sigma(iPhI+3)|TG_.sigma(iPhI+5);
      listV_.push_back(Vertex(V[ 9]+mVN,TG_.sigma(4)                          |sITF|sDOM,Pt[9]));
        sITF = TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[10]+mVN,TG_.sigma(2)|TG_.sigma(4)             |sITF|sDOM,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)     |sDOM,Pt[11]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[12]+mVN,TG_.sigma(4)|             TG_.sigma(3)|sITF|sDOM,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)     |sDOM,Pt[13]));
        sITF = TG_.sigma(iPhI+3);
      listV_.push_back(Vertex(V[14]+mVN,TG_.sigma(4)|TG_.sigma(5)             |sITF|sDOM,Pt[14]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+3);
      listV_.push_back(Vertex(V[15]+mVN,TG_.sigma(5)|TG_.sigma(7)|TG_.sigma(6)|sITF|sDOM,Pt[15]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[16]+mVN,TG_.sigma(5)|             TG_.sigma(3)|sITF|sDOM,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)     |sDOM,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)     |sDOM,Pt[18]));
      listV_.push_back(Vertex(V[19]+mVN,TG_.sigma(8)|TG_.sigma(1)|TG_.sigma(9)     |sDOM,Pt[19]));
      listV_.push_back(Vertex(V[20]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(9)     |sDOM,Pt[20]));
        sITF = TG_.sigma(iPhI+4);
      listV_.push_back(Vertex(V[21]+mVN,TG_.sigma(2)|             TG_.sigma(9)|sITF|sDOM,Pt[21]));
        sITF = TG_.sigma(iPhI+4)|TG_.sigma(iPhI+5);
      listV_.push_back(Vertex(V[22]+mVN,TG_.sigma(7)|TG_.sigma(8)|TG_.sigma(9)|sITF|sDOM,Pt[22]));
        sITF = TG_.sigma(iPhI+5);
      listV_.push_back(Vertex(V[23]+mVN,TG_.sigma(4)|             TG_.sigma(9)|sITF|sDOM,Pt[23]));
      listV_.push_back(Vertex(V[24]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(9)     |sDOM,Pt[24]));
      listV_.push_back(Vertex(V[25]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(9)     |sDOM,Pt[25]));
      listV_.push_back(Vertex(V[26]+mVN,TG_.sigma(5)|TG_.sigma(7)|TG_.sigma(9)     |sDOM,Pt[26]));
      VertexNum += 26;
      // Cubes with Z>0
      listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      listT_.push_back(Hexahedron(++ElementNum, V[9],V[12],V[10],V[11],V[4],V[8],V[3],V[7]));
      listT_.push_back(Hexahedron(++ElementNum, V[14],V[13],V[9],V[12],V[15],V[16],V[4],V[8]));
      listT_.push_back(Hexahedron(++ElementNum, V[15],V[16],V[4],V[8],V[18],V[17],V[1],V[5]));
      // Cubes with Z<0
      listT_.push_back(Hexahedron(++ElementNum, V[22],V[4],V[21],V[3],V[19],V[1],V[20],V[2]));
      listT_.push_back(Hexahedron(++ElementNum, V[23],V[9],V[24],V[10],V[22],V[4],V[21],V[3]));
      listT_.push_back(Hexahedron(++ElementNum, V[25],V[14],V[23],V[9],V[26],V[15],V[22],V[4]));
      break;
//      8 octants (cube) = = = = = = = = = = = = = = = = = = = = = = = = = = =
   case 8:
      title_ = "Cube - Hexahedron mesh over 8 octants";
      if (nboctants > 0) {
         nbBound = 6, nbIntrf = 3, nbSbDom = 1;
         TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
         // With 5x8 initial hexahedrons.
//       Description of the boundary patches
         TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X>0";
         TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y>0";
         TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z>0";
         TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center point (vertex 4), X<0";
         TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center point (vertex 4), Y<0";
         TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center point (vertex 4), Z<0";
//       Description of the interface patches
         TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 7
         TG_.setDescription(++iPh) = "Interface: XZ plane";
         TG_.setDescription(++iPh) = "Interface: XY plane";
//       Description of the subdomain patches
         TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
           sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[ 1]+mVN,TG_.sigma(1)                          |sITF|sDOM,Pt[1]));
           sITF = TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[ 2]+mVN,TG_.sigma(1)|TG_.sigma(2)             |sITF|sDOM,Pt[2]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[ 3]+mVN,TG_.sigma(2)                          |sITF|sDOM,Pt[3]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[ 4]+mVN,                                       sITF|sDOM,Pt[4]));
           sITF = TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[ 5]+mVN,             TG_.sigma(1)|TG_.sigma(3)|sITF|sDOM,Pt[5]));
         listV_.push_back(Vertex(V[ 6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[6]));
           sITF = TG_.sigma(iPhI);
         listV_.push_back(Vertex(V[ 7]+mVN,TG_.sigma(2)|             TG_.sigma(3)|sITF|sDOM,Pt[7]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[ 8]+mVN,                          TG_.sigma(3)|sITF|sDOM,Pt[8]));
           sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[ 9]+mVN,TG_.sigma(4)                          |sITF|sDOM,Pt[9]));
           sITF = TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[10]+mVN,TG_.sigma(2)|TG_.sigma(4)             |sITF|sDOM,Pt[10]));
         listV_.push_back(Vertex(V[11]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)     |sDOM,Pt[11]));
           sITF = TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[12]+mVN,TG_.sigma(4)|             TG_.sigma(3)|sITF|sDOM,Pt[12]));
         listV_.push_back(Vertex(V[13]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)     |sDOM,Pt[13]));
           sITF = TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[14]+mVN,TG_.sigma(4)|TG_.sigma(5)             |sITF|sDOM,Pt[14]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[15]+mVN,TG_.sigma(5)                          |sITF|sDOM,Pt[15]));
           sITF = TG_.sigma(iPhI);
         listV_.push_back(Vertex(V[16]+mVN,TG_.sigma(5)|             TG_.sigma(3)|sITF|sDOM,Pt[16]));
         listV_.push_back(Vertex(V[17]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)     |sDOM,Pt[17]));
           sITF = TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[18]+mVN,TG_.sigma(5)|TG_.sigma(1)             |sITF|sDOM,Pt[18]));
           sITF = TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[19]+mVN,             TG_.sigma(1)|TG_.sigma(6)|sITF|sDOM,Pt[19]));
         listV_.push_back(Vertex(V[20]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)     |sDOM,Pt[20]));
           sITF = TG_.sigma(iPhI);
         listV_.push_back(Vertex(V[21]+mVN,TG_.sigma(2)|             TG_.sigma(6)|sITF|sDOM,Pt[21]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[22]+mVN,                          TG_.sigma(6)|sITF|sDOM,Pt[22]));
           sITF = TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[23]+mVN,TG_.sigma(4)|             TG_.sigma(6)|sITF|sDOM,Pt[23]));
         listV_.push_back(Vertex(V[24]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(6)     |sDOM,Pt[24]));
         listV_.push_back(Vertex(V[25]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)     |sDOM,Pt[25]));
           sITF = TG_.sigma(iPhI);
         listV_.push_back(Vertex(V[26]+mVN,TG_.sigma(5)|             TG_.sigma(6)|sITF|sDOM,Pt[26]));
         listV_.push_back(Vertex(V[27]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)     |sDOM,Pt[27]));
         VertexNum += 27;
         // Cubes with Z>0
         listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
         listT_.push_back(Hexahedron(++ElementNum, V[9],V[12],V[10],V[11],V[4],V[8],V[3],V[7]));
         listT_.push_back(Hexahedron(++ElementNum, V[14],V[13],V[9],V[12],V[15],V[16],V[4],V[8]));
         listT_.push_back(Hexahedron(++ElementNum, V[15],V[16],V[4],V[8],V[18],V[17],V[1],V[5]));
         // Cubes with Z<0
         listT_.push_back(Hexahedron(++ElementNum, V[22],V[4],V[21],V[3],V[19],V[1],V[20],V[2]));
         listT_.push_back(Hexahedron(++ElementNum, V[23],V[9],V[24],V[10],V[22],V[4],V[21],V[3]));
         listT_.push_back(Hexahedron(++ElementNum, V[25],V[14],V[23],V[9],V[26],V[15],V[22],V[4]));
         listT_.push_back(Hexahedron(++ElementNum, V[26],V[15],V[22],V[4],V[27],V[18],V[19],V[1]));
      }
      else {
         nbBound = 6, nbIntrf = 0, nbSbDom = 1;
         TG_ = TopoGeom(nbBound,nbIntrf,nbSbDom);
         // With 5 initial hexahedrons.
//       Description of the boundary patches
         TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center of the cube, X>0";
         TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center of the cube, Y>0";
         TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center of the cube, Z>0";
         TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to the center of the cube, X<0";
         TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to the center of the cube, Y<0";
         TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to the center of the cube, Z<0";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
         listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(6)|sDOM,Pt[27]));
         listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)|sDOM,Pt[20]));
         listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(6)|sDOM,Pt[24]));
         listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)|sDOM,Pt[25]));
         listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(5)|TG_.sigma(1)|TG_.sigma(3)|sDOM,Pt[17]));
         listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sDOM,Pt[6]));
         listV_.push_back(Vertex(V[7]+mVN,TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(3)|sDOM,Pt[11]));
         listV_.push_back(Vertex(V[8]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(3)|sDOM,Pt[13]));
         VertexNum += 8;
         listT_.push_back(Hexahedron(++ElementNum, V[4],V[8],V[3],V[7],V[1],V[5],V[2],V[6]));
      }
      break;
   }
}

} // end of namespace subdivision
} // end of namespace xlifepp
