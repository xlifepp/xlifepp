/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VolMeshTetSphere.hpp
  \author Y. Lafranche
  \since 15 Oct 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::VolMeshTetSphere class

  This class allows the construction of a volumic mesh of a sphere or part of sphere.
  The mesh is made of tetrahedra and is built by successive subdivisions of an
  initial elementary mesh consisting of n tetrahedra, n=1,...8, lying each in one
  octant of the 3D space around the origin.
  Depending on the type argument, the mesh will fill the original tetrahedra or
  the corresponding portion of the sphere.

  Two special cases are the half-sphere and the sphere, where such a mesh can be
  obtained after subdivision of one initial tetrahedron. In these two cases, the
  final mesh is to be compared with the one obtained starting from 4 (respectively
  8 tetrahedra): the number of elements is smaller, but if type > 0 (spheric subdi-
  vision) then some tetrahedrons are flat.
*/

#ifndef VOL_MESH_TET_SPHERE_HPP
#define VOL_MESH_TET_SPHERE_HPP

#include "TetrahedronMesh.hpp"
#include "PointUtils.hpp"

#include <vector>
#include <iostream>

namespace xlifepp {
namespace subdivision {

/*!
   \class VolMeshTetSphere
*/
class VolMeshTetSphere : public TetrahedronMesh
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   VolMeshTetSphere(const std::vector<std::pair<real_t, dimen_t> >& rots, const int nboctants,
                    const number_t nbsubdiv=0, const number_t order=1,
                    const number_t type=1, const real_t radius=1.,
                    const Point Center=Point(0,0,0),
                    const number_t minVertexNum=1, const number_t minElementNum=1);

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   //! prints, on stream os, some statistics about the mesh
   void statistics(std::ostream& os) const ;

private:

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
   //! create initial mesh
   void initMesh(const std::vector<std::pair<real_t, dimen_t> >& rots, const int nboctants,
                 const real_t radius, const Point& Center,
                 number_t& VertexNum, number_t& ElementNum);

}; // end of Class VolMeshTetSphere

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* VOL_MESH_TET_SPHERE_HPP */
