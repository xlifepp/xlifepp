/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CartesianFig.hpp
  \author Y. Lafranche
  \since 08 Apr 2014
  \date 08 Apr 2014

  \brief definition of the xlifepp::subdivision::CartesianFig class

  Class xlifepp::subdivision::CartesianFig is a template class used as a frame for the definition of
  cartesian elements, namely quadrangles and hexahedrons.
*/

#ifndef CARTESIAN_FIG_HPP
#define CARTESIAN_FIG_HPP

#include "GeomFigure.hpp"

namespace xlifepp {
namespace subdivision {

/*!
   \class CartesianFig
*/
template<class T_>
class CartesianFig : public GeomFigure
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   CartesianFig(const number_t num, const number_t bdSideNum)
   :GeomFigure(num,nb_main_vertices_,bdSideNum){}

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! returns the number of main vertices of the figure
   static number_t numberOfMainVertices() { return nb_main_vertices_; }
   virtual number_t numberOfO1Vertices() const { return nb_main_vertices_; }

   //! returns the number of edges of the figure
   static number_t numberOfEdges() { return nb_edges_; }
   virtual number_t numberOfEdgesinFig() const { return nb_edges_; }

   //! returns the number of faces of the figure
   static number_t numberOfFaces() { return nb_faces_; }

   //! returns the number of vertices by face of the figure
   virtual number_t numberOfVertByFace() const { return 4; }

   //! get back the orientation of a face. Returns 1 if positive, -1 if negative.
   virtual short faceOrientation(short indFace) const { return 1; }

   //! returns the high order vertices (given by their rank) of face number i >= 1
   virtual std::vector<number_t> rkOfHOVeOnFace(const number_t Order, const number_t i) const;

protected:
   static const number_t nb_main_vertices_; //!< number of main vertices, i.e. vertices of order 1
   static const number_t nb_edges_;         //!< number of edges
   static const number_t nb_faces_;         //!< number of faces

}; // end of Class CartesianFig

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* CARTESIAN_FIG_HPP */
