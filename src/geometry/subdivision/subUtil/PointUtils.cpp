/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file PointUtils.cpp
  \author Y. Lafranche
  \since 04 Dec 2007
  \date 09 Jul 2012

  \brief Implementation of xlifepp::Point class additionnal related functions
*/

#include "PointUtils.hpp"

namespace xlifepp {
namespace subdivision {

/*!
 Computation of the barycenter of the points stored in the vector VP, with
 associated coefficients stored in coef.
 */
Point barycenter(const real_t *coef, const std::vector<Point>& VP){
   real_t s=0;
   Point p(std::vector<real_t>(VP[0].size(),0.));

   for (size_t n=0; n<VP.size(); n++) {
      p += coef[n]*VP[n];
      s += coef[n];
   }
   p /= s;
   return p;
}

/*!
Computation of the barycenter of the points stored in the vector VP, with
associated coefficients stored in coefn as std::vector.
*/
Point barycenter(const std::vector<real_t>& coef, const std::vector<Point>& VP) {
	real_t s = 0;
	Point p(std::vector<real_t>(VP[0].size(), 0.));

	std::vector<real_t>::const_iterator itc = coef.begin();
	std::vector<Point>::const_iterator itp = VP.begin();
	for (; itp != VP.end(); ++itp, ++itc)
	{
		p += *itc * *itp;
		s += *itc;
	}
	p /= s;
	return p;
}

//! Projection of the point P onto the plane defined by the point A and the normal unitary vector U
Point projOnPlane(const Point& P, const Point& A, const Vect& U){
   Vect PA=toVector(P,A);
   real_t lambda = dot(PA,U);
   return translate(P,lambda, U);
}

/*!
 Projection onto the sphere S of the barycenter of the points stored in VP
 with associated coefficients stored in coef. The points in VP are assumed
 to be lying on the sphere S centered at the origin, with the radius Radius.
 */
Point projOnSph(const real_t *coef, const std::vector<Point>& VP, const real_t Radius){
   Point P=barycenter(coef,VP);
   // d is the distance from the barycenter to the origin
   real_t d = norm(P);
   // project the barycenter onto the sphere
   P /= d;
   P *= Radius;
   return P;
}

/*!
 Rotation of the point P by the angle theta (given by their cosine ct and
 sine st) around the axis defined by the point A and the unitary vector U
 in the plane defined by the point P and the normal unitary vector U.
 */
Point rotInPlane(const Point& P, const real_t ct, const real_t st, const Point& A, const Vect& U){
   Point Q(P);
   Point B = projOnPlane(A,P,U);
   Vect BP=toVector(B,P);
   real_t lambda = norm(BP);
   if (lambda > theTolerance) {
      Vect e1(BP/lambda);
      Vect e2 = crossProduct(U,e1);
      // (e1, e2) is a unitary basis in the plane orthogonal to U
      real_t coef = st * lambda;
      for (number_t i=0; i<Q.size(); i++) {
         Q[i] = B[i] + ct * BP[i] + coef * e2[i];
      }
   }
   //else { the point P lies on the rotation axis. No computation is needed. }
   return Q;
}

/*!
 Rotation of P1 by the angle alpha = k/n * ang(P1,O,P2) in the plane (P1,O,P2).
 P1 and P2 are assumed to be lying onto the sphere, centered at the origin 0.
 */
Point rotOnSph(const Point& P1, const Point& P2, const int k, const int n){
   Point O(0,0,0);// Origin
   Point P(O);
   Vect V1=toVector(O,P1);
   Vect V2=toVector(O,P2);
   Vect W = crossProduct(V1,V2);// W = V1 x V2
   // compute the radius r of the sphere from P1 (should be the same from P2)
   real_t r = norm(V1);
   // compute the angle alpha = ang(V1, V2)
   real_t nW = norm(W);
   real_t alpha = std::asin(nW/(r*r));
   if (dot(V1,V2) < 0) alpha = pi_ - alpha;// alpha = ang(P1, O, P2)
   alpha /= n;
   alpha *= k;
   W *= 1./nW;// normalize W
   Vect U = crossProduct(W,V1);// U = W x V1
   // rotate P1 by angle alpha
   real_t ca = std::cos(alpha);
   real_t sa = std::sin(alpha);
   for (number_t i=0; i<P.size(); i++) {
      P[i] = ca*V1[i] + sa*U[i];
   }
   return P;
}

//! Translation of the point P by the vector lambda*U
Point translate(const Point& P, const real_t lambda, const Vect& U){
   Point Q(P);
   for (number_t i=0; i<Q.size(); i++) {
      Q[i] += lambda*U[i];
   }
   return Q;
}

} // end of namespace subdivision
} // end of namespace xlifepp
