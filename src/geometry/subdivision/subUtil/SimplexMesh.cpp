/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SimplexMesh.cpp
  \author Y. Lafranche
  \since 13 Jun 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::SimplexMesh class members and related functions
*/

#include "SimplexMesh.hpp"
#include "Tetrahedron.hpp"
#include "Triangle.hpp"

#include <algorithm>
#include <vector>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
/*!
 Effective computation of high order vertices inside the face of element Elem.
 The face is defined by vrV, the ranks of its 3 vertices in the vertex list.
 If the element is a triangle, the face is the element itself.
 */
template<class T_>
void SimplexMesh<T_>::computeHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                                  const vector<number_t>& vrV){
   const Vertex& V1=this->listV_[vrV[0]];
   const Vertex& V2=this->listV_[vrV[1]];
   const Vertex& V3=this->listV_[vrV[2]];
   refnum_t localcod = V1.locCode() & V2.locCode() & V3.locCode();
   vector<Point> VP(3);
   VP[0] = V1.geomPt();
   VP[1] = V2.geomPt();
   VP[2] = V3.geomPt();
   real_t coef[3];
   Point P;
   for (number_t i1=1; i1<order; i1++) {
      coef[0] = i1;
      for (number_t i2=1; i2<order-i1; i2++) {
        coef[1] = i2;
        coef[2] = order-i1-i2;
        P = (this->*(this->newVertexPt_))(localcod,coef,VP);
        // add rank of the new vertex in the element list:
        Elem.vertices_.push_back(VertexNum);
        // store the new vertex in the general list listV_:
        this->listV_.push_back(Vertex(++VertexNum,localcod,P));
      }
   }
}
/*!
 Create or retrieve high order vertices inside the face numFace of element Elem.
 Such vertices exist only for values of order >= 3.
 */
template<class T_>
void SimplexMesh<T_>::createHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                                 const number_t numFace, map_set_pair_in& SeenFaces){
   vector<number_t> vrV = Elem.rkOfO1VeOnFace(numFace);
   // Map key is a set in order to create a unique entry in the map, regardless of
   // the way the face is defined.
   number_t& rV1 = vrV[0];
   number_t& rV2 = vrV[1];
   number_t& rV3 = vrV[2];
   set_n Face; Face.insert(rV1); Face.insert(rV2); Face.insert(rV3);
   map_set_pair_in::iterator itSF;

   if ((itSF=SeenFaces.find(Face)) == SeenFaces.end()) {
      // This face has not yet been seen: compute the corresponding vertices
      // Store the face along with the corresponding couple (rank of the first new
      // vertex created in the face, rank of the first main vertex defining the face)
      pair_in NV (VertexNum, rV1);
      SeenFaces.insert(make_pair(Face,NV));

      computeHOfV(Elem, order, VertexNum, vrV);
   }
   else {
// Face found: get back the corresponding vertices whose numbers are consecutive
// over this face, starting from num.
      number_t num = (itSF->second).first;
   // Find the face definition: if the face found is (1,2,3), then the current
   // face is (1,3,2), (2,1,3) or (3,2,1), and cannot be (1,2,3), (2,3,1) nor
   // (3,1,2) because of the orientation of the tetrahedrons.
      number_t fv = (itSF->second).second;
      int ind[3];
      for (int i=0; i<3; i++) {ind[i]=i;}
      if (fv == rV1) {
         ind[1]=2;ind[2]=1;}// current is (1,3,2) : swap indices 2 and 3
      else if (fv == rV2) {
         ind[0]=1;ind[1]=0;}// current is (2,1,3) : swap indices 1 and 2
      else {
         ind[0]=2;ind[2]=0;}// current is (3,2,1) : swap indices 1 and 3
   // Build the table of barycentric coordinates associated to each point of the
   // face: ind[i]=i would give the original correspondence, i.e. the one used
   // when the point was created (cf. SimplexMesh<T_>::computeHOfV).
      vector< vector<number_t> > tbc;
      vector<number_t> bc(4);
      const int irv = 3; // index of the rank of the vertex in bc (last element)
      for (number_t i1=1; i1<order; i1++) {
         bc[ind[0]] = i1;
         for (number_t i2=1; i2<order-i1; i2++) {
            bc[ind[1]] = i2;
            bc[ind[2]] = order-i1-i2;
            bc[irv] = num++;
            tbc.push_back(bc);
         }
      }
   // Now sort the table according to the first 2 columns. After that, going
   // through the table will give the vertices in the correct order.
      sort(tbc.begin(),tbc.end(),this->cmpvect);
      vector< vector<number_t> >::iterator ittbc;
      for (ittbc=tbc.begin(); ittbc != tbc.end(); ittbc++) {
         Elem.vertices_.push_back((*ittbc)[irv]);// add rank of the vertex
      }
   }
}

// class instantiations:
template class SimplexMesh<Triangle>;
template class SimplexMesh<Tetrahedron>;

} // end of namespace subdivision
} // end of namespace xlifepp
