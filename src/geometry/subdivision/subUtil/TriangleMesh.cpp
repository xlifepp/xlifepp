/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TriangleMesh.cpp
  \author Y. Lafranche
  \since 24 May 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::TriangleMesh class members and related functions
*/

#include "TriangleMesh.hpp"
#include "TeXPolygon.hpp"

#include <algorithm>
#include <vector>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------

/*!
 Prints, on stream ftex, Fig4TeX instructions to draw the numbering convention
 for a triangle mesh of order k.
 To be called as TriangleMesh(1,0,k,0).printTeXNumberConvention(ftex);
 */
void TriangleMesh::printTeXNumberConvention(ostream& ftex) const {
   if (subdiv_level_ > 0 || listT_.size() > 1) {
      cerr << "*** Error in printTeXNumberConvention:" << endl;
      cerr << "*** Numbering convention output function expects only one element.";
      cerr << endl;
   }
   int dim = max(5,int(order_+1));
   ftex << "\\input fig4tex.tex" << endl;
   ftex << "\\def\\drawFace#1#2#3{" << endl;
   ftex << "\\psset(color=\\FaceColor, fill=yes)\\psline[#1,#2,#3]" << endl;
   ftex << "\\psset(color=\\defaultcolor, fill=no)\\psline[#1,#2,#3,#1]}"<<endl;
   ftex << "%" << endl;
   ftex << "\\def\\defpts{" << endl;
   printTeXInArea(ftex,0,boundaryArea);
   ftex << "}" << endl;
   ftex << "% 1. Definition of characteristic points" << endl;
   ftex << "\\figinit{" << dim << "cm,orthogonal}" << endl;
   ftex << "\\defpts" << endl;
   ftex << "\\figset proj(psi=40, theta=45)" << endl;
   ftex << "%" << endl;
   ftex << "% 2. Creation of the graphical file" << endl;
   ftex << "\\psbeginfig{}" << endl;
   ftex << "\\figpt 0:(0,-1,0)\\psaxes 0(0.2)" << endl;
   number_t numBoundary4 = 4;
   printTeXFacesInArea(ftex,boundaryArea,numBoundary4);
   ftex << "\\psendfig" << endl;
   ftex << "%" << endl;
   ftex << "% 3. Writing text on the figure" << endl;
   ftex << "\\figvisu{\\figBoxA}{}{" << endl;
   ftex << "\\figsetmark{$\\figBullet$}\\figsetptname{{\\bf #1}}" << endl;
   printTeXInArea(ftex,1,boundaryArea,numBoundary4);
   ftex << "}" << endl;
   ftex << "\\centerline{\\box\\figBoxA}" << endl;
   ftex << "\\medskip" << endl;
   ftex << "\\centerline{\\bf Nodes numbering convention for the triangle of order "<< order_ << ".}" << endl;
   ftex << "%-------------------------------- End of figure --------------------------------" << endl;
   ftex << "\\vfill" << endl;
}

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
/*!
 Subdivision of a triangle T into 4 triangles.

 Input arguments:
 \param T: triangle to be subdivided

 Input and output arguments (updated by this function) :
 \param ElementNum: number of the last triangle created in the mesh
 \param VertexNum: number of the last vertex created in the mesh
 \param listT: list of triangles in the mesh
 \param SeenEdges: temporary map used to build the mesh (contains the edges already seen,
                    i.e. taken into account, along with the associated vertex number,
                    in order to avoid vertex duplication)

 Numbering and orientation convention
 - - - - - - - - - - - - - - - - - - -
 The vertices and edges are numbered according to a convention given in Triangle.hpp.
 The triangle T=(1,2,3) is subdivided into 4 "sub-"triangles: 3 at each
 vertex plus 1 in the "kernel". Thus, the subdivision algorithm involves 3 more
 vertices. They are the midpoints of the edges of the triangle T if a flat
 subdivision is requested ; otherwise, the midpoints are projected onto the
 boundary surface. Their numbers follow the numbering convention given below:
 \verbatim
                   3                                         3
                  / \                                       / \
                 /   \                                     /   \
                /     \                                   /     \
               /       \                                 /       \
              /         \         SUBDIVISION           /         \
             /           \                             /           \
            /             \          ---->            /             \
           /               \                         6. . . . . . . .5
          /                 \                       / .             . \
         /                   \                     /   .           .   \
        /                     \                   /     .         .     \
       /                       \                 /       .       .       \
      /                         \               /         .     .         \
     /                           \             /           .   .           \
    /                             \           /             . .             \
   /_______________________________\         /_______________4_______________\
  1                                2        1                                2
 \endverbatim

 If, for instance, the points 2 and 3 lie on the boundary and if a curved mesh (non flat)
 mesh is requested, then the point 5 is projected onto the boundary surface.

 Each "sub-"triangle is defined by a sequence of vertices that matches the above
 numbering convention.
 Nota: the orientation of the edges is not used in this subdivision algorithm.
 */
void TriangleMesh::algoSubdiv(const Triangle& T, number_t& ElementNum,
                              number_t& VertexNum, vector<Triangle>& listT,
                              map_pair_num& SeenEdges) {
// Creation of at most nb_edges_by_element_ new vertices: their rank in the list listV_ is retained in rV
   vector<number_t> rV(nb_edges_by_element_);
   for (number_t i=0; i<nb_edges_by_element_; i++) {
      rV[i]=createVertex(VertexNum,T.rkOfO1VeOnEdge(i+1),SeenEdges);}

// To help transmission of the curved boundary edge number to the correct 2 sub-triangles:
   number_t bdSideNo[]={0,0,0}, bdednum=T.bdSideOnCP();
   if (bdednum > 0) {
      for (number_t i=0; i<2; i++) {
         bdSideNo[T.getrkEdge(bdednum-1,i)] = bdednum;
      }
   }

// 1. Triangles at each vertex
// Vi is T.rankOfVertex(i) for i=1,...3 and is rV[i-4] for i=4,...6.
   listT.push_back(Triangle(++ElementNum,T.rankOfVertex(1),rV[0],rV[2],bdSideNo[0])); // (1,4,6)
   listT.push_back(Triangle(++ElementNum,rV[0],T.rankOfVertex(2),rV[1],bdSideNo[1])); // (4,2,5)
   listT.push_back(Triangle(++ElementNum,rV[2],rV[1],T.rankOfVertex(3),bdSideNo[2])); // (6,5,3)

// 2. Triangle inside
   listT.push_back(Triangle(++ElementNum,rV[0],rV[1],rV[2])); // (4,5,6)
}

/*!
 Create high order vertices inside the triangle T.
 On input, VertexNum is the previous number used. On output, it is the last one.
 */
void TriangleMesh::createHOiV(Triangle&  T, const number_t order, number_t& VertexNum) {
   computeHOfV(T, order, VertexNum, T.rkOfO1VeOnFace(1));
}

/*!
 Create the initial mesh (or "seed" of the mesh) to be subdivided for a geometry of revolution,
 i.e. defined by an axis of revolution and circular sections. The general shape is a truncated
 cone, that can be a cylinder if the radius is constant.
 In the following, the word "object" is used to designate the geometric shape.

 The triangles are specified by an oriented set of vertices so that the normal they define
 is exterior to the object.

 \param nbslices: number of slices of elements orthogonal to the axis of the object
                      If this input value is equal to 0, a number of slices is computed from
                      the geometrical data.
 \param radius1, radius2 : radii of the object
 \param CharacPts: the two end points of the axis of the object, corresponding respectively
                      to radius1 and radius2
 \param VertexNum: number of the last vertex created (modified on output)
 \param ElementNum: number of the last element created (modified on output)
 \param vSI: vector for shape information at both ends of the object:
                     there can be nothing in which case there is a boundary,
                     or a "hat" in which case there is a subdomain.
 */
void TriangleMesh::initMesh(const number_t nbslices, const real_t radius1, const real_t radius2,
                            const vector<Point>& CharacPts, number_t& VertexNum,
                            number_t& ElementNum, const vector<ShapeInfo>& vSI){
   real_t R1, R2, slice_height;
   Point P1, P2;
   vector<ShapeInfo> cvSI;
   string bottomPt, topPt, shape;
   bool iscone;
   DefaultGeometry *DG;
   SurfPlane *SP;
   SurfCone *SC;
   vector <PatchGeometry *> EBS;
   int nb_sl;
   initRevMesh(nbslices, radius1,radius2, CharacPts, vSI,
               R1,R2, P1,P2, cvSI, bottomPt, topPt, iscone, shape, DG, SP, SC, EBS, nb_sl, slice_height);

   number_t iPh = 0, iPhSD;
   title_ = shape + " - Triangle mesh";
   { // Definition of 0, 1 or 2 boundaries...
      number_t nbBound = 0;
      if ( cvSI[0].shapeCode_ == None ) { nbBound++; }
      if ( cvSI[1].shapeCode_ == None ) { nbBound++; }
      vector<number_t> B_patch(nbBound), B_dimen(nbBound);
      for (size_t i=0; i<nbBound; i++) { B_patch[i] = i+1; B_dimen[i] = 1; }
     // ... nbIntrf interfaces...
     // (a plane between two successive slices, plus eventually one plane between each end slice
     //  and the corresponding "hat" end shape)
      number_t nbIntrf = nb_sl-1, nbSbDom = nb_sl;
      if (EBS[0]->curvedShape()) {nbIntrf++; nbSbDom++;} // if non Flat
      else if ( cvSI[0].shapeCode_ != None ) { nbSbDom++; }   // Flat end shape
      if (EBS[1]->curvedShape()) {nbIntrf++; nbSbDom++;}
      else if ( cvSI[1].shapeCode_ != None ) { nbSbDom++; }
      vector<number_t> I_patch(nbIntrf), I_dimen(nbIntrf);
      number_t numPa = nbBound;
      for (size_t i=0; i<nbIntrf; i++) { I_patch[i] = ++numPa; I_dimen[i] = 1; }
     // ...and nbSbDom subdomains
     // (each slice plus eventually each "hat" end shape)
      vector<number_t> D_patch(nbSbDom), D_dimen(nbSbDom);
      for (size_t i=0; i<nbSbDom; i++) { D_patch[i] = ++numPa; D_dimen[i] = 2; }

      number_t nbBI =  nbBound + nbIntrf;
      number_t nbPatches =  nbBI + nbSbDom;
      vector<PatchGeometry *> P_geom(nbPatches);
      for (size_t i=0; i<nbBI; i++) { P_geom[i] = SP; }

      if (type_ == 0) {
         // P_geom = {SP,      SP, SP,...   SP,  EBS[0],DG,...   DG,EBS[1]}
         //           \ nbBound /   \_nbIntrf_/  \__       nbSbDom     __/   (nbBound may be 0)
         delete SC; // not needed
         for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = DG; }
     }
      else {
         // P_geom = {SP,      SP, SP,...   SP,  EBS[0],SC,...   SC,EBS[1]}
         //           \ nbBound /   \_nbIntrf_/  \__       nbSbDom     __/   (nbBound may be 0)
         delete DG; // not needed
         for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = SC; }
      }
      if ( cvSI[0].shapeCode_ != None ) { P_geom[nbBI] = EBS[0]; }
      if ( cvSI[1].shapeCode_ != None ) { P_geom[nbPatches-1] = EBS[1]; }
      TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
      iPhSD = nbBound+nbIntrf;
   }
   refnum_t sBEs1=0, sBEs2=0; // localization code of the following patches
//       Description of the boundary patches
   if ( cvSI[0].shapeCode_ == None ) {
    TG_.setDescription(++iPh) = "Boundary: End curve on the side of end point " + bottomPt; sBEs1 = TG_.sigma(iPh); }
   if ( cvSI[1].shapeCode_ == None ) {
    TG_.setDescription(++iPh) = "Boundary: End curve on the side of end point " + topPt;    sBEs2 = TG_.sigma(iPh); }

// Steps 1 and 2 of the construction
   vector<Point> Pt;
   real_t prevRitrf;
   number_t rVaFirst;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
   startConeTrunk(R1, P1, cvSI[0].shapeCode_, bottomPt, EBS, SC, slice_height, nb_sl, iscone, sBEs1,
                  iPh, iPhSD, VertexNum, ElementNum, Pt, prevRitrf, rVaFirst);

// 3. Set of 4 vertices on top of last slice and associated triangles
   Vect AxV = SC -> AxisVector();
   for (size_t np=0; np<5; np++) { Pt[np] = translate(Pt[np],slice_height,AxV); }
      if (iscone) {
        Point& homcen(Pt[4]);
        // Compute the homothety ratio between 2 consecutive slices. homcen is the homothety center.
        real_t ratio = SC -> radiusAt(homcen) / prevRitrf;
        // Apply homothety to all the points in the interface, except the center.
        for (size_t np=0; np<4; np++) { Pt[np] = translate(homcen,ratio,toVector(homcen,Pt[np])); }
      }
   refnum_t sITFslice, sDOM;
  ++iPhSD;
  sDOM = TG_.sigma(iPhSD);    // Last slice
   if (EBS[1]->curvedShape()) { // if non Flat (there is a non flat hat)
      sITFslice = TG_.sigma(++iPh);     // Last interface between last slice and top end subdomain
      sDOM |= TG_.sigma(iPhSD+1); // Last slice and top end subdomain sharing this interface
   }
   else {
      sITFslice = 0;                    // No more transversal interface
      if ( cvSI[1].shapeCode_ != None ) { sDOM |= TG_.sigma(iPhSD+1); }// Flat lid
   }
   for (size_t np=0; np<4; np++) {
      listV_.push_back(Vertex(++VertexNum,sBEs2|sITFslice|sDOM,Pt[np]));
   }
   // Associated triangles
   number_t rVa = rVaFirst, rVb = rVa+1;
   subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum); rVa++; rVb++;
   subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum); rVa++; rVb++;
   subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum); rVa++; rVb=rVaFirst;
   subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum);

   rVaFirst+=4;
   if (EBS[1]->curvedShape()) { // if non Flat (there is a non flat hat)
// 4.a. Add triangles on the top
//      The central point on top of the last slice is internal and thus should not be inserted.
      Point TopPt(EBS[1]->EndPt2()); // get Apex computed in endBoundaryShapes

      // Insert last vertex associated to top point in the global list
         sDOM = TG_.sigma(iPhSD+1); // Top end subdomain
         listV_.push_back(Vertex(++VertexNum,sDOM,TopPt)); // rank rVd
      // Insert last 4 triangles in the global list

      number_t rVa = rVaFirst, rVb = rVa+1, rVd = rVaFirst+4;
/*!
 \verbatim
                 d
                 |
             a+2 |                  d is the top point
              \  |                  a, b are in the top circular section of the object
               \ |
     a+3________\|_________b=a+1
                 \
                  \
                   \
                    a
 \endverbatim
*/
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd)); rVa++; rVb++;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd)); rVa++; rVb++;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd)); rVa++; rVb=rVaFirst;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVd));
//      Description of the interface patch
      TG_.setDescription(iPh) = "Interface: Section containing end point" + topPt;
   }
   else {
    if ( cvSI[1].shapeCode_ != None ) {// there is a flat lid
// 4.b The central point of the top face lies on last subdomain patch
         sDOM = TG_.sigma(iPhSD+1); // Top end subdomain
         listV_.push_back(Vertex(++VertexNum,sDOM,Pt[4]));
/*!
 \verbatim


             a+2
              \                    a, b, c are in the top circular section of the object
               \
     a+3________\_________b=a+1
                 \c
                  \
                   \
                    a
 \endverbatim
 */
      // Insert last 4 triangles in the global list
      // They are oriented so that the normal is exterior to the object.
      // edge 1 is on the curved boundary.
      number_t rVa = rVaFirst, rVb = rVa+1, rVc =  rVaFirst+4;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVc,1)); rVa++; rVb++;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVc,1)); rVa++; rVb++;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVc,1)); rVa++; rVb=rVaFirst;
      listT_.push_back(Triangle(++ElementNum,rVa,rVb,rVc,1));
    }
    // else {} // iPhSD+1 is undefined !
   }

//       Description of the subdomain patches
   if ( cvSI[0].shapeCode_ != None ) { TG_.setDescription(++iPh) = "End subdomain on the side of end point " + bottomPt; }
   for (int isl=1; isl<=nb_sl; isl++) {
      ostringstream ss;
      ss << "Slice " << isl;
      TG_.setDescription(++iPh) = ss.str();
   }
   if ( cvSI[1].shapeCode_ != None ) { TG_.setDescription(++iPh) = "End subdomain on the side of end point " + topPt; }
}

/*!
 This function makes the first part of the initial mesh (or "seed" of the mesh) to be subdivided
 for a geometry of revolution,
 i.e. defined by an axis of revolution and circular sections. The general shape is a truncated
 cone, that can be a cylinder if the radius is constant.
 In the following, the word "object" is used to designate the geometric shape.
 */
void TriangleMesh::startConeTrunk(real_t R1, const Point& P1, const GeomEndShape bnd1, const string& bottomPt,
                                  const vector <PatchGeometry *>& EBS,
                                  const SurfCone *SC, real_t slice_height, int nb_sl, bool iscone, refnum_t sBEs1,
                                  number_t& iPh, number_t& iPhSD, number_t& VertexNum, number_t& ElementNum,
                                  vector<Point>& Pt, real_t& prevRitrf, number_t& rVaFirst) {
// Define 5 "reference" points on the circle centered at the origin.
// Then, rotate and translate them so that they lie on the bottom face of the object.
    Pt.push_back(Point(R1,0,0));  // Pt[0]
    Pt.push_back(Point(0,R1,0));  // Pt[1]
    Pt.push_back(Point(-R1,0,0)); // Pt[2]
    Pt.push_back(Point(0,-R1,0)); // Pt[3]
    Pt.push_back(Point(0,0,0));   // Pt[4]
   Vect Z(0,0,1);
   Vect AxV = SC -> AxisVector();
   Vect Nor = crossProduct(Z,AxV); // rotation axis (Z and AxV are both unitary vectors)
   real_t st = norm(Nor);
   if (st > theTolerance){
   // the axis of the object is not Z: we need to rotate the points around the axis (Pt[4],Nor)
      Nor *= (1./st); // normalize Nor
      real_t ct = dot(Z,AxV);
      for (size_t np=0; np<4; np++) { Pt[np] = rotInPlane(Pt[np],ct,st,Pt[4],Nor); }
   }
   Vect OCP1=toVector(Pt[4],P1);
   for (size_t np=0; np<5; np++) { Pt[np] = translate(Pt[np],1.,OCP1); }

   refnum_t sITFslice;
   refnum_t sDOM = TG_.sigma(++iPhSD); // first subdomain: bottom end subdomain or first slice
   rVaFirst = 0;
   if (EBS[0]->curvedShape()) { // if non Flat (there is a non flat hat)
// 1.a. Add triangles at bottom.
//      Set of 4 vertices at the bottom of the first slice whose center is internal, plus the bottom point.
      Point BottomPt(EBS[0]->EndPt2()); // get Apex computed in endBoundaryShapes
      // Insert first 5 vertices in the global list
         listV_.push_back(Vertex(++VertexNum,sDOM,BottomPt)); // rank 0
         rVaFirst++;
      sITFslice = TG_.sigma(++iPh);     // first transversal interface
      sDOM |= TG_.sigma(iPhSD+1);       // Bottom end subdomain and first slice
      for (size_t np=0; np<4; np++) {
         listV_.push_back(Vertex(++VertexNum,sITFslice|sDOM,Pt[np]));   // rank 1, 2, 3, 4
      }
         // -> center is internal here and thus should not be inserted

/*!
 \verbatim
             3
              \
               \                    0 is the bottom point
       4________\c_________2        1, 2, 3, 4, c are in the bottom circular section of the object
                 \                  c is the center (not inserted)
                 |\
                 | \
                 |  1
                 |
                 0
 \endverbatim
*/
      // Insert first 4 triangles in the global list
      listT_.push_back(Triangle(++ElementNum,2,1,0));
      listT_.push_back(Triangle(++ElementNum,3,2,0));
      listT_.push_back(Triangle(++ElementNum,4,3,0));
      listT_.push_back(Triangle(++ElementNum,1,4,0));
//      Description of the interface patch
      TG_.setDescription(iPh) = "Interface: Section containing end point " + bottomPt;
   }
   else {
    if (bnd1 == None) {
 // 1.b Set of 4 vertices at the bottom of the first slice.
//     They lie on the boundary.
 /*!
 \verbatim
             2
              \
               \
       3________\_________1        0, 1, 2, 3 are in the bottom circular section of the object
                 \
                  \
                   \
                    0
 \endverbatim
*/
      for (size_t np=0; np<4; np++) {
         listV_.push_back(Vertex(++VertexNum,sBEs1|sDOM,Pt[np])); // rank 0, 1, 2, 3
      }
      --iPhSD;
    }
    else {// there is a flat lid
// 1.c Set of 5 vertices at the bottom of the first slice.
/*!
 \verbatim
             3
              \
               \
       4________\c_________2        c, 1, 2, 3, 4 are in the bottom circular section of the object
                 \                  c=0 is the center
                  \
                   \
                    1
 \endverbatim
*/
         listV_.push_back(Vertex(++VertexNum,sDOM,Pt[4]));  // rank 0 (rVc)
         rVaFirst++;
      sDOM |= TG_.sigma(iPhSD+1);       // Bottom end subdomain and first slice
      for (size_t np=0; np<4; np++) {
         listV_.push_back(Vertex(++VertexNum,sDOM,Pt[np])); // rank 1, 2, 3, 4
      }
      // Insert first 4 triangles in the global list
      // They are oriented so that the normal is exterior to the object.
      // edge 1 is on the curved boundary.
      number_t rVc=0;
      listT_.push_back(Triangle(++ElementNum,2,1,rVc,1));
      listT_.push_back(Triangle(++ElementNum,3,2,rVc,1));
      listT_.push_back(Triangle(++ElementNum,4,3,rVc,1));
      listT_.push_back(Triangle(++ElementNum,1,4,rVc,1));
    }
   }
// 2. Set of 4 vertices on top of each slice and associated triangles (the 5 starting points are all transformed)
   // We consider the center point of the larger basis of the object (P1).
   // The images of this point by translations along the axis will be the centers of successive
   // homotheties in each interface plane.
   Point& homcen(Pt[4]);// warning: homcen is a reference to a point whose coordinates change
   prevRitrf = R1;
   for (int isl=1; isl<nb_sl; isl++,rVaFirst+=4) {
      for (size_t np=0; np<5; np++) { Pt[np] = translate(Pt[np],slice_height,AxV); }
      if (iscone) {
        // Compute the homothety ratio between 2 consecutive slices. homcen is the homothety center.
        real_t Ritrf = SC -> radiusAt(homcen), ratio = Ritrf / prevRitrf;
        prevRitrf = Ritrf;
        // Apply homothety to all the points in the interface, except the center.
        for (size_t np=0; np<4; np++) { Pt[np] = translate(homcen,ratio,toVector(homcen,Pt[np])); }
      }
      sITFslice = TG_.sigma(++iPh);                          // Next transversal interface
     ++iPhSD;
     sDOM = TG_.sigma(iPhSD) | TG_.sigma(iPhSD+1); // Two slices sharing this interface
      for (size_t np=0; np<4; np++) {
         listV_.push_back(Vertex(++VertexNum,sITFslice|sDOM,Pt[np]));
      }
         // center is internal here and thus should not be inserted
      // Associated triangles which are on the surface of the cone
      /*
       \verbatim
                      +b'
                     /|      Each quadrangle is of the form (a,b,b',a').
                   /  |       (a,b) = rotations of (0,1)
                 +a'  |
                 |    +b
                 |   /
                 | /
                 +a
       \endverbatim
      */
      number_t rVa = rVaFirst, rVb = rVa+1;
      subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum); rVa++; rVb++;
      subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum); rVa++; rVb++;
      subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum); rVa++; rVb=rVaFirst;
      subdivQuadrangle(rVa, rVb, rVb+4, rVa+4, ElementNum);
//      Description of the interface patch
      ostringstream ss;
      ss << "Interface: Section " << isl;
      TG_.setDescription(iPh) = ss.str();
   }
}

/*!
 Create two triangles that subdivide a quadrangle by chosing one of the two diagonals
 and keep the same orientation.
 Ui, i=1,...4 are the ranks of the vertices in the general vertex list.
 The vertices must be ordered as shown on the following figure.
 ElementNum is the number of the last triangle created and is updated here.

 \verbatim
           4 +-------+ 3      Subdivision:
             |     / |                1,2,3
             |   /   |                1,3,4
             | /     |
           1 +-------+ 2
 \endverbatim
 */
void TriangleMesh::subdivQuadrangle(const number_t U1, const number_t U2, const number_t U3, const number_t U4,
                                    number_t& ElementNum){
   listT_.push_back(Triangle(++ElementNum,U1,U2,U3));
   listT_.push_back(Triangle(++ElementNum,U1,U3,U4));
}

/*!
 Define some macros in the file associated to the stream ftex in order to display
 the mesh using Fig4TeX.
 */
void TriangleMesh::printTeXHeader(ostream& ftex) const {
   ftex << "\\def\\drawFace#1#2#3#4{" << endl;
   ftex << "\\figset(color=#4, fill=yes)\\figdrawline[#1,#2,#3]" << endl;
   ftex << "\\figset(color=default, fill=no)\\figdrawline[#1,#2,#3,#1]}" << endl;
   ftex << "\\def\\drawEdge#1#2#3{" << endl;
   ftex << "\\figset(color=#3, width=3)\\figdrawline[#1,#2]" << endl;
   ftex << "\\figset(color=default, with=default)}"<<endl;
   ftex << "\\def\\drawElem#1#2#3{\\figdrawline[#1,#2,#3,#1]}" << endl;
}

/*!
 Prints, on stream ftex, Fig4TeX instructions to define points to be used for
 the display of the mesh. These points are located in the domain. Boundary points
 and points lying on the interfaces (used for edges) also belong to this set,
 so the last argument is unused.
 */
void TriangleMesh::printTeXPoints(ostream& ftex, const bool withInterface) const {
   printTeXInArea(ftex,0,subdomainArea);
}

/*!
 Prints, on stream ftex, Fig4TeX instructions to draw faces of a triangle mesh
 and edges belonging to any area of kind TA. The faces and edges are ordered
 from the farthest to the nearest along the observation direction defined by the
 longitude psi and the latitude theta, given in degrees.
 */
void TriangleMesh::printTeXSortedAreaFoE(ostream& ftex, const topologicalArea TA,
                                         const float psi, const float theta) const {
//   Faces
   number_t last_sdom = TG_.numberOf(subdomainArea);
   for (number_t numArea=1; numArea<=last_sdom; numArea++) {
      // Define subdomain colors
      ftex << "\\def\\Color" << string(1,'@'+numArea)  // ColorA, ColorB...
           << "{" << colorOf(subdomainArea,numArea) << "}% " << TG_.nameOf(subdomainArea,numArea) << endl;
   }
   // Define inside color. This color is used to display faces seen from the interior of the domain.
   // This situation may occur depending on the shape of the domain and on the observation direction.
   number_t offset = last_sdom+1;
   string inside(1,'@'+offset);
   ftex << "\\def\\Color" << inside
        << "{" << colorOf(subdomainArea,offset) << "}% inside of domain (may be unused)" << endl;
//   Edges
   number_t last_area = TG_.numberOf(TA);
   for (number_t numArea=1; numArea<=last_area; numArea++) {
      // Define colors of TA
      ftex << "\\def\\Color" << string(1,'@'+numArea+offset)
           << "{" << colorOf(TA,numArea) << "}% " << TG_.nameOf(TA,numArea) << endl;
   }

   vector<TeXPolygon> BF;
//  Collect all the subdomain faces and keep their associated number.
   number_t number_of_faces = 0;
   for (number_t numArea=1; numArea<=last_sdom; numArea++) {
      vector< vector<number_t> > FonB = rk_facesIn(subdomainArea,numArea);
      number_of_faces += FonB.size();
      vector< vector<number_t> >::iterator itFonB;
      for (itFonB=FonB.begin(); itFonB != FonB.end(); itFonB++) {
         BF.push_back(TeXPolygon(*itFonB,numArea,listV_));
         }
      }
//  Collect all the edges on all 1D areas of kind TA and keep their
//  associated number, shifted by offset to select them below during file creation.
//  Indeed, as edges and faces are all treated the same way by the sort process,
//  we need an indicator to distinguish the two kinds of TeXPolygon.
   number_t number_of_edges = 0;
   for (number_t numArea=1; numArea<=last_area; numArea++) {
      //if (TG_.dimensionOf(TA,numArea) == 1) { // 1D boundary
      vector< pair_nn > EonB = rk_edgesIn(TA,numArea);
      number_of_edges += EonB.size();
      vector< pair_nn >::iterator itEonB;
      for (itEonB=EonB.begin(); itEonB != EonB.end(); itEonB++) {
         BF.push_back(TeXPolygon(*itEonB,numArea+offset,listV_));
         }
      }

//  Sort the faces from the farthest to the nearest

//  1. Compute the vector defining the observation direction
   real_t psird = psi*pi_/180.;
   real_t thetard = theta*pi_/180.;
   real_t ct = std::cos(thetard);
//   TeXPolygon::OD = Vect(ct*cos(psird), ct*sin(psird), sin(thetard));
   TeXPolygon::initObsDir(Vect(ct*std::cos(psird), ct*std::sin(psird), std::sin(thetard)));

//  2. Apply sort algorithm (only for 3D data since in 2D it is unusefull, and moreover
//     it should not be performed for consistency reason)
   bool in3D = listV_[0].geomPt().size() > 2;
   if (in3D) sort(BF.begin(),BF.end());

//  3. Print the sorted faces and edges
   ftex << "% " << number_of_edges << " edges on " << TG_.kindOf(TA) << " " ;
   for (number_t numArea=1; numArea<=last_area; numArea++) {
      //if (TG_.dimensionOf(TA,numArea) == 1) {
         ftex << TG_.nameOf(TA,numArea) << ", ";
      //}
   }
   ftex << endl;
   ftex << "% " << number_of_faces << " faces on " << TG_.kindOf(subdomainArea) << " " ;
   for (number_t numArea=1; numArea<=last_sdom; numArea++) {
      //if (TG_.dimensionOf(subdomainArea,numArea) != 1) {
         ftex << TG_.nameOf(subdomainArea,numArea) << ", ";
      //}
   }
   ftex << endl;
   vector<TeXPolygon>::iterator itBF;
   vector<number_t>::const_iterator itF;
   for (itBF=BF.begin(); itBF != BF.end(); itBF++) {
      if (itBF->attrib() > offset) {
         ftex << "\\drawEdge";
         for (itF=itBF->Vrank().begin(); itF != itBF->Vrank().end(); itF++) {
            ftex << "{" << listV_[*itF].number() << "}";
         }
         ftex << "{\\Color" << string(1,'@'+itBF->attrib()) << "}";
      }
      else {
         ftex << "\\drawFace";
         for (itF=itBF->Vrank().begin(); itF != itBF->Vrank().end(); itF++) {
            ftex << "{" << listV_[*itF].number() << "}";
         }
         if (!in3D || (in3D && itBF->isExtVisible())) {
           ftex << "{\\Color" << string(1,'@'+itBF->attrib()) << "}";
         }
         else {
           ftex << "{\\Color" << inside << "}";
         }
      }
      ftex << endl;
   }
}

} // end of namespace subdivision
} // end of namespace xlifepp
