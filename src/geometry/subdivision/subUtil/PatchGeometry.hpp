/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file PatchGeometry.hpp
  \author Y. Lafranche
  \since 10 Nov 2008
  \date 08 Mar 2014

  \brief definition of the xlifepp::subdivision::PatchGeometry class

  inheriting classes
  ------------------
    - xlifepp::subdivision::DefaultGeometry    (type_ = 0)
        - xlifepp::subdivision::SurfPlane
    - xlifepp::subdivision::SurfRevolution
        - xlifepp::subdivision::SurfCone       (type_ = 1)
        - xlifepp::subdivision::SurfCylinder   (type_ = 2)
        - xlifepp::subdivision::SurfEllipsoid  (type_ = 3)
        - xlifepp::subdivision::SurfSphere     (type_ = 4)
*/

#ifndef PATCH_GEOMETRY_HPP
#define PATCH_GEOMETRY_HPP

#include "PointUtils.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \struct DivideByZero
   Structure used to transmit information in case an exception is thrown.
 */
struct DivideByZero {
   std::string descr;
   std::vector<real_t> val;
   std::vector<Point> VP;
};

/*!
   \class PatchGeometry
   abstract base class that defines the geometrical
   information associated to the patches of the domain.
*/
class PatchGeometry
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   PatchGeometry(const number_t type, const std::string descr)
:type_(type),description_(descr){}

   //! destructor
   virtual ~PatchGeometry(){}
//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! returns the type of shape
   number_t typeOfShape() const {return type_;}

   //! returns true if the shape is non plane, false otherwise
   bool curvedShape() const {return type_ > 0;}

   //! returns the description of the shape
   std::string description() const {return description_;}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! projection onto the boundary of the barycenter of the points in VP with coefficients in coef
   virtual Point projOnBound(const real_t *coef, const std::vector<Point>& VP) const = 0;

   //@{
   /*!
     returns the end points of the axis of a surface of revolution.
     Irrelevant for other surfaces.
    */
   virtual Point EndPt1() const {return Point(0,0,0);}
   virtual Point EndPt2() const {return Point(0,0,0);}
   //@}

protected:
   //! Exception handling
   static DivideByZero DbZ;

   /*!
     type of shape (0 : line or plane, >0 : curved)
     "curved" means that the boundary is a non plane surface upon witch
     we are able to project points
    */
   number_t type_;
   //! description of the shape
   std::string description_;

}; // end of Class PatchGeometry

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* PATCH_GEOMETRY_HPP */
