/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TeXutil.cpp
  \author Y. Lafranche
  \since 16 Oct 2008
  \date 08 Mar 2014

  \brief Implementation of TeX utility extern functions
*/

#include "TeXutil.hpp"

#include <cstring>
using namespace std;

namespace xlifepp {
namespace subdivision {

/*!
 Global substitution.
 This function substitutes each occurrence of substring that in str by the
 string bythat.
 */
void gsubstitute(string& str, const char* that,  const char* bythat) {
   size_t found, lgthat = strlen(that), lgbythat = strlen(bythat);
   found=str.find(that);
   while (found!=string::npos) {
      str.replace(found,lgthat,bythat);
      found=str.find(that,found+lgbythat);
   }
}

/*!
 TeX format.
 This function returns a copy of strorig where some characters have been replaced
 by their equivalent in typewriter font (tt) in order to allow compilation by TeX.
 */
string fmtTeX(const string& strorig) {
   string str(strorig);
   gsubstitute(str,"\\","\\char`\\\\");  // to be done first
   gsubstitute(str,"#","\\#");
   gsubstitute(str,"&","\\&");
   gsubstitute(str,"%","\\%");
   gsubstitute(str,"_","\\_");
   return str;
}

} // end of namespace subdivision
} // end of namespace xlifepp
