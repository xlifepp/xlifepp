/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Quadrangle.cpp
  \author Y. Lafranche
  \since 3 Fev 2014
  \date 3 Fev 2014

  \brief Implementation of xlifepp::subdivision::Quadrangle class members and related functions
*/

#include "Quadrangle.hpp"

#include <algorithm>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Static variables
//-------------------------------------------------------------------------------
// Index array defining the edges according to the following numering convention:
// edge #1=(1,2), edge #2=(2,3), edge #3=(4,3), edge #4=(1,4)
short Quadrangle::rkEdge[/* nb_edges_ */][2] = {{0,1}, {1,2}, {3,2}, {0,3}};

// Index array defining the faces according to the following numering convention:
// face #1=(1,2,3,4)
short Quadrangle::rkFace[/* nb_faces_ */][4] = {{0,1,2,3}};


//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Conversion constructor. Constructor by vertices, given by their rank in the list
 */
Quadrangle::Quadrangle(const number_t num, const number_t rV1, const number_t rV2,
                       const number_t rV3, const number_t rV4, const number_t bdSideNum)
:CartesianFig<Quadrangle>(num,bdSideNum){
   vertices_[0] = rV1;
   vertices_[1] = rV2;
   vertices_[2] = rV3;
   vertices_[3] = rV4;
}


//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
/*!
 returns the numbers (>= 1) of the edges defining the face, i.e. the quadrangle itself
 */
vector<short> Quadrangle::numEdgesOfFace(const number_t i) {
   vector<short> V(nb_edges_);
   for (number_t j=0; j<nb_edges_; j++) { V[j] = j+1; }
   return V;
}
/*!
 returns the ranks (>= 0) of the vertices defining the edges of the quadrangle
 */
vector<pair<short,short> > Quadrangle::rkEdgeVertices() {
   vector<pair<short,short> > V(nb_edges_);
   for (number_t i=0; i<nb_edges_; i++) { V[i] = make_pair(rkEdge[i][0],rkEdge[i][1]); }
   return V;
}

/*!
 This function gives the local position of the vertices of any quadrangle of
 the mesh correspondig to the numbering convention explained on top of the
 Quadrangle class header file (Quadrangle.hpp).

 The numbering convention of high order vertices is deduced from a double tensor
 product of the standard numbering over the segment defined by the integers [0,1,...k].
 Thus, the entry BC[i] in the returned vector BC contains a vector of 2 integers whose
 components BC[i][j] are such that 0<= BC[i][j] <= k, where k is the order of the mesh.
 Each component is the result of a permutation of a 2D tensor product of the uniform
 subdivision of the interval [0,1,...k]. This gives an intrinsic way of numbering and
 localization of the points if these components are sorted in alphanumeric order.
 Nota: this does not match with the numbering of the vertices XLiFE++ uses.

 Moreover, the returned vector gives the location of the vertices according
 to the way they are encountered in the numbering process implemented by the
 functions GeomFigureMesh<T_>::createHOV, GeomFigureMesh<T_>::createHOeV,
 and QuadrangleMesh::createHOiV.
 */
vector< vector<number_t> > Quadrangle::numberingOfVertices(const number_t Order) {
   number_t order = Order;
   if (order < 1) {order = 1;}

   number_t NbVert((order+1)*(order+1));
   vector<number_t> V(2,0);
   vector< vector<number_t> > BC(NbVert,V);

//    main vertices (order 1)
//    #1 -> (0, 0), #4 -> (0, k), #2 -> (k, 0), #3 -> (k, k)
   number_t rkVert[] = {0,3,1,2};
   const number_t ivx(0), ivy(1);
   number_t indBC(0);
   for (number_t ix=0; ix<2; ix++) {
      V[ivx] = ix*order;
      for (number_t iy=0; iy<2; iy++) {
         V[ivy] = iy*order;
            BC[rkVert[indBC++]] = V;
         }
      }

//    edge vertices (cf. GeomFigureMesh<T_>::createHOeV)
//    vertex numbering is decreasing along the edges
//      edge #1=(1,2) along X -> (., 0), edge #2=(2,3) along Y -> (k, .)
//      edge #3=(4,3) along X -> (., k), edge #4=(1,4) along Y -> (0, .)
   for (number_t indEdge=0; indEdge<nb_edges_; indEdge++) {
      V = BC[rkEdge[indEdge][0]];
      number_t iv = indEdge%2;
      for (number_t i1=1; i1<order; i1++) {
         V[iv] = order-i1;
         BC[indBC++] = V;
      }
   }

//    internal vertices (cf. QuadrangleMesh::createHOiV)
//    increasing order along X first then Y
   for (number_t iy=1; iy<order; iy++) {
      V[ivy] = iy;
      for (number_t ix=1; ix<order; ix++) {
         V[ivx] = ix;
         BC[indBC++] = V;
         }
      }
   return BC;
}

} // end of namespace subdivision
} // end of namespace xlifepp
