/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshQuaCube.cpp
  \author Y. Lafranche
  \since 28 Apr 2014
  \date 28 Apr 2014

  \brief Implementation of xlifepp::subdivision::SurfMeshQuaCube class members and related functions
*/

#include "SurfMeshQuaCube.hpp"

#include <sstream>

namespace xlifepp {
namespace subdivision {
using std::map;
using std::pair;
using std::vector;

//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

/*!
 Build a mesh of quadrangles by successive subdivisions from an initial mesh of quadrangles.

 \param rots: rotations to be applied to the cube to get its final position.
   Each rotation, if any, is defined by an angle in degrees and the number of the
   absolute axis (1, 2 or 3) around which the rotation is made.
   Each rotation is applied in turn to the cube starting from the canonical initial
   position where the cube is centered at the origin and its edges are parallel to the axes.

 \param nboctants: number of octants to be filled
   nboctants may take a value in [1, 8].
   For the big cube, the initial mesh may consist of 6 quadrangles instead of
   24 when this big cube is the juxtatposition of 8 small cubes, which reduces
   the number of quadrangles of the final mesh by a factor 4.
   In order to activate this behaviour, nboctants must take the value -8.

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each quadrangle is subdivided into 4 quadrangles with the
   same orientation as the original one: if the first quadrangle is (1,2,3,4), the
   vector 12x13 defines the exterior normal to the face.

 \param order: order of the quadrangles in the final mesh (1 by default)
   The default value is 1, which leads to a Q1 mesh, in which case each
   quadrangle is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh.

 \param edgeLength: edge length of the cube (1. by default)
 \param Center: center of the cube ((0,0,0) by default)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
SurfMeshQuaCube::SurfMeshQuaCube(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                                 const number_t nbsubdiv, const number_t order,
                                 const real_t edgeLength, const Point Center,
                                 const number_t minVertexNum, const number_t minElementNum)
: QuadrangleMesh(nbsubdiv, order, 0, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   initMesh(rots,nboctants,edgeLength,Center,VertexNum,ElementNum);
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
// Private member functions
//-------------------------------------------------------------------------------

/*!
  Create the initial mesh (or "seed" of the mesh) to be subdivided.
  In each octant, the boundary of the cube is a set of 3 quadrangles sharing a vertex.
 */
void SurfMeshQuaCube::initMesh(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                               const real_t edLen, const Point& Center,
                               number_t& VertexNum, number_t& ElementNum){
// Define the 27 characteristic points of the cube near the origin Pt[4] (corners,
// middle of the edges and faces) in order to define the selected octants.
   vector<Point> Pt(cubePoints(edLen, rots, Center));

   DefaultGeometry *DG = new DefaultGeometry();
   // to help numbering: i = index i vector Pt, V[i] = rank in the global list of vertices
   //                0,1,2,3, 4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27
   const int V[] = {-1,0,1,2,-1,3,4,5,6,7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
   // Here, Pt[4] is unused and thus, so is V[4]. As a consequence, ranks of the 26 points used
   // are shifted to take this into account: they are consecutive in the interval [0,25].
   int iPh = 0;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
   number_t mVN = minVertexNum_;
   refnum_t sBx, sBy, sBz, sDOM, sIx, sIy, sIz;
   switch (int(std::abs(float(nboctants)))) {
   default:
//      Whole cube = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   case 0:
      title_ = "Cube - Quadrangle mesh over the cube";
      { // Definition of 1 subdomain (no boundary on the surface of the cube, no interface)
        number_t B_patch[] = {0}, B_dimen[] = {0};
        number_t I_patch[] = {0}, I_dimen[] = {0};
        number_t D_patch[] = {1}, D_dimen[] = {2};
        PatchGeometry *P_geom[] = {DG}; // patch # 1
        TG_ = TopoGeom(                               0,B_patch,B_dimen,
                                                      0,I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);

//       Description of the subdomain patches (# 1)
      TG_.setDescription(++iPh) = "The surface of the cube"; sDOM = TG_.sigma(iPh);

      //              0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27
      const int W[]={-1,-1,-1,-1,-1,-1, 0,-1,-1,-1,-1, 1,-1, 2,-1,-1,-1, 3,-1,-1, 4,-1,-1,-1, 5, 6,-1, 7};
//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(W[ 6]+mVN,sDOM,Pt[ 6]));
      listV_.push_back(Vertex(W[11]+mVN,sDOM,Pt[11]));
      listV_.push_back(Vertex(W[13]+mVN,sDOM,Pt[13]));
      listV_.push_back(Vertex(W[17]+mVN,sDOM,Pt[17]));
      listV_.push_back(Vertex(W[20]+mVN,sDOM,Pt[20]));
      listV_.push_back(Vertex(W[24]+mVN,sDOM,Pt[24]));
      listV_.push_back(Vertex(W[25]+mVN,sDOM,Pt[25]));
      listV_.push_back(Vertex(W[27]+mVN,sDOM,Pt[27]));
      VertexNum += 8;
      listT_.push_back(Quadrangle(++ElementNum,W[27],W[20],W[6],W[17]));
      listT_.push_back(Quadrangle(++ElementNum,W[24],W[25],W[13],W[11]));
      listT_.push_back(Quadrangle(++ElementNum,W[24],W[11],W[6],W[20]));
      listT_.push_back(Quadrangle(++ElementNum,W[27],W[17],W[13],W[25]));
      listT_.push_back(Quadrangle(++ElementNum,W[13],W[17],W[6],W[11]));
      listT_.push_back(Quadrangle(++ElementNum,W[27],W[25],W[24],W[20]));
      }
      break;
//      1 octant (1/8 cube)  = = = = = = = = = = = = = = = = = = = = = = = = =
   case 1:
      title_ = "Cube - Quadrangle mesh over 1 octant X>0, Y>0, Z>0";
      { // Definition of 3 boundaries and 1 subdomain
        number_t B_patch[] = {1,2,3}, B_dimen[] = {1,1,1};
        number_t I_patch[] =     {0}, I_dimen[] =     {0};
        number_t D_patch[] =     {4}, D_dimen[] =     {2};
        PatchGeometry *P_geom[] = {DG,DG,DG, DG}; // patch # 1,2,3, 4
        TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                      0,I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the boundary patches (# 1,2,3)
      TG_.setDescription(++iPh) = "Boundary: edges in YZ plane containing the center"; sBx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane containing the center"; sBy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane containing the center"; sBz = TG_.sigma(iPh);
//       Description of the subdomain patches (# 4)
      TG_.setDescription(++iPh) = "Faces in octant #1"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(V[1]+mVN,    sBy|sBz|sDOM,Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,        sBz|sDOM,Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,sBx    |sBz|sDOM,Pt[3]));
      listV_.push_back(Vertex(V[5]+mVN,    sBy    |sDOM,Pt[5]));
      listV_.push_back(Vertex(V[6]+mVN,            sDOM,Pt[6]));
      listV_.push_back(Vertex(V[7]+mVN,sBx        |sDOM,Pt[7]));
      listV_.push_back(Vertex(V[8]+mVN,sBx|sBy    |sDOM,Pt[8]));
      VertexNum += 7;
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      break;
//      2 octants (1/4 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 2:
      title_ = "Cube - Quadrangle mesh over 2 octants Y>0, Z>0";
      { // Definition of 2 boundaries, 1 interface and 1 subdomain
        number_t B_patch[] =   {1,2}, B_dimen[] =   {1,1};
        number_t I_patch[] =     {4}, I_dimen[] =     {1};
        number_t D_patch[] =     {3}, D_dimen[] =     {2};
        PatchGeometry *P_geom[] = {DG,DG, DG, DG}; // patch # 1,2, 3, 4
        TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                       sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the boundary patches (# 1,2)
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane containing the center"; sBy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane containing the center"; sBz = TG_.sigma(iPh);
//       Description of the subdomain patches (# 3)
      TG_.setDescription(++iPh) = "Faces in octants #1 and 2"; sDOM = TG_.sigma(iPh);

//       Description of the interface patches (# 4)
      TG_.setDescription(++iPh) = "Interface: YZ plane containing the center"; sIx = TG_.sigma(iPh);

      listV_.push_back(Vertex(V[ 1]+mVN,sBy|sBz|sDOM    ,Pt[ 1]));
      listV_.push_back(Vertex(V[ 2]+mVN,    sBz|sDOM    ,Pt[ 2]));
      listV_.push_back(Vertex(V[ 3]+mVN,    sBz|sDOM|sIx,Pt[ 3]));
      listV_.push_back(Vertex(V[ 5]+mVN,sBy    |sDOM    ,Pt[ 5]));
      listV_.push_back(Vertex(V[ 6]+mVN,        sDOM    ,Pt[ 6]));
      listV_.push_back(Vertex(V[ 7]+mVN,        sDOM|sIx,Pt[ 7]));
      listV_.push_back(Vertex(V[ 8]+mVN,sBy    |sDOM|sIx,Pt[ 8]));
      listV_.push_back(Vertex(V[ 9]+mVN,sBy|sBz|sDOM    ,Pt[ 9]));
      listV_.push_back(Vertex(V[10]+mVN,    sBz|sDOM    ,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,        sDOM    ,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,sBy    |sDOM    ,Pt[12]));
      VertexNum += 11;
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[9],V[12],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[11],V[7],V[3]));
      listT_.push_back(Quadrangle(++ElementNum,V[12],V[8],V[7],V[11]));
      break;
//      3 octants (3/8 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 3:
      title_ = "Cube - Quadrangle mesh over 3 octants Z>0, minus the octant X>0, Y<0";
      { // Definition of 3 boundaries, 2 interfaces and 1 subdomain
        number_t B_patch[] = {1,2,3}, B_dimen[] = {1,1,1};
        number_t I_patch[] =   {5,6}, I_dimen[] =   {1,1};
        number_t D_patch[] =     {4}, D_dimen[] =     {2};
        PatchGeometry *P_geom[] = {DG,DG,DG, DG, DG,DG}; // patch # 1,2,3, 4, 5,6
        TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                       sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the boundary patches (# 1,2,3)
      TG_.setDescription(++iPh) = "Boundary: edges in YZ plane containing the center"; sBx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane containing the center"; sBy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane containing the center"; sBz = TG_.sigma(iPh);
//       Description of the subdomain patches (# 4)
      TG_.setDescription(++iPh) = "Faces in octants #1, 2 and 3"; sDOM = TG_.sigma(iPh);
//       Description of the interface patches (# 5,6)
      TG_.setDescription(++iPh) = "Interface: YZ plane containing the center"; sIx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XZ plane containing the center"; sIy = TG_.sigma(iPh);

      listV_.push_back(Vertex(V[ 1]+mVN,    sBy|sBz|sDOM        ,Pt[ 1]));
      listV_.push_back(Vertex(V[ 2]+mVN,        sBz|sDOM        ,Pt[ 2]));
      listV_.push_back(Vertex(V[ 3]+mVN,        sBz|sDOM|sIx    ,Pt[ 3]));
      listV_.push_back(Vertex(V[ 5]+mVN,    sBy    |sDOM        ,Pt[ 5]));
      listV_.push_back(Vertex(V[ 6]+mVN,            sDOM        ,Pt[ 6]));
      listV_.push_back(Vertex(V[ 7]+mVN,            sDOM|sIx    ,Pt[ 7]));
      listV_.push_back(Vertex(V[ 8]+mVN,sBx|sBy    |sDOM|sIx|sIy,Pt[ 8]));
      listV_.push_back(Vertex(V[ 9]+mVN,        sBz|sDOM    |sIy,Pt[ 9]));
      listV_.push_back(Vertex(V[10]+mVN,        sBz|sDOM        ,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,            sDOM        ,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,            sDOM    |sIy,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,            sDOM        ,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,        sBz|sDOM        ,Pt[14]));
      listV_.push_back(Vertex(V[15]+mVN,sBx    |sBz|sDOM        ,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,sBx        |sDOM        ,Pt[16]));
      VertexNum += 15;
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[9],V[12],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[11],V[7],V[3]));
      listT_.push_back(Quadrangle(++ElementNum,V[12],V[8],V[7],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[9],V[14],V[13],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[15],V[16],V[13],V[14]));
      listT_.push_back(Quadrangle(++ElementNum,V[13],V[16],V[8],V[12]));
      break;
//      4 octants (1/2 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 4:
      title_ = "Cube - Quadrangle mesh over 4 octants Z>0";
      { // Definition of 1 boundary, 2 interfaces and 1 subdomain
        number_t B_patch[] =   {1}, B_dimen[] =   {1};
        number_t I_patch[] = {3,4}, I_dimen[] = {1,1};
        number_t D_patch[] =   {2}, D_dimen[] =   {2};
        PatchGeometry *P_geom[] = {DG, DG, DG,DG}; // patch # 1, 2, 3,4
        TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                       sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the boundary patches (# 1)
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane containing the center"; sBz = TG_.sigma(iPh);
//       Description of the subdomain patches (# 2)
      TG_.setDescription(++iPh) = "Faces in octants #1 to 4, in the half-space Z>0"; sDOM = TG_.sigma(iPh);
//       Description of the interface patches (# 3,4)
      TG_.setDescription(++iPh) = "Interface: YZ plane containing the center"; sIx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XZ plane containing the center"; sIy = TG_.sigma(iPh);

      listV_.push_back(Vertex(V[ 1]+mVN,sBz|sDOM    |sIy,Pt[ 1]));
      listV_.push_back(Vertex(V[ 2]+mVN,sBz|sDOM        ,Pt[ 2]));
      listV_.push_back(Vertex(V[ 3]+mVN,sBz|sDOM|sIx    ,Pt[ 3]));
      listV_.push_back(Vertex(V[ 5]+mVN,    sDOM    |sIy,Pt[ 5]));
      listV_.push_back(Vertex(V[ 6]+mVN,    sDOM        ,Pt[ 6]));
      listV_.push_back(Vertex(V[ 7]+mVN,    sDOM|sIx    ,Pt[ 7]));
      listV_.push_back(Vertex(V[ 8]+mVN,    sDOM|sIx|sIy,Pt[ 8]));
      listV_.push_back(Vertex(V[ 9]+mVN,sBz|sDOM    |sIy,Pt[ 9]));
      listV_.push_back(Vertex(V[10]+mVN,sBz|sDOM        ,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,    sDOM        ,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,    sDOM    |sIy,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,    sDOM        ,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,sBz|sDOM        ,Pt[14]));
      listV_.push_back(Vertex(V[15]+mVN,sBz|sDOM|sIx    ,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,    sDOM|sIx    ,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,    sDOM        ,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,sBz|sDOM        ,Pt[18]));
      VertexNum += 17;
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[9],V[12],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[11],V[7],V[3]));
      listT_.push_back(Quadrangle(++ElementNum,V[12],V[8],V[7],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[9],V[14],V[13],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[15],V[16],V[13],V[14]));
      listT_.push_back(Quadrangle(++ElementNum,V[13],V[16],V[8],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[1],V[5],V[17]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[17],V[16],V[15]));
      listT_.push_back(Quadrangle(++ElementNum,V[16],V[17],V[5],V[8]));
      break;
//      5 octants (5/8 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 5:
      title_ = "Cube - Quadrangle mesh over 5 octants, 4 with Z>0, plus the octant X>0, Y>0, Z<0";
      { // Definition of 3 boundaries, 3 interfaces and 1 subdomain
        number_t B_patch[] = {1,2,3}, B_dimen[] = {1,1,1};
        number_t I_patch[] = {5,6,7}, I_dimen[] = {1,1,1};
        number_t D_patch[] =     {4}, D_dimen[] =     {2};
        number_t nbBound = sizeof(B_patch)/sizeof(number_t);
        PatchGeometry *P_geom[] = {DG,DG,DG, DG, DG,DG,DG}; // patch # 1,2,3, 4, 5,6,7
        TG_ = TopoGeom(                         nbBound,B_patch,B_dimen,
                       sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the boundary patches (# 1,2,3)
      TG_.setDescription(++iPh) = "Boundary: edges in YZ plane containing the center"; sBx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane containing the center"; sBy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane containing the center"; sBz = TG_.sigma(iPh);
//       Description of the subdomain patches (# 4)
      TG_.setDescription(++iPh) = "Faces in octants #1 to 5"; sDOM = TG_.sigma(iPh);
//       Description of the interface patches (# 5,6,7)
      TG_.setDescription(++iPh) = "Interface: YZ plane containing the center"; sIx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XZ plane containing the center"; sIy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XY plane containing the center"; sIz = TG_.sigma(iPh);

      listV_.push_back(Vertex(V[ 1]+mVN,    sBy|sBz|sDOM    |sIy|sIz,Pt[ 1]));
      listV_.push_back(Vertex(V[ 2]+mVN,            sDOM        |sIz,Pt[ 2]));
      listV_.push_back(Vertex(V[ 3]+mVN,sBx    |sBz|sDOM|sIx    |sIz,Pt[ 3]));
      listV_.push_back(Vertex(V[ 5]+mVN,            sDOM    |sIy    ,Pt[ 5]));
      listV_.push_back(Vertex(V[ 6]+mVN,            sDOM            ,Pt[ 6]));
      listV_.push_back(Vertex(V[ 7]+mVN,            sDOM|sIx        ,Pt[ 7]));
      listV_.push_back(Vertex(V[ 8]+mVN,            sDOM|sIx|sIy    ,Pt[ 8]));
      listV_.push_back(Vertex(V[ 9]+mVN,        sBz|sDOM    |sIy    ,Pt[ 9]));
      listV_.push_back(Vertex(V[10]+mVN,        sBz|sDOM            ,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,            sDOM            ,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,            sDOM    |sIy    ,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,            sDOM            ,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,        sBz|sDOM            ,Pt[14]));
      listV_.push_back(Vertex(V[15]+mVN,        sBz|sDOM|sIx        ,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,            sDOM|sIx        ,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,            sDOM            ,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,        sBz|sDOM            ,Pt[18]));
      listV_.push_back(Vertex(V[19]+mVN,    sBy    |sDOM            ,Pt[19]));
      listV_.push_back(Vertex(V[20]+mVN,            sDOM            ,Pt[20]));
      listV_.push_back(Vertex(V[21]+mVN,sBx        |sDOM            ,Pt[21]));
      listV_.push_back(Vertex(V[22]+mVN,sBx|sBy    |sDOM            ,Pt[22]));
      VertexNum += 21;
      // Cubes with Z>0
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[9],V[12],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[11],V[7],V[3]));
      listT_.push_back(Quadrangle(++ElementNum,V[12],V[8],V[7],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[9],V[14],V[13],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[15],V[16],V[13],V[14]));
      listT_.push_back(Quadrangle(++ElementNum,V[13],V[16],V[8],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[1],V[5],V[17]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[17],V[16],V[15]));
      listT_.push_back(Quadrangle(++ElementNum,V[16],V[17],V[5],V[8]));
      // Cubes with Z<0
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[20],V[2],V[1]));
      listT_.push_back(Quadrangle(++ElementNum,V[21],V[3],V[2],V[20]));
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[22],V[21],V[20]));
      break;
//      6 octants (3/4 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 6:
      title_ = "Cube - Quadrangle mesh over 6 octants, 4 with Z>0, plus 2 octants Y>0, Z<0";
      { // Definition of 2 boundaries, 3 interfaces and 1 subdomain
        number_t B_patch[] =   {1,2}, B_dimen[] =   {1,1};
        number_t I_patch[] = {4,5,6}, I_dimen[] = {1,1,1};
        number_t D_patch[] =     {3}, D_dimen[] =     {2};
        PatchGeometry *P_geom[] = {DG,DG, DG, DG,DG,DG}; // patch # 1,2, 3, 4,5,6
        TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                       sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane containing the center"; sBy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane containing the center"; sBz = TG_.sigma(iPh);
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Faces in octants #1 to 6"; sDOM = TG_.sigma(iPh);
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane containing the center"; sIx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XZ plane containing the center"; sIy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XY plane containing the center"; sIz = TG_.sigma(iPh);

      listV_.push_back(Vertex(V[ 1]+mVN,sBy|sBz|sDOM    |sIy|sIz,Pt[ 1]));
      listV_.push_back(Vertex(V[ 2]+mVN,        sDOM        |sIz,Pt[ 2]));
      listV_.push_back(Vertex(V[ 3]+mVN,        sDOM|sIx    |sIz,Pt[ 3]));
      listV_.push_back(Vertex(V[ 5]+mVN,        sDOM    |sIy    ,Pt[ 5]));
      listV_.push_back(Vertex(V[ 6]+mVN,        sDOM            ,Pt[ 6]));
      listV_.push_back(Vertex(V[ 7]+mVN,        sDOM|sIx        ,Pt[ 7]));
      listV_.push_back(Vertex(V[ 8]+mVN,        sDOM|sIx|sIy    ,Pt[ 8]));
      listV_.push_back(Vertex(V[ 9]+mVN,sBy|sBz|sDOM    |sIy|sIz,Pt[ 9]));
      listV_.push_back(Vertex(V[10]+mVN,        sDOM        |sIz,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,        sDOM            ,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,        sDOM    |sIy    ,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,        sDOM            ,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,    sBz|sDOM            ,Pt[14]));
      listV_.push_back(Vertex(V[15]+mVN,    sBz|sDOM|sIx        ,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,        sDOM|sIx        ,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,        sDOM            ,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,    sBz|sDOM            ,Pt[18]));
      listV_.push_back(Vertex(V[19]+mVN,sBy    |sDOM            ,Pt[19]));
      listV_.push_back(Vertex(V[20]+mVN,        sDOM            ,Pt[20]));
      listV_.push_back(Vertex(V[21]+mVN,        sDOM|sIx        ,Pt[21]));
      listV_.push_back(Vertex(V[22]+mVN,sBy    |sDOM|sIx        ,Pt[22]));
      listV_.push_back(Vertex(V[23]+mVN,sBy    |sDOM            ,Pt[23]));
      listV_.push_back(Vertex(V[24]+mVN,        sDOM            ,Pt[24]));
      VertexNum += 23;
      // Cubes with Z>0
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[9],V[12],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[11],V[7],V[3]));
      listT_.push_back(Quadrangle(++ElementNum,V[12],V[8],V[7],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[9],V[14],V[13],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[15],V[16],V[13],V[14]));
      listT_.push_back(Quadrangle(++ElementNum,V[13],V[16],V[8],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[1],V[5],V[17]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[17],V[16],V[15]));
      listT_.push_back(Quadrangle(++ElementNum,V[16],V[17],V[5],V[8]));
      // Cubes with Z<0
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[20],V[2],V[1]));
      listT_.push_back(Quadrangle(++ElementNum,V[21],V[3],V[2],V[20]));
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[22],V[21],V[20]));
      listT_.push_back(Quadrangle(++ElementNum,V[24],V[23],V[9],V[10]));
      listT_.push_back(Quadrangle(++ElementNum,V[24],V[10],V[3],V[21]));
      listT_.push_back(Quadrangle(++ElementNum,V[22],V[23],V[24],V[21]));
      break;
//      7 octants (7/8 cube) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 7:
      title_ = "Cube - Quadrangle mesh over 7 octants, 8 octants minus the octant X>0, Y<0, Z<0";
      { // Definition of 3 boundaries, 3 interfaces and 1 subdomain
        number_t B_patch[] = {1,2,3}, B_dimen[] = {1,1,1};
        number_t I_patch[] = {5,6,7}, I_dimen[] = {1,1,1};
        number_t D_patch[] =     {4}, D_dimen[] =     {2};
        number_t nbBound = sizeof(B_patch)/sizeof(number_t);
        PatchGeometry *P_geom[] = {DG,DG,DG, DG, DG,DG,DG}; // patch # 1,2,3, 4, 5,6,7
        TG_ = TopoGeom(                         nbBound,B_patch,B_dimen,
                       sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the boundary patches (# 1,2,3)
      TG_.setDescription(++iPh) = "Boundary: edges in YZ plane containing the center"; sBx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane containing the center"; sBy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane containing the center"; sBz = TG_.sigma(iPh);
//       Description of the subdomain patches (# 4)
      TG_.setDescription(++iPh) = "Faces in octants #1 to 7"; sDOM = TG_.sigma(iPh);
//       Description of the interface patches (# 5,6,7)
      TG_.setDescription(++iPh) = "Interface: YZ plane containing the center"; sIx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XZ plane containing the center"; sIy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XY plane containing the center"; sIz = TG_.sigma(iPh);

      listV_.push_back(Vertex(V[ 1]+mVN,    sBy|sBz|sDOM    |sIy|sIz,Pt[ 1]));
      listV_.push_back(Vertex(V[ 2]+mVN,            sDOM        |sIz,Pt[ 2]));
      listV_.push_back(Vertex(V[ 3]+mVN,            sDOM|sIx    |sIz,Pt[ 3]));
      listV_.push_back(Vertex(V[ 5]+mVN,            sDOM    |sIy    ,Pt[ 5]));
      listV_.push_back(Vertex(V[ 6]+mVN,            sDOM            ,Pt[ 6]));
      listV_.push_back(Vertex(V[ 7]+mVN,            sDOM|sIx        ,Pt[ 7]));
      listV_.push_back(Vertex(V[ 8]+mVN,            sDOM|sIx|sIy    ,Pt[ 8]));
      listV_.push_back(Vertex(V[ 9]+mVN,            sDOM    |sIy|sIz,Pt[ 9]));
      listV_.push_back(Vertex(V[10]+mVN,            sDOM        |sIz,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,            sDOM            ,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,            sDOM    |sIy    ,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,            sDOM            ,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,            sDOM        |sIz,Pt[14]));
      listV_.push_back(Vertex(V[15]+mVN,sBx    |sBz|sDOM|sIx    |sIz,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,            sDOM|sIx        ,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,            sDOM            ,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,        sBz|sDOM            ,Pt[18]));
      listV_.push_back(Vertex(V[19]+mVN,    sBy    |sDOM            ,Pt[19]));
      listV_.push_back(Vertex(V[20]+mVN,            sDOM            ,Pt[20]));
      listV_.push_back(Vertex(V[21]+mVN,            sDOM|sIx        ,Pt[21]));
      listV_.push_back(Vertex(V[22]+mVN,sBx|sBy    |sDOM|sIx|sIy    ,Pt[22]));
      listV_.push_back(Vertex(V[23]+mVN,            sDOM    |sIy    ,Pt[23]));
      listV_.push_back(Vertex(V[24]+mVN,            sDOM            ,Pt[24]));
      listV_.push_back(Vertex(V[25]+mVN,            sDOM            ,Pt[25]));
      listV_.push_back(Vertex(V[26]+mVN,sBx        |sDOM            ,Pt[26]));
      VertexNum += 25;
      // Cubes with Z>0
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[9],V[12],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[11],V[7],V[3]));
      listT_.push_back(Quadrangle(++ElementNum,V[12],V[8],V[7],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[9],V[14],V[13],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[15],V[16],V[13],V[14]));
      listT_.push_back(Quadrangle(++ElementNum,V[13],V[16],V[8],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[1],V[5],V[17]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[17],V[16],V[15]));
      listT_.push_back(Quadrangle(++ElementNum,V[16],V[17],V[5],V[8]));
      // Cubes with Z<0
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[20],V[2],V[1]));
      listT_.push_back(Quadrangle(++ElementNum,V[21],V[3],V[2],V[20]));
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[22],V[21],V[20]));
      listT_.push_back(Quadrangle(++ElementNum,V[24],V[23],V[9],V[10]));
      listT_.push_back(Quadrangle(++ElementNum,V[24],V[10],V[3],V[21]));
      listT_.push_back(Quadrangle(++ElementNum,V[22],V[23],V[24],V[21]));
      listT_.push_back(Quadrangle(++ElementNum,V[23],V[25],V[14],V[9]));
      listT_.push_back(Quadrangle(++ElementNum,V[26],V[15],V[14],V[25]));
      listT_.push_back(Quadrangle(++ElementNum,V[26],V[25],V[23],V[22]));
      break;
//      8 octants (cube) = = = = = = = = = = = = = = = = = = = = = = = = = = =
   case 8:
      title_ = "Cube - Quadrangle mesh over 8 octants";
      { // Definition of 3 interfaces and 1 subdomain (no boundary on the surface of the cube)
        number_t B_patch[] =     {0}, B_dimen[] =     {0};
        number_t I_patch[] = {2,3,4}, I_dimen[] = {1,1,1};
        number_t D_patch[] =     {1}, D_dimen[] =     {2};
        PatchGeometry *P_geom[] = {DG, DG,DG,DG}; // patch # 1, 2,3,4
        TG_ = TopoGeom(                               0,B_patch,B_dimen,
                       sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                       sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
      }
//       Description of the subdomain patches (# 1)
      TG_.setDescription(++iPh) = "The surface of the cube"; sDOM = TG_.sigma(iPh);
//       Description of the interface patches (# 2,3,4)
      TG_.setDescription(++iPh) = "Interface: YZ plane containing the center"; sIx = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XZ plane containing the center"; sIy = TG_.sigma(iPh);
      TG_.setDescription(++iPh) = "Interface: XY plane containing the center"; sIz = TG_.sigma(iPh);

      listV_.push_back(Vertex(V[ 1]+mVN,sDOM    |sIy|sIz,Pt[ 1]));
      listV_.push_back(Vertex(V[ 2]+mVN,sDOM        |sIz,Pt[ 2]));
      listV_.push_back(Vertex(V[ 3]+mVN,sDOM|sIx    |sIz,Pt[ 3]));
      listV_.push_back(Vertex(V[ 5]+mVN,sDOM    |sIy    ,Pt[ 5]));
      listV_.push_back(Vertex(V[ 6]+mVN,sDOM            ,Pt[ 6]));
      listV_.push_back(Vertex(V[ 7]+mVN,sDOM|sIx        ,Pt[ 7]));
      listV_.push_back(Vertex(V[ 8]+mVN,sDOM|sIx|sIy    ,Pt[ 8]));
      listV_.push_back(Vertex(V[ 9]+mVN,sDOM    |sIy|sIz,Pt[ 9]));
      listV_.push_back(Vertex(V[10]+mVN,sDOM        |sIz,Pt[10]));
      listV_.push_back(Vertex(V[11]+mVN,sDOM            ,Pt[11]));
      listV_.push_back(Vertex(V[12]+mVN,sDOM    |sIy    ,Pt[12]));
      listV_.push_back(Vertex(V[13]+mVN,sDOM            ,Pt[13]));
      listV_.push_back(Vertex(V[14]+mVN,sDOM        |sIz,Pt[14]));
      listV_.push_back(Vertex(V[15]+mVN,sDOM|sIx    |sIz,Pt[15]));
      listV_.push_back(Vertex(V[16]+mVN,sDOM|sIx        ,Pt[16]));
      listV_.push_back(Vertex(V[17]+mVN,sDOM            ,Pt[17]));
      listV_.push_back(Vertex(V[18]+mVN,sDOM        |sIz,Pt[18]));
      listV_.push_back(Vertex(V[19]+mVN,sDOM    |sIy    ,Pt[19]));
      listV_.push_back(Vertex(V[20]+mVN,sDOM            ,Pt[20]));
      listV_.push_back(Vertex(V[21]+mVN,sDOM|sIx        ,Pt[21]));
      listV_.push_back(Vertex(V[22]+mVN,sDOM|sIx|sIy    ,Pt[22]));
      listV_.push_back(Vertex(V[23]+mVN,sDOM    |sIy    ,Pt[23]));
      listV_.push_back(Vertex(V[24]+mVN,sDOM            ,Pt[24]));
      listV_.push_back(Vertex(V[25]+mVN,sDOM            ,Pt[25]));
      listV_.push_back(Vertex(V[26]+mVN,sDOM|sIx        ,Pt[26]));
      listV_.push_back(Vertex(V[27]+mVN,sDOM            ,Pt[27]));
      VertexNum += 26;
      // Cubes with Z>0
      listT_.push_back(Quadrangle(++ElementNum,V[1],V[2],V[6],V[5]));
      listT_.push_back(Quadrangle(++ElementNum,V[3],V[7],V[6],V[2]));
      listT_.push_back(Quadrangle(++ElementNum,V[8],V[5],V[6],V[7]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[9],V[12],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[10],V[11],V[7],V[3]));
      listT_.push_back(Quadrangle(++ElementNum,V[12],V[8],V[7],V[11]));
      listT_.push_back(Quadrangle(++ElementNum,V[9],V[14],V[13],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[15],V[16],V[13],V[14]));
      listT_.push_back(Quadrangle(++ElementNum,V[13],V[16],V[8],V[12]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[1],V[5],V[17]));
      listT_.push_back(Quadrangle(++ElementNum,V[18],V[17],V[16],V[15]));
      listT_.push_back(Quadrangle(++ElementNum,V[16],V[17],V[5],V[8]));
      // Cubes with Z<0
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[20],V[2],V[1]));
      listT_.push_back(Quadrangle(++ElementNum,V[21],V[3],V[2],V[20]));
      listT_.push_back(Quadrangle(++ElementNum,V[19],V[22],V[21],V[20]));
      listT_.push_back(Quadrangle(++ElementNum,V[24],V[23],V[9],V[10]));
      listT_.push_back(Quadrangle(++ElementNum,V[24],V[10],V[3],V[21]));
      listT_.push_back(Quadrangle(++ElementNum,V[22],V[23],V[24],V[21]));
      listT_.push_back(Quadrangle(++ElementNum,V[23],V[25],V[14],V[9]));
      listT_.push_back(Quadrangle(++ElementNum,V[26],V[15],V[14],V[25]));
      listT_.push_back(Quadrangle(++ElementNum,V[26],V[25],V[23],V[22]));
      listT_.push_back(Quadrangle(++ElementNum,V[27],V[19],V[1],V[18]));
      listT_.push_back(Quadrangle(++ElementNum,V[27],V[18],V[15],V[26]));
      listT_.push_back(Quadrangle(++ElementNum,V[27],V[26],V[22],V[19]));
      break;
   }
}

} // end of namespace subdivision
} // end of namespace xlifepp
