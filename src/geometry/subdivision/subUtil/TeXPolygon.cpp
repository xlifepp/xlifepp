/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TeXPolygon.cpp
  \author Y. Lafranche
  \since 17 May 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::TeXPolygon class members and related functions
*/

#include "TeXPolygon.hpp"

#include <vector>
using namespace std;

namespace xlifepp {
namespace subdivision {

Vect TeXPolygon::OD; // static data member

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
  //! main constructor
TeXPolygon::TeXPolygon(const vector<number_t>& vrV, const number_t numBoundary,
                       const vector<Vertex>& listV)
: vrV_(vrV), numBoundary_(numBoundary){
//   Store the geometric points associated to the vertices
   for (int i=0; i<3; i++){
      geomPts_.push_back(listV[vrV[i]].geomPt());
   }
}

  //! constructor for edges
TeXPolygon::TeXPolygon(const pair_nn& prV, const number_t numBoundary,
                       const vector<Vertex>& listV)
: numBoundary_(numBoundary){
//   Store the vertex indices
   vrV_.push_back(prV.first);
   vrV_.push_back(prV.second);
//   Store the geometric points associated to the vertices
   for (int i=0; i<2; i++){
      geomPts_.push_back(listV[vrV_[i]].geomPt());
   }
//   Build an artificial face by adding the middle point M of the edge [V1, V2].
//   The resulting face (V1,V2,M) is degenerated into a line.
//   (replacing it by the image of V2 by the rotation of pi/2 around
//    the axis (V1, OD) does not lead to better results)
   real_t coef[] = {1., 1.};
   Point M=barycenter(coef, geomPts_);
   geomPts_.push_back(M);
}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
/*!
 This function returns true if the exterior of the face is visible with respect
 to the observation direction, false otherwise.
 */
bool TeXPolygon::isExtVisible() const{
   Vect a1=toVector(geomPts_[0],geomPts_[1]), a2=toVector(geomPts_[0],geomPts_[2]), normex = crossProduct(a1,a2);
   return dot(OD,normex) > 0;
}

/*!
 Comparison between two boundary faces U and V, U being the current object.
 In short, "U < V" if U is "less" visible than V.
 The function returns true ("U < V") or false according to the following algorithm:
  - if one face is visible and the other hidden, the function returns true if U is
    hidden and V is visible, false otherwise,
  - if both faces are hidden or visible, they are sorted in increasing order
    according to the distance from their isobarycenter to the observation point.
 */
bool TeXPolygon::operator<(const TeXPolygon& V) const{
   const vector<Point> &pU = geomPts_, &pV = V.geomPts_;
   // Normal vector to the face oriented towards the exterior of the tetrahedron
   Vect a1U=toVector(pU[0],pU[1]), a2U=toVector(pU[0],pU[2]), normexU = crossProduct(a1U,a2U);
   Vect a1V=toVector(pV[0],pV[1]), a2V=toVector(pV[0],pV[2]), normexV = crossProduct(a1V,a2V);
   // Orientation of the face
   real_t psU = dot(OD,normexU), psV = dot(OD,normexV);
   // Filter degenerated faces: if a face is degenerated, situation which occurs
   // when it has been built by the "edge constructor", the second branch of the
   // test must be used.
   if (psU*psV < 0 && std::abs(psU) > theTolerance && std::abs(psV) > theTolerance) {
                       // One face is hidden, the other is visible
      return psU < psV;
   }
   else {              // Both faces are hidden or visible
      real_t coef[] = {1., 1., 1.};
      Point Orig(0.,0.,0.);
      Point cgU=barycenter(coef, pU);
      Point cgV=barycenter(coef, pV);
      Vect vU=toVector(Orig,cgU), vV=toVector(Orig,cgV);
      // Computing the abscissa of the cg on the line (Orig, OD) is sufficient
      psU = dot(OD,vU);
      psV = dot(OD,vV);
      return psU < psV;
   }
}

} // end of namespace subdivision
} // end of namespace xlifepp
