/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TopoGeom.cpp
  \author Y. Lafranche
  \since 23 Apr 2009
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::TopoGeom class members and related functions
*/

#include "TopoGeom.hpp"
#include "TeXutil.hpp"

#include <algorithm>
#include <sstream>
#include <climits>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Simple constructor by number of boundaries, interfaces and subdomains.
 nbBound, nbIntrf and nbSbDom are respectively the number of boundaries, interfaces
 and subdomains to be created. Once the object is built by this constructor, they
 each consist of one patch ; they can then be modified by setBoundaries, setInterfaces
 and setSubdomains.
 The default shape of the associated patches is plane.
 The default dimension of the boundaries and interfaces is 2.
 The default dimension of the subdomains is 3.
 Some members have no default values. They can be changed by the public modifiers.
 Nota: TopoGeom(n)  is equivalent to
       TopoGeom(n, B_patch, B_dimen, 0, I_patch, I_dimen, 1, D_patch, D_dimen, P_geom)
       with B_patch = {1,2...,n}, B_dimen = {2,2...,2} (n times),
            I_patch = {},         I_dimen = {},
            D_patch = {n+1},      D_dimen = {3},
            P_geom = {SP,... SP, DG} (n times SP + DG).
 */
TopoGeom::TopoGeom(const number_t nbBound, const number_t nbIntrf, const number_t nbSbDom)
:nbBndryPatch_(nbBound), nbIntrfPatch_(nbIntrf), nbSbDomPatch_(nbSbDom),
 nbPatches_(nbBndryPatch_ + nbIntrfPatch_ + nbSbDomPatch_),
 Bndry_patch_(nbBndryPatch_), Intrf_patch_(nbIntrfPatch_), SbDom_patch_(nbSbDomPatch_) {

//  By default, each boundary, interface and subdomain is assumed to consist of one patch.
//  Boundary and interface patches are plane and are numbered starting from 1.
   SurfPlane *SP = new SurfPlane();
   number_t patchNum = 1;
   for (number_t i=0; i<nbBndryPatch_; i++) {
      Bndry_patch_[i] = patchNum++;
      Ptch_dim_.push_back(2);
      Ptch_geom_.push_back(SP);
   }

//  Patch associated to the interfaces.
   for (number_t i=0; i<nbIntrfPatch_; i++) {
      Intrf_patch_[i] = patchNum++;
      Ptch_dim_.push_back(2);
      Ptch_geom_.push_back(SP);
   }
//  Patch associated to the subdomains.
   DefaultGeometry *DG = new DefaultGeometry();
   for (number_t i=0; i<nbSbDomPatch_; i++) {
      SbDom_patch_[i] = patchNum++;
      Ptch_dim_.push_back(3);
      Ptch_geom_.push_back(DG);
   }

//  Initialization of the other internal tables
   initTables();
}
/*!
 General constructor.
 The nbBound boundaries (respectively the nbIntrf interfaces and the nbSbDom subdomains)
 to be created are defined by the associated patch numbers B_patch (respectively I_patch
 and D_patch) and their dimensions B_dimen (respectively I_dimen and D_dimen).
 The numbers start from 1.
 The nbBound+nbIntrf+nbSbDom patches are defined by their associated shapes P_geom.
 Once the object is built by this constructor, each boundary, interface or subdomain consists
 of one patch ; they can then be modified by setBoundaries, setInterfaces and setSubdomains.

 Caution: Arrays B_patch and B_dimen must have the same size nbBound.
           Arrays I_patch and I_dimen must have the same size nbIntrf.
           Arrays D_patch and D_dimen must have the same size nbSbDom.
           Array P_geom must have nbBound+nbIntrf+nbSbDom elements.
           This is not checked (and cannot be with this prototype).

 To be called as (for example):
  TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
*/
TopoGeom::TopoGeom(const number_t nbBound, const number_t* B_patch, const number_t* B_dimen,
                   const number_t nbIntrf, const number_t* I_patch, const number_t* I_dimen,
                   const number_t nbSbDom, const number_t* D_patch, const number_t* D_dimen,
                   PatchGeometry **const P_geom)
:nbBndryPatch_(nbBound), nbIntrfPatch_(nbIntrf), nbSbDomPatch_(nbSbDom),
 nbPatches_(nbBndryPatch_ + nbIntrfPatch_ + nbSbDomPatch_),
 Bndry_patch_(B_patch,B_patch+nbBndryPatch_), Intrf_patch_(I_patch,I_patch+nbIntrfPatch_),
 SbDom_patch_(D_patch,D_patch+nbSbDomPatch_), Ptch_geom_(P_geom,P_geom+nbPatches_) {

   Ptch_dim_.resize(nbPatches_);
   for (size_t i=0; i<nbBndryPatch_; i++) { Ptch_dim_[Bndry_patch_[i]-1] = B_dimen[i]; }
   for (size_t i=0; i<nbIntrfPatch_; i++) { Ptch_dim_[Intrf_patch_[i]-1] = I_dimen[i]; }
   for (size_t i=0; i<nbSbDomPatch_; i++) { Ptch_dim_[SbDom_patch_[i]-1] = D_dimen[i]; }

//  Initialization of the other internal tables
   initTables();
}

//constructor from std::vector instead of array
TopoGeom::TopoGeom(const vector<number_t>& B_patch, const vector<number_t>& B_dimen,
	                const vector<number_t>& I_patch, const vector<number_t>& I_dimen,
	                const vector<number_t>& D_patch, const vector<number_t>& D_dimen,
	                const vector<PatchGeometry *>& P_geom)
:nbBndryPatch_(B_patch.size()), nbIntrfPatch_(I_patch.size()), nbSbDomPatch_(D_patch.size()),
 nbPatches_(nbBndryPatch_ + nbIntrfPatch_ + nbSbDomPatch_),
 Bndry_patch_(B_patch), Intrf_patch_(I_patch),
 SbDom_patch_(D_patch), Ptch_geom_(P_geom) {

   Ptch_dim_.resize(nbPatches_);
   for (size_t i=0; i<nbBndryPatch_; i++) { Ptch_dim_[Bndry_patch_[i]-1] = B_dimen[i]; }
   for (size_t i=0; i<nbIntrfPatch_; i++) { Ptch_dim_[Intrf_patch_[i]-1] = I_dimen[i]; }
   for (size_t i=0; i<nbSbDomPatch_; i++) { Ptch_dim_[SbDom_patch_[i]-1] = D_dimen[i]; }

	//  Initialization of the other internal tables
	initTables();
}


//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------

/*!
 Returns the kind of area (boundary, interface or subdomain).
 */
string TopoGeom::kindOf(const topologicalArea TA) const{
   switch(TA) {
      default:
      case boundaryArea:
         return string("Boundary");
         break;
      case interfaceArea:
         return string("Interface");
         break;
      case subdomainArea:
         return string("Subdomain");
         break;
   }
   return string("Boundary"); // default
}
/*!
 Returns the number of boundary areas, interface areas or subdomain areas.
 */
number_t TopoGeom::numberOf(const topologicalArea TA) const{
   switch(TA) {
      default:
      case boundaryArea:
         return BndryPGr_.size();
         break;
      case interfaceArea:
         return IntrfPGr_.size();
         break;
      case subdomainArea:
         return SbDomPGr_.size();
         break;
   }
   return BndryPGr_.size(); // default
}
/*!
 Returns boundary mask, interface mask or subdomain mask.
 */
refnum_t TopoGeom::maskOf(const topologicalArea TA) const{
   switch(TA) {
      default:
      case boundaryArea:
         return Bndry_mask_;
         break;
      case interfaceArea:
         return Intrf_mask_;
         break;
      case subdomainArea:
         return SbDom_mask_;
         break;
   }
   return Bndry_mask_; // default
}
/*!
 Returns the dimension associated to the topological area TA with number num,
 num >=1, which can be of kind boundary, interface or subdomain.
 It is the dimension associated to one of its patches ; we take the first one.
 */
number_t TopoGeom::dimensionOf(const topologicalArea TA, const number_t num) const{
   vector<number_t>::const_iterator it;
   switch(TA) {
      default:
      case boundaryArea:
         it    = BndryPGr_.at(num-1).begin();
         break;
      case interfaceArea:
         it    = IntrfPGr_.at(num-1).begin();
         break;
      case subdomainArea:
         it    = SbDomPGr_.at(num-1).begin();
         break;
   }
   return Ptch_dim_[*it-1];
}
/*!
 Returns the reference number (localization code) associated to the topological area
 TA with number num, num >=1, which can be of kind boundary, interface or subdomain.
 */
refnum_t TopoGeom::localCodeOf(const topologicalArea TA, const number_t num) const{
   switch(TA) {
      default:
      case boundaryArea:
         return Bndry_refnum_.at(num-1);
         break;
      case interfaceArea:
         return Intrf_refnum_.at(num-1);
         break;
      case subdomainArea:
         return SbDom_refnum_.at(num-1);
         break;
   }
   return Bndry_refnum_.at(num-1); // default
}
/*!
 Returns the description associated to a boundary, an interface or a subdomain whose
 number is num, num >=1.
 The description is deduced from the patch descriptions.
*/
string TopoGeom::descriptionOf(const topologicalArea TA, const number_t num) const{
   vector<number_t>::const_iterator it;
   vector<number_t>::const_iterator itEnd;
   switch(TA) {
    default:
    case boundaryArea:
       it    = BndryPGr_.at(num-1).begin();
       itEnd = BndryPGr_.at(num-1).end();
       break;
    case interfaceArea:
       it    = IntrfPGr_.at(num-1).begin();
       itEnd = IntrfPGr_.at(num-1).end();
       break;
    case subdomainArea:
       it    = SbDomPGr_.at(num-1).begin();
       itEnd = SbDomPGr_.at(num-1).end();
       break;
   }
   string s(Ptch_description_[*it - 1]);
   for (it++; it != itEnd; it++) {
    s += ", and " + Ptch_description_[*it-1];
   }
   return s;
}

/*!
 Returns the name associated to a boundary, an interface or a subdomain whose
 number is num, num >=1.
 The name is deduced from the patch names.
 */
string TopoGeom::nameOf(const topologicalArea TA, const number_t num) const{
   vector<number_t>::const_iterator it;
   vector<number_t>::const_iterator itEnd;
   switch(TA) {
      default:
      case boundaryArea:
         it    = BndryPGr_.at(num-1).begin();
         itEnd = BndryPGr_.at(num-1).end();
         break;
      case interfaceArea:
         it    = IntrfPGr_.at(num-1).begin();
         itEnd = IntrfPGr_.at(num-1).end();
         break;
      case subdomainArea:
         it    = SbDomPGr_.at(num-1).begin();
         itEnd = SbDomPGr_.at(num-1).end();
         break;
   }
   string s(Ptch_name_[*it - 1]);
   for (it++; it != itEnd; it++) {
      s += "_U_" + Ptch_name_[*it-1];
   }
   return s;
}
/*!
 Returns the user attribute associated to the topological area TA with number num,
 num >=1, which can be of kind boundary, interface or subdomain.
 */
string TopoGeom::getAttribute(const topologicalArea TA, const number_t num) const{
   switch(TA) {
      default:
      case boundaryArea:
         return Bndry_attribute_.at(num-1);
         break;
      case interfaceArea:
         return Intrf_attribute_.at(num-1);
         break;
      case subdomainArea:
         return SbDom_attribute_.at(num-1);
         break;
   }
   return Bndry_attribute_.at(num-1); // default
}
/*!
 Returns a reference to the user attribute associated to the topological area TA
 with number num, num >=1, which can be of kind boundary, interface or subdomain.
 */
string& TopoGeom::setAttribute(const topologicalArea TA, const number_t num) {
   switch(TA) {
      default:
      case boundaryArea:
         return Bndry_attribute_.at(num-1);
         break;
      case interfaceArea:
         return Intrf_attribute_.at(num-1);
         break;
      case subdomainArea:
         return SbDom_attribute_.at(num-1);
         break;
   }
   return Bndry_attribute_.at(num-1); // default
}

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------

/*!
 Prints, on stream os, the information about the boundaries, interfaces and subdomains.
 */
void TopoGeom::print(ostream& os, const bool forTeX) const {
   printTA(os, forTeX, boundaryArea , BndryPGr_);
   printTA(os, forTeX, interfaceArea, IntrfPGr_);
   printTA(os, forTeX, subdomainArea, SbDomPGr_);
}
/*!
 Prints, on stream os, on which patches lies the point whose localization code
 is localcod. If the localization code is zero, the point is sayed to be "Nowhere",
 which is an error.
 */
void TopoGeom::printLoc(ostream& os, const refnum_t localcod) const {
   if (localcod > 0) {
      int i=0;
      refnum_t j=localcod;
      while (j > 0) {
         refnum_t r = j%2;
         if (r > 0) {os << " " << Ptch_name_[i];}
         else {os << "        ";}
         j/=2; i++;
      }
   } else {
      os << " Nowhere";
   }
}

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
/*!
 When this function is called, each patch is associated to a topological area
 and to a shape (PatchGeometry object).
 Initialization of the masks and the internal tables XXXPGr_, XXX_refnum_,
 Ptch_name_ and Curv_Ptch_ (list of curved patches).
 Initialization of the reference numbers, also called localization codes,
 associated to the different patches: Ptch_refnum_[i] = 2^i.
 These values are used to set the localization code of a vertex with the help
 of the access function sigma.
 From the following properties:
  2^i & 2^j = delta_ij
  2^i | 2^j = 2^i + 2^j
 we get a tool for intersection and union set control.
 */
void TopoGeom::initTables() {
   if ( nbPatches_ > CHAR_BIT*sizeof(refnum_t) ) {
      nbBndryPatch_ = nbIntrfPatch_ = nbSbDomPatch_ = nbPatches_ = 0;
      cout << "*** Error in initTables: too many topological patches. Abort." << endl;
      throw -1;
   }
   Ptch_refnum_.resize(nbPatches_);
   Ptch_name_.resize(nbPatches_);
   Ptch_description_.resize(nbPatches_);

//  Initialization of the reference numbers
   Ptch_refnum_[0] = 1;
   for (number_t i=1; i<nbPatches_; i++) {
      Ptch_refnum_[i] = Ptch_refnum_[i-1]*2;
   }

//  Patch name for dimension 1, 2 and 3 :
   string PN[] = {"Kappa_", "Sigma_", "Omega_"};
   size_t INDEX[] = {0,0,0};
//  Initialization of the list of curved patches and names of the patches.
//  Initialization of the mask and the reference numbers of:
   refnum_t sig;
//  1. the boundaries

   Bndry_mask_ = 0;
   for (number_t i=0; i<nbBndryPatch_; i++) {
      size_t indP = Bndry_patch_[i]-1, idimTA = Ptch_dim_[indP]-1;
      sig = Ptch_refnum_[indP];
      stringstream ss; ss << PN[idimTA] << ++INDEX[idimTA]; ss >> Ptch_name_[indP];
      Bndry_mask_ |= sig;
      PatchGeometry* PS = Ptch_geom_[indP];
      if (PS -> curvedShape()) {
         Curv_Ptch_.push_back(make_pair(PS,sig));
      }
   }
//  2. the interfaces

   Intrf_mask_ = 0;
   for (number_t i=0; i<nbIntrfPatch_; i++) {
      size_t indP = Intrf_patch_[i]-1, idimTA = Ptch_dim_[indP]-1;
      sig = Ptch_refnum_[indP];
      stringstream ss; ss << PN[idimTA] << ++INDEX[idimTA]; ss >> Ptch_name_[indP];
      Intrf_mask_ |= sig;
      PatchGeometry* PS = Ptch_geom_[indP];
      if (PS -> curvedShape()) {
         Curv_Ptch_.push_back(make_pair(PS,sig));
      }
   }
//  3. the subdomains

   SbDom_mask_ = 0;
   for (number_t i=0; i<nbSbDomPatch_; i++) {
      size_t indP = SbDom_patch_[i]-1, idimTA = Ptch_dim_[indP]-1;
      sig = Ptch_refnum_[indP];
      stringstream ss; ss << PN[idimTA] << ++INDEX[idimTA]; ss >> Ptch_name_[indP];
      SbDom_mask_ |= sig;
      PatchGeometry* PS = Ptch_geom_[indP];
      if (PS -> curvedShape()) {
         Curv_Ptch_.push_back(make_pair(PS,sig));
      }
   }

   setDefaultGroups();
}
/*!
 Prints, on stream os, the information about topological areas of a given kind.
 */
void TopoGeom::printTA(ostream& os, const bool forTeX, const topologicalArea TA,
                       const vector< vector<number_t> >& Xgroup) const {
   string prefix = forTeX ? "\\" : "" ;
   vector< vector<number_t> >::const_iterator itavv;
   vector<number_t>::const_iterator itav;

   number_t numTA = numberOf(TA);
   string kindTA = kindOf(TA);
   string Area = (numTA>1)?" areas ":" area ";
   os << prefix << " ** " << numTA << " \"" << kindTA << "\"" << Area << ":" << endl;
   number_t numArea;
   for (numArea = 1, itavv=Xgroup.begin(); itavv != Xgroup.end(); numArea++, itavv++) {
      os << prefix << " * " << kindTA << " " << numArea << " made of:" << endl;
      for (itav=itavv->begin(); itav != itavv->end(); itav++) {
         number_t Pa = *itav;
         size_t indP = Pa-1;
         os << prefix << "    Patch number " << Pa << endl;
         string name  = forTeX ? fmtTeX(Ptch_name_[indP]) : Ptch_name_[indP];
         os << prefix << "     Name: " << name << endl;
         os << prefix << "     Dimension: " << Ptch_dim_[indP] << endl;
         string shape = forTeX ? fmtTeX(Ptch_geom_[indP]->description()) : Ptch_geom_[indP]->description();
         os << prefix << "     Geometry: " << shape << endl;
         string descr = forTeX ? fmtTeX(Ptch_description_[indP]) : Ptch_description_[indP];
         os << prefix << "     Description: " << descr << endl;
//         os << prefix << "     Ref Number: " << Ptch_refnum_[indP] << endl;
      }
      string useratt = forTeX ? fmtTeX(getAttribute(TA,numArea)) : getAttribute(TA,numArea);
      os << prefix << "    User attribute: " << useratt << endl;
   }
}
/*!
 Check the Xgroup table according to the number of patches created during construction.
 Returns true if the distribution of the patches among the groups is coherent,
 false otherwise.
 */
bool TopoGeom::checkGroup(const vector< vector<number_t> >& Xgroup,
                          const vector<number_t>& Xpatch) const {
   vector< vector<number_t> >::const_iterator itavv;
   vector<number_t>::const_iterator itav;
   vector<number_t>::iterator itV;

   // Record every patch number used in the object:
   vector<number_t> V(Xpatch);

   // Check that every patch used in the object is present in the group:
   for (itavv=Xgroup.begin(); itavv != Xgroup.end(); itavv++) {
      for (itav=itavv->begin(); itav != itavv->end(); itav++) {
         itV = find(V.begin(),V.end(),*itav);
         if (itV != V.end()) {*itV = 0;} // mark that the patch number is found in V
      }
   }
   // Has a patch been forgotten ?
   for (itav=V.begin(); itav != V.end(); itav++) {
      if (*itav > 0) {
         cout << "*** Error in checkGroup: patch number " << *itav << " is unused." << endl;
         return false; // Yes: error.
      }
   }
   // No: it's OK.
   return true;
}
/*!
 Initialization of the reference numbers (localization codes) of the areas of a
 given kind according to the distribution stored in Xgroup.
 */
void TopoGeom::setAreaRefnum(const vector< vector<number_t> >& Xgroup,
                             vector<refnum_t>& AreaRefnum) {
   vector< vector<number_t> >::const_iterator itavv;
   vector<number_t>::const_iterator itav;
   vector<refnum_t>::iterator itV;

   AreaRefnum.clear();
   AreaRefnum.resize(Xgroup.size());
   for (itV=AreaRefnum.begin(), itavv=Xgroup.begin(); itavv != Xgroup.end(); itavv++,itV++) {
      *itV = 0;
      for (itav=itavv->begin(); itav != itavv->end(); itav++) {
         *itV |= Ptch_refnum_[*itav-1];
      }
   }
}
/*!
 Initialization of the group tables: by default, for a given kind of area, each
 group contains one patch. Thus, initially, there are as many groups as patches.
 */
void TopoGeom::setDefaultGroups() {

   Bndry_attribute_.clear(); Bndry_attribute_.resize(nbBndryPatch_);
   BndryPGr_       .clear(); BndryPGr_       .resize(nbBndryPatch_);
   for (number_t i=0; i<nbBndryPatch_; i++) {
      size_t numP = Bndry_patch_[i];
      BndryPGr_[i].clear();
      BndryPGr_[i].push_back(numP);
   }
   setAreaRefnum(BndryPGr_, Bndry_refnum_);
   if ( ! checkGroup(BndryPGr_,Bndry_patch_) ) {
      cout << "*** Error in initTables: boundary groups incoherent." << endl;
   }

   Intrf_attribute_.clear(); Intrf_attribute_.resize(nbIntrfPatch_);
   IntrfPGr_       .clear(); IntrfPGr_       .resize(nbIntrfPatch_);
   for (number_t i=0; i<nbIntrfPatch_; i++) {
      size_t numP = Intrf_patch_[i];
      IntrfPGr_[i].clear();
      IntrfPGr_[i].push_back(numP);
   }
   setAreaRefnum(IntrfPGr_, Intrf_refnum_);
   if ( ! checkGroup(IntrfPGr_,Intrf_patch_) ) {
      cout << "*** Error in initTables: interface groups incoherent." << endl;
   }

   SbDom_attribute_.clear(); SbDom_attribute_.resize(nbSbDomPatch_);
   SbDomPGr_       .clear(); SbDomPGr_       .resize(nbSbDomPatch_);
   for (number_t i=0; i<nbSbDomPatch_; i++) {
      size_t numP = SbDom_patch_[i];
      SbDomPGr_[i].clear();
      SbDomPGr_[i].push_back(numP);
   }
   setAreaRefnum(SbDomPGr_, SbDom_refnum_);
   if ( ! checkGroup(SbDomPGr_,SbDom_patch_) ) {
      cout << "*** Error in initTables: subdomain groups incoherent." << endl;
   }
}
/*!
 Re-initialization of the group table according to the user's definition Ugroup.
 Returns true if the new definition is coherent, false otherwise.
 */
bool TopoGeom::setUserGroups(const number_t index, const vector< vector<number_t> >& Ugroup) {
   bool OK;
   switch (index) {
   default:
   case boundaryArea:
      if ( (OK = checkGroup(Ugroup,Bndry_patch_)) ) {
         BndryPGr_ = Ugroup;
         setAreaRefnum(BndryPGr_, Bndry_refnum_);
      }
      else {
         cout << "*** Error in setUserGroups called for boundaries. " << endl;
         cout << "    Redefinition of groups arrangement failed. No change was made." << endl;
      }
      break;
   case interfaceArea:
      if ( (OK = checkGroup(Ugroup,Intrf_patch_)) ) {
         IntrfPGr_ = Ugroup;
         setAreaRefnum(IntrfPGr_, Intrf_refnum_);
      }
      else {
         cout << "*** Error in setUserGroups called for interfaces. " << endl;
         cout << "    Redefinition of groups arrangement failed. No change was made." << endl;
      }
      break;
   case subdomainArea:
      if ( (OK = checkGroup(Ugroup,SbDom_patch_)) ) {
         SbDomPGr_ = Ugroup;
         setAreaRefnum(SbDomPGr_, SbDom_refnum_);
      }
      else {
         cout << "*** Error in setUserGroups called for subdomains. " << endl;
         cout << "    Redefinition of groups arrangement failed. No change was made." << endl;
      }
      break;
   }
   return OK;
}

} // end of namespace subdivision
} // end of namespace xlifepp
