/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshTriSphere.cpp
  \author Y. Lafranche
  \since 19 Dec 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::SurfMeshTriSphere class members and related functions
*/

#include "SurfMeshTriSphere.hpp"

#include <vector>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of triangles by successive subdivisions over the surface of a sphere.

 \param rots: rotations to be applied to the sphere to get its final position.
   Each rotation, if any, is defined by an angle in degrees and the number of the
   absolute axis (1, 2 or 3) around which the rotation is made.
   Each rotation is applied in turn to the sphere starting from the canonical initial
   position where the sphere is centered at the origin and its local axes are parallel
   to the absolute axes.

 \param nboctants: number of octants to be filled
   nboctants may take a value in [0, 8].
   If nboctants=0, a mesh of the whole sphere is computed starting from an icosahedron,
   which leads to a less structured mesh than the one obtained with nboctants=8.
   For the half-sphere (4 octants) and the sphere (8 octants), when a spherical
   subdivision is requested (argument type different from 0, see below), the initial
   mesh may consist of one single triangle instead of respectively 4 and 8 triangles,
   which reduces the number of triangles of the final mesh by a factor 4 and 8 respectively.
   In order to activate this behaviour, nboctants must take the value -4 and -8
   respectively.

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each triangle is subdivided into 4 triangles with the
   same orientation as the original one: if the first triangle is (1,2,3), the
   vectors 12x13 define the exterior normal to the face.

 \param order: order of the triangles in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   triangle is defined by its 3 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, these vertices
   lie on the sphere. See below the two possible ways to compute these vertices.

 \param type: type of the subdivision (1 by default)
   . if type = 0, the algorithm leads to a simple (flat) subdivision of the initial
   mesh where new vertices are all the midpoints of the edges.
   . if type = 1, the algorithm leads to a subdivision where the vertices belonging to
   a prescribed boundary are computed so that they lie on the sphere ; they are computed
   as the radial projections of the vertices of the Lagrange mesh of order k over the
   triangular chord face.

 \param radius: radius of the sphere (1. by default)
 \param Center: center of the sphere ((0,0,0) by default)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
SurfMeshTriSphere::SurfMeshTriSphere(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                                     const number_t nbsubdiv, const number_t order, const number_t type,
                                     const real_t radius, const Point Center,
                                     const number_t minVertexNum, const number_t minElementNum)
: TriangleMesh(nbsubdiv, order, type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   initMesh(rots,nboctants,radius,Center,VertexNum,ElementNum);
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
/*!
 Create the initial mesh (or "seed" of the mesh) to be subdivided.
 */
void SurfMeshTriSphere::initMesh(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                                 const real_t radius, const Point& Center,
                                 number_t& VertexNum, number_t& ElementNum) {
   vector<Point> Pt;
    Pt.push_back(Point(0,0,0));      // unused (to help numbering)
    Pt.push_back(Point(radius,0,0)); // Pt[1]
    Pt.push_back(Point(0,radius,0)); // Pt[2]
    Pt.push_back(Point(0,0,radius)); // Pt[3]
    Pt.push_back(Point(-radius,0,0));// Pt[4]
    Pt.push_back(Point(0,-radius,0));// Pt[5]
    Pt.push_back(Point(0,0,-radius));// Pt[6]
   // Rotate the points starting from their initial position, then translate them to their final position.
   Vect OC=toVector(Point(0,0,0),Center);
   rotNtrans(rots, OC, Pt);

   DefaultGeometry *DG = new DefaultGeometry();
   SurfSphere *SS = new SurfSphere(Center,radius);
   const int V[] = {-1,0,1,2,3,4,5}; // to help numbering
   int iPh = 0;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
   number_t mVN = minVertexNum_, sDOM;
   switch (int(std::abs(float(nboctants)))) {
   default:
//      Whole sphere = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   case 0:
      title_ = "Sphere - Triangle mesh over the sphere";
      {
        vector<Point> Pt; // Points defining an icosahedron
         real_t phi = (1+std::sqrt(5.))/2, bb = radius/std::sqrt(2+phi), aa=phi*bb;
         Pt.push_back(Point(0,0,0));     // unused (to help numbering)
         Pt.push_back(Point( aa,-bb,0)); // Pt[ 1]
         Pt.push_back(Point( aa, bb,0)); // Pt[ 2]
         Pt.push_back(Point(-aa, bb,0)); // Pt[ 3]
         Pt.push_back(Point(-aa,-bb,0)); // Pt[ 4]
         Pt.push_back(Point(0, aa,-bb)); // Pt[ 5]
         Pt.push_back(Point(0, aa, bb)); // Pt[ 6]
         Pt.push_back(Point(0,-aa, bb)); // Pt[ 7]
         Pt.push_back(Point(0,-aa,-bb)); // Pt[ 8]
         Pt.push_back(Point(-bb,0, aa)); // Pt[ 9]
         Pt.push_back(Point( bb,0, aa)); // Pt[10]
         Pt.push_back(Point( bb,0,-aa)); // Pt[11]
         Pt.push_back(Point(-bb,0,-aa)); // Pt[12]
        // Rotate the points starting from their initial position, then translate them to their final position.
        Vect OC=toVector(Point(0,0,0),Center);
        rotNtrans(rots, OC, Pt);

       // Definition of 1 subdomain (no boundary on the surface of the sphere, no interface)
        number_t B_patch[] = {0}, B_dimen[] = {0};
        number_t I_patch[] = {0}, I_dimen[] = {0};
        number_t D_patch[] = {1}, D_dimen[] = {2};
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {DG}; // patch # 1
         TG_ = TopoGeom(                               0,B_patch,B_dimen,
                                                       0,I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         PatchGeometry *P_geom[] = {SS}; // patch # 1
         TG_ = TopoGeom(                               0,B_patch,B_dimen,
                                                       0,I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }

//       Description of the subdomain patches (# 1)
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Surface of the icosahedron";
      }
      else {
         TG_.setDescription(++iPh) = "The surface of the sphere";
      }
      sDOM = TG_.sigma(iPh);

      //              0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12
      const int W[]={-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11}; // to help numbering
//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(W[ 1]+mVN,sDOM,Pt[ 1]));
      listV_.push_back(Vertex(W[ 2]+mVN,sDOM,Pt[ 2]));
      listV_.push_back(Vertex(W[ 3]+mVN,sDOM,Pt[ 3]));
      listV_.push_back(Vertex(W[ 4]+mVN,sDOM,Pt[ 4]));
      listV_.push_back(Vertex(W[ 5]+mVN,sDOM,Pt[ 5]));
      listV_.push_back(Vertex(W[ 6]+mVN,sDOM,Pt[ 6]));
      listV_.push_back(Vertex(W[ 7]+mVN,sDOM,Pt[ 7]));
      listV_.push_back(Vertex(W[ 8]+mVN,sDOM,Pt[ 8]));
      listV_.push_back(Vertex(W[ 9]+mVN,sDOM,Pt[ 9]));
      listV_.push_back(Vertex(W[10]+mVN,sDOM,Pt[10]));
      listV_.push_back(Vertex(W[11]+mVN,sDOM,Pt[11]));
      listV_.push_back(Vertex(W[12]+mVN,sDOM,Pt[12]));
      VertexNum += 12;
      listT_.push_back(Triangle(++ElementNum,W[1],W[2],W[10]));
      listT_.push_back(Triangle(++ElementNum,W[2],W[1],W[11]));
      listT_.push_back(Triangle(++ElementNum,W[3],W[4],W[9]));
      listT_.push_back(Triangle(++ElementNum,W[4],W[3],W[12]));

      listT_.push_back(Triangle(++ElementNum,W[5],W[6],W[2]));
      listT_.push_back(Triangle(++ElementNum,W[6],W[5],W[3]));
      listT_.push_back(Triangle(++ElementNum,W[7],W[8],W[1]));
      listT_.push_back(Triangle(++ElementNum,W[8],W[7],W[4]));

      listT_.push_back(Triangle(++ElementNum,W[9],W[10],W[6]));
      listT_.push_back(Triangle(++ElementNum,W[10],W[9],W[7]));
      listT_.push_back(Triangle(++ElementNum,W[11],W[12],W[5]));
      listT_.push_back(Triangle(++ElementNum,W[12],W[11],W[8]));

      listT_.push_back(Triangle(++ElementNum,W[1],W[10],W[7]));
      listT_.push_back(Triangle(++ElementNum,W[2],W[6],W[10]));
      listT_.push_back(Triangle(++ElementNum,W[3],W[9],W[6]));
      listT_.push_back(Triangle(++ElementNum,W[4],W[7],W[9]));

      listT_.push_back(Triangle(++ElementNum,W[1],W[8],W[11]));
      listT_.push_back(Triangle(++ElementNum,W[2],W[11],W[5]));
      listT_.push_back(Triangle(++ElementNum,W[3],W[5],W[12]));
      listT_.push_back(Triangle(++ElementNum,W[4],W[12],W[8]));
      }
      break;
//      1 octant (1/8 sphere)  = = = = = = = = = = = = = = = = = = = = = = = = =
   case 1:
      title_ = "Sphere - Triangle mesh over 1 octant X>0, Y>0, Z>0";
      { // Definition of 3 boundaries and 1 subdomain
        number_t B_patch[] = {1,2,3}, B_dimen[] = {1,1,1};
        number_t I_patch[] =     {0}, I_dimen[] =     {0};
        number_t D_patch[] =     {4}, D_dimen[] =     {2};
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {DG,DG,DG, DG}; // patch # 1,2,3, 4
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                       0,I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         PatchGeometry *P_geom[] = {DG,DG,DG, SS}; // patch # 1,2,3, 4
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                       0,I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
      }
//       Description of the boundary patches (# 1,2,3)
      TG_.setDescription(++iPh) = "Boundary: edge in YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: edge in XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: edge in XY plane, opposite to vertex 3";
//       Description of the subdomain patches (# 4)
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Face opposite to the origin";
      }
      else {
         //  portion of the sphere delimited by the 3 previous planes.
         TG_.setDescription(++iPh) = "1 octant of the sphere, defined by X>0, Y>0, Z>0,\n"
               "                   centered at the origin";
      }
      sDOM = TG_.sigma(iPh);
//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|sDOM,Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(3)|TG_.sigma(1)|sDOM,Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|sDOM,Pt[3]));
      VertexNum += 3;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      break;
//      2 octants (1/4 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 2:
      title_ = "Sphere - Triangle mesh over 2 octants Y>0, Z>0";
      { // Definition of 2 boundaries, 1 interface and 1 subdomain
        number_t B_patch[] =   {1,2}, B_dimen[] =   {1,1};
        number_t I_patch[] =     {4}, I_dimen[] =     {1};
        number_t D_patch[] =     {3}, D_dimen[] =     {2};
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {DG,DG, DG, DG}; // patch # 1,2, 3, 4
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         PatchGeometry *P_geom[] = {DG,DG, SS, DG}; // patch # 1,2, 3, 4
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
      }
//       Description of the boundary patches (# 1,2)
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane";
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane";
//       Description of the subdomain patches (# 3)
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "The 2 faces opposite to the origin";
      }
      else {
         TG_.setDescription(++iPh) = "2 octants of the sphere, defined by Y>0, Z>0,\n"
               "                   centered at the origin";
      }
//       Description of the interface patches (# 4)
      TG_.setDescription(++iPh) = "Interface: YZ plane, joining vertices 2 and 3";

      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)             ,Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,             TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4),Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)             |TG_.sigma(3)|TG_.sigma(4),Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)             ,Pt[4]));
      VertexNum += 4;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      break;
//      3 octants (3/8 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 3:
      title_ = "Sphere - Triangle mesh over 3 octants Z>0, minus the octant X>0, Y<0";
      { // Definition of 3 boundaries, 2 interfaces and 1 subdomain
        number_t B_patch[] = {1,2,3}, B_dimen[] = {1,1,1};
        number_t I_patch[] =   {5,6}, I_dimen[] =   {1,1};
        number_t D_patch[] =     {4}, D_dimen[] =     {2};
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {DG,DG,DG, DG, DG,DG}; // patch # 1,2,3, 4, 5,6
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         PatchGeometry *P_geom[] = {DG,DG,DG, SS, DG,DG}; // patch # 1,2,3, 4, 5,6
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
      }
//       Description of the boundary patches (# 1,2,3)
      TG_.setDescription(++iPh) = "Boundary: edges in YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane, opposite to vertex 3";
//       Description of the subdomain patches (# 4)
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "The 3 faces opposite to the origin";
      }
      else {
         TG_.setDescription(++iPh) = "3 octants of the sphere defined by the half-sphere Z>0 minus\n"
               "                   the octant X>0, Y<0, centered at the origin";
      }
//       Description of the interface patches (# 5,6)
      TG_.setDescription(++iPh) = "Interface: YZ plane, joining vertices 2 and 3";
      TG_.setDescription(++iPh) = "Interface: XZ plane, joining vertices 4 and 3";

      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)                          ,Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,             TG_.sigma(3)|TG_.sigma(4)|TG_.sigma(5)             ,Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6),Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,             TG_.sigma(3)|TG_.sigma(4)             |TG_.sigma(6),Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|TG_.sigma(3)|TG_.sigma(4)                          ,Pt[5]));
      VertexNum += 5;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[5],V[3]));
      break;
//      4 octants (1/2 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 4:
      title_ = "Sphere - Triangle mesh over 4 octants Z>0";
      if (type_ == 0 || nboctants > 0) {
      { // Definition of 1 boundary, 2 interfaces and 1 subdomain
        number_t B_patch[] =   {1}, B_dimen[] =   {1};
        number_t I_patch[] = {3,4}, I_dimen[] = {1,1};
        number_t D_patch[] =   {2}, D_dimen[] =   {2};
        if (type_ == 0) {
            PatchGeometry *P_geom[] = {DG, DG, DG,DG}; // patch # 1, 2, 3,4
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
            PatchGeometry *P_geom[] = {DG, SS, DG,DG}; // patch # 1, 2, 3,4
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
      }
//       Description of the boundary patches (# 1)
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane";
//       Description of the subdomain patches (# 2)
      if (type_ == 0) {
        TG_.setDescription(++iPh) = "The 4 faces opposite to the origin in the half-space Z>0";
      }
      else {
        TG_.setDescription(++iPh) = "4 octants of the sphere (half-sphere), defined by Z>0";
      }
//       Description of the interface patches (# 3,4)
         TG_.setDescription(++iPh) = "Interface: YZ plane, joining vertices 2,3,5";
         TG_.setDescription(++iPh) = "Interface: XZ plane, joining vertices 4,3,1";

      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)|TG_.sigma(2)            |TG_.sigma(4),Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)            ,Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,            TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4),Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)            |TG_.sigma(4),Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)            ,Pt[5]));
      VertexNum += 5;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[5],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[5],V[1],V[3]));
      }
      else {
           // With 3 initial triangles, 2 patches are not sufficient: we need 4 patches.
         { // Definition of 1 boundary (in the plane) and 3 subdomains
           number_t B_patch[] =     {4}, B_dimen[] =      {1};
           number_t I_patch[] =     {0}, I_dimen[] =      {0};
           number_t D_patch[] = {1,2,3}, D_dimen[] = {2,2,2};
           PatchGeometry *P_geom[] = {SS,SS,SS, DG}; // patch # 1,2,3, 4
           number_t nbSbDom = sizeof(D_patch)/sizeof(number_t);
           TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                         0,I_patch,I_dimen,
                                                   nbSbDom,D_patch,D_dimen, P_geom);
//          Make only one domain
            vector< vector<number_t> > VSG;
            VSG.push_back( vector<number_t>(D_patch,D_patch+nbSbDom) );
            if (! TG_.setSubdomains(VSG)) {
               cout << "*** Error in SurfMeshTriSphere::initMesh while making subdomains." << endl;
            }
          }
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Third of the half-sphere opposite to vertex 1";
      TG_.setDescription(++iPh) = "Third of the half-sphere opposite to vertex 2";
      TG_.setDescription(++iPh) = "Third of the half-sphere opposite to vertex 3";
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: edge in XY plane, opposite to vertex 4";
      real_t ang = 2.*pi_/3., rc = radius*std::cos(ang), rs = radius*std::sin(ang);
      vector<Point> Pti;
      Pti.push_back(Point(rc, rs,0));
      Pti.push_back(Point(rc,-rs,0));
      rotNtrans(rots, OC, Pti);
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4),Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(3)|TG_.sigma(1)|TG_.sigma(4),Pti[0]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4),Pti[1]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3),Pt[3]));
      VertexNum += 4;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[4]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[3],V[4]));
      listT_.push_back(Triangle(++ElementNum,V[3],V[1],V[4]));
      }
      break;
//      5 octants (5/8 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 5:
      title_ = "Sphere - Triangle mesh over 5 octants, 4 with Z>0, plus the octant X>0, Y>0, Z<0";
      { // Definition of 5 boundaries, 3 interfaces and 1 subdomain
 // A REVOIR ............. Supprimer 2 boundaries dans le plan XY
        number_t B_patch[] = {1,2,3,4,5}, B_dimen[] = {1,1,1,1,1};
        number_t I_patch[] =     {7,8,9}, I_dimen[] =     {1,1,1};
        number_t D_patch[] =         {6}, D_dimen[] =         {2};
        number_t nbBound = sizeof(B_patch)/sizeof(number_t);
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {DG,DG,DG,DG,DG, DG, DG,DG,DG}; // patch # 1,2,3,4,5, 6, 7,8,9
         TG_ = TopoGeom(                         nbBound,B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         PatchGeometry *P_geom[] = {DG,DG,DG,DG,DG, SS, DG,DG,DG}; // patch # 1,2,3,4,5, 6, 7,8,9
         TG_ = TopoGeom(                         nbBound,B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
//       Make only one boundary in the XY plane
        vector< vector<number_t> > VSG;
        VSG.push_back( vector<number_t>(B_patch+2,B_patch+nbBound) );
        VSG.push_back( vector<number_t>(B_patch+0,B_patch+1) );
        VSG.push_back( vector<number_t>(B_patch+1,B_patch+2) );
        if (! TG_.setBoundaries(VSG)) {
            cout << "*** Error in SurfMeshTriSphere::initMesh while making boundaries." << endl;
        }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: edge in YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: edge in XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: edge in XY plane with X<0 and Y>0, opposite to vertex 3";
      TG_.setDescription(++iPh) = "Boundary: edge in XY plane with X<0 and Y<0, opposite to vertex 3";
      TG_.setDescription(++iPh) = "Boundary: edge in XY plane with X>0 and Y<0, opposite to vertex 3";
//       Description of the subdomain patches
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "The 5 faces opposite to the origin";
      }
      else {
         TG_.setDescription(++iPh) = "5 octants of the sphere defined by the half-sphere Z>0 plus\n"
               "                     the octant X>0, Y>0, Z<0, centered at the origin";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane, joining vertices 2,3,5";
      TG_.setDescription(++iPh) = "Interface: XZ plane, joining vertices 4,3,1";
      TG_.setDescription(++iPh) = "Interface: XY plane, joining vertices 1,2";

      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(5)|TG_.sigma(6)            |TG_.sigma(8)|TG_.sigma(9),Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|TG_.sigma(3)|TG_.sigma(6)|TG_.sigma(7)            |TG_.sigma(9),Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,                          TG_.sigma(6)|TG_.sigma(7)|TG_.sigma(8)            ,Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(3)|TG_.sigma(4)|TG_.sigma(6)             |TG_.sigma(8)            ,Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)|TG_.sigma(7)                         ,Pt[5]));
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)                                      ,Pt[6]));
      VertexNum += 6;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[5],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[5],V[1],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[1],V[6],V[2]));
      break;
//      6 octants (3/4 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 6:
      title_ = "Sphere - Triangle mesh over 6 octants, 4 with Z>0, plus 2 octants Y>0, Z<0";
      { // Definition of 2 boundaries, 3 interfaces and 1 subdomain
        number_t B_patch[] =   {1,2}, B_dimen[] =   {1,1};
        number_t I_patch[] = {4,5,6}, I_dimen[] = {1,1,1};
        number_t D_patch[] =     {3}, D_dimen[] =     {2};
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {DG,DG, DG, DG,DG,DG}; // patch # 1,2, 3, 4,5,6
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         PatchGeometry *P_geom[] = {DG,DG, SS, DG,DG,DG}; // patch # 1,2, 3, 4,5,6
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane";
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane";
//       Description of the subdomain patches
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "The 6 faces opposite to the origin";
      }
      else {
         TG_.setDescription(++iPh) = "6 octants of the sphere defined by the half-sphere Z>0 plus\n"
               "                   the 2 octants Y>0, Z<0, centered at the origin";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane, joining vertices 2,3,5,6";
      TG_.setDescription(++iPh) = "Interface: XZ plane, joining vertices 4,3,1";
      TG_.setDescription(++iPh) = "Interface: XY plane, joining vertices 1,2,4";

      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)             |TG_.sigma(5)|TG_.sigma(6),Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,                          TG_.sigma(3)|TG_.sigma(4)             |TG_.sigma(6),Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,                          TG_.sigma(3)|TG_.sigma(4)|TG_.sigma(5)             ,Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)             |TG_.sigma(5)|TG_.sigma(6),Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,             TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)                          ,Pt[5]));
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)             |TG_.sigma(3)|TG_.sigma(4)                          ,Pt[6]));
      VertexNum += 6;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[5],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[5],V[1],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[1],V[6],V[2]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[6],V[4]));
      break;
//      7 octants (7/8 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 7:
      title_ = "Sphere - Triangle mesh over 7 octants, 8 octants minus the octant X>0, Y<0, Z<0";
      { // Definition of 3 boundaries, 6 interfaces and 1 subdomain
        number_t B_patch[] =        {1,2,3}, B_dimen[] =       {1,1,1};
        number_t I_patch[] = {5,6,7,8,9,10}, I_dimen[] = {1,1,1,1,1,1};
        number_t D_patch[] =            {4}, D_dimen[] =           {2};
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {DG,DG,DG, DG, DG,DG,DG,DG,DG,DG}; // patch # 1,2,3, 4, 5,6,7,8,9,10
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         PatchGeometry *P_geom[] = {DG,DG,DG, SS, DG,DG,DG,DG,DG,DG}; // patch # 1,2,3, 4, 5,6,7,8,9,10
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                        sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: edges in YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: edges in XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: edges in XY plane, opposite to vertex 3";
//       Description of the subdomain patches
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "The 7 faces opposite to the origin";
      }
      else {
         TG_.setDescription(++iPh) = "The sphere minus the octant X>0, Y<0, Z<0 centered at the origin";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane, Z>=0, joining vertices 2,3,5";    // patch 5
      TG_.setDescription(++iPh) = "Interface: XZ plane, Z>=0, joining vertices 1,3,4";    // patch 6
      TG_.setDescription(++iPh) = "Interface: XY plane, Y>=0, joining vertices 1,2,4";    // patch 7
      TG_.setDescription(++iPh) = "Interface: XY plane, Y<=0, joining vertices 4,5";      // patch 8
      TG_.setDescription(++iPh) = "Interface: YZ plane, Z<=0, joining vertices 2,6";      // patch 9
      TG_.setDescription(++iPh) = "Interface: XZ plane, Z<=0, joining vertices 4,6";      // patch 10

      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)|TG_.sigma(6)|TG_.sigma(7),Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,                          TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(7)|TG_.sigma(9),Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,                          TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6),Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,                          TG_.sigma(4)|TG_.sigma(6)|TG_.sigma(7)|TG_.sigma(8)|TG_.sigma(10),Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|TG_.sigma(3)|TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(8),Pt[5]));
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4)|TG_.sigma(9)|TG_.sigma(10),Pt[6]));
      VertexNum += 6;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[5],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[5],V[1],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[1],V[6],V[2]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[6],V[4]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[6],V[5]));
      break;
//      8 octants (sphere) = = = = = = = = = = = = = = = = = = = = = = = = = = =
   case 8:
      title_ = "Sphere - Triangle mesh over 8 octants";
      if (type_ == 0 || nboctants > 0) {
      { // Definition of 3 interfaces and 1 subdomain (no boundary on the sphere)
        number_t B_patch[] =     {0}, B_dimen[] =     {0};
        number_t I_patch[] = {2,3,4}, I_dimen[] = {1,1,1};
        number_t D_patch[] =     {1}, D_dimen[] =     {2};
        if (type_ == 0) {
           PatchGeometry *P_geom[] = {DG, DG,DG,DG}; // patch # 1, 2,3,4
           TG_ = TopoGeom(                               0,B_patch,B_dimen,
                          sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                          sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
           }
        else {
           PatchGeometry *P_geom[] = {SS, DG,DG,DG}; // patch # 1, 2,3,4
           TG_ = TopoGeom(                               0,B_patch,B_dimen,
                          sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                          sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
       }
//       Description of the subdomain patches
      if (type_ == 0) {
        TG_.setDescription(++iPh) = "The 8 faces opposite to the origin";
      }
      else {
        TG_.setDescription(++iPh) = "The sphere centered at the origin";
      }
//       Description of the interface patches
         TG_.setDescription(++iPh) = "Interface: YZ plane, joining vertices 2,3,5,6";
         TG_.setDescription(++iPh) = "Interface: XZ plane, joining vertices 4,3,1,6";
         TG_.setDescription(++iPh) = "Interface: XY plane, joining vertices 1,2,4,5";

      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)             |TG_.sigma(3)|TG_.sigma(4),Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|TG_.sigma(2)             |TG_.sigma(4),Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)             ,Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)             |TG_.sigma(3)|TG_.sigma(4),Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|TG_.sigma(2)             |TG_.sigma(4),Pt[5]));
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)             ,Pt[6]));
      VertexNum += 6;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[5],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[5],V[1],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[1],V[6],V[2]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[6],V[4]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[6],V[5]));
      listT_.push_back(Triangle(++ElementNum,V[5],V[6],V[1]));
      }
      else {
         // With 4 initial triangles, we need 4 boundary patches to define the boundary.
         { // Definition of 4 subdomains
           number_t B_patch[] =       {0}, B_dimen[] =       {0};
           number_t I_patch[] =       {0}, I_dimen[] =       {0};
           number_t D_patch[] = {1,2,3,4}, D_dimen[] = {2,2,2,2};
           delete DG; // not needed
           PatchGeometry *P_geom[] = {SS,SS,SS,SS}; // patch # 1, 2, 3, 4
           number_t nbSbDom = sizeof(D_patch)/sizeof(number_t);
           TG_ = TopoGeom(      0,B_patch,B_dimen,
                                0,I_patch,I_dimen,
                          nbSbDom,D_patch,D_dimen, P_geom);
//          Make only one domain
           vector< vector<number_t> > VSG;
           VSG.push_back( vector<number_t>(D_patch,D_patch+nbSbDom) );
           if (! TG_.setSubdomains(VSG)) {
           cout << "*** Error in SurfMeshTriSphere::initMesh while making subdomains." << endl;
           }
        }
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Quarter of the sphere opposite to vertex 1";
      TG_.setDescription(++iPh) = "Quarter of the sphere opposite to vertex 2";
      TG_.setDescription(++iPh) = "Quarter of the sphere opposite to vertex 3";
      TG_.setDescription(++iPh) = "Quarter of the sphere opposite to vertex 4";

      real_t hcd = radius/std::sqrt(3);
      vector<Point> Pti;
      Pti.push_back(Point( hcd,-hcd,-hcd));
      Pti.push_back(Point(-hcd, hcd,-hcd));
      Pti.push_back(Point( hcd, hcd, hcd));
      Pti.push_back(Point(-hcd,-hcd, hcd));
      rotNtrans(rots, OC, Pti);
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4),Pti[0]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(3)|TG_.sigma(1)|TG_.sigma(4),Pti[1]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4),Pti[2]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3),Pti[3]));
      VertexNum += 4;
      listT_.push_back(Triangle(++ElementNum,V[1],V[2],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[4],V[3]));
      listT_.push_back(Triangle(++ElementNum,V[2],V[1],V[4]));
      listT_.push_back(Triangle(++ElementNum,V[4],V[1],V[3]));
      }
      break;
   }
   if (type_ == 0) { delete SS; }
}

} // end of namespace subdivision
} // end of namespace xlifepp
