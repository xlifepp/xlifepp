/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file PointUtils.hpp
  \authors Y. Lafranche, N. Kielbasiewicz
  \since 04 Dec 2007
  \date 09 Jul 2012

  \brief additional computation functions using class xlifepp::Point
 */

#ifndef POINT_UTILS_HPP
#define POINT_UTILS_HPP

#include "utils.h"
#include "types.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

//! barycenter of n points
Point barycenter(const real_t *coef, const std::vector<Point>& VP);
Point barycenter(const std::vector<real_t>& coef, const std::vector<Point>& VP);

//! projection onto the plane defined by the point A and the normal unitary vector U
Point projOnPlane(const Point& p, const Point& A, const Vect& U);

//! projection onto a sphere of radius Radius of the barycenter of n points
Point projOnSph(const real_t *coef, const std::vector<Point>& VP, const real_t Radius);

//! rotation of the current point around the axis defined by the point A and
//! the unitary vector U, in the plane orthogonal to U
Point rotInPlane(const Point& p, const real_t ct, const real_t st, const Point& A, const Vect& U);

//! rotation of P1 by the angle k/n * ang(P1, O, P2)
Point rotOnSph(const Point& P1, const Point& P2, const int k, const int n);

//! translation of the current point by the vector lambda*U
Point translate(const Point& p, const real_t lambda, const Vect& U);

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* POINT_UTILS_HPP */
