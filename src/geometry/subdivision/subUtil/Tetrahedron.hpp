/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Tetrahedron.hpp
  \author Y. Lafranche
  \since 07 Dec 2007
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::Tetrahedron class

 Class Tetrahedron defines a tetrahedron as a list of vertices.
 This class also holds the so-called "high order vertices" involved in the
 Lagrange tetrahedron of order k, k>1. The 4 "main" vertices correspond to k=1.
 High order vertices allow a better approximation of the geometry.

 Terminology: Strictly speaking, a tetrahedron has 4 vertices. But to make short,
              the additionnal points involved in the Lagrange tetrahedron of order
              greater than 1 are also called "vertices" although they are not.

 Local numbering convention
 --------------------------
 Assuming the current tetrahedron is defined by the sequence of vertices (1,2,3,4),
 the faces and edges are numbered according to the following convention:
 \verbatim
                   3
                  /.\
                 / . \             Faces: Number Vertices       Edges: Number Vertices
                /  .  \                     1     4,3,2                  1      4,1
               /   .   \                    2     4,1,3                  2      4,2
              /    .    \                   3     4,2,1                  3      4,3
             /     .     \                  4     1,2,3                  4      2,3
            /      .      \                                              5      1,3
           /       .       \                                             6      1,2
          /      . 4 .      \      Faces: Number Edges
         /     .       .     \              1    3,-4,2 (-4 is edge 3,2)
        /    .           .    \             2    1,5,3
       /   .               .   \            3    2,-6,1 (-6 is edge 2,1)
      /  .                   .  \           4    6,4,5
     / .                       . \
    /.                           .\
   /_______________________________\
  1                                2
 \endverbatim

 The face (i,j,k) is oriented so that the cross-product ij x ik defines a normal vector
 to the face oriented towards the exterior of the tetrahedron (positive orientation).
 In other words, if l is the 4th vertex, (ij x ik) . il < O  holds.
 The edge (i,j) is oriented from i towards j.
 Moreover, for any face (i,j,k), the two edges (i,j) and (i,k) are in the list of
 the 6 edges.

 The numbering convention associated to higher order vertices (order > 1) is
 described in the document ./doc/NumConvTet.pdf.

 In the implementation, the 4 main vertices (of order 1) are stored at the
 beginning of the internal vector vertices_ ; high order vertices, if any,
 are stored in the following positions according the numbering convention.
 The relation between the position of each vertex and its local number is given
 by the function numberingOfVertices.
*/

#ifndef TETRAHEDRON_HPP
#define TETRAHEDRON_HPP

#include "Vertex.hpp"
#include "Simplex.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class Tetrahedron
*/
class Tetrahedron : public Simplex<Tetrahedron>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! constructor by vertices, given by their rank in the list
   Tetrahedron(const number_t num, const number_t rV1, const number_t rV2,
               const number_t rV3, const number_t rV4, const number_t bdSideNum=0);

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! get back the rank of a vertex of an edge
   virtual short getrkEdge(short indEdge, short indVert) const { return rkEdge[indEdge][indVert]; }

   //! get back the rank of a vertex of a face
   virtual short getrkFace(short indFace, short indVert) const { return rkFace[indFace][indVert]; }

   //! returns the numbers (>= 1) of the edges defining the face number i
   static std::vector<short> numEdgesOfFace(const number_t i);

   //! returns the ranks (>= 0) of the vertices defining the edges of the tetrahedron
   static std::vector<std::pair<short,short> > rkEdgeVertices();
//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! volume of the tetrahedron
   real_t volume(const std::vector<Vertex>& listV) const ;

   //! radius of inscribed sphere
   real_t inRadius(const std::vector<Vertex>& listV) const ;

   //! diameter of the tetrahedron (longest edge)
   real_t diameter(const std::vector<Vertex>& listV) const ;

   //! position of the vertices in relation with the numbering convention
   static std::vector< std::vector<number_t> > numberingOfVertices(const number_t Order) ;

private:
   static short rkEdge[/* nb_edges_ */][2];   //!< Index array defining the edges
   static short rkFace[/* nb_faces_ */][3];   //!< Index array defining the faces
   static short nuEdge[/* nb_faces_ */][3];   //!< Array containing the edge numbers defining a face

}; // end of Class Tetrahedron

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* TETRAHEDRON_HPP */
