/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TopoGeom.hpp
  \author Y. Lafranche
  \since 23 Apr 2009
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::TopoGeom class

 This class holds the topological and geometrical informations associated to
 the domain in which the mesh is to be built. They are organized hierarchically
 in two levels which are from top to bottom:
   - areas: boundaries, interfaces and subdomains,
   - patches: boundary, interface and subdomain patches.
 Areas are made of patches.

 We consider three kinds of area:
  - boundary,  i.e. an external boundary delimiting the shape of the domain,
  - interface, i.e. a internal boundary, which appears at the initial step of
               the mesh generation as a natural separator between two subdomains
               or two parts of the same domain,
  - subdomain, such that the domain is the union of the subdomains and the
               intersection between two subdomains is empty or is an interface.

 The basic entity is called patch. The patches are distributed over the different
 kinds of area and are thus called boundary patches, interface patches and subdomain
 patches ; they can then be grouped together to form the boundaries, the interfaces
 and the subdomains.

 The boundary patches are defined during the construction of the object and cannot
 be modified later. However they can be arranged in groups, in order to make the
 union of two or more boundary patches. The groups and their contents can be modified
 by the function setBoundaries. In the simplest situation, there are as many boundaries
 as boundary patches initially created, each boundary containing only one boundary
 patch. The same applies for the interfaces and the subdomains. The modifiers are
 respectively setInterfaces and setSubdomains.

 The boundaries, the interfaces, the subdomains, and the patches are all numbered
 starting from 1.

 Patches
 -------
 Patches bear the topological and geometrical informations related to the area
 it is associated to. Topology deals with the question: "Which vertices belong
 to which patches ?". This is done by the mean of localization codes (see below).
 Geometry deals with the question: "What is the location of a new vertex ?".
 Each patch is associated to a so called "patch geometry" which defines the method
 to be used to compute the coordinates of a new vertex. The default method is
 to compute the new point as a barycenter of other ones. Patches that are not
 associated to this default method are also called curved patches. They are
 gathered in a list (Curv_Ptch_) used by the computational routine newVertexPtGen
 of the class xlifepp::subdivision::SubdivisionMesh.

 Localization code
 -----------------
 sigma(i) is the internal code of Pi, the patch number i. To any point involved in
 the mesh definition, one can associate a localization code which is defined in the
 following way:
  - if the point belongs to Pi only, code = sigma(i),
  - if the point belongs to both Pi and Pj, code = sigma(i) | sigma(j),
    provided that for any couple (Pi, Pj) with i != j, sigma(i) & sigma(j) = 0.
 Thus, a point can be localized using the following properties. A point bearing the
 localization code LC belongs:
  - to Pi,                            if and only if  LC &  sigma(i) is > 0,
  - to the union of Pi and Pj,        if and only if  LC & (sigma(i) | sigma(j)) is > 0,
  - to the intersection of Pi and Pj, if and only if  LC &  sigma(i) & sigma(j)  is > 0.

 Each patch has a localization code given by the function localCodeOf.

 Since a boundary, an interface or a subdomain may be the union of patches, each of
 them has a localization code computed as described above.

 The unions of all the boundary patches, all the interface patches and all the
 subdomain patches have each their localization code. They are given by the function
 maskOf. These values are independant of the distribution of the patches among the
 areas and are used as test masks in order to select one of these kinds of area.

 IMPORTANT NOTE:
  The elements of Ptch_geom_ are pointers initialized through dynamic memory
  allocation. The associated memory is freed at top level by the destructor of
  SubdivisionMesh because the objects pointed to must be created only once and
  must remain in memory until the SubdivisionMesh object that created them dies.
  The same objects are also pointed to in Curv_Ptch_.
  Consequently, there is no special copy constructor, overloaded assignment
  operator nor destructor in this class.
*/

#ifndef TOPO_GEOM_HPP
#define TOPO_GEOM_HPP

#include "config.h"
#include "types.hpp"
#include "DefaultGeometry.hpp"
#include "SurfRevolution.hpp"

#include <iostream>
#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class TopoGeom
*/
class TopoGeom
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! default constructor
   TopoGeom():nbBndryPatch_(0),nbIntrfPatch_(0),nbSbDomPatch_(0),nbPatches_(0) {}

   //! simple constructor by number of boundaries, interfaces and subdomains
   TopoGeom(const number_t nbBound, const number_t nbIntrf=0, const number_t nbSbDom=1);

   /*!
     general constructor by boundaries, interfaces and subdomains, and their
     associated patches and dimensions, along with the patches shapes.
    */
   TopoGeom(const number_t nbBound, const number_t* B_patch, const number_t* B_dimen,
            const number_t nbIntrf, const number_t* I_patch, const number_t* I_dimen,
            const number_t nbSbDom, const number_t* D_patch, const number_t* D_dimen,
            PatchGeometry **const P_geom);

   TopoGeom(const std::vector<number_t>& B_patch, const std::vector<number_t>& B_dimen,
            const std::vector<number_t>& I_patch, const std::vector<number_t>& I_dimen,
            const std::vector<number_t>& D_patch, const std::vector<number_t>& D_dimen,
            const std::vector<PatchGeometry *>& P_geom);

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   // Areas
   //! returns kind of area (boundary, interface or subdomain)
   std::string kindOf(const topologicalArea TA) const;

   //! returns number of boundaries, interfaces or subdomains.
   number_t numberOf(const topologicalArea TA) const;

   //! returns boundary mask, interface mask or subdomain mask
   refnum_t maskOf(const topologicalArea TA) const;


   //! returns dimension of boundary area, interface area or subdomain area num, num >=1
   number_t dimensionOf(const topologicalArea TA, const number_t num) const;

   /*!
     returns reference number (localization code) of boundary area, interface area
     or subdomain area num, num >=1
    */
   refnum_t localCodeOf(const topologicalArea TA, const number_t num) const;

   /*!
     returns description of boundary, interface or subdomain num, num >=1.
     The description is deduced from the patch descriptions.
    */
   std::string descriptionOf(const topologicalArea TA, const number_t num) const;

   /*!
     returns name of boundary, interface or subdomain num, num >=1
     The name is deduced from the patch names.
    */
   std::string nameOf(const topologicalArea TA, const number_t num) const;

   /*!
     returns user attribute of area num, num >=1, of kind TA.
     A kind of area may be a boundary, an interface or a subdomain.
    */
   std::string getAttribute(const topologicalArea TA, const number_t num) const;


   // Patches
   //! returns total number of patches
   number_t numberOfPatches() const {return nbPatches_;}

   //! number of boundary patches, interface patches and subdomain patches
   number_t numberOfBndryPatches() const {return nbBndryPatch_; }
   number_t numberOfIntrfPatches() const {return nbIntrfPatch_; }
   number_t numberOfSbDomPatches() const {return nbSbDomPatch_; }

   //! returns reference number (localization code) of patch num, num >=1
   refnum_t sigma(const number_t num) const {return Ptch_refnum_.at(num-1);}

   //! returns pointer to shape object associated to patch num, num >=1
   PatchGeometry* shape(const number_t num) const {return Ptch_geom_.at(num-1);}


   //! returns number of curved patches
   number_t numberOfCurvedPatches() const {return Curv_Ptch_.size();}

   /*!
     returns data associated to a curved patch (shape aNd localization code)
     whose index is ind in the vector
    */
   std::pair<PatchGeometry*,refnum_t> shapeNlcode(const number_t ind) const {return Curv_Ptch_.at(ind);}

//-------------------------------------------------------------------------------
//  Public modifiers, provided to change the value of non critical members.
//-------------------------------------------------------------------------------
   /*!
     returns a reference to user attribute of area num, num >=1, of kind TA.
     A kind of area may be a boundary, an interface or a subdomain.
    */
   std::string& setAttribute(const topologicalArea TA, const number_t num);

   //! returns reference to description of patch num, num >=1
   std::string& setDescription(const number_t num) {return Ptch_description_.at(num-1);}

   //! redefines boundaries
   bool setBoundaries(const std::vector< std::vector<number_t> >& group) {
                                         return setUserGroups(boundaryArea,group); }

   //! redefines interfaces
   bool setInterfaces(const std::vector< std::vector<number_t> >& group){
                                         return setUserGroups(interfaceArea,group); }

   //! redefines subdomains
   bool setSubdomains(const std::vector< std::vector<number_t> >& group){
                                         return setUserGroups(subdomainArea,group); }

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   /*!
     prints, on stream os, the information about the boundaries,
     interfaces and subdomains
    */
   void print(std::ostream& os, const bool forTeX=false) const;
   void print(PrintStream& os, const bool forTeX=false) const {print(os.currentStream(),forTeX);}


   /*!
     prints, on stream os, the name of the patches corresponding
     to the localisation code localcod
    */
   void printLoc(std::ostream& os, const refnum_t localcod) const;
   void printLoc(PrintStream& os, const refnum_t localcod) const {print(os.currentStream(),localcod);}


private:
   //! number of boundary patches, interface patches and subdomain patches
   number_t nbBndryPatch_, nbIntrfPatch_, nbSbDomPatch_;
   //! number of patches (= nbBndryPatch_ + nbIntrfPatch_ + nbSbDomPatch_)
   number_t nbPatches_;
   /*!
     boundary mask, interface mask and subdomain mask defined to consider
     all the patches belonging to a kind of area as a whole set.
    */
   refnum_t Bndry_mask_, Intrf_mask_, SbDom_mask_;

   // In the following, we have:
   //  . patch number i+1 is defined by XXX_patch_[i],
   //  . 1 <= XXX_patch_[i] <= nbPatches_.

   // Boundaries
   //! boundary areas (each made of a group of patches)              (size = nbBdryGroups_)
   std::vector< std::vector<number_t> > BndryPGr_;
   //! reference numbers (localization codes) of the boundary areas  (size = nbBdryGroups_)
   std::vector<refnum_t> Bndry_refnum_;
   //! user attribute associated to the boundaries           (size = nbBndryPatch_ at most)
   std::vector<std::string>   Bndry_attribute_;

   //! patches lying in boundary areas                               (size = nbBndryPatch_)
   std::vector<number_t> Bndry_patch_;

   // Interfaces
   //!  interface areas (each made of a group of patches)            (size = nbIntrfGroups_)
   std::vector< std::vector<number_t> > IntrfPGr_;
   //! reference numbers (localization codes) of the interface areas (size = nbIntrfGroups_)
   std::vector<refnum_t> Intrf_refnum_;
   //! user attribute associated to the interfaces            (size = nbIntrfPatch_ at most)
   std::vector<std::string>   Intrf_attribute_;

   //! patches lying in interface areas                               (size = nbIntrfPatch_)
   std::vector<number_t> Intrf_patch_;

   // Subdomains
   //!  subdomain areas (each made of a group of patches)            (size = nbSbDomGroups_)
   std::vector< std::vector<number_t> > SbDomPGr_;
   //! reference numbers (localization codes) of the subdomain areas (size = nbSbDomGroups_)
   std::vector<refnum_t> SbDom_refnum_;
   //! user attribute associated to the subdomains            (size = nbSbDomPatch_ at most)
   std::vector<std::string>   SbDom_attribute_;

   //! patches lying in subdomain areas                               (size = nbSbDomPatch_)
   std::vector<number_t> SbDom_patch_;

   // Patches
   // Informations related to the patches are stored in the following vectors.
   // The order (distribution among boundaries, interfaces or subdomains)
   // is the same for each vector. It depends on the construction of the object.
   // Direct access (via an index) to these vectors is allowed during the early stage of
   // the construction process, i.e. from the constructor TopoGeom::TopoGeom and the
   // function TopoGeom::initTables, but also from the functions *Mesh::initMesh
   // via the access function TopoGeom::sigma and the modifier TopoGeom::setDescription ;
   // afterwards, access to these data must be made via the XXXPGr_ structure, as in the
   // printTA function. An exception is the function TopoGeom::shape called by the destructor
   // SubdivisionMesh::~SubdivisionMesh() because in this function, there is no need
   // to distinguish between the different kinds of areas. Reference map is given below.

   //! reference numbers (localization codes) of the patches    (size = nbPatches_)
   std::vector<refnum_t>       Ptch_refnum_;
   /*!
     dimension of the geometrical domain associated to the
     patches (1 = curve, 2 = surface, 3 = volume)               (size = nbPatches_)
    */
   std::vector<number_t>       Ptch_dim_;
   //! shape of the patches                                     (size = nbPatches_)
   std::vector<PatchGeometry*> Ptch_geom_;
   //! name of the patches                                      (size = nbPatches_)
   std::vector<std::string>         Ptch_name_;
   //! description of the patches                               (size = nbPatches_)
   std::vector<std::string>         Ptch_description_;

   // Others
   /*!
     list of shapes and localisation codes of the patches
     whose associated shapes are not plane	  (size = nbCurv_Ptch_ <= nbPatches_)
    */
   std::vector< std::pair<PatchGeometry*,refnum_t> > Curv_Ptch_;
/*
 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

                             Reference map of Ptch_XXX vectors

                      referenced in                    referenced in

Ptch_refnum_     (I)TopoGeom::initTables
                    TopoGeom::printTA
                    TopoGeom::setAreaRefnum
                    sigma                            *Mesh::initMesh                    (AF)

Ptch_geom_       (I)TopoGeom::TopoGeom               *Mesh::initMesh
                    TopoGeom::initTables
                    TopoGeom::printTA
                    shape                           SubdivisionMesh::~SubdivisionMesh() (AF)

Ptch_name_          TopoGeom::nameOf(TA,num)
                    TopoGeom::printLoc
                 (I)TopoGeom::initTables()
                    TopoGeom::printTA

Ptch_description_   TopoGeom::initTables()
                    TopoGeom::printTA
                 (I)setDescription                    *Mesh::initMesh                   (AF)

(I)  means initialization
(AF) means direct access by the mean of an access function
 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
*/
//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
   //! initialization of the internal tables
   void initTables();

   //! prints, on stream os, the information about a topological area.
   void printTA(std::ostream& os, const bool forTeX, const topologicalArea TA,
                const std::vector< std::vector<number_t> >& Xgroup) const;

   /*!
     checks the coherence of the groups with respect to the patches
     created during the construction of the object
    */
   bool checkGroup(const std::vector< std::vector<number_t> >& Xgroup, const std::vector<number_t>& Xpatch) const;

   //! initialization of the reference numbers of the areas of a given kind
   void setAreaRefnum(const std::vector< std::vector<number_t> >& Xgroup, std::vector<refnum_t>& AreaRefnum);

   //! initialization of the group tables with the default values
   void setDefaultGroups();

   //! define new distribution of boundaries, interfaces or domains
   bool setUserGroups(const number_t index, const std::vector< std::vector<number_t> >& Ugroup);

}; // end of Class TopoGeom

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* TOPO_GEOM_HPP */
