/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file types.hpp
  \author Y. Lafranche
  \since 19 Jun 2008
  \date 08 Mar 2014

  \brief types header file
*/

#ifndef SUBDIVISION_TYPES_HPP
#define SUBDIVISION_TYPES_HPP

#include "utils.h"

#include <map>
#include <set>
#include <vector>

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Type definitions
//-------------------------------------------------------------------------------
typedef std::pair<number_t, number_t> pair_nn;
typedef std::pair<int_t, number_t> pair_in;
typedef std::map<pair_nn, number_t> map_pair_num;

typedef std::set<number_t> set_n;
typedef std::map< set_n, std::vector< pair_nn > > map_set_vec;
typedef std::map< set_n, pair_nn > map_set_pair;
typedef std::map< set_n, pair_in > map_set_pair_in;
typedef std::map< set_n, number_t > map_set_num;

typedef number_t refnum_t;

class SubdivisionMesh;
typedef Point (SubdivisionMesh::*PtCompFun)
              (const refnum_t localcod, const real_t *coef, const std::vector<Point>& VP) const;

enum topologicalArea {boundaryArea, interfaceArea, subdomainArea};

typedef Vector<real_t> Vect;

// The two following are used in class VolMeshTetCylinder and in class VolMeshTetCone.
enum GeomEndShape{None, Flat, Cone, Ellipsoid, Sphere};
struct ShapeInfo {
  //! shape of the end surface of the cylinder (or cone)
  GeomEndShape shapeCode_;
  //! distance between the basis of the cylinder (or cone) and the apex of the surface
  real_t dist_;

  ShapeInfo():shapeCode_(Flat), dist_(0.) {}
  ShapeInfo(GeomEndShape ges, real_t di):shapeCode_(ges), dist_(di) {}
};

} // end of namespace subdivision

} // end of namespace xlifepp

#endif /* SUBDIVISION_TYPES_HPP */
