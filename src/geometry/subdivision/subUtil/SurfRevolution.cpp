/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfRevolution.cpp
  \author Y. Lafranche
  \since 23 Oct 2009
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::SurfRevolution and child classes members and related functions
*/

#include "SurfRevolution.hpp"

using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
SurfRevolution::SurfRevolution(const number_t type, const string descr,
                               const Point& P1, const Point& P2, const real_t radius)
:PatchGeometry(type,descr), EndPt1_(P1), EndPt2_(P2), radius_(radius), Uaxis_(toVector(P1,P2)){
   height_ = norm(Uaxis_);
   // try to normalize the vector Uaxis_
   try {
      if (height_ < theTolerance) {
         DbZ.descr = descr;
         DbZ.val.push_back(height_);
         DbZ.val.push_back(radius);
         DbZ.VP.push_back(P1);
         DbZ.VP.push_back(P2);
         throw DbZ;
      }
      else {
         Uaxis_ *= (1./height_);
      }
   }
   catch (DivideByZero DbZ) {
      cout << "*******************************************************" << endl;
      cout << "From SurfRevolution constructor ("<< DbZ.descr << "):" << endl;
      cout << " attempt to divide by " << DbZ.val[0] << endl;
      cout << "Radius is: " << DbZ.val[1] << endl;
      cout << "Points are:" << endl;
      cout<<DbZ.VP[0]<<endl;
      cout<<DbZ.VP[1]<<endl;
      cout << "*******************************************************" << endl;
   }
}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
/*!
 Projection onto the cone of the barycenter of the points in VP with coefficients
 in coef. The points in VP are assumed to lie on the cone (this is not checked).
 The projection is made in the plane orthogonal to the axis containing the point
 to project.
 */
Point SurfCone::projOnBound(const real_t *coef, const vector<Point>& VP) const {
   Point P=barycenter(coef,VP);
// compute the projection H of P onto the axis of the cone
   Point H = projOnAxis(P);
// compute the radius of the section of the cone containing H and P
   real_t RadH = radiusAt(H);
// compute the projection of P onto the surface of the cone
   return pointOnLine(H,P,RadH);
}
/*!
  Computes the radius of the section of the cone whose center is P. Thus, P is assumed
  to lie on the axis. This is not checked.
 */
real_t SurfCone::radiusAt(const Point& P) const {
   return std::sqrt(EndPt2_.squareDistance(P))*coef_ + radius2_;
}
//! return the radii of the cone
vector<real_t> SurfCone::radii() const {
   vector<real_t> R;
   R.push_back(radius_);
   R.push_back(radius2_);
   return R;
}

/*!
 Projection onto the cylinder of the barycenter of the points in VP with coefficients
 in coef. The points in VP are assumed to lie on the cylinder (this is not checked).
 */
Point SurfCylinder::projOnBound(const real_t *coef, const vector<Point>& VP) const {
   Point P=barycenter(coef,VP);
// compute the projection H of P onto the axis of the cylinder
   Point H = projOnAxis(P);
// compute the projection of P onto the surface of the cylinder
   return pointOnLine(H,P,radius_);
}

/*!
 Projection onto the ellipsoid of the barycenter of the points in VP with coefficients
 in coef. The points in VP are assumed to lie on the ellipsoid (this is not checked).
 The projection is made onto the half of the ellipsoid whose apex is EndPt2_.
 */
Point SurfEllipsoid::projOnBound(const real_t *coef, const vector<Point>& VP) const {
   Point P=barycenter(coef,VP);
// The intersection of the plane (P1,P2,P) and the ellipsoid is an ellipse whose
// radii are radius_ and height_. First, check that P1, P2 and P are not on the
// same line, which is true if P1P x P1P2 = 0.
   Vect V=toVector(EndPt1_,P);
   Vect W = crossProduct(V,Uaxis_);
   real_t nor = norm(W);
   if (nor < theTolerance) { // P lies on the axis
      return EndPt2_;
      }
   else {
// compute the unitary vector Ux orthogonal to Uaxis_ in the plane (P1,P2,P)
// in the direction of P
      W *= (1./nor);
      Vect Ux = crossProduct(Uaxis_,W);
// compute the component of P1P with respect to Ux
      real_t x = dot(V,Ux);
// compute the component of P1P with respect to Uaxis_
      real_t y = dot(V,Uaxis_);
// Let M be the intersection of the line P1P with the ellipse and theta be the
// parametrization angle of the ellipse. We could compute theta as
// theta = atan((radius_*y)/(height_*x)) and then get xm=radius_*cos(theta) and
// ym=height_*sin(theta). Here is a faster procedure, where s=sin(theta) :
      real_t ay = radius_*y, bx = height_*x;
      real_t s = ay/std::sqrt(ay*ay+bx*bx), c=std::sqrt(1-s*s);
      real_t xm=radius_*c, ym=height_*s;
// compute the norm of P1M
      real_t Rad = std::sqrt(xm*xm+ym*ym);
// compute the projection of P onto the surface of the ellipsoid
      return pointOnLine(EndPt1_,P,Rad);
      }
}

/*!
 Projection onto the sphere of the barycenter of the points in VP with coefficients
 in coef. The points in VP are assumed to lie on the sphere (this is not checked).
 */
Point SurfSphere::projOnBound(const real_t *coef, const vector<Point>& VP) const {
   Point P=barycenter(coef,VP);
// project the barycenter onto the sphere
   return pointOnLine(EndPt1_,P,radius_);
}

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
/*!
 Compute the projection H of P onto the axis of the surface
 */
Point SurfRevolution::projOnAxis(const Point& P) const {
   Vect V=toVector(EndPt1_,P);
   real_t lambda = dot(V,Uaxis_);
   Point H(EndPt1_);
   return translate(H,lambda,Uaxis_);
}

/*!
 Compute the point at distance D of H on the line (H,P)
 */
Point SurfRevolution::pointOnLine(const Point& H, const Point& P, const real_t D) const {
   // d is the distance between H and P.
   // d is non zero since P is not far from the surface in the context of use
   real_t d = std::sqrt(H.squareDistance(P));
   real_t lambda = D / d;
   Vect U=toVector(H,P);
   Point Q(H);
   return translate(Q,lambda,U);
}

} // end of namespace subdivision
} // end of namespace xlifepp
