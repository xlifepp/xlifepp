/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfRevolution.hpp
  \author Y. Lafranche
  \since 23 Oct 2009
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::SurfRevolution class and its childs

  Class xlifepp::subdivision::SurfRevolution is an abstract class that defines
  a surface of revolution in the 3D space.

  inheriting classes
  ------------------
    - xlifepp::subdivision::SurfCone       (type = 1)
    - xlifepp::subdivision::SurfCylinder   (type = 2)
    - xlifepp::subdivision::SurfEllipsoid  (type = 3)
    - xlifepp::subdivision::SurfSphere     (type = 4)
*/

#ifndef SURF_REVOLUTION_HPP
#define SURF_REVOLUTION_HPP

#include <vector>

#include "PatchGeometry.hpp"

namespace xlifepp {
namespace subdivision {

/*!
   \class SurfRevolution
*/
class SurfRevolution : public PatchGeometry
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   SurfRevolution(const number_t type, const std::string descr,
                  const Point& P1, const Point& P2, const real_t radius);

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! returns the unitary axis vector
   Vect AxisVector() const {return Uaxis_;}

   //! returns the distance between the two end points of the axis
   real_t Height() const {return height_;}

   //@{
   //! returns the end points of the axis
   virtual Point EndPt1() const {return EndPt1_;}
   virtual Point EndPt2() const {return EndPt2_;}
   //@}

protected:
   //! the two end points of the axis defining the surface of revolution
   Point EndPt1_, EndPt2_;
   //! radius of the circular section of the surface in a plane orthogonal to the axis
   real_t radius_;
   //! unitary vector defining the direction of the axis P1P2
   Vect Uaxis_;
   //! distance between the two end points of the axis
   real_t height_;

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! returns the projection of P onto the axis of the surface
   Point projOnAxis(const Point& P) const;
   //! returns the point at distance D of H on the line (H,P)
   Point pointOnLine(const Point& H, const Point& P, const real_t D) const;

}; // end of Class SurfRevolution


/*!
   \class SurfCone
*/
class SurfCone : public SurfRevolution
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   /*!
     main constructor for a cone or a truncated cone
     P1 is the center of the circular basis of the cone.
     P2 is the vertex of the cone (in which case radius2=0) or the center of the
        other circular basis of the truncated cone.
     radius1 is the radius of the circular section of the cone in the
        plane orthogonal to the axis (P1,P2) containing P1. It should not be null.
     radius2 is null or is the radius of the other circular section of the
        truncated cone in the plane orthogonal to the axis (P1,P2) containing P2.
              R2
             ---- P2  ^
            /   |     |
           /    |     | heigth
          /     |     |
         -------- P1  v
             R1
    */
   SurfCone(const Point& P1, const Point& P2, const real_t radius1, const real_t radius2=0)
   :SurfRevolution(1,"conical",P1,P2,radius1), radius2_(radius2) {
      coef_ = (radius_ - radius2_) / height_;
   }

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   /*!
     projection onto the cone of the barycenter of the points in VP with
     coefficients in coef
    */
   virtual Point projOnBound(const real_t *coef, const std::vector<Point>& VP) const;
   /*!
     Computes the radius of the section of the cone whose center is P. Thus, P is assumed
     to lie on the axis. This is not checked.
    */
   real_t radiusAt(const Point& P) const;

   //! return the radii of the cone
   std::vector<real_t> radii() const;
protected:
   //! radius of the circular section of the truncated cone in a plane orthogonal to the axis
   real_t radius2_;
   //! slope of the cone with respect to the axis
   real_t coef_;

}; // end of Class SurfCone

/*!
   \class SurfCylinder
*/
class SurfCylinder : public SurfRevolution
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   /*!
     main constructor
     radius is the radius of the circular section of the cylinder in any
     plane orthogonal to the axis (P1,P2).
    */
   SurfCylinder(const Point& P1, const Point& P2, const real_t radius)
   :SurfRevolution(2,"cylindrical",P1,P2,radius){}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   /*!
     projection onto the cylinder of the barycenter of the points in VP with
     coefficients in coef
    */
   virtual Point projOnBound(const real_t *coef, const std::vector<Point>& VP) const;

}; // end of Class SurfCylinder

/*!
   \class SurfEllipsoid
*/
class SurfEllipsoid : public SurfRevolution
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   /*!
     main constructor
     P1 is the center of the circular section of the ellipsoid.
     P2 is one of the two apexes on the axis of the ellipsoid.
     radius is the radius of the circular section of the ellipsoid in the
     plane orthogonal to the axis (P1,P2) containing P1.
    */
   SurfEllipsoid(const Point& P1, const Point& P2, const real_t radius)
   :SurfRevolution(3,"ellipsoidal",P1,P2,radius) {}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   /*!
     projection onto the ellipsoid of the barycenter of the points in VP with
     coefficients in coef
    */
   virtual Point projOnBound(const real_t *coef, const std::vector<Point>& VP) const;

}; // end of Class SurfEllipsoid

/*!
   \class SurfSphere
*/
class SurfSphere : public SurfRevolution
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   SurfSphere(const Point& Center, const real_t radius)
   :SurfRevolution(4,"spherical",Center,Center+radius,radius){}

   //! other constructor for compatibility with other surfaces
   SurfSphere(const Point& Center, const Point& PtOnAxis, const real_t radius)
   :SurfRevolution(4,"spherical",Center,PtOnAxis,radius){}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   /*!
     projection onto the sphere of the barycenter of the points in VP with
     coefficients in coef
    */
   virtual Point projOnBound(const real_t *coef, const std::vector<Point>& VP) const;

}; // end of Class SurfSphere

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* SURF_REVOLUTION_HPP */
