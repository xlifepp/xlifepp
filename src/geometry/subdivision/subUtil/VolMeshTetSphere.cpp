/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VolMeshTetSphere.cpp
  \author Y. Lafranche
  \since 15 Oct 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::VolMeshTetSphere class members and related functions
*/

#include "VolMeshTetSphere.hpp"

#include <vector>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of tetrahedrons by successive subdivisions.

 \param rots: rotations to be applied to the sphere to get its final position.
   Each rotation, if any, is defined by an angle in degrees and the number of the
   absolute axis (1, 2 or 3) around which the rotation is made.
   Each rotation is applied in turn to the sphere starting from the canonical initial
   position where the sphere is centered at the origin and its local axes are parallel
   to the absolute axes.

 \param nboctants: number of octants to be filled
   nboctants may take a value in [1, 8].
   For the half-sphere and the sphere, when a spherical subdivision is requested
   (argument type different from 0, see below), the initial mesh may consist of one
   single tetrahedron instead of respectively 4 and 8 tetrahedrons, which reduces
   the number of tetrahedrons of the final mesh by a factor 4 and 8 respectively.
   In order to activate this behaviour, nboctants must take the value -4 and -8
   respectively.

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each tetrahedron is subdivided into 8 tetrahedrons with the
   same orientation as the original one: if the first tetrahedron is (1,2,3,4), the
   vectors 12x13, 41x43, 43x42 and 42x41 define the exterior normals to the faces.

 \param order: order of the tetrahedra in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   tetrahedron is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, the vertices
   belonging to the appropriate boundary lie on the sphere. See below the two
   possible ways to compute these vertices.

 \param type: type of the subdivision (1 by default)
   . if type = 0, the algorithm leads to a simple (flat) subdivision of the initial
   mesh where new vertices are all the midpoints of the edges.
   . if type = 1, the algorithm leads to a subdivision where the vertices belonging to
   a prescribed boundary are computed so that they lie on the sphere ; they are computed
   as the radial projections of the vertices of the Lagrange mesh of order k over the
   triangular chord face.

 \param radius: radius of the sphere (1. by default)
 \param Center: center of the sphere ((0,0,0) by default)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
VolMeshTetSphere::VolMeshTetSphere(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                                   const number_t nbsubdiv, const number_t order,
                                   const number_t type, const real_t radius, const Point Center,
                                   const number_t minVertexNum, const number_t minElementNum)
:TetrahedronMesh(nbsubdiv, order,type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   initMesh(rots,nboctants,radius,Center,VertexNum,ElementNum);
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------

/*!
 Prints, on stream os, some statistics about a tetrahedron mesh
 */
void VolMeshTetSphere::statistics(ostream& os) const {
   real_t refvol, refhsrho = 2*std::sqrt(6);
   if (type_==0) {
      refvol = 1./6.; // volume of unit trirectangle tetrahedron
   } else {
      refvol = pi_/6.; // 1/8 of unit sphere
   }
   os << "          ******************" << endl;
   os << "          *   Statistics   *" << endl;
   os << "          ******************" << endl;
   os << "Tetrahedron    Volume    % RefVol   h/rho   Quality" << endl;
   real_t sv=0, sp=0;
   real_t hsrhomin=1.e20, hsrhomax=0;
   real_t qualmin=1.e20, qualmax=0;
   os.setf(ios::fixed,ios::floatfield);

   vector<Tetrahedron>::const_iterator itT;
   for (itT=listT_.begin(); itT != listT_.end(); itT++) {
      real_t v = itT->volume(listV_);
      os << setw(6)<< itT->number() <<"        "<<setw(8)<<setprecision(6)<< v;
      sv += v;
      real_t r=v/refvol*100;
      os <<"   "<<setw(6)<<setprecision(2)<< r;
      real_t hsrho = itT->diameter(listV_)/itT->inRadius(listV_);
      os <<"   "<<setw(8)<<setprecision(4)<< hsrho;
      if (hsrho < hsrhomin) hsrhomin = hsrho;
      if (hsrho > hsrhomax) hsrhomax = hsrho;
      real_t qual = hsrho/refhsrho;
      os << "   "<< qual << endl;
      if (qual < qualmin) qualmin = qual;
      if (qual > qualmax) qualmax = qual;
      sp += r;
   }
   os << "              --------   ------" << endl;
   os << "              " << setw(8)<<setprecision(6)<< sv;
   os << "   "<<setw(6)<<setprecision(2)<< sp << endl << endl;
   os << "Reference volume = " << setprecision(6)<< refvol << endl;
   os << "Min h/rho = " << hsrhomin << "  Max h/rho = " << hsrhomax << endl;
   os << "Min Quality = " << qualmin << " Max Quality = " << qualmax << endl;
   os << " For a regular tetrahedron, h/rho is RefRat = 2*sqrt(6) = " << 2*std::sqrt(6);
   os << endl;
   os << " Quality is the ratio (h/rho) / RefRat, so that the best quality is 1.";
   os << endl << endl;
   os.unsetf(ios::fixed);
}

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
/*!
 Create the initial mesh (or "seed" of the mesh) to be subdivided.
 */
void VolMeshTetSphere::initMesh(const vector<pair<real_t, dimen_t> >& rots, const int nboctants,
                                const real_t radius, const Point& Center,
                                number_t& VertexNum, number_t& ElementNum){
   vector<Point> Pt;
    Pt.push_back(Point(0,0,0));      // unused (to help numbering)
    Pt.push_back(Point(radius,0,0)); // Pt[1]
    Pt.push_back(Point(0,radius,0)); // Pt[2]
    Pt.push_back(Point(0,0,radius)); // Pt[3]
    Pt.push_back(Point(0,0,0));      // Pt[4]
    Pt.push_back(Point(-radius,0,0));// Pt[5]
    Pt.push_back(Point(0,-radius,0));// Pt[6]
    Pt.push_back(Point(0,0,-radius));// Pt[7]
   // Rotate the points starting from their initial position, then translate them to their final position.
   Vect OC=toVector(Point(0,0,0),Center);
   rotNtrans(rots, OC, Pt);

   DefaultGeometry *DG = new DefaultGeometry();
   SurfPlane *SP = new SurfPlane();
   SurfSphere *SS = new SurfSphere(Center,radius);
   const int V[] = {-1,0,1,2,3,4,5,6}; // to help numbering
   int iPh = 0, iPhI;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
   number_t mVN = minVertexNum_, sBND, sITF, sDOM;
   switch (int(std::abs(float(nboctants)))) {
   default:
//      Whole sphere = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
   case 0:
      title_ = "Ball - Tetrahedron mesh of the ball";
      {
        vector<Point> Pt; // Points defining an icosahedron
         real_t phi = (1+std::sqrt(5.))/2, bb = radius/std::sqrt(2+phi), aa=phi*bb;
         Pt.push_back(Point(0,0,0));     // unused (to help numbering)
         Pt.push_back(Point( aa,-bb,0)); // Pt[ 1]
         Pt.push_back(Point( aa, bb,0)); // Pt[ 2]
         Pt.push_back(Point(-aa, bb,0)); // Pt[ 3]
         Pt.push_back(Point(-aa,-bb,0)); // Pt[ 4]
         Pt.push_back(Point(0, aa,-bb)); // Pt[ 5]
         Pt.push_back(Point(0, aa, bb)); // Pt[ 6]
         Pt.push_back(Point(0,-aa, bb)); // Pt[ 7]
         Pt.push_back(Point(0,-aa,-bb)); // Pt[ 8]
         Pt.push_back(Point(-bb,0, aa)); // Pt[ 9]
         Pt.push_back(Point( bb,0, aa)); // Pt[10]
         Pt.push_back(Point( bb,0,-aa)); // Pt[11]
         Pt.push_back(Point(-bb,0,-aa)); // Pt[12]
         Pt.push_back(Point(0,0,0));     // Pt[13]
        // Rotate the points starting from their initial position, then translate them to their final position.
        Vect OC=toVector(Point(0,0,0),Center);
        rotNtrans(rots, OC, Pt);

       // Definition of 1 boundary, no interface and 1 subdomain
        number_t B_patch[] = {1}, B_dimen[] = {2};
        number_t I_patch[] = {0}, I_dimen[] = {0};
        number_t D_patch[] = {2}, D_dimen[] = {3};
        if (type_ == 0) {
         PatchGeometry *P_geom[] = {SP, DG}; // patch # 1, 2
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                       0,I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }
        else {
         delete SP; // not needed
         PatchGeometry *P_geom[] = {SS, DG}; // patch # 1, 2
         TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                       0,I_patch,I_dimen,
                        sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
        }

//       Description of the boundary patches (# 1)
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Boundary: faces of the icosahedron";
      }
      else {
         TG_.setDescription(++iPh) = "Boundary: The sphere centered at vertex 13";
      }
      sBND = TG_.sigma(iPh);
//       Description of the subdomain patches (# 2)
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

      //              0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13
      const int W[]={-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12}; // to help numbering
//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      sBND |= sDOM;
      listV_.push_back(Vertex(W[ 1]+mVN,sBND,Pt[ 1]));
      listV_.push_back(Vertex(W[ 2]+mVN,sBND,Pt[ 2]));
      listV_.push_back(Vertex(W[ 3]+mVN,sBND,Pt[ 3]));
      listV_.push_back(Vertex(W[ 4]+mVN,sBND,Pt[ 4]));
      listV_.push_back(Vertex(W[ 5]+mVN,sBND,Pt[ 5]));
      listV_.push_back(Vertex(W[ 6]+mVN,sBND,Pt[ 6]));
      listV_.push_back(Vertex(W[ 7]+mVN,sBND,Pt[ 7]));
      listV_.push_back(Vertex(W[ 8]+mVN,sBND,Pt[ 8]));
      listV_.push_back(Vertex(W[ 9]+mVN,sBND,Pt[ 9]));
      listV_.push_back(Vertex(W[10]+mVN,sBND,Pt[10]));
      listV_.push_back(Vertex(W[11]+mVN,sBND,Pt[11]));
      listV_.push_back(Vertex(W[12]+mVN,sBND,Pt[12]));
      listV_.push_back(Vertex(W[13]+mVN,sDOM,Pt[13]));
      VertexNum += 13;
      listT_.push_back(Tetrahedron(++ElementNum,W[1],W[2],W[10],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[2],W[1],W[11],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[3],W[4],W[9],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[4],W[3],W[12],W[13],4));

      listT_.push_back(Tetrahedron(++ElementNum,W[5],W[6],W[2],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[6],W[5],W[3],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[7],W[8],W[1],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[8],W[7],W[4],W[13],4));

      listT_.push_back(Tetrahedron(++ElementNum,W[9],W[10],W[6],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[10],W[9],W[7],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[11],W[12],W[5],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[12],W[11],W[8],W[13],4));

      listT_.push_back(Tetrahedron(++ElementNum,W[1],W[10],W[7],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[2],W[6],W[10],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[3],W[9],W[6],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[4],W[7],W[9],W[13],4));

      listT_.push_back(Tetrahedron(++ElementNum,W[1],W[8],W[11],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[2],W[11],W[5],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[3],W[5],W[12],W[13],4));
      listT_.push_back(Tetrahedron(++ElementNum,W[4],W[12],W[8],W[13],4));
      }
      break;
//      1 octant (1/8 sphere)  = = = = = = = = = = = = = = = = = = = = = = = = =
   case 1:
      title_ = "Ball - Tetrahedron mesh over 1 octant X>0, Y>0, Z>0";
      { // Definition of 4 boundaries and 1 subdomain
         number_t B_patch[] = {1,2,3,4}, B_dimen[] = {2,2,2,2};
         number_t I_patch[] =       {0}, I_dimen[] =       {0};
         number_t D_patch[] =       {5}, D_dimen[] =       {3};
         if (type_ == 0) {
            PatchGeometry *P_geom[] = {SP,SP,SP,SP, DG}; // patch # 1,2,3,4, 5
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                          0,I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         else {
            PatchGeometry *P_geom[] = {SP,SP,SP,SS, DG}; // patch # 1,2,3,4, 5
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                          0,I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to vertex 3";
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Boundary: Face opposite to the center point (vertex 4)";
      }
      else {
         //  portion of the sphere delimited by the 3 previous planes.
         TG_.setDescription(++iPh) = "Boundary: 1 octant of the sphere, defined by X>0, Y>0, Z>0,\n"
               "                   centered at vertex 4";
      }
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)|sDOM,Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(3)|TG_.sigma(1)|TG_.sigma(4)|sDOM,Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4)|sDOM,Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sDOM,Pt[4]));
      VertexNum += 4;
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
      break;
//      2 octants (1/4 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 2:
      title_ = "Ball - Tetrahedron mesh over 2 octants Y>0, Z>0";
      { // Definition of 3 boundaries, 1 interface and 1 subdomain
         number_t B_patch[] = {1,2,3}, B_dimen[] = {2,2,2};
         number_t I_patch[] =     {4}, I_dimen[] =     {2};
         number_t D_patch[] =     {5}, D_dimen[] =     {3};
         if (type_ == 0) {
            PatchGeometry *P_geom[] = {SP,SP,SP, SP, DG}; // patch # 1,2,3, 4, 5
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         else {
            PatchGeometry *P_geom[] = {SP,SP,SS, SP, DG}; // patch # 1,2,3, 4, 5
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: XZ plane";
      TG_.setDescription(++iPh) = "Boundary: XY plane";
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Boundary: The 2 faces opposite to the center point (vertex 4)";
      }
      else {
         TG_.setDescription(++iPh) = "Boundary: 2 octants of the sphere, defined by Y>0, Z>0,\n"
               "                   centered at vertex 4";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 4
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[1]));
      listV_.push_back(Vertex(V[2]+mVN,             TG_.sigma(2)|TG_.sigma(3)|sITF|sDOM,Pt[2]));
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)             |TG_.sigma(3)|sITF|sDOM,Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)             |sITF|sDOM,Pt[4]));
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)     |sDOM,Pt[5]));
      VertexNum += 5;
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[2],V[5],V[3],V[4],4));
      break;
//      3 octants (3/8 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 3:
      title_ = "Ball - Tetrahedron mesh over 3 octants Z>0, minus the octant X>0, Y<0";
      { // Definition of 3 boundaries, 2 interfaces and 1 subdomain
         number_t B_patch[] = {1,2,3,4}, B_dimen[] = {2,2,2,2};
         number_t I_patch[] =     {5,6}, I_dimen[] =     {2,2};
         number_t D_patch[] =       {7}, D_dimen[] =       {3};
         if (type_ == 0) {
            PatchGeometry *P_geom[] = {SP,SP,SP,SP, SP,SP, DG}; // patch # 1,2,3,4, 5,6, 7
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         else {
            PatchGeometry *P_geom[] = {SP,SP,SP,SS, SP,SP, DG}; // patch # 1,2,3,4, 5,6, 7
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to vertex 3";
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Boundary: The 3 faces opposite to the center point (vertex 4)";
      }
      else {
         TG_.setDescription(++iPh) = "Boundary: 3 octants of the sphere defined by the half-sphere Z>0\n"
               "                   minus the octant X>0, Y<0, centered at vertex 4";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 5
      TG_.setDescription(++iPh) = "Interface: XZ plane";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)     |sDOM,Pt[1]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[2]+mVN,             TG_.sigma(3)|TG_.sigma(4)|sITF|sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4)|sITF|sDOM,Pt[3]));
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[5]+mVN,             TG_.sigma(3)|TG_.sigma(4)|sITF|sDOM,Pt[5]));
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(3)|TG_.sigma(4)     |sDOM,Pt[6]));
      VertexNum += 6;
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[2],V[5],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[5],V[6],V[3],V[4],4));
      break;
//      4 octants (1/2 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 4:
      title_ = "Ball - Tetrahedron mesh over 4 octants Z>0";
      if (type_ == 0 || nboctants > 0) {
            { // Definition of 2 boundaries, 2 interfaces and 1 subdomain
            number_t B_patch[] = {1,2}, B_dimen[] = {2,2};
            number_t I_patch[] = {3,4}, I_dimen[] = {2,2};
            number_t D_patch[] =   {5}, D_dimen[] =   {3};
            if (type_ == 0) {
               PatchGeometry *P_geom[] = {SP,SP, SP,SP, DG}; // patch # 1,2, 3,4, 5
               TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                              sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                              sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
               }
            else {
               PatchGeometry *P_geom[] = {SP,SS, SP,SP, DG}; // patch # 1,2, 3,4, 5
               TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                              sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                              sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         }
//       Description of the boundary patches
         TG_.setDescription(++iPh) = "Boundary: XY plane";
         if (type_ == 0) {
            TG_.setDescription(++iPh) = "Boundary: The 4 faces opposite to the center point (vertex 4)\n"
                  "                   in the half-space Z>0";
         }
         else {
            TG_.setDescription(++iPh) = "Boundary: 4 octants of the sphere (half-sphere), defined by Z>0";
         }
//       Description of the interface patches
         TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 3
         TG_.setDescription(++iPh) = "Interface: XZ plane";
//       Description of the subdomain patches
         TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
           sITF = TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)|TG_.sigma(2)|sITF|sDOM,Pt[1]));
           sITF = TG_.sigma(iPhI);
         listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|TG_.sigma(2)|sITF|sDOM,Pt[2]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[3]+mVN,             TG_.sigma(2)|sITF|sDOM,Pt[3]));
         listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)             |sITF|sDOM,Pt[4]));
           sITF = TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|TG_.sigma(2)|sITF|sDOM,Pt[5]));
           sITF = TG_.sigma(iPhI);
         listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(2)|sITF|sDOM,Pt[6]));
         VertexNum += 6;
         listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[2],V[5],V[3],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[5],V[6],V[3],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[6],V[1],V[3],V[4],4));
      }
      else {
         // With 1 initial tetrahedron, 2 boundary patches are not sufficient: we need 4 patches
         // because, for example, "cutting the corner at point 1" (by subdivision) leads to an
         // internal face which would be considered lying on the boundary since its 3 vertices
         // would belong to the same boundary.
         { // Definition of 4 boundaries and 1 subdomain
           // The boundary associated to the sphere is the union
           // of the 3 patches opposite to vertices 1, 2 and 3.
            number_t B_patch[] = {1,2,3,4}, B_dimen[] = {2,2,2,2};
            number_t I_patch[] =       {0}, I_dimen[] =       {0};
            number_t D_patch[] =       {5}, D_dimen[] =       {3};
            PatchGeometry *P_geom[] = {SS,SS,SS, SP, DG}; // patch # 1,2,3,4, 5
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                          0,I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
//       Description of the boundary patches
         TG_.setDescription(++iPh) = "Boundary: Third of the half-sphere opposite to vertex 1";
         TG_.setDescription(++iPh) = "Boundary: Third of the half-sphere opposite to vertex 2";
         TG_.setDescription(++iPh) = "Boundary: Third of the half-sphere opposite to vertex 3";
         TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to vertex 4";
//       Description of the subdomain patches
         TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
         real_t ang = 2.*pi_/3., rc = radius*std::cos(ang), rs = radius*std::sin(ang);
         vector<Point> Pti;
         Pti.push_back(Point(rc, rs,0));
         Pti.push_back(Point(rc,-rs,0));
         rotNtrans(rots, OC, Pti);
         listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)|sDOM,Pt[1]));
         listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(3)|TG_.sigma(1)|TG_.sigma(4)|sDOM,Pti[0]));
         listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4)|sDOM,Pti[1]));
         listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sDOM,Pt[3]));
         VertexNum += 4;
         listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[4],V[3]));
      }
      break;
//      5 octants (5/8 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 5:
      title_ = "Ball - Tetrahedron mesh over 5 octants, 4 with Z>0, plus the octant X>0, Y>0, Z<0";
      { // Definition of 6 boundaries, 3 interfaces and 1 subdomain
         number_t B_patch[] = {1,2,3,4,5,6}, B_dimen[] = {2,2,2,2,2,2};
         number_t I_patch[] =       {7,8,9}, I_dimen[] =       {2,2,2};
         number_t D_patch[] =          {10}, D_dimen[] =           {3};
         if (type_ == 0) {
            PatchGeometry *P_geom[] = {SP,SP,SP,SP,SP,SP, SP,SP,SP, DG}; // patch # 1,2,3,4,5,6, 7,8,9, 10
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         else {
            PatchGeometry *P_geom[] = {SP,SP,SP,SP,SP,SS, SP,SP,SP, DG}; // patch # 1,2,3,4,5,6, 7,8,9, 10
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: XY plane with X<0 and Y>0, opposite to vertex 3";
      TG_.setDescription(++iPh) = "Boundary: XY plane with X<0 and Y<0, opposite to vertex 3";
      TG_.setDescription(++iPh) = "Boundary: XY plane with X>0 and Y<0, opposite to vertex 3";
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Boundary: The 5 faces opposite to the center point (vertex 4)";
      }
      else {
         TG_.setDescription(++iPh) = "Boundary: 5 octants of the sphere defined by the half-sphere Z>0\n"
               "                     plus the octant X>0, Y>0, Z<0, centered at vertex 4";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 7
      TG_.setDescription(++iPh) = "Interface: XZ plane";
      TG_.setDescription(++iPh) = "Interface: XY plane";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(5)|TG_.sigma(6)|sITF|sDOM,Pt[1]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|TG_.sigma(3)|TG_.sigma(6)|sITF|sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[3]+mVN,                          TG_.sigma(6)|sITF|sDOM,Pt[3]));
        sBND = TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)|TG_.sigma(5);
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[4]+mVN,                                  sBND|sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(3)|TG_.sigma(4)|TG_.sigma(6)|sITF|sDOM,Pt[5]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(4)|TG_.sigma(5)|TG_.sigma(6)|sITF|sDOM,Pt[6]));
      listV_.push_back(Vertex(V[7]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(6)     |sDOM,Pt[7]));
      VertexNum += 7;
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[2],V[5],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[5],V[6],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[6],V[1],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[7],V[2],V[4],4));
      break;
//      6 octants (3/4 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 6:
      title_ = "Ball - Tetrahedron mesh over 6 octants, 4 with Z>0, plus 2 octants Y>0, Z<0";
      { // Definition of 3 boundaries, 3 interfaces and 1 subdomain
         number_t B_patch[] = {1,2,3}, B_dimen[] = {2,2,2};
         number_t I_patch[] = {4,5,6}, I_dimen[] = {2,2,2};
         number_t D_patch[] =     {7}, D_dimen[] =     {3};
         if (type_ == 0) {
            PatchGeometry *P_geom[] = {SP,SP,SP, SP,SP,SP, DG}; // patch # 1,2,3, 4,5,6, 7
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         else {
            PatchGeometry *P_geom[] = {SP,SP,SS, SP,SP,SP, DG}; // patch # 1,2,3, 4,5,6, 7
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: XZ plane";
      TG_.setDescription(++iPh) = "Boundary: XY plane";
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Boundary: The 6 faces opposite to the center point (vertex 4)";
      }
      else {
         TG_.setDescription(++iPh) = "Boundary: 6 octants of the sphere defined by the half-sphere Z>0\n"
               "                   plus the 2 octants Y>0, Z<0, centered at vertex 4";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 4
      TG_.setDescription(++iPh) = "Interface: XZ plane";
      TG_.setDescription(++iPh) = "Interface: XY plane";
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sITF|sDOM,Pt[1]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[2]+mVN,                          TG_.sigma(3)|sITF|sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[3]+mVN,                          TG_.sigma(3)|sITF|sDOM,Pt[3]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)             |sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sITF|sDOM,Pt[5]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[6]+mVN,             TG_.sigma(2)|TG_.sigma(3)|sITF|sDOM,Pt[6]));
        sITF = TG_.sigma(iPhI);
      listV_.push_back(Vertex(V[7]+mVN,TG_.sigma(1)             |TG_.sigma(3)|sITF|sDOM,Pt[7]));
      VertexNum += 7;
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[2],V[5],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[5],V[6],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[6],V[1],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[7],V[2],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[2],V[7],V[5],V[4],4));
      break;
//      7 octants (7/8 sphere) = = = = = = = = = = = = = = = = = = = = = = = = =
   case 7:
      title_ = "Ball - Tetrahedron mesh over 7 octants, 8 octants minus the octant X>0, Y<0, Z<0";
      { // Definition of 4 boundaries, 3 interfaces and 1 subdomain
         number_t B_patch[] =      {1,2,3,4}, B_dimen[] =     {2,2,2,2};
         number_t I_patch[] = {5,6,7,8,9,10}, I_dimen[] = {2,2,2,2,2,2};
         number_t D_patch[] =           {11}, D_dimen[] =           {3};
         if (type_ == 0) {
            PatchGeometry *P_geom[] = {SP,SP,SP,SP, SP,SP,SP,SP,SP,SP, DG}; // patch # 1,2,3,4, 5,6,7,8,9,10, 11
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         else {
            PatchGeometry *P_geom[] = {SP,SP,SP,SS, SP,SP,SP,SP,SP,SP, DG}; // patch # 1,2,3,4, 5,6,7,8,9,10, 11
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                           sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
      }
//       Description of the boundary patches
      TG_.setDescription(++iPh) = "Boundary: YZ plane, opposite to vertex 1";
      TG_.setDescription(++iPh) = "Boundary: XZ plane, opposite to vertex 2";
      TG_.setDescription(++iPh) = "Boundary: XY plane, opposite to vertex 3";
      if (type_ == 0) {
         TG_.setDescription(++iPh) = "Boundary: The 7 faces opposite to the center point (vertex 4)";
      }
      else {
         TG_.setDescription(++iPh) = "Boundary: The sphere minus the octant X>0, Y<0, Z<0 centered at vertex 4";
      }
//       Description of the interface patches
      TG_.setDescription(++iPh) = "Interface: YZ plane, Z>=0"; iPhI = iPh; // patch iPhI (5)
      TG_.setDescription(++iPh) = "Interface: XZ plane, Z>=0";             // patch iPhI+1
      TG_.setDescription(++iPh) = "Interface: XY plane, Y>=0";             // patch iPhI+2
      TG_.setDescription(++iPh) = "Interface: XY plane, Y<=0";             // patch iPhI+3
      TG_.setDescription(++iPh) = "Interface: YZ plane, Z<=0";             // patch iPhI+4
      TG_.setDescription(++iPh) = "Interface: XZ plane, Z<=0";             // patch iPhI+5
//       Description of the subdomain patches
      TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
      listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)|sITF|sDOM,Pt[1]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2)|TG_.sigma(iPhI+4);
      listV_.push_back(Vertex(V[2]+mVN,                          TG_.sigma(4)|sITF|sDOM,Pt[2]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
      listV_.push_back(Vertex(V[3]+mVN,                          TG_.sigma(4)|sITF|sDOM,Pt[3]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2)|TG_.sigma(iPhI+3)|TG_.sigma(iPhI+4)|TG_.sigma(iPhI+5);
      listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sITF|sDOM,Pt[4]));
        sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2)|TG_.sigma(iPhI+3)|TG_.sigma(iPhI+5);
      listV_.push_back(Vertex(V[5]+mVN,                          TG_.sigma(4)|sITF|sDOM,Pt[5]));
        sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+3);
      listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|TG_.sigma(3)|TG_.sigma(4)|sITF|sDOM,Pt[6]));
        sITF = TG_.sigma(iPhI+4)|TG_.sigma(iPhI+5);
      listV_.push_back(Vertex(V[7]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4)|sITF|sDOM,Pt[7]));
      VertexNum += 7;
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[2],V[5],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[5],V[6],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[6],V[1],V[3],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[1],V[7],V[2],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[2],V[7],V[5],V[4],4));
      listT_.push_back(Tetrahedron(++ElementNum,V[5],V[7],V[6],V[4],4));
      break;
//      8 octants (sphere) = = = = = = = = = = = = = = = = = = = = = = = = = = =
   case 8:
      title_ = "Ball - Tetrahedron mesh over 8 octants";
      if (type_ == 0 || nboctants > 0) {
         { // Definition of 1 boundary, 3 interfaces and 1 subdomain
            number_t B_patch[] =     {1}, B_dimen[] =     {2};
            number_t I_patch[] = {2,3,4}, I_dimen[] = {2,2,2};
            number_t D_patch[] =     {5}, D_dimen[] =     {3};
            if (type_ == 0) {
               PatchGeometry *P_geom[] = {SP, SP,SP,SP, DG}; // patch # 1, 2,3,4, 5
               TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                              sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                              sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
               }
            else {
               PatchGeometry *P_geom[] = {SS, SP,SP,SP, DG}; // patch # 1, 2,3,4, 5
               TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                              sizeof(I_patch)/sizeof(number_t),I_patch,I_dimen,
                              sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
            }
         }
//       Description of the boundary patches
         if (type_ == 0) {
            TG_.setDescription(++iPh) = "Boundary: The 8 faces opposite to the center point (vertex 4)";
         }
         else {
            TG_.setDescription(++iPh) = "Boundary: The sphere centered at vertex 4";
         }
//       Description of the interface patches
         TG_.setDescription(++iPh) = "Interface: YZ plane"; iPhI = iPh; // patch 2
         TG_.setDescription(++iPh) = "Interface: XZ plane";
         TG_.setDescription(++iPh) = "Interface: XY plane";
//       Description of the subdomain patches
         TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
           sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(1)|sITF|sDOM,Pt[1]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(1)|sITF|sDOM,Pt[2]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|sITF|sDOM,Pt[3]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[4]+mVN,             sITF|sDOM,Pt[4]));
           sITF = TG_.sigma(iPhI+1)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[5]+mVN,TG_.sigma(1)|sITF|sDOM,Pt[5]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+2);
         listV_.push_back(Vertex(V[6]+mVN,TG_.sigma(1)|sITF|sDOM,Pt[6]));
           sITF = TG_.sigma(iPhI)|TG_.sigma(iPhI+1);
         listV_.push_back(Vertex(V[7]+mVN,TG_.sigma(1)|sITF|sDOM,Pt[7]));
         VertexNum += 7;
         listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[2],V[5],V[3],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[5],V[6],V[3],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[6],V[1],V[3],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[1],V[7],V[2],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[2],V[7],V[5],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[5],V[7],V[6],V[4],4));
         listT_.push_back(Tetrahedron(++ElementNum,V[6],V[7],V[1],V[4],4));
      }
      else {
         // With 1 initial tetrahedron, we need 4 boundary patches to define the boundary.
         { // Definition of 4 boundaries, 3 interfaces and 1 subdomain
            number_t B_patch[] = {1,2,3,4}, B_dimen[] = {2,2,2,2};
            number_t I_patch[] =       {0}, I_dimen[] =       {0};
            number_t D_patch[] =       {5}, D_dimen[] =       {3};
            delete SP; // not needed
            PatchGeometry *P_geom[] = {SS,SS,SS,SS, DG}; // patch # 1,2,3,4, 5
            TG_ = TopoGeom(sizeof(B_patch)/sizeof(number_t),B_patch,B_dimen,
                                                          0,I_patch,I_dimen,
                           sizeof(D_patch)/sizeof(number_t),D_patch,D_dimen, P_geom);
         }
//       Description of the boundary patches
         TG_.setDescription(++iPh) = "Boundary: Quarter of the sphere opposite to vertex 1";
         TG_.setDescription(++iPh) = "Boundary: Quarter of the sphere opposite to vertex 2";
         TG_.setDescription(++iPh) = "Boundary: Quarter of the sphere opposite to vertex 3";
         TG_.setDescription(++iPh) = "Boundary: Quarter of the sphere opposite to vertex 4";
//       Description of the subdomain patches
         TG_.setDescription(++iPh) = "Interior of the domain"; sDOM = TG_.sigma(iPh);

//       Definition of the initial vertices, taking into account the boundaries,
//       interfaces and subdomains they belong to.
         real_t hcd = radius/std::sqrt(3);
         vector<Point> Pti;
         Pti.push_back(Point( hcd,-hcd,-hcd));
         Pti.push_back(Point(-hcd, hcd,-hcd));
         Pti.push_back(Point( hcd, hcd, hcd));
         Pti.push_back(Point(-hcd,-hcd, hcd));
         rotNtrans(rots, OC, Pti);
         listV_.push_back(Vertex(V[1]+mVN,TG_.sigma(2)|TG_.sigma(3)|TG_.sigma(4)|sDOM,Pti[0]));
         listV_.push_back(Vertex(V[2]+mVN,TG_.sigma(3)|TG_.sigma(1)|TG_.sigma(4)|sDOM,Pti[1]));
         listV_.push_back(Vertex(V[3]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(4)|sDOM,Pti[2]));
         listV_.push_back(Vertex(V[4]+mVN,TG_.sigma(1)|TG_.sigma(2)|TG_.sigma(3)|sDOM,Pti[3]));
         VertexNum += 4;
         listT_.push_back(Tetrahedron(++ElementNum,V[1],V[2],V[3],V[4]));
      }
      break;
   }
   if (type_ == 0) { delete SS; }
}

} // end of namespace subdivision
} // end of namespace xlifepp
