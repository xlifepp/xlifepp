/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TriangleMesh.hpp
  \author Y. Lafranche
  \since 24 May 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::TriangleMesh class

  This class is an abstract class that holds a mesh of triangles built
  by successive subdivisions of an initial elementary mesh consisting of several
  triangles.

  The numbering convention used to denote the vertices and edges of the
  triangles is described on top of the header file of the class xlifepp::subdivision::Triangle,
  namely Triangle.hpp.

  inheriting classes
  ------------------
   - xlifepp::subdivision::SurfMeshTriCone
   - xlifepp::subdivision::SurfMeshTriCylinder
   - xlifepp::subdivision::SurfMeshTriDisk
   - xlifepp::subdivision::SurfMeshTriSet
   - xlifepp::subdivision::SurfMeshTriSphere
*/

#ifndef TRIANGLE_MESH_HPP
#define TRIANGLE_MESH_HPP

#include "Triangle.hpp"
#include "SimplexMesh.hpp"

#include <iostream>
#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class TriangleMesh
*/
class TriangleMesh : public SimplexMesh<Triangle>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   TriangleMesh(const number_t nbsubdiv, const number_t order, const number_t type,
                const number_t minVertexNum, const number_t minElementNum)
: SimplexMesh<Triangle>(nbsubdiv, order, type, minVertexNum, minElementNum,
                        (order+1)*(order+2)/2, 4)
{}

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   /*!
     prints, on stream ftex, Fig4TeX instructions to draw the numbering
     convention for a triangle mesh of order k
    */
   virtual void printTeXNumberConvention(std::ostream& ftex) const ;
   virtual void printTeXNumberConvention(PrintStream& ftex) const {printTeXNumberConvention(ftex.currentStream());}

protected:
//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! subdivision of a triangle into 4 triangles
   void algoSubdiv(const Triangle& T, number_t& ElementNum, number_t& VertexNum,
                   std::vector<Triangle>& listT, map_pair_num& SeenEdges);// override

   /*!
     create or retrieve high order vertices inside the face numFace of element Elem
     -> this function is irrelevant in the case of a triangle
    */
   virtual void createHOfV(Triangle& Elem, const number_t order, number_t& VertexNum,
                           const number_t numFace, map_set_pair_in& SeenFaces){/* unused */}

   //! create high order vertices inside the triangle
   virtual void createHOiV(Triangle&  Elem, const number_t order, number_t& VertexNum);


   //! create initial mesh for a geometry of revolution
   void initMesh(const number_t nbslices,
                 const real_t radius1, const real_t radius2, const std::vector<Point>& CharacPts,
                 number_t& VertexNum, number_t& ElementNum, const std::vector<ShapeInfo>& vSI);
   //! utilitary function used by previous one
   void startConeTrunk(real_t R1, const Point& P1, const GeomEndShape bnd1, const std::string& bottomPt,
                       const std::vector <PatchGeometry *>& EBS,
                       const SurfCone *SC, real_t slice_height, int nb_sl, bool iscone, refnum_t sBEs1,
                       number_t& iPh, number_t& iPhSD, number_t& VertexNum, number_t& ElementNum,
                       std::vector<Point>& Pt, real_t& prevRitrf, number_t& rVaFirst);

   //! create triangles that subdivide a quadrangle
   void subdivQuadrangle(const number_t U1, const number_t U2, const number_t U3, const number_t U4,
                         number_t& ElementNum);

   //--- Fig4TeX printing functions

   //! prints, on stream ftex, definition of some TeX macros
   virtual void printTeXHeader(std::ostream& ftex) const;
   virtual void printTeXHeader(PrintStream& ftex) const {printTeXHeader(ftex.currentStream());}

   /*!
     prints, on stream ftex, Fig4TeX instructions to define points to be used
     for the display of the mesh
    */
   virtual void printTeXPoints(std::ostream& ftex, const bool withInterface) const ;
   virtual void printTeXPoints(PrintStream& ftex, const bool withInterface) const {printTeXPoints(ftex.currentStream(), withInterface);}

   /*!
     prints, on stream ftex, Fig4TeX instructions to draw faces or edges of a mesh
     belonging to areas of kind TA, sorted along a direction defined by psi and theta.
    */
   virtual void printTeXSortedAreaFoE(std::ostream& ftex, const topologicalArea TA,
                                      const float psi, const float theta) const ;
   virtual void printTeXSortedAreaFoE(PrintStream& ftex, const topologicalArea TA,
                                      const float psi, const float theta) const
          {printTeXSortedAreaFoE(ftex.currentStream(), TA, psi, theta);}

}; // end of Class TriangleMesh

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* TRIANGLE_MESH_HPP */
