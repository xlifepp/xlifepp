/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VolMeshHexCube.hpp
  \author Y. Lafranche
  \since 24 Feb 2014
  \date 24 Feb 2014

  \brief Definition of the xlifepp::subdivision::VolMeshHexCube class

  This class is a class that holds a mesh of hexahedra built by
  successive subdivisions of an initial elementary mesh consisting of n groups
  of hexahedra, n=1,...8. Each group of hexahedra fills a cube which is lying
  in one octant of the 3D space around the origin.

  The mesh of the "big" cube is a special case. This big cube can be considered
  as the juxtaposition of 8 small cubes or as itself, leading to two meshes
  obtained by subdividing the 8 small cubes or by subdividing directly this big
  cube.

  The different geometrical configurations that can be obtained are shown in the
  file ./doc/demoCubeHexa.pdf.
*/

#ifndef VOL_MESH_HEX_CUBE_HPP
#define VOL_MESH_HEX_CUBE_HPP

#include <iostream>
#include <map>
#include <vector>

#include "HexahedronMesh.hpp"

namespace xlifepp {
namespace subdivision {

/*!
   \class VolMeshHexCube
*/
class VolMeshHexCube : public HexahedronMesh
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   VolMeshHexCube(const std::vector<std::pair<real_t, dimen_t> >& rots, const int nboctants,
                  const number_t nbsubdiv=0, const number_t order=1,
                  const real_t edgeLength=1., const Point Center=Point(0,0,0),
                  const number_t minVertexNum=1, const number_t minElementNum=1);

private:

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
   //! create initial mesh
   void initMesh(const std::vector<std::pair<real_t, dimen_t> >& rots, const int nboctants,
                 const real_t edgeLength, const Point& Center,
                 number_t& VertexNum, number_t& ElementNum);

}; // end of Class VolMeshHexCube

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* VOL_MESH_HEX_CUBE_HPP */
