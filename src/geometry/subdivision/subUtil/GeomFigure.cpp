/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomFigure.cpp
  \author Y. Lafranche
  \since 05 Apr 2008
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::GeomFigure class members and related functions
*/

#include "GeomFigure.hpp"

#include <algorithm>
using namespace std;

namespace xlifepp {
namespace subdivision {

/*!
 Access function returning the edge number i, i in {1,...nb_edges_}. The edge is
 defined by the rank of the vertices of order 1 in the vertex list.
 */
pair_nn GeomFigure::rkOfO1VeOnEdge(const number_t i) const {
   int indEdge = i-1;
   return make_pair(vertices_[getrkEdge(indEdge,0)], vertices_[getrkEdge(indEdge,1)]);
}

/*!
 Access function returning the face number i, i in {1,...nb_faces_}. The face is
 defined by the rank of the vertices of order 1 in the vertex list.
 */
vector<number_t> GeomFigure::rkOfO1VeOnFace(const number_t i) const {
   number_t nvf(numberOfVertByFace());
   vector<number_t> V(nvf);
   int indFace = i-1;
   for (number_t j=0; j<nvf; j++) {V[j] = vertices_[getrkFace(indFace,j)];}
   return V;
}

/*!
 Access function returning the high order vertices (given by their rank) of edge number i >= 1
 The order is not stored in this class, so it needs to be passed by argument ; it should be
 the correct one, otherwise the result is false.
 */
vector<number_t> GeomFigure::rkOfHOVeOnEdge(const number_t Order, const number_t i) const{
   number_t km1=Order-1;
   vector<number_t> V(km1);
   for (number_t j=0, k=numberOfO1Vertices()+(i-1)*km1; j<km1; j++,k++) { V[j] = vertices_[k]; }
   return V;
}
/*!
 Access function returning the high order vertices (given by their rank) of face number i >= 1
 The order is not stored in this class, so it needs to be passed by argument ; it should be
 the correct one, otherwise the result is false.
 For the triangle and the quadrangle, the face is the element itself and the input number i
 should be equal to 1.
 */
vector<number_t> GeomFigure::rkOfHOVeOnFace(const number_t Order, const number_t i) const{
   number_t km1=Order-1, km1sq=km1*km1;
   vector<number_t> V(km1sq);
   for (number_t j=0, k=numberOfO1Vertices()+km1*numberOfEdgesinFig()+(i-1)*km1sq; j<km1sq; j++,k++)
   { V[j] = vertices_[k]; }
   return V;
}

/*!
  returns a normal vector to the face number i >= 1
  The vector is not normalized and is oriented towards the exterior of the element.
 */
vector<real_t> GeomFigure::extNormVec(const number_t i, const vector<Vertex>& listV) const{
   int indFace = i-1;
   // Get the first 3 vertices of the face (P1,P2,P3)
   Vect V1 = toVector(listV[vertices_[getrkFace(indFace,0)]].geomPt(),listV[vertices_[getrkFace(indFace,1)]].geomPt());
   Vect V2 = toVector(listV[vertices_[getrkFace(indFace,0)]].geomPt(),listV[vertices_[getrkFace(indFace,2)]].geomPt());

   // The returned vector is P1P2 x P1P3 or the opposite
   if (faceOrientation(indFace) > 0) { return crossProduct(V1,V2); }
   else                              { return crossProduct(V2,V1); }
}

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
/*!
 Prints, on stream os, the definition of an element.
 The vertices are given by their rank in the general list of vertices
 of class GeomFigureMesh.
 */
void GeomFigure::print(ostream& os) const {
   os << "Element " << num_ << ": ";
   for (size_t i=0; i<vertices_.size(); i++) {os << vertices_[i] << " ";}
   os << endl;
}

} // end of namespace subdivision
} // end of namespace xlifepp
