/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VolMeshTetRev.cpp
  \author Y. Lafranche
  \since 02 June 2015
  \date 02 June 2015

  \brief Implementation of xlifepp::subdivision::VolMeshTetCone and xlifepp::subdivision::VolMeshTetCylinder class members and related functions
*/

#include "VolMeshTetRev.hpp"
#include <sstream>

using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of tetrahedrons by successive subdivisions in a cone or a truncated cone.
 The cylinder is a particular kind of truncated cone which has a special constructor below.

 \param nbslices: number of slices of elements orthogonal to the axis of the cone
            (0 by default, which means that the number of slices is automatically
            computed ; if nbslices>0, the given value has precedence and is taken
            into account)

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each tetrahedron is subdivided into 8 tetrahedrons with the
   same orientation as the original one: if the first tetrahedron is (1,2,3,4), the
   vectors 12x13, 41x43, 43x42 and 42x41 define the exterior normals to the faces.

 \param order: order of the tetrahedra in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   tetrahedron is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, the vertices
   belonging to the appropriate boundary lie on the surface of the cone.

 \param type: type of the subdivision (1 by default)
   type equal to 0 leads to a simple (flat) subdivision of the initial
        mesh where new vertices are all the midpoints of the edges.
   type equal to 1 leads to a subdivision where the boundary vertices are computed
        so that they lie on the corresponding surface

 \param radius1       : radius of the basis of the cone containing P1 (1. by default)
 \param radius2       : radius of the other basis of the cone containing P2 (0.5 by default)
 \param P1, P2        : the two end points of the axis of the cone
 \param endShape1     : shape of the end surface of the cone on P1 or P2 side
 \param endShape2     : (Flat by default). The possible values are of GeomEndShape type.
 \param distance1     : distance between P1 (resp. P2) and the apex of the surface when the
 \param distance2     : previous parameter is non Flat (default=0.)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)

 Nota: If none of the radii is null, this defines a truncated cone. The parameters endShape1
        ans endShape2 are both applicable.
        If one of the radii is null, the corresponding point, P1 or P2, is the apex of the cone
        and the corresponding endShape is not applicable.
 */
VolMeshTetCone::VolMeshTetCone(const number_t nbslices,
                               const number_t nbsubdiv, const number_t order,
                               const number_t type, const real_t radius1, const real_t radius2,
                               const Point P1, const Point P2,
                               const GeomEndShape endShape1, const GeomEndShape endShape2,
                               const real_t distance1, const real_t distance2,
                               const number_t minVertexNum, const number_t minElementNum)
:TetrahedronMesh(nbsubdiv, order,type, minVertexNum, minElementNum) {
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   vector<Point> CP;   CP.push_back(P1); CP.push_back(P2);
   vector<ShapeInfo> vSI;
    vSI.push_back(ShapeInfo(endShape1,distance1));
    vSI.push_back(ShapeInfo(endShape2,distance2));
   if (radius1 > 0. && radius2 > 0.) {// truncated cone
      initMesh(nbslices,radius1,radius2,CP,VertexNum,ElementNum,vSI);
   }
   else {// "true" cone: one of the radii is null
      initMeshCone(nbslices,radius1,radius2,CP,VertexNum,ElementNum,vSI);
   }
   buildNcheck(VertexNum);
}

//-------------------------------------------------------------------------------
//  Private member functions
//-------------------------------------------------------------------------------
/*!
 Create the initial mesh (or "seed" of the mesh) to be subdivided for a true cone,
 i.e. defined by an axis of revolution and one circular section.

 In the following, the word "object" is used to designate the cone.

 nbslices: number of slices of elements orthogonal to the axis of the object
               If this input value is equal to 0, a number of slices is computed from
               the geometrical data.
 radius: radius of the section of the basis of the object
 CharacPts: the two end points of the axis of the object ; the first one is the center
               of the basis, the second one is the apex of the cone.
 swappedPts: false if the points in CharacPts are the original data points (in the order
               given by the user), true if they have been swapped, since here we need P2
               to be the apex of the cone.
 VertexNum: number of the last vertex created (modified on output)
 ElementNum: number of the last element created (modified on output)
 SI: shape information at the end of the object (on the basis side)
 */
void VolMeshTetCone::initMeshCone(const number_t nbslices, const real_t radius1, const real_t radius2,
                                  const vector<Point>& CharacPts,
                                  number_t& VertexNum, number_t& ElementNum,
                                  const vector<ShapeInfo>& vSI) {
   real_t R1, R2, slice_height;
   Point P1, P2;
   vector<ShapeInfo> cvSI;
   string bottomPt, topPt, shape;
   bool iscone;
   DefaultGeometry *DG;
   SurfPlane *SP;
   SurfCone *SC;
   vector <PatchGeometry *> EBS;
   int nb_sl;
   initRevMesh(nbslices, radius1,radius2, CharacPts, vSI,
               R1,R2, P1,P2, cvSI, bottomPt, topPt, iscone, shape, DG, SP, SC, EBS, nb_sl, slice_height);

   delete EBS[1];// discard this unused end shape
   number_t iPh = 0, iPhSD;
   title_ = shape + " - Tetrahedron mesh";
   { // Definition of 2 boundaries...
      //vector<number_t> B_patch{1,2}, B_dimen{2,2}; Ready for C++11
      vector<number_t> B_patch(2, 1), B_dimen(2, 2);
      B_patch[1] = 2;
      number_t nbBound = B_patch.size();
     // ... nbIntrf interfaces...
     // (two orthogonal internal planes containing the axis, a plane between
     //  two successive slices, plus eventually one plane between the end slice
     //  and the corresponding "hat" end shape)
      number_t nbIntrf = 2 + nb_sl-1, nbSbDom = nb_sl;
      if (EBS[0]->curvedShape()) {nbIntrf++; nbSbDom++;} // if non Flat
      vector<number_t> I_patch(nbIntrf), I_dimen(nbIntrf);
      number_t numPa = nbBound;
      for (size_t i=0; i<nbIntrf; i++) { I_patch[i] = ++numPa; I_dimen[i] = 2; }
     // ...and nbSbDom subdomains
     // (each slice plus eventually one "hat" end shape)
      vector<number_t> D_patch(nbSbDom), D_dimen(nbSbDom);
      for (size_t i=0; i<nbSbDom; i++) { D_patch[i] = ++numPa; D_dimen[i] = 3; }

      number_t nbBI =  nbBound + nbIntrf;
      number_t nbPatches =  nbBI + nbSbDom;
      vector<PatchGeometry *> P_geom(nbPatches);
      P_geom[0] = EBS[0];
      for (size_t i=1; i<nbBI; i++) { P_geom[i] = SP; }
      for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = DG; }

      if (type_ == 0) {
         // P_geom = { EBS[0],   SP, SP,...   SP, DG,...   DG}
         //           \_ nbBound _/  \_nbIntrf_/  \_nbSbDom_/
         delete SC; // not needed
      }
      else {
         // P_geom = { EBS[0],   SC, SP,...   SP, DG,...   DG}
         //           \_ nbBound _/  \_nbIntrf_/  \_nbSbDom_/
         P_geom[1] = SC;
      }
      TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
      iPhSD = nbBound+nbIntrf;
   }
   refnum_t sBEs1, sBS, sIIp1, sIIp2; // localization code of the following patches
//       Description of the boundary patches
   TG_.setDescription(++iPh) = "Boundary: End surface on the side of end point " + bottomPt; sBEs1 = TG_.sigma(iPh); // patch 1
   TG_.setDescription(++iPh) = "Boundary: Surface of the " + shape;                          sBS   = TG_.sigma(iPh); // patch 2
//       Description of the first two interface patches
   TG_.setDescription(++iPh) = "Interface: Internal plane 1 containing axis";                sIIp1 = TG_.sigma(iPh); // patch 3
   number_t iPhIIp1 = iPh;
   TG_.setDescription(++iPh) = "Interface: Internal plane 2 containing axis";                sIIp2 = TG_.sigma(iPh); // patch 4

// Steps 1 and 2 of the construction
   vector<Point> Pt;
   real_t prevRitrf;
   number_t rVaFirst, rVc;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
   startConeTrunk(R1, P1, EBS, SC, slice_height, nb_sl, iscone, sBEs1, sBS, sIIp1, sIIp2, iPhIIp1,
                  iPh, iPhSD, VertexNum, ElementNum, Pt, prevRitrf, rVaFirst, rVc);

// 3. Add tetrahedrons on the top
      // Insert last vertex associated to top point in the global list
         refnum_t sDOM = TG_.sigma(iPhSD+1); // Top end subdomain
         refnum_t sITF = sIIp1|sIIp2; // two internal planes
         listV_.push_back(Vertex(++VertexNum,sBS|sITF|sDOM,P2)); // rank rVd
      // Insert last 4 tetrahedra in the global list
      number_t rVa = rVaFirst, rVb = rVa+1, rVd = rVc+1;
/*!
 \verbatim
                 d
                 |
             a+2 |                  d is the top point (P2)
              \  |                  a, b, c are in a circular section of the object (basis or last interface)
               \ |                  c is the center
     a+3________\|_________b=a+1
                 \c
                  \
                   \
                    a
 \endverbatim
*/
      listT_.push_back(Tetrahedron(++ElementNum,rVa,rVb,rVd,rVc,4)); rVa++; rVb++;
      listT_.push_back(Tetrahedron(++ElementNum,rVa,rVb,rVd,rVc,4)); rVa++; rVb++;
      listT_.push_back(Tetrahedron(++ElementNum,rVa,rVb,rVd,rVc,4)); rVa++; rVb=rVaFirst;
      listT_.push_back(Tetrahedron(++ElementNum,rVa,rVb,rVd,rVc,4));

//       Description of the subdomain patches
   if (EBS[0]->curvedShape()) { TG_.setDescription(++iPh) = "End subdomain on the side of end point " + bottomPt; }
   for (int isl=1; isl<=nb_sl; isl++) {
      ostringstream ss;
      ss << "Slice " << isl;
      TG_.setDescription(++iPh) = ss.str();
   }
}

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Build a mesh of tetrahedrons by successive subdivisions in a cylinder.

 \param nbslices: number of slices of elements orthogonal to the axis of the cone
            (0 by default, which means that the number of slices is automatically
            computed ; if nbslices>0, the given value has precedence and is taken
            into account)

 \param nbsubdiv: subdivision level (0 by default)
   nbsubdiv = 0 corresponds to the initial mesh.
   For nbsubdiv > 0, each tetrahedron is subdivided into 8 tetrahedrons with the
   same orientation as the original one: if the first tetrahedron is (1,2,3,4), the
   vectors 12x13, 41x43, 43x42 and 42x41 define the exterior normals to the faces.

 \param order: order of the tetrahedra in the final mesh (1 by default)
   The default value is 1, which leads to a P1 mesh, in which case each
   tetrahedron is defined by its 4 vertices.
   For higher orders, the supplemental vertices correspond to the regular
   Lagrange mesh. Moreover, if the second argument (type) is non 0, the vertices
   belonging to the appropriate boundary lie on the surface of the cone.

 \param type: type of the subdivision (1 by default)
   type equal to 0 leads to a simple (flat) subdivision of the initial
        mesh where new vertices are all the midpoints of the edges.
   type equal to 1 leads to a subdivision where the boundary vertices are computed
        so that they lie on the corresponding surface

 \param radius: radius of the cylinder (1. by default)
 \param P1, P2        : the two end points of the axis of the cone
 \param endShape1     : shape of the end surface of the cone on P1 or P2 side
 \param endShape2     : (Flat by default). The possible values are of GeomEndShape type.
 \param distance1     : distance between P1 (resp. P2) and the apex of the surface when the
 \param distance2     : previous parameter is non Flat (default=0.)
 \param minVertexNum: minimum number associated to the vertices of the mesh (1 by default)
 \param minElementNum: minimum number associated to the elements of the mesh (1 by default)
 */
VolMeshTetCylinder::VolMeshTetCylinder(const number_t nbslices,
                                       const number_t nbsubdiv, const number_t order,
                                       const number_t type, const real_t radius,
                                       const Point P1, const Point P2,
                                       const GeomEndShape endShape1, const GeomEndShape endShape2,
                                       const real_t distance1, const real_t distance2,
                                       const number_t minVertexNum, const number_t minElementNum)
:TetrahedronMesh(nbsubdiv, order,type, minVertexNum, minElementNum)
{
//   Initialization (nbsubdiv=0)
   number_t VertexNum, ElementNum;
   vector<Point> CP;   CP.push_back(P1); CP.push_back(P2);
   vector<ShapeInfo> vSI;
   vSI.push_back(ShapeInfo(endShape1,distance1));
   vSI.push_back(ShapeInfo(endShape2,distance2));
   initMesh(nbslices,radius,radius,CP,VertexNum,ElementNum,vSI);
   buildNcheck(VertexNum);
}

} // end of namespace subdivision

} // end of namespace xlifepp
