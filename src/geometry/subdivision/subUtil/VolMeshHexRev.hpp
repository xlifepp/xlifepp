/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file VolMeshHexRev.hpp
  \author Y. Lafranche
  \since 10 Fev 2014
  \date 10 Fev 2014

  \brief Definition of the xlifepp::subdivision::VolMeshHexCone and xlifepp::subdivision::VolMeshHexCylinder classes

  These classes create a volumic mesh of an object of revolution, i.e. a cone, a truncated
  cone or a cylinder, made of hexahedra, which is built by successive subdivisions of an
  initial elementary mesh. When the subdivision level increases, the mesh tends to fill
  the interior of the object.
*/

#ifndef VOL_MESH_HEX_REV_HPP
#define VOL_MESH_HEX_REV_HPP

#include <vector>

#include "HexahedronMesh.hpp"

namespace xlifepp {
namespace subdivision {

/*!
   \class VolMeshHexCone
*/
class VolMeshHexCone : public HexahedronMesh
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   VolMeshHexCone(const number_t nbslices=0,
                  const number_t nbsubdiv=0, const number_t order=1,
                  const number_t type=1, const real_t radius1=1., const real_t radius2=0.5,
                  const Point P1=Point(0,0,0), const Point P2=Point(0,0,1),
                  const number_t minVertexNum=1, const number_t minElementNum=1);

}; // end of Class VolMeshHexCone

/*!
   \class VolMeshHexCylinder
*/
class VolMeshHexCylinder : public HexahedronMesh
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   VolMeshHexCylinder(const number_t nbslices=0,
                      const number_t nbsubdiv=0, const number_t order=1,
                      const number_t type=1, const real_t radius=1.,
                      const Point P1=Point(0,0,0), const Point P2=Point(0,0,1),
                      const number_t minVertexNum=1, const number_t minElementNum=1);

}; // end of Class VolMeshHexCylinder

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* VOL_MESH_HEX_REV_HPP */
