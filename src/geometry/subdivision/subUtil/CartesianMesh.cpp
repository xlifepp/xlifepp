/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file CartesianMesh.cpp
  \author Y. Lafranche
  \since 08 Apr 2014
  \date 08 Apr 2014

  \brief Implementation of xlifepp::subdivision::CartesianMesh class members and related functions
*/

#include "CartesianMesh.hpp"
#include "Hexahedron.hpp"
#include "Quadrangle.hpp"

#include <algorithm>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
/*!
 Effective computation of high order vertices inside the face of element Elem.
 The points lie on a regular mesh computed using barycentric coordinates ; they
 are projected onto the boundary surface if this case happen.
 The face is defined by vrV, the ranks of its 4 vertices in the vertex list.
 If the element is a quadrangle, the face is the element itself.
 */
template<class T_>
void CartesianMesh<T_>::computeHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                                    const vector<number_t>& vrV){
   const Vertex& V1=this->listV_[vrV[0]];
   const Vertex& V2=this->listV_[vrV[1]];
   const Vertex& V3=this->listV_[vrV[2]];
   const Vertex& V4=this->listV_[vrV[3]];
   refnum_t localcod = V1.locCode() & V2.locCode() & V3.locCode() & V4.locCode();
   /*
     compute grid mesh based on quadrangle (V1,V2,V3,V4) whose vertices are considered counterclockwise
                    V4 +-------------------+ V3
                       |            (k-1)^2|         Way of numbering (k = order):
                       |   *   *  ...  *   |
                       |         .         |             ^
              edge e0  |         .         | edge e1     | 2 (from V1 towards V4)
                       |         .         |             |
                       |   *   *  ...  *   |             |
                       |   1   2      k-1  |               -------->
                    V1 +-------------------+ V2                1 (from V1 towards V2)
   */
   vector<Point> VP(2), VPe0(2), VPe1(2);
   VPe0[0] = V1.geomPt();
   VPe0[1] = V4.geomPt();
   VPe1[0] = V2.geomPt();
   VPe1[1] = V3.geomPt();
   real_t coef[2];
   Point P;
   for (number_t i1=1; i1<order; i1++) {
      coef[1] = i1;
      coef[0] = order-i1;
      VP[0] = barycenter(coef,VPe0);
      VP[1] = barycenter(coef,VPe1);
      for (number_t i2=1; i2<order; i2++) {
        coef[1] = i2;
        coef[0] = order-i2;
        // The computed point is P = x (y V1 + (1-y) V4) + (1-x) (y V2 + (1-y) V3) with
        // x = (k-i2)/k and y = (k-i1)/k, or the projection of P onto the boundary surface.
        P = (this->*(this->newVertexPt_))(localcod,coef,VP);
        // add rank of the new vertex in the element list:
        Elem.vertices_.push_back(VertexNum);
        // store the new vertex in the general list listV_:
        this->listV_.push_back(Vertex(++VertexNum,localcod,P));
      }
   }
}
/*!
 Effective computation of high order vertices inside the face of element Elem using
 an algorithm intended to uniformise the position of the points in the case where an
 edge of the quadrangle lies on a curved boundary patch (CP).
 The face is defined by vrV, the ranks of its 4 vertices in the vertex list.
 If the element is a quadrangle, the face is the element itself.
 The vector vnE contains the numbers (>=1) of the (ordered) edges defining the boundary
 of the quadrangular face.
 */
template<class T_>
void CartesianMesh<T_>::computeHOfV4CP(T_& Elem, const number_t order, number_t& VertexNum,
                                       const vector<number_t>& vrV, const vector<short>& vnE){
   const Vertex& V1=this->listV_[vrV[0]];
   const Vertex& V2=this->listV_[vrV[1]];
   const Vertex& V3=this->listV_[vrV[2]];
   const Vertex& V4=this->listV_[vrV[3]];
   refnum_t localcod = V1.locCode() & V2.locCode() & V3.locCode() & V4.locCode();
/*
  Generic order k mesh of k*k quadrangles inside the quadrangle (V1,V2,V3,V4):
  \verbatim
                 k(k+1)+1                 (k+1)^2
                    V4 +---+---+---+---+---+ V3
                       |   |   |   |   |   |                  p   p+1 with p = n+k+1
                       +---+---+---+---+---+                  +---+
                       |   |   |   |   |   |                  |   |
                 2k+3  +---+---+---+---+---+ 3(k+1)           +---+
                       |   |   |   |   |   |                  n   n+1
                  k+2  +---+---+---+---+---+ 2(k+1)      all quadrangles are
                       |   |   |   |   |   |             of the form (n,n+1,p+1,p)
                    V1 +---+---+---+---+---+ V2
                       1   2   . . .   k  k+1
  \endverbatim
  To make the mesh, we use "true" numbering: ranks of vertices and edge vertices already
  created and new ones (inside) starting from VertexNum.
 */
   number_t kp1=order+1, kp2=order+2, kp1sq=kp1*kp1, ind=0;
   // rank = rkUnk U rkData
   vector<number_t> rank(kp1sq), rkUnk((order-1)*(order-1)), rkData(4*order);
   // Vertices
   rank[0] = vrV[0]; rank[order] = vrV[1];
   rank[kp1sq-1] = vrV[2]; rank[kp1sq-kp1] = vrV[3];
   for (int i=0; i<4; i++) { rkData[ind++] = vrV[i]; }

   // Edges
   vector<number_t> rkeV;
   vector<number_t>::const_reverse_iterator itrkeV;
   // Edge V1-V2
   rkeV = Elem.rkOfHOVeOnEdge(order,vnE[0]), itrkeV=rkeV.rbegin();
   for (int i=1; i<order; i++) { rank[i] = *itrkeV++; rkData[ind++] = rank[i]; }
   // Edge V2-V3
   rkeV = Elem.rkOfHOVeOnEdge(order,vnE[1]), itrkeV=rkeV.rbegin();
   for (int i=1,j=order+kp1; i<order; i++,j+=kp1) { rank[j] = *itrkeV++; rkData[ind++] = rank[j]; }
   // Edge V4-V3
   rkeV = Elem.rkOfHOVeOnEdge(order,vnE[2]), itrkeV=rkeV.rbegin();
   for (int i=1,j=order*kp1+1; i<order; i++,j++) { rank[j] = *itrkeV++; rkData[ind++] = rank[j]; }
   // Edge V1-V4
   rkeV = Elem.rkOfHOVeOnEdge(order,vnE[3]), itrkeV=rkeV.rbegin();
   for (int i=1,j=kp1; i<order; i++,j+=kp1) { rank[j] = *itrkeV++; rkData[ind++] = rank[j]; }

   // Internal points (which are the unknowns)
   number_t valrk=VertexNum;
   for (int j=1, ir=kp2, ind=0; j<order; j++) {
      for (int i=1; i<order; i++) { rkUnk[ind++] = valrk; rank[ir++] = valrk++; }
      ir += 2;// skip last point on this line and first one on next line
   }

   // Make the mesh of quadrangles
   vector< vector<number_t> > quamesh(order*order,vector<number_t>(4));
   for (int j=0, in=0, ind=0; j<order; j++,in++) {
      for (int i=0; i<order; i++,ind++,in++) {
         quamesh[ind][0] = rank[in];
         quamesh[ind][1] = rank[in+1];
         quamesh[ind][2] = rank[in+kp2];
         quamesh[ind][3] = rank[in+kp1];
      }
   }

   // Compute points in uniform positions.
   // Nota: The following function builds the matrix. However, in this context, the connectivity
   //       mesh is the same for all quadrangles, so that a generic matrix could be built and
   //       its LLt decomposition computed only once. This could be done for optimization purpose.
   vector<Point> pts = this->unifMesh(quamesh, Quadrangle::rkEdgeVertices(), rkUnk, rkData);
   // Add these points as new vertices. They are already in the correct order.
   for (vector<Point>::iterator itpts=pts.begin(); itpts<pts.end(); itpts++) {
      // add rank of the new vertex in the element list:
      Elem.vertices_.push_back(VertexNum);
      // store the new vertex in the general list listV_:
      this->listV_.push_back(Vertex(++VertexNum,localcod,*itpts));
   }
}

// class instantiations :
template class CartesianMesh<Quadrangle>;
template class CartesianMesh<Hexahedron>;

} // end of namespace subdivision
} // end of namespace xlifepp
