/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshQuaRev.hpp
  \author Y. Lafranche
  \since 1 Jul 2015
  \date 1 Jul 2015

  \brief Definition of the xlifepp::subdivision::SurfMeshQuaCone and xlifepp::subdivision::SurfMeshQuaCylinder classes

  These classes create a surfacic mesh of an object of revolution, i.e. a cone, a truncated
  cone or a cylinder, made of quadrangles, which is built by successive subdivisions of an
  initial elementary mesh.
*/

#ifndef SURF_MESH_QUA_REV_HPP
#define SURF_MESH_QUA_REV_HPP

#include "QuadrangleMesh.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class SurfMeshQuaCone
*/
class SurfMeshQuaCone : public QuadrangleMesh
{
public:
//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

   //! main constructor
   SurfMeshQuaCone(const number_t nbslices=0,
                   const number_t nbsubdiv=0, const number_t order=1,
                   const number_t type=1, const real_t radius1=1., const real_t radius2=0.5,
                   const Point P1=Point(0,0,0), const Point P2=Point(0,0,1),
                   const GeomEndShape endShape1=None,
                   const GeomEndShape endShape2=None,
                   const number_t minVertexNum=1, const number_t minElementNum=1);

}; // end of Class SurfMeshQuaCone

/*!
   \class SurfMeshQuaCylinder
*/
class SurfMeshQuaCylinder : public QuadrangleMesh
{
public:
//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

   //! main constructor
   SurfMeshQuaCylinder(const number_t nbslices=0,
                       const number_t nbsubdiv=0, const number_t order=1,
                       const number_t type=1, const real_t radius=1.,
                       const Point P1=Point(0,0,0), const Point P2=Point(0,0,1),
                       const GeomEndShape endShape1=None,
                       const GeomEndShape endShape2=None,
                       const number_t minVertexNum=1, const number_t minElementNum=1);

}; // end of Class SurfMeshQuaCylinder

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* SURF_MESH_QUA_REV_HPP */
