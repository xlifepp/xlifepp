/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Hexahedron.hpp
  \author Y. Lafranche
  \since 3 Fev 2014
  \date 19 Mar 2014

  \brief Definition of the xlifepp::subdivision::Hexahedron class

 This class defines a hexahedron as a list of vertices.
 This class also holds the so-called "high order vertices" involved in the
 Lagrange hexahedron of order k, k>1. The 8 "main" vertices correspond to k=1.
 High order vertices allow a better approximation of the geometry.

 Terminology: Strictly speaking, a hexahedron has 8 vertices. But to make short,
              the additionnal points involved in the Lagrange hexahedron of order
              greater than 1 are also called "vertices" although they are not.

 Local numbering convention
 --------------------------
 Assuming the current hexahedron is defined by the sequence of vertices (1,2,3,4,5,6,7,8),
 the faces and edges are numbered according to the following convention:
 \verbatim
              4 +------------------+ 8   Faces: Number Vertices   Edges: Number Vertices
              / .                 /|              1    1,3,4,2             1      1,2
            /   .               /  |              2    5,7,8,6             2      3,4
          /     .             /    |              3    1,2,6,5             3      5,6
        /       .           /      |              4    3,4,8,7             4      7,8
      /         .         /        |              5    1,5,7,3             5      1,3
  2 +------------------+ 6         |              6    2,6,8,4             6      2,4
    |           .      |           |     Faces: Number Edges               7      5,7
    |         3 +......|...........+ 7            1    5,2,6,1             8      6,8
    |         .        |         /                2    7,4,8,3             9      1,5
    |       .          |       /      Z           3    1,10,3,9           10      2,6
    |     .            |     /        ^    Y      4    2,12,4,11          11      3,7
    |   .              |   /          |   /       5    9,7,11,5           12      4,8
    | .                | /            | /         6    10,8,12,6
  1 +------------------+ 5            +-----> X
 \endverbatim

 The vertex number N is chosen such that its position is defined by the triple (i,j,k) in
 {0,1}^3 verifying N-1 = i 2^2 + j 2^1 + k 2^0 and i,j and k defining one end-point of the
 segment [0,1] on the axis X, Y and Z respectively.
 The faces 2p and 2p-1 are opposite, p = 1, 2, 3.
 The face (i,j,k,l) is oriented so that the cross-product ij x ik defines a normal vector
 to the face which is:
  - oriented towards the exterior of the hexahedron for faces 2, 4 and 6 (positive orientation),
  - oriented towards the interior of the hexahedron for faces 1, 3 and 5 (negative orientation).
 Thus, the exterior normal to face n is (-1)^n (ij x ik).
 The edge (i,j) is oriented from i towards j.

 In the implementation, the 8 main vertices (of order 1) are stored at the
 beginning of the internal vector vertices_ ; high order vertices, if any,
 are stored in the following positions according the numbering convention.
 The relation between the position of each vertex and its local number is given
 by the function numberingOfVertices.
*/

#ifndef HEXAHEDRON_HPP
#define HEXAHEDRON_HPP

#include "CartesianFig.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class Hexahedron
*/
class Hexahedron : public CartesianFig<Hexahedron>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! constructor by vertices, given by their rank in the list
   Hexahedron(const number_t num,
              const number_t rV1, const number_t rV2, const number_t rV3, const number_t rV4,
              const number_t rV5, const number_t rV6, const number_t rV7, const number_t rV8,
              const number_t bdSideNum=0);

//-------------------------------------------------------------------------------
//  Public Access functions
//-------------------------------------------------------------------------------
   //! get back the orientation of a face. Returns 1 if positive, -1 if negative.
   virtual short faceOrientation(short indFace) const { return ((indFace % 2) == 0) ? 1 : -1 ; }

   //! get back the rank of a vertex of an edge
   virtual short getrkEdge(short indEdge, short indVert) const { return rkEdge[indEdge][indVert]; }

   //! get back the rank of a vertex of a face
   virtual short getrkFace(short indFace, short indVert) const { return rkFace[indFace][indVert]; }

   //! returns the numbers (>= 1) of the edges defining the face number i
   static std::vector<short> numEdgesOfFace(const number_t i);

   //! returns the ranks (>= 0) of the vertices defining the edges of the hexahedron
   static std::vector<std::pair<short,short> > rkEdgeVertices();
//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
   //! position of the vertices in relation with the numbering convention
   static std::vector< std::vector<number_t> > numberingOfVertices(const number_t Order) ;

private:
   static short rkEdge[/* nb_edges_ */][2];   //!< Index array defining the edges
   static short rkFace[/* nb_faces_ */][4];   //!< Index array defining the faces
   static short nuEdge[/* nb_faces_ */][4];   //!< Array containing the edge numbers defining a face

}; // end of Class Hexahedron

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* HEXAHEDRON_HPP */
