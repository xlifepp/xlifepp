/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file QuadrangleMesh.cpp
  \author Y. Lafranche
  \since 08 Apr 2014
  \date 08 Apr 2014

  \brief Implementation of xlifepp::subdivision::QuadrangleMesh class members and related functions
*/

#include "QuadrangleMesh.hpp"
#include "TeXPolygon.hpp"

#include <algorithm>
#include <vector>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
/*!
 Subdivision of a quadrangle T into 4 quadrangles.

 Input arguments:
 \param T: quadrangle to be subdivided

 Input and output arguments (updated by this function) :
 \param ElementNum: number of the last quadrangle created in the mesh
 \param VertexNum: number of the last vertex created in the mesh
 \param listT: list of quadrangles in the mesh
 \param SeenEdges: temporary map used to build the mesh (contains the edges already seen,
                    i.e. taken into account, along with the associated vertex number,
                    in order to avoid vertex duplication)

 Numbering and orientation convention
 - - - - - - - - - - - - - - - - - - -
 The vertices and edges are numbered according to a convention given in Quadrangle.hpp.
 The quadrangle T=(1,2,3,4) is subdivided into 4 "sub-"quadrangles.
 Thus, the subdivision algorithm involves 5 more vertices. They are the isobarycenter
 of the 4 orginal vertices (1,2,3,4), plus the midpoints of the edges of the quadrangle T
 if a flat subdivision is requested ; otherwise, the midpoints are projected onto the
 boundary surface. Their numbers follow the numbering convention given below:
 \verbatim

 4                             3                  4              7              3
  +---------------------------+                    +-------------+-------------+
  |                           |                    |             |             |
  |                           |                    |             |             |
  |                           |                    |             |             |
  |                           |   SUBDIVISION      |             |             |
  |                           |                    |             | 9           |
  |                           |      ---->       8 +-------------+-------------+ 6
  |                           |                    |             |             |
  |                           |                    |             |             |
  |                           |                    |             |             |
  |                           |                    |             |             |
  |                           |                    |             |             |
  +---------------------------+                    +-------------+-------------+
 1                             2                  1              5              2

 \endverbatim

 If, for instance, the points 2 and 3 lie on the boundary and if a curved mesh (non flat)
 mesh is requested, then the point 6 is projected onto the boundary surface.

 Each "sub-"quadrangle is defined by a sequence of vertices that matches the above
 numbering convention.
 Nota: the orientation of the edges is not used in this subdivision algorithm.
 */
void QuadrangleMesh::algoSubdiv(const Quadrangle& T, number_t& ElementNum,
                                number_t& VertexNum, vector<Quadrangle>& listT,
                                map_pair_num& SeenEdges) {
   const int NbNV = nb_edges_by_element_ + 1;
// Creation of at most nb_edges_by_element_ new vertices: their rank in the list listV_ is retained in rV
   number_t irv(0);
   vector<number_t> rV(NbNV);
   for (number_t i=0; i<nb_edges_by_element_; i++) {
      rV[irv++]=createVertex(VertexNum,T.rkOfO1VeOnEdge(i+1),SeenEdges);}
   rV[irv++]=createVertex(VertexNum,T.rankOfVertices());

// To help transmission of the curved boundary edge number to the correct 2 sub-quadrangles:
   number_t bdSideNo[]={0,0,0,0}, bdednum=T.bdSideOnCP();
   if (bdednum > 0) {
      for (number_t i=0; i<2; i++) {
         bdSideNo[T.getrkEdge(bdednum-1,i)] = bdednum;
      }
   }

// Quadrangles at each vertex
// Vi is T.rankOfVertex(i) for i=1,...4 and is rV[i-5] for i=5,...9.
   listT.push_back(Quadrangle(++ElementNum,T.rankOfVertex(1),rV[0],rV[4],rV[3],bdSideNo[0])); // (1,5,9,8)
   listT.push_back(Quadrangle(++ElementNum,rV[0],T.rankOfVertex(2),rV[1],rV[4],bdSideNo[1])); // (5,2,6,9)
   listT.push_back(Quadrangle(++ElementNum,rV[4],rV[1],T.rankOfVertex(3),rV[2],bdSideNo[2])); // (9,6,3,7)
   listT.push_back(Quadrangle(++ElementNum,rV[3],rV[4],rV[2],T.rankOfVertex(4),bdSideNo[3])); // (8,9,7,4)
}

/*!
 Create high order vertices inside the quadrangle T.
 On input, VertexNum is the previous number used. On output, it is the last one.
 */
void QuadrangleMesh::createHOiV(Quadrangle& T, const number_t order, number_t& VertexNum) {
   // If the quadrangle has an edge lying on a curved boundary patch (CP), the points are
   // computed so that their positions are uniformized, in order to take into account the
   // shape of the boundary.
   // Otherwise, the points are computed by the default algorithm which gives points in
   // regular positions.
   if (T.bdSideOnCP() != 0) {
          computeHOfV4CP(T, order, VertexNum, T.rkOfO1VeOnFace(1), Quadrangle::numEdgesOfFace(1)); }
   else { computeHOfV   (T, order, VertexNum, T.rkOfO1VeOnFace(1)); }
}

/*!
 Create the initial mesh (or "seed" of the mesh) to be subdivided for a geometry of revolution,
 i.e. defined by an axis of revolution and circular sections. The general shape is a truncated
 cone, that can be a cylinder if the radius is constant.
 In the following, the word "object" is used to designate the geometric shape.

 The quadrangles are specified by an oriented set of vertices so that the normal they define
 is exterior to the object.

 \param nbslices: number of slices of elements orthogonal to the axis of the object
                     If this input value is equal to 0, a number of slices is computed from
                     the geometrical data.
 \param radius1, radius2 : radii of the object
 \param CharacPts: the two end points of the axis of the object, corresponding respectively
                     to radius1 and radius2
 \param VertexNum: number of the last vertex created (modified on output)
 \param ElementNum: number of the last element created (modified on output)
 \param vSI: vector for shape information at both ends of the object:
                     there can be nothing in which case there is a boundary,
                     or a "flat lid" in which case there is a subdomain.
 */
void QuadrangleMesh::initMesh(const number_t nbslices, const real_t radius1, const real_t radius2,
                              const vector<Point>& CharacPts, number_t& VertexNum,
                              number_t& ElementNum, const vector<ShapeInfo>& vSI){
   real_t R1, R2, slice_height;
   Point P1, P2;
   vector<ShapeInfo> cvSI;
   string bottomPt, topPt, shape;
   bool iscone;
   DefaultGeometry *DG;
   SurfPlane *SP;
   SurfCone *SC;
   vector <PatchGeometry *> EBS;
   int nb_sl;
   initRevMesh(nbslices, radius1,radius2, CharacPts, vSI,
               R1,R2, P1,P2, cvSI, bottomPt, topPt, iscone, shape, DG, SP, SC, EBS, nb_sl, slice_height);

   number_t iPh = 0, iPhSD;
   title_ = shape + " - Quadrangle mesh";
   { // Definition of 0, 1 or 2 boundaries...
      number_t nbBound = 0;
      if ( cvSI[0].shapeCode_ == None ) { nbBound++; }
      if ( cvSI[1].shapeCode_ == None ) { nbBound++; }
      vector<number_t> B_patch(nbBound), B_dimen(nbBound);
      for (size_t i=0; i<nbBound; i++) { B_patch[i] = i+1; B_dimen[i] = 1; }
     // ... nbIntrf interfaces...
     // (a plane between two successive slices, plus eventually one plane between each end slice
     //  and the corresponding "hat" end shape)
      number_t nbIntrf = nb_sl-1, nbSbDom = nb_sl;
      if (EBS[0]->curvedShape()) {nbIntrf++; nbSbDom++;} // if non Flat
      else if ( cvSI[0].shapeCode_ != None ) { nbSbDom++; }   // Flat end shape
      if (EBS[1]->curvedShape()) {nbIntrf++; nbSbDom++;}
      else if ( cvSI[1].shapeCode_ != None ) { nbSbDom++; }
      vector<number_t> I_patch(nbIntrf), I_dimen(nbIntrf);
      number_t numPa = nbBound;
      for (size_t i=0; i<nbIntrf; i++) { I_patch[i] = ++numPa; I_dimen[i] = 1; }
     // ...and nbSbDom subdomains
     // (each slice plus eventually each "hat" end shape)
      vector<number_t> D_patch(nbSbDom), D_dimen(nbSbDom);
      for (size_t i=0; i<nbSbDom; i++) { D_patch[i] = ++numPa; D_dimen[i] = 2; }

      number_t nbBI =  nbBound + nbIntrf;
      number_t nbPatches =  nbBI + nbSbDom;
      vector<PatchGeometry *> P_geom(nbPatches);
      for (size_t i=0; i<nbBI; i++) { P_geom[i] = SP; }

      if (type_ == 0) {
         // P_geom = {SP,      SP, SP,...   SP,  EBS[0],DG,...   DG,EBS[1]}
         //           \ nbBound /   \_nbIntrf_/  \__       nbSbDom     __/   (nbBound may be 0)
         delete SC; // not needed
         for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = DG; }
     }
      else {
         // P_geom = {SP,      SP, SP,...   SP,  EBS[0],SC,...   SC,EBS[1]}
         //           \ nbBound /   \_nbIntrf_/  \__       nbSbDom     __/   (nbBound may be 0)
         delete DG; // not needed
         for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = SC; }
      }
      if ( cvSI[0].shapeCode_ != None ) { P_geom[nbBI] = EBS[0]; }
      if ( cvSI[1].shapeCode_ != None ) { P_geom[nbPatches-1] = EBS[1]; }
      TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
      iPhSD = nbBound+nbIntrf;
   }
   refnum_t sBEs1=0, sBEs2=0; // localization code of the following patches
//       Description of the boundary patches
   if ( cvSI[0].shapeCode_ == None ) {
    TG_.setDescription(++iPh) = "Boundary: End curve on the side of end point " + bottomPt; sBEs1 = TG_.sigma(iPh); }
   if ( cvSI[1].shapeCode_ == None ) {
    TG_.setDescription(++iPh) = "Boundary: End curve on the side of end point " + topPt;    sBEs2 = TG_.sigma(iPh); }

// Define 4 "reference" points on the circle of radius R centered at the origin and
// 4 points on the circle of radius R/3.
// Then, rotate and translate them so that they lie on the bottom face of the object.
   vector<Point> Pt;
    {
    real_t R(R1/3.);
    Pt.push_back(Point(R,0,0));  // Pt[0]
    Pt.push_back(Point(0,R,0));  // Pt[1]
    Pt.push_back(Point(-R,0,0)); // Pt[2]
    Pt.push_back(Point(0,-R,0)); // Pt[3]
    }
    Pt.push_back(Point(R1,0,0));  // Pt[4]
    Pt.push_back(Point(0,R1,0));  // Pt[5]
    Pt.push_back(Point(-R1,0,0)); // Pt[6]
    Pt.push_back(Point(0,-R1,0)); // Pt[7]
   size_t nPt(Pt.size()); // = 8
   Vect Z(0,0,1);
   Vect AxV = SC -> AxisVector();
   Vect Nor = crossProduct(Z,AxV); // rotation axis (Z and AxV are both unitary vectors)
   Point Origin(0,0,0);
   real_t st = norm(Nor);
   if (st > theTolerance){
   // the axis of the object is not Z: we need to rotate the points around the axis (Origin,Nor)
      Nor *= (1./st); // normalize Nor
      real_t ct = dot(Z,AxV);
      for (size_t np=0; np<nPt; np++) { Pt[np] = rotInPlane(Pt[np],ct,st,Origin,Nor); }
   }
   Vect OCP1=toVector(Origin,P1);
   for (size_t np=0; np<nPt; np++) { Pt[np] = translate(Pt[np],1.,OCP1); }

   number_t sITF, sITFslice;
   number_t sDOM = TG_.sigma(iPhSD+1); // first subdomain: bottom end subdomain or first slice
   number_t rVaFirst = 0;
   number_t VN = minVertexNum_, EN = minElementNum_;

   if ( cvSI[0].shapeCode_ != None ) {// bottom lid
/*!
 \verbatim
             6
              \
               2                    points 0 to 7
       7____3___\___1______5        are in the bottom circular section of the object
                 \
                  0
                   \
                    4
 \endverbatim
*/
// 1a. Set of 8 vertices at the bottom of the first slice
//    The first 4 belong only to the of bottom lid the object
      for (size_t np=0; np<4; np++) {
         listV_.push_back(Vertex(VN++,sDOM,Pt[np])); // rank 0 to 3
      }
//    The last 4 also belong to the surface of the object
     ++iPhSD;
     sDOM |= TG_.sigma(iPhSD+1);
      // Insert first 5 quadrangles in the global list
      // They are oriented so that the normal is exterior to the object.
      // 1. internal quadrangle: (a,d,c,b) with (a,b,c,d) = (0,1,2,3)
      number_t rVa = rVaFirst, rVb = rVa+1, rVc = rVb+1, rVd = rVc+1;
      listT_.push_back(Quadrangle(EN++, rVa,rVd,rVc,rVb)); // ranks are needed here
      // 2. external quadrangles: (a,d,c,b) with (a,b,c,d) = rotations of (0,4,5,1)
      //    edge 3 is (c,b) and is on the curved boundary.
     rVd = rVb; rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVd,rVc,rVb,3));
     rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVd,rVc,rVb,3));
     rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVd,rVc,rVb,3));
     rVa++; rVd=rVaFirst;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Quadrangle(EN++, rVa,rVd,rVc,rVb,3));
      rVaFirst += 4;
   }
// 1.b Set of 4 external vertices at the bottom of the first slice
      for (size_t np=4; np<nPt; np++) {
         listV_.push_back(Vertex(VN++,sBEs1|sDOM,Pt[np])); // rank 4 to 7
      }

// 2. Set of 4 vertices on top of each slice and associated quadrangles (the 8 starting points are all transformed)
/*!
 \verbatim
             6
              \
               \                    points 4, 5, 6 and 7 are in the current section of the object
       7________\__________5
                 \
                  \
                   \
                    4
 \endverbatim
*/
   // We consider the center point of the larger basis of the object (P1).
   // The images of this point by translations along the axis will be the centers of successive
   // homotheties in each interface plane.
   Point homcen(P1);
   real_t prevRitrf = R1;
   for (int isl=1; isl<nb_sl; isl++,rVaFirst+=4) {
      for (size_t np=0; np<nPt; np++) { Pt[np] = translate(Pt[np],slice_height,AxV); }
      if (iscone) {
        homcen = translate(homcen,slice_height,AxV);
        // Compute the homothety ratio between 2 consecutive slices. homcen is the homothety center.
        real_t Ritrf = SC -> radiusAt(homcen), ratio = Ritrf / prevRitrf;
        prevRitrf = Ritrf;
        // Apply homothety to all the points in the interface.
        for (size_t np=0; np<nPt; np++) { Pt[np] = translate(homcen,ratio,toVector(homcen,Pt[np])); }
      }
      sITFslice = TG_.sigma(++iPh);                          // Next transversal interface
     ++iPhSD;
     sDOM = TG_.sigma(iPhSD) | TG_.sigma(iPhSD+1); // Two slices sharing this interface
      for (size_t np=4; np<nPt; np++) {
         sITF = sITFslice; // transversal interface
         listV_.push_back(Vertex(VN++,sITF|sDOM,Pt[np]));
      }
      // Associated quadrangles which are on the surface of the cone
      /*
       \verbatim
                      +b'
                     /|      Each quadrangle is of the form (a,b,b',a').
                   /  |       (a,b) = rotations of (4,5)
                 +a'  |
                 |    +b
                 |   /
                 | /
                 +a
       \endverbatim
      */
      number_t rVa = rVaFirst, rVb = rVa+1;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));
      rVa  = rVb++;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));
      rVa  = rVb++;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));
      rVa  = rVb; rVb=rVaFirst;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));
//      Description of the interface patch
      ostringstream ss;
      ss << "Interface: Section " << isl;
      TG_.setDescription(iPh) = ss.str();
   }
// 3. Set of 4 vertices on top of last slice and associated quadrangles
      for (size_t np=0; np<nPt; np++) { Pt[np] = translate(Pt[np],slice_height,AxV); }
      if (iscone) {
        homcen = P2;
        // Compute the homothety ratio between 2 consecutive slices. homcen is the homothety center.
        real_t ratio = R2 / prevRitrf;
        // Apply homothety to all the points in the interface.
        for (size_t np=0; np<nPt; np++) { Pt[np] = translate(homcen,ratio,toVector(homcen,Pt[np])); }
      }
  ++iPhSD;
  sDOM = TG_.sigma(iPhSD);    // Last slice
      if ( cvSI[1].shapeCode_ != None ) { sDOM |= TG_.sigma(iPhSD+1); }
      for (size_t np=4; np<nPt; np++) {
         listV_.push_back(Vertex(VN++,sBEs2|sDOM,Pt[np]));
      }
      // Associated quadrangles which are on the surface of the cone
      number_t rVa = rVaFirst, rVb = rVa+1;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));
      rVa  = rVb++;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));
      rVa  = rVb++;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));
      rVa  = rVb; rVb=rVaFirst;
      listT_.push_back(Quadrangle(EN++, rVa, rVb, rVb+4, rVa+4));

      if ( cvSI[1].shapeCode_ != None ) { // top lid
// 4. Add the 5 quadrangles on the top
      sDOM = TG_.sigma(iPhSD+1);
      for (size_t np=0; np<4; np++) {
         listV_.push_back(Vertex(VN++,sDOM,Pt[np])); // ranks of these points are the last 4
      }
/*!
 \verbatim
             6
              \
               2                    points 0 to 7
       7____3___\___1______5        are in the top circular section of the object
                 \                  These numbers are the indices in Pt.
                  0
                   \
                    4
             r+2
              \
              r+6                    They have been pushed in the vertex list in this order,
     r+3___r+7__\__r+5_____r+1       starting from rVaFirst:
                 \
                 r+4
                   \
                    r = rVaFirst
 \endverbatim
*/
      rVaFirst+=4;
      // 1. internal quadrangle: (a,b,c,d) = (0,1,2,3)
      number_t rVa = rVaFirst+4, rVb = rVa+1, rVc = rVb+1, rVd = rVc+1;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd)); // ranks are needed here
      // 2. external quadrangles: (a,b,c,d) = rotations of (0,4,5,1)
      //    edge 2 is (b,c) and is on the curved boundary.
        rVd = rVb; rVb = rVa-4; rVc = rVd-4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
        rVa++; rVd++;  rVb = rVa-4; rVc = rVd-4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
        rVa++; rVd++;  rVb = rVa-4; rVc = rVd-4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
        rVa++; rVd=rVaFirst+4;  rVb = rVa-4; rVc = rVd-4;
      listT_.push_back(Quadrangle(EN++, rVa,rVb,rVc,rVd,2));
      }

//       Description of the subdomain patches
   if ( cvSI[0].shapeCode_ != None ) { TG_.setDescription(++iPh) = "End subdomain on the side of end point " + bottomPt; }
   for (int isl=1; isl<=nb_sl; isl++) {
      ostringstream ss;
      ss << "Slice " << isl;
      TG_.setDescription(++iPh) = ss.str();
   }
   if ( cvSI[1].shapeCode_ != None ) { TG_.setDescription(++iPh) = "End subdomain on the side of end point " + topPt; }
   VertexNum = VN - 1;
   ElementNum = EN - 1;
}

/*!
 Define some macros in the file associated to the stream ftex in order to display
 the mesh using Fig4TeX.
 */
void QuadrangleMesh::printTeXHeader(ostream& ftex) const {
   ftex << "\\def\\drawFace#1#2#3#4#5{" << endl;
   ftex << "\\figset(color=#5, fill=yes)\\figdrawline[#1,#2,#3,#4]" << endl;
   ftex << "\\figset(color=default, fill=no)\\figdrawline[#1,#2,#3,#4,#1]}" << endl;
   ftex << "\\def\\drawEdge#1#2#3{" << endl;
   ftex << "\\figset(color=#3, width=3)\\figdrawline[#1,#2]" << endl;
   ftex << "\\figset(color=default, with=default)}"<<endl;
   ftex << "\\def\\drawElem#1#2#3#4{\\figdrawline[#1,#2,#3,#4,#1]}" << endl;
}

/*!
 Prints, on stream ftex, Fig4TeX instructions to define points to be used for
 the display of the mesh. These points are located in the domain. Boundary points
 and points lying on the interfaces (used for edges) also belong to this set,
 so the last argument is unused.
 */
void QuadrangleMesh::printTeXPoints(ostream& ftex, const bool withInterface) const {
   printTeXInArea(ftex,0,subdomainArea);
}

/*!
 Prints, on stream ftex, Fig4TeX instructions to draw faces of a quadrangle mesh
 and edges belonging to any area of kind TA. The faces and edges are ordered
 from the farthest to the nearest along the observation direction defined by the
 longitude psi and the latitude theta, given in degrees.
 */
void QuadrangleMesh::printTeXSortedAreaFoE(ostream& ftex, const topologicalArea TA,
                                           const float psi, const float theta) const {
//   Faces
   number_t last_sdom = TG_.numberOf(subdomainArea);
   for (number_t numArea=1; numArea<=last_sdom; numArea++) {
      // Define subdomain colors
      ftex << "\\def\\Color" << string(1,'@'+numArea)  // ColorA, ColorB...
           << "{" << colorOf(subdomainArea,numArea) << "}% " << TG_.nameOf(subdomainArea,numArea) << endl;
   }
   // Define inside color. This color is used to display faces seen from the interior of the domain.
   // This situation may occur depending on the shape of the domain and on the observation direction.
   number_t offset = last_sdom+1;
   string inside(1,'@'+offset);
   ftex << "\\def\\Color" << inside
        << "{" << colorOf(subdomainArea,offset) << "}% inside of domain (may be unused)" << endl;
//   Edges
   number_t last_area = TG_.numberOf(TA);
   for (number_t numArea=1; numArea<=last_area; numArea++) {
      // Define colors of TA
      ftex << "\\def\\Color" << string(1,'@'+numArea+offset)
           << "{" << colorOf(TA,numArea) << "}% " << TG_.nameOf(TA,numArea) << endl;
   }

   vector<TeXPolygon> BF;
//  Collect all the subdomain faces and keep their associated number.
   number_t number_of_faces = 0;
   for (number_t numArea=1; numArea<=last_sdom; numArea++) {
      vector< vector<number_t> > FonB = rk_facesIn(subdomainArea,numArea);
      number_of_faces += FonB.size();
      vector< vector<number_t> >::iterator itFonB;
      for (itFonB=FonB.begin(); itFonB != FonB.end(); itFonB++) {
         BF.push_back(TeXPolygon(*itFonB,numArea,listV_));
         }
      }
//  Collect all the edges on all 1D areas of kind TA and keep their
//  associated number, shifted by offset to select them below during file creation.
//  Indeed, as edges and faces are all treated the same way by the sort process,
//  we need an indicator to distinguish the two kinds of TeXPolygon.
   number_t number_of_edges = 0;
   for (number_t numArea=1; numArea<=last_area; numArea++) {
      //if (TG_.dimensionOf(TA,numArea) == 1) { // 1D boundary
      vector< pair_nn > EonB = rk_edgesIn(TA,numArea);
      number_of_edges += EonB.size();
      vector< pair_nn >::iterator itEonB;
      for (itEonB=EonB.begin(); itEonB != EonB.end(); itEonB++) {
         BF.push_back(TeXPolygon(*itEonB,numArea+offset,listV_));
         }
      }

//  Sort the faces from the farthest to the nearest

//  1. Compute the vector defining the observation direction
   real_t psird = psi*pi_/180.;
   real_t thetard = theta*pi_/180.;
   real_t ct = std::cos(thetard);
//   TeXPolygon::OD = Vect(ct*cos(psird), ct*sin(psird), sin(thetard));
   TeXPolygon::initObsDir(Vect(ct*std::cos(psird), ct*std::sin(psird), std::sin(thetard)));

//  2. Apply sort algorithm (only for 3D data since in 2D it is unusefull, and moreover
//     it should not be performed for consistency reason)
   bool in3D = listV_[0].geomPt().size() > 2;
   if (in3D) sort(BF.begin(),BF.end());

//  3. Print the sorted faces and edges
   ftex << "% " << number_of_edges << " edges on " << TG_.kindOf(TA) << " " ;
   for (number_t numArea=1; numArea<=last_area; numArea++) {
      //if (TG_.dimensionOf(TA,numArea) == 1) {
         ftex << TG_.nameOf(TA,numArea) << ", ";
      //}
   }
   ftex << endl;
   ftex << "% " << number_of_faces << " faces on " << TG_.kindOf(subdomainArea) << " " ;
   for (number_t numArea=1; numArea<=last_sdom; numArea++) {
      //if (TG_.dimensionOf(subdomainArea,numArea) != 1) {
         ftex << TG_.nameOf(subdomainArea,numArea) << ", ";
      //}
   }
   ftex << endl;
   vector<TeXPolygon>::iterator itBF;
   vector<number_t>::const_iterator itF;
   for (itBF=BF.begin(); itBF != BF.end(); itBF++) {
      if (itBF->attrib() > offset) {
         ftex << "\\drawEdge";
         for (itF=itBF->Vrank().begin(); itF != itBF->Vrank().end(); itF++) {
            ftex << "{" << listV_[*itF].number() << "}";
         }
         ftex << "{\\Color" << string(1,'@'+itBF->attrib()) << "}";
      }
      else {
         ftex << "\\drawFace";
         for (itF=itBF->Vrank().begin(); itF != itBF->Vrank().end(); itF++) {
            ftex << "{" << listV_[*itF].number() << "}";
         }
         if (!in3D || (in3D && itBF->isExtVisible())) {
           ftex << "{\\Color" << string(1,'@'+itBF->attrib()) << "}";
         }
         else {
           ftex << "{\\Color" << inside << "}";
         }
      }
      ftex << endl;
   }
}

} // end of namespace subdivision
} // end of namespace xlifepp
