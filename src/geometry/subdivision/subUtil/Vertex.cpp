/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Vertex.cpp
  \author Y. Lafranche
  \since 07 Dec 2007
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::Vertex class members and related functions
*/

#include "Vertex.hpp"

#include <iomanip>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------

/*!
 Prints, on stream os, the attributes of the vertex
 */
void Vertex::print(ostream& os, const TopoGeom& TG) const {
   os << "Vertex " << setw(4) << num_ << " ";
   os << P_;
   os << "  Localization:";
   TG.printLoc(os,localcod_);
   os << endl;
}
/*!
 Prints, on stream os, the coordinates of the point associated to the vertex
 in scientific format.
 */
void Vertex::print(ostream& os) const {
   os << P_;
}
/*!
 Prints, on stream os, the coordinates of the point associated to the vertex
 in standard format for TeX exploitation.
 */
void Vertex::printTeX(ostream& os) const {
   P_.printTeX(os);
}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
/*!
 Computes of the square of the distance between the current vertex and vertex V
 */
real_t Vertex::squareDistance(const Vertex& V) const {
   return P_.squareDistance(V.P_);
}

} // end of namespace subdivision
} // end of namespace xlifepp
