/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SurfMeshQuaSet.hpp
  \author Y. Lafranche
  \since 13 Mar 2014
  \date 13 Mar 2014

  \brief Definition of the xlifepp::subdivision::SurfMeshQuaSet class

  This class allows the construction of a surfacic mesh made of quadrangles.
  It is built by successive subdivisions of an initial elementary mesh consisting
  of quadrangles given by the user.
*/

#ifndef SURF_MESH_QUA_SET_HPP
#define SURF_MESH_QUA_SET_HPP

#include "QuadrangleMesh.hpp"

#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class SurfMeshQuaSet
*/
class SurfMeshQuaSet : public QuadrangleMesh
{
public:
//-------------------------------------------------------------------------------
// Constructors, Destructor
//-------------------------------------------------------------------------------

   //! main constructor
   SurfMeshQuaSet(const std::vector<Point>& pts, const std::vector<std::vector<number_t> >& tri,
                  const std::vector<std::vector<number_t> >& bound,
                  const number_t nbsubdiv=0, const number_t order=1,
                  const number_t minElementNum=1);
private:
//-------------------------------------------------------------------------------
// Private member functions
//-------------------------------------------------------------------------------

   //! create initial mesh
   void initMesh(const std::vector<Point>& pts, const std::vector<std::vector<number_t> >& tri,
                 const std::vector<std::vector<number_t> >& bound,
                 number_t& VertexNum, number_t& ElementNum);

   //! retrieve localisation code of point num from boundaries
   refnum_t getSig(number_t num, const std::vector<std::vector<number_t> >& bound);

}; // end of Class SurfMeshQuaSet

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* SURF_MESH_QUA_SET_HPP */
