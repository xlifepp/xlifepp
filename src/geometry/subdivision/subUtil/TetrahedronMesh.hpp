/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file TetrahedronMesh.hpp
  \author Y. Lafranche
  \since 07 Dec 2007
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::TetrahedronMesh class

  This class is an abstract class that holds a mesh of tetrahedra built
  by successive subdivisions of an initial elementary mesh consisting of several
  tetrahedra.

  The numbering convention used to denote the vertices, faces and edges of the
  tetrahedrons is described on top of the header file of the class xlifepp::subdivision::Tetrahedron,
  namely Tetrahedron.hpp.

  inheriting classes
  ------------------
    - xlifepp::subdivision::VolMeshTetCone
    - xlifepp::subdivision::VolMeshTetCube
    - xlifepp::subdivision::VolMeshTetCylinder
    - xlifepp::subdivision::VolMeshTetSphere
*/

#ifndef TETRAHEDRON_MESH_HPP
#define TETRAHEDRON_MESH_HPP

#include "Tetrahedron.hpp"
#include "SimplexMesh.hpp"

#include <iostream>
#include <vector>

namespace xlifepp {
namespace subdivision {

/*!
   \class TetrahedronMesh
*/
class TetrahedronMesh : public SimplexMesh<Tetrahedron>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   TetrahedronMesh(const number_t nbsubdiv, const number_t order, const number_t type,
                   const number_t minVertexNum, const number_t minElementNum)
: SimplexMesh<Tetrahedron>(nbsubdiv, order, type, minVertexNum,minElementNum,
                           (order+1)*(order+2)*(order+3)/6, 8)
{}

//-------------------------------------------------------------------------------
//  I/O utilities
//-------------------------------------------------------------------------------
   /*!
     prints, on stream ftex, Fig4TeX instructions to draw the numbering
     convention for a tetrahedron mesh of order k
    */
   virtual void printTeXNumberConvention(std::ostream& ftex) const ;
   virtual void printTeXNumberConvention(PrintStream& ftex) const {printTeXNumberConvention(ftex.currentStream());}

protected:
//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! subdivision of a tetrahedron into 8 tetrahedra
   void algoSubdiv(const Tetrahedron& T, number_t& ElementNum, number_t& VertexNum,
                   std::vector<Tetrahedron>& listT, map_pair_num& SeenEdges);// override

   //! create or retrieve high order vertices inside the tetrahedron
   virtual void createHOiV(Tetrahedron& T, const number_t order, number_t& VertexNum);

   //! create initial mesh for a geometry of revolution
   void initMesh(const number_t nbslices,
                 const real_t radius1, const real_t radius2, const std::vector<Point>& CharacPts,
                 number_t& VertexNum, number_t& ElementNum, const std::vector<ShapeInfo>& vSI);
   //! utilitary function used by previous one
   void startConeTrunk(real_t R1, const Point& P1, const std::vector <PatchGeometry *>& EBS,
                       const SurfCone *SC, real_t slice_height, int nb_sl, bool iscone,
                       refnum_t sBEs1, refnum_t sBS, refnum_t sIIp1, refnum_t sIIp2, number_t iPhIIp1,
                       number_t& iPh, number_t& iPhSD, number_t& VertexNum, number_t& ElementNum,
                       std::vector<Point>& Pt, real_t& prevRitrf, number_t& rVaFirst, number_t& rVc);

   //! create tetrahedrons that subdivide a prism
   void subdivPrism(const number_t U1, const number_t U2, const number_t U3,
                    const number_t U4, const number_t U5, const number_t U6,
                    number_t& ElementNum);

   //@{
   //! prints, on stream ftex, definition of some TeX macros
   virtual void printTeXHeader(std::ostream& ftex) const;
   virtual void printTeXHeader(PrintStream& ftex) const {printTeXHeader(ftex.currentStream());}
   //@}

}; // end of Class TetrahedronMesh

} // end of namespace subdivision

} // end of namespace xlifepp

#endif /* TETRAHEDRON_MESH_HPP */
