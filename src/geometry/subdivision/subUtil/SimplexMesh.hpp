/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SimplexMesh.hpp
  \author Y. Lafranche
  \since 13 Jun 2008
  \date 08 Mar 2014

  \brief Definition of the xlifepp::subdivision::SimplexMesh class

  This class is a template abstract class used as a frame for meshes made
  of simplices, namely triangles and tetrahedrons.

  inheriting classes
  ------------------
    - TetrahedronMesh
    - TriangleMesh
*/

#ifndef SIMPLEX_MESH_HPP
#define SIMPLEX_MESH_HPP

#include "Simplex.hpp"
#include "GeomFigureMesh.hpp"

#include <vector>

namespace xlifepp {

//! namespace dedicated to tools of "subdivision" mesh generator
namespace subdivision {

/*!
   \class SimplexMesh
*/
template<class T_>
class SimplexMesh : public GeomFigureMesh<T_>
{
public:
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
   SimplexMesh(const number_t nbsubdiv, const number_t order, const number_t type,
               const number_t minVertexNum, const number_t minElementNum,
               const number_t totNbV, const number_t subF)
: GeomFigureMesh<T_>(nbsubdiv, order, type, minVertexNum, minElementNum, totNbV,
                     Simplex<T_>::numberOfMainVertices(),
                     Simplex<T_>::numberOfEdges(), Simplex<T_>::numberOfFaces(), subF)
{}


protected:
//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
   //! compute high order vertices inside the face of element Elem defined by vrV
   void computeHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                    const std::vector<number_t>& vrV);

   //! create or retrieve high order vertices inside the face numFace of element Elem
   virtual void createHOfV(T_& Elem, const number_t order, number_t& VertexNum,
                           const number_t numFace, map_set_pair_in& SeenFaces);

   //! create high order vertices inside the simplexe Elem
   virtual void createHOiV(T_& Elem, const number_t order, number_t& VertexNum) = 0;

}; // end of Class SimplexMesh

} // end of namespace subdivision
} // end of namespace xlifepp
#endif /* SIMPLEX_MESH_HPP */
