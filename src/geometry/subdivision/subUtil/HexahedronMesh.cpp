/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file HexahedronMesh.cpp
  \author Y. Lafranche
  \since 07 Fev 2014
  \date 07 Fev 2014

  \brief Implementation of xlifepp::subdivision::HexahedronMesh class members and related functions
*/

#include "HexahedronMesh.hpp"
#include "PointUtils.hpp"

#include <algorithm>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
   //! main constructor
HexahedronMesh::HexahedronMesh(const number_t nbsubdiv, const number_t order, const number_t type,
                               const number_t minVertexNum, const number_t minElementNum)
: CartesianMesh<Hexahedron>(nbsubdiv, order, type, minVertexNum, minElementNum,
                            (order+1)*(order+1)*(order+1), 8)
{
// Initialization of the barycentric coordinates (eventually) used in used in createHOfV.
// Doing this in the constructor need storage but insures it is done only once.
   if (order > 1) {
      const number_t irv = 4; // index of the rank of the vertex in obc (last element)
      vector<number_t> obc(irv+1);
      number_t rank(0);
      // Store barycentric coordinates in the same order as is done in createHOfV.
      // Last element holds the creation rank.
      for (number_t i1=1; i1<order; i1++) {
         for (number_t i2=1; i2<order; i2++) {
            obc[0] = (order-i2)*(order-i1);
            obc[1] = i2*(order-i1);
            obc[2] = i2*i1;
            obc[3] = (order-i2)*i1;
            obc[irv] = rank++;
            tobc.push_back(obc);
         }
      }
      stobc = tobc;
      // Sort the table in lexicographical order (only the first 2
      // columns are involved in the sort and default < operator is used)
      sort(stobc.begin(),stobc.end());
   }
}

//-------------------------------------------------------------------------------
//  Protected member functions
//-------------------------------------------------------------------------------
/*!
 Subdivision of a hexahedron T into 8 hexahedrons.

 Input arguments:
 \param T: hexahedron to be subdivided

 Input and output arguments (updated by this function) :
 \param ElementNum: number of the last hexahedron created in the mesh
 \param VertexNum: number of the last vertex created in the mesh
 \param listT: list of hexahedrons in the mesh
 \param SeenEdges: temporary map used to build the mesh (contains the edges already seen,
                    i.e. taken into account, along with the associated vertex number,
                    in order to avoid vertex duplication)
 \param SeenFaces: temporary map used to build the mesh (contains the faces already seen,
                    i.e. taken into account, along with the associated vertex number,
                    in order to avoid vertex duplication)

 Numbering and orientation convention
 - - - - - - - - - - - - - - - - - - -
 The vertices, faces and edges are numbered according to a convention given in Hexahedron.hpp.
 The hexahedron T=(1,2,3,4,5,6,7,8) is subdivided into 8 "sub-"hexahedrons.
 Thus, the subdivision algorithm involves 19 more vertices: 12 on the edges, 6 on the
 faces and one last point in the center of the hexahedron.
 They are the midpoints of the edges of the hexahedron T for every internal edge.
 For boundary edges, the additional points are also the midpoints if a flat subdivision
 is requested ; otherwise, the midpoints are projected onto the boundary surface.
 The same apply for the points on the faces: they are the isobarycenter of the 4 vertices
 of the face except for boundary faces in the case of a non flat subdivision where
 this point is projected onto the boundary surface.

 Their numbers follow the numbering of the edges and faces which leads to a convention given below:
 \verbatim
                               4 +--------20--------+ 8              4 +------------------+ 8
                               / .                 /|                / .                 /|
      AFTER                  /   .               /  |              /   .               /  |
                          14     .             16   |            /     .  26         /    |
   SUBDIVISION           /      10           /      12         /       .       24  /      |
                       /         .         /        |        /         .         /        |
      ---->        2 +--------18--------+ 6         |    2 +------------------+ 6         |
                     |           .      |           |      |     21    .  27  |     22    |
                     |         3 +......|.19........+ 7    |         3 +......|...........+ 7
                     |         .        |         /        |         .        |         /
   Z                 9       .          11      /          |       . 23       |       /
   ^     Y           |    13            |     15           |     .         25 |     /
   |   /             |   .              |   /              |   .              |   /
   | /               | .                | /                | .                | /
   +-----> X       1 +--------17--------+ 5              1 +------------------+ 5
                        12 edge points                     6 face points and 1 internal point
 \endverbatim
 If, for instance, the points 1, 2, 5 and 6 lie on the boundary and if a curved mesh (non
 flat) mesh is requested, then the points 9, 11, 17, 18 and 23 are projected onto the boundary
 surface.

 Each "sub-"hexahedron is defined by a sequence of vertices that matches the above
 numbering convention.
 Nota: the orientation of the edges is not used in this subdivision algorithm.
 */
void HexahedronMesh::algoSubdivH(const Hexahedron& T, number_t& ElementNum, number_t& VertexNum,
                                 vector<Hexahedron>& listT, map_pair_num& SeenEdges, map_set_num& SeenFaces) {
   const number_t NbNV = nb_edges_by_element_ + nb_faces_by_element_ + 1;
// Creation of at most NbNV new vertices: their rank in the list listV_ is retained in rV
   //number_t rV[NbNV], irv(0);
   number_t irv(0);
   vector<number_t> rV(NbNV);
   for (number_t i=0; i<nb_edges_by_element_; i++) {
      rV[irv++]=createVertex(VertexNum,T.rkOfO1VeOnEdge(i+1),SeenEdges);}
   for (number_t i=0; i<nb_faces_by_element_; i++) {
      rV[irv++]=createVertex(VertexNum,T.rkOfO1VeOnFace(i+1),SeenFaces);}
   rV[irv++]=createVertex(VertexNum,T.rankOfVertices());

// To help transmission of the curved boundary face number to the correct 4 sub-hexahedrons:
   number_t bdSideNo[]={0,0,0,0,0,0,0,0}, bdfanum=T.bdSideOnCP();
   if (bdfanum > 0) {
      for (number_t i=0; i<4; i++) {
         bdSideNo[T.getrkFace(bdfanum-1,i)] = bdfanum;
      }
   }

// Hexahedrons at each vertex
// Vi is T.rankOfVertex(i) for i=1,...8 and is rV[i-9] for i=9,...27.
    // at vertex 1 : (1,9,13,21, 17,23,25,27)
   listT.push_back(Hexahedron(++ElementNum, T.rankOfVertex(1),rV[ 0],rV[ 4],rV[12], rV[ 8],rV[14],rV[16],rV[18],bdSideNo[0]));
    // at vertex 2 : (9,2,21,14, 23,18,27,26)
   listT.push_back(Hexahedron(++ElementNum, rV[ 0],T.rankOfVertex(2),rV[12],rV[ 5], rV[14],rV[ 9],rV[18],rV[17],bdSideNo[1]));
    // at vertex 3 : (13,21,3,10, 25,27,19,24)
   listT.push_back(Hexahedron(++ElementNum, rV[ 4],rV[12],T.rankOfVertex(3),rV[ 1], rV[16],rV[18],rV[10],rV[15],bdSideNo[2]));
    // at vertex 4 : (21,14,10,4, 27,26,24,20)
   listT.push_back(Hexahedron(++ElementNum, rV[12],rV[ 5],rV[ 1],T.rankOfVertex(4), rV[18],rV[17],rV[15],rV[11],bdSideNo[3]));
    // at vertex 5 : (17,23,25,27, 5,11,15,22)
   listT.push_back(Hexahedron(++ElementNum, rV[ 8],rV[14],rV[16],rV[18], T.rankOfVertex(5),rV[ 2],rV[ 6],rV[13],bdSideNo[4]));
    // at vertex 6 : (23,18,27,26, 11,6,22,16)
   listT.push_back(Hexahedron(++ElementNum, rV[14],rV[ 9],rV[18],rV[17], rV[ 2],T.rankOfVertex(6),rV[13],rV[ 7],bdSideNo[5]));
    // at vertex 7 : (25,27,19,24, 15,22,7,12)
   listT.push_back(Hexahedron(++ElementNum, rV[16],rV[18],rV[10],rV[15], rV[ 6],rV[13],T.rankOfVertex(7),rV[ 3],bdSideNo[6]));
    // at vertex 8 : (27,26,24,20, 22,16,12,8)
   listT.push_back(Hexahedron(++ElementNum, rV[18],rV[17],rV[15],rV[11], rV[13],rV[ 7],rV[ 3],T.rankOfVertex(8),bdSideNo[7]));
}
/*!
 Build the mesh by successive subdivisions of the initial mesh.
 Input arguments:
   VertexNum: number of the last vertex created in the initial mesh
 Output arguments:
   VertexNum: number of the last vertex created in the mesh

 Nota: this function overloads GeomFigureMesh<T_>::buildMesh because of the
       treatment needed for the faces.
  */
void HexahedronMesh::buildMesh(number_t& VertexNum){
//  At each subdivision, the hexahedrons are numbered starting from minElementNum_ (via
//  the statement ElementNum=minElementNum_-1), since theses are all new hexahedrons.
//  On the contrary, the vertices are all kept from the beginning and the vertex counter
//  VertexNum must not be reinitialized.
//  Nota: By construction, the elements stored in listT_ are grouped by subdomains.
   for (number_t i=0; i<subdiv_level_; i++) {
      number_t ElementNum = minElementNum_ - 1;
      vector<Hexahedron> tmplistT;
      // listT_.size() is the number of hexahedra built during previous subdivision
      tmplistT.reserve(subdivisionFactor_*listT_.size());
      map_pair_num SeenEdges;
      map_set_num SeenFaces;
      vector<Hexahedron>::iterator itT;
      for (itT=listT_.begin(); itT != listT_.end(); itT++) {
         algoSubdivH(*itT,ElementNum,VertexNum,tmplistT,SeenEdges,SeenFaces);
      }
      listT_ = tmplistT;
   }
   initDefaultUserAttribute();
}

/*!
 Create high order vertices inside the face numFace of element Elem.
 Their numbers are consecutive on the face.
 The creation process of the new vertices and the numbering defined in
 Hexahedron::numberingOfVertices are coherent.
 */
void HexahedronMesh::createHOfV(Hexahedron& Elem, const number_t order, number_t& VertexNum,
                                const number_t numFace, map_set_pair_in& SeenFaces){
   vector<number_t> vrV = Elem.rkOfO1VeOnFace(numFace);
   // Map key is a set in order to create a unique entry in the map, regardless of
   // the way the face is defined.
   number_t& rV1 = vrV[0];
   number_t& rV2 = vrV[1];
   number_t& rV3 = vrV[2];
   number_t& rV4 = vrV[3];
   set_n Face; Face.insert(rV1); Face.insert(rV2); Face.insert(rV3); Face.insert(rV4);
   map_set_pair_in::iterator itSF;
   int curFacOri(Elem.faceOrientation(numFace));

   if ((itSF=SeenFaces.find(Face)) == SeenFaces.end()) {
      // This face has not yet been seen: compute the corresponding vertices.
      // Store the face along with the corresponding couple (rank of the first new
      // vertex created in the face, rank of the first main vertex defining the face).
      // Moreover, the orientation of the face is kept through the sign of the new vertex
      // number, which is never 0.
      // This will be useful to make the correspondence between two faces of neighbor
      // hexahedrons (see below "else" part).
      pair_in NV (VertexNum * curFacOri, rV1);
      SeenFaces.insert(make_pair(Face,NV));

      number_t bFoCP = Elem.bdSideOnCP();
      // A null boundary face number (bFoCP) means that the hexahedron is not a boundary element.
      // If this number is different from 0 and different from the current face, the high order
      // vertices are computed by the procedure designed to uniformise the location of the points
      // on this face.
      // Otherwise they are computed by the "standard" way (via barycentric coordinates), which
      // leads to a regular set of points.
      if (bFoCP != 0 && bFoCP != numFace) {
             computeHOfV4CP(Elem, order, VertexNum, vrV, Hexahedron::numEdgesOfFace(numFace)); }
      else { computeHOfV   (Elem, order, VertexNum, vrV); }
   }
   else {
// Face found: get back the corresponding vertices whose numbers are consecutive
// over this face, starting from num.
      int_t num = (itSF->second).first;
      int fndFacOri(1);
      if (num < 0) { fndFacOri = -fndFacOri; num = -num; }
      // If the order is 2, there is only one point in the middle of the face. if (order == 2) {Elem.vertices_.push_back(num); } else {// add rank of the vertex

   // Find the face definition: if the face found is (1,2,3,4), then the current
   // face is a circular permutation of (1,2,3,4) if the faces have different orientations (case a),
   // and  is a circular permutation of (1,4,3,2) if the faces have the same orientation (case b),
   // because of the orientation of the hexahedrons.
      number_t fv = (itSF->second).second;
      // detect the position of the first vertex of the found face among the vertices of the current face
      int i0;
      for (i0=0; i0<4; i0++) { if (fv == vrV[i0]) break; }
      int cpdo[]={0,1,2,3,0,1,2}, // case a: towards the right starting from index 0
          cpso[]={3,2,1,0,3,2,1}; // case b: towards the left  starting from index 3
          // In case b, if fv is the first vertex of the current face, then the permutation is (1,4,3,2).
          // If it is the second, then the permutation is (2,1,4,3) and so on, which explains the index 3-i0 below.
      // test the product of orientations of the found face and the current face
      int *ind;
      if (fndFacOri*curFacOri < 0) { ind = &cpdo[i0]; }
      else                         { ind = &cpso[3-i0]; }// see explanation above
      // ind is a vector of indices allowing to affect the barycentric coordinates of
      // the internal points of the face to each vertex of the face in the correct order.
      // Barycentric coordinates are used to map the points of the two faces.

   // Build the table of barycentric coordinates associated to each point of the
   // face: ind[i]=i would give the original correspondence, i.e. the one used
   // when the point was created (in the "if" part above).
      vector< vector<number_t> > tbc;
      const number_t irv = tobc[0].size()-1; // index of the rank of the vertex in bc (last element)
      vector<number_t> bc(irv+1);            //  reordered barycentric coordinates
      // Reaffect original barycentric coordinates to the main vertices of the current face along with
      // the associated point number (num) used during the creation phase (in the "if" part above).
      for (vector< vector<number_t> >::const_iterator ittobc=tobc.begin(); ittobc != tobc.end(); ittobc++) {
         bc[ind[0]] = (*ittobc)[0];
         bc[ind[1]] = (*ittobc)[1];
         bc[ind[2]] = (*ittobc)[2];
         bc[ind[3]] = (*ittobc)[3];
         bc[irv] = num++;
         tbc.push_back(bc);
      }
   // Now sort the table in lexicographical order to get the correspondence with stobc (only the first 2
   // columns are involved in the sort and default < operator is used). After that, going through the
   // last column of the tables will give the vertices in the correct order, which are then stored in Vr.
      sort(tbc.begin(), tbc.end());
      vector<number_t> Vr(tbc.size());
      for (vector< vector<number_t> >::const_iterator itstobc=stobc.begin(), ittbc=tbc.begin();
           itstobc != stobc.end(); itstobc++, ittbc++) {
         Vr[(*itstobc)[irv]] = (*ittbc)[irv];
      }
      for (vector<number_t>::iterator itVr=Vr.begin(); itVr != Vr.end(); itVr++) {
         Elem.vertices_.push_back(*itVr);// add rank of the vertex
      }
   }
}

/*!
 Create high order vertices inside the hexahedron T.
 On input, VertexNum is the previous number used. On output, it is the last one.
 */
void HexahedronMesh::createHOiV(Hexahedron& T, const number_t order, number_t& VertexNum){
   vector<refnum_t> lc(nb_main_vertices_by_element_);
   for (number_t i=0; i<nb_main_vertices_by_element_; i++) {
      number_t rV = T.vertices_[i];
      lc[i] = listV_[rV].locCode();
   }
   refnum_t localcod = lc[0];
   for (number_t i=1; i<nb_main_vertices_by_element_; i++) { localcod &= lc[i]; }// localcod should be zero

   if (T.bdSideOnCP() != 0 ) {
      // compute uniform localization of points in this element because
      // it has a boundary face which is part of a curved patch
/*
  Generic order k mesh of k^3 hexahedrons inside the hexahedron T. The points on the bottom
  face (0,4,6,2) are numbered from 1 to (k+1)^2 (like in CartesianMesh<T_>::computeHOfV4CP):
  \verbatim
                 k(k+1)+1                 (k+1)^2
                   [2] +---+---+---+---+---+ [6]
                       |   |   |   |   |   |                  p   p+1 with p=n+k+1
                       +---+---+---+---+---+                  +---+
                       |   |   |   |   |   |                  |   |
                 2k+3  +---+---+---+---+---+ 3(k+1)           +---+
                       |   |   |   |   |   |                  n   n+1
                  k+2  +---+---+---+---+---+ 2(k+1)      all quadrangles are
                       |   |   |   |   |   |             of the form (n,n+1,p+1,p)
                   [0] +---+---+---+---+---+ [4]
                       1   2   . . .   k  k+1
  \endverbatim
  Each "slice" along the third dimension contains the (k+1)^2 following points, so that the
  generic hexadron has the numbering given below:
  \verbatim
                                                     (k+1)^3
                                 [3] +------------------+ [7]
                                   / .                 /|
                                 /   .               /  |                      r +--------+ r+1
                               /     .             /    |                      / .      / |
                             /       .           /      + 3(k+1)^2         q +--------+q+1|
                       [1] /         .         /        |                    |   .    |   |
            k(k+1)^2 + 1 +------------------+ [5]       + 2(k+1)^2           | p + . .| . + p+1
                         |           |      |           |                    | .      | /
                         |       [2] +......|...........+ [6]              n +--------+ n+1
                         |         .        |         /(k+1)^2
            2(k+1)^2 + 1 +  +    .       +  +       /             all hexahedrons are of the form
                         |     .            |     /               (n,q, p,r, n+1,q+1, p+1,r+1)
             (k+1)^2 + 1 +  +.           +  +   /                 with p = n+k+1, q = n+(k+1)^2
                         | .                | /                   and r = q+k+1.
                     [0] +--+------------+--+ [4]
                         1  2    . . .   k k+1
  \endverbatim
  To make the mesh, we use "true" numbering: ranks of vertices, edge vertices and face vertices already
  created and new ones (inside) starting from VertexNum.
 */
   number_t kp1=order+1, kp2=order+2, kp1sq=kp1*kp1, kp1cu=kp1sq*kp1, ind=0;
   // rank = rkUnk U rkData
   vector<number_t> rank(kp1cu), rkUnk((order-1)*(order-1)*(order-1)), rkData(2*(3*order*order+1));
   // Bottom vertices, first slice (idem CartesianMesh<T_>::computeHOfV4CP)
   rank[0] = T.vertices_[0]; rank[order] = T.vertices_[4];
   rank[kp1sq-1] = T.vertices_[6]; rank[kp1sq-kp1] = T.vertices_[2];
   // Top vertices, last slice (same indices but translated by a "vertical offset" equal to k(k+1)^2)
   number_t voff = order*kp1sq;
   rank[voff] = T.vertices_[1]; rank[order+voff] = T.vertices_[5];
   rank[kp1sq-1+voff] = T.vertices_[7]; rank[kp1sq-kp1+voff] = T.vertices_[3];
   for (number_t i=0; i<nb_main_vertices_by_element_; i++) { rkData[ind++] = T.vertices_[i]; }

   // Edges: for {1,2,3,4},    index of 0,2,4,6 plus kp1sq
   //        for {5,6,7,8},    index of 0,1,4,5 plus kp1
   //        for {9,10,11,12}, index of 0,1,2,3 plus 1
   //                1      2            3            4          5    6         7          8               9  10      11           12
   number_t iebeg[]={kp1sq, 2*kp1sq-kp1, order+kp1sq, 2*kp1sq-1, kp1, voff+kp1, order+kp1, order+voff+kp1, 1, voff+1, kp1sq-order, kp1sq-order+voff};
   number_t ieoff[]={kp1sq, kp1sq,       kp1sq,       kp1sq,     kp1, kp1,      kp1,       kp1,            1, 1,      1,           1};
   for (number_t iedge=0; iedge<nb_edges_by_element_; iedge++) {
      vector<number_t> rkeV = T.rkOfHOVeOnEdge(order,iedge+1);
      vector<number_t>::const_reverse_iterator itrkeV=rkeV.rbegin();
      number_t joff = ieoff[iedge];
      for (number_t i=1, j=iebeg[iedge]; i<order; i++, j+=joff) { rank[j] = *itrkeV++; rkData[ind++] = rank[j]; }
   }

   // Faces:         1          2                3        4              5    6
   number_t ifbeg[]={kp1sq+kp1, order+kp1sq+kp1, 1+kp1sq, 2*kp1sq-order, kp2, kp2+voff};
   number_t ifoff[]={kp1,       kp1,             kp1sq,   kp1sq,         1,   1};
   number_t jfoff[]={kp1sq,     kp1sq,           1,       1,             kp1, kp1};
   for (number_t iface=0; iface<nb_faces_by_element_; iface++) {
      vector<number_t> rkfV = T.rkOfHOVeOnFace(order,iface+1);
      vector<number_t>::const_iterator itrkfV=rkfV.begin();
      number_t ioff = ifoff[iface], joff = jfoff[iface];
      for (number_t j=1, jbeg=ifbeg[iface]; j<order; j++, jbeg+=joff) {
         for (number_t i=1, ibeg=jbeg; i<order; i++, ibeg+=ioff) {
            rank[ibeg] = *itrkfV++; rkData[ind++] = rank[ibeg];
         }
      }
   }

   // Internal points (which are the unknowns)
   number_t valrk=VertexNum;
   for (number_t k=1, ir=kp2+kp1sq, ind=0; k<order; k++) {
      for (number_t j=1; j<order; j++) {
         for (number_t i=1; i<order; i++) { rkUnk[ind++] = valrk; rank[ir++] = valrk++; }
         ir += 2;// skip last point on this line and first one on next line
      }
      ir += 2*kp1;// skip last line on this slice and first line on next slice
   }

   // Make the mesh of hexahedrons
   vector< vector<number_t> > hexamesh(order*order*order,vector<number_t>(nb_main_vertices_by_element_));
   for (number_t k=0, in=0, ind=0; k<order; k++,in+=kp1) {
      for (number_t j=0; j<order; j++,in++) {
         for (number_t i=0; i<order; i++,ind++,in++) {
            hexamesh[ind][0] = rank[in];
            hexamesh[ind][1] = rank[in+kp1sq];
            hexamesh[ind][2] = rank[in+kp1];
            hexamesh[ind][3] = rank[in+kp1+kp1sq];
            hexamesh[ind][4] = rank[in+1];
            hexamesh[ind][5] = rank[in+kp1sq+1];
            hexamesh[ind][6] = rank[in+kp2];
            hexamesh[ind][7] = rank[in+kp2+kp1sq];
         }
      }
   }

   // Compute points in uniform positions.
   // Nota: The following function builds the matrix. However, in this context, the connectivity
   //       mesh is the same for all hexahedrons, so that a generic matrix could be built and
   //       its LLt decomposition computed only once. This could be done for optimization purpose.
   vector<Point> pts = this->unifMesh(hexamesh, Hexahedron::rkEdgeVertices(), rkUnk, rkData);
   // Add these points as new vertices. They are already in the correct order.
   for (vector<Point>::iterator itpts=pts.begin(); itpts<pts.end(); itpts++) {
      // add rank of the new vertex in the element list:
      T.vertices_.push_back(VertexNum);
      // store the new vertex in the general list listV_:
      this->listV_.push_back(Vertex(++VertexNum,localcod,*itpts));
   }
   } // end of curved patch case
   else {// default localization of points (barycenters)
/*
            Compute grid mesh based on vertices of hexahedron T
            by computing three levels of barycenters of couples of points.
 \verbatim
                                   3 +------------------+ 7
                                   / .                 /|
                                 /   .               /  |
                               /     .             /    |
                             /   f0e1.           /      |f1e1
       Face 0 --->         /         .         /        |
   (defined by         1 +------------------+ 5         |     <--- Face 1
   edges 0 and 1)        |           |      |           |         (defined by
                         |         2 +......|...........+ 6       edges 0 and 1)
                     f0e0|         .        |f1e0     /
                         |       .          |       /
                VP[0]-----> *  * . . * . . *| * <------VP[1]
                         |   .       P      |   /
                         | .                | /
                       0 +------------------+ 4
 \endverbatim
*/
   vector<number_t>& rV = T.vertices_;
   vector<Point> VPf0e0(2), VPf0e1(2), VPf1e0(2), VPf1e1(2);
   VPf0e0[0] = listV_[rV[0]].geomPt();   VPf1e0[0] = listV_[rV[4]].geomPt();
   VPf0e0[1] = listV_[rV[1]].geomPt();   VPf1e0[1] = listV_[rV[5]].geomPt();
   VPf0e1[0] = listV_[rV[2]].geomPt();   VPf1e1[0] = listV_[rV[6]].geomPt();
   VPf0e1[1] = listV_[rV[3]].geomPt();   VPf1e1[1] = listV_[rV[7]].geomPt();
   vector<Point> VPf0(2), VPf1(2), VP(2);
   real_t coef[2];
   Point P;
   for (number_t i1=1; i1<order; i1++) {
      coef[1] = i1;
      coef[0] = order-i1;
      VPf0[0] = barycenter(coef,VPf0e0);
      VPf0[1] = barycenter(coef,VPf0e1);
      VPf1[0] = barycenter(coef,VPf1e0);
      VPf1[1] = barycenter(coef,VPf1e1);
      for (number_t i2=1; i2<order; i2++) {
         coef[1] = i2;
         coef[0] = order-i2;
         VP[0] = barycenter(coef,VPf0);
         VP[1] = barycenter(coef,VPf1);
         for (number_t i3=1; i3<order; i3++) {
            coef[1] = i3;
            coef[0] = order-i3;
            P = barycenter(coef,VP);
           // add rank of the new vertex in the hexahedron list:
            T.vertices_.push_back(VertexNum);
           // store the new vertex in the general list listV_:
            listV_.push_back(Vertex(++VertexNum,localcod,P));
         }
      }
   }
   }// else
}
/*!
 Create the initial mesh (or "seed" of the mesh) to be subdivided for a geometry of revolution,
 i.e. defined by an axis of revolution and circular sections. The general shape is a truncated
 cone, that can be a cylinder if the radius is constant.
 In the following, the word "object" is used to designate the geometric shape.

 \param nbslices: number of slices of elements orthogonal to the axis of the object
                     If this input value is equal to 0, a number of slices is computed from
                     the geometrical data.
 \param radius1, radius2 : radii of the object
 \param CharacPts: the two end points of the axis of the object, corresponding respectively
                     to radius1 and radius2
 \param VertexNum: number of the last vertex created (modified on output)
 \param ElementNum: number of the last element created (modified on output)
 \param vSI: unused
 */
void HexahedronMesh::initMesh(const number_t nbslices,
                              const real_t radius1, const real_t radius2, const vector<Point>& CharacPts,
                              number_t& VertexNum, number_t& ElementNum, const vector<ShapeInfo>& vSI){
   real_t R1, R2, slice_height;
   Point P1, P2;
   vector<ShapeInfo> cvSI;
   string bottomPt, topPt, shape;
   bool iscone;
   DefaultGeometry *DG;
   SurfPlane *SP;
   SurfCone *SC;
   vector <PatchGeometry *> EBS;
   int nb_sl;
   initRevMesh(nbslices, radius1,radius2, CharacPts, vSI,
               R1,R2, P1,P2, cvSI, bottomPt, topPt, iscone, shape, DG, SP, SC, EBS, nb_sl, slice_height);

   number_t iPh = 0, iPhSD;
   title_ = shape + " - Hexahedron mesh";
   { // Definition of 3 boundaries...
      //vector<number_t> B_patch{1,2,3}, B_dimen{2,2,2}; Ready for C++11
      vector<number_t> B_patch(3,1), B_dimen(3,2);
      B_patch[1] = 2; B_patch[2] = 3;
      number_t nbBound = B_patch.size();
     // ... nbIntrf interfaces...
     // (four orthogonal internal half-planes containing the axes, a plane between
     //  two successive slices, plus eventually one plane between each end slice
     //  and the corresponding "hat" end shape)
      number_t nbIntrf = 4 + nb_sl-1, nbSbDom = nb_sl;
      if (EBS[0]->curvedShape()) {nbIntrf++; nbSbDom++;} // if non Flat
      if (EBS[1]->curvedShape()) {nbIntrf++; nbSbDom++;}
      vector<number_t> I_patch(nbIntrf), I_dimen(nbIntrf);
      number_t numPa = nbBound;
      for (size_t i=0; i<nbIntrf; i++) { I_patch[i] = ++numPa; I_dimen[i] = 2; }
     // ...and nbSbDom subdomains
     // (each slice plus eventually each "hat" end shape)
      vector<number_t> D_patch(nbSbDom), D_dimen(nbSbDom);
      for (size_t i=0; i<nbSbDom; i++) { D_patch[i] = ++numPa; D_dimen[i] = 3; }

      number_t nbBI =  nbBound + nbIntrf;
      number_t nbPatches =  nbBI + nbSbDom;
      vector<PatchGeometry *> P_geom(nbPatches);
     P_geom[0] = EBS[0];
     P_geom[1] = EBS[1];
      for (size_t i=2; i<nbBI; i++) { P_geom[i] = SP; }
      for (size_t i=nbBI; i<nbPatches; i++) { P_geom[i] = DG; }

      if (type_ == 0) {
         // P_geom = {EBS[0],EBS[1],SP, SP,...   SP, DG,...   DG}
         //           \__ nbBound  __/  \_nbIntrf_/  \_nbSbDom_/
         delete SC; // not needed
      }
      else {
         // P_geom = {EBS[0],EBS[1],SC, SP,...   SP, DG,...   DG}
         //           \__ nbBound  __/  \_nbIntrf_/  \_nbSbDom_/
         P_geom[2] = SC;
      }
      TG_ = TopoGeom(B_patch,B_dimen, I_patch,I_dimen, D_patch,D_dimen, P_geom);
      iPhSD = nbBound+nbIntrf;
   }
   refnum_t sBEs1, sBEs2, sBS; // localization code of the following patches
//       Description of the boundary patches
   TG_.setDescription(++iPh) = "Boundary: End surface on the side of end point " + bottomPt; sBEs1 = TG_.sigma(iPh); // patch 1
   TG_.setDescription(++iPh) = "Boundary: End surface on the side of end point " + topPt;    sBEs2 = TG_.sigma(iPh); // patch 2
   TG_.setDescription(++iPh) = "Boundary: Surface of the " + shape;                          sBS   = TG_.sigma(iPh); // patch 3
//       Description of the first four interface patches
   TG_.setDescription(++iPh) = "Interface: Internal half-plane 1 containing axis"; // patch 4
   number_t iPhIIp1 = iPh;
   TG_.setDescription(++iPh) = "Interface: Internal half-plane 2 containing axis"; // patch 5
   TG_.setDescription(++iPh) = "Interface: Internal half-plane 3 containing axis"; // patch 6
   TG_.setDescription(++iPh) = "Interface: Internal half-plane 4 containing axis"; // patch 7

// Define 4 "reference" points on the circle of radius R centered at the origin and
// 4 points on the circle of radius R/3.
// Then, rotate and translate them so that they lie on the bottom face of the object.
   vector<Point> Pt;
    {
    real_t R(R1/3.);
    Pt.push_back(Point(R,0,0));  // Pt[0]
    Pt.push_back(Point(0,R,0));  // Pt[1]
    Pt.push_back(Point(-R,0,0)); // Pt[2]
    Pt.push_back(Point(0,-R,0)); // Pt[3]
    }
    Pt.push_back(Point(R1,0,0));  // Pt[4]
    Pt.push_back(Point(0,R1,0));  // Pt[5]
    Pt.push_back(Point(-R1,0,0)); // Pt[6]
    Pt.push_back(Point(0,-R1,0)); // Pt[7]
   size_t nPt(Pt.size()); // = 8
   Vect Z(0,0,1);
   Vect AxV = SC -> AxisVector();
   Vect Nor = crossProduct(Z,AxV); // rotation axis (Z and AxV are both unitary vectors)
   Point Origin(0,0,0);
   real_t st = norm(Nor);
   if (st > theTolerance){
   // the axis of the object is not Z: we need to rotate the points around the axis (Origin,Nor)
      Nor *= (1./st); // normalize Nor
      real_t ct = dot(Z,AxV);
      for (size_t np=0; np<nPt; np++) { Pt[np] = rotInPlane(Pt[np],ct,st,Origin,Nor); }
   }
   Vect OCP1=toVector(Origin,P1);
   for (size_t np=0; np<nPt; np++) { Pt[np] = translate(Pt[np],1.,OCP1); }

   number_t sITF, sITFslice;
   number_t sDOM = TG_.sigma(iPhSD+1); // first subdomain: bottom end subdomain or first slice
   number_t rVaFirst = 0;
   ElementNum = minElementNum_ - 1;
   VertexNum = minVertexNum_ - 1;
/*!
 \verbatim
             6
              \
               2                    points 0 to 7
       7____3___\___1______5        are in the bottom circular section of the object
                 \
                  0
                   \                Internal half-plane 1 is (0,4 ; Z)
                    4               Internal half-plane 2 is (1,5 ; Z)
                                    Internal half-plane 3 is (2,6 ; Z)
                                    Internal half-plane 4 is (3,7 ; Z)
 \endverbatim
*/
// 1. Set of 8 vertices at the bottom of the first slice (on patch 1)
//    The last 4 also belong to the surface of the object (patch 3)
      for (size_t np=0; np<4; np++) {
         sITF = TG_.sigma(iPhIIp1 + np%4); // one internal half-plane
         listV_.push_back(Vertex(++VertexNum,sBEs1             |sITF|sDOM,Pt[np])); // rank 0 to 3
      }
      for (size_t np=4; np<nPt; np++) {
         sITF = TG_.sigma(iPhIIp1 + np%4); // one internal half-plane
         listV_.push_back(Vertex(++VertexNum,sBEs1|sBS|sITF|sDOM,Pt[np])); // rank 4 to 7
      }

// 2. Set of 8 vertices on top of each slice and associated hexahedra
   // We consider the center point of the larger basis of the object (P1).
   // The images of this point by translations along the axis will be the centers of successive
   // homotheties in each interface plane.
   Point homcen(P1);
   real_t prevRitrf = R1;
   for (int isl=1; isl<nb_sl; isl++,rVaFirst+=nPt) {
      for (size_t np=0; np<nPt; np++) { Pt[np] = translate(Pt[np],slice_height,AxV); }
      if (iscone) {
        homcen = translate(homcen,slice_height,AxV);
        // Compute the homothety ratio between 2 consecutive slices. homcen is the homothety center.
        real_t Ritrf = SC -> radiusAt(homcen), ratio = Ritrf / prevRitrf;
        prevRitrf = Ritrf;
        // Apply homothety to all the points in the interface.
        for (size_t np=0; np<nPt; np++) { Pt[np] = translate(homcen,ratio,toVector(homcen,Pt[np])); }
      }
      sITFslice = TG_.sigma(++iPh);                          // Next transversal interface
     ++iPhSD;
     sDOM = TG_.sigma(iPhSD) | TG_.sigma(iPhSD+1); // Two slices sharing this interface
      for (size_t np=0; np<4; np++) {
         sITF = TG_.sigma(iPhIIp1 + np%4)|sITFslice; // one internal half-plane + transversal interface
         listV_.push_back(Vertex(++VertexNum,             sITF|sDOM,Pt[np]));
      }
      for (size_t np=4; np<nPt; np++) {
         sITF = TG_.sigma(iPhIIp1 + np%4)|sITFslice; // one internal half-plane + transversal interface
         listV_.push_back(Vertex(++VertexNum,sBS|sITF|sDOM,Pt[np]));
      }
      // Associated hexahedra
      // 1. internal hexahedron: (a,b,c,d) = (0,1,2,3)
      number_t rVa = rVaFirst, rVb = rVa+1, rVc = rVb+1, rVd = rVc+1;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt));
      /* 2. external hexahedrons: (a,b,c,d) = rotations of (0,4,5,1)
       \verbatim
            d'+-------+c'
             /.      /.      Each hexahedron is of the form (a,a', d,d', b,b', c,c').
            / .     / .      Face 2 is (b,c,c',b') and is on the curved boundary.
         a'+-------+b'.
           | d+....|..+c
           | .     | .
           |.      |.
          a+-------+b
       \endverbatim
      */
     rVd = rVb; rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));
     rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));
     rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));
     rVa++; rVd=rVaFirst;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));
//      Description of the interface patch
      ostringstream ss;
      ss << "Interface: Section " << isl;
      TG_.setDescription(iPh) = ss.str();
   }
// 3. Set of 8 vertices on top of last slice (on patch 2) and associated hexahedra
      for (size_t np=0; np<nPt; np++) { Pt[np] = translate(Pt[np],slice_height,AxV); }
      if (iscone) {
        homcen = P2;
        // Compute the homothety ratio between 2 consecutive slices. homcen is the homothety center.
        real_t ratio = R2 / prevRitrf;
        // Apply homothety to all the points in the interface.
        for (size_t np=0; np<nPt; np++) { Pt[np] = translate(homcen,ratio,toVector(homcen,Pt[np])); }
      }
  ++iPhSD;
  sDOM = TG_.sigma(iPhSD);    // Last slice
      for (size_t np=0; np<4; np++) {
         sITF = TG_.sigma(iPhIIp1 + np%4); // one internal half-plane
         listV_.push_back(Vertex(++VertexNum,sBEs2             |sITF|sDOM,Pt[np]));
      }
      for (size_t np=4; np<nPt; np++) {
         sITF = TG_.sigma(iPhIIp1 + np%4); // one internal half-plane
         listV_.push_back(Vertex(++VertexNum,sBEs2|sBS|sITF|sDOM,Pt[np]));
      }
      // Associated hexahedra
      // 1. internal hexahedron: (a,b,c,d) = (0,1,2,3)
      number_t rVa = rVaFirst, rVb = rVa+1, rVc = rVb+1, rVd = rVc+1;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt));
      // 2. external hexahedrons: (a,b,c,d) = translations of (0,4,5,1)
  rVd = rVb; rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));
  rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));
  rVa++; rVd++;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));
  rVa++; rVd=rVaFirst;  rVb = rVa+4; rVc = rVd+4;
      listT_.push_back(Hexahedron(++ElementNum, rVa,rVa+nPt, rVd,rVd+nPt, rVb,rVb+nPt, rVc,rVc+nPt,2));

//       Description of the subdomain patches
   for (int isl=1; isl<=nb_sl; isl++) {
      ostringstream ss;
      ss << "Slice " << isl;
      TG_.setDescription(++iPh) = ss.str();
   }
}

/*!
 Define some macros in the file associated to the stream ftex in order to display
 the mesh using Fig4TeX.
 */
void HexahedronMesh::printTeXHeader(ostream& ftex) const {
   ftex << "\\def\\drawFace#1#2#3#4#5{" << endl;
   ftex << "\\figset(color=#5, fill=yes)\\figdrawline[#1,#2,#3,#4]" << endl;
   ftex << "\\figset(color=default, fill=no)\\figdrawline[#1,#2,#3,#4,#1]}" << endl;
   ftex << "\\def\\drawElem#1#2#3#4#5#6#7#8{" << endl;
   ftex << "\\figdrawline[#1,#2,#4,#3,#1,#5,#6,#8,#7,#5]" << endl;
   ftex << "\\figdrawline[#2,#6]\\figdrawline[#3,#7]\\figdrawline[#4,#8]}" << endl;
}

} // end of namespace subdivision
} // end of namespace xlifepp
