/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Tetrahedron.cpp
  \author Y. Lafranche
  \since 07 Dec 2007
  \date 08 Mar 2014

  \brief Implementation of xlifepp::subdivision::Tetrahedron class members and related functions
*/

#include "Tetrahedron.hpp"

#include <algorithm>
using namespace std;

namespace xlifepp {
namespace subdivision {

//-------------------------------------------------------------------------------
//  Static variables
//-------------------------------------------------------------------------------
// Index array defining the edges according to the following numering convention:
// edge #1=(4,1), edge #2=(4,2), edge #3=(4,3)
// edge #4=(2,3), edge #5=(1,3), edge #6=(1,2)
short Tetrahedron::rkEdge[/* nb_edges_ */][2] = {{3,0}, {3,1}, {3,2}, {1,2}, {0,2}, {0,1}};

// Index array defining the faces according to the following numering convention:
// face #1=(4,3,2), face #2=(4,1,3), face #3=(4,2,1), face #4=(1,2,3)
short Tetrahedron::rkFace[/* nb_faces_ */][3] = {{3,2,1}, {3,0,2}, {3,1,0}, {0,1,2}};

// Array containing the edge numbers (>=1) defining a face
// face #1=(3,-4,2), face #2=(1,5,3), face #3=(2,-6,1), face #4=(6,4,5)
short Tetrahedron::nuEdge[/* nb_faces_ */][3] = {{3,-4,2}, {1,5,3}, {2,-6,1}, {6,4,5}};
//-------------------------------------------------------------------------------
//  Constructors, Destructor
//-------------------------------------------------------------------------------
/*!
 Conversion constructor. Constructor by vertices, given by their rank in the list
 */
Tetrahedron::Tetrahedron(const number_t num, const number_t rV1, const number_t rV2,
                         const number_t rV3, const number_t rV4, const number_t bdSideNum)
:Simplex<Tetrahedron>(num,bdSideNum){
   vertices_[0] = rV1;
   vertices_[1] = rV2;
   vertices_[2] = rV3;
   vertices_[3] = rV4;
}

//-------------------------------------------------------------------------------
//  Other public member functions
//-------------------------------------------------------------------------------
/*!
 returns the numbers (>= 1) of the edges defining the face number i
 */
vector<short> Tetrahedron::numEdgesOfFace(const number_t i) {
   return vector<short> (nuEdge[i-1], nuEdge[i-1]+3);
}
/*!
 returns the ranks (>= 0) of the vertices defining the edges of the tetrahedron
 */
vector<pair<short,short> > Tetrahedron::rkEdgeVertices() {
   vector<pair<short,short> > V(nb_edges_);
   for (number_t i=0; i<nb_edges_; i++) { V[i] = make_pair(rkEdge[i][0],rkEdge[i][1]); }
   return V;
}
/*!
 Volume of the current tetrahedron: [(41 x 42) . 43] / 6
*/
real_t Tetrahedron::volume(const vector<Vertex>& listV) const {
   Vect V1=toVector(listV[vertices_[3]].geomPt(),listV[vertices_[0]].geomPt());
   Vect V2=toVector(listV[vertices_[3]].geomPt(),listV[vertices_[1]].geomPt());
   Vect V3=toVector(listV[vertices_[3]].geomPt(),listV[vertices_[2]].geomPt());
   Vect W = crossProduct(V1,V2);
   real_t v = dot(W,V3);
   return v/6;
}
/*!
 Radius of inscribed sphere inside the current tetrahedron: 3 V / (S1+S2+S3+S4)
*/
real_t Tetrahedron::inRadius(const vector<Vertex>& listV) const {
   real_t s=0;
   Vect V1;
   Vect V2=toVector(listV[vertices_[3]].geomPt(),listV[vertices_[2]].geomPt());
   for (number_t i=0; i<nb_main_vertices_-1; i++) {
      V1=V2;
      V2=toVector(listV[vertices_[3]].geomPt(),listV[vertices_[i]].geomPt());
      s += norm(crossProduct(V1,V2));
   }
   V1=toVector(listV[vertices_[2]].geomPt(),listV[vertices_[0]].geomPt());
   V2=toVector(listV[vertices_[2]].geomPt(),listV[vertices_[1]].geomPt());
   s += norm(crossProduct(V1,V2));
   return 6*volume(listV)/s;
}
/*!
 Diameter of the tetrahedron (longest edge)
 */
real_t Tetrahedron::diameter(const vector<Vertex>& listV) const {
   Vect V=toVector(listV[vertices_[1]].geomPt(),listV[vertices_[0]].geomPt());
   real_t dmax = norm(V);
   for (number_t j=2; j<nb_main_vertices_; j++) {
      for (number_t i=0; i<j; i++) {
         V=toVector(listV[vertices_[j]].geomPt(),listV[vertices_[i]].geomPt());
         real_t d = norm(V);
         if (d > dmax) dmax = d;
      }
   }
   return dmax;
}
/*!
 This function gives the local position of the vertices of any tetrahedron of
 the mesh corresponding to the numbering convention explained on top of the
 Tetrahedron class header file (Tetrahedron.hpp).

 The entry BC[i] in the returned vector BC contains a vector whose components
 BC[i][j] are equal to k*lambda_j, where k is the order of the mesh and lambda_j
 is the jth barycentric coordinate of the vertex number i+1 with respect to the
 4 main vertices of the tetrahedron(*). Thus, the returned vector gives the
 location of the vertices according to the way they are encountered in the
 numbering process implemented by the functions GeomFigureMesh<T_>::createHOV,
 GeomFigureMesh<T_>::createHOeV, SimplexMesh<T_>::createHOfV and TetrahedronMesh::createHOiV.

 (*) In fact, the last value is not stored since it is equal to k - sum of the
     previous values.
 */
vector< vector<number_t> > Tetrahedron::numberingOfVertices(const number_t Order) {
   vector< vector<number_t> > BC;
   const number_t nbVert = nb_main_vertices_; // = 4
   vector<number_t> V(nbVert,0);
   number_t order = Order;

   if (order < 1) {order = 1;}

//    main vertices (order 1)
   for (number_t indV=0; indV<nbVert; indV++) {
      V[indV] = order;
      BC.push_back(vector<number_t>(V.begin(),V.end()-1));
      V[indV] = 0;
   }
//    edge vertices (cf. GeomFigureMesh<T_>::createHOeV)
   for (number_t indEdge=0; indEdge<nb_edges_; indEdge++) {
      V = vector<number_t> (nbVert,0);
      for (number_t i1=1; i1<order; i1++) {
         V[rkEdge[indEdge][0]] = i1;
         V[rkEdge[indEdge][1]] = order-i1;
         BC.push_back(vector<number_t>(V.begin(),V.end()-1));
      }
   }
//    face vertices (cf. SimplexMesh<T_>::createHOfV)
   for (number_t indFace=0; indFace<nb_faces_; indFace++) {
      V = vector<number_t> (nbVert,0);
      for (number_t i1=1; i1<order; i1++) {
         V[rkFace[indFace][0]] = i1;
         for (number_t i2=1; i2<order-i1; i2++) {
            V[rkFace[indFace][1]] = i2;
            V[rkFace[indFace][2]] = order-i1-i2;
            BC.push_back(vector<number_t>(V.begin(),V.end()-1));
         }
      }
   }
//    internal vertices (cf. TetrahedronMesh::createHOiV)
   for (number_t i1=1; i1<order; i1++) {
      V[0] = i1;
      for (number_t i2=1; i2<order-i1; i2++) {
         V[1] = i2;
         for (number_t i3=1; i3<order-i1-i2; i3++) {
            V[2] = i3;
            V[3] = order-i1-i2-i3;
            BC.push_back(vector<number_t>(V.begin(),V.end()-1));
            }
         }
      }
   return BC;
}

} // end of namespace subdivision
} // end of namespace xlifepp
