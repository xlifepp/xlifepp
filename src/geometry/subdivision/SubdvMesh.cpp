/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SubdvMesh.cpp
  \author Y. Lafranche
  \since 17 Mar 2008
  \date  02 Aug 2015

  \brief Implementation of member functions of class xlifepp::Mesh related to subdivision
*/

#include "../Mesh.hpp"
#include "subUtil/SurfMeshQuaCube.hpp"
#include "subUtil/SurfMeshQuaDisk.hpp"
#include "subUtil/SurfMeshQuaRev.hpp"
#include "subUtil/SurfMeshQuaSet.hpp"
#include "subUtil/SurfMeshTriDisk.hpp"
#include "subUtil/SurfMeshTriRev.hpp"
#include "subUtil/SurfMeshTriSet.hpp"
#include "subUtil/SurfMeshTriSphere.hpp"

#include "subUtil/VolMeshHexCube.hpp"
#include "subUtil/VolMeshHexRev.hpp"
#include "subUtil/VolMeshTetCube.hpp"
#include "subUtil/VolMeshTetRev.hpp"
#include "subUtil/VolMeshTetSphere.hpp"

using namespace std;

namespace xlifepp {

void createTeXFile(const string_t& TeXFilename, subdivision::SubdivisionMesh *TM_p,
                   const float psi=-30, const float theta=20,
                   const number_t nbviews=1, const std::string& DimProj="2cm,orthogonal",
                   const bool withInterface=false, const bool withElems=false);

/*!
  Construction of a mesh of tetrahedrons inside a (portion of) a sphere or
  a mesh of triangles on the surface of a (portion of) a sphere, depending on
  the input shape.
  \param sph: sphere, geometrical body to be meshed
  \param shape: shape of the elements to be used:
                     tetrahedron for the mesh of the volume  of the sphere
                     triangle    for the mesh of the surface of the sphere
  \param nboctants: number of octants to be filled
  \param nbsubdiv: subdivision level (default value 0 gives the initial mesh)
  \param order: order of the elements in the final mesh (default=1)
  \param type: type of the subdivision [0 : flat, 1 or 2 : spheric] (default=1)
  \param TeXFilename: name of file to be used to write Fig4TeX instructions for
                       a graphical representation of the mesh (empty by default
                       in which case no file is created)
 More information in subdivision::VolMeshTetSphere.c++.
*/
void Mesh::subdvMesh(Ball& sph, const ShapeType shape, const int nboctants,
                     const number_t nbsubdiv, const number_t order,
                     const number_t type, const string_t& TeXFilename) {
  trace_p->push("Mesh::subdvMesh (Ball)");
  theLogStream << " +Mesh constructor(Ball, ShapeType, nboctants...)" << eol << "@ this=" << this << eol;
  if ( order == 0 ) { error("order_zero"); }

  isMadeOfSimplices_ = true; // this mesh is made of simplices

      // compute the mesh using the subdivision algorithm
      // points and elements are both numbered starting from 1
  subdivision::SubdivisionMesh *SM_p=nullptr;
  switch(shape) {
    case _tetrahedron: {
      subdivision::TetrahedronMesh *TM_p;
      SM_p = TM_p = new subdivision::VolMeshTetSphere(sph.rotations(), nboctants, nbsubdiv,
                                                      order, type, sph.radius(), sph.center());
      build3Delements(TM_p, shape);
      break;
    }
    case _triangle: {
      subdivision::TriangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshTriSphere(sph.rotations(), nboctants, nbsubdiv,
                                                       order, type, sph.radius(), sph.center());
      build2Delements(TM_p, shape);
      break;
    }
    default:
      error("mesh_not_handled", "sphere", words("shape", shape));
      break;
  }
  createTeXFile(TeXFilename, SM_p);
  delete SM_p;
  geometry_p->updateBB(computeBB());

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  // sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)
  trace_p->pop();
}

/*!
  Construction of a mesh of tetrahedrons or hexahedrons inside a (portion of) a cube or
  a mesh of triangles or quadrangles on the surface of a (portion of) a cube, depending on
  the input shape.
  \param cube: cube, geometrical body to be meshed
  \param shape: shape of the elements to be used:
                      - tetrahedron or hexahedron for the mesh of the volume of the cube
                      - quadrangle for the mesh of the surface of the cube
  \param nboctants: number of octants to be filled
  \param nbsubdiv: subdivision level (default value 0 gives the initial mesh)
  \param order: order of the elements in the final mesh (default=1)
  \param TeXFilename: name of file to be used to write Fig4TeX instructions for
                       a graphical representation of the mesh (empty by default
                       in which case no file is created)
 */
void Mesh::subdvMesh(Cube& cube, const ShapeType shape, const int nboctants,
                     const number_t nbsubdiv, const number_t order,
                     const string_t& TeXFilename) {
  trace_p->push("Mesh::subdvMesh (Cube)");
  theLogStream << " +Mesh constructor(Cube, ShapeType, nboctants...)" << eol << "@ this=" << this << eol;
  if ( order == 0 ) { error("order_zero"); }

      // compute the mesh using the subdivision algorithm
      // points and elements are both numbered starting from 1
  subdivision::SubdivisionMesh *SM_p=nullptr;
  switch(shape) {
    case _hexahedron: {
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::HexahedronMesh *TM_p;
      SM_p = TM_p = new subdivision::VolMeshHexCube(cube.rotations(), nboctants, nbsubdiv,
                                                    order, cube.edgeLen(), cube.center());
      build3Delements(TM_p, shape);
      break;
    }
    case _tetrahedron: {
      isMadeOfSimplices_ = true; // this mesh is made of simplices
      subdivision::TetrahedronMesh *TM_p;
      SM_p = TM_p = new subdivision::VolMeshTetCube(cube.rotations(), nboctants, nbsubdiv,
                                                    order, cube.edgeLen(), cube.center());
      build3Delements(TM_p, shape);
      break;
    }
    case _quadrangle: {
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::QuadrangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshQuaCube(cube.rotations(), nboctants, nbsubdiv,
                                                     order, cube.edgeLen(), cube.center());
      build2Delements(TM_p, shape);
      break;
    }
    default:
      error("mesh_not_handled", "cube", words("shape", shape));
      break;
  }
  createTeXFile(TeXFilename, SM_p);
  delete SM_p;
  geometry_p->updateBB(computeBB());

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  // sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)

  trace_p->pop();
}

/*!
 Construction of a mesh of tetrahedrons or hexahedrons inside a truncated cone, a cone
 or a cylinder, or a mesh of triangles or quadrangles on the surface of the object,
 depending on the input shape.
  \param trunk: geometrical body to be meshed (truncated cone, cone or cylinder)
  \param shape: shape of the elements to be used:
                     - tetrahedron or hexahedron for the mesh of the volume of the object
                     - triangle or quadrangle for the mesh of the surface of the object
  \param nbsubdom: number of sub-domains (slices of elements) orthogonal to the axis
                     of the object.
  \param nbsubdiv: subdivision level (default value 0 gives the initial mesh)
  \param order: order of the elements in the final mesh (default=1)
  \param type: type of the subdivision [0 : flat, 1 : curved] (default=1)
  \param TeXFilename: name of file to be used to write Fig4TeX instructions for
                       a graphical representation of the mesh (empty by default
                       in which case no file is created)
 */
void Mesh::subdvMesh(RevTrunk& trunk, const ShapeType shape, const number_t nbsubdom,
                     const number_t nbsubdiv, const number_t order,
                     const number_t type, const string_t& TeXFilename) {
  trace_p->push("Mesh::subdvMesh (RevTrunk)");
  theLogStream << " +Mesh constructor(RevTrunk, ShapeType, nbSubDomains...)" << eol << "@ this=" << this << eol;
  if ( order == 0 ) { error("order_zero"); }

      // compute the mesh using the subdivision algorithm
      // points and elements are both numbered starting from 1
  subdivision::SubdivisionMesh *SM_p=nullptr;
  if (trunk.radius1() == trunk.radius2()) {// cylinder
   switch(shape) {
    case _hexahedron: {
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::HexahedronMesh *TM_p;
      SM_p = TM_p = new subdivision::VolMeshHexCylinder(nbsubdom, nbsubdiv, order, type,
                                                        trunk.radius1(), trunk.center1(), trunk.center2());
      build3Delements(TM_p, shape, nbsubdom);
      break;
    }
    case _tetrahedron: {
      isMadeOfSimplices_ = true; // this mesh is made of simplices
      subdivision::TetrahedronMesh *TM_p;
      SM_p = TM_p = new subdivision::VolMeshTetCylinder(nbsubdom, nbsubdiv, order, type,
                                                        trunk.radius1(), trunk.center1(), trunk.center2(),
                                                        subdivision::GeomEndShape(trunk.endShape1()),
                                                        subdivision::GeomEndShape(trunk.endShape2()),
                                                        trunk.distance1(),trunk.distance2());
      build3Delements(TM_p, shape, nbsubdom);
      break;
    }
    case _quadrangle: {
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::QuadrangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshQuaCylinder(nbsubdom, nbsubdiv, order, type,
                                                         trunk.radius1(), trunk.center1(), trunk.center2(),
                                                         subdivision::GeomEndShape(trunk.endShape1()),
                                                         subdivision::GeomEndShape(trunk.endShape2()));
      build2Delements(TM_p, shape, nbsubdom);
      break;
    }
    case _triangle: {
      isMadeOfSimplices_ = true; // this mesh is made of simplices
      subdivision::TriangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshTriCylinder(nbsubdom, nbsubdiv, order, type,
                                                         trunk.radius1(), trunk.center1(), trunk.center2(),
                                                         subdivision::GeomEndShape(trunk.endShape1()),
                                                         subdivision::GeomEndShape(trunk.endShape2()),
                                                         trunk.distance1(),trunk.distance2());
      build2Delements(TM_p, shape, nbsubdom);
      break;
    }
    default:
      error("mesh_not_handled", "cylinder", words("shape", shape));
      break;
   }
  }
  else {// cone or truncated cone
   switch(shape) {
    case _hexahedron: {
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::HexahedronMesh *TM_p;
      SM_p = TM_p = new subdivision::VolMeshHexCone(nbsubdom, nbsubdiv, order, type,
                                                    trunk.radius1(), trunk.radius2(), trunk.center1(), trunk.center2());
      build3Delements(TM_p, shape, nbsubdom);
      break;
    }
    case _tetrahedron: {
      isMadeOfSimplices_ = true; // this mesh is made of simplices
      subdivision::TetrahedronMesh *TM_p;
      SM_p = TM_p = new subdivision::VolMeshTetCone(nbsubdom, nbsubdiv, order, type,
                                                    trunk.radius1(), trunk.radius2(), trunk.center1(), trunk.center2(),
                                                    subdivision::GeomEndShape(trunk.endShape1()),
                                                    subdivision::GeomEndShape(trunk.endShape2()),
                                                    trunk.distance1(),trunk.distance2());
      build3Delements(TM_p, shape, nbsubdom);
      break;
    }
    case _quadrangle: {
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::QuadrangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshQuaCone(nbsubdom, nbsubdiv, order, type,
                                                     trunk.radius1(), trunk.radius2(), trunk.center1(), trunk.center2(),
                                                     subdivision::GeomEndShape(trunk.endShape1()),
                                                     subdivision::GeomEndShape(trunk.endShape2()));
      build2Delements(TM_p, shape, nbsubdom);
      break;
    }
    case _triangle: {
      isMadeOfSimplices_ = true; // this mesh is made of simplices
      subdivision::TriangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshTriCone(nbsubdom, nbsubdiv, order, type,
                                                     trunk.radius1(), trunk.radius2(), trunk.center1(), trunk.center2(),
                                                     subdivision::GeomEndShape(trunk.endShape1()),
                                                     subdivision::GeomEndShape(trunk.endShape2()),
                                                     trunk.distance1(),trunk.distance2());
      build2Delements(TM_p, shape, nbsubdom);
      break;
    }
    default:
      error("mesh_not_handled", "cone", words("shape", shape));
      break;
   }
  }
  createTeXFile(TeXFilename, SM_p);
  delete SM_p;
  geometry_p->updateBB(computeBB());

  // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  // sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)
  trace_p->pop();
}

/*!
  Construction of a mesh using a subdivision algorithm starting from an initial elementary
  set of elements defined by the parameters pts (set of points among which vertices
  of the elements are found) and elems (list of elements). The boundaries of the domain are
  lists of numbers of vertices stored in the parameter bounds.

 \param pts: set of vertices of the initial mesh. This vector should contain at least N points,
   where N is the number of vertices of the element. They are implicitly numbered starting from 1.

 \param elems: set of elements of the initial mesh. Each element of the vector is a sequence
   of N numbers, each of them being the number of a vertex in the vector pts according to the
   implicit numbering starting from 1.
   Some points in the vector pts may not be referenced ; thus, only a subset of them may
   be used to define the initial mesh.

 \param bounds: list of the boundaries of the domain. A boundary is defined by the list of
   the vertex numbers lying on it, in the initial mesh ; no order is required.
   Several boundaries must be defined if, in the initial mesh (elems), there are internal
   edges whose vertices are both boundary vertices. In this case, the two vertices must belong
   to two different boundaries, otherwise the (internal) points created by the subdivision
   process along this edge would be considered as boundary vertices.

  \param shape: shape of the elements in the mesh
  \param nbsubdiv: subdivision level (default value 0 gives the initial mesh)
  \param order: order of the elements in the final mesh (default=1)
  \param TeXFilename: name of file to be used to write Fig4TeX instructions for
                       a graphical representation of the mesh (empty by default
                       in which case no file is created)
 */
void Mesh::subdvMesh(const std::vector<Point>& pts, const std::vector<std::vector<number_t> >& elems,
                     const std::vector<std::vector<number_t> >& bounds, const ShapeType shape,
                     const number_t nbsubdiv, const number_t order,
                     const string_t& TeXFilename) {
  trace_p->push("Mesh::subdvMesh (SetOfElems)");
  theLogStream <<  " +Mesh constructor(pts, elems, bounds...)" << eol << "@ this=" << this << eol;
  if ( order == 0 ) { error("order_zero"); }

  // compute the mesh using the subdivision algorithm
  // points and elements are both numbered starting from 1
  subdivision::SubdivisionMesh *SM_p=nullptr;
  switch (shape) {
    case _triangle: {// triangle  elems[0].size() == 3
      isMadeOfSimplices_ = true; // this mesh is made of simplices
      subdivision::TriangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshTriSet(pts, elems, bounds, nbsubdiv, order);
      build2Delements(TM_p, shape);
      break;
    }
    case _quadrangle: {// quadrangle   elems[0].size() == 4
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::QuadrangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshQuaSet(pts, elems, bounds, nbsubdiv, order);
      build2Delements(TM_p, shape);
      break;
    }
    case _tetrahedron: // tetrahedron   elems[0].size() == 4
    case _hexahedron: // hexahedron   elems[0].size() == 8
    default:
      error("mesh_not_handled", "SetOfElems", "");
      break;
  }
  if (pts[0].size() == 3) { createTeXFile(TeXFilename, SM_p); } // ,-30,20,1,"2cm,orthogonal",false,true
  else                    { createTeXFile(TeXFilename, SM_p,-90,90); } // 2D view
  delete SM_p;
  geometry_p->updateBB(computeBB());

  //compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  //sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)
  trace_p->pop();
}

/*!
  Construction of a mesh of a disk or a portion of a disk, made of triangles or
  quadrangles depending on the input shape.
  \param pdisk: portion of disk, geometrical body to be meshed
  \param shape: shape of the elements to be used: triangle or quadrangle
  \param nbsubdiv: subdivision level (default value 0 gives the initial mesh)
  \param order: order of the elements in the final mesh (default=1)
  \param type: type of the subdivision [0 : flat, 1 : circular] (default=1)
  \param TeXFilename: name of file to be used to write Fig4TeX instructions for
                       a graphical representation of the mesh (empty by default
                       in which case no file is created)
*/
void Mesh::subdvMesh(Disk& pdisk, const ShapeType shape,
                     const number_t nbsubdiv, const number_t order,
                     const number_t type, const string_t& TeXFilename) {
  trace_p->push("Mesh::subdvMesh(Disk)");
  theLogStream << " +Mesh constructor(Disk, ShapeType, nbsubdiv...)" << eol << "@ this=" << this << eol;
  if ( order == 0 ) { error("order_zero"); }

  // compute the mesh using the subdivision algorithm
  // points and elements are both numbered starting from 1
  subdivision::SubdivisionMesh *SM_p=nullptr;
  real_t tmin=pdisk.thetamin()*180/pi_, tmax=pdisk.thetamax()*180/pi_; // move to degree to agree with angle unit used in subdivision algorithms
  switch(shape) {
    case _triangle: {
      isMadeOfSimplices_ = true; // this mesh is made of simplices
      subdivision::TriangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshTriDisk(pdisk.nbSubdiv(), order, pdisk.type(),
                                                     pdisk.radius(), pdisk.center(), tmin, tmax);
      build2Delements(TM_p, shape);
      break;
    }
    case _quadrangle: {
      isMadeOfSimplices_ = false; // this mesh is not made of simplices
      subdivision::QuadrangleMesh *TM_p;
      SM_p = TM_p = new subdivision::SurfMeshQuaDisk(pdisk.nbSubdiv(), order, pdisk.type(),
                                                     pdisk.radius(), pdisk.center(), tmin, tmax);
      build2Delements(TM_p, shape);
      break;
    }
    default:
      error("mesh_not_handled", "portion of disk", words("shape", shape));
      break;
  }
  if (pdisk.center().size() == 3) { createTeXFile(TeXFilename, SM_p); }
  else                            { createTeXFile(TeXFilename, SM_p,-90,90); } // 2D view ,1,"2cm,orthogonal",false,true
  delete SM_p;
  geometry_p->updateBB(computeBB());

    // compute measures and orientation of mesh elements
  buildGeomData();
  setShapeTypes();

  // sides and sidesOfSides lists are not built (see buildSides and buildSideOfSides)
  trace_p->pop();
}

/*!
  Manages construction of domains of a mesh computed using the subdivision algorithm.

  The output of this function consist in the 3 vectors boundaryNames, interfaceNames,
  subdomainNames, and the name of the whole domain domName.
  The GeomDomain objects are created in the final Mesh object if the corresponding names
  in theses 3 vectors are not empty.

  This function takes into account 3 data, initialized during the creation of the Geometry
  object, or one of its derived classes. This Geometry object is accessed through the
  pointer geometry_p. These 3 data are:
   a. the domain name, accessed through geometry_p,
   b. the vector containing the side names (boundary names), accessed through geometry_p,
   c. only in the case of a revolution object, the number of subdomains along the axis,
      given by the argument nbsubdom.
      In this case, the interfaces between the couples of adjacent subdomains may be found
      in the last part of the vector containing the side names (b. above), after the "true"
      boundary names ; missing data are considered each to be the empty string.

  If the domain name (a. above) is empty, the subdomains and interfaces created by default
  by the subdivision algorithm are taken into account without change.

  If the vector containing the boundary names (b. above) has length 0, the boundaries
  created by default by the subdivision algorithm are taken into account without change.
  If the vector containing the boundary names (b. above) has length 1, a single boundary
  is created, and a default name is supplied if the name contained in the vector is empty.
  If this vector holds less names than there are boundary patches created by the subdivision
  algorithm, the function terminates with an error message. Otherwise, its content is scanned
  in order to make a unique domain for those patches bearing the same name. An empty name
  means no domain creation.

  The argument nbsubdom is taken into account at last, and only if the object is of revolution
  (cone, truncated cone or cylinder).
*/
void Mesh::manageDomains(subdivision::SubdivisionMesh *SM_p, number_t nbsubdom, dimen_t elementDim,
                         vector<string_t>& boundaryNames, vector<string_t>& interfaceNames,
                         vector<string_t>& subdomainNames, string_t& domName) {
  boundaryNames.clear();
  interfaceNames.clear();
  subdomainNames.clear();

//  1. Boundaries
  number_t nbBndryP = SM_p->numberOfBndryPatches();
  number_t patchNum = 1;// first boundary patch number
  vector<string_t> sn = geometry_p->sideNames();
  number_t nbsn = sn.size();

  if(nbsn == 0) {
    // Use default behavior of the subdivision algorithm.
    for (number_t numBoundary=1; numBoundary <= SM_p->numberOfBoundaries(); numBoundary++) {
      boundaryNames.push_back(SM_p->boundaryName(numBoundary));
    }
  }
  else if(nbsn == 1 && nbBndryP > 0) {
    // Definition of a single boundary, union of all the boundary patches
    vector< vector<number_t> > group(1);
    for (number_t numBndryP=1; numBndryP<=nbBndryP; numBndryP++, patchNum++) {
      group[0].push_back(patchNum);
    }
    if (SM_p->setBoundaries(group)) {
      // resize(1) in fact here since SM_p->numberOfBoundaries() has changed
      boundaryNames.resize(SM_p->numberOfBoundaries(),sn[0]);
      if (boundaryNames[0].empty()) { boundaryNames[0] = SM_p->boundaryName(1); }
    }
    else {
      cout << "*** manageDomains: redefinition of boundaries failed. (" << name_ << ")" << endl;
    }
  }
  else if (nbsn >= nbBndryP) {
    // Several boundaries are wanted: check whether some patches should be grouped
    vector< vector<number_t> > group;
    vector<string_t>::const_iterator itsnb(sn.begin()), itsn(itsnb);
    for (number_t numBndryP=1; numBndryP<=nbBndryP; numBndryP++, itsn++, patchNum++) {
      string_t name = sn[numBndryP-1];
      vector<string_t>::const_iterator itf;
      if (name.empty()) { itf = itsn; }
      else {
        // check if this name is the same as a previous one
        itf = find(itsnb,itsn,name);
      }
      if (itf != itsn) { // name already seen
        group[itf-itsnb].push_back(patchNum);
      }
      else { // new name, maybe empty
        vector<number_t> bnd;
        bnd.push_back(patchNum);
        group.push_back(bnd);
        boundaryNames.push_back(name);
      }
    }
    if (! SM_p->setBoundaries(group)) {
      cout << "*** manageDomains: redefinition of boundaries failed. (" << name_ << ")" << endl;
    }
    if (SM_p->numberOfBoundaries() != boundaryNames.size()) {
      cout << "*** manageDomains: problem in redefinition of boundaries. (" << name_ << ")" << endl;
    }
  }
  else {
    error("bad_size",words("shape", geometry_p->shape())+" sideNames",SM_p->numberOfBndryPatches(),nbsn);
  }

//  2. Subdomains and eventually, interfaces
//     -> domName data has priority over nbsubdom data
  domName = geometry_p->domName();
  if (domName.empty()) { // keep default behavior of subdivision algorithm
    domName = string("Omega");
    for (number_t numInterface=1; numInterface <= SM_p->numberOfInterfaces(); numInterface++) {
      interfaceNames.push_back(SM_p->interfaceName(numInterface));
    }
    for (number_t numSbDom=1; numSbDom <= SM_p->numberOfSubdomains(); numSbDom++) {
      subdomainNames.push_back(SM_p->subdomainName(numSbDom));
    }
  }
  else {
    // At this point, patchNum is the first interface patch number.
    patchNum = 1 + nbBndryP + SM_p->numberOfIntrfPatches(); // first subdomain patch number
    if (nbsubdom <= 1) {// general case (erroneous data nbsubdom=0 is handle here, 1 is assumed)
      // Discard interfaces
      interfaceNames.resize(SM_p->numberOfInterfaces(),string_t());
      // Make a single domain, union of all the subdomain patches
      number_t nbSbDomP = SM_p->numberOfSbDomPatches();
      vector< vector<number_t> > group(1);
      for (number_t numSbDomP=1; numSbDomP<=nbSbDomP; numSbDomP++, patchNum++) {
        group[0].push_back(patchNum);
      }
      if (SM_p->setSubdomains(group)) {
        // resize(1) in fact here since SM_p->numberOfSubdomains() has changed
        subdomainNames.resize(SM_p->numberOfSubdomains(),domName);
      }
      else {
        cout << "*** manageDomains: redefinition of subdomains failed. (" << name_ << ")" << endl;
      }
    }
    else {// this case is only relevant for an object of revolution
      switch (geometry_p->shape()) {
        default: cout << "*** manageDomains warning: not an object of revolution. (" << name_ << ")" << endl;
                break;
        case _revTrunk:
        case _revCylinder:
        case _revCone: break;
      }
      // Retrieve "end-shape" information from the geometry
      RevTrunk& geom(*geometry_p->revTrunk());
      bool endSh[2];
      switch (elementDim) {
        case 2:
          endSh[0] = geom.endShape1() != _gesNone;
          endSh[1] = geom.endShape2() != _gesNone;
          break;
        default:
          endSh[0] = geom.endShape1() != _gesNone && geom.endShape1() != _gesFlat;
          endSh[1] = geom.endShape2() != _gesNone && geom.endShape2() != _gesFlat;
          break;
      }
      number_t nbEnd=0;
      for (int i=0; i<2; i++) { if (endSh[i]) { nbEnd++; } }
      // For an object of revolution, each interface or subdomain corresponds to one patch.
      // nbsubdom subdomains along the axis are requested
      // and there are nbEnd "end-shape" subdomains
      number_t nbslices = SM_p->numberOfSubdomains() - nbEnd, nsl = nbslices/nbsubdom;
      // There are nbslices slices of elements. By default, the subdivision algorithm creates
      // an interface between two adjacent slices, plus one between each "end-shape" subdomain
      // and the adjacent slice.
      // Along the axis, we keep only the interfaces between two adjacent subdomains, each made
      // of nsl slices grouped together, since the number of slices is a multiple of nbsubdom.
      // The interface between an end-shape domain and its neighbor is preserved.
      // The subdomains names are built by adding an index after domName. The interface names are
      // taken in the last part of the vector sn, after the boundary names ; missing names are
      // replaced by empty strings, which prevents the corresponding mesh domains to be created.

      // The interface patches need not be reorganized. The following vector will be modified
      // according to sn contents.
      interfaceNames.resize(SM_p->numberOfInterfaces());
      vector<string_t>::const_iterator itsn(sn.begin()+nbBndryP);

      // Make nbsubdom+nbEnd groups of subdomain patches: one for each "end-shape" domain if any,
      // and nbsubdom groups of nsl slices.
      vector< vector<number_t> > group;
      dimen_t numSubDom=1, indIntrf=0;
      if (endSh[0]) {
        vector<number_t> sbd; sbd.push_back(patchNum++);
        group.push_back(sbd);
        subdomainNames.push_back(domName+tostring(numSubDom++));  // or SM_p->subdomainName(numSubDom++)
        bool noIntrf = elementDim == 2 && geom.endShape1() == _gesFlat;// end-shape subdomain, but no interface
        if ((! noIntrf) && (itsn < sn.end())) { interfaceNames[indIntrf] = *itsn; itsn++; }
        if (noIntrf) { indIntrf--; }
      }
      else { indIntrf--; }

      for (number_t numSD=1; numSD < nbsubdom; numSD++) {
        vector<number_t> sbd;
        for (number_t i=0; i<nsl; i++) { sbd.push_back(patchNum++); indIntrf++; }
        group.push_back(sbd);
        subdomainNames.push_back(domName+tostring(numSubDom++));  // or SM_p->subdomainName(numSubDom++)
        if (itsn < sn.end()) { interfaceNames[indIntrf] = *itsn; itsn++; }
      }
      // Last subdomain
      vector<number_t> sbd;
      for (number_t i=0; i<nsl; i++) { sbd.push_back(patchNum++); indIntrf++; }
      group.push_back(sbd);
      subdomainNames.push_back(domName+tostring(numSubDom++));  // or SM_p->subdomainName(numSubDom++)

      if (endSh[1]) {
        vector<number_t> sbd; sbd.push_back(patchNum++);
        group.push_back(sbd);
        subdomainNames.push_back(domName+tostring(numSubDom++));  // or SM_p->subdomainName(numSubDom++)
        bool noIntrf = elementDim == 2 && geom.endShape2() == _gesFlat;// end-shape subdomain, but no interface
        if ((! noIntrf) && (itsn < sn.end())) { interfaceNames[indIntrf] = *itsn; }
      }
      if (! SM_p->setSubdomains(group)) {
        cout << "*** manageDomains: redefinition of subdomains failed. (" << name_ << ")" << endl;
      }
      if (SM_p->numberOfSubdomains() != subdomainNames.size()) {
        cout << "*** manageDomains: problem in redefinition of subdomains. (" << name_ << ")" << endl;
      }
    }// end of else of if (nbsubdom <= 1)
  }// end of else of if (domName.empty())
}

//! returns the bounding box containing the mesh.
const vector<RealPair> Mesh::computeBB() {
  number_t dim(nodes[0].size());
  vector<RealPair> BB(dim);
  for (size_t i=0; i<dim; i++) { BB[i] = RealPair(nodes[0][i],nodes[0][i]); }
  for (vector<Point>::const_iterator it_nodes=nodes.begin(); it_nodes<nodes.end(); it_nodes++) {
    for (size_t i=0; i<dim; i++) {
      real_t z((*it_nodes)[i]);
      if (z<BB[i].first)  {BB[i].first  = z;}
      if (z>BB[i].second) {BB[i].second = z;}
    }
  }
  return BB;
}

//! create TeX file, using Fig4TeX macros, to draw the mesh
void createTeXFile(const string_t& TeXFilename, subdivision::SubdivisionMesh *TM_p,
                   const float psi, const float theta,
                   const number_t nbviews, const std::string& DimProj,
                   const bool withInterface, const bool withElems) {
  if (! TeXFilename.empty()) {
    std::ofstream ftex(TeXFilename.c_str());
    TM_p->printTeX(ftex,psi,theta,nbviews,DimProj,withInterface,withElems);
    ftex << "\\bye\n";
  }
}

} // end of namespace xlifepp
