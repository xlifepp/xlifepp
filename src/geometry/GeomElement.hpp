/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file GeomElement.hpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 13 apr 2012
  \date 5 jul 2013

  \brief Definition of the xlifepp::GeomElement class

  A xlifepp::GeomElement object represents the geometric support of mesh elements and is of two kinds:
    - plain mesh element: has always a xlifepp::MeshElement structure and not parent
    - side of a mesh element: has always a parent and may have a MeshElement structure

  A xlifepp::MeshElement object mainly carries
    - Reference Element (Lagrange finite element)
    - some numbering vectors for nodes, sides, sides of side and neighbor elements
*/


#ifndef GEOM_ELEMENT_HPP
#define GEOM_ELEMENT_HPP

#include "GeomMapData.hpp"
#include "finiteElements.h"
#include "utils.h"
#include "config.h"

namespace xlifepp
{
// forward class declarations
class Mesh;
class MeshDomain;

//------------------------------------------------------------------------------------
/*!
 \class MeshElement
 object represents the geometric support of a mesh element.
 It mainly stores global numberings of element
*/
//------------------------------------------------------------------------------------
class MeshElement
{
  public:
    std::vector<Point*> nodes;                //!< a subset of Mesh::nodes (direct access to nodes)
    std::vector<number_t> nodeNumbers;        //!< node numbers: a subset of Mesh::nodes (index starts from 1)
    std::vector<number_t> vertexNumbers;      //!< vertexNumbers: a subset of Mesh::nodes (may be equal to nodeNumbers, index starts from 1))
    std::vector<real_t> measures;             //!< length or area or volume of element and of its sides
    short int orientation;                    //!< element orientation (sign of jacobian determinant), 0 means unset orientation
    Point centroid;                           //!< centroid of the element, set by computeMeasures
    real_t size;                              //!< characteristic size of element, set by computeMeasures
    mutable GeomMapData* geomMapData_p;       //!< useful data for geometric map (jacobian,...)
    bool linearMap;                           //!< true if geometric map from ref element is linear (for instance first order simplicial element)

    //possibly additional useful information, only built if required
    mutable MeshElement* meltP1;              //!< P1 related mesh element if curved mesh element (2/3/4 first nodes) else itself
    std::vector<number_t> sideNumbers;        //!< element side numbers: a subset of Mesh::sides_ (built if required)
    std::vector<number_t> sideOfSideNumbers;  //!< element side of side numbers: : a subset of Mesh::sideOfSides_ (built if required)
    const MeshElement* firstOrderParent_;     //!< parent element when the current element is a first order element coming from an element of order>1
    std::vector<Point*> isoNodes;             //!< list of isoNodes (nodes in parameters domain) if required, default empty (deleted by destructor)
    number_t mapIndex = 1;                    //!< parametrization index (MeshElement has to be map in the same parameters domain)
    std::set<number_t> sideMapTransition;     //!< specify the sides where the parametrization is changing

  private:
    const RefElement* refElt_p;               //!< pointer to associated Reference Element (for geometric interpolation)
    number_t index_;                          //!< element index (=number_ of GeomElement)
    dimen_t spaceDim_;                        //!< space dimension (node dimension)

  public:
    //constructor/destructor
    MeshElement();                                     //!< default constructor
    MeshElement(const RefElement*, dimen_t, number_t); //!< constructor of plain element from ReferenceElement and space dimension
    MeshElement(const MeshElement&);                   //!< copy constructor
    ~MeshElement();                                    //!< destructor
    void setNodes(std::vector<Point>&);                //!< update node pointers vector from Mesh::nodes

  private:
    void operator=(const MeshElement&);                //!< no assignment operator=

  public:
    //accessors
    number_t index() const                             //! return element index
    {return index_;}
    number_t& index()                                  //! return element index
    {return index_;}
    dimen_t spaceDim() const                           //! return space dimension
    {return spaceDim_; }

    //properties
    number_t order() const                             //! return order of Lagrange interpolation
    {return refElt_p->order();}
    bool isSimplex() const                             //! return true for a segment or triangle or tetrahedron
    {return refElt_p->isSimplex();}
    real_t measure(const number_t sideNo = 0) const    //! return measure of element if side_no = 0, measure of side number side_no > 0
    {return measures[sideNo];}
    const RefElement* refElement(number_t i = 0) const //! return reference element if i=0 else refelement of side i
    {return refElt_p->refElement(i);}
    const GeomRefElement* geomRefElement(number_t i = 0) const  //! return geometric reference element if i=0 else geomrefelement of side i
    {return refElement(i)->geomRefElem_p;}
    dimen_t elementDim() const                         //! return space dimension of element
    {return geomRefElement()->dim();}
    ShapeType shapeType(number_t i = 0) const          //! return the shape type of element (i=0) and its sides (_segment, _triangle, ...)
    {return geomRefElement()->shapeType(i);}
    number_t vertexNumber(number_t i) const            //! return the global number of vertex i (i=1,...)
    {return vertexNumbers[i - 1];}
    number_t nodeNumber(number_t i) const              //! return the global number of node i (i=1,...)
    {return nodeNumbers[i - 1];}
    std::vector<number_t> verticesNumbers(number_t s = 0) const;   //!< return the global numbers of vertices of element (s=0), of side s>0
    bool checkLinearMap() const;                       //!< check if linear map is available

    Point& vertex(number_t i) const                    //! return the vertex i (i=1,...)
    {return *nodes[i-1];}
    Point& node(number_t i) const                      //! return the node i (i=1,...)
    {return *nodes[i-1];}
    const Point& vertexOnSide(number_t i, number_t s) const        //! return the vertex i (i=1,...) on side s
    {return *nodes[geomRefElement()->sideVertexNumber(i, s)-1];}
    const Point& vertexOnSideOfSide(number_t i, number_t ss) const //! return the vertex i (i=1,...) on side of side ss
    {return *nodes[geomRefElement()->sideOfSideVertexNumber(i, ss)-1];}
    const Point& vertexOppositeToSide(number_t s) const            //! return a vertex opposite to side s
    {return *nodes[geomRefElement()->vertexOppositeSide(s)-1];}

    number_t numberOfNodes() const                     //! returns number of nodes
    {return nodeNumbers.size();}
    number_t numberOfVertices() const                  //! returns number of vertices
    {return vertexNumbers.size();}
    number_t numberOfSides() const                     //! returns number of sides
    {return geomRefElement()->nbSides();}
    number_t numberOfSideOfSides() const               //! returns number of sides of sides
    {return geomRefElement()->nbSideOfSides();}

    // compute measures and orientation
    const GeomMapData& geomMapData(const std::vector<real_t>& p=std::vector<real_t>(),
                       bool withJ=false, bool withJm1=false, bool withN=false) const;  //! build the GeomMapData if not built
    void computeMeasures();                          //!< compute measure of element and its side
    void computeMeasure();                           //!< compute measure of element
    void computeMeasureOfSides();                    //!< compute measure sides of element
    void computeOrientation();                       //!< compute orientation of element: sign(det(jacobian))
    Point center() const;                            //!< compute the center of element
    real_t characteristicSize() const;               //!< compute a characteristic size of element (diameter of the circumscribed ball or max of length of edges)
    std::vector<real_t> normalVector(number_t s) const;  //!< return outward normal vector on side s>0, if s=0 normal vector to element
    const std::vector<real_t>& normalVector() const;     //!< return normal vector to element according to orientation
    std::vector<real_t> tangentVector(number_t e) const; //!< return tangent vector on edge

    // various utilities
    bool contains(const std::vector<real_t>&);                   //!< return true if mesh element contains a point given as Point or std::vector<real_t>
    Point projection(const std::vector<real_t>&, real_t&) const; //!< projection of a point onto meshelement
    std::vector<MeshElement*>splitP1() const;                    //!< split MeshElement in P1 (simplex and order 1) mesh elements
    MeshElement* toP1() const;                                   //!< associate a P1 meshelement to current (linearization)

    // GeomMapData management
    void clearGeomMapData()                          //! clear geometric map data (i.e geoMapData object)
    {if(geomMapData_p!=nullptr)delete geomMapData_p; geomMapData_p = nullptr;}

    void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<< (std::ostream&, const MeshElement&);

}; // end of class MeshElement ===================================================

std::ostream& operator<< (std::ostream&, const MeshElement&); //!< prints characteristics and point numbers
real_t distance(const MeshElement&, const MeshElement&); //!< distance from two MeshElement

typedef std::pair<MeshElement*, number_t> EltNumPair; //!< useful alias of a pair of MeshElement* and number_t

//------------------------------------------------------------------------------------
/*!
 \class GeomElement
 object represents the geometric support of mesh elements.
 It either a plain element or a side element (side of a GeomElement).
  - when it is a plain element, there is always a MeshElement associated to its but no parentSides
  - when it is a side element, there is at least a parent (parentSides is not empty) and by default no MeshElement,
    but if required, MeshElement may be constructed (buildSideMeshElement)

*/
//------------------------------------------------------------------------------------
class GeomElement;

//! useful typedef for GeomElement class
typedef std::pair<GeomElement*, number_t> GeoNumPair;  //!< alias of a pair of GeomElement* and a number
typedef real_t (*ColoringRule)(const GeomElement&, const std::vector<real_t>&); //!< alias of a function describing a GeomElement coloring rule
typedef real_t (*VectorColoringRule)(const GeomElement&, const std::vector<Vector<real_t> >&); //!< alias of a function describing a GeomElement coloring rule

class GeomElement
{
  protected:
    const Mesh* mesh_p;                         //!< pointer to the const mesh
    number_t number_;                           //!< unique id number of geometric element
    mutable MeshElement* meshElement_p;         //!< pointer to a MeshElement (may be 0 in case of side element)
    std::vector<GeoNumPair> parentSides_;       //!< if a side element, stores the pairs of parent element and side number

  public:
     //additionnal information
     number_t materialId;               //!< material id (default= 0)
     number_t domainId;                 //!< domain id (default=0)
     real_t theta;                      //!< angle to describe local curvature (theta in x-y plane), default 0
     real_t phi;                        //!< angle to describe local curvature (phi in x-z plane), default 0
     mutable GeomElement* twin_p;       //!< twin geomElement in case of geomElement on an interface
     mutable real_t color;              //!< useful to mark an element (default = 0)
     mutable real_t flag;               //!< useful to mark an element in internal process (default = 0)
     mutable void* userData_p;          //!< a free pointer allowing users to attach data of any type to the Element (casting and memory management are the responsability of the users)
     mutable std::vector<void*> userData_ps; //!< a free void pointer vector to attach data of any type to the Element (useful when mutiltithreading)

    //constructor and destructor
    GeomElement(const Mesh* m = nullptr, number_t n = 0) //! basic constructor
      : mesh_p(m), number_(n), meshElement_p(nullptr), materialId(0), domainId(0), theta(0), phi(0), twin_p(nullptr), color(0), flag(0), userData_p(nullptr) {}

    GeomElement(const Mesh*, const RefElement*,
                dimen_t, number_t);                 //!< construct a plain element from Mesh, ReferenceElement and space dimension
    GeomElement(GeomElement*, number_t, number_t);  //!< construct a side element from GeomElement parent and side number
    GeomElement(const GeomElement&);                //!< copy constructor
    GeomElement& operator=(const GeomElement&);     //!< assign operator

    ~GeomElement()                                  //!  destructor
    {if(meshElement_p != nullptr) { delete meshElement_p; }}

    //build utilities
    MeshElement* buildSideMeshElement() const;      //!< create a MeshElement object when element it a side element (not automatic)
    void deleteMeshElement();                       //!< destroy meshElement information

    //simple accessors
    const Mesh* meshP() const {return mesh_p;}      //!< return mesh pointer
    const Mesh*& meshP() {return mesh_p;}           //!< return mesh pointer
    number_t number() const {return number_;}       //!< returns element number
    number_t& number() {return number_;}            //!< returns element number (writable)
    bool hasMeshElement() const                     //! true if it handles a MeshElement structure
    {return meshElement_p != nullptr;}
    bool isSideElement() const                      //! true if it is a side element
    {return parentSides_.size() > 0;}
    number_t numberOfParents() const                //! number of parents of side element
    {return parentSides_.size();}

    //complex accessors depending of the GeomElement type (plain or side)
    const MeshElement* meshElement() const;                    //!< return meshElement_p if not null else error
    MeshElement* meshElement() ;                               //!< return meshElement_p if not null (non const) else error
    const GeomElement* parent(number_t i = 0) const;           //!< return i-th parent (no check)
    const GeoNumPair parentSide(number_t i) const;             //!< return i-th parent and its side (no check)
    std::vector<GeoNumPair>& parentSides()                     //! access to parentSides_
      {return parentSides_;}
    const std::vector<GeoNumPair>& parentSides() const         //! access to parentSides_ (const)
      {return parentSides_;}
    const GeomElement* parentInDomain(const MeshDomain*) const;//!< look for a parent in a domain (if exists return the first one)
    dimen_t elementDim() const;                                //!< return dimension of the element
    dimen_t spaceDim() const;                                  //!< return space dimension, i.e nodes dimension
    const RefElement* refElement(number_t i = 0) const;        //!< return reference element if i=0 else ref element of side i
    const GeomRefElement* geomRefElement(number_t i = 0) const //! return geomrefelement if i=0 else geomrefelement of side i
      {return refElement(i)->geomRefElement();}

    number_t numberOfNodes() const;                             //!< returns number of nodes of element
    number_t numberOfVertices() const;                          //!< returns number of vertices of element
    number_t numberOfSides() const;                             //!< returns number of sides (faces in 3D, edges in 2D)
    number_t numberOfSideOfSides() const;                       //!< returns number of edges (edges in 3D)
    ShapeType shapeType(number_t i = 0) const;                  //!< return the shape type of element (i=0) and its sides (_segment, _triangle, ...)
    number_t vertexNumber(number_t i) const;                    //!< return the global number of vertex i (i=1,...)
    number_t nodeNumber(number_t i) const;                      //!< return the global number of node i (i=1,...)
    std::vector<number_t> vertexNumbers(number_t s = 0) const;  //!< return the global numbers of vertices if s=0, of side s else
    std::vector<number_t> nodeNumbers(number_t s = 0) const;    //!< return the global numbers of nodes if s=0, of side s else
    string_t encodeElement(number_t s = 0, number_t ss=0) const;//!< string key of element or side element or side of side element from ascending vertex numbers
    string_t encodeElementNodes(number_t s = 0) const;          //!< string key of element or side element from ascending node numbers
    void clearGeomMapData()                                     //! clear geometric map data (i.e geoMapData object)
    {if(meshElement_p!=nullptr) meshElement_p->clearGeomMapData();}
    const Point& vertex(number_t i, number_t s=0) const;         //!< return the ith vertex of element or side s (i=1,...)
    const Point& sideOfSideVertex(number_t i, number_t ss) const;//!< return the ith vertex of side of side ss>=1 (i=1,...)
    const Point& edgeVertex(number_t i, number_t e) const;       //!< return the ith vertex of edge e>=1 (i=1,...) (side in 2D, side of side in 3D)
    const GeomElement* sideElement(number_t s) const;            //! if exists, return the side element associated to side s>=1 else return 0

    //various stuff
    GeoNumPair elementSharingSide(number_t s) const;                                     //!< return the adjacent element by side s as a pair of (GeomElement*, number_t)
    std::vector<GeoNumPair> elementsSharingVertex(number_t v, bool o = false) const;     //!< return the list of adjacent elements by vertex v
    std::vector<GeoNumPair> elementsSharingSideOfSide(number_t s, bool o = false) const; //!< return the list of adjacent elements by side of side s
    std::vector<GeomElement*> splitP1() const;                //!< split element in first order elements and return as a list
//    std::vector<GeomElement*> splitO1() const;                //!< split element in first order elements and return as a list (unused)
    real_t measure(const number_t sideNo = 0) const;          //! return measure of element if side_no = 0, measure of side number side_no > 0
    real_t size() const;                                      //!< characteristic size of element (computed if not available)
    Point center() const;                                     //!< compute the center of element
    bool contains(const std::vector<real_t>&);                //!< true if point p as vector belongs to element
    Point projection(const std::vector<real_t>&, real_t&);    //!< projection of a point onto element
    std::vector<real_t> normalVector(number_t s) const;       //!< return outward normal vector on side s>0 (not normalized) or normal to element when s=0
    const std::vector<real_t>& normalVector() const;          //!< return normal vector to element according to orientation
    std::vector<real_t> tangentVector(number_t) const;        //!< return tangent vector on an edge (not normalized)

    //print utilities
    void printVertices(std::ostream&,IOFormat f=_undefFormat) const;  //!< print vertices in default format, raw format and matlab format
    void printVertices(PrintStream& os,IOFormat f=_undefFormat) const {printVertices(os.currentStream(),f);}
    void printVertices(CoutStream& os, IOFormat f=_undefFormat) const {printVertices(os.printStream->currentStream(),f);}
    void print(std::ostream&) const;                                  //!< print geomelement characteristics
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<< (std::ostream&, const GeomElement&);
};

std::ostream& operator<< (std::ostream&, const GeomElement&); //!< outputs geomelement characteristics
std::ostream& operator<< (std::ostream&, const GeoNumPair&); //!< outputs GeoNumPair characteristics

//! create the side index  map of a list of elements
void createSideIndex(const std::vector<GeomElement*>& elements,
                     std::map<string_t,std::vector<GeoNumPair> >& sideIndex);
//! create the side index  map of a MeshDomain
void createSideIndex(const MeshDomain & mdom,
                     std::map<string_t,std::vector<GeoNumPair> >& sideIndex);
//! create the side index map of side elements of a list of elements
void createSideEltIndex(const std::vector<GeomElement*>& elements,
                        std::map<string_t, GeomElement* >& sideEltIndex);

//Overloaded operators == and < (to sort Lists), and << for output
bool operator== (const GeomElement&, const GeomElement&);  //!<overload operator == to sort elements
bool operator< (const GeomElement&, const GeomElement&);   //!<operator < to sort elements

/*!
 \class GeomElementLess
 a very small class defining a functor to sort std::vector of GeomElement*
        ascendingly  according to their number
*/
class GeomElementLess
{
  public:
    bool operator()(const GeomElement* x, const GeomElement* y)   //! operator < to sort elements
    {return x->number() < y->number(); }
}; // end of class GeomElementLess ===============================================

//------------------------------------------------------------------------------------
/*
 some utilities
*/
//------------------------------------------------------------------------------------
real_t triangleArea(const Point&, const Point&, const Point&);                        //!< area of a triangle
real_t tetrahedronVolume(const Point&, const Point&, const Point&, const Point&);     //!< volume of a tetrahedron
real_t defaultColoringRule(const GeomElement& gelt, const std::vector<real_t>& val);  //!< default Geomelement coloring rule
real_t defaultVectorColoringRule(const GeomElement& gelt, const std::vector<Vector<real_t> >& val);  //!< default Geomelement vector coloring rule
void childNodes(Node<GeomElement>*, const MeshDomain*, GeomElement*, std::set<GeomElement*>&); //!< internal tool
} // end of namespace xlifepp

#endif /* GEOM_ELEMENT_HPP */


//===============================  oldstuff of GeomElment class =============================================================

//real_t* coords(const number_t ) const;             //!<returns pointer to coordinates of point of local number

//Global numbering
//    void minmaxPointNumbers (std::pair<int, int>&) const;     //!<compute min and max of point numbers in element
//    void checkPointNumbers (const std::pair<int, int>&,
//                            std::vector<bool>&) const;
//    void computeNumberOfVertices (number_t&,
//                                  std::vector<number_t>&); //!<compute number of vertices in elements not seen in previous elements
//    number_t vertices (std::vector<Point>&,
//                     std::vector<number_t>&);              //!<create vertex global numbering
//   number_t sides(std::vector<GeomSide>&,
//                std::vector<number_t>&,
//                std::vector<std::vector<number_t> >&);   //!<create edge global numbering
//
//   number_t sideofsides(std::vector<GeomSide>&, std::vector<number_t>&, std::vector<std::vector<number_t> >&);
//   //!<create face global numbering
//
//   void reorderVertices(number_t sideNo, number_t* veReordered); //!<reorder face vertices on face (side_no = 1, ...) using vertex global numbering
//
//   number_t sharedVertices(const MeshElement&,std::vector< std::pair<number_t,number_t> >&) const;
//   //!<returns the number of vertices shared with another element and the pairs of local numbers of vertices in elements
//
//   void reorderSharedVertices(number_t,std::vector< std::pair<number_t,number_t> >&);
//   //!<sort pairs of local numbers of shared vertices according to ascending global numbering
//
//   number_t sharedEdges(const MeshElement&,std::vector< pair<number_t,number_t> >&) const;
//   //!<returns the number of sides shared with another element and the pairs of local numbers of edges in elements
//
//   number_t sharedSideOfSides(const MeshElement&,std::vector< pair<number_t,number_t> >&) const;
//   //!<returns the number of faces shared with another element and the pairs of local numbers of faces in elements

//    bool isNeighbor(const MeshElement& ge) const;   //!<is element a neighbor of another ?
//
//    //  Elementary computations
//    void computeMeasure();
//    void computeMeasureOfSides();
//    void orientation(MeshElementStorage&);    //!<computes element orientation
//    void orientation(const real_t);             //!<set element orientation
//    void normalize(Vector<real_t>&) const;      //!<normalize normal std::vector into a outward unit normal std::vector
//    void nullNormalVector(real_t) const;
