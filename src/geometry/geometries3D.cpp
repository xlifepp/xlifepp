/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries3D.cpp
  \authors N. Kielbasiewicz, Y. Lafranche
  \since 18 oct 2012
  \date 30 jul 2015

  \brief This file contains the implementation of methods of Volume class and Polyhedron and Ellipsoid classes
  defined in geometries3D.hpp
*/

#include "geometries2D.hpp"
#include "geometries3D.hpp"
#include "geometries_utils.hpp"

namespace xlifepp
{

//===========================================================
// Volume and child classes member functions
//===========================================================

//! default volume is inside bounding box [0,1]^3
Volume::Volume()
  : Geometry(BoundingBox(0., 1., 0., 1., 0., 1.), 3) {}

void Volume::buildParam(const Parameter& p)
{
  trace_p->push("Volume::buildParam");
  ParameterKey key = p.key();

  switch (key)
  {
    case _pk_face_names:
    case _pk_side_names:
    {
      if (p.type() == _string) { sideNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { sideNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    case _pk_edge_names:
    {
      if (p.type() == _string) { sideOfSideNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { sideOfSideNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    case _pk_vertex_names:
    {
      if (p.type() == _string) { sideOfSideOfSideNames_.resize(1, p.get_s()); }
      else if (p.type() == _stringVector) { sideOfSideOfSideNames_ = p.get_sv(); }
      else { error("param_badtype", words("value", p.type()), words("param key", key)); }
      break;
    }
    default: Geometry::buildParam(p); break;
  }
  for (number_t i = 0; i < sideNames_.size(); ++i)
  {
    if (sideNames_[i].find("#") == 0) { error("domain_name_invalid"); }
    if (sideNames_[i].find(" ") == 0) { error("domain_name_invalid"); }
  }
  for (number_t i = 0; i < sideOfSideNames_.size(); ++i)
  {
    if (sideOfSideNames_[i].find("#") == 0) { error("domain_name_invalid"); }
    if (sideOfSideNames_[i].find(" ") == 0) { error("domain_name_invalid"); }
  }
  for (number_t i = 0; i < sideOfSideOfSideNames_.size(); ++i)
  {
    if (sideOfSideOfSideNames_[i].find("#") == 0) { error("domain_name_invalid"); }
    if (sideOfSideOfSideNames_[i].find(" ") == 0) { error("domain_name_invalid"); }
  }
  trace_p->pop();
}

void Volume::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Volume::buildDefaultParam");
  switch (key)
  {
    case _pk_face_names:
    case _pk_side_names: sideNames_.resize(0); break;
    case _pk_edge_names: sideOfSideNames_.resize(0); break;
    case _pk_vertex_names: sideOfSideOfSideNames_.resize(0); break;
    default: Geometry::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Volume::getParamsKeys()
{
  std::set<ParameterKey> params = Geometry::getParamsKeys();
  params.insert(_pk_side_names);
  params.insert(_pk_face_names);
  params.insert(_pk_edge_names);
  params.insert(_pk_vertex_names);
  return params;
}

bool Volume::isTranslated(const Geometry& g, Point& T) const
{
  if (shape() != g.shape()) { return false; }
  return isTranslatedPoints(p_, g.volume()->p_, T);
}

//=======================================================================================================
//                                  Polyhedron member functions
//=======================================================================================================
//! default polyhedron is tetrahedron (0,0,0) (1,0,0) (0,1,0) (0,0,1)
Polyhedron::Polyhedron() : Volume()
{
  p_.resize(4);
  p_[0] = Point(0., 0., 0.);
  p_[1] = Point(1., 0., 0.);
  p_[2] = Point(0., 1., 0.);
  p_[3] = Point(0., 0., 1.);
  faces_.resize(4);
  faces_[0] = new Triangle(p_[0], p_[1], p_[2]);
  faces_[1] = new Triangle(p_[0], p_[1], p_[3]);
  faces_[2] = new Triangle(p_[1], p_[2], p_[3]);
  faces_[3] = new Triangle(p_[2], p_[0], p_[3]);
  shape_ = _polyhedron;
  computeMB();
}

void Polyhedron::buildP()
{
  for (number_t i = 0; i < faces_.size(); ++i)
  {
    boundingBox += faces_[i]->boundingBox;
    for (number_t j = 0; j < faces_[i]->p().size(); ++j)
    {
      int_t foundId = -1;
      for (number_t k = 0; k < p_.size(); ++k)
      {
        if (p_[k] == faces_[i]->p(j + 1)) { foundId = j; }
      }
      if (foundId == -1) { p_.push_back(faces_[i]->p(j + 1)); }
    }
  }
}

void Polyhedron::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Polyhedron::build");
  shape_ = _polyhedron;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _polyhedron)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
  }

  // faces has to be set (no default values)
  if (params.find(_pk_faces) != params.end()) { error("param_missing", "faces"); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  buildP();
  //setEdges();
  updateSideNames();
  computeMB();
  trace_p->pop();
}

void Polyhedron::buildParam(const Parameter& p)
{
  trace_p->push("Polyhedron::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_faces:
    {
      switch (p.type())
      {
        case _pointer:
        {
          const std::vector<Polygon>& faces = *reinterpret_cast<const std::vector<Polygon>*>(p.get_p());
          faces_.resize(faces.size());
          for (number_t i = 0; i < faces_.size(); ++i) { faces_[i] = faces[i].clonePG(); }
          break;
        }
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Volume::buildParam(p); break;
  }
  trace_p->pop();
}

void Polyhedron::buildDefaultParam(ParameterKey key)
{
  Volume::buildDefaultParam(key);
}

std::set<ParameterKey> Polyhedron::getParamsKeys()
{
  std::set<ParameterKey> params = Volume::getParamsKeys();
  params.insert(_pk_faces);
  // _side_names, _face_names, _edge_,ames and _vertex_names are disabled
  params.erase(_pk_side_names);
  params.erase(_pk_face_names);
  params.erase(_pk_edge_names);
  params.erase(_pk_vertex_names);
  return params;
}

Polyhedron::Polyhedron(Parameter p1) : Volume()
{
  std::vector<Parameter> ps={p1};
  build(ps);
}

Polyhedron::Polyhedron(Parameter p1, Parameter p2) : Volume()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Polyhedron::Polyhedron(const Polyhedron& ph) : Volume(ph)
{
  //edges_=ph.edges_;
  faces_.resize(ph.faces_.size());
  for (number_t i = 0; i < faces_.size(); ++i)
  {
    faces_[i] = ph.faces_[i]->clonePG();
  }
}

string_t Polyhedron::asString() const
{
  string_t s("Polyhedron (");
  s += tostring(faces_.size()) + " faces, ";
  //s += tostring(edges_.size()) + " edges )";
  return s;
}

std::vector<const Point*> Polyhedron::boundNodes() const
{
  std::vector<const Point*> nodes(p_.size());
  for (number_t i = 0; i < p_.size(); ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

std::vector<Point*> Polyhedron::nodes()
{
  std::vector<Point*> nodes(p_.size());
  for (number_t i = 0; i < p_.size(); ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

std::vector<const Point*> Polyhedron::nodes() const
{
  std::vector<const Point*> nodes(p_.size());
  for (number_t i = 0; i < p_.size(); ++i) { nodes[i] = &p_[i]; }
  return nodes;
}

//update side names from face domain names
void Polyhedron::updateSideNames()
{
  if (shape_ != _polyhedron) { return; } // to prevent a call from a Polyhedron child which has its own sides naming
  sideNames_.resize(faces_.size(), string_t(""));
  std::vector<Polygon*>::const_iterator itf = faces_.begin();
  std::vector<string_t>::iterator itn = sideNames_.begin();
  bool hasSideNames = false;
  for (; itf != faces_.end(); ++itf, ++itn) //loop on faces
  {
    *itn = (*itf)->domName();
    if (*itn != "") { hasSideNames = true; }
  }
  if (!hasSideNames) { sideNames_.clear(); } // no side names
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Polyhedron::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves, curves2;

  for (number_t i = 0; i < faces_.size(); ++i)
  {
    for (number_t j = 0; j < faces_[i]->curves().size(); ++j)
    {
      curves2 = faces_[i]->curves();
      if (findBorder(curves2[j], curves) == -1)
      {
        curves.push_back(curves2[j]);
      }
    }
  }
  return curves;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Polyhedron::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(faces_.size());
  for (number_t i = 0; i < faces_.size(); ++i)
  {
    surfs[i] = faces_[i]->surfs()[0];
  }
  return surfs;
}

//! collect in a list all canonical geometry's with name n
void Polyhedron::collect(const string_t& n, std::list<Geometry*>& geoms) const
{
  if (domName_ == n) { geoms.push_back(const_cast<Geometry*>(static_cast<const Geometry*>(this))); }
  //loop on sides
  std::vector<Polygon*>::const_iterator its = faces_.begin();
  for (; its != faces_.end(); ++its)
  {
    if ((*its)->domName() == n) { geoms.push_back((*its)->clone()); } //create geometry related to side by cloning
  }
}

//! create boundary geometry of polyhedron as a composite (union of faces)
Geometry& Polyhedron::buildBoundary() const
{
  boundaryGeometry_ = new Geometry(boundingBox, 2);
  Geometry& g = *boundaryGeometry_;
  g.shape(_composite);
  g.minimalBox = minimalBox;
  string_t name = "";
  if (domName_ != "") { name = domName_ + "_boundary"; }
  if (name != "") { g.domName(name); }

  std::map<number_t, Geometry*>& cps = g.components();
  std::map<number_t, std::vector<number_t> >& geos = g.geometries();
  for (number_t i = 0; i < faces_.size(); i++)
  {
    string_t fname = "";
    if (sideNames_.size() == 1) { fname = sideNames_[0]; }
    else if (sideNames_.size() > i) { fname = sideNames_[i]; }
    Polygon& pol = *faces_[i];
    std::vector<Point>& vs = pol.p();
    std::vector<real_t>& hs = pol.h();
    std::vector<number_t>& ns = pol.n();
    ShapeType sh = pol.shape();
    theCout << "face " << i << eol << pol << eol << std::flush;
    switch (sh)
    {
      case _triangle: if (hs.size() > 0) cps[i] = new Triangle(_v1 = vs[0], _v2 = vs[1], _v3 = vs[2], _domain_name = fname, _hsteps = hs);
        else            { cps[i] = new Triangle(_v1 = vs[0], _v2 = vs[1], _v3 = vs[2], _domain_name = fname, _nnodes = ns); }
        break;
      case _rectangle: if (hs.size() > 0) cps[i] = new Rectangle(_v1 = vs[0], _v2 = vs[1], _v4 = vs[3], _domain_name = fname, _hsteps = hs);
        else            { cps[i] = new Rectangle(_v1 = vs[0], _v2 = vs[1], _v4 = vs[3], _domain_name = fname, _nnodes = ns); }
        break;
      case _parallelogram: if (hs.size() > 0) cps[i] = new Parallelogram(_v1 = vs[0], _v2 = vs[1], _v4 = vs[3], _domain_name = fname, _hsteps = hs);
        else            { cps[i] = new Parallelogram(_v1 = vs[0], _v2 = vs[1], _v4 = vs[3], _domain_name = fname, _nnodes = ns); }
        break;
      case _quadrangle: if (hs.size() > 0) cps[i] = new Quadrangle(_v1 = vs[0], _v2 = vs[1], _v3 = vs[2], _v4 = vs[3], _domain_name = fname, _hsteps = hs);
        else            { cps[i] = new Quadrangle(_v1 = vs[0], _v2 = vs[1], _v3 = vs[2], _v4 = vs[3], _domain_name = fname, _nnodes = ns); }
        break;
      case _polygon:    if (hs.size() > 0) cps[i] = new Polygon(_vertices = vs, _domain_name = fname, _hsteps = hs);
        else            { cps[i] = new Polygon(_vertices = vs, _domain_name = fname, _nnodes = ns); }
        break;
      default:
        break;
    }
    geos[i].push_back(i);
  }
  return g;
}

//=======================================================================================================
//                               Tetrahedron member functions
//=======================================================================================================
//! default tetrahedron is tetrahedron (0,0,0) (1,0,0) (0,1,0) (0,0,1)
Tetrahedron::Tetrahedron() : Polyhedron()
{
  n_.resize(6, 2);
  shape_ = _tetrahedron;
  computeMB();
  setFaces();
}

void Tetrahedron::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Tetrahedron::build");
  shape_ = _tetrahedron;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _faces key is replaced by _v1, _v2 and _v3
  params.erase(_pk_faces);

  p_.resize(4);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _tetrahedron)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (v1,v2,v3,v4) has to be set (no default values)
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  if (params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(6, n0); }
    else if (n_.size() != 6) { error("bad_size", "nnodes", 6, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(4, h0); }
    else if (h_.size() != 4) { error("bad_size", "hsteps", 4, h_.size()); }
  }

  boundingBox = BoundingBox(p_[0], p_[1], p_[2], p_[3]);
  computeMB();
  setFaces();
  trace_p->pop();
}

//set faces an edges
void Tetrahedron::setFaces()
{
  faces_.resize(4);
  std::vector<string_t> facenames(4);
  if (sideNames_.size() >= 4) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3]}; }
  else if (sideNames_.size() >= 1) { facenames.resize(4, sideNames_[0]); }
  std::vector<std::vector<string_t>> edgenames(4);
  if (sideOfSideNames_.size() >= 6)
  {
    edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2]};
    edgenames[1] = {sideOfSideNames_[0], sideOfSideNames_[4], sideOfSideNames_[3]};
    edgenames[2] = {sideOfSideNames_[1], sideOfSideNames_[5], sideOfSideNames_[4]};
    edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[3], sideOfSideNames_[5]};
  }
  else if (sideOfSideNames_.size() >= 1)
  {
    edgenames.resize(4, std::vector<string_t>(3,sideOfSideNames_[0]));
  }
  std::vector<std::vector<string_t>> vertexnames(4);
  if (sideOfSideOfSideNames_.size() >= 4)
  {
    vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2]};
    vertexnames[1] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[3]};
    vertexnames[2] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
    vertexnames[3] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3]};
  }
  else if (sideOfSideOfSideNames_.size() >= 1)
  {
    vertexnames.resize(4, std::vector<string_t>(3, sideOfSideOfSideNames_[0]));
  }
  
  if (h_.size() != 0)
  {
    faces_[0] = new Triangle(p_[0], p_[1], p_[2], Reals(h_[0], h_[1], h_[2]), facenames[0], edgenames[0], vertexnames[0]);
    faces_[1] = new Triangle(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[3]), facenames[1], edgenames[1], vertexnames[1]);
    faces_[2] = new Triangle(p_[1], p_[2], p_[3], Reals(h_[1], h_[2], h_[3]), facenames[2], edgenames[2], vertexnames[2]);
    faces_[3] = new Triangle(p_[2], p_[0], p_[3], Reals(h_[2], h_[0], h_[3]), facenames[3], edgenames[3], vertexnames[3]);
  }
  else
  {
    faces_[0] = new Triangle(p_[0], p_[1], p_[2], Numbers(n_[0], n_[1], n_[2]), facenames[0], edgenames[0], vertexnames[0]);
    faces_[1] = new Triangle(p_[0], p_[1], p_[3], Numbers(n_[0], n_[4], n_[3]), facenames[1], edgenames[1], vertexnames[1]);
    faces_[2] = new Triangle(p_[1], p_[2], p_[3], Numbers(n_[1], n_[5], n_[4]), facenames[2], edgenames[2], vertexnames[2]);
    faces_[3] = new Triangle(p_[2], p_[0], p_[3], Numbers(n_[2], n_[3], n_[5]), facenames[3], edgenames[3], vertexnames[3]);
  }
}

real_t Tetrahedron::measure() const
{
  real_t h;
  Point P = projectionOnStraightLine(p_[2], p_[0], p_[1], h);
  real_t area = 0.5 * (p_[0].distance(p_[1])) * h;
  P = projectionOnTriangle(p_[3], p_[0], p_[1], p_[2], h);
  return area * h / 3.;
}

void Tetrahedron::buildParam(const Parameter& p)
{
  trace_p->push("Tetrahedron::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p_[0] = p.get_pt(); break;
        case _integer: p_[0] = Point(real_t(p.get_i())); break;
        case _real: p_[0] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p_[1] = p.get_pt(); break;
        case _integer: p_[1] = Point(real_t(p.get_i())); break;
        case _real: p_[1] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v3:
    {
      switch (p.type())
      {
        case _pt: p_[2] = p.get_pt(); break;
        case _integer: p_[2] = Point(real_t(p.get_i())); break;
        case _real: p_[2] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v4:
    {
      switch (p.type())
      {
        case _pt: p_[3] = p.get_pt(); break;
        case _integer: p_[3] = Point(real_t(p.get_i())); break;
        case _real: p_[3] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integerVector:
        {
          std::vector<number_t> n = p.get_nv();
          n_.resize(n.size());
          for (number_t i = 0; i < n.size(); ++i) { n_[i] = n[i] > 2 ? n[i] : 2; }
          break;
        }
        case _integer: n_ = std::vector<number_t>(1, p.get_n() > 2 ? p.get_n() : 2); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _realVector: h_ = p.get_rv(); break;
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_i())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Polyhedron::buildParam(p); break;
  }
  trace_p->pop();
}

void Tetrahedron::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Tetrahedron::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_ = std::vector<number_t>(6, 2); break;
    default: Polyhedron::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Tetrahedron::getParamsKeys()
{
  std::set<ParameterKey> params = Volume::getParamsKeys();
  params.insert(_pk_faces);
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_v3);
  params.insert(_pk_v4);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

Tetrahedron::Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Tetrahedron::Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Tetrahedron::Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Tetrahedron::Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Tetrahedron::Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Tetrahedron::Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Tetrahedron::Tetrahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

string_t Tetrahedron::asString() const
{
  string_t s("Tetrahedron (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[2].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Tetrahedron::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(6);
  std::vector<const Point*> vertices(2);
  vertices[0] = &p_[0]; vertices[1] = &p_[1];
  curves[0] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[1]; vertices[1] = &p_[2];
  curves[1] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[2]; vertices[1] = &p_[0];
  curves[2] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[0]; vertices[1] = &p_[3];
  curves[3] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[1]; vertices[1] = &p_[3];
  curves[4] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[2]; vertices[1] = &p_[3];
  curves[5] = std::make_pair(_segment, vertices);

  return curves;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Tetrahedron::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(4);
  std::vector<const Point*> vertices(3);
  vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[2];
  surfs[0] = std::make_pair(_triangle, vertices);
  vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[3];
  surfs[1] = std::make_pair(_triangle, vertices);
  vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[3];
  surfs[2] = std::make_pair(_triangle, vertices);
  vertices[0] = &p_[2]; vertices[1] = &p_[0]; vertices[2] = &p_[3];
  surfs[3] = std::make_pair(_triangle, vertices);

  return surfs;
}

//=======================================================================================================
//                                  Hexahedron member functions
//=======================================================================================================
Hexahedron::Hexahedron() : Polyhedron()
{
  p_.resize(8);
  p_[0] = Point(0., 0., 0.);
  p_[1] = Point(1., 0., 0.);
  p_[2] = Point(1., 1., 0.);
  p_[3] = Point(0., 1., 0.);
  p_[4] = Point(0., 0., 1.);
  p_[5] = Point(1., 0., 1.);
  p_[6] = Point(1., 1., 1.);
  p_[7] = Point(0., 1., 1.);
  n_.resize(12, 2);
  shape_ = _hexahedron;
  computeMB();
  setFaces();
}

void Hexahedron::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Hexahedron::build");
  shape_ = _hexahedron;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _faces key is replaced by _v1, _v2, _v3, _v4, _v5, _v6, _v7 and _v8
  params.erase(_pk_faces);

  p_.resize(8);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _hexahedron)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (v1,v2,v3,v4) has to be set (no default values)
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v3) != params.end()) { error("param_missing", "v3"); }
  if (params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v5) != params.end()) { error("param_missing", "v5"); }
  if (params.find(_pk_v6) != params.end()) { error("param_missing", "v6"); }
  if (params.find(_pk_v7) != params.end()) { error("param_missing", "v7"); }
  if (params.find(_pk_v8) != params.end()) { error("param_missing", "v8"); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // checking nnodes and hsteps size
  if (n_.size() != 0)
  {
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
    else if (n_.size() != 12) {error("bad_size", "nnodes", 12, n_.size()); }
  }
  if (h_.size() != 0)
  {
    if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(8, h0); }
    else if (h_.size() != 8) {error("bad_size", "hsteps", 8, h_.size()); }
  }

  boundingBox = BoundingBox(p_[0], p_[1], p_[3], p_[4]);
  computeMB();
  setFaces();
  trace_p->pop();
}

void Hexahedron::buildParam(const Parameter& p)
{
  trace_p->push("Hexahedron::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p_[0] = p.get_pt(); break;
        case _integer: p_[0] = Point(real_t(p.get_i())); break;
        case _real: p_[0] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p_[1] = p.get_pt(); break;
        case _integer: p_[1] = Point(real_t(p.get_i())); break;
        case _real: p_[1] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v3:
    {
      switch (p.type())
      {
        case _pt: p_[2] = p.get_pt(); break;
        case _integer: p_[2] = Point(real_t(p.get_i())); break;
        case _real: p_[2] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v4:
    {
      switch (p.type())
      {
        case _pt: p_[3] = p.get_pt(); break;
        case _integer: p_[3] = Point(real_t(p.get_i())); break;
        case _real: p_[3] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v5:
    {
      switch (p.type())
      {
        case _pt: p_[4] = p.get_pt(); break;
        case _integer: p_[4] = Point(real_t(p.get_i())); break;
        case _real: p_[4] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v6:
    {
      switch (p.type())
      {
        case _pt: p_[5] = p.get_pt(); break;
        case _integer: p_[5] = Point(real_t(p.get_i())); break;
        case _real: p_[5] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v7:
    {
      switch (p.type())
      {
        case _pt: p_[6] = p.get_pt(); break;
        case _integer: p_[6] = Point(real_t(p.get_i())); break;
        case _real: p_[6] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v8:
    {
      switch (p.type())
      {
        case _pt: p_[7] = p.get_pt(); break;
        case _integer: p_[7] = Point(real_t(p.get_i())); break;
        case _real: p_[7] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integerVector:
        {
          std::vector<number_t> n = p.get_nv();
          n_.resize(n.size());
          for (number_t i = 0; i < n.size(); ++i) { n_[i] = n[i] > 2 ? n[i] : 2; }
          break;
        }
        case _integer: n_ = std::vector<number_t>(1, p.get_n() > 2 ? p.get_n() : 2); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _realVector: h_ = p.get_rv(); break;
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_i())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Polyhedron::buildParam(p); break;
  }
  trace_p->pop();
}

void Hexahedron::setFaces()
{
  faces_.resize(6);
  std::vector<string_t> facenames(6);
  if (sideNames_.size() >= 6) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
  else if (sideNames_.size() >= 1) { facenames.resize(6, sideNames_[0]); }
  std::vector<std::vector<string_t>> edgenames(6);
  if (sideOfSideNames_.size() >= 12)
  {
    edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
    edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
    edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
    edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
    edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
    edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
  }
  else if (sideOfSideNames_.size() >= 1)
  {
    edgenames.resize(6, std::vector<string_t>(4,sideOfSideNames_[0]));
  }
  std::vector<std::vector<string_t>> vertexnames(6);
  if (sideOfSideOfSideNames_.size() >= 4)
  {
    vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
    vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
    vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
    vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
    vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
    vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
  }
  else if (sideOfSideOfSideNames_.size() >= 1)
  {
    vertexnames.resize(6, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
  }

  if (h_.size() != 0)
  {
    faces_[0] = new Quadrangle(p_[0], p_[1], p_[2], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
    faces_[1] = new Quadrangle(p_[4], p_[5], p_[6], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
    faces_[2] = new Quadrangle(p_[0], p_[1], p_[5], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
    faces_[3] = new Quadrangle(p_[3], p_[2], p_[6], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
    faces_[4] = new Quadrangle(p_[0], p_[3], p_[7], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
    faces_[5] = new Quadrangle(p_[1], p_[2], p_[6], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
  }
  else
  {
    faces_[0] = new Quadrangle(p_[0], p_[1], p_[2], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
    faces_[1] = new Quadrangle(p_[4], p_[5], p_[6], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
    faces_[2] = new Quadrangle(p_[0], p_[1], p_[5], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
    faces_[3] = new Quadrangle(p_[3], p_[2], p_[6], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
    faces_[4] = new Quadrangle(p_[0], p_[3], p_[7], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
    faces_[5] = new Quadrangle(p_[1], p_[2], p_[6], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
  }
}

void Hexahedron::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Hexahedron::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_ = std::vector<number_t>(12, 2); break;
    default: Polyhedron::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Hexahedron::getParamsKeys()
{
  std::set<ParameterKey> params = Volume::getParamsKeys();
  params.insert(_pk_faces);
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_v3);
  params.insert(_pk_v4);
  params.insert(_pk_v5);
  params.insert(_pk_v6);
  params.insert(_pk_v7);
  params.insert(_pk_v8);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  return params;
}

Hexahedron::Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Hexahedron::Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Hexahedron::Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10)
  : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Hexahedron::Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

Hexahedron::Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

Hexahedron::Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13};
  build(ps);
}

Hexahedron::Hexahedron(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12, Parameter p13, Parameter p14) : Polyhedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14};
  build(ps);
}

string_t Hexahedron::asString() const
{
  string_t s("Hexahedron (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[2].roundToZero().toString() + ", " + p_[3].roundToZero().toString();
  s += p_[4].roundToZero().toString() + ", " + p_[5].roundToZero().toString() + ", " + p_[6].roundToZero().toString() + ", " + p_[7].roundToZero().toString() + ")";
  return s;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Hexahedron::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves(12);
  std::vector<const Point*> vertices(2);
  vertices[0] = &p_[0]; vertices[1] = &p_[1];
  curves[0] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[1]; vertices[1] = &p_[2];
  curves[1] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[2]; vertices[1] = &p_[3];
  curves[2] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[3]; vertices[1] = &p_[0];
  curves[3] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[4]; vertices[1] = &p_[5];
  curves[4] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[5]; vertices[1] = &p_[6];
  curves[5] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[6]; vertices[1] = &p_[7];
  curves[6] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[7]; vertices[1] = &p_[4];
  curves[7] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[0]; vertices[1] = &p_[4];
  curves[8] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[1]; vertices[1] = &p_[5];
  curves[9] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[2]; vertices[1] = &p_[6];
  curves[10] = std::make_pair(_segment, vertices);
  vertices[0] = &p_[3]; vertices[1] = &p_[7];
  curves[11] = std::make_pair(_segment, vertices);

  return curves;
}

std::vector<std::pair<ShapeType, std::vector<const Point*> > > Hexahedron::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs(6);
  std::vector<const Point*> vertices(4);
  vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[2]; vertices[3] = &p_[3];
  surfs[0] = std::make_pair(_quadrangle, vertices);
  vertices[0] = &p_[4]; vertices[1] = &p_[5]; vertices[2] = &p_[6]; vertices[3] = &p_[7];
  surfs[1] = std::make_pair(_quadrangle, vertices);
  vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[5]; vertices[3] = &p_[4];
  surfs[2] = std::make_pair(_quadrangle, vertices);
  vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[7]; vertices[3] = &p_[6];
  surfs[3] = std::make_pair(_quadrangle, vertices);
  vertices[0] = &p_[3]; vertices[1] = &p_[0]; vertices[2] = &p_[4]; vertices[3] = &p_[7];
  surfs[4] = std::make_pair(_quadrangle, vertices);
  vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6]; vertices[3] = &p_[5];
  surfs[5] = std::make_pair(_quadrangle, vertices);

  return surfs;
}

//=======================================================================================================
//                                  Parallepiped member functions
//=======================================================================================================
Parallelepiped::Parallelepiped() : Hexahedron(), nboctants_(8)
{
  v_ = p_;
  shape_ = _parallelepiped;
  computeMB();
}

void Parallelepiped::buildVNHAndBBox()
{
  if (nboctants_ == 8)
  {
    v_ = p_;
    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(12, n0);
        n_[1] = n1;
        n_[3] = n1;
        n_[5] = n1;
        n_[7] = n1;
        for (number_t i = 8; i < 12; ++i) { n_[i] = n2; }
      }
      else if (n_.size() != 12) { error("bad_size", "nnodes", 12, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(8, h0); }
      else if (h_.size() != 8) { error("bad_size", "hsteps", 8, h_.size()); }
    }
    boundingBox = BoundingBox(p_[0], p_[1], p_[3], p_[4]);
  }
  else if (nboctants_ == 7)
  {
    // definition of true vertices
    v_.resize(14);
    // used vertices of the parallelepiped
    v_[0] = p_[0]; v_[1] = p_[2]; v_[2] = p_[3]; v_[3] = p_[4]; v_[4] = p_[5]; v_[5] = p_[6]; v_[6] = p_[7];
    // vertices that are on the middles of edges
    v_[7] = 0.5 * (p_[0] + p_[1]); v_[8] = 0.5 * (p_[1] + p_[2]); v_[9] = 0.5 * (p_[1] + p_[5]);
    // vertices that are on the centers of faces
    v_[10] = 0.5 * (p_[0] + p_[2]); v_[11] = 0.5 * (p_[0] + p_[5]); v_[12] = 0.5 * (p_[1] + p_[6]);
    // center of the volume
    v_[13] = 0.5 * (p_[0] + p_[6]);

    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(21, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(21);
        n_[0] = (n0 - 1) / 2 + 1;
        n_[1] = (n1 - 1) / 2 + 1;
        n_[2] = (n1 - 1) / 2 + 1;
        n_[3] = (n0 - 1) / 2 + 1;
        n_[4] = n0;
        n_[5] = n1;
        n_[6] = n0;
        n_[7] = n1;
        n_[8] = n0;
        n_[9] = n1;
        n_[10] = (n0 - 1) / 2 + 1;
        n_[11] = (n1 - 1) / 2 + 1;
        n_[12] = (n0 - 1) / 2 + 1;
        n_[13] = (n1 - 1) / 2 + 1;
        n_[14] = n2;
        n_[15] = (n2 - 1) / 2 + 1;
        n_[16] = n2;
        n_[17] = n2;
        n_[18] = (n2 - 1) / 2 + 1;
        n_[19] = (n2 - 1) / 2 + 1;
        n_[20] = (n2 - 1) / 2 + 1;

      }
      else if (n_.size() == 12)
      {
        std::vector<number_t> n(21);
        n[0] = (n_[0] - 1) / 2 + 1;
        n[1] = (n_[1] - 1) / 2 + 1;
        n[2] = (n_[3] - 1) / 2 + 1;
        n[3] = (n_[2] - 1) / 2 + 1;
        n[4] = n_[2];
        n[5] = n_[3];
        n[6] = n_[4];
        n[7] = n_[5];
        n[8] = n_[6];
        n[9] = n_[7];
        n[10] = (n_[0] - 1) / 2 + 1;
        n[11] = (n_[1] - 1) / 2 + 1;
        n[12] = (n_[2] - 1) / 2 + 1;
        n[13] = (n_[3] - 1) / 2 + 1;
        n[14] = n_[8];
        n[15] = (n_[9] - 1) / 2 + 1;
        n[16] = n_[10];
        n[17] = n_[11];
        n[18] = (n_[8] - 1) / 2 + 1;
        n[19] = (n_[11] - 1) / 2 + 1;
        n[20] = (n_[10] - 1) / 2 + 1;
        n_.clear();
        n_.resize(21);
        n_ = n;
      }
      else if (n_.size() != 21) { error("bad_size", "nnodes", 21, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(14, h0); }
      else if (h_.size() == 8)
      {
        std::vector<real_t> h(14);
        h[0] = h_[0];
        h[1] = h_[2];
        h[2] = h_[3];
        h[3] = h_[4];
        h[4] = h_[5];
        h[5] = h_[6];
        h[6] = h_[7];
        h[7] = 0.5 * (h_[0] + h_[1]);
        h[8] = 0.5 * (h_[1] + h_[2]);
        h[9] = 0.5 * (h_[1] + h_[5]);
        h[10] = 0.25 * (h_[0] + h_[1] + h_[2] + h_[3]);
        h[11] = 0.25 * (h_[0] + h_[1] + h_[5] + h_[4]);
        h[12] = 0.25 * (h_[1] + h_[2] + h_[6] + h_[5]);
        h[13] = 0.125 * (h_[0] + h_[1] + h_[2] + h_[3] + h_[4] + h_[5] + h_[6] + h_[7]);
        h_.clear();
        h_ = h;
      }
      else if (h_.size() != 14) { error("bad_size", "hsteps", 14, h_.size()); }
    }
    boundingBox = BoundingBox(p_[0], p_[1], p_[3], p_[4]);
  }
  else if (nboctants_ == 6)
  {
    // definition of true vertices
    v_.resize(12);
    // used vertices of the parallelepiped
    v_[0] = p_[2]; v_[1] = p_[3]; v_[2] = p_[4]; v_[3] = p_[5]; v_[4] = p_[6]; v_[5] = p_[7];
    // vertices that are on the middles of edges
    v_[6] = 0.5 * (p_[0] + p_[3]); v_[7] = 0.5 * (p_[0] + p_[4]); v_[8] = 0.5 * (p_[1] + p_[2]); v_[9] = 0.5 * (p_[1] + p_[5]);
    // vertices that are on the centers of faces
    v_[10] = 0.5 * (p_[0] + p_[7]); v_[11] = 0.5 * (p_[1] + p_[6]);

    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(18, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(18);
        n_[0] = n0;
        n_[1] = (n1 - 1) / 2 + 1;
        n_[2] = n0;
        n_[3] = (n1 - 1) / 2 + 1;
        n_[4] = n0;
        n_[5] = n1;
        n_[6] = n0;
        n_[7] = n1;
        n_[8] = n0;
        n_[9] = (n1 - 1) / 2 + 1;
        n_[10] = n0;
        n_[11] = (n1 - 1) / 2 + 1;
        n_[12] = (n2 - 1) / 2 + 1;
        n_[13] = (n2 - 1) / 2 + 1;
        n_[14] = n2;
        n_[15] = n2;
        n_[16] = (n2 - 1) / 2 + 1;
        n_[17] = (n2 - 1) / 2 + 1;
      }
      else if (n_.size() == 12)
      {
        std::vector<number_t> n(18);
        n[0] = n_[0];
        n[1] = (n_[1] - 1) / 2 + 1;
        n[2] = n_[2];
        n[3] = (n_[3] - 1) / 2 + 1;
        n[4] = n_[4];
        n[5] = n_[5];
        n[6] = n_[6];
        n[7] = n_[7];
        n[8] = n_[0];
        n[9] = (n_[1] - 1) / 2 + 1;
        n[10] = n_[2];
        n[11] = (n_[3] - 1) / 2 + 1;
        n[12] = (n_[8] - 1) / 2 + 1;
        n[13] = (n_[9] - 1) / 2 + 1;
        n[14] = n_[10];
        n[15] = n_[11];
        n[16] = (n_[11] - 1) / 2 + 1;
        n[17] = (n_[10] - 1) / 2 + 1;
        n_.clear();
        n_.resize(18);
        n_ = n;
      }
      else if (n_.size() != 18) { error("bad_size", "nnodes", 18, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(12, h0); }
      else if (h_.size() == 8)
      {
        std::vector<real_t> h(12);
        h[0] = h_[2];
        h[1] = h_[3];
        h[2] = h_[4];
        h[3] = h_[5];
        h[4] = h_[6];
        h[5] = h_[7];
        h[6] = 0.5 * (h_[0] + h_[3]);
        h[7] = 0.5 * (h_[0] + h_[4]);
        h[8] = 0.5 * (h_[1] + h_[2]);
        h[9] = 0.5 * (h_[1] + h_[5]);
        h[10] = 0.25 * (h_[0] + h_[3] + h_[7] + h_[4]);
        h[11] = 0.25 * (h_[1] + h_[2] + h_[6] + h_[5]);
        h_.clear();
        h_ = h;
      }
      else if (h_.size() != 12) { error("bad_size", "hsteps", 12, h_.size()); }
    }
    boundingBox = BoundingBox(p_[0], p_[1], p_[3], p_[4]);
  }
  else if (nboctants_ == 5)
  {
    // definition of true vertices
    v_.resize(14);
    // used vertices of the parallelepiped
    v_[0] = p_[2]; v_[1] = p_[4]; v_[2] = p_[5]; v_[3] = p_[6]; v_[4] = p_[7];
    // vertices that are on the middles of edges
    v_[5] = 0.5 * (p_[0] + p_[4]); v_[6] = 0.5 * (p_[1] + p_[2]); v_[7] = 0.5 * (p_[1] + p_[5]); v_[8] = 0.5 * (p_[2] + p_[3]); v_[9] = 0.5 * (p_[3] + p_[7]);
    // vertices that are on the centers of faces
    v_[10] = 0.5 * (p_[0] + p_[2]); v_[11] = 0.5 * (p_[1] + p_[6]); v_[12] = 0.5 * (p_[2] + p_[7]);
    // center of the volume
    v_[13] = 0.5 * (p_[0] + p_[6]);

    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(21, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(21);
        n_[0] = (n0 - 1) / 2 + 1;
        n_[1] = (n1 - 1) / 2 + 1;
        n_[2] = (n0 - 1) / 2 + 1;
        n_[3] = (n1 - 1) / 2 + 1;
        n_[4] = n0;
        n_[5] = n1;
        n_[6] = n0;
        n_[7] = n1;
        n_[8] = n0;
        n_[9] = (n1 - 1) / 2 + 1;
        n_[10] = (n0 - 1) / 2 + 1;
        n_[11] = (n1 - 1) / 2 + 1;
        n_[12] = (n0 - 1) / 2 + 1;
        n_[13] = n1;
        n_[14] = (n2 - 1) / 2 + 1;
        n_[15] = (n2 - 1) / 2 + 1;
        n_[16] = n2;
        n_[17] = (n2 - 1) / 2 + 1;
        n_[18] = (n2 - 1) / 2 + 1;
        n_[19] = (n2 - 1) / 2 + 1;
        n_[20] = (n2 - 1) / 2 + 1;
      }
      else if (n_.size() == 12)
      {
        std::vector<number_t> n(21);
        n[0] = (n_[0] - 1) / 2 + 1;
        n[1] = (n_[1] - 1) / 2 + 1;
        n[2] = (n_[2] - 1) / 2 + 1;
        n[3] = (n_[3] - 1) / 2 + 1;
        n[4] = n_[4];
        n[5] = n_[5];
        n[6] = n_[6];
        n[7] = n_[7];
        n[8] = n_[0];
        n[9] = (n_[1] - 1) / 2 + 1;
        n[10] = (n_[0] - 1) / 2 + 1;
        n[11] = (n_[1] - 1) / 2 + 1;
        n[12] = (n_[2] - 1) / 2 + 1;
        n[13] = n_[3];
        n[14] = (n_[8] - 1) / 2 + 1;
        n[15] = (n_[9] - 1) / 2 + 1;
        n[16] = n_[10];
        n[17] = (n_[11] - 1) / 2 + 1;
        n[18] = (n_[8] - 1) / 2 + 1;
        n[19] = (n_[9] - 1) / 2 + 1;
        n[20] = (n_[11] - 1) / 2 + 1;
        n_.clear();
        n_.resize(21);
        n_ = n;
      }
      else if (n_.size() != 21) { error("bad_size", "nnodes", 21, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(14, h0); }
      else if (h_.size() == 8)
      {
        std::vector<real_t> h(14); // h3, h5, h6, h7, h8, h15, h23, h26, h34, h48, h1234, h2376, h3487, hc
        h[0] = h_[2];
        h[1] = h_[4];
        h[2] = h_[5];
        h[3] = h_[6];
        h[4] = h_[7];
        h[5] = 0.5 * (h_[0] + h_[4]);
        h[6] = 0.5 * (h_[1] + h_[2]);
        h[7] = 0.5 * (h_[1] + h_[5]);
        h[8] = 0.5 * (h_[2] + h_[3]);
        h[9] = 0.5 * (h_[3] + h_[7]);
        h[10] = 0.25 * (h_[0] + h_[1] + h_[2] + h_[3]);
        h[11] = 0.25 * (h_[1] + h_[2] + h_[6] + h_[5]);
        h[12] = 0.25 * (h_[2] + h_[3] + h_[7] + h_[6]);
        h[13] = 0.125 * (h_[0] + h_[1] + h_[2] + h_[3] + h_[4] + h_[5] + h_[6] + h_[7]);
        h_.clear();
        h_ = h;
      }
      else if (h_.size() != 14) { error("bad_size", "hsteps", 14, h_.size()); }
    }
    boundingBox = BoundingBox(p_[0], p_[1], p_[3], p_[4]);
  }
  else if (nboctants_ == 4)
  {
    // definition of true vertices
    v_.resize(8);
    // used vertices of the parallelepiped
    v_[4] = p_[4]; v_[5] = p_[5]; v_[6] = p_[6]; v_[7] = p_[7];
    // vertices that are on the middles of edges
    v_[0] = 0.5 * (p_[0] + p_[4]); v_[1] = 0.5 * (p_[1] + p_[5]); v_[2] = 0.5 * (p_[2] + p_[6]); v_[3] = 0.5 * (p_[3] + p_[7]);

    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(12, n0);
        n_[1] = n1;
        n_[3] = n1;
        n_[5] = n1;
        n_[7] = n1;
        for (number_t i = 8; i < 12; ++i) { n_[i] = n2; }
      }
      else if (n_.size() != 12) { error("bad_size", "nnodes", 12, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(8, h0); }
      else if (h_.size() != 8) { error("bad_size", "hsteps", 8, h_.size()); }
    }
    boundingBox = BoundingBox(v_[4], v_[5], v_[7], v_[0]);
  }
  else if (nboctants_ == 3)
  {
    // definition of true vertices
    v_.resize(12);
    // used vertices of the parallelepiped
    v_[0] = p_[4]; v_[1] = p_[6]; v_[2] = p_[7];
    // vertices that are on the middles of edges
    v_[3] = 0.5 * (p_[0] + p_[4]); v_[4] = 0.5 * (p_[2] + p_[6]); v_[5] = 0.5 * (p_[3] + p_[7]); v_[6] = 0.5 * (p_[4] + p_[5]); v_[7] = 0.5 * (p_[5] + p_[6]);
    // vertices that are on the centers of faces
    v_[8] = 0.5 * (p_[0] + p_[5]); v_[9] = 0.5 * (p_[1] + p_[6]); v_[10] = 0.5 * (p_[4] + p_[6]);
    // center of the volume
    v_[11] = 0.5 * (p_[0] + p_[6]);

    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(18, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(18);
        n_[0] = (n0 - 1) / 2 + 1;
        n_[1] = (n1 - 1) / 2 + 1;
        n_[2] = (n0 - 1) / 2 + 1;
        n_[3] = (n1 - 1) / 2 + 1;
        n_[4] = n0;
        n_[5] = n1;
        n_[6] = (n0 - 1) / 2 + 1;
        n_[7] = (n1 - 1) / 2 + 1;
        n_[8] = (n0 - 1) / 2 + 1;
        n_[9] = (n1 - 1) / 2 + 1;
        n_[10] = n0;
        n_[11] = n1;
        n_[12] = (n2 - 1) / 2 + 1;
        n_[13] = (n2 - 1) / 2 + 1;
        n_[14] = (n2 - 1) / 2 + 1;
        n_[15] = (n2 - 1) / 2 + 1;
        n_[16] = (n2 - 1) / 2 + 1;
        n_[17] = (n2 - 1) / 2 + 1;
      }
      else if (n_.size() == 12)
      {
        std::vector<number_t> n(18);
        n[0] = (n_[0] - 1) / 2 + 1;
        n[1] = (n_[3] - 1) / 2 + 1;
        n[2] = (n_[2] - 1) / 2 + 1;
        n[3] = (n_[1] - 1) / 2 + 1;
        n[4] = n_[2];
        n[5] = n_[3];
        n[6] = (n_[4] - 1) / 2 + 1;
        n[7] = (n_[7] - 1) / 2 + 1;
        n[8] = (n_[6] - 1) / 2 + 1;
        n[9] = (n_[5] - 1) / 2 + 1;
        n[10] = n_[6];
        n[11] = n_[7];
        n[12] = (n_[8] - 1) / 2 + 1;
        n[13] = (n_[8] - 1) / 2 + 1;
        n[14] = (n_[11] - 1) / 2 + 1;
        n[15] = (n_[10] - 1) / 2 + 1;
        n[16] = (n_[10] - 1) / 2 + 1;
        n[17] = (n_[11] - 1) / 2 + 1;
        n_.clear();
        n_.resize(18);
        n_ = n;
      }
      else if (n_.size() != 18) { error("bad_size", "nnodes", 18, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(12, h0); }
      else if (h_.size() == 8)
      {
        std::vector<real_t> h(12);
        h[0] = h_[4];
        h[1] = h_[6];
        h[2] = h_[7];
        h[3] = 0.5 * (h_[0] + h_[4]);
        h[4] = 0.5 * (h_[2] + h_[6]);
        h[5] = 0.5 * (h_[3] + h_[7]);
        h[6] = 0.5 * (h_[4] + h_[5]);
        h[7] = 0.5 * (h_[5] + h_[6]);
        h[8] = 0.25 * (h_[0] + h_[1] + h_[5] + h_[4]);
        h[9] = 0.25 * (h_[1] + h_[2] + h_[6] + h_[5]);
        h[10] = 0.25 * (h_[4] + h_[5] + h_[6] + h_[7]);
        h[11] = 0.125 * (h_[0] + h_[1] + h_[2] + h_[3] + h_[4] + h_[5] + h_[6] + h_[7]);
        h_.clear();
        h_ = h;
      }
      else if (h_.size() != 12) { error("bad_size", "hsteps", 12, h_.size()); }
    }
    boundingBox = BoundingBox(v_[3], 0.5 * (p_[1] + p_[5]), v_[5], v_[0]);
  }
  else if (nboctants_ == 2)
  {
    // definition of true vertices
    v_.resize(8);
    // used vertices of the parallelogram
    v_[6] = p_[6]; v_[7] = p_[7];
    // vertices that are on the middles of edges
    v_[2] = 0.5 * (p_[2] + p_[6]); v_[3] = 0.5 * (p_[3] + p_[7]); v_[4] = 0.5 * (p_[4] + p_[7]); v_[5] = 0.5 * (p_[5] + p_[6]);
    // vertices that are on the centers of tfaces
    v_[0] = 0.5 * (p_[0] + p_[7]); v_[1] = 0.5 * (p_[1] + p_[6]);

    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(12, n0);
        n_[1] = n1;
        n_[3] = n1;
        n_[5] = n1;
        n_[7] = n1;
        for (number_t i = 8; i < 12; ++i) { n_[i] = n2; }
      }
      else if (n_.size() != 12) { error("bad_size", "nnodes", 12, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(8, h0); }
      else if (h_.size() != 8) { error("bad_size", "hsteps", 8, h_.size()); }
    }
    boundingBox = BoundingBox(v_[6], v_[7], v_[3], v_[4]);
  }
  else if (nboctants_ == 1)
  {
    // definition of true vertices
    v_.resize(8);
    // used vertices of the parallelogram
    v_[6] = p_[6];
    // vertices that are on the middles of edges
    v_[2] = 0.5 * (p_[2] + p_[6]); v_[5] = 0.5 * (p_[5] + p_[6]); v_[7] = 0.5 * (p_[6] + p_[7]);
    // vertices that are on the centers of faces
    v_[1] = 0.5 * (p_[1] + p_[6]); v_[3] = 0.5 * (p_[2] + p_[7]); v_[4] = 0.5 * (p_[4] + p_[6]);
    // center of the volume
    v_[0] = 0.5 * (p_[0] + p_[6]);

    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(12, n0);
        n_[1] = n1;
        n_[3] = n1;
        n_[5] = n1;
        n_[7] = n1;
        for (number_t i = 8; i < 12; ++i) { n_[i] = (n2 - 1) / 2 + 1; }
      }
      else if (n_.size() != 12) { error("bad_size", "nnodes", 12, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(8, h0); }
      else if (h_.size() != 8) { error("bad_size", "hsteps", 8, h_.size()); }
    }
    boundingBox = BoundingBox(v_[7], v_[1], v_[5], v_[6]);
  }
  else if (nboctants_ > 8) { error("is_greater", nboctants_, 8); }
  else { error("is_lesser", nboctants_, 1); }
}

void Parallelepiped::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Parallelepiped::build");
  shape_ = _parallelepiped;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _faces key is replaced by _v1, _v2, _v4 and _v5
  params.erase(_pk_faces);
  // _v3, _v6, _v7, _v8 are disabled
  params.erase(_pk_v3);
  params.erase(_pk_v6);
  params.erase(_pk_v7);
  params.erase(_pk_v8);

  p_.resize(8);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _parallelepiped)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (v1,v2,v4,v5) has to be set (no default values)
  if (params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v5) != params.end()) { error("param_missing", "v5"); }

  Point q = p_[1] - p_[0];
  Point q2 = p_[3] - p_[0];
  Point q3 = p_[4] - p_[0];
  real_t a, b, c, ap, bp, cp, as, bs, cs;
  a = dot(q, q); b = dot(q, q2); c = dot(q, q3);
  ap = dot(q, q2); bp = dot(q2, q2); cp = dot(q2, q3);
  as = dot(q, q3); bs = dot(q2, q3); cs = dot(q3, q3);
  if (std::abs(a * bp * cs + ap * bs * c + as * b * cp - c * bp * as - cp * bs * a - cs * b * ap) < theTolerance)
  { error("geometry_incoherent_points", words("shape", _parallelepiped)); }
  // p_ is not fully built
  p_[2] = p_[1] + p_[3] - p_[0];
  p_[5] = p_[1] + p_[4] - p_[0];
  p_[6] = p_[2] + p_[4] - p_[0];
  p_[7] = p_[3] + p_[4] - p_[0];

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  buildVNHAndBBox();
  computeMB();
  setFaces();
  trace_p->pop();
}

void Parallelepiped::setFaces()
{
  // first, we have to determine the number of faces, edges and vertices
  number_t nbFaces = 6, nbEdges=12, nbVertices=8;
  if (nboctants_ == 8) { nbFaces = 6, nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ == 7) { nbFaces = 9; nbEdges = 21, nbVertices = 14; }
  else if (nboctants_ == 6) { nbFaces = 8; nbEdges = 18, nbVertices = 12; }
  else if (nboctants_ == 5) { nbFaces = 9; nbEdges = 21, nbVertices = 14; }
  else if (nboctants_ == 4) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ == 3) { nbFaces = 8; nbEdges = 18, nbVertices = 12; }
  else if (nboctants_ == 2) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ == 1) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ > 8) { error("is_greater", nboctants_, 8); }
  else { error("is_lesser", nboctants_, 1); }

  faces_.resize(nbFaces);

  if (nboctants_==8)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else if (nboctants_==7)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7], sideNames_[8]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3], sideOfSideNames_[4], sideOfSideNames_[5]};
      edgenames[1] = {sideOfSideNames_[6], sideOfSideNames_[7], sideOfSideNames_[8], sideOfSideNames_[9]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[18], sideOfSideNames_[10], sideOfSideNames_[15], sideOfSideNames_[6], sideOfSideNames_[14]};
      edgenames[3] = {sideOfSideNames_[4], sideOfSideNames_[16], sideOfSideNames_[8], sideOfSideNames_[9]};
      edgenames[4] = {sideOfSideNames_[5], sideOfSideNames_[17], sideOfSideNames_[9], sideOfSideNames_[14]};
      edgenames[5] = {sideOfSideNames_[16], sideOfSideNames_[7], sideOfSideNames_[15], sideOfSideNames_[11], sideOfSideNames_[20], sideOfSideNames_[3]};
      edgenames[6] = {sideOfSideNames_[10], sideOfSideNames_[11], sideOfSideNames_[12], sideOfSideNames_[13]};
      edgenames[7] = {sideOfSideNames_[2], sideOfSideNames_[20], sideOfSideNames_[12], sideOfSideNames_[19]};
      edgenames[8] = {sideOfSideNames_[1], sideOfSideNames_[19], sideOfSideNames_[13], sideOfSideNames_[18]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(6, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(6, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
      edgenames[8].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2]};
      vertexnames[1] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[3]};
      vertexnames[3] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[3]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[8]};
      vertexnames[6] = {sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
      vertexnames[7] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
      vertexnames[8] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[13], sideOfSideOfSideNames_[11]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[8].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Polygon(Points(v_[0], v_[7], v_[10], v_[8], v_[1], v_[2]), Reals(h_[0], h_[7], h_[10], h_[8], h_[1], h_[2]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(v_[3], v_[4], v_[6], Reals(h_[3], h_[4], h_[5], h_[6]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Polygon(Points(v_[0], v_[7], v_[11], v_[9], v_[4], v_[3]), Reals(h_[0], h_[7], h_[11], h_[9], h_[4], h_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(v_[2], v_[1], v_[6], Reals(h_[2], h_[1], h_[5], h_[6]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(v_[0], v_[2], v_[3], Reals(h_[0], h_[2], h_[6], h_[3]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[1], v_[5], v_[4], v_[9], v_[12], v_[8]), Reals(h_[1], h_[5], h_[4], h_[9], h_[11], h_[8]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Parallelogram(v_[11], v_[9], v_[13], Reals(h_[11], h_[9], h_[12], h_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[10], v_[8], v_[13], Reals(h_[10], h_[8], h_[12], h_[13]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Parallelogram(v_[7], v_[10], v_[11], Reals(h_[7], h_[10], h_[13], h_[11]), facenames[8], edgenames[8], vertexnames[8]);
    }
    else
    {
      faces_[0] = new Polygon(Points(v_[0], v_[7], v_[10], v_[8], v_[1], v_[2]), Numbers(n_[0], n_[1], n_[2], n_[3], n_[4], n_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(v_[3], v_[4], v_[6], Numbers(n_[6], n_[7], n_[8], n_[9]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Polygon(Points(v_[0], v_[7], v_[11], v_[9], v_[4], v_[3]), Numbers(n_[0], n_[18], n_[10], n_[15], n_[6], n_[14]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(v_[2], v_[1], v_[6], Numbers(n_[4], n_[16], n_[8], n_[17]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(v_[0], v_[2], v_[3], Numbers(n_[5], n_[17], n_[9], n_[14]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[1], v_[5], v_[4], v_[9], v_[12], v_[8]), Numbers(n_[6], n_[7], n_[15], n_[11], n_[20], n_[3]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Parallelogram(v_[11], v_[9], v_[13], Numbers(n_[10], n_[11], n_[12], n_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[10], v_[8], v_[13], Numbers(n_[2], n_[20], n_[12], n_[19]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Parallelogram(v_[7], v_[10], v_[11], Numbers(n_[1], n_[19], n_[13], n_[18]), facenames[8], edgenames[8], vertexnames[8]);
    }
  }
  else if (nboctants_==6)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[8], sideOfSideNames_[13], sideOfSideNames_[4], sideOfSideNames_[12]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[14], sideOfSideNames_[6], sideOfSideNames_[15]};
      edgenames[4] = {sideOfSideNames_[12], sideOfSideNames_[11], sideOfSideNames_[16], sideOfSideNames_[3], sideOfSideNames_[15], sideOfSideNames_[17]};
      edgenames[5] = {sideOfSideNames_[13], sideOfSideNames_[9], sideOfSideNames_[17], sideOfSideNames_[1], sideOfSideNames_[14], sideOfSideNames_[5]};
      edgenames[6] = {sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11]};
      edgenames[7] = {sideOfSideNames_[0], sideOfSideNames_[17], sideOfSideNames_[10], sideOfSideNames_[11]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(4, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(6, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1]};
      vertexnames[1] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[2] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2]};
      vertexnames[3] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[4] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5]};
      vertexnames[5] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[4]};
      vertexnames[6] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10]};
      vertexnames[7] = {sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Parallelogram(v_[6], v_[8], v_[1], Reals(h_[6], h_[8], h_[0], h_[1]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(v_[2], v_[3], v_[5], Reals(h_[2], h_[3], h_[4], h_[5]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(v_[7], v_[9], v_[2], Reals(h_[7], h_[9], h_[3], h_[2]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(v_[1], v_[0], v_[5], Reals(h_[1], h_[0], h_[4], h_[5]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Polygon(Points(v_[2], v_[7], v_[10], v_[6], v_[1], v_[5]), Reals(h_[2], h_[7], h_[10], h_[6], h_[1], h_[5]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[3], v_[9], v_[11], v_[8], v_[0], v_[4]), Reals(h_[3], h_[9], h_[11], h_[8], h_[0], h_[4]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Parallelogram(v_[7], v_[9], v_[10], Reals(h_[7], h_[9], h_[11], h_[10]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[6], v_[8], v_[10], Reals(h_[6], h_[8], h_[11], h_[10]), facenames[7], edgenames[7], vertexnames[7]);
    }
    else
    {
      faces_[0] = new Parallelogram(v_[6], v_[8], v_[1], Numbers(n_[6], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(v_[2], v_[3], v_[5], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(v_[7], v_[9], v_[2], Numbers(n_[8], n_[13], n_[4], n_[12]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(v_[1], v_[0], v_[5], Numbers(n_[2], n_[14], n_[6], n_[15]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Polygon(Points(v_[2], v_[7], v_[10], v_[6], v_[1], v_[5]), Numbers(n_[12], n_[11], n_[16], n_[3], n_[15], n_[7]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[3], v_[9], v_[11], v_[8], v_[0], v_[4]), Numbers(n_[13], n_[9], n_[17], n_[1], n_[14], n_[5]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Parallelogram(v_[7], v_[9], v_[10], Numbers(n_[8], n_[9], n_[10], n_[11]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[6], v_[8], v_[10], Numbers(n_[0], n_[17], n_[10], n_[11]), facenames[7], edgenames[7], vertexnames[7]);
    }
  }
  else if (nboctants_==5)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7], sideNames_[8]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[8], sideOfSideNames_[15], sideOfSideNames_[4], sideOfSideNames_[14]};
      edgenames[3] = {sideOfSideNames_[16], sideOfSideNames_[6], sideOfSideNames_[17], sideOfSideNames_[12], sideOfSideNames_[20], sideOfSideNames_[2]};
      edgenames[4] = {sideOfSideNames_[13], sideOfSideNames_[17], sideOfSideNames_[7], sideOfSideNames_[14]};
      edgenames[5] = {sideOfSideNames_[15], sideOfSideNames_[9], sideOfSideNames_[19], sideOfSideNames_[1], sideOfSideNames_[16], sideOfSideNames_[5]};
      edgenames[6] = {sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11], sideOfSideNames_[12], sideOfSideNames_[13]};
      edgenames[7] = {sideOfSideNames_[0], sideOfSideNames_[19], sideOfSideNames_[10], sideOfSideNames_[18]};
      edgenames[8] = {sideOfSideNames_[31], sideOfSideNames_[20], sideOfSideNames_[11], sideOfSideNames_[18]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(4, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(6, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(6, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
      edgenames[8].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[8]};
      vertexnames[1] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4]};
      vertexnames[2] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1]};
      vertexnames[3] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[8]};
      vertexnames[4] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[1]};
      vertexnames[5] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3]};
      vertexnames[6] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[13], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[9]};
      vertexnames[7] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[13]};
      vertexnames[8] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[8].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Parallelogram(v_[10], v_[6], v_[8], Reals(h_[10], h_[6], h_[0], h_[8]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(v_[1], v_[2], v_[4], Reals(h_[1], h_[2], h_[3], h_[4]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(v_[5], v_[7], v_[1], Reals(h_[5], h_[7], h_[2], h_[1]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Polygon(Points(v_[0], v_[3], v_[4], v_[9], v_[12], v_[8]), Reals(h_[0], h_[3], h_[4], h_[9], h_[12], h_[8]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(v_[5], v_[9], v_[1], Reals(h_[5], h_[9], h_[4], h_[1]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[2], v_[7], v_[11], v_[6], v_[0], v_[3]), Reals(h_[2], h_[7], h_[11], h_[6], h_[0], h_[3]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Polygon(Points(v_[5], v_[7], v_[11], v_[13], v_[12], v_[9]), Reals(h_[5], h_[7], h_[11], h_[13], h_[12], h_[9]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[10], v_[6], v_[13], Reals(h_[10], h_[6], h_[11], h_[13]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Parallelogram(v_[10], v_[8], v_[13], Reals(h_[10], h_[8], h_[12], h_[13]), facenames[8], edgenames[8], vertexnames[8]);
    }
    else
    {
      faces_[0] = new Parallelogram(v_[10], v_[6], v_[8], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(v_[1], v_[2], v_[4], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(v_[5], v_[7], v_[1], Numbers(n_[8], n_[15], n_[4], n_[14]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Polygon(Points(v_[0], v_[3], v_[4], v_[9], v_[12], v_[8]), Numbers(n_[16], n_[6], n_[17], n_[12], n_[20], n_[2]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(v_[5], v_[9], v_[1], Numbers(n_[13], n_[17], n_[7], n_[14]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[2], v_[7], v_[11], v_[6], v_[0], v_[3]), Numbers(n_[15], n_[9], n_[19], n_[1], n_[16], n_[5]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Polygon(Points(v_[5], v_[7], v_[11], v_[13], v_[12], v_[9]), Numbers(n_[8], n_[9], n_[10], n_[11], n_[12], n_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[10], v_[6], v_[13], Numbers(n_[0], n_[19], n_[10], n_[18]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Parallelogram(v_[10], v_[8], v_[13], Numbers(n_[3], n_[20], n_[11], n_[18]), facenames[8], edgenames[8], vertexnames[8]);
    }
  }
  else if (nboctants_==4)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else if (nboctants_==3)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3], sideOfSideNames_[4], sideOfSideNames_[5]};
      edgenames[1] = {sideOfSideNames_[6], sideOfSideNames_[7], sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[13], sideOfSideNames_[6], sideOfSideNames_[12]};
      edgenames[3] = {sideOfSideNames_[4], sideOfSideNames_[17], sideOfSideNames_[10], sideOfSideNames_[16]};
      edgenames[4] = {sideOfSideNames_[5], sideOfSideNames_[17], sideOfSideNames_[11], sideOfSideNames_[12]};
      edgenames[5] = {sideOfSideNames_[3], sideOfSideNames_[16], sideOfSideNames_[9], sideOfSideNames_[15]};
      edgenames[6] = {sideOfSideNames_[2], sideOfSideNames_[15], sideOfSideNames_[8], sideOfSideNames_[14]};
      edgenames[7] = {sideOfSideNames_[1], sideOfSideNames_[14], sideOfSideNames_[7], sideOfSideNames_[13]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(6, sideNames_[0]);
      edgenames[1].resize(6, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(4, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[1] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2]};
      vertexnames[2] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0]};
      vertexnames[3] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1]};
      vertexnames[4] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[0]};
      vertexnames[5] = {sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[7]};
      vertexnames[6] = {sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10]};
      vertexnames[7] = {sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Polygon(Points(v_[3], v_[8], v_[11], v_[9], v_[4], v_[5]), Reals(h_[3], h_[8], h_[11], h_[9], h_[4], h_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Polygon(Points(v_[0], v_[6], v_[10], v_[7], v_[1], v_[2]), Reals(h_[0], h_[6], h_[10], h_[7], h_[1], h_[2]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(v_[3], v_[8], v_[0], Reals(h_[3], h_[8], h_[6], h_[0]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(v_[4], v_[5], v_[1], Reals(h_[4], h_[5], h_[2], h_[1]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(v_[3], v_[5], v_[0], Reals(h_[3], h_[5], h_[2], h_[0]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(v_[9], v_[4], v_[7], Reals(h_[9], h_[4], h_[1], h_[7]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Parallelogram(v_[11], v_[9], v_[10], Reals(h_[11], h_[9], h_[7], h_[10]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[8], v_[11], v_[6], Reals(h_[8], h_[11], h_[10], h_[6]), facenames[7], edgenames[7], vertexnames[7]);
    }
    else
    {
      faces_[0] = new Polygon(Points(v_[3], v_[8], v_[11], v_[9], v_[4], v_[5]), Numbers(n_[0], n_[1], n_[2], n_[3], n_[4], n_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Polygon(Points(v_[0], v_[6], v_[10], v_[7], v_[1], v_[2]), Numbers(n_[6], n_[7], n_[8], n_[9], n_[10], n_[11]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(v_[3], v_[8], v_[0], Numbers(n_[0], n_[13], n_[6], n_[12]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(v_[4], v_[5], v_[1], Numbers(n_[4], n_[17], n_[10], n_[16]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(v_[3], v_[5], v_[0], Numbers(n_[5], n_[17], n_[11], n_[12]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(v_[9], v_[4], v_[7], Numbers(n_[3], n_[16], n_[9], n_[15]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Parallelogram(v_[11], v_[9], v_[10], Numbers(n_[2], n_[15], n_[8], n_[14]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Parallelogram(v_[8], v_[11], v_[6], Numbers(n_[1], n_[14], n_[7], n_[13]), facenames[7], edgenames[7], vertexnames[7]);
    }
  }
  else if (nboctants_==2)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else // nboctants_==1 (already checked here)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Parallelogram(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Parallelogram(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Parallelogram(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Parallelogram(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Parallelogram(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Parallelogram(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
}

void Parallelepiped::buildParam(const Parameter& p)
{
  trace_p->push("Cube::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_nboctants:
    {
      switch (p.type())
      {
        case _integer: nboctants_ = p.get_n(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Hexahedron::buildParam(p); break;
  }
  trace_p->pop();
}

void Parallelepiped::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Parallelepiped::buildParam");
  switch (key)
  {
    case _pk_nboctants: nboctants_ = 8; break;
    default: Hexahedron::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Parallelepiped::getParamsKeys()
{
  std::set<ParameterKey> params = Hexahedron::getParamsKeys();
  params.insert(_pk_nboctants);
  // _v3, _v6, _v7, _v8 are disabled
  params.erase(_pk_v3);
  params.erase(_pk_v6);
  params.erase(_pk_v7);
  params.erase(_pk_v8);
  return params;
}

Parallelepiped::Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Hexahedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Parallelepiped::Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Hexahedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Parallelepiped::Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Hexahedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Parallelepiped::Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Hexahedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Parallelepiped::Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Hexahedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Parallelepiped::Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Hexahedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Parallelepiped::Parallelepiped(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Hexahedron()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Parallelepiped::Parallelepiped(const Parallelepiped& p) : Hexahedron(p), nboctants_(p.nboctants_), v_(p.v_)
{}

string_t Parallelepiped::asString() const
{
  string_t s("Parallelepiped (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ", " + p_[4].roundToZero().toString() + ", " + tostring(nboctants_) + " octants)";
  return s;
}

std::vector<const Point*> Parallelepiped::boundNodes() const
{
  std::vector<const Point*> nodes(v_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  return nodes;
}

std::vector<const Point*> Parallelepiped::wholeNodes() const
{
  std::vector<const Point*> nodes(v_.size() + p_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  for (number_t i = v_.size(); i < nodes.size(); ++i) { nodes[i] = &p_[i - v_.size()]; }
  return nodes;
}

std::vector<Point*> Parallelepiped::wholeNodes()
{
  std::vector<Point*> nodes(v_.size() + p_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  for (number_t i = v_.size(); i < nodes.size(); ++i) { nodes[i] = &p_[i - v_.size()]; }
  return nodes;
}

real_t Parallelepiped::measure() const
{
  real_t h;
  Point P = projectionOnStraightLine(p_[2], p_[0], p_[1], h);
  real_t area = (p_[0].distance(p_[1])) * h;
  P = projectionOnTriangle(p_[4], p_[0], p_[1], p_[2], h);
  if (nboctants_ == 8) { return area * h; }
  else { return area * h * real_t(nboctants_) / 8.; }
}

void Parallelepiped::computeMB()
{
  if (nboctants_ < 3) { minimalBox = MinimalBox(v_[0], v_[1], v_[3], v_[4]); }
  else if (nboctants_ < 5) { minimalBox = MinimalBox(0.5 * (p_[0] + p_[4]), 0.5 * (p_[1] + p_[5]), 0.5 * (p_[3] + p_[7]), p_[4]); }
  else { minimalBox = MinimalBox(p_[0], p_[1], p_[3], p_[4]); }
}

//=======================================================================================================
//                                  Cuboid member functions
//=======================================================================================================
Cuboid::Cuboid() : Parallelepiped(), center_(Point(0.5, 0.5, 0.5)), origin_(Point(0., 0., 0.)), isCenter_(false), isOrigin_(false),
  xlength_(1.), ylength_(1.), zlength_(1.), xmin_(0.), xmax_(1.), ymin_(0.), ymax_(1.), zmin_(0.), zmax_(1.),
  isBounds_(false)
{
  shape_ = _cuboid;
  computeMB();
}

void Cuboid::buildP()
{
  if (isBounds_)
  {
    p_[0] = Point(xmin_, ymin_, zmin_);
    p_[1] = Point(xmax_, ymin_, zmin_);
    p_[2] = Point(xmax_, ymax_, zmin_);
    p_[3] = Point(xmin_, ymax_, zmin_);
    p_[4] = Point(xmin_, ymin_, zmax_);
    p_[5] = Point(xmax_, ymin_, zmax_);
    p_[6] = Point(xmax_, ymax_, zmax_);
    p_[7] = Point(xmin_, ymax_, zmax_);
    origin_ = p_[0];
    center_ = (p_[0] + p_[6]) / 2.;
    xlength_ = xmax_ - xmin_;
    ylength_ = ymax_ - ymin_;
    zlength_ = zmax_ - zmin_;
  }
  else if (isCenter_)
  {
    Point shift0(-xlength_ / 2., -ylength_ / 2., -zlength_ / 2.);
    p_[0] = center_ + shift0;
    Point shift1(xlength_ / 2., -ylength_ / 2., -zlength_ / 2.);
    p_[1] = center_ + shift1;
    Point shift2(xlength_ / 2., ylength_ / 2., -zlength_ / 2.);
    p_[2] = center_ + shift2;
    Point shift3(-xlength_ / 2., ylength_ / 2., -zlength_ / 2.);
    p_[3] = center_ + shift3;
    Point shift4(-xlength_ / 2., -ylength_ / 2., zlength_ / 2.);
    p_[4] = center_ + shift4;
    Point shift5(xlength_ / 2., -ylength_ / 2., zlength_ / 2.);
    p_[5] = center_ + shift5;
    Point shift6(xlength_ / 2., ylength_ / 2., zlength_ / 2.);
    p_[6] = center_ + shift6;
    Point shift7(-xlength_ / 2., ylength_ / 2., zlength_ / 2.);
    p_[7] = center_ + shift7;
    origin_ = p_[0];
    xmin_ = xmax_ = ymin_ = ymax_ = zmin_ = zmax_ = 0.;
  }
  else if (isOrigin_)
  {
    p_[0] = origin_;
    Point shift1(xlength_, 0., 0.);
    p_[1] = origin_ + shift1;
    Point shift2(xlength_, ylength_, 0.);
    p_[2] = origin_ + shift2;
    Point shift3(0., ylength_, 0.);
    p_[3] = origin_ + shift3;
    Point shift4(0., 0., zlength_);
    p_[4] = origin_ + shift4;
    Point shift5(xlength_, 0., zlength_);
    p_[5] = origin_ + shift5;
    Point shift6(xlength_, ylength_, zlength_);
    p_[6] = origin_ + shift6;
    Point shift7(0., ylength_, zlength_);
    p_[7] = origin_ + shift7;
    center_ = (p_[0] + p_[6]) / 2.;
    xmin_ = xmax_ = ymin_ = ymax_ = zmin_ = zmax_ = 0.;
  }
  else // vertices keys are used so p_[2] is not defined
  {
    p_[2] = p_[1] + p_[3] - p_[0];
    p_[5] = p_[1] + p_[4] - p_[0];
    p_[6] = p_[2] + p_[4] - p_[0];
    p_[7] = p_[3] + p_[4] - p_[0];
    origin_ = p_[0];
    center_ = (p_[0] + p_[6]) / 2.;
    xlength_ = p_[0].distance(p_[1]);
    ylength_ = p_[0].distance(p_[3]);
    zlength_ = p_[0].distance(p_[4]);
    xmin_ = xmax_ = ymin_ = ymax_ = zmin_ = zmax_ = 0.;
    if (dot(p_[3] - p_[0], p_[1] - p_[0]) > theTolerance ||
        dot(p_[4] - p_[0], p_[1] - p_[0]) > theTolerance ||
        dot(p_[4] - p_[0], p_[3] - p_[0]) > theTolerance) { error("geometry_incoherent_points", words("shape", _cuboid)); }
  }
}

void Cuboid::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Cuboid::build");
  shape_ = _cuboid;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _faces key is replaced by (_v1,_v2,_v4,_v5), (_center|_origin, _xlength, _ylength, _zlength)
  // or (_xmin, _xmax, _ymin, _ymax, _zmin, _zmax)
  params.erase(_pk_faces);
  
  p_.resize(8);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _cuboid)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);

    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (v1,v2,v4,v5), (xmin,xmax,ymin,ymax,zmin,zmax),
    // (center,xlength,ylength,zlength), (origin,xlength,ylength,zlength)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_xmin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_xmax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmax)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_ymin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_ymax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymax)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_zmin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zmin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_zmax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zmax)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_xlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xlength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_ylength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ylength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_zlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zlength)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_v4) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v4)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_v5) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v5)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_xlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xlength)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_ylength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ylength)); }
    if ((key == _pk_xmin || key == _pk_xmax || key == _pk_ymin || key == _pk_ymax || key == _pk_zmin || key == _pk_zmax) &&
        usedParams.find(_pk_zlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zlength)); }
    if (key == _pk_center && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if (key == _pk_origin && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_v4) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v4)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_v5) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v5)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_xmin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmin)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_xmax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xmax)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_ymin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymin)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_ymax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ymax)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_zmin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zmin)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength) &&
        usedParams.find(_pk_zmax) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zmax)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (v1,v2,v4) or (xmin,xmax,ymin,ymax) or (center,xlength,ylength) has to be set (no default values)
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v5) != params.end()) { error("param_missing", "v5"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v5) != params.end()) { error("param_missing", "v5"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v5) != params.end()) { error("param_missing", "v5"); }
  if (params.find(_pk_v5) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v5) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v5) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_zmin) != params.end()) { error("param_missing", "zmin"); }
  if (params.find(_pk_xmin) == params.end() && params.find(_pk_zmax) != params.end()) { error("param_missing", "zmax"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_zmin) != params.end()) { error("param_missing", "zmin"); }
  if (params.find(_pk_xmax) == params.end() && params.find(_pk_zmax) != params.end()) { error("param_missing", "zmax"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_zmin) != params.end()) { error("param_missing", "zmin"); }
  if (params.find(_pk_ymin) == params.end() && params.find(_pk_zmax) != params.end()) { error("param_missing", "zmax"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_zmin) != params.end()) { error("param_missing", "zmin"); }
  if (params.find(_pk_ymax) == params.end() && params.find(_pk_zmax) != params.end()) { error("param_missing", "zmax"); }
  if (params.find(_pk_zmin) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_zmin) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_zmin) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if (params.find(_pk_zmin) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_zmin) == params.end() && params.find(_pk_zmax) != params.end()) { error("param_missing", "zmax"); }
  if (params.find(_pk_zmax) == params.end() && params.find(_pk_xmin) != params.end()) { error("param_missing", "xmin"); }
  if (params.find(_pk_zmax) == params.end() && params.find(_pk_xmax) != params.end()) { error("param_missing", "xmax"); }
  if (params.find(_pk_zmax) == params.end() && params.find(_pk_ymin) != params.end()) { error("param_missing", "ymin"); }
  if (params.find(_pk_zmax) == params.end() && params.find(_pk_ymax) != params.end()) { error("param_missing", "ymax"); }
  if (params.find(_pk_zmax) == params.end() && params.find(_pk_zmin) != params.end()) { error("param_missing", "zmin"); }
  if ((params.find(_pk_center) == params.end() || params.find(_pk_origin) == params.end()) && params.find(_pk_xlength) != params.end())
  { error("param_missing", "xlength"); }
  if ((params.find(_pk_center) == params.end() || params.find(_pk_origin) == params.end()) && params.find(_pk_ylength) != params.end())
  { error("param_missing", "ylength"); }
  if ((params.find(_pk_center) == params.end() || params.find(_pk_origin) == params.end()) && params.find(_pk_zlength) != params.end())
  { error("param_missing", "zlength"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_ylength) != params.end()) { error("param_missing", "ylength"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_zlength) != params.end()) { error("param_missing", "zlength"); }
  if (params.find(_pk_ylength) == params.end() && params.find(_pk_xlength) != params.end()) { error("param_missing", "xlength"); }
  if (params.find(_pk_ylength) == params.end() && params.find(_pk_zlength) != params.end()) { error("param_missing", "zlength"); }
  if (params.find(_pk_zlength) == params.end() && params.find(_pk_xlength) != params.end()) { error("param_missing", "xlength"); }
  if (params.find(_pk_zlength) == params.end() && params.find(_pk_ylength) != params.end()) { error("param_missing", "ylength"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_center) != params.end() && params.find(_pk_origin) != params.end())
  { error("param_missing", "center or origin"); }
  isCenter_ = true;
  isOrigin_ = true;
  isBounds_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_xlength) != params.end())
  { params.erase(_pk_xlength); params.erase(_pk_ylength);  params.erase(_pk_zlength); }
  if (params.find(_pk_center) != params.end()) { params.erase(_pk_center); isCenter_ = false; }
  if (params.find(_pk_origin) != params.end()) { params.erase(_pk_origin); isOrigin_ = false; }
  if (params.find(_pk_xmin) != params.end())
  {
    params.erase(_pk_xmin); params.erase(_pk_xmax);
    params.erase(_pk_ymin); params.erase(_pk_ymax);
    params.erase(_pk_zmin); params.erase(_pk_zmax);
    isBounds_ = false;
  }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_v4); params.erase(_pk_v5); }

  // p_ is not (fully) built so we do
  buildP();

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  buildVNHAndBBox();
  computeMB();
  setFaces();
  trace_p->pop();
}

void Cuboid::setFaces()
{
  // first, we have to determine the number of faces, edges and vertices
  number_t nbFaces = 6, nbEdges=12, nbVertices=8;
  if (nboctants_ == 8) { nbFaces = 6, nbEdges=12, nbVertices=8; }
  else if (nboctants_ == 7) { nbFaces = 9; nbEdges = 21, nbVertices = 14; }
  else if (nboctants_ == 6) { nbFaces = 8; nbEdges = 18, nbVertices = 12; }
  else if (nboctants_ == 5) { nbFaces = 9; nbEdges = 21, nbVertices = 14; }
  else if (nboctants_ == 4) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ == 3) { nbFaces = 8; nbEdges = 18, nbVertices = 12; }
  else if (nboctants_ == 2) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ == 1) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ > 8) { error("is_greater", nboctants_, 8); }
  else { error("is_lesser", nboctants_, 1); }

  faces_.resize(nbFaces);

  if (nboctants_==8)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else if (nboctants_==7)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7], sideNames_[8]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3], sideOfSideNames_[4], sideOfSideNames_[5]};
      edgenames[1] = {sideOfSideNames_[6], sideOfSideNames_[7], sideOfSideNames_[8], sideOfSideNames_[9]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[18], sideOfSideNames_[10], sideOfSideNames_[15], sideOfSideNames_[6], sideOfSideNames_[14]};
      edgenames[3] = {sideOfSideNames_[4], sideOfSideNames_[16], sideOfSideNames_[8], sideOfSideNames_[9]};
      edgenames[4] = {sideOfSideNames_[5], sideOfSideNames_[17], sideOfSideNames_[9], sideOfSideNames_[14]};
      edgenames[5] = {sideOfSideNames_[16], sideOfSideNames_[7], sideOfSideNames_[15], sideOfSideNames_[11], sideOfSideNames_[20], sideOfSideNames_[3]};
      edgenames[6] = {sideOfSideNames_[10], sideOfSideNames_[11], sideOfSideNames_[12], sideOfSideNames_[13]};
      edgenames[7] = {sideOfSideNames_[2], sideOfSideNames_[20], sideOfSideNames_[12], sideOfSideNames_[19]};
      edgenames[8] = {sideOfSideNames_[1], sideOfSideNames_[19], sideOfSideNames_[13], sideOfSideNames_[18]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(6, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(6, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
      edgenames[8].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2]};
      vertexnames[1] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[3]};
      vertexnames[3] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[3]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[8]};
      vertexnames[6] = {sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
      vertexnames[7] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
      vertexnames[8] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[13], sideOfSideOfSideNames_[11]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[8].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Polygon(Points(v_[0], v_[7], v_[10], v_[8], v_[1], v_[2]), Reals(h_[0], h_[7], h_[10], h_[8], h_[1], h_[2]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(v_[3], v_[4], v_[6], Reals(h_[3], h_[4], h_[5], h_[6]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Polygon(Points(v_[0], v_[7], v_[11], v_[9], v_[4], v_[3]), Reals(h_[0], h_[7], h_[11], h_[9], h_[4], h_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(v_[2], v_[1], v_[6], Reals(h_[2], h_[1], h_[5], h_[6]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(v_[0], v_[2], v_[3], Reals(h_[0], h_[2], h_[6], h_[3]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[1], v_[5], v_[4], v_[9], v_[12], v_[8]), Reals(h_[1], h_[5], h_[4], h_[9], h_[11], h_[8]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Rectangle(v_[11], v_[9], v_[13], Reals(h_[11], h_[9], h_[12], h_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[10], v_[8], v_[13], Reals(h_[10], h_[8], h_[12], h_[13]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Rectangle(v_[7], v_[10], v_[11], Reals(h_[7], h_[10], h_[13], h_[11]), facenames[8], edgenames[8], vertexnames[8]);
    }
    else
    {
      faces_[0] = new Polygon(Points(v_[0], v_[7], v_[10], v_[8], v_[1], v_[2]), Numbers(n_[0], n_[1], n_[2], n_[3], n_[4], n_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(v_[3], v_[4], v_[6], Numbers(n_[6], n_[7], n_[8], n_[9]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Polygon(Points(v_[0], v_[7], v_[11], v_[9], v_[4], v_[3]), Numbers(n_[0], n_[18], n_[10], n_[15], n_[6], n_[14]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(v_[2], v_[1], v_[6], Numbers(n_[4], n_[16], n_[8], n_[17]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(v_[0], v_[2], v_[3], Numbers(n_[5], n_[17], n_[9], n_[14]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[1], v_[5], v_[4], v_[9], v_[12], v_[8]), Numbers(n_[6], n_[7], n_[15], n_[11], n_[20], n_[3]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Rectangle(v_[11], v_[9], v_[13], Numbers(n_[10], n_[11], n_[12], n_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[10], v_[8], v_[13], Numbers(n_[2], n_[20], n_[12], n_[19]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Rectangle(v_[7], v_[10], v_[11], Numbers(n_[1], n_[19], n_[13], n_[18]), facenames[8], edgenames[8], vertexnames[8]);
    }
  }
  else if (nboctants_==6)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[8], sideOfSideNames_[13], sideOfSideNames_[4], sideOfSideNames_[12]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[14], sideOfSideNames_[6], sideOfSideNames_[15]};
      edgenames[4] = {sideOfSideNames_[12], sideOfSideNames_[11], sideOfSideNames_[16], sideOfSideNames_[3], sideOfSideNames_[15], sideOfSideNames_[17]};
      edgenames[5] = {sideOfSideNames_[13], sideOfSideNames_[9], sideOfSideNames_[17], sideOfSideNames_[1], sideOfSideNames_[14], sideOfSideNames_[5]};
      edgenames[6] = {sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11]};
      edgenames[7] = {sideOfSideNames_[0], sideOfSideNames_[17], sideOfSideNames_[10], sideOfSideNames_[11]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(4, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(6, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1]};
      vertexnames[1] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[2] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2]};
      vertexnames[3] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[4] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5]};
      vertexnames[5] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[4]};
      vertexnames[6] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10]};
      vertexnames[7] = {sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Rectangle(v_[6], v_[8], v_[1], Reals(h_[6], h_[8], h_[0], h_[1]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(v_[2], v_[3], v_[5], Reals(h_[2], h_[3], h_[4], h_[5]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(v_[7], v_[9], v_[2], Reals(h_[7], h_[9], h_[3], h_[2]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(v_[1], v_[0], v_[5], Reals(h_[1], h_[0], h_[4], h_[5]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Polygon(Points(v_[2], v_[7], v_[10], v_[6], v_[1], v_[5]), Reals(h_[2], h_[7], h_[10], h_[6], h_[1], h_[5]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[3], v_[9], v_[11], v_[8], v_[0], v_[4]), Reals(h_[3], h_[9], h_[11], h_[8], h_[0], h_[4]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Rectangle(v_[7], v_[9], v_[10], Reals(h_[7], h_[9], h_[11], h_[10]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[6], v_[8], v_[10], Reals(h_[6], h_[8], h_[11], h_[10]), facenames[7], edgenames[7], vertexnames[7]);
    }
    else
    {
      faces_[0] = new Rectangle(v_[6], v_[8], v_[1], Numbers(n_[6], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(v_[2], v_[3], v_[5], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(v_[7], v_[9], v_[2], Numbers(n_[8], n_[13], n_[4], n_[12]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(v_[1], v_[0], v_[5], Numbers(n_[2], n_[14], n_[6], n_[15]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Polygon(Points(v_[2], v_[7], v_[10], v_[6], v_[1], v_[5]), Numbers(n_[12], n_[11], n_[16], n_[3], n_[15], n_[7]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[3], v_[9], v_[11], v_[8], v_[0], v_[4]), Numbers(n_[13], n_[9], n_[17], n_[1], n_[14], n_[5]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Rectangle(v_[7], v_[9], v_[10], Numbers(n_[8], n_[9], n_[10], n_[11]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[6], v_[8], v_[10], Numbers(n_[0], n_[17], n_[10], n_[11]), facenames[7], edgenames[7], vertexnames[7]);
    }
  }
  else if (nboctants_==5)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7], sideNames_[8]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[8], sideOfSideNames_[15], sideOfSideNames_[4], sideOfSideNames_[14]};
      edgenames[3] = {sideOfSideNames_[16], sideOfSideNames_[6], sideOfSideNames_[17], sideOfSideNames_[12], sideOfSideNames_[20], sideOfSideNames_[2]};
      edgenames[4] = {sideOfSideNames_[13], sideOfSideNames_[17], sideOfSideNames_[7], sideOfSideNames_[14]};
      edgenames[5] = {sideOfSideNames_[15], sideOfSideNames_[9], sideOfSideNames_[19], sideOfSideNames_[1], sideOfSideNames_[16], sideOfSideNames_[5]};
      edgenames[6] = {sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11], sideOfSideNames_[12], sideOfSideNames_[13]};
      edgenames[7] = {sideOfSideNames_[0], sideOfSideNames_[19], sideOfSideNames_[10], sideOfSideNames_[18]};
      edgenames[8] = {sideOfSideNames_[31], sideOfSideNames_[20], sideOfSideNames_[11], sideOfSideNames_[18]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(4, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(6, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(6, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
      edgenames[8].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[8]};
      vertexnames[1] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4]};
      vertexnames[2] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1]};
      vertexnames[3] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[8]};
      vertexnames[4] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[1]};
      vertexnames[5] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3]};
      vertexnames[6] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[13], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[9]};
      vertexnames[7] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[13]};
      vertexnames[8] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[8].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Rectangle(v_[10], v_[6], v_[8], Reals(h_[10], h_[6], h_[0], h_[8]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(v_[1], v_[2], v_[4], Reals(h_[1], h_[2], h_[3], h_[4]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(v_[5], v_[7], v_[1], Reals(h_[5], h_[7], h_[2], h_[1]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Polygon(Points(v_[0], v_[3], v_[4], v_[9], v_[12], v_[8]), Reals(h_[0], h_[3], h_[4], h_[9], h_[12], h_[8]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(v_[5], v_[9], v_[1], Reals(h_[5], h_[9], h_[4], h_[1]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[2], v_[7], v_[11], v_[6], v_[0], v_[3]), Reals(h_[2], h_[7], h_[11], h_[6], h_[0], h_[3]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Polygon(Points(v_[5], v_[7], v_[11], v_[13], v_[12], v_[9]), Reals(h_[5], h_[7], h_[11], h_[13], h_[12], h_[9]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[10], v_[6], v_[13], Reals(h_[10], h_[6], h_[11], h_[13]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Rectangle(v_[10], v_[8], v_[13], Reals(h_[10], h_[8], h_[12], h_[13]), facenames[8], edgenames[8], vertexnames[8]);
    }
    else
    {
      faces_[0] = new Rectangle(v_[10], v_[6], v_[8], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(v_[1], v_[2], v_[4], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(v_[5], v_[7], v_[1], Numbers(n_[8], n_[15], n_[4], n_[14]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Polygon(Points(v_[0], v_[3], v_[4], v_[9], v_[12], v_[8]), Numbers(n_[16], n_[6], n_[17], n_[12], n_[20], n_[2]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(v_[5], v_[9], v_[1], Numbers(n_[13], n_[17], n_[7], n_[14]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[2], v_[7], v_[11], v_[6], v_[0], v_[3]), Numbers(n_[15], n_[9], n_[19], n_[1], n_[16], n_[5]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Polygon(Points(v_[5], v_[7], v_[11], v_[13], v_[12], v_[9]), Numbers(n_[8], n_[9], n_[10], n_[11], n_[12], n_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[10], v_[6], v_[13], Numbers(n_[0], n_[19], n_[10], n_[18]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new Rectangle(v_[10], v_[8], v_[13], Numbers(n_[3], n_[20], n_[11], n_[18]), facenames[8], edgenames[8], vertexnames[8]);
    }
  }
  else if (nboctants_==4)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else if (nboctants_==3)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3], sideOfSideNames_[4], sideOfSideNames_[5]};
      edgenames[1] = {sideOfSideNames_[6], sideOfSideNames_[7], sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[13], sideOfSideNames_[6], sideOfSideNames_[12]};
      edgenames[3] = {sideOfSideNames_[4], sideOfSideNames_[17], sideOfSideNames_[10], sideOfSideNames_[16]};
      edgenames[4] = {sideOfSideNames_[5], sideOfSideNames_[17], sideOfSideNames_[11], sideOfSideNames_[12]};
      edgenames[5] = {sideOfSideNames_[3], sideOfSideNames_[16], sideOfSideNames_[9], sideOfSideNames_[15]};
      edgenames[6] = {sideOfSideNames_[2], sideOfSideNames_[15], sideOfSideNames_[8], sideOfSideNames_[14]};
      edgenames[7] = {sideOfSideNames_[1], sideOfSideNames_[14], sideOfSideNames_[7], sideOfSideNames_[13]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(6, sideNames_[0]);
      edgenames[1].resize(6, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(4, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[1] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2]};
      vertexnames[2] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0]};
      vertexnames[3] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1]};
      vertexnames[4] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[0]};
      vertexnames[5] = {sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[7]};
      vertexnames[6] = {sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10]};
      vertexnames[7] = {sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Polygon(Points(v_[3], v_[8], v_[11], v_[9], v_[4], v_[5]), Reals(h_[3], h_[8], h_[11], h_[9], h_[4], h_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Polygon(Points(v_[0], v_[6], v_[10], v_[7], v_[1], v_[2]), Reals(h_[0], h_[6], h_[10], h_[7], h_[1], h_[2]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(v_[3], v_[8], v_[0], Reals(h_[3], h_[8], h_[6], h_[0]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(v_[4], v_[5], v_[1], Reals(h_[4], h_[5], h_[2], h_[1]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(v_[3], v_[5], v_[0], Reals(h_[3], h_[5], h_[2], h_[0]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(v_[9], v_[4], v_[7], Reals(h_[9], h_[4], h_[1], h_[7]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Rectangle(v_[11], v_[9], v_[10], Reals(h_[11], h_[9], h_[7], h_[10]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[8], v_[11], v_[6], Reals(h_[8], h_[11], h_[10], h_[6]), facenames[7], edgenames[7], vertexnames[7]);
    }
    else
    {
      faces_[0] = new Polygon(Points(v_[3], v_[8], v_[11], v_[9], v_[4], v_[5]), Numbers(n_[0], n_[1], n_[2], n_[3], n_[4], n_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Polygon(Points(v_[0], v_[6], v_[10], v_[7], v_[1], v_[2]), Numbers(n_[6], n_[7], n_[8], n_[9], n_[10], n_[11]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(v_[3], v_[8], v_[0], Numbers(n_[0], n_[13], n_[6], n_[12]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(v_[4], v_[5], v_[1], Numbers(n_[4], n_[17], n_[10], n_[16]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(v_[3], v_[5], v_[0], Numbers(n_[5], n_[17], n_[11], n_[12]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(v_[9], v_[4], v_[7], Numbers(n_[3], n_[16], n_[9], n_[15]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Rectangle(v_[11], v_[9], v_[10], Numbers(n_[2], n_[15], n_[8], n_[14]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new Rectangle(v_[8], v_[11], v_[6], Numbers(n_[1], n_[14], n_[7], n_[13]), facenames[7], edgenames[7], vertexnames[7]);
    }
  }
  else if (nboctants_==2)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else // nboctants_==1 (already checked here)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new Rectangle(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Rectangle(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Rectangle(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Rectangle(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Rectangle(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Rectangle(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
}

void Cuboid::buildParam(const Parameter& p)
{
  trace_p->push("Cuboid::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: center_ = p.get_pt(); break;
        case _integer: center_ = Point(real_t(p.get_i())); break;
        case _real: center_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_origin:
    {
      switch (p.type())
      {
        case _pt: origin_ = p.get_pt(); break;
        case _integer: origin_ = Point(real_t(p.get_i())); break;
        case _real: origin_ = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xlength:
    {
      switch (p.type())
      {
        case _integer: xlength_ = real_t(p.get_n()); break;
        case _real: xlength_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ylength:
    {
      switch (p.type())
      {
        case _integer: ylength_ = real_t(p.get_n()); break;
        case _real: ylength_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_zlength:
    {
      switch (p.type())
      {
        case _integer: zlength_ = real_t(p.get_n()); break;
        case _real: zlength_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xmin:
    {
      switch (p.type())
      {
        case _integer: xmin_ = real_t(p.get_i()); break;
        case _real: xmin_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xmax:
    {
      switch (p.type())
      {
        case _integer: xmax_ = real_t(p.get_i()); break;
        case _real: xmax_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ymin:
    {
      switch (p.type())
      {
        case _integer: ymin_ = real_t(p.get_i()); break;
        case _real: ymin_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ymax:
    {
      switch (p.type())
      {
        case _integer: ymax_ = real_t(p.get_i()); break;
        case _real: ymax_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_zmin:
    {
      switch (p.type())
      {
        case _integer: zmin_ = real_t(p.get_i()); break;
        case _real: zmin_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_zmax:
    {
      switch (p.type())
      {
        case _integer: zmax_ = real_t(p.get_i()); break;
        case _real: zmax_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Parallelepiped::buildParam(p); break;
  }
  trace_p->pop();
}

void Cuboid::buildDefaultParam(ParameterKey key)
{
  Parallelepiped::buildDefaultParam(key);
}

std::set<ParameterKey> Cuboid::getParamsKeys()
{
  std::set<ParameterKey> params = Parallelepiped::getParamsKeys();
  params.insert(_pk_center);
  params.insert(_pk_origin);
  params.insert(_pk_xlength);
  params.insert(_pk_ylength);
  params.insert(_pk_zlength);
  params.insert(_pk_xmin);
  params.insert(_pk_xmax);
  params.insert(_pk_ymin);
  params.insert(_pk_ymax);
  params.insert(_pk_zmin);
  params.insert(_pk_zmax);
  return params;
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

Cuboid::Cuboid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : Parallelepiped()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

Cuboid::Cuboid(const Cuboid& c) : Parallelepiped(c), center_(c.center_), origin_(c.origin_), isCenter_(c.isCenter_),
  isOrigin_(c.isOrigin_), xlength_(c.xlength_), ylength_(c.ylength_), zlength_(c.zlength_),
  xmin_(c.xmin_), xmax_(c.xmax_), ymin_(c.ymin_), ymax_(c.ymax_), zmin_(c.zmin_), zmax_(c.zmax_),
  isBounds_(c.isBounds_)
{}

string_t Cuboid::asString() const
{
  string_t s("Cuboid (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ", " + p_[4].roundToZero().toString() + ", " + tostring(nboctants_) + " octants)";
  return s;
}

//=======================================================================================================
//                                  Cube member functions
//=======================================================================================================
Cube::Cube() : Cuboid()
{ shape_ = _cube; }

void Cube::buildP()
{
  if (isCenter_)
  {
    Point shift0(-xlength_ / 2., -ylength_ / 2., -zlength_ / 2.);
    p_[0] = center_ + shift0;
    Point shift1(xlength_ / 2., -ylength_ / 2., -zlength_ / 2.);
    p_[1] = center_ + shift1;
    Point shift2(xlength_ / 2., ylength_ / 2., -zlength_ / 2.);
    p_[2] = center_ + shift2;
    Point shift3(-xlength_ / 2., ylength_ / 2., -zlength_ / 2.);
    p_[3] = center_ + shift3;
    Point shift4(-xlength_ / 2., -ylength_ / 2., zlength_ / 2.);
    p_[4] = center_ + shift4;
    Point shift5(xlength_ / 2., -ylength_ / 2., zlength_ / 2.);
    p_[5] = center_ + shift5;
    Point shift6(xlength_ / 2., ylength_ / 2., zlength_ / 2.);
    p_[6] = center_ + shift6;
    Point shift7(-xlength_ / 2., ylength_ / 2., zlength_ / 2.);
    p_[7] = center_ + shift7;
    origin_ = p_[0];
  }
  else if (isOrigin_)
  {
    p_[0] = origin_;
    Point shift1(xlength_, 0., 0.);
    p_[1] = origin_ + shift1;
    Point shift2(xlength_, ylength_, 0.);
    p_[2] = origin_ + shift2;
    Point shift3(0., ylength_, 0.);
    p_[3] = origin_ + shift3;
    Point shift4(0., 0., zlength_);
    p_[4] = origin_ + shift4;
    Point shift5(xlength_, 0., zlength_);
    p_[5] = origin_ + shift5;
    Point shift6(xlength_, ylength_, zlength_);
    p_[6] = origin_ + shift6;
    Point shift7(0., ylength_, zlength_);
    p_[7] = origin_ + shift7;
    center_ = (p_[0] + p_[6]) / 2.;
  }
  else // vertices keys are used so p_[2] is not defined
  {
    p_[2] = p_[1] + p_[3] - p_[0];
    p_[5] = p_[1] + p_[4] - p_[0];
    p_[6] = p_[2] + p_[4] - p_[0];
    p_[7] = p_[3] + p_[4] - p_[0];
    origin_ = p_[0];
    center_ = (p_[0] + p_[6]) / 2.;
    xlength_ = p_[0].distance(p_[1]);
    ylength_ = p_[0].distance(p_[3]);
    zlength_ = p_[0].distance(p_[4]);
    if (dot(p_[3] - p_[0], p_[1] - p_[0]) > theTolerance ||
        dot(p_[4] - p_[0], p_[1] - p_[0]) > theTolerance ||
        dot(p_[4] - p_[0], p_[3] - p_[0]) > theTolerance) { error("geometry_incoherent_points", words("shape", _cube)); }
  }
}

void Cube::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Cube::build");
  shape_ = _cube;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;
  // _faces key is replaced by (_v1,_v2,_v4), (_center|_origin, _xlength, _ylength) or (_xmin, _xmax, _ymin, _ymax)
  params.erase(_pk_faces);
  // _v3, _v6, _v7, _v8, _xlength, _ylength, _zlength, _xmin, _xmax, _ymin, _ymax, _zmin and _zmax are disabled
  params.erase(_pk_v3);
  params.erase(_pk_v6);
  params.erase(_pk_v7);
  params.erase(_pk_v8);
  params.erase(_pk_xlength);
  params.erase(_pk_ylength);
  params.erase(_pk_zlength);
  params.erase(_pk_xmin);
  params.erase(_pk_xmax);
  params.erase(_pk_ymin);
  params.erase(_pk_ymax);
  params.erase(_pk_zmin);
  params.erase(_pk_zmax);

  p_.resize(8);
  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _cube)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (v1,v2,v4), (center,length) or (origin,length)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v4 || key == _pk_v5) && usedParams.find(_pk_length) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_length)); }
    if (key == _pk_center && usedParams.find(_pk_origin) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_origin)); }
    if (key == _pk_origin && usedParams.find(_pk_center) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_center)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_length) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_length) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_length) && usedParams.find(_pk_v4) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v4)); }
    if ((key == _pk_center || key == _pk_origin || key == _pk_length) && usedParams.find(_pk_v5) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v5)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (v1,v2,v4) or (xmin,xmax,ymin,ymax) or (center,xlength,ylength) has to be set (no default values)
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v4) != params.end()) { error("param_missing", "v4"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v4) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if ((params.find(_pk_center) == params.end() || params.find(_pk_origin) == params.end()) && params.find(_pk_length) != params.end())
  { error("param_missing", "length"); }
  if (params.find(_pk_length) == params.end() && params.find(_pk_center) != params.end() && params.find(_pk_origin) != params.end())
  { error("param_missing", "center or origin"); }
  isCenter_ = true;
  isOrigin_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_length) != params.end()) { params.erase(_pk_length); }
  if (params.find(_pk_center) != params.end()) { params.erase(_pk_center); isCenter_ = false; }
  if (params.find(_pk_origin) != params.end()) { params.erase(_pk_origin); isOrigin_ = false; }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_v4); params.erase(_pk_v5); }

  // p_ is not (fully) built so we do
  buildP();
  if (std::abs(p_[0].distance(p_[1]) - p_[0].distance(p_[3])) > theTolerance ||
      std::abs(p_[0].distance(p_[1]) - p_[0].distance(p_[4])) > theTolerance ||
      std::abs(p_[0].distance(p_[3]) - p_[0].distance(p_[4])) > theTolerance)
  { error("geometry_incoherent_points", words("shape", _cube)); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  buildVNHAndBBox();
  computeMB();
  setFaces();
  trace_p->pop();
}

void Cube::setFaces()
{
  // first, we have to determine the number of faces, edges and vertices
  number_t nbFaces = 6, nbEdges=12, nbVertices=8;
  if (nboctants_ == 8) { nbFaces = 6, nbEdges=12, nbVertices=8; }
  else if (nboctants_ == 7) { nbFaces = 9; nbEdges = 21, nbVertices = 14; }
  else if (nboctants_ == 6) { nbFaces = 8; nbEdges = 18, nbVertices = 12; }
  else if (nboctants_ == 5) { nbFaces = 9; nbEdges = 21, nbVertices = 14; }
  else if (nboctants_ == 4) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ == 3) { nbFaces = 8; nbEdges = 18, nbVertices = 12; }
  else if (nboctants_ == 2) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ == 1) { nbFaces = 6; nbEdges = 12, nbVertices = 8; }
  else if (nboctants_ > 8) { error("is_greater", nboctants_, 8); }
  else { error("is_lesser", nboctants_, 1); }

  faces_.resize(nbFaces);

  if (nboctants_==8)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else if (nboctants_==7)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7], sideNames_[8]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3], sideOfSideNames_[4], sideOfSideNames_[5]};
      edgenames[1] = {sideOfSideNames_[6], sideOfSideNames_[7], sideOfSideNames_[8], sideOfSideNames_[9]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[18], sideOfSideNames_[10], sideOfSideNames_[15], sideOfSideNames_[6], sideOfSideNames_[14]};
      edgenames[3] = {sideOfSideNames_[4], sideOfSideNames_[16], sideOfSideNames_[8], sideOfSideNames_[9]};
      edgenames[4] = {sideOfSideNames_[5], sideOfSideNames_[17], sideOfSideNames_[9], sideOfSideNames_[14]};
      edgenames[5] = {sideOfSideNames_[16], sideOfSideNames_[7], sideOfSideNames_[15], sideOfSideNames_[11], sideOfSideNames_[20], sideOfSideNames_[3]};
      edgenames[6] = {sideOfSideNames_[10], sideOfSideNames_[11], sideOfSideNames_[12], sideOfSideNames_[13]};
      edgenames[7] = {sideOfSideNames_[2], sideOfSideNames_[20], sideOfSideNames_[12], sideOfSideNames_[19]};
      edgenames[8] = {sideOfSideNames_[1], sideOfSideNames_[19], sideOfSideNames_[13], sideOfSideNames_[18]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(6, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(6, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
      edgenames[8].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2]};
      vertexnames[1] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[3]};
      vertexnames[3] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[3]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[8]};
      vertexnames[6] = {sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
      vertexnames[7] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
      vertexnames[8] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[13], sideOfSideOfSideNames_[11]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[8].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Polygon(Points(v_[0], v_[7], v_[10], v_[8], v_[1], v_[2]), Reals(h_[0], h_[7], h_[10], h_[8], h_[1], h_[2]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(v_[3], v_[4], v_[6], Reals(h_[3], h_[4], h_[5], h_[6]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Polygon(Points(v_[0], v_[7], v_[11], v_[9], v_[4], v_[3]), Reals(h_[0], h_[7], h_[11], h_[9], h_[4], h_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(v_[2], v_[1], v_[6], Reals(h_[2], h_[1], h_[5], h_[6]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(v_[0], v_[2], v_[3], Reals(h_[0], h_[2], h_[6], h_[3]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[1], v_[5], v_[4], v_[9], v_[12], v_[8]), Reals(h_[1], h_[5], h_[4], h_[9], h_[11], h_[8]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new SquareGeo(v_[11], v_[9], v_[13], Reals(h_[11], h_[9], h_[12], h_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[10], v_[8], v_[13], Reals(h_[10], h_[8], h_[12], h_[13]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new SquareGeo(v_[7], v_[10], v_[11], Reals(h_[7], h_[10], h_[13], h_[11]), facenames[8], edgenames[8], vertexnames[8]);
    }
    else
    {
      faces_[0] = new Polygon(Points(v_[0], v_[7], v_[10], v_[8], v_[1], v_[2]), Numbers(n_[0], n_[1], n_[2], n_[3], n_[4], n_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(v_[3], v_[4], v_[6], Numbers(n_[6], n_[7], n_[8], n_[9]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new Polygon(Points(v_[0], v_[7], v_[11], v_[9], v_[4], v_[3]), Numbers(n_[0], n_[18], n_[10], n_[15], n_[6], n_[14]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(v_[2], v_[1], v_[6], Numbers(n_[4], n_[16], n_[8], n_[17]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(v_[0], v_[2], v_[3], Numbers(n_[5], n_[17], n_[9], n_[14]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[1], v_[5], v_[4], v_[9], v_[12], v_[8]), Numbers(n_[6], n_[7], n_[15], n_[11], n_[20], n_[3]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new SquareGeo(v_[11], v_[9], v_[13], Numbers(n_[10], n_[11], n_[12], n_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[10], v_[8], v_[13], Numbers(n_[2], n_[20], n_[12], n_[19]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new SquareGeo(v_[7], v_[10], v_[11], Numbers(n_[1], n_[19], n_[13], n_[18]), facenames[8], edgenames[8], vertexnames[8]);
    }
  }
  else if (nboctants_==6)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[8], sideOfSideNames_[13], sideOfSideNames_[4], sideOfSideNames_[12]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[14], sideOfSideNames_[6], sideOfSideNames_[15]};
      edgenames[4] = {sideOfSideNames_[12], sideOfSideNames_[11], sideOfSideNames_[16], sideOfSideNames_[3], sideOfSideNames_[15], sideOfSideNames_[17]};
      edgenames[5] = {sideOfSideNames_[13], sideOfSideNames_[9], sideOfSideNames_[17], sideOfSideNames_[1], sideOfSideNames_[14], sideOfSideNames_[5]};
      edgenames[6] = {sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11]};
      edgenames[7] = {sideOfSideNames_[0], sideOfSideNames_[17], sideOfSideNames_[10], sideOfSideNames_[11]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(4, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(6, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1]};
      vertexnames[1] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[2] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2]};
      vertexnames[3] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[4] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5]};
      vertexnames[5] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[4]};
      vertexnames[6] = {sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10]};
      vertexnames[7] = {sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new SquareGeo(v_[6], v_[8], v_[1], Reals(h_[6], h_[8], h_[0], h_[1]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(v_[2], v_[3], v_[5], Reals(h_[2], h_[3], h_[4], h_[5]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(v_[7], v_[9], v_[2], Reals(h_[7], h_[9], h_[3], h_[2]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(v_[1], v_[0], v_[5], Reals(h_[1], h_[0], h_[4], h_[5]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Polygon(Points(v_[2], v_[7], v_[10], v_[6], v_[1], v_[5]), Reals(h_[2], h_[7], h_[10], h_[6], h_[1], h_[5]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[3], v_[9], v_[11], v_[8], v_[0], v_[4]), Reals(h_[3], h_[9], h_[11], h_[8], h_[0], h_[4]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new SquareGeo(v_[7], v_[9], v_[10], Reals(h_[7], h_[9], h_[11], h_[10]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[6], v_[8], v_[10], Reals(h_[6], h_[8], h_[11], h_[10]), facenames[7], edgenames[7], vertexnames[7]);
    }
    else
    {
      faces_[0] = new SquareGeo(v_[6], v_[8], v_[1], Numbers(n_[6], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(v_[2], v_[3], v_[5], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(v_[7], v_[9], v_[2], Numbers(n_[8], n_[13], n_[4], n_[12]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(v_[1], v_[0], v_[5], Numbers(n_[2], n_[14], n_[6], n_[15]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new Polygon(Points(v_[2], v_[7], v_[10], v_[6], v_[1], v_[5]), Numbers(n_[12], n_[11], n_[16], n_[3], n_[15], n_[7]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[3], v_[9], v_[11], v_[8], v_[0], v_[4]), Numbers(n_[13], n_[9], n_[17], n_[1], n_[14], n_[5]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new SquareGeo(v_[7], v_[9], v_[10], Numbers(n_[8], n_[9], n_[10], n_[11]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[6], v_[8], v_[10], Numbers(n_[0], n_[17], n_[10], n_[11]), facenames[7], edgenames[7], vertexnames[7]);
    }
  }
  else if (nboctants_==5)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7], sideNames_[8]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[8], sideOfSideNames_[15], sideOfSideNames_[4], sideOfSideNames_[14]};
      edgenames[3] = {sideOfSideNames_[16], sideOfSideNames_[6], sideOfSideNames_[17], sideOfSideNames_[12], sideOfSideNames_[20], sideOfSideNames_[2]};
      edgenames[4] = {sideOfSideNames_[13], sideOfSideNames_[17], sideOfSideNames_[7], sideOfSideNames_[14]};
      edgenames[5] = {sideOfSideNames_[15], sideOfSideNames_[9], sideOfSideNames_[19], sideOfSideNames_[1], sideOfSideNames_[16], sideOfSideNames_[5]};
      edgenames[6] = {sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11], sideOfSideNames_[12], sideOfSideNames_[13]};
      edgenames[7] = {sideOfSideNames_[0], sideOfSideNames_[19], sideOfSideNames_[10], sideOfSideNames_[18]};
      edgenames[8] = {sideOfSideNames_[31], sideOfSideNames_[20], sideOfSideNames_[11], sideOfSideNames_[18]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(4, sideNames_[0]);
      edgenames[1].resize(4, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(6, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(6, sideNames_[0]);
      edgenames[6].resize(6, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
      edgenames[8].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[8]};
      vertexnames[1] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4]};
      vertexnames[2] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1]};
      vertexnames[3] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[8]};
      vertexnames[4] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[1]};
      vertexnames[5] = {sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3]};
      vertexnames[6] = {sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[13], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[9]};
      vertexnames[7] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[13]};
      vertexnames[8] = {sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[12], sideOfSideOfSideNames_[13]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[8].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new SquareGeo(v_[10], v_[6], v_[8], Reals(h_[10], h_[6], h_[0], h_[8]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(v_[1], v_[2], v_[4], Reals(h_[1], h_[2], h_[3], h_[4]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(v_[5], v_[7], v_[1], Reals(h_[5], h_[7], h_[2], h_[1]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Polygon(Points(v_[0], v_[3], v_[4], v_[9], v_[12], v_[8]), Reals(h_[0], h_[3], h_[4], h_[9], h_[12], h_[8]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(v_[5], v_[9], v_[1], Reals(h_[5], h_[9], h_[4], h_[1]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[2], v_[7], v_[11], v_[6], v_[0], v_[3]), Reals(h_[2], h_[7], h_[11], h_[6], h_[0], h_[3]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Polygon(Points(v_[5], v_[7], v_[11], v_[13], v_[12], v_[9]), Reals(h_[5], h_[7], h_[11], h_[13], h_[12], h_[9]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[10], v_[6], v_[13], Reals(h_[10], h_[6], h_[11], h_[13]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new SquareGeo(v_[10], v_[8], v_[13], Reals(h_[10], h_[8], h_[12], h_[13]), facenames[8], edgenames[8], vertexnames[8]);
    }
    else
    {
      faces_[0] = new SquareGeo(v_[10], v_[6], v_[8], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(v_[1], v_[2], v_[4], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(v_[5], v_[7], v_[1], Numbers(n_[8], n_[15], n_[4], n_[14]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new Polygon(Points(v_[0], v_[3], v_[4], v_[9], v_[12], v_[8]), Numbers(n_[16], n_[6], n_[17], n_[12], n_[20], n_[2]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(v_[5], v_[9], v_[1], Numbers(n_[13], n_[17], n_[7], n_[14]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new Polygon(Points(v_[2], v_[7], v_[11], v_[6], v_[0], v_[3]), Numbers(n_[15], n_[9], n_[19], n_[1], n_[16], n_[5]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new Polygon(Points(v_[5], v_[7], v_[11], v_[13], v_[12], v_[9]), Numbers(n_[8], n_[9], n_[10], n_[11], n_[12], n_[13]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[10], v_[6], v_[13], Numbers(n_[0], n_[19], n_[10], n_[18]), facenames[7], edgenames[7], vertexnames[7]);
      faces_[8] = new SquareGeo(v_[10], v_[8], v_[13], Numbers(n_[3], n_[20], n_[11], n_[18]), facenames[8], edgenames[8], vertexnames[8]);
    }
  }
  else if (nboctants_==4)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else if (nboctants_==3)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5], sideNames_[6], sideNames_[7]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3], sideOfSideNames_[4], sideOfSideNames_[5]};
      edgenames[1] = {sideOfSideNames_[6], sideOfSideNames_[7], sideOfSideNames_[8], sideOfSideNames_[9], sideOfSideNames_[10], sideOfSideNames_[11]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[13], sideOfSideNames_[6], sideOfSideNames_[12]};
      edgenames[3] = {sideOfSideNames_[4], sideOfSideNames_[17], sideOfSideNames_[10], sideOfSideNames_[16]};
      edgenames[4] = {sideOfSideNames_[5], sideOfSideNames_[17], sideOfSideNames_[11], sideOfSideNames_[12]};
      edgenames[5] = {sideOfSideNames_[3], sideOfSideNames_[16], sideOfSideNames_[9], sideOfSideNames_[15]};
      edgenames[6] = {sideOfSideNames_[2], sideOfSideNames_[15], sideOfSideNames_[8], sideOfSideNames_[14]};
      edgenames[7] = {sideOfSideNames_[1], sideOfSideNames_[14], sideOfSideNames_[7], sideOfSideNames_[13]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames[0].resize(6, sideNames_[0]);
      edgenames[1].resize(6, sideNames_[0]);
      edgenames[2].resize(4, sideNames_[0]);
      edgenames[3].resize(4, sideNames_[0]);
      edgenames[4].resize(4, sideNames_[0]);
      edgenames[5].resize(4, sideNames_[0]);
      edgenames[6].resize(4, sideNames_[0]);
      edgenames[7].resize(4, sideNames_[0]);
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5]};
      vertexnames[1] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2]};
      vertexnames[2] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[0]};
      vertexnames[3] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[1]};
      vertexnames[4] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[0]};
      vertexnames[5] = {sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[7]};
      vertexnames[6] = {sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[9], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[10]};
      vertexnames[7] = {sideOfSideOfSideNames_[8], sideOfSideOfSideNames_[11], sideOfSideOfSideNames_[10], sideOfSideOfSideNames_[6]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames[0].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[1].resize(6, sideOfSideOfSideNames_[0]);
      vertexnames[2].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[3].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[4].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[5].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[6].resize(4, sideOfSideOfSideNames_[0]);
      vertexnames[7].resize(4, sideOfSideOfSideNames_[0]);
    }

    if (h_.size() != 0)
    {
      faces_[0] = new Polygon(Points(v_[3], v_[8], v_[11], v_[9], v_[4], v_[5]), Reals(h_[3], h_[8], h_[11], h_[9], h_[4], h_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Polygon(Points(v_[0], v_[6], v_[10], v_[7], v_[1], v_[2]), Reals(h_[0], h_[6], h_[10], h_[7], h_[1], h_[2]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(v_[3], v_[8], v_[0], Reals(h_[3], h_[8], h_[6], h_[0]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(v_[4], v_[5], v_[1], Reals(h_[4], h_[5], h_[2], h_[1]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(v_[3], v_[5], v_[0], Reals(h_[3], h_[5], h_[2], h_[0]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(v_[9], v_[4], v_[7], Reals(h_[9], h_[4], h_[1], h_[7]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new SquareGeo(v_[11], v_[9], v_[10], Reals(h_[11], h_[9], h_[7], h_[10]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[8], v_[11], v_[6], Reals(h_[8], h_[11], h_[10], h_[6]), facenames[7], edgenames[7], vertexnames[7]);
    }
    else
    {
      faces_[0] = new Polygon(Points(v_[3], v_[8], v_[11], v_[9], v_[4], v_[5]), Numbers(n_[0], n_[1], n_[2], n_[3], n_[4], n_[5]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new Polygon(Points(v_[0], v_[6], v_[10], v_[7], v_[1], v_[2]), Numbers(n_[6], n_[7], n_[8], n_[9], n_[10], n_[11]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(v_[3], v_[8], v_[0], Numbers(n_[0], n_[13], n_[6], n_[12]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(v_[4], v_[5], v_[1], Numbers(n_[4], n_[17], n_[10], n_[16]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(v_[3], v_[5], v_[0], Numbers(n_[5], n_[17], n_[11], n_[12]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(v_[9], v_[4], v_[7], Numbers(n_[3], n_[16], n_[9], n_[15]), facenames[5], edgenames[5], vertexnames[5]);
      faces_[6] = new SquareGeo(v_[11], v_[9], v_[10], Numbers(n_[2], n_[15], n_[8], n_[14]), facenames[6], edgenames[6], vertexnames[6]);
      faces_[7] = new SquareGeo(v_[8], v_[11], v_[6], Numbers(n_[1], n_[14], n_[7], n_[13]), facenames[7], edgenames[7], vertexnames[7]);
    }
  }
  else if (nboctants_==2)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
  else // nboctants_==1 (already checked here)
  {
    std::vector<string_t> facenames(nbFaces);
    if (sideNames_.size() >= nbFaces) { facenames = {sideNames_[0], sideNames_[1], sideNames_[2], sideNames_[3], sideNames_[4], sideNames_[5]}; }
    else if (sideNames_.size() >= 1) { facenames.resize(nbFaces, sideNames_[0]); }
    std::vector<std::vector<string_t>> edgenames(nbFaces);
    if (sideOfSideNames_.size() >= nbEdges)
    {
      edgenames[0] = {sideOfSideNames_[0], sideOfSideNames_[1], sideOfSideNames_[2], sideOfSideNames_[3]};
      edgenames[1] = {sideOfSideNames_[4], sideOfSideNames_[5], sideOfSideNames_[6], sideOfSideNames_[7]};
      edgenames[2] = {sideOfSideNames_[0], sideOfSideNames_[9], sideOfSideNames_[4], sideOfSideNames_[3]};
      edgenames[3] = {sideOfSideNames_[2], sideOfSideNames_[10], sideOfSideNames_[6], sideOfSideNames_[11]};
      edgenames[4] = {sideOfSideNames_[3], sideOfSideNames_[11], sideOfSideNames_[7], sideOfSideNames_[8]};
      edgenames[5] = {sideOfSideNames_[1], sideOfSideNames_[10], sideOfSideNames_[5], sideOfSideNames_[9]};
    }
    else if (sideOfSideNames_.size() >= 1)
    {
      edgenames.resize(nbFaces, std::vector<string_t>(4,sideOfSideNames_[0]));
    }
    std::vector<std::vector<string_t>> vertexnames(nbFaces);
    if (sideOfSideOfSideNames_.size() >= nbVertices)
    {
      vertexnames[0] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[3]};
      vertexnames[1] = {sideOfSideOfSideNames_[4], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[2] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[5], sideOfSideOfSideNames_[4]};
      vertexnames[3] = {sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[7]};
      vertexnames[4] = {sideOfSideOfSideNames_[0], sideOfSideOfSideNames_[3], sideOfSideOfSideNames_[7], sideOfSideOfSideNames_[4]};
      vertexnames[5] = {sideOfSideOfSideNames_[1], sideOfSideOfSideNames_[2], sideOfSideOfSideNames_[6], sideOfSideOfSideNames_[5]};
    }
    else if (sideOfSideOfSideNames_.size() >= 1)
    {
      vertexnames.resize(nbFaces, std::vector<string_t>(4,sideOfSideOfSideNames_[0]));
    }

    if (h_.size() != 0)
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Reals(h_[0], h_[1], h_[2], h_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Reals(h_[4], h_[5], h_[6], h_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Reals(h_[0], h_[1], h_[5], h_[4]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Reals(h_[3], h_[2], h_[6], h_[7]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Reals(h_[0], h_[3], h_[7], h_[4]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Reals(h_[1], h_[2], h_[6], h_[5]), facenames[5], edgenames[5], vertexnames[5]);
    }
    else
    {
      faces_[0] = new SquareGeo(p_[0], p_[1], p_[3], Numbers(n_[0], n_[1], n_[2], n_[3]), facenames[0], edgenames[0], vertexnames[0]);
      faces_[1] = new SquareGeo(p_[4], p_[5], p_[7], Numbers(n_[4], n_[5], n_[6], n_[7]), facenames[1], edgenames[1], vertexnames[1]);
      faces_[2] = new SquareGeo(p_[0], p_[1], p_[4], Numbers(n_[0], n_[9], n_[4], n_[3]), facenames[2], edgenames[2], vertexnames[2]);
      faces_[3] = new SquareGeo(p_[3], p_[2], p_[7], Numbers(n_[2], n_[10], n_[6], n_[11]), facenames[3], edgenames[3], vertexnames[3]);
      faces_[4] = new SquareGeo(p_[0], p_[3], p_[4], Numbers(n_[3], n_[11], n_[7], n_[8]), facenames[4], edgenames[4], vertexnames[4]);
      faces_[5] = new SquareGeo(p_[1], p_[2], p_[5], Numbers(n_[1], n_[10], n_[5], n_[9]), facenames[5], edgenames[5], vertexnames[5]);
    }
  }
}

void Cube::buildParam(const Parameter& p)
{
  trace_p->push("Cube::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_length:
    {
      switch (p.type())
      {
        case _integer: xlength_ = ylength_ = zlength_ = real_t(p.get_n()); break;
        case _real: xlength_ = ylength_ = zlength_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Cuboid::buildParam(p); break;
  }
  trace_p->pop();
}

void Cube::buildDefaultParam(ParameterKey key)
{
  Cuboid::buildDefaultParam(key);
}

std::set<ParameterKey> Cube::getParamsKeys()
{
  std::set<ParameterKey> params = Cuboid::getParamsKeys();
  params.insert(_pk_length);
  return params;
}

Cube::Cube(Parameter p1, Parameter p2) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

Cube::Cube(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : Cuboid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

Cube::Cube(const Cube& c) : Cuboid(c)
{}

number_t Cube::nbSubdiv() const
{
  number_t n0 = n(1);
  for (number_t i = 1; i < 12; ++i) { if (n(i + 1) > n0) { n0 = n(i + 1); } }
  return static_cast<number_t>(theTolerance + std::log(n0 - 1) / std::log(2));
}

//! Format Cube as string: "center = (.,.,.), edge length = L"
string_t Cube::asString() const
{
  string_t s("Cube (");
  s += p_[0].roundToZero().toString() + ", " + p_[1].roundToZero().toString() + ", " + p_[3].roundToZero().toString() + ", " + p_[4].roundToZero().toString() + ", " + tostring(nboctants_) + " octants)";
  return s;
}

//=======================================================================================================
//                                  Ellipsoid member functions
//=======================================================================================================
//! default ellipsoid is sphere of center (0,0,0) and radius 1
Ellipsoid::Ellipsoid() : Volume(), xradius_(1.), yradius_(1.), zradius_(1.), isAxis_(false), nboctants_(8), type_(1)
{
  shape_ = _ellipsoid;
  p_.resize(7);
  p_[0] = Point(0., 0., 0.);
  p_[1] = Point(1., 0., 0.);
  p_[2] = Point(0., 1., 0.);
  p_[3] = Point(-1., 0., 0.);
  p_[4] = Point(0., -1., 0.);
  p_[5] = Point(0., 0., -1.);
  p_[6] = Point(0., 0., 1.);
  n_.resize(12, 2);
  computeMB();
}

void Ellipsoid::buildPVNHAndBBox()
{
  // first, we have to build P
  if (isAxis_) // center and lengths keys are used
  {
    Point shift1(xradius_, 0., 0.);
    p_[1] = p_[0] + shift1;
    Point shift2(0., yradius_, 0.);
    p_[2] = p_[0] + shift2;
    Point shift3(-xradius_, 0., 0.);
    p_[3] = p_[0] + shift3;
    Point shift4(0., -yradius_, 0.);
    p_[4] = p_[0] + shift4;
    Point shift5(0., 0., -zradius_);
    p_[5] = p_[0] + shift5;
    Point shift6(0., 0., zradius_);
    p_[6] = p_[0] + shift6;
  }
  else // vertices keys are used
  {
    p_[3] = 2.*p_[0] - p_[1];
    p_[4] = 2.*p_[0] - p_[2];
    p_[5] = 2.*p_[0] - p_[6];
    xradius_ = p_[0].distance(p_[1]);
    yradius_ = p_[0].distance(p_[2]);
    zradius_ = p_[0].distance(p_[6]);
    if (dot(p_[2] - p_[0], p_[1] - p_[0]) > theTolerance ||
        dot(p_[6] - p_[0], p_[1] - p_[0]) > theTolerance ||
        dot(p_[6] - p_[0], p_[2] - p_[0]) > theTolerance) { error("geometry_incoherent_points", words("shape", shape_)); }
  }

  if (nboctants_ == 8 || nboctants_ == 0)
  {
    // true list of vertices
    v_ = p_;
    // checking nnodes and hsteps size
    if (n_.size() != 0)
    {
      if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
      else if (n_.size() == 3)
      {
        number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
        n_.clear();
        n_.resize(12, n0);
        for (number_t i = 4; i < 8; ++i) { n_[i] = n1; }
        for (number_t i = 8; i < 12; ++i) { n_[i] = n2; }
      }
      else if (n_.size() != 12) { error("bad_size", "nnodes", 12, n_.size()); }
    }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(6, h0); }
      else if (h_.size() != 6) { error("bad_size", "hsteps", 6, h_.size()); }
    }
    boundingBox = BoundingBox(p_[5] + p_[3] - p_[2], p_[5] + p_[1] - p_[2], p_[5] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]);
  }
  else if (nboctants_ == 7)
  {
    // true list of vertices
    v_ = p_;
    // checking nnodes and hsteps size
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(15, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(15, n0);
      for (number_t i = 4; i < 8; ++i) { n_[i] = n1; }
      for (number_t i = 8; i < 12; ++i) { n_[i] = n2; }
      // classical approximation of the perimeter of an ellipse is used here
      n_[12] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[13] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[14] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
    }
    else if (n_.size() == 12)
    {
      std::vector<number_t> n(15);
      for (number_t i = 0; i < 12; ++i) { n[i] = n_[i]; }
      number_t n0 = (n_[0] + n_[1] + n_[2] + n_[3]) / 4, // average number of nodes on each arc of the first base ellipse
               n1 = (n_[4] + n_[5] + n_[6] + n_[7]) / 4,
               n2 = (n_[8] + n_[9] + n_[10] + n_[11]) / 4;
      // classical approximation of the perimeter of an ellipse is used here
      n[12] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[13] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[14] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
      n_.clear();
      n_.resize(15);
      n_ = n;
    }
    else if (n_.size() != 15) { error("bad_size", "nnodes", 15, n_.size()); }

    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(7, h0); }
      else if (h_.size() != 7) { error("bad_size", "hsteps", 7, h_.size()); }
    }
    boundingBox = BoundingBox(p_[5] + p_[3] - p_[2], p_[5] + p_[1] - p_[2], p_[5] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]);
  }
  else if (nboctants_ == 6)
  {
    // true list of vertices
    v_ = p_;
    // checking nnodes and hsteps size
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(15, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(15, n0);
      for (number_t i = 4; i < 8; ++i) { n_[i] = n1; }
      for (number_t i = 8; i < 11; ++i) { n_[i] = n2; }
      // classical approximation of the perimeter of an ellipse is used here
      n_[11] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[12] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[13] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[14] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
    }
    else if (n_.size() == 12)
    {
      std::vector<number_t> n(15);
      for (number_t i = 0; i < 10; ++i) { n[i] = n_[i]; }
      n[10] = n_[11];
      number_t n0 = (n_[0] + n_[1] + n_[2] + n_[3]) / 4, // average number of nodes on each arc of the first base ellipse
               n1 = (n_[4] + n_[5] + n_[6] + n_[7]) / 4,
               n2 = (n_[8] + n_[9] + n_[10] + n_[11]) / 4;
      // classical approximation of the perimeter of an ellipse is used here
      n[11] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[12] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[13] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[14] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
      n_.clear();
      n_.resize(15);
      n_ = n;
    }
    else if (n_.size() != 15) { error("bad_size", "nnodes", 15, n_.size()); }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(7, h0); }
      else if (h_.size() != 7) { error("bad_size", "hsteps", 7, h_.size()); }
    }
    boundingBox = BoundingBox(p_[5] + p_[3] - p_[2], p_[5] + p_[1] - p_[2], p_[5] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]);
  }
  else if (nboctants_ == 5)
  {
    // true list of vertices
    v_ = p_;
    // checking nnodes and hsteps size
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(15, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(15, n0);
      for (number_t i = 4; i < 7; ++i) { n_[i] = n1; }
      for (number_t i = 7; i < 10; ++i) { n_[i] = n2; }
      // classical approximation of the perimeter of an ellipse is used here
      n_[10] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[11] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[12] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[13] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[14] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
    }
    else if (n_.size() == 12)
    {
      std::vector<number_t> n(15);
      for (number_t i = 0; i < 6; ++i) { n[i] = n_[i]; }
      n[6] = n_[7];
      n[7] = n_[8];
      n[8] = n_[9];
      n[9] = n_[11];
      number_t n0 = (n_[0] + n_[1] + n_[2] + n_[3]) / 4, // average number of nodes on each arc of the first base ellipse
               n1 = (n_[4] + n_[5] + n_[6] + n_[7]) / 4,
               n2 = (n_[8] + n_[9] + n_[10] + n_[11]) / 4;
      // classical approximation of the perimeter of an ellipse is used here
      n[10] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[11] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[12] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[13] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[14] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
      n_.clear();
      n_.resize(15);
      n_ = n;
    }
    else if (n_.size() != 15) { error("bad_size", "nnodes", 15, n_.size()); }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(7, h0); }
      else if (h_.size() != 7) { error("bad_size", "hsteps", 7, h_.size()); }
    }
    boundingBox = BoundingBox(p_[5] + p_[3] - p_[2], p_[5] + p_[1] - p_[2], p_[5] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]);
  }
  else if (nboctants_ == 4)
  {
    // true list of vertices
    v_.resize(6);
    v_[0] = p_[0]; v_[1] = p_[1]; v_[2] = p_[2]; v_[3] = p_[3]; v_[4] = p_[4]; v_[5] = p_[6];
    // checking nnodes and hsteps size
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(12, n0);
      for (number_t i = 4; i < 6; ++i) { n_[i] = n1; }
      for (number_t i = 6; i < 8; ++i) { n_[i] = n2; }
      // classical approximation of the perimeter of an ellipse is used here
      n_[8] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[9] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[10] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[11] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
    }
    else if (n_.size() == 12)
    {
      std::vector<number_t> n(12);
      for (number_t i = 0; i < 6; ++i) { n[i] = n_[i]; }
      n[6] = n_[8];
      n[7] = n_[9];
      number_t n0 = (n_[0] + n_[1] + n_[2] + n_[3]) / 4, // average number of nodes on each arc of the first base ellipse
               n1 = (n_[4] + n_[5] + n_[6] + n_[7]) / 4,
               n2 = (n_[8] + n_[9] + n_[10] + n_[11]) / 4;
      // classical approximation of the perimeter of an ellipse is used here
      n[8] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[9] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[10] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[11] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_.clear();
      n_.resize(12);
      n_ = n;
    }
    else if (n_.size() != 12) { error("bad_size", "nnodes", 12, n_.size()); }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(6, h0); }
      else if (h_.size() != 6) { error("bad_size", "hsteps", 6, h_.size()); }
    }
    boundingBox = BoundingBox(p_[0] + p_[3] - p_[2], p_[0] + p_[1] - p_[2], p_[0] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]);
  }
  else if (nboctants_ == 3)
  {
    // true list of vertices
    v_.resize(6);
    v_[0] = p_[0]; v_[1] = p_[1]; v_[2] = p_[2]; v_[3] = p_[3]; v_[4] = p_[4]; v_[5] = p_[6];
    // checking nnodes and hsteps size
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(12, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(12, n0);
      for (number_t i = 3; i < 5; ++i) { n_[i] = n1; }
      for (number_t i = 5; i < 7; ++i) { n_[i] = n2; }
      // classical approximation of the perimeter of an ellipse is used here
      n_[7] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[8] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[9] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[10] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[11] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
    }
    else if (n_.size() == 12)
    {
      std::vector<number_t> n(12);
      n[0] = n_[0];
      n[1] = n_[1];
      n[2] = n_[2];
      n[3] = n_[4];
      n[4] = n_[5];
      n[5] = n_[8];
      n[6] = n_[9];
      number_t n0 = (n_[0] + n_[1] + n_[2] + n_[3]) / 4, // average number of nodes on each arc of the first base ellipse
               n1 = (n_[4] + n_[5] + n_[6] + n_[7]) / 4,
               n2 = (n_[8] + n_[9] + n_[10] + n_[11]) / 4;
      // classical approximation of the perimeter of an ellipse is used here
      n[7] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[8] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[9] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[10] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[11] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
      n_.clear();
      n_.resize(12);
      n_ = n;
    }
    else if (n_.size() != 12) { error("bad_size", "nnodes", 12, n_.size()); }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(6, h0); }
      else if (h_.size() != 6) { error("bad_size", "hsteps", 6, h_.size()); }
    }
    boundingBox = BoundingBox(p_[0] + p_[3] - p_[2], p_[0] + p_[1] - p_[2], p_[0] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]);
  }
  else if (nboctants_ == 2)
  {
    // true list of vertices
    v_.resize(5);
    v_[0] = p_[0]; v_[1] = p_[1]; v_[2] = p_[2]; v_[3] = p_[3]; v_[4] = p_[6];
    // checking nnodes and hsteps size
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(9, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(9, n0);
      for (number_t i = 2; i < 4; ++i) { n_[i] = n1; }
      n_[4] = n2;
      // classical approximation of the perimeter of an ellipse is used here
      n_[5] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[6] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[7] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[8] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
    }
    else if (n_.size() == 12)
    {
      std::vector<number_t> n(9);
      n[0] = n_[0];
      n[1] = n_[1];
      n[2] = n_[4];
      n[3] = n_[5];
      n[4] = n_[8];
      number_t n0 = (n_[0] + n_[1] + n_[2] + n_[3]) / 4, // average number of nodes on each arc of the first base ellipse
               n1 = (n_[4] + n_[5] + n_[6] + n_[7]) / 4,
               n2 = (n_[8] + n_[9] + n_[10] + n_[11]) / 4;
      // classical approximation of the perimeter of an ellipse is used here
      n[5] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[6] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[7] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[8] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
      n_.clear();
      n_.resize(9);
      n_ = n;
    }
    else if (n_.size() != 9) { error("bad_size", "nnodes", 9, n_.size()); }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(5, h0); }
      else if (h_.size() != 5) { error("bad_size", "hsteps", 5, h_.size()); }
    }
    boundingBox = BoundingBox(p_[3], p_[1], p_[0] + p_[2] - p_[1], p_[6] + p_[1] - p_[0]);
  }
  else if (nboctants_ == 1)
  {
    // true list of vertices
    v_.resize(4);
    v_[0] = p_[0]; v_[1] = p_[1]; v_[2] = p_[2]; v_[3] = p_[6];
    // checking nnodes and hsteps size
    if (n_.size() == 1) { number_t n0 = n_[0]; n_.resize(6, n0); }
    else if (n_.size() == 3)
    {
      number_t n0 = n_[0], n1 = n_[1], n2 = n_[2];
      n_.clear();
      n_.resize(9);
      n_[0] = n0;
      n_[1] = n1;
      n_[2] = n2;
      // classical approximation of the perimeter of an ellipse is used here
      n_[3] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n_[4] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n_[5] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
    }
    else if (n_.size() == 12)
    {
      std::vector<number_t> n(6);
      n[0] = n_[0];
      n[1] = n_[4];
      n[2] = n_[8];
      number_t n0 = (n_[0] + n_[1] + n_[2] + n_[3]) / 4, // average number of nodes on each arc of the first base ellipse
               n1 = (n_[4] + n_[5] + n_[6] + n_[7]) / 4,
               n2 = (n_[8] + n_[9] + n_[10] + n_[11]) / 4;
      // classical approximation of the perimeter of an ellipse is used here
      n[3] = number_t(2. / pi_ * std::sqrt(n1 * n1 + n0 * n0 - n2 * n2));
      n[4] = number_t(2. / pi_ * std::sqrt(n0 * n0 + n2 * n2 - n1 * n1));
      n[5] = number_t(2. / pi_ * std::sqrt(n2 * n2 + n1 * n1 - n0 * n0));
      n_.clear();
      n_.resize(6);
      n_ = n;
    }
    else if (n_.size() != 6) { error("bad_size", "nnodes", 6, n_.size()); }
    if (h_.size() != 0)
    {
      if (h_.size() == 1) { real_t h0 = h_[0]; h_.resize(4, h0); }
      else if (h_.size() != 4) { error("bad_size", "hsteps", 4, h_.size()); }
    }
    boundingBox = BoundingBox(p_[0], p_[1], p_[2], p_[6]);
  }
  else if (nboctants_ > 8) { error("is_greater", nboctants_, 8); }
  else { error("is_lesser", nboctants_, 0); }
}

void Ellipsoid::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Ellipsoid::build");
  shape_ = _ellipsoid;
  p_.resize(7);
  std::set<ParameterKey> params = getParamsKeys(), usedParams;

  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _ellipsoid)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (center,v1,v2,v6), (center,xlength,ylength,zlength) or (center,xradius,yradius,zradius)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v6 || key == _pk_xradius || key == _pk_yradius || key == _pk_zradius)
        && usedParams.find(_pk_xlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xlength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v6 || key == _pk_xradius || key == _pk_yradius || key == _pk_zradius)
        && usedParams.find(_pk_ylength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_ylength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v6 || key == _pk_xradius || key == _pk_yradius || key == _pk_zradius)
        && usedParams.find(_pk_zlength) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zlength)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v6 || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength)
        && usedParams.find(_pk_xradius) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_xradius)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v6 || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength)
        && usedParams.find(_pk_yradius) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_yradius)); }
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v6 || key == _pk_xlength || key == _pk_ylength || key == _pk_zlength)
        && usedParams.find(_pk_zradius) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_zradius)); }
    if ((key == _pk_xlength || key == _pk_ylength || key == _pk_zlength || key == _pk_xradius || key == _pk_yradius || key == _pk_zradius)
        && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_xlength || key == _pk_ylength || key == _pk_zlength || key == _pk_xradius || key == _pk_yradius || key == _pk_zradius)
        && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_xlength || key == _pk_ylength || key == _pk_zlength || key == _pk_xradius || key == _pk_yradius || key == _pk_zradius)
        && usedParams.find(_pk_v6) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v6)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (center,v1,v2,v6) or (center,xlength,ylength,zlength) has to be set (no default values)
  if (params.find(_pk_center) != params.end()) { error("param_missing", "center"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v6) != params.end()) { error("param_missing", "v6"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v6) != params.end()) { error("param_missing", "v6"); }
  if (params.find(_pk_v6) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v6) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_ylength) != params.end()) { error("param_missing", "ylength"); }
  if (params.find(_pk_xlength) == params.end() && params.find(_pk_zlength) != params.end()) { error("param_missing", "zlength"); }
  if (params.find(_pk_ylength) == params.end() && params.find(_pk_xlength) != params.end()) { error("param_missing", "xlength"); }
  if (params.find(_pk_ylength) == params.end() && params.find(_pk_zlength) != params.end()) { error("param_missing", "zlength"); }
  if (params.find(_pk_zlength) == params.end() && params.find(_pk_xlength) != params.end()) { error("param_missing", "xlength"); }
  if (params.find(_pk_zlength) == params.end() && params.find(_pk_ylength) != params.end()) { error("param_missing", "ylength"); }
  if (params.find(_pk_xradius) == params.end() && params.find(_pk_yradius) != params.end()) { error("param_missing", "yradius"); }
  if (params.find(_pk_xradius) == params.end() && params.find(_pk_zradius) != params.end()) { error("param_missing", "zradius"); }
  if (params.find(_pk_yradius) == params.end() && params.find(_pk_xradius) != params.end()) { error("param_missing", "xradius"); }
  if (params.find(_pk_yradius) == params.end() && params.find(_pk_zradius) != params.end()) { error("param_missing", "zradius"); }
  if (params.find(_pk_zradius) == params.end() && params.find(_pk_xradius) != params.end()) { error("param_missing", "xradius"); }
  if (params.find(_pk_zradius) == params.end() && params.find(_pk_yradius) != params.end()) { error("param_missing", "yradius"); }

  isAxis_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_xlength) != params.end())
  {
    params.erase(_pk_xlength);
    params.erase(_pk_ylength);
    params.erase(_pk_zlength);
    isAxis_ = false;
  }
  if (params.find(_pk_xradius) != params.end())
  {
    params.erase(_pk_xradius);
    params.erase(_pk_yradius);
    params.erase(_pk_zradius);
    isAxis_ = false;
  }
  if (params.find(_pk_v1) != params.end())
  {
    params.erase(_pk_v1);
    params.erase(_pk_v2);
    params.erase(_pk_v6);
    isAxis_ = true;
  }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // p_ is not (fully) built so we do
  buildPVNHAndBBox();

  computeMB();
  trace_p->pop();
}

void Ellipsoid::buildParam(const Parameter& p)
{
  trace_p->push("Ellipsoid::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_center:
    {
      switch (p.type())
      {
        case _pt: p_[0] = p.get_pt(); break;
        case _integer: p_[0] = Point(real_t(p.get_i())); break;
        case _real: p_[0] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v1:
    {
      switch (p.type())
      {
        case _pt: p_[1] = p.get_pt(); break;
        case _integer: p_[1] = Point(real_t(p.get_i())); break;
        case _real: p_[1] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v2:
    {
      switch (p.type())
      {
        case _pt: p_[2] = p.get_pt(); break;
        case _integer: p_[2] = Point(real_t(p.get_i())); break;
        case _real: p_[2] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_v6:
    {
      switch (p.type())
      {
        case _pt: p_[6] = p.get_pt(); break;
        case _integer: p_[6] = Point(real_t(p.get_i())); break;
        case _real: p_[6] = Point(p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xlength:
    {
      switch (p.type())
      {
        case _integer: xradius_ = 0.5 * real_t(p.get_n()); break;
        case _real: xradius_ = 0.5 * p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_ylength:
    {
      switch (p.type())
      {
        case _integer: yradius_ = 0.5 * real_t(p.get_n()); break;
        case _real: yradius_ = 0.5 * p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_zlength:
    {
      switch (p.type())
      {
        case _integer: zradius_ = 0.5 * real_t(p.get_n()); break;
        case _real: zradius_ = 0.5 * p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_xradius:
    {
      switch (p.type())
      {
        case _integer: xradius_ = real_t(p.get_n()); break;
        case _real: xradius_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_yradius:
    {
      switch (p.type())
      {
        case _integer: yradius_ = real_t(p.get_n()); break;
        case _real: yradius_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_zradius:
    {
      switch (p.type())
      {
        case _integer: zradius_ = real_t(p.get_n()); break;
        case _real: zradius_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_type:
    {
      switch (p.type())
      {
        case _integer: type_ = dimen_t(p.get_n()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nnodes:
    {
      switch (p.type())
      {
        case _integerVector:
        {
          std::vector<number_t> n = p.get_nv();
          n_.resize(n.size());
          for (number_t i = 0; i < n.size(); ++i) { n_[i] = n[i] > 2 ? n[i] : 2; }
          break;
        }
        case _integer: n_ = std::vector<number_t>(1, p.get_n() > 2 ? p.get_n() : 2); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_hsteps:
    {
      switch (p.type())
      {
        case _realVector: h_ = p.get_rv(); break;
        case _integer: h_ = std::vector<real_t>(1, real_t(p.get_n())); break;
        case _real: h_ = std::vector<real_t>(1, p.get_r()); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    case _pk_nboctants:
    {
      switch (p.type())
      {
        case _integer: nboctants_ = p.get_n(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Volume::buildParam(p); break;
  }
  trace_p->pop();
}

void Ellipsoid::buildDefaultParam(ParameterKey key)
{
  trace_p->push("Ellipsoid::buildDefaultParam");
  switch (key)
  {
    case _pk_nnodes: n_ = std::vector<number_t>(12, 2); break;
    case _pk_type: type_ = 1; break;
    case _pk_nboctants: nboctants_ = 0; break;
    default: Volume::buildDefaultParam(key); break;
  }
  trace_p->pop();
}

std::set<ParameterKey> Ellipsoid::getParamsKeys()
{
  std::set<ParameterKey> params = Volume::getParamsKeys();
  params.insert(_pk_center);
  params.insert(_pk_v1);
  params.insert(_pk_v2);
  params.insert(_pk_v6);
  params.insert(_pk_xlength);
  params.insert(_pk_ylength);
  params.insert(_pk_zlength);
  params.insert(_pk_xradius);
  params.insert(_pk_yradius);
  params.insert(_pk_zradius);
  params.insert(_pk_nnodes);
  params.insert(_pk_hsteps);
  params.insert(_pk_nboctants);
  params.insert(_pk_type);
  return params;
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

Ellipsoid::Ellipsoid(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : Volume()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

Ellipsoid::Ellipsoid(const Ellipsoid& e) : Volume(e), v_(e.v_), xradius_(e.xradius_), yradius_(e.yradius_), zradius_(e.zradius_),
  isAxis_(e.isAxis_), nboctants_(e.nboctants_), type_(e.type_)
{}

number_t Ellipsoid::nbSubdiv() const
{
  number_t n = n_[0];
  for (number_t i = 1; i < n_.size(); ++i)
  {
    if (n_[i] > n) { n = n_[i]; }
  }
  return static_cast<number_t>(theTolerance + std::log(n - 1) / std::log(2));
}

bool Ellipsoid::isFull() const
{
  return (nboctants_ == 1 || nboctants_ == 8);
}

bool Ellipsoid::isBall() const
{
  if (dynamic_cast<const Ball*>(this) != nullptr) { return true; } //real ball
  return (xradius_ == yradius_ &&  xradius_ == zradius_ &&
          std::abs(dot(p_[1] - p_[0], p_[2] - p_[0])) < theTolerance &&
          std::abs(dot(p_[1] - p_[0], p_[3] - p_[0])) < theTolerance &&
          std::abs(dot(p_[2] - p_[0], p_[3] - p_[0])) < theTolerance);
}

string_t Ellipsoid::asString() const
{
  string_t s("Ellipsoid (center = ");
  s += p_[0].roundToZero().toString() + ", ";
  s += "1st apogee = " + p_[1].roundToZero().toString() + ", 2nd apogee = " + p_[2].roundToZero().toString() + ", 3rd apogee = " + p_[6].roundToZero().toString() + ")";
  return s;
}

std::vector<const Point*> Ellipsoid::boundNodes() const
{
  std::vector<const Point*> nodes;
  if (nboctants_ == 8 || nboctants_ == 0)
  {
    nodes.resize(6);
    for (number_t i = 0; i < 6; ++i) { nodes[i] = &p_[i + 1]; }
  }
  else
  {
    nodes.resize(v_.size());
    for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  }
  return nodes;
}

std::vector<Point*> Ellipsoid::nodes()
{
  std::vector<Point*> nodes(v_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  return nodes;
}

std::vector<Point*> Ellipsoid::wholeNodes()
{
  std::vector<Point*> nodes(v_.size() + p_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  for (number_t i = v_.size(); i < nodes.size(); ++i) { nodes[i] = &p_[i - v_.size()]; }
  return nodes;
}

std::vector<const Point*> Ellipsoid::wholeNodes() const
{
  std::vector<const Point*> nodes(v_.size() + p_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  for (number_t i = v_.size(); i < nodes.size(); ++i) { nodes[i] = &p_[i - v_.size()]; }
  return nodes;
}

std::vector<const Point*> Ellipsoid::nodes() const
{
  std::vector<const Point*> nodes(v_.size());
  for (number_t i = 0; i < v_.size(); ++i) { nodes[i] = &v_[i]; }
  return nodes;
}

//! returns list of curve vertices
std::vector<std::pair<ShapeType, std::vector<const Point*> > > Ellipsoid::curves() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves;
  std::vector<const Point*> vertices(2);
  if (nboctants_ == 8 || nboctants_ == 0)
  {
    curves.resize(12);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[3];
    curves[5] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[5];
    curves[6] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[1];
    curves[7] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[8] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[4];
    curves[9] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[5];
    curves[10] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[2];
    curves[11] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ == 7)
  {
    curves.resize(15);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[3];
    curves[5] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[5];
    curves[6] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[1];
    curves[7] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[8] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[4];
    curves[9] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[5];
    curves[10] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[2];
    curves[11] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1];
    curves[12] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4];
    curves[13] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[5];
    curves[14] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ == 6)
  {
    curves.resize(15);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[3];
    curves[5] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[5];
    curves[6] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[1];
    curves[7] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[8] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[4];
    curves[9] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[2];
    curves[10] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1];
    curves[11] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3];
    curves[12] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4];
    curves[13] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[5];
    curves[14] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ == 5)
  {
    curves.resize(15);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[3];
    curves[5] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[1];
    curves[6] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[7] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[4];
    curves[8] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[5]; vertices[1] = &p_[2];
    curves[9] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1];
    curves[10] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3];
    curves[11] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2];
    curves[12] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4];
    curves[13] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[5];
    curves[14] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ == 4)
  {
    curves.resize(12);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[3];
    curves[5] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[6] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[4];
    curves[7] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1];
    curves[8] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3];
    curves[9] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2];
    curves[10] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4];
    curves[11] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ == 3)
  {
    curves.resize(12);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[3];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[5] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[4];
    curves[6] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1];
    curves[7] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3];
    curves[8] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2];
    curves[9] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4];
    curves[10] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[6];
    curves[11] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ == 2)
  {
    curves.resize(9);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[6]; vertices[1] = &p_[3];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1];
    curves[5] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3];
    curves[6] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2];
    curves[7] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[6];
    curves[8] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ == 1)
  {
    curves.resize(6);
    vertices[0] = &p_[1]; vertices[1] = &p_[2];
    curves[0] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[6];
    curves[1] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[6];
    curves[2] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1];
    curves[3] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2];
    curves[4] = std::make_pair(_ellArc, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[6];
    curves[5] = std::make_pair(_ellArc, vertices);
  }
  else if (nboctants_ > 8) { error("is_greater", nboctants_, 8); }
  else { error("is_lesser", nboctants_, 0); }

  return curves;
}

//! returns list of face vertices
std::vector<std::pair<ShapeType, std::vector<const Point*> > > Ellipsoid::surfs() const
{
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs;
  std::vector<const Point*> vertices(3);
  if (nboctants_ == 8 || nboctants_ == 0)
  {
    surfs.resize(8);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[6];
    surfs[1] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[6];
    surfs[2] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1]; vertices[2] = &p_[6];
    surfs[3] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[5];
    surfs[4] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[5];
    surfs[5] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[5];
    surfs[6] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1]; vertices[2] = &p_[5];
    surfs[7] = std::make_pair(_ellipsoidSidePart, vertices);
  }
  else if (nboctants_ == 7)
  {
    surfs.resize(10);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[6];
    surfs[1] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[6];
    surfs[2] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1]; vertices[2] = &p_[6];
    surfs[3] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[5];
    surfs[4] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[5];
    surfs[5] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[5];
    surfs[6] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4]; vertices[2] = &p_[5];
    surfs[7] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[5]; vertices[2] = &p_[1];
    surfs[8] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[4];
    surfs[9] = std::make_pair(_ellipticSector, vertices);
  }
  else if (nboctants_ == 6)
  {
    surfs.resize(10);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[6];
    surfs[1] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[6];
    surfs[2] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1]; vertices[2] = &p_[6];
    surfs[3] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[5];
    surfs[4] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[5];
    surfs[5] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3]; vertices[2] = &p_[5];
    surfs[6] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[5]; vertices[2] = &p_[1];
    surfs[7] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4]; vertices[2] = &p_[3];
    surfs[8] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[4];
    surfs[9] = std::make_pair(_ellipticSector, vertices);
  }
  else if (nboctants_ == 5)
  {
    surfs.resize(10);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[6];
    surfs[1] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[6];
    surfs[2] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1]; vertices[2] = &p_[6];
    surfs[3] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[5];
    surfs[4] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2]; vertices[2] = &p_[5];
    surfs[5] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[5]; vertices[2] = &p_[1];
    surfs[6] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3]; vertices[2] = &p_[2];
    surfs[7] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4]; vertices[2] = &p_[3];
    surfs[8] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[4];
    surfs[9] = std::make_pair(_ellipticSector, vertices);
  }
  else if (nboctants_ == 4)
  {
    surfs.resize(8);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[6];
    surfs[1] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[6];
    surfs[2] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[4]; vertices[1] = &p_[1]; vertices[2] = &p_[6];
    surfs[3] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2]; vertices[2] = &p_[1];
    surfs[4] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3]; vertices[2] = &p_[2];
    surfs[5] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4]; vertices[2] = &p_[3];
    surfs[6] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[1]; vertices[2] = &p_[4];
    surfs[7] = std::make_pair(_ellipticSector, vertices);
  }
  else if (nboctants_ == 3)
  {
    surfs.resize(6);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[6];
    surfs[1] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[3]; vertices[1] = &p_[4]; vertices[2] = &p_[6];
    surfs[2] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2]; vertices[2] = &p_[1];
    surfs[3] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3]; vertices[2] = &p_[2];
    surfs[4] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[4]; vertices[2] = &p_[3];
    surfs[5] = std::make_pair(_ellipticSector, vertices);
  }
  else if (nboctants_ == 2)
  {
    surfs.resize(4);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[2]; vertices[1] = &p_[3]; vertices[2] = &p_[6];
    surfs[1] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2]; vertices[2] = &p_[1];
    surfs[2] = std::make_pair(_ellipticSector, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[3]; vertices[2] = &p_[2];
    surfs[3] = std::make_pair(_ellipticSector, vertices);
  }
  else if (nboctants_ == 1)
  {
    surfs.resize(2);
    vertices[0] = &p_[1]; vertices[1] = &p_[2]; vertices[2] = &p_[6];
    surfs[0] = std::make_pair(_ellipsoidSidePart, vertices);
    vertices[0] = &p_[0]; vertices[1] = &p_[2]; vertices[2] = &p_[1];
    surfs[1] = std::make_pair(_ellipticSector, vertices);
  }
  else if (nboctants_ > 8) { error("is_greater", nboctants_, 8); }
  else { error("is_lesser", nboctants_, 0); }
  return surfs;
}

real_t Ellipsoid::measure() const
{
  number_t nboct = nboctants_;
  if (nboctants_ == 0) { nboct = 8; }
  return pi_ * xradius_ * yradius_ * zradius_ / 6.*nboct;
}

//! computes the minimal box
void Ellipsoid::computeMB()
{
  if (nboctants_ == 1) { minimalBox = MinimalBox(p_[0], p_[1], p_[2], p_[6]); }
  else if (nboctants_ == 2) { minimalBox = MinimalBox(p_[3], p_[1], p_[3] + p_[2] - p_[0], p_[6] + p_[1] - p_[0]); }
  else if (nboctants_ < 5 && nboctants_ != 0)
  {
    number_t vn = v_.size();
    minimalBox = MinimalBox(p_[0] + p_[3] - p_[2], p_[0] + p_[1] - p_[2], p_[0] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]);
  }
  else { minimalBox = MinimalBox(p_[5] + p_[3] - p_[2], p_[5] + p_[1] - p_[2], p_[5] + p_[2] - p_[1], p_[6] + p_[3] - p_[2]); }
}

//! create boundary geometry of ellipsoid as a composite (union of faces)
Geometry& Ellipsoid::buildBoundary() const
{
  Point C = p_[0], A1 = p_[1], A2 = p_[2], A3 = p_[6];
  Ellipsoid el(_center = C, _v1 = A1, _v2 = A2, _v6 = A3);
  string_t fname = "";
  if (sideNames_.size() == 1) { fname = sideNames_[0]; }
  number_t ns = 0;
  for (number_t i = 0; i < sideNames_.size(); i++)
    if (sideNames_[i] != "") { ns++; }
  if ((nboctants_ < 1 || nboctants_ >= 8) && ns <= 1) // full EllipsoidSidePart
  {
    EllipsoidSidePart* e = new EllipsoidSidePart(el, -pi_, pi_, -pi_ / 2, pi_ / 2);
    if (fname == "") { fname = domName_ + "_boundary"; }
    e->domName(fname);
    if (h().size() > 0) { e->h() = h(); }
    else { e->n() = n(); }
    boundaryGeometry_ = e;
    return *boundaryGeometry_;
  }

  boundaryGeometry_ = new Geometry(boundingBox, 2);
  Geometry& g = *boundaryGeometry_;
  g.shape(_composite);
  g.minimalBox = minimalBox;
  string_t name = "";
  if (domName_ != "") { name = domName_ + "_boundary"; }
  if (name != "") { g.domName(name); }
  std::map<number_t, Geometry*>& cps = g.components();
  std::map<number_t, std::vector<number_t> >& geos = g.geometries();
  std::vector<std::pair<ShapeType, std::vector<const Point*> > > ss = surfs();
  for (number_t i = 0; i < ss.size(); i++)
  {
    if (sideNames_.size() > i) { fname = sideNames_[i]; }
    std::vector<const Point*>& vs = ss[i].second;
    ShapeType sh = ss[i].first;
    switch (sh)
    {
      case _ellipsoidSidePart:
      {
        Point S1 = toEllipticCoordinates(*vs[0], C, A1, A2, A3),
              S2 = toEllipticCoordinates(*vs[1], C, A1, A2, A3),
              S3 = toEllipticCoordinates(*vs[2], C, A1, A2, A3); //third apogee or perigee
        real_t tm = std::min(S1[1], S2[1]), tM = std::max(S1[1], S2[1]);
        real_t pm = std::min(0., S3[2]), pM = std::max(0., S3[2]);
        cps[i] = new EllipsoidSidePart(el, tm, tM, pm, pM);
        cps[i]->domName(fname);
        if (h().size() > 0) { cps[i]->setHstep(h()[0]); }
        else { cps[i]->setNnodes(n()[0]); }
      }
      break;
      case _ellipticSector:
      {
        cps[i] = new Ellipse(_center = *vs[0], _v1 = *vs[1], _v2 = *vs[2], _angle1 = 0, _angle2 = pi_ / 2, _domain_name = fname);
        if (h().size() > 0) { cps[i]->setHstep(h()[0]); }
        else { cps[i]->setNnodes(n()[0]); }
      }
      break;
      default:
        break;
    }
    geos[i].push_back(i);
  }
  return g;
}


//=======================================================================================================
//                                  Ball member functions
//=======================================================================================================
//! default is ball of center (0,0,0) and radius 1
Ball::Ball() : Ellipsoid()
{ shape_ = _ball; }

void Ball::build(const std::vector<Parameter>& ps)
{
  trace_p->push("Ball::build");
  shape_ = _ball;
  std::set<ParameterKey> params = getParamsKeys(), usedParams;

  // managing params
  for (number_t i = 0; i < ps.size(); ++i)
  {
    ParameterKey key = ps[i].key();
    buildParam(ps[i]);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end())
      { error("geom_unexpected_param_key", words("param key", key), words("shape", _ball)); }
      else { warning("param_already_used", words("param key", key)); }
    }
    usedParams.insert(key);
    // user must use nnodes or hsteps, not both
    if (key == _pk_hsteps && usedParams.find(_pk_nnodes) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_nnodes)); }
    if (key == _pk_nnodes && usedParams.find(_pk_hsteps) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_hsteps)); }
    // user must side_names or face_names, not both
    if (key == _pk_face_names && usedParams.find(_pk_side_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_side_names)); }
    if (key == _pk_side_names && usedParams.find(_pk_face_names) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_face_names)); }

    // user must use only one of the following combination: (center,v1,v2) or (center,xlength,ylength)
    if ((key == _pk_v1 || key == _pk_v2 || key == _pk_v6) && usedParams.find(_pk_radius) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_radius)); }
    if ((key == _pk_radius) && usedParams.find(_pk_v1) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v1)); }
    if ((key == _pk_radius) && usedParams.find(_pk_v2) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v2)); }
    if ((key == _pk_radius) && usedParams.find(_pk_v6) != usedParams.end())
    { error("param_conflict", words("param key", key), words("param key", _pk_v6)); }
  }

  // if hsteps is not used, nnodes is used instead and there is no default value
  if (params.find(_pk_hsteps) != params.end()) { params.erase(_pk_hsteps); }

  // only one among _side_names and _face_names is used, so we remove the unused one
  if (usedParams.find(_pk_side_names) != usedParams.end()) { params.erase(_pk_face_names); }
  if (usedParams.find(_pk_face_names) != usedParams.end()) { params.erase(_pk_side_names); }
  
  // (center,v1,v2) or (center,xlength,ylength) has to be set (no default values)
  if (params.find(_pk_center) != params.end()) { error("param_missing", "center"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }
  if (params.find(_pk_v1) == params.end() && params.find(_pk_v6) != params.end()) { error("param_missing", "v6"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v2) == params.end() && params.find(_pk_v6) != params.end()) { error("param_missing", "v6"); }
  if (params.find(_pk_v6) == params.end() && params.find(_pk_v1) != params.end()) { error("param_missing", "v1"); }
  if (params.find(_pk_v6) == params.end() && params.find(_pk_v2) != params.end()) { error("param_missing", "v2"); }

  isAxis_ = true;
  // now, we clean unwanted keys
  if (params.find(_pk_radius) != params.end()) { params.erase(_pk_radius); isAxis_ = false; }
  if (params.find(_pk_v1) != params.end()) { params.erase(_pk_v1); params.erase(_pk_v2); params.erase(_pk_v6); }

  std::set<ParameterKey>::const_iterator it_p;
  for (it_p = params.begin(); it_p != params.end(); ++it_p) { buildDefaultParam(*it_p); }

  // p_ is not (fully) built so we do
  buildPVNHAndBBox();

  if ((std::abs(p_[0].distance(p_[1]) - p_[0].distance(p_[2])) > theTolerance) ||
      (std::abs(p_[0].distance(p_[1]) - p_[0].distance(p_[6])) > theTolerance) ||
      (std::abs(p_[0].distance(p_[1]) - p_[0].distance(p_[6])) > theTolerance))
  { error("geometry_incoherent_points", words("shape", _ball)); }

  computeMB();
  trace_p->pop();
}

void Ball::buildParam(const Parameter& p)
{
  trace_p->push("Ball::buildParam");
  ParameterKey key = p.key();
  switch (key)
  {
    case _pk_radius:
    {
      switch (p.type())
      {
        case _integer: xradius_ = yradius_ = zradius_ = real_t(p.get_i()); break;
        case _real: xradius_ = yradius_ = zradius_ = p.get_r(); break;
        default: error("param_badtype", words("value", p.type()), words("param key", key));
      }
      break;
    }
    default: Ellipsoid::buildParam(p); break;
  }
  trace_p->pop();
}

void Ball::buildDefaultParam(ParameterKey key)
{
  Ellipsoid::buildDefaultParam(key);
}

std::set<ParameterKey> Ball::getParamsKeys()
{
  std::set<ParameterKey> params = Ellipsoid::getParamsKeys();
  params.insert(_pk_radius);
  // xlength, ylength, zlength, xradius, yradius and zradius are replaced by radius
  params.erase(_pk_xlength);
  params.erase(_pk_ylength);
  params.erase(_pk_zlength);
  params.erase(_pk_xradius);
  params.erase(_pk_yradius);
  params.erase(_pk_zradius);
  return params;
}

Ball::Ball(Parameter p1, Parameter p2) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
  build(ps);
}

Ball::Ball(Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6, Parameter p7, Parameter p8, Parameter p9, Parameter p10, Parameter p11, Parameter p12) : Ellipsoid()
{
  std::vector<Parameter> ps={p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12};
  build(ps);
}

Ball::Ball(const Ball& b) : Ellipsoid(b) {}

//! Format Ball as string: "Ball (center = (.,.,.), radius = R)"
string_t Ball::asString() const
{
  string_t s("Ball (center = ");
  s += p_[0].roundToZero().toString() + ", ";
  s += "1st point = " + p_[1].roundToZero().toString() + ", 2nd point = " + p_[2].roundToZero().toString() + ", 3rd point = " + p_[6].roundToZero().toString() + ")";
  return s;
}

/*! parametrization of ball (spherical coordinates)
    u,v,w in [0,1]x[0,1]x[0,1], t=pi*(2u-1), l=pi*(v-0.5), r=w*R
    x = r*cos(t)*cos(l), y = r*sin(t)*cos(l), z = r*sin(l)
*/
Vector<real_t> Ball::funParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const
{
  real_t R=radius();
  real_t r = pt[2]*R, t = pi_*(2*pt[0]-1), l=pi_*(pt[1]-0.5),
        ct = std::cos(t), st = std::sin(t), cl=std::cos(l), sl=std::sin(l);
  Vector<real_t> P(3);
  switch (dif)
  {
    case _id:
      P[0]=r*ct*cl; P[1]=r*st*cl; P[2]=r*sl;
      break;
    case _d1:
      P[0]=-2*pi_*r*st*cl; P[1]=2*pi_*r*ct*cl; P[2]=0;
      break;
    case _d2:
      P[0]=-pi_*r*ct*sl; P[1]=-pi_*r*st*sl; P[2]=0;
      break;
    case _d3:
      P[0]=R*ct*cl; P[1]=R*st*cl; P[2]=R*sl;
      break;
    case _d11:
      P[0]=-4*pi_*pi_*r*ct*cl; P[1]=-4*pi_*pi_*r*st*cl; P[2]=0;
    case _d21:
    case _d12:
      P[0]=2*pi_*pi_*r*st*sl; P[1]=-2*pi_*pi_*r*ct*sl; P[2]=0;
      break;
    case _d31:
    case _d13:
      P[0]=-2*pi_*R*st*cl; P[1]=2*pi_*R*ct*cl; P[2]=0;
    case _d22:
      P[0]=-pi_*pi_*r*ct*cl; P[1]=-pi_*pi_*r*st*cl; P[2]=0;
      break;
    case _d32:
    case _d23:
      P[0]=-pi_*R*ct*sl; P[1]=-pi_*R*st*sl; P[2]=0;
      break;
    case _d33:
      P[0]=0; P[1]=0; P[2]=0;
      break;
    default: parfun_error("Ellipse parametrization", dif);
  }
  return P;
}

//! inverse of parametrization (u,v)=invf(pt)
Vector<real_t> Ball::invParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const
{
  if (dif != _id) { parfun_error("Ellipse::invParametrization", dif); }
  Vector<real_t> uvw(3);
  real_t R=radius();
  Point p0p = pt - p_[0]; // p_[0] center
  real_t r=norm2(p0p);
  uvw[2]=r/R;
  if(uvw[2]>1+theTolerance) { parfun_error("Ellipse::invParametrization", dif); }
  uvw[2]=std::min(uvw[2],1.);
  uvw[1]=0.5+std::asin(pt[2]/r)/pi_;
  uvw[0]=0.5*(1+std::atan2(pt[1],pt[0])/pi_);
  return uvw;
}

/*! parametrization of ball boundary (spherical coordinates)
    u,v in [0,1]x[0,1]x[0,1], t=pi*(2u-1), l=pi*(v-0.5)
    x = R*cos(t)*cos(l), y = R*sin(t)*cos(l), z = R*sin(l)
*/
Vector<real_t> Ball::funBoundaryParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const
{
  real_t R=radius();
  real_t t = pi_*(2*pt[0]-1), l=pi_*(pt[1]-0.5),
         ct = std::cos(t), st = std::sin(t), cl=std::cos(l), sl=std::sin(l);
  Vector<real_t> P(3);
  switch (dif)
  {
    case _id:
      P[0]=R*ct*cl; P[1]=R*st*cl; P[2]=R*sl;
      break;
    case _d1:
      P[0]=-2*pi_*R*st*cl; P[1]=2*pi_*R*ct*cl; P[2]=0;
      break;
    case _d2:
      P[0]=-pi_*R*ct*sl; P[1]=-pi_*R*st*sl; P[2]=0;
      break;
    case _d11:
      P[0]=-4*pi_*pi_*R*ct*cl; P[1]=-4*pi_*pi_*R*st*cl; P[2]=0;
    case _d21:
    case _d12:
      P[0]=2*pi_*pi_*R*st*sl; P[1]=-2*pi_*pi_*R*ct*sl; P[2]=0;
      break;
    case _d22:
      P[0]=-pi_*pi_*R*ct*cl; P[1]=-pi_*pi_*R*st*cl; P[2]=0;
      break;
    default: parfun_error("Ellipse parametrization", dif);
  }
  return P;
}

//! inverse of parametrization (u,v)=invbf(pt)
Vector<real_t> Ball::invBoundaryParametrization(const Point& pt, Parameters& pars, DiffOpType dif) const
{
  if (dif != _id) { parfun_error("Ellipse::invBoundaryParametrization", dif); }
  Vector<real_t> uv(2);
  real_t R=radius();
  Point p0p = pt - p_[0]; // p_[0] center
  real_t r=norm2(p0p);
  if(std::abs(r-R)>theTolerance*R) { parfun_error("Ellipse::invBoundaryParametrization", dif); }
  uv[1]=0.5+std::asin(pt[2]/r)/pi_;
  uv[0]=0.5*(1+std::atan2(pt[1],pt[0])/pi_);
  return uv;
}

} // end of namespace xlifepp
