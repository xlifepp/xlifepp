/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Geometry.hpp
  \authors D. Martin, E. Lunéville, N. Kielbasiewicz
  \since 10 dec 2007
  \date 17 oct 2014

  \brief Definition of the xlifepp::Geometry class and childs

  xlifepp::Geometry class represents geometric data of the "physical" problem
  It provides the following minimum informations:
  - the physical space dimension
  - the names of space variables (for documentation purpose)
  - a bounding box
  - a minimal box (a sort of box convex hull)
  - parametrization of the geometry and of its boundary, both are not mandatory available:
      by default: 1D geometries have a parametrization while 2D/3D geometries have a boundary parametrization
  - some additional data useful for building complex geometries (loop, holes,...)
*/

#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

#include "config.h"
#include "utils.h"
#include "mathsResources.h"
#include "Parametrization.hpp"

#ifdef XLIFEPP_WITH_OPENCASCADE
#include <TopoDS_Shape.hxx>
#include <TDocStd_Document.hxx>
#endif

namespace xlifepp
{

int_t shapeDim(ShapeType sh); //!< dimension of a shape type
void buildNameAndSuffixTransformParams(std::vector<Parameter>& ps, string_t& name, string_t& suffix);

class GeometricGeodesic;
/*!
   \class BoundingBox
  utility class to describe a bounding box
 */
class BoundingBox
{
  private:
    std::vector<RealPair> bounds_; //!< Bounding values of the box [xmin,xmax]x[ymin,ymax]x[zmin,zmax]

  public:
    BoundingBox() { bounds_.resize(0); }                                 //!< void constructor
    BoundingBox(const std::vector<RealPair>& bds) : bounds_(bds) {}      //!< basic constructor
    BoundingBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax, real_t zmin, real_t zmax); //!< basic constructor 3D box
    BoundingBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax); //!< basic constructor 2D box
    BoundingBox(real_t xmin, real_t xmax); //!< basic constructor 1D box
    BoundingBox(const Point& p0, const Point& p1, const Point& p2, const Point& p3); //!< general constructor 3D box
    BoundingBox(const Point& p0, const Point& p1, const Point& p2); //!< general constructor 2D box
    BoundingBox(const Point& p0, const Point& p1); //!< general constructor 1D box
    BoundingBox(const std::vector<Point>& vp); //!< genereal constructor from a set of points
    dimen_t dim() const { return bounds_.size(); }           //!< dimension of the bounding box
    std::vector<RealPair> bounds() const { return bounds_; } //!< return bound in all direction
    RealPair bounds(dimen_t i) const;                        //!< return box bounds in direction i (from 1 to dim)
    RealPair& bounds(dimen_t i) {return bounds_[i - 1];}     //!< return box bounds in direction i (from 1 to dim), non const
    RealPair xbounds() const { return bounds(1); }           //!< return box bounds in first direction, if exists
    RealPair ybounds() const { return bounds(2); }           //!< return box bounds in second direction, if exists
    RealPair zbounds() const { return bounds(3); }           //!< return box bounds in third direction, if exists
    Point minPoint() const;                                  //!< return the min point (left,bottom,front)
    Point maxPoint() const;                                  //!< return the max point (right,top,back)
    std::vector<Point> points();                             //!< return the vertices of the bounding box
    real_t diameter() const;                                 //!< return the max of (xmax-xmin, ymax -ymin, zmax-zmin);
    real_t diameter2() const;                                //!< return sqrt((xmax-xmin)^2+(ymax -ymin)^2+ (zmax-zmin)^2)
    BoundingBox& operator +=(const BoundingBox& bb);         //!< merge a bounding box to the current one
    BoundingBox& operator ^=(const BoundingBox& bb);         //!< intersect a bounding box to the current one (may be void)
    void print(std::ostream& os) const;                      //!< print BoundingBox
    void print(PrintStream& os) const { print(os.currentStream()); }
    string_t asString() const;                               //!< format as string: [a,b]x[c,d]x[e,f]

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a BoundingBox
    BoundingBox& transform(const Transformation& t);
    //! apply a translation on a BoundingBox (1 key)
    BoundingBox& translate(const Parameter& p1);
    //! apply a translation on a BoundingBox (vector version)
    BoundingBox& translate(std::vector<real_t> u = std::vector<real_t>(3, 0.));
    //! apply a translation on a BoundingBox (3 reals version)
    BoundingBox& translate(real_t ux, real_t uy = 0., real_t uz = 0.);
    //! apply a rotation on a BoundingBox (1 key)
    BoundingBox& rotate2d(const Parameter& p1);
    //! apply a rotation on a BoundingBox (2 keys)
    BoundingBox& rotate2d(const Parameter& p1, const Parameter& p2);
    //! apply a rotation on a BoundingBox
    BoundingBox& rotate2d(const Point& c, real_t angle = 0.);
    //! apply a rotation on a BoundingBox (1 key)
    BoundingBox& rotate3d(const Parameter& p1);
    //! apply a rotation on a BoundingBox (2 keys)
    BoundingBox& rotate3d(const Parameter& p1, const Parameter& p2);
    //! apply a rotation on a BoundingBox (3 keys)
    BoundingBox& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3);
    //! apply a rotation on a BoundingBox
    BoundingBox& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.);
    //! apply a rotation on a BoundingBox
    BoundingBox& rotate3d(real_t dx, real_t dy, real_t angle);
    //! apply a rotation on a BoundingBox
    BoundingBox& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle);
    //! apply a rotation on a BoundingBox
    BoundingBox& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle);
    //! apply a rotation on a BoundingBox
    BoundingBox& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle);
    //! apply a homothety on a BoundingBox (1 key)
    BoundingBox& homothetize(const Parameter& p1);
    //! apply a homothety on a BoundingBox (2 keys)
    BoundingBox& homothetize(const Parameter& p1, const Parameter& p2);
    //! apply a homothety on a BoundingBox
    BoundingBox& homothetize(const Point& c, real_t factor = 1.);
    //! apply a homothety on a BoundingBox
    BoundingBox& homothetize(real_t factor);
    //! apply a point reflection on a BoundingBox (1 key)
    BoundingBox& pointReflect(const Parameter& p1);
    //! apply a point reflection on a BoundingBox
    BoundingBox& pointReflect(const Point& c);
    //! apply a reflection2d on a BoundingBox (1 key)
    BoundingBox& reflect2d(const Parameter& p1);
    //! apply a reflection2d on a BoundingBox (2 keys)
    BoundingBox& reflect2d(const Parameter& p1, const Parameter& p2);
    //! apply a reflection2d on a BoundingBox
    BoundingBox& reflect2d(const Point& c, std::vector<real_t> d = std::vector<real_t>(2, 0.));
    //! apply a reflection2d on a BoundingBox
    BoundingBox& reflect2d(const Point& c, real_t dx, real_t dy = 0.);
    //! apply a reflection3d on a BoundingBox (1 key)
    BoundingBox& reflect3d(const Parameter& p1);
    //! apply a reflection3d on a BoundingBox (2 keys)
    BoundingBox& reflect3d(const Parameter& p1, const Parameter& p2);
    //! apply a reflection3d on a BoundingBox
    BoundingBox& reflect3d(const Point& c, std::vector<real_t> n = std::vector<real_t>(3, 0.));
    //! apply a reflection3d on a BoundingBox
    BoundingBox& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.);
};

std::ostream& operator<<(std::ostream& os, const BoundingBox& bb); //!< output BoundingBox
std::ostream& operator<<(std::ostream& os, const RealPair& rp);    //!< output pair of reals
real_t dist(const BoundingBox& bb1, const BoundingBox& bb2);       //!< distance from two bounding boxes

/*!
   \class MinimalBox
  utility class to describe a minimal box
  This is like the bounding box, but independant from the orientation.
 */
class MinimalBox
{
  protected:
    std::vector<Point> bounds_;              //!< Bounding values of the box [xmin,xmax]x[ymin,ymax]x[zmin,zmax]

  public:
    MinimalBox() { bounds_.resize(0); }                                 //!< void constructor
    MinimalBox(const std::vector<Point>& bds) : bounds_(bds) {}         //!< basic constructor
    MinimalBox(const std::vector<RealPair>&);                           //!< basic constructor
    MinimalBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax, real_t zmin, real_t zmax); //!< basic constructor 3D box
    MinimalBox(real_t xmin, real_t xmax, real_t ymin, real_t ymax); //!< basic constructor 2D box
    MinimalBox(real_t xmin, real_t xmax); //!< basic constructor 1D box
    MinimalBox(const Point& p0, const Point& p1, const Point& p2, const Point& p3); //!< general constructor 3D box
    MinimalBox(const Point& p0, const Point& p1, const Point& p2); //!< general constructor 2D box
    MinimalBox(const Point& p0, const Point& p1); //!< general constructor 1D box
    dimen_t dim() const;                                  //!< dimension of the bounding box
    std::vector<Point> bounds() const { return bounds_; } //!< return bounds_
    std::vector<Point> vertices() const;                  //!< returns vertices
    Point boundPt(dimen_t i) const;                       //!< return box bounds in direction i (from 1 to dim)
    Point minPoint() const { return bounds_[0]; }         //!< return the min point (left,bottom,front)
    Point maxPoint() const;                               //!< return the max point (right,top,back)
    void print(std::ostream& os) const;                   //!< print MinimalBox
    void print(PrintStream& os) const {print(os.currentStream());}
    string_t asString() const;                            //!< format as string: [a,b]x[c,d]x[e,f]

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a MinimalBox
    MinimalBox& transform(const Transformation& t);
    //! apply a translation on a MinimalBox (1 key)
    MinimalBox& translate(const Parameter& p1);
    //! apply a translation on a MinimalBox (vector version)
    MinimalBox& translate(std::vector<real_t> u);
    //! apply a translation on a MinimalBox (3 reals version)
    MinimalBox& translate(real_t ux, real_t uy = 0., real_t uz = 0.);
    //! apply a rotation on a MinimalBox (1 key)
    MinimalBox& rotate2d(const Parameter& p1);
    //! apply a rotation on a MinimalBox (2 keys)
    MinimalBox& rotate2d(const Parameter& p1, const Parameter& p2);
    //! apply a rotation on a MinimalBox
    MinimalBox& rotate2d(const Point& c, real_t angle = 0.);
    //! apply a rotation on a MinimalBox (1 key)
    MinimalBox& rotate3d(const Parameter& p1);
    //! apply a rotation on a MinimalBox (2 keys)
    MinimalBox& rotate3d(const Parameter& p1, const Parameter& p2);
    //! apply a rotation on a MinimalBox (3 keys)
    MinimalBox& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3);
    //! apply a rotation on a MinimalBox
    MinimalBox& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.);
    //! apply a rotation on a MinimalBox
    MinimalBox& rotate3d(real_t dx, real_t dy, real_t angle);
    //! apply a rotation on a MinimalBox
    MinimalBox& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle);
    //! apply a rotation on a MinimalBox
    MinimalBox& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle);
    //! apply a rotation on a MinimalBox
    MinimalBox& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle);
    //! apply a homothety on a MinimalBox (1 key)
    MinimalBox& homothetize(const Parameter& p1);
    //! apply a homothety on a MinimalBox (2 keys)
    MinimalBox& homothetize(const Parameter& p1, const Parameter& p2);
    //! apply a homothety on a MinimalBox
    MinimalBox& homothetize(const Point& c, real_t factor = 1.);
    //! apply a homothety on a MinimalBox
    MinimalBox& homothetize(real_t factor);
    //! apply a point reflection on a MinimalBox (1 key)
    MinimalBox& pointReflect(const Parameter& p1);
    //! apply a point reflection on a MinimalBox
    MinimalBox& pointReflect(const Point& c);
    //! apply a reflection 2D on a MinimalBox (1 key)
    MinimalBox& reflect2d(const Parameter& p1);
    //! apply a reflection 2D on a MinimalBox (2 keys)
    MinimalBox& reflect2d(const Parameter& p1, const Parameter& p2);
    //! apply a reflection 2D on a MinimalBox
    MinimalBox& reflect2d(const Point& c, std::vector<real_t> d = std::vector<real_t>(2, 0.));
    //! apply a reflection 2D on a MinimalBox
    MinimalBox& reflect2d(const Point& c, real_t dx, real_t dy = 0.);
    //! apply a reflection 3D on a MinimalBox (1 key)
    MinimalBox& reflect3d(const Parameter& p1);
    //! apply a reflection 3D on a MinimalBox (2 keys)
    MinimalBox& reflect3d(const Parameter& p1, const Parameter& p2);
    //! apply a reflection 3D on a MinimalBox
    MinimalBox& reflect3d(const Point& c, std::vector<real_t> n = std::vector<real_t>(3, 0.));
    //! apply a reflection 3D on a MinimalBox
    MinimalBox& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.);
};

std::ostream& operator<<(std::ostream& os, const MinimalBox& mb); //!< output MinimalBox

// forward declarations
class Curve;
class Surface;
class Volume;
class Segment;
class EllArc;
class CircArc;
class ParametrizedArc;
class SplineArc;
class Polygon;
class Triangle;
class Quadrangle;
class Parallelogram;
class Rectangle;
class SquareGeo;
class Ellipse;
class Disk;
class ParametrizedSurface;
class SplineSurface;
class SetOfElems;
class SetOfPoints;
class Polyhedron;
class Tetrahedron;
class Hexahedron;
class Parallelepiped;
class Cuboid;
class Cube;
class Ellipsoid;
class Ball;
class Trunk;
class Cylinder;
class Prism;
class Cone;
class Pyramid;
class RevTrunk;
class RevCylinder;
class RevCone;
class EllipsoidSidePart;
class TrunkSidePart;
class ExtrusionData;
class GeoNode;

#ifdef XLIFEPP_WITH_OPENCASCADE
class OCData;
typedef std::pair<TopoDS_Shape*, TDocStd_Document*> ShapeDocPair;
#endif


/*!
   \class Geometry
  handles geometric data of the physical problem
 */
class Geometry
{
  public:
    BoundingBox boundingBox;                    //!< the bounding box
    MinimalBox minimalBox;                      //!< the smallest box containing the geometry
    mutable bool force;                         //!< tag to force inclusion when geometry engines fails
    mutable bool crackable;                     //!< tag to define if the geometry should be cracked or not
    mutable CrackType crackType;                //!< type of crack (open or closed)
    mutable string_t crackDomNameToOpen;        //!< when open crack, name to the side domain to open.
    bool isPlaneSurface;                        //!< used only for 2D loop geometries
    static bool checkInclusion;                 //!< to manage inclusion checking in composite algorithms
  protected:
    string_t domName_;                          //!< geometry domain name
    dimen_t dim_;                               //!< physical space dimension
    ShapeType shape_;                           //!< geometrical shape
    std::vector<string_t> sideNames_;           //!< list of side domains' names
    std::vector<string_t> sideOfSideNames_;     //!< list of side of side domains' names
    std::vector<string_t> sideOfSideOfSideNames_; //!< list of side of side of side domains' names
    std::vector<string_t> theNamesOfVariables_; //!< names of variables (x,y,z), (r,theta)
    string_t teXFilename_;                      //!< name of the file containing the tex code drawing the geometry
  private:
    mutable std::map<number_t, Geometry*> components_;       //!< composite or loop geometry if not empty
    std::map<number_t, std::vector<number_t> > geometries_;  //!< composite geometry if not empty
    std::map<number_t, std::vector<number_t> > loops_;       //!< loop geometry if not empty
    mutable std::vector<std::vector<int_t> > connectedParts_;//!< list of components by connected parts (used by parametrization)
    mutable number_t nbParts_;                               //!< number of canonical parts in connectedParts structure
    mutable ExtrusionData* extrusionData_;                   //!< extrusion geometry if not 0
  protected:
    mutable Parametrization* parametrization_;               //!< pointer to parametrization if not 0
    mutable Parametrization* boundaryParametrization_;       //!< pointer to boundary parametrization if not 0
    mutable Geometry* boundaryGeometry_;                     //!< pointer to boundary geometry (0 by default)
    mutable Geometry* auxiliaryGeometry_;                    //!< pointer to auxiliary geometry (0 by default)

  public:
    GeoNode* geoNode_; //!< GeoNode pointer to handle tree representation of non canonical geometries

    Geometry();                                   //!< void constructor
    Geometry(const Geometry& g);                  //!< copy constructor
    explicit Geometry(const BoundingBox& bb, const string_t& na = "", ShapeType sh = _noShape,
                      const string_t& nx = "x", const string_t& ny = "y",
                      const string_t& nz = "z");          //!< basic constructor from bounding box
    Geometry(const BoundingBox& bb, dimen_t d, const string_t& na = "", ShapeType sh = _noShape,
             const string_t& nx = "x", const string_t& ny = "y",
             const string_t& nz = "z");          //!< basic constructor from bounding box and dim
    explicit Geometry(dimen_t d, const string_t& na = "", ShapeType sh = _noShape,
                      const string_t& nx = "x", const string_t& ny = "y",
                      const string_t& nz = "z");          //!< basic constructor from (unit bounding box)
    explicit Geometry(const string_t& fn);       //!< constructor from file (only Brep up to now)

  protected:
    void buildParam(const Parameter& p);
    void buildDefaultParam(ParameterKey key);
    std::set<ParameterKey> getParamsKeys();
    virtual void initParametrization()
    {error("free_error", "no initParametrization defined");}

  public:
    void init(const string_t& nx = "x", const string_t& ny = "y", const string_t& nz = "z");      //!< intialize OCData, varNames and check domName
    void initName(const string_t& nx = "x", const string_t& ny = "y", const string_t& nz = "z");  //!< intialize varNames and check domName
    virtual Geometry* clone() const { return new Geometry(*this); } //!< virtual copy constructor
    virtual ~Geometry();

    Geometry& operator=(const Geometry& g);   //!< assign operator
    bool operator==(const Geometry& g) const; //!< comparison operator

    dimen_t dim() const { return dim_; }                  //!< returns the physical space dimension
    void dim(dimen_t d) {dim_ = d; }                      //!< sets the physical space dimension
    dimen_t dimPoint() const;                             //!< dimension of points, may differ from dimension of the geometry
    ShapeType shape() const { return shape_; }            //!< returns the geometrical shape
    void shape(ShapeType sh) { shape_ = sh; }             //!< sets the geometrical shape
    const string_t& domName() const { return domName_; }  //!< get the domain name
    string_t& domName() { return domName_; }              //!< get the domain name (writable)
    void domName(const string_t& nm) { domName_ = nm; }   //!< set the domain name
    const string_t& teXFilename() const { return teXFilename_; } //!< returns tex file name
    void teXFilename(const string_t& fn) { teXFilename_ = fn; }  //!< set filename to get a TeX output (drawing of the mesh)
    bool isCanonical() const {return (shape_ > _fromFile && shape_ < _composite);}

    //@{
    //! returns the list of side domains' names
    const std::vector<string_t>& sideNames() const { return sideNames_; }
    std::vector<string_t>& sideNames() { return sideNames_; }
    //@}
    //@{
    //! returns the list of side of side domains' names
    const std::vector<string_t>& sideOfSideNames() const { return sideOfSideNames_; }
    std::vector<string_t>& sideOfSideNames() { return sideOfSideNames_; }
    //@}
    //@{
    //! returns the list of side of side of side domains' names
    const std::vector<string_t>& sideOfSideOfSideNames() const { return sideOfSideOfSideNames_; }
    std::vector<string_t>& sideOfSideOfSideNames() { return sideOfSideOfSideNames_; }
    //@}
    std::map<number_t, Geometry*>& components() { return components_;}             //!< accessor to components_
    const std::map<number_t, Geometry*>& components() const { return components_;} //!< accessor to components_ (const)
    const std::map<number_t, std::vector<number_t> >& geometries() const { return geometries_; } //!< accessor to geometries_ (const)
    std::map<number_t, std::vector<number_t> >& geometries() { return geometries_; }             //!< accessor to geometries_ (const)
    const std::map<number_t, std::vector<number_t> >& loops() const { return loops_; }           //!< accessor to loops_ (const)
    const std::vector<std::vector<int_t> >& connectedParts() const {return connectedParts_; }    //!< accessor to connectedParts_ (const)
    number_t nbParts() const {return nbParts_;}//!< accessor to nbParts_ (const)

    void clearCompositeData();
    void clearExtrusionData();
    void clearParametrization();

    //! return the nnodes of the geometry
    virtual const std::vector<number_t>& n() const
    { error("not_handled", "Geometry::n()"); return *new std::vector<number_t>(); }
    virtual std::vector<number_t>& n()
    { error("not_handled", "Geometry::n()"); return *new std::vector<number_t>(); }
    //! accessor to number of nodes on the Geometry (read only)
    virtual number_t n(number_t i) const { error("not_handled"); return 2;}
    //! return the hsteps of the geometry
    virtual const std::vector<real_t>& h() const
    { error("not_handled", "Geometry::h()"); return *new std::vector<real_t>(); }
    virtual std::vector<real_t>& h()
    { error("not_handled", "Geometry::h()"); return *new std::vector<real_t>(); }
    //! set the main h step, if not overriden do nothing
    virtual void setHstep(real_t h) {return;}
    //! set the main nnodes, if not overriden do nothing
    virtual void setNnodes(number_t i) {return;}

    Strings buildSideNamesAfterCheck(number_t n, number_t n2 = 0) const; //!< check if n side names are given and complete when single or empty
    void checkSideNamesAndUpdate(number_t n, number_t n2 = 0); //!< check if n side names are given and update size when single or empty
    void sideNames(const std::vector<string_t> sn) { sideNames_ = sn; }

    ExtrusionData* extrusionData()             //! return the data for extruded geometry
    { return extrusionData_; }
    const ExtrusionData* extrusionData() const //! return the data for extruded geometry (const)
    { return extrusionData_; }

    const Parametrization& parametrization() const;          //!< return parametrization if allocated
    Parametrization* parametrizationP()                      //! return parametrization pointer
    { return parametrization_; }
    const Parametrization& boundaryParametrization() const;      //!< return boundary_parametrization if allocated
    void setParametrization(const Parametrization& par);         //!< set parametrization
    void setBoundaryParametrization(const Parametrization& par); //!< set boundary_parametrization
    Parametrization* boundaryParametrizationP()                  //! return boundary prametrization pointer
    { return boundaryParametrization_; }

    Geometry* find(const string_t& n) const; //!< return geometry with name n (a canonical geometry or a composite of canonical geometry's)
    virtual void collect(const string_t& n, std::list<Geometry*>& geoms) const; //!< collect in a list all canonical geometry's with name n

    void print(std::ostream& os) const; //!< print Geometry
    virtual void printDetail(std::ostream& os) const {} //!< print additional information
    void print(PrintStream& os) const { print(os.currentStream()); }
    void print(CoutStream& os) const { print(std::cout); print(os.printStream->currentStream()); }
    void printBoundNodes(std::ostream& os) const; //!< print Geometry boundNodes() vector
    void printBoundNodes(PrintStream& os) const { printBoundNodes(os.currentStream()); }
    void printNodes(std::ostream& os) const;      //!< print Geometry nodes() vector
    void printNodes(PrintStream& os) const { printNodes(os.currentStream()); }
    void printCurves(std::ostream& os) const;     //!< print Geometry curves() vector
    void printCurves(PrintStream& os) const { printCurves(os.currentStream()); }
    void printSurfs(std::ostream& os) const;      //!< print Geometry surfs() vector
    void printSurfs(PrintStream& os) const { printSurfs(os.currentStream()); }
    friend std::ostream& operator<<(std::ostream& os, const Geometry& g);
    void addSuffix(const string_t& s); //!< add a suffix to all names (geometry names and side domain names)
    //! format as string
    virtual string_t asString() const { return string_t(); }

    //! compute the bounding box for a composite/loop geometry
    void computeBB();
    //! compute the minimal box for a composite/loop geometry
    virtual void computeMB() {} //!< compute the minimal box
    void updateBB(const std::vector<RealPair>& bb) { boundingBox = BoundingBox(bb); } //!< update the bounding box
    void buildConnectedParts() const;                //!< build connectedParts structure
    Parametrization& buildParametrization() const;   //!< build parametrization if not built     (for canonical geometry)
    bool buildC0PiecewiseParametrization() const;    //!< try to build piecewise parametrization (for composite geometry)
    void buildPiecewiseParametrization() const;      //!< build non C0 piecewise parametrization (for composite geometry)
    void buildPiecewiseError(const string_t& = "") const;
    void buildSideMaps(std::map<Geometry*, std::vector<std::set<number_t> > >& geomsides,
                       std::map<std::set<number_t>, std::list<std::pair<Geometry*, number_t> > >& sidemap) const;

    //! return the list of boundary nodes defining the Geometry
    virtual std::vector<const Point*> boundNodes() const;
    //! return the list of nodes defining the Geometry
    virtual std::vector<Point*> nodes();
    //! return the list of nodes defining the Geometry (const)
    virtual std::vector<const Point*> nodes() const;
    //! return a node belonging to the Geometry (const)
    virtual Point firstNode() const;
    //! return the last node index
    virtual number_t lastNodeIndex() const;
    //! return the list of curves defining the borders of a 2D Geometry
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > curves() const;
    //! return the list of surfaces defining the borders of a 3D Geometry
    virtual std::vector<std::pair<ShapeType, std::vector<const Point*> > > surfs() const;
    //! return the number of sides of the geometry
    virtual number_t nbSides() const;
    virtual bool withNnodes() const //! check if geometry is defined only with _nnodes or with _hsteps option
    { error("not_handled" "Geometry::withNnodes()"); return false; }
    //! i-th value is nnodes of the Geometry's i-th border, else -1 if defined with hsteps
    virtual std::vector<int_t> nnodesPerBorder()
    { error("not_handled", "Geometry::nnodesPerBorder()"); return std::vector<int_t>(); }
    //! return the length/area/volume of the geometry
    virtual real_t measure() const
    { error("not_handled", "Geometry::measure()"); return 0.; }

    //! return true if closed geometry
    virtual bool isClosed() const
    { return false;}

    //! return true if geometry is plane
    virtual bool isPlane() const
    { return false;}

    //! return true if there is a translation between current geometry and an other one, T is the translation vector
    virtual bool isTranslated(const Geometry& g, Point& T) const
    { return false;}

    //! return the number of sides of the geometry
    number_t nbLateralSidesForExtrusion() const;
    bool isCoplanar(const Geometry& g) const; //!< check if geometry is coplanar with another geometry (g)
    bool isInside(const Geometry& g) const; //!< check if geometry is inside another geometry (g)
    //! build a geometric geodesic starting at (x,dx)
    virtual GeometricGeodesic geodesic(const Point& x, const Point& dx, bool wCA = false, bool wT = false);

    virtual const Curve* curve() const //! access to child Curve object (const)
    { error("bad_geometry", asString(), words("shape", shape_), "curve"); return nullptr;}
    virtual Curve* curve()  //! access to child Curve object (non const)
    { error("bad_geometry", asString(), words("shape", shape_), "curve"); return nullptr;}

    virtual const Surface* surface() const //! access to child Surface object (const)
    { error("bad_geometry", asString(), words("shape", shape_), "surface"); return nullptr; }
    virtual Surface* surface()  //! access to child Surface object (non const)
    { error("bad_geometry", asString(), words("shape", shape_), "surface"); return nullptr; }

    virtual const Segment* segment() const //! access to child Segment object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _segment)); return nullptr; }
    virtual Segment* segment() //! access to child Segment object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _segment)); return nullptr; }

    virtual const EllArc* ellArc() const //! access to child EllArc object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellArc)); return nullptr; }
    virtual EllArc* ellArc() //! access to child EllArc object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellArc)); return nullptr; }

    virtual const CircArc* circArc() const //! access to child CircArc object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _circArc)); return nullptr; }
    virtual CircArc* circArc() //! access to child CircArc object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _circArc)); return nullptr; }

    virtual const ParametrizedArc* parametrizedArc() const //! access to child CircArc object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parametrizedArc)); return nullptr; }
    virtual ParametrizedArc* parametrizedArc() //! access to child CircArc object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parametrizedArc)); return nullptr; }

    virtual const SplineArc* splineArc() const //! access to child CircArc object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _splineArc)); return nullptr; }
    virtual SplineArc* splineArc() //! access to child CircArc object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _splineArc)); return nullptr; }

    virtual const Polygon* polygon() const //! access to child Polygon object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _polygon)); return nullptr; }
    virtual Polygon* polygon() //! access to child Polygon object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _polygon)); return nullptr; }

    virtual const Triangle* triangle() const //! access to child Triangle object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _triangle)); return nullptr; }
    virtual Triangle* triangle() //! access to child Triangle object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _triangle)); return nullptr; }

    virtual const Quadrangle* quadrangle() const //! access to child Quadrangle object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _quadrangle)); return nullptr; }
    virtual Quadrangle* quadrangle() //! access to child Quadrangle object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _quadrangle)); return nullptr; }

    virtual const Parallelogram* parallelogram() const //! access to child Parallelogram object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parallelogram)); return nullptr; }
    virtual Parallelogram* parallelogram() //! access to child Parallelogram object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parallelogram)); return nullptr; }

    virtual const Rectangle* rectangle() const //! access to child Rectangle object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _rectangle)); return nullptr; }
    virtual Rectangle* rectangle() //! access to child Rectangle object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _rectangle)); return nullptr; }

    virtual const SquareGeo* square() const //! access to child SquareGeo object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _square)); return nullptr; }
    virtual SquareGeo* square() //! access to child SquareGeo object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _square)); return nullptr; }

    virtual const Ellipse* ellipse() const //! access to child Ellipse object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellipse)); return nullptr; }
    virtual Ellipse* ellipse() //! access to child Ellipse object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellipse)); return nullptr; }

    virtual const Disk* disk() const //! access to child Disk object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _disk)); return nullptr; }
    virtual Disk* disk() //! access to child Disk object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _disk)); return nullptr; }

    virtual const ParametrizedSurface* parametrizedSurface() const //! access to child ParametrizedSurface object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parametrizedSurface)); return nullptr; }
    virtual ParametrizedSurface* parametrizedSurface() //! access to child ParametrizedSurface object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parametrizedSurface)); return nullptr; }

    virtual const SplineSurface* splineSurface() const //! access to child SplineSurface object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _splineSurface)); return nullptr; }
    virtual SplineSurface* splineSurface() //! access to child SplineSurface object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _splineSurface)); return nullptr; }

    virtual const Volume* volume() const //! access to child Volume object (const)
    { error("bad_geometry", asString(), words("shape", shape_), "volume"); return nullptr; }
    virtual Volume* volume()  //! access to child Volume object (non const)
    { error("bad_geometry", asString(), words("shape", shape_), "volume"); return nullptr; }

    virtual const Polyhedron* polyhedron() const //! access to child Polyhedron object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _polyhedron)); return nullptr; }
    virtual Polyhedron* polyhedron() //! access to child Polyhedron object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _polyhedron)); return nullptr; }

    virtual const Tetrahedron* tetrahedron() const //! access to child tetrahedron object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _tetrahedron)); return nullptr; }
    virtual Tetrahedron* tetrahedron() //! access to child Tetrahedron object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _tetrahedron)); return nullptr; }

    virtual const Hexahedron* hexahedron() const //! access to child Hexahedron object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _hexahedron)); return nullptr; }
    virtual Hexahedron* hexahedron() //! access to child Hexahedron object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _hexahedron)); return nullptr; }

    virtual const Parallelepiped* parallelepiped() const //! access to child Parallelepiped object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parallelepiped)); return nullptr; }
    virtual Parallelepiped* parallelepiped() //! access to child Parallelepiped object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _parallelepiped)); return nullptr; }

    virtual const Cuboid* cuboid() const //! access to child Cuboid object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cuboid)); return nullptr; }
    virtual Cuboid* cuboid() //! access to child Cuboid object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cuboid)); return nullptr; }

    virtual const Cube* cube() const //! access to child Cube object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cube)); return nullptr; }
    virtual Cube* cube() //! access to child Cube object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cube)); return nullptr; }

    virtual const Ellipsoid* ellipsoid() const //! access to child Ellipsoid object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellipsoid)); return nullptr; }
    virtual Ellipsoid* ellipsoid() //! access to child Ellipsoid object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellipsoid)); return nullptr; }

    virtual const Ball* ball() const //! access to child Ball object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ball)); return nullptr; }
    virtual Ball* ball() //! access to child Ball object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ball)); return nullptr; }

    virtual const RevTrunk* revTrunk() const //! access to child RevTrunk object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _revTrunk)); return nullptr; }
    virtual RevTrunk* revTrunk() //! access to child RevTrunk object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _revTrunk)); return nullptr; }

    virtual const Trunk* trunk() const //! access to child Trunk object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _trunk)); return nullptr; }
    virtual Trunk* trunk() //! access to child Trunk object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _trunk)); return nullptr; }

    virtual const Cylinder* cylinder() const //! access to child Cylinder object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cylinder)); return nullptr; }
    virtual Cylinder* cylinder() //! access to child Cylinder object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cylinder)); return nullptr; }

    virtual const RevCylinder* revCylinder() const //! access to child RevCylinder object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _revCylinder)); return nullptr; }
    virtual RevCylinder* revCylinder() //! access to child RevCylinder object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _revCylinder)); return nullptr; }

    virtual const Prism* prism() const //! access to child Prism object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _prism)); return nullptr; }
    virtual Prism* prism() //! access to child Prism object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _prism)); return nullptr; }

    virtual const Cone* cone() const //! access to child Cone object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cone)); return nullptr; }
    virtual Cone* cone() //! access to child Cone object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _cone)); return nullptr; }

    virtual const RevCone* revCone() const //! access to child RevCone object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _revCone)); return nullptr; }
    virtual RevCone* revCone() //! access to child RevCone object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _revCone)); return nullptr; }

    virtual const Pyramid* pyramid() const //! access to child Pyramid object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _pyramid)); return nullptr; }
    virtual Pyramid* pyramid() //! access to child Pyramid object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _pyramid)); return nullptr; }

    virtual const EllipsoidSidePart* ellipsoidSidePart() const //! access to child EllipsoidSidePart object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellipsoidSidePart)); return nullptr; }
    virtual EllipsoidSidePart* ellipsoidSidePart() //! access to child Pyramid object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _ellipsoidSidePart)); return nullptr; }

    virtual const TrunkSidePart* trunkSidePart() const //! access to child EllipsoidSidePart object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _trunkSidePart)); return nullptr; }
    virtual TrunkSidePart* trunkSidePart() //! access to child Pyramid object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _trunkSidePart)); return nullptr; }

    virtual const SetOfElems* setofelems() const //! access to child SetOfElems object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _setofelems)); return nullptr; }
    virtual SetOfElems* setofelems() //! access to child SetOfElems object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _setofelems)); return nullptr; }

    virtual const SetOfPoints* setofpoints() const //! access to child SetOfPoints object (const)
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _setofpoints)); return nullptr; }
    virtual SetOfPoints* setofpoints() //! access to child SetOfPoints object
    { error("bad_geometry", asString(), words("shape", shape_), words("shape", _setofpoints)); return nullptr; }

    //================================================
    //         transformations facilities
    //================================================
    //! apply a geometrical transformation on a Geometry
    virtual Geometry& transform(const Transformation& t);
    //! apply a translation on a Geometry (1 key)
    virtual Geometry& translate(const Parameter& p1)
    { return transform(Translation(p1)); }
    //! apply a translation on a Geometry (vector version)
    virtual Geometry& translate(std::vector<real_t> u)
    {
      warning("deprecated", "Geometry::translate(Reals)", "Geometry::translate(_direction=xxx)");
      return transform(Translation(_direction=u));
    }
    //! apply a translation on a Geometry (3 reals version)
    virtual Geometry& translate(real_t ux, real_t uy = 0., real_t uz = 0.)
    {
      warning("deprecated", "Geometry::translate(Real, Real, Real)", "Geometry::translate(_direction={xxx, yyy, zzz})");
      return transform(Translation(_direction={ux, uy, uz}));
    }
    //! apply a rotation 2D on a Geometry (1 key)
    virtual Geometry& rotate2d(const Parameter& p1)
    { return transform(Rotation2d(p1)); }
    //! apply a rotation 2D on a Geometry (2 keys)
    virtual Geometry& rotate2d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation2d(p1, p2)); }
    //! apply a rotation 2D on a Geometry
    virtual Geometry& rotate2d(const Point& c, real_t angle = 0.)
    {
      warning("deprecated", "Geometry::rotate2d(Point, Real)", "Geometry::rotate2d(_center=xxx, _angle=yyy)");
      return transform(Rotation2d(_center=c, _angle=angle));
    }
    //! apply a rotation 3D on a Geometry (1 key)
    virtual Geometry& rotate3d(const Parameter& p1)
    { return transform(Rotation3d(p1)); }
    //! apply a rotation 3D on a Geometry (2 keys)
    virtual Geometry& rotate3d(const Parameter& p1, const Parameter& p2)
    { return transform(Rotation3d(p1, p2)); }
    //! apply a rotation 3D on a Geometry (3 keys)
    virtual Geometry& rotate3d(const Parameter& p1, const Parameter& p2, const Parameter& p3)
    { return transform(Rotation3d(p1, p2, p3)); }
    //! apply a rotation 3D on a Geometry
    virtual Geometry& rotate3d(const Point& c, std::vector<real_t> d = std::vector<real_t>(3, 0.), real_t angle = 0.)
    {
      warning("deprecated", "Geometry::rotate3d(Point, Reals, Real)", "Geometry::rotate3d(_center=xxx, _axis=yyy, _angle=zzz)");
      return transform(Rotation3d(_center=c, _axis=d, _angle=angle));
    }
    //! apply a rotation 3D on a Geometry
    virtual Geometry& rotate3d(real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Geometry::rotate3d(Real, Real, Real)", "Geometry::rotate3d(_axis={dx, dy}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation 3D on a Geometry
    virtual Geometry& rotate3d(real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Geometry::rotate3d(Real, Real, Real, Real)", "Geometry::rotate3d(_axis={dx, dy, dz}, _angle=zzz)");
      return transform(Rotation3d(_center=Point(0., 0., 0.), _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a rotation on a Geometry
    virtual Geometry& rotate3d(const Point& c, real_t dx, real_t dy, real_t angle)
    {
      warning("deprecated", "Geometry::rotate3d(Point, Real, Real, Real)", "Geometry::rotate3d(_center=xxx, _axis={dx, dy}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
    }
    //! apply a rotation on a Geometry
    virtual Geometry& rotate3d(const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
    {
      warning("deprecated", "Geometry::rotate3d(Point, Real, Real, Real, Real)", "Geometry::rotate3d(_center=xxx, _axis={dx, dy, dz}, _angle=yyy)");
      return transform(Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
    }
    //! apply a homothety on a Geometry (1 key)
    virtual Geometry& homothetize(const Parameter& p1)
    { return transform(Homothety(p1)); }
    //! apply a homothety on a Geometry (2 keys)
    virtual Geometry& homothetize(const Parameter& p1, const Parameter& p2)
    { return transform(Homothety(p1, p2)); }
    //! apply a homothety on a Geometry
    virtual Geometry& homothetize(const Point& c = Point(0., 0., 0.), real_t factor = 1.)
    {
      warning("deprecated", "Geometry::homothetize(Point, Real)", "Geometry::homothetize(_center=xxx, _scale=yyy)");
      return transform(Homothety(_center=c, _scale=factor));
    }
    //! apply a homothety on a Geometry
    virtual Geometry& homothetize(real_t factor)
    {
      warning("deprecated", "Geometry::homothetize(Real)", "Geometry::homothetize(_scale=yyy)");
      return transform(Homothety(_center=Point(0., 0., 0.), _scale=factor));
    }
    //! apply a point reflection on a Geometry (1 key)
    virtual Geometry& pointReflect(const Parameter& p1)
    { return transform(PointReflection(p1)); }
    //! apply a point reflection on a Geometry
    virtual Geometry& pointReflect(const Point& c = Point(0., 0., 0.))
    {
      warning("deprecated", "Geometry::pointReflect(Point)", "Geometry::pointReflect(_center=xxx)");
      return transform(PointReflection(_center=c));
    }
    //! apply a reflection2d on a Geometry (1 key)
    virtual Geometry& reflect2d(const Parameter& p1)
    { return transform(Reflection2d(p1)); }
    //! apply a reflection2d on a Geometry (2 keys)
    virtual Geometry& reflect2d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection2d(p1, p2)); }
    //! apply a reflection2d on a Geometry
    virtual Geometry& reflect2d(const Point& c = Point(0., 0.), std::vector<real_t> d = std::vector<real_t>(2, 0.))
    {
      warning("deprecated", "Geometry::reflect2d(Point, Reals)", "Geometry::reflect2d(_center=xxx, _direction=yyy)");
      return transform(Reflection2d(_center=c, _direction=d));
    }
    //! apply a reflection2d on a Geometry
    virtual Geometry& reflect2d(const Point& c, real_t dx, real_t dy = 0.)
    {
      warning("deprecated", "Geometry::reflect2d(Point, Real, Real)", "Geometry::reflect2d(_center=xxx, _direction={dx, dy})");
      return transform(Reflection2d(_center=c, _direction={dx, dy}));
    }
    //! apply a reflection3d on a Geometry (1 key)
    virtual Geometry& reflect3d(const Parameter& p1)
    { return transform(Reflection3d(p1)); }
    //! apply a reflection3d on a Geometry (2 keys)
    virtual Geometry& reflect3d(const Parameter& p1, const Parameter& p2)
    { return transform(Reflection3d(p1, p2)); }
    //! apply a reflection3d on a Geometry
    virtual Geometry& reflect3d(const Point& c = Point(0., 0., 0.), std::vector<real_t> n = std::vector<real_t>(3, 0.))
    {
      warning("deprecated", "Geometry::reflect3d(Point, Reals)", "Geometry::reflect3d(_center=xxx, _normal=yyy)");
      return transform(Reflection3d(_center=c, _normal=n));
    }
    //! apply a reflection3d on a Geometry
    virtual Geometry& reflect3d(const Point& c, real_t nx, real_t ny, real_t nz = 0.)
    {
      warning("deprecated", "Geometry::reflect3d(Point, Real, Real, Real)", "Geometry::reflect3d(_center=xxx, _normal={nx, ny, nz})");
      return transform(Reflection3d(_center=c, _normal=Point(nx, ny, nz)));
    }

    Geometry& crack(CrackType ct, string_t domNameToOpen); //!< force geometry to crack
    Geometry& uncrack(); //!< disable geometry to crack
    Geometry& operator+(); //!< force behavior in union of geometries
    Geometry& operator!(); //!< force behavior in union of geometries
    Geometry& operator-(); //!< disable force behavior in union of geometries
    void cleanInclusions(); //!< clean geometries_ so that inclusions are not included by other ones in the list

    // addition of 2 geometries
    Geometry& operator+=(const Geometry& g); //!< union of g1 and g2 (general case)
    Geometry& operator-=(const Geometry& g); //!< definition of a hole (general case)
    friend Geometry operator+(const Geometry& g1, const Geometry& g2);
    friend Geometry operator-(const Geometry& g1, const Geometry& g2);
    friend void addCompositeAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void addCompositeAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void addCompositeAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void addLoopAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void addLoopAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void addCanonicalAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrCompositeAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrCompositeAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrCompositeAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrLoopAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrLoopAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrLoopAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrCanonicalAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrCanonicalAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend void substrCanonicalAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
    friend Geometry toComposite(const Geometry& g);

    //surfaceFrom, VolumeFrom and extrusion stuff
    friend Geometry surfaceFrom(const Geometry& c, string_t domName, bool isPlaneSurface);
    friend Geometry planeSurfaceFrom(const Geometry& c, string_t domName);
    friend Geometry ruledSurfaceFrom(const Geometry& c, string_t domName);
    friend Geometry volumeFrom(const Geometry& s, string_t domName);
    friend Geometry extrude(const Geometry& g, const Transformation& t, const std::vector<Parameter>& ps);
    friend Geometry extrude(const Geometry& g, const Transformation& t, number_t layers, string_t domName,
                            std::vector<string_t> sidenames);
    friend Geometry extrude(const Geometry& g, const Transformation& t, number_t layers, std::vector<string_t> sidenames);
    friend Geometry extrude(const Geometry& g, const Transformation& t, string_t domName, std::vector<string_t> sidenames);
    friend Geometry extrude(const Geometry& g, const Transformation& t, std::vector<string_t> sidenames);

    // boundary of a geometry
    virtual Geometry& buildBoundary() const
    {error("free_error", "boundary of " + words("shape", shape_) + " is not yet handled"); return const_cast<Geometry&>(*this);}
    Geometry& boundary() const;

    //stuff requiring Open Cascade API, all this stuff is implemented in OpenCascade.cpp
#ifdef XLIFEPP_WITH_OPENCASCADE
  protected:
    mutable OCData* ocData_;      //!< strucure handled main OC TopoDS_Shape and related data
  public:
    void initOC();                        //!< init OCData
    void clearOC();                       //!< clear OCData
    void cloneOC(const Geometry&);        //!< clone OCData
    const OCData& ocData() const;         //!< return OCData related to Geometry (built on fly)
    OCData& ocData();                     //!< return OCData related to Geometry (built on fly)
    const TopoDS_Shape& ocShape() const;  //!< return TopoDS_Shape object related to geometry (built on fly)
    bool hasDoc() const;
    void buildOCData() const;                //!< build OCData (may be on fly)
    void ocTransformP(const Transformation&);//!< apply a Transformation to OC oject if available (NOT PROTECTED BY OPENCASCADE MACRO!)

    void printOCInfo(CoutStream&) const;  //!< print info related to OC shapes to CoutStream
    void printOCInfo(std::ostream&) const;//!< print info related to OC shapes to ostream
    void loadFromBrep(const string_t& file, const Strings& domNames = Strings(),
                      const Strings& sideNames = Strings(), const Reals& hsteps = Reals()); //!< build geometry from brep file (implemented in OpenCascade.cpp)
    void setOCName(OCShapeType oct, const std::vector<number_t>& nums, const string_t& na); //!< set name to some OC shapes given by a type and some num/id
    void setOCName(OCShapeType oct, number_t num, const string_t& na);    //!< set name to an OC shape given by its type and num/id (0 means all shapes of OCShapeTYpe)
    void setOCHstep(const std::vector<number_t>& nums, real_t hstep);     //!< set hstep (>0) to some OC vertices given by some num/id
    void setOCHstep(number_t num, real_t hstep);                          //!< set hstep (>0) to an OC vertex given by its num/id (0 means all vertices)
    void setOCNnode(const std::vector<number_t>& nums, number_t nnode);   //!< set nnode (>1) to some OC edges given by some num/id
    void setOCNnode(number_t num, number_t nnode);                        //!< set nnode (>1) to an OC edge given by its num/id (0 means all edges)

    Geometry& operator^=(const Geometry& g); //!< build of common part
    friend Geometry operator^(const Geometry& g1, const Geometry& g2);  //!< create common part
#endif

    //!< apply a Transformation to OC oject if available (binding ocTransformP to avoid spoiling a lot of functions with OC macro)
    void ocTransform(const Transformation& t)
    {
#ifdef XLIFEPP_WITH_OPENCASCADE
      ocTransformP(t);
#endif
    }
};

std::ostream& operator<<(std::ostream&, const Geometry&); //!< output Geometry

// add and substr stuff
Geometry toComposite(const Geometry& g); //!< return a composite geometry with only one component: g
Geometry operator+(const Geometry& g1, const Geometry& g2);                //!< union of g1 and g2 (general case)
Geometry operator-(const Geometry& g1, const Geometry& g2);                //!< g2 hole of g1 (general case)
void addCompositeAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
void addCompositeAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
void addCompositeAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
void addLoopAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
void addLoopAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
void addCanonicalAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrCompositeAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrCompositeAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrCompositeAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrLoopAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrLoopAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrLoopAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrCanonicalAndComposite(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrCanonicalAndLoop(const Geometry& g1, const Geometry& g2, Geometry& g);
void substrCanonicalAndCanonical(const Geometry& g1, const Geometry& g2, Geometry& g);

//! definition of a geometry 2D from an union of boundaries 1D
Geometry surfaceFrom(const Geometry& c, string_t domName = "", bool isPlaneSurface = true);
//! definition of a geometry 2D from an union of boundaries 1D
inline Geometry planeSurfaceFrom(const Geometry& c, string_t domName = "")
{ return surfaceFrom(c, domName, true); }
//! definition of a geometry 2D from an union of boundaries 1D
inline Geometry ruledSurfaceFrom(const Geometry& c, string_t domName = "")
{ return surfaceFrom(c, domName, false); }
//! definition of a geometry 3D from an union of boundaries 2D
Geometry volumeFrom(const Geometry& s, string_t domName = "");
//! main external routine for the definition of a geometry by extrusion of another geometry, with a list if parameters
Geometry extrude(const Geometry& g, const Transformation& t, const std::vector<Parameter>& ps);
//! Definition of a geometry by extrusion of another geometry, with 1 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p);
//! Definition of a geometry by extrusion of another geometry, with 2 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2);
//! Definition of a geometry by extrusion of another geometry, with 3 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3);
//! Definition of a geometry by extrusion of another geometry, with 4 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3, Parameter p4);
//! Definition of a geometry by extrusion of another geometry, with 5 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5);
//! Definition of a geometry by extrusion of another geometry, with 6 Parameter
Geometry extrude(const Geometry& g, const Transformation& t, Parameter p1, Parameter p2, Parameter p3, Parameter p4, Parameter p5, Parameter p6);
//! definition of a geometry by extrusion of another geometry, with name and side names
inline Geometry extrude(const Geometry& g, const Transformation& t, number_t layers, string_t domName,
                        std::vector<string_t> sidenames = std::vector<string_t>())
{ return extrude(g, t, _layers = layers, _domain_name = domName, _side_names = sidenames); }
//! definition of a geometry by extrusion of another geometry, with name and side names
inline Geometry extrude(const Geometry& g, const Transformation& t, number_t layers, const char* domName,
                        std::vector<string_t> sidenames = std::vector<string_t>())
{ return extrude(g, t, _layers = layers, _domain_name = string_t(domName), _side_names = sidenames); }
//! definition of a geometry by extrusion of another geometry, with name and side name
inline Geometry extrude(const Geometry& g, const Transformation& t, number_t layers, string_t domName, string_t sidenames)
{ return extrude(g, t, _layers = layers, _domain_name = domName, _side_names = std::vector<string_t>(1, sidenames)); }
//! definition of a geometry by extrusion of another geometry, with name and side name
inline Geometry extrude(const Geometry& g, const Transformation& t, number_t layers, const char* domName, const char* sidenames)
{ return extrude(g, t, _layers = layers, _domain_name = string_t(domName), _side_names = std::vector<string_t>(1, sidenames)); }
//! definition of a geometry by extrusion of another geometry, with side names
inline Geometry extrude(const Geometry& g, const Transformation& t, number_t layers,
                        std::vector<string_t> sidenames = std::vector<string_t>())
{ return extrude(g, t, _layers = layers, _side_names = sidenames); }
//! definition of a geometry by extrusion of another geometry, with name and side names
inline Geometry extrude(const Geometry& g, const Transformation& t, string_t domName,
                        std::vector<string_t> sidenames = std::vector<string_t>())
{ return extrude(g, t, _domain_name = domName, _side_names = sidenames); }
//! definition of a geometry by extrusion of another geometry, with name and side names
inline Geometry extrude(const Geometry& g, const Transformation& t, const char* domName,
                        std::vector<string_t> sidenames = std::vector<string_t>())
{ return extrude(g, t, _domain_name = string_t(domName), _side_names = sidenames); }
//! definition of a geometry by extrusion of another geometry, with name and side name
inline Geometry extrude(const Geometry& g, const Transformation& t, string_t domName, string_t sidenames)
{ return extrude(g, t, _domain_name = domName, _side_names = sidenames); }
//! definition of a geometry by extrusion of another geometry, with name and side name
inline Geometry extrude(const Geometry& g, const Transformation& t, const char* domName, const char* sidenames)
{ return extrude(g, t, _domain_name = string_t(domName), _side_names = string_t(sidenames)); }
//! definition of a geometry by extrusion of another geometry, with side names
inline Geometry extrude(const Geometry& g, const Transformation& t, std::vector<string_t> sidenames = std::vector<string_t>())
{ return extrude(g, t, _side_names = sidenames); }

//! definition of a geometry by restriction to boundary
inline Geometry& boundary(const Geometry& g)
{ return g.boundary(); }
inline Geometry& operator~(const Geometry& g)
{ return g.boundary(); }


//! user shortcut to crack one geometry
void crack(Geometry& g1, CrackType ct = _closedCrack, string_t domNameToOpen = string_t());
//! user shortcut to crack 2 geometries
void crack(Geometry& g1, Geometry& g2, CrackType ct = _closedCrack, string_t domNameToOpen = string_t());
//! user shortcut to crack 3 geometries
void crack(Geometry& g1, Geometry& g2, Geometry& g3, CrackType ct = _closedCrack, string_t domNameToOpen = string_t());
//! user shortcut to crack 4 geometries
void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, CrackType ct = _closedCrack, string_t domNameToOpen = string_t());
//! user shortcut to crack 5 geometries
void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, CrackType ct = _closedCrack, string_t domNameToOpen = string_t());
//! user shortcut to crack 6 geometries
void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6, CrackType ct = _closedCrack, string_t domNameToOpen = string_t());
//! user shortcut to crack 7 geometries
void crack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6, Geometry& g7, CrackType ct = _closedCrack, string_t domNameToOpen = string_t());

//! user shortcut to crack one geometry
inline void openCrack(Geometry& g1, string_t domNameToOpen)
{ crack(g1, _openCrack, domNameToOpen); }
//! user shortcut to crack 2 geometries
inline void openCrack(Geometry& g1, Geometry& g2, string_t domNameToOpen)
{ crack(g1, g2, _openCrack, domNameToOpen); }
//! user shortcut to crack 3 geometries
inline void openCrack(Geometry& g1, Geometry& g2, Geometry& g3, string_t domNameToOpen)
{ crack(g1, g2, g3, _openCrack, domNameToOpen); }
//! user shortcut to crack 4 geometries
inline void openCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, string_t domNameToOpen)
{ crack(g1, g2, g3, g4, _openCrack, domNameToOpen); }
//! user shortcut to crack 5 geometries
inline void openCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, string_t domNameToOpen)
{ crack(g1, g2, g3, g4, g5, _openCrack, domNameToOpen); }
//! user shortcut to crack 6 geometries
inline void openCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6, string_t domNameToOpen)
{ crack(g1, g2, g3, g4, g5, g6, _openCrack, domNameToOpen); }
//! user shortcut to crack 7 geometries
inline void openCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6, Geometry& g7,
                      string_t domNameToOpen)
{ crack(g1, g2, g3, g4, g5, g6, g7, _openCrack, domNameToOpen); }

//! user shortcut to crack one geometry
inline void closedCrack(Geometry& g1)
{ crack(g1, _closedCrack); }
//! user shortcut to crack 2 geometries
inline void closedCrack(Geometry& g1, Geometry& g2)
{ crack(g1, g2, _closedCrack); }
//! user shortcut to crack 3 geometries
inline void closedCrack(Geometry& g1, Geometry& g2, Geometry& g3)
{ crack(g1, g2, g3, _closedCrack); }
//! user shortcut to crack 4 geometries
inline void closedCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4)
{ crack(g1, g2, g3, g4, _closedCrack); }
//! user shortcut to crack 5 geometries
inline void closedCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5)
{ crack(g1, g2, g3, g4, g5, _closedCrack); }
//! user shortcut to crack 6 geometries
inline void closedCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6)
{ crack(g1, g2, g3, g4, g5, g6, _closedCrack); }
//! user shortcut to crack 7 geometries
inline void closedCrack(Geometry& g1, Geometry& g2, Geometry& g3, Geometry& g4, Geometry& g5, Geometry& g6, Geometry& g7)
{ crack(g1, g2, g3, g4, g5, g6, g7, _closedCrack); }

//! find if a curve/surf is in a list of curves/surfs
int_t findBorder(const std::pair<ShapeType, std::vector<const Point*> >& border, const std::vector<std::pair<ShapeType,
                 std::vector<const Point*> > >& borders);
//! determine if 2 geometries (list of vertices) have the same orientation
bool sameOrientation(std::vector<const Point*> border1, std::vector<const Point*> border2);
//! check if points pts1 are related by a translation to points pts2 (T is the translation vector)
bool isTranslatedPoints(const std::vector<Point>& pts1, const std::vector<Point>& pts2, Point& T);

//================================================
//         transformations facilities
//================================================
//! apply a geometrical transformation on a Geometry (external)
Geometry transform(const Geometry& g, const Transformation& t);
//! apply a translation on a Geometry (1 key) (template external)
inline Geometry translate(const Geometry& g, const Parameter& p1)
{ return transform(g, Translation(p1)); }
//! apply a translation on a Geometry (vector version) (template external)
inline Geometry translate(const Geometry& g, std::vector<real_t> u = std::vector<real_t>(3,0.))
{
  warning("deprecated", "translate(Geometry, Reals)", "translate(Geometry, _direction=xxx)");
  return transform(g, Translation(_direction=u));
}
//! apply a translation on a Geometry (3 reals version) (template external)
inline Geometry translate(const Geometry& g, real_t ux, real_t uy, real_t uz = 0.)
{
  warning("deprecated", "translate(Geometry, Real, Real, Real)", "translate(Geometry, _direction={ux, uy, uz})");
  return transform(g, Translation(_direction={ux, uy, uz}));
}
//! apply a rotation 2d on a Geometry (1 key) (template external)
inline Geometry rotate2d(const Geometry& g, const Parameter& p1)
{ return transform(g, Rotation2d(p1)); }
//! apply a rotation 2d on a Geometry (2 keys) (template external)
inline Geometry rotate2d(const Geometry& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Rotation2d(p1, p2)); }
//! apply a rotation 2d on a Geometry (template external)
inline Geometry rotate2d(const Geometry& g, const Point& c = Point(0.,0.), real_t angle = 0.)
{
  warning("deprecated", "rotate2d(Geometry, Point, Real)", "rotate2d(Geometry, _center=xxx, _angle=yyy)");
  return transform(g, Rotation2d(_center=c, _angle=angle));
}
//! apply a rotation 3d on a Geometry (1 key) (template external)
inline Geometry rotate3d(const Geometry& g, const Parameter& p1)
{ return transform(g, Rotation3d(p1)); }
//! apply a rotation 3d on a Geometry (2 keys) (template external)
inline Geometry rotate3d(const Geometry& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Rotation3d(p1, p2)); }
//! apply a rotation 3d on a Geometry (3 keys) (template external)
inline Geometry rotate3d(const Geometry& g, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return transform(g, Rotation3d(p1, p2, p3)); }
//! apply a rotation 3d on a Geometry (template external)
inline Geometry rotate3d(const Geometry& g, const Point& c = Point(0.,0.,0.),
                                    std::vector<real_t> d = std::vector<real_t>(3,0.), real_t angle = 0.)
{
  warning("deprecated", "rotate3d(Geometry, Point, Reals, Real)", "rotate3d(Geometry, _center=xxx, _axis=yyy, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis=d, _angle=angle));
}
//! apply a rotation 3d on a Geometry (template external)
inline Geometry rotate3d(const Geometry& g, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Geometry, Real, Real, Real)", "rotate3d(Geometry, _axis={dx, dy}, _angle=zzz)");
  return transform(g, Rotation3d(_axis={dx, dy}, _angle=angle));
}
//! apply a rotation 3d on a Geometry (template external)
inline Geometry rotate3d(const Geometry& g, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Geometry, Real, Real, Real, Real)", "rotate3d(Geometry, _axis={dx, dy, dz}, _angle=zzz)");
  return transform(g, Rotation3d(_axis={dx, dy, dz}, _angle=angle));
}
//! apply a rotation 3d on a Geometry (template external)
inline Geometry rotate3d(const Geometry& g, const Point& c, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Geometry, Point, Real, Real, Real)", "rotate3d(Geometry, _center=xxx, _axis={dx, dy}, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
}
//! apply a rotation 3d on a Geometry (template external)
inline Geometry rotate3d(const Geometry& g, const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Geometry, Point, Real, Real, Real, Real)", "rotate3d(Geometry, _center=xxx, _axis={dx, dy, dz}, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
}
//! apply a homothety on a Geometry (1 key) (template external)
inline Geometry homothetize(const Geometry& g, const Parameter& p1)
{ return transform(g, Homothety(p1)); }
//! apply a homothety on a Geometry (2 keys) (template external)
inline Geometry homothetize(const Geometry& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Homothety(p1, p2)); }
//! apply a homothety on a Geometry (template external)
inline Geometry homothetize(const Geometry& g, const Point& c = Point(0.,0.,0.), real_t factor = 0.)
{
  warning("deprecated", "homothetize(Geometry, Point, Real)", "homothetize(Geometry, _center=xxx, _scale=yyy)");
  return transform(g, Homothety(_center=c, _scale=factor));
}
//! apply a homothety on a Geometry (template external)
inline Geometry homothetize(const Geometry& g, real_t factor)
{
  warning("deprecated", "homothetize(Geometry, Real)", "homothetize(Geometry, _scale=xxx)");
  return transform(g, Homothety(_scale=factor));
}
//! apply a point reflection on a Geometry (1 key)  (template external)
inline Geometry pointReflect(const Geometry& g, const Parameter& p1)
{ return transform(g, PointReflection(p1)); }
//! apply a point reflection on a Geometry (template external)
inline Geometry pointReflect(const Geometry& g, const Point& c = Point(0.,0.,0.))
{
  warning("deprecated", "pointReflect(Geometry, Real)", "pointReflect(Geometry, _center=xxx)");
  return transform(g, PointReflection(_center=c));
}
//! apply a reflection 2d on a Geometry (1 key) (template external)
inline Geometry reflect2d(const Geometry& g, const Parameter& p1)
{ return transform(g, Reflection2d(p1)); }
//! apply a reflection 2d on a Geometry (2 keys) (template external)
inline Geometry reflect2d(const Geometry& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Reflection2d(p1, p2)); }
//! apply a reflection 2d on a Geometry (template external)
inline Geometry reflect2d(const Geometry& g, const Point& c = Point(0.,0.),
                                     std::vector<real_t> d = std::vector<real_t>(2,0.))
{
  warning("deprecated", "reflect2d(Geometry, Point, Reals)", "reflect2d(Geometry, _center=xxx, _direction=yyy)");
  return transform(g, Reflection2d(_center=c, _direction=d));
}
//! apply a reflection 2d on a Geometry (template external)
inline Geometry reflect2d(const Geometry& g, const Point& c, real_t dx, real_t dy = 0.)
{
  warning("deprecated", "reflect2d(Geometry, Point, Real, Real)", "reflect2d(Geometry, _center=xxx, _direction={dx, dy})");
  return transform(g, Reflection2d(_center=c, _direction={dx, dy}));
}
//! apply a reflection 3d on a Geometry (1 key) (template external)
inline Geometry reflect3d(const Geometry& g, const Parameter& p1)
{ return transform(g, Reflection3d(p1)); }
//! apply a reflection 3d on a Geometry (2 keys) (template external)
inline Geometry reflect3d(const Geometry& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Reflection3d(p1, p2)); }
//! apply a reflection 3d on a Geometry (template external)
inline Geometry reflect3d(const Geometry& g, const Point& c = Point(0.,0.,0.),
                                     std::vector<real_t> n = std::vector<real_t>(3,0.))
{
  warning("deprecated", "reflect3d(Geometry, Point, Reals)", "reflect3d(Geometry, _center=xxx, _normal=yyy)");
  return transform(g, Reflection3d(_center=c, _normal=n));
}
//! apply a reflection 3d on a Geometry (template external)
inline Geometry reflect3d(const Geometry& g, const Point& c, real_t nx, real_t ny, real_t nz = 0.)
{
  warning("deprecated", "reflect3d(Geometry, Point, Real, Real, Real)", "reflect2d(Geometry, _center=xxx, _normal={nx, ny, nz})");
  return transform(g, Reflection3d(_center=c, _normal={nx, ny, nz}));
}

//===================================================================================================================
/*!
   \class GeoNode
    utility class to describe a hierarchical composite construction
    each node represent either a unary operation on a geometry/GeoNode or a binary operation on two geometries/GeoNodes
                               op(g1) op(g1,g2)
    its main purpose is to describe as a tree some complex composite geometries involving several minus, plus, common operations
    the GeoNode will be attached to any geometry as a member data (not allocated for canonical geometries)
    GeoNode may be used in two way, either handling GeoNode(s) or Geometry(s) (terminal leaf)
    in a compact construction such as (g1+g2+g3)-g4 the representation will be: geonode(-, &geonode(+, &geonode(+&g1,&g2) ,&g3), &g4)  (two additional GeoNodes)
    if g=g1+g2+g3 has been already constructed, the representation will be geonode(-, &g, &g4) (no additional GeoNode !)
        in other words as soon as a geometry pointer is available it is used instead of a new geo node
    all the tree may be unrolled using expand() function
 */

enum GeoOperation {_noneGeOp, _plusGeOp, _minusGeOp, _commonGeOp, _loopGeOp, _extrusionGeOp};

class GeoNode
{
  private:
    GeoOperation geop_;    //!< geometric operation involved by the node
    GeoNode* node1_;       //!< first GeoNode operand involved by the node if concerned
    GeoNode* node2_;       //!< second GeoNode operand involved by the node if concerned
    Geometry* geom1_;      //!< first Geometry operand involved by the node if concerned
    Geometry* geom2_;      //!< second Geometry operand involved by the node if concerned
    number_t level_;       //!< level of node (0 root level)

  public:
    GeoNode(GeoOperation op, Geometry* g, number_t l = 0)
      : geop_(op), node1_(nullptr), node2_(nullptr), geom1_(g), geom2_(nullptr), level_(l)
    { updateLevel(l); } //unary node
    GeoNode(GeoOperation op, GeoNode* g, number_t l = 0)
      : geop_(op), node1_(g), node2_(nullptr), geom1_(nullptr), geom2_(nullptr), level_(l)
    { updateLevel(l); }  //unary node
    GeoNode(GeoOperation op, Geometry* g1, Geometry* g2, number_t l = 0)
      : geop_(op), node1_(nullptr), node2_(nullptr), geom1_(g1), geom2_(g2), level_(l)
    { updateLevel(l); }//binary node
    GeoNode(GeoOperation op, GeoNode* g1, GeoNode* g2, number_t l = 0)
      : geop_(op), node1_(g1), node2_(g2), geom1_(nullptr), geom2_(nullptr), level_(l)
    { updateLevel(l); }  //binary node
    GeoNode(GeoOperation op, GeoNode* g1, Geometry* g2, number_t l = 0)
      : geop_(op), node1_(g1), node2_(nullptr), geom1_(nullptr), geom2_(g2), level_(l)
    { updateLevel(l); }  //binary node
    GeoNode(GeoOperation op, Geometry* g1, GeoNode* g2, number_t l = 0)
      : geop_(op), node1_(nullptr), node2_(g2), geom1_(g1), geom2_(nullptr), level_(l)
    { updateLevel(l); } //binary node
    GeoNode(const GeoNode& gn);             //!< copy constructor (no deep copy of geometry pointers, see updateGeo)
    GeoNode& operator=(const GeoNode& gn);  //!< assign operator  (no deep copy of geometry pointers, see updateGeo)
    ~GeoNode();                             //!< destructor, geometry pointeurs are not deleted
    void updateGeo(std::map<Geometry*, Geometry*>& newgeo); //!< update Geometry pointers from a map of Geometry* moves
    void updateLevel(number_t l);           //!< increment by one level_ of each node from current node

    void expand(); //!< expand recursively geometry pointers having themselves GeoNode representations

    GeoOperation op() const {return geop_;}
    Geometry* geom1() const {return geom1_;}
    Geometry* geom2() const {return geom2_;}
    GeoNode* node1() const {return node1_;}
    GeoNode* node2() const {return node2_;}
    number_t level() const {return level_;}
    bool isBinary() const {return geop_ > _noneGeOp && geop_ < _loopGeOp;}

    string_t asString() const;    //!< string representation of GeoNode

#ifdef XLIFEPP_WITH_OPENCASCADE
    ShapeDocPair ocBuild(); //!<recursive construction of the TopoDS_Shape related to current node
#endif
};

GeoNode expand(const GeoNode& gn);   //!< create the expansion of a GeoNode
string_t asString(GeoOperation op);  //!< string representation of GeoOperation

//===================================================================================================================
/*!
   \class ExtrusionData collects all data required to do an extrusion
*/
class ExtrusionData
{
  private:
    Transformation* extrusion_=nullptr; //!< transformation used by extrusion (translation, rotation, ...)
    number_t layers_=1;                 //!< numbers of layers
    string_t domName_;                  //!< name of the "volume" domain
    Strings baseDomainNames_;           //!< names of basis of extruded geometry
    Strings lateralSideNames_;          //!< names of lateral sides of extrudes domains, as many as sides of main basis
    std::vector<number_t> n_;           //!< number of nodes on edges
    std::vector<real_t> h_;             //!< local mesh size at vertices (may be empty)
    //additional data used by extrusion of mesh
    par_fun fun_=nullptr;               //!< Rm->Rn function handling extrusion trajectory
    dimen_t namingDomain_ = 0;          //!< 0 no naming, 1 naming global domain, 2 naming all domains
    dimen_t namingSection_ = 0;         //!< 0 no naming, 1 naming start and end section, 2 naming all sections
    dimen_t namingSide_ = 0;            //!< 0 no naming, 1 naming global sides, 2 naming all section sides
    Transformation* initialTransf_=nullptr;    //!< 0 initial transformation
    std::vector<Transformation*> transformations_; //!< list of transformations used by extrusion (translation, rotation, ...)
    Point center_;                      //!< when fun extrusion, the center of initial section (computed or given)
  public:
    ExtrusionData() = default;
    //! main constructor of an ExtrusionData with Transformation and Parameters
    ExtrusionData(const Transformation& t, bool isGeoExtrusion, const std::vector<Parameter>& ps);
    //! main constructor of an ExtrusionData with a list of Transformation and Parameters
    ExtrusionData(const std::vector<Transformation*>& ts, bool isGeoExtrusion, const std::vector<Parameter>& ps);
    //! main constructor of an ExtrusionData with a parametrization (par_fun)
    ExtrusionData(par_fun f, bool isGeoExtrusion, const std::vector<Parameter>& ps);
    //! full copy constructor
    ExtrusionData(const ExtrusionData& e);
    void clear();
    ~ExtrusionData();
    ExtrusionData& operator=(const ExtrusionData& e);
    //! management of key/value parameters for extrude routines
    void buildParam(const Parameter& p);
    //! management of default values of key/value parameters for extrude routines
    void buildDefaultParam(ParameterKey key);
    //! geometry extrusion parameter keys
    std::set<ParameterKey> getGeoParamsKeys();
    void geoParams(const std::vector<Parameter>& ps);
    //! mesh extrusion parameter keys
    std::set<ParameterKey> getMeshParamsKeys();
    void meshParams(const std::vector<Parameter>& ps);

    // accessors
    Transformation*& extrusion()            //! return the extrusion transformation (non const)
    { return extrusion_; }
    const Transformation* extrusion() const //! return the extrusion transformation (const)
    { return extrusion_; }
    number_t& layers()
    { return layers_; }
    number_t layers() const
    { return layers_; }
    string_t& domName()
    { return domName_; }
    const string_t& domName() const
    { return domName_; }
    Strings& baseDomainNames()
    { return baseDomainNames_; }
    const Strings& baseDomainNames() const
    { return baseDomainNames_; }
    string_t& baseDomainName(number_t i)
    { return baseDomainNames_[i]; }
    const string_t& baseDomainName(number_t i) const
    { return baseDomainNames_[i]; }
    Strings& lateralSideNames()
    { return lateralSideNames_; }
    const Strings& lateralSideNames() const
    { return lateralSideNames_; }
    std::vector<number_t>& n()
    { return n_; }
    const std::vector<number_t>& n() const
    { return n_; }
    std::vector<real_t>& h()
    { return h_; }
    const std::vector<real_t>& h() const
    { return h_; }
    par_fun fun() const
    { return fun_; };
    par_fun& fun()
    { return fun_; };
    dimen_t namingDomain() const
    { return namingDomain_; }
    dimen_t& namingDomain()
    { return namingDomain_; }
    dimen_t namingSection() const
    { return namingSection_; }
    dimen_t& namingSection()
    { return namingSection_; }
    dimen_t namingSide() const
    { return namingSide_; }
    dimen_t& namingSide()
    { return namingSide_; }
    const std::vector<Transformation*>& transformations() const
    { return transformations_;}
    std::vector<Transformation*>& transformations()
    { return transformations_;}
    Transformation*& initialTransf()
    { return initialTransf_; }
    const Transformation* initialTransf() const
    { return initialTransf_; }
    const Point& center() const
    { return center_; }
    Point& center()
    { return center_; }
};

} // end of namespace xlifepp

#endif // GEOMETRY_HPP
