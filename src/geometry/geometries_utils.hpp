/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file geometries_utils.hpp
  \authors Y. Lafranche, N. Kielbasiewicz
  \since 18 oct 2012
  \date 30 jul 2015

  \brief Additional functions related to geometries
*/

#ifndef GEOMETRIES_UTILS_HPP
#define GEOMETRIES_UTILS_HPP

#include "Geometry.hpp"

namespace xlifepp
{

number_t nodesDim(const Geometry& g);

string_t oneOfStrings(const string_t& sn, int i=0);
string_t oneOfStrings(const std::vector<string_t>& sn, int i=0);
string_t oneOfStringsIfVector(const string_t& sn, int i=0);
string_t oneOfStringsIfVector(const std::vector<string_t>& sn, int i=0);
number_t nbSubDomainsIfScalar(string_t sn, number_t nbSubDomains);
number_t nbSubDomainsIfScalar(const std::vector<string_t>& sn, number_t nbSubDomains);

//---------------------------------------------------------------------------
// transformations facilities (template external)
//---------------------------------------------------------------------------

//! apply a geometrical transformation on a Geom (template external)
template<class Geom> Geom transform(const Geom& g, const Transformation& t);
//! apply a translation on a Geom (1 key) (template external)
template<class Geom> Geom translate(const Geom& g, const Parameter& p1)
{ return transform(g, Translation(p1)); }
//! apply a translation on a Geom (vector version) (template external)
template<class Geom> Geom translate(const Geom& g, std::vector<real_t> u = std::vector<real_t>(3,0.))
{
  warning("deprecated", "translate(Geom, Reals)", "translate(Geom, _direction=xxx)");
  return transform(g, Translation(_direction=u));
}
//! apply a translation on a Geom (3 reals version) (template external)
template<class Geom> Geom translate(const Geom& g, real_t ux, real_t uy, real_t uz = 0.)
{
  warning("deprecated", "translate(Geom, Real, Real, Real)", "translate(Geom, _direction={ux, uy, uz})");
  return transform(g, Translation(_direction={ux, uy, uz}));
}
//! apply a rotation 2d on a Geom (1 key) (template external)
template<class Geom> Geom rotate2d(const Geom& g, const Parameter& p1)
{ return transform(g, Rotation2d(p1)); }
//! apply a rotation 2d on a Geom (2 keys) (template external)
template<class Geom> Geom rotate2d(const Geom& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Rotation2d(p1, p2)); }
//! apply a rotation 2d on a Geom (template external)
template<class Geom> Geom rotate2d(const Geom& g, const Point& c = Point(0.,0.), real_t angle = 0.)
{
  warning("deprecated", "rotate2d(Geom, Point, Real)", "rotate2d(Geom, _center=xxx, _angle=yyy)");
  return transform(g, Rotation2d(_center=c, _angle=angle));
}
//! apply a rotation 3d on a Geom (1 key) (template external)
template<class Geom> Geom rotate3d(const Geom& g, const Parameter& p1)
{ return transform(g, Rotation3d(p1)); }
//! apply a rotation 3d on a Geom (2 keys) (template external)
template<class Geom> Geom rotate3d(const Geom& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Rotation3d(p1, p2)); }
//! apply a rotation 3d on a Geom (3 keys) (template external)
template<class Geom> Geom rotate3d(const Geom& g, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{ return transform(g, Rotation3d(p1, p2, p3)); }
//! apply a rotation 3d on a Geom (template external)
template<class Geom> Geom rotate3d(const Geom& g, const Point& c = Point(0.,0.,0.),
                                    std::vector<real_t> d = std::vector<real_t>(3,0.), real_t angle = 0.)
{
  warning("deprecated", "rotate3d(Geom, Point, Reals, Real)", "rotate3d(Geom, _center=xxx, _axis=yyy, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis=d, _angle=angle));
}
//! apply a rotation 3d on a Geom (template external)
template<class Geom> Geom rotate3d(const Geom& g, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Geom, Real, Real, Real)", "rotate3d(Geom, _axis={dx, dy}, _angle=zzz)");
  return transform(g, Rotation3d(_axis={dx, dy}, _angle=angle));
}
//! apply a rotation 3d on a Geom (template external)
template<class Geom> Geom rotate3d(const Geom& g, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Geom, Real, Real, Real, Real)", "rotate3d(Geom, _axis={dx, dy, dz}, _angle=zzz)");
  return transform(g, Rotation3d(_axis={dx, dy, dz}, _angle=angle));
}
//! apply a rotation 3d on a Geom (template external)
template<class Geom> Geom rotate3d(const Geom& g, const Point& c, real_t dx, real_t dy, real_t angle)
{
  warning("deprecated", "rotate3d(Geom, Point, Real, Real, Real)", "rotate3d(Geom, _center=xxx, _axis={dx, dy}, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis={dx, dy}, _angle=angle));
}
//! apply a rotation 3d on a Geom (template external)
template<class Geom> Geom rotate3d(const Geom& g, const Point& c, real_t dx, real_t dy, real_t dz, real_t angle)
{
  warning("deprecated", "rotate3d(Geom, Point, Real, Real, Real, Real)", "rotate3d(Geom, _center=xxx, _axis={dx, dy, dz}, _angle=zzz)");
  return transform(g, Rotation3d(_center=c, _axis={dx, dy, dz}, _angle=angle));
}
//! apply a homothety on a Geom (1 key) (template external)
template<class Geom> Geom homothetize(const Geom& g, const Parameter& p1)
{ return transform(g, Homothety(p1)); }
//! apply a homothety on a Geom (2 keys) (template external)
template<class Geom> Geom homothetize(const Geom& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Homothety(p1, p2)); }
//! apply a homothety on a Geom (template external)
template<class Geom> Geom homothetize(const Geom& g, const Point& c = Point(0.,0.,0.), real_t factor = 0.)
{
  warning("deprecated", "homothetize(Geom, Point, Real)", "homothetize(Geom, _center=xxx, _scale=yyy)");
  return transform(g, Homothety(_center=c, _scale=factor));
}
//! apply a homothety on a Geom (template external)
template<class Geom> Geom homothetize(const Geom& g, real_t factor)
{
  warning("deprecated", "homothetize(Geom, Real)", "homothetize(Geom, _scale=xxx)");
  return transform(g, Homothety(_scale=factor));
}
//! apply a point reflection on a Geom (1 key)  (template external)
template<class Geom> Geom pointReflect(const Geom& g, const Parameter& p1)
{ return transform(g, PointReflection(p1)); }
//! apply a point reflection on a Geom (template external)
template<class Geom> Geom pointReflect(const Geom& g, const Point& c = Point(0.,0.,0.))
{
  warning("deprecated", "pointReflect(Geom, Real)", "pointReflect(Geom, _center=xxx)");
  return transform(g, PointReflection(_center=c));
}
//! apply a reflection 2d on a Geom (1 key) (template external)
template<class Geom> Geom reflect2d(const Geom& g, const Parameter& p1)
{ return transform(g, Reflection2d(p1)); }
//! apply a reflection 2d on a Geom (2 keys) (template external)
template<class Geom> Geom reflect2d(const Geom& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Reflection2d(p1, p2)); }
//! apply a reflection 2d on a Geom (template external)
template<class Geom> Geom reflect2d(const Geom& g, const Point& c = Point(0.,0.),
                                     std::vector<real_t> d = std::vector<real_t>(2,0.))
{
  warning("deprecated", "reflect2d(Geom, Point, Reals)", "reflect2d(Geom, _center=xxx, _direction=yyy)");
  return transform(g, Reflection2d(_center=c, _direction=d));
}
//! apply a reflection 2d on a Geom (template external)
template<class Geom> Geom reflect2d(const Geom& g, const Point& c, real_t dx, real_t dy = 0.)
{
  warning("deprecated", "reflect2d(Geom, Point, Real, Real)", "reflect2d(Geom, _center=xxx, _direction={dx, dy})");
  return transform(g, Reflection2d(_center=c, _direction={dx, dy}));
}
//! apply a reflection 3d on a Geom (1 key) (template external)
template<class Geom> Geom reflect3d(const Geom& g, const Parameter& p1)
{ return transform(g, Reflection3d(p1)); }
//! apply a reflection 3d on a Geom (2 keys) (template external)
template<class Geom> Geom reflect3d(const Geom& g, const Parameter& p1, const Parameter& p2)
{ return transform(g, Reflection3d(p1, p2)); }
//! apply a reflection 3d on a Geom (template external)
template<class Geom> Geom reflect3d(const Geom& g, const Point& c = Point(0.,0.,0.),
                                     std::vector<real_t> n = std::vector<real_t>(3,0.))
{
  warning("deprecated", "reflect3d(Geom, Point, Reals)", "reflect3d(Geom, _center=xxx, _normal=yyy)");
  return transform(g, Reflection3d(_center=c, _normal=n));
}
//! apply a reflection 3d on a Geom (template external)
template<class Geom> Geom reflect3d(const Geom& g, const Point& c, real_t nx, real_t ny, real_t nz = 0.)
{
  warning("deprecated", "reflect3d(Geom, Point, Real, Real, Real)", "reflect2d(Geom, _center=xxx, _normal={nx, ny, nz})");
  return transform(g, Reflection3d(_center=c, _normal={nx, ny, nz}));
}

} // end of namespace xlifepp

#endif // GEOMETRIES_UTILS_HPP
