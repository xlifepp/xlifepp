/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ARInterface.hpp
  \author Y. Lafranche
  \since 23 Nov 2015
  \date  08 Dec 2015

  \brief Definition of the xlifepp::ARInterface class

 The class xlifepp::ARInterface is an interface class between XLiFE++ and Arpack++
 designed to enable the use of Arpack++'s own classes to solve eigenvalue problems
 in the context of a XLiFE++ program.

 This class is an abstract base class so that the classes to be used in the program
 are the classes that are derived from this one (see 2. below).

 1. Arpack++ classes
    ----------------
 The Arpack++ classes intended to be used are those that require user-defined
 matrix-vector products (see documentation file arpack++/doc/arpackpp.doc.gz
 in the archive available at http://www.ime.unicamp.br/~chico/arpack++/).
 They are: ARSymStdEig, ARNonSymStdEig, ARCompStdEig, ARSymGenEig, ARNonSymGenEig
            and ARCompGenEig.

 Thus, several public functions MultXXX are provided to compute the different
 matrix-vector products needed by the Arpack++ classes. For that, some XLiFE++
 own functions are used.

 2. Interface classes between XLiFE++ and Arpack++
    -----------------------------------------------
 Several user interface classes have been created, allowing to use Arpack++
 classes in a rather straightforward way. They are:
  - ARInterfaceStdReg and ARInterfaceStdShf for standard eigenvalue problems,
  - ARInterfaceGenSymReg, ARInterfaceGenReg, ARInterfaceGenShf, ARInterfaceGenBuc,
    ARInterfaceGenCay and ARInterfaceGenCsh for generalized eigenvalue problems.

 The hierarchy of the classes is the following:
 (ARInterface) > 1. ARInterfaceStdReg
               > 2. ARInterfaceStdShf
               >   (ARInterfaceGen)  > 1. ARInterfaceGenSymReg
                                     > 2. ARInterfaceGenReg
                                     > 3. ARInterfaceGenShf
                                     > 4. ARInterfaceGenBuc
                                     > 5. ARInterfaceGenCay
                                     > 6. ARInterfaceGenCsh
 ARInterface and ARInterfaceGen are abstract classes.

 Given an Arpack++ class and a computational mode, the user has to:
  a. create a ARInterfaceXXX object defining the matrices involved,
  b. create an Arpack++ object defining the problem and using the previous object
     to specify the matrix-vector products needed.
 The name of the matrix-vector product functions are the generic names used in the
 Arpack++ documentation so that the user's task reduces nearly to a copy-paste
 operation. Those names are MultOPx, MultAx and MultBx.

 3. Link between classes
    -------------------
 Here follows the list of the Arpack++ classes along with the name of the matrix-vector
 product functions and the interface constructor to be used according to the nature of
 the problem. The arguments of the interface constructors are given under each of them
 in a short way ; some of them have two forms.

 * Standard eigenvalue problem Au = l u

===================================================================================
 Kind of problem  Computational  Matrix-vector        Name of member    Interface
 (Arpack++ class)      mode      products required    functions to use  constructor
===================================================================================
 real symmetric  |
 (ARSymStdEig)   | Regular       y <- A * x             MultOPx      ARInterfaceStdReg
                 |                                                   (1. A)
 real non        |-----------------------------------------------------------------
 symmetric       |
 (ARNonSymStdEig)| Shift
                 | and invert    y <- inv(A-sigma I)*x  MultOPx      ARInterfaceStdShf
 complex         |                                                   (2. A_s, AsI)
 (ARCompStdEig)  |
===================================================================================

 * Generalized eigenvalue problem Au = l Bu
 The matrix A defines the kind of problem.
 If A is real, the matrix B is required to be real symmetric positive semi-definite,
 except in regular mode where it should be positive definite. In bukling mode, the
 real symmetric matrix A is required to be positive semi-definite while B is only
 required to be real symmetric indefinite.
 If A is complex, the matrix B is required to be hermitian positive semi-definite,
 except in regular mode where it should be positive definite. Notice that B may still
 be real and symmetric.

===================================================================================
 Kind of problem  Computational  Matrix-vector        Name of member    Interface
 (Arpack++ class)      mode      products required    functions to use  constructor
===================================================================================
                 | Regular       y <- inv(B)*A * x ;    MultOPx      ARInterfaceGenSymReg
                 |                      x <- A * x                   (1. A_B)
                 |               y <- B * x             MultBx
                 |-----------------------------------------------------------------
                 | Shift         y <- inv(A-sigma B)*x  MultOPx      ARInterfaceGenShf
 real symmetric  | and invert    y <- B * x             MultBx       (3. A_B_s, B_AsB)
 (ARSymGenEig)   |-----------------------------------------------------------------
                 | Buckling      y <- inv(A-sigma B)*x  MultOPx      ARInterfaceGenBuc
                 |               y <- A * x             MultBx       (4. A_B_s)
                 |-----------------------------------------------------------------
                 |               y <- inv(A-sigma B)*x  MultOPx      ARInterfaceGenCay
                 | Cayley        y <- A * x             MultAx       (5. A_B_s)
                 |               y <- B * x             MultBx
===================================================================================
                 | Regular       y <- inv(B)*A * x      MultOPx      ARInterfaceGenReg
                 |               y <- B * x             MultBx       (2. A_B)
                 |-----------------------------------------------------------------
 real non        | Real shift    y <- inv(A-sigma B)*x  MultOPx      ARInterfaceGenShf
 symmetric       | and invert    y <- B * x             MultBx       (3. A_B_s, B_AsB)
 (ARNonSymGenEig)|-----------------------------------------------------------------
                 | Complex shift y <- Inv(A-sigma B)*x  MultOPx      ARInterfaceGenCsh
                 | and invert    y <- A * x             MultAx       (6. A_B_p_sr_si)
                 |               y <- B * x             MultBx
                 | Nota: Inv(A-sigma B) is real{inv(A-sigma B)} or imag{inv(A-sigma B)}
===================================================================================
                 | Regular       y <- inv(B)*A * x      MultOPx      ARInterfaceGenReg
                 |               y <- B * x             MultBx       (2. A_B)
 complex         |-----------------------------------------------------------------
 (ARCompGenEig)  | Shift         y <- inv(A-sigma B)*x  MultOPx      ARInterfaceGenShf
                 | and invert    y <- B * x             MultBx       (3. A_B_s, B_AsB)
===================================================================================

 4.Usage
   -----
 In the file where the Arpack++ object is to be defined, one should include this file,
 which is done by the mean of another header file present in XLiFE++'s distribution:

 ../largeMatrix/LargeMatrix.hpp

 (or any header including this file directly or indirectly)

 and additionally, at least one of the Arpack++ header files:

 arssym.h     // for ARSymStdEig
 argsym.h     // for ARSymGenEig

 arsnsym.h    // for ARNonSymStdEig
 argnsym.h    // for ARNonSymGenEig

 arscomp.h    // for ARCompStdEig
 argcomp.h    // for ARCompGenEig

 For example, if we want to solve a real symmetric generalized problem in buckling
 mode (Arpack++ class ARSymGenEig), we will have to specify two matrix-vector products.
 The first product must compute inv(A-sigma B)*x and corresponds to the function
 called MultOPx. The second product must compute  A * x and corresponds to the function
 called MultBx (this is not an error, in all other cases MultBx is supposed to compute
 B * x). In the documentation, the description of the constructor in shift and invert
 and buckling modes is:
  ARSymGenEig(char InvertMode, int n, int nev,
              FOP* objOP, TypeOPx MultOPx,
              FB*  objB , TypeBx MultBx  , FLOAT sigma, ... + optional parameters ...)

 Thus, in our program, once matrices A and B have been defined, and assuming A is real
 symmetric positive semi-definite and B is symmetric, we'll have to write something like
 the following:

//   Define matrices A and B:
  TermMatrix A(...), B(...);
//   Turns A and B in internal scalar representation and returns a pointer to data:
  MatrixEntry* meA_p = A.matrixData(), * meB_p = B.matrixData();

//   Define the interface objet, choosing Buckling mode:
  real_t sigma = ...;
  typedef ARInterfaceGenBuc< LargeMatrix<real_t> > IntObj_t;
  IntObj_t IntObj(meA_p->getLargeMatrix<real_t>(), meB_p->getLargeMatrix<real_t>(), sigma);

//   Define Arpack++ problem:
  int n = IntObj.GetN(); // Dimension of the problem
  int nev = ...;         // Number of eigenvalues to be computed
  ARSymGenEig<real_t, IntObj_t, IntObj_t >
   ArProb('B', n, nev, &IntObj, &IntObj_t::MultOPx,
                       &IntObj, &IntObj_t::MultBx, sigma);

 Then the eigenvalues and eigenvectors can be computed and used:

//    Compute eigenvalues and eigenvectors
  int nconv = ArProb.FindEigenvectors();

//    Extract ith eigenvalue and eigenvector, 0 <= i < nconv
  for (int i = 0; i < nconv; i++) {
    real_t lambda = ArProb.Eigenvalue(i);
    real_t* v = ArProb.RawEigenvector(i);
    // use lambda and v[k], k = 0, 1,... n-1.
  }

 5. Warning
    -------
 The interface object and the Arpack++ problem are two objects which are tightly linked
 and thus should remain coherent.

 Once the Arpack++ problem is fully defined, the parameters describing the problem should
 not be modified. Thus, in particular, the Arpack++ functions ChangeShift and NoShift
 MUST NOT BE USED because the matrix-vector product (defined in the interface object)
 would not reflect the change of the shift. For the same reason, the SetXXXMode functions,
 which are intended to define the problem starting from an object built by the default
 constructor, should not be used in this context.
 The functions ChangeMultBx and ChangeMultOPx should not be used either since the
 matrix-vector product functions are provided by the interface object.

 If the shift has to be changed, then the user MUST create a new interface object
 (ARInterfaceXXX object) and use it to define a new Arpack++ problem.

 Take note however that the SetXXXMode functions can be used in the definition phase of
 the problem. After the Arpack++ problem object has been defined, one can modify all the
 other non critical parameters by the mean of the functions ChangeMaxit, ChangeNcv,
 ChangeNev, ChangeTol and ChangeWhich.

More examples
-------------
 Examples showing how to precisely manage the different objects in the different
 cases can be found in the file arpackSolve.cpp.

*/

#ifndef ARINTERFACE_H
#define ARINTERFACE_H

#include "ArpackProb.hpp"
#include "../largeMatrix/LargeMatrix.hpp"
#include <vector>

/*
  Every computational mode, except regular mode for a standard eigenvalue problem,
  requires the computation of M^-1 * x, which reduces to solve a linear system.
  In order to speed up this computation, which is done many times with the same matrix M,
  a factorisation of M is computed once and stored.
  The matrix M can be B, A - sigma*I or A - sigma*B according to the computation mode.
  Thus, in every class derived from ARInterface where a factorisation is needed, there
  exist two data members whose definition is:
  //! Factorization of the multOPx matrix in order to speed up linear systems solving
  M_ *fact_p;
  //! Linear system solver using this factorization
  void (M_::*Solver_)(vector<K_>& b, vector<K_>& x) const;

  These data members are initialized by a member function:
  //! Compute the factorisation of the MultOPx matrix and select the solver
  void factorize();

  In every class derived from ARInterface where a shift is needed, there exist
  a data member whose definition is:
  //! A - sigma B, for non regular modes in generalized problems
  //  (B is I for non regular mode in standard problems)
  const M_ *matAsB_p;
*/


#ifdef XLIFEPP_WITH_UMFPACK
  #include "umfpackSupport.h"
#endif

namespace xlifepp {

#ifdef XLIFEPP_WITH_UMFPACK
  template<class M_>
  struct FactPtrTrait{ typedef UmfPackLU<M_> *FactPtr; };
  template<class K_, class M_>
  struct SolverPtrTrait{ typedef void (UmfPackLU<M_>::*SolverPtr)(const std::vector<K_>& b, std::vector<K_>& x) const; };
#else
  template<class M_>
  struct FactPtrTrait{ typedef M_ *FactPtr; };
  template<class K_, class M_>
  struct SolverPtrTrait{ typedef void (M_::*SolverPtr)(std::vector<K_>& b, std::vector<K_>& x) const; };
#endif

/*
   Utility functions devoted to the allocation of a temporary matrix
   created when a shifted mode is used.
*/
//! Allocation of temporary matrix A - s Id
template<class K_, class MA_>
MA_* allocAsI(const MA_& matA, const K_ sigma) {
  if(sigma==K_(0)) return new MA_(matA);  // only copy of matA
  MA_* matAsI_p = new MA_(_idMatrix, matA.storageType(), matA.accessType(), matA.nbRows, matA.nbCols, -sigma);
  matAsI_p->toStorage(matA.storagep());
  *matAsI_p += const_cast<MA_&>(matA);
  return matAsI_p;
}
//! Allocation of temporary matrix A - s B
template<class K_, class MAsB_, class MB_>
MAsB_* allocAsB(const MAsB_& matA, const MB_& matB, const K_ sigma) {
  if(sigma==K_(0)) return new MAsB_(matA);  // only copy of matA
  MAsB_* sB_p = new MAsB_(matB);
  *sB_p *= -sigma;
  MAsB_* matAsB_p = addMatrixMatrixSkyline(matA, *sB_p);
  delete sB_p;
  return matAsB_p;
}
template<class MAB_, class MAsB_>
MAsB_* allocAsB(const MAB_& matA, const MAB_& matB, const complex_t sigma) {
  if(sigma==complex_t(0)) return new MAsB_(matA);  // only copy of matA
  MAsB_* sB_p = new MAsB_(matB);
  *sB_p *= -sigma;
  MAsB_* A_p = new MAsB_(matA);
  MAsB_* matAsB_p = addMatrixMatrixSkyline(*A_p, *sB_p);
  delete A_p;
  delete sB_p;
  return matAsB_p;
}

/*
   Utility function that warns if the condition number of the factorized matrix
   matName, pointed to by fact_p, seems too large.
*/
template<class M_p>
void checkCond(M_p fact_p, string_t matName) {
  real_t cond = fact_p->cond();
  if(cond > 1.0E+8) { warning("ill_cond", matName, tostring(cond)); }
}

/*!
  \class ARInterface
  \tparam K_ type of elements
  \tparam MA_ type of the matrix
*/
template<class K_, class MA_>
class ARInterface { // MA_ is the type of the matrix A, whose elements should be of type K_
public:
/*
 -------------------------------------------------------------------------------
  Constructors, Destructor
 -------------------------------------------------------------------------------
*/
  //! A constructor
  ARInterface(const MA_& matA);

  //! default constructor (A is not directly used)
  ARInterface();

  //! destructor
  virtual ~ARInterface(){}

/*
 -------------------------------------------------------------------------------
  Other public member functions
 -------------------------------------------------------------------------------
*/
  //! main matrix-vector product required for all derived classes
  virtual void MultOPx (K_ *x, K_ *y) = 0;

  //! dimension of the problem to be solved (dimension of the square matrix A)
  number_t GetN() const { return dim_; }

  /*! return the name of the factorization used internally to solve linear
     systems, when the computation mode requires it. */
  std::string KindOfFactorization() const { return fact_name_; }

  //! return the name of the interface object and constructor used
  std::string ConstructorName() const { return ctor_name_; }

protected:
  //! left hand-side matrix A, e.g. stiffness matrix (given data)
  const MA_ *matA_p;

  //! dimension of the matrix A
  number_t dim_;

  //! name of the algorithm used to compute the factorization pointed to by fact_p
  std::string fact_name_;

  //! name of the interface object and constructor used
  std::string ctor_name_;

  //! work vectors used in MultOPx, resized to dim_ in constructors
  std::vector<K_> lx, ly;
/*
 -------------------------------------------------------------------------------
  Protected member functions
 -------------------------------------------------------------------------------
*/
  //! Matrix-vector product y <- A * x
  void bz_MultAx  (K_ *x, K_ *y);

  //! Check dimensions of the matrice mat
  template<class M_>
  void checkDims(const M_* mat);

  //! resize work vectors
  void resizewv() { lx.resize(dim_); ly.resize(dim_); }
}; // end of Class ARInterface ================================================


/* ****************************************************
  Classes for the standard eigenvalue problem Au = l u
  **************************************************** */

/*!
  \class ARInterfaceStdReg
  Class for standard eigenvalue problem in regular mode
  Matrix-vector product provided: y <- A * x
*/
template<class K_, class MA_>
class ARInterfaceStdReg : public ARInterface<K_, MA_> {
public:

  //! A constructor for standard eigenvalue problem in regular mode
  ARInterfaceStdReg(const MA_& matA);

  //! matrix-vector product y <- A * x
  void MultOPx (K_ *x, K_ *y); // override

private:
  //! Duplicating such object is disabled
  ARInterfaceStdReg(const ARInterfaceStdReg&);
  ARInterfaceStdReg & operator=(const ARInterfaceStdReg&);

}; // end of Class ARInterfaceStdReg ============================================

/*!
  \class ARInterfaceStdShf
  Class for standard eigenvalue problem in shift and invert mode
  Matrix-vector product provided: y <- inv(A-sigma*Id) * x
*/
template<class K_, class MA_>
class ARInterfaceStdShf : public ARInterface<K_, MA_> {
public:

  /*! A_s constructor for standard problem in shift and invert mode.
      sigma is the shift.
      The matrix A-sigma*Id is computed internally and stored. */
  ARInterfaceStdShf(const MA_& matA, const K_ sigma);

  /*! AsI constructor for standard problem in shift and invert mode.
      The input matrix matAsI corresponds to A-sigma*Id and must be provided
      by the user. */
  ARInterfaceStdShf(const MA_& matAsI);

  //! matrix-vector product y <- inv(A-sigma*Id) * x
  void MultOPx (K_ *x, K_ *y); // override

  //! destructor
  ~ARInterfaceStdShf(); // override

private:
  /*! A - sigma Id, for shift and invert mode in standard problems
      Built temporarily from given data A and sigma by the first constructor. */
  const MA_ *matAsI_p;

  typename FactPtrTrait<MA_>::FactPtr fact_p;
  typename SolverPtrTrait<K_, MA_>::SolverPtr Solver_;

  //! Compute the factorisation of *matAsI_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARInterfaceStdShf(const ARInterfaceStdShf&);
  ARInterfaceStdShf & operator=(const ARInterfaceStdShf&);

}; // end of Class ARInterfaceStdShf ============================================


/* ********************************************************
  Classes for the generalized eigenvalue problem Au = l Bu
  ******************************************************** */

/*!
  \class ARInterfaceGen
  Abstract class for generalized eigenvalue problem
  Matrix-vector product provided: y <- B * x
*/
template<class K_, class MA_, class MB_>
class ARInterfaceGen : public ARInterface<K_, MA_> {
public:

  //! A_B constructor
  ARInterfaceGen(const MA_& matA, const MB_& matB);

  //! B constructor (matrix A is not directly used)
  ARInterfaceGen(const MB_& matB);

  /*! matrix-vector product y <- B * x required for all derived classes
      except for ARInterfaceGenBuc which needs y <- A * x. This is why
      this function is declared virtual.
      When the problem is complex, x and y are complex but B is real. */
  virtual void MultBx (K_ *x, K_ *y);

protected:
  //! right hand-side matrix B, e.g. mass matrix (given data)
  const MB_ *matB_p;

private:
  //! Duplicating such object is disabled
  ARInterfaceGen(const ARInterfaceGen&);
  ARInterfaceGen & operator=(const ARInterfaceGen&);

}; // end of Class ARInterfaceGen ===============================================

/*!
  \class ARInterfaceGenSymReg
  Class for real symmetric generalized eigenvalue problem in regular mode
  Matrix-vector product provided: y <- inv(B)*A * x and y <- B * x
*/
template<class MAB_>
class ARInterfaceGenSymReg : public ARInterfaceGen<real_t, MAB_, MAB_> {
public:

  /*! A_B constructor
      A real symmetric, B real symmetric positive definite */
  ARInterfaceGenSymReg(const MAB_& matA, const MAB_& matB);

  //! matrix-vector products y <- inv(B)*A * x and x <- A * x
  void MultOPx (real_t *x, real_t *y); // override

  //! destructor
  ~ARInterfaceGenSymReg(); // override

private:
  std::vector<real_t> Ax; // work vector used in MultOPx, resized to dim_ in constructor
  typename FactPtrTrait<MAB_>::FactPtr fact_p;
  typename SolverPtrTrait<real_t, MAB_>::SolverPtr Solver_;

  //! Compute the factorisation of *matB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARInterfaceGenSymReg(const ARInterfaceGenSymReg&);
  ARInterfaceGenSymReg & operator=(const ARInterfaceGenSymReg&);

}; // end of Class ARInterfaceGenSymReg =========================================

/*!
  \class ARInterfaceGenReg
  Class for generalized eigenvalue problem in regular mode
  except in the real symmetric case
  Matrix-vector product provided: y <- inv(B)*A * x and y <- B * x
*/
template<class K_, class MA_, class MB_>
class ARInterfaceGenReg : public ARInterfaceGen<K_, MA_, MB_> {
public:

  /*! A_B constructor
      B symmetric positive definite */
  ARInterfaceGenReg(const MA_& matA, const MB_& matB);

  //! matrix-vector product y <- inv(B)*A * x
  void MultOPx (K_ *x, K_ *y); // override

  //! destructor
  ~ARInterfaceGenReg(); // override

private:
  std::vector<K_> Ax; // work vector used in MultOPx, resized to dim_ in constructor
  typename FactPtrTrait<MB_>::FactPtr fact_p;
  typename SolverPtrTrait<K_, MB_>::SolverPtr Solver_;

  //! Compute the factorisation of *matB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARInterfaceGenReg(const ARInterfaceGenReg&);
  ARInterfaceGenReg & operator=(const ARInterfaceGenReg&);

}; // end of Class ARInterfaceGenReg ============================================

/*!
  \class ARInterfaceGenShf
  Class for generalized eigenvalue problem in shift and invert mode
  Real symmetric, real non symmetric and complex cases.
  Here, A and sigma have the same type.
  The special case real non symmetric with complex sigma is treated in class ARInterfaceGenCsh.
  Matrix-vector products provided: y <- inv(A-sigma B)*x and y <- B * x
*/
template<class K_, class MA_, class MB_>
class ARInterfaceGenShf : public ARInterfaceGen<K_, MA_, MB_> {
public:

  /*! A_B_s constructor
      B symmetric positive semi-definite */
  ARInterfaceGenShf(const MA_& matA, const MB_& matB, const K_ sigma);

  /*! B_AsB constructor
      The input matrix matAsB corresponds to A-sigma*B and must be provided
      by the user. */
  ARInterfaceGenShf(const MB_& matB, const MA_& matAsB);

  //! matrix-vector product y <- inv(A-sigma B)*x
  void MultOPx (K_ *x, K_ *y); // override

  //! destructor
  ~ARInterfaceGenShf(); // override

private:
  /*! A - sigma B
      Built temporarily from the given data A, sigma and B by the first constructor. */
  const MA_ *matAsB_p;

  typename FactPtrTrait<MA_>::FactPtr fact_p;
  typename SolverPtrTrait<K_, MA_>::SolverPtr Solver_;

  //! Compute the factorisation of *matAsB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARInterfaceGenShf(const ARInterfaceGenShf&);
  ARInterfaceGenShf & operator=(const ARInterfaceGenShf&);

}; // end of Class ARInterfaceGenShf ============================================

/*!
  \class ARInterfaceGenBuc
  Class for generalized eigenvalue problem in buckling mode
  Real symmetric case.
  Matrix-vector products provided: y <- inv(A-sigma B)*x and y <- A * x
*/
template<class MAB_>
class ARInterfaceGenBuc : public ARInterfaceGen<real_t, MAB_, MAB_> {
public:

  /*! A_B_s constructor
      A symmetric positive semi-definite
      B symmetric indefinite */
  ARInterfaceGenBuc(const MAB_& matA, const MAB_& matB, const real_t sigma);

  //! matrix-vector product y <- inv(A-sigma B)*x
  void MultOPx (real_t *x, real_t *y); // override

  //! matrix-vector product y <- A * x
  void MultBx (real_t *x, real_t *y); // override

  //! destructor
  ~ARInterfaceGenBuc(); // override

private:
  /*! A - sigma B
      Built temporarily from the given data A, sigma and B. */
  const MAB_ *matAsB_p;

  typename FactPtrTrait<MAB_>::FactPtr fact_p;
  typename SolverPtrTrait<real_t, MAB_>::SolverPtr Solver_;

  //! Compute the factorisation of *matAsB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARInterfaceGenBuc(const ARInterfaceGenBuc&);
  ARInterfaceGenBuc & operator=(const ARInterfaceGenBuc&);

}; // end of Class ARInterfaceGenBuc ============================================

/*!
  \class ARInterfaceGenCay
  Class for generalized eigenvalue problem in Cayley mode
  Real symmetric case.
  Matrix-vector products provided: y <- inv(A-sigma B)*x, y <- A * x and y <- B * x
*/
template<class MAB_>
class ARInterfaceGenCay : public ARInterfaceGen<real_t, MAB_, MAB_> {
public:

  /*! A_B_s constructor
      A symmetric
      B symmetric positive semi-definite */
  ARInterfaceGenCay(const MAB_& matA, const MAB_& matB, const real_t sigma);

  //! matrix-vector product y <- inv(A-sigma B)*x
  void MultOPx (real_t *x, real_t *y); // override

  //! matrix-vector product y <- A * x
  void MultAx (real_t *x, real_t *y);

  //! destructor
  ~ARInterfaceGenCay(); // override

private:
  /*! A - sigma B
      Built temporarily from the given data A, sigma and B. */
  const MAB_ *matAsB_p;

  typename FactPtrTrait<MAB_>::FactPtr fact_p;
  typename SolverPtrTrait<real_t, MAB_>::SolverPtr Solver_;

  //! Compute the factorisation of *matAsB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARInterfaceGenCay(const ARInterfaceGenCay&);
  ARInterfaceGenCay & operator=(const ARInterfaceGenCay&);

}; // end of Class ARInterfaceGenCay ============================================

/*!
  \class ARInterfaceGenCsh
  Class for generalized eigenvalue problem in complex shift and invert mode
  Real non symmetric case with complex shift.
  Matrix-vector products provided: y <- Re/Im{inv(A-sigma B)}*x, y <- A * x and y <- B * x
*/
template<class MAB_, class MAsB_>
class ARInterfaceGenCsh : public ARInterfaceGen<real_t, MAB_, MAB_> {
public:

  /*! A_B_p_sr_si constructor
      A real non symmetric
      B symmetric positive semi-definite */
  ARInterfaceGenCsh(const MAB_& matA, const MAB_& matB, const char part,
                    const real_t sigmaR, const real_t sigmaI);

  /*! matrix-vector product y <- real{inv(A-sigma B)} * x if part = 'R'
                            y <- imag{inv(A-sigma B)} * x if part = 'I'
                            The system is solved in complex arithmetic. */
  void MultOPx (real_t *x, real_t *y); // override

  //! matrix-vector product y <- A * x
  void MultAx (real_t *x, real_t *y);

  //! destructor
  ~ARInterfaceGenCsh(); // override

private:
  /*! A - sigma B
      Built temporarily from the given data A, sigma and B. */
  const MAsB_ *matAsB_p;
  /*! true if real part of the result vector is to be used, false if the
      imaginary part is to be used */
  bool realPart_;

  //! complex work vectors used in MultOPx, resized to dim_ in constructor
  std::vector<complex_t> clx, cly;

  typename FactPtrTrait<MAsB_>::FactPtr fact_p;
  /*! Here, the computation of inv(A-sigma B)*x should be done in complex arithmetic.
      Then, we take the real or imaginary part of the result according to part argument. */
  typename SolverPtrTrait<complex_t, MAsB_>::SolverPtr Solver_;

  //! Compute the factorisation of *matAsB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARInterfaceGenCsh(const ARInterfaceGenCsh&);
  ARInterfaceGenCsh & operator=(const ARInterfaceGenCsh&);

}; // end of Class ARInterfaceGenCsh ============================================


// Declaration of some external utility functions

//! Create a new matrix (especially a LargeMatrix) with a skyline storage
template<class M_>
M_ *newSkyline(const M_ *mat_p);

//! Convert array of K_ to vector
template<class K_>
void array2Vector(const K_* source, std::vector<K_>& vec);

//! Convert vector to an array of K_
template<class K_>
void vector2Array(std::vector<K_>& vec, K_* target);

/*
 -------------------------------------------------------------------------------
   Classes for SVD computation
   The hierarchy of the classes is the following:
   ARStdFrame<real_t> > ARInterfaceRSVD > ARInterfaceRSVDmGEnReg
                                        > ARInterfaceRSVDmLTnReg
                                        > ARInterfaceRSVDmGEnShf
                                        > ARInterfaceRSVDmLTnShf
   ARStdFrame<complex_t> > ARInterfaceCSVD > ARInterfaceCSVDmGEnReg
                                           > ARInterfaceCSVDmLTnReg
                                           > ARInterfaceCSVDmGEnShf
                                           > ARInterfaceCSVDmLTnShf
 -------------------------------------------------------------------------------
*/
template <typename T>
class LargeMatrix;
/*!
  \class ARInterfaceRSVD
  The ARInterfaceRSVD class handle real data.
  Given a real matrix A and a real vector x, this class defines the products:
   . (At) * A * x and A * (At) * x, for computation near the ends of the spectrum,
   . ((At) * A - s * Id) * x and (A * (At) - s * Id) * x, for computation near a real shift s.
*/
class ARInterfaceRSVD : public ARStdFrame<real_t> {
public:
   //! constructor
   ARInterfaceRSVD(TermMatrix& A, int dimn, int dimx, std::string cname, const real_t sigma = 0.);

   //! destructor
   ~ARInterfaceRSVD(){ }

   //! returns matrix pointer
   const LargeMatrix<real_t>* matPtr() const { return matA_p; }

protected:
   //! pointers to internal data objects
   const LargeMatrix<real_t> *matA_p;

   //! shift used to search for singular values near a prescribed number
   const real_t shift_;

   //! work vector used in matrix-vector products, resized to dimension of the problem in constructor
   std::vector<real_t> Tx;

}; // end of Class ARInterfaceRSVD =========================================

class ARInterfaceRSVDmGEnReg : public ARInterfaceRSVD {
public:
   //! constructor
   ARInterfaceRSVDmGEnReg(TermMatrix& A, int dimn, int dimx)
   : ARInterfaceRSVD(A, dimn, dimx, "ARInterfaceRSVDmGEnReg") {}

  //! matrix-vector product: y <- (At) * A * x = A' * A * x
   void MultOPx (real_t *x, real_t *y);

}; // end of Class ARInterfaceRSVDmGEnReg =========================================

class ARInterfaceRSVDmLTnReg : public ARInterfaceRSVD {
public:
   //! constructor
   ARInterfaceRSVDmLTnReg(TermMatrix& A, int dimn, int dimx)
   : ARInterfaceRSVD(A, dimn, dimx, "ARInterfaceRSVDmLTnReg") {}

  //! matrix-vector product: y <- A * (At) * x = A * A' * x
   void MultOPx (real_t *x, real_t *y);

}; // end of Class ARInterfaceRSVDmLTnReg =========================================

class ARInterfaceRSVDmGEnShf : public ARInterfaceRSVD {
public:
   //! constructor
   ARInterfaceRSVDmGEnShf(TermMatrix& A, int dimn, int dimx, const real_t sigma)
   : ARInterfaceRSVD(A, dimn, dimx, "ARInterfaceRSVDmGEnShf", sigma) {}

  //! matrix-vector product: y <- ((At) * A - s * Id) * x = (A' * A - s * Id) * x
   void MultOPx (real_t *x, real_t *y);

}; // end of Class ARInterfaceRSVDmGEnShf =========================================

class ARInterfaceRSVDmLTnShf : public ARInterfaceRSVD {
public:
   //! constructor
   ARInterfaceRSVDmLTnShf(TermMatrix& A, int dimn, int dimx, const real_t sigma)
   : ARInterfaceRSVD(A, dimn, dimx, "ARInterfaceRSVDmLTnShf", sigma) {}

  //! matrix-vector product: y <- (A * (At) - s * Id) * x = (A * A' - s * Id) * x
   void MultOPx (real_t *x, real_t *y);

}; // end of Class ARInterfaceRSVDmLTnShf =========================================

/*!
  \class ARInterfaceCSVD
  The ARInterfaceCSVD class handle complex data.
  Given a complex matrix A and a complex vector x, this class defines the products:
   . (A*) * A * x and A * (A*) * x, for computation near the ends of the spectrum,
   . ((A*) * A - s * Id) * x and (A * (A*) - s * Id) * x, for computation near a real shift s.
*/
class ARInterfaceCSVD : public ARStdFrame<complex_t> {
public:
   //! constructor
   ARInterfaceCSVD(TermMatrix& A, int dimn, int dimx, std::string cname, const real_t sigma = 0.);

   //! destructor
   ~ARInterfaceCSVD(){ }

   //! returns matrix pointer
   const LargeMatrix<complex_t>* matPtr() const { return matA_p; }

protected:
   //! pointers to internal data objects
   const LargeMatrix<complex_t> *matA_p;

   //! shift used to search for singular values near a prescribed number
   const real_t shift_;

   //! work vector used in matrix-vector products, resized to dimension of the problem in constructor
   std::vector<complex_t> Tx;

}; // end of Class ARInterfaceCSVD =========================================

class ARInterfaceCSVDmGEnReg : public ARInterfaceCSVD {
public:
   //! constructor
   ARInterfaceCSVDmGEnReg(TermMatrix& A, int dimn, int dimx)
   : ARInterfaceCSVD(A, dimn, dimx, "ARInterfaceCSVDmGEnReg") {}

  //! matrix-vector product: y <- (A*) * A * x = A' * A * x
   void MultOPx (complex_t *x, complex_t *y);

}; // end of Class ARInterfaceCSVDmGEnReg =========================================

class ARInterfaceCSVDmLTnReg : public ARInterfaceCSVD {
public:
   //! constructor
   ARInterfaceCSVDmLTnReg(TermMatrix& A, int dimn, int dimx)
   : ARInterfaceCSVD(A, dimn, dimx, "ARInterfaceCSVDmLTnReg") {}

  //! matrix-vector product: y <- A * (A*) * x = A * A' * x
   void MultOPx (complex_t *x, complex_t *y);

}; // end of Class ARInterfaceCSVDmLTnReg =========================================

class ARInterfaceCSVDmGEnShf : public ARInterfaceCSVD {
public:
   //! constructor
   ARInterfaceCSVDmGEnShf(TermMatrix& A, int dimn, int dimx, const real_t sigma)
   : ARInterfaceCSVD(A, dimn, dimx, "ARInterfaceCSVDmGEnShf", sigma) {}

  //! matrix-vector product: y <- ((A*) * A - s * Id) * x = (A' * A - s * Id) * x
   void MultOPx (complex_t *x, complex_t *y);

}; // end of Class ARInterfaceCSVDmGEnShf =========================================

class ARInterfaceCSVDmLTnShf : public ARInterfaceCSVD {
public:
   //! constructor
   ARInterfaceCSVDmLTnShf(TermMatrix& A, int dimn, int dimx, const real_t sigma)
   : ARInterfaceCSVD(A, dimn, dimx, "ARInterfaceCSVDmLTnShf", sigma) {}

  //! matrix-vector product: y <- (A * (A*) - s * Id) * x = (A * A' - s * Id) * x
   void MultOPx (complex_t *x, complex_t *y);

}; // end of Class ARInterfaceCSVDmLTnShf =========================================

} // end of namespace xlifepp

#endif /* ARINTERFACE_H */
