/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ARGenToStd.cpp
  \author Y. Lafranche
  \since 15 Nov 2016
  \date  15 Nov 2016

  \brief Implementation of xlifepp::ARGenToStdReg and xlifepp::ARGenToStdShf class members and related functions
*/

#include "ARGenToStd.hpp"
#include "../largeMatrix/LargeMatrix.hpp"
#include "../term/TermMatrix.hpp"

#ifdef XLIFEPP_WITH_ARPACK
  namespace xlifepp {
  // Class ARGenToStdReg =========================================
  //! Constructor
  template <class TA_, class TB_>
  ARGenToStdReg<TA_, TB_>::ARGenToStdReg(TermMatrix& A, TermMatrix& B)
  : ARStdFrame<TA_>(A), matA_p(&A.matrixData()->getLargeMatrix<TA_>()),
                        matB_p(&B.matrixData()->getLargeMatrix<TB_>())
  { Ax.resize(this->GetN()); factorize(/* matB */); }

  //! Destructor
  template <class TA_, class TB_>
  ARGenToStdReg<TA_, TB_>::~ARGenToStdReg(){ delete fact_p; }

  //! Matrix-vector product y <- inv(B)*A * x
  template <class TA_, class TB_>
  void ARGenToStdReg<TA_, TB_>::MultOPx (TA_ *x, TA_ *y) {
    array2Vector(x, this->lx);
    multMatrixVector(*matA_p, this->lx, Ax);
  // Solve linear system. Matlab equivalent: ly = matB_p \ Ax;
    (fact_p->*Solver_)(Ax, this->ly); // store the solution into ly
    vector2Array(this->ly, y);
  }

  /*! Compute the factorisation of the matrix B which should be invertible.
      The factorization is LDLt if B is symmetric, LDL* if B is complex selfadjoint
      and LU otherwise.
      Select the solver required to solve the linear system B * y = x */
  template <class TA_, class TB_>
  void ARGenToStdReg<TA_, TB_>::factorize()
  {
    typedef LargeMatrix<TB_> MB_; //using MB_ = LargeMatrix<TB_>;
    #ifdef XLIFEPP_WITH_UMFPACK
      fact_p = new UmfPackLU<MB_>(*matB_p);
      Solver_ = &UmfPackLU<MB_>::solve;
    #else
      fact_p = newSkyline(matB_p);

      SymType   st = this->matB_p->symmetry();
      if ( st == _symmetric ) {
        ldltFactorize(*fact_p);
        Solver_ = &MB_::ldltSolve;
      }
      else {
        luFactorize(*fact_p);
        Solver_ = &MB_::luSolve;
      }
    #endif
    checkCond(fact_p, "B");
  }
  template<>// Specialization when MB_ is a complex matrix
  void ARGenToStdReg<complex_t, complex_t>::factorize() {
    typedef LargeMatrix<complex_t> MB_; //using MB_ = LargeMatrix<complex_t>;
    #ifdef XLIFEPP_WITH_UMFPACK
      fact_p = new UmfPackLU<MB_>(*matB_p);
      Solver_ = &UmfPackLU<MB_>::solve;
    #else
      fact_p = newSkyline(matB_p);

      SymType   st = this->matB_p->symmetry();
      if ( st == _selfAdjoint ) {
        ldlstarFactorize(*fact_p);
        Solver_ = &MB_::ldlstarSolve;
      }
      else {
        luFactorize(*fact_p);
        Solver_ = &MB_::luSolve;
      }
    #endif
    checkCond(fact_p, "B");
  }

  // Class ARGenToStdShf =========================================
  //! Constructor
  template <class TA_, class TB_>
  ARGenToStdShf<TA_, TB_>::ARGenToStdShf(TermMatrix& A, TermMatrix& B, const TA_ sigma)
  : ARStdFrame<TA_>(A), matA_p(&A.matrixData()->getLargeMatrix<TA_>()),
                        matB_p(&B.matrixData()->getLargeMatrix<TB_>()),
    matAsB_p(allocAsB<TA_, LargeMatrix<TA_>, LargeMatrix<TB_> >(*matA_p, *matB_p, sigma))
  { factorize(/* matAsB */); delete matAsB_p; }

    //! Destructor
  template <class TA_, class TB_>
  ARGenToStdShf<TA_, TB_>::~ARGenToStdShf(){ delete fact_p; }

    //! Matrix-vector product y <- inv(inv(B)*A-sigma*Id)*x, computed as y <- inv(A-sigma*B) * (B*x)
  template <class TA_, class TB_>
  void ARGenToStdShf<TA_, TB_>::MultOPx (TA_ *x, TA_ *y) {
    array2Vector(x, this->lx);
    multMatrixVector(*matB_p, this->lx, Ax); // B*x
  // Solve linear system. Matlab equivalent: ly = matAsB_p \ Ax;
    (fact_p->*Solver_)(Ax, this->ly); // store the solution into ly
    vector2Array(this->ly, y);
  }

  /*! Compute the factorisation of the matrix M = A-sigma B
      Select the solver required to solve the linear system M * y = x */
  template <class TA_, class TB_>
  void ARGenToStdShf<TA_, TB_>::factorize() {
    typedef LargeMatrix<TA_> MA_; //using MA_ = LargeMatrix<TA_>;
    #ifdef XLIFEPP_WITH_UMFPACK
      fact_p = new UmfPackLU<MA_>(*matAsB_p);
      Solver_ = &UmfPackLU<MA_>::solve;
    #else
      fact_p = newSkyline(matAsB_p);
      luFactorize(*fact_p);
      Solver_ = &MA_::luSolve;
    #endif
    checkCond(fact_p, "(A - sigma B)");
  }

  //   Class instantiations
  template class ARGenToStdReg<real_t,    real_t>;
  template class ARGenToStdReg<complex_t, real_t>;
  template class ARGenToStdReg<complex_t, complex_t>;

  template class ARGenToStdShf<real_t,    real_t>;
  template class ARGenToStdShf<complex_t, real_t>;
  template class ARGenToStdShf<complex_t, complex_t>;

  } // end of namespace xlifepp
#endif

