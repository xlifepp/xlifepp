/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file arpackSolve.hpp
  \author Y. Lafranche
  \since 18 Jan 2016
  \date 18 Jan 2016

  \brief Header file for some external functions related to Arpack usage by users
*/
#ifndef ARPACKSOLVE_HPP
#define ARPACKSOLVE_HPP

#ifdef XLIFEPP_WITH_ARPACK
  #include "ARInterface.hpp"

  namespace xlifepp {

  class EigenElements;
  EigenElements arpackSolve(ArpackProb& ARPb);

  } // end of namespace xlifepp
#endif

#endif /* ARPACKSOLVE_HPP */
