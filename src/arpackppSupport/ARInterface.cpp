/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ARInterface.cpp
  \author Y. Lafranche
  \since 23 Nov 2015
  \date  08 Dec 2015

  \brief Implementation of xlifepp::ARInterface class members and related functions
*/

#include "ARInterface.hpp"

namespace xlifepp {

/*
 -------------------------------------------------------------------------------
   Constructors
 -------------------------------------------------------------------------------
*/
/* ***********************
     Abstract base class
   *********************** */

// 1. A constructor
template<class K_, class MA_>
ARInterface<K_, MA_>::ARInterface(const MA_& matA)
:matA_p(&matA), dim_(matA.numberOfCols()), fact_name_("No factorisation")
{ checkDims(matA_p); resizewv(); }

// 2. default constructor (A is not directly used)
template<class K_, class MA_>
ARInterface<K_, MA_>::ARInterface()
:matA_p(nullptr), dim_(0), fact_name_("No factorisation")
{}


/* **************************************
     Constructors for standard problems
   ************************************** */

// 1. Regular mode
//    A constructor
template<class K_, class MA_>
ARInterfaceStdReg<K_, MA_>::ARInterfaceStdReg(const MA_& matA)
:ARInterface<K_, MA_>(matA)
{ this->ctor_name_ = "ARInterfaceStdReg (A)"; }

// 2. Shift and invert mode
//    A_s constructor. The matrix A-sigma*Id is computed internally and stored.
template<class K_, class MA_>
ARInterfaceStdShf<K_, MA_>::ARInterfaceStdShf(const MA_& matA, const K_ sigma)
:ARInterface<K_, MA_>(matA),
 //matAsI_p(new MA_(matA + (-sigma) * identityMatrix(matA))),<- does'nt work
 matAsI_p(allocAsI(matA, sigma))
{ this->ctor_name_ = "ARInterfaceStdShf (A_s)"; factorize(/* matAsI */); delete matAsI_p; }

//    AsI constructor. The matrix A-sigma*Id is provided by the user.
template<class K_, class MA_>
ARInterfaceStdShf<K_, MA_>::ARInterfaceStdShf(const MA_& matAsI)
:ARInterface<K_, MA_>(), matAsI_p(&matAsI)
{
  this->ctor_name_ = "ARInterfaceStdShf (AsI)";
  this->dim_ = matAsI_p->numberOfCols();
  this->checkDims(matAsI_p);
  this->resizewv();
  factorize(/* matAsI */);
}

/* *****************************************
     Constructors for generalized problems
   ***************************************** */

//  0. Intermediate abstract class for generalized problems
//     A_B constructor.
template<class K_, class MA_, class MB_>
ARInterfaceGen<K_, MA_, MB_>::ARInterfaceGen(const MA_& matA, const MB_& matB)
:ARInterface<K_, MA_>(matA), matB_p(&matB)
{ this->checkDims(matB_p); }

//     B constructor.
template<class K_, class MA_, class MB_>
ARInterfaceGen<K_, MA_, MB_>::ARInterfaceGen(const MB_& matB)
:ARInterface<K_, MA_>(), matB_p(&matB)
{ this->dim_ = matB_p->numberOfCols(); this->checkDims(matB_p); this->resizewv(); }

// 1. Regular mode for real symmetric case
//    A_B constructor
template<class MAB_>
ARInterfaceGenSymReg<MAB_>::ARInterfaceGenSymReg(const MAB_& matA, const MAB_& matB)
:ARInterfaceGen<real_t, MAB_, MAB_>(matA, matB)
{ this->ctor_name_ = "ARInterfaceGenSymReg (A_B)"; Ax.resize(this->dim_); factorize(/* matB */); }

// 2. Regular mode except in the real symmetric case
//    A_B constructor
template<class K_, class MA_, class MB_>
ARInterfaceGenReg<K_, MA_, MB_>::ARInterfaceGenReg(const MA_& matA, const MB_& matB)
:ARInterfaceGen<K_, MA_, MB_>(matA, matB)
{ this->ctor_name_ = "ARInterfaceGenReg (A_B)"; Ax.resize(this->dim_); factorize(/* matB */); }

// 3. Shift and invert mode except in the real non symmetric case
//    A_B_s constructor
template<class K_, class MA_, class MB_>
ARInterfaceGenShf<K_, MA_, MB_>::ARInterfaceGenShf(const MA_& matA, const MB_& matB, const K_ sigma)
:ARInterfaceGen<K_, MA_, MB_>(matA, matB), //matAsB_p(new MA_(matA + (-sigma)*matB)), <- does'nt work
 matAsB_p(allocAsB<K_, MA_, MB_>(matA, matB, sigma))
{ this->ctor_name_ = "ARInterfaceGenShf (A_B_s)";
  factorize(/* matAsB */); delete matAsB_p; }

//    B_AsB constructor
template<class K_, class MA_, class MB_>
ARInterfaceGenShf<K_, MA_, MB_>::ARInterfaceGenShf(const MB_& matB, const MA_& matAsB)
:ARInterfaceGen<K_, MA_, MB_>(matB), matAsB_p(&matAsB)
{ this->ctor_name_ = "ARInterfaceGenShf (B_AsB)"; factorize(/* matAsB */); }

// 4. Buckling mode. Real symmetric case.
//    A_B_s constructor
template<class MAB_>
ARInterfaceGenBuc<MAB_>::ARInterfaceGenBuc(const MAB_& matA, const MAB_& matB, const real_t sigma)
:ARInterfaceGen<real_t, MAB_, MAB_>(matA, matB),
 //matAsB_p(new MAB_(matA + (-sigma)*matB)), <- does'nt work
 matAsB_p(allocAsB<real_t, MAB_, MAB_>(matA, matB, sigma))
{ this->ctor_name_ = "ARInterfaceGenBuc (A_B_s)";
  factorize(/* matAsB */); delete matAsB_p; }

// 5. Cayley mode. Real symmetric case.
//    A_B_s constructor
template<class MAB_>
ARInterfaceGenCay<MAB_>::ARInterfaceGenCay(const MAB_& matA, const MAB_& matB, const real_t sigma)
:ARInterfaceGen<real_t, MAB_, MAB_>(matA, matB),
 //matAsB_p(new MAB_(matA + (-sigma)*matB)), <- does'nt work
 matAsB_p(allocAsB<real_t, MAB_, MAB_>(matA, matB, sigma))
{ this->ctor_name_ = "ARInterfaceGenCay (A_B_s)";
  factorize(/* matAsB */); delete matAsB_p; }

// 6. Complex shift and invert mode. Real non symmetric case.
//    A_B_p_sr_si constructor
template<class MAB_, class MAsB_>
ARInterfaceGenCsh<MAB_, MAsB_>::ARInterfaceGenCsh(const MAB_& matA, const MAB_& matB, const char part,
                                                  const real_t sigmaR, const real_t sigmaI)
:ARInterfaceGen<real_t, MAB_, MAB_>(matA, matB),
 //matAsB_p(new MAsB_(matA + (-complex_t(sigmaR,sigmaI))*matB)), <- does'nt work
 matAsB_p(allocAsB<MAB_, MAsB_>(matA, matB, complex_t(sigmaR,sigmaI))), realPart_(part=='R')
{ this->ctor_name_ = "ARInterfaceGenCsh (A_B_p_sr_si)";
  clx.resize(this->dim_); cly.resize(this->dim_);
  factorize(/* matAsB */); delete matAsB_p; }
/*
 -------------------------------------------------------------------------------
 Destructors
 -------------------------------------------------------------------------------
*/

template<class K_, class MA_>
ARInterfaceStdShf<K_, MA_>::~ARInterfaceStdShf() { delete fact_p; }

template<class MAB_>
ARInterfaceGenSymReg<MAB_>::~ARInterfaceGenSymReg() { delete fact_p; }

template<class K_, class MA_, class MB_>
ARInterfaceGenReg<K_, MA_, MB_>::~ARInterfaceGenReg() { delete fact_p; }

template<class K_, class MA_, class MB_>
ARInterfaceGenShf<K_, MA_, MB_>::~ARInterfaceGenShf() { delete fact_p; }

template<class MAB_>
ARInterfaceGenBuc<MAB_>::~ARInterfaceGenBuc() { delete fact_p; }

template<class MAB_>
ARInterfaceGenCay<MAB_>::~ARInterfaceGenCay() { delete fact_p; }

template<class MAB_, class MAsB_>
ARInterfaceGenCsh<MAB_, MAsB_>::~ARInterfaceGenCsh() { delete fact_p; }

/*
 -------------------------------------------------------------------------------
   Other public member functions
 -------------------------------------------------------------------------------
*/

/* ***********************************************************
     Matrix-vector products for standard eigenvalue problems
   *********************************************************** */

// 1. Regular mode
//    Matrix-vector product y <- A * x
template<class K_, class MA_>
void ARInterfaceStdReg<K_, MA_>::MultOPx (K_ *x, K_ *y)
{ ARInterface<K_, MA_>::bz_MultAx(x,y); }

// 2. Shift and invert mode
//    Matrix-vector product y <- inv(A-sigma*I) * x
template<class K_, class MA_>
void ARInterfaceStdShf<K_, MA_>::MultOPx(K_ *x, K_ *y) {
  array2Vector(x, this->lx);
  // Solve linear system. Matlab equivalent: ly = matAsI_p \ lx;
  (fact_p->*Solver_)(this->lx, this->ly); // store the solution into ly
  vector2Array(this->ly, y);
}

/* **************************************************************
     Matrix-vector products for generalized eigenvalue problems
   ************************************************************** */

// 0. Abstract class for generalized eigenvalue problem
//    Matrix-vector product y <- B * x
template<class K_, class MA_, class MB_>
void ARInterfaceGen<K_, MA_, MB_>::MultBx (K_ *x, K_ *y) {
/*  array2Vector(x, this->lx);
  multMatrixVector(*matB_p, this->lx, this->ly);
  vector2Array(this->ly, y);*/
  multMatrixVector(*matB_p, x, y);
}

// 1. Regular mode for real symmetric case
//    Matrix-vector products y <- inv(B)*A * x and x <- A * x
template<class MAB_>
void ARInterfaceGenSymReg<MAB_>::MultOPx (real_t *x, real_t *y) {
  array2Vector(x, this->lx);
  multMatrixVector(*(this->matA_p), this->lx, Ax);
// Important: when solving real symmetric generalized problems
// in regular mode, this function must additionally store x <- A*x.
// (see remark 5 in arpack++/include/saupp.h
//  and line 37 in  arpack++/examples/matprod/sym/sgenprba.h)
  vector2Array(Ax, x);
// Solve linear system. Matlab equivalent: ly = matB_p \ Ax;
  (fact_p->*Solver_)(Ax, this->ly); // store the solution into ly
  vector2Array(this->ly, y);
}

// 2. Regular mode except in the real symmetric case
//    Matrix-vector product y <- inv(B)*A * x
template<class K_, class MA_, class MB_>
void ARInterfaceGenReg<K_, MA_, MB_>::MultOPx (K_ *x, K_ *y) {
  array2Vector(x, this->lx);
  multMatrixVector(*(this->matA_p), this->lx, Ax);
// Solve linear system. Matlab equivalent: ly = matB_p \ Ax;
  (fact_p->*Solver_)(Ax, this->ly); // store the solution into ly
  vector2Array(this->ly, y);
}

// 3. Shift and invert mode. Real symmetric and complex cases.
//    Matrix-vector product y <- inv(A-sigma*B) * x
template<class K_, class MA_, class MB_>
void ARInterfaceGenShf<K_, MA_, MB_>::MultOPx (K_ *x, K_ *y) {
  array2Vector(x, this->lx);
// Solve linear system. Matlab equivalent: ly = matAsB_p \ lx;
  (fact_p->*Solver_)(this->lx, this->ly); // store the solution into ly
  vector2Array(this->ly, y);
}

// 4. Buckling mode. Real symmetric case.
//    Matrix-vector product y <- inv(A-sigma*B) * x
template<class MAB_>
void ARInterfaceGenBuc<MAB_>::MultOPx (real_t *x, real_t *y) {
  array2Vector(x, this->lx);
// Solve linear system. Matlab equivalent: ly = matAsB_p \ lx;
  (fact_p->*Solver_)(this->lx, this->ly); // store the solution into ly
  vector2Array(this->ly, y);
}
//    Matrix-vector product y <- A * x
template<class MAB_>
void ARInterfaceGenBuc<MAB_>::MultBx (real_t *x, real_t *y)
{ ARInterface<real_t, MAB_>::bz_MultAx(x,y); }

// 5. Cayley mode. Real symmetric case.
//    Matrix-vector product y <- inv(A-sigma*B) * x
template<class MAB_>
void ARInterfaceGenCay<MAB_>::MultOPx (real_t *x, real_t *y) {
  array2Vector(x, this->lx);
// Solve linear system. Matlab equivalent: ly = matAsB_p \ lx;
  (fact_p->*Solver_)(this->lx, this->ly); // store the solution into ly
  vector2Array(this->ly, y);
}
//    Matrix-vector product y <- A * x
template<class MAB_>
void ARInterfaceGenCay<MAB_>::MultAx (real_t *x, real_t *y)
{ ARInterface<real_t, MAB_>::bz_MultAx(x,y); }

// 6. Complex shift and invert mode. Real non symmetric case.
//    Matrix-vector product y <- real(inv(A-sigma B)) * x or imag(inv(A-sigma B)) * x
template<class MAB_, class MAsB_>
void ARInterfaceGenCsh<MAB_, MAsB_>::MultOPx (real_t *x, real_t *y)
{
// Ref.: arpack++/examples/matprod/nonsym/ngenprbc.h (MultOPvRe)
  std::vector<complex_t>::iterator it = clx.begin();
  while ( it != clx.end() ) { *it++ = *x++; }
// lx is complex. matAsB_p is complex, so is ly.
// Solve linear system. Matlab equivalent: ly = matAsB_p \ lx;
  (fact_p->*Solver_)(clx, cly); // store the solution into ly
  // extract real or imaginary part of ly into y
  if (realPart_) {
    for (it = cly.begin(); it != cly.end(); it++) { *y++ = it->real(); }
  }
  else {
    for (it = cly.begin(); it != cly.end(); it++) { *y++ = it->imag(); }
  }
}

//    Matrix-vector product y <- A * x
template<class MAB_, class MAsB_>
void ARInterfaceGenCsh<MAB_, MAsB_>::MultAx (real_t *x, real_t *y)
{ ARInterface<real_t, MAB_>::bz_MultAx(x,y); }

/*
 -------------------------------------------------------------------------------
   Protected member functions
 -------------------------------------------------------------------------------
*/
/* *************************************
     Utility functions in base class
   ************************************* */
//    Matrix-vector product y = A * x
template<class K_, class MA_>
void ARInterface<K_, MA_>::bz_MultAx   (K_ *x, K_ *y) {
/*  array2Vector(x, this->lx);
  multMatrixVector(*matA_p, this->lx, this->ly);
  vector2Array(this->ly, y);*/
  multMatrixVector(*matA_p, x, y);
}
// Check dimensions of the matrice Mat
template<class K_, class MA_>
template<class M_>
void ARInterface<K_, MA_>::checkDims(const M_* mat) {
  if ( mat->numberOfRows() != dim_ ) { error("entry_mismatch_dims", mat->numberOfRows(), dim_); }
  if ( mat->numberOfCols() != dim_ ) { error("entry_mismatch_dims", mat->numberOfCols(), dim_); }
}

/*
 -------------------------------------------------------------------------------
   Private member functions
 -------------------------------------------------------------------------------
*/
/* ***************************************************
     Factorizations for standard eigenvalue problems
   *************************************************** */

/*! Compute the factorisation of the matrix M = A-sigma*Id
    Select the solver required to solve the linear system M * y = x */
template<class K_, class MA_>
void ARInterfaceStdShf<K_, MA_>::factorize() {
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MA_>(*matAsI_p);
    Solver_ = &UmfPackLU<MA_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(matAsI_p);

    SymType   st = matAsI_p->symmetry();
    ValueType ty = matAsI_p->valueType();
    if ( st == _symmetric ) { // OK for Real and Complex
      ldltFactorize(*fact_p);
      Solver_ = &MA_::ldltSolve;
      this->fact_name_ = "XLiFE++ LDLt";
    }
    else if ( ty == _complex && st == _selfAdjoint ) {
      ldlstarFactorize(*fact_p);
      Solver_ = &MA_::ldlstarSolve;
      this->fact_name_ = "XLiFE++ LDL*";
    }
    else {
      luFactorize(*fact_p);
      Solver_ = &MA_::luSolve;
      this->fact_name_ = "XLiFE++ LU";
    }
  #endif
  checkCond(fact_p, "(A - sigma Id)");
}
/* ******************************************************
     Factorizations for generalized eigenvalue problems
   ****************************************************** */

/*! Compute the factorisation of the matrix B
    which should be real symmetric positive definite.
    Select the solver required to solve the linear system B * y = x */
template<class MAB_>
void ARInterfaceGenSymReg<MAB_>::factorize() {
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MAB_>(*(this->matB_p));
    Solver_ = &UmfPackLU<MAB_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(this->matB_p);

    ldltFactorize(*fact_p);
    Solver_ = &MAB_::ldltSolve;
    this->fact_name_ = "XLiFE++ LDLt";
  #endif
  checkCond(fact_p, "B");
}
template<class K_, class MA_, class MB_>// Here, MB_ is always a real matrix
void ARInterfaceGenReg< K_, MA_, MB_>::factorize() {
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MB_>(*(this->matB_p));
    Solver_ = &UmfPackLU<MB_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(this->matB_p);

    SymType   st = this->matB_p->symmetry();
    if ( st == _symmetric ) {
      ldltFactorize(*fact_p);
      Solver_ = &MB_::ldltSolve;
      this->fact_name_ = "XLiFE++ LDLt";
    }
    else {
      luFactorize(*fact_p);
      Solver_ = &MB_::luSolve;
      this->fact_name_ = "XLiFE++ LU";
      warning("hypothesis_not_verified", "real", "symmetric");
    }
  #endif
  checkCond(fact_p, "B");
}
template<>// Specialization when MB_ is a complex matrix (other template parameters are also fixed in this case)
void ARInterfaceGenReg< complex_t, LargeMatrix<complex_t>, LargeMatrix<complex_t> >::factorize() {
  typedef LargeMatrix<complex_t> MB_; //using MB_ = LargeMatrix<complex_t>;
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MB_>(*(this->matB_p));
    Solver_ = &UmfPackLU<MB_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(this->matB_p);

    SymType   st = this->matB_p->symmetry();
    if ( st == _selfAdjoint ) {
      ldlstarFactorize(*fact_p);
      Solver_ = &MB_::ldlstarSolve;
      this->fact_name_ = "XLiFE++ LDL*";
    }
    else {
      luFactorize(*fact_p);
      Solver_ = &MB_::luSolve;
      this->fact_name_ = "XLiFE++ LU";
      warning("hypothesis_not_verified", "complex", "selfadjoint");
    }
  #endif
  checkCond(fact_p, "B");
}
/*! Compute the factorisation of the matrix M = A-sigma B
    Select the solver required to solve the linear system M * y = x */
template<class K_, class MA_, class MB_>
void ARInterfaceGenShf< K_, MA_, MB_>::factorize() {
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MA_>(*matAsB_p);
    Solver_ = &UmfPackLU<MA_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(matAsB_p);

    SymType   st = matAsB_p->symmetry();
    ValueType ty = matAsB_p->valueType();
    if ( st == _symmetric ) { // OK for Real and Complex
      ldltFactorize(*fact_p);
      Solver_ = &MA_::ldltSolve;
      this->fact_name_ = "XLiFE++ LDLt";
    }
    else if ( ty == _complex && st == _selfAdjoint ) {
      ldlstarFactorize(*fact_p);
      Solver_ = &MA_::ldlstarSolve;
      this->fact_name_ = "XLiFE++ LDL*";
    }
    else {
      luFactorize(*fact_p);
      Solver_ = &MA_::luSolve;
      this->fact_name_ = "XLiFE++ LU";
    }
  #endif
  checkCond(fact_p, "(A - sigma B)");
}
/*! Compute the factorisation of the matrix M = A-sigma B
    which should be real symmetric.
    Select the solver required to solve the linear system M * y = x */
template<class MAB_>
void ARInterfaceGenBuc<MAB_>::factorize() {
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MAB_>(*matAsB_p);
    Solver_ = &UmfPackLU<MAB_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(matAsB_p);

    ldltFactorize(*fact_p);
    Solver_ = &MAB_::ldltSolve;
    this->fact_name_ = "XLiFE++ LDLt";
  #endif
  checkCond(fact_p, "(A - sigma B)");
}
/*! Compute the factorisation of the matrix M = A-sigma B
    which should be real symmetric.
    Select the solver required to solve the linear system M * y = x */
template<class MAB_>
void ARInterfaceGenCay<MAB_>::factorize() {
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MAB_>(*matAsB_p);
    Solver_ = &UmfPackLU<MAB_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(matAsB_p);

    ldltFactorize(*fact_p);
    Solver_ = &MAB_::ldltSolve;
    this->fact_name_ = "XLiFE++ LDLt";
  #endif
  checkCond(fact_p, "(A - sigma B)");
}
/*! Compute the factorisation of the matrix M = A-sigma B
    which should be complex. A is real non symmetric, B real symmetric,
    sigma complex scalar, so the factorization is LU.
    Select the solver required to solve the linear system M * y = x */
template<class MAB_, class MAsB_>
void ARInterfaceGenCsh<MAB_, MAsB_>::factorize() {
  #ifdef XLIFEPP_WITH_UMFPACK
    fact_p = new UmfPackLU<MAsB_>(*matAsB_p);
    Solver_ = &UmfPackLU<MAsB_>::solve;
    this->fact_name_ = "UmfPack LU";
  #else
    fact_p = newSkyline(matAsB_p);

    luFactorize(*fact_p);
    Solver_ = &MAsB_::luSolve;
    this->fact_name_ = "XLiFE++ LU";
  #endif
  checkCond(fact_p, "(A - sigma B)");
}

/*
 -------------------------------------------------------------------------------
   External utility functions
 -------------------------------------------------------------------------------
*/

// Convert array of K_ to vector
template<typename K_>
void array2Vector(const K_* source, std::vector<K_>& vec) {
  typename std::vector<K_>::iterator it = vec.begin();
  while ( it != vec.end() ) { *it++ = *source++; }
}

// Convert vector to an array of K_
template<typename K_>
void vector2Array(std::vector<K_>& vec, K_* target) {
  typename std::vector<K_>::iterator it = vec.begin();
  while ( it != vec.end() ) { *target++ = *it++; }
}

/*!
  This function allocates a new matrix, equal to the input matrix but with a skyline storage.
  The new matrix can then be factorized using one of the internal factorization algorithms
  available in XLiFE++, such as LDLt, LDL* or LU.
*/
template<class M_>
M_ *newSkyline(const M_ *mat_p) {
  M_ *fact_p;
  if ( mat_p->storageType() != _skyline) { fact_p = new M_(*mat_p, true); }
  else                                   { fact_p = new M_(*mat_p); }
  fact_p->toSkyline();
  return fact_p;
}
/*
 -------------------------------------------------------------------------------
   External functions instantiations
 -------------------------------------------------------------------------------
*/
template void array2Vector(const real_t* source, std::vector<real_t>& vec);
template void array2Vector(const complex_t* source, std::vector<complex_t>& vec);

template void vector2Array(std::vector<real_t>& vec, real_t* target);
template void vector2Array(std::vector<complex_t>& vec, complex_t* target);

template LargeMatrix<real_t> *newSkyline(const LargeMatrix<real_t> *mat_p);
template LargeMatrix<complex_t> *newSkyline(const LargeMatrix<complex_t> *mat_p);

/*
 -------------------------------------------------------------------------------
   Class instantiations
 -------------------------------------------------------------------------------
*/
template class ARInterfaceStdReg< real_t, LargeMatrix<real_t> >;
template class ARInterfaceStdReg< complex_t, LargeMatrix<complex_t> >;

template class ARInterfaceStdShf< real_t, LargeMatrix<real_t> >;
template class ARInterfaceStdShf< complex_t, LargeMatrix<complex_t> >;

template class ARInterfaceGenSymReg< LargeMatrix<real_t> >;

template class ARInterfaceGenReg< real_t, LargeMatrix<real_t>, LargeMatrix<real_t> >;
template class ARInterfaceGenReg< complex_t, LargeMatrix<complex_t>, LargeMatrix<real_t> >;
template class ARInterfaceGenReg< complex_t, LargeMatrix<complex_t>, LargeMatrix<complex_t> >;

template class ARInterfaceGenShf< real_t, LargeMatrix<real_t>, LargeMatrix<real_t> >;
template class ARInterfaceGenShf< complex_t, LargeMatrix<complex_t>, LargeMatrix<real_t> >;
template class ARInterfaceGenShf< complex_t, LargeMatrix<complex_t>, LargeMatrix<complex_t> >;

template class ARInterfaceGenBuc< LargeMatrix<real_t> >;

template class ARInterfaceGenCay< LargeMatrix<real_t> >;

template class ARInterfaceGenCsh< LargeMatrix<real_t>, LargeMatrix<complex_t> >;

/*
 -------------------------------------------------------------------------------
   Implementation of member functions of classes for SVD computation
 -------------------------------------------------------------------------------
*/

//!    Matrix-vector product y <- (At) * A * x = A' * A * x
void ARInterfaceRSVDmGEnReg::MultOPx (real_t *x, real_t *y) {
   array2Vector(x, lx);
   multMatrixVector(*matA_p, lx, Tx);
   multVectorMatrix(*matA_p, Tx, ly);
   vector2Array(ly, y);
}
//!    Matrix-vector product y <- A * (At) * x = A * A' * x
void ARInterfaceRSVDmLTnReg::MultOPx (real_t *x, real_t *y) {
   array2Vector(x, lx);
   multVectorMatrix(*matA_p, lx, Tx);
   multMatrixVector(*matA_p, Tx, ly);
   vector2Array(ly, y);
}

//!    Matrix-vector product y <- ((At) * A - s * Id) * x = (A' * A - s * Id) * x
void ARInterfaceRSVDmGEnShf::MultOPx (real_t *x, real_t *y) {
   array2Vector(x, lx);
   multMatrixVector(*matA_p, lx, Tx);
   multVectorMatrix(*matA_p, Tx, ly);
   for (std::vector<real_t>::iterator it=ly.begin(); it != ly.end(); it++) { *it -= shift_* *x++; }
   vector2Array(ly, y);
}
//!    Matrix-vector product y <- (A * (At) - s * Id) * x = (A * A' - s * Id) * x
void ARInterfaceRSVDmLTnShf::MultOPx (real_t *x, real_t *y) {
   array2Vector(x, lx);
   multVectorMatrix(*matA_p, lx, Tx);
   multMatrixVector(*matA_p, Tx, ly);
   for (std::vector<real_t>::iterator it=ly.begin(); it != ly.end(); it++) { *it -= shift_* *x++; }
   vector2Array(ly, y);
}

//!    Matrix-vector product y <- (A*) * A * x = A' * A * x
void ARInterfaceCSVDmGEnReg::MultOPx (complex_t *x, complex_t *y) {
   array2Vector(x, lx);
   multMatrixVector(*matA_p, lx, Tx);
   for (std::vector<complex_t>::iterator it=Tx.begin(); it != Tx.end(); it++) { *it = std::conj(*it); }
   multVectorMatrix(*matA_p, Tx, ly);
   for (std::vector<complex_t>::iterator it=ly.begin(); it != ly.end(); it++) { *it = std::conj(*it); }
   vector2Array(ly, y);
}
//!    Matrix-vector product y <- A * (A*) * x = A * A' * x
void ARInterfaceCSVDmLTnReg::MultOPx (complex_t *x, complex_t *y) {
   array2Vector(x, lx);
   for (std::vector<complex_t>::iterator it=lx.begin(); it != lx.end(); it++) { *it = std::conj(*it); }
   multVectorMatrix(*matA_p, lx, Tx);
   for (std::vector<complex_t>::iterator it=Tx.begin(); it != Tx.end(); it++) { *it = std::conj(*it); }
   multMatrixVector(*matA_p, Tx, ly);
   vector2Array(ly, y);
}

//!    Matrix-vector product y <- ((A*) * A - s * Id) * x = (A' * A - s * Id) * x
void ARInterfaceCSVDmGEnShf::MultOPx (complex_t *x, complex_t *y) {
   array2Vector(x, lx);
   multMatrixVector(*matA_p, lx, Tx);
   for (std::vector<complex_t>::iterator it=Tx.begin(); it != Tx.end(); it++) { *it = std::conj(*it); }
   multVectorMatrix(*matA_p, Tx, ly);
   for (std::vector<complex_t>::iterator it=ly.begin(); it != ly.end(); it++) { *it = std::conj(*it) - shift_* *x++; }
   vector2Array(ly, y);
}
//!    Matrix-vector product y <- (A * (A*) - s * Id) * x = (A * A' - s * Id) * x
void ARInterfaceCSVDmLTnShf::MultOPx (complex_t *x, complex_t *y) {
   array2Vector(x, lx);
   for (std::vector<complex_t>::iterator it=lx.begin(); it != lx.end(); it++) { *it = std::conj(*it); }
   multVectorMatrix(*matA_p, lx, Tx);
   for (std::vector<complex_t>::iterator it=Tx.begin(); it != Tx.end(); it++) { *it = std::conj(*it); }
   multMatrixVector(*matA_p, Tx, ly);
   for (std::vector<complex_t>::iterator it=ly.begin(); it != ly.end(); it++) { *it -= shift_* *x++; }
   vector2Array(ly, y);
}

} // end of namespace xlifepp
