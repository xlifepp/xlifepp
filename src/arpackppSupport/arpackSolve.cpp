/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file arpackSolve.cpp
  \author Y. Lafranche
  \since 15 Dec 2015
  \date 18 Jan 2016

  \brief Implementation of some external functions related to Arpack usage
*/

#include "../term/TermMatrix.hpp"
#include "arpackSolve.hpp"
#include "ARGenToStd.hpp"

#ifdef XLIFEPP_WITH_ARPACK
  #include "arssym.h"     // for ARSymStdEig
  #include "argsym.h"     // for ARSymGenEig

  #include "arsnsym.h"    // for ARNonSymStdEig
  #include "argnsym.h"    // for ARNonSymGenEig

  #include "arscomp.h"    // for ARCompStdEig
  #include "argcomp.h"    // for ARCompGenEig

  namespace xlifepp {

  template<class K_>
  struct ResultTypeTrait{ static const ValueType type = _complex; };
  template<>
  struct ResultTypeTrait<real_t>{ static const ValueType type = _real; };

  // Record informations from the interface object
  template<class K_, class MA_>
  void getInfos(const ARInterface<K_, MA_>& IntObj, ArpackProb& ARPb){
    ARPb.KindOfFactorization(IntObj.KindOfFactorization());
    ARPb.InterfaceObj(IntObj.ConstructorName());
  }

  typedef std::vector<std::pair<complex_t, VectorEntry*> > VpacVEp;

  // Transmit parameters to the true Arpack++ object, compute the eigen elements,
  // retrieve parameters after computation.
  template<class P_, class K_>// type of the Arpack++ problem, type of the scalars
  VpacVEp getResults(P_& arProb, ArpackProb& ARPb, VpacVEp&) {
    ARPb.setParameters(arProb);
    int nconv = arProb.FindEigenvectors();
    if (nconv == 0) { warning("eigenvalues_not_found"); }
    ARPb.getParameters(arProb);
    // Save control object in a global one to allow user inquiries at top level.
    ARPb.ArpackObj(typeid(P_).name());
    _ARprob = ARPb;

    int nev = arProb.GetNev();
    if (nconv > nev) { nconv = nev; }
    // Collect data into vvp to create result object res
    VpacVEp vvp(nconv);
    for (int i = 0; i < nconv; i++) {
      // Note: the function StlEigenvector does the right job for any computational mode used in Arpack
      std::vector<K_>* stlev_p = arProb.StlEigenvector(i); // Creation of a new temporary vector
      VectorEntry* eigvec_p = new VectorEntry(*stlev_p);   // Copy data
      delete stlev_p;                                      // Free the temporary vector
      vvp[i] = std::make_pair(arProb.Eigenvalue(i), eigvec_p);
    }
    return vvp;
  }

  template<class P_, class K_>// type of the Arpack++ problem, type of the scalars
  EigenElements getResults(P_& arProb, ArpackProb& ARPb, EigenElements&) {
    VpacVEp vvp = getResults<P_, K_>(arProb, ARPb, vvp);
    const TermMatrix* A_p = ARPb.characteristicMatrix();
    bool isr = ResultTypeTrait<K_>::type == _real;
    return EigenElements(A_p, A_p->isSingleUnknown(), isr, vvp, ARPb.SortKind(), ARPb.getResName());
  }

  // Arpack computation using a class provided by the user
  template<class R_>
  R_ arpUsrComp(ArpackProb& ARPb) {
    int nev = ARPb.GetNev();
    complex_t sigmaC = ARPb.GetShift();
    real_t    sigmaR = sigmaC.real();
    R_ R_type;

    if (ARPb.isStd()) { // standard problem ===============================================
      if (ARPb.isRegular()) { // Regular mode ---------------------------------------------
        if (ARPb.isReal()) {
          typedef ARInterfaceFrame<real_t> IntObj_t;
          IntObj_t& IntObj = ARPb.getrUsrCl();
          if ( ARPb.isSym() ) { // Real symmetric case
            typedef ARSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, R_type);
          }
          else { // Real non symmetric case
            typedef ARNonSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
          }
        }
        else { // Complex case
          typedef ARInterfaceFrame<complex_t> IntObj_t;
          IntObj_t& IntObj = ARPb.getcUsrCl();
          typedef ARCompStdEig< real_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
        }
      }
      else { // Shifted modes -------------------------------------------------------------
        if (ARPb.isReal()) {
          typedef ARInterfaceFrame<real_t> IntObj_t;
          IntObj_t& IntObj = ARPb.getrUsrCl();
          if ( ARPb.isSym() ) { // Real symmetric case
            typedef ARSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx, sigmaR);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, R_type);
          }
          else { // Real non symmetric case
            typedef ARNonSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx, sigmaR);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
          }
        }
        else { // Complex case
          typedef ARInterfaceFrame<complex_t> IntObj_t;
          IntObj_t& IntObj = ARPb.getcUsrCl();
          typedef ARCompStdEig< real_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx, sigmaC);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
        }
      }
    } // end standard problem =============================================================
    else { // generalized problem =========================================================
      if (ARPb.isReal()) {
      typedef ARInterfaceFrame<real_t> IntObj_t;
      IntObj_t& IntObj = ARPb.getrUsrCl();
      if ( ARPb.isSym() ) { // Real symmetric case ---------
          if (ARPb.isRegular()) { // Regular mode
            typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                &IntObj, &IntObj_t::MultBx);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, R_type);
          }
          else if (ARPb.cptnMode() == 'S') { // Shift and Inverse mode
            typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb('S', IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                    &IntObj, &IntObj_t::MultBx, sigmaR);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, R_type);
          }
          else if (ARPb.cptnMode() == 'B') { // Buckling mode
            typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb('B', IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                    &IntObj, &IntObj_t::MultBx, sigmaR);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, R_type);
          }
          else if (ARPb.cptnMode() == 'C') { // Cayley mode
            typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                &IntObj, &IntObj_t::MultAx,
                                                &IntObj, &IntObj_t::MultBx, sigmaR);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, R_type);
          }
        }
        else { // Real non symmetric case -------------------------------------------------
          if (ARPb.isRegular()) { // Regular mode
            typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                &IntObj, &IntObj_t::MultBx);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
          }
          else if (ARPb.cptnMode() == 'S') { // Real Shift and Inverse mode
            typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                &IntObj, &IntObj_t::MultBx, sigmaR);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
          }
          else if (ARPb.cptnMode() == 'R') { // Complex Shift and Inverse mode using Re(inv(A-sigma*B))
            real_t sigmaI = ARPb.GetShiftImag();
            typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                &IntObj, &IntObj_t::MultAx,
                                                &IntObj, &IntObj_t::MultBx, 'R', sigmaR, sigmaI);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
          }
          else if (ARPb.cptnMode() == 'I') { // Complex Shift and Inverse mode using Im(inv(A-sigma*B))
            real_t sigmaI = ARPb.GetShiftImag();
            typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                &IntObj, &IntObj_t::MultAx,
                                                &IntObj, &IntObj_t::MultBx, 'I', sigmaR, sigmaI);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
          }
        }
      }
      else { // Complex case --------------------------------------------------------------
        typedef ARInterfaceFrame<complex_t> IntObj_t;
        IntObj_t& IntObj = ARPb.getcUsrCl();
        if (ARPb.isRegular()) { // Regular mode
          typedef ARCompGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
        }
        else { // Shift and Inverse mode
          typedef ARCompGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx, sigmaC);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, R_type);
        }
      }
    } //  end generalized problem =========================================================
    return R_type; // dummy return
  }

  // Interface with Arpack++
  EigenElements arpackSolve(ArpackProb& ARPb) {
    TermMatrix* A_p = ARPb.getPtrA();
    // If the problem is defined through a user class, call the function dealing with that case:
    if (A_p == nullptr) return arpUsrComp<EigenElements>(ARPb);

    // else, the problem is defined through TermMatrix objects that we use here:
    MatrixEntry* meA_p = A_p->matrixData();
    int nev = ARPb.GetNev();
    complex_t sigmaC = ARPb.GetShift();
    real_t    sigmaR = sigmaC.real();
    EigenElements EEtype;

    if (ARPb.isStd()) { // standard problem ===============================================
      if (ARPb.isRegular()) { // Regular mode ---------------------------------------------
        if (ARPb.isReal()) {
          typedef ARInterfaceStdReg< real_t, LargeMatrix<real_t> > IntObj_t;
          IntObj_t IntObj(meA_p->getLargeMatrix<real_t>()); getInfos(IntObj, ARPb);
          if ( ARPb.isSym() && (! ARPb.isForcedNonSym()) ) { // Real symmetric case
            typedef ARSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, EEtype);
          }
          else { // Real non symmetric case
            typedef ARNonSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
          }
        }
        else { // Complex case
          typedef ARInterfaceStdReg<complex_t, LargeMatrix<complex_t> > IntObj_t;
          IntObj_t IntObj(meA_p->getLargeMatrix<complex_t>()); getInfos(IntObj, ARPb);
          typedef ARCompStdEig< real_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
      }
      else { // Shifted modes -------------------------------------------------------------
        if (ARPb.isReal()) {
          typedef ARInterfaceStdShf< real_t, LargeMatrix<real_t> > IntObj_t;
          IntObj_t IntObj(meA_p->getLargeMatrix<real_t>(), sigmaR); getInfos(IntObj, ARPb);
          if ( ARPb.isSym() && (! ARPb.isForcedNonSym()) ) { // Real symmetric case
            typedef ARSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx, sigmaR);
            return getResults<ARProb_t, real_t>(ArProb, ARPb, EEtype);
          }
          else { // Real non symmetric case
            typedef ARNonSymStdEig< real_t, IntObj_t > ARProb_t;
            ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx, sigmaR);
            return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
          }
        }
        else { // Complex case
          typedef ARInterfaceStdShf< complex_t, LargeMatrix<complex_t> > IntObj_t;
          IntObj_t IntObj(meA_p->getLargeMatrix<complex_t>(), sigmaC); getInfos(IntObj, ARPb);
          typedef ARCompStdEig< real_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx, sigmaC);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
      }
    } // end standard problem =============================================================
    else { // generalized problem =========================================================
      // B should be real, except if A is complex where B can be real or complex
      TermMatrix* B_p = ARPb.getPtrB();
      MatrixEntry* meB_p = B_p->matrixData();
      bool BisComplex(B_p->valueType() == _complex);
      if (ARPb.isReal()) {
      if (BisComplex) { error("bad_type"); }
      LargeMatrix<real_t> &matA = meA_p->getLargeMatrix<real_t>(), &matB = meB_p->getLargeMatrix<real_t>();
      if ( ARPb.isSym() && (! ARPb.isForcedNonSym()) ) { // Real symmetric case ---------
        if (ARPb.isRegular()) { // Regular mode
          typedef ARInterfaceGenSymReg< LargeMatrix<real_t> > IntObj_t;
          IntObj_t  IntObj(matA, matB); getInfos(IntObj, ARPb);
          typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx);
          return getResults<ARProb_t, real_t>(ArProb, ARPb, EEtype);
        }
        else if (ARPb.cptnMode() == 'S') { // Shift and Inverse mode
          typedef ARInterfaceGenShf< real_t, LargeMatrix<real_t>, LargeMatrix<real_t> > IntObj_t;
          IntObj_t IntObj(matA, matB, sigmaR); getInfos(IntObj, ARPb);
          typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb('S', IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                  &IntObj, &IntObj_t::MultBx, sigmaR);
          return getResults<ARProb_t, real_t>(ArProb, ARPb, EEtype);
        }
        else if (ARPb.cptnMode() == 'B') { // Buckling mode
          typedef ARInterfaceGenBuc< LargeMatrix<real_t> > IntObj_t;
          IntObj_t  IntObj(matA, matB, sigmaR); getInfos(IntObj, ARPb);
          typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb('B', IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                                  &IntObj, &IntObj_t::MultBx, sigmaR);
          return getResults<ARProb_t, real_t>(ArProb, ARPb, EEtype);
        }
        else if (ARPb.cptnMode() == 'C') { // Cayley mode
          typedef ARInterfaceGenCay< LargeMatrix<real_t> > IntObj_t;
          IntObj_t IntObj(matA, matB, sigmaR); getInfos(IntObj, ARPb);
          typedef ARSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultAx,
                                              &IntObj, &IntObj_t::MultBx, sigmaR);
          return getResults<ARProb_t, real_t>(ArProb, ARPb, EEtype);
        }
      }
      else { // Real non symmetric case -------------------------------------------------
        if (ARPb.isRegular()) { // Regular mode
          typedef ARInterfaceGenReg< real_t, LargeMatrix<real_t>, LargeMatrix<real_t> > IntObj_t;
          IntObj_t  IntObj(matA, matB); getInfos(IntObj, ARPb);
          typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
        else if (ARPb.cptnMode() == 'S') { // Real Shift and Inverse mode
          typedef ARInterfaceGenShf< real_t, LargeMatrix<real_t>, LargeMatrix<real_t> > IntObj_t;
          IntObj_t IntObj(matA, matB, sigmaR); getInfos(IntObj, ARPb);
          typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx, sigmaR);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
        else if (ARPb.cptnMode() == 'R') { // Complex Shift and Inverse mode using Re(inv(A-sigma*B))
          real_t sigmaI = ARPb.GetShiftImag();
          typedef ARInterfaceGenCsh< LargeMatrix<real_t>, LargeMatrix<complex_t> > IntObj_t;
          IntObj_t IntObj(matA, matB, 'R', sigmaR, sigmaI);
          getInfos(IntObj, ARPb);
          typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultAx,
                                              &IntObj, &IntObj_t::MultBx, 'R', sigmaR, sigmaI);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
        else if (ARPb.cptnMode() == 'I') { // Complex Shift and Inverse mode using Im(inv(A-sigma*B))
          real_t sigmaI = ARPb.GetShiftImag();
          typedef ARInterfaceGenCsh< LargeMatrix<real_t>, LargeMatrix<complex_t> > IntObj_t;
          IntObj_t IntObj(matA, matB, 'I', sigmaR, sigmaI);
          getInfos(IntObj, ARPb);
          typedef ARNonSymGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultAx,
                                              &IntObj, &IntObj_t::MultBx, 'I', sigmaR, sigmaI);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
      }
      }
      else { // Complex case --------------------------------------------------------------
      LargeMatrix<complex_t> &matA = meA_p->getLargeMatrix<complex_t>();
      if (BisComplex) {
        LargeMatrix<complex_t> &matB = meB_p->getLargeMatrix<complex_t>();
        if (ARPb.isRegular()) { // Regular mode
          typedef ARInterfaceGenReg< complex_t, LargeMatrix<complex_t>, LargeMatrix<complex_t> > IntObj_t;
          IntObj_t IntObj(matA, matB); getInfos(IntObj, ARPb);
          typedef ARCompGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
        else { // Shift and Inverse mode
          typedef ARInterfaceGenShf< complex_t, LargeMatrix<complex_t>, LargeMatrix<complex_t> > IntObj_t;
          IntObj_t IntObj(matA, matB, sigmaC); getInfos(IntObj, ARPb);
          typedef ARCompGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx, sigmaC);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
      }
      else {
        LargeMatrix<real_t> &matB = meB_p->getLargeMatrix<real_t>();
        if (ARPb.isRegular()) { // Regular mode
          typedef ARInterfaceGenReg< complex_t, LargeMatrix<complex_t>, LargeMatrix<real_t> > IntObj_t;
          IntObj_t IntObj(matA, matB); getInfos(IntObj, ARPb);
          typedef ARCompGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
        else { // Shift and Inverse mode
          typedef ARInterfaceGenShf< complex_t, LargeMatrix<complex_t>, LargeMatrix<real_t> > IntObj_t;
          IntObj_t IntObj(matA, matB, sigmaC); getInfos(IntObj, ARPb);
          typedef ARCompGenEig< real_t, IntObj_t, IntObj_t > ARProb_t;
          ARProb_t ArProb(IntObj.GetN(), nev, &IntObj, &IntObj_t::MultOPx,
                                              &IntObj, &IntObj_t::MultBx, sigmaC);
          return getResults<ARProb_t, complex_t>(ArProb, ARPb, EEtype);
        }
      }
      }
    } //  end generalized problem =========================================================

    return EigenElements(); // dummy return
  }

  /*! Utility function that retrieves an iterator on the parameter corresponding to a key pk, inside the vector pars.
      Returns pars.end() if the key is not found in pars.
  */
  typedef std::vector<Parameter>::const_iterator vPci_t;
  vPci_t fetchPar(const std::vector<Parameter>& pars, const ParameterKey& pk) {
    vPci_t itp;
    for (itp=pars.begin(); itp != pars.end(); itp++) { if (itp->key() == pk) break; }
    return itp;
  }

  //! Utility function that takes into account Arpack++ parameters set by the user
  void setOptParams(const std::vector<Parameter>& pars, const std::set<ParameterKey>& upars,
                    ArpackProb* eigProb_p) {
    ParameterKey pk = _pk_forceNonSym;
    if (upars.find(pk) != upars.end()) {
      eigProb_p->forceNonSym();
    }
    pk = _pk_maxIt;
    if (upars.find(pk) != upars.end()) {
      eigProb_p->ChangeMaxit(fetchPar(pars, pk)->get_i());
    }
    pk = _pk_ncv;
    if (upars.find(pk) != upars.end()) {
      eigProb_p->ChangeNcv(fetchPar(pars, pk)->get_i());
    }
    pk = _pk_sort;
    if (upars.find(pk) != upars.end()) {
      eigProb_p->SortKind(EigenSortKind(fetchPar(pars, pk)->get_i()));
    }
    pk = _pk_tolerance;
    if (upars.find(pk) != upars.end()) {
      eigProb_p->ChangeTol(fetchPar(pars, pk)->get_r());
    }
    pk = _pk_verbose;
    if (upars.find(pk) != upars.end()) {
      if (fetchPar(pars, pk)->get_n() != 0) { eigProb_p->Trace(); }
    }
    pk = _pk_which;
    if (upars.find(pk) != upars.end()) {
      eigProb_p->ChangeWhich(fetchPar(pars, pk)->get_s());
    }

    // The following parameter has nothing to do with Arpack. It is put inside the ArpackProb object
    // just to simplify its transmission to the final XLiFE++'s result object.
    pk = _pk_name;
    if (upars.find(pk) != upars.end()) {
      eigProb_p->setResName(fetchPar(pars, pk)->get_s());
    }
    else {
      eigProb_p->setResName(string_t("eigvec")); // same value as in parseEigenPars
    }
  }

  //! Utility function that retrieves the shift chosen by the user
  std::complex<double> getShiftValue(const std::vector<Parameter>& pars) {
    std::complex<double> sigc;

    vPci_t itp = fetchPar(pars, _pk_sigma);
    if (itp->type() == _integer)   { sigc = std::complex<double>(double(itp->get_i()), 0.); }
    else if (itp->type() == _real) { sigc = std::complex<double>(itp->get_r(), 0.); }
    else { sigc = itp->get_c(); }
    return sigc;
  }

  /*! Utility function that drives the computation of the eigenvalue problem using Arpack.
      Default routine that takes into account the parameters specified by the user without any change.
  */
  EigenElements arpCompute(TermMatrix* A, TermMatrix* B, int nev, EigenComputationalMode mode,
                          const std::vector<Parameter>& pars, const std::set<ParameterKey>& upars) {
    bool regular = upars.find(_pk_sigma) == upars.end();
    ArpackProb* eigProb_p=nullptr;
    if (B == nullptr) {// standard eigenvalue problem
      if (regular) { eigProb_p = new ArpackProb(A, nev); }
      else         { std::complex<double> sigc = getShiftValue(pars);
                    eigProb_p = new ArpackProb(A, nev, sigc.real(), sigc.imag()); }
    }
    else {// generalized eigenvalue problem
      if (regular) { eigProb_p = new ArpackProb(A,B,nev); }
      else {
        std::complex<double> sigc = getShiftValue(pars);
        if (A->valueType() == _complex) { eigProb_p = new ArpackProb(A,B,nev, sigc.real(),sigc.imag()); }
        else {// A is real ; here, we rely on the mode chosen by the user
          switch (mode) {
            case _buckling: eigProb_p = new ArpackProb(A, B, nev, sigc.real(), 'B'); break;
            case _cayley: eigProb_p = new ArpackProb(A, B, nev, sigc.real(), 'C'); break;
            case _cshiftRe: eigProb_p = new ArpackProb(A, B, nev, sigc.real(), sigc.imag(), 'R'); break;
            case _cshiftIm: eigProb_p = new ArpackProb(A, B, nev, sigc.real(), sigc.imag(), 'I'); break;
            default: eigProb_p = new ArpackProb(A, B, nev, sigc.real()); break;
          }
        }
      }
    }
    setOptParams(pars, upars, eigProb_p);
    EigenElements res = arpackSolve(*eigProb_p);
    delete eigProb_p;
    if(A->constraints_u()!=nullptr) res.applyEssentialConditions(*A->constraints_u());
    return res;
  }

  /*! Utility function that drives the computation of the eigenvalue problem using Arpack.
      This routine performs additionnal tests on the matrix B and may modify the default
      behavior by converting the problem into a standard one ; in this case, an information
      message is output, except if this conversion is requested by the user.
  */
  EigenElements arpCheckNCompute(TermMatrix* A, TermMatrix* B, int nev, EigenComputationalMode mode,
                                const std::vector<Parameter>& pars, const std::set<ParameterKey>& upars) {
    if (B != nullptr) {// generalized eigenvalue problem
      bool convertPb = false, rmatcshitf = false;
      SymType symtyp = _symmetric;
      // convToStd is true if the conversion to a standard problem is requested by the user:
      // in which case this conversion is enforced, even if this does not seem necessary.
      bool convToStd = upars.find(_pk_convToStd) != upars.end();

      // Make some consistency checks and, if it seems necessary, convert the problem to a standard one
      // by creating a so-called "user class" in order to try to carry out the computation.
      bool regular = upars.find(_pk_sigma) == upars.end();
      std::complex<double> sigc;
      if (!regular) { sigc = getShiftValue(pars); }
      ARStdFrame<complex_t>* Cusrcl_p = nullptr;
      ARStdFrame<real_t>* Rusrcl_p = nullptr;
      ArpackProb* eigProb_p = nullptr;
      if (A->valueType() == _real) {// A is real
        if (convToStd || (B->symmetry() != _symmetric)) {
          convertPb = true;
          if (regular) {
            Rusrcl_p = new ARGenToStdReg<real_t, real_t>(*A, *B);
            string_t which_st("");
            if (upars.find(_pk_which) != upars.end()) { which_st = fetchPar(pars, _pk_which)->get_s(); }
            char const* which_p = which_st.c_str();
            eigProb_p = new ArpackProb(*Rusrcl_p,nev,which_p,false);
          }
          else {
            double sigmaR = sigc.real();
            Rusrcl_p = new ARGenToStdShf<real_t, real_t>(*A, *B, sigmaR);
            eigProb_p = new ArpackProb(*Rusrcl_p,nev,false,sigmaR);
            if (upars.find(_pk_mode) != upars.end()) {
              // The parameter 'mode' has been specified by the user, so the shift is complex:
              // record the fact that a warning message should be output (below).
              EigenComputationalMode ecm = EigenComputationalMode(fetchPar(pars, _pk_mode)->get_n());
              if (ecm == _cshiftRe || ecm == _cshiftIm) { rmatcshitf = true; }
            } // else { rmatcshitf unchanged (false) }
          }
        }
        else { convertPb = false; }
      }
      else {// A is complex
        if (B->valueType() == _real && (convToStd || B->symmetry() != _symmetric)) {
          convertPb = true;
          if (regular) { Cusrcl_p = new ARGenToStdReg<complex_t, real_t>(*A, *B); }
          else         { Cusrcl_p = new ARGenToStdShf<complex_t, real_t>(*A, *B, sigc); }
        }
        else if (B->valueType() == _complex && (convToStd || B->symmetry() != _selfAdjoint)) {
          convertPb = true;
          symtyp = _selfAdjoint;
          if (regular) { Cusrcl_p = new ARGenToStdReg<complex_t, complex_t>(*A, *B); }
          else         { Cusrcl_p = new ARGenToStdShf<complex_t, complex_t>(*A, *B, sigc); }
        }
        else { convertPb = false; }
        if (convertPb) {
          if (regular) {
            string_t which_st("");
            if (upars.find(_pk_which) != upars.end()) { which_st = fetchPar(pars, _pk_which)->get_s(); }
            char const* which_p = which_st.c_str();
            eigProb_p = new ArpackProb(*Cusrcl_p,nev,which_p);
          }
          else { eigProb_p = new ArpackProb(*Cusrcl_p,nev, sigc.real(), sigc.imag()); }
        }
      }
      if (convertPb) {
        if (!convToStd) { info("conv_tostd", words("symmetry",symtyp)); }
        if (rmatcshitf) { warning("cplx_shift"); }
        // The problem has been converted to a standard one by creating a so-called "user class".
        // Now, use the adequate function to perform the computation.
        setOptParams(pars, upars, eigProb_p);
        EigenElements res = arpUsrComp<EigenElements>(*eigProb_p);
        if(A->constraints_u()!=nullptr) res.applyEssentialConditions(*A->constraints_u());
        delete eigProb_p;
        delete Cusrcl_p;
        delete Rusrcl_p;
        return res;
      }
      else { return arpCompute(A, B, nev, mode, pars, upars); }// everything seems OK, use default routine
    }
    else { return arpCompute(A, B, nev, mode, pars, upars); }// use default routine
  }

  //! Utility function to control or reset Arpack++ parameters for SVD computation
  void controlParams(int dimx, ArpackProb& Arpb) {
    Arpb.SortKind(_decr_realpart);
    if (Arpb.GetWhich() == std::string("SM")) {
      Arpb.ChangeNcv(std::min(dimx, std::max(3*Arpb.GetNev(), Arpb.GetNcv()))); }
  }

  // Computation of the singular values of the matrice A, through the computation of
  // the eigenvalues of A' * A or A * A'. The singular vectors are also computed.
  SvdElements arpSVDCompute(TermMatrix* A, int nev, const std::vector<Parameter>& pars,
                            const std::set<ParameterKey>& upars) {
    number_t m = A->numberOfRows(), n = A->numberOfCols();
    int dimx = ((m >= n) ? n : m) - 1;
    bool regular = upars.find(_pk_sigma) == upars.end();
    EigenElements eiel;
    VpacVEp vvp;
    SvdElements res;
    if (regular) {// regular computational mode - ArpackProb II 1. constructor
      if (A->valueType() == _real) {
        const ARInterfaceRSVD* interCl_p = (m >= n)
            ? static_cast<const ARInterfaceRSVD*>(new ARInterfaceRSVDmGEnReg(*A,n,m))
            : static_cast<const ARInterfaceRSVD*>(new ARInterfaceRSVDmLTnReg(*A,m,n));
        ArpackProb Arpb(*interCl_p, nev, "LM");
        setOptParams(pars, upars, &Arpb); controlParams(dimx, Arpb);
        eiel = arpUsrComp<EigenElements>(Arpb);
  /*    vvp = arpUsrComp<VpacVEp>(Arpb);
      res = SvdElements(interCl_p->matPtr(), vvp);
    interCl_p->matPtr()->saveToFile("matRlarg.dat",_coo);*/
  /*/----
    const LargeMatrix<real_t>* mat_p = interCl_p->matPtr();
    std::vector<real_t> X(n,1.), Y(m);
    int i;
    i=1; for (std::vector<real_t>::iterator it=X.begin(); it!=X.end(); it++) { *it = 0.1*i++; }
    multMatrixVector(*mat_p, X, Y);
    thePrintStream << "multMatrixVector: Y = A * X =" << std::endl;
    for (std::vector<real_t>::iterator it=Y.begin(); it!=Y.end(); it++) { thePrintStream << *it << std::endl; }
    std::vector<real_t> Z(m,1.);
    i=1; for (std::vector<real_t>::iterator it=Z.begin(); it!=Z.end(); it++) { *it = 0.1*i++; }
    multVectorMatrix(*mat_p, Z, X);
    thePrintStream << "multVectorMatrix: Y = A' * X =" << std::endl;
    for (std::vector<real_t>::iterator it=X.begin(); it!=X.end(); it++) { thePrintStream << *it << std::endl; }
  //----*/
        delete interCl_p;
      }
      else {
        const ARInterfaceCSVD* interCl_p = (m >= n)
            ? static_cast<const ARInterfaceCSVD*>(new ARInterfaceCSVDmGEnReg(*A,n,m))
            : static_cast<const ARInterfaceCSVD*>(new ARInterfaceCSVDmLTnReg(*A,m,n));
        ArpackProb Arpb(*interCl_p, nev, "LM");
        setOptParams(pars, upars, &Arpb); controlParams(dimx, Arpb);
        eiel = arpUsrComp<EigenElements>(Arpb);
  /*    vvp = arpUsrComp<VpacVEp>(Arpb);
      res = SvdElements(interCl_p->matPtr(), vvp);
    interCl_p->matPtr()->saveToFile("matClarg.dat",_coo);*/
        delete interCl_p;
      }
    }
    else {// shifted computational mode - ArpackProb II 2a. or 2c. constructor
      std::complex<double> sigc = getShiftValue(pars);
      double sigma = sigc.real() * sigc.real();
      if (A->valueType() == _real) {// ArpackProb II 2a. constructor
        const ARInterfaceRSVD* interCl_p = (m >= n)
            ? static_cast<const ARInterfaceRSVD*>(new ARInterfaceRSVDmGEnShf(*A,n,m,sigma))
            : static_cast<const ARInterfaceRSVD*>(new ARInterfaceRSVDmLTnShf(*A,m,n,sigma));
        ArpackProb Arpb(*interCl_p, nev, "SM");
        setOptParams(pars, upars, &Arpb);
        Arpb.ChangeWhich("SM"); // ensure computational mode
        controlParams(dimx, Arpb);
        eiel = arpUsrComp<EigenElements>(Arpb);
  /*    vvp = arpUsrComp<VpacVEp>(Arpb);
      res = SvdElements(interCl_p->matPtr(), vvp);
    interCl_p->matPtr()->saveToFile("matRlarg.dat",_coo);*/
        delete interCl_p;
      }
      else {// ArpackProb II 2c. constructor
        const ARInterfaceCSVD* interCl_p = (m >= n)
            ? static_cast<const ARInterfaceCSVD*>(new ARInterfaceCSVDmGEnShf(*A,n,m,sigma))
            : static_cast<const ARInterfaceCSVD*>(new ARInterfaceCSVDmLTnShf(*A,m,n,sigma));
        ArpackProb Arpb(*interCl_p, nev, "SM");
        setOptParams(pars, upars, &Arpb);
        Arpb.ChangeWhich("SM"); // ensure computational mode
        controlParams(dimx, Arpb);
        eiel = arpUsrComp<EigenElements>(Arpb);
  /*    vvp = arpUsrComp<VpacVEp>(Arpb);
      res = SvdElements(interCl_p->matPtr(), vvp);
    interCl_p->matPtr()->saveToFile("matClarg.dat",_coo);*/
        delete interCl_p;
      }
      for (std::vector<complex_t>::iterator it=eiel.values.begin(); it != eiel.values.end(); it++)
        { *it += sigma; }// retrieve wanted eigenvalues
    }
    return SvdElements(*A, eiel);
  //  return res;
  }

  // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  // ArpackProb class out of header file implementation:
  // (template function that needs ARrcNonSymGenEig's definition)
  template<class P_> double ArpackProb::getSigmaI(P_& arProb) const {
    ARrcNonSymGenEig<real_t>* nonSymPb_p = dynamic_cast<ARrcNonSymGenEig<real_t>*>(&arProb);
    if (nonSymPb_p) { return nonSymPb_p->GetShiftImag(); }
    else            { return 0.; }
  }

  // ArpackProb class out of header file implementation:
  // (functions that need TermMatrix's definition)
  bool ArpackProb::isMatAReal() const { return A_p->valueType() == _real; }
  bool ArpackProb::isMatASym() const  { return A_p->symmetry() == _symmetric; }

  // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  // ARInterfaceFrame class out of header file implementation:
  // (functions that need TermMatrix's definition)
  template<class K_>
  ARInterfaceFrame<K_>::ARInterfaceFrame(const TermMatrix& charMat, bool stdp, std::string cname)
  : charMat_p(&charMat), dim_(charMat_p->numberOfCols()), stdPb_(stdp), ctor_name_(cname) {
    lx.resize(dim_);
    ly.resize(dim_);
  }
  template ARInterfaceFrame<real_t>::ARInterfaceFrame(const TermMatrix& charMat, bool stdp, std::string cname);
  template ARInterfaceFrame<complex_t>::ARInterfaceFrame(const TermMatrix& charMat, bool stdp, std::string cname);

  template<class K_>
  ARInterfaceFrame<K_>::ARInterfaceFrame(const TermMatrix& charMat, int dim, bool stdp, std::string cname)
  : charMat_p(&charMat), dim_(dim), stdPb_(stdp), ctor_name_(cname) {
    lx.resize(dim_);
    ly.resize(dim_);
  }
  template ARInterfaceFrame<real_t>::ARInterfaceFrame(const TermMatrix& charMat, int dim, bool stdp, std::string cname);
  template ARInterfaceFrame<complex_t>::ARInterfaceFrame(const TermMatrix& charMat, int dim, bool stdp, std::string cname);

  ARInterfaceRSVD::ARInterfaceRSVD(TermMatrix& A, int dimn, int dimx, std::string cname, const real_t sigma)
  : ARStdFrame(A,dimn,cname), matA_p(&A.matrixData()->getLargeMatrix<real_t>()), shift_(sigma) { Tx.resize(dimx); }

  ARInterfaceCSVD::ARInterfaceCSVD(TermMatrix& A, int dimn, int dimx, std::string cname, const real_t sigma)
  : ARStdFrame(A,dimn,cname), matA_p(&A.matrixData()->getLargeMatrix<complex_t>()), shift_(sigma) { Tx.resize(dimx); }

  // * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  /*! User functions to be called after a computation with Arpack. They mimic the Arpack++ member
      functions provided to give access to internal values of the Arpack++ object that are related
      to the last computation of eigen elements done.
      These are external functions ; their names are built from the original names in Arpack++
      by prepending the 2 letters ar.
  */
  bool arParametersDefined() { return _ARprob.ParametersDefined(); }
  bool arGetAutoShift() { return _ARprob.GetAutoShift(); }
  int arGetMaxit() { return _ARprob.GetMaxit(); }
  int arGetMode()  { return _ARprob.GetMode(); }
  int arGetIter()  { return _ARprob.GetIter(); }
  int arGetN()     { return _ARprob.GetN(); }
  int arGetNcv()   { return _ARprob.GetNcv(); }
  int arGetNev()   { return _ARprob.GetNev(); }
  std::complex<double> arGetShift() { return _ARprob.GetShift(); }
  double arGetShiftImag()  { return _ARprob.GetShiftImag(); }
  double arGetTol()        { return _ARprob.GetTol(); }
  std::string arGetWhich() { return _ARprob.GetWhich(); }
  int arConvergedEigenvalues() { return _ARprob.ConvergedEigenvalues(); }
  std::string arGetModeStr()   { return _ARprob.GetModeStr(); }

  //! This function returns a string containing the main informations retrieved from the last computation with Arpack.
  std::string arEigInfos() {
    using std::endl;
    std::ostringstream out;
    out.precision(6);
    out << "Summary of last Arpack computation:" << endl;
    if (!arParametersDefined()) { out << "** Some internal variables were not correctly defined." << endl; }
    out << " Number of eigenvalues (requested / converged): " << arGetNev() << " / " << arConvergedEigenvalues() << endl;
    out << " Computational mode: " << arGetModeStr() << endl;
    if (arGetMode() < 3) {
      out << " Part of the spectrum requested: " << arGetWhich() << endl;
    }
    else {
      out << " Shift used: " << arGetShift() + complex_t(0., arGetShiftImag()) << endl;
    }
    out << " Problem size = " << arGetN() << ",  Tolerance = " << arGetTol() << endl;
    out << " Nb_iter / Nb_iter_Max = " << arGetIter() << " / " << arGetMaxit()
        << ",  Number of Arnoldi vectors = " << arGetNcv() << endl;
    return out.str();
  }

  std::string arpackObj() { return _ARprob.ArpackObj(); }
  std::string arInterfaceObj() { return _ARprob.InterfaceObj(); }
  std::string arKindOfFactorization() { return _ARprob.KindOfFactorization(); }

  } // end of namespace xlifepp
#endif
