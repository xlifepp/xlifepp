/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ArpackProb.hpp
  \author Y. Lafranche
  \since 25 Sep 2015
  \date 25 Sep 2015

  \brief definition of the ArpackProb control class

  Class ArpackProb is a class used as a substitute for the true Arpack++ problem
  which, in the context of XLiFE++ usage, is built internally and consequently not available
  to the user at top level.
  This class allows the user to keep control on the problem being solved:
   1. before the computation of the eigen elements, by setting some parameters that may
      be critical to ensure convergence ;
   2. after the computation, by inquiring about the computational process.

  For ease of use, the member functions of this class have the same name as the member
  functions of the Arpack++'s classes ; these are described in the Arpack++ user's guide
  available at  http://www.ime.unicamp.br/~chico/arpack++/arpackpp.ps.gz

  The functions designed to set parameters begin with Change. All the functions defined
  in Arpack++ are not available here. Only the functions that are useful in the context
  of XLiFE++'s usage are implemented, namely ChangeNcv, ChangeMaxit, ChangeTol,
  ChangeWhich, plus Trace to get informations to be output during the computation.

  The functions designed to inquire information after computation begin with Get, namely
  GetAutoShift, GetMaxit, GetMode, GetModeStr, GetIter, GetN, GetNcv, GetNev, GetShift,
  GetShiftImag, GetTol, GetWhich, plus ConvergedEigenvalues, ParametersDefined.

  \note In addition to GetMode(), the function GetModeStr() returns a string containing
        the description of the computational mode used.

  Warning: Since this class is not templated, the GetShift() function returns a complex
           number, even when the probleme is real ; in this case, obviously, the imaginary
           part will be 0.
           Also, the function GetShiftImag() will silently return 0 in all irrelevant cases
           since this function make sense only for a nonsymmetric generalized problem when
           a complex shift is used.

  Internally, before computation, all the parameters set by the user are transmitted to
  the true Arpack++ problem with the help of the function setParameters. After computation,
  the function getParameters is called in order to insure the Getxxx functions called by
  the user give a correct answer.
*/

#ifndef ARPACK_PROB_HPP
#define ARPACK_PROB_HPP

#include <typeinfo>
#include "../utils/Messages.hpp"

namespace xlifepp {
class TermMatrix; // predeclaration ; class definition intentionally not used here
/*!
   \class ARInterfaceFrame
*/
template<class K_> // K_ is the type (real or complex) of charMat, the matrix defining the problem
class ARInterfaceFrame {
public:
  //! constructors
  ARInterfaceFrame(const TermMatrix& charMat, bool stdp, std::string cname);
  ARInterfaceFrame(const TermMatrix& charMat, int dim, bool stdp, std::string cname);

  //! destructor
  virtual ~ARInterfaceFrame(){}

//   Other public member functions
//   -----------------------------
  //! main matrix-vector product required for all derived classes
  virtual void MultOPx (K_ *x, K_ *y) = 0;

  //! matrix-vector product y <- B * x required for some Arpack++ classes
  virtual void MultBx (K_ *x, K_ *y) { error("nofunc","a class derived from ARInterfaceFrame,","MultBx"); }

  //! matrix-vector product y <- A * x required for some Arpack++ classes
  virtual void MultAx (real_t *x, real_t *y) { error("nofunc","a class derived from ARInterfaceFrame,","MultAx"); }

  //! returns characteristic matrix describing the problem (to retrieve dimension and attached unknown)
  const TermMatrix* characteristicMatrix() const { return charMat_p; }

  //! returns the dimension of the problem to be solved
  int GetN() const { return dim_; }

  //! returns true if the problem to be solved is standard, false if it is a generalized one
  bool isStd() const { return stdPb_; }

  //! return the name of the interface object and constructor used
  std::string ConstructorName() const { return ctor_name_; }

protected:
  //! work vectors used in MultOPx, resized to dim in constructor
  std::vector<K_> lx, ly;
private:
  //! pointer to characteristic matrix describing the problem
  const TermMatrix* charMat_p;
  //! dimension of the problem to be solved
  int dim_;
  //! kind of problem: standard (true) or generalized (false)
  bool stdPb_;
  //! name of the interface object and constructor used
  std::string ctor_name_;

}; // end of Class ARInterfaceFrame =============================================

template<class K_>
class ARStdFrame : public ARInterfaceFrame<K_> {
public:
  //! constructor
  ARStdFrame(const TermMatrix& charMat, std::string cname = "")
  : ARInterfaceFrame<K_>(charMat, true,
    cname.empty()?std::string("user class derived from ARStdFrame<")+typeid(K_).name()+std::string(">"):cname) { }
  ARStdFrame(const TermMatrix& charMat, int dim, std::string cname = "")
  : ARInterfaceFrame<K_>(charMat, dim, true,
    cname.empty()?std::string("user class derived from ARStdFrame<")+typeid(K_).name()+std::string(">"):cname) { }

}; // end of Class ARStdFrame ===================================================

template<class K_>
class ARGenFrame : public ARInterfaceFrame<K_> {
public:
  //! constructor
  ARGenFrame(const TermMatrix& charMat, std::string cname = "")
  : ARInterfaceFrame<K_>(charMat, false,
    cname.empty()?std::string("user class derived from ARGenFrame<")+typeid(K_).name()+std::string(">"):cname) { }
  ARGenFrame(const TermMatrix& charMat, int dim, std::string cname = "")
  : ARInterfaceFrame<K_>(charMat, dim, false,
    cname.empty()?std::string("user class derived from ARGenFrame<")+typeid(K_).name()+std::string(">"):cname) { }

//   Other public member functions
//   -----------------------------
  //! matrix-vector product y <- B * x required for some Arpack++ classes
  virtual void MultBx (K_ *x, K_ *y) = 0;

   //! matrix-vector product y <- A * x required for some Arpack++ classes
  virtual void MultAx (real_t *x, real_t *y) { error("nofunc","a class derived from ARGenFrame,","MultAx"); }

}; // end of Class ARGenFrame ===================================================

/*!
   \class ArpackProb
*/

class ArpackProb {
public:
//-------------------------------------------------------------------------------
//  Constructors
//-------------------------------------------------------------------------------
  // I. Constructors from TermMatrix objects, internally used by XLiFE++.
  //    User is allowed to call them as well.
  // Constructors for standard eigenvalue problems
  // 1a. Regular mode
  ArpackProb(TermMatrix* A, int nevp)
  : A_p(A), B_p(nullptr), charMat_p(A_p), stdPb(true), cMode('-'), AnonSym_(false),
    realPb(isMatAReal()), symPb(isMatASym()), nev(nevp), sigmaR(0.0), sigmaI(0.0)
   { defaultParams(); }
  // 1b. Shift and invert mode
  ArpackProb(TermMatrix* A, int nevp, double sigmap, double sigmaIp=0.0)
  : A_p(A), B_p(nullptr), charMat_p(A_p), stdPb(true), cMode('S'), AnonSym_(false),
    realPb(isMatAReal()), symPb(isMatASym()), nev(nevp), sigmaR(sigmap,sigmaIp), sigmaI(sigmaIp)
   { defaultParams(); }

  // Constructors for generalized eigenvalue problems
  // 2a. Regular mode
  ArpackProb(TermMatrix* A, TermMatrix* B, int nevp)
  : A_p(A), B_p(B), charMat_p(A_p), stdPb(false), cMode('-'), AnonSym_(false),
    realPb(isMatAReal()), symPb(isMatASym()), nev(nevp), sigmaR(0.0), sigmaI(0.0)
   { defaultParams(); }
  // 2b. A real symmetric, shift and invert mode, buckling and Cayley mode
  //     A real nonsymmetric, (real) shift and invert mode
  ArpackProb(TermMatrix* A, TermMatrix* B, int nevp, double sigmap, char cModep = 'S')
  : A_p(A), B_p(B), charMat_p(A_p), stdPb(false), cMode(cModep), AnonSym_(false),
    realPb(isMatAReal()), symPb(isMatASym()), nev(nevp), sigmaR(sigmap), sigmaI(0.0)
   { defaultParams(); }
  // 2c. A real nonsymmetric, complex shift and invert mode
  //     A complex, shift and invert mode
  ArpackProb(TermMatrix* A, TermMatrix* B, int nevp, double sigmap, double sigmaIp, char cModep = 'S')
  : A_p(A), B_p(B), charMat_p(A_p), stdPb(false), cMode(cModep), AnonSym_(false),
    realPb(isMatAReal()), symPb(isMatASym()), nev(nevp), sigmaR(sigmap,sigmaIp), sigmaI(sigmaIp)
   { defaultParams(); }

  // II. Constructors from user class
  // 1. Constructors for regular mode (for standard or generalized eigenvalue problems)
  // 1a. real case
   ArpackProb(const ARInterfaceFrame<real_t>& usrcl, int nevp, const char* whichp, bool symp = true)
  : A_p(nullptr), B_p(nullptr), charMat_p(usrcl.characteristicMatrix()), stdPb(usrcl.isStd()), rusrcl_p(&usrcl), cusrcl_p(nullptr),
    cMode('-'), AnonSym_(false), realPb(true), symPb(symp), intob_obj_(usrcl.ConstructorName()),
    which(std::string(whichp)), nev(nevp), sigmaR(0.0), sigmaI(0.0)
   { defaultParams(); }//InterfaceObj(std::string("ARInterfaceFrame<real_t> provided by the user")); }
  // 1b. complex case
   ArpackProb(const ARInterfaceFrame<complex_t>& usrcl, int nevp, const char* whichp)
  : A_p(nullptr), B_p(nullptr), charMat_p(usrcl.characteristicMatrix()), stdPb(usrcl.isStd()), rusrcl_p(nullptr), cusrcl_p(&usrcl),
    cMode('-'), AnonSym_(false), realPb(false), symPb(false), intob_obj_(usrcl.ConstructorName()),
    which(std::string(whichp)), nev(nevp), sigmaR(0.0), sigmaI(0.0)
   { defaultParams(); }//InterfaceObj(std::string("ARInterfaceFrame<complex_t> provided by the user")); }

  // 2. Constructors for shifted computational mode (for standard or generalized eigenvalue problems)
  //    Note: for standard eigenvalue problems, the last argument (computational mode) is irrelevant.
  // 2a. real symmetric case:
  //       - for standard problems: shift and invert mode (default)
  //       - for generalized problems: shift and invert mode (default), buckling and Cayley mode
  //     real nonsymmetric case:
  //       - for standard or generalized problems: (real) shift and invert mode
   ArpackProb(const ARInterfaceFrame<real_t>& usrcl, int nevp, bool symp, double sigmap, char cModep = 'S')
  : A_p(nullptr), B_p(nullptr), charMat_p(usrcl.characteristicMatrix()), stdPb(usrcl.isStd()), rusrcl_p(&usrcl), cusrcl_p(nullptr),
    cMode(cModep), AnonSym_(false), realPb(true), symPb(symp), intob_obj_(usrcl.ConstructorName()),
    nev(nevp), sigmaR(sigmap), sigmaI(0.0)
   { defaultParams(); }//InterfaceObj(std::string("ARInterfaceFrame<real_t> provided by the user")); }
  // 2b. real nonsymmetric case, for generalized problems only: complex shift and invert mode
   ArpackProb(const ARInterfaceFrame<real_t>& usrcl, int nevp, double sigmap, double sigmaIp, char cModep = 'R')
  : A_p(nullptr), B_p(nullptr), charMat_p(usrcl.characteristicMatrix()), stdPb(usrcl.isStd()), rusrcl_p(&usrcl), cusrcl_p(nullptr),
    cMode(cModep), AnonSym_(false), realPb(true), symPb(false), intob_obj_(usrcl.ConstructorName()),
    nev(nevp), sigmaR(sigmap,sigmaIp), sigmaI(sigmaIp)
   { defaultParams(); }//InterfaceObj(std::string("ARInterfaceFrame<real_t> provided by the user")); }
  // 2c. complex case, for standard or generalized problems: shift and invert mode
   ArpackProb(const ARInterfaceFrame<complex_t>& usrcl, int nevp, double sigmap, double sigmaIp = 0.0)
  : A_p(nullptr), B_p(nullptr), charMat_p(usrcl.characteristicMatrix()), stdPb(usrcl.isStd()), rusrcl_p(nullptr), cusrcl_p(&usrcl),
    cMode('S'), AnonSym_(false), realPb(false), symPb(false), intob_obj_(usrcl.ConstructorName()),
    nev(nevp), sigmaR(sigmap,sigmaIp), sigmaI(sigmaIp)
   { defaultParams(); }//InterfaceObj(std::string("ARInterfaceFrame<complex_t> provided by the user")); }

//-------------------------------------------------------------------------------
//  Public member functions
//-------------------------------------------------------------------------------
  // 0. User functions to set or get the way the eigenvalues are sorted
  void SortKind(EigenSortKind sk) { sortKind = sk; }
  EigenSortKind SortKind() { return sortKind; }

  // The functions listed in groups 1 and 2 below are user functions with the same name
  // and calling sequence as the member functions of a "true" Arpack++ problem described
  // in Arpack++'s documentation. The page numbers refer to this documentation.

  // 1a. Functions that allow changes in problem parameters (page 31 and 160)
  void ChangeNcv(int ncvp) { ncv = ncvp; }
  void ChangeMaxit(int maxitp) { maxit = maxitp; }
  void ChangeTol(double tolp) { tol = tolp; }
  void ChangeWhich(const std::string& whichp) { which = whichp; }

  // 1b. Trace function (page 164)
  void Trace(const int digit = -5, const int getv0 = 0, const int aupd = 1,
             const int aup2 = 0,   const int aitr = 0,  const int eigt = 0,
             const int apps = 0,   const int gets = 0,  const int eupd = 0) {
    digit_ = digit; getv0_ = getv0; aupd_ = aupd;
    aup2_ = aup2; aitr_ = aitr; eigt_ = eigt;
    apps_ = apps; gets_ = gets; eupd_ = eupd;
     trace_ = true;
  }

  // 2. Functions that provide access to internal variables values (page 32 and 156)
  bool ParametersDefined() const { return PrepareOK; }
  bool GetAutoShift() const { return AutoShift; }
  int GetMaxit() const { return maxit; }
  int GetMode()  const { return mode; }
  int GetIter()  const { return niter; }
  int GetN()     const { return n; }
  int GetNcv()   const { return ncv; }
  int GetNev()   const { return nev; }
  std::complex<double> GetShift() const { return sigmaR; }
  double GetShiftImag() const { return sigmaI; }
  double GetTol() const { return tol; }
  std::string GetWhich() const { return which; }

  int ConvergedEigenvalues() const { return nconv; }

  std::string GetModeStr() const {
  using std::string;
    switch(mode) {
      case 1: return string("regular mode (standard problem)");
      case 2: return string("regular inverse mode (generalized problem)");
      case 3: if (isReal() && (cptnMode() != 'S') ) {// real nonsymmetric generalized problem
                return string("complex shift and invert mode with OP = real{inv(A-sigma*B))} (real nonsymmetric generalized problem)");}
              else {
                return string("shift and invert mode"); }
      case 4: if (isSym() && ! isForcedNonSym()) {
                return string("buckling mode (real symmetric generalized problem)"); }
              else {
                return string("complex shift and invert mode with OP = imag{inv(A-sigma*B))} (real nonsymmetric generalized problem)");}
      case 5: return string("Cayley mode (real symmetric generalized problem)");
      default: return string("undefined mode (this is an error)");
    }
  }

  // 3. Function used to set some parameters to the "true" Arpack++ problem arProb before computation
  //    as requested by the user with the functions of group 1 above.
  //    For internal usage only.
  template<class P_> void setParameters(P_& arProb) const {
    if (ncv   != 0)      { arProb.ChangeNcv(ncv); }
    if (maxit != 0)      { arProb.ChangeMaxit(maxit); }
    if (tol   != 0.)     { arProb.ChangeTol(tol); }
    if (! which.empty()) { arProb.ChangeWhich(which); }
    if (trace_)          { arProb.Trace(digit_, getv0_, aupd_, aup2_, aitr_, eigt_, apps_, gets_, eupd_); }
  }

  // 4. Function used to get some parameters from the "true" Arpack++ problem arProb after computation
  //    so that the user can be sure of what Arpack really did, with the functions of group 2 above.
  //    For internal usage only.
  template<class P_> void getParameters(P_& arProb) {
    PrepareOK = arProb.ParametersDefined();
    AutoShift = arProb.GetAutoShift();
    maxit = arProb.GetMaxit();
    mode  = arProb.GetMode();
    niter = arProb.GetIter();
    n     = arProb.GetN();
    ncv   = arProb.GetNcv();
    nev   = arProb.GetNev();
    tol   = arProb.GetTol();
    which = arProb.GetWhich();
    nconv = arProb.ConvergedEigenvalues();
    sigmaR= arProb.GetShift();
    sigmaI= getSigmaI(arProb);
  }
  template<class P_> double getSigmaI(P_& arProb) const;

  // 5. Functions designed for internal use only
  TermMatrix* getPtrA() const { return A_p; }
  TermMatrix* getPtrB() const { return B_p; }
  ARInterfaceFrame<real_t>&    getrUsrCl() const { return const_cast<ARInterfaceFrame<real_t>&>   (*rusrcl_p); }
  ARInterfaceFrame<complex_t>& getcUsrCl() const { return const_cast<ARInterfaceFrame<complex_t>&>(*cusrcl_p); }
  const TermMatrix* characteristicMatrix() const { return charMat_p; }
  // -> the previous function is only used internally to retrieve some technical data related to the problem,
  //    namely the dimension and storage information linked to the attached unknown.
  char cptnMode()   const { return cMode; }
  bool isStd()      const { return stdPb; }
  bool isRegular()  const { return cMode == '-'; }
  bool isReal()     const { return realPb; }
  bool isSym()      const { return symPb; }
  bool isMatAReal() const; // type of matrix A, if A available
  bool isMatASym()  const; // symmetry of matrix A, if A available

  // The following function is only relevant when matrix A is available and symmetric,
  // to allow use of non symetric algorithms
  void forceNonSym()                 { AnonSym_ = true; }
  bool isForcedNonSym() const { return AnonSym_; }

  std::string ArpackObj() const   { return arp_obj_; }
  void ArpackObj(const std::string& str) { arp_obj_= str; }
  std::string InterfaceObj() const   { return intob_obj_; }
  void InterfaceObj(const std::string& str) { intob_obj_= str; }
  std::string KindOfFactorization() const   { return fact_name_; }
  void KindOfFactorization(const std::string& str) { fact_name_ = str; }

  // Access functions to the name to be transmitted to the final XLiFE++'s result object.
  void setResName(const std::string& namep) { res_name_ = namep; }
  std::string getResName() const { return res_name_; }

private:
  // Data members for internal use
  TermMatrix *A_p, *B_p; //!< Pointers to matrices defining the problem
  const TermMatrix* charMat_p; //!< pointer to characteristic matrix describing the problem
  bool stdPb;            //!< kind of problem: standard (true) or generalized (false)
  const ARInterfaceFrame<real_t>* rusrcl_p;    //!< pointer to user class (real case)
  const ARInterfaceFrame<complex_t>* cusrcl_p; //!< pointer to user class (complex case)
  char cMode;            //!< Computational mode requested for a real generalized problem (non regular case)
  /* cMode should be one of 'S', 'B', 'C', 'R', 'I' according to the following table:
     cMode for A symmetric           cMode for A nonsymmetric
     'S'  = Shift and Inverse mode    'S'  = Real Shift and Inverse mode
     'B'  = Buckling mode             'R'  = Complex Shift and Inverse mode using Re(inv(A-sigma*B))
     'C'  = Cayley mode               'I'  = Complex Shift and Inverse mode using Im(inv(A-sigma*B))
  */
  bool AnonSym_;   //!< true means: if A is real symmetric, a computational mode intended for a real
                   // nonsymmetric matrix will be used ; irrelevant if A is not real symmetric.
  bool realPb;     //!< true if the problem to be solved is real, false otherwise (thus complex)
  bool symPb;      //!< true if the problem to be solved is symmetric, false otherwise
  EigenSortKind  sortKind; //!< sort the computed eigevalues by module (0=default), real part (1) or imag part (2)

  // Record parameters related to Arpack++ and the interface object between XLiFE++ and Arpack++
  std::string arp_obj_;   //!< name of the Arpack++ object
  std::string intob_obj_; //!< name of the interface object and constructor used
  std::string fact_name_; //!< name of the algorithm used to compute the factorization

  // The following data members are the same as in Arpack++, with the original comment.
  // Parameters that can be set by the user before computation
  int     ncv;        //!< Number of Arnoldi vectors generated at each iteration.
  int     maxit;      //!< Maximum number of Arnoldi update iterations allowed.
  std::string  which; //!< Specify which of the Ritz values of OP to compute.
  double  tol;        //!< Stopping criterion (relative accuracy of Ritz values).

  int digit_, getv0_, aupd_, aup2_, aitr_, eigt_, apps_, gets_, eupd_;
  bool trace_;

  // Parameters retrieved from the Arpack++ problem after computation.
  // Some of them are true results (e.g. niter) ; some others are input data that are
  // retrieved back after computation to make sure of the computation context.
  bool    PrepareOK;  //!< Indicates if internal variables were correctly set.
  bool    AutoShift;  //!< Indicates if implicit shifts will be generated
                      // internally (or will be supplied by the user).
  int     mode;       //!< Indicates the type of the eigenproblem (regular,
                      // shift and invert, etc).
  int     niter;      //!< Actual number of Arnoldi iterations taken
  int     n;          //!< Dimension of the eigenproblem.
  int     nev;        //!< Number of eigenvalues to be computed. 0 < nev < n-1.
  int     nconv;      //!< Number of "converged" Ritz values.
  std::complex<double> sigmaR; //!< Shift (real part only if problem is nonsymmetric).
  double  sigmaI;     //!< Imaginary part of shift (for nonsymmetric problems).

  //! The following data member has nothing to do with Arpack.
  //! It is put here just to simplify its transmission to the final XLiFE++'s result object.
  std::string res_name_;

  //! Private function
  //! Initialization of parameters to their default value.
  //! -> Some data members are initialized to default values tested in setParameters
  void defaultParams() {
    arp_obj_ = fact_name_ = std::string("unknown");
    sortKind = _incr_module;
    ncv = maxit = 0;
    tol = 0.0;
    trace_ = PrepareOK = AutoShift = false;
    mode = niter = n = nconv = 0;
  }
}; // end of Class ArpackProb ===================================================


// Declaration of user functions to be called after a computation with Arpack.
// They are defined in arpackSolve.cpp
bool arParametersDefined();
bool arGetAutoShift();
int arGetMaxit();
int arGetMode();
int arGetIter();
int arGetN();
int arGetNcv();
int arGetNev();
std::complex<double> arGetShift();
double arGetShiftImag();
double arGetTol();
std::string arGetWhich();
int arConvergedEigenvalues();
std::string arGetModeStr();
//! This function calls the previous ones, gather the informations in a string which is returned
std::string arEigInfos();

std::string arInterfaceObj();
std::string arpackObj();
std::string arKindOfFactorization();

} // end of namespace xlifepp

#endif /* ARPACK_PROB_HPP */
