/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ARGenToStd.hpp
  \author Y. Lafranche
  \since 15 Nov 2016
  \date  15 Nov 2016

  \brief Definition of the xlifepp::ARGenToStdReg and xlifepp::ARGenToStdShf classes


*/

#ifndef ARGENTOSTD_H
#define ARGENTOSTD_H

#include "ARInterface.hpp"

namespace xlifepp {

/*!
  \class ARGenToStdReg
  Class used to transform a generalized problem into a standard one.
  B is supposed to be invertible.
  Regular mode case.
  Matrix-vector product provided: y <- inv(B)*A * x
*/
template <class TA_, class TB_> // TA_ (resp. TB_) is the type of the matrix A (resp. B)
class ARGenToStdReg : public ARStdFrame<TA_> {
public:
  //! Constructor
  ARGenToStdReg(TermMatrix& A, TermMatrix& B);

  //! Destructor
  ~ARGenToStdReg();

  //! Matrix-vector product required: y <- inv(B)*A * x
  void MultOPx (TA_ *x, TA_ *y);

private:
  //! pointers to internal data objects
  const LargeMatrix<TA_> *matA_p;
  const LargeMatrix<TB_> *matB_p;
  //! pointer to temporary factorized matrix B
  typename FactPtrTrait<LargeMatrix<TB_> >::FactPtr fact_p;
  typename SolverPtrTrait<TA_, LargeMatrix<TB_> >::SolverPtr Solver_;

  //! Compute the factorisation of *matB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARGenToStdReg(const ARGenToStdReg&);
  ARGenToStdReg & operator=(const ARGenToStdReg&);

  //! work vectors used in MultOPx, resized to dimension of the problem in constructor
  std::vector<TA_> Ax;

}; // end of Class ARGenToStdReg =========================================

/*!
  \class ARGenToStdShf
  Class used to transform a generalized problem into a standard one.
  B is supposed to be invertible.
  Shift mode case.
  Matrix-vector product provided: y <- inv(inv(B)*A-sigma*Id)*x
  sigma has the type of A
*/
template <class TA_, class TB_>
class ARGenToStdShf : public ARStdFrame<TA_> {
public:

  //! Constructor
  ARGenToStdShf(TermMatrix& A, TermMatrix& B, const TA_ sigma);

  //! Destructor
  ~ARGenToStdShf(); // override

  //! Matrix-vector product y <- inv(inv(B)*A-sigma*Id)*x
  void MultOPx (TA_ *x, TA_ *y); // override

private:
  //! pointers to internal data objects
  const LargeMatrix<TA_> *matA_p;
  const LargeMatrix<TB_> *matB_p;
  /*! A - sigma B
      Built temporarily from the given data A, sigma and B. */
  const LargeMatrix<TA_> *matAsB_p;

  //! pointer to temporary factorized matrix A-sigma B
  typename FactPtrTrait<LargeMatrix<TA_> >::FactPtr fact_p;
  typename SolverPtrTrait<TA_, LargeMatrix<TA_> >::SolverPtr Solver_;

  //! Compute the factorisation of *matAsB_p and select the solver
  void factorize();

  //! Duplicating such object is disabled
  ARGenToStdShf(const ARGenToStdShf&);
  ARGenToStdShf & operator=(const ARGenToStdShf&);

  //! work vectors used in MultOPx, resized to dimension of the problem in constructor
  std::vector<TA_> Ax;

}; // end of Class ARGenToStdShf ============================================

} // end of namespace xlifepp

#endif /* ARGENTOSTD_H */
