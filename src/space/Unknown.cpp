/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Unknown.cpp
  \author E. Lunéville
  \since 20 fev 2012
  \date 12 jun 2012

  \brief Implementation of xlifepp::Unknown class and related functionnalities
*/

#include "Unknown.hpp"

namespace xlifepp
{
  number_t Unknown::last_rank=0;

//------------------------------------------------------------------------------------
// Unknown member functions
//------------------------------------------------------------------------------------
// constructors,destructor
Unknown::Unknown()
  : name_(""), space_p(nullptr), nbOfComponents_(0), conjugate_(false), dualUnknown_(nullptr), rank_(0), isUnknown(true)
{}

void Unknown::buildParam(const Parameter& p)
{
  ParameterKey key=p.key();
  switch (key)
  {
    case _pk_name:
    {
      if (p.type() == _string) { name_=p.get_s(); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      if (findUnknown(name_) != nullptr) { error("unknown_redefined", name_); }
      break;
    }
    case _pk_rank:
    {
      if (p.type() == _integer) { rank_=p.get_n(); }
      else { error("param_badtype",words("value",p.type()),words("param key",key)); }
      break;
    }
    case _pk_dim:
    {
      switch (p.type())
      {
        case _integer:
        case _enumDimensionType:
          nbOfComponents_=p.get_n(); // items of DimensionType have the value corresponding to the dim
          break;
        default: error("param_badtype",words("value",p.type()),words("param key",key));
      }
      break;
    }
    default:
      error("unknown_unexpected_param_key",words("param key",key));
  }
}

void Unknown::build(Space* sp_p, Unknown* u_p, bool iu, const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams;
  params.insert(_pk_name);
  params.insert(_pk_rank);
  params.insert(_pk_dim);

  name_="";
  nbOfComponents_=1;
  rank_=0;

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    buildParam(p);
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParams.find(key) == usedParams.end()) { error("unknown_unexpected_param_key", words("param key",key)); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParams.insert(key);
  }

  space_p=sp_p;
  dualUnknown_=u_p;
  conjugate_ = false;
  isUnknown=iu;

  if (sp_p->dimFun()>1 && nbOfComponents_>1)
  {
    warning("free_warning","handling a vector unknown ("+name_+") of a vector space basis ("+sp_p->name()+") is hazardous");
  }

  // default value for rank
  if (u_p != nullptr)
  {
    if (u_p->dualUnknown_ != nullptr) { error("dual_unknown_already_defined", u_p->name_); }
    u_p->dualUnknown_=this;
    if (rank_==0) { rank_=u_p->rank_; }
    nbOfComponents_=u_p->nbOfComponents_;
  }
  else
  {
    if (rank_==0) { rank_=++last_rank; }
  }

  // default value for name
  if (name_ == "") { name_="unknown_"+tostring(rank_); }

  // add to the spaces list and get the index
  theUnknowns.push_back(this);
}

Unknown::Unknown(Space& sp, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  build(&sp, 0, true, ps);
}

Unknown::Unknown(Space& sp, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  build(&sp, 0, true, ps);
}

Unknown::Unknown(Space& sp, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(&sp, 0, true, ps);
}

Unknown::Unknown(Unknown& u, const Parameter& p1)
{
  std::vector<Parameter> ps={p1};
  build(u.space_p, &u, false, ps);
}

Unknown::Unknown(Unknown& u, const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps={p1, p2};
  build(u.space_p, &u, false, ps);
}

Unknown::Unknown(Unknown& u, const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps={p1, p2, p3};
  build(u.space_p, &u, false, ps);
}

Unknown::Unknown(Space& sp, const string_t& nm, dimen_t d, number_t r)
{
  warning("deprecated", "Unknown(Space, String, Dimen, Number)", "Unknown(Space, _name=?, _dim=?, _rank=?)");
  // check if unknown already exists (unique name!)
  if (findUnknown(nm) != nullptr) { error("unknown_redefined", nm); }
  name_ = nm;
  nbOfComponents_=d;
  space_p = &sp;
  if (sp.dimFun()>1 && d>1) { warning("free_warning","handling a vector unknown ("+name_+") of a vector space basis ("+sp.name()+") is hazardous" ); }
  conjugate_ = false;
  dualUnknown_=nullptr;
  isUnknown=true;
  // add to the spaces list and get the index
  theUnknowns.push_back(this);
  rank_=r;
  //if (rank_==0) rank_=theUnknowns.size();
  if (rank_==0) rank_=++last_rank;
}

Unknown::Unknown(Space& sp, const char* nm, dimen_t d, number_t r)
{
  warning("deprecated", "Unknown(Space, String, Dimen, Number)", "Unknown(Space, _name=?, _dim=?, _rank=?)");
  // check if unknown already exists (unique name!)
  if (findUnknown(nm) != nullptr) { error("unknown_redefined", nm); }
  name_ = nm;
  nbOfComponents_=d;
  space_p = &sp;
  if (sp.dimFun()>1 && d>1) { warning("free_warning","handling a vector unknown ("+name_+") of a vector space basis ("+sp.name()+") is hazardous" ); }
  conjugate_ = false;
  dualUnknown_=nullptr;
  isUnknown=true;
  // add to the spaces list and get the index
  theUnknowns.push_back(this);
  rank_=r;
  //if (rank_==0) rank_=theUnknowns.size();
  if (rank_==0) rank_=++last_rank;
}

//testfunction (dual unknown) constructor from unknown (user)
Unknown::Unknown(Unknown& u, const string_t& nm, number_t r)
{
  warning("deprecated", "Unknown(Unknown, String, Number)", "Unknown(Unknown, _name=?, _rank=?)");
  // check if unknown already exists (unique name!)
  name_ = nm;
  if (nm=="") name_=u.name_+"dual";
  if (findUnknown(nm) != nullptr) { error("unknown_redefined", nm); }
  space_p = u.space_p;
  nbOfComponents_=u.nbOfComponents_;
  conjugate_ = false;
  dualUnknown_=&u;
  isUnknown=false;
  if (u.dualUnknown_ != nullptr) { error("dual_unknown_already_defined", u.name_); }
  u.dualUnknown_=this;
  // add to the spaces list and get the index
  theUnknowns.push_back(this);
  rank_=r;
  if (rank_==0) rank_=u.rank_;    //same rank as u
}

//testfunction (dual unknown) constructor from unknown (user)
Unknown::Unknown(Unknown& u, const char* nm, number_t r)
{
  warning("deprecated", "Unknown(Unknown, String, Number)", "Unknown(Unknown, _name=?, _rank=?)");
  // check if unknown already exists (unique name!)
  name_ = nm;
  if (name_=="") name_=u.name_+"_dual";
  if (findUnknown(nm) != nullptr) { error("unknown_redefined", nm); }
  space_p = u.space_p;
  nbOfComponents_=u.nbOfComponents_;
  conjugate_ = false;
  dualUnknown_=&u;
  isUnknown=false;
  if (u.dualUnknown_ != nullptr) { error("dual_unknown_already_defined", u.name_); }
  u.dualUnknown_=this;
  // add to the spaces list and get the index
  theUnknowns.push_back(this);
  rank_=r;
  if (rank_==0) rank_=u.rank_;    //same rank as u
}

Unknown::~Unknown()
{
  // delete components of unknown if created
  if (!isComponent() && nbOfComponents() > 1)
  {
    for (dimen_t i = 1; i <= nbOfComponents(); i++)
    {
      string_t nam = name_ + "_" + tostring(i);
      Unknown* u = findUnknown(nam);
      if (u != nullptr) { delete u; }
    }
  }
  // erase from list of Unknowns
  it_vu it(find(theUnknowns.begin(), theUnknowns.end(), this));
  if (it != theUnknowns.end()) { theUnknowns.erase(it); }
}

//find space by name
Unknown* Unknown::findUnknown(const string_t& na)
{
  std::vector<Unknown*>::iterator it;
  for (it = Unknown::theUnknowns.begin(); it != Unknown::theUnknowns.end(); it++)
    if ((*it)->name()==na) return *it;
  return nullptr;
}

// delete all unknown objects
void Unknown::clearGlobalVector()
{
  while ( Unknown::theUnknowns.size() > 0 ) { delete Unknown::theUnknowns[0];}
  last_rank=0;
}

// delete and remove unknowns relating to Space sp
void clearUnknowns(const Space& sp)
{
  for (auto it=Unknown::theUnknowns.begin(); it!=Unknown::theUnknowns.end(); ++it)
  {
    if((*it)->space()==&sp)
    {
      delete *it;  // erase from global vector
      it=Unknown::theUnknowns.begin();
    }
  }
}

// print the list of unknowns in memory
void Unknown::printAllUnknowns(std::ostream& out)
{
  number_t vb=theVerboseLevel;
  verboseLevel(1);
  out<<"Unknowns in memory: "<<eol;
  std::vector<Unknown*>::iterator it=theUnknowns.begin();
  for (; it!=theUnknowns.end(); ++it) out<<"  "<<(*it)<<": "<<(**it)<<eol;
  verboseLevel(vb);
}

//return the dual unknown
const Unknown& Unknown::dual() const
{
  if (dualUnknown_==nullptr) error("no_dual_unknown",name_);
  return *dualUnknown_;
}

// return the index of unknown in the unknowns list
// useful to order the unknowns
number_t Unknown:: index() const
{
  for (number_t i = 0; i < theUnknowns.size(); i++)
    if (theUnknowns[i] == this) { return i;}
  error("unknown_noindex", this->name_);
  return 0;
}

// return the type of unknown (scalar or vector)
StrucType Unknown::strucType() const
{
  if (nbOfComponents_ == 1) { return _scalar; }
  return _vector;
}

// type of unknown, given by its space
UnknownType Unknown::type() const
{
  switch (space_p->typeOfSubSpace())
  {
    case _feSpace: return _feUnknown;
    case _spSpace: return _spUnknown;
    case _prodSpace: return _mixedUnknown;
    default: error("unknown_undef_type", space_p->typeOfSpace());
  }
  return _mixedUnknown; // fake return
}

// create in memory the ComponentOfUnknown u_i, i=1,..,d
Unknown& Unknown::operator[](dimen_t i)
{
  // find if it already exists
  string_t nam = name_ + "_" + tostring(i);
  Unknown* u = findUnknown(nam);
  if (u != nullptr) { return *u; }
  // create a new unknown (ComponentOfUnknown)
  dimen_t d = nbOfComponents_;
  //dimen_t d = std::max(nbOfComponents_,dimFun());
  if (d < 2) { return *this; } // nothing to create, return itself
  if (i < 1 || i > d) { error("unknown_indexoutside",name_); }
  ComponentOfUnknown* cu = new ComponentOfUnknown(*this, i);
  u = static_cast<Unknown*>(cu); //downcast
  theUnknowns.push_back(u);

  if (dualUnknown_==nullptr) return *u;
  // create also dual component function, if dualUnknown !=nullptr
  {
   ComponentOfUnknown* cv = new ComponentOfUnknown(*dualUnknown_, i);
   cv->dualUnknown_=u;
   u->dualUnknown_=static_cast<Unknown*>(cv);
   theUnknowns.push_back(static_cast<Unknown*>(cv));
  }
  return *u;
}

/*! set the rank of unknown
*/
void Unknown::setRank(number_t r)
{
  for (number_t i = 0; i < theUnknowns.size(); i++)
  {
    Unknown* ui=theUnknowns[i];
    if (ui!=this && ui->rank_==r )
    {
      where("Unknown::setRank");
      error("unknown_rank_already_used",r,ui->name_);
    }
  }
  rank_=r;
}

//--------------------------------------------------------------------------------
// External related functions
//--------------------------------------------------------------------------------

// find an unknown by its name
Unknown* findUnknown(const string_t& nm)
{
  for (cit_vu it = Unknown::theUnknowns.begin(); it != Unknown::theUnknowns.end(); it++)
    if (nm == (*it)->name()) { return *it; }
  return nullptr;
}

// set to true or false the temporary conjugate flag
Unknown& conj(Unknown& u)
{
  u.conjugate_ = !u.conjugate_;
  return u;
}

//set ranks of unknowns given by vector of same size
void setRanks(std::vector<Unknown*>& us,const std::vector<number_t>& rs)
{
  if (us.size()!=rs.size())
  {
    where("setRanks(Vector<Unknown*>, Vector<Number>)");
    error("unknowns_ranks_mismatch_size",us.size(), rs.size());
  }
  std::set<Unknown*> sus(us.begin(),us.end());
  std::map<number_t,Unknown*> notfree;
  for (it_vu it = Unknown::theUnknowns.begin(); it != Unknown::theUnknowns.end(); it++)
  {
    if (sus.find(*it)!=sus.end()) notfree[(*it)->rank()]=*it;
  }
  std::vector<number_t>::const_iterator itr;
  for (itr=rs.begin();itr!=rs.end();itr++)
  {
    std::map<number_t,Unknown*>::iterator itm=notfree.find(*itr);
    if (itm!=notfree.end()) error("unknown_rank_already_used",*itr,itm->second->name());
  }
  std::vector<Unknown*>::iterator itu=us.begin();
  itr=rs.begin();
  for (;itu!=us.end(); itu++, itr++) (*itu)->setRank(*itr);
}

void setRanks(Unknown&u1, number_t r1)
{
  u1.setRank(r1);
}
void setRanks(Unknown&u1, number_t r1, Unknown& u2, number_t r2)
{
  std::vector<Unknown*> us(2,&u1);
  us[1]=&u2;
  std::vector<number_t> rs(2,r1);
  rs[1]=r2;
  setRanks(us,rs);
}

void setRanks(Unknown&u1, number_t r1, Unknown& u2, number_t r2, Unknown& u3, number_t r3)
{
  std::vector<Unknown*> us(3,&u1);
  us[1]=&u2; us[2]=&u3;
  std::vector<number_t> rs(3,r1);
  rs[1]=r2; rs[2]=r3;
  setRanks(us,rs);
}

void setRanks(Unknown&u1, number_t r1, Unknown& u2, number_t r2, Unknown& u3, number_t r3, Unknown& u4, number_t r4)
{
  std::vector<Unknown*> us(4,&u1);
  us[1]=&u2; us[2]=&u3; us[3]=&u4;
  std::vector<number_t> rs(4,r1);
  rs[1]=r2; rs[2]=r3; rs[3]=r4;
  setRanks(us,rs);
}

// print utility
std::ostream& operator<<(std::ostream& os, const Unknown& u)
{
  if (theVerboseLevel > 0)
  {
    string_t w=words("unknown");
    if (u.isTestFunction()) w=words("test function");
    os << w << " \"" << u.name_ << "\" (" << u.rank_ << ") ";
    if(find (Space::theSpaces.begin(), Space::theSpaces.end(), u.space_p)!=Space::theSpaces.end())  // may be deallocated
      os << (*u.space_p);
  }
  return os;
}

//------------------------------------------------------------------------------------
// ComponentOfUnknown member functions
//------------------------------------------------------------------------------------
ComponentOfUnknown::ComponentOfUnknown(const Unknown& u, dimen_t i)
{
  name_ = u.name() + "_" + tostring(i);
  space_p = u.space();
  u_p = &u;
  nbOfComponents_=1;
  i_ = i;
  conjugate_ = false;
  dualUnknown_=u.dual_p();  //be careful, dual unknown is the parent dual unknown, may be it should be the component of dual unknown ?
}

//------------------------------------------------------------------------------------
// ComponentsOfUnknown member functions
//------------------------------------------------------------------------------------
ComponentsOfUnknown::ComponentsOfUnknown(const Unknown& u, const std::set<dimen_t>& is)
{
  init(u,is);
}

ComponentsOfUnknown::ComponentsOfUnknown(const Unknown& u, dimen_t i, dimen_t j, dimen_t k, dimen_t l, dimen_t m, dimen_t n)
{
  std::set<dimen_t> is;
  if (i>0) is.insert(i);
  if (j>0) is.insert(j);
  if (k>0) is.insert(k);
  init(u,is);
}

void ComponentsOfUnknown::init(const Unknown& u, const std::set<dimen_t>& is)
{
  is_ = is;
  u_p = &u;
  nbOfComponents_=is_.size();
  if (nbOfComponents_==0)
  {
    where("ComponentsOfUnknown::init");
    error("is_void","components");
  }
  if (nbOfComponents_>=u.nbOfComponents())
  {
    where("ComponentsOfUnknown::init");
    error("unknown_component_too_big",u.name());
  }
  name_ = u.name();
  std::set<dimen_t>::iterator itl=is_.begin();
  for (;itl!=is_.end();itl++)
  {
    if (*itl > u.nbOfComponents())
    {
      where("ComponentsOfUnknown::init");
      error("unknown_indexoutside",u.name(),*itl);
    }
    name_ += "_" + tostring(*itl);
  }
  space_p = u.space();
  conjugate_ = false;
  dualUnknown_=u.dual_p();  //be careful, dual unknown is the parent dual unknown, may be it should be the component of dual unknown ?
}

} // end of namespace xlifepp
