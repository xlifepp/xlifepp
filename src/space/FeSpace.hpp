/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FeSpace.hpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 13 jun 2013

  \brief Definition of xlifepp::FeSpace class inherited from xlifepp::Space class

  describing finite element space based on finite element interpolation description (xlifepp::Interpolation)
  and mainly managing:
    - a list of finite elements: vector<xlifepp::Element>
    - a list of global degrees of freedom: vector<xlifepp::FeDof>
  and possibly:
    - a map xlifepp::GeomElement -> element number (useful to interpolation tools) :map<xlifepp::GeomElement*, xlifepp::number_t>
*/

#ifndef FE_SPACE_HPP
#define FE_SPACE_HPP

#include "config.h"
#include "Space.hpp"
#include "Dof.hpp"
#include "Element.hpp"
#include "finiteElements.h"

namespace xlifepp
{

//------------------------------------------------------------------------------------
/*!
    \class FeSpace
    defines general data of a finite element space.
    FeSpace class is not directly exposed to end users
*/
//------------------------------------------------------------------------------------
class FeSpace : public Space
{
  protected:
    const Interpolation* interpolation_p;    //!< pointer to finite element interpolation description
    bool optimizedNumbering_;                //!< true if dof numbering is bandwith optimized

  public:
    std::vector<Element> elements;           //!< list of finite elements
    std::vector<FeDof> dofs;                 //!< list of global degrees of freedom
    std::set<RefElement *> refElts;          //!< set of pointer to RefElement involved in FeSpace (usually one)

    mutable std::map<GeomElement*, number_t> gelt2elt; //!< map GeomElement -> element number (useful to interpolation tools)
    mutable std::map<number_t, number_t> dofid2rank_;  //!< map dof iD -> rank in dofs vector

    static number_t lastEltIndex;            //!< to manage a unique index for elements

    //constructor stuff
    FeSpace(const GeomDomain& gd, const Interpolation& in, const string_t& na, bool opt=true, bool withLocateData=true);    //!< constructor from GeomDomain and Interpolation(s)
    void buildElements();                    //!< construct the list of elements
    void buildFeDofs();                      //!< construct the list of FeDofs
    void buildgelt2elt() const;              //!< construct the map gelt2elt from elements list
    void builddofid2rank() const;            //!< construct the map dofid2rank from dofs list
    void buildBCRTSpace();                   //!< special construction of BuffaChristiansen-RT space
    void addToBcDof(BCDof*, FeDof&, number_t, std::list<GeoNumPair>&,
                    number_t, const Point&, const Point&, GeomElement*, int rev=1); //!< tool for buildBCRTSpace()
    void addToBcDofBoundary(BCDof*, FeDof&, number_t, std::list<GeoNumPair>&,
                            number_t, const Point&, const Point&, GeomElement*, int rev=1); //!< tool for buildBCRTSpace()

    //accessors
    virtual const FeSpace* feSpace() const {return this;}
    virtual FeSpace* feSpace() {return this;}
    virtual const Space* rootSpace() const                 //! access to root space pointer (const)
    {return static_cast<const Space*>(this);}
    virtual Space* rootSpace()                             //! access to root space pointer
    {return static_cast<Space*>(this);}
    virtual number_t dimSpace()const                       //! return the space dimension
    {return dofs.size();}
    virtual number_t nbDofs()const                         //! number of dofs (a dof may be a vector dof)
    {return dofs.size();}
    virtual number_t dofId(number_t n) const               //! return the DoF id of n-th space DoF (n=1,...)
    {return dofs[n-1].id();}
    virtual std::vector<number_t> dofIds() const;          //!< return the DoF ids on space
    virtual const Dof& dof(number_t n) const               //! return n-th FeDof as Dof (n=1,...)
    {return static_cast<const Dof&>(dofs[n-1]);}
    virtual const FeDof& fedof(number_t n) const           //! return n-th FeDof (n=1,...)
    {return dofs[n-1];}
     virtual const std::vector<FeDof>& feDofs() const      //! return dofs vector
    {return dofs;}
    dimen_t dimElements()const;                            //!< return the dimension of FE elements
    const Interpolation* interpolation() const             //! return pointer to interpolation
    {return interpolation_p;}
    virtual bool include(const Space*) const;              //!< return true if space is included in current space
    virtual ValueType valueType() const;                   //!< return value type of basis function (real or complex)
    virtual StrucType strucType() const;                   //!< return structure type of basis function (scalar or vector)
    virtual bool isSpectral() const;                       //!< true if spectral space
    virtual bool isFE() const;                             //!< true if FE space or FeSubspace
    virtual bool extensionRequired() const;                //!< true if space has no trace space
    virtual std::vector<number_t> dofsOn(const GeomDomain&,
            int_t ord=-1, dimen_t dim=0, ProjectionType =_noProjection) const; //! return the set of dof numbers of dofs "located" on a domain

    //renumbering stuff
    Graph graphOfDofs();                                   //!<create dof connection graph
    std::pair<number_t, number_t> renumberDofs();          //!<renumber dofs with bandwith minimisation

    //stuff for Element
    number_t nbOfElements() const                          //!number of elements for FESpace and FESubspace only
      {return elements.size();}
    const Element* element_p(number_t k) const             //!access to k-th (k>=0) element (pointer) for FESpace and FESubspace only
      {return &(elements[k]);}
    const Element& element(number_t k) const               //!access to k-th (k>=0) element (pointer) for FESpace and FESubspace only
      {return elements[k];}
    const Element* element_p(GeomElement* gelt) const;     //!< access to element associated to a geomelement
    const Element& element(GeomElement* gelt) const;       //!< access to element associated to a geomelement
    number_t numElement(GeomElement* gelt) const;          //!< access to element number associated to a geomelement
    const std::vector<number_t>& elementDofs(number_t k) const //! access to dofs ranks (local numbering) of k-th (>=0) element for FESpace and FESubspace only
      {return elements[k].dofNumbers;}
    std::vector<number_t> elementParentDofs(number_t k) const //!access to dofs ranks (parent numbering, same as local) of k-th (>=0) element for FESpace and FESubspace only
      {return elements[k].dofNumbers; }
    const std::set<RefElement*>& refElements() const       //! access to set of pointer to RefElement (refElts) involved in FESpace
    {return refElts;}
    number_t maxDegree() const;                            //!< max degree of polynomials involved by shape functions


     //stuff for Dofs
     const std::map<number_t, number_t>&  dofid2rank() const  //! return the map dof to rank, built on fly
      {builddofid2rank(); return dofid2rank_;}
    void shiftDofs(number_t n)                             //! shift dofs numbering from n (special utility for SpSpace)
      {error("not_handled","shiftDofs()");}

    //interpolation functions
    template <typename T, typename K>
    T& interpolate(const Vector<K>&, const Point&, T&, DiffOpType =_id, bool useNearest=false) const; //!< compute interpolation value at P
    template <typename T, typename K>
    T& interpolate(const Vector<K>&, const Point&, const Element*,  T&, DiffOpType =_id, bool useNearest=false) const;
    Dof& locateDof(const Point &) const;                                       //!< locate Lagrange dof nearest a given point
    const Element* locateElement(const Point& P, bool useNearest=false,
                                 bool errorOnOutDom=true, real_t tol=theLocateToleranceFactor) const; //!< locate element of the Fespace containing P

    //print and output facilities
    virtual void print(std::ostream&)const;                //!<print utility
};

//------------------------------------------------------------------------------------
//  templated member functions
//------------------------------------------------------------------------------------
// compute FE interpolation value at P
template <typename T, typename K>
T& FeSpace::interpolate(const Vector<K>& v, const Point& P, T& vint, DiffOpType d, bool useNearest) const
{
  vint=T();
  const Element* elt=locateElement(P,useNearest,true);
  if(elt==nullptr) return vint;
  return elt->interpolate(v,P,elt->dofNumbers,vint,d);
}

template <typename T, typename K>
T& FeSpace::interpolate(const Vector<K>& v, const Point& P, const Element* eltp,  T& vint, DiffOpType d, bool useNearest) const
{
  vint=T();
  const Element* elt=eltp;
  if(elt==nullptr) elt=locateElement(P,useNearest,true);
  if(elt==nullptr) return vint;
  return elt->interpolate(v,P,elt->dofNumbers,vint,d);
}

} // end of namespace xlifepp

#endif // FE_SPACE_HPP

