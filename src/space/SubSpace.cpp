/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SubSpace.cpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 31 jul 2012

  \brief Implementation of xlifepp::SubSpace class and functionnalities
*/

#include "SubSpace.hpp"

namespace xlifepp
{
//constructor from geom domain, parent space and name
//this does not construct FeSubSpace (subspace of FeSpace on a meshDomain)
//concern only composite domains
SubSpace::SubSpace(const GeomDomain& dom, Space& ps, const string_t& na)
{
  trace_p->push("SubSpace::Subspace");
  string_t nam = na;
  if(nam == "") { nam = ps.name() + " on " + dom.name(); }
  spaceInfo_p = new SpaceInfo(nam, &dom, ps.conformingSpace(), ps.dimFun(), _subSpace);
  parent_p = &ps;
  space_p = this;
  //create subspace dofs numbering
  switch(ps.typeOfSpace())
    {
      case _feSpace:
        dofsOfFeSubspace();
        break;
      case _spSpace:
        dofsOfSpSubspace();
        break;
      case _subSpace:
        dofsOfSubSubspace();
        break;
      default:
        error("spacetype_not_handled",words("space",ps.typeOfSpace()));
    }
  global=false;
  trace_p->pop();
}

//constructor from geom domain, dof ids and parent space and name
// dof numbers (a list of dof ids) must be a subset of space dof numbers
//
SubSpace::SubSpace(const GeomDomain& dom, const std::vector<number_t>& dofids, Space& sp, const string_t& na)
{
  trace_p->push("SubSpace::Subspace");
  string_t nam = na;
  if(nam == "") { nam = sp.name() + " on " + dom.name(); }
  spaceInfo_p = new SpaceInfo(nam, &dom, sp.conformingSpace(), sp.dimFun(), _subSpace);
  parent_p = &sp;
  space_p = this;
  std::map<number_t, number_t> spdofids;

  for(number_t k = 1; k <= sp.nbDofs(); k++) spdofids[sp.dofId(k)] = k;
  dofNumbers_.resize(dofids.size());
  std::vector<number_t>::const_iterator itd = dofids.begin();
  std::vector<number_t>::iterator it = dofNumbers_.begin();
  for(; itd != dofids.end(); itd++, it++)
    {
      std::map<number_t, number_t>::iterator itm = spdofids.find(*itd);
      if(itm == spdofids.end()) error("dof_out_of_set");
      *it = itm->second;
    }
  global=false;
  trace_p->pop();
}

SubSpace::SubSpace(const std::vector<number_t>& dofids, Space& sp, const string_t& na)
{
  trace_p->push("SubSpace::Subspace");
  string_t nam = na;
  if(nam == "") { nam = sp.name() + " on ? "; }
  spaceInfo_p = new SpaceInfo(nam, 0, sp.conformingSpace(), sp.dimFun(), _subSpace);
  parent_p = &sp;
  space_p = this;
  std::map<number_t, number_t> spdofids;

  for(number_t k = 1; k <= sp.nbDofs(); k++) spdofids[sp.dofId(k)] = k;
  dofNumbers_.resize(dofids.size());
  std::vector<number_t>::const_iterator itd = dofids.begin();
  std::vector<number_t>::iterator it = dofNumbers_.begin();
  for(; itd != dofids.end(); itd++, it++)
    {
      std::map<number_t, number_t>::iterator itm = spdofids.find(*itd);
      if(itm == spdofids.end()) error("dof_out_of_set");
      *it = itm->second;
    }
  global=false;
  trace_p->pop();
}

//destructor, delete subspaces if domain is a composite domain
SubSpace::~SubSpace()
{
//  if(domain()->domType() == _compositeDomain)
//    {
//      const std::vector<const GeomDomain*>& doms = domain()->compositeDomain()->domains();
//      std::vector<const GeomDomain*>::const_iterator it;
//      for(it = doms.begin(); it != doms.end(); it++)
//        { delete findSubSpace(*it, space_p); }
//    }
}

//build same Dofs numbering as parent Dofs numbering (trivial numbering)
void SubSpace::dofsOfSpSubspace()
{
  dofNumbers_.resize(parent_p->dimSpace());
  for(number_t n = 0; n < parent_p->dimSpace(); n++) { dofNumbers_[n] = n; }
}


/*! access to dofNumbers list in root space numbering
 remind that dofNumbers vector in SubSpace is related to the numbering of parent
 which can be a subspace and not the root space
*/
std::vector<number_t> SubSpace::dofRootNumbers() const
{
  if(parent_p->typeOfSpace() != _subSpace) return dofNumbers_;
  trace_p->push("SubSpace::dofRootNumbers");
  std::vector<number_t> dofr(dofNumbers_.size());
  std::vector<number_t>::const_iterator itd;
  std::vector<number_t>::iterator itr;
  for(itd = dofNumbers_.begin(); itd != dofNumbers_.end(); itd++, itr++)
    *itr = dofRootNumber(*itd);
  trace_p->pop();
  return dofr;
}

// access to the dof number in root space dof numbering
number_t SubSpace::dofRootNumber(number_t k) const
{
  //if(parent_p->typeOfSpace() != _subSpace) return k;
  if(parent_p->typeOfSpace() != _subSpace) return dofNumbers_[k-1];
  return parent_p->subSpace()->dofRootNumber(k);
}

// return the DoF ids on space
std::vector<number_t> SubSpace::dofIds() const
{
  std::vector<number_t> ids(dofNumbers_.size());
  std::vector<number_t>::iterator it = ids.begin();
  number_t k = 1;
  for(; it != ids.end(); it++, k++) *it = dofId(k);
  return ids;
}

//! construct the map dofid2rank from dofs list, ranks start from 1
void SubSpace::builddofid2rank() const
{
    if(dofid2rank_.size()>0) return; //already built
    for(number_t k=1;k<=dofNumbers_.size();++k)
        dofid2rank_.insert(std::pair<number_t, number_t>(dofId(k),k));
}

/*!--------------------------------------------------------------------------------
  build dofs numbering of subspace when parent is a Fe space
  if domain is a composite domain (union or intersection of domains)
      - subspaces of each domain are created if they do not exist
      - dofNumbers numbering relative to parent space is computed
  if domain is a meshdomain, dofNumbers is computed by FeSubSpace function
--------------------------------------------------------------------------------*/
void SubSpace::dofsOfFeSubspace()
{
  if(domain()->domType() != _compositeDomain) { return; }  //dofs numbering is created outside

  trace_p->push("SubSpace::dofsOfFeSubspace");
  //travel domains and create subspaces (if do not exist) and dofNumbers
  const std::vector<const GeomDomain*>& doms = domain()->compositeDomain()->domains();
  //create subspace of parent space for each domain
  std::vector<Space*> subspaces;
  parent_p->createSubSpaces(doms, subspaces);
  //travel domains creating dofNumbers
  std::vector<const GeomDomain*>::const_iterator it;
  number_t dimsp = parent_p->dimSpace();   //number of dofs of parent space
  std::vector<number_t> mark(dimsp+1, 0); //to mark the encountered dofs
  for(it = doms.begin(); it != doms.end(); it++)
    {
      Space* sp = Space::findSubSpace(*it, parent_p);
      if (sp==nullptr) error("space_not_found");
      std::vector<number_t>::iterator itd=sp->subSpace()->dofNumbers_.begin(), itde=sp->subSpace()->dofNumbers_.end();
      for(; itd!=itde; ++itd) mark[*itd] += 1;
      //for(number_t n = 0; n < sp->dimSpace(); n++) { mark[sp->subSpace()->dofNumbers_[n]] += 1;}
    }
  number_t k = 0;
  dofNumbers_.resize(dimsp, 0);
  if(domain()->isIntersection())
    {
      number_t l = doms.size();
      for(number_t n = 0; n <= dimsp; n++)
        if(mark[n] == l)         //keep dofs marked l times
          {
            dofNumbers_[k] = n;
            k++;
          }
    }
  else if(domain()->isUnion())
    {
      for(number_t n = 0; n <= dimsp; n++)
        if(mark[n] >0)      //keep dofs marked at least once
          {
            dofNumbers_[k] = n;
            k++;

          }
    }
  //resize dofnumbers_
  dofNumbers_.resize(k);
#if __cplusplus >= 201103L   //c++11 enabled
  dofNumbers_.shrink_to_fit();
#else
  std::vector<number_t>(dofNumbers_).swap(dofNumbers_);
#endif
  //Note: dofNumbers_.size()=dimsp means that the subspace is actually the whole space, it is a little bit silly !
  trace_p->pop();
}

/*!--------------------------------------------------------------------------------
  build dofs numbering of subspace when parent is a Subspace
  two cases are possible:
    - domain is a meshdomain and domain parent is a composite domain (e.g  D1 and D1+D2)
    - domain and parent domains are composite domains (e. g D1+D2 and D1+D2+D3)
  we use a general algorithm based on root dof numbers comparison
--------------------------------------------------------------------------------*/
void SubSpace::dofsOfSubSubspace()
{
  trace_p->push("SubSpace::dofsOfSubSubspace");

  std::vector<number_t> pardofs=parent_p->subSpace()->dofRootNumbers();   //retry root dof numbers of parent space
  std::map<number_t,number_t> sortdofs;
  std::vector<number_t>::iterator it=pardofs.begin(), itd;
  number_t n=0;
  for(; it!=pardofs.end(); it++, n++) sortdofs[*it]=n;
  Space* rsp=parent_p->rootSpace();
  Space* sp=findSubSpace(domain(),rsp);
  if(sp==nullptr) sp=createSubSpace(*domain(),*rsp,rsp->name()+"_"+domain()->name());
  std::vector<number_t> dofs=sp->subSpace()->dofRootNumbers();   //retry root dof numbers of parent space
  dofNumbers_.resize(dofs.size());
  itd=dofNumbers_.begin();
  for(it=dofs.begin(); it!=dofs.end(); it++, itd++) *itd=sortdofs[*it];
  trace_p->pop();
}

ValueType SubSpace::valueType() const     // return value type of basis function (real or complex)
{
  return parent_p->valueType();
}

StrucType SubSpace::strucType() const    // return structure type of basis function (scalar or vector)
{
  return parent_p->strucType();
}

//check type of Space
bool SubSpace::isSpectral() const
{
  return parent_p->isSpectral();
}

bool SubSpace::isFE() const
{
  return parent_p->isFE();
}

// return true if (sub)space is included in current space
// true cases:  same SubSpace's, sp parent is current subspace
//               or set of sp dof numbers included in set of current subspace dof numbers
// Note: it should be faster to use GeomDomains but it is less robust
bool SubSpace::include(const Space* sp) const
{
  if(sp->typeOfSpace() != _subSpace) return false; // a subspace can not include a space
  if(sp->rootSpace() != rootSpace()) return false; // not the same root space
  const SubSpace* subsp = sp->subSpace();
  if(subsp == this) return true;                 // same subspaces
  if(subsp->parent() == static_cast<const Space*>(this)) return true; // sp parent is current subspace
  std::vector<number_t> currentDofs = dofRootNumbers(); // current dofNumbers list in root space numbering
  std::vector<number_t> spDofs = subsp->dofRootNumbers(); // sp dofNumbers list in root space numbering
  std::sort(currentDofs.begin(), currentDofs.end());  // sort dof numbers
  std::vector<number_t>::iterator itd = spDofs.begin();
  while(itd != spDofs.end())
    {
      if(!std::binary_search(currentDofs.begin(), currentDofs.end(), *itd)) return false; //one sp dof does not belong to set of current dofs
      itd++;
    }
  return true;
}

//print utility
void SubSpace::print(std::ostream& out) const
{
  if(theVerboseLevel > 0)
    {
      printSpaceInfo(out);        //print spaceinfo
      out << " parent space: " << parent_p->name();
    }
  if(theVerboseLevel > 2)  //print numbering DoF
    {
      out << "\nDof Numbering:";
      for(number_t n = 0; n < dofNumbers_.size(); n++)
        { out << "\n  local number: " << n + 1 << ", parent number: " << dofNumbers_[n] << ", dof id:" << dofId(n + 1); }
    }
}

std::ostream& operator <<(std::ostream& out, const SubSpace& sp)
{sp.print(out); return out;}

} // end of namespace xlifepp
