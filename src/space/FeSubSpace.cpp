/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FeSubSpace.cpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 31 jul 2012

  \brief Implementation of xlifepp::FeSubSpace class members and functionnalities
*/

#include "FeSubSpace.hpp"
#include "FeSpace.hpp"

namespace xlifepp
{

//construct subspace of a FeSpace only when GeomDomain is a MeshDomain
//see SubSpace constructor for composite domains
FeSubSpace::FeSubSpace(const GeomDomain& gd, Space& sp, const string_t& na)
{
  trace_p->push("FeSubSpace::FeSubspace");
  if(gd.domType() != _meshDomain) { trace_p->pop(); error("domain_notmesh", gd.name(), words("domain type", gd.domType())); }
  if(sp.domain()==nullptr) { trace_p->pop(); error("null_pointer","domain"); }
  if(gd.mesh()!= sp.domain()->mesh()) { trace_p->pop(); error("domain_space_mismatch_mesh"); }

  string_t nam = na;
  if(nam == "") { nam = sp.name() + " on " + gd.name(); }
  spaceInfo_p = new SpaceInfo(nam, &gd, sp.conformingSpace(), sp.dimFun(), _subSpace);
  parent_p = &sp;
  space_p = this;
  global=false;

  //case of domain and parent domain of the same dimension
  if(gd.dim() == sp.dimDomain())  //extract only elements of parent's domain
    {
      //create GeomDomain elements index along their pointers in a map
      std::map<GeomElement*, number_t> index;
      number_t l = 0;
      const std::vector<GeomElement*>& geoElts = gd.meshDomain()->geomElements;
      std::vector<GeomElement*>::const_iterator itge;
      for(itge = geoElts.begin(); itge != geoElts.end(); itge++, l++)
        { index.insert(std::pair<GeomElement*, number_t> (*itge, l));}
      //create list of subspace elements traveling the list of elements of parent space
      elements.resize(l);
      std::vector<Element>::iterator ite;
      std::vector<Element>& spElts = sp.feSpace()->elements;
      l = 0;
      for(ite = spElts.begin(); ite != spElts.end(); ite++)
        if(index.count(ite->geomElt_p) > 0)
          {elements[l] = & (*ite); l++; }
      index.clear();   //free memory
      if(l<elements.size())
      {
         error("free_error"," some elements of domain "+gd.name()+" have not be found in "+sp.name()+" elements list");
      }
      //create dofRanks and dofNumbers from map of global dof numbers to local dof numbers
      // NOTE: numbering order is preserved in order to 'preserve' bandwith of matrices
      std::map<number_t, number_t> numloc; //global dof numbers of elements in domain
      std::vector<Element*>::iterator itpe;
      std::vector<number_t>::iterator itd;
      for(itpe = elements.begin(); itpe != elements.end(); itpe++)
        for(itd = (*itpe)->dofNumbers.begin(); itd != (*itpe)->dofNumbers.end(); itd++)
          if(numloc.count(*itd) == 0) numloc[*itd] = 0;
      //prevent abnormal issues
      if(numloc.size()==0) error("space_domain_incompatible_elements", gd.name(), sp.domain()->name());
      //update dof numbers (global dof numbers relative to parent space dofs)
      std::map<number_t, number_t>::iterator itnl;
      dofNumbers_.resize(numloc.size());
      std::vector<number_t>::iterator itdn = dofNumbers_.begin();
      number_t i = 1;
      for(itnl = numloc.begin(); itnl != numloc.end(); itnl++, itdn++, i++)
        {
          number_t n = itnl->first;
          *itdn = n;             //update global dof number of local dof number
          itnl->second = i;      //update local dof number of global dof number
        }
      //update dof ranks (dof numbers of elements relative to parent space dofs)
      dofRanks.resize(elements.size());
      std::vector<std::vector<number_t> >::iterator itdr = dofRanks.begin();
      for(itpe = elements.begin(); itpe != elements.end(); itpe++, itdr++)
        {
          Element* elt = *itpe;
          itdr->resize(elt->dofNumbers.size());
          std::vector<number_t>::iterator itl = itdr->begin();
          for(itd = elt->dofNumbers.begin(); itd != elt->dofNumbers.end(); itd++, itl++)
            *itl = numloc[*itd];
          //update refelts
          if(refElts.find(elt->refElt_p)==refElts.end()) refElts.insert(elt->refElt_p);
        }
      trace_p->pop();
      return;
    }

  //case of a side domain
  if(gd.dim() == sp.dimDomain() - 1)
    {
//      if(gd.dim()==0)   // special case of 0d domain, only one element SHOULD BE DELETED SOON
//        {
//          elements.resize(1);
//          dofNumbers_.resize(1);
//          dofRanks.resize(1);
//          GeomElement* geoElt=gd.meshDomain()->geomElements[0];
//          GeoNumPair gp = geoElt->parentSide(0);  // ->(parent GeomElement, side number)
//          const Element* elt = sp.element_p(gp.first);  // parent Element
//          RefElement* refelt = elt->refElt_p->refElement(1); // ref elt of side 1 of parent (= point ref elt)
//          elements[0] = new Element(sp.feSpace(), ++FeSpace::lastEltIndex, refelt, geoElt, this); // create side element;
//          number_t n = elt->refElt_p->sideDofNumbers_[gp.second-1][0];
//          dofNumbers_[0] = elt->dofNumbers[n-1];
//          dofRanks[0] = std::vector<number_t>(1,1);
//          elements[0]->parents_.push_back(const_cast<Element*>(elt));
//          elements[0]->dofNumbers[0] = dofNumbers_[0];
//          if(refElts.find(refelt) == refElts.end()) { refElts.insert(refelt); }
//          trace_p->pop();
//          return;
//        }
      //general case
      //create the set of all the geom elements of parent space (sp)
      std::set<GeomElement*> parelts;
      std::vector<Element>::iterator ite;
      std::vector<Element>& spElts = sp.feSpace()->elements;
      for(ite = spElts.begin(); ite != spElts.end(); ite++) parelts.insert(ite->geomElt_p);

      //create index of parent of geom elements of domain gd: parent geom elt* -> (side geom elt*, side num)
      std::multimap<GeomElement*, std::pair<GeomElement*,number_t> > index;
      const std::vector<GeomElement*>& geoElts = gd.meshDomain()->geomElements;
      std::vector<GeomElement*>::const_iterator itge;
      for(itge = geoElts.begin(); itge != geoElts.end(); itge++)
        {
          (*itge)->flag=0;
          std::vector<GeoNumPair>& parsides = (*itge)->parentSides(); //parents and side numbers
          std::vector<GeoNumPair>::iterator itvg=parsides.begin();
          for(; itvg!=parsides.end(); itvg++)
            {
              GeomElement* gep = itvg->first;  //parent of geom side element
              if(parelts.find(gep)!=parelts.end())
                index.insert(std::make_pair(gep, std::make_pair(*itge,itvg->second)));  //does not avoid multiple (interface, corner) ## to be improved
            }
        }
      //create side elements, traveling elements of fespace
      number_t k=0, l=0;
      elements.resize(geoElts.size());
      std::map<number_t, number_t> numloc; //global dof numbers of elements in domain
      typedef std::multimap<GeomElement*, std::pair<GeomElement*,number_t> >::iterator it_mmg;
      for(ite = spElts.begin(); ite != spElts.end(); ite++)  //loop on finite elements of FeSpace
        {
          std::pair<it_mmg, it_mmg> itis = index.equal_range(ite->geomElt_p);
          for(it_mmg iti=itis.first; iti!=itis.second; ++iti) //loop on all side geom elements related to the current finite element
            {
              GeomElement* geoElt = iti->second.first;        //geometric side element
              // geoElt->flag==0 means that the geom side element has never been viewed, so a new side element is created
              // else no side element created  (management of multiple)
              if(geoElt->flag==0)
                {
                  geoElt->flag=1;
                  //find or create the side reference element
                  if(ite->refElt_p->sideRefElems().size()==0)  //no side element, error
                    error("no_side_element", ite->refElt_p->name());
                  geoElt->buildSideMeshElement();
                  number_t sidenum = iti->second.second;       //its side number in parent geom element
                  RefElement* refelt = ite->refElt_p->refElement(sidenum);
                  if(refElts.find(refelt)==refElts.end()) refElts.insert(refelt); //update refelts

                  // find if the side of parent geom element is the geom side element with no permuted vertex numbers
                  std::vector<number_t> sn=geoElt->meshElement()->verticesNumbers(),               //vertex numbers of geom side element
                                        vn=ite->geomElt_p->meshElement()->verticesNumbers(sidenum);//vertex numbers of side of parent element
                  std::vector<number_t>::iterator itvn=vn.begin(), itsn=sn.begin();
                  while(itvn!=vn.end() && *itvn==*itsn) {++itvn;++itsn;}       //loop on vertices of side of parent geom element

                  // if permuted vertex numbers, a new twin geom side element is created and related to the original one by the twin members
                  if(itvn!=vn.end())
                    {
                      GeomElement* ng= new GeomElement(ite->geomElt_p, sidenum, geoElt->number());  //geomElement indexed with the same original geoElt index
                      ng->twin_p=geoElt; geoElt->twin_p=ng;
                      geoElt = ng;  // take this new geom side element as current geom side element
                    }

                  //create the FE side element
                  Element* sidelt = new Element(sp.feSpace(), ++FeSpace::lastEltIndex, refelt, geoElt, this);    //create side element;
                  sidelt->parents_.push_back(&(*ite)); //update parent element of side element

                  //update global numbering
                  const std::vector<number_t> locdofnum = ite->refElt_p->sideDofNumbers_[sidenum - 1];
                  number_t nbdofs = locdofnum.size();
                  for(number_t i = 0; i < nbdofs; i++)
                    {
                      number_t nd = ite->dofNumbers[locdofnum[i] - 1];
                      sidelt->dofNumbers[i] = nd;
                      if(numloc.count(nd) == 0) {numloc[nd] = k; k++;}  //index dof of element
                    }

                  //store the FE side element in list of FE elements of FeSubspace
                  elements[l] = sidelt;
                  l++;
                } //end test if(geoElt->flag==0)
            } //end loop on all side geom elements
        } //end loop on finite elements of FeSpace

      if(l!=geoElts.size()) //some elements have not be found or too many elements
        {
          string_t msg="FeSubspace constructor fails because some side elements of domain "
                       + gd.name()+" have not been found in space "+sp.name()+"\n";
          msg+= "try to update side element of side domain using "+gd.name()+".updateParentOfSideElements()";
          error("free_error",msg);
        }

      //update dofNumbers and dofRanks
      dofNumbers_.resize(numloc.size());
      std::map<number_t, number_t>::iterator itnl;
      for(itnl = numloc.begin(); itnl != numloc.end(); itnl++)
        dofNumbers_[itnl->second] = itnl->first;
      dofRanks.resize(elements.size());
      std::vector<Element*>::iterator itpe;
      std::vector<std::vector<number_t> >::iterator itdr = dofRanks.begin();
      for(itpe = elements.begin(); itpe != elements.end(); itpe++, itdr++)
        {
          Element* elt = *itpe;
          itdr->resize(elt->dofNumbers.size());
          std::vector<number_t>::iterator itd, itl = itdr->begin();
          for(itd = elt->dofNumbers.begin(); itd != elt->dofNumbers.end(); itd++, itl++)
            *itl = numloc[*itd] + 1;
        }
      trace_p->pop();
      return;
    }

//case of a side of side domain (edge in 3D)
  if(gd.dim() == sp.dimDomain() - 2)
    {
      error("not_yet_implemented", "FeSubSpace for side of side domain");
    }
  error("space_domain_incompatible_dims", gd.name(), sp.name(), gd.dim(), sp.dimDomain());
}


//construct subspace of a SubSpace (FE) only when GeomDomain is a MeshDomain
//see SubSpace constructor for composite domains
// note: isFE is a fake argument to distinguish constructor
FeSubSpace::FeSubSpace(const GeomDomain& gd, Space& sp, bool isFE, const string_t& na)
{
  trace_p->push("FeSubSpace::FeSubspace");
  if(gd.domType() != _meshDomain) { trace_p->pop(); error("domain_notmesh", gd.name(), words("domain type",gd.domType())); }
  if(sp.domain()==nullptr) { trace_p->pop(); error("null_pointer","domain"); }
  if(gd.mesh()!= sp.domain()->mesh()) { trace_p->pop(); error("domain_space_mismatch_mesh"); }

  string_t nam = na;
  if(nam == "") { nam = sp.name() + " on " + gd.name(); }
  spaceInfo_p = new SpaceInfo(nam, &gd, sp.conformingSpace(), sp.dimFun(), _subSpace);
  parent_p = &sp;
  space_p = this;
  global=false;

  //use rootspace FeSubSpace to build elements, dofranks and dofnumbers, temporary
  FeSubSpace fesub(gd,*sp.rootSpace());
  //copy modified elements
  elements.resize(fesub.elements.size());
  std::vector<Element*>::iterator ite=fesub.elements.begin(), itf=elements.begin();
  for(; ite!=fesub.elements.end(); ite++, itf++)
    {
      *itf= *ite;
      (*itf)->feSubSpace_p=this;
      //update refelts
      RefElement* refelt = (*itf)->refElt_p;
      if(refElts.find(refelt)==refElts.end()) refElts.insert(refelt);
    }
  fesub.elements.clear();   //to avoid auto destruction of Element (see Element destructor)
  //create dof numbers
  std::vector<number_t> pardofs=sp.subSpace()->dofRootNumbers();   //retry root dof numbers of parent subspace (sp)
  std::map<number_t,number_t> sortdofs;
  std::vector<number_t>::iterator it=pardofs.begin(), itd;
  number_t n=1;
  for(; it!=pardofs.end(); it++, n++) sortdofs[*it]=n;
  std::vector<number_t> dofs=fesub.subSpace()->dofRootNumbers();   //retry root dof numbers of subspace (fesub)
  dofNumbers_.resize(dofs.size());
  itd=dofNumbers_.begin();
  n=1;
  for(it=dofs.begin(); it!=dofs.end(); it++, itd++, n++) {*itd=sortdofs[*it]; sortdofs[*it]=n;}
  //copy and update dofranks
  dofRanks=fesub.dofRanks;
  std::vector<std::vector<number_t> >::iterator itr=dofRanks.begin();
  for(; itr!=dofRanks.end(); itr++)
    {
      std::vector<number_t>::iterator itn=itr->begin();
      for(; itn!=itr->end(); itn++) *itn=sortdofs[fesub.dofRootNumber(*itn)];
    }
  trace_p->pop();
}

//destructor: destroy only elements that are side elements
FeSubSpace::~FeSubSpace()
{
  //thePrintStream<<"FeSubSpace::~FeSubSpace() "<<name()<<eol;
  std::vector<Element*>::iterator itel;
  for(itel = elements.begin(); itel != elements.end(); itel++)
    if((*itel)->parents_.size() != 0) delete *itel; //it is a side element
}

//------------------------------------------------------------------------------------------------------
/*! buildgelt2elt: construct the map gelt2elt from elements list
                    and the map eltRanks
    thread safe because
       if not built, it is build only by the thread 0
       other threads wait for the construction
    Note: it is better to build it outside a parallel region !
*/
//------------------------------------------------------------------------------------------------------
void FeSubSpace::buildgelt2elt() const
{
  number_t s=gelt2elt.size();
  if(s!=0) return;        //already built
  #pragma omp critical
  {
    std::vector<Element*>::const_iterator ite=elements.begin();
    number_t n=0;
    for(; ite!=elements.end(); ite++,n++)
    {
        gelt2elt[(*ite)->geomElt_p]=n;
        eltRanks[*ite]=n;
    }
  }
}

//! access to element number associated to a geomelement
number_t FeSubSpace::numElement(GeomElement* gelt) const
{
  if(gelt2elt.size()==0) buildgelt2elt();
  std::map<GeomElement*, number_t>::const_iterator itm=gelt2elt.find(gelt);
  if(itm!=gelt2elt.end()) return itm->second;
  //use geomelement number to locate element (slow)
  number_t n=gelt->number(), k=0;
  std::vector<Element*>::const_iterator ite=elements.begin();      //!< list of finite elements (or subelements)
  for(; ite!=elements.end(); ++ite, ++k)
    if((*ite)->geomElt_p->number()==n) return k;
  return theNumberMax;
}

/*! return element linked to a given geom element
    used gelt2elt map; has to be already built
*/
const Element* FeSubSpace::element_p(GeomElement* gelt) const
{
  if(gelt2elt.size()==0) buildgelt2elt();
  std::map<GeomElement*, number_t>::const_iterator itm=gelt2elt.find(gelt);
  if(itm!=gelt2elt.end()) return elements[itm->second];
  //use geomelement number to locate element (slow)
  number_t n=gelt->number(), k=0;
  std::vector<Element*>::const_iterator ite=elements.begin();      //!< list of finite elements (or subelements)
  for(; ite!=elements.end(); ++ite, ++k)
    if((*ite)->geomElt_p->number()==n) return (*ite);
  return nullptr;
}

const Element& FeSubSpace::element(GeomElement* gelt) const
{
  const Element* eltp=element_p(gelt);
  if(eltp==nullptr)
    {
      where("FeSubSpace::element_p(GeomElement*)");
      error("geoelt_not_found");
    }
  return *eltp;
}

const Element* FeSubSpace::locateElement(const Point& P, bool useNearest, bool errorOnOutDom, real_t tol) const
{
  const MeshDomain* dom=domain()->meshDomain();
  if (dom==nullptr || dom->numberOfElements() == 0)
  {
    where("FeSubSpace::locateElement(...)");
    error("domain_notmesh",domain()->name(), words("domain type", domain()->domType()));
  }
  real_t di=0.;
  GeomElement* gelt=nullptr;
  if(!useNearest)
  {
      gelt=dom->locate(P, true); // try with locate in silent mode
      if(gelt==nullptr)
      {
       if(errorOnOutDom) locateError(false,"FeSubSpace::locateElement",*dom,P,di);
       else return nullptr;
      }
  }
  //use nearest
  Point q=P;
  gelt = dom->nearest(q,di);
  if (gelt==nullptr)
  {
    if(errorOnOutDom) locateError(false,"FeSubSpace::locateElement",*dom,P,di);
    return nullptr;
  }
  if (di > gelt->size()*tol)
  {
    if(errorOnOutDom) locateError(true,"FeSubSpace::locateElement",*dom,P,di);
    return nullptr;
  }
  if (gelt2elt.size()==0) buildgelt2elt();  //build once
  return element_p(gelt);
}

//access to dofs ranks (local numbering) of k-th (>=0) element for FESpace and FESubspace only
const std::vector<number_t>& FeSubSpace::elementDofs(number_t k) const
{
  return dofRanks[k]; //case of a largest subspace
}

//access to dofs ranks in parent numbering of k-th (>=0) element for FESpace and FESubspace only
std::vector<number_t> FeSubSpace::elementParentDofs(number_t k) const
{
  std::vector<number_t> dofnum(dofRanks[k].size(), 0);
  std::vector<number_t>::const_iterator itdr;
  std::vector<number_t>::iterator itdn = dofnum.begin();
  for(itdr = dofRanks[k].begin(); itdr != dofRanks[k].end(); itdr++, itdn++)
    *itdn = dofNumbers_[*itdr - 1];
  return dofnum;
}

//access to FeDofs vector, if not exist built on fly
const std::vector<FeDof>& FeSubSpace::feDofs() const
{
  if(fedofs.size()==0)
    {
      const std::vector<FeDof>& parentFeDofs=parent_p->feDofs();
      std::vector<number_t>::const_iterator itn = dofNumbers_.begin();
      fedofs.resize(dofNumbers_.size());
      std::vector<FeDof>::iterator itd=fedofs.begin();
      for(; itn!=dofNumbers_.end(); ++itn, ++itd) *itd=parentFeDofs[*itn-1];
    }
  return fedofs;
}

//print utilities
void FeSubSpace::print(std::ostream& out) const
{
  if(theVerboseLevel > 0)
    {
      SubSpace::print(out);
    }
  if(theVerboseLevel > 2)  //print numbering DoF (first and last theVerboseLevel number of element)
    {
      out << "\nElement list:";
      number_t nb = std::min(elements.size(), (size_t)theVerboseLevel);
      for(number_t i = 0; i < nb; i++)
        {
          out << eol;
          elements[i]->print(out);
          out << "\ndof ranks:";
          for(number_t j = 0; j < dofRanks[i].size(); j++)  out << " " << dofRanks[i][j];
        }
      nb = std::max(nb, elements.size() - nb - 1);
      for(number_t i = nb; i < elements.size(); i++)
        {
          out << eol;
          elements[i]->print(out);
          out << "\ndof ranks:";
          for(number_t j = 0; j < dofRanks[i].size(); j++)  out << " " << dofRanks[i][j];
        }
    }
}

std::ostream& operator <<(std::ostream& out, const FeSubSpace& sp)
{sp.print(out); return out;}

} // end of namespace xlifepp
