/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SpectralBasis.hpp
  \author E. Lunéville
  \since 20 fev 2012
  \date 12 jun 2012

  \brief Definition of the xlifepp::SpectralBasis class

  It deals with functions globally defined over a geometric domain. A xlifepp::SpectralBasis is the finite set of
  functions spanning a vectorial space of finite dimension (see class xlifepp::SpSpace). Such functions are generally
  involved in spectral approximation (for instance DtN map, Mortar element, ...)
  A xlifepp::SpectralBasis may be either defined with an analytic expression (xlifepp::SpectralBasisFun class)
  or by a set of interpolate functions (xlifepp::SpectralBasisInt class) :
  \verbatim
                  child
  SpectralBasis --------| SpectralBasisFun (defined from xlifepp::Function class)
                        | SpectralBasisInt (defined from set of xlifepp::TermVector)
  \endverbatim

  xlifepp::SpectralBasis, xlifepp::SpectralBasisFun and xlifepp::SpectralBasisInt are normally not instanciated by end user
  They are instanciated by xlifepp::SpSpace class through a constructor of xlifepp::Space class (see Space.hpp)
*/

#ifndef SPECTRAL_BASIS_HPP
#define SPECTRAL_BASIS_HPP

#include "config.h"
#include "utils.h"
#include "geometry.h"

namespace xlifepp
{

/*!
   \class SpectralBasis
   defines an abstract class of global basis functions (not finite element)
 */
class SpectralBasis
{
  protected:
    number_t numberOfFun_;      //!< number of function in the basis
    number_t dimFun_;           //!< dimension of the spectral functions
    const GeomDomain* domain_;  //!< geometric domain support
    ValueType returnedType_;    //!< type of returned value (one among _real, _complex)
    StrucType returnedStruct_;  //!< structure of returned value (one among _scalar, _vector)
    FuncFormType funcFormType_; //!< type of basis functions (_analytical,_interpolated)

    SpectralBasis()             //! default constructor
      : numberOfFun_(0), dimFun_(1), domain_(nullptr), returnedType_(_real), returnedStruct_(_scalar) {}

    SpectralBasis(number_t n, number_t d, const GeomDomain& g, ValueType r = _real, StrucType s = _scalar) //! explicit constructor
      : numberOfFun_(n), dimFun_(d), domain_(&g), returnedType_(r), returnedStruct_(s) {}

  public:
    virtual ~SpectralBasis() {}                               //!< virtual destructor
    number_t numberOfFun() const {return numberOfFun_;}       //!< returns number of basis functions
    dimen_t dimFun() const {return dimFun_;}                  //!< returns dimension of basis functions
    number_t dimSpace() const {return numberOfFun_;}          //!< returns space dimension (=number of basis functions)
    const GeomDomain* domain() const {return domain_;}        //!< returns related domain
    FuncFormType funcFormType() const {return funcFormType_;} //!< returns type of basis functions
    ValueType valueType() const {return returnedType_;}       //!< returns type of returned value
    StrucType strucType() const {return returnedStruct_;}     //!< returns structure of returned value

    virtual SpectralBasis* clone() const =0; //!< virtual copy constructor
    virtual void setGeomElementPointer(const void*) const
    {
      where("SpectralBasis::setGeomElementPointer()");
      error("function_type_not_handled", words("function type",funcFormType_));
    }

    virtual void print(std::ostream&) const = 0; //!< print utility
    virtual void print(PrintStream& os) const =0;

    //@{
    /*!
      evaluate linear combination of spectral functions, say v = sum_k a_k f_k(P)
      no virtual template !!
    */
    virtual real_t& evaluate(const std::vector<real_t>&, const Vector<real_t>&, real_t&) const =0;
    virtual complex_t& evaluate(const std::vector<real_t>&, const Vector<complex_t>&, complex_t&) const =0;
    //@}
};

std::ostream& operator<<(std::ostream&, const SpectralBasis&); //!< print operator

/*!
  \class SpectralBasisFun

  defines class of global basis functions given by an analytic expression.
  It uses the general Function class (defined in utils lib) with function of the form:
      \code
      T function(const Point&, Parameters&)
      T is either a real_t or a complex_t or a Vector<real_t> or a Vector<complex_t>
      \endcode
  providing the computation of the n-th basis function at a given point.

  For instance, to define the spectral basis a_n sin(n pi x), define the standard c++ function:
      \code
      real_t sin_n(const Point &p,Parameters& pa = defaultParameters) {
        real_t x=p.x();
        real_t h=pa("h");                    //get the parameter h (user definition)
        number_t n=pa("basis size");         //get the size of the basis
        number_t id=pa("basis index");       //get the index of function to compute
        return sqrt(2/h)*sin(id*pi*x/h);}  //computation
      }
      \endcode
  and use it as follows:
      \code
      Parameters p(1,"h");
      SpectralBasisFun sinBasis(Omega,Function(sin_n,p),10); //define the set of basis function
      real_t s1;
      sinBasis.functions(1,Point(0.5),s1); //compute the basis function n=1 at point x=0.5 in S1
      Vector<real_t> sn;
      sinBasis.functions(Point(0.5),sn);   //compute all the basis functions at point x=0.5 in Sn
      \endcode
 */

class SpectralBasisFun : public SpectralBasis
{
  protected:
    mutable Function functions_; //!< function object defining all the basis function
  public:
    SpectralBasisFun(const GeomDomain&, const Function&, number_t); //!< constructor
    SpectralBasisFun(const Function&, number_t n);                  //!< constructor with no GeomDomain
    SpectralBasisFun(const SpectralBasisFun &);                     //!< copy constructor
    virtual SpectralBasisFun* clone() const                         //!  clone of current returned as pointer (covariant)
       {return new SpectralBasisFun(*this);}
    SpectralBasisFun& operator = (const SpectralBasisFun &);        //!< assign operator
    ~SpectralBasisFun() {}

    Function& basis() {return functions_;}                          //!< access to functions
    const Function& basis() const {return functions_;}              //!< access to functions
    virtual void setGeomElementPointer(const void* pd) const        //! link geomElement pointer to function
    {functions_.setParam(pd, "GeomElement pointer");}
    virtual void print(std::ostream&) const;
    virtual void print(PrintStream& os) const {print(os.currentStream());}

    // templated computation functions
    template<typename T>
    T& function(number_t, const Point&, T&, DiffOpType =_id) const;              //!< compute n-th function
    template<typename T>
    Vector<T>& functions(const Point& P, Vector<T>& res, DiffOpType =_id) const; //!< compute all functions
    template<typename T>
    T& evaluateT(const std::vector<real_t>&, const Vector<T>&, T&) const; //!< evaluate linear combination of basis functions
    virtual real_t& evaluate(const std::vector<real_t>& p, const Vector<real_t>& a, real_t& v) const
      {return evaluateT(p,a,v);} //!< evaluate linear combination of real basis functions
    virtual complex_t& evaluate(const std::vector<real_t>& p, const Vector<complex_t>& a, complex_t& v) const
      {return evaluateT(p,a,v);} //!< evaluate linear combination of complex basis functions
};

//templated computation functions
template<typename T>
T& SpectralBasisFun::function(number_t n, const Point& P, T& res, DiffOpType dif) const // compute the n-th function
{
  basis().params()("basis index") = n;             // specify index n in params_ (_id,_dx,_dy,_dz)
  setBasisIndex(n);                                // set basis index thread data
  basis().params()("derivative") = number_t(dif);  // specify derivative in params_
  setDerivative(n);                                // set derivative index thread data
  basis()(P, res);                                 // compute the value
  return res;
}

template<typename T>
Vector<T>& SpectralBasisFun::functions(const Point& P, Vector<T>& res, DiffOpType dif) const // compute all basis functions
{
  res.resize(numberOfFun_);
  basis().params()("derivative") = number_t(dif);    // specify derivative as number_t in params_ (_id,_dx,_dy,_dz)
  setDerivative(number_t(dif));                      // set derivative index thread data
  for (number_t n = 1; n <= numberOfFun_; n++)
  {
    basis().params()("basis index") = n;   // specify index n in params_
    setBasisIndex(n);                      // set basis index thread data
    basis()(P, res(n));                    // compute the n-th value
  }
  return res;
}

//evaluate linear combination of spectral functions, say v = sum_k a_k f_k(P)
//currently works only for scalars of same value type
template<typename T>
T& SpectralBasisFun::evaluateT(const std::vector<real_t>& P, const Vector<T>& a, T& v) const
{
    Vector<T> fs;
    functions(P,fs);
    typename Vector<T>::const_iterator ita=a.begin();
    typename Vector<T>::iterator itf=fs.begin();
    for(;ita!=a.end() && itf!=fs.end(); ita++, itf++) v+= *ita * *itf;
    return v;
}

} // end of namespace xlifepp

#endif // SPECTRAL_BASIS_HPP
