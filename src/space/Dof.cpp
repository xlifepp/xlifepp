/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Dof.cpp
  \author E. Lunéville
  \since 25 fev 2012
  \date 12 jun 2012

  \brief Implementation of dof classes members and functionalities
*/

#include "Dof.hpp"
#include "Space.hpp"
#include "SpSpace.hpp"
#include "Element.hpp"
#include "utils.h"
#include "Unknown.hpp"

namespace xlifepp
{

//---------------------------------------------------------------------------
// Dof member functions and related external functions
//---------------------------------------------------------------------------

//comparison operators for sort
//---------------------------------------------------------------------------

bool operator==(const Dof& dof1, const Dof& dof2)
{
  return (dof1.id() == dof2.id());
}
bool operator<(const Dof& dof1, const Dof& dof2)
{
  return (dof1.id() < dof2.id());
}

void Dof::print(std::ostream& out) const
{
  out << message("dof_info", words("dof", doftype_), id_);
}

std::ostream& operator<<(std::ostream& out, const Dof& d)
{
  d.print(out);
  return out;
}

//---------------------------------------------------------------------------
// FeDof member functions and related external functions
//---------------------------------------------------------------------------
//compute coords of a FeDof from coords of refdof
void FeDof::setCoords() const
{
  GeomElement* geoelt=inElements[0].first->geomElt_p;
  MeshElement* meltp=geoelt->meshElement();
  if(meltp==nullptr) meltp=geoelt->buildSideMeshElement();
  Point refpt=refDof_p->point();
  if(refpt.size()==0)
    {
      xlifepp::where("FeDof::setCoords()");
      error("is_void","refpt");
    }
  GeomMapData gmap(meltp,refpt);
  coords_= gmap.geomMap(refpt);
}

//return FeDof coordinates, built in if not exist
Point& FeDof::coords() const
{
  if(coords_.size()==0) setCoords();
  return coords_;
}

//return the i-th coordinates of FeDof (i>=1)
real_t FeDof::coords(const dimen_t i) const
{
  if(coords_.size()==0) setCoords();
  return coords_[i-1];
}

//return FeDof coordinates, built in if not exist
dimen_t FeDof::dim() const
{
  return inElements[0].first->geomElt_p->spaceDim();
}

// return some dof data as string_t, w: width in char of dof numbers
string_t FeDof::dofData(number_t w) const
{
  number_t n = std::floor(std::log10(id_))+1;
  number_t s=0;
  if(w>0 && w>n) s=w-n;
  string_t sd= "FE dof "+tostring(id_)+string_t(s+1,' ');
  sd+=coords().toString();
  return sd;
}

Space* FeDof::space() const
{
  return reinterpret_cast<Space*>(space_p);
}
//===================================================================================
//! eval dof (viewed as a distribution) at a function f, e.g. for a lagrange dof: d_i(f)=f(Mi)
//===================================================================================
Value FeDof::operator()(const Function& f, const Function& gradf, const Function& grad2f) const
{
  dimen_t dim = refDof_p->supportDim();
  dimen_t dimfun = dimFun();
  number_t ord=refDof_p->order();
  DiffOpType op=refDof_p->diffop();
  ValueType vt=f.valueType();
  StrucType st=f.strucType();
  dimPair ds=f.dims();
  number_t nv=ds.first/dimfun;  // nv>1 and dimfun>1 not at the same time !
  real_t r; complex_t c;
  Vector<real_t> vr; Vector<complex_t>vc;
  switch (dim)
  {
    case 0: // use only f (Lagrange dof, u.n, u.t dof)
    {
      Point& P = coords();
      if(f.isVoidFunction()) error("free_error","dof(f) requires a non void f ");
      switch(ord)
      {
        case 0:
          if (projVector_.size()<2) // not a projection, nodal value
          {
            if (st==_scalar && nv==1) // scalar case
            {
              if (vt==_real) return Value(f(P,r));
              else return Value(f(P,c));
            }
            else if (st==_vector && ds.first==nv)    // vector case (scalar dof)
            {
              if(vt==_real) return Value(f(P,vr));
              else return Value(f(P,vc));
            }
            else error("free_error"," function passed to dof(f) not consistent");
          }
          else // use projVector_ (u.n, u.t dof)
          {
            if (st==_vector && ds.first==dimfun) //f should return a vector of dim projVector = dimfun !
            {
              if (vt==_real) return Value(dot(f(P,vr),projVector_));
              else return Value(dot(f(P,vc),projVector_));
            }
            else  error("free_error","dof(f) requires a vector function");
          }
          break;
        case 1:
          if (gradf.isVoidFunction()) error("free_error","dof(f,gradf) requires the gradient of f ");
          if (dimfun>1) error("free_error","dof(f,gradf) does not handle vector dof with derivatives");
          if (nv==1) //scalar unknown request
          {
            switch (op)
            {
              case _dx:
                if (vt==_real) return Value(gradf(P,vr)[0]);
                else  return Value(gradf(P,vc)[0]);
              case _dy:
                if (vt==_real) return Value(gradf(P,vr)[1]);
                else return Value(gradf(P,vc)[1]);
              case _dz:
                if (vt==_real) return Value(gradf(P,vr)[2]);
                else return Value(gradf(P,vc)[2]);
              case _ndotgrad:
              case _ncrossgrad:
                if (vt==_real) return Value(dot(gradf(P,vr),projVector_));
                else return Value(dot(gradf(P,vc),projVector_));
              default: error("free_error","unknown diff operator in FeDof::operator()");
            }
            if(projVector_.size()!=0)
            {
              if (vt==_real) return Value(dot(gradf(P,vr),projVector_));
              else return Value(dot(gradf(P,vc),projVector_));
            }
            if (derVector_.size()!=0)
            {
              if (vt==_real) return Value(dot(gradf(P,vr),derVector_));
              else return Value(dot(gradf(P,vc),derVector_));
            }
          }
          else //vector unknown request
          {
            if (vt==_real)
            {
              gradf(P,vr);
              Vector<real_t> res(nv,0.);
              for (number_t i=0;i<nv;i++)
              {
                number_t ti=nv*i;
                switch (op)
                {
                  case _dx: res[i]=vr[ti]; break;
                  case _dy: res[i]=vr[ti+1]; break;
                  case _dz: res[i]=vr[ti+2]; break;
                  case _ndotgrad:
                  case _ncrossgrad: res[i]=dot(std::vector<real_t>(vr.begin()+ti,vr.begin()+(ti+nv)),projVector_); break;
                  default: error("free_error","unknown diff operator in FeDof::operator()");
                }
              }
              return Value(res);
            }
            else //complex
            {
              gradf(P,vc);
              Vector<complex_t> res(nv,complex_t(0.));
              for(number_t i=0;i<nv;i++)
              {
                number_t ti=nv*i;
                switch (op)
                {
                  case _dx: res[i]=vc[ti]; break;
                  case _dy: res[i]=vc[ti+1]; break;
                  case _dz: res[i]=vc[ti+2]; break;
                  case _ndotgrad:
                  case _ncrossgrad: res[i]=dot(std::vector<complex_t>(vc.begin()+ti,vc.begin()+(ti+nv)),projVector_); break;
                  default: error("free_error","unknown diff operator in FeDof::operator()");
                }
              }
              return Value(res);
            }
          }
          break;
        case 2:
          if (grad2f.isVoidFunction())  error("free_error","dof(f,gf,grad2f) requires the second derivatives of f ");
          if (dimfun>1) error("free_error","dof(f,gf,grad2f) does not handle vector dof with second derivatives");
          if (nv==1) //scalar unknown request
          {
            switch (op)  // grad2f should return a vector [dxx, dyy, dxy, dzz, dxz, dyz] depending on the dimension
            {
              case _dxx:
                if (vt==_real) return Value(grad2f(P,vr)[0]);
                else return Value(grad2f(P,vc)[0]);
              case _dyy:
                if (vt==_real) return Value(grad2f(P,vr)[1]);
                else return Value(grad2f(P,vc)[1]);
              case _dxy:
                if (vt==_real) return Value(grad2f(P,vr)[2]);
                else  return Value(grad2f(P,vc)[2]);
              case _dzz:
                if (vt==_real) return Value(grad2f(P,vr)[3]);
                else return Value(grad2f(P,vc)[3]);
              case _dxz:
                if (vt==_real) return Value(grad2f(P,vr)[4]);
                else return Value(grad2f(P,vc)[4]);
              case _dyz:
                if (vt==_real) return Value(grad2f(P,vr)[5]);
                else return Value(grad2f(P,vc)[5]);
              default: error("free_error","unknown diff operator in FeDof::operator()");
            }
          }
          else
          {
            number_t s=nv*(nv+1)/2;
            if (vt==_real)
            {
              grad2f(P,vr);
              Vector<real_t> res(nv,0.);
              for(number_t i=0;i<nv;i++)
              {
                number_t ti=s*i;
                switch (op)
                {
                  case _dxx: res[i]=vr[ti]; break;
                  case _dyy: res[i]=vr[ti+1]; break;
                  case _dxy: res[i]=vr[ti+2]; break;
                  case _dzz: res[i]=vr[ti+3]; break;
                  case _dxz: res[i]=vr[ti+4]; break;
                  case _dyz: res[i]=vr[ti+5]; break;
                  default: error("free_error","unknown diff operator in FeDof::operator()");
                }
              }
              return Value(res);
            }
            else //complex
            {
              grad2f(P,vc);
              Vector<complex_t> res(nv,complex_t(0.));
              for(number_t i=0;i<nv;i++)
              {
                number_t ti=s*i;
                switch (op)
                {
                  case _dxx: res[i]=vc[ti]; break;
                  case _dyy: res[i]=vc[ti+1]; break;
                  case _dxy: res[i]=vc[ti+2]; break;
                  case _dzz: res[i]=vc[ti+3]; break;
                  case _dxz: res[i]=vc[ti+4]; break;
                  case _dyz: res[i]=vc[ti+5]; break;
                  default: error("free_error","unknown diff operator in FeDof::operator()");
                }
                return Value(res);
              }
            }
          }
          break;
        default:  error("free_error","dof(f) not yet available for dof of order "+tostring(ord));
      }
    }
    break;
    default: return evalProjectDof(f,gradf,grad2f);
    //    error("free_error","dof(f) not yet available for dof with support dim = "+tostring(dim));
  }
  return Value(0);
}
//===================================================================================
//! evaluate dof at function f using polynomials dof definition, may be used for any dof
// may require gradf and grad2f
// generic case
// see particular cases in ref element when spaces Ek, Fk or Kk are not allocated
//===================================================================================
Value FeDof::evalProjectDof(const Function& f, const Function& gradf, const Function& grad2f) const
{
  if (supportDim()==0) return (*this)(f,gradf,grad2f);  //ponctual dof: use Dof(f,gradf,grad2f)
  if (projVector_.size()==0)
      error("free_error", "up to now, RefElement::operator() works only for int_s u.p q dof");
  //non pontual dof, defined
  RefElement* relt = refDof_p->refElement();
  DofLocalization w=refDof_p->where();               // hierarchic localization of dof
  FESubType fs = relt->interpolation_p->subtype;
  number_t e=refDof_p->supportNumber();              // edge localization if edge
  number_t i=refDof_p->index();                      // rank of the dof in its localization
  const GeomElement& gelt=*elements()[0].first->geomElt_p;
  number_t k = relt->interpolation_p->numtype;
  Vector<real_t> vr;
  Vector<complex_t> vc;
  if (w==_onEdge) //edge D.o.Fs:  v-> int_e v.t q, q in Ek
  {
    Point S1 = gelt.edgeVertex(1,e), S2=gelt.edgeVertex(2,e);  //map to segment [0,1]
    if (relt->Ek.size()==0)  return relt->evalEdgeDof(S1,S2,e,i,f,gradf,grad2f);
    real_t mes=S1.distance(S2);
    dimen_t d=relt->maxDegree + relt->Ek.degree();
    Quadrature* quad=findQuadrature(_segment, Quadrature::bestQuadRule(_segment,d), d);  //quadrature rule on segment of order d (integrates exactly x^d)
    bool revEdge = relt->reverseEdge;
    std::vector<real_t>::const_iterator itc=quad->coords().begin(), itw=quad->weights().begin(),ite=quad->weights().end();
    PolynomialsBasis::const_iterator itq=relt->Ek.begin();
    for (number_t j=1;j<i;j++) itq++;
    const Polynomial& q=(*itq)[0];
    if (f.valueType()==_real)
    {
      real_t res=0.;
      for (;itw!=ite; ++itw, ++itc)
      {
        real_t s=*itc;
        if (revEdge) res+=q(s)*dot(f((1-s)*S2+s*S1,vr),projVector_)* *itw;
        else         res+=q(s)*dot(f((1-s)*S1+s*S2,vr),projVector_)* *itw;
      }
      res*= mes;
      return Value(res);
    }
    else
    {
      complex_t res=0.;
      for (;itw!=ite; ++itw, ++itc)
      {
        real_t s=*itc;
        if (revEdge) res+=q(s)*dot(f((1-s)*S2+s*S1,vc),projVector_)* *itw;
        else         res+=q(s)*dot(f((1-s)*S1+s*S2,vc),projVector_)* *itw;
      }
      res*= mes;
      return Value(res);
    }
  }
  if (w==_onFace) //face D.o.Fs:  v-> int_f v.q, q in Fk,  ONLY 3D ELEMENT !
  {
    GeomElement sidelt(const_cast<GeomElement*>(&gelt), e, 0);   //temporary side geom element
    sidelt.buildSideMeshElement();     //build side meshelement
    if (relt->Fk.size()==0)  return relt->evalFaceDof(*this,sidelt,f,gradf,grad2f);
    dimen_t d=relt->maxDegree + relt->Fk.degree();
    ShapeType sh = sidelt.shapeType();
    GeomMapData gd(sidelt.meshElement());
    dimen_t di=sidelt.elementDim();
    gd.computeJacobianMatrix(Point(std::vector<real_t>(di, 0.)));
    gd.computeJacobianDeterminant();
    Quadrature* quad=findBestQuadrature(sh, d);  //quadrature rule on shape of order d (integrates exactly x^d)
    std::vector<real_t>::const_iterator itc=quad->coords().begin(), itw=quad->weights().begin(),ite=quad->weights().end();
    PolynomialsBasis::const_iterator itq=relt->Fk.begin();
    for (number_t j=1;j<i;j++) itq++;
    const Polynomial& q=(*itq)[0];
    if (f.valueType()==_real)
    {
      real_t res=0.;
      for (; itw!=ite; itc+=2, ++itw)
      {
        real_t x=*itc, y=*(itc+1);
        res+=dot(f(gd.geomMap(Point(x,y)),vr), eval(*itq,x,y))* *itw ; // doit-on appliquer l'inverse de la map covariant à f ????
      }
      res*= gd.differentialElement;
      return Value(res);
    }
    else
    {
      complex_t res=0.;
      for (; itw!=ite; itc+=2, ++itw)
      {
        real_t x=*itc, y=*(itc+1);
        res+=dot(f(gd.geomMap(Point(x,y)),vc), eval(*itq,x,y))* *itw ;  // doit-on appliquer l'inverse de la map covariant à f ????
      }
      res*= gd.differentialElement;
      return Value(res);
    }
  }
  //element  D.o.Fs (k>0) :  v-> int_t v.q,  q in Kk
  if (gelt.meshElement()==nullptr) gelt.buildSideMeshElement();
  if (relt->Kk.size()==0)  return relt->evalEltDof(*this,f,gradf,grad2f);
  const MeshElement* melt = gelt.meshElement();
  GeomMapData* mapdata=melt->geomMapData_p;
  if (mapdata==nullptr)
  {
    mapdata=new GeomMapData(melt);
    melt->geomMapData_p = mapdata;
    mapdata->computeJacobianMatrix(std::vector<real_t>(gelt.spaceDim(),0.));
    mapdata->computeJacobianDeterminant();
  }
  dimen_t di=gelt.elementDim();
  PolynomialsBasis::const_iterator itq=relt->Kk.begin();
  for (number_t j=1;j<i;j++) itq++;
  dimen_t d=relt->maxDegree+relt->Kk.degree();
  Quadrature* quad=findBestQuadrature(gelt.shapeType(), d);  //quadrature rule on shape of order d (integrates exactly x^d)
  std::vector<real_t>::const_iterator itc=quad->coords().begin(), itw=quad->weights().begin(),ite=quad->weights().end();
  if (f.valueType()==_real)
  {
    real_t res=0.;
    for (; itw!=ite; itc+=di, ++itw)
    {
      real_t x=*itc, y=*(itc+1), z=(di==2?0:*(itc+2));
      if (di==2)
        res+=dot(f(mapdata->geomMap(Point(x,y)),vr), eval(*itq,x,y))* *itw ; // doit-on appliquer l'inverse de la map covariant à f ????
      else if (di==3)
        res+=dot(f(mapdata->geomMap(Point(x,y,z)),vr), eval(*itq,x,y,z))* *itw ;
    }
    res*= mapdata->differentialElement;;
    return Value(res);
  }
  else
  {
    complex_t res=0.;
    for (; itw!=ite; itc+=di, ++itw)
    {
      real_t x=*itc, y=*(itc+1),  z=(di==2?0:*(itc+2));
      if (di==2)
        res+=dot(f(mapdata->geomMap(Point(x,y)),vc), eval(*itq,x,y))* *itw ; // doit-on appliquer l'inverse de la map covariant à f ????
      else if(di==3)
        res+=dot(f(mapdata->geomMap(Point(x,y,z)),vc), eval(*itq,x,y,z))* *itw ;
    }
    res*= mapdata->differentialElement;
    return Value(res);
  }
  xlifepp::where("FeDof::evalProjectDof");
  error("free_error","abnormal value type, contact administrator");
  return Value(0);
}

/* specific eval face dof for NedelecEdgeFirstTetrahedronPk
   dof: related dof
   selt: physical face
   f, gradf, grad2f: function to be evaluated
*/
Value NedelecEdgeFirstTetrahedronPk::evalFaceDof(const FeDof& dof, const GeomElement& selt, const Function& f, const Function&gradf, const Function&grad2f) const
{
  number_t i = dof.refDofP()->index(), fa = dof.refDofP()->supportNumber();
  LagrangeStdTrianglePk triPk(findInterpolation(_Lagrange, _standard, interpolation_p->numtype-2, H1));
  PolynomialsBasis &Q=triPk.Wk;       //create dual space for face dofs: P_(k-2)[x,y]
  dimen_t d = maxDegree + Q.degree();
  dimen_t di=selt.elementDim();
  GeomMapData gd(selt.meshElement());
  gd.computeJacobianMatrix(Point(std::vector<real_t>(di, 0.)));
  gd.computeJacobianDeterminant();
  Vector<real_t> S12=selt.vertex(2)-selt.vertex(1), S13=selt.vertex(3)-selt.vertex(1), S23=selt.vertex(3)-selt.vertex(2);
  Vector<real_t> V1=S12, V2=S13;
  switch(fa)
  {
    case 1: V1=-S23; V2=-S13; break;
    case 2: V1= S23; V2=-S12; break;
    case 3: V1=-S23; V2=-S13; break;
    default: break;
  }
  Quadrature* quad=findBestQuadrature(_triangle, d);  //quadrature rule on triangle of order d (integrates exactly x^d)
  std::vector<real_t>::const_iterator itc=quad->coords().begin(), itw=quad->weights().begin(),ite=quad->weights().end();
  PolynomialsBasis::const_iterator itq=Q.begin();
  number_t i2=(i-1)/2;
  for(number_t j=0;j<i2;j++) itq++;
  const Polynomial& q=(*itq)[0];
  Vector<real_t> vr; Vector<complex_t> vc;
  if(f.valueType()==_real)
    {
      real_t res=0.;
      for(; itw!=ite; itc+=2, ++itw)
      {
        real_t x=*itc, y=*(itc+1);
        if(i%2==0) res+=q(x,y)*dot(f(gd.geomMap(Point(x,y)),vr), V2)* *itw ;
        else       res+=q(x,y)*dot(f(gd.geomMap(Point(x,y)),vr), V1)* *itw ;

      }
      //res*= gd.differentialElement;
      res*= 2;
      return Value(res);
    }
    else
    {
      complex_t res=0.;
      for(; itw!=ite; itc+=2, ++itw)
      {
        real_t x=*itc, y=*(itc+1);
        if(i%2==0) res+=q(x,y)*dot(f(gd.geomMap(Point(x,y)),vc), V2)* *itw ;
        else       res+=q(x,y)*dot(f(gd.geomMap(Point(x,y)),vc), V1)* *itw ;
      }
      //res*= gd.differentialElement;
      res*= 2;
      return Value(res);
    }
}

/* specific eval face dof for NedelecFaceFirstTetrahedronPk
   dof: related dof
   selt: physical face
   f, gradf, grad2f: function to be evaluated
*/
Value NedelecFaceFirstTetrahedronPk::evalFaceDof(const FeDof& dof, const GeomElement& selt, const Function& f, const Function& gradf, const Function& grad2f) const
{
  number_t i = dof.refDofP()->index(), fa = dof.refDofP()->supportNumber();
  LagrangeStdTrianglePk triPk(findInterpolation(_Lagrange, _standard, interpolation_p->numtype-1, H1));
  PolynomialsBasis &Q=triPk.Wk;       //create dual space for face dofs: P_(k-1)[x,y]
  dimen_t d = maxDegree + Q.degree();
  dimen_t di=selt.elementDim();
  GeomMapData gd(selt.meshElement());
  gd.computeJacobianMatrix(Point(std::vector<real_t>(di, 0.)));
  gd.computeJacobianDeterminant();
  Quadrature* quad=findBestQuadrature(_triangle, d);  //quadrature rule on triangle of order d (integrates exactly x^d)
  std::vector<real_t>::const_iterator itc=quad->coords().begin(), itw=quad->weights().begin(),ite=quad->weights().end();
  PolynomialsBasis::const_iterator itq=Q.begin();
  for(number_t j=1;j<i;j++) itq++;
  const Polynomial& q=(*itq)[0];
  Vector<real_t> vr; Vector<complex_t> vc;
  const Vector<real_t>& n = dof.projVector();
  if(f.valueType()==_real)
    {
      real_t res=0.;
      for(; itw!=ite; itc+=2, ++itw)
      {
        real_t x=*itc, y=*(itc+1);
        res+=q(x,y)*dot(f(gd.geomMap(Point(x,y)),vr), n)* *itw ;
      }
      res*= gd.differentialElement;
      return Value(res);
    }
    else
    {
      complex_t res=0.;
      for(; itw!=ite; itc+=2, ++itw)
      {
        real_t x=*itc, y=*(itc+1);
        res+=q(x,y)*dot(f(gd.geomMap(Point(x,y)),vc), n)* *itw ;
      }
      res*= gd.differentialElement;
      return Value(res);
    }
}

/* specific eval face dof for NedelecEdgeFirstHexahedronPk
   dof: related dof
   selt: physical face
   f, gradf, grad2f: function to be evaluated
   face dofs (k>1)  : v-> int_f (v x n).q, q in Q_(k-2,k-1) x Q_(k-1,k-2)  2k(k-1) dofs by face
*/
Value NedelecEdgeFirstHexahedronPk::evalFaceDof(const FeDof& dof, const GeomElement& selt, const Function& f, const Function& gradf, const Function& grad2f) const
{
  number_t i = dof.refDofP()->index(), fa = dof.refDofP()->supportNumber();
  PolynomialsBasis Q(_DQ3k,2,interpolation_p->numtype-1);
  dimen_t d = maxDegree + Q.degree();
  //dof are ordering following ascending vertex numbers
  number_t i1=1, mini=selt.vertexNumber(1), i2, i3, ii;
  for(number_t i=2;i<=4;i++)
    if(selt.vertexNumber(i)<mini) {i1=i;mini=selt.vertexNumber(i);}
  i2= (i1<4?i1+1:1); i3= (i1>1?i1-1:4);
  if(selt.vertexNumber(i3)<selt.vertexNumber(i2)) {ii=i2;i2=i3;i3=ii;}
  Point S1 = selt.vertex(i1), S2=selt.vertex(i2), S4=selt.vertex(i3), P;
  Point t1 = S2-S1, t2=S4-S1;
  real_t m=norm(crossProduct(t1,t2));
  t1/=norm(t1); t2/=norm(t2);
  const Vector<real_t>& n = dof.projVector();
  //change tangent vector because covariant map change tangent vector ti <= J*(J^-1 n x J^-1 ti)
  GeomMapData* gm=dof.elements()[0].first->geomElt_p->meshElement()->geomMapData_p; //should not be null pointer
  gm->invertJacobianMatrix();
  Vector<real_t> jn=gm->inverseJacobianMatrix*n;
  t1=Point(gm->jacobianMatrix*crossProduct(jn,gm->inverseJacobianMatrix*Vector<real_t>(t1)));
  t2=Point(gm->jacobianMatrix*crossProduct(jn,gm->inverseJacobianMatrix*Vector<real_t>(t2)));
  //choose quadrature
  Quadrature* quad=findBestQuadrature(_quadrangle, d);  //quadrature rule on quadrangle of order d (integrates exactly x^d)
  std::vector<real_t>::const_iterator itc=quad->coords().begin(), itw=quad->weights().begin(),ite=quad->weights().end();
  PolynomialsBasis::const_iterator itq=Q.begin();
  for(number_t j=1;j<i;j++) itq++;
  const Polynomial& q=(*itq)[0];
  real_t res; complex_t ces;
  Vector<real_t> vr; Vector<complex_t> vc;
  bool isReal= f.valueType()==_real;
  std::vector<real_t> qis(3,0.);
  for(; itw!=ite; itc+=2, ++itw)
  {
    real_t x=*itc, y=*(itc+1);
    real_t qi1=eval(*itq,x,y)[0], qi2=eval(*itq,x,y)[1]; //polynomial q on ref quadrangle
    qis = qi1*t1+qi2*t2;
    P = (1-x-y)*S1+x*S2+y*S4;  //=S1+x*t1+y*t2
    if(isReal) res+= dot(qis,f(P,vr))* *itw;
    else       ces+= dot(qis,f(P,vc))* *itw;
  }
  if(isReal) return Value(res*=m);
  return Value(ces*=m);
}

/* specific eval elt dof for NedelecEdgeFirstHexahedronPk
   gelt: physical element
   i: dof index
   f, gradf, grad2f: function to be evaluated
   hexahedron dofs (k>1) : u-> int_t u.q,       q in Q_(k-1,k-2,k-2) x Q_(k-2,k-1,k-2)x Q_(k-2,k-2,k-1)   3k(k-1)^2 dofs
*/
Value NedelecEdgeFirstHexahedronPk::evalEltDof(const FeDof& dof, const Function& f, const Function& gradf, const Function& grad2f) const
{
    RefElement* relt = dof.refDofP()->refElement();
    number_t i=dof.refDofP()->index();                      // rank of the dof in its localization
    const GeomElement& gelt=*dof.elements()[0].first->geomElt_p;
    number_t k = relt->interpolation_p->numtype;
    Vector<real_t> vr;
    Vector<complex_t> vc;
    const MeshElement* melt = gelt.meshElement();
    GeomMapData* mapdata=melt->geomMapData_p;
    if(mapdata==nullptr)
    {
      mapdata=new GeomMapData(melt);
      melt->geomMapData_p = mapdata;
      mapdata->computeJacobianMatrix(std::vector<real_t>(gelt.spaceDim(),0.));
      mapdata->computeJacobianDeterminant();
    }
    PolynomialsBasis Kk(_DQk,3,k-1);
    PolynomialsBasis::iterator itq=Kk.begin();
    for(number_t j=1;j<i;j++) itq++;
    dimen_t d=relt->maxDegree+Kk.degree();
    Quadrature* quad=findBestQuadrature(gelt.shapeType(), d);  //quadrature rule on shape of order d (integrates exactly x^d)
    std::vector<real_t>::const_iterator itc=quad->coords().begin(), itw=quad->weights().begin(),ite=quad->weights().end();
    if(f.valueType()==_real)
    {
      real_t res=0.;
      for(; itw!=ite; itc+=3, ++itw)
      {
        real_t x=*itc, y=*(itc+1), z=*(itc+2);
        res+=dot(f(mapdata->geomMap(Point(x,y,z)),vr), mapdata->jacobianMatrix*Vector<real_t>(eval(*itq,x,y,z)))* *itw ;
      }
      return Value(res);
    }
    else
    {
      complex_t res=0.;
      for(; itw!=ite; itc+=3, ++itw)
       {
        real_t x=*itc, y=*(itc+1),  z=*(itc+2);
        res+=dot(f(mapdata->geomMap(Point(x,y,z)),vc), mapdata->jacobianMatrix*Vector<real_t>(eval(*itq,x,y,z)))* *itw ;
      }
      return Value(res);
    }

}


//===================================================================================
//                                  print stuff
//===================================================================================
void FeDof::print(std::ostream& out) const
{
  string_t sh = words("shared");
  string_t vi = " ";
  if(refDof_p->supportDim()!=0) vi="virtual ";
  if(!shared_) { sh = words("not shared"); }
  out << message("fedof_info", id_, "support dim "+tostring(refDof_p->supportDim()),
                 refDof_p->supportNumber(), sh);
  out<<vi<<"coords: "<<coords();
  if(dofExt_!=nullptr) dofExt_->print(out);
}

//to be used in table presentation
void FeDof::printTab(std::ostream& out) const
{
  out.setf(std::ios::scientific);
  dimen_t sd=refDof_p->supportDim();
  out << std::setw(8) << id_ << " |" << std::setw(9) << words(shared_) << " |"
      << std::setw(10) << tostring(sd) << " | " << std::setw(11) << refDof_p->name() << " |"
      << std::setw(10) << words("dof location",refDof_p->where());
  //if(refDof_p->supportNumber()>0) out<<" "<<refDof_p->supportNumber();
  out<< " |";
  std::stringstream ss;
  std::vector<std::pair<Element*, number_t> >::const_iterator it;
  for(it = inElements.begin(); it != inElements.end(); it++) { ss << " " << it->first->number() << "(" << it->second << ")"; }
  out<<ss.str()<<string_t(std::max(1,int(20-ss.str().size())),' ');
  out<<std::setprecision(3);
  if(sd==0) out<< "    coords = "<<coords();
  else      out<< "    virtual coords = "<<coords();
  if(refDof_p->typeOfProjection()==_dotnProjection)  out<< "    n = "<<projVector_;
  if(refDof_p->typeOfProjection()==_crossnProjection)out<< "    t = "<<projVector_;
  if(dofExt_!=nullptr) {out<<" "; dofExt_->print(out);}

  out << eol;
  out.unsetf(std::ios::scientific);
}

/*to be used in table 2 presentation
     ti      ts   td         tt           tw             tc                tp                tn
  |  id  |shared|dim|       type       | where  |    connection    |    coordinates   |     normal    |
*/void FeDof::printTab2(std::ostream& out, number_t ti,number_t ts, number_t td,
                        number_t tt, number_t tw, number_t tc, number_t tp, number_t tn) const
{
  out.setf(std::ios::scientific);
  dimen_t sd=refDof_p->supportDim();
  out << "    |" << format(tostring(id_),ti) << "|" << format(words(shared_),ts) << "|"
      << format(tostring(sd),td) << "|" << format(refDof_p->name(),tt) << "|"
      << format(words("dof location",refDof_p->where()),tw)<< "|";
  std::stringstream ss;
  std::vector<std::pair<Element*, number_t> >::const_iterator it;
  for(it = inElements.begin(); it != inElements.end(); it++) { ss << " " << it->first->number() << "(" << it->second << ")"; }
  out<<format(ss.str(),tc)<<"|"<<std::setprecision(3);
  if(sd==0) out<< format(tostring(coords()),tp);
  else      out<< format(tostring(coords())+"*",tp);
  out<<"|";
  if(refDof_p->typeOfProjection()==_dotnProjection)  out<< format("n="+tostring(projVector_),tn);
  else if(refDof_p->typeOfProjection()==_crossnProjection)out<< format("t="+tostring(projVector_),tn);
  else out<<string_t(tn,' ');
  out<<"|";
  if(dofExt_!=nullptr) {out<<" "; dofExt_->print(out);}
  out << eol;
  out.unsetf(std::ios::scientific);
}

//dof dimension (shape function dim)
dimen_t SpDof::dimDof() const
{return space_p->dimFun();}

void SpDof::print(std::ostream& out) const
{
  out << message("spdof_info", words("dof", doftype_), id_, space_p->name());
}

// return some dof data as string_t
string_t SpDof::dofData(number_t w) const
{
  return "sp dof "+tostring(id_)+" ";
}

Space* SpDof::space() const
{return reinterpret_cast<Space*>(space_p);}

/*============================================================================================================
  DofComponent class functions
  ==========================================================================================================*/

DofComponent DofComponent::dual() const
{
  return DofComponent(u_p->dual_p(), dofnum, numc);
}

std::vector<DofComponent> dualDofComponents(const std::vector<DofComponent>& cdofs)
{
  std::vector<DofComponent> dcdofs(cdofs.size());
  std::vector<DofComponent>::const_iterator itc=cdofs.begin();
  std::vector<DofComponent>::iterator itd=dcdofs.begin();
  for(; itc!=cdofs.end(); itc++, itd++) *itd = itc->dual();
  return dcdofs;
}

void DofComponent::print(std::ostream& os) const
{
  //os<<"unknown "<<u_p->name()<<" dof number "<<dofnum<<" component "<<numc;
  os<<u_p->name()<<"_"<<dofnum;
  if(u_p->nbOfComponents()>1) os<<"_"<<numc;
}

std::ostream& operator<<(std::ostream& os, const DofComponent& dofc)
{
  dofc.print(os);
  return os;
}

bool operator ==(const DofComponent& dc1, const DofComponent& dc2)
{
  return (dc1.u_p == dc2.u_p && dc1.dofnum == dc2.dofnum && dc1.numc == dc2.numc);
}

bool operator !=(const DofComponent& dc1, const DofComponent& dc2)
{
  return (dc1.u_p != dc2.u_p || dc1.dofnum != dc2.dofnum || dc1.numc != dc2.numc);
}

bool operator <(const DofComponent& dc1, const DofComponent& dc2)
{
  if(dc1.u_p < dc2.u_p) return true;
  if(dc1.u_p > dc2.u_p) return false;
  if(dc1.dofnum < dc2.dofnum) return true;
  if(dc1.dofnum > dc2.dofnum) return false;
  if(dc1.numc < dc2.numc) return true;
  return false;
}

//! create cdofs from unknown dofs
std::vector<DofComponent> createCdofs(const Unknown* u, const std::vector<number_t>& dofs)
{
  dimen_t nbcu = u->nbOfComponents();
  std::vector<DofComponent> cdofs(dofs.size()*nbcu);
  std::vector<number_t>::const_iterator itd=dofs.begin();
  std::vector<DofComponent>::iterator itdc=cdofs.begin();
  for(; itd!=dofs.end(); itd++)
    for(dimen_t d = 0; d < nbcu; d++, itdc++) *itdc= DofComponent(u, *itd, d+1);
  return cdofs;
}

/*!renumber cdofs vector (cdv2) along cdofs vector (cdv1)
   assuming cdof of cdv1 are unique
 if cdv1=cdv2 nothing is done: the result is of size 0
 if cdv1!=cdv2 : rn is the renumbering vector
    ex:  cdv1 = cd8 cd4 cd5 cd1 cd4 cd7 cd6
         cdv2 = cd3 cd5 cd7 cd4
         rn   = 0 3 6 5
   0 means that cd3 does not belong to cdv1
   3 means that cd5 is the third cdofs of cdv1
   ...
*/
std::vector<number_t> renumber(const std::vector<DofComponent>& cdv1, const std::vector<DofComponent>& cdv2)
{
  trace_p->push("renumber(cdofs,cdofs)");
  std::vector<number_t> renum;
  std::vector<DofComponent>::const_iterator itc1=cdv1.begin(), itc2=cdv2.begin();
  if(cdv1.size()==cdv2.size())
    {
      while(itc1!=cdv1.end() && *itc1==*itc2) {itc1++; itc2++;}
      if(itc1==cdv1.end()) {trace_p->pop(); return renum;}  //same cdofs in same order
    }
  std::map<DofComponent,number_t> index1;  //index cdv1
  itc1=cdv1.begin(); itc2=cdv2.begin();
  number_t n=1;
  for(; itc1!=cdv1.end(); itc1++, n++) index1[*itc1]=n;
  renum.resize(cdv2.size(),0);
  std::map<DofComponent,number_t>::iterator itm;
  std::vector<number_t>::iterator itr=renum.begin();
  for(; itc2!=cdv2.end(); itc2++, itr++)
    {
      itm=index1.find(*itc2);
      if(itm!=index1.end()) *itr=itm->second;
    }
  trace_p->pop();
  return renum;
}

/*============================================================================================================
  BcDof class functions
  ==========================================================================================================*/
void BCsub::print(std::ostream& out) const
{out<<"("<<elt_p->number()<<", "<<sub_elt_num<<", "<<edge_num<<", "<<coef<<") ";}

std::ostream& operator<<(std::ostream& out, const BCsub& bcs)
{bcs.print(out); return out;}

/*! compute the shape value related to the BcDof
    P: physical space point where to compute
    elt: element containing P
    der1 : if true compute also first derivatives
    der2 : if true compute second derivatives (not managed)
    s: additional vector (not used)
*/

ShapeValues BCDof::computeShapeValue(const Point& P, const Element& elt, bool der1, bool der2, Vector<real_t>* s) const
{
  ShapeValues shs;
  number_t d=P.size();
  shs.w.resize(d,0.);
  shs.dw.resize(d,std::vector<real_t>(d,0.));
  //locate point P
  GeomElement* gelt=elt.geomElt_p;
  MeshElement* melt=gelt->meshElement();
  if(melt==nullptr) melt=gelt->buildSideMeshElement();
  GeomMapData* mapdata = melt->geomMapData_p;
  if(mapdata==nullptr) mapdata=new GeomMapData(melt);
  Point Q=mapdata->geomMapInverse(P);
  real_t l2=Q[0], l3=Q[1], l1=1-l2-l3;
  Point& S1=*(melt->nodes[0]),&S2=*(melt->nodes[1]),&S3=*(melt->nodes[2]);  // element vertices
  Point V1,V2,V3; // sub element vertices
  number_t subelt;
  if(l1>=l2)
    {
      if(l1<l3)
        {
          subelt=5;
          V1=S3; V2=0.5*(S1+S3); V3=melt->centroid;
        }
      else
        {
          if(l2<l3)
            {
              subelt=6;
              V1=S1; V2=melt->centroid; V3=0.5*(S1+S3);
            }
          else
            {
              subelt=1;
              V1=S1; V2=0.5*(S1+S2); V3=melt->centroid;
            }
        }
    }
  else //l1<l2
    {
      if(l2<l3)
        {
          subelt=4;
          V1=S3; V2=melt->centroid; V3=0.5*(S2+S3);
        }
      else
        {
          if(l1<l3)
            {
              subelt=3;
              V1=S2; V2=0.5*(S2+S3); V3=melt->centroid;
            }
          else
            {
              subelt=2;
              V1=S2; V2=melt->centroid; V3=0.5*(S1+S2);
            }
        }
    }
  real_t mes=0.5*std::abs(crossProduct(V2-V1,V3-V1)[2]);
  //compute shape function
  std::list<BCsub>::const_iterator itb= bcsubs.begin();
  for(; itb!=bcsubs.end(); ++itb)
    if(&elt==itb->elt_p && subelt==itb->sub_elt_num) itb->computeShapeValue(P,V1,V2,V3,mes,der1,der2,shs,s);
  return shs;
}
//! compute sub part of BC shape function, V1,V2,V3 vertices of the sub element (der2 not managed)
ShapeValues& BCsub::computeShapeValue(const Point& P, const Point& V1, const Point& V2, const Point& V3, real_t mes,
                                      bool der1, bool der2, ShapeValues& shs, Vector<real_t>* s) const
{
  Point R;
  number_t d=P.size();
  real_t a=coef/(2*mes), b1=a*dist(V1,V2), b2=a*dist(V2,V3), b3=a*dist(V1,V3);
  switch(edge_num)
    {
      case 1: R=b1*(P-V3); break;
      case 2: R=b2*(P-V1); break;
      case 3: R=b3*(P-V2); break;
    }
  shs.w[0]+=R[0];
  shs.w[1]+=R[1];
  if(d==3) shs.w[2]+=R[2];
  if(!der1) return shs;
  switch(edge_num)
    {
      case 1: shs.dw[0][0]+=b1; shs.dw[1][1]+=b1;
        if(d==3) shs.dw[2][2]+=b1; break;
      case 2: shs.dw[0][0]+=b2; shs.dw[1][1]+=b2;
        if(d==3) shs.dw[2][2]+=b2; break;
      case 3: shs.dw[0][0]+=b3; shs.dw[1][1]+=b3;
        if(d==3) shs.dw[2][2]+=b3; break;
    }
  return shs;
}

} // end of namespace xlifepp
