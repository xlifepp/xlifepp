/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Space.cpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 31 jul 2012

  \brief Implementation of space classes and functionnalities
*/

#include "Space.hpp"
#include "SpSpace.hpp"
#include "FeSpace.hpp"
#include "SubSpace.hpp"
#include "FeSubSpace.hpp"

#include "Unknown.hpp"
#include "../term/TermMatrix.hpp"

namespace xlifepp
{

//---------------------------------------------------------------------------
// Space member functions and related external functions
//---------------------------------------------------------------------------

void Space::buildSpace(const std::vector<Parameter>& ps)
{
  std::set<ParameterKey> params, usedParams, usedParamsFull;
  params.insert(_pk_basis);
  params.insert(_pk_basis_dim);
  params.insert(_pk_dim);
  params.insert(_pk_domain);
  params.insert(_pk_FE_type);
  params.insert(_pk_FE_subtype);
  params.insert(_pk_interpolation);
  params.insert(_pk_name);
  params.insert(_pk_order);
  params.insert(_pk_Sobolev_type);
  params.insert(_pk_optimizeNumbering);
  params.insert(_pk_notOptimizeNumbering);
  params.insert(_pk_withLocateData);
  params.insert(_pk_withoutLocateData);

  number_t dim, order, dimFun=1;
  FEType feType;
  FESubType feSubtype;
  InterpolationType interpolationType;
  Interpolation* interpolation_p=nullptr;
  SobolevType sobolevType;
  const GeomDomain* domain=nullptr;
  const SpectralBasis* spectralBasis=nullptr;
  const Function* basisFunction=nullptr;
  const TermVectors* basisTermVectors=nullptr;
  bool optimizeNumbering=true, withLocateData=true;
  string_t name;

  for (number_t i=0; i < ps.size(); ++i)
  {
    const Parameter& p=ps[i];
    ParameterKey key=p.key();
    switch (key)
    {
      case _pk_basis:
      {
        if (p.type() == _pointerFunction) { basisFunction=static_cast<const Function*>(p.get_p()); }
        else if ( p.type() == _pointerTermVectors) { basisTermVectors=static_cast<const TermVectors*>(p.get_p()); }
        else { error("param_badtype",words("value",p.type()),words("param key",key)); }
        break;
      }
      case _pk_basis_dim:
      {
        if (p.type()==_integer) { dimFun=p.get_n(); }
        else { error("param_badtype",words("value",p.type()),words("param key",key)); }
        break;
      }
      case _pk_dim:
      {
        if (p.type()==_integer) { dim=p.get_n(); }
        else { error("param_badtype",words("value",p.type()),words("param key",key)); }
        break;
      }
      case _pk_domain:
      {
        if (p.type()==_pointerGeomDomain) { domain=static_cast<const GeomDomain*>(p.get_p()); }
        else { error("param_badtype",words("value",p.type()),words("param key",key)); }
        break;
      }
      case _pk_FE_type:
      {
        switch (p.type())
        {
          case _integer:
          case _enumFEType:
            feType=FEType(p.get_n());
            break;
          default: error("param_badtype",words("value",p.type()),words("param key",key)); 
        }
        break;
      }
      case _pk_FE_subtype:
      {
        switch (p.type())
        {
          case _integer:
          case _enumFESubType:
            feSubtype=FESubType(p.get_n());
            break;
          default: error("param_badtype",words("value",p.type()),words("param key",key)); 
        }
        break;
      }
      case _pk_interpolation:
      {
        switch (p.type())
        {
          case _integer:
          case _enumInterpolationType:
            interpolationType=InterpolationType(p.get_n());
            break;
          default: error("param_badtype",words("value",p.type()),words("param key",key)); 
        }
        break;
      }
      case _pk_name:
      {
        if (p.type()==_string) { name=p.get_s(); }
        else { error("param_badtype",words("value",p.type()),words("param key",key)); }
        break;
      }
      case _pk_order:
      {
        if (p.type()==_integer) { order=p.get_n(); }
        else { error("param_badtype",words("value",p.type()),words("param key",key)); }
        break;
      }
      case _pk_Sobolev_type:
      {
        switch (p.type())
        {
          case _integer:
          case _enumSobolevType:
            sobolevType=SobolevType(p.get_n());
            break;
          default: error("param_badtype",words("value",p.type()),words("param key",key)); 
        }
        break;
      }
      case _pk_optimizeNumbering:
      {
        optimizeNumbering=true;
        break;
      }
      case _pk_notOptimizeNumbering:
      {
        optimizeNumbering=false;
        break;
      }
      case _pk_withLocateData:
      {
        withLocateData=true;
        break;
      }
      case _pk_withoutLocateData:
      {
        withLocateData=false;
        break;
      }
      default:
        error("space_unexpected_param_key",words("param key",key));
    }
    if (params.find(key) != params.end()) { params.erase(key); }
    else
    {
      if (usedParamsFull.find(key) == usedParamsFull.end())
      { error("space_unexpected_param_key", words("param key",key), name); }
      else { warning("param_already_used", words("param key",key)); }
    }
    usedParamsFull.insert(key);
    if (key != _pk_name) { usedParams.insert(key); }
  }

  // if _basis is used, it is a spectral space
  if (usedParams.find(_pk_basis) != usedParams.end())
  {
    // when defining a Space with a Function (namely a SpSpace), this could be a true Function or a TermVectors
    if (basisFunction != nullptr)
    {
      // if it is a true function, the user should also use keys _domain and _dim and other ones are not allowed
      if (usedParams.find(_pk_domain) == usedParams.end()) { error("param_missing","_domain"); }
      if (usedParams.find(_pk_dim) == usedParams.end()) { error("param_missing","_dim"); }
      for (std::set<ParameterKey>::const_iterator cit=usedParams.begin(); cit != usedParams.end(); ++cit)
      {
        if (*cit != _pk_basis && *cit != _pk_dim && *cit != _pk_basis_dim && *cit != _pk_domain) { error("param_conflict", words("param key",*cit), words("param key",_pk_basis)); }
      }
      buildSpFun(*domain, *basisFunction, dim, dimFun, name);
    }
    else // basisTermVectors != nullptr
    {
      // if it is a TermVectors, other keys are not allowed
      for (std::set<ParameterKey>::const_iterator cit=usedParams.begin(); cit != usedParams.end(); ++cit)
      {
        if (*cit != _pk_basis && *cit != _pk_domain) { error("param_conflict", words("param key",*cit), words("param key",_pk_basis)); }
      }
      SpectralBasisInt* sbi= new SpectralBasisInt(*basisTermVectors);
      SpSpace* sp_p = new SpSpace(name, sbi->domain(), sbi->numberOfFun(), sbi->dimFun(), static_cast<SpectralBasis*>(sbi));
      space_p = static_cast<Space*>(sp_p);        //cast SpSpace* to Space*
      spaceInfo_p = sp_p->spaceInfo_p;            //copy SpaceInfo pointer
      global=false;
      theSpaces.push_back(this);                  //store space in list of spaces
    }
  }
  else // _basis is not used
  {
    // If _basis_dim is used, it is still the definition of a SpSpace
    if (usedParams.find(_pk_basis_dim) != usedParams.end())
    {
      // _basis_dim cannot be used without _basis, _dim and _domain, and other keys are not allowed
      if (usedParams.find(_pk_basis) == usedParams.end()) { error("param_missing","_basis"); }
      if (usedParams.find(_pk_dim) == usedParams.end()) { error("param_missing","_dim"); }
      if (usedParams.find(_pk_domain) == usedParams.end()) { error("param_missing","_domain"); }
      for (std::set<ParameterKey>::const_iterator cit=usedParams.begin(); cit != usedParams.end(); ++cit)
      {
        if (*cit != _pk_basis && *cit != _pk_dim && *cit != _pk_domain)
        {
          error("param_conflict", words("param key",*cit), words("param key",_pk_basis_dim));
        }
      }
      buildSpFun(*domain, *basisFunction, dim, dimFun, name);
    }
    else // _basis_dim is not used
    {
      // every set of keys concerning true spectral spaces are already checked.
      if (usedParams.find(_pk_dim) != usedParams.end())
      {
        // _dim can still be used for spaces on R^n
        for (std::set<ParameterKey>::const_iterator cit=usedParams.begin(); cit != usedParams.end(); ++cit)
        {
          if (*cit != _pk_dim && *cit != _pk_domain)
          {
            error("param_conflict", words("param key",*cit), words("param key",_pk_dim));
          }
        }
        SpSpace* sp_p = new SpSpace(name, *domain, dim, 1, 0);
        space_p = static_cast<Space*>(sp_p);        //cast SpSpace* to Space*
        spaceInfo_p = sp_p->spaceInfo_p;            //copy SpaceInfo pointer
        global=false;
        theSpaces.push_back(this);                  //store space in list of spaces
      }
      else // _dim is not used
      {
        // now, we have to deal with sets of keys about definition of FESpace
        // _domain, _interpolation
        // _domain, _FE_type, order[, _FE_subtype][, _Sobolev_type]
        // Optional keys: _withLocateData, _withoutLocateData, _optimizeNumbering, _notOptimizeNumbering
        if (usedParams.find(_pk_interpolation) != usedParams.end())
        {
          if (usedParams.find(_pk_domain) == usedParams.end()) { error("param_missing","_domain"); }
          if (interpolation_p == nullptr) { interpolation_p=findInterpolation(interpolationType, domain->dim()); }
        }
        else
        {
          if (usedParams.find(_pk_domain) == usedParams.end()) { error("param_missing","_domain"); }
          if (usedParams.find(_pk_FE_type) == usedParams.end()) { error("param_missing","_FE_type"); }
          if (usedParams.find(_pk_order) == usedParams.end()) { error("param_missing","_order"); }
          switch (feType)
          {
            case _Lagrange:
            {
              if (usedParams.find(_pk_FE_subtype) == usedParams.end()) { feSubtype=_standard; }
              if (usedParams.find(_pk_Sobolev_type) == usedParams.end()) { sobolevType=_H1; }
              break;
            }
            case _CrouzeixRaviart:
            {
              if (usedParams.find(_pk_FE_subtype) == usedParams.end()) { feSubtype=_standard; }
              if (usedParams.find(_pk_Sobolev_type) == usedParams.end()) { sobolevType=_H1; }
              break;
            }
            case _RaviartThomas:
            case _NedelecFace:
            case _BuffaChristiansen:
            {
              if (usedParams.find(_pk_FE_subtype) == usedParams.end()) { feSubtype=_standard; }
              if (usedParams.find(_pk_Sobolev_type) == usedParams.end()) { sobolevType=_Hdiv; }
              break;
            }
            case _Nedelec:
            case _NedelecEdge:
            {
              if (usedParams.find(_pk_FE_subtype) == usedParams.end()) { feSubtype=_firstFamily; }
              if (usedParams.find(_pk_Sobolev_type) == usedParams.end()) { sobolevType=_Hcurl; }
              break;
            }
            case _Morley:
            {
              if (usedParams.find(_pk_FE_subtype) == usedParams.end()) { feSubtype=_standard; }
              if (usedParams.find(_pk_Sobolev_type) == usedParams.end()) { sobolevType=_H1; }
              break;
            }
            case _Argyris:
            {
              if (!isTestMode) error("free_error","development of Argyris finite element is still under progress");
              if (usedParams.find(_pk_FE_subtype) == usedParams.end()) { feSubtype=_standard; }
              if (usedParams.find(_pk_Sobolev_type) == usedParams.end()) { sobolevType=_H2; }
              break;
            }
            default:
              where("Space::Space(key1=val1, ...");
              error("bad_interp_type", feType);
          }
          interpolation_p=findInterpolation(feType, feSubtype, order, sobolevType);
        }
        FeSpace* sp_p = new FeSpace(*domain, *interpolation_p, name, optimizeNumbering, withLocateData); // create FeSpace
        space_p = static_cast<Space*>(sp_p);           // cast FeSpace* to Space*
        spaceInfo_p = sp_p->spaceInfo_p;               // copy SpaceInfo pointer
        global=false;
        theSpaces.push_back(this);                     // store space in list of spaces
      }
    }
  }
}

Space::Space(const Parameter& p1)
{
  std::vector<Parameter> ps(1, p1);
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2)
{
  std::vector<Parameter> ps(2);
  ps[0]=p1; ps[1]=p2;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3)
{
  std::vector<Parameter> ps(3);
  ps[0]=p1; ps[1]=p2; ps[2]=p3;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4)
{
  std::vector<Parameter> ps(4);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5)
{
  std::vector<Parameter> ps(5);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6)
{
  std::vector<Parameter> ps(6);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6, const Parameter& p7)
{
  std::vector<Parameter> ps(7);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6, const Parameter& p7, const Parameter& p8)
{
  std::vector<Parameter> ps(8);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7; ps[7]=p8;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9)
{
  std::vector<Parameter> ps(9);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7; ps[7]=p8; ps[8]=p9;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10)
{
  std::vector<Parameter> ps(10);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7; ps[7]=p8; ps[8]=p9; ps[9]=p10;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10,
             const Parameter& p11)
{
  std::vector<Parameter> ps(11);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7; ps[7]=p8; ps[8]=p9; ps[9]=p10;
  ps[10]=p11;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10,
             const Parameter& p11, const Parameter& p12)
{
  std::vector<Parameter> ps(12);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7; ps[7]=p8; ps[8]=p9;
  ps[9]=p10; ps[10]=p11; ps[11]=p12;
  buildSpace(ps);
}

Space::Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
             const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10,
             const Parameter& p11, const Parameter& p12, const Parameter& p13)
{
  std::vector<Parameter> ps(13);
  ps[0]=p1; ps[1]=p2; ps[2]=p3; ps[3]=p4; ps[4]=p5; ps[5]=p6; ps[6]=p7; ps[7]=p8; ps[8]=p9;
  ps[9]=p10; ps[10]=p11; ps[11]=p12; ps[12]=p13;
  buildSpace(ps);
}

// build a spectral space from analytic functions (Function)
void Space::buildSpFun(const GeomDomain& dom, const Function& f, number_t nbFun, dimen_t dimFun, const string_t& na)
{
  SpectralBasisFun* sb_p = new SpectralBasisFun(dom, f, nbFun);  //build functions basis
  SpSpace* sp_p = new SpSpace(na, dom, nbFun, dimFun, static_cast<SpectralBasis*>(sb_p));
  space_p = static_cast<Space*>(sp_p);        //cast SpSpace* to Space*
  spaceInfo_p = sp_p->spaceInfo_p;            //copy SpaceInfo pointer
  global=false;
  theSpaces.push_back(this);                  //store space in list of spaces
}

// constructors of space from spectral basis
Space::Space(const SpectralBasis& spb, const string_t& na)
{
  SpectralBasis* sb_p = spb.clone();       //deep copy of basis
  SpSpace* sp_p = new SpSpace(na, sb_p->domain(), sb_p->numberOfFun(), sb_p->dimFun(), static_cast<SpectralBasis*>(sb_p));
  space_p = static_cast<Space*>(sp_p);        //cast SpSpace* to Space*
  spaceInfo_p = sp_p->spaceInfo_p;            //copy SpaceInfo pointer
  global=false;
  theSpaces.push_back(this);                  //store space in list of spaces
}

/*!
    constructor of Fe space (from domain and interpolation)
    if opt=true, dof numbering is optimized to reduce the matrix bandwith
*/
Space::Space(const GeomDomain& dom, const Interpolation& in, const string_t& na, bool opt)
{
  FeSpace* sp_p = new FeSpace(dom, in, na, opt); // create FeSpace
  space_p = static_cast<Space*>(sp_p);           // cast FeSpace* to Space*
  spaceInfo_p = sp_p->spaceInfo_p;               // copy SpaceInfo pointer
  global=false;
  theSpaces.push_back(this);                     // store space in list of spaces
}

//constructor of subspace from domain and parent space
Space::Space(const GeomDomain& dom, Space& sp, const string_t& na)
{
  SubSpace* sp_p = createSubSpace(dom, sp, na); // create subspace
  space_p = static_cast<Space*>(sp_p);          // cast SubSpace* to Space*
  spaceInfo_p = sp_p->spaceInfo_p;              // copy SpaceInfo pointer
  global=false;
  theSpaces.push_back(this);                    // store space in list of spaces
}

/*! constructor of subspace from domain, dof numbers list and parent space
    dofids is the list of dof number ids linked to domain g
    this constructor is usefull in case of subspace of subspace
*/
Space::Space(const GeomDomain& dom, const std::vector<number_t>& dofids, Space& sp, const string_t& na)
{
  SubSpace* sp_p = new SubSpace(dom, dofids, sp, na); // create subspace
  space_p = static_cast<Space*>(sp_p);                // cast SubSpace* to Space*
  spaceInfo_p = sp_p->spaceInfo_p;                    // copy SpaceInfo pointer
  global=false;
  theSpaces.push_back(this);                          // store space in list of spaces
}

//no domain (case of abstract space)
Space::Space(const std::vector<number_t>& dofids, Space& sp, const string_t& na)
{
  SubSpace* sp_p = new SubSpace(dofids, sp, na); //create subspace
  space_p = static_cast<Space*>(sp_p);        //cast SubSpace* to Space*
  spaceInfo_p = sp_p->spaceInfo_p;            //copy SpaceInfo pointer
  global=false;
  theSpaces.push_back(this);                  //store space in list of spaces
}

//create a new subspace
SubSpace* Space::createSubSpace(const GeomDomain& dom, Space& sp, const string_t& na)
{
  SubSpace* sp_p = nullptr;
  if(dom.domType() == _meshDomain && sp.typeOfSpace() == _feSpace)  // create a FeSubspace (standard case)
    { sp_p = new FeSubSpace(dom, sp, na); return sp_p; }
  if(dom.domType() == _meshDomain && sp.isFE())                     // create a FeSubspace of a Subspace (more intricate)
    { sp_p = new FeSubSpace(dom, sp, true, na); return sp_p; }
  sp_p = new SubSpace(dom, sp, na);                                 // create a SubSpace
  return sp_p;
}

//create subspaces of current space from a list of subdomains, return list of spaces (subspaces)
void Space::createSubSpaces(const std::vector<const GeomDomain*>& doms, std::vector<Space*>& subspaces)
{
  subspaces.resize(doms.size(), 0);
  for(number_t n = 0; n < doms.size(); n++)  //travel list of domains
    {
      if(doms[n] != domain())
        {
          subspaces[n] = Space::findSubSpace(doms[n], this);                    //subspace already exist
          if(subspaces[n] == nullptr) { subspaces[n] = new Space(*doms[n], *this); }  //create subspace
        }
      else { subspaces[n] = this; }
    }
}

/*! build subspaces of current space from a list of subdomains, return list of spaces (subspaces) and returns the largest space (pointer to)
  this function:
   - travel along domains
   - identify the largest geometric domain
   - build the largest subspace
   - create subspace related to each domain if it does not exist
   - create dofs numbering between domain subspaces ands largest subspace

 dofs numbering:
    whole space     largest subspace    domain subspace
            1               5                2
            2               3                1
            3               1
            4
            5
*/
Space* Space::buildSubspaces(const std::vector<const GeomDomain*>& doms, std::vector<Space*>& subspaces)
{
  trace_p->push("Space::buildSubspaces");
  //case of not a FeSpace
  number_t nbd = doms.size();             //number of domains
  if(nbd == 0) return nullptr;                //no subspace
  if(typeOfSpace() != _feSpace)
    {
      subspaces.resize(nbd, this);      //all the subspaces are the current space
      trace_p->pop();
      return this;
    }

  // temporally change domains : replace all sidesdomain's by their parents
  // for sidesdomain, subspace will be not the real subspace but its volumic extension
  std::vector<const GeomDomain*> tdoms(doms);
  for(number_t k=0;k<tdoms.size();++k)
    if(tdoms[k]->meshDomain()->isSidesOf_p!=nullptr) tdoms[k]=tdoms[k]->meshDomain()->isSidesOf_p;

  //identify largest domain and create a new one if necessary
  const GeomDomain* wholeDomain = domain();    //mesh domain of the whole space
  const GeomDomain* largestdomain = wholeDomain->meshDomain()->largestDomain(tdoms);

  // deal with composite domain - EXPERIMENTAL -
  if(largestdomain->domType() != _meshDomain)
  {
    if(largestdomain->domType()==_compositeDomain)
    {
       const CompositeDomain* cdom = largestdomain->compositeDomain(); //case of a union of domains
       if(cdom->isUnion()) //merge domains
       {
          largestdomain=const_cast<const GeomDomain*>(&merge(cdom->domains(),cdom->name()));
       }
    }
    else  largestdomain=wholeDomain;
  }

  //create largest domain
  Space* sp = this;
  if(largestdomain != wholeDomain && largestdomain != wholeDomain->meshDomain())
  {
    sp = Space::findSubSpace(largestdomain, this);
    if(sp == nullptr) sp = new Space(*largestdomain, *this);
  }

  //create subspaces or find them
  subspaces.resize(nbd, 0);
  sp->createSubSpaces(tdoms, subspaces);
  //end of buildSubspaces
  trace_p->pop();
  return sp;
}

//destructor
Space::~Space()
{
  if(!global && space_p != this)   //destroy "child"
  {
    //before destroying this space, destroy subspaces that have this space as parent
    std::vector<Space*> childs=findSubSpaces(this);
    std::vector<Space*>::iterator itc=childs.begin();
    for(; itc!=childs.end(); itc++)
       if(*itc!=nullptr) delete *itc;
    clearProjectors(*space_p);//clear projectors that depends on space space_p
    clearTerms(*space_p);     //clear terms that depends on space space_p
    clearStorages(*space_p);  //clear storages that depends on space space_p
    clearUnknowns(*space_p);  //clear unknown that depends on space space_p
    delete space_p;          //destroy space_p
    clearProjectors(*this);//clear projectors that depends on current space
    clearTerms(*this);     //clear terms that depends on current space
    clearStorages(*this);  //clear storages that depends on current space
    clearUnknowns(*this);  //clear unknown that depends on current space
    if(spaceInfo_p != nullptr) delete spaceInfo_p;
  }
  //remove space from spaces list
  std::vector<Space*>::iterator it = find(theSpaces.begin(), theSpaces.end(), this);
  if(it != theSpaces.end()) { theSpaces.erase(it);}
}

// delete all space objects
void Space::clearGlobalVector()
{
  while(Space::theSpaces.size() > 0) { delete Space::theSpaces[0];}
}

//in case of space, return spaceType_
//in case of subspace, return the spaceType of the first parent (recursive)
SpaceType Space::typeOfSubSpace() const
{
  if(typeOfSpace() != _subSpace) { return typeOfSpace(); }
  return subSpace()->parent()->typeOfSubSpace();
}

//access pointer to FeSpace
const FeSpace* Space::feSpace() const
{
  if(space_p != this) { return space_p->feSpace(); }  //call with the true space
  return nullptr;
}

//access pointer to FeSpace
FeSpace* Space::feSpace()
{
  if(space_p != this) { return space_p->feSpace(); }  //call with the true space
  return nullptr;
}

//access pointer to FeSubSpace
const FeSubSpace* Space::feSubSpace() const
{
  if(space_p != this) { return space_p->feSubSpace(); }  //call with the true space
  return nullptr;
}


//access pointer to SpSpace
const SpSpace* Space::spSpace() const
{
  if(space_p != this) { return space_p->spSpace(); }  //call with the true space
  return nullptr;
}

//access pointer to SubSpace
const SubSpace* Space::subSpace() const
{
  if(space_p!=nullptr && space_p != this) { return space_p->subSpace(); }  //call with the true space (const)
  return nullptr;
}

SubSpace* Space::subSpace()
{
  if(space_p!=nullptr && space_p != this) { return space_p->subSpace(); }  //call with the true space
  return nullptr;
}

const Space* Space::rootSpace() const                   // access to root space pointer
{
  if(space_p!=nullptr && space_p != this) { return space_p->rootSpace(); }  //call with the true space
  return this;
}

Space* Space::rootSpace()                   // access to root space pointer
{
  if(space_p!=nullptr && space_p != this) { return space_p->rootSpace(); }  //call with the true space
  return this;
}

ValueType Space::valueType() const                          // return value type of basis function (real or complex)
{
  if(space_p != this) { return space_p->valueType();}
  return _real;
}

StrucType Space::strucType() const                         // return structure type of basis function (scalar or vector)
{
  if(space_p != this) { return space_p->strucType();}
  return _scalar;
}

//check type of Space
bool Space::isSpectral() const
{
  if(space_p != this) { return space_p->isSpectral();}
  return false;
}

bool Space::isFE() const
{
  if(space_p != this) { return space_p->isFE();}
  return false;
}

bool Space::extensionRequired() const
{
  if(space_p != this) { return space_p->extensionRequired();}
  return false;
}

number_t Space::dofId(number_t n) const                     //return the DoF id of n-th space DoF
{
  if(space_p != this) { return space_p->dofId(n); }     //call with the true space
  error("not_handled", "Space::dofId");
  return 0;
}

const Dof& Space::dof(number_t n) const                    // return n-th dof (n=1,...)
{
  if(space_p != this) { return space_p->dof(n); }        //call with the true space
  error("not_handled", "Space::dof");
  return *(new Dof());   //fake return
}

// return the DoF ids on space
std::vector<number_t> Space::dofIds() const
{
  if(space_p != this) {return space_p->dofIds();}      //call with the true space
  error("not_handled", "Space::dofIds");
  return std::vector<number_t>(0);  //fake return
}

// return true if (sub)space is included in current space
bool Space::include(const Space* sp) const
{
  if(space_p != this) { return space_p->include(sp); }        //call with the true space
  return false;
}

//find space by name
Space* Space::findSpace(const string_t& na)
{
  std::vector<Space*>::iterator it;
  for(it = theSpaces.begin(); it != theSpaces.end(); it++)
    if((*it)->name()==na) return *it;
  return nullptr;
}

//find subspace linked to a domain
Space* Space::findSubSpace(const GeomDomain* dom, Space* sp)
{
  if (sp->domain() == dom) return sp;
  std::vector<Space*>::iterator it;
  for (it = theSpaces.begin(); it != theSpaces.end(); it++)
  {
    Space* spit = *it;
    if (spit != nullptr && spit->typeOfSpace() == _subSpace && spit->domain() == dom && spit->subSpace()->parent() == sp)
    { return spit; }
  }
  return nullptr;   //not found
}

//find subspace linked to a domain
const Space* Space::findSubSpace(const GeomDomain* dom, const Space* sp)
{
  if (sp->domain() == dom) return sp;
  std::vector<Space*>::iterator it;
  for (it = theSpaces.begin(); it != theSpaces.end(); it++)
  {
    Space* spit = *it;
    if (spit != nullptr && spit->typeOfSpace() == _subSpace && spit->domain() == dom && spit->subSpace()->parent() == sp)
    { return spit; }
  }
  return nullptr;   //not found
}

//find subspaces linked to a space
std::vector<Space*> Space::findSubSpaces(Space* sp)
{
  std::vector<Space*> subs;
  std::vector<Space*>::iterator it;
  for(it = theSpaces.begin(); it != theSpaces.end(); it++)
    {
      Space* spit = *it;
      if(spit != nullptr && spit->typeOfSpace() == _subSpace && spit->subSpace()!=nullptr)
        if(spit->subSpace()->parent() == sp) { subs.push_back(spit); }
    }
  return subs;
}

//find subspaces linked to a space
std::vector<Space*> Space::findSubSpaces(const Space* sp)
{
  std::vector<Space*> subs;
  std::vector<Space*>::iterator it;
  for(it = theSpaces.begin(); it != theSpaces.end(); it++)
    {
      Space* spit = *it;
      if(spit != nullptr && spit->typeOfSpace() == _subSpace && spit->subSpace()->parent() == sp)
        { subs.push_back(spit); }
    }
  return subs;
}

//output all spaces in memory
void Space::printAllSpaces(std::ostream& out)
{
  std::vector<Space*>::iterator it;
  out << "Spaces in memory:\n";
  for(it = theSpaces.begin(); it != theSpaces.end(); it++) out << "  " << (*it) << ": " << (**it) << eol;
}

//return the dimension of space (virtual form)
number_t Space::dimSpace() const
{return space_p->dimSpace();}

//return the dimension of space (virtual form)
number_t Space::nbDofs() const
{return space_p->nbDofs();}

// optimize dofs numbering to reduce matrix bandwith, return skyline storage size before and after, (0,0) if no renumbering
std::pair<number_t, number_t> Space::renumberDofs()
{
  if(space_p != this) return space_p->renumberDofs(); //call with the true space
  warning("not_handled", "Space::renumberDofs");
  return std::pair<number_t, number_t>(0, 0);
}

//print general space attributes
void Space::printSpaceInfo(std::ostream& out) const
{
  string_t dn="?";
  if(spaceInfo_p->domain_p!=nullptr)
  {
    if(GeomDomain::findDomain(spaceInfo_p->domain_p->domainp())!=nullptr)  // domain_p may be deallocated, look in GeomDomains::theDomains
      dn=spaceInfo_p->domain_p->name();                              // that stores child of GeomDomains
  }
  if(theVerboseLevel > 0)
    out << message("space_def", spaceInfo_p->name, words("space", spaceInfo_p->spaceType),dn);
  if(theVerboseLevel == 1) return;
  out << eol << message("space_def+", spaceInfo_p->dimFun, words("Sobolev", spaceInfo_p->spaceConforming), dimSpace());
}

//print specific space attributes (default when not defined in children)
void Space::print(std::ostream& out) const
{
  if(space_p != this) { space_p->print(out); }   //call with the true space
  else { printSpaceInfo(out); }                  //print spaceinfo
}

//output a Space to a stream
std::ostream& operator<< (std::ostream& out, const Space& sp)
{
  sp.print(out);
  return out;
}

//compare spaces using their size s1<s2 if size(s1)>size(s2)
//used in construction of largest subspace
bool compareSpaceUsingSize(const Space* sp1, const Space* sp2)
{
  return sp1->dimSpace() > sp2->dimSpace();
}

/*!renumber dofs of a space (sp2) along dof numbering of space (sp1)
 if the spaces are the same nothing is done: the result is of size 0
 if the spaces are not the same: rn is the renumbering vector
 if sp2 is larger than sp1 : exemple
        sp1 = 8 4 5 1 4 7 6
        sp2 = 3 5 7 4
        rn  = 0 3 6 5
   0 means that 3 does not belong to sp1
   3 means that 5 is the third element of sp1
   ...
if sp1 is larger than sp2 : exemple
        sp1 = 4 3 6 8
        sp2 = 1 2 3 4 5 6 7 8 9
        rn  = 0 0 2 1 0 3 0 8 0
   0 means that 1,2,5, ... does not belong to sp1
   2 means that 3 is the second element of sp1
   ...
*/
std::vector<number_t> renumber(const Space* sp1, const Space* sp2)
{
  std::vector<number_t> dofrenum;
  if(sp1 == sp2) return dofrenum; //nothing to do

  trace_p->push("renumber(Space*,Space*)");
  const Space* rs1 = sp1->rootSpace();
  const Space* rs2 = sp2->rootSpace();
  if(rs1 != rs2) error("diff_rootspaces");
  SpaceType st1 = sp1->typeOfSpace(), st2 = sp2->typeOfSpace();

  //create a map of dof root numbers of sp1 : dof root number -> rank in dof number
  std::map<number_t, number_t> dofrank1;
  if(st1 == _subSpace)
    {
      const SubSpace* subsp1 = sp1->subSpace();
      const std::vector<number_t>& dofnum1 = subsp1->dofNumbers();
      for(number_t k=1; k<=dofnum1.size(); k++) dofrank1[subsp1->dofRootNumber(k)] = k;

    }
  else //trivial map
    {
      for(number_t k = 1; k <= sp1->dimSpace(); k++) dofrank1[k] = k;
    }

  //travel dofs sp2
  if(st2 == _subSpace)
    {
      const SubSpace* subsp2 = sp2->subSpace();
      const std::vector<number_t>& dofnum2 = subsp2->dofNumbers();
      dofrenum.resize(dofnum2.size());
      std::vector<number_t>::iterator itrn = dofrenum.begin();
      std::map<number_t, number_t>::iterator itm, ite = dofrank1.end();
      for(number_t k=1; k<=dofnum2.size(); k++, itrn++)
        {
          *itrn = 0;
          itm = dofrank1.find(subsp2->dofRootNumber(k));
          if(itm != ite) *itrn = itm->second;
        }
    }
  else
    {
      dofrenum.resize(sp2->dimSpace());
      std::vector<number_t>::iterator itrn = dofrenum.begin();
      std::map<number_t, number_t>::iterator itm, ite = dofrank1.end();
      for(number_t k = 1; k <= sp2->dimSpace(); k++, itrn++)
        {
          *itrn = 0;
          itm = dofrank1.find(k);
          if(itm != ite) *itrn = itm->second;
        }
    }

  //check if renumbering is not a trivial one
  std::vector<number_t>::iterator itrn = dofrenum.begin();
  number_t k = 1;
  while(itrn != dofrenum.end() && *itrn == k) {k++; itrn++;}
  if(itrn == dofrenum.end() && sp1->dimSpace()==sp2->dimSpace())
    dofrenum.clear(); //trivial renumbering

  trace_p->pop();
  return dofrenum;
}

//!< return sub-space or trace space on domain of a space, created if not exist
Space& subSpace(Space& sp, const GeomDomain& dom)
{
  Space* ssp=Space::findSubSpace(&dom, &sp);
  if (ssp==nullptr) ssp = new Space(dom,sp);
  return *ssp;
}

/*! merge subspaces given by a list of Space pointer
    the union is allowed only for subspaces of same root space
    this function may return the root space, one of the subspaces or a new subspace
    if newSubspaces is true,  new subspaces with union as parent are created and return as space pointers
*/
Space* mergeSubspaces(Space*& sp1, Space*& sp2, bool newSubspaces)
{
  std::vector<Space*> sps(2);
  sps[0] = sp1; sps[1] = sp2;
  Space* spu = mergeSubspaces(sps, newSubspaces);
  if (newSubspaces) {sp1 = sps[0]; sp2 = sps[1];}
  return spu;
}

Space* mergeSubspaces(std::vector<Space*>& sps, bool newSubspaces)
{
  trace_p->push("mergeSubspaces(vector<Space*>)");
  Space* spl = unionOf(sps);
  if(newSubspaces) //create new subspaces if required
    {
      std::vector<Space*>::iterator its = sps.begin();
      for(its = sps.begin(); its != sps.end(); its++) //create new subspace of union if parent space is not spu
        {
          Space* sp = *its;
          if(sp != spl && sp->typeOfSpace() == _subSpace && sp->subSpace()->parent() != spl) //create a new subspace with spl as parent
            {
              if(sp->domain()!=nullptr)
                *its = new Space(*sp->domain(), sp->subSpace()->dofNumbers(), *spl);
              else
                *its = new Space(sp->subSpace()->dofNumbers(), *spl);  //no domain defined
            }
        }
    }
  trace_p->pop();
  return spl;
}

/*! union of subspaces
    the result, returned as space pointer, may be
     - one subspace of the list if it includes all others
     - the root space if the union gives the whole space
     - a new subspace
*/
Space* unionOf(std::vector<Space*>& sps)
{
  trace_p->push("unionOf(vector<Space*>)");
  std::vector<Space*>::iterator its = sps.begin();
  Space* spu = (*its)->rootSpace(), *spl = *its; its++;
  for (; its != sps.end(); its++)
  {
    Space* sp = *its;
    if(sp->rootSpace() != spu) error("diff_rootspaces", "in mergeSubspaces(vector<Space*>), subspaces have not the same rootspace");
    if(sp->dimSpace() > spl->dimSpace()) spl = sp;
  }
  if(spl == spu) {trace_p->pop(); return spl;} //the root space is in list of spaces

  // at this stage all spaces are subspaces
  bool included = true;
  its = sps.begin();
  while (its != sps.end() && included)
  {
    Space* sp = *its;
    included = spl->include(sp);
    its++;
  }
  if (included) {trace_p->pop(); return spl;} //the largest space contains all others

  //construct a new subspace or identify an existing one
  //two different cases are handled along there is domains defined or not
  Space* spun = nullptr;
  bool withdomain = true;
  for (its = sps.begin(); its != sps.end() && withdomain; its++)
    withdomain = (*its)->domain()!=nullptr;

  if (withdomain) //all subspaces have domain
  {
    std::vector<const GeomDomain*> doms;
    string_t na = "";
    for (its = sps.begin(); its != sps.end(); its++)
    {
      const GeomDomain* dom = (*its)->domain();
      if (dom == nullptr) error("null_pointer", "GeomDomain");
      switch (dom->domType())
        {
        case _meshDomain: doms.push_back(dom); break;
        case _compositeDomain:
        {
          std::vector<const GeomDomain*> cdoms = dom->compositeDomain()->basicDomains();
          doms.insert(doms.end(), cdoms.begin(), cdoms.end());
          break;
        }
        default: error("domain_notmeshorcomposite", dom->name(), dom->domType());
      }
    }
    // check if the union of domains subspace already exists
    //const GeomDomain* dom = geomUnionOf(lsd);
    const GeomDomain* dom = geomUnionOf(doms,spu->domain());
    spun = Space::findSubSpace(dom, spu);
    if(spun == nullptr) spun= new Space(*dom, *spu); // create new subspace
  }
  else //no domain subspace construction, use only dofs
  {
    std::set<number_t> alldofs;
    for (its = sps.begin(); its != sps.end(); its++)
    {
      std::vector<number_t> dofs=(*its)->dofIds();
      alldofs.insert(dofs.begin(),dofs.end());
    }
    std::vector<number_t> vdofs(alldofs.begin(), alldofs.end());
    spun = new Space(vdofs, *spu); // create new subspace
  }

  trace_p->pop();
  return spun;
}


//list of coordinates of ponctual dofs of Space on a given domain, return a void list if there is no ponctual dof
std::vector<Point> dofCoords(Space& sp, const GeomDomain& dom)
{
  std::vector<Point> pts;
  if(sp.isFE())
    {
      Space* subsp=Space::findSubSpace(&dom,&sp);
      if(subsp==nullptr) subsp=new Space(dom, sp, sp.name()+"_"+dom.name());
      number_t n=subsp->nbDofs();
      pts.reserve(n);
      number_t k=0;
      for(number_t i=1; i<=n; i++)
        if(subsp->dof(i).isPonctual()) {k++; pts.push_back(subsp->dof(i).coords());}
      pts.resize(k);
      return pts;
    }

  where("dofCoords");
  error("not_fe_space_type",sp.name());
  return pts;
}

//==================================================================================================
//  Member functions of SpaceMap
//==================================================================================================

/*! build the numbering vector using the L2 projection operator P=inv(M_2)*M_21 where
     - M2  is the TermMatrix associated to the bilinear form intg(dom,u2|v2)
     - M21 is the TermMatrix associated to the bilinear form intg(dom,u1|v2)
     when V1|dom and V2|dom are trace spaces of same FE interpolation, P is the permutation matrix related
     to the numbering V1|dom -> V2|dom
*/
void SpaceMap::buildMap()
{
  // check consistency
  const Interpolation* int1=V1_p->interpolation(), *int2=V2_p->interpolation();
  if(int1!=int2)
    {
      if(int1->type!=int2->type || int1->subtype!=int2->subtype ||
          int1->numtype!=int2->numtype || int1->conformSpace!=int2->conformSpace)
        error("free_error","in SpaceMap construction, "+V1_p->name()+" and "+V2_p->name()+" do not have the same interpolation");
    }
  if(V1_p->domain()->mesh()!=V2_p->domain()->mesh() || V1_p->domain()->mesh()!=domain_p->mesh())
    error("free_error","in SpaceMap construction, "+V1_p->name()+" and "+V2_p->name()+" and "+domain_p->name()+" are not defined on the same mesh");
  if(!V1_p->isFE())
    error("free_error","in Space Map construction, "+V1_p->name()+" is not a FE space");
  if(domain_p->meshDomain()==nullptr)
    error("free_error","in Space Map construction, "+domain_p->name()+" is not a Mesh domain");
  if(!domain_p->meshDomain()->isInterface())
    error("free_error","in Space Map construction, "+domain_p->name()+" is not an interface");

  trace_p->push("SpaceMap::buildMap()");

  //create temporary unknowns and test functions
  Unknown u1("u1", const_cast<Space *>(V1_p), 1, false, 0, 1, true),
          u2("u2", const_cast<Space *>(V2_p), 1, false, 0, 1, true);
  TestFunction v1("v1", const_cast<Space *>(V1_p), 1, false, &u1, 1, false),
               v2("v2", const_cast<Space *>(V2_p), 1, false, &u2, 1, false);

  //create matrix and factorize
  TermMatrix M2(intg(*domain_p,u2|v2)), M21(intg(*domain_p,u1|v2));
  ldltFactorize(M2,M2);

  //solve and build dofMap
  number_t m = M21.numberOfCols();
  dofMap.resize(m);
  for(number_t c=1; c<=m; c++)
    {
      Vector<real_t> x=factSolve(M2,M21.column(c)).roundToZero().asRealVector();
      number_t nnz=0, cmax=0;
      real_t vmax=0., v;
      for(number_t i=0; i<x.size(); i++)
        {
          v=std::abs(x[i]);
          if(v> 10*theEpsilon) nnz++;
          if(v>vmax) {cmax=i; vmax=v;}
        }
      if(nnz>1) warning("free_warning", "in Space Map construction, "+tostring(nnz)+" non zero coefficients in a permutation column ");
      dofMap[c-1]=cmax;
    }

  trace_p->pop();
  return;
}

//! print SpaceMap
void SpaceMap::print(std::ostream& os) const
{
  if(theVerboseLevel==0) return;
  os<<"space map "<<V1_p->name()<<"|"<<domain_p->name()<<" -> "<<V2_p->name()<<"|"<<domain_p->name()<<eol;
  number_t n=dofMap.size();
  if(theVerboseLevel==1 || n==0) return;
  std::vector<number_t>::const_iterator it=dofMap.begin();
  os<<" map = [";
  if(n <= 2*theVerboseLevel+5)
    {
      os<<*it++;
      for(; it!=dofMap.end(); ++it)  os<<", "<<*it;
    }
  else
    {
      os<<*it++;
      number_t k=0;
      for(; k<theVerboseLevel; ++it, k++)  os<<", "<<*it;
      os<<", ...";
      it=dofMap.begin()+ (n-theVerboseLevel);
      k=0;
      for(; k<theVerboseLevel; ++it, k++)  os<<", "<<*it;
    }
  os<<"]"<<eol;
  return;
}


} // end of namespace xlifepp



