/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FeSpace.cpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 31 jul 2012

  \brief Implementation of xlifepp::FeSpace class members and functionalities
*/

#include "FeSpace.hpp"
#include "SubSpace.hpp"
#include "BCElement.hpp"

namespace xlifepp
{

//constructor of Fe type space from a MeshDomain and an Interpolation
//it is a whole space that is the largest space based on the computational domain
//if the GeomDomain is a part of the whole domain, in particular a boundary of it
//only dofs on the part domain will be defined !
//when opt is true the dofs are renumbered to minimize bandwith

FeSpace::FeSpace(const GeomDomain& gd, const Interpolation& in, const string_t& na, bool opt, bool withLocateData)
{
  trace_p->push("FeSpace::constructor");

  if (gd.domType() != _meshDomain)
  {
    error("domain_notmesh", gd.name(), words("domain type", gd.domType()));  //works only for meshdomain
  }
  dimen_t dF = 1;
  if (!in.isScalar()) dF = gd.dim(); //dimension of vector basis functions is the domain dimension
  space_p = this;
  spaceInfo_p = new SpaceInfo(na, &gd, in.conformSpace, dF, _feSpace);
  interpolation_p = &in;
  FeSpace::lastEltIndex = 0; //reset to 0 the lastEltIndex
  const MeshDomain* md=gd.meshDomain();
  if (withLocateData) { if (md->kdtree.isVoid()) md->buildKdTree(); }
  if (!md->orientationComputed) md->buildGeomData();  //construct measures and orientation of each element of domain
  switch (in.type)
  {
    case _BuffaChristiansen: // special case
    {
      buildBCRTSpace();
      optimizedNumbering_ = false;  //no numbering optimisation
      global=false;
      break;
    }
    default:  // standard case
    {
      buildElements();           //construct the list of elements
      buildFeDofs();             //construct the list of dofs
      optimizedNumbering_ = false;
      if (opt)
      {
        std::pair<number_t, number_t> skys = renumberDofs(); //renumber dofs to minimize bandwith
        if (theVerboseLevel > 0) info("fespace_dof_renumbering", na, skys.first, skys.second);
      }
      optimizedNumbering_ = opt;
      global=false;
    }
  }
  trace_p->pop();
}

//==========================================================================================================
//Eric's note
//   when a FeSpace is constructed from geometric elements which are MeshSideElement's
//   that is the case of FeSpace defined from a boundary of a mesh,
//   it will be probably better to construct a true MeshElement's because it carries all the informations
//   that is not the case of MeshSideElement which carries only parent element and a side number
//   leading to more intricate code ...
//==========================================================================================================

//buildElements: create Finite Element space elements from GeomDomain element
void FeSpace::buildElements()
{
  trace_p->push("FeSpace::buildElements");
  const MeshDomain* mdom = domain()->meshDomain();
  RefElement* re_p;
  number_t nelt = FeSpace::lastEltIndex;
  std::vector<GeomElement*>::const_iterator itGe;
  elements.reserve(mdom->geomElements.size());

  if (mdom->isSideDomain())
    //sort geomElement according to the rank of its first parents number and sidenumber
    //in order to preserve order of parent elements
  {
    std::map<std::pair<number_t, number_t>,GeomElement*> index;
    for (itGe = mdom->geomElements.begin(); itGe != mdom->geomElements.end(); itGe++)
    {
      number_t n=(*itGe)->parent()->number(), s=(*itGe)->parentSide(0).second;
      index[std::make_pair(n,s)]=*itGe;
    }
    std::map<std::pair<number_t, number_t>,GeomElement*>::iterator iti=index.begin();
    for (; iti!=index.end(); iti++)
    {
      re_p = findRefElement(iti->second->shapeType(), interpolation_p); // create reference element (if not exist) and element
      elements.push_back(Element(this, ++nelt, re_p, iti->second));
      if(refElts.find(re_p)==refElts.end()) refElts.insert(re_p);
    }
  }
  else //not a side domain
  {
    // create Element of FESpace from mesh geometric element and reference element
    for (itGe = mdom->geomElements.begin(); itGe != mdom->geomElements.end(); itGe++)
    {
      re_p = findRefElement((*itGe)->shapeType(), interpolation_p);  // create reference element (if not exist) and element
      elements.push_back(Element(this, ++nelt, re_p, *itGe));
      if(refElts.find(re_p)==refElts.end()) refElts.insert(re_p);
    }
  }

  FeSpace::lastEltIndex = nelt;  //update last element number in FeSpace
  trace_p->pop();
}

/*!buildFeDofs: create FeDofs of FeSpace defined by a mesh domain and an interpolation
algorithm:
   for Discontinuous Galerkin interpolation (_L2 space comforming), all local dofs are global dofs
   for "continuous" approximation with sharable dofs
     the algorithm travels the list of elements
     on each element, local dofs are indexed along a DofKey object wich uniquely determines a global dof:
        vertex dof: V + global mesh vertex number
        side dof:   S + ascending global mesh numbers of side vertices + local dof number
        side of side dof: E + "ascending" global mesh numbers of side vertices  + local dof number
        internal or not sharable dof: not indexed
     thus, a global dof is created from a local dof if it is not already indexed or if it is a non sharable dof

   NOTE: algorithm of this new version is more general than the previous one
            it works by using a loop on ref dofs of ref elements while
            the old one using a loop on vertices, edges, faces and non sharable dofs
            Now all the dof informations are provided by refdof and not by refdof and refelement
            that leads to a more general and compact algorithm, probably more efficient !
*/

void FeSpace::buildFeDofs()
{
  trace_p->push("FeSpace::buildFeDofs");

  //maximum size of dofs vector: sum of number of element dofs
  //in order to reserve enough memory to store dofs, avoiding additional cost in dofs.push_back command
  std::vector<Element>::iterator itElt;
  number_t maxnbdof=0;
  for(itElt = elements.begin(); itElt != elements.end(); itElt++) maxnbdof+=itElt->refElt_p->nbDofs();
  dofs.reserve(maxnbdof);
  number_t dofn = 1;         //dof's numbering starts at 1

  //Galerkin dicontinuous approximation, none sharable dofs, add all the local dofs
  if (conformingSpace() == _L2)
  {
    for (itElt = elements.begin(); itElt != elements.end(); itElt++)
      for (number_t i = 0; i < itElt->refElt_p->nbDofs(); i++)
      {
        dofs.push_back(FeDof(*this, itElt->refElt_p->refDofs[i], dofn));  //add new dof
        itElt->dofNumbers[i] = dofn;
        dofs.back().inElement(std::pair<Element*, number_t>(&(*itElt), i + 1)); // update dof inElements vector
        dofn++;
      }
    trace_p->pop();
    return;
  }

  //Continuous approximation, sharable dofs, see explanation above
  std::map<DofKey, number_t> dofIndex;        //global dof key -> global dof number
  std::map<DofKey, number_t>::iterator it_SF;

  //travel elements and create dof
  for (itElt = elements.begin(); itElt != elements.end(); itElt++)
  {
    RefElement* refElt = itElt->refElt_p;
    GeomElement* geoElt = itElt->geomElt_p;
    dimen_t dimElt = geoElt->elementDim();
    std::vector<DofKey> index(refElt->nbDofs());  //string dof index

    //travel refdofs of refelement
    std::vector<RefDof*>::const_iterator  itr=refElt->refDofs.begin(), itre=refElt->refDofs.end();
    number_t i=0;
    for (; itr!=itre; ++itr, ++i)
    {
      RefDof* rd=*itr;
      if (rd->sharable())
      {
        switch (rd->where())   //test hierarchic localization of refdof
        {
          case _onVertex: // encode as V , global_mesh_vertex_number, local dof number
            index[i] = DofKey(_onVertex, geoElt->vertexNumber(rd->supportNumber()),rd->index());
            break;
          case _onEdge: //encode as E, "ascending" global mesh numbers of side vertices , local dof number
          {
            std::vector<number_t> v;
            if (dimElt==2) v= geoElt->geomRefElement()->sideVertexNumbers()[rd->supportNumber()-1]; //local numbers of vertices on side k
            else v= geoElt->geomRefElement()->sideOfSideVertexNumbers()[rd->supportNumber()-1];
            number_t v0=geoElt->vertexNumber(v[0]), v1=geoElt->vertexNumber(v[1]);
            if (v1>v0) index[i] =DofKey(_onEdge,v0,v1,rd->index());
            else
            {
              if(dimElt==2) index[i] = DofKey(_onEdge,v1,v0,refElt->sideDofsMap(rd->index(),2,1)); //2d
              else  index[i] = DofKey(_onEdge,v1,v0,refElt->sideofsideDofsMap(rd->index(),2,1));   //3d
            }
            break;
          }
          case _onFace: //3d case, encode as F, ascending global mesh numbers of side vertices, local dof number"
          {
            std::vector<number_t> v = geoElt->geomRefElement()->sideVertexNumbers()[rd->supportNumber()-1]; //local numbers of vertices on side k
            std::map<number_t,number_t> ordv;  //small map to order vertex global numbers
            std::vector<number_t>::iterator itv=v.begin();
            number_t k=1;
            for (; itv!=v.end(); ++itv, ++k) ordv[geoElt->vertexNumber(*itv)]=k;
            std::map<number_t,number_t>::iterator ito = ordv.begin();
            std::vector<number_t> ip(v.size());
            std::vector<number_t> iv(v.size());
            std::vector<number_t>::iterator iti=ip.begin(), itj=iv.begin();
            for (; ito!=ordv.end(); ++ito,++iti, ++itj)
            {
              *itj=ito->first;
              *iti=ito->second;
            }
            index[i]=DofKey(_onFace, iv, refElt->sideDofsMap(rd->index(),ip[0],ip[1],ip[2]));
            break;
          }
          default: //_element (probably non sharable), error for the moment
            error("dof_unexpected");
          }
      }
      else index[i] = DofKey(_onElement,itElt->number(), 0, rd->index());  //non sharable
    }

    //create new dofs from unique index
    for (number_t i = 0; i < refElt->nbDofs(); i++)
    {
      DofKey& dki=index[i];
      it_SF = dofIndex.find(dki);
      if (it_SF == dofIndex.end())  //create new dof
      {
        DofLocalization whi=dki.where;
        bool sh = (whi != _onElement);
        dofs.push_back(FeDof(*this, refElt->refDofs[i], dofn, sh));   //create dof
        itElt->dofNumbers[i] = dofn;                                  //update dof numbering of current element
        dofIndex[dki] = dofn;                                         //add key in global dof map
        FeDof& fed=dofs.back();
        const RefDof* rd=fed.refDofP();
        fed.inElement(std::pair<Element*, number_t>(&(*itElt), i + 1)); //update dof inElements vector
        ProjectionType pt = fed.refDofP()->typeOfProjection();
        if (pt == _dotnProjection) //set reference normal vector
        {
          if ((dimElt==2 && whi !=_onEdge) || (dimElt==3 && whi !=_onFace)) error("dof_unexpected");
          fed.projVector()=geoElt->normalVector(rd->supportNumber());
          fed.projVector().normalize();
        }
        if (pt == _crossnProjection) //set reference tangential vector
        {
          if (whi == _onEdge)    //on an edge, take difference between vertices of the edge as tangent vector
          {
            number_t s=rd->supportNumber(), n1, n2;
            fed.projVector()=geoElt->tangentVector(s);
            const GeomRefElement* gr = geoElt->meshElement()->geomRefElement();
            if (dimElt==2)
            {
              n1=geoElt->vertexNumber(gr->sideVertexNumber(1, s));
              n2=geoElt->vertexNumber(gr->sideVertexNumber(2, s));
            }
            else
            {
              n1=geoElt->vertexNumber(gr->sideOfSideVertexNumber(1, s));
              n2=geoElt->vertexNumber(gr->sideOfSideVertexNumber(2, s));
            }
            if (n1>n2) fed.projVector()*=-1; //orient with ascending vertex numbers
            fed.projVector().normalize();
          }
          else error("dof_unexpected");
        }
        dofn++;
      }
      else //global dof already exists, update
      {
        itElt->dofNumbers[i] = it_SF->second;
        dofs[it_SF->second - 1].inElement(std::pair<Element*, number_t>(&(*itElt), i + 1)); //update dof inElements vector
      }
    }
  }//end of for(itElt= ...
  //in C+11+ dofs.shrink_to_fit(); will cause the extra memory reserved to dofs to be freed
  trace_p->pop();
}

//------------------------------------------------------------------------------------------------------
/*! buildBCRTSpace: special construction of BuffaChristiansen-RT space
    from F.P. Andriulli, K.f Cools, H. Bagci, F. Olyslager, A. Buffa, S. Christiansen and E. Michielssen
         A Multiplicative Calderon Preconditioner for the Electric Field Integral Equation
         IEEE TRANSACTIONS ON ANTENNAS AND PROPAGATION, VOL. 56, NO. 8, AUGUST 2008
    and XLiFE++ documentation
*/
//------------------------------------------------------------------------------------------------------
void FeSpace::buildBCRTSpace()
{
  trace_p->push("FeSpace::buildBCRTSpace");
  const MeshDomain* mdom = domain()->meshDomain();
  const std::vector<Point>& meshnodes = mdom->mesh()->nodes;
  RefElement* ref_p = findRefElement(_triangle, interpolation_p); // create reference element (if not exist) and element
  std::map<string_t,number_t> dofnum;

  //build the list of GeomElement's connected to vertices
  std::vector<GeomElement*>::const_iterator itge;
  std::map<number_t, std::list<GeoNumPair> > velts; //element list indexed by vertex number, giving the list of elements connected to vertex
  std::map<number_t, bool > onBoundary;             //for each vertex index the boundary status
  for (itge = mdom->geomElements.begin(); itge != mdom->geomElements.end(); ++itge)
  {
    (*itge)->flag=0; //reset flag of element
    number_t nbv = (*itge)->numberOfVertices();
    for (number_t i=1; i<=nbv; i++)
    {
      number_t vi=(*itge)->vertexNumber(i);
      onBoundary[vi]=false;
      GeoNumPair gp(*itge,i);
      //build element list
      if(velts.find(vi)==velts.end()) velts.insert(std::make_pair(vi, std::list<GeoNumPair>(1,gp)));
      else velts[vi].push_back(gp);
    }
  }
  //order the list of GeomElement's connected to vertices
  std::map<number_t,std::list<GeoNumPair> >::iterator itve=velts.begin();
  std::list<GeomElement*>::iterator itl, itn;
  for (; itve!=velts.end(); ++itve)
  {
    number_t vi=itve->first;  // main vertex number
    std::list<GeoNumPair>& eltsi=itve->second; //connected elements
    //do a map of all edges connected to vi, indexed by the other vertex number, at most two elements by edge
    //   el1=(vi,vj,vk1), el2=(vi,vj,vk2) :  ledges[vj] = ((el1, vk1), (el2, vk2))
    //   if a boundary, only one triangle:  ledges[vj] = ((el1, vk1), (0, 0))
    std::map<number_t,std::pair<GeoNumPair,GeoNumPair> > ledges;
    std::list<GeoNumPair>::iterator itl=eltsi.begin();
    std::map<GeomElement*, number_t> es; //to store temporary edge numbers
    for (; itl!=eltsi.end(); ++itl)
    {
      es[itl->first]=itl->second;
      number_t n1=0,n2=0;
      std::vector<number_t>vs=(itl->first)->vertexNumbers();
      if (vs[0]==vi) {n1=vs[1];n2=vs[2];}
      else
      {
        if(vs[1]==vi) {n1=vs[0];n2=vs[2];}
        else {n1=vs[0];n2=vs[1];}
      }
      if (ledges.find(n1)==ledges.end()) ledges.insert(std::make_pair(n1,std::make_pair(GeoNumPair(itl->first,n2),GeoNumPair(itl->first,0))));
      else ledges[n1].second=GeoNumPair(itl->first,n2);
      if (ledges.find(n2)==ledges.end()) ledges.insert(std::make_pair(n2,std::make_pair(GeoNumPair(itl->first,n1),GeoNumPair(itl->first,0))));
      else ledges[n2].second=GeoNumPair(itl->first,n1);
    }
    //detect first element on a boundary, if found, choose it to start the list of ordered element
    std::map<number_t,std::pair<GeoNumPair,GeoNumPair> >::iterator itms=ledges.begin(),itm;
    itm=ledges.end();
    while (itms!=ledges.end()) //detect edges on boundary
    {
      if (itms->second.second.second==0)   // edge not shared
      {
        if (itm==ledges.end())
        {
          onBoundary[vi]=true;   // edge [vi,itms->first] is on boundary
          itm=itms;              // start with this edge on boundary
        }
        onBoundary[itms->first]=true;
      }
      ++itms;
    }

    if (itm==ledges.end()) itm=ledges.begin();  //no edges on boundary, start with first
    //create ordered geo elements list
    number_t  k=1, nbe=eltsi.size();
    eltsi.clear();
    GeoNumPair g=itm->second.first;
    GeomElement* ge=g.first;
    number_t n = g.second;
    eltsi.push_back(GeoNumPair(ge,es[ge]));
    while (k<nbe)
    {
      itm=ledges.find(n);
      g=itm->second.first;
      if (g.first==ge) g=itm->second.second;
      ge=g.first;
      n=g.second;
      eltsi.push_back(GeoNumPair(ge,es[ge]));
      k++;
    }
    //counterclockwise list using the criteria (e1 x e2).n >0 where e1 is the first edge and e2 the second edge of the list
    if (eltsi.size()>1) //nothing to do if only one element
    {
      itl = eltsi.begin();
      std::vector<number_t> v1=itl->first->vertexNumbers(itl->second);
      itl++;
      std::vector<number_t> v2=itl->first->vertexNumbers(itl->second);
      Point u1,u2;
      if (v1[0]== vi) u1 = meshnodes[v1[1]-1]-meshnodes[vi-1];
      else u1 = meshnodes[v1[0]-1]-meshnodes[vi-1];
      if (v2[0]== vi) u2 = meshnodes[v2[1]-1]-meshnodes[vi-1];
      else u2 = meshnodes[v2[0]-1]-meshnodes[vi-1];
      Point n(0.,0.,1.);
      if (itl->first->spaceDim()==3)  n=itl->first->normalVector(); //manifold, get normal vector
      if (mixedProduct(u1,u2,n)<0) eltsi.reverse();
    }
  }

  //thePrintStream<<"  onBoundary = "<< onBoundary<<eol;

  //now build BC elements as RT elements with extra dofs (see doc)
  buildElements();
  buildgelt2elt();
  std::vector<Element>::iterator itel=elements.begin();
  for (; itel!=elements.end(); ++itel) itel->dofNumbers.clear();
  std::map<string_t,number_t> dofIndex; //temporary structure for building dofs
  std::map<string_t,number_t>::iterator itm;
  number_t n=0;
  for (itel=elements.begin(); itel!=elements.end(); ++itel)
  {
    GeomElement* gelt=itel->geomElt_p;
    number_t ns = gelt->numberOfSides();
    // intern dofs (ns)
    for (number_t s=1; s<=ns; s++)
    {
      string_t es=gelt->encodeElement(s);
      itm=dofIndex.find(es);
      if (itm==dofIndex.end()) //create new dof and update elements
      {
        n++;
        dofs.push_back(FeDof(*this, ref_p->refDofs[s-1], n, true));//create dof
        FeDof& fed=dofs.back();
        //fed.inElement(std::pair<Element*, number_t>(&(*itel), s)); //update dof inElements vector
        fed.projVector()=gelt->normalVector(fed.refDofP()->supportNumber());
        fed.projVector().normalize();
        //create BCDof
        BCDof* bcd= new BCDof();
        // declare as dof for all elements connected to edge vertices (counterclockwise)
        std::vector<number_t> vs=gelt->vertexNumbers(s);
        number_t v1 = vs[0], v2=vs[1];
        //thePrintStream<<"============= create BC dof "<<n<<" on edge "<<s<<" (v1="<<v1<<" v2="<<v2<<") of element "<<gelt->number()<<" ============="<<eol<<std::flush;
        Point X=0.5*(meshnodes[v1-1]+meshnodes[v2-1]);
        if (onBoundary[v1]) addToBcDofBoundary(bcd,fed,s,velts[v1],v1,meshnodes[v1-1],X,gelt);
        else               addToBcDof(bcd,fed,s,velts[v1],v1,meshnodes[v1-1],X,gelt);
        if (onBoundary[v2]) addToBcDofBoundary(bcd,fed,s,velts[v2],v2,meshnodes[v2-1],X,gelt,-1);
        else               addToBcDof(bcd,fed,s,velts[v2],v2,meshnodes[v2-1],X,gelt,-1);
        fed.dofExt_= bcd;
        dofIndex[es] = n;                        //add key in global dof map
      }
    }
    //extern dof
  }
  trace_p->pop();
}

/*! tools for FeSpace:: buildBCRTSpace()
    add to the BC dof located on side s of element gelt, the parts of shape basis related to one vertex of an edge
      bcd: BcDof
      fed: FeDof
      s: side number
      elts:  list of elements (counterclockwise) connected to the vertex (vnum) of side s
      X:  middle point of side s
      gelt: related geometric element
      rev: reverse order
    vertex not on the boundary
 */
void FeSpace::addToBcDof(BCDof* bcd, FeDof& fed, number_t s, std::list<GeoNumPair>& elts, number_t vnum,
                         const Point& vertex, const Point& X, GeomElement* gelt, int rev)
{
  std::list<GeoNumPair>::iterator itl=elts.begin(), itle=elts.end();
  number_t n=elts.size(), dofnum=fed.id();
  while (itl!=itle && itl->first!=gelt) itl++;
  if (itl==itle)
  {
    where("FeSpace::addToBcDof(...)");
    error("geoelt_not_found");
  }
  number_t e = itl->second;         // first edge
  if (e!=s)  //start with the next element
  {
    itl++;
    if (itl==itle) itl=elts.begin();
    e = itl->second;
  }
  GeomElement* cgelt=itl->first;    //current element
  number_t k=0;
  for (number_t i=1; i<=n; i++)
  {
    Element* celt=const_cast<Element*>(element_p(cgelt));
    std::vector<number_t> cvs=cgelt->vertexNumbers(e);
    real_t l, coef;
    number_t subelt, subedge;

    //first edge
    if (i!=1)  // not the first element
    {
      l = 0.5*cgelt->measure(e);
      coef = (real_t(n)-k)/(2*n*l);
      if (cvs[0]==vnum) {subedge=1; subelt=2*e-1;}
      else {subedge=3; subelt=2*e;}
    }
    else //first element
    {
      l=dist(cgelt->center(),X);
      coef=real_t(n)/(2*n*l);
      subedge=2;
      if (cvs[0]==vnum) subelt=2*e-1;
      else subelt=2*e;
    }
    bcd->add(celt, subelt, subedge, -coef*rev);

    //intermediate edges
    l=dist(cgelt->center(),vertex);
    coef = (real_t(n)-k-1)/(2*n*l);
    if (cvs[0]==vnum) {subedge=3; subelt=2*e-1;}
    else {subedge=1; subelt=2*e;}
    bcd->add(celt, subelt, subedge, coef*rev);
    if (cvs[0]==vnum) {subedge=1; subelt=(e==1)?6:2*(e-1);}
    else {subedge=3; subelt=(e==3)?1:2*e+1;}
    bcd->add(celt, subelt, subedge, -coef*rev);

    //last edge
    if (i!=n)  //not last element
    {
      number_t e2=e+2;
      if (e2==4) e2=1;
      if (e2==5) e2=2;
      l=0.5*cgelt->measure(e2);
      coef = (real_t(n)-k-2)/(2*n*l);
      if (cvs[0]==vnum) {subedge=3; subelt=(e==1)?6:2*(e-1);}
      else {subedge=1; subelt=(e==3)?1:2*e+1;}
    }
    else //last element
    {
      l=dist(cgelt->center(),X);
      coef=-real_t(n)/(2*n*l);
      subedge=2;
      if (cvs[0]==vnum) subelt=2*e-1;
      else subelt=2*e;
    }
    bcd->add(celt, subelt, subedge, coef*rev);

    // update dofNumbers and inElement data
    const std::vector<std::pair<Element*, number_t> >& inelts = fed.elements();
    std::vector<std::pair<Element*, number_t> >::const_iterator iti=inelts.begin();
    while (iti!=inelts.end()&& celt!=iti->first) iti++;
    if (iti==inelts.end())
    {
      celt->dofNumbers.push_back(dofnum);
      fed.inElement(std::pair<Element*, number_t>(celt, celt->dofNumbers.size())); //update dof inElements vector
    }
    //move to next element
    itl++;
    if (itl==itle) itl=elts.begin();
    cgelt=itl->first;    //current element
    e = itl->second;     // first edge
    k+=2;
  }
}

/*! tools for FeSpace:: buildBCRTSpace()
    add to the BC dof located on side s of element gelt, the parts of shape basis related to one vertex of an edge
    elts is the list of elements (counterclockwise) connected to the vertex (vnum) of side s and X is the middle point of side s
    vertex on the boundary
 */
void FeSpace::addToBcDofBoundary(BCDof* bcd, FeDof& fed, number_t s, std::list<GeoNumPair>& elts, number_t vnum,
                                 const Point& vertex, const Point& X, GeomElement* gelt, int rev)
{
  std::list<GeoNumPair>::iterator itl=elts.begin(), itle=elts.end();
  std::vector<number_t> refedge = gelt->vertexNumbers(s);  //vertex numbers of reference edge supporting current BcDof
  number_t n=elts.size(), dofnum=fed.id();
  real_t l, coef = (1.-n)/n;
  number_t subelt, subedge;
  bool onRef=false;

  for (itl=elts.begin(); itl!=itle; ++itl)
  {
    GeomElement* cgelt=itl->first;    //current geom element
    number_t e= itl->second;          //first connected edge number
    Element* celt=const_cast<Element*>(element_p(cgelt));
    std::vector<number_t> cvs=cgelt->vertexNumbers(e);

    //first edge
    onRef=false;
    if ((cvs[0]==refedge[0] && cvs[1]==refedge[1]) || (cvs[0]==refedge[1] && cvs[1]==refedge[0]))
      {onRef=true;  coef= (2.-n)/(2*n);}
    l = 0.5*cgelt->measure(e);
    if (cvs[0]==vnum) {subedge=1; subelt=2*e-1;}
    else {subedge=3; subelt=2*e;}
    bcd->add(celt, subelt, subedge, -rev*coef/l);
    if (onRef) //add transverse edge
    {
      l=dist(cgelt->center(),X);
      bcd->add(celt, subelt, 2, -rev/(2.*l));
      coef= 1./n;  //after ref edge
    }

    //intermediate edge
    l=dist(cgelt->center(),vertex);
    if (cvs[0]==vnum) {subedge=3; subelt=2*e-1;}
    else {subedge=1; subelt=2*e;}
    bcd->add(celt, subelt, subedge, rev*coef/l);
    if (cvs[0]==vnum) {subedge=1; subelt=(e==1)?6:2*(e-1);}
    else {subedge=3; subelt=(e==3)?1:2*e+1;}
    bcd->add(celt, subelt, subedge, -rev*coef/l);

    //last edge
    onRef=false;
    number_t e2=e+2;
    if (e2==4) e2=1;
    if (e2==5) e2=2;
    cvs=cgelt->vertexNumbers(e2);
    if ((cvs[0]==refedge[0] && cvs[1]==refedge[1]) || (cvs[0]==refedge[1] && cvs[1]==refedge[0]))
      {onRef=true;  coef= (2.-n)/(2*n);}
    l=0.5*cgelt->measure(e2);
    if (cvs[0]==vnum) {subedge=3; subelt=(e==1)?6:2*(e-1);}
    else {subedge=1; subelt=(e==3)?1:2*e+1;}
    bcd->add(celt, subelt, subedge, rev*coef/l);
    if (onRef) //add transverse edge
    {
      l=dist(cgelt->center(),X);
      bcd->add(celt, subelt, 2, -rev/(2.*l));
      coef= 1./n;  //after ref edge
    }

    // update dofNumbers and inElement data
    const std::vector<std::pair<Element*, number_t> >& inelts = fed.elements();
    std::vector<std::pair<Element*, number_t> >::const_iterator iti=inelts.begin();
    while (iti!=inelts.end()&& celt!=iti->first) iti++;
    if (iti==inelts.end())
    {
      celt->dofNumbers.push_back(dofnum);
      fed.inElement(std::pair<Element*, number_t>(celt, celt->dofNumbers.size())); //update dof inElements vector
    }
  }
}

//------------------------------------------------------------------------------------------------------
/*!buildgelt2elt: construct the map gelt2elt from elements list */
//------------------------------------------------------------------------------------------------------
void FeSpace::buildgelt2elt() const
{
  number_t s=gelt2elt.size();
  if (s!=0) return;
  std::vector<Element>::const_iterator ite=elements.begin();
  number_t n=0;
  for(; ite!=elements.end(); ite++, n++) gelt2elt[ite->geomElt_p]=n;
}

//! construct the map dofid2rank from dofs list, ranks start from 1
void FeSpace::builddofid2rank() const
{
  if (dofid2rank_.size()>0) return; //already built
  std::vector<FeDof>::const_iterator itd=dofs.begin();
  number_t k=1;
  for (; itd!=dofs.end(); ++itd,++k)
    dofid2rank_.insert(std::pair<number_t, number_t>(itd->id(),k));
}

/*! access to element number associated to a geomelement
    if not found return the size of gelt2elt (number of elements)
 */
number_t FeSpace::numElement(GeomElement* gelt) const
{
  if (gelt2elt.size()==0) buildgelt2elt();
  std::map<GeomElement*, number_t>::const_iterator itm=gelt2elt.find(gelt);
  if (itm==gelt2elt.end()) return gelt2elt.size();
  return itm->second;
}

/*! return element linked to a given geom element
    used gelt2elt map; has to be already built
*/
const Element* FeSpace::element_p(GeomElement* gelt) const
{
  return &element(gelt);
}

const Element& FeSpace::element(GeomElement* gelt) const
{
  if (gelt2elt.size()==0) buildgelt2elt();
  std::map<GeomElement*, number_t>::const_iterator itm=gelt2elt.find(gelt);
  if (itm==gelt2elt.end())
  {
    where("FeSpace::element_p(GeomElement*)");
    error("geoelt_not_found");
  }
  return elements[itm->second];
}

const Element* FeSpace::locateElement(const Point& P, bool useNearest, bool errorOnOutDom, real_t tol) const
{
  const MeshDomain* dom=domain()->meshDomain();
  if (dom==nullptr || dom->numberOfElements() == 0)
  {
    where("FeSpace::locateElement(...)");
    error("domain_notmesh",domain()->name(), words("domain type", domain()->domType()));
  }
  real_t di=0.;
  GeomElement* gelt=nullptr;
  if(!useNearest) //  locate in silent mode
  {
     gelt=dom->locate(P,true);
     if (gelt==nullptr)
     {
       if(errorOnOutDom) locateError(false,"FeSpace::locateElement",*dom,P,di);
       else return nullptr;
     }
  }
  //use nearest
  Point q=P;
  gelt = dom->nearest(q,di);
  if (gelt==nullptr)
  {
    if(errorOnOutDom) locateError(false,"FeSpace::locateElement",*dom,P,di);
    return nullptr;
  }
  if (di > gelt->size()*tol)
  {
    if(errorOnOutDom) locateError(true,"FeSpace::locateElement",*dom,P,di);
    return nullptr;
  }
  if (gelt2elt.size()==0) buildgelt2elt();  //build once
  return element_p(gelt);
}

/*! locate Lagrange dof nearest of a given point */
Dof& FeSpace::locateDof(const Point & P) const
{
  const MeshDomain* dom=domain()->meshDomain();
  if (dom==nullptr)
  {
    where("FeSpace::locateDof(...)");
    error("domain_notmesh",domain()->name(), words("domain type",domain()->domType()));
  }

  //find nearest GeomElement
  real_t d;
  Point cP=P;
  GeomElement* gelt = dom->nearest(cP, d);
  if (gelt2elt.size()==0) buildgelt2elt();  //build once
  const Element& elt=element(gelt);

  //find nearest dof
  const std::vector<number_t>& dns=elt.dofNumbers;
  d=theRealMax;
  number_t i=0;
  for (std::vector<number_t>::const_iterator itd=dns.begin(); itd!=dns.end(); ++itd)
  {
    const Point& dP = dofs[*itd-1].coords();
    real_t q=cP.distance(dP);
    if(q<d) {d=q; i=*itd;}
  }
  if (i==0)
  {
    where("FeSpace::locateDof(...)");
    error("dof_not_found");
  }
  return const_cast<FeDof&>(dofs[i-1]);
}

/*! max degree of polynomials involved by shape functions
    if not polynomial return a default degree 5
*/
number_t FeSpace::maxDegree() const
{
  number_t deg=0;
  std::set<RefElement *>::const_iterator itr=refElts.begin();
  for(; itr!=refElts.end(); ++itr) deg=std::max(deg,(*itr)->maxDegree);
  return deg;
}

//--------------------------------------------------------------------------------
//   create Graph of Finite Element Dofs
//--------------------------------------------------------------------------------
Graph FeSpace::graphOfDofs()
{
  trace_p->push("FeSpace::graphOfDofs");
  // create sets of neighbor Dof's for all Finite Element Dofs in FESpace
  Graph dofsGraph(nbDofs());
  std::vector<number_t>::iterator dof_i, dof_j;

  for (std::vector<Element>::iterator fe_i = elements.begin(); fe_i != elements.end(); fe_i++)
  {
    for (dof_i = fe_i->dofNumbers.begin(); dof_i != fe_i->dofNumbers.end(); dof_i++)
    {
      // node set i contains all j's sharing same element with node i
      //for(dof_j = dof_i + 1; dof_j != fe_i->dofNumbers.end(); dof_j++)   //Eric correction
      for (dof_j = dof_i ; dof_j != fe_i->dofNumbers.end(); dof_j++)
      {
        dofsGraph[*dof_i - 1].push_back(*dof_j); // all j-nodes belong to i-node set
        dofsGraph[*dof_j - 1].push_back(*dof_i); // i-node belong to all j-node sets
      }
    }
  }
  // for each dof make neighbors unique and sort'em by ascending degree
  dofsGraph.sortByAscendingDegree();

  trace_p->pop();
  return dofsGraph;
}

//--------------------------------------------------------------------------------
//   create new D.o.F numbering to minimize band width
//--------------------------------------------------------------------------------
std::pair<number_t, number_t> FeSpace::renumberDofs()
{
  trace_p->push("FeSpace::renumberDofs");
  // create connected graph of dofs for all Finite Element dofs in FESpace
  Graph dofsGraph = graphOfDofs();
  // get band-width and number of entries in skyline storage
  number_t sky = dofsGraph.bandWitdhAndSkyline().second;
  if (optimizedNumbering_)   //already optimized
  {
    warning("fe_space_already_optimised", name(), sky);
    trace_p->pop();
    return std::pair<number_t, number_t>(sky, sky);
  }
  // renumber nodes to minimize bandwidth
  std::vector<number_t> renumberedDof = dofsGraph.renumber(nbDofs());

  //if trivial renumbering (1, 2, ...) ?
  bool trivial=true;
  number_t n=1;
  std::vector<number_t>::iterator itr=renumberedDof.begin();
  while (trivial && itr!= renumberedDof.end())
  {
    trivial = *itr==n;
    n++;
    itr++;
  }

  number_t skyopt=sky;
  if (!trivial)
  {
    std::vector<Element>::iterator fe_i = elements.begin();
    for (; fe_i != elements.end(); fe_i++) // copy new dofs numbering
      fe_i->dofsRenumbered(renumberedDof, dofs);
    std::sort(dofs.begin(), dofs.end());  // sort FE space dofs
    skyopt = graphOfDofs().bandWitdhAndSkyline().second; //new skyline size
  }

  optimizedNumbering_ = true;
  trace_p->pop();
  return std::pair<number_t, number_t>(sky, skyopt);
}

// return the DoF ids on space
std::vector<number_t> FeSpace::dofIds() const
{
  std::vector<number_t> ids(dofs.size());
  std::vector<FeDof>::const_iterator itd = dofs.begin();
  std::vector<number_t>::iterator it = ids.begin();
  for(; it != ids.end(); it++, itd++) *it = itd->id();
  return ids;
}

//return the dimension of FE elements (1,2, or 3)
dimen_t FeSpace::dimElements() const
{
  if (elements.size() == 0) return 0;   //no elements !?
  return elements[0].dimElt();
}

ValueType FeSpace::valueType() const     // return value type of basis function (real or complex)
{
  return _real;
}

StrucType FeSpace::strucType() const    // return structure type of basis function (scalar or vector)
{
  if (interpolation_p->isScalar()) return _scalar;
  return _vector;
}

//check type of Space
bool FeSpace::isSpectral() const
{
  return false;
}

bool FeSpace::isFE() const
{
  return true;
}

//! true if trace space cannot be defined from trace of FE on side, case of edge FE
bool FeSpace::extensionRequired() const
{
  std::set<RefElement*>::const_iterator itr =refElts.begin();
  for (; itr != refElts.end(); ++itr)
  {
    if((*itr)->dim() >1 && (*itr)->sideRefElems().size()==0) return true;
  }
  return false;
}

/*! return the set of dof numbers of dofs "located" on a domain:
    when a plain domain, return all the dofs
    when a side domain, return only dofs with support in the side domain
    when a side of side domain, return only dofs with support in the side of side domain

    filter dofs along some characteristics (see Refdof class):
      order_: order of a derivative D.o.F or a moment D.o.F (0)
      supportDim_: dimension of the D.o.F geometric support (0 point, 1 segment, ...)
      projectionType_: type of projection (_noProjection , _givenProjection, _dotnProjection, _crossnProjection)

      order = -1 means all dofs

    NOTE: this function is useful when the restriction of FeSpace on a side does not define a FeSubSpace
           for instance, this is the case for Raviart, Nedelec space where the trace on a side domain is not a finite element space
*/
std::vector<number_t> FeSpace::dofsOn(const GeomDomain& gd, int_t ord, dimen_t sdim, ProjectionType pt) const
{
  trace_p->push("FeSpace::dofsOn");
  if (gd.domType() != _meshDomain)
  {
    trace_p->pop();
    error("domain_notmesh", gd.name(), words("domain type", gd.domType()));
  }
  if (gd.mesh()!= domain()->mesh())
  {
    trace_p->pop();
    error("domain_space_mismatch_mesh");
  }
  if (&gd == domain())   //same domain
  {
    trace_p->pop();
    return dofIds();
  }
  std::set<number_t> dofset;               //to store dof numbers

  if (gd.dim() == dimDomain())   //domains has the same dimension, look for common elements
  {
    //create GeomDomain elements index along their pointers in a map
    std::map<GeomElement*, number_t> index;
    number_t l = 0;
    const std::vector<GeomElement*>& geoElts = gd.meshDomain()->geomElements;
    std::vector<GeomElement*>::const_iterator itge;
    for (itge = geoElts.begin(); itge != geoElts.end(); itge++, l++)
    {
      index.insert(std::pair<GeomElement*, number_t> (*itge, l));
    }
    std::vector<Element>::const_iterator ite = elements.begin();
    for (; ite != elements.end(); ite++)
      if (index.count(ite->geomElt_p) > 0)
      {
        std::vector<number_t>::const_iterator itn=ite->dofNumbers.begin();
        for (; itn!=ite->dofNumbers.end(); ++itn)
        {
          const RefDof* r = dofs[*itn-1].refDofP();
          if (ord==-1 || (r->supportDim()==sdim && r->order()==ord && r->typeOfProjection()==pt))
                dofset.insert(*itn);
        }
      }
    trace_p->pop();
    return std::vector<number_t>(dofset.begin(),dofset.end());
  }

  if (gd.dim() == dimDomain()-1)   //side domain
  {
    if (gelt2elt.size()==0) buildgelt2elt();   //build map GeomElement to Element
    const std::vector<GeomElement*>& geoElts = gd.meshDomain()->geomElements;
    std::vector<GeomElement*>::const_iterator itge;
    for (itge = geoElts.begin(); itge != geoElts.end(); itge++)
    {
      std::vector<GeoNumPair>& parsides = (*itge)->parentSides(); //parents and side numbers
      std::vector<GeoNumPair>::iterator itvg=parsides.begin();
      for (; itvg!=parsides.end(); itvg++)   //travel parents
      {
        std::map<GeomElement*, number_t>::iterator itm=gelt2elt.find(itvg->first);
        if (itm!=gelt2elt.end())  //element found
        {
          const Element& elt = elements[itm->second];
          number_t s = itvg->second;
          if (elt.refElt_p->sideDofNumbers_.size()>0)   //add side dofs
          {
            const std::vector<number_t>& sidedof = elt.refElt_p->sideDofNumbers_[s-1];
            std::vector<number_t>::const_iterator itn=sidedof.begin();
            for (; itn!=sidedof.end(); ++itn)
            {
              number_t n = elt.dofNumbers[*itn-1];
              const RefDof* r = dofs[n-1].refDofP();
              if (ord==-1 || (r->supportDim()==sdim && r->order()==ord && r->typeOfProjection()==pt))
                dofset.insert(n);
            }
          }
          if (elt.refElt_p->sideOfSideDofNumbers_.size()>0)   //add side of side dofs belonging to side s
          {
            const std::vector<int_t>& sdn=elt.refElt_p->geomRefElem_p->sideOfSideNumbers(s);
            std::vector<int_t>::const_iterator ite = sdn.begin();
            for (; ite!=sdn.end(); ++ite)  //loop on sides of side s
            {
              const std::vector<number_t>& sidedof = elt.refElt_p->sideOfSideDofNumbers_[std::abs(*ite)-1];
              std::vector<number_t>::const_iterator itn=sidedof.begin();
              for (; itn!=sidedof.end(); ++itn)
              {
                number_t n = elt.dofNumbers[*itn-1];
                const RefDof* r = dofs[n-1].refDofP();
                if (ord==-1 || (r->supportDim()==sdim && r->order()==ord && r->typeOfProjection()==pt))
                  dofset.insert(n);
              }
            }
          }
        }
      }
    }
    trace_p->pop();
    return std::vector<number_t>(dofset.begin(),dofset.end());
  }

  if (gd.dim() == dimDomain()-2)   //side of side domain
  {
    const MeshDomain* md=gd.meshDomain();
    if (md->vertexElements.size()==0) md->buildVertexElements();
    for(auto& e : md->vertexElements)  //loop on domain vertices
    {
      const Point& p=e.first;
      for(auto d : dofs)  //loop on space dofs (expensive algorithm, to be improved)
        if(norm(p-d.coords())<theTolerance)
        {
          dofset.insert(d.id());
          break;
        }
    }
    trace_p->pop();
    return std::vector<number_t>(dofset.begin(),dofset.end());
  }

  error("space_domain_incompatible_dims", gd.name(), name(), gd.dim(), dimDomain());
  trace_p->pop();
  return  std::vector<number_t>(1); //fake return
}

// return true if (sub)space is included in current space
// true cases: a subspace of FeSpace or same FeSpace's
bool FeSpace::include(const Space* sp) const
{
  if (sp->typeOfSpace() == _feSpace) return (sp->feSpace() == this);
  if (sp->typeOfSpace() == _subSpace)
  {
    const SubSpace* subsp = sp->subSpace();
    if (subsp->rootSpace() == static_cast<const Space*>(this)) return true;
  }
  return false;
}

//--------------------------------------------------------------------------------
//    print Fe space
//--------------------------------------------------------------------------------
void FeSpace::print(std::ostream& os) const
{
  if (theVerboseLevel <= 0) return;
  printSpaceInfo(os);           //print general space information
  if (theVerboseLevel < 3) return;
  os << eol;
  interpolation_p->print(os);   //print interpolation information
  // print D.O.F list
  number_t level_fact = 2 * theVerboseLevel;
  os << eol << message("fespace_dofinfo", dofs.size())<<" ";
  if (optimizedNumbering_) os << message("fespace_dofinfo2");

  std::vector<FeDof>::const_iterator dof_ib1 = dofs.begin(), dof_ie1 = dofs.end(),
                                     dof_ib2 = dof_ie1, dof_ie2 = dof_ie1, dof_i;
  if (dofs.size() > 2 * level_fact)
  {
    dof_ie1 = dof_ib1 + level_fact;
    dof_ib2 = dof_ie2 - level_fact;
    os << " (" << words("first and last") << " " << level_fact << " " << words("DoF") << ")";
  }

  //compute field size
  number_t ti=6,ts=6,td=3,tt=0,tw=0,tc=0,tp=0,tn=0,len;
  for (dof_i = dof_ib1; dof_i != dofs.end(); dof_i++)
  {
    tt=std::max(tt,dof_i->refDofP()->name().size());
    tw=std::max(tw,words("dof location",dof_i->refDofP()->where()).size());
    std::stringstream sc;
    std::vector<std::pair<Element*, number_t> >::const_iterator it;
    for (it = dof_i->elements().begin(); it != dof_i->elements().end(); it++)
    {
      sc << " " << it->first->number() << "(" << it->second << ")";
      tc=std::max(tc,sc.str().size());
    }
    tp=std::max(tp,tostring(dof_i->coords()).size());
    if(dof_i->refDofP()->typeOfProjection()!=_noProjection) tn=std::max(tn,tostring(dof_i->projVector()).size());
  }
  tp+=1;tp=std::max(number_t(10),tp);
  tc=std::max(tc,number_t(18));
  tn+=2;tn=std::max(tn,number_t(10));
  len = ti+ts+td+tt+tw+tc+tp+tn+9;
  os << eol << "    |" << format(words("DoF"),ti) << "|" << format(words("shared"),ts)<<"|dim|"
     << format("type",tt)<<"|"<<format("where",tw)<<"|"<< format(words("linked to elements"),tc)<<"|"
     << format("coords (*)",tp)<<"|"<<format("projection",tn)<<"|"
     << eol<<"    "<<string_t(len,'-')<<eol;
  for (dof_i = dof_ib1; dof_i != dof_ie1; dof_i++)
  {
    dof_i->printTab2(os,ti,ts,td,tt,tw,tc,tp,tn);
  }
  if (dofs.size() > 2 * level_fact)
  {
    os  << "     ...." << eol;
  }
  for (dof_i = dof_ib2; dof_i != dof_ie2; dof_i++)
  {
    dof_i->printTab2(os,ti,ts,td,tt,tw,tc,tp,tn);
  }
  // print 'level_fact' first element, all when the number of elements is less than theVerboseLevel
  if(theVerboseLevel<10) return;
  level_fact = theVerboseLevel;
  os << eol;
  if (dimDomain() == dimElements())  os << message("fespace_eltinfo", elements.size());
  else        if (dimDomain() == 1)  os << message("fespace_edgeltinfo", elements.size());
  else                  os << message("fespace_faceltinfo", elements.size());
  std::vector<Element>::const_iterator fe_ib1 = elements.begin(), fe_ie1 = elements.end();
  std::vector<Element>::const_iterator fe_ib2 = fe_ie1, fe_ie2 = fe_ie1, fe_i;
  if (elements.size() > 2 * level_fact)
  {
    fe_ie1 = fe_ib1 + level_fact;
    fe_ib2 = fe_ie2 - level_fact;
    os << " (" << words("first and last") << " " << level_fact << " " << words("elements") << ")";
  }
  const RefElement* refelt = nullptr;
  for (fe_i = fe_ib1; fe_i != fe_ie1; fe_i++)
  {
    if (fe_i->refElt_p != refelt)
    {
      refelt = fe_i->refElt_p;  //print refelt if different from previous one
      os << std::endl << (*refelt);
    }
    os << std::endl << *fe_i;
  }
  if (fe_ib2 != fe_ie2)
  {
    os << "\n...";
    for (fe_i = fe_ib2; fe_i != fe_ie2; fe_i++)
    {
      if (fe_i->refElt_p != refelt)
      {
        refelt = fe_i->refElt_p;
        os << std::endl << (*refelt);
      }
      os << std::endl << *fe_i;
    }
  }
}

// return reference dof number of n-th dof
// member function of Element class implemented here to avoid cross dependancy !
number_t Element::refDofNumber(number_t n) const
{
  const FeDof& fed=feSpace_p->fedof(n);
  const std::vector<std::pair<Element*, number_t> >& inelts=fed.elements();
  std::vector<std::pair<Element*, number_t> >::const_iterator ite=inelts.begin();
  while (ite!=inelts.end())
    if (ite->first == this) return ite->second;
    else ++ite;
  return 0;
}

} // end of namespace xlifepp
