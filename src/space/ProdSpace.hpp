/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ProdSpace.hpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 13 jun 2013

  \brief Definition of the xlifepp::ProdSpace class

  It describes product managing a list of spaces as a vector<xlifepp::Space*>

  !!!! CURRENTLY NOT USED !!!!
*/

#ifndef PROD_SPACE_HPP
#define PROD_SPACE_HPP

#include "config.h"
#include "Space.hpp"

namespace xlifepp
{

//------------------------------------------------------------------------------------
/*!
   \class ProdSpace
   defines data related to a product of spaces
   this class may be useful when interpolations along the components are differents
   when they are all the same, use standard space definitions
   NOT USED, for future
 */
//------------------------------------------------------------------------------------
class ProdSpace : public Space
{
  protected:
    std::vector<Space*> spaces_;   //!< list of spaces defining the space product
  public:
    virtual number_t dimSpace() const; //!< returns space dimension
    virtual number_t nbdofs() const;  //!< returns number of dofs
    virtual const Space* rootSpace() const //! access to root space pointer (const)
    {return static_cast<const Space*>(this);}
    virtual Space* rootSpace()             //! access to root space pointer
    {return static_cast<Space*>(this);}
    virtual number_t dofId(number_t n) const   //! return the DoF id of n-th space DoF (n=1,...)
    {
      error("not_handled","ProdSpace::dofId");
      return 0;
    }
};

} // end of namespace xlifepp

#endif // PROD_SPACE_HPP
