/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file FeSubSpace.hpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 13 jun 2013

  \brief Definition of the xlifepp::FeSubSpace class, inherited from xlifepp::SubSpace class

  Manage additional informations:
    - a list of finite elements (or subelements) : vector<xlifepp::Element*>
    - the numbering of D.o.Fs of domain elements in domain D.o.Fs: vector<std::vector<xlifepp::number_t>>
    - the map xlifepp::GeomElement -> element number: map<xlifepp::GeomElement*, number_t>  (if required)
*/

#ifndef FE_SUB_SPACE_HPP
#define FE_SUB_SPACE_HPP

#include "config.h"
#include "SubSpace.hpp"
#include "Dof.hpp"
#include "Element.hpp"

namespace xlifepp
{

/*!
   \class FeSubSpace
   inheriting from SubSpace handles additionnal data for elementary Fe type subspace.
   Elementary means a subspace based on a single mesh domain (not an union of mesh domains)
   This structure is useful for Fe computation on sub domains,
   when the subdomain is not of the same dim as parent domain, Elements are SubElements
 */
class FeSubSpace : public SubSpace
{
  public:
    std::vector<Element*> elements;                    //!< list of finite elements (or subelements)
    std::vector<std::vector<number_t> > dofRanks;      //!< numbering of D.o.Fs of domain elements in domain D.o.Fs
    mutable std::map<Element*,number_t> eltRanks;      //!< map Element* -> element number (useful to interpolation tools)
    mutable std::map<GeomElement*, number_t> gelt2elt; //!< map GeomElement -> element number (useful to interpolation tools)
    mutable std::vector<FeDof> fedofs;                 //!< explicit list of FeDofs, built if required
    std::set<RefElement *> refElts;                    //!< set of pointer to RefElement involved in FeSpace (usually one)

    FeSubSpace(const GeomDomain&, Space&, const string_t& na = "");     //!< constructor specifying geom domain, parent space and name
    FeSubSpace(const GeomDomain&, Space&, bool, const string_t& na=""); //!< constructor specifying geom domain, parent subspace and name
    ~FeSubSpace();                                     //!< destructor
    void buildgelt2elt() const;                        //!< construct the map get2elt from elements list

    virtual bool isFeSubspace() const {return true;}            //!< returns true if FeSubSpace
    const FeSubSpace* feSubSpace() const                        //! autocast (see virtual function in parent)
    {return this;}
    virtual const Space* rootSpace() const                      //! access to root space pointer (const)
    {return parent_p->rootSpace();}
    virtual Space* rootSpace()                                  //! access to root space pointer
    {return parent_p->rootSpace();}
    const Interpolation* interpolation() const                  //! return pointer to interpolation
    {return rootSpace()->interpolation();}
    number_t nbOfElements() const                               //! number of elements for FESpace and FESubspace only
      {return elements.size();}
    const Element* element_p(number_t k) const                  //! access to k-th (k>=0) element (pointer) for FESpace and FESubspace only
      {return elements[k];}
    const Element& element(number_t k) const                    //! access to k-th (k>=0) element (pointer) for FESpace and FESubspace only
      {return *elements[k];}
    const Element* element_p(GeomElement* gelt) const;          //!< access to element associated to a geomelement
    const Element& element(GeomElement* gelt) const;            //!< access to element associated to a geomelement
    number_t numElement(GeomElement* gelt) const;               //!< access to element number associated to a geomelement
    const std::vector<number_t>& elementDofs(number_t k) const; //!< access to dofs ranks (local numbering) of k-th (>=0) element for FESpace and FESubspace only
    std::vector<number_t> elementParentDofs(number_t k) const;  //!< access to dofs ranks (parent numbering) of k-th (>=0) element for FESpace and FESubspace only
    virtual const std::vector<FeDof>& feDofs() const;           //!< return fedofs vector
    const std::set<RefElement*>& refElements() const            //! access to set of pointer to RefElement (refElts) involved in FESpace
    {return refElts;}
    template <typename T, typename K>
    T& interpolate(const Vector<K>&, const Point&, T&, DiffOpType =_id, bool useNearest=false) const; //!< compute interpolated value at P
    template <typename T, typename K>
    T& interpolate(const Vector<K>&, const Point&, const Element*, T&, DiffOpType =_id,bool useNearest=false) const;//!< compute interpolated value at P knowing Element
    const Element* locateElement(const Point& p, bool useNearest=false,
                                 bool errorOnDomainOut = true, real_t tol=theLocateToleranceFactor) const;         //!< locate element of the FeSubspace containing P

    void print(std::ostream&) const;                                    //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator <<(std::ostream&, const FeSubSpace&);
};

std::ostream& operator <<(std::ostream&, const FeSubSpace&); //!< print operator

//------------------------------------------------------------------------------------
//  templated member functions
//------------------------------------------------------------------------------------
template <typename T, typename K>
T& FeSubSpace::interpolate(const Vector<K>& v, const Point& P, T& vint, DiffOpType d, bool useNearest) const
{
  vint=T();
  const Element* elt=locateElement(P,useNearest,true);
  if(elt==nullptr) return vint;
  return elt->interpolate(v,P,dofRanks[eltRanks[const_cast<Element*>(elt)]],vint,d);
}

template <typename T, typename K>
T& FeSubSpace::interpolate(const Vector<K>& v, const Point& P, const Element* eltp,  T& vint, DiffOpType d, bool useNearest) const
{
  vint=T();
  const Element* elt=eltp;
  if(elt==nullptr) elt=locateElement(P,useNearest,true);
  if(elt==nullptr) return vint;
  return elt->interpolate(v,P,dofRanks[eltRanks[const_cast<Element*>(elt)]],vint,d);
}

} // end of namespace xlifepp

#endif // SPACE_HPP
