/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SpSpace.hpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 13 jun 2013

  \brief Definition of the xlifepp::SpSpace class inherited from xlifepp::Space

  It describes spectral space, either based on explicit functions or interpolated functions
  and mainly managing:
    - a xlifepp::SpectralBasis object describing the functions basis
    - a list of global degrees of freedom: vector<xlifepp::SpDof>
*/

#ifndef SP_SPACE_HPP
#define SP_SPACE_HPP

#include "config.h"
#include "Space.hpp"
#include "SpectralBasis.hpp"
#include "Dof.hpp"

namespace xlifepp
{

//------------------------------------------------------------------------------------
/*!
   \class SpSpace
   defines general data of a space defined by a set of spectral functions
   SpSpace class is not exposed to end users
 */
//------------------------------------------------------------------------------------
class SpSpace : public Space
{
  protected:
    SpectralBasis* spectralBasis_; //!< object function encapsulating the spectral functions
    //either defined in an analytical form or by interpolated functions (TermVector)

  public:
    std::vector<SpDof> dofs;       //!< list of global degrees of freedom
    //constructor/destructor
    SpSpace(const string_t&, const GeomDomain&, number_t, dimen_t, SpectralBasis*); //!< constructor with reference on domain
    SpSpace(const string_t&, const GeomDomain*, number_t, dimen_t, SpectralBasis*); //!< constructor with pointer on domain
                                                                              //when there is no domain, pass 0 as domain pointer
    ~SpSpace(); //!< destructor

    //accessors
    SpectralBasis* spectralBasis()const {return spectralBasis_;} //!< returns the spectral basis
    virtual const SpSpace* spSpace()const {return this;}     //!< downcast function
    //virtual number_t dimSpace()const {return dofs.size()*dimCom();}  //!< true space dimension
    virtual number_t dimSpace()const {return dofs.size();}   //!< true space dimension
    virtual dimen_t dimFun() const                           //! dof dimension (shape function dim)
    {return spectralBasis_->dimFun();}
    virtual number_t nbDofs()const {return dofs.size();}     //!< number of dofs (a dof may be a vector dof)
    virtual const Space* rootSpace() const                   //! access to root space pointer
    {return static_cast<const Space*>(this);}
    virtual number_t dofId(number_t n) const                 //! return the DoF id of n-th space DoF (n=1,...)
    {return dofs[n-1].id();}
    virtual std::vector<number_t> dofIds() const;            //!< return the DoF ids on space
    virtual const Dof& dof(number_t n) const                 //! return n-th spdof as dof (n=1,...)
    {return static_cast<const Dof&>(dofs[n-1]);}
    virtual bool include(const Space*) const;                //!< return true if space is included in current space
    virtual ValueType valueType() const;                     //!< return value type of basis function (real or complex)
    virtual StrucType strucType() const;                     //!< return structure type of basis function (scalar or vector)
    virtual bool isSpectral() const;                         //!< true if spectral space
    virtual bool isFE() const;                               //!< true if FE space or FeSubspace

    //specific virtual functions to FE type space (FeSpace and FeSubSpace), error for SpSpace
    virtual number_t nbOfElements() const                             //! number of elements for FESpace and FESubspace only
      {error("not_handled","nbOfElements()");return 0;}
    virtual const Element* element_p(number_t k) const                //! access to k-th element (pointer) for FESpace and FESubspace only
      {error("not_handled","element_p(k)");return nullptr;}
    virtual const Element* element_p(GeomElement* gelt) const         //! acces to element associated to a geomelement
      {error("not_handled","element_p(geom elt)");return nullptr;}
    virtual const std::vector<number_t>& elementDofs(number_t k) const //! access to dofs ranks (local numbering) of k-th element for FESpace and FESubspace only
      {error("not_handled","elementDofs()");return *new std::vector<number_t>();}
    virtual std::vector<number_t> elementParentDofs(number_t k) const //! access to dofs ranks (parent numbering) of k-th element for FESpace and FESubspace only
      {error("not_handled","elementParentDofs()");return std::vector<number_t>();}
    virtual const std::set<RefElement*>& refElements() const          //! access to set of pointer to RefElement (refElts) involved in FESpace
     {error("not_handled","refElements()");return *new std::set<RefElement*>();}

    void shiftDofs(number_t n);                             //!< shift dofs numbering from n (special utility for SpSpace)

    //utilities
    //-------------------------------------------------------------------
    template<typename T>
    T& spectralFun(number_t, const Point&, T&) const;         //!< compute the value of the n-th function at a point in case of spectral space
    template<typename T>
    Vector<T>& spectralFun(const Point&, Vector<T>&) const; //!< compute the values of all functions at a point in case of spectral space

    virtual void print(std::ostream&) const;               //!< print utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
};

//------------------------------------------------------------------------------------
//  templated member functions
//------------------------------------------------------------------------------------
//compute the value of the n-th function at a point in case of spectral space
template<typename T>
T& SpSpace::spectralFun(number_t n, const Point& P, T& res) const
{
  if(typeOfSpace() != _spSpace)
  {
    where("SpSpace::spectralFun(...)");
    error("space_nofun", name());
  }
  SpectralBasis* sp=static_cast<SpSpace*>(space_p)->spectralBasis();
  if(sp->funcFormType()!=_analytical)
  {
    where("SpSpace::spectralFun(...)");
    error("fun_not_analytical");
  }
  static_cast<SpectralBasisFun*>(sp)->function(n,P,res);
  return res;
}

//compute the values of all functions at a point in case of spectral space
template<typename T>
Vector<T>& SpSpace::spectralFun(const Point& P, Vector<T>& res) const
{
  if(typeOfSpace() != _spSpace)
  {
    where("SpSpace::spectralFun(...)");
    error("space_nofun", name());
  }
  SpectralBasis* sp=static_cast<SpSpace*>(space_p)->spectralBasis();
  if(sp->funcFormType()!=_analytical)
  {
    where("SpSpace::spectralFun(...)");
    error("fun_not_analytical");
  }
  static_cast<SpectralBasisFun*>(sp)->functions(P,res);
  return res;
}

} // end of namespace xlifepp

#endif // SP_SPACE_HPP
