/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SpSpace.cpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 31 jul 2012

  \brief Implementation of xlifepp::SpSpace class members and functionnalities
*/

#include "SpSpace.hpp"
#include "SubSpace.hpp"

namespace xlifepp
{
//explicit constructor of SpSpace
SpSpace::SpSpace(const string_t& na, const GeomDomain& g, number_t n, dimen_t dC, SpectralBasis* sb)
{
  if(sb!=nullptr)   spaceInfo_p = new SpaceInfo(na, &g, Hinf,  sb->dimFun(), _spSpace);
  else   spaceInfo_p = new SpaceInfo(na, &g, Hinf,  1, _spSpace);
  spectralBasis_ = sb;
  space_p = this;
  global=false;
  //construction of dofs, trivial numbering 1-> n
  dofs.reserve(n);
  for(number_t i = 1; i <= n; i++) { dofs.push_back(SpDof(*this, i));}
}

SpSpace::SpSpace(const string_t& na, const GeomDomain* g, number_t n, dimen_t dC, SpectralBasis* sb)
{
  if(sb!=nullptr) spaceInfo_p = new SpaceInfo(na, g, Hinf,  sb->dimFun(), _spSpace);
  else spaceInfo_p = new SpaceInfo(na, g, Hinf,  1, _spSpace);
  spectralBasis_ = sb;
  space_p = this;
  global=false;
  //construction of dofs, trivial numbering 1-> n
  dofs.reserve(n);
  for(number_t i = 1; i <= n; i++) { dofs.push_back(SpDof(*this, i));}
}

//destructor of SpSpace
SpSpace::~SpSpace()
{
  delete spectralBasis_;
}

//check type of Space
bool SpSpace::isSpectral() const
{
  return true;
}

bool SpSpace::isFE() const
{
  return false;
}

// return the DoF ids on space
std::vector<number_t> SpSpace::dofIds() const
{
  std::vector<number_t> ids(dofs.size());
  std::vector<SpDof>::const_iterator itd = dofs.begin();
  std::vector<number_t>::iterator it = ids.begin();
  for(; it != ids.end(); it++, itd++) *it = itd->id();
  return ids;
}

// shift dofs numbering from n (special utility for SpSpace)
void SpSpace::shiftDofs(number_t n)
{
  std::vector<SpDof>::iterator it = dofs.begin();
  for(; it != dofs.end(); it++) it->id()+=n;
}

ValueType SpSpace::valueType() const                          // return value type of basis function (real or complex)
{
  if(spectralBasis_!=nullptr) return spectralBasis_->valueType();
  return _real;
}

StrucType SpSpace::strucType() const                         // return structure type of basis function (scalar or vector)
{
  if(spectralBasis_!=nullptr) return spectralBasis_->strucType();
  return _scalar;
}

//print specific space attributes
void SpSpace::print(std::ostream& out) const
{
  if (theVerboseLevel > 0)
  {
    printSpaceInfo(out);        //print spaceinfo
    if (theVerboseLevel > 1)
    {
      if (spectralBasis_!=nullptr)
      {
        out << std::endl;
        spectralBasis_->print(out);
      }  //print specific info of SpSpace
    }
  }
  if (theVerboseLevel > 2)     //print list of Dofs
  {
    out << "\nList of Dofs:";
    for(number_t i = 0; i < dofs.size(); i++) { out << "\n" << dofs[i]; }
  }
}

// return true if (sub)space is included in current space
// true cases: a subspace of SpSpace or same SpSpace's
bool SpSpace::include(const Space* sp) const
{
  if(sp->typeOfSpace() == _spSpace) return (sp->spSpace() == this);
  if(sp->typeOfSpace() == _subSpace)
    {
      const SubSpace* subsp = sp->subSpace();
      if(subsp->rootSpace() == static_cast<const Space*>(this)) return true;
    }
  return false;
}

} // end of namespace xlifepp

