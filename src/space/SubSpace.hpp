/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SubSpace.hpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 13 jun 2013

  \brief Definition of the xlifepp::SubSpace class inherited from xlifepp::Space class

  It describes a sub space of a space, in terms of dofs:
    - a parent space ( a xlifepp::Space pointer), may be a xlifepp::SubSpace
    - a list of dof numbers (numbering related to parent) : vector<xlifepp::number_t>
  More information are defined in children.
*/

#ifndef SUB_SPACE_HPP
#define SUB_SPACE_HPP

#include "config.h"
#include "Space.hpp"
#include "Dof.hpp"

namespace xlifepp
{

//------------------------------------------------------------------------------------
/*!
   \class SubSpace
   defines data related to a 'subspace' of a space
   the subspace may be geometrically supported by a subset of geometrical domain
   or be a restriction to a boundary of the parent geometrical domain (trace space) or both
   The SubSpace class is mainly used as a dof numbering object in computation.
   SubSpace class is not exposed to end users
 */
//------------------------------------------------------------------------------------
class SubSpace : public Space
{
  protected:
    Space* parent_p;                                          //!< the parent space
    std::vector<number_t> dofNumbers_;                        //!< global numbers of D.o.Fs in parent D.o.Fs numbering

  public:

    mutable std::map<number_t, number_t> dofid2rank_;         //!< map dof iD -> rank in dofs vector
    void builddofid2rank() const;                             //!< construct the map dofid2rank from dofNumbers list

    //constructors/destructor
    SubSpace()                                                //! default constructor
    {parent_p = nullptr; space_p = nullptr; spaceInfo_p = nullptr; global=false;}
    SubSpace(const GeomDomain&, Space&, const string_t& na = "");   //!< constructor specifying geom domain, parent space and name
    SubSpace(const GeomDomain&, const std::vector<number_t>&, Space&,
             const string_t& na="");                            //!< constructor specifying geom domain, dof ids list and parent space and name
    SubSpace(const std::vector<number_t>&, Space&, const string_t& na="");  //!< constructor specifying dof ids list and parent space and name
    ~SubSpace();                                               //!< destructor

    //accessors
    Space* parent()const {return parent_p;}                   //!< returns parent space
    std::vector<number_t>& dofNumbers()                       //! access to dofNumbers list
    {return dofNumbers_;}
    const std::vector<number_t>& dofNumbers() const           //! access to dofNumbers list (const)
    {return dofNumbers_;}
    std::vector<number_t> dofRootNumbers() const;             //!< access to dofNumbers list in root space numbering
    number_t dofRootNumber(number_t k) const;                 //!< access to dof number in root space numbering
    virtual const SubSpace* subSpace()const {return this;}    //!< access to SubSpace from Space class (const)
    virtual SubSpace* subSpace() {return this;}               //!< access to SubSpace from Space class
    virtual number_t dimSpace()const                          //! dim of subspace (number of DoFs)
    {return dofNumbers_.size();}
    virtual number_t nbDofs()const                            //! number of dofs (a dof may be a vector dof)
    {return dofNumbers_.size();}
    virtual number_t dofId(number_t n) const                  //! return the DoF id of n-th space DoF (n=1,...)
    {return parent_p->dofId(dofNumbers_[n-1]);}
    virtual std::vector<number_t> dofIds() const;             //!< return the DoF ids on subspace
    virtual const Dof& dof(number_t n) const                  //! return n-th fedof as dof (n=1,...)
    {return parent_p->dof(dofNumbers_[n-1]);}
    virtual bool isFeSubspace() const {return false;}         //!< returns false (a subspace is not a fesubspace)
    virtual const FeSubSpace* feSubSpace() const              //! downcast to FESubSpace
    {error("downcast_failure","FeSubSpace");return nullptr;}
    virtual const Space* rootSpace() const                    //! access to root space pointer (const)
    {return parent_p->rootSpace();}
    virtual Space* rootSpace()                                //! access to root space pointer
    {return parent_p->rootSpace();}
    virtual ValueType valueType() const;                      //!< return value type of basis function (real or complex)
    virtual StrucType strucType() const;                      //!< return structure type of basis function (scalar or vector)
    virtual bool isSpectral() const;                          //!< true if spectral space
    virtual bool isFE() const;                                //!< true if FE space or FeSubspace
    virtual const std::vector<FeDof>& feDofs() const          //! return dofs vector
    {
      error("not_handled", "SubSpace::feDofs()");
      return *new  std::vector<FeDof>();
    }

    //various utilities
    virtual bool include(const Space *) const;                //!< return true if space is included in current subspace

    //specific virtual functions to FE type space (FeSpace and FeSubSpace), error for SubSpace
    virtual number_t nbOfElements() const                          //! number of elements for FESpace and FESubspace only
      {error("not_handled","SubSpace::nbOfElements()");return 0;}
    virtual const Element* element_p(number_t k) const             //! access to k-th element (pointer) for FESpace and FESubspace only
      {error("not_handled","SubSpace::element_p()");return nullptr;}
    //! access to dofs ranks (local numbering) of k-th element for FESpace and FESubspace only
    virtual const std::vector<number_t>& elementDofs(number_t k) const
      {error("not_handled","SubSpace::elementDofs()");return *new std::vector<number_t>();}
    //!access to dofs ranks (parent numbering) of k-th element for FESpace and FESubspace only
    virtual std::vector<number_t> elementParentDofs(number_t k) const
      {error("not_handled","SubSpace::elementParentDofs()");return std::vector<number_t>();}
    virtual const std::set<RefElement*>& refElements() const       //! access to set of pointer to RefElement (refElts) involved in FESpace
     {error("not_handled","SubSpace::refElements()");return *new std::set<RefElement*>();}
    void shiftDofs(number_t n)                             //! shift dofs numbering from n (special utility for SpSpace)
     {error("not_handled","SubSpace::shiftDofs()");}
    const std::map<number_t, number_t>&  dofid2rank() const  //! return the map dof to rank, built on fly
    {builddofid2rank(); return dofid2rank_;}

    //dof numbering functions
    void createNumbering();                                   //!< create numbering of subspace DoFs
    void dofsOfFeSubspace();                                  //!< build dofs numbering of Fe SubSpace
    void dofsOfSpSubspace();                                  //!< build dofs numbering of Sp SubSpace
    void dofsOfSubSubspace();                                 //!< build dofs numbering of Subspace of SubSpace

    //print and output facilities
    void print(std::ostream&) const;                         //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator <<(std::ostream&,const SubSpace&);
};

std::ostream& operator <<(std::ostream&,const SubSpace&); //!< print operator

} // end of namespace xlifepp

#endif // SUB_SPACE_HPP
