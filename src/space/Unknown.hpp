/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Unknown.hpp
  \author E. Lunéville
  \since 28 fev 2012
  \date 12 jun 2012

  \brief Definition of the xlifepp::Unknown class

  Class xlifepp::Unknown is a base class to store information for problem unknowns
  It is related to a discrete functions space (see class Space)
  TestFunction is an alias of Unknown
*/

#ifndef UNKNOWN_HPP
#define UNKNOWN_HPP

#include "config.h"
#include "Space.hpp"
#include "FeSpace.hpp"

namespace xlifepp
{
//forward declaration
class ComponentOfUnknown;
class EssentialCondition;
class TermVector;
//class OperatorOn;  //experimental

/*!
  \class Unknown
  end user's class to manage problem unknowns
 */
class Unknown
{
  protected:
    string_t name_;            //!< name of the unknown
    Space* space_p;            //!< pointer to the user's space where lives the unknown
    dimen_t nbOfComponents_;   //!< number of components of the unknown (default=1)
    mutable bool conjugate_;   //!< temporary flag for conjugate operation in OperatorOnUnknown construction (see operator lib)
    Unknown * dualUnknown_;    //!< dual unknown (test function)
    number_t rank_;            //!< rank to order unknowns

  public:
    bool isUnknown;             //!< true if it is an unknown, false if it is a test function (see constructor)
    mutable number_t nbEcShare = 0; //!< special counter used by the Essential condition machinery (ghost row unknown space)
    static number_t last_rank;  //!< last rank to set unique unknown rank
    static std::vector<Unknown*> theUnknowns;     //!< list of pointers of all Unknowns
    static void clearGlobalVector();              //!< delete all unknown objects
    static Unknown* findUnknown(const string_t&); //!< find unknown by its name
    static void printAllUnknowns(std::ostream&);  //!< print the list of Unknown objects in memory
    static void printAllUnknowns(PrintStream& os) {printAllUnknowns(os.currentStream());}

    // constructor/destructor
    Unknown();                                //!< default constructor

    void buildParam(const Parameter& p);
    void build(Space* sp, Unknown* u, bool iu, const std::vector<Parameter>& ps);
    Unknown(Space& sp, const Parameter& p1);
    Unknown(Space& sp, const Parameter& p1, const Parameter& p2);
    Unknown(Space& sp, const Parameter& p1, const Parameter& p2, const Parameter& p3);
    Unknown(Unknown& u, const Parameter& p1);
    Unknown(Unknown& u, const Parameter& p1, const Parameter& p2);
    Unknown(Unknown& u, const Parameter& p1, const Parameter& p2, const Parameter& p3);

    Unknown(Space& sp, const string_t& nm, dimen_t d=1, number_t r=0); //!< unknown constructor from space, d is the number of components (user), r the unknown rank
    Unknown(Space& sp, const char* nm, dimen_t d=1, number_t r=0); //!< unknown constructor from space, d is the number of components (user), r the unknown rank
    Unknown(const string_t& nm, Space& sp, dimen_t d=1, number_t r=0); //!< unknown constructor from space, d is the number of components (user), r the unknown rank
    Unknown(const string_t& na, Space* sp, dimen_t nc, bool co, Unknown* du, number_t ra, bool iu) //! full explicit constructor
    : name_(na), space_p(sp), nbOfComponents_(nc), conjugate_(co), dualUnknown_(du), rank_(ra), isUnknown(iu) {if(du!=nullptr) du->dualUnknown_=this;}
  protected:
    Unknown(Unknown& u,const string_t& na="", number_t r=0);       //!< copy constructor
    Unknown(Unknown& u,const char* na, number_t r=0);       //!< copy constructor
  public:
    virtual ~Unknown();

    // accessors and properties
    string_t name() const {return name_;}          //!< return name
    Space* space() const {return space_p;}         //!< return pointer to space
    UnknownType type() const;                      //!< return type of unknown
    StrucType strucType() const;                   //!< return the structure type of an unknown (scalar or vector)
    number_t index() const;                        //!< return index of unknown (its rank in the Unknows vector)
    bool conjugate()const {return conjugate_;}     //!< return the conjugate state (true/false) flag
    bool& conjugate() {return conjugate_;}         //!< return the conjugate state (true/false) flag
    void conjugate(bool v) const {conjugate_ = v;} //!< set the conjugate state flag
    const Unknown& dual() const;                   //!< return the dual unknown
    Unknown* dual_p() const                        //!  return the pointer to dual unknown
      { return dualUnknown_; }
    number_t rank() const                          //!  return the unknown rank (const)
      { return rank_; }
    void setRank(number_t);                        //!< set the rank with checking (user)
    virtual bool isTestFunction() const {return false;}
    // specific virtual functions to deal with ComponentOfUnknown (child class)
    virtual dimen_t nbOfComponents() const                //! dimension of unknown
      { return nbOfComponents_; }
    virtual bool isComponent() const                      //! true if a ComponentOfUnknown
      { return false; }
    virtual const Unknown* parent() const                 //! return parent (itself)
      { return this; }
    virtual dimen_t componentIndex() const                //! return component index if a ComponentOfUnknown, 0 if not
      { return 0; }
    virtual const ComponentOfUnknown* asComponent() const //! return Unknown as a ComponentOfUnknown if it is
      { return nullptr; }
    virtual dimen_t dimFun() const                        //! dimension of basis functions
      { return space_p->dimFun(); }
    bool isSpectral() const                               //! true if spectral unknown (spectral space)
      { return space_p->isSpectral(); }
    bool isFE() const                                     //! true if FE unknown (FE space)
      { return space_p->isFE(); }
    bool isLagrange() const
      { return space_p->typeOfSpace() == _feSpace && space_p->feSpace()->interpolation()->type==_Lagrange;}
    /*! order of polynom used by unknown (degree od basis), decreased by order of derivation
    as we use numtype (order of interpolation) we distinguish Pk or Qk using a ShapeType argument*/
    number_t degree() const;                              //! if FEspace, max degree of polynoms else return 5
    bool extensionRequired() const                        //! true if unknown space has no trace space
      { return space_p->extensionRequired();}

    // create component of unknown
    Unknown& operator[](dimen_t);         //!< create in memory the ComponentOfUnknown u_i
    // Unknown& operator()(dimen_t i)        //! create in memory the ComponentOfUnknown u_i
    //   { return (*this)[i]; }
    // Unknown& operator()(dimen_t, dimen_t =0, dimen_t =0,
    //                    dimen_t =0, dimen_t=0, dimen_t=0);  //!< create in memory the ComponentsOfUnknown u_i_j_k_l_m_n (up to 6)
    // OperatorOn& operator()(VariableName) const;   //experimental

    //Equation constructor (implemented in LcOperatorOnUnknown.cpp in operator lib)
    EssentialCondition operator = (const real_t &);          //!< construct equation u = r
    EssentialCondition operator = (const complex_t &);       //!< construct equation u = c
    EssentialCondition operator = (const Function &);        //!< construct equation u = f
    EssentialCondition operator = (const TermVector &);      //!< construct equation u = TermVector (implemented in TermVector.cpp)


    // utilities
    friend std::ostream& operator<<(std::ostream&, const Unknown&);
    friend Unknown& conj(Unknown&);
}; // end of class Unknown

std::ostream& operator<<(std::ostream&, const Unknown&); //!< print an unknown
Unknown& conj(Unknown&); //!< set to true or false the temporary conjugate flag

//useful typedefs
typedef std::vector<Unknown*>::iterator it_vu;
typedef std::vector<Unknown*>::const_iterator cit_vu;

//------------------------------------------------------------------------------------
//External related functions
//------------------------------------------------------------------------------------
Unknown* findUnknown(const string_t&); //!< find Unknown in list of Unknown
//@{
//! set ranks of unknowns
void setRanks(std::vector<Unknown*>&, const std::vector<number_t>&);
void setRanks(Unknown&, number_t r);
void setRanks(Unknown&, number_t, Unknown&, number_t);
void setRanks(Unknown&, number_t, Unknown&, number_t, Unknown&, number_t);
void setRanks(Unknown&, number_t, Unknown&, number_t, Unknown&, number_t, Unknown&, number_t);
//@}

typedef Unknown TestFct; //!< alias of Unknown

/*!
  \class TestFunction
  end user's class to manage problem test functions
 */
class TestFunction : public Unknown
{
  public:
    //! constructor from an xlifepp::Unknown
    TestFunction(Unknown& u, const Parameter& p1) : Unknown(u, p1) {}
    TestFunction(Unknown& u, const Parameter& p1, const Parameter& p2) : Unknown(u, p1, p2) {}
    TestFunction(Unknown& u, const Parameter& p1, const Parameter& p2, const Parameter& p3) : Unknown(u, p1, p2, p3) {}
    TestFunction(Unknown& u, const string_t& nm="", number_t r=0) : Unknown(u, nm, r) {}
    TestFunction(Unknown& u, const char* nm, number_t r=0) : Unknown(u, nm, r) {}
    //! constructor from a xlifepp::Space
    TestFunction(Space& sp, const string_t& nm="", dimen_t d=1, number_t r=0) : Unknown(sp, nm, d, r) {}
    TestFunction(Space& sp, const char* nm, dimen_t d=1, number_t r=0) : Unknown(sp, nm, d, r) {}
    TestFunction(const string_t& nm, Space* sp, dimen_t nc, bool co, Unknown* u, number_t ra, bool iu) //! full explicit constructor
    : Unknown(nm, sp, nc, co, u, ra, false) {}
    virtual bool isTestFunction() const {return true;}
};
inline Unknown& conj(TestFunction& v) { return conj(static_cast<Unknown&>(v)); }

/*!
  \class ComponentOfUnknown
  a class to manage a component of unknown (vector)
 */
class ComponentOfUnknown : public Unknown
{
  protected:
    const Unknown* u_p;  //!< Unknown which this unknown is a component
    dimen_t i_;          //!< index of the component
  public:
    ComponentOfUnknown(const Unknown&, dimen_t);            //!< constructor of ComponentOfUnknown
    virtual dimen_t nbOfComponents() const {return 1;}      //!< number of components=1
    virtual dimen_t dimFun() const  {return 1;}             //!< dimension of basis functions
    virtual bool isComponent() const {return true;}         //!< true if a ComponentOfUnknown
    virtual const Unknown* parent() const {return u_p;}     //!< return parent
    virtual dimen_t componentIndex() const {return i_;}     //!< return component index
    virtual const ComponentOfUnknown* asComponent() const   //! return Unknown as a ComponentOfUnknown
    {return this;}
    virtual bool isTestFunction() const
    {return u_p->isTestFunction();}
};

/*!
  \class ComponentsOfUnknown
  a class to manage components of unknown (vector)
  does not allow to manage all the components of an unknown
 */
class ComponentsOfUnknown : public Unknown
{
  protected:
    const Unknown* u_p;      //!< Unknown which this unknown is a component
    std::set<dimen_t> is_;   //!< index of the component (index starts from 1)
  public:
    ComponentsOfUnknown(const Unknown&, const std::set<dimen_t>&);  //!< constructor of ComponentOfUnknown from set of index
    ComponentsOfUnknown(const Unknown&, dimen_t,
              dimen_t =0, dimen_t =0, dimen_t =0, dimen_t =0, dimen_t =0);  //!< constructor of ComponentOfUnknown from up to 6 index
    void init(const Unknown&, const std::set<dimen_t>&);            //!< actual constructor

    virtual dimen_t nbOfComponents() const {return is_.size();}     //!< number of components=1
    virtual dimen_t dimFun() const  {return is_.size();}            //!< dimension of basis functions
    virtual bool isComponents() const {return true;}                //!< true if a ComponentsOfUnknown
    virtual const Unknown* parent() const {return u_p;}             //!< return parent
    virtual const ComponentsOfUnknown* asComponents() const         //! return Unknown as a ComponentOfUnknown
      {return this;}
    virtual const std::set<dimen_t>& componentIndices() const       //! return component indices as set of
      {return is_;}
};


//==================================================================================================
// to deal with collections of Unknown or TestFunction
//==================================================================================================

typedef PCollection<Unknown> Unknowns; //!< collection of Unknown pointers

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/* specialization of PCollectionItem
    when us is a Unknown's, allows to write us(i) = Unknown(...) with no hard copy
*/
template<> class PCollectionItem<Unknown>
{
public:
    std::vector<Unknown*>::iterator itp;
    PCollectionItem(std::vector<Unknown*>::iterator it) : itp(it){}
    Unknown& operator = (const Unknown& u)
       {
           //create a copy of u on the heap, shared pointers
           Unknown & nu=*new Unknown(u.name(), u.space(), u.nbOfComponents(), u.conjugate(),u.dual_p(), u.rank(), u.isUnknown);
           *itp=const_cast<Unknown*>(&nu);
           return **itp;
        }
    operator Unknown&(){return **itp;}  //autocast PCollectionItem->Unknown&
};

#endif

typedef PCollection<TestFunction> TestFunctions; //!< collection of TestFunction pointers

//! to build a list of dual xlifepp::TestFunction of a list of xlifepp::Unknown
inline TestFunctions dualOf(const PCollection<Unknown>& us, const Strings& names=Strings())
{
  Strings na=names;
  if (na.size() == 0) { na.resize(us.size()); }
  number_t n=na.size();
  if (us.size() > n)
  {
    for (number_t i=n; i < us.size(); ++i) { na.push_back(us[i]->name()); }
  }
  for (number_t i=0; i < us.size(); ++i)
  {
    if (na[i] == string_t()) { na[i] = us[i]->name()+"*"; }
  }
  // new testfunction's are created on the heap and never deleted !
  TestFunctions vs;
  std::vector<Unknown*>::const_iterator itu=us.begin();
  Strings::const_iterator itn=na.begin();
  for (;itu!=us.end();++itu,++itn) vs<<(*new TestFunction(**itu, _name=*itn));
  return vs;
}

} // end of namespace xlifepp

#endif // UNKNOWN_HPP
