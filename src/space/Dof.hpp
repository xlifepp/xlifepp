/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Dof.hpp
  \author E. Lunéville
  \since 25 fev 2012
  \date 12 jun 2012

  \brief Definition of the classes related to dof (degree of freedom)

  xlifepp::Dof is a general class for inherited specialized classes:

  \verbatim
         child
  Dof ----------- | FeDof   (finite element DoF)
                  | SpDof   (spectral DoF)
  \endverbatim

  A xlifepp::Dof is a generalized index in a space. In its basic form it is a numeric index
  but it may carries other informations (in particular in case of finite element spaces)
*/

#ifndef DOF_HPP
#define DOF_HPP

#include "config.h"
#include "finiteElements.h"

#include <iostream>

namespace xlifepp
{
// forward defined in space.hpp element.hpp
class Space;
class FeSpace;
class SpSpace;
class Element;
class Unknown;

/*!
   \class Dof
   generalized index in a space
 */
class Dof
{
  protected:
    number_t id_;           //!< id of dof
    DofType doftype_;       //!< type of dof (_feDof, _spDof)

  public:
    //constructor/destructor
    Dof(DofType t = _otherDof, number_t i = 0) //! constructors
      : id_(i), doftype_(t) {}
    virtual~Dof() {} //!< destructor

    //accessors
    DofType dofType()const {return doftype_;} //!< read access to type of dof
    number_t id() const {return id_;}         //!< access to dof id (read)
    number_t& id() {return id_;}              //!< access to dof id (read/write)
    virtual Point& coords() const             //! access to dof coordinates when exists (FE Lagrange dof)
    {error("dof_no_coords"); return *new Point();}
    virtual bool hasCoords() const            //! true if coords has been set
    {return false;}
    virtual dimen_t dimFun() const            //! dof dimension (shape function dim)
    {return 1;}
    virtual bool isPonctual() const           //! true if a dof with ponctual support
    {return false;}
    virtual real_t coords(const dimen_t i) const   //! access to the i-th dof coordinates (i=1,2,...) when exists (FE Lagrange dof)
      {error("dof_no_coords"); return 0.;}
    virtual void setCoords() const             //! compute coordinates of dof if defined
      {return;}
    virtual Space* space() const
      {error("dof_no_space"); return nullptr;}
    virtual Value operator()(const Function& f, const Function& gf=Function(), const Function& g2f=Function()) const  //!< eval dof (viewed as a distribution) at a function f (gf, g2f may be required)
    {error("free_error","eval dof at a function not available"); return Value(0);}

    //print utilities
    virtual string_t dofData(number_t w=0) const {return "";} //!< return dof data as string_t
    virtual void print(std::ostream&) const; //!< printing utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const Dof&);
};

std::ostream& operator<<(std::ostream&, const Dof&); //!< print operator

bool operator==(const Dof&, const Dof&);      //!< equality between dofs
bool operator< (const Dof&, const Dof&);      //!< order between dofs

/*!
   \class FeDofExtension
   virtual class to store extra informations on FeDof
 */
class FeDofExtension
{
  public:
    virtual ~FeDofExtension() {}      //!< destructor
    virtual string_t type() const =0; //!< type of extension
    virtual void print(std::ostream&) const=0;     //!< printing utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    virtual ShapeValues computeShapeValue(const Point&, const Element& elt, bool der1, bool der2, Vector<real_t>* =nullptr) const //! compute the shape value associated to current dof
    {error("free_error"," in FeDofExtension::computeShapeValue no computing function"); return ShapeValues();}
};

//------------------------------------------------------------------------------------
/*!
   \class FeDof
   to store finite element global dof informations
 */
//------------------------------------------------------------------------------------
class FeDof : public Dof
{
  protected:
    FeSpace* space_p;           //!< pointer to FeSpace
    const RefDof*  refDof_p;    //!< pointer to a Reference D.o.F
    bool shared_;               //!< implies D.o.F is shared between elements
    Vector<real_t> derVector_;  //!< direction vector(s) of a derivative D.o.F (specific interpolation)
    Vector<real_t> projVector_; //!< direction vector(s) of a projection D.o.F (specific interpolation)
    mutable Point coords_;      //!< coordinates of the FeDof, built if required by setCoords
    std::vector<std::pair<Element*, number_t> > inElements; //!< list of elements related to Dof and local number of dof in elements

  public:
    mutable FeDofExtension* dofExt_;    //!< additional information for non standard FE dof (e.g Buffa-Christiansen dof)

  public:
    FeDof()                                                                   //! default constructor
      : Dof(_feDof, 0), space_p(nullptr), refDof_p(nullptr), shared_(false), dofExt_(nullptr) {}
    FeDof(FeSpace& sp, const RefDof* rd = nullptr, number_t i = 0, bool sh = false) //!  basic constructor
      : Dof(_feDof, i), space_p(&sp), refDof_p(rd), shared_(sh), dofExt_(nullptr) {}

    const RefDof*  refDofP() const           //! return pointer to a Reference D.o.F
      {return refDof_p;}
    bool hasCoords() const                   //! true if coords has been set
      {return coords_.size()>0;}
    Point& coords() const;                   //!< return coordinates of FeDof
    real_t coords(const dimen_t i) const;    //!< return the i-th coordinates of FeDof
    dimen_t dim() const;                     //!< mesh space dimension
    dimen_t dimFun() const                   //! dof dimension (shape function dim)
      {return refDof_p->dim();}
    void inElement(const std::pair<Element*, number_t>& pen)  //!  add pair of (Element*,number_t) to inElements vector
      {inElements.push_back(pen);}
    const std::vector<std::pair<Element*, number_t> >& elements() const
       {return inElements;}
    DofLocalization where() const            //! get DoF hierarchic localization ( _onVertex, _onEdge , _onFace, _onElement)
      {return refDof_p->where();}
    dimen_t supportDim() const { return refDof_p->supportDim();}

//    number_t supportNumber() const           //!return DoF localization support (vertex number, edge number, face number)
//      {return refDof_p->supportNumber();}
    ProjectionType typeOfProjection() const
      {return refDof_p->typeOfProjection(); }//! access to projection type (const)
    Vector<real_t>& projVector()             //! access to projection vector (non const)
      {return projVector_;}
    const Vector<real_t>& projVector() const //! access to projection vector (const)
      {return projVector_;}
    void setCoords() const;                  //!< compute coordinates of dof
    virtual bool isPonctual() const          //! true if a dof with ponctual support
      {return refDof_p->supportDim()==0;}
    virtual Space* space() const;

    //! compute the shape vaue related to FeDof, restricted for the moment to dof with extension (e.g. Buffa-Christiansen)
    virtual ShapeValues computeShapeValue(const Point& P, const Element& elt, bool der1, bool der2, Vector<real_t>* ns =nullptr) const
    {
        if(dofExt_==nullptr) error("free_error", "FeDof::computeShapeValue is reserved to FeDof with extension");
        return dofExt_->computeShapeValue(P, elt, der1, der2, ns);
    }
    //! eval dof (viewed as a distribution) at a function f, e.g. for a lagrange dof: d_i(f)=f(Mi)
    virtual Value operator()(const Function& f, const Function& gradf=Function(),const Function& grad2f=Function()) const;
    Value evalProjectDof(const Function& f, const Function& gradf, const Function& grad2f) const;

    virtual string_t dofData(number_t w=0) const;  //!< return id and coords as string_t
    virtual void print(std::ostream&) const;       //!< print dof information
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    virtual void printTab(std::ostream&) const;   //!< print dof information in table environment
    virtual void printTab(PrintStream& os) const {printTab(os.currentStream());}
    virtual void printTab2(std::ostream& os, number_t ti,number_t ts, number_t td,
                 number_t tt, number_t tw, number_t tc, number_t tp, number_t tn) const;  //!< print dof information in table environment
    virtual void printTab2(PrintStream& os, number_t ti,number_t ts, number_t td,
                 number_t tt, number_t tw, number_t tc, number_t tp, number_t tn) const
                 {printTab2(os.currentStream(),ti,ts,td,tt,tw,tc,tp,tn);}
};

//------------------------------------------------------------------------------------
/*!
   \class SpDof
   to store spectral dof specific informations
 */
//------------------------------------------------------------------------------------
class SpDof : public Dof
{
  protected:
    SpSpace* space_p;                        //!< pointer to SpSpace
  public:
    SpDof()                                  //! default constructor
      : Dof(_spDof, 0), space_p(nullptr) {}
    SpDof(SpSpace& sp, number_t i)           //! constructors
      : Dof(_spDof, i), space_p(&sp) {}
    virtual string_t dofData(number_t w=0) const;  //!< return some dof data as string_t
    virtual dimen_t dimDof() const;                //! dof dimension (shape function dim)
    virtual void print(std::ostream&) const; //!< printing utility
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    virtual Space* space() const;
};

//============================================================================================================
/*!
   \class DofComponent
   class handles a component of dof say (dof number, unknown, component number)
*/
//============================================================================================================
class DofComponent
{
public:
    const Unknown* u_p;       //!< unknown pointer link to ConstraintsDof
    number_t dofnum;          //!< Dof number in space
    dimen_t numc;             //!< component number if Dof refers to a vector unknown (1 if scalar unknown)

    DofComponent(const Unknown* u=nullptr, number_t dn=0, dimen_t nc=1) //! constructor
    : u_p(u), dofnum(dn), numc(nc) {}

    DofComponent dual() const; //!< returns dual of dof

    void print(std::ostream&) const; //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const DofComponent&);
};

std::ostream& operator<<(std::ostream&, const DofComponent&); //!< print operator

bool operator < (const DofComponent&, const DofComponent&);   //!< less than
bool operator ==(const DofComponent&, const DofComponent&);   //!< equality
bool operator !=(const DofComponent&, const DofComponent&);   //!< diffference
std::vector<DofComponent> createCdofs(const Unknown* u, const std::vector<number_t>& dofs); //!< create cdofs from unknown dofs
std::vector<DofComponent> dualDofComponents(const std::vector<DofComponent>& cdofs);        //!< create dual cdofs list from cdofs list
std::vector<number_t> renumber(const std::vector<DofComponent>& cdv1, const std::vector<DofComponent>& cdv2); //!< renumbering cdofs utility

/*!
  \class BCsub
  structure defining a part of BC shape function
*/
class BCsub
{
 public:
    const Element* elt_p;    // element supporting sub RT shape function
    number_t sub_elt_num;    // sub element number in element
    number_t edge_num;       // edge number in element associated to RT shape function
    real_t coef;             // coefficient related to sub RT shape function

    BCsub(const Element* e, number_t sn, number_t en, real_t c):
        elt_p(e), sub_elt_num(sn), edge_num(en), coef(c) {}
    ShapeValues& computeShapeValue(const Point& P, const Point&, const Point&, const Point&,
                                   real_t, bool der1, bool der2, ShapeValues& shs, Vector<real_t>* s=nullptr) const;
    void print(std::ostream&) const;     //!< printing utility
    friend std::ostream& operator<<(std::ostream&, const BCsub&);
};

/*!
   \class BCDof
   to store Buffa-Chritiensen global dof informations
  see xlife++ documention for details
 */
class BCDof : public FeDofExtension
{
 public:
     std::list<BCsub> bcsubs;
     string_t type() const {return "Buffa-Christiansen";}
     void add(const Element* e, number_t sn, number_t en, real_t c)
     {bcsubs.push_back(BCsub(e,sn,en,c));}
     virtual void print(std::ostream& out) const     //! printing utility
     {
         out<<"Buffa-Christiansen shape function: ";
         for(std::list<BCsub>::const_iterator it=bcsubs.begin();it!=bcsubs.end(); ++it)
            out<<(*it)<<" ";
     }
     virtual ShapeValues computeShapeValue(const Point&,  const Element&, bool der1, bool der2, Vector<real_t>* =nullptr) const; //!< compute the shape value associated to current BcDof
};

} // end of namespace xlifepp

#endif // DOF_HPP
