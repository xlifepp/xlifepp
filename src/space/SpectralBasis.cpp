/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file SpectralBasis.cpp
  \author E. Lunéville
  \since 20 fev 2012
  \date 12 jun 2012

  \brief Implementation of xlifepp::SpectralBasis class members and related functionnalities
*/

#include "SpectralBasis.hpp"

namespace xlifepp
{
//constructor with geomDomain and function
SpectralBasisFun::SpectralBasisFun(const GeomDomain& dom, const Function& f, number_t nbFun)
  : SpectralBasis(nbFun, f.dims().first, dom, f.typeReturned(), f.structReturned()), functions_(f,true)
{
  funcFormType_ = _analytical;
  if(functions_.params_p()!=nullptr && functions_.params().contains("basis size"))
    functions_.parameter("basis size")=nbFun;
  else
    functions_.addParam(*new Parameter(nbFun, "basis size"));
  if(functions_.params_p()!=nullptr && functions_.params().contains("basis index"))
    functions_.parameter("basis index")=number_t(1);
  else
    functions_.addParam(*new Parameter(1, "basis index"));
  if(functions_.params_p()!=nullptr && functions_.params().contains("derivative"))
    functions_.parameter("derivative")=number_t(_id);
  else
    functions_.addParam(*new Parameter(number_t(_id), "derivative"));
}

//constructor with no geomDomain and function
SpectralBasisFun::SpectralBasisFun(const Function& f, number_t nbFun)
{
  funcFormType_ = _analytical;
  domain_=nullptr;
  numberOfFun_=nbFun;
  dimFun_=f.dims().first;
  returnedType_=f.typeReturned();
  returnedStruct_=f.structReturned();
  //functions_=f;
  functions_.fullCopy(f);
  if (functions_.params_p()!=nullptr && functions_.params().contains("basis size"))
    functions_.parameter("basis size")=nbFun;
  else
    functions_.addParam(*new Parameter(nbFun, "basis size"));

  if (functions_.params_p()!=nullptr && functions_.params().contains("basis index"))
    functions_.parameter("basis index")=number_t(1);
  else
    functions_.addParam(*new Parameter(1, "basis index"));

  if (functions_.params_p()!=nullptr && functions_.params().contains("derivative"))
    functions_.parameter("derivative")=number_t(_id);
  else
    functions_.addParam(*new Parameter(number_t(_id), "derivative"));
}

// copy constructor (full copy of functions_ to be safe)
SpectralBasisFun::SpectralBasisFun(const SpectralBasisFun & sbf)
: SpectralBasis(sbf)
{
  functions_ = Function(sbf.functions_,true);   //full copy
}

// assign operator (full copy of functions_ to be safe)
SpectralBasisFun& SpectralBasisFun::operator = (const SpectralBasisFun & sbf)
{
    numberOfFun_=sbf.numberOfFun_;
    dimFun_=sbf.dimFun_;
    domain_=sbf.domain_;
    returnedType_=sbf.returnedType_;
    returnedStruct_=sbf.returnedStruct_;
    funcFormType_=sbf.funcFormType_;
    functions_ = Function(sbf.functions_,true);   //full copy
    return *this;
}


void SpectralBasisFun::print(std::ostream& out) const
{
  out << message("spectral_fun_def", functions_.name());
  out << " (nbfun="<<numberOfFun_<<", dimFun="<<dimFun_<<")";
  out << "\n       Parameters: " << functions_.params();
}

std::ostream& operator<<(std::ostream& os,const SpectralBasis& spb)
{
    spb.print(os);
    return os;
}



} // end of namespace xlifepp

