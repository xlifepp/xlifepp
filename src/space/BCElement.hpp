/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file BCElement.hpp
  \authors E. Lunéville
  \since 28 nov 2017
  \date 28 nov 2017

  \brief Definition of  Buffa-Christiansen elements

  Buffa-Christiansen elements are defined on the dual mesh of a mesh
  This file provides the definition of class

        BCRTElement: dual of the RT element (2d)

  It inherits from the ExtendElement virtual class that is referenced by Element class
  Note that BC element does not use reference element

*/

// ############################## IN PROGRESS ########################################

#ifndef BCELEMENT_HPP
#define BCELEMENT_HPP

namespace xlifepp
{

/*! ExtendedElement class is a virtual class handling elements that are not standard
    for instance, Buffa-Christiansen elements
    ExtendElement objects are referenced by Element objects
*/
class ExtendedElement
{
  public:
    virtual string_t name()const =0 ;
    virtual ~ExtendedElement(){}
};

//! class describing a part of BC-RT shape function: sub RT shape function
class BCRTShapeFunctionPart
{
public:
  const GeomElement* gelt_p;//!< pointer to element supporting sub RT shape function
  number_t sub_elt_num;     //!< sub element number in element
  number_t edge_num;        //!< edge number related to sub RT shape function
  real_t coeff;             //!< coefficient related to sub RT shape function
  BCRTShapeFunctionPart(const GeomElement* ge, number_t sen, number_t en, real_t c)
  : gelt_p(ge),sub_elt_num(sen), edge_num(en), coeff(c) {}
};

//! class describing a BC-RT shape function that is a linear combination of RT shape function defined on sub triangle
class BCRTShapeFunction
{
public:
  std::list<BCRTShapeFunctionPart> shapeParts;
};

/*! class describing a BC-RT element considered as an element defined on a triangle of the primal mesh
    in this interpretation, element has inner dofs and outer dofs
*/

class BCRTElement : public ExtendedElement
{
public:
    /*! dofs numbers ordered as follows
        inner dofs "located" on triangle edges (3)
        outer dofs  located on edges connected to vertex i (except triangle edges)
    */
    std::vector<BCRTShapeFunction> shapeFunctions;

    void addShapePart(number_t i, const GeomElement* ge, number_t sen, number_t en, real_t c)
    {shapeFunctions[i].shapeParts.push_back(BCRTShapeFunctionPart(ge,sen,en,c));}

    virtual string_t name() const {return "BC-RT element";}
    // the following function 1. leads to compilation error and 2. is unused ==> commented
//    virtual ShapeValues computeShapeValues(const Point&, bool withderivative,
//                                           Vector<real_t>* =nullptr) const {}  //!< compute BCRT shape values at a point
};

} // end of namespace xlifepp

#endif // BCELEMENT_HPP
