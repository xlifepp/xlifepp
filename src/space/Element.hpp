/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Element.hpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 14 apr 2012
  \date 5 jul 2013

  \brief Definition of class xlifepp::Element which handle a finite element

  This is:
    - a geometric element supporting the finite element
    - a reference finite element supporting the finite element
    - a dof numbering
    - the finite element space it is linked

  When the element is supported by a side, list of parent elements is set
  the side number being known from the geometric element support
*/

#ifndef ELEMENT_HPP
#define ELEMENT_HPP

#include "config.h"
#include "finiteElements.h"
#include "Dof.hpp"
#include "SpectralBasis.hpp"
#include "geometry.h"
#include "BCElement.hpp"

namespace xlifepp
{

class FeSubSpace;

/*!
   \class Element
   defines data related to one finite element.
   It is mainly defined from its geometric support (GeomElement)
   and its finite element interpolation defined by a RefElement
   It carries also its dof numbering relative to

 */
class Element
{
  protected:
    FeSpace* feSpace_p;                  //!< pointer to Finite Element parent space
    number_t number_;                    //!< element number in FeSpace

  public:
    FeSubSpace* feSubSpace_p;              //!< pointer to Finite Element parent sub space if it belongs to a FESubSpace (0 if not)
    RefElement* refElt_p;                  //!< pointer to associated reference element object
    GeomElement* geomElt_p;                //!< pointer to Geometric Element support
    std::vector<number_t> dofNumbers;      //!< global numbering of element dofs in FeSpace dofs
    std::vector<Element*> parents_;        //!< parent elements when a side of element
    ExtendedElement* extElt_p;             //!< pointer to extension of element (0 by default)
    mutable Vector<real_t>* dofSigns_p;    //!< sign (+1/-1) of dofs, when void all sign equal to +1 (build on fly)
    mutable void* userData_p;              //!< a free pointer allowing users to attach data of any type to the Element (casting and memory management are the responsability of the users)

    Element(FeSpace*, number_t, RefElement*, GeomElement*, FeSubSpace* =nullptr);  //!< basic constructor
    virtual ~Element() {} //!< destructor (needs to be virtual)

    const FeSpace* feSpaceP() const                                          //! pointer to Finite Element parent space
    {return feSpace_p;}
    dimen_t dimElt() const                                                   //! dimension of the element
    {return refElt_p->geomRefElem_p->dim();}
    dimen_t dim() const                                                      //! dimension of node (space dimension)
    {return geomElt_p->spaceDim();}
    ShapeType shapeType() const                                              //! shape of the element
    {return geomElt_p->shapeType();}
    number_t number() const                                                  //! return number_ attribute
    {return number_;}
    FEType feType() const
    {return refElt_p->interpolation().type;}                                 //!< return FE type ( _Lagrange, _Hermite, ...)
    number_t feOrder() const
    {return refElt_p->interpolation().numtype;}                              //!< return FE numtype (order for Lagrange)
    dimen_t dimFun() const
    {return refElt_p->dimShapeFunction;}                                     //!< return dimension of shape functions

    //various utilities
    std::pair<Element*,number_t> parentSide()const;                                        //!< return first parent and side number if side element
    number_t refDofNumber(number_t) const;                                                 //!< return reference dof number of n th dofs (implemented in FeSpace.cpp)
    void dofsRenumbered(const std::vector<number_t>&, std::vector<FeDof>&);                //!< renumber dofs
    Point toReferenceSpace(const Point& p) const;                                          //!< point in physical space to point to reference space (inverse map)
    virtual ShapeValues computeShapeValues(const Point&, bool refPoint, bool der1,
                                           bool der2, Vector<real_t>* =nullptr) const;           //!< compute shape values at a reference or physical point
    Vector<real_t> computeNormalVector(const Point&) const;                                //!< compute normal vector at a physical point
    const Vector<real_t>& normalVector() const;                                            //!< access to normal vector if computed
    Vector<real_t>& normalVector();                                                        //!< access to normal vector if computed (writable)
    splitvec_t splitO1(std::map<number_t,number_t>* renumbering) const; //!< split element in first order elements and return as a list of dof numbers

    bool signDof(Vector<real_t>&) const;       //!< dof sign correction for interpolation involving normal or tangential dofs (RT, Nedelec, ...)
    void setDofSigns() const;                  //!< set dof sign correction for interpolation involving normal or tangential dofs (RT, Nedelec, ...)
    Vector<real_t>& getDofSigns() const;       //!< get dof sign correction for interpolation involving normal or tangential dofs (RT, Nedelec, ...)
    void mapShapeValues(GeomMapData&, number_t ord, dimen_t dimfun, Vector<real_t>*,
                        ShapeValues&, ShapeValues&) const;     //!< map shapevalues
    PolynomialsBasis shapeFunctions() const;                   //!< return shape functions as polynomials (if available)

    //interpolation tools
    template<typename T>
    T& interpolate(const Vector<T>&, const Point&, const std::vector<number_t>&, T&,
                   DiffOpType =_id) const;                                                 //!< compute interpolated value at P, scalar to scalar and vector to vector cases
    template<typename T>
    Vector<T>& interpolate(const Vector<T>&, const Point&, const std::vector<number_t>&,
                           Vector<T>&,  DiffOpType =_id) const;                           //!< compute interpolated value at P, scalar to vector case
    template<typename T>
    T& interpolate(const Vector<Vector<T> >&, const Point&, const std::vector<number_t>&,
                   T&, DiffOpType = _id) const;                                   //!< compute interpolated value at P, vector to scalar case
    template<typename T>
    T& interpolate(const VectorEntry&, const Point&, const std::vector<number_t>&, T&,  DiffOpType =_id) const;         //!< compute interpolated at P

    //print tool
    void print(std::ostream&) const;                                //!< print utility
    void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream&, const Element&);
};

std::ostream& operator<<(std::ostream&, const Element&); //!< print operator

//-----------------------------------------------------------------------------------------------------------------
//  templated functions
//-----------------------------------------------------------------------------------------------------------------
/*! compute interpolated value at P from vector of dof coefficients in global numbering:
       vint = sum_i v(I) d_wi(P)   i local dof number, I global dof number, say dofNumers(i)
    v: dof coefficients in global numbering of type K
    P: point in physical space where to interpolate
    d: differential operator: _id -> value, _dx -> first derivative of value, ...
    vint: interpolated value of type T (returned)
    dofnum: numbering of element dofs relative to dof coefficients vector

    Note: P has to belong to element (no test here), use MeshDomain::locate to find element containing a point

    There are 4 cases:
        coefficient v and result vint are scalar (Lagrange scalar)
        coefficient v is scalar and  result vint is vector (e.g edge element)
        coefficient v is vector and  result vint is scalar (case of difop vector-> scalar, e.g div)
        coefficient v and result vint are vector (Lagrange vector)
     vint has to be of the right type !

    Recall that shapevalues are always stored in a continuous vector
*/

//scalar -> scalar and vector -> vector cases
template<typename T>
T& Element::interpolate(const Vector<T>& v, const Point& P, const std::vector<number_t>& dofnum, T& vint, DiffOpType d) const
{
  vint=v[0];  // to get correct size
  vint*=0;    // init from 0
  std::vector<number_t>::const_iterator itd=dofnum.begin(), ite=dofnum.end();
  if(d==_id)
    {
      ShapeValues shv=computeShapeValues(P,false,false,false);  // P in physical space !
      std::vector<real_t>::iterator its=shv.w.begin();
      for(; itd!=ite; itd++, its++)
        vint+= v[*itd-1]* *its;
      return vint;
    }
  if(d<=_d3)  //simple derivative
    {
      ShapeValues shv=computeShapeValues(P,false,true,false);
      std::vector<real_t>::iterator its= shv.dw.begin()->begin();
      for(; itd!=dofNumbers.end(); itd++, its++)
        vint+= v[*itd-1]* *its;
      return vint;
    }

  where("Element::interpolate(...)");
  error("order01_derivative_op_only");
  return vint;
}

//scalar -> vector case
template<typename T>
Vector<T>& Element::interpolate(const Vector<T>& v, const Point& P, const std::vector<number_t>& dofnum, Vector<T>& vint, DiffOpType d) const
{
  dimen_t dif=dimFun();
  dif+=dim()-dimElt();  // correct the dimension of shape function if mapping from 2d->3d
  vint.resize(dif);     // set size of result
  vint*=0;    // init from 0
  std::vector<number_t>::const_iterator itd=dofnum.begin(), ite=dofnum.end();
  if(d==_id)
    {
      ShapeValues shv=computeShapeValues(P,false,false,false);
      std::vector<real_t>::iterator its=shv.w.begin();
      for(; itd!=ite; ++itd)
        for(number_t i=0; i<dif; i++, ++its)
          vint[i]+= v[*itd-1]* *its;
      return vint;

    }
  if(d<=_d3)  //simple derivative
    {
      ShapeValues shv=computeShapeValues(P,false,true,false);
      std::vector<real_t>::iterator its= shv.dw.begin()->begin();
      for(; itd!=dofNumbers.end(); ++itd)
        for(number_t i=0; i<dif; i++, ++its)
          vint[i]+= v[*itd-1]* *its;
      return vint;
    }

  where("Element::interpolate(...)");
  error("order01_derivative_op_only");
  return vint;
}

//vector -> scalar case
template<typename T>
T& Element::interpolate(const Vector<Vector<T> >& v, const Point& P, const std::vector<number_t>& dofnum, T& vint, DiffOpType d) const
{
  error("not_yet_implemented","Element::interpolate, vector to scalar");
  return vint;
}

/*! compute interpolated values at P, using VectorEntry
    Note: automatic access to entries consistent with type T
*/
template<typename T>
T& Element::interpolate(const VectorEntry& v, const Point& P, const std::vector<number_t>& dofnum, T& vint, DiffOpType d) const
{
  Vector<T>* vp=v.entriesp<T>();
  if(vp==nullptr) error("null_pointer","entries");
  return interpolate(*vp,P,dofnum,vint,d);
}

enum AdjacentStatus {_notAdjacent=0, _adjacentByElement, _adjacentBySide, _adjacentBySideOfSide, _adjacentByVertex};
//! print operator for enum associated to a dictionary
std::ostream& operator<<(std::ostream& out, const AdjacentStatus& as);

/*!
    \class AdjacenceInfo
    structure to describe how two GeomElement or Element are geometrically related

    - dist: distance of elements (real_t)
    - nxn: cross product of unitary normals (real_t) to measure parallelism
    - status: one of AdjacentStatus (_notAdjacent,_adjacentByElement,_adjacentBySide,_adjacentBySideOfSide, _adjacentByVertex)
    - numbers: pair of numbers when they are adjacent: side numbers or side of side numbers or vertex numbers
    \note works only with underlying first order geometric element
*/
class AdjacenceInfo
{
  public:
    real_t dist;   //!< distance between elements
    real_t nxn;    //!< cross product of unitary normals (real_t) to measure parallelism
    AdjacentStatus status; //!< one of AdjacentStatus (_notAdjacent,_adjacentByElement,_adjacentBySide,_adjacentBySideOfSide, _adjacentByVertex)
    std::vector<number_t> sharedVertex1; //!< numbers of shared vertices
    std::vector<number_t> sharedVertex2; //!< numbers of shared vertices

    //@{
    //! constructor
    AdjacenceInfo() : dist(0.), nxn(0.), status(_notAdjacent) {}
    AdjacenceInfo(const Element& elt1, const Element& elt2, bool computeDist=true,bool computeNxN=true)
      : dist(0.), nxn(0.), status(_notAdjacent)
    {init(*elt1.geomElt_p, *elt2.geomElt_p, computeDist, computeNxN);}
    AdjacenceInfo(const GeomElement& elt1, const GeomElement& elt2, bool computeDist=true, bool computeNxN=true)
      : dist(0.), nxn(0.), status(_notAdjacent)
    {init(elt1, elt2, computeDist, computeNxN);}
    //@}

    //! true fonction used in constructors
    void init(const GeomElement& elt1, const GeomElement& elt2, bool computeDist, bool computeNxN);

    void print(std::ostream& out) const
    {out<<words("adjacence status",status)<<", distance = "<<dist<<" nxn = "<<nxn;}
    virtual void print(PrintStream& os) const {print(os.currentStream());}
    friend std::ostream& operator<<(std::ostream& out, const AdjacenceInfo& adi)
    {adi.print(out); return out;}
};

} // end of namespace xlifepp

#endif // ELEMENT_HPP
