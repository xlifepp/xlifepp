/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Element.cpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 14 apr 2012
  \date 5 jul 2013

  \brief Implementation of xlifepp::Element class members and related functions
*/

#include "Element.hpp"
#include "FeSpace.hpp"

namespace xlifepp
{

Element::Element(FeSpace* fes, number_t n, RefElement* ref, GeomElement* geo, FeSubSpace* fsb)
  : feSpace_p(fes), number_(n), feSubSpace_p(fsb), refElt_p(ref), geomElt_p(geo), extElt_p(nullptr), dofSigns_p(nullptr), userData_p(nullptr)
{
    if(refElt_p!=nullptr) dofNumbers.resize(refElt_p->refDofs.size());
}

// return first parent and side number if a side element
std::pair<Element*,number_t> Element::parentSide() const
{
  if(parents_.size()==0)
    {
      where("Element::parentSide");
      error("elt_not_side");
    }
  return std::pair<Element*,number_t>(parents_[0],geomElt_p->parentSides()[0].second);
}

//dof renumbering tool
void Element::dofsRenumbered(const std::vector<number_t>& renumberedDof, std::vector<FeDof>& feDofs)
{
  for(std::vector<number_t>::iterator dof_i = dofNumbers.begin(); dof_i != dofNumbers.end(); dof_i++)
    {
      number_t n=*dof_i - 1;
      feDofs[n].id() = renumberedDof[n]; // affect new D.o.F number to renumbered D.o.F
      *dof_i = renumberedDof[n];         // affect new D.o.F number
    }
}

/*! map shapevalues in physical space regarding some FE rules
    WARNING: mapdata has to be allocated previously, jacobian and normal have to be up to date */
void Element::mapShapeValues(GeomMapData& mapdata, number_t ord, dimen_t dimfun, Vector<real_t>* sign_p, ShapeValues& sh, ShapeValues& shmap) const
{
  if(&sh!=&shmap) shmap.assign(sh);
  FEMapType femt = refElt_p->mapType;
  bool rotsh = refElt_p->rotateDof, changeSign = false, mapsh = femt !=_standardMap || ord>0;
  if(refElt_p->dofCompatibility == _signDofCompatibility)
  {
      if(dofSigns_p==nullptr) setDofSigns();
      changeSign=dofSigns_p->size()!=0;
      sign_p = dofSigns_p;
  }

  if(!mapsh && !changeSign && !rotsh) return;

  MeshElement* melt = geomElt_p->meshElement();
  if(rotsh) refElt_p->rotateDofs(melt->verticesNumbers(), shmap, ord>0);    //rotate shape values
  if(mapsh)   //map the shapevalues according to FE map type
    {
      if((ord > 0 || femt==_covariantPiolaMap) && mapdata.inverseJacobianMatrix.size()==0)
        mapdata.invertJacobianMatrix(); //compute inverse jacobian matrix if require
      switch(femt) //shape values in physical space
        {
          case _contravariantPiolaMap: shmap.contravariantPiolaMap(shmap, mapdata, ord>0, ord>1); break;
          case _covariantPiolaMap: shmap.covariantPiolaMap(shmap, mapdata, ord>0, ord>1); break;
          case _MorleyMap: shmap.Morley2dMap (shmap, mapdata, ord>0, ord>1); break;
          case _ArgyrisMap: shmap.Argyris2dMap(shmap, mapdata, ord>0, ord>1); break;
          default: shmap.map(shmap, mapdata, ord>0, ord>1);
        }
    }
  if(changeSign) shmap.changeSign(*sign_p, dimfun, ord>0, ord>1);  //change sign of shape functions according to sign vector
}

/*! set dof sign correction for interpolation involving normal or tangential dofs (RT, Nedelec, ...)
    only sign correction for the moment (normal to side or tangent to edge dofs)
    for each dof: compute inner product between a reference normal/tangent given by global dof and local normal/tangent
                   and take its sign
    ### build geometric information before calling sign ###
 */
void Element::setDofSigns() const
{
  if(dofSigns_p!=nullptr) return; // signs are already computed
  if(refElt_p->dofCompatibility!=_signDofCompatibility) return;
  bool change=false;
  number_t dim=geomElt_p->elementDim();
  if(dim==1) return;  //nothing to do
  std::vector<number_t>::const_iterator itd=dofNumbers.begin();
  dofSigns_p=new Vector<real_t>(dofNumbers.size(),1.);
  Vector<real_t>::iterator its=dofSigns_p->begin();
  const FeSpace* fesp=feSpaceP();
  number_t i=1;
  for(; itd!=dofNumbers.end(); ++itd, ++its, i++)
    {
      const FeDof& fed = fesp->fedof(*itd);
      if(fed.typeOfProjection()==_dotnProjection)     // case of normal projection, assume dof on a side (edge in 2D, face in 3D)
        {
          number_t s = refElt_p->sideOf(i); //COSTLY, TO BE IMPROVED
          std::vector<real_t> ns = geomElt_p->normalVector(s);   //compute normal on side s (see MeshElement::normalVector(s))
          real_t sig = std::inner_product(ns.begin(),ns.end(),fed.projVector().begin(),0.);
          if(sig<0) {*its =-1.; change=true;}
        }

      if(fed.typeOfProjection()==_crossnProjection)  // case of tangential projection, assume dof on an edge (in 2D or 3D)
        {
          number_t s;
          if(dim==2) s = refElt_p->sideOf(i);        //2D case
          else       s = refElt_p->sideOfSideOf(i);  //3D case
          std::vector<real_t> ts = geomElt_p->tangentVector(s);   //compute tangent on edge s (edge_vertex2 -edge_vertex1)
          real_t sig =std::inner_product(ts.begin(),ts.end(),fed.projVector().begin(),0.);
          if(sig<0) {*its=-1.; change=true;}
        }
    }
    if(!change) dofSigns_p->clear();  // free memory when all signs are positive
}

/*! get dof signs, compute them if not computed
*/
Vector<real_t>& Element::getDofSigns() const
{
    if(dofSigns_p==nullptr) setDofSigns();
    return *dofSigns_p;
}

/*! dof sign correction for interpolation involving normal or tangential dofs (RT, Nedelec, ...)
   only sign correction for the moment (normal to side or tangent to edge dofs)
   for each dof: compute inner product between a reference normal/tangent given by global dof and local normal/tangent
                  and take its sign
   ### old method, keep it for non regressive ###
*/
bool Element::signDof(Vector<real_t>& signs) const
{
  bool change=false;
  number_t dim=geomElt_p->elementDim();
  if(dim==1) return false;  //nothing to do
  std::vector<number_t>::const_iterator itd=dofNumbers.begin();
  signs.resize(dofNumbers.size());
  Vector<real_t>::iterator its=signs.begin();
  for(; its!=signs.end(); ++its) *its = 1.;
  const FeSpace* fesp=feSpaceP();
  its=signs.begin();
  number_t i=1;
  for(; itd!=dofNumbers.end(); ++itd, ++its, i++)
    {
      const FeDof& fed = fesp->fedof(*itd);
      if(fed.typeOfProjection()==_dotnProjection)     // case of normal projection, assume dof on a side (edge in 2D, face in 3D)
        {
          number_t s = refElt_p->sideOf(i); //COSTLY, TO BE IMPROVED
          std::vector<real_t> ns = geomElt_p->normalVector(s);   //compute normal on side s (see MeshElement::normalVector(s))
          real_t sig = std::inner_product(ns.begin(),ns.end(),fed.projVector().begin(),0.);
          if(sig<0) {*its =-1.; change=true;}
        }

      if(fed.typeOfProjection()==_crossnProjection)  // case of tangential projection, assume dof on an edge (in 2D or 3D)
        {
          number_t s;
          if(dim==2) s = refElt_p->sideOf(i);        //2D case
          else       s = refElt_p->sideOfSideOf(i);  //3D case
          std::vector<real_t> ts = geomElt_p->tangentVector(s);   //compute tangent on edge s (edge_vertex2 -edge_vertex1)
          real_t sig =std::inner_product(ts.begin(),ts.end(),fed.projVector().begin(),0.);
          if(sig<0) {*its=-1.; change=true;}
        }
    }
  return change;
}

//! return physical shape functions of current element as polynomials (if available)
PolynomialsBasis Element::shapeFunctions() const
{
  const MeshElement* melt=geomElt_p->meshElement();
  if(melt==nullptr) melt=geomElt_p->buildSideMeshElement();
  PolynomialsBasis& pbref=refElt_p->Wk;
  if(pbref.size()==0 || !geomElt_p->meshElement()->linearMap)
    {
      where("Element::shapeFunctions()");
      error("no_shape_functions",refElt_p->name());
    }
  if(melt->geomMapData_p==nullptr) melt->geomMapData_p = new GeomMapData(melt); //data structure for geometric transform
  GeomMapData* mapdata = melt->geomMapData_p;
  if(mapdata->jacobianMatrix.size()==0) mapdata->computeJacobianMatrix(geomElt_p->center());
  mapdata->invertJacobianMatrix();
  // map coordinates  J^-1 x - J^-1 x0 +xr0
  dimen_t d=refElt_p->dim();
  std::vector<Polynomial> e(d);
  e[0]=Polynomial(Monomial(1,0,0));
  if(d>1) e[1]=Polynomial(Monomial(0,1,0));
  if(d>2) e[2]=Polynomial(Monomial(0,0,1));
  std::vector<Polynomial> c = mapdata->inverseJacobianMatrix* e;
  Vector<real_t> q=mapdata->inverseJacobianMatrix * Vector<real_t>(*melt->nodes[0]);
  std::vector<real_t>::const_iterator itv=refElt_p->geomRefElement()->vertices(); //first reference element vertex
  for(number_t i=0; i<d; i++, ++itv)  c[i]+=Polynomial(Monomial(0,0,0),*itv-q[i]);
  //build vector map
  Matrix<real_t> M;
  bool domap=false;
  switch(refElt_p->mapType)
    {
      case _covariantPiolaMap:    M=mapdata->covariantPiolaMap(); domap=true; break;
      case _contravariantPiolaMap: M=mapdata->contravariantPiolaMap(); domap=true; break;
      default: domap=false;
    }
  Vector<real_t> sign;
  bool changeSign=false;
  if(refElt_p->dofCompatibility == _signDofCompatibility) changeSign = signDof(sign);
  // build physical shape functions
  PolynomialsBasis pbphy(pbref);
  PolynomialsBasis::iterator itp=pbphy.begin();
  dimen_t dv = pbphy.dimVec;
  number_t k=0;
  for(; itp!=pbphy.end(); ++itp, ++k)
    {
      for(dimen_t i=0; i<dv; i++) //map coordinates
        {
          Polynomial& P=(*itp)[i];
          if(d==1) P=P(c[0]);
          if(d==2) P=P(c[0],c[1]);
          if(d==3) P=P(c[0],c[1],c[2]);
          if(changeSign && sign[k]<0)  P*=-1.;
        }
      if(domap) *itp= M* *itp;
    }
  return pbphy;
}

void Element::print(std::ostream& os) const
{
  if(theVerboseLevel <= 0) { return; }
  os << "- " << words("element") << " " << number_ << " -\n";
  //if(theVerboseLevel > 10 && refElt_p!=nullptr) { os << (*refElt_p); }
  if(refElt_p!=nullptr) os << words("reference element") << " " << refElt_p->name() << eol;
  os << words("DoF numbers") << ":";
  for(number_t i = 0; i < dofNumbers.size(); i++) { os << " " << dofNumbers[i]; }
  os << eol << *geomElt_p;
}

std::ostream& operator<<(std::ostream& os, const Element& Elt)
{
  Elt.print(os);
  return os;
}

// split mesh element in first order elements of same shape and return split elements in a list
splitvec_t Element::splitO1(std::map<number_t,number_t>* renumbering=nullptr) const
{
  splitvec_t listel;
  std::vector<number_t>::const_iterator itvn;
  if(refElt_p==nullptr)
    {
      where("Element::splitO1");
      error("split_no_ref_elt");
    }
  if(refElt_p->order() == 0)   //use geometric P1 support in this degenerated case, values are saved by cell
    {
      std::vector<number_t>::iterator itn;
      std::vector<number_t> nodeNumbers = geomElt_p->vertexNumbers();
      for(itn=nodeNumbers.begin(); itn != nodeNumbers.end(); itn++) *itn = (*renumbering)[*itn];
      listel.push_back(std::make_pair(refElt_p->shapeType(),nodeNumbers));
      return listel;
    }

  if(refElt_p->order() == 1)   //no new element, copy of pointer
    {
      std::vector<number_t> nodeNumbers;
      for(itvn=dofNumbers.begin(); itvn != dofNumbers.end(); itvn++)
        {
          if(renumbering!=nullptr) {nodeNumbers.push_back((*renumbering)[*itvn]);}
          else {nodeNumbers.push_back(*itvn);}
        }
      listel.push_back(std::make_pair(refElt_p->shapeType(), nodeNumbers));
      return listel;
    }
  // split element
  const std::vector<std::pair<ShapeType,std::vector<number_t> > >& subeltnum = refElt_p->getO1splitting();
  std::vector<std::pair<ShapeType,std::vector<number_t> > >::const_iterator ite;
  for(ite=subeltnum.begin(); ite!=subeltnum.end(); ite++)
    {
      std::vector<number_t> nodeNumbers;
      for(itvn=(ite->second).begin(); itvn != (ite->second).end(); itvn++)
        {
          number_t n=dofNumbers[*itvn-1];
          if(renumbering!=nullptr) {nodeNumbers.push_back((*renumbering)[n]);}
          else {nodeNumbers.push_back(n);}
        }
      listel.push_back(std::make_pair(ite->first,nodeNumbers));
    }
  return listel;
}

//! point in physical space to point to reference space (inverse map)
Point Element::toReferenceSpace(const Point& p) const
{
  MeshElement* melt = geomElt_p->meshElement();
  if(melt == nullptr) melt = geomElt_p->buildSideMeshElement();  //it is a side element with no meshElement structure, build one
  if(melt->geomMapData_p ==nullptr) melt->geomMapData_p= new GeomMapData(melt);
  return melt->geomMapData_p->geomMapInverse(p);          //point in reference space
}

/*! compute shape values at a point in ref space (if refPoint is true) in physical space else
    if the sign_p pointer =0 then sign dofs vector is computed here if required else pointer is used */
// #### second derivative not yet managed, to be done in futur
ShapeValues Element::computeShapeValues(const Point& p, bool refPoint, bool der1, bool der2, Vector<real_t>* sign_p) const
{
  if(!refElt_p->hasShapeValues) //special case of ref element with no shape value (e.g. Buffa-Christiansen)
    {
      number_t nd=dofNumbers.size();
      dimen_t dimf=refElt_p->dimShapeFunction;
      dimen_t dims=dim();  //space dim
      if(dimf>1 && dimf<dims) dimf=dims; //change if vector shape function and manifold (not working for 1D element, fix it in future)
      ShapeValues shv;
      shv.w.resize(nd*dimf);
      shv.dw.resize(dims,std::vector<real_t>(nd*dimf));
      std::vector<number_t>::const_iterator itn=dofNumbers.begin();
      number_t k=0;
      for(number_t n=0; n<nd; n++, ++itn, k+=dimf)
        {
          const FeDof& fd=feSpace_p->fedof(*itn);
          ShapeValues svn = fd.computeShapeValue(p,*this, der1, der2);
          for(number_t j=0; j<dimf; j++)
            {
              shv.w[k+j]=svn.w[j];
              if(der1)
                {
                  for(number_t d=0; d<dims; d++)
                    shv.dw[d][k+j]=svn.dw[d][j];
                }
            }
        }
      return shv;
    }

  if(refElt_p==nullptr || refElt_p->order()==0)   //shortcut for P0 interpolation or no ref element (0d element)
    {
      ShapeValues shv;
      shv.w=std::vector<real_t>(1,1.);
      return shv;
    }
  Point q = p;
  if(!refPoint) q = toReferenceSpace(p);                             //get point in reference space
  GeomMapData* gmap = geomElt_p->meshElement()->geomMapData_p;
  ShapeValues shv(*refElt_p,der1,der2);  // to size shv
  refElt_p->computeShapeValues(q.begin(), shv, der1, der2);   //shape values in reference space
  if(refElt_p->rotateDof) refElt_p->rotateDofs(geomElt_p->meshElement()->verticesNumbers(), shv, der1, der2);    //rotate shape values
  switch(refElt_p->mapType)                                 //map reference shapevalues to physical space
  {
    case _contravariantPiolaMap: shv.contravariantPiolaMap(shv, *gmap, der1, der2); break;
    case _covariantPiolaMap: shv.covariantPiolaMap(shv,*gmap, der1, der2); break;
    case _MorleyMap: shv.Morley2dMap (shv, *gmap, der1, der2); break;
    case _ArgyrisMap: shv.Argyris2dMap(shv, *gmap, der1, der2); break;
    default: shv.map(shv, *gmap, der1, der2);
  }
  //sign correction required by some non Lagrange FE (RT, Nedelec, ...)
  if(refElt_p->dofCompatibility == _signDofCompatibility)
    {
      bool chs = false;
      Vector<real_t> sign;
      if(sign_p==nullptr || (sign_p!=nullptr && sign_p->size()==0)) {chs=signDof(sign); sign_p=&sign;} //compute sign of local dofs
      else chs=true;
      if(chs) shv.changeSign(*sign_p, shv.w.size()/refElt_p->nbDofs(), der1, der2);  //change sign of shape functions according to sign vector
    }
  return shv;
}

//! compute normal vector at a physical point, force some recomputation
Vector<real_t> Element::computeNormalVector(const Point& p) const
{
  MeshElement* melt = geomElt_p->meshElement();
  if(melt == nullptr) melt = geomElt_p->buildSideMeshElement();  //it is a side element with no meshElement structure, build one
  if(melt->geomMapData_p ==nullptr) melt->geomMapData_p= new GeomMapData(melt);
  GeomMapData* gmap = melt->geomMapData_p;
  Point q=gmap->geomMapInverse(p);                   // point in reference space
  gmap->computeJacobianMatrix(q);                    // compute jacobian
  gmap->computeNormalVector();                       // compute unnormalized normal vector
  gmap->normalize();                                 // normalize normal vector (always an outward normal)
  return gmap->normalVector;
}

//! return  normal vector assuming already computed
const Vector<real_t>& Element::normalVector() const
{
  return geomElt_p->meshElement()->geomMapData_p->normalVector;
}

//! return  normal vector assuming already computed
Vector<real_t>& Element::normalVector()
{
  return geomElt_p->meshElement()->geomMapData_p->normalVector;
}

std::ostream& operator<<(std::ostream& out, const AdjacentStatus& as)
{ out << words("adjacence status", as); return out; }

void AdjacenceInfo::init(const GeomElement& elt1, const GeomElement& elt2, bool computeDist, bool computeNxN)
{
  dist=0.;
  status=_notAdjacent;
  std::vector<number_t>::iterator its;
  if(sharedVertex1.size()>0)
    for(its=sharedVertex1.begin(); its!=sharedVertex1.end(); ++its) *its=0;
  if(sharedVertex2.size()>0)
    for(its=sharedVertex2.begin(); its!=sharedVertex2.end(); ++its) *its=0;

  if(&elt1==&elt2) //same GeomElement
    {
      status=_adjacentByElement;
      return;
    }
  const MeshElement* melt1=elt1.meshElement(), *melt2=elt2.meshElement();
  if(melt1==nullptr) melt1=elt1.buildSideMeshElement();
  if(melt2==nullptr) melt2=elt2.buildSideMeshElement();
  bool sameMesh = elt1.meshP()==elt2.meshP();

  //general case
  dimen_t dimelt1=melt1->elementDim(), dimelt2=melt2->elementDim();
  number_t nbv1=melt1->numberOfVertices(), nbv2=melt2->numberOfVertices();
  number_t maxv=std::min(nbv1,nbv2);
  sharedVertex1.resize(maxv,0); sharedVertex2.resize(maxv,0);
  //find common vertices
  number_t c=0;
  if(sameMesh) // in same mesh, compare vertex numbers
    {
      const std::vector<number_t>& vs1=melt1->vertexNumbers;
      const std::vector<number_t>& vs2=melt2->vertexNumbers;
      std::vector<number_t>::const_iterator it1=vs1.begin(),it2b=vs2.begin(), it2;
      for(number_t i=1; i<=nbv1 && c<maxv; ++i, ++it1)
        {
          it2=it2b;
          for(number_t j=1; j<=nbv2 && c<maxv; ++j, ++it2)
            if(*it1 == *it2)
              {
                sharedVertex1[c]=i; sharedVertex2[c]=j; c++;
              }
        }
    }
  else //compare vertex coordinates
    {
      for(number_t i=1; i<= nbv1 && c<maxv; i++)
        {
          Point & v1=melt1->vertex(i);
          for(number_t j=1; j<= nbv2 && c<maxv; j++)
            if(v1.distance(melt2->vertex(j)) < theTolerance)
              {
                sharedVertex1[c]=i;  sharedVertex2[c]=j; c++;
              }
        }
    }
  if(c==0) // not adjacent
  {
    if(computeDist) //compute relative distance of elements
       dist = xlifepp::dist(melt1->centroid, melt2->centroid)/std::max(melt1->size,melt2->size);    //d=0 when adjacence !
    return;
  }
  //update adjacence status and relative distance of elements
  if(dimelt1==dimelt2)  //standard case
  {
   switch(c)
    {
      case 0: status=_notAdjacent; break;
      case 1: status=_adjacentByVertex; break;
      case 2:
        {
          if(dimelt1==1) status=_adjacentByElement;
          if(dimelt1==2) status=_adjacentBySide;
          if(dimelt1==3) status=_adjacentBySideOfSide;
        }
        break;
      default: //more than 3 common vertices
        {
          if(dimelt1==2) status=_adjacentByElement;
          if(dimelt1==3) status=_adjacentBySide;
        }
    }
   //compute nxn if required
   if(computeNxN && status!=_adjacentByElement)  //nxn = 0 when coplanar or colinear elements
   {
      where("AdjacenceInfo::init(...)");
      error("not_yet_implemented","AdjacenceInfo::init() (coplanar case)");
   }
   return;
  }

  // elements with different dimensions
  dimen_t mindim = std::min(dimelt1,dimelt2), maxdim=std::max(dimelt1,dimelt2);
  if(maxdim==mindim+1)
  {
    switch(c)
    {
      case 1: status=_adjacentByVertex; break;
      case 2:
        {
          if(maxdim==2) status=_adjacentBySide;
          if(maxdim==3) status=_adjacentBySideOfSide;
        }
        break;
      default: //more than 3 common vertices (mindim=2, maxdim=3)
        {
          status=_adjacentBySide;
        }
    }
    return;
  }
  where("AdjacenceInfo::init(...)");
  error("not_yet_implemented","AdjacenceInfo::init() (case of an element of dim 1 with an element of dim 3)");
}

} // end of namespace xlifepp
