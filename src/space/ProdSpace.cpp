/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file ProdSpace.cpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 31 jul 2012

  \brief Implementation of xlifepp::ProdSpace class members and functionnalities
*/

#include "ProdSpace.hpp"

namespace xlifepp
{
number_t ProdSpace::dimSpace()const
{
  number_t ns = spaces_.size(), d = 1;
  if(ns == 0) { return 0; }
  for(number_t i = 0; i < spaces_.size(); i++) { d *= spaces_[i]->dimSpace(); }
  return d;
}

number_t ProdSpace::nbdofs() const
{
  number_t ns = spaces_.size(), d = 1;
  if(ns == 0) { return 0; }
  for(number_t i = 0; i < spaces_.size(); i++) { d *= spaces_[i]->nbDofs(); }
  return d;
}

} // end of namespace xlifepp
