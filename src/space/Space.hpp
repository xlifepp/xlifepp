/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file Space.hpp
  \author E. Lunéville
  \since 24 fev 2012
  \date 13 jun 2013

  \brief Definition of the classes related to spaces

  This is the dicrete spaces defined by a geometrical domain (from mesh)
  and an interpolation (finite element or set of functions)

  xlifepp::Space is the parent class of inherited specialized classes:
  \verbatim
        pointer to        child
  Space ---------> Space -------| FeSpace   (finite element)
                                | SpSpace   (spectral basis)
                                | SubSpace  (subspace of a space) ----> FeSubSpace (specific subspace FE data)
                                | ProdSpace (tensor product of spaces)
  \endverbatim

  A xlifepp::Space object has a pointer to an other xlifepp::Space object which points to the "true" space
  It is a trick to avoid the end users to explicitely instanciate child space objects.
  The spaceInfo_p pointer points to a xlifepp::SpaceInfo object shared by the "user" space and the "true" space
  In this paradigm, any member function of space encapsulating child member function looks like

  \code
  virtual ret_arg Space::member_fun(args)
  {
    if (space_p != this) space_p->member_fun(args); // call the same with the "true" space
    else { ... }                                    // if the "true" space,
  }

  ret_arg ChildSpace::member_fun(args)              // the "true" member function
    { ... }
  \endcode

  Because of polymorphic behaviour, if a child do not propose member_fun it is the else block of xlifepp::Space::member_fun
  which is applied. Do not work with templated member function (no virtual template member function in C++) !!!

  Additionnal class:  xlifepp::SpaceInfo: general space information

  Space constructors are based on a key-value system. Here are the available keys :
  - _basis: for spectral spaces, it is the key devoted to define basis as functions (Function) or interpolated data (TermVectors)
  - _basis_dim: when spectral basis is defined by functions, it sets the dimension of functions
  - _dim: for spectral spaces or spaces on $\mathbb{R}^N$, it defnes the space dimension
  - _domain: the domain on which the space is defined
  - _interpolation (or _FE_type, _FE_subtype, _order, _Sobolev_type): keys devoted to the definition of the finite element
    interpolation
  - _name: the name of the space for print purpose
  _ _optimizeNumbering / _notOptimizeNumbering: flags to activate/deactivate numbering optimization (activated by default)
  - _withLocateData / _withoutLocateData: flags to activate the building of the tree used for localization of points (activated by default)
*/

#ifndef SPACE_HPP
#define SPACE_HPP

#include "config.h"
#include "Dof.hpp"
#include "geometry.h"
#include "SpectralBasis.hpp"

namespace xlifepp
{

//forward declarations
class FeSpace;
class SpSpace;
class SubSpace;
class FeSubSpace;
class TermVectors;

/*!
   \class SpaceInfo
   structure to store general information of a space
 */
class SpaceInfo
{
  public:
    string_t name;                //!< name of space
    const GeomDomain* domain_p;   //!< geometric domain supporting the space
    SobolevType spaceConforming;  //!< conforming space: L2 or H1 or Hdiv or Hcurl or H2 or Hinf
    dimen_t dimFun;               //!< number of components of the basis functions
    SpaceType spaceType;          //!< type of space definition (FESpace,SPSpace,SubSpace,ProductSpace,OtherSpace)
    SpaceInfo() {}                //!< default constructor
    //! general constructor
    SpaceInfo(const string_t& na, const GeomDomain* gd, SobolevType so, dimen_t dF, SpaceType st)
      : name(na), domain_p(gd), spaceConforming(so), dimFun(dF), spaceType(st) {}
};

/*!
   \class Space
   base class of spaces
 */
class Space
{
  protected:
    Space* space_p;                //!< pointer to the "true" space
    SpaceInfo* spaceInfo_p;        //!< pointer to the space information structure

    Space() {space_p=nullptr;spaceInfo_p=nullptr;}   //!< default constructor
    Space& operator=(const Space& V)
    { space_p=V.space_p; spaceInfo_p=V.spaceInfo_p; return *this; }   //!< assign operator (shared pointers!)

  public:
    mutable bool global;  //!< declare space as global, if true forbid deletion of pointer space_p

    Space(const Space& V) //! copy constructor (shared pointers!)
    { space_p=V.space_p; spaceInfo_p=V.spaceInfo_p; V.global=true; }
    virtual ~Space();     //!< destructor
    void printSpaceInfo(std::ostream& out) const; //!< print utility
    void printSpaceInfo(PrintStream& os) const    //! print utility
    { printSpaceInfo(os.currentStream()); }
    virtual void print(std::ostream& out) const;  //!< print utility
    virtual void print(PrintStream& os) const     //! print utility
    { print(os.currentStream()); }

    //static definitions
    static std::vector<Space*> theSpaces; //!< unique list of defined spaces

    static void clearGlobalVector();      //!< delete all space objects

    static Space* findSpace(const string_t& na);                              //!< find space by its name
    static Space* findSubSpace(const GeomDomain* dom, Space* sp);             //!< find subspace in list
    static const Space* findSubSpace(const GeomDomain* dom, const Space* sp); //!< find subspace in list
    std::vector<Space*> findSubSpaces(Space* sp);                             //!< find subspaces linked to a space
    std::vector<Space*> findSubSpaces(const Space* sp);                       //!< find subspaces linked to a space

    void buildSpace(const std::vector<Parameter>& ps);

    // user space constructors
    //-------------------------------------------------------------------

    Space(const Parameter& p1);
    Space(const Parameter& p1, const Parameter& p2);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7, const Parameter& p8);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10,
          const Parameter& p11);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10,
          const Parameter& p11, const Parameter& p12);
    Space(const Parameter& p1, const Parameter& p2, const Parameter& p3, const Parameter& p4, const Parameter& p5,
          const Parameter& p6, const Parameter& p7, const Parameter& p8, const Parameter& p9, const Parameter& p10,
          const Parameter& p11, const Parameter& p12, const Parameter& p13);

    // advanced constructors
    //-------------------------------------------------------------------
    //! real constructor of analytic functions spectral space
    void buildSpFun(const GeomDomain& dom, const Function& f, number_t nbFun, dimen_t dimFun, const string_t& na);
    //! constructor of spectral space from explicit spectral basis
    Space(const SpectralBasis& spb, const string_t& na);
    //! constructor from domain and Interpolation (FeSpace)
    Space(const GeomDomain& dom, const Interpolation& in, const string_t& na, bool opt = true);
    //! constructor from GeomDomain and Space (SubSpace)
    Space(const GeomDomain& dom, Space& sp, const string_t& na = "");
    //! constructor from GeomDomain, dofIds list and Space (SubSpace)
    Space(const GeomDomain& dom, const std::vector<number_t>& dofids, Space& sp, const string_t& na = "");
    //! constructor from dofIds list and Space (SubSpace)
    Space(const std::vector<number_t>& dofids, Space& sp, const string_t& na = "");
    //! actual subspace constructor
    SubSpace* createSubSpace(const GeomDomain& dom, Space&sp, const string_t& na);
    //! create subspaces of current space from a list of subdomains
    void createSubSpaces(const std::vector<const GeomDomain*>& doms, std::vector<Space*>& subspaces);
    //! build subspaces from a list of subdomains and the largest subspace
    Space* buildSubspaces(const std::vector<const GeomDomain*>& doms, std::vector<Space*>& subspaces);

    //access to SpaceInfo properties
    //-------------------------------------------------------------------
    string_t name() const {return spaceInfo_p->name;}                           //!< returns space name
    SobolevType conformingSpace() const {return spaceInfo_p->spaceConforming;}  //!< returns space conformity
    const GeomDomain* domain() const {return spaceInfo_p->domain_p;}            //!< returns space related domain
    dimen_t dimDomain() const {return spaceInfo_p->domain_p->dim();}            //!< returns dimension of space related domain
    dimen_t dimPoint() const {return spaceInfo_p->domain_p->spaceDim();}        //!< returns dimension of points
    dimen_t dimFun() const {return spaceInfo_p->dimFun;}                        //!< returns dimension of basis functions
    SpaceType typeOfSpace() const {return spaceInfo_p->spaceType;}              //!< returns type of space
    SpaceType typeOfSubSpace() const;                                           //!< returns type of sub-space
    const Space* space() const {return space_p;}                                //!< returns true space object
    const SpaceInfo* spaceInfo() const {return spaceInfo_p;}                    //!< returns true space object
    virtual number_t dimSpace() const;                            //!< space dimension (number of dofs)
    virtual number_t nbDofs() const;                              //!< number of dofs (a dof may be a vector dof)
    virtual const FeSpace* feSpace() const;                       //!< access to child fespace object (const)
    virtual FeSpace* feSpace();                                   //!< access to child fespace object
    virtual const FeSubSpace* feSubSpace() const;                 //!< access to child fesubspace object (const)
    virtual const SpSpace* spSpace() const;                       //!< access to child spspace object (const)
    virtual const SubSpace* subSpace() const;                     //!< access to child subspace object (const)
    virtual SubSpace* subSpace();                                 //!< access to SubSpace from Space class
    virtual const Space* rootSpace() const;                       //!< access to root space pointer (const)
    virtual Space* rootSpace();                                   //!< access to root space pointer
    virtual number_t dofId(number_t n) const;                     //!< return the n-th DoF id of space
    virtual std::vector<number_t> dofIds() const;                 //!< return the DoF ids on space
    virtual std::pair<number_t, number_t> renumberDofs();         //!< optimize dofs numbering to reduce matrix bandwith
    virtual const Dof& dof(number_t n) const;                     //!< return n-th dof (n=1,...)
    virtual bool include(const Space* sp) const;                  //!< return true if space is included in current space
    virtual ValueType valueType() const;                          //!< return value type of basis function (real or complex)
    virtual StrucType strucType() const;                          //!< return structure type of basis function (scalar or vector)
    virtual bool isSpectral() const;                              //!< true if spectral space
    virtual bool isFE() const;                                    //!< true if FE space or FeSubspace
    virtual bool extensionRequired() const;                       //!< true if space has no trace space

    // specific virtual functions for FE type space (FeSpace and FeSubSpace), used in FE computation
    virtual const Interpolation* interpolation() const                 //! return pointer to interpolation
    {return space_p->interpolation();}
    virtual number_t nbOfElements() const                              //! number of elements for FESpace and FESubspace only
    {return space_p->nbOfElements();}
    virtual const Element* element_p(number_t k) const                 //! access to k-th element (pointer) for FESpace and FESubspace only
    {return space_p->element_p(k);}
    virtual const Element& element(number_t k) const                   //! access to k-th (k>=0) element for FESpace and FESubspace only
    {return space_p->element(k);}
    virtual const Element* element_p(GeomElement* gelt) const          //! access to element (pointer) associated to a geomelement
    {return space_p->element_p(gelt);}
    virtual const Element& element(GeomElement* gelt) const            //! access to element associated to a geomelement
    {return space_p->element(gelt);}
    virtual number_t numElement(GeomElement* gelt) const               //! access to element number associated to a geomelement
    {return space_p->numElement(gelt);}
    virtual const std::vector<number_t>& elementDofs(number_t k) const //! access to dofs ranks (local numbering) of k-th element for FESpace and FESubspace only
    {return space_p->elementDofs(k);}
    virtual std::vector<number_t> elementParentDofs(number_t k) const  //! access to dofs ranks (parent numbering) of k-th element for FESpace and FESubspace only
    {return space_p->elementParentDofs(k);}
    virtual const std::set<RefElement*>& refElements() const          //! access to set of pointer to RefElement involved in FESpace and FESubspace only
    {return space_p->refElements();}
    virtual void buildgelt2elt() const                                //! construct the map gelt2elt from elements list
    {return space_p->buildgelt2elt();}
    virtual const std::vector<FeDof>& feDofs() const                  //! access to vector of fedofs
    {return space_p->feDofs();}
    virtual const Point& feDofCoords(number_t k) const                //! return coordinates of dot as a point (may be virtual coordinates when not a Lagrange dof)
    {return feDofs()[k].coords();}
    virtual void builddofid2rank() const                              //! construct the map dofid2rank from dofs list
    {return space_p->builddofid2rank();}
    virtual const std::map<number_t, number_t>&  dofid2rank() const   //! return the map dof to rank, built on fly (only for FeSpace and SubSpace)
    {return space_p->dofid2rank();}
    virtual Dof& locateDof(const Point& P) const                      //!< locate Lagrange dof nearest a given point (only for FeSpace)
    {return space_p->locateDof(P);}

    //specific virtual functions for SP type space
    virtual void shiftDofs(number_t n)                               //! shift dofs numbering from n (special utility for SpSpace)
    {return space_p->shiftDofs(n);}

    //print utilities
    static void printAllSpaces(std::ostream& out);                                     //!< output all spaces in memory
    static void printAllSpaces(PrintStream& os) {printAllSpaces(os.currentStream());}  //!< output all spaces in memory
    friend std::ostream& operator<<(std::ostream& out, const Space& sp);
};

std::ostream& operator<<(std::ostream& out, const Space& sp);     //!< output space characteristics

//extern functions
bool compareSpaceUsingSize(const Space* sp1, const Space* sp2);     //!< compare spaces using their sizes
std::vector<number_t> renumber(const Space* sp1, const Space* sp2); //!< renumber dofs of a space along dof numbering of an another space
Space& subSpace(Space& sp, const GeomDomain& dom);                  //!< return sub-space or trace space on domain of a space, created if not exist
inline Space& operator |(Space& sp, const GeomDomain& dom)          //!< return sub-space or trace space on domain of a space, created if not exist
{return subSpace(sp,dom);}
Space* mergeSubspaces(Space* & sp1, Space* & sp2, bool newSubspaces=false); //!< merge two subspaces
Space* mergeSubspaces(std::vector<Space*>& sps, bool newSubspaces=false);   //!< merge a list of subspaces
Space* unionOf(std::vector<Space*>& sps);                                   //!< union of subspaces
std::vector<Point> dofCoords(Space& sp, const GeomDomain& dom);             //!< list of coordinates of ponctual dofs of Space on a given domain
void clearTerms(const Space& sp);                                           //!< clear Terms depending on a space (implemented in Term.cpp)
void clearStorages(const Space& sp);                                        //!< clear Storages depending on a space (implemented in Term.cpp)
void clearProjectors(const Space& sp);                                      //!< clear Projectors depending on a space (implemented in Projector.cpp)
void clearUnknowns(const Space& sp);                                        //!< clear Unknowns depending on a space (implemented in Unknown.cpp)

//==================================================================================================
// to deal with collections of spaces
//==================================================================================================
typedef PCollection<Space> Spaces; //!< collection of Space pointers

//@{
//! build a collection of space according to collections of domains and collection of interpolations
inline Spaces spaces(const Domains& doms,const Interpolations& ints, bool opt=true) // few domains, few interpolations
{
  Spaces vs;
  std::vector<GeomDomain*>::const_iterator itd=doms.begin();
  std::vector<Interpolation*>::const_iterator iti=ints.begin();
  for(; itd!=doms.end(); ++itd, ++iti) vs<<(*new Space(**itd,**iti,(*iti)->shortname+"_"+(*itd)->name(),opt));
  return vs;
}

inline Spaces spaces(const Domains& doms,const Interpolation& inte, bool opt=true) // few domains, one interpolation
{
  Spaces vs;
  std::vector<GeomDomain*>::const_iterator itd=doms.begin();
  for(; itd!=doms.end(); ++itd) vs<<(*new Space(**itd, inte, inte.shortname+"_"+(*itd)->name(),opt));
  return vs;
}

inline Spaces spaces(const GeomDomain& dom,const Interpolations& ints, bool opt=true) // few domains, one interpolation
{
  Spaces vs;
  std::vector<Interpolation*>::const_iterator iti=ints.begin();
  for(; iti!=ints.end(); ++iti) vs<<(*new Space(dom, **iti, (*iti)->shortname+"_"+dom.name(),opt));
  return vs;
}
//@}

#ifndef DOXYGEN_SHOULD_SKIP_THIS

/* specialization of PCollectionItem
    when Vs is a Spaces allows to write Vs(i) = Space(...) with no hard copy
*/
template<> class PCollectionItem<Space>
{
  public:
    std::vector<Space*>::iterator itp;
    PCollectionItem(std::vector<Space*>::iterator it) : itp(it) {}
    Space& operator = (const Space& sp)
    {
      Space & nsp=*new Space(sp);      //create a copy of sp on the heap, shared pointers !
      Space::theSpaces.push_back(&nsp);//add to theSpaces list
      sp.global=true;                  //declare original space as global to disable deletion
      *itp=const_cast<Space*>(&nsp);
      return **itp;
    }
    operator Space&() {return **itp;}  //autocast PCollectionItem->T&
};

#endif

//==================================================================================================
/*! a class handling the map between dof numbers of the trace of 2 spaces (V1,V2) on an interface
    the spaces have to be FE spaces on a same mesh with same interpolation !
    This class manages a numbering vector, say m, that relates dof numbers of V1 to dof numbers of V2 :
                    i_th dof of V1 = m(i)_th dof of V2
    This map allows to transport a TermVector defined on V1 to a TermVector defined on V2
*/
//==================================================================================================
class SpaceMap
{
  private:
    const GeomDomain* domain_p;       //!< interface domain
    const Space* V1_p, *V2_p;         //!< spaces involved in the mapping
    void buildMap();                  //!< effective construction of the map

  public:
    std::vector<number_t> dofMap;     //!< numbering vector describing the map V1->V2 (index start from 0)
    SpaceMap(const Space& V1, const Space& V2, const GeomDomain& dom)   // main constructor
      : domain_p(&dom), V1_p(&V1), V2_p(&V2) {buildMap();}

    number_t operator()(number_t i) const //!< return the dof number of V2 of the dof number of V1 (index start from 1)
    {return dofMap[i-1];}

   //io functions
    void print(std::ostream&) const;                           //!< print SpaceMap
    void print(PrintStream& os) const {print(os.currentStream());}
};

inline std::ostream& operator<<(std::ostream& os, const SpaceMap& sm)
{
   sm.print(os);
   return os;
}

} // end of namespace xlifepp

#endif // SPACE_HPP
