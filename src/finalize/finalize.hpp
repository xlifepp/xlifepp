/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file finalize.hpp
  \author N. Kielbasiewicz
  \since 22 fev 2013
  \date 22 fev 2013

  \brief Finalization procedures for global lists of objects
 */
#ifndef FINALIZE_HPP
#define FINALIZE_HPP
#include <iostream>
#include "utils.h"

namespace xlifepp
{
//! finalize execution of XLiFE++
void finalize();
void resetThreadData();
void printAllInMemory(std::ostream& os, size_t vb=1);
void printAllInMemory(PrintStream& os, size_t vb=1);
} // end of namespace xlifepp

#endif /* FINALIZE_HPP */
