/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file finalize.cpp
  \author N. Kielbasiewicz
  \since 22 fev 2013
  \date 25 fev 2013

  \brief finalize functions definitions
 */

#include "finalize.hpp"
#include "finiteElements.h"
#include "operator.h"
#include "space.h"
#include "geometry.h"
#include "largeMatrix.h"
#include "term.h"

namespace xlifepp
{

//! finalize execution of XLiFE++
void finalize()
{
  // clean all non user run-time lists
  Projector::clearGlobalVector();  //before Term::clearGlobalVector !!!
  Term::clearGlobalVector();
  MatrixStorage::clearGlobalVector();
  Unknown::clearGlobalVector();
  Space::clearGlobalVector();
  DomainMap::clearGlobalVector();
  GeomDomain::clearGlobalVector();
  Quadrature::clearGlobalVector();
  Interpolation::clearGlobalVector();
  GeomRefElement::clearGlobalVector();
  RefElement::clearGlobalVector();
  DifferentialOperator::clearGlobalVector();
}

void resetThreadData()
{
    theThreadData.resetAll();
}

// print the list of  all objects in memory
void printAllInMemory(std::ostream& os, size_t vb)
{
  number_t vbl=theVerboseLevel;
  verboseLevel(vb);
  os<<"============================================================================="<<eol;
  GeomRefElement::printAllGeomRefElements(os);
  RefElement::printAllRefElements(os);
  Interpolation::printAllInterpolations(os);
  Quadrature::printAllQuadratures(os);
  GeomDomain::printTheDomains(os);
  MatrixStorage::printAllStorages(os);
  Space::printAllSpaces(os);
  Unknown::printAllUnknowns(os);
  Term::printAllTerms(os);
  Projector::printAllProjectors(os);
  os<<"============================================================================="<<eol;
  verboseLevel(vbl);
}

void printAllInMemory(PrintStream& os, size_t vb)
{
  printAllInMemory(os.currentStream(),vb);
}

} // end of namespace xlifepp
