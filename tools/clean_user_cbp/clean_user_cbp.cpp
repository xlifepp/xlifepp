#include <iostream>
#include <string>
#include <fstream>

using namespace std;

int main(int argc, char* argv[ ])
{
  //analyze argument
  bool verbose=false;
  string cbpFile="", newcbpFile="";
  for(int i=1;i<argc;i++)
  {
      string s(argv[i]);
      if(s=="-v") verbose=true;
      else
        { if(cbpFile=="") cbpFile=s;
          else if(newcbpFile=="") newcbpFile=s;
        }
  }
  if(cbpFile=="")
    {
      cout <<"no file specified, usage : " <<endl;
      cout <<"   clean_user_cpb [-v] project_name.cbp [new_project_name.cbp]"<<endl;
      cout <<"-v for the verbose mode";
      return -1;
    }
  if(newcbpFile=="") newcbpFile=cbpFile;

  //read file
  ifstream fs(cbpFile.c_str());
  if(!fs.is_open())
    {
      cout << "Unable to open CodeBlocks file " << cbpFile << endl;
      return -2;
    }
  string target = "", header = "", tail = "", line = "";
  bool inheader = true, clean = false;
  while(!fs.eof())
    {
      getline(fs,line);
      if(line.find("<Target") != string::npos)   //start target field
        {
          inheader = false;
          if(line.find("/fast") != string::npos || line.find("\"all\"") != string::npos ||
              line.find("\"edit_cache\"") != string::npos || line.find("\"rebuild_cache\"") != string::npos)  //exclude target
            {
              while(line.find("</Target") == string::npos)  getline(fs,line);
              clean = true;
            }
          else // keep target
            {
              target = line + "\n";
              getline(fs,line);
              while(line.find("</Target") == string::npos)
                {
                  target += line + "\n";
                  getline(fs,line);
                }
              target += line + "\n";
            } //end target field
        }
      else
        {
          if(inheader) header += line + "\n";
          else tail += line + "\n";
        }
    }
  fs.close();

  //save to file
  if(clean)
    {
      ofstream fout(newcbpFile.c_str());
      fout<<header<<target<<tail;
      fout.close();
      if(verbose) cout<<cbpFile<< " has been cleaned; result in "<<newcbpFile;
    }
  else if(verbose)
    cout<<"nothing to clean in "<<cbpFile;
  return 0;
}

