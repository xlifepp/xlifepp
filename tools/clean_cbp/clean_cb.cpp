/*!
 tool to clean XLiFE++ codeblocks project (delete fast targets and sort targets)
 usage : clean_cb xxx.cbp  (clean_cb : name of executable)
 xxx.cbp is replaced by the cleaned one

 author : Eric Luneville
 date : 12 jan 2022
*/
#include <iostream>
#include <fstream>
#include <string>
#include <map>
using namespace std;

string pathOf(const string& fname)
{
     size_t pos = fname.find_last_of("\\/");
     return (string::npos == pos)
         ? ""
         : fname.substr(0, pos);
}

string upperCase(const string& s)
{
  string t(s);
  for (string::iterator s_i = t.begin(); s_i < t.end() ; ++s_i)
  {
    *s_i = toupper(*s_i);
  }
  return t;
}

int main(int argc, char** argv)
{
   if(argc==1)
   {
      cout<<"No file given"<<endl;
      return -1;
   }
   string fn=argv[1];
   if(fn.substr(fn.find_last_of(".") + 1) != "cbp")
   {
      cout<<"Not a cbp file given"<<endl;
      return -1;
   }
   ifstream fin(fn.c_str());
   if(!fin.is_open())
   {
     cout << "Unable to open file " << fn << endl;
     return -2;
   }

   string line, header, tail, target, targetcore, exe, base;
   size_t p, q;
   bool inTarget=false, inHeader=true, inFast=false;
   map<string,string> targets;
   while (getline(fin,line)) //read xxx.cbp line by line
   {
     if(line.find("<Target")!=string::npos)  //enter in target definition
     {
       inTarget=true;
       if(line.find("/fast")==string::npos)  //not a fast target
       {
         p=line.find('"'); q=line.find('"',p+1);
         target=line.substr(p+1,q-p-1);
         inHeader=false;
         targetcore=line+"\n";
       }
       else inFast=true;
     }
     else
     {
       if(inTarget)
       {
         targetcore+=line+"\n";
         if(line.find("</Target>")!=string::npos)  //quit target definition
         {
           if(!inFast)  // not a fast target
           {
              p=targetcore.find("Option output");
              base="";
              if(p!=string::npos)
              {
                p=targetcore.find('"',p); q=targetcore.find('"',p+1);
                exe=targetcore.substr(p+1,q-p-1); base=pathOf(exe);
                p=targetcore.find("working_dir");
                p=targetcore.find('"',p); q=targetcore.find('"',p+1);
                targetcore.replace(p+1,q-p-1,base); //change working dir
              }
              //sorting
              if(target.find("unit_")!=string::npos)      target="AAA_"+upperCase(target);
              else if (target.find("sys_")!=string::npos) target="AAB_"+upperCase(target);
              else if (target.find("ext_")!=string::npos) target="AAC_"+upperCase(target);
              else if (target.find("dev_")!=string::npos) target="AAD_"+upperCase(target);
              else
              {
                if(base=="") target="ZZZ_"+upperCase(target);
                else target="YYY_"+upperCase(target);
              }
              targets[target]=targetcore;  //insert in map
           }
           inTarget=false; inFast=false;
         }
       }
       else if(line!="" && !inFast)
       {
         if(inHeader) header+=line+"\n";  //
         else tail+=line+"\n";
       }
     }
   }
   fin.close();
   ofstream fout(fn, ofstream::out | ofstream::trunc);
   fout<<header;
   for(map<string,string>::iterator it=targets.begin();it!=targets.end();++it)
   {
      fout<<it->second<<endl;
   }
   fout<<tail<<endl;
   return 0;
}
