#!/bin/sh
#
# The aim of this hook is to update the VERSION file at the root of XLiFE++ project
# when you call the git commit command
# you can skip it by adding --no-verify option to the git commit command
#

release=$(git tag -l | tail -1)
revisionHead=$(git rev-list --count HEAD)
revisionTag=$(git rev-list --count $release)
revision=$(expr $revisionHead - $revisionTag + 1)
date=$(date +%F)
echo "OVERVIEW: XLiFE++ source files" > VERSION.txt
echo "VERSION: $release-r$revision" >> VERSION.txt
echo "DATE: $date" >> VERSION.txt
git add VERSION.txt
