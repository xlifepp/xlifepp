/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file mathsResources.h
   \author N. Kielbasiewicz
   \since 28 dec 2011
   \date 28 dec 2011

   \brief XLiFE++ overall header file for mathsResources library
 */

#include "mathsResources/combinatorics.hpp"
#include "mathsResources/GaussFormulae.hpp"
#include "mathsResources/polynomialsRoots.hpp"
#include "mathsResources/exactSolutions/exactSolutions.hpp"
#include "mathsResources/greenFunctions/Navier3dKernel.hpp"
#include "mathsResources/greenFunctions/Maxwell3dKernel.hpp"
#include "mathsResources/greenFunctions/Helmholtz2dKernel.hpp"
#include "mathsResources/greenFunctions/Helmholtz3dKernel.hpp"
#include "mathsResources/greenFunctions/Laplace2dKernel.hpp"
#include "mathsResources/greenFunctions/Laplace3dKernel.hpp"
#include "mathsResources/specialFunctions/specialFunctions.hpp"
#include "mathsResources/specialFunctions/FockFunction.hpp"
#include "mathsResources/specialFunctions/MalyuzhinetsFunction.hpp"
#include "mathsResources/randomGenerators.hpp"
#include "mathsResources/fft.hpp"
#include "mathsResources/spline.hpp"
#include "mathsResources/earcut.hpp"
#include "mathsResources/quadratureMethods.hpp"
#include "mathsResources/OdeSolver.hpp"
