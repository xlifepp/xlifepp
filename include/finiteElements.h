/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file finiteElements.h
   \author N. Kielbasiewicz
   \since 28 dec 2011
   \date 28 dec 2011

   \brief XLiFE++ overall header file for finiteElements library
 */

#include "finiteElements/Interpolation.hpp"
#include "finiteElements/RefDof.hpp"
#include "finiteElements/GeomRefElement.hpp"
#include "finiteElements/segment/GeomRefSegment.hpp"
#include "finiteElements/triangle/GeomRefTriangle.hpp"
#include "finiteElements/quadrangle/GeomRefQuadrangle.hpp"
#include "finiteElements/tetrahedron/GeomRefTetrahedron.hpp"
#include "finiteElements/hexahedron/GeomRefHexahedron.hpp"
#include "finiteElements/prism/GeomRefPrism.hpp"
#include "finiteElements/pyramid/GeomRefPyramid.hpp"
#include "finiteElements/splitUtils.hpp"
#include "finiteElements/RefElement.hpp"
#include "finiteElements/segment/RefSegment.hpp"
#include "finiteElements/segment/LagrangeSegment.hpp"
#include "finiteElements/triangle/RefTriangle.hpp"
#include "finiteElements/triangle/LagrangeTriangle.hpp"
#include "finiteElements/triangle/RaviartThomasTriangle.hpp"
#include "finiteElements/triangle/NedelecTriangle.hpp"
#include "finiteElements/triangle/MorleyTriangle.hpp"
#include "finiteElements/triangle/ArgyrisTriangle.hpp"
#include "finiteElements/quadrangle/RefQuadrangle.hpp"
#include "finiteElements/quadrangle/LagrangeQuadrangle.hpp"
#include "finiteElements/quadrangle/NedelecEdgeQuadrangle.hpp"
#include "finiteElements/tetrahedron/RefTetrahedron.hpp"
#include "finiteElements/tetrahedron/LagrangeTetrahedron.hpp"
#include "finiteElements/tetrahedron/NedelecFaceTetrahedron.hpp"
#include "finiteElements/tetrahedron/NedelecEdgeTetrahedron.hpp"
#include "finiteElements/hexahedron/RefHexahedron.hpp"
#include "finiteElements/hexahedron/LagrangeHexahedron.hpp"
#include "finiteElements/hexahedron/NedelecEdgeHexahedron.hpp"
#include "finiteElements/prism/LagrangePrism.hpp"
#include "finiteElements/integration/Quadrature.hpp"
#include "finiteElements/integration/QuadratureRule.hpp"
#include "finiteElements/integration/IntegrationMethod.hpp"
#include "finiteElements/integration/FilonIM.hpp"

