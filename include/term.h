﻿/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file term.h
   \author N. Kielbasiewicz
   \since 28 dec 2011
   \date 28 dec 2011

   \brief XLiFE++ overall header file for term library
 */

#include "term/Projector.hpp"
#include "term/Term.hpp"
#include "term/LcTerm.hpp"
#include "term/PreconditionerTerm.hpp"
#include "term/SuTermVector.hpp"
#include "term/TermVector.hpp"
#include "term/SuTermMatrix.hpp"
#include "term/TermMatrix.hpp"
#include "term/SpectralBasisInt.hpp"
#include "term/computation/termUtils.hpp"
#include "term/computation/otherComputation.hpp"
#include "term/TensorKernel.hpp"
#include "term/KernelOperatorOnTermVector.hpp"
#include "term/SymbolicTermMatrix.hpp"
