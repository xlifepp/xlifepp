/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file geometry.h
   \author N. Kielbasiewicz
   \since 28 dec 2011
   \date 28 dec 2011

   \brief XLiFE++ overall header file for geometry library
 */

#include "geometry/Parametrization.hpp"
#include "geometry/Geodesic.hpp"
#include "geometry/Geometry.hpp"
#include "geometry/GeomDomain.hpp"
#include "geometry/GeomElement.hpp"
#include "geometry/GeomMapData.hpp"
#include "geometry/Mesh.hpp"
#include "geometry/geometries_utils.hpp"
#include "geometry/geometries1D.hpp"
#include "geometry/geometries2D.hpp"
#include "geometry/geometries3D.hpp"
#include "geometry/DomainMap.hpp"
#include "geometry/Extension.hpp"
#include "geometry/OpenCascade/OpenCascade.hpp"
