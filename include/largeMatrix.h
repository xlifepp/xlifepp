/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file largeMatrix.h
   \author N. Kielbasiewicz
   \since 28 dec 2011
   \date 28 dec 2011

   \brief XLiFE++ overall header file for largeMatrix library
 */

#include "largeMatrix/LargeMatrix.hpp"
#include "largeMatrix/MatrixStorage.hpp"
#include "largeMatrix/denseStorage/DenseStorage.hpp"
#include "largeMatrix/denseStorage/ColDenseStorage.hpp"
#include "largeMatrix/denseStorage/RowDenseStorage.hpp"
#include "largeMatrix/denseStorage/DualDenseStorage.hpp"
#include "largeMatrix/denseStorage/SymDenseStorage.hpp"
#include "largeMatrix/csStorage/CsStorage.hpp"
#include "largeMatrix/csStorage/ColCsStorage.hpp"
#include "largeMatrix/csStorage/RowCsStorage.hpp"
#include "largeMatrix/csStorage/DualCsStorage.hpp"
#include "largeMatrix/csStorage/SymCsStorage.hpp"
#include "largeMatrix/skylineStorage/SkylineStorage.hpp"
#include "largeMatrix/skylineStorage/DualSkylineStorage.hpp"
#include "largeMatrix/skylineStorage/SymSkylineStorage.hpp"
#include "largeMatrix/MatrixEntry.hpp"
