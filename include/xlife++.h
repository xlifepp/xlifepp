/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file xlife++.h
   \author D. Martin, N. Kielbasiewicz
   \since 06 dec 2002
   \date 28 dec 2011

   \brief XLiFE++ overall header file

   (to be included in user's main program ONLY)
 */
#ifndef XLIFEPP_H
#define XLIFEPP_H

//--------------------------------------------------------------------------------
// Initialization
//--------------------------------------------------------------------------------
#include "init.h"
#include "finalize.h"

//--------------------------------------------------------------------------------
// main libraries
//--------------------------------------------------------------------------------
#include "utils.h"
#include "largeMatrix.h"
#include "hierarchicalMatrix.h"
#include "mathsResources.h"
#include "finiteElements.h"
#include "geometry.h"
#include "space.h"
#include "operator.h"
#include "form.h"
#include "term.h"
#include "solvers.h"
#include "others.h"

//--------------------------------------------------------------------------------
// Global Scope Constants and Variables
//--------------------------------------------------------------------------------
#include "user_typedefs.h"
#include "globalScopeData.h"

#endif /* XLIFEPP_H */
