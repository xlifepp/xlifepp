/*!
  Header of all test functions.
  \file testList.hpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 3 dec 2011
  \date 5 nov 2012

  |||||||||||||||||||||||||||||||||||||||||||||||||||||\n
  |||  THIS FILE IS CONFIGURED DURING INSTALLATION  |||\n
  |||||||||||||||||||||||||||||||||||||||||||||||||||||
*/

#ifndef TEST_LIST_HPP
#define TEST_LIST_HPP

#include "utils.h"

${TESTLIST_CODE}

#endif // TEST_LIST_HPP
