#ifndef SETUP_HPP
#define SETUP_HPP

/*!
  Macro definition for operating system

  - #define OS_IS_UNIX
  - #define OS_IS_WIN
 */
#define ${OS_TYPE}

//! XLiFE++ installation path
#define INSTALL_PATH "${CMAKE_SOURCE_DIR}"

//! path to visuTermVec.m
#define VISUTERMVECM_PATH "${XLIFEPP_VISUTERMVECM_PATH}"

/*!
  Macro definition for precision of floating types (pick one and only one)

  - #define STD_TYPES
  - #define LONG_TYPES
  - #define LONGLONG_TYPES
 */
#define ${PREC_TYPE}

/*!
  Macro definition for precision of compiler (pick one and only one)

  - #define COMPILER_IS_32_BITS
  - #define COMPILER_IS_64_BITS
*/
#define COMPILER_IS_${BITS_TYPE}_BITS

/*!
  Macro definition for encoding string

  - #define WIDE_STRING
  - #define STD_STRING
 */
#define ${STRING_TYPE}

/*!
  Macro definition for including Arpack++ header file

  - #define XLIFEPP_WITHOUT_ARPACK
  - #define XLIFEPP_WITH_ARPACK
 */
#define ${ARPACK}

/*!
  Macro definition for including Umfpack header file

  - #define XLIFEPP_WITHOUT_UMFPACK
  - #define XLIFEPP_WITH_UMFPACK
 */
#define ${UMFPACK}

/*!
  Macro definition for including Omp header

  - #define XLIFEPP_WITHOUT_OMP
  - #define XLIFEPP_WITH_OMP
 */
#define ${OMP}

/*!
  Macro definition for using gmsh with system calls

  - #define XLIFEPP_WITHOUT_GMSH
  - #define XLIFEPP_WITH_GMSH
 */
#define ${GMSH}
#define GMSH_EXECUTABLE "${XLIFEPP_GMSH_EXECUTABLE}"
#define GMSH_VERSION "${XLIFEPP_GMSH_VERSION}"

/*!
  Macro definition for including OpenCASCADE headers

  - #define XLIFEPP_WITHOUT_OPENCASCADE
  - #define XLIFEPP_WITH_OPENCASCADE
 */
#define ${OPENCASCADE}

/*!
  Macro definition for using Paraview with system calls

  - #define XLIFEPP_WITHOUT_PARAVIEW
  - #define XLIFEPP_WITH_PARAVIEW
 */
#define ${PARAVIEW}
#define PARAVIEW_EXECUTABLE "${XLIFEPP_PARAVIEW_EXECUTABLE}"

/*!
  Macro definition for including Eigen header files

  - #define XLIFEPP_WITHOUT_EIGEN
  - #define XLIFEPP_WITH_EIGEN
 */
#define ${EIGEN}

/*!
  Macro definition for including AMOS header files

  - #define XLIFEPP_WITHOUT_AMOS
  - #define XLIFEPP_WITH_AMOS
 */
#define ${AMOS}

/*!
  Macro definition for including magma header file

  - #define XLIFEPP_WITHOUT_MAGMA
  - #define XLIFEPP_WITH_MAGMA
 */
#define ${MAGMA}

/*!
  Macro definition for including Metis header file

  - #define XLIFEPP_WITHOUT_METIS
  - #define XLIFEPP_WITH_METIS
 */
#define ${METIS}

//! Debug parameter
// #define DEBUG_ON
//! macro definition for decorated Fortran routines name called in C/C++
// #define NO_TRAILING_UNDERSCORE

#endif /* SETUP_HPP */
