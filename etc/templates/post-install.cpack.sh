#!/bin/sh

XLIFEPP_BIN_INSTALL_DIR="@CPACK_PACKAGING_INSTALL_PREFIX@"
echo "**********************************"
echo "Running post installation script:"
echo "mkdir -p $XLIFEPP_BIN_INSTALL_DIR/build"
mkdir -p $XLIFEPP_BIN_INSTALL_DIR/build
echo "cd $XLIFEPP_BIN_INSTALL_DIR/build"
cd $XLIFEPP_BIN_INSTALL_DIR/build
echo "@XLIFEPP_USR_CONFIGURE_CMAKE_COMMAND@"
@XLIFEPP_USR_CONFIGURE_CMAKE_COMMAND@
echo "**********************************"
echo "Building aliases in /usr/local/bin :"
echo "ln -sf @CPACK_PACKAGING_INSTALL_PREFIX@/bin/xlifepp.sh /usr/local/bin/xlifepp.sh"
ln -sf @CPACK_PACKAGING_INSTALL_PREFIX@/xlifepp/bin/xlifepp.sh /usr/local/bin/xlifepp.sh
echo "ln -sf @CPACK_PACKAGING_INSTALL_PREFIX@/bin/xlifepp_configure.sh /usr/local/bin/xlifepp_configure.sh"
ln -sf @CPACK_PACKAGING_INSTALL_PREFIX@/xlifepp/bin/xlifepp_configure.sh /usr/local/bin/xlifepp_configure.sh
echo "**********************************"
echo "XLiFE++ configuration done"
