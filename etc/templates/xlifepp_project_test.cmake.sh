#! /bin/bash


COUNTER=1
find @CMAKE_SOURCE_DIR@/examples/ -name '*.cpp' -exec basename {} \; | while read file; do
project_dir="project-${COUNTER}"
let COUNTER=COUNTER+1
echo "$COUNTER"
mkdir "$project_dir"
compiler=`ls @CMAKE_SOURCE_DIR@/lib/@XLIFEPP_ARCH@`
cd $project_dir; {
if [ -z $1 ]; then
@CMAKE_SOURCE_DIR@/bin/xlifepp.sh -noi -b -g -f $file -cxx $compiler
else
@CMAKE_SOURCE_DIR@/bin/xlifepp.sh -noi -b -g -f $file -cxx $compiler -omp
fi
echo "include(CTest)" >> CMakeLists.txt
cpp_file=(`ls *.cpp`)
echo "$cpp_file"
echo "add_test(NAME ${cpp_file%.cpp} COMMAND \${exe} )" >> CMakeLists.txt
echo "set_tests_properties(${cpp_file%.cpp} PROPERTIES FAIL_REGULAR_EXPRESSION \"Error XLiFE\")" >> CMakeLists.txt
cmake .
make
ctest
}; cd -
rm -rf $project_dir
done
