@echo OFF

REM Global variables and options
set XLIFEPP_DIR="@NATIVE_CMAKE_SOURCE_DIR@"
set XLIFEPP_ARCH="@XLIFEPP_ARCH@"
set XLIFEPP_VERSION="@VERSION_NUMBER@"
set XLIFEPP_DATE="@VERSION_DATE@"
set XLIFEPP_DOC_VIEWER=""
set XLIFEPP_DOC_TYPE=""
set XLIFEPP_BUILD=1
set XLIFEPP_BUILD_CHECK_DIRECTORY=1
set XLIFEPP_BUILD_CHECK_DIRECTORY_TMP=1
set XLIFEPP_BUILD_CXX_COMPILER="@XLIFEPP_CXX_COMPILER@"
set XLIFEPP_BUILD_CXX_COMPILER_TMP="@XLIFEPP_CXX_COMPILER@"
set XLIFEPP_BUILD_CXX_REAL_COMPILER="@XLIFEPP_CXX_REAL_COMPILER@"
set XLIFEPP_BUILD_CXX_REAL_COMPILER_TMP="@XLIFEPP_CXX_REAL_COMPILER@"
set XLIFEPP_BUILD_INTERACTIVE=1
set XLIFEPP_BUILD_GENERATE=1
set XLIFEPP_BUILD_DIRECTORY=.
set XLIFEPP_BUILD_DIRECTORY_TMP=.
set XLIFEPP_BUILD_TYPE="@CMAKE_BUILD_TYPE@"
set XLIFEPP_BUILD_TYPE_TMP="@CMAKE_BUILD_TYPE@"
set XLIFEPP_BUILD_WITH_FILE="None"
set XLIFEPP_BUILD_WITH_FILE_TMP="None"
set XLIFEPP_BUILD_WITH_OMP=@XLIFEPP_OMP@
set XLIFEPP_BUILD_WITH_OMP_TMP=@XLIFEPP_OMP@
set XLIFEPP_BUILD_GENERATOR="Unix Makefiles"
set XLIFEPP_BUILD_GENERATOR_TMP="Unix Makefiles"
set XLIFEPP_VERBOSE_LEVEL=1
set XLIFEPP_CMAKE_CMD="@CMAKE_COMMAND@"
set XLIFEPP_FROM_SRCS="@XLIFEPP_FROM_SRCS@"
set XLIFEPP_USR_CMAKELISTS_PATH="@NATIVE_CMAKE_BINARY_DIR@"
set XLIFEPP_INFO_TXT_DIR="@NATIVE_LIBRARY_OUTPUT_PATH@"
set XLIFEPP_INFO_TXT_DIR_TMP="@NATIVE_LIBRARY_OUTPUT_PATH@"

REM Parsing options
:PARSARGS
if "%1" == "" goto PARSARGSDOC
if "%1" == "-b" goto ARGBUILD
if "%1" == "--build" goto ARGBUILD
if "%1" == "-bt" goto ARGBUILDTYPE
if "%1" == "--build-type" goto ARGBUILDTYPE
if "%1" == "--check" goto ARGCHECK
if "%1" == "--cxx-compiler" goto ARGBUILDCXXCOMPILER
if "%1" == "--cxx" goto ARGBUILDCXXCOMPILER
if "%1" == "-d" goto ARGBUILDDIRECTORY
if "%1" == "--directory" goto ARGBUILDDIRECTORY
if "%1" == "--doc" goto ARGDOCTYPE
if "%1" == "--doc-viewer" goto ARGDOCVIEWERTYPE
if "%1" == "-g" goto ARGBUILDGENERATE
if "%1" == "--generate" goto ARGBUILDGENERATE
if "%1" == "-gn" goto ARGBUILDGENERATOR
if "%1" == "--generator-name" goto ARGBUILDGENERATOR
if "%1" == "-h" goto ARGBUILDHELP
if "%1" == "--help" goto ARGBUILDHELP
if "%1" == "-i" goto ARGBUILDINTERACTIVE
if "%1" == "--interactive" goto ARGBUILDINTERACTIVE
if "%1" == "-id" goto ARGINFODIR
if "%1" == "--info-dir" goto ARGINFODIR
if "%1" == "-f" goto ARGBUILDWITHFILE
if "%1" == "--main-file" goto ARGBUILDWITHFILE
if "%1" == "-nof" goto ARGBUILDWITHOUTFILE
if "%1" == "--no-main-file" goto ARGBUILDWITHOUTFILE
if "%1" == "-noi" goto ARGBUILDNONINTERACTIVE
if "%1" == "--non-interactive" goto ARGBUILDNONINTERACTIVE
if "%1" == "-nog" goto ARGBUILDNOGENERATE
if "%1" == "--no-generate" goto ARGBUILDNOGENERATE
if "%1" == "-v" goto ARGBUILDVERSION
if "%1" == "--version" goto ARGBUILDVERSION
if "%1" == "-vl" goto ARGBUILDVERBOSE
if "%1" == "--verbose-level" goto ARGBUILDVERBOSE
if "%1" == "-omp" goto ARGBUILDWITHOMP
if "%1" == "--with-omp" goto ARGBUILDWITHOMP
if "%1" == "-nomp" goto ARGBUILDWITHOUTOMP
if "%1" == "--without-omp" goto ARGBUILDWITHOUTOMP
REM Arg read is unexpected
echo Unknown option/argument %1
goto ARGBUILDHELP

:ARGBUILD
set XLIFEPP_BUILD=1
shift
goto PARSARGS

:ARGBUILDTYPE
shift
set XLIFEPP_BUILD_TYPE_TMP=%1
shift
goto PARSARGS

:ARGCHECK
set XLIFEPP_BUILD_CHECK_DIRECTORY_TMP=1
shift
goto PARSARGS

:ARGBUILDCXXCOMPILER
shift
set XLIFEPP_BUILD_CXX_COMPILER_TMP=%1
shift
goto PARSARGS

:ARGBUILDDIRECTORY
shift
set XLIFEPP_BUILD_DIRECTORY_TMP=%1
shift
goto PARSARGS

:ARGDOCTYPE
shift
set XLIFEPP_DOC_TYPE=%1
shift
goto PARSARGS

:ARGDOCVIEWERTYPE
shift
set XLIFEPP_DOC_VIEWER=%1
shift
goto PARSARGS

:ARGBUILDGENERATE
set XLIFEPP_BUILD_GENERATE=1
shift
goto PARSARGS

:ARGBUILDGENERATOR
shift
set XLIFEPP_BUILD_GENERATOR_TMP=%1
shift
goto PARSARGS

:ARGBUILDHELP
call :HELP

:ARGBUILDINTERACTIVE
set XLIFEPP_BUILD_INTERACTIVE=1
shift
goto PARSARGS

:ARGINFODIR
shift
set XLIFEPP_INFO_TXT_DIR_TMP=%1
shift
goto PARSARGS

:ARGBUILDNONINTERACTIVE
set XLIFEPP_BUILD_INTERACTIVE=0
shift
goto PARSARGS

:ARGBUILDNOGENERATE
set XLIFEPP_BUILD_GENERATE=O
shift
goto PARSARGS

:ARGBUILDVERSION
echo XLiFE++ version %XLIFEPP_VERSION% updated on %XLIFEPP_DATE%
goto END

:ARGBUILDVERBOSE
shift
set XLIFEPP_VERBOSE_LEVEL=%1
shift
goto PARSARGS

:ARGBUILDWITHFILE
shift
set XLIFEPP_BUILD_WITH_FILE_TMP=%1
shift
goto PARSARGS

:ARGBUILDWITHOUTFILE
set XLIFEPP_BUILD_WITH_FILE_TMP="None"
shift
goto PARSARGS

:ARGBUILDWITHOMP
shift
set XLIFEPP_BUILD_WITH_OMP_TMP=1
shift
goto PARSARGS

:ARGBUILDWITHOUTOMP
set XLIFEPP_BUILD_WITH_OMP_TMP=0
shift
goto PARSARGS

:PARSARGSDOC
if %XLIFEPP_DOC_TYPE% == "user" %XLIFEPP_DOC_VIEWER% "%XLIFEPP_DIR%/doc/tex/user_documentation.pdf"
if %XLIFEPP_DOC_TYPE% == "dev" %XLIFEPP_DOC_VIEWER% "%XLIFEPP_DIR%/doc/tex/dev_documentation.pdf"
if %XLIFEPP_DOC_TYPE% == "tutorial" %XLIFEPP_DOC_VIEWER% "%XLIFEPP_DIR%/doc/tex/tutorial.pdf"
if %XLIFEPP_DOC_TYPE% == "example" %XLIFEPP_DOC_VIEWER% "%XLIFEPP_DIR%/doc/tex/example.pdf"
if %XLIFEPP_DOC_TYPE% == "install" %XLIFEPP_DOC_VIEWER% "%XLIFEPP_DIR%/doc/tex/install.pdf"
goto PARSARGSOK

:PARSARGSOK
if %XLIFEPP_BUILD_INTERACTIVE% EQU 1 goto PARSARGSEND
set XLIFEPP_BUILD_DIRECTORY=%XLIFEPP_BUILD_DIRECTORY_TMP%
set XLIFEPP_CHECK_DIRECTORY=%XLIFEPP_CHECK_DIRECTORY_TMP%
set XLIFEPP_BUILD_CXX_COMPILER=%XLIFEPP_BUILD_CXX_COMPILER_TMP%
set XLIFEPP_BUILD_CXX_REAL_COMPILER=%XLIFEPP_BUILD_CXX_REAL_COMPILER_TMP%
set XLIFEPP_BUILD_TYPE=%XLIFEPP_BUILD_TYPE_TMP%
set XLIFEPP_BUILD_GENERATOR=%XLIFEPP_BUILD_GENERATOR_TMP%
set XLIFEPP_BUILD_WITH_FILE=%XLIFEPP_BUILD_WITH_FILE_TMP%
set XLIFEPP_BUILD_WITH_OMP=%XLIFEPP_BUILD_WITH_OMP_TMP%
set XLIFEPP_INFO_TXT_DIR=%XLIFEPP_INFO_TXT_DIR_TMP%

if %XLIFEPP_VERBOSE_LEVEL% LSS 2 goto PARSARGSEND
echo Parsing arguments:
echo - Project directory: %XLIFEPP_BUILD_DIRECTORY%
echo - Cxx compiler: %XLIFEPP_BUILD_CXX_COMPILER%
echo - CMake build type: %XLIFEPP_BUILD_TYPE%
if %XLIFEPP_BUILD_GENERATE% EQU 0 echo - Run CMake ? No
if %XLIFEPP_BUILD_GENERATE% EQU 1 echo - Run CMake ? Yes
if %XLIFEPP_BUILD_WITH_OMP% EQU 0 echo - Compile with OpenMP flag ? No
if %XLIFEPP_BUILD_WITH_OMP% EQU 1 echo - Compile with OpenMP flag ? Yes
echo - CMake IDE generator: %XLIFEPP_BUILD_GENERATOR%
if %XLIFEPP_BUILD_WITH_MAIN% EQU 0 echo - Copy main.cpp ? No
if %XLIFEPP_BUILD_WITH_MAIN% EQU 1 echo - Copy main.cpp ? Yes (%XLIFEPP_BUILD_WITH_FILE%)

:PARSARGSEND
echo *********************************
echo *           xlifepp             *
echo *********************************
echo.

if %XLIFEPP_BUILD_INTERACTIVE% EQU 0 goto DIRECTORYNONINTERACTIVE
set project_dir="%cd%"
set /p project_dir="Project directory (default is current directory): "
echo test > %project_dir%\fichier.txt
if exist %project_dir%\fichier.txt goto DIRECTORYEXISTS
if %XLIFEPP_VERBOSE_LEVEL% GEQ 1 echo %project_dir% does not exists. We create it !
mkdir -p %project_dir%

:CHECKPROJECTDIRECTORY
echo "Checking project directory %project_dir%"
set XLIFEPP_NATIVE_DIR=%XLIFEPP_DIR:\=%
set XLIFEPP_NATIVE_DIR=%XLIFEPP_NATIVE_DIR:/=\%
echo %project_dir% | find /c "%XLIFEPP_NATIVE_DIR:"=%\" > "%project_dir:"=%\fichier.txt"
for /f "delims=" %%i in ('type "%project_dir:"=%\fichier.txt"') do (
  if %%i neq 0 goto PROJECTDIRERROR
)
del "%project_dir:"=%\fichier.txt"

:GENERATORSCHOICE
echo.
if %XLIFEPP_BUILD_INTERACTIVE% EQU 0 goto GENERATORSNONINTERACTIVE
@WIN_GENERATORS_CHOICE@
set answer=1
set /p answer="Your choice (default is 1): "
if %answer% LSS 1 goto ANSWERERROR
if %answer% GTR @NB_GENERATORS@ goto ANSWERERROR

@WIN_GENERATORS_DEF@
:CXXCOMPILER
if %XLIFEPP_BUILD_INTERACTIVE% EQU 0 goto CXXCOMPILERNONINTERACTIVE
if not %XLIFEPP_FROM_SRCS% == "ON" goto CXXCOMPILERFROMBINDIST
set iter=1
echo The following compilers are available:
for /f "delims=" %%f in ('dir /b "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%"') do (
    echo %iter% -^> %%f
    set /a iter = %iter%+1
)
set /a iter = %iter%-1
set answerCompiler=1
if %iter% EQU 1 goto CXXCOMPILERWORK
set /p answerCompiler="Your choice (default is 1): "
if %answerCompiler% LSS 1 goto ANSWERCOMPILERERROR
if %answerCompiler% GTR %iter% goto ANSWERCOMPILERERROR

:CXXCOMPILERWORK
set iter=1
for /f "delims=" %%f in ('dir /b "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%"') do (
    if %iter% == %answerCompiler% call :SETCOMPILER %%f
    set /a iter = %iter%+1
)
goto CLEANPDIR

:CXXCOMPILERFROMBINDIST
set compiler=%XLIFEPP_BUILD_CXX_COMPILER%

:CLEANPDIR
del /q "%project_dir:"=%\CMakeCache.txt" 2> NUL
rmdir /s /q "%project_dir:"=%\CMakeFiles" 2> NUL
del /q "%project_dir:"=%\cmake_install.cmake" 2> NUL

:CHECKMAININCPPFILES
if %XLIFEPP_BUILD_INTERACTIVE% EQU 0 goto MAINNONINTERACTIVE
set iter=0
dir /s /b %project_dir% | findstr /m ".cpp .c++ .cc" > "%project_dir:"=%\fichier.txt"
for /f %%f in ('type "%project_dir:"=%\fichier.txt"') do (
    set /a iter = %iter%+1
)
del "%project_dir:"=%\fichier.txt"

if %iter% EQU 0 goto MAINFILE
set iter=0
findstr /m /c:" main(" %project_dir:"=%\*.cpp %project_dir:"=%\*.c++ %project_dir:"=%\*.cc 2> NUL > "%project_dir:"=%\fichier.txt"
findstr /n ^^ "%project_dir:"=%\fichier.txt" | find /c ":" > "%project_dir:"=%\fichier2.txt"
for /f %%f in ('type "%project_dir:"=%\fichier2.txt"') do (
    if %%f GTR 1 goto MULTIPLEMAINERROR
)
del "%project_dir:"=%\fichier.txt"
del "%project_dir:"=%\fichier2.txt"
goto MAINOK

:MAINFILE
@WIN_MAIN_FILES_CHOICE@
set answerFile=1
set /p answerFile="Your choice (default is 1): "
if %answerFile% LSS 1 goto ANSWERFILEERROR
if %answerFile% GTR @NB_MAIN_FILES@ goto ANSWERFILEERROR

@WIN_MAIN_FILES_DEF@
goto COPYINGFILE

:MAINOK
echo There are source files with only one main function

:OMP
if %XLIFEPP_BUILD_INTERACTIVE% EQU 0 goto OMPNONINTERACTIVE
if not %XLIFEPP_FROM_SRCS% == "ON" goto OMPFROMBINDIST
set iter=1
echo You can use:
if exist "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%\%compiler:"=%\omp" (
    echo %iter% -^> multi-threading with OpenMP
    set /a iter = %iter%+1 
)
if exist "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%\%compiler:"=%\seq" (
    echo %iter% -^> sequential
    set /a iter = %iter%+1 
)
set /a iter = %iter%-1
set answerParMode=1
if %iter% EQU 1 goto OMPWORK
set /p answerParMode="Your choice (default is 1): "
if %answerParMode% LSS 1 goto ANSWEROMPERROR
if %answerParMode% GTR %iter% goto ANSWEROMPERROR

:OMPWORK
set iter=1
if exist "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%\%compiler:"=%\omp" (
    if %iter% == %answerParMode% call :SETPARMODE "omp"
    set /a iter = %iter%+1 
)
if exist "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%\%compiler:"=%\seq" (
    if %iter% == %answerParMode% call :SETPARMODE "seq"
    set /a iter = %iter%+1 
)
goto BUILDTYPE

:OMPFROMBINDIST
if %XLIFEPP_BUILD_WITH_OMP% EQU 0 call :SETPARMODE "seq"
if %XLIFEPP_BUILD_WITH_OMP% EQU 1 call :SETPARMODE "omp"

:BUILDTYPE
if %XLIFEPP_BUILD_INTERACTIVE% EQU 0 goto BUILDTYPENONINTERACTIVE
if not %XLIFEPP_FROM_SRCS% == "ON" goto BUILDTYPEFROMBINDIST
set iter=1
echo The following build types are available:
for /f "delims=" %%f in ('dir /a:d /b "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%\%compiler:"=%\%parMode:"=%"') do (
    echo %iter% -^> %%f
    set /a iter = %iter%+1
)

set /a iter = %iter%-1
set answerBuildType=1
if %iter% EQU 1 goto BUILDTYPEWORK
set /p answerBuildType="Your choice (default is 1): "
if %answerBuildType% LSS 1 goto ANSWERBUILDTYPEERROR
if %answerBuildType% GTR %iter% goto ANSWERBUILDTYPEERROR

:BUILDTYPEWORK
set iter=1
for /f "delims=" %%f in ('dir /a:d /b "%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%\%compiler:"=%\%parMode:"=%"') do (
    if %iter% == %answerBuildType% call :SETBUILDTYPE %%f
    set /a iter = %iter%+1
)
goto READINFOFILE

:BUILDTYPEFROMBINDIST
set buildtype=%XLIFEPP_BUILD_TYPE%

:READINFOFILE
if %XLIFEPP_BUILD_INTERACTIVE% EQU 0 goto INFOTXTNONINTERACTIVE
set info_path="%XLIFEPP_NATIVE_DIR:"=%\lib\%XLIFEPP_ARCH:"=%\%compiler:"=%\%parMode:"=%"
if exist "%info_path:"=%\%buildtype:"=%\info.txt" (
    set info_path="%info_path:"=%\%buildtype:"=%"
)

:READINFOFILEFIRSTLINE
for /f "tokens=1 delims=" %%f in ('type "%info_path:"=%\info.txt"') do (
    for /f "tokens=2 delims==" %%g in ('echo %%f') do (
        call :SETTRUECOMPILER "%%g"
        goto READINFOFILESECONDLINE
    )
)

:READINFOFILESECONDLINE
for /f "tokens=1 skip=1 delims=" %%f in ('type "%info_path:"=%\info.txt"') do (
    for /f "tokens=2 delims==" %%g in ('echo %%f') do (
        call :SETBUILDPATH "%%g"
    )
)
goto COPYINGCMAKE

:COPYINGCMAKE
if %XLIFEPP_VERBOSE_LEVEL% GEQ 1 echo Copying CMakeLists.txt
xcopy "%buildPath:"=%\CMakeLists.txt" %project_dir%

:CMAKECALL
if %parMode% == "seq" goto CMAKECALLWITHOUTOMP
if %XLIFEPP_VERBOSE_LEVEL% GEQ 2 echo Command to run: %XLIFEPP_CMAKE_CMD% %project_dir% -G\"%generator:"=%\" -DCMAKE_BUILD_TYPE=%buildtype% -DCMAKE_CXX_COMPILER=%trueCompiler:\=/% -DENABLE_OMP=ON -B%project_dir%
if %XLIFEPP_BUILD_GENERATE% EQU 1 %XLIFEPP_CMAKE_CMD% %project_dir% -G%generator% -DCMAKE_BUILD_TYPE=%buildtype% -DCMAKE_CXX_COMPILER=%trueCompiler:/=\% -DENABLE_OMP=ON -B%project_dir%

:END
cmd /k

:DIRECTORYNONINTERACTIVE
set project_dir=%XLIFEPP_BUILD_DIRECTORY%
goto GENERATORSCHOICE

:GENERATORSNONINTERACTIVE
set generator=%XLIFEPP_BUILD_GENERATOR%
goto CXXCOMPILER

:: erase fichier.txt created by the command test when directory exists
:DIRECTORYEXISTS
if %XLIFEPP_VERBOSE_LEVEL% GEQ 1 echo %project_dir% exists
del "%project_dir:"=%\fichier.txt"
goto CHECKPROJECTDIRECTORY

:: managing answer for generator
:ANSWERERROR
echo %answer% is not between 1 and @NB_GENERATORS@ !!! Exit
goto END

:MAINNONINTERACTIVE
if NOT %XLIFEPP_BUILD_WITH_MAIN% EQU "None" goto COPYINGFILE
goto OMP

:INFOTXTNONINTERACTIVE
set info_path=%XLIFEPP_INFO_TXT_DIR%
goto READINFOFILEFIRSTLINE

:COPYINGFILE
if %XLIFEPP_VERBOSE_LEVEL% GEQ 1 echo Copying %XLIFEPP_BUILD_WITH_FILE%
if %XLIFEPP_BUILD_WITH_FILE% == "None" goto OMP
if %XLIFEPP_FROM_SRCS% == "ON" goto COPYINGFILEFROMSRCS
xcopy "%XLIFEPP_NATIVE_DIR:"=%\share\examples\%XLIFEPP_BUILD_WITH_FILE:"=%" %project_dir%
goto OMP

:COPYINGFILEFROMSRCS
if %XLIFEPP_BUILD_WITH_FILE% == "main.cpp" xcopy "%XLIFEPP_NATIVE_DIR:"=%\usr\main.cpp" %project_dir%
if NOT %XLIFEPP_BUILD_WITH_FILE% == "main.cpp" xcopy "%XLIFEPP_NATIVE_DIR:"=%\examples\%XLIFEPP_BUILD_WITH_FILE:"=%" %project_dir%
goto OMP

:: managing answer for main file
:ANSWERFILEERROR
echo %answerFile% is not between 1 and @NB_MAIN_FILES@ !!! Abort
goto END

:ANSWERCOMPILERERROR
echo %answerCompiler% is not between 1 and %iter% !!! Abort
goto END

:ANSWERBUILDTYPEERROR
echo %answerBuildType% is not between 1 and %iter% !!! Abort
goto END

:ANSWEROMPERROR
echo %answerParMode% is not between 1 and %iter% !!! Abort
goto END

:PROJECTDIRERROR
echo A project directory cannot be inside %XLIFEPP_NATIVE_DIR% (XLiFE++ home directory) ! Abort
goto END

:MULTIPLEMAINERROR
echo There are source files with several main functions ! Abort
goto END

:CXXCOMPILERNONINTERACTIVE
set compiler=%XLIFEPP_BUILD_CXX_COMPILER%
goto CLEANPDIR

:OMPNONINTERACTIVE
set omp=%XLIFEPP_BUILD_WITH_OMP%
goto BUILDTYPE

:BUILDTYPENONINTERACTIVE
set buildtype=%XLIFEPP_BUILD_TYPE%
goto READINFOFILE

:CMAKECALLWITHOUTOMP
if %XLIFEPP_VERBOSE_LEVEL% GEQ 2 echo Command to run: %XLIFEPP_CMAKE_CMD% %project_dir:/=\% -G\"%generator:"=%\" -DCMAKE_BUILD_TYPE=%buildtype% -DCMAKE_CXX_COMPILER=%trueCompiler:\=/% -B%project_dir%
if %XLIFEPP_BUILD_GENERATE% EQU 1 %XLIFEPP_CMAKE_CMD% %project_dir:/=\% -G%generator% -DCMAKE_BUILD_TYPE=%buildtype% -DCMAKE_CXX_COMPILER=%trueCompiler:\=/% -B%project_dir%
goto END

:: functions to set variables
:SETCOMPILER
set compiler=%1
goto :eof

:SETTRUECOMPILER
for /f "tokens=* delims= " %%a in (%1) do set trueCompiler="%%a"
set trueCompiler=%trueCompiler:\ = %
set trueCompiler=%trueCompiler:/=\%
goto :eof

:SETBUILDPATH
for /f "tokens=* delims= " %%a in (%1) do set buildPath="%%a"
set buildPath=%buildPath:\ = %
set buildPath=%buildPath:/=\%
goto :eof

:SETBUILDTYPE
set buildtype=%1
goto :eof

:SETOMP
set omp=%1
goto :eof

:SETPARMODE
set parMode=%1
goto :eof

:: display help
:HELP
echo OVERVIEW: XLiFE++ user script
echo VERSION: %XLIFEPP_RELEASE%-r%XLIFEPP_REVISION%
echo DATE: %XLIFEPP_DATE%
echo.
echo USAGE:
echo     xlifepp.bat --build ^[--interactive^] ^[(--generate^|--no-generate)^]
echo     xlifepp.bat --build --non-interactive ^[(--generate^|--no-generate)^]
echo                        ^[--compiler ^<compiler^>^] ^[--directory ^<dir^>^]
echo                        ^[--generator-name ^<generator^>^] 
echo                        ^[--build-type ^<build-type^>^]
echo                        ^[(--with-omp|--without-omp)^]
echo     xlifepp.bat --help
echo     xlifepp.bat --version
echo.
echo MAIN OPTIONS:
echo     --build, -b               copy cmake files and eventually sample of
echo                               main file
echo                               and run cmake on it to prepare your so-called
echo                               project directory. This is the default
echo     --check                   check project directory (outside of XLiFE++ home directory)
echo     --doc ^<doctype^>           display ^<doctype^> documentation. ^<doctype^> can be user,
echo                               dev, install, examples or tutorial
echo     --doc-viewer ^<exe^>        set the viewer to use to display documentation.
echo                               Default is acroread
echo     --generate, -g            generate the project. Used with --build option.
echo                               This is the default.
echo     --help, -help, -h         show the current help
echo     --interactive, -i         run xlifepp in interactive mode. Used with
echo                               --build option. This is the default
echo     --non-interactive, -noi   run xlifepp in non interactive mode. Used with
echo                               --build option
echo     --no-generate, -nog       prevent generation of your project. You will
echo                               do it yourself.
echo     --version, -v             print version number of XLiFE++ and its date
echo     --verbose-level ^<value^>,  set the verbose level. Default value is 1
echo     -vl ^<value^>
echo.
echo OPTIONS FOR BUILD IN NON INTERACTIVE MODE:
echo     --build-type ^<value^>,     set cmake build type (Debug, Release, ...).
echo     -bt ^<value^>
echo     --cxx-compiler ^<value^>,   set the C++ compiler to use.
echo     -cxx ^<value^>
echo     --directory ^<dir^>,        set the directory where you want to build
echo     -d ^<dir^>                  your project
echo     --generator-name ^<name^>,  set the cmake generator.
echo     -gn ^<name^>
echo     -f ^<filename^>,              copy ^<filename^> as a main file for the user
echo     --main-file ^<filename^>      project
echo     --indo-dir, -id           set the directory where the info.txt file is
echo     -nof,                     do not copy a main file. This is the default.
echo     --no-main-file
echo     --with-omp, -omp          activates OpenMP mode
echo     --without-omp, -nomp      deactivates OpenMP mode
goto END
