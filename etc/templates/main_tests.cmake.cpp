/*!
  \file @TEST_FILE@.cpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 1 nov 2011
  \date 6 fev 2014

  main program to launch one test function

  |||||||||||||||||||||||||||||||||||||||||||||||||||||\n
  |||  THIS FILE IS CONFIGURED DURING INSTALLATION  |||\n
  |||||||||||||||||||||||||||||||||||||||||||||||||||||
*/

// STL include
#include <iostream>

// xlife++ include
#include "xlife++.h"
#include "testList.hpp"
#include "testUtils.hpp"

using namespace xlifepp;

int main(int argc, char** argv)
{
  isTestMode = true;
  @PRINT_FILE@
  // initialisation
  init(_trackingMode=true);
  Options opts;
  opts.addOption("check", "-c", false);
  opts.parse(argc, argv);
  bool check=opts("check");
  
  // perform test
  @TEST_CODE@

  Real tcpu=totalCpuTime("Total CPU time");
  Real tela=totalElapsedTime("Total elapsed time");
  if (tela>0) std::cout<<"Speed-up ratio : "<< tcpu/tela<<eol;
  
  return 0;
}
