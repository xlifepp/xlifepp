#!/usr/bin/env ruby 

$opt = Hash.new
$opt[:xlifeppbuilddir] = "@CMAKE_BINARY_DIR@"
$opt[:arch] = "@CMAKE_SYSTEM_PROCESSOR@-@XLIFEPP_SYSTEM_NAME@"
$opt[:build_type] = "@CMAKE_BUILD_TYPE@"
$opt[:compiler] = "@XLIFEPP_CXX_COMPILER@"
$opt[:mode] = "true"
$opt[:jobs] = 1
$opt[:dry_run] = false
$opt[:verbose] = 0
$opt[:test] = ""  
$opt[:help] = "NAME:
    xlifepp_test_runner.rb deals with all automatic tasks such as updating doxygen, generating
    commit history and generating snapshots and releases
     
SYNOPSIS:
    xlifepp_test_runner.rb -c [-n] <test>
    xlifepp_test_runner.rb -e [-n] <test>
    xlifepp_test_runner.rb -h 
    
DESCRIPTION:
    XLiFE++ is a C++ library with a large set of tests. Each test can be launched in 2 modes :
      - the edit mode (default in XLiFE++ executables) makes tests generating results file, so called res file
      - the check mode (default in xlifepp_test_runner.rb) makes tests comparing results to the current res file

    As XLiFE++ compilation is based on CMake, it is not a good way to define running targets, because
    of generators (to Eclipse, XCode, Visual Studio, CodeBlocks, ...). On the one hand, the lesser targets
    there are the easier XLiFE++ is to compile and run, on the other hand targets in generators are not just
    compilation targets: you can run them, and pass parameters to them.
    However, XLiFE++ developers using cmake in command-line mode must have an easy way to run tests.
    That's what xlifepp_test_runner.rb is for !!!

OPTIONS:
    -h, --help                     shows the current help
    -bt, --build-type              sets the build type (Debug, Release)
    -c, --check                    test will be run in check mode (default)
    -cxx, --cxx-compiler           sets the compiler to use
    -e, --edit                     test will be run in edit mode
    -j <num>, --jobs <num>         number of jobs used in compilation step (default is 1)
    <test>                         name of the test (Examples: all, unit_Mesh_gmsh, sys_1d, ...)
    -n                             activates dry-run mode
    -v                             activates verbose level (same as --verbose-level 1)
    --verbose-level <num>          allows to set the verbose level. Default is 0, none.
"


## parsargs -- options parser
#    options -> options list
def parsargs(options)
  while not options.empty?
    case options[0]
    when "-h", "--help"
      puts $opt[:help]
      exit
    when "-bt", "--build-type"
      $opt[:build_type] = options[1].to_s
      options.shift
      next
    when "-c", "--check"
      $opt[:mode] = "true"
      options.shift
      next
    when "-cxx", "--cxx-compiler"
      $opt[:compiler] = options[1].to_s
      options.slice!(0..1)
      next
    when "-e", "--edit"
      $opt[:mode] = "false"
      options.shift
      next
    when "-j", "--jobs"
      $opt[:jobs] = options[1].to_i
      options.slice!(0..1)
      next
    when "-n"
      $opt[:dry_run] = true
      options.shift
      next
    when "-v"
      $opt[:verbose] = 1
      options.shift
      next
    when "--verbose-level"
      $opt[:verbose] = options[1].to_i
      options.slice!(0..1)
      next
    else
      $opt[:test] = options[0]
      options.shift
      next
    end
  end

  if $opt[:test].empty?
    puts "\nERROR in xlifepp_test_runner.rb :"
    puts "No test given ! Examples: all, unit_Mesh_gmsh, sys_1d, ..."
    exit
  end

end

def xlifeppTestRunner args
  # parsing options
  parsargs(args)
  file=$opt[:test]+"-"+$opt[:arch]+"-"+$opt[:compiler]+"-"+$opt[:build_type]
  Dir.chdir($opt[:xlifeppbuilddir])
  cmd="make "+file+" -j"+$opt[:jobs].to_s
  puts "Test command: #{cmd} ... " if $opt[:verbose] >= 1
  system("make",file,"-j"+$opt[:jobs].to_s) if not $opt[:dry_run]

  Dir.chdir($opt[:xlifeppbuilddir]+"/tests/bin/"+$opt[:test])
  if File.exist?(file)
    cmd = "./"+file+" -c "+$opt[:mode]
    puts "Test command: #{cmd} ... " if $opt[:verbose] >= 1
    exec("./"+file,"-c",$opt[:mode]) if not $opt[:dry_run]
  else
    puts "\nERROR in xlifepp_test_runner.rb :"
    puts "Test file "+file+" does not exists"
    exit
  end
  puts "xlifepp_test_runner.rb: That's all folks !!!" if $opt[:verbose] >= 1
end

xlifeppTestRunner ARGV if __FILE__ == $0
