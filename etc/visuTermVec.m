function visuTermVec(coord, elem, elemtype, unknown, val, rootfn, opts)
% Make graphs of vectors or eigenvectors computed by XLiFE++ for 1D, 2D and 3D geometries.
%
% Input arguments are:
% coord (real)
%    Coordinates of the interpolation nodes, one node per row.
%    The nodes are implicitly numbered from 1 to size(coord,1).
% elem (integer)
%    Array containing the lists of elements: elem(i,:) is an element of type elemtype(i).
%    Format of the array: one element per row, column i holds the i-th
%    interpolation node, given by its number in the array coord above.
% elemtype (integer)
%    Vector containing the type of each element present in the mesh, in the same order
%    as the array elem above.
%    Each type is a code number in XLiFE++'s internal codification:
%     2 = point, 3 = segment, 4 = triangle, 5 = quadrangle,
%     6 = tetrahedron, 7 = hexahedron, 8 = prism, 9 = pyramid.
% unknown (string)
%    1-column array containing the name of the unknowns, or their components in the vector
%    case, one name per row.
% val (real or complex)
%    Values corresponding to the unknowns, stored column-wise, one column per component unknown.
%    Each row contains the value at a node, or in an element if the interpolation degree is 0,
%    in the same order as the array coord or elem respectively.
%    Indeed, the graphical function 'patch' used, makes the correspondence according to the
%    number of rows of this array, which should be size(coord,1) or size(elem,1) respectively.
% rootfn (string)
%    Filename of the calling script.
% opts (string)
%    Optional argument. If specified in the calling sequence, opts is an array of string
%    options governing output. Available options are:
%     - 'chsign' : allow to draw values with opposite sign 
%     - 'mesh'   : draw the mesh only (no other graph is drawn)
%     - 'silent' : suppress display of graphical tuning hints
%
% Nota: elem and elemtype are unused in 1D

% Author: Yvon.Lafranche@univ-rennes1.fr (26 Apr. 2017)

% Take options into account (if any)
meshOK = 0;
hintOK = 1;
if nargin > 6
 for indop=1:size(opts,1)
  switch strtrim(opts(indop,:))
   case 'chsign'
        val = -val;
   case 'mesh'
        meshOK = 1;
   case 'silent'
        hintOK = 0;
  end
 end
end

% Take into account the mesh option first : if requested, draw the mesh and return.
if meshOK
  switch (size(coord,2))
    case 1
      draw1D2Dmesh(coord,elem,elemtype)
    case 2
      draw1D2Dmesh(coord,elem,elemtype)
    case 3
      [edges, edgelts] = credgelist(elem,elemtype);
      draw3Dmesh(coord,edges)
  end
  return
end

% Look for the eigenvalue corresponding to these data, in the case these data are
% the components of an eigenvector.
% Split the filename which should be of the form prefix_N_Domain or prefix_N,
% where N is the number of the eigenvalue.
idx = strfind(rootfn,'_');
if length(idx) == 0
  % The filename is not in the form expected in the eigenvalue case.
  % Make the eigenvalue file search fail.
  eigfn = '';
else
  eigfn = [rootfn(1:idx(1)-1),'_eigenvalues'];
end
if exist(eigfn,'file') == 0
% No eigenvalue file found
  eigtxt = '';
else
% Get the eigenvalue corresponding to these data, from the filename (contains the
% rank of the eigenvalue in the file eigfn) and the values stored in the file eigfn
% (format: one column means real values, two columns means complex values)
  lambdas = load(eigfn);
  if length(idx) == 1, idx(2) = length(rootfn)+1; end
  streig = rootfn(idx(1)+1:idx(2)-1);
  numeig = str2num(streig);
  if size(lambdas,2) == 1
    lambda = lambdas(numeig);
  else
    lambda = lambdas(numeig,1) + lambdas(numeig,2)*i;
  end
  eigtxt = ['\lambda_{',streig,'} = ',num2str(lambda)];
end

glims = [min(coord); max(coord)];
switch (size(coord,2))
  case 1
    graph1D(coord, unknown, val, rootfn, eigtxt, glims)
  case 2
    graph2D3D(coord, elem, unknown, val, rootfn, eigtxt, glims,[])
  case 3
    if max(elemtype) > 5
      % Value limits based on the whole data set
      globvlims = [];
      for iu = 1:size(unknown,1)
       vmin = min(min(real(val(:,iu))),min(imag(val(:,iu))));
       vmax = max(max(real(val(:,iu))),max(imag(val(:,iu))));
       globvlims = [globvlims; [vmin vmax]];
      end
      % Create the list of all the edges of the mesh
      % Nota: this must be done from the original mesh and not from the subdivided mesh
      %       made of tetrahedra (built by crtetlist), because this introduces some points
      %       that may disturb the definition of the boundary of the geometrical model.
      [edges, edgelts] = credgelist(elem,elemtype);
      % Initial cutting plane
      ptPL = (glims(1,:) + glims(2,:)) / 2;
      norPL = [1 -1 0];
      % Draw the mesh along with the normal vector defining the cutting plane
      norlen = norm(glims(2,:)-glims(1,:))/4; % Length of the arrow
      [hp,hq] = drawMeCuPl(coord,edges,ptPL,norPL,norlen,glims);
      disp('The intersection plane is defined by the point P and the orthogonal vector V.')
      [ok, ptPL, norPL] = changeplane(ptPL,norPL, hp,hq,norlen,glims);
      while ok
       [pts, vf, edgnplane] = intersect(ptPL,norPL,edges,coord,val);
       tri = crtri(norPL, pts,coord,elem,elemtype, edgnplane,edgelts);
       % Graph with these settings
       if ~isempty(tri)
        graph2D3D(pts, tri, unknown, vf, rootfn, eigtxt, glims,globvlims)
        view(norPL)
       end
       [ok, ptPL, norPL] = changeplane(ptPL,norPL, hp,hq,norlen,glims);
      end
    else
      graph2D3D(coord, elem, unknown, val, rootfn, eigtxt, glims,[])
    end
end
printhints(rootfn,hintOK)
end
%---------------------------------------------------------------------------------------------
function graph1D(coord, unknown, val, rootfn, eigtxt, glims)
% Make graphs of vectors or eigenvectors for 1D geometries.
%
% Input arguments are:
% coord (real)          : see top of file
% unknown (string)      : see top of file
% val (real or complex) : see top of file
% rootfn (string)       : see top of file
% eigtxt (string)       : eigenvalue (if relevant)
% glims (real)          : geometrical limits of the domain (bounding box)

c = sortrows([coord val]);
for iu = 1:size(unknown,1)
  strtitle = strrep([rootfn, ', ', unknown(iu,:)],'_','\_');
  ud = iu+1;
  vmin = min(min(real(c(:,ud))),min(imag(c(:,ud))));
  vmax = max(max(real(c(:,ud))),max(imag(c(:,ud))));
  vlims = [vmin vmax];
  figure
  if isreal(c(:,ud))
   if ~isempty(eigtxt), strtitle = [strtitle, ', ', eigtxt]; end
   subgraph1D(c(:,1), c(:,ud), '', strtitle, glims,[])
  else
  % Plot real part, imaginary part, modulus and argument on the same figure
   subplot(2,2,3)
    subgraph1D(c(:,1), real(c(:,ud)), 'Real part', '', glims,vlims)
   subplot(2,2,1)
    subgraph1D(c(:,1), imag(c(:,ud)), 'Imaginary part', strtitle, glims,vlims)
   subplot(2,2,4)
    subgraph1D(c(:,1),  abs(c(:,ud)), 'Modulus', '', glims,[])
   subplot(2,2,2)
    subgraph1D(c(:,1), atan2(imag(c(:,ud)), real(c(:,ud))), 'Argument', eigtxt, glims,[])
  end
end
end
%---------------------------------------------------------------------------------------------
function subgraph1D(x,y, strleg, strtitle, glims,vlims)
% Utility function used to draw 1D data
%
% Input arguments are:
% x, y (real)               : abscissae and ordinates of the curve
% strleg, strtitle (string) : legend and title
% glims : limits to be used on the geometry (in order to get the same aspect in all the figures)
% vlims : limits to be used on the values (in order to get the same scale in all the figures)

plot(x,y)
xlabel(strleg)
title(strtitle)
xlim(glims)
if ~isempty(vlims), ylim(vlims), end
end
%---------------------------------------------------------------------------------------------
function graph2D3D(coord, elem, unknown, val, rootfn, eigtxt, glims,globvlims)
% Make graphs of vectors or eigenvectors for 2D geometries.
%
% Input arguments are:
% coord (real)          : see top of file
% elem (integer)        : see top of file
% unknown (string)      : see top of file
% val (real or complex) : see top of file
% rootfn (string)       : see top of file
% eigtxt (string)       : eigenvalue (if relevant)
% glims (real)          : geometrical limits of the domain (bounding box)
% globvlims (real)      : global value limits (for each unknown) in order to get the same
%                         limits for the value on subsequent graphs (useful for volumic
%                         domains when the cutting plane is changed by the user)

dim = size(coord,2);
for iu = 1:size(unknown,1)
  strtitle = strrep([rootfn, ', ', unknown(iu,:)],'_','\_');
  rvmin = min(real(val(:,iu)));
  ivmin = min(imag(val(:,iu)));
  rvmax = max(real(val(:,iu)));
  ivmax = max(imag(val(:,iu)));
  if isempty(globvlims)
   vmin = min(rvmin,ivmin);
   vmax = max(rvmax,ivmax);
   vlims = [vmin vmax];
  else
   vlims = globvlims(iu,:);
   disp(['Value range for unknown ',int2str(iu),' : Real part = [',num2str([rvmin,rvmax]),...
                                               ']   Imag part = [',num2str([ivmin,ivmax]),']'])
  end
  stvec=struct('vertices',coord, 'faces', elem, 'facevertexcdata', []);
  figure
  if isreal(val(:,iu))
   if ~isempty(eigtxt), strtitle = [strtitle, ', ', eigtxt]; end
   stvec.facevertexcdata = val(:,iu);
   subgraph2D3D(dim, stvec, '', strtitle, glims,vlims)
  else
  % Plot real part, imaginary part, modulus and argument on the same figure
   subplot(2,2,3)
    stvec.facevertexcdata = real(val(:,iu));
    subgraph2D3D(dim, stvec, 'Real part', '', glims,vlims)
   subplot(2,2,1)
    stvec.facevertexcdata = imag(val(:,iu));
    subgraph2D3D(dim, stvec, 'Imaginary part', strtitle, glims,vlims)
   subplot(2,2,4)
    stvec.facevertexcdata = abs(val(:,iu));
    subgraph2D3D(dim, stvec, 'Modulus', '', glims,[])
   subplot(2,2,2)
    stvec.facevertexcdata = atan2(imag(val(:,iu)), real(val(:,iu)));
    subgraph2D3D(dim, stvec, 'Argument', eigtxt, glims,[])
  end
end
end
%---------------------------------------------------------------------------------------------
function subgraph2D3D(dim, st, strleg,strtitle, glims,vlims)
% Utility function used to draw 2D or 3D data
%
% Input arguments are:
% dim                       : space dimension
% st (struct)               : structure containing data for the patch function
% strleg, strtitle (string) : legend and title
% glims : limits to be used on the geometry (in order to get the same aspect in all the figures)
% vlims : limits to be used on the values (in order to get the same scale in all the figures)

patch(st)
axis equal
xlabel(strleg)
title(strtitle)
shading interp
colorbar
switch dim
 case 2
  view(2)
  set(gca,'XLim',glims(:,1),'YLim',glims(:,2))
  if (~isempty(vlims) && abs(vlims(2)-vlims(1)>eps)), set(gca,'ZLim',vlims,'CLim',vlims), end
 case 3
  view(3)
  set(gca,'XLim',glims(:,1),'YLim',glims(:,2),'ZLim',glims(:,3))
  if ~isempty(vlims), set(gca,'CLim',vlims), end
  grid on
end
end
%---------------------------------------------------------------------------------------------
function printhints(rootfn,hintOK)
if ~hintOK, return, end
fprintf('%s\n','Tuning suggestions:', ' figure(N)', ' subplot(2,2,k)', ...
               ' rotate3d,   grid,   box', ' view(2),  view(3),  view([Nx Ny Nz])', ...
               ' shading faceted', ' axis equal,  axis normal', ...
               ' set(gca,''xtick'',[])       or  set(gca,''xtick'',[...])', ...
               ' caxis([...])', ' xlabel(...), ylabel(...), title(...)',...
              [' print(''-dpng'',''',rootfn,'FigN.png'')']);
end
%---------------------------------------------------------------------------------------------
function [edges, edgelts] = credgelist(elem,elemtype)
% Utility function that creates the list (edges) of the edges in the mesh.
% The output variables are:
%  - edges: 2-column array whose each line corresponds to an edge defined by the numbers of
% its two end-points, which are vertices of the mesh, taken from the array elem. The edges are
% sorted in ascending order according to the numbers of the vertices ; each edge appears only
% once in the list.
%  - edgelts: cell-array containing the numbers of the elements of the mesh sharing an edge.
% In other words, edgelts{I} is the list of the elements the edge number I belongs to, I being
% the rank of the edge in the above array edges.
edges = [];
for elty = 6:9
 ind = find(elemtype==elty);
 if ~isempty(ind)
  switch elty
   case 6 % tetrahedron
    edges = [edges; tetraedges(elem,ind)];
   case 7 % hexahedron
    edges = [edges; hexaedges(elem,ind)];
   case 8 % prism
    edges = [edges; prismedges(elem,ind)];
   case 9 % pyramid
    edges = [edges; pyramedges(elem,ind)];
  end
 end
end
% Remove identical edges and determine associated elements
sedges = sortrows([min(edges(:,1:2),[],2) max(edges(:,1:2),[],2) edges(:,3)]);
edges = unique(sedges(:,1:2),'rows');
ntoted = size(edges,1);
% Reserve memory
edgelts = cell(1,ntoted);
% Determine the list of elements each edge belongs to
disedges = diff(sedges(:,1:2));
ind = find(abs(disedges(:,1))+abs(disedges(:,2))>0);
ki = 1;
for ii=1:length(ind)
 ks = ind(ii);
 edgelts{ii} = sedges([ki:ks],3).';
 ki = ks + 1;
end
edgelts{ntoted} = sedges([ki:size(sedges,1)],3).';
end
%---------------------------------------------------------------------------------------------
function edges = tetraedges(elem,ind)
% Utility function that creates the list of the edges of the tetrahedra
% present in the array elem at rows numbers specified by ind.
edges = [
 [elem(ind,1) elem(ind,2) ind]
 [elem(ind,1) elem(ind,3) ind]
 [elem(ind,1) elem(ind,4) ind]
 [elem(ind,2) elem(ind,3) ind]
 [elem(ind,2) elem(ind,4) ind]
 [elem(ind,3) elem(ind,4) ind]
];
end
%---------------------------------------------------------------------------------------------
function edges = hexaedges(elem,ind)
% Utility function that creates the list of the edges of the hexahedra
% present in the array elem at rows numbers specified by ind.
edges = [
 [elem(ind,1) elem(ind,2) ind]
 [elem(ind,2) elem(ind,3) ind]
 [elem(ind,3) elem(ind,4) ind]
 [elem(ind,4) elem(ind,1) ind]
 [elem(ind,5) elem(ind,6) ind]
 [elem(ind,6) elem(ind,7) ind]
 [elem(ind,7) elem(ind,8) ind]
 [elem(ind,8) elem(ind,5) ind]
 [elem(ind,1) elem(ind,5) ind]
 [elem(ind,2) elem(ind,6) ind]
 [elem(ind,3) elem(ind,7) ind]
 [elem(ind,4) elem(ind,8) ind]
];
end
%---------------------------------------------------------------------------------------------
function edges = prismedges(elem,ind)
% Utility function that creates the list of the edges of the prisms
% present in the array elem at rows numbers specified by ind.
edges = [
 [elem(ind,1) elem(ind,2) ind]
 [elem(ind,2) elem(ind,3) ind]
 [elem(ind,3) elem(ind,1) ind]
 [elem(ind,4) elem(ind,5) ind]
 [elem(ind,5) elem(ind,6) ind]
 [elem(ind,6) elem(ind,4) ind]
 [elem(ind,1) elem(ind,4) ind]
 [elem(ind,2) elem(ind,5) ind]
 [elem(ind,3) elem(ind,6) ind]
];
end
%---------------------------------------------------------------------------------------------
function edges = pyramedges(elem,ind)
% Utility function that creates the list of the edges of the pyramids
% present in the array elem at rows numbers specified by ind.
edges = [
 [elem(ind,1) elem(ind,2) ind]
 [elem(ind,2) elem(ind,3) ind]
 [elem(ind,3) elem(ind,4) ind]
 [elem(ind,4) elem(ind,1) ind]
 [elem(ind,1) elem(ind,5) ind]
 [elem(ind,2) elem(ind,5) ind]
 [elem(ind,3) elem(ind,5) ind]
 [elem(ind,4) elem(ind,5) ind]
];
end
%---------------------------------------------------------------------------------------------
function draw1D2Dmesh(coord,elem,elemtype)
% Plot the wireframe mesh defining the 1D domain in a 1D, 2D or 3D space
% or the 2D domain in a 2D or 3D space.

% Draw up the list of edges in the mesh
edges = [];
for elty = 3:5
 ind = find(elemtype==elty);
 if ~isempty(ind)
  switch elty
   case 3 % segment
    edges = [edges; [elem(ind,1) elem(ind,2)]];
   case 4 % triangle
    edges = [edges; triedges(elem,ind)];
   case 5 % quadrangle
    edges = [edges; quadedges(elem,ind)];
  end
 end
end
% Remove identical edges
edges = unique([min(edges,[],2) max(edges,[],2)],'rows');
figure('Color','w');
% Draw the mesh
dim = size(coord,2);
co = [coord; NaN(1,dim)];
num = [edges size(co,1)*ones(size(edges,1),1)].';
num = num(:);
switch dim
 case 1
  plot(co(num,1),zeros(length(co(num,1)),1),'+-')
 case 2
  plot(co(num,1),co(num,2))
 case 3
  MatOct = ver;
  if MatOct(1).Name(1) == 'O'
%   Ok for both Matlab and Octave
   plot3(co(num,1),co(num,2),co(num,3))
  else
%   Ok for Matlab only
   hold on
   for k=1:size(elem,1)
    fill3([co(elem(k,:),1);co(elem(k,1),1)],...
          [co(elem(k,:),2);co(elem(k,1),2)],...
          [co(elem(k,:),3);co(elem(k,1),3)],    [0.8 0.8 0.8],'Facealpha',0.8)
   end
  end
  rotate3d
end
axis equal
title('Domain mesh based on interpolation nodes.')
end
%---------------------------------------------------------------------------------------------
function edges = triedges(elem,ind)
% Utility function that creates the list of the edges of the triangles
% present in the array elem at rows numbers specified by ind.
edges = [
 [elem(ind,1) elem(ind,2)]
 [elem(ind,1) elem(ind,3)]
 [elem(ind,2) elem(ind,3)]
];
end
%---------------------------------------------------------------------------------------------
function edges = quadedges(elem,ind)
% Utility function that creates the list of the edges of the quadrangles
% present in the array elem at rows numbers specified by ind.
edges = [
 [elem(ind,1) elem(ind,2)]
 [elem(ind,2) elem(ind,3)]
 [elem(ind,3) elem(ind,4)]
 [elem(ind,4) elem(ind,1)]
];
end
%---------------------------------------------------------------------------------------------
function draw3Dmesh(coord,edges)
% Plot the wireframe mesh defining the 3D domain

figure;
% Draw the mesh
co = [coord; NaN(1,3)];
num = [edges size(co,1)*ones(size(edges,1),1)].';
num = num(:);
% Given three vectors, plot3 draws a single continuous line. But, inserting a dummy point with
% coordinates (NaN, NaN, NaN) after each edge (every three points) allows to draw a broken line.
plot3(co(num,1),co(num,2),co(num,3))
axis equal
rotate3d
title('Domain mesh based on interpolation nodes.')
end
%---------------------------------------------------------------------------------------------
function [hp,hq] = drawMeCuPl(coord,edges,ptPL,norPL,norlen,glims)
% Plot the wireframe mesh defining the 3D domain, along with the cutting plane.
% Returns the handles of the arrow and the polygon defining the cutting plane.

draw3Dmesh(coord,edges)
hold
% Normal vector to the cutting plane
v = norPL/norm(norPL)*norlen;
hq=quiver3(ptPL(1),ptPL(2),ptPL(3), v(1),v(2),v(3),'color','red','linewidth',3);
% Intersection of the cutting plane and the bounding box
polpts = cutbox(ptPL,norPL,glims);
hp = plot3(polpts(:,1),polpts(:,2),polpts(:,3),'color','red','linewidth',3);
end
%---------------------------------------------------------------------------------------------
function polpts = cutbox(ptPL,norPL,glims)
% Utility function that computes the polygon which is the intersection
% of the cutting plane and the bounding box of the mesh.
% The polygon is returned as a list of vertex coordinates in a "ready to draw" form.

% Coordinates of the bounding box
bbco = zeros(8,3);
bbco(1,:) = glims(1,:);
bbco(2,:) = [glims(2,1) glims(1,2) glims(1,3)];
bbco(3,:) = [glims(2,1) glims(2,2) glims(1,3)];
bbco(4,:) = [glims(1,1) glims(2,2) glims(1,3)];
bbco(5,:) = [glims(1,1) glims(1,2) glims(2,3)];
bbco(6,:) = [glims(2,1) glims(1,2) glims(2,3)];
bbco(7,:) = glims(2,:);
bbco(8,:) = [glims(1,1) glims(2,2) glims(2,3)];
bbedg = hexaedges(1:8,1);
bbedg = bbedg(:,1:2);
% Compute the intersection points between the plane and the bounding box
[bbpts, vf, edgnplane] = intersect(ptPL,norPL,bbedg,bbco,ones(8,1));
% G is the barycenter of these nbp points that are the vertices of a convex polygon
nbp = size(bbpts,1);
G = sum(bbpts)/nbp;
% Now look for the correct connections between some of these points in order to draw the polygon
% List of all possible edges between these points
pedg = [];
for ii=1:nbp-1
 pedg = [pedg ; [ii*ones(nbp-ii,1) [ii+1:nbp].']];
end
% Distance between G and the mid-points of these edges
nbMi = size(pedg,1);
GMi = (bbpts(pedg(:,1),:)+bbpts(pedg(:,2),:))/2 - ones(nbMi,1)*G;
dist = zeros(nbMi,1);
for ii=1:nbMi
 dist(ii) = norm(GMi(ii,:));
end
sdist = sortrows([dist [1:nbMi].'],-1);
% Select the nbp first edges that defines the boundary of the polygon
fedg = pedg(sdist(1:nbp,2),:);
bbpts = [bbpts; NaN(1,3)];
num = [fedg size(bbpts,1)*ones(size(fedg,1),1)].';
polpts = bbpts(num(:),:);
end
%---------------------------------------------------------------------------------------------
function [ok, nptPL, nnorPL] = changeplane(ptPL,norPL, hp,hq,norlen,glims)
% Change the definition of the cutting plane currently defined by the point ptPL
% and the orthogonal vector norPL.
% This function allows to specify a new point nptPL or a new vector nnorPL.
% ok is used by the calling function to control the interaction with the user.
nptPL = ptPL;
nnorPL = norPL;
disp('')
disp(['Current value of P = ',num2str(ptPL)])
disp(['Current value of V = ',num2str(norPL)])
fprintf('  %s\n', '1 : change P and draw', '2 : change V and draw', '3 : draw without change', '0 : quit')
ok = input('Your choice (other value equivalent to 3) : ');
if isempty(ok), ok = -1; end
switch ok
 case 1
  nptPL(1) = input('Give first  new coordinate (Px) : ');
  nptPL(2) = input('Give second new coordinate (Py) : ');
  nptPL(3) = input('Give third  new coordinate (Pz) : ');
 case 2
  nnorPL(1) = input('Give first  new component (Vx) : ');
  nnorPL(2) = input('Give second new component (Vy) : ');
  nnorPL(3) = input('Give third  new component (Vz) : ');
end
% Update the position of the cutting plane on the figure displaying the mesh (unless
% this figure has been destroyed)
if ishandle(hq) && ishandle(hp)
 v = nnorPL/norm(nnorPL)*norlen;
 set(hq, 'udata',v(1) ,'vdata',v(2) ,'wdata',v(3), ...
         'xdata',nptPL(1), 'ydata',nptPL(2), 'zdata',nptPL(3))
 polpts = cutbox(nptPL,nnorPL,glims);
 set(hp, 'xdata',polpts(:,1), 'ydata',polpts(:,2), 'zdata',polpts(:,3))
end
end
%---------------------------------------------------------------------------------------------
function [pts, vf, edgnplane] = intersect(ptPL,norPL,edges,coord,val)
% Computes the intersection of the edges of the mesh with the cutting plane.
% pts       : list of the points which are the intersections of the edges
%             and the plane passing by ptPL and orhogonal to the vector norPL.
% vf        : value associated to each of these points, computed by linear interpolation
%             along the edge it comes from.
% edgnplane : the sub-list of the edges crossed by the plane.
epsil = 1.e3*eps;
ptPL = ptPL(:).';
norPL = norPL(:).';

% Identify which edge end-points lie in the plane. This potentially targets any vertex
% of the mesh, and each of them should be counted once.
% Identify the edges these vertices belong to: strictly speeking, they are not crossed
% by the plane, but one or both of their end-points belong to the plane.
ned = size(edges,1);
MptPL = ones(ned,1)*ptPL;
MnorPL = ones(ned,1)*norPL;
PAn = dot(coord(edges(:,1),:)-MptPL, MnorPL, 2);
PBn = dot(coord(edges(:,2),:)-MptPL, MnorPL, 2);
% Check first end-point of each edge
ind = find(abs(PAn) < epsil);
edgnplane = ind.';
indpts = edges(ind,1);
% Check second end-point of each edge
ind = find(abs(PBn) < epsil);
edgnplane = unique([edgnplane ind.']);
indpts = unique([indpts; edges(ind,2)]);
% List of vertices lying in the plane and associated values:
pts = [coord(indpts,:)];
vf = [val(indpts)];
%
% Now identify which edges AB are really crossed by the plane.
ind = find(PAn .* PBn < -epsil);
if ~isempty(ind)
 edgnplane = [edgnplane ind.'];
 iA = edges(ind,1);
 iB = edges(ind,2);
 AB = coord(iB,:) - coord(iA,:);
 ABn = dot(AB, ones(size(AB,1),1)*norPL, 2);
 % Compute the intersection of the edge and the plane.
 % Limit cases where A or B or both A and B have already been taken into account,
 % so ABn is not null.
 lam = - PAn(ind) ./ ABn;
 pts = [pts; coord(iA,:)+(lam*ones(1,3)).*AB];
 vf = [vf; (1-lam).*val(iA) + lam.*val(iB)];
end
end
%---------------------------------------------------------------------------------------------
function tri = crtri(norPL, pts,coord,elem,elemtype, edgnplane,edgelts)
% Creates a triangulation whose vertices are the 3D points (in pts)
% lying in the plane orthogonal to the vector norPL
nbpts = size(pts,1);
if nbpts < 3
 disp('*** WARNING: degenerated data set. The plane probably does not intersect the domain.')
 tri = [];
else
 n = min(10,nbpts);
 lg = 0;
 for i=1:n
  p = pts(i,:) - pts(1,:);
  lgi = dot(p, p);
  if lgi > lg
   lg = lgi;
   j = i;
  end
 end
 % Pj is expected to be "far" from P1.
 % Compute axes (P1 ; vx,vy) in the plane orthogonal to the vector norPL.
 vx = pts(j,:) - pts(1,:);
 vx = vx / norm(vx);
 vy = cross(norPL,vx);
 vy = vy / norm(vy);
 % Compute the 2D coordinates of the given points in these axes
 % and create the corresponding (2D) triangulation
 Mptsmpt1 = pts-ones(nbpts,1)*pts(1,:);
 x = dot(Mptsmpt1, ones(nbpts,1)*vx, 2);
 y = dot(Mptsmpt1, ones(nbpts,1)*vy, 2);
 tri = delaunay(x,y);
 tri = trimtri(tri, pts,coord,elem,elemtype, edgnplane,edgelts);
end
end
%---------------------------------------------------------------------------------------------
function [ttri, elim] = trimtri(tri, pts,coord,elem,elemtype, edgnplane,edgelts)
% Remove triangles that are outside the 3D domain.
% ttri is the result of this check and is a subset of tri.
% elim is true if some triangles have been removed, false if the triangulation is unchanged.

mepsil = -1.e-6;
elim = 0;
% Draw up the list of elements crossed by the plane
elemplane = [];
for ned=1:size(edgnplane,2)
 elemplane = [elemplane edgelts{edgnplane(ned)}];
end
elemplane=unique(elemplane);
%
% Isobarycenters of the triangles lying in the plane
gcn = [(pts(tri(:,1),:) + pts(tri(:,2),:) + pts(tri(:,3),:))/3];
%
% Draw up the list of the tetrahedra subdividing the elements crossed by the plane
tetlist = crtetlist(elem(elemplane,:),elemtype(elemplane));
% Check the position of the isobarycenters of the triangles with respect to
% the tetrahedra using their barycentric coordinates
A1 = [coord(tetlist(:,1),:)];
A2 = [coord(tetlist(:,2),:)];
A3 = [coord(tetlist(:,3),:)];
A4 = [coord(tetlist(:,4),:)];
V1=A1-A4; V2=A2-A4; V3=A3-A4; W = cross(V1,V2); v = dot(W,V3,2);
nbtet = size(tetlist,1);
% Keep the triangles that are inside the domain
lambda = zeros(nbtet,4);
k = [];
rk = 1;
for nt=1:size(gcn,1)
 M = ones(nbtet,1)*gcn(nt,:);
 V1=A2-M; V2=A4-M; V3=A3-M; W = cross(V1,V2); lambda(:,1) = dot(W,V3,2)./v;
 V1=A4-M; V2=A1-M; V3=A3-M; W = cross(V1,V2); lambda(:,2) = dot(W,V3,2)./v;
 V1=A2-M; V2=A1-M; V3=A4-M; W = cross(V1,V2); lambda(:,3) = dot(W,V3,2)./v;
 lambda(:,4) = 1 - lambda(:,1) - lambda(:,2) - lambda(:,3);
 %V1=A1-M; V2=A2-M; V3=A3-M; W = cross(V1,V2); lambda(:,4) = dot(W,V3,2)./v;
 if max(min(lambda,[],2)) > mepsil
  % lambda_i > 0 for some tetrahedron, so the point is inside the domain
  k = [k nt];
 else
  %disp(['Triangle ', int2str(nt),' removed'])
  elim = 1;
 end
end
ttri = tri(k,:);
end
%---------------------------------------------------------------------------------------------
function tetlist = crtetlist(elem,elemtype)
% Create the list of the tetrahedra deduced from the initial mesh
% by subdividing each element into several tetrahedra.
% Remark: if all elements in the initial mesh are tetrahedra, the output list tetlist
%         is equal to the input data elem.
tetlist = [];
for elty = 6:9
 ind = find(elemtype==elty);
 if ~isempty(ind)
  switch elty
   case 6 % tetrahedron
    tetlist = [tetlist; elem(ind, :)];
   case 7 % hexahedron
    tetlist = [tetlist
               elem(ind, [1 3 8 4])
               elem(ind, [1 8 6 5])
               elem(ind, [3 1 6 2])
               elem(ind, [3 6 8 7])
               elem(ind, [1 6 8 3])];
   case 8 % prism
    tetlist = [tetlist
               elem(ind, [2 3 4 1])
               elem(ind, [2 4 6 5])
               elem(ind, [2 6 4 3])];
   case 9 % pyramid
    tetlist = [tetlist
               elem(ind, [2 3 5 1])
               elem(ind, [3 4 5 1])];
  end
 end
end
end
