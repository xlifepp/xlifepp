# define user CMakeFiles.txt
# variables to be set :
# CMAKE_SOURCE_DIR -> the directory where xlife++ is installed, namely containing this CMakeLists.txt file
# LAPACKBLAS : possible values -> XLIFEPP_WITH_LAPACKBLAS, XLIFEPP_WITHOUT_LAPACKBLAS
# ARPACK : possible values -> XLIFEPP_WITH_ARPACK, XLIFEPP_WITHOUT_ARPACK
# UMFPACK : possible values -> XLIFEPP_WITH_UMFPACK, XLIFEPP_WITHOUT_UMFPACK
# OMP : possible values -> XLIFEPP_WITH_OMP, XLIFEPP_WITHOUT_OMP
# EIGEN : possible values -> XLIFEPP_WITH_EIGEN, XLIFEPP_WITHOUT_EIGEN
# AMOS : possible values -> XLIFEPP_WITH_AMOS, XLIFEPP_WITHOUT_AMOS
# OpenMP_CXX_FLAGS -> flags to add to compiler to manage OpenMP
# XLIFEPP_FROM_SRCS -> ON if XLiFE++ is installed from sources, else OFF
# XLIFEPP_INFO_TXT_DIR -> the directory where info.txt file is
configure_file(${CMAKE_SOURCE_DIR}/etc/templates/CMakeLists-usr.cmake.txt ${CMAKE_BINARY_DIR}/CMakeLists.txt @ONLY)

# ask cmake --help to get the list of available generators
execute_process(COMMAND ${CMAKE_COMMAND} --help OUTPUT_VARIABLE cmake_help)
set(GENERATORSTRINGTOFIND "The following generators are available on this platform:")
# string(FIND ${cmake_help} "The following generators are available on this platform (* marks default):" cmake_generators_start_index)
string(FIND ${cmake_help} ${GENERATORSTRINGTOFIND} cmake_generators_start_index)
if (${cmake_generators_start_index} STREQUAL "-1")
  set(GENERATORSTRINGTOFIND "The following generators are available on this platform (* marks default):")
  string(FIND ${cmake_help} ${GENERATORSTRINGTOFIND} cmake_generators_start_index)
endif()
if (${cmake_generators_start_index} STREQUAL "-1")
  message(FATAL_ERROR "The cmake --help command displays text that is not handled. Please contact administrators and give them the version of cmake you use.")
endif()
string(LENGTH ${GENERATORSTRINGTOFIND} stringlength)
math(EXPR cmake_generators_start_index ${cmake_generators_start_index}+${stringlength}+1)
string(SUBSTRING ${cmake_help} ${cmake_generators_start_index} -1 cmake_generators)
file(WRITE ${CMAKE_BINARY_DIR}/cmake_generators.txt ${cmake_generators})
file(STRINGS ${CMAKE_BINARY_DIR}/cmake_generators.txt cmake_generator_list)
file(REMOVE ${CMAKE_BINARY_DIR}/cmake_generators.txt)
list(LENGTH cmake_generator_list nbgeneratorlines)
math(EXPR nbgeneratorlinesM1 ${nbgeneratorlines}-1)

########################################################################################################################
#
# With cmake 3.3.2 on MacOS X Mavericks 10.9.5, `cmake --help` gives :
#
#  Unix Makefiles               = Generates standard UNIX makefiles.
#  Ninja                        = Generates build.ninja files.
#  Xcode                        = Generate Xcode project files.
#  CodeBlocks - Ninja           = Generates CodeBlocks project files.
#  CodeBlocks - Unix Makefiles  = Generates CodeBlocks project files.
#  CodeLite - Ninja             = Generates CodeLite project files.
#  CodeLite - Unix Makefiles    = Generates CodeLite project files.
#  Eclipse CDT4 - Ninja         = Generates Eclipse CDT 4.0 project files.
#  Eclipse CDT4 - Unix Makefiles= Generates Eclipse CDT 4.0 project files.
#  KDevelop3                    = Generates KDevelop 3 project files.
#  KDevelop3 - Unix Makefiles   = Generates KDevelop 3 project files.
#  Kate - Ninja                 = Generates Kate project files.
#  Kate - Unix Makefiles        = Generates Kate project files.
#  Sublime Text 2 - Ninja       = Generates Sublime Text 2 project files.
#  Sublime Text 2 - Unix Makefiles
#                               = Generates Sublime Text 2 project files.
#
###########################
#
# With CMake 3.4.3 on Windows 7, `cmake --help`gives :
#
#  Visual Studio 14 2015 [arch] = Generates Visual Studio 2015 project files.
#                                 Optional [arch] can be "Win64" or "ARM".
#  Visual Studio 12 2013 [arch] = Generates Visual Studio 2013 project files.
#                                 Optional [arch] can be "Win64" or "ARM".
#  Visual Studio 11 2012 [arch] = Generates Visual Studio 2012 project files.
#                                 Optional [arch] can be "Win64" or "ARM".
#  Visual Studio 10 2010 [arch] = Generates Visual Studio 2010 project files.
#                                 Optional [arch] can be "Win64" or "IA64".
#  Visual Studio 9 2008 [arch]  = Generates Visual Studio 2008 project files.
#                                 Optional [arch] can be "Win64" or "IA64".
#  Visual Studio 8 2005 [arch]  = Generates Visual Studio 2005 project files.
#                                 Optional [arch] can be "Win64".
#  Visual Studio 7 .NET 2003    = Generates Visual Studio .NET 2003 project
#                                 files.
#  Visual Studio 7              = Deprecated.  Generates Visual Studio .NET
#                                 2002 project files.
#  Visual Studio 6              = Deprecated.  Generates Visual Studio 6
#                                 project files.
#  Borland Makefiles            = Generates Borland makefiles.
#  NMake Makefiles              = Generates NMake makefiles.
#  NMake Makefiles JOM          = Generates JOM makefiles.
#  Green Hills MULTI            = Generates Green Hills MULTI files
#                                 (experimental, work-in-progress).
#  MSYS Makefiles               = Generates MSYS makefiles.
#  MinGW Makefiles              = Generates a make file for use with
#                                 mingw32-make.
#  Unix Makefiles               = Generates standard UNIX makefiles.
#  Ninja                        = Generates build.ninja files.
#  Watcom WMake                 = Generates Watcom WMake makefiles.
#  CodeBlocks - MinGW Makefiles = Generates CodeBlocks project files.
#  CodeBlocks - NMake Makefiles = Generates CodeBlocks project files.
#  CodeBlocks - Ninja           = Generates CodeBlocks project files.
#  CodeBlocks - Unix Makefiles  = Generates CodeBlocks project files.
#  CodeLite - MinGW Makefiles   = Generates CodeLite project files.
#  CodeLite - NMake Makefiles   = Generates CodeLite project files.
#  CodeLite - Ninja             = Generates CodeLite project files.
#  CodeLite - Unix Makefiles    = Generates CodeLite project files.
#  Eclipse CDT4 - MinGW Makefiles
#                               = Generates Eclipse CDT 4.0 project files.
#  Eclipse CDT4 - NMake Makefiles
#                               = Generates Eclipse CDT 4.0 project files.
#  Eclipse CDT4 - Ninja         = Generates Eclipse CDT 4.0 project files.
#  Eclipse CDT4 - Unix Makefiles= Generates Eclipse CDT 4.0 project files.
#  Kate - MinGW Makefiles       = Generates Kate project files.
#  Kate - NMake Makefiles       = Generates Kate project files.
#  Kate - Ninja                 = Generates Kate project files.
#  Kate - Unix Makefiles        = Generates Kate project files.
#  Sublime Text 2 - MinGW Makefiles
#                               = Generates Sublime Text 2 project files.
#  Sublime Text 2 - NMake Makefiles
#                               = Generates Sublime Text 2 project files.
#  Sublime Text 2 - Ninja       = Generates Sublime Text 2 project files.
#  Sublime Text 2 - Unix Makefiles
#                               = Generates Sublime Text 2 project files.
#
###########################
#
# there are lines of the form :
# - "generator name          = generator comment": this is the standard case
# - "generator name                             ": In this case, the comment will be on the next line.
# - "                        = generator comment": In this case, the generator name will be on the previous line.
# - "generator prefix [arch] = generator comment": In this case, we have to create a generator name per value of [arch]
#                                                  Possible values are between double quotes on the next line.
# - "                          generator comment": In this case, the generator name and the beginning of the comment will be on
#                                                  the previous line. It may contain [arch definition]
#
########################################################################################################################

set(buffer "")
set(isArch "0")

foreach (i RANGE ${nbgeneratorlinesM1})
  list(GET cmake_generator_list ${i} line)
  if (NOT ${line} STREQUAL "")
    string(FIND ${line} = EQUALINDEX)
    string(STRIP ${line} buffer)
    string(SUBSTRING ${buffer} 0 1 FIRSTCHARACTER)
    string(FIND ${line} ${FIRSTCHARACTER} FIRSTLETTERINDEX)
    string(LENGTH ${line} STRLENGTH)
    if (NOT ${EQUALINDEX} STREQUAL "-1")
      # there is a = character
      if (${EQUALINDEX} STREQUAL ${FIRSTLETTERINDEX})
        # line is of the form "    = comment"
        math(EXPR EQUALINDEXP2 ${EQUALINDEX}+2)
        string(SUBSTRING ${line} ${EQUALINDEXP2} -1 generatorComment)
        string(STRIP ${generatorComment} generatorComment)
        list(APPEND generatorsComments ${generatorComment})
      elseif( ${FIRSTCHARACTER} STREQUAL "*")
        # line is of the form "* name = comment"
        math(EXPR EQUALINDEXM1 ${EQUALINDEX}-1)
        string(SUBSTRING ${line} 1 ${EQUALINDEXM1} generatorName)
        math(EXPR EQUALINDEXP2 ${EQUALINDEX}+2)
        string(SUBSTRING ${line} ${EQUALINDEXP2} -1 generatorComment)
        string(STRIP ${generatorName} generatorName)
        string(FIND ${generatorName} "[arch]" ARCHINDEX)
        if (NOT ${ARCHINDEX} STREQUAL "-1")
          # if generator name contains "[arch]" we store what is before "[arch]"
          # generator names will be defined by analysing next line
          string(SUBSTRING ${generatorName} 0 ${ARCHINDEX} generatorName)
          string(STRIP ${generatorName} generatorName)
          set(isArch "1")
        endif()
        list(APPEND generatorsNames ${generatorName})
        string(STRIP ${generatorComment} generatorComment)
        list(APPEND generatorsComments ${generatorComment})
      else()
        # line is of the form "name = comment"
        string(SUBSTRING ${line} 0 ${EQUALINDEX} generatorName)
        math(EXPR EQUALINDEXP2 ${EQUALINDEX}+2)
        string(SUBSTRING ${line} ${EQUALINDEXP2} -1 generatorComment)
        string(STRIP ${generatorName} generatorName)
        string(FIND ${generatorName} "[arch]" ARCHINDEX)
        if (NOT ${ARCHINDEX} STREQUAL "-1")
          # if generator name contains "[arch]" we store what is before "[arch]"
          # generator names will be defined by analysing next line
          string(SUBSTRING ${generatorName} 0 ${ARCHINDEX} generatorName)
          string(STRIP ${generatorName} generatorName)
          set(isArch "1")
        endif()
        list(APPEND generatorsNames ${generatorName})
        string(STRIP ${generatorComment} generatorComment)
        list(APPEND generatorsComments ${generatorComment})
      endif()
    else()
      # there is no = character
      if (${FIRSTLETTERINDEX} STREQUAL "2")
        # line is of the form "name        "
        string(STRIP ${line} generatorName)
        list(APPEND generatorsNames ${generatorName})
      else()
        # line is of the form "      comment"
        # if [arch] was detected in the previous line, the last generator of the list must be removed but used to append true names
        if (${isArch} STREQUAL "1")
          # comment contains strings corresponding to possible values of [arch]
          # we have to get all of them
          string(REGEX MATCHALL \"[^\"]+\" matches ${line})
          list(LENGTH matches nbMatches)
          list(LENGTH generatorsNames nbGenerators)
          math(EXPR j ${nbGenerators}-1)
          list(GET generatorsNames ${j} rootname)
          list(REMOVE_AT generatorsNames ${j})
          list(LENGTH generatorsComments nbComments)
          math(EXPR j ${nbComments}-1)
          list(GET generatorsComments ${j} rootcomment)
          list(REMOVE_AT generatorsComments ${j})
          foreach (match IN LISTS matches)
            string(REPLACE \" " " match ${match})
            string(STRIP ${match} match)
            list(APPEND generatorsNames "${rootname} ${match}")
            list(APPEND generatorsComments "${rootcomment}")
          endforeach()
          set(isArch "0")
        else()
          # we have to update the last stored comment
          list(LENGTH generatorsComments nbComments)
          math(EXPR j ${nbComments}-1)
          list(GET generatorsComments ${j} rootcomment)
          list(REMOVE_AT generatorsComments ${j})
          string(STRIP ${line} comment)
          list(APPEND generatorsComments "${rootcomment} ${comment}")
        endif()
      endif()
    endif()
  endif()
endforeach()

# now, we have the good list of available generators
# First, we check if Green Hills MULTI generator is in the list to put it at the end
list(FIND generatorsNames "Green Hills MULTI" GHM_INDEX)
if (NOT GHM_INDEX STREQUAL "-1")
  list(GET generatorsNames ${GHM_INDEX} GHMLabel)
  list(GET generatorsComments ${GHM_INDEX} GHMComment)
  list(REMOVE_AT generatorsNames ${GHM_INDEX})
  list(REMOVE_AT generatorsComments ${GHM_INDEX})
  list(APPEND generatorsNames ${GHMLabel})
  list(APPEND generatorsComments ${GHMComment})
endif()

# now, we have the good list of available generators in the rightful order
list(LENGTH generatorsNames NB_GENERATORS)
set(UNIX_GENERATORS_CHOICE "echo \"The following generators are available on this platform:\"\n")
set(UNIX_GENERATORS_DEF "case \$answer in\n")
set(UNIX_GENERATORS_CHECK_CHOICE "case \$answer in\n      ")
set(WIN_GENERATORS_CHOICE "echo The following generators are available on this platform:\n")
set(WIN_GENERATORS_DEF "")
set(generatorIndex "0")
foreach (generator IN LISTS generatorsNames)
  math(EXPR generatorIndex ${generatorIndex}+1)
  set(UNIX_GENERATORS_CHOICE "${UNIX_GENERATORS_CHOICE}    echo \"${generatorIndex} -> ${generator}\"\n")
  set(WIN_GENERATORS_CHOICE "${WIN_GENERATORS_CHOICE}echo ${generatorIndex} -^> ${generator}\n")
  set(UNIX_GENERATORS_DEF "${UNIX_GENERATORS_DEF}      ${generatorIndex})\n        generator=\"${generator}\"\n        ;;\n")
  set(WIN_GENERATORS_DEF "${WIN_GENERATORS_DEF}if %answer% EQU ${generatorIndex} set generator=\"${generator}\"\n")
  if (${generatorIndex} STREQUAL "1")
    set(UNIX_GENERATORS_CHECK_CHOICE "${UNIX_GENERATORS_CHECK_CHOICE}${generatorIndex}")
  else()
    set(UNIX_GENERATORS_CHECK_CHOICE "${UNIX_GENERATORS_CHECK_CHOICE}|${generatorIndex}")
  endif()
endforeach()
set(UNIX_GENERATORS_DEF "${UNIX_GENERATORS_DEF}    esac\n")
set(UNIX_GENERATORS_CHECK_CHOICE "${UNIX_GENERATORS_CHECK_CHOICE})\n        ;;\n      \"\")\n        answer=1\n        ;;\n      *)\n        echo \"\$answer is not between 1 and ${NB_GENERATORS} !!! Abort\"\n        exit\n        ;;\n    esac\n")

# now, we get the full list of example files
file(GLOB examples RELATIVE ${CMAKE_SOURCE_DIR}/examples ${CMAKE_SOURCE_DIR}/examples/*.cpp)
if (NOT XLIFEPP_FROM_SRCS)
  file(GLOB examples RELATIVE ${CMAKE_SOURCE_DIR}/share/examples ${CMAKE_SOURCE_DIR}/share/examples/*.cpp)
endif()

list(SORT examples)
list(APPEND mainFiles "main.cpp")
foreach (example IN LISTS examples)
  list(APPEND mainFiles ${example})
endforeach()
list(LENGTH mainFiles NB_MAIN_FILES)
set(UNIX_MAIN_FILES_CHOICE "echo \"The following main files are available:\"\n")
set(UNIX_MAIN_FILES_DEF "case \$answerFile in\n")
set(UNIX_MAIN_FILES_CHECK_CHOICE "case \$answerFile in\n          ")
set(WIN_MAIN_FILES_CHOICE "echo \"The following main files are available:\"\n")
set(WIN_MAIN_FILES_DEF "")
set(mainFileIndex "0")
foreach (mainFile IN LISTS mainFiles)
  math(EXPR mainFileIndex ${mainFileIndex}+1)
  set(UNIX_MAIN_FILES_CHOICE "${UNIX_MAIN_FILES_CHOICE}        echo \"${mainFileIndex} -> ${mainFile}\"\n")
  set(WIN_MAIN_FILES_CHOICE "${WIN_MAIN_FILES_CHOICE}echo ${mainFileIndex} -^> ${mainFile}\n")
  set(UNIX_MAIN_FILES_DEF "${UNIX_MAIN_FILES_DEF}          ${mainFileIndex})\n            XLIFEPP_BUILD_WITH_FILE=\"${mainFile}\"\n            ;;\n")
  set(WIN_MAIN_FILES_DEF "${WIN_MAIN_FILES_DEF}if %answerFile% EQU ${mainFileIndex} set XLIFEPP_BUILD_WITH_FILE=\"${mainFile}\"\n")
  if (${mainFileIndex} STREQUAL "1")
    set(UNIX_MAIN_FILES_CHECK_CHOICE "${UNIX_MAIN_FILES_CHECK_CHOICE}${mainFileIndex}")
  else()
    set(UNIX_MAIN_FILES_CHECK_CHOICE "${UNIX_MAIN_FILES_CHECK_CHOICE}|${mainFileIndex}")
  endif()
endforeach()
set(UNIX_MAIN_FILES_DEF "${UNIX_MAIN_FILES_DEF}        esac\n")
set(UNIX_MAIN_FILES_CHECK_CHOICE "${UNIX_MAIN_FILES_CHECK_CHOICE})\n            ;;\n          \"\")\n            answerFile=1\n            ;;\n          *)\n            echo \"\$answerFile is not between 1 and ${NB_MAIN_FILES} !!! Abort\"\n            exit\n            ;;\n        esac\n")

set(XLIFEPP_OMP 0)
if (${OMP} STREQUAL XLIFEPP_WITH_OMP)
  set(XLIFEPP_OMP 1)
endif()

# default application to open a pdf
set(DEFAULT_PDF_VIEWER acroread)
if (APPLE)
  set(DEFAULT_PDF_VIEWER open)
endif()

# generating xlifepp.sh script
# variables to be set :
# NATIVE_CMAKE_SOURCE_DIR -> the directory where xlife++ is installed, namely containing this CMakeLists.txt file
# NATIVE_CMAKE_BINARY_DIR -> the directory where cmake is run (the build directory)
# NATIVE_LIBRARY_OUTPUT_PATH -> the directory where XLiFE++ libraries are generated
# CMAKE_CXX_COMPILER -> the compiler used to build XLiFE++
# CMAKE_CXX_REAL_COMPILER -> the compiler with absolute and true path, used to build XLiFE++
# CMAKE_BUILD_TYPE -> Debug/Release/...
# VERSION_NUMBER -> version number of the last commit
# VERSION_DATE -> date of the last commit
# DEFAULT_PDF_VIEWER -> default application to use to open pdf documentation
# XLIFEPP_OMP -> 0 if without OpenMP, else 1
# XLIFEPP_FROM_SRCS -> ON if XLiFE++ is installed from sources, else OFF
# UNIX_GENERATORS_CHOICE -> see above
# UNIX_GENERATORS_CHECK_CHOICE -> see above
# UNIX_GENERATORS_DEF -> see above
# UNIX_MAIN_FILES_CHOICE -> see above
# UNIX_MAIN_FILES_CHECK_CHOICE -> see above
# UNIX_MAIN_FILES_DEF -> see above
cleanPathWithBlankSpaces(NATIVE_CMAKE_SOURCE_DIR "${NATIVE_CMAKE_SOURCE_DIR}")
cleanPathWithBlankSpaces(NATIVE_CMAKE_BINARY_DIR "${NATIVE_CMAKE_BINARY_DIR}")
cleanPathWithBlankSpaces(NATIVE_LIBRARY_OUTPUT_PATH "${NATIVE_LIBRARY_OUTPUT_PATH}")
configure_file(${CMAKE_SOURCE_DIR}/etc/templates/xlifepp.cmake.sh ${CMAKE_SOURCE_DIR}/bin/xlifepp.sh @ONLY)
file(CHMOD ${CMAKE_SOURCE_DIR}/bin/xlifepp.sh FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

# generating xlifepp.bat script
# variables to be set :
# NATIVE_CMAKE_SOURCE_DIR -> the directory where xlife++ is installed, namely containing this CMakeLists.txt file
# NATIVE_CMAKE_BINARY_DIR -> the directory where cmake is run (the build directory)
# NATIVE_LIBRARY_OUTPUT_PATH -> the directory where XLiFE++ libraries are generated
# CMAKE_CXX_COMPILER -> the compiler used to build XLiFE++
# CMAKE_CXX_REAL_COMPILER -> the compiler with absolute and true path, used to build XLiFE++
# CMAKE_BUILD_TYPE -> Debug/Release/...
# VERSION_NUMBER -> version number of the last commit
# VERSION_DATE -> date of the last commit
# XLIFEPP_OMP -> 0 if without OpenMP, else 1
# XLIFEPP_FROM_SRCS -> ON if XLiFE++ is installed from sources, else OFF
# WIN_GENERATORS_CHOICE -> see above
# WIN_GENERATORS_DEF -> see above
# WIN_MAIN_FILES_CHOICE -> see above
# WIN_MAIN_FILES_DEF -> see above
# NB_GENERATORS -> see above
# NB_MAIN_FILES -> see above
if (WIN32)
  configure_file(${CMAKE_SOURCE_DIR}/etc/templates/xlifepp.cmake.bat ${CMAKE_SOURCE_DIR}/bin/xlifepp.bat @ONLY)
  file(CHMOD ${CMAKE_SOURCE_DIR}/bin/xlifepp.bat FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
endif()
