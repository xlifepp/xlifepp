# function guessCompiler
#   guess which compiler is under a given name by extracting its real basename,
#   in order to get a short name with the version number inside
#   Parameters:
#     RES_CXX_COMPILER: the short name of the compiler
#     RES_CXX_REAL_COMPILER: the real name of the compiler (no symlinks)
#     CXX_COMPILER: the input compiler name (destined to be CMAKE_CXX_COMPILER)
#
#   Example:
#   Your compiler is g++-8 on a Mac OS (installed by Homebrew)
#      guessCompiler(A B C "/usr/local/bin/g++-8")
#
#   A will be "g++-8.2.1"
#   B will be "/usr/local/Cellar/gcc@8/8.4.0_1/bin/g++-8"
#   C will be 8.2.1
#
function(guessCompiler RES_CXX_COMPILER RES_CXX_REAL_COMPILER RES_CXX_COMPILER_VERSION CXX_COMPILER)
  get_filename_component(REAL_COMPILER ${CXX_COMPILER} REALPATH)
  get_filename_component(COMPILER ${REAL_COMPILER} NAME)
  if (${COMPILER} STREQUAL "clang")
    unset(REAL_COMPILER)
    find_path(REAL_COMPILER clang++)
    set(REAL_COMPILER ${REAL_COMPILER}/clang++)
    set(COMPILER clang++)
  endif()
  if (${COMPILER} STREQUAL "clang++" 
   OR ${COMPILER} STREQUAL "clang++-libc++"
   OR ${COMPILER} STREQUAL "g++"
   OR ${COMPILER} STREQUAL "c++"
   OR ${COMPILER} STREQUAL "icc"
   OR ${COMPILER} STREQUAL "g++.exe"
   OR ${COMPILER} STREQUAL "c++.exe")
   execute_process(COMMAND ${CXX_COMPILER} -dumpversion OUTPUT_VARIABLE COMPILER_VERSION)
   string(STRIP ${COMPILER_VERSION} COMPILER_VERSION)
   if (${COMPILER} STREQUAL "clang++-libc++")
    set(COMPILER "clang++")
   endif()
   set(COMPILER "${COMPILER}-${COMPILER_VERSION}")
  endif()
  # shorten compiler name when prefixed with x86_64-linux-gnu- (on some Linux OS)
  string(REPLACE "x86_64-linux-gnu-" "" COMPILER ${COMPILER})
  set(${RES_CXX_COMPILER} ${COMPILER} PARENT_SCOPE)
  set(${RES_CXX_REAL_COMPILER} ${REAL_COMPILER} PARENT_SCOPE)
  set(${RES_CXX_COMPILER_VERSION} ${COMPILER_VERSION} PARENT_SCOPE)
endfunction(guessCompiler)

# function addIfEnsuredOnce
#   Add a string to a variable (list or string) only it is not already found
#   Parameters:
#     TYPE: to tell if the variable is a list or a string. Possible values : LIST, VAR
#     VARL: the variable to be updated
#     ELEMENT: the string to add to VAR
#
#   Examples :
#     set(MYVAR "toto tata")
#     set(MYLIST "toto")
#     list(APPEND MYLIST "tata")
#     addIfEnsuredOnce(VAR MYVAR "tata")
#     addIfEnsuredOnce(LIST MYLIST "tutu")
#
function (addIfEnsuredOnce TYPE VARL ELEMENT)
  if (${TYPE} STREQUAL "VAR")
    string(FIND "${${VARL}}" "${ELEMENT}" OUT)
    if (OUT STREQUAL "-1")
      set(${VARL} "${${VARL}} ${ELEMENT}" PARENT_SCOPE)
    endif()
  elseif(${TYPE} STREQUAL "LIST")
    list(FIND ${VARL} "${ELEMENT}" OUT)
    if (OUT STREQUAL "-1")
      list(APPEND ${VARL} ${ELEMENT})
      set (${VARL} "${${VARL}}"  PARENT_SCOPE)
    endif()
  else()
    message(FATAL_ERROR "First argument must be VAR or LIST")
  endif()
endfunction()

# function cleanpathWithBlankSpaces
#   When paths contains blank spaces preceded by a backslash, backslash is removed
#   Parameters:
#     OUTPUT: the variable to be set
#     PATH: the path to be cleaned
#
function (cleanPathWithBlankSpaces OUTPUT PATH)
  string(REPLACE "\\ " " " OUT "${PATH}")
  set(${OUTPUT} "${OUT}" PARENT_SCOPE)
endfunction()
