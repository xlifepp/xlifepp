# FindCAMD is aimed to be a submodule of FindCHOLMOD
# It does not work independently !
#
# module inputs
#     CAMD_INCLUDE_DIR = specific directory where to find camd.h
#     CAMD_LIB_DIR = specific directory where to find camd library
# module outputs
#     CAMD_INCLUDE = camd.h with full path
#     CAMD_LIBRARY = camd library with full path

if (CAMD_INCLUDE AND CAMD_LIBRARY)
  set(CAMD_FIND_QUIETLY TRUE)
endif(CAMD_INCLUDE AND CAMD_LIBRARY)

# Check for header file
find_path(CAMD_INCLUDE NAMES camd.h PATHS ${CAMD_INCLUDE_DIR} $ENV{CAMD_INCLUDE_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES suitesparse ufsparse include Include NO_DEFAULT_PATH)
find_path(CAMD_INCLUDE NAMES camd.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES suitesparse ufsparse include Include)
if (CAMD_INCLUDE)
  get_filename_component(CAMD_INCLUDE ${CAMD_INCLUDE} REALPATH)
  message(STATUS "Found camd.h: ${CAMD_INCLUDE}")
endif()
mark_as_advanced(CAMD_INCLUDE)

# Check for CAMD library
find_library(CAMD_LIBRARY camd PATHS ${CAMD_LIB_DIR} $ENV{CAMD_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(CAMD_LIBRARY camd PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
if (CAMD_LIBRARY)
  get_filename_component(CAMD_LIBRARY ${CAMD_LIBRARY} REALPATH)
  message(STATUS "Found CAMD: ${CAMD_LIBRARY}")
endif()
mark_as_advanced(CAMD_LIBRARY)

# Standard package handling
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CAMD DEFAULT_MSG CAMD_LIBRARY CAMD_INCLUDE)
