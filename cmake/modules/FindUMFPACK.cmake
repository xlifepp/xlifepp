# FindUMFPACK main module to find UMFPACK libraries and dependencies

# Umfpack lib usually requires linking to a blas library.
# It is up to the user of this module to find a BLAS and link to it.
#
# module inputs
#     UMFPACK_INCLUDE_DIR = specific directory where to find umfpack.h
#     UMFPACK_LIB_DIR = specific directory where to find umfpack library
# module outputs
#     UMFPACK_INCLUDES = umfpack.h and other includes with full paths
#     UMFPACK_LIBRARIES = umfpack library and other libraries with full paths

# Check for header file
find_path(UMFPACK_INCLUDE umfpack.h PATHS $ENV{UMFPACK_INCLUDE_DIR} ${UMFPACK_INCLUDE_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES suitesparse ufsparse include Include NO_DEFAULT_PATH)
find_path(UMFPACK_INCLUDE umfpack.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES suitesparse ufsparse include Include)
if (UMFPACK_INCLUDE)
  get_filename_component(UMFPACK_INCLUDE ${UMFPACK_INCLUDE} REALPATH)
  message(STATUS "Found umfpack.h: ${UMFPACK_INCLUDE}")
endif()
mark_as_advanced(UMFPACK_INCLUDE)

# Check for UMFPACK library
find_library(UMFPACK_LIBRARY umfpack PATHS $ENV{UMFPACK_LIB_DIR} ${UMFPACK_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(UMFPACK_LIBRARY umfpack PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
if (UMFPACK_LIBRARY)
  get_filename_component(UMFPACK_LIBRARY ${UMFPACK_LIBRARY} REALPATH)
  message(STATUS "Found UMFPACK: ${UMFPACK_LIBRARY}")
endif()
mark_as_advanced(UMFPACK_LIBRARY)

# Find SuiteSparse dependencies
find_package(AMD QUIET)
find_package(CHOLMOD QUIET)

# Collect includes
set(UMFPACK_INCLUDES)
if (CHOLMOD_INCLUDES)
  set(UMFPACK_INCLUDES ${UMFPACK_INCLUDES} ${CHOLMOD_INCLUDES})
endif()
if (UMFPACK_INCLUDE)
  set(umfpackFlagIndex "-1")
  if(UMFPACK_INCLUDES)
    list(FIND UMFPACK_INCLUDES ${UMFPACK_INCLUDE} umfpackFlagIndex)
  endif()
  if (umfpackFlagIndex STREQUAL "-1")
    list(APPEND UMFPACK_INCLUDES ${UMFPACK_INCLUDE})
  endif()
endif()
if (AMD_INCLUDE)
  set(amdFlagIndex "-1")
  if(UMFPACK_INCLUDES)
    list(FIND UMFPACK_INCLUDES ${AMD_INCLUDE} amdFlagIndex)
  endif()
  if(amdFlagIndex STREQUAL "-1")
    list(APPEND UMFPACK_INCLUDES ${AMD_INCLUDE})
  endif()
endif()

# Check for SUITESPARSE library on Apple
if (APPLE)
  find_library(SUITESPARSE_LIBRARY SuiteSparse PATHS ${SUITESPARSE_LIB_DIR} $ENV{SUITESPARSE_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
  find_library(SUITESPARSE_LIBRARY SuiteSparse PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
  mark_as_advanced(SUITESPARSE_LIBRARY)
endif()

# Check for SuiteSparse_config.h
find_path(SUITESPARSECONFIG_INCLUDE NAMES SuiteSparse_config.h PATHS ${SUITESPARSECONFIG_INCLUDE_DIR} $ENV{SUITESPARSECONFIG_INCLUDE_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES suitesparse ufsparse include Include NO_DEFAULT_PATH)
find_path(SUITESPARSECONFIG_INCLUDE NAMES SuiteSparse_config.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES suitesparse ufsparse include Include)
mark_as_advanced(SUITESPARSECONFIG_INCLUDE)

# Check for SUITESPARSECONFIG library
find_library(SUITESPARSECONFIG_LIBRARY suitesparseconfig PATHS ${SUITESPARSECONFIG_LIB_DIR} $ENV{SUITESPARSECONFIG_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(SUITESPARSECONFIG_LIBRARY suitesparseconfig PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
mark_as_advanced(SUITESPARSECONFIG_LIBRARY)

# Collect libraries
if (UMFPACK_LIBRARY)
  set(UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${UMFPACK_LIBRARY})
endif()
if (AMD_FOUND)
  set(UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${AMD_LIBRARY})
endif()
if (CHOLMOD_FOUND)
  set(UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${CHOLMOD_LIBRARIES})
endif()

if (APPLE)
  if (SUITESPARSE_LIBRARY)
    get_filename_component(SUITESPARSE_LIBRARY ${SUITESPARSE_LIBRARY} REALPATH)
    set(UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${SUITESPARSE_LIBRARY})
  endif()
endif()

if (SUITESPARSECONFIG_LIBRARY)
  get_filename_component(SUITESPARSECONFIG_LIBRARY ${SUITESPARSECONFIG_LIBRARY} REALPATH)
  set(UMFPACK_LIBRARIES ${UMFPACK_LIBRARIES} ${SUITESPARSECONFIG_LIBRARY})
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(UMFPACK DEFAULT_MSG UMFPACK_INCLUDES UMFPACK_LIBRARIES)

mark_as_advanced(UMFPACK_INCLUDES UMFPACK_LIBRARIES)
