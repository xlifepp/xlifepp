# FindCOLAMD is aimed to be a submodule of FindCHOLMOD
# It does not work independently !
#
# module inputs
#     COLAMD_INCLUDE_DIR = specific directory where to find colamd.h
#     COLAMD_LIB_DIR = specific directory where to find colamd library
# module outputs
#     COLAMD_INCLUDE = colamd.h with full path
#     COLAMD_LIBRARY = colamd library with full path

if (COLAMD_INCLUDE AND COLAMD_LIBRARY)
  set(COLAMD_FIND_QUIETLY TRUE)
endif(COLAMD_INCLUDE AND COLAMD_LIBRARY)

# Check for header file
find_path(COLAMD_INCLUDE NAMES colamd.h PATHS ${COLAMD_INCLUDE_DIR} $ENV{COLAMD_INCLUDE_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES suitesparse ufsparse include Include NO_DEFAULT_PATH)
find_path(COLAMD_INCLUDE NAMES colamd.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES suitesparse ufsparse include Include)
if (COLAMD_INCLUDE)
  get_filename_component(COLAMD_INCLUDE ${COLAMD_INCLUDE} REALPATH)
  message(STATUS "Found colamd.h: ${COLAMD_INCLUDE}")
endif()
mark_as_advanced(COLAMD_INCLUDE)

# Check for COLAMD library
find_library(COLAMD_LIBRARY colamd PATHS ${COLAMD_LIB_DIR} $ENV{COLAMD_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(COLAMD_LIBRARY colamd PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
if (COLAMD_LIBRARY)
  get_filename_component(COLAMD_LIBRARY ${COLAMD_LIBRARY} REALPATH)
  message(STATUS "Found COLAMD: ${COLAMD_LIBRARY}")
endif()
mark_as_advanced(COLAMD_LIBRARY)

# Standard package handling
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(COLAMD DEFAULT_MSG COLAMD_LIBRARY COLAMD_INCLUDE)
