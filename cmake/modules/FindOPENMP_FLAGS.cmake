# - Finds OPENMP support
# This module can be used to detect OPENMP support in a compiler.
# If the compiler supports OPENMP, the flags required to compile with
# openmp support are set.  
#
# The following variables are set:
#   OPENMP_C_FLAGS - flags to add to the C compiler for OPENMP support (skipped)
#   OPENMP_CXX_FLAGS - flags to add to the CXX compiler for OPENMP support
#   OPENMP_FOUND - true if OPENMP is detected
#
# Supported compilers can be found at http://openmp.org/wp/openmp-compilers/


# Copyright 2008, 2009 <André Rigland Brodtkorb> Andre.Brodtkorb@ifi.uio.no
#
# Redistribution AND use is allowed according to the terms of the New 
# BSD license. 
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.


#include(CheckCSourceCompiles)
include(CheckCXXSourceCompiles)
include(FindPackageHandleStandardArgs)

set(OPENMP_FLAG_CANDIDATES
  #Gnu
  "-fopenmp"
  #Microsoft Visual Studio
  "/openmp"
  #Intel windows
  "-Qopenmp" 
  #Intel
  "-openmp" 
  #Empty, if compiler automatically accepts openmp
  " "
  #Sun
  "-xopenmp"
  #HP
  "+Oopenmp"
  #IBM XL C/c++
  "-qsmp"
  #Portland Group
  "-mp"
)

# sample openmp source code to test
set(OPENMP_TEST_SOURCE 
"
#include <omp.h>
int main() { 
#ifdef _OPENMP
  return 0; 
#else
  breaks_on_purpose
#endif
}
")

# if these are set then do not try to find them again,
# by avoiding any try_compiles for the flags
#if (DEFINED OPENMP_C_FLAGS AND DEFINED OPENMP_CXX_FLAGS)
#  set(OPENMP_FLAG_CANDIDATES)
#endif()
#if (DEFINED OPENMP_CXX_FLAGS)
#  set(OPENMP_FLAG_CANDIDATES)
#endif()

# check c compiler
#foreach (FLAG ${OPENMP_C_FLAG_CANDIDATES})
#  set(SAFE_CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS}")
#  set(CMAKE_REQUIRED_FLAGS "${FLAG}")
#  set(OPENMP_C_FLAG_DETECTED)
#  message(STATUS "Try OPENMP C flag = [${FLAG}]")
#  unset(OPENMP_C_FLAG_DETECTED CACHE)
#  check_c_source_compiles("${OPENMP_TEST_SOURCE}" OPENMP_C_FLAG_DETECTED)
#  set(CMAKE_REQUIRED_FLAGS "${SAFE_CMAKE_REQUIRED_FLAGS}")
#  if (OPENMP_C_FLAG_DETECTED)
#    set(OPENMP_C_FLAGS_INTERNAL "${FLAG}")
#    break()
#  endif(OPENMP_C_FLAG_DETECTED) 
#endforeach(FLAG ${OPENMP_C_FLAG_CANDIDATES})

# check cxx compiler
foreach (FLAG ${OPENMP_FLAG_CANDIDATES})
  set(SAFE_CMAKE_REQUIRED_FLAGS "${CMAKE_REQUIRED_FLAGS}")
  set(CMAKE_REQUIRED_FLAGS "${FLAG}")
  set(OPENMP_CXX_FLAG_DETECTED)
  message(STATUS "Try OPENMP CXX flag = [${FLAG}]")
  unset(OPENMP_CXX_FLAG_DETECTED CACHE)
  check_cxx_source_compiles("${OPENMP_TEST_SOURCE}" OPENMP_CXX_FLAG_DETECTED)
  set(CMAKE_REQUIRED_FLAGS "${SAFE_CMAKE_REQUIRED_FLAGS}")
  if (OPENMP_CXX_FLAG_DETECTED)
    set(OPENMP_CXX_FLAGS_INTERNAL "${FLAG}")
    break()
  endif(OPENMP_CXX_FLAG_DETECTED)
endforeach()

#set(OPENMP_C_FLAGS "${OPENMP_C_FLAGS_INTERNAL}" CACHE STRING "C compiler flags for OPENMP parallelization")
set(OPENMP_CXX_FLAGS "${OPENMP_CXX_FLAGS_INTERNAL}" CACHE STRING "C++ compiler flags for OPENMP parallelization")

# handle the standard arguments for find_package
#find_package_handle_standard_args(OPENMP DEFAULT_MSG OPENMP_C_FLAGS OPENMP_CXX_FLAGS )
find_package_handle_standard_args(OPENMP DEFAULT_MSG OPENMP_CXX_FLAGS )

#mark_as_advanced(OPENMP_C_FLAGS OPENMP_CXX_FLAGS)
mark_as_advanced(OPENMP_CXX_FLAGS)
