# FindAMD is aimed to be a submodule of FindUMFPACK
# It does not work independently !
#
# module inputs
#     METIS_INCLUDE_DIR = specific directory where to find amd.h
#     METIS_LIB_DIR = specific directory where to find amd library
# module outputs
#     METIS_INCLUDE = amd.h with full path
#     METIS_LIBRARY = amd library with full path

if (METIS_INCLUDE AND METIS_LIBRARY)
  set(METIS_FIND_QUIETLY TRUE)
endif(METIS_INCLUDE AND METIS_LIBRARY)

# Check for header file
find_path(METIS_INCLUDE NAMES metis.h PATHS ${METIS_INCLUDE_DIR} $ENV{METIS_INCLUDE_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES suitesparse ufsparse include Include NO_DEFAULT_PATH)
find_path(METIS_INCLUDE NAMES metis.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES suitesparse ufsparse include Include)
if (METIS_INCLUDE)
  get_filename_component(METIS_INCLUDE ${METIS_INCLUDE} REALPATH)
  message(STATUS "Found metis.h: ${METIS_INCLUDE}")
endif()
mark_as_advanced(METIS_INCLUDE)

# Check for METIS library
find_library(METIS_LIBRARY metis PATHS ${METIS_LIB_DIR} $ENV{METIS_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(METIS_LIBRARY metis PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
if (METIS_LIBRARY)
  get_filename_component(METIS_LIBRARY ${METIS_LIBRARY} REALPATH)
  message(STATUS "Found METIS: ${METIS_LIBRARY}")
endif()
mark_as_advanced(METIS_LIBRARY)

# Standard package handling
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(METIS DEFAULT_MSG METIS_LIBRARY METIS_INCLUDE)
