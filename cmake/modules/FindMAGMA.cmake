# FindMAGMA main module to find MAGMA libraries and dependencies
#
# module inputs
#     MAGMA_INCLUDE_DIR = specific directory where to find magma.h
#     MAGMA_LIB_DIR = specific directory where to find magma library
# module outputs
#     MAGMA_INCLUDE = magma.h with full path
#     MAGMA_LIBRARY = magma library with full path

if (MAGMA_INCLUDE AND MAGMA_LIBRARY)
  set(MAGMA_FIND_QUIETLY TRUE)
endif(MAGMA_INCLUDE AND MAGMA_LIBRARY)

# Check for header file
find_path(MAGMA_INCLUDE NAMES magma.h PATHS ${MAGMA_INCLUDE_DIR} $ENV{MAGMA_INCLUDE_DIR} PATH_SUFFIXES include Include NO_DEFAULT_PATH)
find_path(MAGMA_INCLUDE NAMES magma.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES include Include)
if (MAGMA_INCLUDE)
  get_filename_component(MAGMA_INCLUDE ${MAGMA_INCLUDE} REALPATH)
  message(STATUS "Found magma.h: ${MAGMA_INCLUDE}")
endif()
mark_as_advanced(MAGMA_INCLUDE)

# Check for MAGMA library
find_library(MAGMA_LIBRARY magma PATHS ${MAGMA_LIB_DIR} $ENV{MAGMA_LIB_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(MAGMA_LIBRARY magma PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
if (MAGMA_LIBRARY)
  get_filename_component(MAGMA_LIBRARY ${MAGMA_LIBRARY} REALPATH)
  message(STATUS "Found MAGMA: ${MAGMA_LIBRARY}")
endif()
mark_as_advanced(MAGMA_LIBRARY)

# Standard package handling
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MAGMA DEFAULT_MSG MAGMA_LIBRARY MAGMA_INCLUDE)
