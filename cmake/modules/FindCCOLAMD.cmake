# FindCCOLAMD is aimed to be a submodule of FindCHOLMOD
# It does not work independently !
#
# module inputs
#     CCOLAMD_INCLUDE_DIR = specific directory where to find ccolamd.h
#     CCOLAMD_LIB_DIR = specific directory where to find ccolamd library
# module outputs
#     CCOLAMD_INCLUDE = ccolamd.h with full path
#     CCOLAMD_LIBRARY = ccolamd library with full path

if (CCOLAMD_INCLUDE AND CCOLAMD_LIBRARY)
  set(CCOLAMD_FIND_QUIETLY TRUE)
endif(CCOLAMD_INCLUDE AND CCOLAMD_LIBRARY)

# Check for header file
find_path(CCOLAMD_INCLUDE NAMES ccolamd.h PATHS ${CCOLAMD_INCLUDE_DIR} $ENV{CCOLAMD_INCLUDE_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES suitesparse ufsparse include Include NO_DEFAULT_PATH)
find_path(CCOLAMD_INCLUDE NAMES ccolamd.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES suitesparse ufsparse include Include)
if (CCOLAMD_INCLUDE)
  get_filename_component(CCOLAMD_INCLUDE ${CCOLAMD_INCLUDE} REALPATH)
  message(STATUS "Found ccolamd.h: ${CCOLAMD_INCLUDE}")
endif()
mark_as_advanced(CCOLAMD_INCLUDE)

# Check for CCOLAMD library
find_library(CCOLAMD_LIBRARY ccolamd PATHS ${CCOLAMD_LIB_DIR} $ENV{CCOLAMD_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(CCOLAMD_LIBRARY ccolamd PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
if (CCOLAMD_LIBRARY)
  get_filename_component(CCOLAMD_LIBRARY ${CCOLAMD_LIBRARY} REALPATH)
  message(STATUS "Found CCOLAMD: ${CCOLAMD_LIBRARY}")
endif()
mark_as_advanced(CCOLAMD_LIBRARY)

# Standard package handling
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CCOLAMD DEFAULT_MSG CCOLAMD_LIBRARY CCOLAMD_INCLUDE)
