# FindAMD is aimed to be a submodule of FindUMFPACK
# It does not work independently !
#
# module inputs
#     AMD_INCLUDE_DIR = specific directory where to find amd.h
#     AMD_LIB_DIR = specific directory where to find amd library
# module outputs
#     AMD_INCLUDE = amd.h with full path
#     AMD_LIBRARY = amd library with full path

if (AMD_INCLUDE AND AMD_LIBRARY)
  set(AMD_FIND_QUIETLY TRUE)
endif(AMD_INCLUDE AND AMD_LIBRARY)

# Check for header file
find_path(AMD_INCLUDE NAMES amd.h PATHS ${AMD_INCLUDE_DIR} $ENV{AMD_INCLUDE_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES suitesparse ufsparse include Include NO_DEFAULT_PATH)
find_path(AMD_INCLUDE NAMES amd.h PATHS ${INCLUDE_INSTALL_DIR} PATH_SUFFIXES suitesparse ufsparse include Include)
if (AMD_INCLUDE)
  get_filename_component(AMD_INCLUDE ${AMD_INCLUDE} REALPATH)
  message(STATUS "Found amd.h: ${AMD_INCLUDE}")
endif()
mark_as_advanced(AMD_INCLUDE)

# Check for AMD library
find_library(AMD_LIBRARY amd PATHS ${AMD_LIB_DIR} $ENV{AMD_LIB_DIR} ${SUITESPARSE_DIR} $ENV{SUITESPARSE_DIR} PATH_SUFFIXES Lib lib NO_DEFAULT_PATH)
find_library(AMD_LIBRARY amd PATHS ${LIB_INSTALL_DIR} PATH_SUFFIXES Lib lib)
if (AMD_LIBRARY)
  get_filename_component(AMD_LIBRARY ${AMD_LIBRARY} REALPATH)
  message(STATUS "Found AMD: ${AMD_LIBRARY}")
endif()
mark_as_advanced(AMD_LIBRARY)

# Standard package handling
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(AMD DEFAULT_MSG AMD_LIBRARY AMD_INCLUDE)
