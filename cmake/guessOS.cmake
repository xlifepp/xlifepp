function(guessUnixOS RES_UNIX_OS)
  set(UNIX_OS "${CMAKE_SYSTEM_NAME}") # default value for non checked Linux distributions
  
  if (${UNIX_OS} STREQUAL "Linux")
    # first, we try to find /etc/os-release that is the standard file for quite almost every recent Linux distribution
    find_file(OSRELEASE_FILE os-release PATHS /etc)
    if (OSRELEASE_FILE)
      file(STRINGS ${OSRELEASE_FILE} DEBIAN_FOUND REGEX Debian)
      file(STRINGS ${OSRELEASE_FILE} UBUNTU_FOUND REGEX Ubuntu)
      file(STRINGS ${OSRELEASE_FILE} MINT_FOUND REGEX Mint)
      file(STRINGS ${OSRELEASE_FILE} FEDORA_FOUND REGEX Fedora)
      if (UBUNTU_FOUND)
        set(UNIX_OS "Ubuntu")
      elseif (MINT_FOUND)
        set(UNIX_OS "Mint")
      elseif (DEBIAN_FOUND)
        set(UNIX_OS "Debian")
      elseif (FEDORA_FOUND)
        set(UNIX_OS "Fedora")
      endif()
    else()
      # Linux distribution is old or "rare"
      # test of older Debian distributions
      # If debian_version exists, DEBIAN_FILE will contain its full path
      # Else if debconf.conf exists, DEBIAN_FILE will contain its full path
      # Else DEBIAN_FILE will contain DEBIAN_FILE_NOTFOUND
      find_file(DEBIAN_FILE NAMES debian_version debconf.conf PATHS /etc)
      if (DEBIAN_FILE)
        # we have to check which kind of Debian distribution it is: Debian, Ubuntu, Mint, ...
        # If legal exists, LEGISS_FILE will contain its full path
        # Else if issue exists, LEGISS_FILE will contain its full path
        # Else LEGISS_FILE will contain LEGISS_FILE_NOTFOUND
        find_file(LEGISS_FILE legal issue PATHS /etc)
        if (LEGISS_FILE)
          file(STRINGS ${LEGISS_FILE} UBUNTU_FOUND REGEX Ubuntu)
          file(STRINGS ${LEGISS_FILE} MINT_FOUND REGEX Mint)
        endif()
        if (UBUNTU_FOUND)
          set(UNIX_OS "Ubuntu")
        elseif (MINT_FOUND)
          set(UNIX_OS "Mint")
        else()
          set(UNIX_OS "Debian")
        endif()
      else()
        # This is not an older Debian distribution
        # We check if it is an old Fedora
        find_file(FEDORA_FILE fedora-release PATHS /etc)
        if (FEDORA_FILE)
          set(Unix_OS "Fedora")
        else()
          # This is not an older Fedora distribution
          # We check if it is a FreeBSD
          find_file(FREEBSD_FILE freebsd-update.conf PATHS /etc)
          if (FREEBSD_FILE)
            set (Unix_OS "FreeBSD")
          endif()
        endif()
      endif()
    endif()
  endif()
  set(${RES_UNIX_OS} ${UNIX_OS} PARENT_SCOPE)
endfunction(guessUnixOS)
