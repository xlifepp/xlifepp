set(CMAKE_MACOSX_RPATH 1)

set(XLIFEPP_DEPS ${UMFPACK_LIBRARIES} ${LAPACK_LIBRARIES} ${BLAS_LIBRARIES})
configure_file("${CMAKE_SOURCE_DIR}/etc/templates/CMakeLists.cpack.txt" "${CMAKE_BINARY_DIR}/CMakeLists-binary.txt" @ONLY)
install(FILES "${CMAKE_BINARY_DIR}/CMakeLists-binary.txt"
        DESTINATION .
        RENAME CMakeLists.txt
        COMPONENT configuration)
install(FILES "${CMAKE_SOURCE_DIR}/VERSION.txt"
        DESTINATION .
        COMPONENT configuration)
install(DIRECTORY "${CMAKE_SOURCE_DIR}/cmake/"
        DESTINATION cmake
        COMPONENT configuration
        PATTERN lock.cmake EXCLUDE
        PATTERN guessOS.cmake EXCLUDE
        PATTERN xlifepp_cpack_management.cmake EXCLUDE)

set(XLIFEPP_CPACK_BLAS_LIB_DEFAULT "\" \"")
set(XLIFEPP_CPACK_LAPACK_LIB_DEFAULT "\" \"")
set(XLIFEPP_CPACK_ARPACK_LIB_DEFAULT "\" \"")
if (NOT XLIFEPP_SYSTEM_ARPACK)
  set(XLIFEPP_CPACK_ARPACK_LIB_DEFAULT "\"\${CMAKE_SOURCE_DIR}/lib\"")
endif()
set(XLIFEPP_CPACK_AMOS_DIR_DEFAULT "\"\${CMAKE_SOURCE_DIR}/lib\"")
set(XLIFEPP_CPACK_SUITESPARSE_HOME_DIR_DEFAULT "\" \"")
if (WIN32)
  if (NOT ${XLIFEPP_BLAS_LIB} STREQUAL " ")
    get_filename_component(XLIFEPP_CPACK_BLAS_LIB_DEFAULT ${XLIFEPP_BLAS_LIB} NAME)
    set(XLIFEPP_CPACK_BLAS_LIB_DEFAULT "\"\${CMAKE_SOURCE_DIR}/lib/${XLIFEPP_CPACK_BLAS_LIB_DEFAULT}\"")
  endif()
  if (NOT ${XLIFEPP_LAPACK_LIB} STREQUAL " ")
    get_filename_component(XLIFEPP_CPACK_LAPACK_LIB_DEFAULT ${XLIFEPP_LAPACK_LIB} NAME)
    set(XLIFEPP_CPACK_LAPACK_LIB_DEFAULT "\"\${CMAKE_SOURCE_DIR}/lib/${XLIFEPP_CPACK_LAPACK_LIB_DEFAULT}\"")
  endif()
  if (NOT ${XLIFEPP_ARPACK_LIB} STREQUAL " ")
    get_filename_component(XLIFEPP_CPACK_ARPACK_LIB_DEFAULT ${XLIFEPP_ARPACK_LIB} NAME)
    set(XLIFEPP_CPACK_ARPACK_LIB_DEFAULT "\"\${CMAKE_SOURCE_DIR}/lib/${XLIFEPP_CPACK_ARPACK_LIB_DEFAULT}\"")
  endif()
  if (NOT ${XLIFEPP_SUITESPARSE_HOME_DIR} STREQUAL " ")
    get_filename_component(XLIFEPP_CPACK_SUITESPARSE_HOME_DIR_DEFAULT ${XLIFEPP_SUITESPARSE_HOME_DIR} NAME)
    set(XLIFEPP_CPACK_SUITESPARSE_HOME_DIR_DEFAULT "\"\${CMAKE_SOURCE_DIR}/ext/${XLIFEPP_CPACK_SUITESPARSE_HOME_DIR_DEFAULT}\"")
  endif()
endif()

configure_file(${CMAKE_SOURCE_DIR}/etc/templates/getBinaryConfig.cmake.cmake ${CMAKE_BINARY_DIR}/getBinaryConfig.cmake @ONLY)

install(FILES ${CMAKE_BINARY_DIR}/getBinaryConfig.cmake
        DESTINATION cmake
        COMPONENT configuration)

file(GLOB headers "${CMAKE_SOURCE_DIR}/include/*.h")
install(FILES ${headers}
        DESTINATION include
        COMPONENT headers)

foreach (_lib ${XLIFEPP_LIBS})
  install(TARGETS ${_lib}
          DESTINATION lib
          COMPONENT libraries)
endforeach()

# management of amos
install(FILES ${XLIFEPP_AMOS_LIB}
        DESTINATION lib
        COMPONENT external_libraries)

# management of arpack when compiled
if (AUTOBUILDARPACK)
  install(FILES ${XLIFEPP_ARPACK_LIB}
          DESTINATION lib
          COMPONENT external_libraries)
endif()

file(GLOB_RECURSE hpp RELATIVE "${CMAKE_SOURCE_DIR}" src/*.hpp)

foreach (_hpp ${hpp})
  set(_path "")
  set(_dir ${_hpp})
  while (NOT ${_dir} STREQUAL src)
    get_filename_component(_parentdir ${_dir} PATH)
    get_filename_component(_hppdir ${_parentdir} NAME_WE)
    if (NOT ${_hppdir} STREQUAL src)
      set(_path ${_hppdir}/${_path})
    endif()
    set(_dir ${_parentdir} )
  endwhile()

  install(FILES ${_hpp}
          DESTINATION include/${_path}
          COMPONENT headers)
endforeach()

install(DIRECTORY "${CMAKE_SOURCE_DIR}/etc/templates/"
        DESTINATION etc/templates
        USE_SOURCE_PERMISSIONS
        COMPONENT configuration
        PATTERN VERSION.cmake.txt EXCLUDE
        PATTERN CMakeLists.cpack.txt EXCLUDE
        PATTERN main_tests.cmake.cpp EXCLUDE
        PATTERN testList.cmake.hpp EXCLUDE
        PATTERN xlifepp_project_test.cmake.sh EXCLUDE
        PATTERN xlifepp_test_runner.cmake.rb EXCLUDE
        PATTERN getBinaryConfig.cmake.cmake EXCLUDE)

install(DIRECTORY "${CMAKE_SOURCE_DIR}/ext/Eigen/"
        DESTINATION ext/Eigen
        USE_SOURCE_PERMISSIONS
        COMPONENT external_libraries)
install(DIRECTORY "${CMAKE_SOURCE_DIR}/ext/ARPACK++/"
        DESTINATION ext/ARPACK++
        USE_SOURCE_PERMISSIONS
        COMPONENT external_libraries)

install(FILES "${CMAKE_SOURCE_DIR}/etc/gmsh/xlifepp_macros.geo"
        DESTINATION etc/gmsh
        COMPONENT others)

install(DIRECTORY "${CMAKE_SOURCE_DIR}/etc/messages/"
        DESTINATION etc/messages
        USE_SOURCE_PERMISSIONS
        COMPONENT others)

install(FILES "${CMAKE_SOURCE_DIR}/doc/tex/user_documentation.pdf"
              "${CMAKE_SOURCE_DIR}/doc/tex/dev_documentation.pdf"
              "${CMAKE_SOURCE_DIR}/doc/tex/examples.pdf"
              "${CMAKE_SOURCE_DIR}/doc/tex/tutorial.pdf"
        DESTINATION share/doc
        COMPONENT documentation)

install(DIRECTORY "${CMAKE_SOURCE_DIR}/examples/"
        DESTINATION share/examples
        USE_SOURCE_PERMISSIONS
        COMPONENT others)
install(FILES "${CMAKE_SOURCE_DIR}/usr/main.cpp"
        DESTINATION share/examples
        COMPONENT others)

install(FILES "${CMAKE_SOURCE_DIR}/etc/visuTermVec.m"
        DESTINATION etc
        COMPONENT others)

if (WIN32)
  if (EXISTS ${XLIFEPP_BLAS_LIB})
    install(FILES ${XLIFEPP_BLAS_LIB} DESTINATION lib COMPONENT external_libraries)
  endif()
  if (EXISTS ${XLIFEPP_LAPACK_LIB})
    install(FILES ${XLIFEPP_LAPACK_LIB} DESTINATION lib COMPONENT external_libraries)
  endif()
  if (EXISTS ${XLIFEPP_ARPACK_LIB})
    install(FILES ${XLIFEPP_ARPACK_LIB} DESTINATION lib COMPONENT external_libraries)
  endif()
  get_filename_component(XLIFEPP_SUITESPARSE_HOME_DIR_DEFAULT ${XLIFEPP_SUITESPARSE_HOME_DIR} NAME)
  install(DIRECTORY ${XLIFEPP_SUITESPARSE_HOME_DIR}/
          DESTINATION ext/${XLIFEPP_SUITESPARSE_HOME_DIR_DEFAULT}
          USE_SOURCE_PERMISSIONS
          COMPONENT external_libraries)
  install(FILES "${CMAKE_SOURCE_DIR}/bin/xlifepp_new_project.exe" DESTINATION bin COMPONENT others)
  install(FILES "${CMAKE_SOURCE_DIR}/bin/xlifepp_configure.exe" DESTINATION bin COMPONENT others)
endif()

file(READ "${CMAKE_SOURCE_DIR}/VERSION.txt" versionContent)
string(REGEX MATCH "VERSION: ([a-zA-Z0-9.?]+)" match ${versionContent} )
set(XLIFEPP_VERSION ${CMAKE_MATCH_1})
string(REPLACE "v" "" CPACK_PACKAGE_VERSION ${XLIFEPP_VERSION})

include(InstallRequiredSystemLibraries)

set(CPACK_BINARY_DEB                   "ON")
set(CPACK_PACKAGE_VENDOR               "POems")
set(CPACK_PACKAGE_NAME                 "XLiFE++")
set(CPACK_PACKAGE_INSTALL_DIRECTORY    "xlifepp") # used bu NSIS generator
set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "${CPACK_PACKAGE_INSTALL_DIRECTORY}-${CPACK_PACKAGE_VERSION}-${LIB_TYPE}")

set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_SOURCE_DIR}/VERSION.txt")
set(CPACK_RESOURCE_FILE_LICENSE    "${CMAKE_SOURCE_DIR}/LICENSE.txt")
set(CPACK_RESOURCE_FILE_README     "${CMAKE_SOURCE_DIR}/README.md")
set(CPACK_PACKAGE_RELOCATABLE TRUE)

set(CPACK_PACKAGING_INSTALL_PREFIX "/usr/local/xlifepp") # default value for Linux systems (Debian, Fedora and co)
set(CPACK_COMPONENTS_ALL_IN_ONE_PACKAGE ON)
set(CPACK_COMPONENTS_GROUPING ALL_COMPONENTS_IN_ONE)
set(CPACK_COMPONENTS_ALL libraries headers others documentation external_libraries configuration)

set(CPACK_PACKAGE_MAINTAINER "pierre.navaro@univ-rennes1.fr")
set(CPACK_PACKAGE_CONTACT    "xlifepp-dev@listes.math.cnrs.fr")

include(${CMAKE_SOURCE_DIR}/cmake/guessOS.cmake)

set(CPACK_PACKAGE_ARCHITECTURE ${XLIFEPP_SYSTEM_NAME})
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_INSTALL_DIRECTORY}-binaries-v${CPACK_PACKAGE_VERSION}-${VERSION_DATE}-${CPACK_PACKAGE_ARCHITECTURE}-${XLIFEPP_CXX_COMPILER}-${BITS_TYPE}bits")

if (NOT WIN32)
  set(XLIFEPP_USR_CONFIGURE_CMAKE_COMMAND "cmake .. $@")
  configure_file("${CMAKE_SOURCE_DIR}/etc/templates/post-install.cpack.sh" "${CMAKE_SOURCE_DIR}/bin/xlifepp_configure.sh" @ONLY)
  install(FILES "${CMAKE_SOURCE_DIR}/bin/xlifepp_configure.sh" DESTINATION bin COMPONENT configuration)
endif()

if (APPLE)
  execute_process(COMMAND sw_vers OUTPUT_VARIABLE OSX_VERSION)
  string(REGEX MATCH "1[0-9]\\.1[0-9]\\.[0-9]" OSX_VERSION ${OSX_VERSION})
  set(CPACK_GENERATOR         "DragNDrop")
  set(CPACK_PACKAGING_INSTALL_PREFIX "/xlifepp") # to build a directory inside the dmg
elseif (WIN32)
  set(CPACK_PACKAGING_INSTALL_PREFIX ${CPACK_INSTALL_PREFIX})
  set(CPACK_GENERATOR         "NSIS")
  set(CPACK_NSIS_PACKAGE_NAME "XLiFE++")
  set(CPACK_NSIS_MUI_ICON     "${CMAKE_SOURCE_DIR}/doc/tex/inputs/pictures/xlifepp.ico")
  set(CPACK_NSIS_MUI_UNIICON  "${CMAKE_SOURCE_DIR}/doc/tex/inputs/pictures/xlifepp.ico")
  set(CPACK_PACKAGE_ICON      "${CMAKE_SOURCE_DIR}/doc/tex/inputs/pictures\\xlife_300_wide.bmp")
  set(CPACK_NSIS_MENU_LINKS   "/bin/xlifepp_new_project.exe" "New XLiFE++ project" "/bin/xlifepp_configure.exe" "Configure XLiFE++")
  set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL "ON")
else()
  # we have to detect if the linux OS is ubuntu, debian, fedora, or anything else 
  guessUnixOS(CPACK_HOSTNAME)

  if (CPACK_HOSTNAME MATCHES "Debian" OR CPACK_HOSTNAME MATCHES "Ubuntu")
    set(CPACK_GENERATOR "DEB")
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "gcc, cmake, gmsh, libsuitesparse-dev, libblas-dev, liblapack-dev, libarpack2-dev, paraview")
    set(XLIFEPP_USR_CONFIGURE_CMAKE_COMMAND "cmake ..")
    configure_file("${CMAKE_SOURCE_DIR}/etc/templates/post-install.cpack.sh" "${CMAKE_SOURCE_DIR}/postinst" @ONLY)
    set(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_SOURCE_DIR}/postinst")
  elseif (CPACK_HOSTNAME MATCHES "Fedora")
    set(CPACK_GENERATOR "RPM")
    set(CPACK_RPM_PACKAGE_REQUIRES "gcc-g++, cmake, gmsh, paraview, suitesparse-devel, arpack-devel")
    set(CPACK_RPM_POST_INSTALL_SCRIPT_FILE "${CMAKE_SOURCE_DIR}/bin/xlifepp_configure.sh")
  elseif (CPACK_HOSTNAME MATCHES "FreeBSD")
    set(CPACK_GENERATOR "FreeBSD")
    set(CPACK_FREEBSD_PACKAGE_DEPS "gcc, cmake, gmsh, paraview, suitesparse, arpack-ng")
  else()
    set(CPACK_GENERATOR "TGZ")
  endif()


endif()
    
set(CPACK_PACKAGE_URL "https://uma.ensta-paris.fr/soft/XLiFE++")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "eXtended Library of Finite Elements in C++")
set(CPACK_PACKAGE_DESCRIPTION " Finite elements library to solve 1D / 2D / 3D, scalar / vector, transient / stationnary / harmonic problems.")

include(CPack)
