# XLiFE++ Installation

XLiFE++' installation process uses [CMake](http://www.cmake.org). The following will describe how to install and compile XLiFE++ with its default configuration. To activate some third-party libraries (or all of them) or to know how to choose your compiler and its potential options (for clang++ on macOS, you will have to set explicitly the path to gfortran library), please refer to the [installation documentation](https://xlifepp.pages.math.cnrs.fr/installation.html).

## Standard installation (Terminal mode)

- Execute the following commands from the directory containing this file:

      mkdir build
      cd build
      cmake ..
      make

- Execute tests to check if your installation is correct:

      make tests
      make test


## IDE installation

A better way to develop XLiFE++ is to use an IDE. CMake can generate multiple native build 
systems on certain platforms (Code::Blocks, Xcode, Visual Studio, Eclipse, ...).
The whole list of generators can be read on [CMake documentation](http://www.cmake.org). To know the list
of available generators on your computer, you can also go to the CMake help (run the
command `cmake --help` on UNIX systems).

- Execute the following command:

      cmake .. -G <generator-name>

  For example :

      cmake .. -G "CodeBlocks - Unix Makefiles"
      cmake .. -G "Eclipse CDT4 - Unix Makefiles"
      cmake .. -G "Xcode"

- Launch your favorite IDE on the generated files to build libraries and compile tests

Remarks:
1. Generators specifying "Unix Makefiles" are compatible with a command line usage
   (see above "Standard installation (Terminal mode)").
   Generators for Xcode are not : they must be created in a separate build directory
   if both usages are wanted. For example :

       cp -r xlifepp/build xlifepp/buildXcode
       cd xlifepp/buildXcode
       cmake .. -G "Xcode"

   (tested under Mac OS X version 10.6.8 and [CMake](http://www.cmake.org) version 2.8.8)

2. If generators for Eclipse are used (command `cmake .. -G "Eclipse CDT4 - Unix Makefiles"`)
   the following message is displayed:

       CMake Warning in CMakeLists.txt:
       The build directory is a subdirectory of the source directory.
       
       This is not supported well by Eclipse.  It is strongly recommended using a
       build directory which is a sibling of the source directory.

   This message is to be ignored since it is wrong: build and src directories are at the
   same level, and build files are created under xlifepp/build/CMakeFiles.
   (tested under Mac OS X version 10.6.8 and [CMake](http://www.cmake.org) version 2.8.8)
