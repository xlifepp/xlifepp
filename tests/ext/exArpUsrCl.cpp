/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file exArpUsrCl.cpp
  \author Y. Lafranche
  \since 15 jan 2016
  \date 15 jan 2016

  \brief Implementation of xlifepp::StdNonSym and xlifepp::StdComp classes
 */

#include "exArpUsrCl.hpp"

namespace xlifepp {

/*!
  Assumptions (not checked) :
    A real
    B real symmetric positive definite
*/
StdNonSym::StdNonSym(TermMatrix& A, TermMatrix& B)
: ARStdFrame(A), matA_p(&A.matrixData()->getLargeMatrix<Real>()),
                 matB_p(&B.matrixData()->getLargeMatrix<Real>()) {
  Ax.resize(GetN());

  fact_p = newSkyline(matB_p);
  ldltFactorize(*fact_p);
}
//!    Matrix-vector product y <- inv(B)*A * x
void StdNonSym::MultOPx (Real *x, Real *y) {
   array2Vector(x, lx);
   multMatrixVector(*matA_p, lx, Ax);
// Solve linear system. Matlab equivalent: ly = matB_p \ Ax;
   (fact_p->ldltSolve)(Ax, ly); // store the solution into ly
   vector2Array(ly, y);
}

/*!
  Assumptions (not checked) :
    A complex
    B real symmetric positive definite
*/
StdComp::StdComp(TermMatrix& A, TermMatrix& B)
: ARStdFrame(A), matA_p(&A.matrixData()->getLargeMatrix<complex_t>()),
                 matB_p(&B.matrixData()->getLargeMatrix<Real>()) {
  Ax.resize(GetN());

  fact_p = newSkyline(matB_p);
  ldltFactorize(*fact_p);
}
//!    Matrix-vector product y <- inv(B)*A * x
void StdComp::MultOPx (complex_t *x, complex_t *y) {
   array2Vector(x, lx);
   multMatrixVector(*matA_p, lx, Ax);
// Solve linear system. Matlab equivalent: ly = matB_p \ Ax;
   (fact_p->ldltSolve)(Ax, ly); // store the solution into ly
   vector2Array(ly, y);
}
} // end of namespace xlifepp
