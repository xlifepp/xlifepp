/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
    \file ext_UmfPackSolver.cpp
    \author Manh Ha NGUYEN
    \since 30 August 2013
    \date 24 Sep 2013

    Low level tests of UmfPackSolver.
    Almost functionalities are checked.
    This function may either creates a reference file storing the results (check=false)
    or compares results to those stored in the reference file (check=true)
    It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace ext_UmfPackSolver {

#ifdef XLIFEPP_WITH_UMFPACK

void testUmfPackSolverCases(std::stringstream& out, Number sizeReal, const std::string rMatrixSym, const std::string rMatrixNonSym,
                       Number sizeCplx, const std::string cMatrixSym, const std::string cMatrixNonSym,
                       StorageType st, AccessType at)
{
  UmfPackSolver uPSolver;
  UmfPackLU<LargeMatrix<Real> > rUmfLU;
  UmfPackLU<LargeMatrix<Complex> > cUmfLU;

  LargeMatrix<Real> rMatSym((rMatrixSym), _coo, sizeReal, sizeReal,st, at);
  LargeMatrix<Real> rMatNonSym((rMatrixNonSym), _coo, sizeReal, sizeReal, st, at);

  // Complex matrices
  LargeMatrix<Complex> cMatSym((cMatrixSym), _coo, sizeCplx, sizeCplx, st, at);
  LargeMatrix<Complex> cMatNonSym((cMatrixNonSym), _coo, sizeCplx, sizeCplx, st, at);

  std::vector<int_t> colPtr, rowIdx;
  std::vector<Real> rvalues;
  std::vector<Complex> cvalues;

  // Real Vector B
  Vector<Real> rvBr(rMatSym.nbRows);
  Vector<Real>::it_vk itrBr; for (itrBr = rvBr.begin(); itrBr != rvBr.end(); ++itrBr) *itrBr = NumTraits<Real>::random();
  Vector<Real> rvXr(rvBr.size());
  Vector<Real> rResidualr(rvBr.size());

  // Complex Vector B for case A real
  Vector<Complex> cvBr(rMatSym.nbRows);
  Vector<Complex>::it_vk itcvBr; for (itcvBr = cvBr.begin(); itcvBr != cvBr.end(); ++itcvBr) *itcvBr = NumTraits<Complex>::random();
  Vector<Complex> cvXr(cvBr.size());
  Vector<Complex> cResidualr(cvBr.size());

  out << "System with real matrix A -----------------------------------------------------------------------------------" << "\n";
  out << "Symmetric system ------------------------------------------------------------------------------------------" << "\n";
  out << "Real matrix B " << "\n";
  uPSolver(rMatSym,rvBr,rvXr);
  multMatrixVector(rMatSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n";

  rMatSym.umfpackSolve(rvBr,rvXr);
  multMatrixVector(rMatSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n \n" ;

  out << "Complex matrix B " << "\n";
  uPSolver(rMatSym,cvBr,cvXr);
  multMatrixVector(rMatSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n";

  rMatSym.umfpackSolve(cvBr,cvXr);
  multMatrixVector(rMatSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n \n";

  out << "Calculating with UmfPackLU " << "\n";
  rUmfLU.compute(rMatSym);

  out << "Real matrix B " << "\n";
  rUmfLU.solve(rvBr,rvXr);
  multMatrixVector(rMatSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n";

  out << "Complex matrix B " << "\n";
  rUmfLU.solve(cvBr,cvXr);
  multMatrixVector(rMatSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n \n";

  rMatSym.extract2UmfPack(colPtr,rowIdx, rvalues);
  out << "Calculating via calling umfpackSolver with column Pointer, row index and values in format of Umfpack" << "\n";
  out << "Real matrix B " << "\n";
  uPSolver(int_t(rMatSym.nbRows), colPtr, rowIdx, &rvalues[0], rvBr, rvXr);
  multMatrixVector(rMatSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n";

  out << "Complex matrix B " << "\n";
  uPSolver(rMatSym.nbRows, colPtr,rowIdx, &rvalues[0],cvBr,cvXr);
  multMatrixVector(rMatSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n";

  rMatSym.umfpackSolve(colPtr, rowIdx, rvalues, rvBr, rvXr);
  multMatrixVector(rMatSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n \n" ;

  rMatSym.umfpackSolve(colPtr,rowIdx, rvalues, cvBr, cvXr);
  multMatrixVector(rMatSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n \n";

  out << "Non-Symmetric system ------------------------------------------------------------------------------------------" << "\n";
  out << "Real matrix B " << "\n";
  uPSolver(rMatNonSym,rvBr,rvXr);
  multMatrixVector(rMatNonSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n";

  rMatNonSym.umfpackSolve(rvBr,rvXr);
  multMatrixVector(rMatNonSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n \n" ;

  out << "Complex matrix B " << "\n";
  uPSolver(rMatNonSym,cvBr,cvXr);
  multMatrixVector(rMatNonSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n";

  rMatNonSym.umfpackSolve(cvBr,cvXr);
  multMatrixVector(rMatNonSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n \n ";

  out << "Calculating with UmfPackLU " << "\n";
  rUmfLU.compute(rMatNonSym);

  out << "Real matrix B " << "\n";
  rUmfLU.solve(rvBr,rvXr);
  multMatrixVector(rMatNonSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n";

  out << "Complex matrix B " << "\n";
  rUmfLU.solve(cvBr,cvXr);
  multMatrixVector(rMatNonSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n \n \n";

  out << "Calculating via calling umfpackSolver with column Pointer, row index and values in format of Umfpack" << "\n";
  rMatNonSym.extract2UmfPack(colPtr,rowIdx, rvalues);

  uPSolver(rMatNonSym.nbRows, colPtr,rowIdx, &rvalues[0],rvBr,rvXr);
  multMatrixVector(rMatNonSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n";

  rMatNonSym.umfpackSolve(colPtr,rowIdx, rvalues,rvBr,rvXr);
  multMatrixVector(rMatNonSym,rvXr,rResidualr);
  rResidualr -= rvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << rResidualr.norm2() << "\n \n" ;

  out << "Complex matrix B " << "\n";
  uPSolver(rMatNonSym.nbRows, colPtr,rowIdx, &rvalues[0],cvBr,cvXr);
  multMatrixVector(rMatNonSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n";

  rMatNonSym.umfpackSolve(colPtr,rowIdx, rvalues, cvBr,cvXr);
  multMatrixVector(rMatNonSym,cvXr,cResidualr);
  cResidualr -= cvBr;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualr.norm2() << "\n \n ";

  // Real Vector B
  Vector<Real> rvBc(cMatSym.nbRows);
  Vector<Real>::it_vk itrBc; for (itrBc = rvBc.begin(); itrBc != rvBc.end(); ++itrBc) *itrBc = NumTraits<Real>::random();
  Vector<Complex> complexB(rvBc);
  Vector<Real> rResidualc(rvBc.size());

  // Complex Vector B
  Vector<Complex> cvBc(cMatSym.nbRows);
  Vector<Complex>::it_vk itcBc; for (itcBc = cvBc.begin(); itcBc != cvBc.end(); ++itcBc) *itcBc = NumTraits<Complex>::random();
  Vector<Complex> cvXc(cvBc.size());
  Vector<Complex> cResidualc(cvBc.size());

  out << "System with complex matrix A -----------------------------------------------------------------------------------" << "\n";
  out << "Symmetric system ------------------------------------------------------------------------------------------" << "\n";
  out << "Real matrix B " << "\n";
  uPSolver(cMatSym,rvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatSym.umfpackSolve(rvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n" ;

  out << "Complex matrix B " << "\n";
  uPSolver(cMatSym,cvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatSym.umfpackSolve(cvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n ";

  out << "Calculating with UmfPackLU " << "\n";
  cUmfLU.compute(cMatSym);

  out << "Real matrix B " << "\n";
  cUmfLU.solve(rvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  out << "Complex matrix B " << "\n";
  cUmfLU.solve(cvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n \n";

  out << "Calculating via calling umfpackSolver with column Pointer, row index and values in format of Umfpack" << "\n";
  cMatSym.extract2UmfPack(colPtr,rowIdx, cvalues);
  uPSolver(cMatSym.nbRows, colPtr,rowIdx, &cvalues[0],rvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatSym.umfpackSolve(colPtr,rowIdx, cvalues,rvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n" ;

  out << "Complex matrix B " << "\n";
  uPSolver(cMatSym.nbRows, colPtr,rowIdx, &cvalues[0],cvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatSym.umfpackSolve(colPtr,rowIdx, cvalues,cvBc,cvXc);
  multMatrixVector(cMatSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n \n ";


  out << "Non-Symmetric system ------------------------------------------------------------------------------------------" << "\n";
  out << "Real matrix B " << "\n";
  uPSolver(cMatNonSym,rvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatNonSym.umfpackSolve(rvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n" ;

  out << "Complex matrix B " << "\n";
  uPSolver(cMatNonSym,cvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatNonSym.umfpackSolve(cvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n ";

  out << "Calculating with UmfPackLU " << "\n";
  cUmfLU.compute(cMatNonSym);

  out << "Real matrix B " << "\n";
  cUmfLU.solve(rvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  out << "Complex matrix B " << "\n";
  cUmfLU.solve(cvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n \n";

  out << "Calculating via calling umfpackSolver with column Pointer, row index and values in format of Umfpack" << "\n";
  cMatNonSym.extract2UmfPack(colPtr,rowIdx, cvalues);

  uPSolver(cMatNonSym.nbRows, colPtr,rowIdx, &cvalues[0],rvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatNonSym.umfpackSolve(colPtr,rowIdx, cvalues,rvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= complexB;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n" ;

  out << "Complex matrix B " << "\n";
  uPSolver(cMatNonSym.nbRows, colPtr,rowIdx, &cvalues[0],cvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling UmfPackSolver " << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n";

  cMatNonSym.umfpackSolve(colPtr,rowIdx, cvalues, cvBc,cvXc);
  multMatrixVector(cMatNonSym,cvXc,cResidualc);
  cResidualc -= cvBc;
  out << "Calculating via calling umfPackSolver function of LargeMatrix" << "\n";
  out << "Residual :||Ax-B|| = " << cResidualc.norm2() << "\n \n ";
}
#endif

void ext_UmfPackSolver(int argc, char* argv[], bool check)
{
  String rootname = "ext_UmfPackSolver";
#ifdef XLIFEPP_WITH_UMFPACK
  trace_p->push(rootname);
  std::stringstream out; // string stream receiving results
  out.precision(testPrec);
  String errors;
  Number nbErrors = 0;
  
  const std::string rMatrixSym(inputsPathTo(rootname+"/bfw782a.coo"));
  const std::string rMatrixNonSym(inputsPathTo(rootname+"/bfw782d.coo"));
  const std::string cMatrixSym(inputsPathTo(rootname+"/mhd1280c.coo"));
  const std::string cMatrixNonSym(inputsPathTo(rootname+"/mhd1280b.coo"));

  out << "LargeMatrix with DENSE ROW storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _dense, _row);
  out << "LargeMatrix with DENSE COL storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _dense, _col);
  out << "LargeMatrix with DENSE DUAL storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _dense, _dual);
  out << "LargeMatrix with COMPRESSED SPARSE ROW storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _cs, _row);
  out << "LargeMatrix with COMPRESSED SPARSE COL storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _cs, _col);
  out << "LargeMatrix with COMPRESSED SPARSE DUAL storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _cs, _dual);
  out << "LargeMatrix with COMPRESSED SPARSE SYM storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _cs, _sym);
  out << "LargeMatrix with SKYLINE DUAL storage ----------------------------------------------------------------------------\n \n";
  testUmfPackSolverCases(out, 782, rMatrixSym, rMatrixNonSym, 1280, cMatrixSym, cMatrixNonSym, _skyline, _dual);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //-------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
#endif
}

}
