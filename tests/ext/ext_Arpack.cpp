/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file ext_Arpack.cpp
  \author Y. Lafranche
  \since 13 dec 2015
  \date 13 dec 2015

  Tests of all Arpack computational modes.

*/
#include "xlife++-libs.h"
#include "testUtils.hpp"
#ifdef XLIFEPP_WITH_ARPACK
#include "exArpUsrCl.hpp"
// A .cpp file is included below because the XLiFE++ test policy requires
// that the creation of the executable file is built from a unique source file.
#include "exArpUsrCl.cpp"
#endif

using namespace xlifepp;

namespace ext_Arpack {
using std::cout;
using std::endl;

void ext_Arpack(int argc, char* argv[], bool check) {

  String rootname = "ext_Arpack";
#ifdef XLIFEPP_WITH_ARPACK

void printRes(std::stringstream& out, EigenElements& areigs, bool inv=false, bool LaplacePb=true);
/*
   Eigen values of the Laplace operator on the segment with Neumann conditions.

   On the segment [0, pi], the eigen values are of the form "square of integers"
   that is to say eigval = k*k, k= 0, 1, 2...
                         = 0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100, 121...
*/

  trace_p->push(rootname);
  std::stringstream out; // string stream receiving results
  out.precision(testPrec);
  String errors;
  Number nbErrors = 0;
  printTestLabel(theCout, "  Eigen values of the Laplacien on the segment with Neumann conditions");
  theCout << "  ===================================================================="<< endl;
  int nbint=1; // number of intervalls
  int dk=60;   // interpolation degree
  theCout << "Interpolation degree = " << dk << endl;

  // mesh : segment [0, pi]
  Mesh  zeroPi(Segment(_xmin=0, _xmax=pi_, _nnodes=nbint+1), 1);
  Domain omega = zeroPi.domain("Omega");

  Interpolation&  interp=interpolation(Lagrange, GaussLobattoPoints, dk, H1);
  Space Vk(omega, interp, "Vk");
  Unknown u(Vk, "u");
  TestFunction v(u, "v");

  int qro = 2*dk+1;
  BilinearForm muv = intg(omega, u * v, defaultQuadrature,qro),
               auv = intg(omega, grad(u) | grad(v), defaultQuadrature,qro) + muv ;
  cpuTime();
  // Compute the Stiffness+Mass and Mass matrices
  TermMatrix A(auv,"auv"), M(muv,"muv");
  cpuTime("Terms computation");

  int Nrows = M.numberOfRows();
  theCout << "Problem dimension = " << Nrows << endl;

  // The eigenvalue problem writes A x = l M x
  // Compute the nev first eigenvalues with smallest magnitude with Arpack
  int nev = std::min(12, Nrows-1);

  printTestLabel(theCout, "\n                    ====== STANDARD PROBLEM ======");
  
  // --> Compute eigen values of Stiffness+Mass matrix, just to test standard problem modes

  Real valsig = 600.;
  { // Real symmetric std pb, Regular mode, LM
    printTestLabel(theCout, "Real symmetric std pb, Regular mode, LM");
    cpuTime();
    EigenElements areigs = arpackSolve(A,_nev=nev);
    cpuTime("Real symmetric std pb, Regular mode, LM");
    printRes(out,areigs,false,false);
  }
  { // Real symmetric std pb, Shift and invert mode
    printTestLabel(theCout, "Real symmetric std pb, Shift and invert mode");
    cpuTime();
    EigenElements areigs = arpackSolve(A,_nev=nev, _sigma=valsig);
    cpuTime("Real symmetric std pb, Shift and invert mode");
    printRes(out,areigs,false,false);
  }
  theCout << endl;

  { // Real nonsymmetric std pb, Regular mode, LM
    printTestLabel(theCout, "Real nonsymmetric std pb, Regular mode, LM");
    cpuTime();
    EigenElements areigs = arpackSolve(A,_nev=nev, _forceNonSym);
    cpuTime("Real nonsymmetric std pb, Regular mode, LM");
    printRes(out,areigs,false,false);
  }
  { // Real nonsymmetric std pb, Shift and invert mode
    printTestLabel(theCout, "Real nonsymmetric std pb, Shift and invert mode");
    cpuTime();
    EigenElements areigs = arpackSolve(A,_nev=nev, _sigma=valsig, _forceNonSym);
    cpuTime("Real nonsymmetric std pb, Shift and invert mode");
    printRes(out,areigs,false,false);
  }
  theCout << endl;

  { // Complex symmetric std pb, Regular mode, LM
    printTestLabel(theCout, "Complex symmetric std pb, Regular mode, LM");
    TermMatrix Ac = toComplex(A);
    cpuTime();
    EigenElements areigs = arpackSolve(Ac,_nev=nev);
    cpuTime("Complex symmetric std pb, Regular mode, LM");
    printRes(out,areigs,false,false);
  }
  { // Complex symmetric std pb, Shift and invert mode
    printTestLabel(theCout, "Complex symmetric std pb, Shift and invert mode");
    TermMatrix Ac = toComplex(A);
    cpuTime();
    EigenElements areigs = arpackSolve(Ac,_nev=nev, _sigma=complex_t(valsig,1.));
    cpuTime("Complex symmetric std pb, Shift and invert mode");
    printRes(out,areigs,false,false);
  }

  printTestLabel(theCout, "\n                    ====== GENERALIZED PROBLEM ======");

  // --> Compute eigen values of the Laplace problem

  valsig = 1.5;
  { // Real symmetric gen pb, Regular mode, LM
    printTestLabel(theCout, "Real symmetric gen pb, Regular mode, LM");
    cpuTime();
    EigenElements areigs = arpackSolve(M,A,_nev=nev, _sort=_decr_module);
    cpuTime("Real symmetric gen pb, Regular mode, LM");
    printRes(out,areigs,true);
  }
  { // Real symmetric gen pb, Regular mode, SM
    printTestLabel(theCout, "Real symmetric gen pb, Regular mode, SM");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _which="SM");
    cpuTime("Real symmetric gen pb, Regular mode, SM");
    printRes(out,areigs);
  }
  { // Real symmetric gen pb, Shift and invert mode
    printTestLabel(theCout, "Real symmetric gen pb, Shift and invert mode");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _sigma=valsig); // Ok with sigma=0
    cpuTime("Real symmetric gen pb, Shift and invert mode");
    printRes(out,areigs);
  }
  { // Real symmetric gen pb, Buckling mode
    printTestLabel(theCout, "Real symmetric gen pb, Buckling mode");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _sigma=valsig, _mode=_buckling);
    cpuTime("Real symmetric gen pb, Buckling mode");
    printRes(out,areigs);
  }
  { // Real symmetric gen pb, Cayley mode
    printTestLabel(theCout, "Real symmetric gen pb, Cayley mode");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _sigma=valsig, _mode=_cayley);
    cpuTime("Real symmetric gen pb, Cayley mode");
    printRes(out,areigs);
  }
  theCout << endl;

  { // Real nonsymmetric gen pb, Regular mode, LM
    printTestLabel(theCout, "Real nonsymmetric gen pb, Regular mode, LM");
    cpuTime();
    EigenElements areigs = arpackSolve(M,A,_nev=nev, _forceNonSym, _sort=_decr_module);
    cpuTime("Real nonsymmetric gen pb, Regular mode, LM");
    printRes(out,areigs,true);
  }
  { // Real nonsymmetric gen pb, Regular mode, SM
    printTestLabel(theCout, "Real nonsymmetric gen pb, Regular mode, SM");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _which="SM", _tolerance=1.e-10, _forceNonSym);
    cpuTime("Real nonsymmetric gen pb, Regular mode, SM");
    printRes(out,areigs);
  }
  { // Real nonsymmetric gen pb, Real Shift and invert mode
    printTestLabel(theCout, "Real nonsymmetric gen pb, Real Shift and invert mode");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _sigma=valsig, _forceNonSym);
    cpuTime("Real nonsymmetric gen pb, Real Shift and invert mode");
    printRes(out,areigs);
  }
  { // Real nonsymmetric gen pb, Complex Shift and invert mode, Re
    printTestLabel(theCout, "Real nonsymmetric gen pb, Complex Shift and invert mode, Re");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _sigma=complex_t(valsig,0.), _mode=_cshiftRe, _forceNonSym);
    cpuTime("Real nonsymmetric gen pb, Complex Shift and invert mode, Re");
    printRes(out,areigs);
  }
  { // Real nonsymmetric gen pb, Complex Shift and invert mode, Im
    printTestLabel(theCout, "Real nonsymmetric gen pb, Complex Shift and invert mode, Im");
    cpuTime();
    EigenElements areigs = arpackSolve(A,M,_nev=nev, _sigma=complex_t(0.5,valsig), _mode=_cshiftIm, _forceNonSym);
    cpuTime("Real nonsymmetric gen pb, Complex Shift and invert mode, Im");
    printRes(out,areigs);
  }
  theCout << endl;

  { // Complex symmetric gen pb (real rhs), Regular mode, LM
    printTestLabel(theCout, "Complex symmetric gen pb (real rhs), Regular mode, LM");
    TermMatrix Mc = toComplex(M);
    cpuTime();
    EigenElements areigs = arpackSolve(Mc,A,_nev=nev, _sort=_decr_module);
    cpuTime("Complex symmetric gen pb (real rhs), Regular mode, LM");
    printRes(out,areigs,true);
  }
  {
    TermMatrix Ac = toComplex(A);
    { // Complex symmetric gen pb (real rhs), Regular mode, SM
      printTestLabel(theCout, "Complex symmetric gen pb (real rhs), Regular mode, SM");
      cpuTime();
      EigenElements areigs = arpackSolve(Ac,M,_nev=nev, _which="SM");
      cpuTime("Complex symmetric gen pb (real rhs), Regular mode, SM");
      printRes(out,areigs);
    } 
    { // Complex symmetric gen pb (real rhs), Shift and invert mode
      printTestLabel(theCout, "Complex symmetric gen pb (real rhs), Shift and invert mode");
      cpuTime();
      EigenElements areigs = arpackSolve(Ac,M,_nev=nev, _sigma=complex_t(valsig,1.));
      cpuTime("Complex symmetric gen pb (real rhs), Shift and invert mode");
      printRes(out,areigs);
    }
  }
  {
    TermMatrix Mc = toComplex(M);
    TermMatrix Ac = toComplex(A);
    { // Complex symmetric gen pb, Regular mode, LM
      printTestLabel(theCout, "Complex symmetric gen pb, Regular mode, LM");
      cpuTime();
      EigenElements areigs = arpackSolve(Mc,Ac,_nev=nev, _sort=_decr_module);
      cpuTime("Complex symmetric gen pb, Regular mode, LM");
      printRes(out,areigs,true);
    }
    { // Complex symmetric gen pb, Regular mode, LM, converted to std pb
      printTestLabel(theCout, "Complex symmetric gen pb, Regular mode, LM, converted to std pb");
      cpuTime();
      EigenElements areigs = arpackSolve(Mc,Ac,_nev=nev, _sort=_decr_module, _convToStd);
      cpuTime("Complex symmetric gen pb, Regular mode, LM, converted to std pb");
      printRes(out,areigs,true);
    }
    { // Complex symmetric gen pb, Regular mode, SM
      printTestLabel(theCout, "Complex symmetric gen pb, Regular mode, SM");
      cpuTime();
      EigenElements areigs = arpackSolve(Ac,Mc,_nev=nev, _which="SM");
      cpuTime("Complex symmetric gen pb, Regular mode, SM");
      printRes(out,areigs);
    }
    { // Complex symmetric gen pb, Regular mode, SM, converted to std pb
      printTestLabel(theCout, "Complex symmetric gen pb, Regular mode, SM, converted to std pb");
      cpuTime();
      EigenElements areigs = arpackSolve(Ac,Mc,_nev=nev, _which="SM", _convToStd);
      cpuTime("Complex symmetric gen pb, Regular mode, SM, converted to std pb");
      printRes(out,areigs);
    }
    { // Complex symmetric gen pb, Shift and invert mode
      printTestLabel(theCout, "Complex symmetric gen pb, Shift and invert mode");
      cpuTime();
      EigenElements areigs = arpackSolve(Ac,Mc,_nev=nev, _sigma=complex_t(valsig,1.));
      cpuTime("Complex symmetric gen pb, Shift and invert mode");
      printRes(out,areigs);
    }
    { // Complex symmetric gen pb, Shift and invert mode, converted to std pb
      printTestLabel(theCout, "Complex symmetric gen pb, Shift and invert mode, converted to std pb");
      cpuTime();
      EigenElements areigs = arpackSolve(Ac,Mc,_nev=nev, _sigma=complex_t(valsig,1.), _convToStd);
      cpuTime("Complex symmetric gen pb, Shift and invert mode, converted to std pb");
      printRes(out,areigs);
    }
  }

  printTestLabel(theCout, "\n                    ====== STANDARD PROBLEM WITH USER CLASS ======");
  
  { // Real nonsymmetric std pb with user class, Regular mode, SM
    printTestLabel(theCout, "Real nonsymmetric std pb with user class, Regular mode, SM");
    StdNonSym usrcl(A,M);
    ArpackProb Arpb(usrcl,nev,"SM",false); // false means use nonsymmetric algorithm
    cpuTime();
    EigenElements areigs = arpackSolve(Arpb);
    cpuTime("Real nonsymmetric std pb with user class, Regular mode, SM");
    printRes(out,areigs);
  }
  theCout << endl;

  { // Complex symmetric std pb with user class, Regular mode, SM
    printTestLabel(theCout, "Complex symmetric std pb with user class, Regular mode, SM");
    TermMatrix Ac = toComplex(A);
    StdComp usrcl(Ac,M);
    ArpackProb Arpb(usrcl,nev,"SM");
    cpuTime();
    EigenElements areigs = arpackSolve(Arpb);
    cpuTime("Complex symmetric std pb with user class, Regular mode, SM");
    printRes(out,areigs);
  }

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //-------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
#endif
}

#ifdef XLIFEPP_WITH_ARPACK
void printRes(std::stringstream& out, EigenElements& areigs, bool inv, bool LaplacePb) {
  out << " Interface object: " << arInterfaceObj() << endl;
  out << " Kind of factorization: " << arKindOfFactorization() << endl;
  out << " Arpack++ object: " << arpackObj() << endl;
  out << arEigInfos();

  out.precision(testPrec);
  out << " Eigenvalues :" << endl;
  int nconv = areigs.numberOfEigenValues();
  // Print the nconv eigenvalues that have been computed. By default, they are sorted by increasing module.
  Real offset = LaplacePb ? 1 : 0;
  if (inv)  for (int i = 0; i < nconv; i++) { theCout << 1./areigs.value(i+1).real() - offset  << endl; }
  else      for (int i = 0; i < nconv; i++) { theCout <<    areigs.value(i+1).real() - offset  << endl; }
}
#endif
} // end of namespace test_Arpack
