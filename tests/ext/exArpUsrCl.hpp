/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file exArpUsrCl.hpp
  \author Y. Lafranche
  \since 15 jan 2016
  \date 15 jan 2016

  \brief Definition of xlifepp::StdNonSym and xlifepp::StdComp classes

  These classes are example classes of what the end user is invited to write
  in order to define an eigenvalue problem whose operator is not a linear
  combinaison of TermMatrix objects.

  The StdNonSym class mainly defines the product inv(B)*A * x, which is used to solve
  the generalized eigenvalue problem  A*x = l B*x, but reformulated as the standard one
  inv(B)*A * x = l x. Since inv(B)*A is not symmetric, the non symmetric algorithm of
  Arpack should then be selected.

  The StdComp class does the same ; the only difference is the type of the matrix A
  which is complex.

 */

#ifndef EX_ARP_USR_CL_HPP
#define EX_ARP_USR_CL_HPP

#include "term.h"

namespace xlifepp {

class StdNonSym: public ARStdFrame<Real> {
public:
   //! constructor
   StdNonSym(TermMatrix& A, TermMatrix& B);

   //! destructor
   ~StdNonSym(){ delete fact_p; }

  //! matrix-vector product required : y <- inv(B)*A * x
   void MultOPx (Real *x, Real *y);

private:
   //! pointers to internal data objects
   const LargeMatrix<Real> *matA_p, *matB_p;
   //! pointer to temporary factorized matrix B
   LargeMatrix<Real>* fact_p;

//! work vectors used in MultOPx, resized to dimension of the problem in constructor
   std::vector<Real> Ax;

}; // end of Class StdNonSym =========================================

class StdComp: public ARStdFrame<complex_t> {
public:
   //! constructor
   StdComp(TermMatrix& A, TermMatrix& B);

   //! destructor
   ~StdComp(){ delete fact_p; }

  //! matrix-vector product required : y <- inv(B)*A * x
   void MultOPx (complex_t *x, complex_t *y);

private:
   //! pointers to internal data objects
   const LargeMatrix<complex_t> *matA_p;
   const LargeMatrix<Real> *matB_p;
   //! pointer to temporary factorized matrix B
   LargeMatrix<Real>* fact_p;

//! work vectors used in MultOPx, resized to dimension of the problem in constructor
   std::vector<complex_t> Ax;

}; // end of Class StdComp =========================================
} // end of namespace xlifepp
#endif // EX_ARP_USR_CL_HPP
