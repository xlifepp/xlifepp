/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file testUtils.cpp
  \author E. Lunéville
  \since 3 dec 2011
  \date 3 may 2012

  Utilities to manage tests.

*/

#include <iostream>
#include <iomanip>
#include "testUtils.hpp"
#include "utils.h"
#include "finalize.h"

namespace xlifepp
{

//! return full path to filename in data directory
String resPathTo(const String& basename)
{
#ifdef OS_IS_WIN
  return Environment::installPath() + "\\tests\\res\\" + basename;
#elif defined(OS_IS_UNIX)
  return Environment::installPath() + "/tests/res/" + basename;
#endif
}

//! return full path to filename in inputs directory
String inputsPathTo(const String& basename, bool checkExist)
{
  String path;
#ifdef OS_IS_WIN
  path = Environment::installPath() + "\\tests\\inputs\\" + basename;
#elif defined(OS_IS_UNIX)
  path = Environment::installPath() + "/tests/inputs/" + basename;
#endif
  if (checkExist) return securedPath(path);
  return rightPath(path);
}

/*!
Perform the difference between the file rootname.res and results stored in the stringstream result.
If opening the file fails, return "tests_rootname : error open file rootname.res"
If there is no difference return  "tests_rootname : OK"
else return "tests_rootname : nb errors" and the lines which differ

be cautious, results of type pointer may be different
*/
String diffResFile(std::stringstream& result, const String& rootname)
{
  // save results in a file or compare results with some references value
  String report = rootname + ":";
  String fname = resPathTo(rootname + ".res");
  bool ok = true;
  result.seekg(std::ios::beg);

  std::ifstream fin(fname.c_str()); // open file of reference results

  if (!fin.is_open()) // check file
  {
    report = "test_" + rootname + ": " + words("error opening file") + " " + fname;
    return report;
  }

  // load each line and compare to the present one (ascii diff)
  Number line = 0, nb_error = 0;

  while (!result.eof() && !fin.eof())
  {
    String sresult, sfin;
    getline(result, sresult);
    getline(fin, sfin);
    line++;

    if (normalizeResData(sresult) != sfin) // difference between two lines
    {
      ok = false;
      nb_error++;
      report += words("difference at line") + " " + tostring(line) + "\n";
      report += "   " + words("reference") + ": " + sfin + "\n";
      report += "         " + words("new") + ": " + normalizeResData(sresult) + "\n";
    }

    if ((result.eof() && !fin.eof()) || (!result.eof() && fin.eof()))
    {
      return report += " " + words("number of lines differs") + " !\n";
    }
  } // end while

  fin.close();
  Number pos = report.find(":");

  if (ok)
  {
    report.insert(pos + 1, " OK");
  }
  else
  {
    String ser = " error";
    if (nb_error > 1) { ser += "s"; }
    report.insert(pos + 1, " " + tostring(nb_error) + ser + "\n");
  }

  return report;
}

/*!
  Save a stringstream result to the file 'rootname.res'
*/
String saveResToFile(std::stringstream& result, const String& rootname)
{
  String fname = resPathTo(rootname + ".res");
  String report = rootname + ": " + words("results saved to") + " " + fname;
  String line;
  result.seekg(std::ios::beg);
  std::ofstream fout(fname.c_str());

  if (! fout.is_open()) // check opening
  {
    report = rootname + ": " + words("error opening save file") + " " + fname;
    return report;
  }

  while (getline(result, line)) { fout << normalizeResData(line) << std::endl; }

  fout.close();
  return report;
}

/*!
  normalize output for res data
  For now, it removes useless zeros in scientific notation
  so that exponent is written with 2 digits as soon as possible
*/
String& normalizeResData(String& data)
{
  std::vector<String> nums, truenums;
  for (Number i=0; i < 10; ++i)
  {
    nums.push_back("00"+tostring(i));
    truenums.push_back("0"+tostring(i));
  }
  for (Number i=10; i < 100; ++i)
  {
    nums.push_back("0"+tostring(i));
    truenums.push_back(tostring(i));
  }

  for (Number i=nums.size()-1; i >= 1; --i)
  {
    replaceString(data,String("e+"+nums[i]),String("e+"+truenums[i]));
    replaceString(data,String("e-"+nums[i]),String("e-"+truenums[i]));
    replaceString(data,String("E+"+nums[i]),String("E+"+truenums[i]));
    replaceString(data,String("E-"+nums[i]),String("E-"+truenums[i]));
  }
  replaceString(data,String("e+000"),String("e+00"));
  replaceString(data,String("e-000"),String("e-00"));
  replaceString(data,String("E+000"),String("E+00"));
  replaceString(data,String("E-000"),String("E-00"));
  return data;
}

void testClean()
{
  theCout.stringStream->str(""); theCout.stringStream->clear();
  finalize();
  verboseLevel(1);
}

void printTestLabel(CoutStream& out, const String& label)
{
  out << "- " << label << std::endl;
}

void printTestLabel(CoutStream& out, Number index, const String& label)
{
  out << "- " << index << " - " << label << std::endl;
}


//! Print utilities
void print_Dom(std::ostream& out, const GeomDomain& dom)
{
  if (theVerboseLevel <= 0) { return; }
  dom.domainInfo().print(out);
  out << ", orientation ";
  if (!dom.meshDomain()->orientationComputed) out << "not ";
  out << "computed";
  if (dom.meshDomain()->extensionof_p!=nullptr)
  {
    if (dom.meshDomain()->extensionType==_sideExtension) out << ", side extension of side domain " << dom.meshDomain()->meshDomain()->extensionof_p->name();
    else if(dom.meshDomain()->extensionType==_vertexExtension) out << ", vertex extension of side domain " << dom.meshDomain()->extensionof_p->name();
    else out << ", ficticious domain related to " << dom.meshDomain()->extensionof_p->name();
  }
  if (dom.meshDomain()->dualCrackDomain_p!=nullptr) out << ", side of a crack, dual side: " << dom.meshDomain()->dualCrackDomain_p->name();
  if (theVerboseLevel < 3) { return; }
  Number tvl = theVerboseLevel, n = dom.meshDomain()->meshDomain()->geomElements.size();
  verboseLevel(std::max(theVerboseLevel/10, Number(1u)));
  out << ", " << dom.meshDomain()->geomElements.size() << " elements";

  std::map<Number, GeomElement* > gemap;
  for (int i=0; i<n; i++) { gemap[dom.meshDomain()->geomElements[i]->number()] = dom.meshDomain()->geomElements[i]; }

  std::map<Number, GeomElement* >::iterator it;
  for( it=gemap.begin(); it!=gemap.end(); it++) { out << eol << *it->second; }
  if (dom.meshDomain()->sideToExt.size()>0)
  {
    std::map<Number, std::set<Number> > segemap;
    Number nse =  dom.meshDomain()->sideToExt.size();
    std::map<GeomElement*, std::list<GeoNumPair> >::const_iterator itm;
    std::list<GeoNumPair>::const_iterator ite;
    for (itm=dom.meshDomain()->sideToExt.begin(); itm !=dom.meshDomain()->sideToExt.end(); ++itm)
    {
      for (ite=itm->second.begin(); ite!=itm->second.end(); ++ite)
        segemap[itm->first->number()].insert(ite->first->number());
    }
    out << eol << "List of elements of extension:";
    std::map<Number, std::set<Number> >::const_iterator itsm;
    for (itsm=segemap.begin(); itsm !=segemap.end(); ++itsm)
    {
      out << eol << "  side elt " << itsm->first << ", extension elts:";
      std::set<Number>::const_iterator itn;
      for (itn=itsm->second.begin(); itn!=itsm->second.end(); ++itn) out << " " << *itn;
    }
  }
  verboseLevel(tvl);
}

void print_Composite(std::ostream& out, const GeomDomain& dom)
{
  String st;
  std::map<String,const GeomDomain*> domainsByName;
  for (Number n = 0; n < dom.compositeDomain()->domains().size(); n++)
  { domainsByName[dom.compositeDomain()->domains()[n]->name()] = dom.compositeDomain()->domains()[n]; }
  std::map<String,const GeomDomain*>::const_iterator itd;
  for (itd=domainsByName.begin(); itd != domainsByName.end(); ++itd)
  {
    if (itd != domainsByName.begin()) { st += " "; }
    st += "'" + itd->first + "'";
  }
  //out << message("compositedomain_info", dom.compositeDomain()->sortedName(), dom.dim(), words("setop", dom.compositeDomain()->setOpType()), st);
  for (itd=domainsByName.begin(); itd != domainsByName.end(); ++ itd)
  {
    out << eol;
    print_Domain(out, *(itd->second));
  }
}

void print_Domain(std::ostream& out, const GeomDomain& dom)
{
  if (theVerboseLevel <= 0) { return; }
  if (dom.domType() == _compositeDomain) { print_Composite(out, dom); }
  else if (dom.domType() == _meshDomain) { print_Dom(out, dom); }
}

void printTheDomains(std::ostream& out, std::vector<const GeomDomain*> doms)
{
  if (theVerboseLevel <= 0) { return; }
  std::vector<const GeomDomain*>::const_iterator it;
  out << "List of all domains in the memory:";
  for (it=doms.begin(); it!=doms.end(); it++)
  {
    out << eol;
    print_Domain(out, **it);
  }
}

void  printCompDirectIter( String&  YN, const String Nom, const TermMatrix& MAT, const TermVector& B, const TermVector& X, const TermVector& UD, Real epsilon, Vector<String>& Vresult, Real et)
{
  TermVector E=B-MAT*X;
  TermVector E1=B-MAT*UD;
  double_t res=norm2(E),res1=norm2(E1), nB=norm2(B);
  std::stringstream ss;
  if ((res< epsilon*nB)) YN="Y";
  else YN="N";
  TermVector  D=X-UD;
  double err=D.norm2(), nUD=norm2(UD);
  String CV="  CV ";
  if ( err>epsilon*nUD ) CV= " NCV ";
  if (isTestMode)
  {
    thePrintStream << Nom<<", (X-Uex)/||Uex|| "<<err/nUD << "  ?<?  " << epsilon <<" nb iter = "<<IterativeSolver::theNumberOfIterations;
    if(et!=0) thePrintStream <<" time = "<<et;
    thePrintStream<<eol;
    Vresult(1)=Nom;
    Vresult(2)=CV;
    Vresult(3)=tostring(err/nUD);
    Vresult(4)=tostring(IterativeSolver::theNumberOfIterations);
    Vresult(5)=tostring(et);
  }
}

//! round to zero a string
void roundToZero(const String& Cvar, String& ligneI)
{
  ligneI=Cvar;
  Strings VSI;
  int nsi;
  if ( (Cvar.find("e-1") != std::string::npos )|| (Cvar.find("e-2") != std::string::npos)
    || (Cvar.find("e-4") != std::string::npos) || (Cvar.find("e-3") != std::string::npos)
    || (Cvar=="-0")  )
  {
    VSI= split(ligneI, ' ');
    nsi=VSI.size();
    ligneI="";
    if (nsi==0)
    {
      ligneI=" 0.000000000e+00";
    }
    else
    {
      for (Number i=0; i < nsi; ++i) //bcle sur mot de la ligne
      {
        if ( (VSI[i].find("e-1") != std::string::npos) || (VSI[i].find("e-2") != std::string::npos)
          || (VSI[i].find("e-4") != std::string::npos) || (VSI[i].find("e-3") != std::string::npos)
          || (VSI[i]=="-0")  )
        {
          // if (i>0) ligneR+=" ";
          if (i==0) {ligneI+="0.000000000e+00";}
          else      {ligneI+=" 0.000000000e+00";}// VSR[i]=0.;
          // if (i==0) {ligneI+=(VSI[i]);}
          // else      {ligneI+=" "+(VSI[i]);}
        }
        else
        {
          // if (i>0) ligneR+=" ";
          if (i==0) {ligneI+=(VSI[i]);}
          else      {ligneI+=" "+(VSI[i]);}
        }
      }
    }
  }
  // else
  // {
  //   ligne=ligneI;d
  // }
  return;
}

//! Test a single value
Number checkValue(Int SI, Int SR, String& errorMsg, const String& info)
{
  Number A = 0;

  thePrintStream << info  << ": ";
  thePrintStream << SI << std::endl;
  if (SI-SR != 0)
  {
    A=1;
    errorMsg += info + ": " + message("bad_integer_value", SI, SR) + "\n";
    thePrintStream << message("bad_integer_value", SI, SR) << std::endl;
  }
  return A;
}

Number checkValue(Number SI, Number SR, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info  << eol;
  thePrintStream << SI << std::endl;
  if (SI-SR != 0)
  {
    A=1;
    errorMsg += info + ": " + message("bad_number_value", SI, SR) + "\n";
    thePrintStream << message("bad_number_value", SI, SR) << std::endl;
  }
  return A;
}

Number checkValue(bool SI, bool SR, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << ": ";
  thePrintStream << SI << std::endl;
  if ( (SR != SI) )
  {
    A=1;
    errorMsg += info + ": " + message("bad_boolean_value", SI, SR) + "\n";
    thePrintStream << message("bad_boolean_value", SI, SR) << eol;
  }
  return A;
}

Number checkValue(Real SI, Real SR, Real tol, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << ": ";
  thePrintStream << SI << std::endl;
  if (std::abs(SI-SR) > tol)
  {
    A=1;
    errorMsg += info + ": " + message("bad_real_value_with_tolerance", SI, SR, tol) + "\n";
    thePrintStream << message("bad_real_value_with_tolerance", SI, SR, tol) <<eol;
  }
  return A;
}

Number checkValue(Complex SI, Complex SR, Real tol, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << ": ";
  thePrintStream << SI << std::endl;
  if ( ( std::abs(realPart(SI-SR)) > tol)|| ( std::abs( imagPart(SI-SR)) > tol ) )
  {
    A=1;
    errorMsg += info + ": " + message("bad_complex_value_with_tolerance", SI, SR, tol) + "\n";
    thePrintStream << message("bad_complex_value_with_tolerance", SI, SR, tol) << eol;
  }
  return A;
}

//! Test a parameter(s)
Number checkValue(Parameter SI, Int SR, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << " (INT_T)" << ": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.name(), nom, errorMsg, info);
  A+=checkValue( SI.get_i(), SR, errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, Real SR, Real tol, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info <<" (REAL_T)" << ": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.get_r(), SR, tol, errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, Complex SR, Real tol, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << "(COMPLEX_T)" <<": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.get_c(), SR, tol, errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, const String& SR, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << " (STRING_T)" <<": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.get_s(), SR, errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, bool SR, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << " (BOOLEAN)" <<": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.get_b(), SR, errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, std::vector<String> SR, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << " (std::vector<String>)" <<": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.name(), nom, errorMsg, info);
  for (int i=0; i<SR.size(); i++)
     A+=checkValue( SI.get_sv()[i], SR[i], errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, std::vector<Int> SR, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << " (std::vector<Int>)" <<": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.name(), nom, errorMsg, info);
  for (int i=0; i<SR.size(); i++)
     A+=checkValue( SI.get_iv()[i], SR[i], errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, std::vector<Real> SR, const String& nom, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << info << " (std::vector<Real>)" <<": ";
  thePrintStream << SI << std::endl;
  A+=checkValue( SI.name(), nom, errorMsg, info);
  for (int i=0; i<SR.size(); i++)
    A+=checkValue( SI.get_rv()[i], SR[i], errorMsg, info);
  return A;
}

Number checkValue(Parameter SI, const String& refFilename, Real tol, const String& nom, String& errorMsg, const String& info, bool check)
{
  Number A = 0;
  thePrintStream << info << " (refFilename)" <<": ";
  thePrintStream << SI << std::endl;
  A+=checkValue(SI.name(), nom, errorMsg, info);
  switch(SI.type())
  {
    case _realVector:
      A+=checkValues(SI.get_rv(), refFilename, tol, errorMsg, info, check);
      break;
    case _complexVector:
      A+=checkValues(SI.get_cv(), refFilename, tol, errorMsg, info, check);
      break;
    case _realMatrix:
      A+=checkValues(SI.get_rm(), refFilename, tol, errorMsg, info, check);
      break;
    case _complexMatrix:
      A+=checkValues(SI.get_cm(), refFilename, tol, errorMsg, info, check);
      break;
    default:
      return A++;
  }
  return A;
}

Number checkValues(Parameters SI, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A=0;
  Number X=SI.list_.size();
  std::vector<String> VS(X);
  for(int i=0; i<=X-1  ; i++)
  {
    VS[i]=tostring( SI(i+1) );
  }
  A+=checkValues( VS, refFilename, errorMsg, info, check);
  thePrintStream << info << " Parameters -> " << refFilename << eol;
  return A;
}

//! test Vector with a reference file with tolerance
Number checkValues(const Vector<Number>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check Vector<Number> /" << refFilename<< eol;
    Number A=0, Val;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=1;
    if (fin)
    {
      String ligne;
      while (!fin.eof())
      {
        while ( std::getline( fin, ligne ) )
        {
          std::istringstream is(ligne);
          is >> Val;
          A+=(R(ii)-Val);
          if ((R(ii)-Val)!=0) errorMsg += info + ": " + message("bad_number_value_with_tolerance", R(ii), Val, tol) + "\n";
          ii++;
        }
      }
    }
    return A;
  }
  else
  {
    thePrintStream << "edit Vector<Number> ";
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int j=1; j<=R.size(); j++ )
    {
      fout << R(j)<< eol;
      thePrintStream << R(j)<< eol;
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const Vector<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check Vector<Real> /" << refFilename<<eol;
    Number A=0;
    Real Val;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=1;
    if (fin)
    {
      String ligne;
      while (fin >> Val)
      {
          A+=checkValue(R(ii), Val, tol, errorMsg, info+" Re("+tostring(ii)+")");
          ii++;
      }
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit Vector<Real>: ";
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
      for (int j=0; j<=R.size()-1; j++ )
      {
        fout << R[j]<< eol;
        thePrintStream << R[j]<< eol;
      }
    fout.close();
    return 0;
  }
}

Number checkValues(const Vector<Complex>& C, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check Vector<Complex> / " << refFilename<<eol;
    Number A=0;
    Real  valR, valI;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=1;
    if (fin)
    {
      String ligne;
      while (fin >> valR)
      {
           A+=checkValue(realPart(C(ii)), valR , tol, errorMsg, info+" Re("+tostring(ii)+")");
           fin>>valI;
           A+=checkValue(imagPart(C(ii)), valI , tol, errorMsg, info+" Im("+tostring(ii)+")");
           ii++;
      }
    }
    return A;
  }
  else
  {
    thePrintStream << " Edit Vector<Real>: " << eol;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int j=0; j<=C.size()-1; j++ )
    {
      fout << realPart(C[j]) << " " <<imagPart(C[j])<< eol;
      thePrintStream << realPart(C[j]) << " +i*" <<imagPart(C[j])<< eol;
    }
    fout.close();
    return 0;
  }
}

//! Test a std::vector with reference file
Number checkValues(const std::vector<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Vector<Real> RV(R);
  return checkValues( RV, refFilename, tol, errorMsg, info, check);
}

Number checkValues(const std::vector<Complex>& C, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Vector<Complex> CV(C);
  return checkValues( CV, refFilename, tol, errorMsg, info, check);
}

Number checkValues(const std::vector<Vector<Real> >& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<Vector<Real> > / " <<refFilename<< " with tolerance "<<tol<< eol<<info<<eol;
    Number A=0;
    Real  Val;
    Number p=R[0].size();
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      String ligne;
      while (fin >> Val)
      {
        A+=checkValue(R[ii][jj], Val , tol, errorMsg, info+"R["+tostring(ii)+"]["+tostring(jj)+"]");
        jj++;
        if (jj==p) { ii++;jj=0;p=R[ii].size();}
      }
    }
    return A;
  }
  else
  {
    thePrintStream << "Edit std::vector<Vector<Real> >: " << eol;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].size()-1; j++ )
        {
          fout << roundToZero(R[i][j], tol)<< " ";
          thePrintStream << R[i][j]<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].size()-1; j++ )
        {
          fout << R[i][j]<< " ";
          thePrintStream << R[i][j]<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const std::vector<Vector<Complex> >& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<Vector<Real> > / " << refFilename << " with tolerance "<<tol<< eol<<info<<eol;
    Number A=0;
    Real  ValR, ValI;
    Number p=R[0].size();
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      String ligne;
      while (fin >> ValR)
      {
        A+=checkValue(realPart(R[ii][jj]), ValR , tol, errorMsg, info+"ReR["+tostring(ii)+"]["+tostring(jj)+"]");
        fin >> ValI;
        A+=checkValue(imagPart(R[ii][jj]), ValI , tol, errorMsg, info+"ImR["+tostring(ii)+"]["+tostring(jj)+"]");

        jj++;
        if (jj==p) { ii++;jj=0;}
      }
    }
    return A;
  }
  else
  {
    thePrintStream << "Edit std::vector<Vector<Real> >: " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int i=0; i<=R.size()-1; i++ )
    {
      for (int j=0; j<=R[i].size()-1; j++ )
      {
        fout << realPart(R[i][j])<< " "<<imagPart(R[i][j])<< " ";
        thePrintStream << realPart(R[i][j])<< " +i*"<<imagPart(R[i][j])<< " ";
      }
    }
    fout.close();
    return 0;
  }
}

//! Test a Matrix with reference file with tolerance
Number checkValues(const Matrix<Real>& M, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=M.numberOfRows();
  Number nbc=M.numberOfCols();
  Real  val;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << "Check Matrix<Real> / "<<refFilename << " with tolerance "<<tol<< eol<<info<<eol;
    if (fin)
    {
      String ligne;
      i=1; j=1;
      while (fin >> val)
      {
        A+=checkValue(M(i,j), val, tol, errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
        if (i==nbr) {i++;j=1;}
        else if (i<nbr) {j++;}

      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "Edit Matrix-" ;
    thePrintStream << " Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    for (int i =1; i<=nbr; i++ )
    {
      for (int j=1; j<=nbc; j++ )
      {
        fout << M(i,j) << " ";
        thePrintStream << M(i,j)<< " ";
      }
      fout << eol;
      thePrintStream << eol;
    }
    return 0;
  }
}

Number checkValues(const Matrix<Complex>& M, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=M.numberOfRows();
  Number nbc=M.numberOfCols();
  Real   valR, valI;
  if (check)
  {
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    if (fin)
    {
      thePrintStream << " check Matrix<Complex>  / " << refFilename << " with tolerance "<<tol<<eol<<info<<eol;
      String ligne;
      i=1; j=1;
      while(fin >> valR)
      {
        A+=checkValue(realPart(M(i,j)), valR , tol, errorMsg, info+" Re("+tostring(i)+","+tostring(j)+")");
        fin>>valI;
        A+=checkValue(imagPart(M(i,j)), valI , tol, errorMsg, info+" Im("+tostring(i)+","+tostring(j)+")");
        if (i==nbr) {i++;j=1;}
        else if (i<nbr) {j++;}
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << " edit Matrix<Complex>: ";
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    for (int i =1; i<=nbr; i++ )
    {
      for (int j=1; j<=nbc; j++ )
      {
        fout << roundToZero(realPart(M(i,j)), tol)<< " "<< roundToZero(imagPart(M(i,j)), tol)<< " ";
        thePrintStream << realPart(M(i,j))<< " "<< imagPart(M(i,j))<< " ";
      }
      fout << eol;
      thePrintStream << eol;
    }
    return 0;
  }
}

//! Test a Vector/Matrix with reference Vector/Matrix with tolerance
Number checkValues(const Vector<Real>& R, const Vector<Real>& E, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  Number nr=R.size();
  Number ne=E.size();
  thePrintStream << " check Vector<Real>  / Vector<Real> with tolerance "<<tol <<":"<<eol<<info<<eol;
  if (ne != nr)
  {
    A=1;
    errorMsg += info + ": " + message("bad_dim", nr, ne) + "\n";
  }
  else
  {
    for (Number i=0; i < nr; ++i)
    {
      A += checkValue(R[i], E[i], tol, errorMsg, info+"("+tostring(i)+")");
    }
  }
  return A;
}

Number checkValues(const Vector<Complex>& R, const Vector<Complex>& E, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  Number nr=R.size();
  Number ne=E.size();
  thePrintStream << " check Vector<Complex>  / Vector<Complex> with tolerance "<<tol<<":"  <<eol<<info<<eol;
  if (ne != nr)
  {
    A=1;
    errorMsg += info + " = " + message("bad_dim", nr, ne) + "\n";
  }
  else
  {
    for (Number i=0; i < nr; ++i)
    {
      A += checkValue(R[i], E[i],  tol, errorMsg, info+tostring(i));
    }
  }
  return A;
}

Number checkValues(const Vector<Vector<Real> >& R, const Matrix<Real>& E, Real tol, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << " check Vector<Vector<Real>>  / Matrix<Real> with tolerance "<<tol<<":"  <<eol<<info<<eol;
  if ( R.size()*R[0].size() != E.size() )
  {
    A++;
    errorMsg+=info+" Size differ";
  }
  for (Number i=0; i < R.size(); ++i)
  {
    for (Number j=0; j < R[i].size(); ++j)
    {
      if ( R[i][j] != E(i+1, j+1) )
      {
        A += checkValue(R[i][j], E(i+1, j+1),  tol, errorMsg, info+tostring(i)+","+tostring(j));
      }
    }
  }
  return A;
}

Number checkValues(const Vector<Vector<Complex> >& R, const Matrix<Complex>& E, Real tol, String& errorMsg, const String& info)
{
  Number A = 0;
  thePrintStream << " check Vector<Vector<Complex>>  / Matrix<Complex> with tolerance "<<tol<<":"  <<eol<<info<<eol;
  if ( R.size()*R[0].size() != E.size() )
  {
    A++;
    errorMsg+=info+" Size differ";
  }
  for (Number i=0; i < R.size(); ++i)
  {
    for (Number j=0; j < R[i].size(); ++j)
    {
      if ( R[i][j] != E(i+1, j+1) )
      {
        A += checkValue(R[i][j], E(i+1, j+1),  tol, errorMsg, info+tostring(i)+","+tostring(j));
      }
    }
  }
  return A;
}

Number checkValues(const Matrix<Real>& M, const Matrix<Real>& Mref, Real tol, String& errorMsg, const String& info)
{
  thePrintStream << " check  Matrix<Real> / Matrix<Real> with tolerance "<<tol<<":"  <<eol<<info<<eol;
  Number A = 0, i, j;
  Number nbrM=M.numberOfRows();
  Number nbcM=M.numberOfCols();
  Number nbrMref=Mref.numberOfRows();
  Number nbcMref=Mref.numberOfCols();
  if ( (nbrM == nbrMref) && (nbcM == nbcMref) )
  {
    for (i=1; i<=nbrM; i++)
    {
      for (j=1; j<=nbcM ; j++)
      {
        A+=checkValue(M(i,j), Mref(i,j), tol, errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
      }
    }
  }
  else
  {
    A++;
    errorMsg+=info+" Size differ";
  }
    return A;
}

Number checkValues(const Matrix<Complex>& M, const Matrix<Complex>& Mref, Real tol, String& errorMsg, const String& info)
{
  thePrintStream << " check  Matrix<Complex> / Matrix<Complex> with tolerance "<<tol<<":"  <<eol<<info<<eol;
  Number A = 0, i, j;
  Number nbrM=M.numberOfRows();
  Number nbcM=M.numberOfCols();
  Number nbrMref=Mref.numberOfRows();
  Number nbcMref=Mref.numberOfCols();
  if ( (nbrM == nbrMref) && (nbcM == nbcMref) )
  {
    for (i=1; i<=nbrM ; i++)
    {
      for (j=1; j<=nbcM ; j++)
      {
        A+=checkValue(M(i,j), Mref(i,j), tol, errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
      }
    }
  }
  else
  {
    A++;
    errorMsg+=info+" Size differ";
  }
   return A;
}

Number checkValues(const Matrix<Complex>& M, const Matrix<Real>&  Mref, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  A=checkValues( M.real(),  Mref, tol,  errorMsg,  info);
  return A;
}


//! Test std::vector<std::pair<Number, Number> > with reference file with tolerance
Number checkValues(const std::vector<std::pair<Number, Number> > VP, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "check std::vector<std::pair<Number, Number> > / " << refFilename <<" with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    Real  Val1, Val2;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0;
    if (fin)
    {
      while (fin >> Val1)
      {
        A+=checkValue(VP[ii].first, Val1 , tol, errorMsg, info+"["+tostring(ii)+"].first");
        fin>>Val2;
        A+=checkValue(VP[ii].second, Val2 , tol, errorMsg, info+"["+tostring(ii)+"].second");
        ii++;
      }
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit Vector<Real>" ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int j=0; j<=VP.size()-1; j++ )
    {
      fout << VP[j].first << " " << VP[j].second << eol;
      thePrintStream << VP[j].first << " " << VP[j].second << eol;
    }
    fout.close();
    return 0;
  }
}

//! test std::vector<Vector[Matrix]EigenDense with reference file with tolerance
Number checkValues(const std::vector<VectorEigenDense<Real> >& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<VectorEigenDense<Real> > / " << refFilename <<" with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    Real  Val;
    Number p=R[0].size();
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      String ligne;
      while (fin >> Val)
      {
        A+=checkValue(R[ii][jj], Val , tol, errorMsg, info+"R["+tostring(ii)+"]["+tostring(jj)+"]");
        jj++;
        if (jj==p) { ii++; jj=0; p=R[ii].size();}
      }
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit std::vector<VectorEigenDense<Real> >: ";
    thePrintStream << "Reference file non check " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].size()-1; j++ )
        {
          fout << roundToZero(R[i][j], tol)<< " ";
          thePrintStream << R[i][j]<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].size()-1; j++ )
        {
          fout << R[i][j]<< " ";
          thePrintStream << R[i][j]<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const std::vector<VectorEigenDense<Complex> >& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<VectorEigenDense<Complex> >  / " << refFilename <<" with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    Real  ValR, ValI;
    Number p=R[0].size();
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      String ligne;
      while (fin >> ValR)
      {
        A+=checkValue(realPart(R[ii][jj]), ValR , tol, errorMsg, info+"ReR["+tostring(ii)+"]["+tostring(jj)+"]");
        fin >> ValI;
        A+=checkValue(imagPart(R[ii][jj]), ValI , tol, errorMsg, info+"ImR["+tostring(ii)+"]["+tostring(jj)+"]");
        jj++;
        if (jj==p) { ii++; jj=0; p=R[ii].size();}
      }
    }
    return A;
  }
  else
  {
    thePrintStream << "Edit std::vector<Vector<Complex> >" ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
     for (int i=0; i<=R.size()-1; i++ )
     {
       for (int j=0; j<=R[i].size()-1; j++ )
       {
         fout << roundToZero(realPart(R[i][j]), tol)<< " "<<roundToZero(imagPart(R[i][j]), tol)<< " ";
         thePrintStream << realPart(R[i][j])<< " +i*"<<imagPart(R[i][j])<< " ";
       }
       fout << eol;
       thePrintStream << eol;
     }
    }
    else
    {
     for (int i=0; i<=R.size()-1; i++ )
     {
       for (int j=0; j<=R[i].size()-1; j++ )
       {
         fout << realPart(R[i][j])<< " "<<imagPart(R[i][j])<< " ";
         thePrintStream << realPart(R[i][j])<< " +i*"<<imagPart(R[i][j])<< " ";
       }
       fout << eol;
       thePrintStream << eol;
     }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const std::vector<MatrixEigenDense<Real> >& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<MatrixEigenDense<Real> >  / " << refFilename <<" with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    Real  Val;
    Number p=R[0].numOfRows()-1, q=R[0].numOfCols()-1;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0, kk=0;
    if (fin)
    {
      String ligne;
      while (fin >> Val)
      {
        A+=checkValue(R[ii].coeff(jj,kk), Val , tol, errorMsg, info+"R["+tostring(ii)+"]["+tostring(jj)+"["+tostring(kk)+"]");
        if (kk==q)
        {
          kk=0;
          if (jj==p) { ii++; jj=0;p=R[ii].numOfRows()-1, q=R[ii].numOfCols()-1; }
          else {jj++;}
        }
        else {kk++;}
      }
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit std::vector<MatrixEigenDense<Real> >";
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].numOfRows()-1; j++ )
        {
          for (int k=0; k<=R[i].numOfCols()-1; k++ )
          {
            fout << roundToZero(R[i].coeff(j,k), tol)<< " ";
            thePrintStream << R[i].coeff(j,k)<< " ";
          }
          fout << eol;
          thePrintStream << eol;
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].numOfRows()-1; j++ )
        {
          for (int k=0; k<=R[i].numOfCols()-1; k++ )
          {
            fout << R[i].coeff(j,k)<< " ";
            thePrintStream << R[i].coeff(j,k)<< " ";
          }
          fout << eol;
          thePrintStream << eol;
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const std::vector<MatrixEigenDense<Complex> >& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<MatrixEigenDense<Complex> >   /  " << refFilename <<" with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    Real  valR,valI;
    Number p=R[0].numOfRows()-1, q=R[0].numOfCols()-1;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0, kk=0;
    if (fin)
    {
      String ligne;
      while (fin >> valR)
      {
        A+=checkValue(realPart(R[ii].coeff(jj,kk)), valR , tol, errorMsg, info+"Re R["+tostring(ii)+"]["+tostring(jj)+"["+tostring(kk)+"]");
        fin >> valI;
        A+=checkValue(imagPart(R[ii].coeff(jj,kk)), valI , tol, errorMsg, info+"Im R["+tostring(ii)+"]["+tostring(jj)+"["+tostring(kk)+"]");
        if (kk==q)
        {
          kk=0;
          if (jj==p) { ii++; jj=0;p=R[ii].numOfRows()-1, q=R[ii].numOfCols()-1; }
          else {jj++;}
        }
        else {kk++;}
      }
    }
    return A;
  }
  else
  {
    thePrintStream << "Edit std::vector<MatrixEigenDense<Complex> >:";
    thePrintStream << "Reference file: " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].numOfRows()-1; j++ )
        {
          for (int k=0; k<=R[i].numOfCols()-1; k++ )
          {
            fout << roundToZero(realPart(R[i].coeff(j,k)), tol)<< " "<<roundToZero(imagPart(R[i].coeff(j,k)), tol)<< " ";
            thePrintStream << realPart(R[i].coeff(j,k))<< " "<<imagPart(R[i].coeff(j,k))<< " ";
          }
          fout << eol;
          thePrintStream << eol;
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        for (int j=0; j<=R[i].numOfRows()-1; j++ )
        {
          for (int k=0; k<=R[i].numOfCols()-1; k++ )
          {
            fout << realPart(R[i].coeff(j,k))<< " "<<imagPart(R[i].coeff(j,k))<< " ";
            thePrintStream << realPart(R[i].coeff(j,k))<< " "<<imagPart(R[i].coeff(j,k))<< " ";
          }
          fout << eol;
          thePrintStream << eol;
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

//! Test different kind of norm of the difference f two Vectors with tolerance
Number checkValuesN2(const Vector<Real>& R, const Vector<Real>& E, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  Real  Err = norm2(R-E);
  thePrintStream << info << ", error N2:" << Err << " with tol=" << tol << eol;
  if ( Err > tol )
  {
    A=1;
    errorMsg += "@@ :-( ERROR  \n" + info + ", error N2:" + tostring(Err) + " with tol=" + tostring(tol) +"\n";
  }
  return A;
}

Number checkValuesN2(const Vector<Complex>& R, const Vector<Complex>& E, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  Real  Err = norm2(R-E);
  thePrintStream << info << ", error N2:" << Err << " with tol=" << tol << eol;
  if ( Err > tol )
  {
    A=1;
    errorMsg += "@@ :-( ERROR  \n" + info + ", error N2:" + tostring(Err) + " with tol=" + tostring(tol) +"\n";
  }
  return A;
}

//! Test VectorEigenDense with a reference file with tolerance
Number checkValues(const VectorEigenDense<Real> & R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check VectorEigenDense<Real> / " <<refFilename <<" with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    Real  Val;
    Number p=R.size();
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0;
    if (fin)
    {
      while (fin >> Val)
      {
        A+=checkValue(R[ii], Val, tol, errorMsg, info+"R["+tostring(ii)+"]");
        ii++;
      }
      A+=checkValue(ii, p , errorMsg, info+"file size"+tostring(ii)+"="+tostring(p));
      if(A>0) {A=1;}
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit VectorEigenDense<Real> : " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
     for (int i=0; i<=R.size()-1; i++ )
     {
       fout << roundToZero(R[i])<< " ";
       thePrintStream << R[i]<< " ";
     }
    }
    else
    {
     for (int i=0; i<=R.size()-1; i++ )
     {
       fout << R[i]<< " ";
       thePrintStream << R[i]<< " ";
     }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const VectorEigenDense<Complex> & R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check VectorEigenDense<Real> / " << refFilename <<" with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    Real  ValR, ValI;
    Number p=R.size();
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0;
    if (fin)
    {
      while (fin >> ValR)
      {
        A+=checkValue(real(R[ii]), ValR, tol, errorMsg, info+"real R["+tostring(ii)+"]");
        fin >> ValI;
        A+=checkValue(imag(R[ii]), ValI, tol, errorMsg, info+"imag R["+tostring(ii)+"]");
        ii++;
      }
      A+=checkValue(ii, p , errorMsg, info+"file size"+tostring(ii)+"!="+tostring(p));
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit VectorEigenDense<Complex>: ";
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        fout << roundToZero(real(R[i]), tol)<< " "<< roundToZero(imag(R[i]), tol)<<" ";
        thePrintStream << real(R[i])<< " "<< imag(R[i])<<" ";
      }
    }
    else
    {
      for (int i=0; i<=R.size()-1; i++ )
      {
        fout << real(R[i])<< " "<< imag(R[i])<<" ";
        thePrintStream << real(R[i])<< " "<< imag(R[i])<<" ";
      }
    }
    fout.close();
    return 0;
  }
}

//! Test different kind of norm of the difference of two TermVectors
Number checkValuesN2(const TermVector& R, const TermVector& E, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  Real  Err = norm2(R-E);
  thePrintStream << info << ", error N2:" << Err << " with tol=" << tol << eol;
  if ( Err > tol )
  {
    A=1;
    errorMsg += "@@ :-( ERROR  \n" + info + ", error N2:" + tostring(Err) + " with tol=" + tostring(tol) +"\n";
  }
  return A;
}

Number checkValuesL2(const TermVector& R, const TermVector& E, const TermMatrix& M, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  TermVector D; D=R-E;
  Real  Err=std::sqrt(real(M*D|D));
  thePrintStream << info << ", error L2:" << Err << " with tol=" << tol << eol;
  if ( Err > tol )
  {
    A=1;
    errorMsg += "@@ :-( ERROR  \n" + info + ", error L2:" + tostring(Err) + " with tol=" + tostring(tol) +"\n";
  }
  return A;
}

Number checkValuesNinfty(const TermVector& R, const TermVector& E, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  Real  Err = norminfty(R-E);
  thePrintStream << info << ", error Ninfty:" << Err << " with tol=" << tol << eol;
  if ( Err > tol )
  {
    A=1;
    errorMsg += "@@ :-( ERROR  \n" + info + ", error Ninfty:" + tostring(Err) + " with tol=" + tostring(tol) +"\n";
  }
  return A;
}

//! Test values of a TermVector with a reference TermVector
Number checkValues(const TermVector& E, const TermVector& R, Real tol, String& errorMsg, const String& info)
{
  Number A=0;
  Number sizeR=R.size(), sizeE=E.size();
  thePrintStream << info << eol;
  String Err_Msg="";

  if ( sizeR != sizeE)
  {
    Err_Msg="size  of "+R.name()+ "("+tostring(sizeR)+") and size of " + E.name() + "("+tostring(sizeE)+") differ \n";
    errorMsg+=Err_Msg;
  }
  else if ( R.valueType() != E.valueType() )
  {
    Err_Msg="valueType of "+R.name()+ " and " + E.name() + " differ\n";
    errorMsg+=Err_Msg;
  }
  else
  {
    if (isTestMode)
    {
      int i=0;
      if (R.valueType() == _real)
      {
        while ( i<sizeR && A<15)
        {
          A+=checkValue(R.getValue(i).asReal(), E.getValue(i).asReal() , tol, errorMsg, info+" DL "+tostring(i)+" differs \n");
          i++;
        }
      }
      else
      {
        while ( i<sizeR && A<15)
        {
          A+=checkValue(R.getValue(i).asComplex(), E.getValue(i).asComplex() , tol, errorMsg, info+" DL "+tostring(i)+" differs \n");
          i++;
        }
      }
      if (i>14 && A>0) { thePrintStream << "    (.....)    " <<eol;}
    }
    else
    {
      if (R.valueType() == _real)
      {
        for (int i=0; i<sizeR; i++)
          A+=checkValue(R.getValue(i).asReal(), E.getValue(i).asReal() , tol, errorMsg, info+" DL "+tostring(i)+" differs \n");
      }
      else
      {
        for (Int i=0; i<sizeR; i++)
          A+=checkValue(R.getValue(i).asComplex(), E.getValue(i).asComplex() , tol, errorMsg, info+" DL "+tostring(i)+" differs \n");
      }
    }
  }
  return A;
}




//! Test a TermVector with a reference file with tolerance
Number checkValues(const TermVector& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << " check TermVector "<< R.name()<<" /" << refFilename << " with tolerance "<<tol<<":"  <<eol<<info<<eol;
    Number A=0;
    if (R.isSingleUnknown())
    {
      Number s=R.size(), c=R.subVector().nbOfComponents(), d=R.nbDofs();

      std::ifstream fin(inputsPathTo(refFilename).c_str());
      int ii=1;
      if (fin)
      {
        String ligne;
        if (c == 1)
        {
          Vector<Real> VR(s);
          if (R.valueType() == _real)
          {
            while (!fin.eof())
            {
              while ( std::getline( fin, ligne ) )
              {
                std::istringstream is(ligne);
                is >> VR(ii);
                A+=checkValue(R.getValue(ii).asReal(), VR(ii), tol, errorMsg, info+"("+tostring(ii)+")");
                ii++;
              }
            }
          }
          else if (R.valueType() == _complex)
          {
            Vector<Real> VI(s);
            Vector<Complex>  VC(s);
            while (!fin.eof())
            {
              while ( std::getline( fin, ligne ) )
              {
                std::istringstream is(ligne);
                is >> VR(ii) >> VI(ii);
                VC(ii)= VR(ii) + i_*VI(ii);
                Number idof=(ii-1)/c+1, icompo=(ii-1)%c+1;
                A+=checkValue(R.getValue(ii).asComplex(), VC(ii), tol, errorMsg, info+"("+tostring(ii)+")");
                ii++;
              }
            }
          }
        }
        else
        {
          Vector<Vector<Real> > VR(s,Vector<Real>(c));
          if (R.valueType() == _real)
          {
            while (!fin.eof())
            {
              while ( std::getline( fin, ligne ) )
              {
                std::istringstream is(ligne);
                for (Number k=0; k< c; ++k) { is >> VR(ii)(k+1); }
                A+=checkValues(R.getValue(ii).asRealVector(), VR(ii), tol, errorMsg, info+"("+tostring(ii)+")");
                ii++;
              }
            }
          }
          else if (R.valueType() == _complex)
          {
            Vector<Vector<Real> > VI(s,Vector<Real>(c));
            Vector<Vector<Complex> > VC(s,Vector<Complex>(c));
            while (!fin.eof())
            {
              while ( std::getline( fin, ligne ) )
              {
                std::istringstream is(ligne);
                for (Number k=0; k< c; ++k)
                {
                  is >> VR(ii)(k+1) >> VI(ii)(k+1);
                  VC(ii)(k+1)= VR(ii)(k+1) + i_*VI(ii)(k+1);
                }
                A+=checkValues(R.getValue(ii).asComplexVector(), VC(ii), tol, errorMsg, info+"("+tostring(ii)+")");
                ii++;
              }
            }
          }
        }
      }
      Number  n=ii-1;
      if (n != d)
      {
        A+=1;
        errorMsg += info + ": " + message("bad_dim", d, n) + "\n";
      }
    }
    else
    {
      // multi unknown TermVector
      // not handled
    }

    return A;
  }
  else
  {
    // saveToFile routine for a TermVector should be updated before activation here
    TermVector RR(R);
    RR.roundToZero(1.0e-13);
    RR.saveToFile(inputsPathTo(refFilename), testPrec);
    clear(RR);
    return 0;
  }
}

//! Test a TermMatrix with a reference file with tolerance
Number checkValues(const TermMatrix& M, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=M.numberOfRows();
  Number nbc=M.numberOfCols();
  LargeMatrix<Real>   MrLM;
  LargeMatrix<Complex>   McLM;
  Real  val, valR, valI;
  if (M.valueType() == _real) { MrLM = M.getLargeMatrix<Real>(); }
  else if (M.valueType() == xlifepp::_complex) { McLM = M.getLargeMatrix<Complex>(); }
  if (check)
  {
    thePrintStream << " check TermMatrix " << M.name() <<"./ "<< refFilename << " with tolerance "<<tol<<":"  <<eol<<info<<eol;
    String ligne;
    if (M.valueType() == _real)
    {
      A=checkValues(MrLM, refFilename, tol, errorMsg, info, check);
    }
    if (M.valueType() == _complex)
    {
      A=checkValues(McLM, refFilename, tol, errorMsg, info, check);
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "Edit TermMatrix " << M.name() <<": " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (M.valueType() == _real)
    {
      checkValues(MrLM, refFilename, tol, errorMsg, info, check);
    }
    else if (M.valueType() == _complex)
    {
      checkValues(McLM, refFilename, tol, errorMsg, info, check);
    }
    else { std::cout << " unknown type:" << M.valueType() << eol; }
    fout.close();
    return 0;
  }
}

//! Check Eigen value associated at EigenElements of a TermMatrix with tolerance
Number checkValues(const TermMatrix& TM, const  EigenElements& EV, Real tol, String& errorMsg, const String& info, bool check)
{
  thePrintStream << "Check Eigen value of a TermMatrix with tolerance:"<<tol<<eol<<info<<eol;
  Number A=0;
  Number  nbEV=EV.numberOfEigenValues();
  TermVector VEA;
  for (Int i=1; i<=nbEV; i++)
  {
    VEA = TM*EV.vector(i)- EV.value(i)*EV.vector(i);
    if ( norminfty(VEA) > tol)
    {
      A++;
      errorMsg += info + ": " + message("bad_eigen_value_with_tolerance_for_associated_eigenvector",  EV.value(i), i, tol) + "\n";
    }
  }
  return A;
}

//! Test a LargeMatrix with a reference file with tolerance
Number checkValues(const LargeMatrix<Real>& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  Real  val;
  // Real tol=1.0e-9;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << " check LargeMatrix<Real> " << LM.name  <<" / "<<refFilename<< " with tolerance "<<tol<< eol << info << eol;;
    if (fin)
    {
      String ligne;
      i=1; j=1;
      while (fin >> val)
      {
        A+=checkValue(LM(i,j), val, tol, errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
        if (j==nbc) {i++;j=1;}
        else if (j<nbc) {j++;}
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << " edit LargeMatrix " << LM.name ;
    thePrintStream << ": Reference file non check" << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    if (isTestMode)
    {
      for (int i =1; i<=nbr; i++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          fout << roundToZero(LM(i,j), tol)<< " ";
          thePrintStream << roundToZero(LM(i,j), tol)<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
     for (int i =1; i<=nbr; i++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          fout << roundToZero(LM(i,j), tol)<< " ";
          thePrintStream <<LM(i,j)<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const LargeMatrix<Complex>& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  Real  valR, valI;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << "check LargeMatrix<Complex> " << LM.name <<" / " <<refFilename<<" with tolerance "<< tol<< eol<<info<<eol;
    if (fin)
    {
      String ligne;
      i=1; j=1;
      while (fin >> valR)
      {
        A+=checkValue(realPart(LM(i,j)), valR , tol, errorMsg, info+" Re("+tostring(i)+","+tostring(j)+")");
        fin>>valI;
        A+=checkValue(imagPart(LM(i,j)), valI , tol, errorMsg, info+" Im("+tostring(i)+","+tostring(j)+")");

        if (j==nbc) {i++;j=1;}
        else if (j<nbc) {j++;}
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "edit LargeMatrix " << LM.name ;
    thePrintStream << ": Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    if (isTestMode)
    {
      for (int i =1; i<=nbr; i++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          fout << roundToZero(realPart(LM(i,j)), tol)<< " "<< roundToZero(imagPart(LM(i,j)), tol)<< " ";
          thePrintStream << realPart(LM(i,j))<< " +i*"<< imagPart(LM(i,j))<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i =1; i<=nbr; i++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          fout << realPart(LM(i,j))<< " "<< imagPart(LM(i,j))<< " ";
          thePrintStream << realPart(LM(i,j))<< " +i*"<< imagPart(LM(i,j))<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const LargeMatrix<Matrix<Real> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{

  Number A = 0, i, j, k, l;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  Number nbrr=LM(1,1).vsize(), nbcc=LM(1,1).hsize();
  Real  val;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << " check LargeMatrix<Matrix<Real> >" << LM.name  << " / " <<refFilename<< " with tolerance "<<eol<<eol<<info<<eol;
    if (fin)
    {
      String ligne;
      i=1; j=1, l=1, k=1;
      while (fin >> val)
      {
        A+=checkValue(LM(i,j)(l,k), val, tol, errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
        if (k < nbcc) {k++;}
        else if (k == nbcc )
        {
          if (l < nbrr)
          {
            if (j<nbc) {j++; k=1;}
            else if (j==nbc) {j=1; l++;k=1;}
          }
          else if (l==nbrr)
          {
            if (j<nbc) {j++; k=1;}
            else if (j==nbc) {j=1; i++; l=1; k=1;}
          }
        }
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << " edit LargeMatrix " << LM.name <<":" ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int i =1; i<=nbr; i++ )
    {
      for (int l =1; l<=nbrr; l++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          for (int k=1; k<=nbcc; k++ )
          {
            fout << roundToZero(LM(i,j)(l,k), tol)<< " ";
            thePrintStream << LM(i,j)(l,k)<< " ";
          }
        }
        fout << eol;
        thePrintStream<<eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const LargeMatrix<Matrix<Complex> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j, k, l;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  Number nbrr=LM(1,1).vsize(), nbcc=LM(1,1).hsize();
  Real  valR, valI;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << "check LargeMatrix<Matrix<Complex> > " << LM.name <<" / " <<refFilename<<" with tolerance "<<tol<<eol<<info<< eol;
    if (fin)
    {
      String ligne;
      i=1; j=1, l=1, k=1;

      while (fin >> valR)
      {
        A+=checkValue(realPart(LM(i,j)(l,k)), valR , tol, errorMsg, info+" Re("+tostring(i)+","+tostring(j)+")");
        fin>>valI;
        A+=checkValue(imagPart(LM(i,j)(l,k)), valI , tol, errorMsg, info+" Im("+tostring(i)+","+tostring(j)+")");

        if (k < nbcc) {k++;}
        else if (k == nbcc )
        {
          if (l < nbrr)
          {
            if (j<nbc) {j++; k=1;}
            else if (j==nbc) {j=1; l++;k=1;}
          }
          else if (l==nbrr)
          {
            if (j<nbc) {j++; k=1;}
            else if (j==nbc) {j=1; i++; l=1; k=1;}
          }
        }
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "edit LargeMatrix " << LM.name <<":" ;
    thePrintStream << " Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int i =1; i<=nbr; i++ )
    {
      for (int l =1; l<=nbrr; l++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          for (int k=1; k<=nbcc; k++ )
          {
            fout << roundToZero(realPart(LM(i,j)(l,k)), tol)<< " "<< roundToZero(imagPart(LM(i,j)(l,k)), tol)<< " ";
            thePrintStream << realPart(LM(i,j)(l,k))<< " "<< imagPart(LM(i,j)(l,k))<< " ";
          }
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

//! Test a LargeMatrix with a diagonale matrix with tolerance
Number checkValues(const LargeMatrix<Real>& LM, Real rV, Real tol, String& errorMsg, const String& info, bool check)
{
  thePrintStream << "check LargeMatrix<Matrix<Real> > " << LM.name <<" / Real with tolerance "<<tol<<eol<<info<< eol;
  Number A = 0, i, j;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  if (check)
  {
    for (Number i=1; i <= nbr; ++i)
    {
      for (Number j=1; j <= nbc; ++j)
      {
        if ( (i ==j) && (LM(i,j) ) != rV ) { A++; }
        else if ( (i != j) && (LM(i,j) ) != 0. )
         { A++;
           errorMsg += "@@ :-( ERROR  \n" + info + ", pb with coefficients:" + tostring(i) + ","+ tostring(j) + " with tol=" + tostring(tol) +"\n";
         }
      }
    }
    return A;
  }
  else
  {
    return 0;
  }
}

Number checkValues(const LargeMatrix<Complex>& LM, Complex cV, Real tol, String& errorMsg, const String& info, bool check)
{
  thePrintStream << "check LargeMatrix<Matrix<Complex> > " << LM.name <<" / " <<cV<<" with tolerance "<<tol<<eol<<info<< eol;
  Number A = 0, i, j;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  if (check)
  {
    for (Number i=1; i <= nbr; ++i)
    {
      for (Number j=1; j <= nbc; ++j)
      {
        if ( (i ==j) && (LM(i,j) ) != cV ) { A++; }
        else if ( (i != j) && (LM(i,j) ) != Complex (0.,0.) )
          {
            A++;
            errorMsg += "@@ :-( ERROR  \n" + info + ", pb with coefficients:" + tostring(i) + ","+ tostring(j) + " with tol=" + tostring(tol) +"\n";
          }
      }
    }
    return A;
  }
  else
  {
    return 0;
  }
}

//! Test a Polynomial[s]Basis with a reference file with tolerance
Number checkValues(const PolynomialBasis& PB, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  thePrintStream << "check PolynomialBasis "<<PB.name<<" / " <<refFilename<<" with tolerance "<<tol<<eol<<info<< eol;
  Number A = 0, i=0;
  if (check)
  {
    String ftmpp= "polynomialBasis.txt";
    std::stringstream ftmp(ftmpp.c_str());
    PB.print(ftmp);
    A+=checkValue(ftmp, refFilename, errorMsg, info, check);
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "edit PolynomialBasis " << PB.name <<": " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    PB.print(fout);
    thePrintStream<< PB << eol;
    return 0;
  }
}

Number checkValues(const PolynomialsBasis& PB,  const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i=0;
  if (check)
  {
    String ftmpp= "polynomialsBasis.txt";
    std::stringstream ftmp(ftmpp.c_str());
    PB.print(ftmp);
    A+=checkValue(ftmp, refFilename, errorMsg, info, check);
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << " edit PolynomialsBasis " << PB.name <<":" << eol;
    thePrintStream << " Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    PB.print(fout);
    thePrintStream << PB << eol;
    return 0;
  }
}

//! Test MultiVecAdapter with a reference file with tolerance
Number checkValues(const MultiVecAdapter<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check MultiVecAdapter<Real> > / " << refFilename <<" with tolerance: "<<tol<< eol<<info<<eol;
    Number A=0;
    Real  ValR, ValI;
    Number   L;
    Dimen    nbV;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      fin >> L ;
      if (L != R.getVecLength() )
      {
        A++;
        errorMsg += info + "bad length : " + message("bad_integer_value", L, R.getVecLength()) + "\n";
      }
      fin >> nbV ;
      if (nbV != R.getNumberVecs() )
      {
        A++;
        errorMsg += info + "bad Number Vectors : " + message("bad_integer_value", nbV, R.getNumberVecs()) + "\n";
      }
      while (fin >> ValR)
      {
        A+=checkValue(realPart(R(ii,jj)), ValR , tol, errorMsg, info+"V["+tostring(ii)+"]["+tostring(jj)+"]");
        jj++;
        if (jj==nbV) { ii++;jj=0;}
      }
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit MultiVecAdapter<Real>." << inputsPathTo(refFilename) <<eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    Number   L;
    Dimen    nbV;
    fout <<  R.getVecLength() << " "<<R.getNumberVecs() <<  eol;
    thePrintStream <<  R.getVecLength() << " "<<R.getNumberVecs() <<  eol;
    if (isTestMode)
    {
      for (int i=0; i<=R.getVecLength()-1; i++ )
      {
        for (int j=0; j<=R.getNumberVecs()-1; j++ )
        {
          fout << roundToZero(R(i,j),tol)<< " ";
          thePrintStream << R(i,j)<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
   else
   {
      for (int i=0; i<=R.getVecLength()-1; i++ )
      {
        for (int j=0; j<=R.getNumberVecs()-1; j++ )
        {
          fout << R(i,j)<< " ";
          thePrintStream << R(i,j)<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
    thePrintStream << "Reference file non check " << inputsPathTo(refFilename)  << " updated" << eol;
}

Number checkValues(const MultiVecAdapter<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<Vector<Complex> > / " << refFilename <<" with tolerance:"<<tol<<eol<<info<< eol;
    Number A=0;
    Real  ValR, ValI;
    Number   L;
    Dimen    nbV;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      fin >> L ;
      if (L != R.getVecLength() )
      {
        A++;
        errorMsg += info + "bad length : " + message("bad_integer_value", L, R.getVecLength()) + "\n";
      }
      fin >> nbV ;
      if (nbV != R.getNumberVecs() )
      {
        A++;
        errorMsg += info + "bad Number Vectors : " + message("bad_integer_value", nbV, R.getNumberVecs()) + "\n";
      }
      while (fin >> ValR)
      {
        A+=checkValue(realPart(R(ii,jj)), ValR , tol, errorMsg, info+"ReV["+tostring(ii)+"]["+tostring(jj)+"]");
        fin >> ValI;
        A+=checkValue(imagPart(R(ii,jj)), ValI , tol, errorMsg, info+"ImV["+tostring(ii)+"]["+tostring(jj)+"]");
        jj++;
        if (jj==nbV) { ii++;jj=0;}
      }
    }
    return A;
  }
  else
  {
    thePrintStream << "Edit MultiVecAdapter<Complex>:" << inputsPathTo(refFilename).c_str();
    thePrintStream << ", Reference file non check " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    Number   L;
    Dimen    nbV;
    fout <<  R.getVecLength() << " "<<R.getNumberVecs() <<  eol;
    if (isTestMode)
    {
      for (int i=0; i<=R.getVecLength()-1; i++ )
      {
        for (int j=0; j<=R.getNumberVecs()-1; j++ )
        {
          fout << roundToZero(realPart(R(i,j)), tol)<< " " << roundToZero(imagPart(R(i,j)), tol)<< " ";
          thePrintStream << realPart(R(i,j))<< " =i*" << imagPart(R(i,j))<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i=0; i<=R.getVecLength()-1; i++ )
      {
        for (int j=0; j<=R.getNumberVecs()-1; j++ )
        {
          fout << realPart(R(i,j))<< " " << imagPart(R(i,j))<< " ";
          thePrintStream << realPart(R(i,j))<< " =i*" << imagPart(R(i,j))<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

//! Test a MatrixEigenDense with a reference file with tolerance
Number checkValues(const MatrixEigenDense<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check MatrixEigenDense<Real>  / " << refFilename <<" with tolerance: "<<tol<<eol<<info<< eol;
    Number A=0;
    Real  valR,valI;
    Number p=R.numOfRows()-1, q=R.numOfCols()-1;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      String ligne;
      while (fin >> valR)
      {
        A+=checkValue(R.coeff(ii,jj), valR , tol, errorMsg, info+" R["+tostring(ii)+"]["+tostring(jj)+"]");
        if (jj==q)
        {
          jj=0;
          ii++;
        }
        else {jj++;}
      }
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit std::vector<MatrixEigenDense<Real> >: " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    Number p=R.numOfRows(), q=R.numOfCols();
    String ligne;
    if (isTestMode)
    {
      for (int i=0; i<p; i++ )
      {
        for (int j=0; j<q; j++ )
        {
          fout << roundToZero(R.coeff(i,j), tol)<< " ";
          thePrintStream << R.coeff(i,j)<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i=0; i<p; i++ )
      {
        for (int j=0; j<q; j++ )
        {
          fout << R.coeff(i,j)<< " ";
          thePrintStream << R.coeff(i,j)<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkValues(const MatrixEigenDense<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check MatrixEigenDense<Complex>  / " << refFilename <<" with tolerance: "<<tol<<info<< eol;
    Number A=0;
    Real  valR,valI;
    Number p=R.numOfRows()-1, q=R.numOfCols()-1;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0, jj=0;
    if (fin)
    {
      String ligne;
      while (fin >> valR)
      {
        A+=checkValue(realPart(R.coeff(ii,jj)), valR , tol, errorMsg, info+"Re R["+tostring(ii)+"["+tostring(jj)+"]");
        fin >> valI;
        A+=checkValue(imagPart(R.coeff(ii,jj)), valI , tol, errorMsg, info+"Im R["+tostring(ii)+"["+tostring(jj)+"]");
        if (jj==q)
        {
          jj=0;
          ii++;
        }
        else {jj++;}
      }
    }
  return A;
  }
  else
  {
    thePrintStream << "Edit MatrixEigenDense<Complex> : " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    Number p=R.numOfRows(), q=R.numOfCols();
    String ligne;
    if (isTestMode)
    {
      for (int i=0; i<p; i++ )
      {
        for (int j=0; j<q; j++ )
        {
          fout << roundToZero(realPart(R.coeff(i,j)), tol)<< " "<<roundToZero(imagPart(R.coeff(i,j)), tol)<< " ";
          thePrintStream << realPart(R.coeff(i,j))<< " "<<imagPart(R.coeff(i,j))<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    else
    {
      for (int i=0; i<p; i++ )
      {
        for (int j=0; j<q; j++ )
        {
          fout << realPart(R.coeff(i,j))<< " "<<imagPart(R.coeff(i,j))<< " ";
          thePrintStream << realPart(R.coeff(i,j))<< " "<<imagPart(R.coeff(i,j))<< " ";
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

//! Testa LowRankMatrix with a reference file wit tolerance
Number checkValues(const LowRankMatrix<Real>& LRM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=LRM.numberOfRows();
  Number nbc=LRM.numberOfCols();
  Real  val, valR, valI;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << " check LowRankMatrix " << nbr<<"x" <<nbc<<" / "<<refFilename <<" with tolerance: "<< tol<<eol<<info<< eol;
    i=1; j=1;
    while (fin >> val)
    {
      A+=checkValue(LRM(i,j), val, tol, errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
      if (j==nbc) {j=1; i++;}
      else {j++;}
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << " edit LowRankMatrix ";
    thePrintStream << " Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    for (int i =1; i<=nbr; i++ )
    {
      for (int j=1; j<=nbc; j++ )
      {
        fout << roundToZero(LRM(i,j), tol)<< " ";
        thePrintStream << LRM(i,j)<< " ";
      }
      fout << eol;
      thePrintStream << eol;
    }
    return 0;
  }
}

//! Test different kind of string
Number checkValue(const Strings& SI, const Strings& SR, String& errorMsg, const String& info, bool printData)
{
  thePrintStream<<"checkValue(const Strings& SI, const Strings& SR, String& errorMsg, const String& info)"<<eol;
  Number A=0;
  Number nsi=SI.size();
  Number nsr=SR.size();
  Strings VVSI, VVSR, tmp;
  Number nnsi, nnsr;
  Number nCharMax=6;
  String ligneI;
  thePrintStream << info << eol;
  if (nsi != nsr)
  {
    A=1;
    errorMsg += info + ": " + message("test_bad_number_of_lines", nsi, nsr) + "\n";
    if (printData) thePrintStream << message("test_bad_number_of_lines", nsi, nsr) << eol;
  }
  else
  {
    for (Number i=0; i < nsi; ++i)
    {
      thePrintStream << SI[i] << std::endl;
      VVSI.clear();
      VVSR.clear();
      tmp=split(SI[i], ' ');
      for (Number j=0; j < tmp.size(); ++j)
      {
        if (tmp[j] != "")
        {
          roundToZero(tmp[j], ligneI);
          VVSI.push_back(ligneI);
        }
      }
      tmp=split(SR[i], ' ');
      for (Number j=0; j < tmp.size(); ++j)
      {
        if (tmp[j] != "")
        {
          roundToZero(tmp[j], ligneI);
          VVSR.push_back(ligneI);
        }
      }
      nnsi=VVSI.size();
      nnsr=VVSR.size();
      if (nnsi != nnsr)
      {
        A += std::abs(Int(nnsr)-Int(nnsi));
        errorMsg += info + ":\n";
        errorMsg += "- line " + tostring(i+1) + " -> " + message("test_bad_number_of_words", nnsi, nnsr) + "\n";
        if (printData) thePrintStream << message("test_bad_number_of_words", nnsi, nnsr) << std::endl;
      }
      else
      {
        for (Number j=0; j < nnsi; ++j)
        {
          if ( ( VVSR[j].size() < nCharMax ) && (VVSI[j].size() < nCharMax ) )
          {
            if (VVSI[j] != VVSR[j])
            {
              A++;
              if (A>0) { errorMsg += info + ":\n";}
              errorMsg += "- line " + tostring(i+1) + " -> " + message("bad_string_value", VVSI[j], VVSR[j]) + "\n";
              if (printData) thePrintStream << message("bad_string_value", VVSI[j], VVSR[j]) << std::endl;
            }
          }
          else
          {
            for (Number jj=0; jj < nCharMax; ++jj)
            {
              if (VVSI[j][jj] != VVSR[j][jj])
              {
                A++;
                if (A>0) { errorMsg += info + ":\n";  }
                errorMsg += "- line " + tostring(i+1) + " -> " + message("bad_string_value", VVSI[j], VVSR[j]) + "\n";
                if (printData) thePrintStream << message("bad_string_value", VVSI[j], VVSR[j]) << std::endl;
              }
            }
          }
        }
      }
    }
  }
  return A;
}

Number checkValue(const String& SI, const String& SR, String& errorMsg, const String& info)
{
  Strings VSI= split(SI, '\n');
  Strings VSR= split(SR, '\n');
  thePrintStream.currentStream() << VSI << eol;
  return checkValue(VSI, VSR, errorMsg, info);
}

Number checkValues(const std::vector<String>& VS, const String& refFilename, String& errorMsg, const String& info, bool check)
{
  if (check)
  {
    thePrintStream << "Check std::vector<Vector<String> > / " << refFilename <<eol;
    Number A=0;
    String  Val;
    Number p=VS.size();
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    int ii=0;
    String ligne;
    Strings VVSI;
    String  VV="";
    int nnsi;
    if (fin)
    {
      while (!fin.eof() && ii<p)
      {
        VVSI= split(VS[ii], '\n');
        nnsi=VVSI.size();
        if (nnsi > 1 )
        {
          for (Number j=0; j < nnsi; ++j)
          {
            std::getline(fin, ligne);
            if (VVSI[j]!="") { A+=checkValue(VVSI[j], ligne , errorMsg, info+"VS["+tostring(ii)+"]"); }
          }
        }
        else
        {
          std::getline( fin, ligne );
          A+=checkValue(VS[ii], ligne , errorMsg, info+"VS["+tostring(ii)+"]");
        }
        ii++;
      }
    }
    return A;
  }
  else
  {
    thePrintStream << ".Edit std::vector<Strings >: " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    Strings SVS;
    for (int i=0; i<=VS.size()-1; i++ )
    {
      fout << VS[i]<<eol;
      thePrintStream << VS[i]<<eol;
    }
    fout.close();
    return 0;
  }
}

Number checkValue(std::stringstream& ssi, const String& refFilename, String& errorMsg, const String& info, bool check, bool printData)
{
  if (check)
  {
    thePrintStream << "Check std::stringstream / " << refFilename <<eol<<info<<eol;
    // we compare the string stream to the content of a reference file (default behavior)
    Strings VSI= split(ssi.str(), '\n');
    std::stringstream ssref;
    ssref.precision(testPrec);
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    ssref << fin.rdbuf();
    fin.close();
    Strings VSR= split(ssref.str(), '\n');
    ssi.str(""); ssi.clear();
    Number n = checkValue(VSI, VSR, errorMsg, info, printData);
    // thePrintStream.currentStream() << VSI << eol;
    return n;
  }
  else
  {
    // we update the reference file
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());

    if (printData)
    {
      thePrintStream << ssi.str();
    }
    fout << ssi.str();

    fout.close();
    ssi.str(""); ssi.clear();
    return 0;
  }
}

Number checkValues(const String& filename, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  std::ifstream finI(filename.c_str());
  std::stringstream ssi;
  String ligneI;
  Strings VSI;
  Real aszero=1.0e-8;
  if (finI)
  {
    if(!isTestMode)
    {
      while (getline(finI, ligneI) )
      {
        ssi << ligneI << std::endl;
      }
    }
    else
    {
      int nsi;
      while (getline(finI, ligneI) )
      {
        ssi << ligneI << std::endl;
      }
    }
  }
  finI.close();

  return checkValue(ssi, refFilename, errorMsg, info, check, false);
}


//! Test visualization of matrix with a reference file
Number checkVisual(const TermMatrix& TM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  thePrintStream << "check TermMatrix visual / " << refFilename<<eol<<info<<eol;
  Number A = 0;
  if (TM.valueType() == xlifepp::_real)
  {
    A+=checkVisual(TM.getLargeMatrix<Real>(), refFilename, tol, errorMsg, info, check);
  }
  else if (TM.valueType() == xlifepp::_complex)
  {
    A+=checkVisual(TM.getLargeMatrix<Complex>(), refFilename, tol, errorMsg, info, check);
  }
  return A;
}

Number checkVisual(const MatrixStorage& MS, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A=0,i,j;
  Number nbr=MS.nbOfRows();
  Number nbc=MS.nbOfColumns();
  String S,val;
  if (check)
  {
    thePrintStream << "check MatrixStorage  visual / " << refFilename<<eol<<info<<eol;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    if (fin)
    {
      i=1,j=1;
      while ( fin >> val )
      {
        for (j=1; j<=nbc; j++)
        {
          if (i==j)
          {
            S ='d';
          }
          else
          {
            if (MS.pos(i,j) != 0.)
            { S='x'; }
            else
            { S='.'; }
          }
          if ( (i >= j) || (MS.accessType() != _sym) )
          {
            A+=checkValue(S, tostring(val[j-1]), errorMsg, info+"("+tostring(i)+",,"+tostring(j)+")");
          }
          else
          { }
        }
        i++;
      }
    }
    return A;
  }
  else
  {
    std::cout << "Edit visual" << refFilename.c_str() <<  eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    Number rmax = std::min(nbr, Number(10 * theVerboseLevel)),
             cmax = std::min(nbc, Number(10 * theVerboseLevel + 5));
    for (Number r = 1; r <= rmax; r++)
    {
      String str(cmax, '.');
      for (Number c = 1; c <= cmax; c++)
      {
        Number p = MS.pos(r, c);
        if (p != 0  && (MS.accessType() != _sym || (MS.accessType() == _sym && r > c)))
        {
          str.at(c - 1) = 'x';
        }
        if (r == c)
        {
          str.at(c - 1) = 'd';
        }
      }
      fout << str<< std::endl;
      thePrintStream << str<< std::endl;
      if (cmax < nbc)
      {
        fout << " ...(continued)";
        thePrintStream << " ...(continued)";
      }
    }
    fout << std::endl;
    thePrintStream << std::endl;
    return 0;
  }
}

Number checkVisual(const LargeMatrix<Real>& LM,  const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  String  val;
  String S;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << " check LargeMatrix<Real> Visual " << LM.name <<" / "<<refFilename<<eol<<info<<eol;
    if (fin)
    {
      String ligne;
      i=1; j=1;
      while (fin >> val)
      {
        for (j=1; j<=nbc; j++)
        {
          if (i==j)
          {
            S='d';
          }
          else
          {
            if (LM(i,j) != 0.)
            { S='x'; }
            else
            { S='.'; }
          }
          A+=checkValue(S, tostring(val[j-1]), errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
        }
        i++;
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << " edit LargeMatrix visual" << LM.name <<":" ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int i =1; i<=nbr; i++ )
    {
      for (int j=1; j<=nbc; j++ )
      {
        if (i==j)
        {
          fout <<'d';
        }
        else
        {
          if (LM(i,j) != 0.)
          { fout<<'x'; thePrintStream<<'x';}
          else
          { fout<<'.'; thePrintStream<<'.';}
        }
      }
      fout << eol;
    }
    fout.close();
    return 0;
  }
}

Number checkVisual(const LargeMatrix<Complex>& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  String  val;
  String S;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << " check LargeMatrix<Complex> Visual " << LM.name <<" / "<<refFilename<<eol<<info<<eol;
    if (fin)
    {
      String ligne;
      i=1; j=1;
      while (fin >> val)
      {
        for (j=1; j<=nbc; j++)
        {
          if (i==j)
          {
            S='d';
          }
          else
          {
            if (std::abs(LM(i,j)) != 0.)
            { S='x'; }
            else
            { S='.'; }
          }
          A+=checkValue(S, tostring(val[j-1]), errorMsg, info+"("+tostring(i)+","+tostring(j)+")");
       }
       i++;
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "Edit LargeMatrix " << LM.name <<"- " << eol;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int i =1; i<=nbr; i++ )
    {
      for (int j=1; j<=nbc; j++ )
      {
        if (i==j)
        {
          fout <<'d';
          thePrintStream <<'d';
        }
        else
        {
          if (std::abs(LM(i,j)) != 0.)
          { fout<<'x';thePrintStream<<'x'; }
          else
          { fout<<'.'; thePrintStream<<'.'; }
        }
      }
      fout << eol;
      thePrintStream << eol;
    }
    fout.close();
    return 0;
  }
}

Number checkVisual(const LargeMatrix<Matrix<Real> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check )
{
  Number A = 0, i, j, k, l;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  Number nbrr=LM(1,1).vsize(), nbcc=LM(1,1).hsize();
  String val, S;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  if (check)
  {
    thePrintStream << " check LargeMatrix<Matrix<Real>> Visual " << LM.name <<" / "<<refFilename<<eol<<info<<eol;
    if (fin)
    {
      i=1; j=1, l=1, k=1;
      while (fin >> val)
      {
        for (int t=0;t<val.size(); ++t )
        {
          if ( (i==j) && (l==k) )
          {
            S +='d';
          }
          else
          {
            if (LM(i,j)(l,k)*LM(i,j)(l,k) != 0.)
            { S+='x'; }
            else
            { S+='.'; }
          }
          if (k < nbcc) {k++;}
          else if (k == nbcc )
          {
            if (l < nbrr)
            {
              if (j<nbc) {j++; k=1;}
              else if (j==nbc) {j=1; l++;k=1;}
            }
            else if (l==nbrr)
            {
              if (j<nbc) {j++; k=1;}
              else if (j==nbc) {j=1; i++; l=1; k=1;}
            }
          }
        }    // bcl t
        A+=checkValue(S, val, errorMsg, info+"ival("+tostring(i)+")");
        S="";
      }
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "edit LargeMatrix Visual " << LM.name ;
    thePrintStream << ", Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int i =1; i<=nbr; i++ )
    {
      for (int l =1; l<=nbrr; l++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          for (int k=1; k<=nbcc; k++ )
          {
            if ( (i==j) && (l==k) )
            {
              fout <<'d';
              thePrintStream <<'d';
            }
            else
            {
              if (LM(i,j)(l,k) != 0.)
              { fout<<'x'; thePrintStream<<'x'; }
              else
              { fout<<'.'; thePrintStream<<'.'; }
            }
          }
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

Number checkVisual(const LargeMatrix<Matrix<Complex> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{
  Number A = 0, i, j, k, l;
  Number nbr=LM.numberOfRows();
  Number nbc=LM.numberOfCols();
  Number nbrr=LM(1,1).vsize(), nbcc=LM(1,1).hsize();
  String  val, S;
  std::ifstream fin(inputsPathTo(refFilename).c_str());
  Number ival;
  if (check)
  {
    thePrintStream << "check LargeMatrix<Matrix<Complex> > visual" << LM.name << eol;
    if (fin)
    {
      String ligne;
      i=1; j=1, l=1, k=1, ival=0;;
      while (fin >> val)
      {
        for (int t=0;t<val.size(); ++t )
        {
          if ( (i==j) && (l==k) )
          {
            S +='d';
          }
          else
          {
            if (LM(i,j)(l,k)*LM(i,j)(l,k) != 0.)
            { S+='x'; }
            else
            { S+='.'; }
          }
          if (k < nbcc) {k++;}
          else if (k == nbcc )
          {
            if (l < nbrr)
            {
              if (j<nbc) {j++; k=1;}
              else if (j==nbc) {j=1; l++;k=1;}
            }
            else if (l==nbrr)
            {
              if (j<nbc) {j++; k=1;}
              else if (j==nbc) {j=1; i++; l=1; k=1;}
            }
          }
        }    // bcl t
        A+=checkValue(S, val, errorMsg, info+"ival("+tostring(i)+")");
        S="";
        ival++;
      }    //end while
    }
    return A;
  }
  else
  {
    // we update the reference file
    thePrintStream << "Edit LargeMatrix Visual " << LM.name <<": " ;
    thePrintStream << "Reference file " << refFilename << " updated" << eol;
    std::ofstream fout(inputsPathTo(refFilename).c_str());
    String ligne;
    for (int i =1; i<=nbr; i++ )
    {
      for (int l =1; l<=nbrr; l++ )
      {
        for (int j=1; j<=nbc; j++ )
        {
          for (int k=1; k<=nbcc; k++ )
          {
            if ( (i==j) &&  (l==k) )
            {
              fout <<'d';
              thePrintStream <<'d';
            }
            else
            {
              if (std::abs(LM(i,j)(l,k)) != 0.)
              {fout<<'x'; thePrintStream<<'x'; }
              else
              {fout<<'.'; thePrintStream<<'.'; }
            }
          }
        }
        fout << eol;
        thePrintStream << eol;
      }
    }
    fout.close();
    return 0;
  }
}

//! utilities for the incomplete factorization
void giveValue(const LargeMatrix<Real>& M0, LargeMatrix<Real>& M1)
{
  for (Number j = 1; j <= M0.nbRows ; j++)
  {
    for (Number i = j; i<= M0.nbRows; i++)
    {
      if ( M0.pos(i,j) != 0 ) {M1(i,j)=M0(i,j);}  //M1(j,i)=M1(i,j);
    }
  }
}

void giveValue(const LargeMatrix<Complex>& M0, LargeMatrix<Complex>& M1)
{
  for (Number j = 1; j <= M0.nbRows ; j++)
  {
    for (Number i = j; i<= M0.nbRows; i++)
    {
      if ( M0.pos(i,j) != 0 ) {M1(i,j)=M0(i,j);}  //M1(j,i)=M1(i,j);
    }
  }
}

void getPart(FactorizationType Part, LargeMatrix<Real>& MIF, LargeMatrix<Real>& MM)
{
  thePrintStream << "getPart(U, Lt, L*, D.Lt, D.L*) "<<Part<<" of LargeMatrix<Real> "<<MM.name<<eol;
  // Get Part U, Lt, L*, D.Lt, D.L* de MIF ->  MM
  Number nbLignes = MIF.nbRows;
  switch (Part)
  {
    case _ilu :
    case _illt :
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <=nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.) { MM(i,j)= MIF(i,j);}
        }
      }
      break;
    case _illstar :
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <= nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.) { MM(i,j)= conj(MIF(i,j)); }
        }
      }
      break;
    case _ildlt :
    {
      LargeMatrix<Real> Mat_DLT(MM);Mat_DLT *= 0.;
      LargeMatrix<Real> Mat_LT(MM); Mat_LT *= 0.;
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <= nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.)
          {
            if (i < j )
            {
              Mat_LT(i,j)= MIF(i,j);
              Mat_DLT(i,j)=0.;
            }
            else if (i==j)
            {
              Mat_DLT(i,j)=MIF(i,j);
              Mat_LT(i,j)=1;
            }
          }
        }
      }
      MM = Mat_DLT*Mat_LT;
      break;
    }
    case _ildlstar :
    {
      LargeMatrix<Real> Mat_Dstar(MM);Mat_Dstar *= 0.;
      LargeMatrix<Real> Mat_Lstar(MM); Mat_Lstar *= 0.;
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <= nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.)
          {
            if (i < j )
            {
              Mat_Lstar(i,j)= MIF(i,j);
              Mat_Dstar(i,j)=0.;
            }
            else if (i==j)
            {
              Mat_Dstar(i,j)=MIF(i,j);
              Mat_Lstar(i,j)=1;
            }
          }
        }
      }
      MM = Mat_Dstar*Mat_Lstar.toConj();
      break;
    }
    default :
      thePrintStream << "Probleme: type de factorisation non prevu" << std::endl;
  }
}

void getPart(FactorizationType Part, LargeMatrix<Complex>& MIF, LargeMatrix<Complex>& MM)
{
  //Get Part U, Lt, L*, D.Lt, D.L* de MIF ->  MM
  thePrintStream << "getPart(U, Lt, L*, D.Lt, D.L*) "<<Part<<" of LargeMatrix<Complex> "<<MM.name<<eol;
  Number nbLignes = MIF.nbRows;
  switch (Part)
  {
    case _ilu :
    case _illt :
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <=nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.) { MM(i,j)= MIF(i,j); }
        }
      }
      break;
    case _illstar :
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <= nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.) { MM(i,j)= conj(MIF(i,j)); }
        }
      }
      break;
    case _ildlt :
    {
      LargeMatrix<Complex> Mat_DLT(MM);Mat_DLT *= 0.;
      LargeMatrix<Complex> Mat_LT(MM); Mat_LT *= 0.;
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <= nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.)
          {
            if (i < j )
            {
              Mat_LT(i,j)= MIF(i,j);
              Mat_DLT(i,j)=0.;
            }
            else if (i==j)
            {
              Mat_DLT(i,j)=MIF(i,j);
              Mat_LT(i,j)=1;
            }
          }
        }
      }
      MM = Mat_DLT*Mat_LT;
      break;
    }
    case _ildlstar :
    {
      LargeMatrix<Complex> Mat_Dstar(MM);Mat_Dstar *= 0.;
      LargeMatrix<Complex> Mat_Lstar(MM); Mat_Lstar *= 0.;
      for (Number i=1; i <=nbLignes; i++)
      {
        for (Number j=i; j <= nbLignes; j++)
        {
          if (MIF.pos(i,j) != 0.)
          {
            if (i < j )
            {
              Mat_Lstar(i,j)= MIF(i,j);
              Mat_Dstar(i,j)=0.;
            }
            else if (i==j)
            {
              Mat_Dstar(i,j)=MIF(i,j);
              Mat_Lstar(i,j)=1;
            }
          }
        }
      }
      MM = Mat_Dstar*Mat_Lstar.toConj();
      break;
    }
    default :
      thePrintStream << "Probleme: type de factorisation non prevu" << std::endl;
  }
}

//! Test incomplete factorization of a TermMatrix or LargeMatrix
Number checkIncompleteFacto(FactorizationType Part, LargeMatrix<Real>& M0, LargeMatrix<Real>& MIF, Real eps, String& errorMsg, const String& info)
{
  thePrintStream << "Check incomplete Factorization of LargeMatrix<Real> "<<M0.name<<":"<<eol;
  Number OK =0;
  std::stringstream out;
  String Name;
  if (Part == _ilu) {Name="iLU";}
  if (Part == _illt) {Name="iLLt";}
  if (Part == _illstar) {Name="iLL*";}
  if (Part == _ildlt) {Name="iLDLt";}
  if (Part == _ildlstar) {Name="iLDL*";}
  MatrixStorage* StoTyp = M0.storagep();
  Number nbLignes = M0.numberOfRows();
  LargeMatrix<Real> TESTM(StoTyp, 0, _noSymmetry); //TESTM = M0;
  LargeMatrix<Real> Mat(TESTM);
  Mat*=0;//M0*0.;
  LargeMatrix<Real> Mat_L(TESTM);
  Mat_L*=0;
  for (Number i=1; i <=nbLignes; i++)
  {
    for (Number j=1; j <= i; j++)
    {
      if (MIF.pos(i,j) != 0.)
      {
        if (i > j) { Mat_L(i,j)= MIF(i,j);}
        else if (i==j)
        {
          if ( (Part == _illt) || (Part == _illstar) )  Mat_L(i,j) = MIF(i,j);
          else Mat_L(i,j)=1.;
        }
      }
    }
  }
  getPart( Part, MIF,  Mat);
  LargeMatrix<Real> MVerif=Mat_L*Mat;
  for (Number j = 1; j <= nbLignes ; j++)
  {
    for (Number i = 1; i <= nbLignes; i++)
    {
      if ( ( MVerif.pos(i,j) != 0 ) && ( M0.pos(i,j) != 0 ) )
      {
        switch (Part)
        {
          case _illt:
          case _ilu:
            if ( i >= j)
            {
              if   ( std::abs(M0(i,j) - MVerif(i,j) ) > eps )
              {
                out << "non valide" << std::endl;
                out << "car " << M0(i,j) << " \\neq " << MVerif(i,j);
                out << " pour L, M(" << i << "," << j << ")" << std::endl;
                OK=1;
              }
            }
            if ( i < j)
            {
              if   ( std::abs(M0(i,j) -MVerif(i,j)) > eps )
              {
                out << "ici" << std::abs(M0(i,j) -MVerif(i,j)) << std::endl;
                out << "non valide" << std::endl;
                out << "car " << M0(i,j) << " \\neq " << MVerif(i,j);
                out << " pour U, M(" << i << "," << j << ")" << std::endl;
                OK=1; //break;
              }
            }
            break;
          case _illstar:
          case _ildlstar:
            if ( i >= j)
            {
              if   ( std::abs(M0(i,j) - MVerif(i,j)) > eps )
              {
                out << "  i >= j " << std::endl;
                out << "non valide" << std::endl;
                out << "car " << M0(i,j) << " \\neq " << MVerif(i,j);
                out << " pour L, M(" << i << "," << j << ")" << std::endl;
                OK=1; //break;
              }
            }
            if (i < j)
            {
              if (std::abs( conj(M0(i,j)) -MVerif(i,j) ) > eps )
              {
                out << "  i < j " << std::endl;
                out << std::abs(conj(M0(j,i)) - MVerif(i,j)) << std::endl ;
                out << "non valide" << std::endl;
                out << "car " << conj(M0(i,j)) << " \\neq " << MVerif(i,j);
                out << " pour U, M(" << i << "," << j << ")" << std::endl;
                OK=1; //break;
              }
            }
            break;
          default:
            break;
        } // end switch
      }
    }
  }
  if ( OK == 1 )
  {
    errorMsg += "@@ :-( ERROR  \n" + info + ", bad incomplete factorization " + " with tol=" + tostring(eps) +"\n";
  }
  else
  {
    thePrintStream << info << " Incomplete factorization  OK"  << " with tol=" << eps << eol;
  }
  return OK;
}

Number checkIncompleteFacto(FactorizationType Part, LargeMatrix<Complex>& M0, LargeMatrix<Complex>& MIF, Real eps, String& errorMsg, const String& info)
{
  thePrintStream << "Check incomplete Factorization of LargeMatrix<Complex> "<<M0.name<<":"<<eol;
  Number OK = 0;
  std::stringstream out;
  String Name;
  if (Part == _ilu) {Name="iLU";}
  if (Part == _illt) {Name="iLLt";}
  if (Part == _illstar) {Name="iLL*";}
  if (Part == _ildlt) {Name="iLDLt";}
  if (Part == _ildlstar) {Name="iLDL*";}
  MatrixStorage* StoTyp = M0.storagep();
  Number nbLignes = M0.numberOfRows();
  LargeMatrix<Complex> TESTM(StoTyp, 0, _noSymmetry); //TESTM = M0;
  LargeMatrix<Complex> Mat(TESTM);
  Mat*=0;//M0*0.;
  LargeMatrix<Complex> Mat_L(TESTM);
  Mat_L*=0;
  for (Number i=1; i <=nbLignes; i++)
  {
    for (Number j=1; j <= i; j++)
    {
      if (MIF.pos(i,j) != 0.)
      {
        if (i > j ) { Mat_L(i,j)= MIF(i,j);}
        else if (i==j)
        {
          if ( (Part == _illt) || (Part == _illstar) )  Mat_L(i,j) = MIF(i,j);
          else {Mat_L(i,j)=1.;}
        }
      }
    }
  }
  getPart(Part, MIF,  Mat);
  LargeMatrix<Complex> MVerif=Mat_L*Mat;
  for (Number j = 1; j <= nbLignes ; j++)
  {
    for (Number i = 1; i <= nbLignes; i++)
    {
      if ( ( MVerif.pos(i,j) != 0 ) && ( M0.pos(i,j) != 0 ) )
      {
        switch (Part)
        {
          case _illt:
          case _ilu:
            if ( i >= j)
            {
              if ( std::abs(M0(i,j) - MVerif(i,j)) > eps )
              {
                out << "non valide" << std::endl;
                out << "car " << M0(i,j) << " \\neq " << MVerif(i,j);
                out << " pour L, M(" << i << "," << j << ")" << std::endl;
                OK=1;
              }
            }
            if ( i < j)
            {
              if   ( std::abs(M0(j,i) -MVerif(i,j)) > eps )
              {
                out << "non valide" << std::endl;
                out << "car " << M0(i,j) << " \\neq " << MVerif(i,j);
                out << " pour U, M(" << i << "," << j << ")" << std::endl;
                OK=1;
              }
            }
            break;
          case _illstar:
          case _ildlstar:
            if ( i >= j)
            {
              if   ( std::abs(M0(i,j) - MVerif(i,j)) > eps )
              {
                out << "  i >= j " << std::endl;
                out << "non valide" << std::endl;
                out << "car " << M0(i,j) << " \\neq " << MVerif(i,j);
                out << " pour L, M(" << i << "," << j << ")" << std::endl;
                OK=1; //break;
              }
            }
            if ( i < j)
            {
              if ( std::abs( conj(M0(i,j)) -MVerif(i,j) ) > eps )
              {
                out << "  i < j " << std::endl;
                out << std::abs(conj(M0(j,i)) - MVerif(i,j)) << std::endl ;
                out << "non valide" << std::endl;
                out << "car " << conj(M0(i,j)) << " \\neq " << MVerif(i,j);
                out << " pour U, M(" << i << "," << j << ")" << std::endl;
                OK=1; //break;
              }
            }
            break;
          default:
            break;
        }
      }
    }
  }
  if ( OK == 1 )
  {
    errorMsg += "@@ :-( ERROR  \n" + info + ", bad incomplete factorization " + " with tol=" + tostring(eps) +"\n";
  }
  else
  {
    thePrintStream << info << ", Incomplete factorization  OK"  << " with tol=" << eps << eol;
  }
  return OK;
}

Number checkIncompleteFacto(FactorizationType Part, TermMatrix& H0, TermMatrix& HIF, Real eps, String& errorMsg, const String& info)
{
  thePrintStream << "Check incomplete Factorization of a TermMatrix "<<eol;
  Number OK = 1;
  if (H0.valueType() == _real)
  {
    LargeMatrix<Real> H0LM; H0LM = H0.getLargeMatrix<Real>();
    LargeMatrix<Real> HIFLM = HIF.getLargeMatrix<Real>();
    OK=checkIncompleteFacto(Part, H0LM, HIFLM, eps, errorMsg, info);
  }
  else if (H0.valueType() == xlifepp::_complex)
  {
    LargeMatrix<Complex> H0LM; H0LM = H0.getLargeMatrix<Complex>();
    LargeMatrix<Complex> HIFLM = HIF.getLargeMatrix<Complex>();
    OK = checkIncompleteFacto(Part, H0LM, HIFLM, eps, errorMsg, info);
  }
  return OK;
}

} // end of namespace xlifepp
