
/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
    \file sys_OpenMP.cpp
    \author Manh Ha NGUYEN
    \since 07 October 2013
    \date 07 October 2013

    Low level tests of functions paralleled with OpenMP.
    In this file, there are some tests to do:
    1. Matrix Vector multiplication : AX=B
    Almost functionalities are checked.
    This function may either creates a reference file storing the results (check=false)
    or compares results to those stored in the reference file (check=true)
    It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_OpenMP
{
Real NE;
template<typename M, typename V>
void test_multOpenMP(M& A, const V& X, V& B, Number nbl, Number test, const String& comment="")
{
  Number nbt=numberOfThreads();
  theCout<<comment<<" number of loop = "<<nbl<<eol;
  if(test==1 || test==3)
    {
      for(Number t=1; t<=nbt; t++)
        {
          numberOfThreads(t);
          Timer timeBegin;
          for(int i = 0; i < nbl; ++i) {multMatrixVector(A, X, B);}
          Timer timeEnd;
          theCout << nbl << " Matrix * Vector, nb threads = "<<t;
          //theCout << " cpu Time = " << timeEnd.deltaCpuTime(&timeBegin);
          theCout << " wall time = " << timeEnd.deltaTime(&timeBegin) << eol;
        }
    }
  StorageType sto=A.storageType();
  if(test==2 || test==3)
    {
      V Y=X;
      V Bt=B;
      M At;
      for(Number t=1; t<=nbt; t++)
        {
          numberOfThreads(t);
          At=A;
          Timer timeBegin;
          if(sto==_skyline) At.luFactorize();
          else
            {
              #ifdef XLIFEPP_WITH_UMFPACK
              At.umfpackFactorize();
              #else
              return;
              #endif
            }
          Timer timeEnd;
          theCout << " LU factorization, nb threads = "<<t;
          //theCout << " cpu Time = " << timeEnd.deltaCpuTime(&timeBegin);
          theCout << ", wall time = " << timeEnd.deltaTime(&timeBegin) << eol;
          Real cput=0., wallt=0.;
          for(int i = 0; i < nbl; ++i)
            {
              Bt=B;
              Timer timeBegin;
              if(sto==_skyline) At.luSolve(B,Y);
              else
                {
                  #ifdef XLIFEPP_WITH_UMFPACK
                  At.umfluSolve(B,Y);
                  #else
                  return;
                  #endif
                }
              Timer timeEnd;
              cput+=timeEnd.deltaCpuTime(&timeBegin);
              wallt+=timeEnd.deltaTime(&timeBegin);
            }
          theCout << nbl <<" Solve AY=B, nb threads = "<<t;
          //theCout << " cpu Time = " << cput;
          V E=X-Y;
          NE=norm2(E);
          theCout<<" error = "<<NE<<eol;
          theCout << " wall time = " <<wallt<<eol;
        }
    }
}

void sys_OpenMP(int argc, char* argv[], bool check)
{
  String rootname = "sys_OpenMP";
  trace_p->push(rootname);
  std::stringstream out; // string stream receiving results
  out.precision(testPrec);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real TestEpsilon=1.e-8;

  Vector<Real> X, B;
  Vector<Real>::it_vk itv;
  Number nbl=4000;

  String f=inputsPathTo(rootname+"/bfw782a.coo");

  Number s=782;
  LargeMatrix<Real> A1(f, _coo, s, s, _skyline, _dual);

  X.resize(s);
  B.reserve(s);
  for(itv = X.begin(); itv != X.end(); ++itv) *itv = NumTraits<Real>::random();
  test_multOpenMP(A1, X, B, nbl, 3,"matrice dual skyline 782x782");
  nbErrors+=checkValue(NE, 0., TestEpsilon, errors, "A1, nbl=4000");
  A1.clearall();

  nbl=100;
  f=inputsPathTo(rootname+"/2DP.coo");
  s=40401;
  LargeMatrix<Real> A2(f, _coo, s, s, _skyline, _dual);
  X.resize(s);
  B.reserve(s);
  for(itv = X.begin(); itv != X.end(); ++itv) *itv = NumTraits<Real>::random();
  test_multOpenMP(A2, X, B, nbl, 3, "matrice dual skyline 40401x40401");
  nbErrors+=checkValue(NE, 0., TestEpsilon, errors, "A2, nbl=4000");
  A2.clearall();

  nbl=20;
  f=inputsPathTo(rootname+"/2DQ20.coo");
  s=90601;
  LargeMatrix<Real> A3(f, _coo, s, s, _skyline, _dual);
  X.resize(s);
  B.reserve(s);
  for(itv = X.begin(); itv != X.end(); ++itv) *itv = NumTraits<Real>::random();
  test_multOpenMP(A3, X, B, nbl, 3, "matrice dual skyline 90601x90601");
  nbErrors+=checkValue(NE, 0., TestEpsilon, errors, "A3, nbl=4000");
  A3.clearall();

  nbl=20;
  LargeMatrix<Real> A3b(f, _coo, s, s, _cs, _dual);
  X.resize(s);
  B.reserve(s);
  for(itv = X.begin(); itv != X.end(); ++itv) *itv = NumTraits<Real>::random();
  test_multOpenMP(A3b, X, B, nbl, 3, "matrice cs dual 90601x90601");
  nbErrors+=checkValue(NE, 0., TestEpsilon, errors, "A1, nbl=4000");
  A3b.clearall();

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  trace_p->pop();
  
}

}
