/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_periodic_3d.cpp
  \author E. Lunéville
	\since 3 dec 2014
	\date 3 dec 2014

	Test eigen value of elasticity problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_eigen_elasticity {

Real lambda=1.5, mu=1., rho=7.86;
Real p=2., q=4.;

Vector<Real> maps(const Point& P, Parameters& pa = defaultParameters)
{
   Vector<Real> Q(P);
   Q(1)-=2;
   return Q;
}

Vector<Real> mapg(const Point& P, Parameters& pa = defaultParameters)
{
   Vector<Real> Q(P);
   Q(2)-=1;
   return Q;
}

Vector<Real> f(const Point& P, Parameters& pa = defaultParameters)
{
    Vector<Real> R(2);
    Real a=(lambda+2*mu)*pi_*pi_;
    R(1)=(a*p*p+rho)*sin(p*pi_*P(1));
    R(2)=(a*q*q+rho)*cos(q*pi_*P(2));
    return R;
}

Vector<Real> uex(const Point& P, Parameters& pa = defaultParameters)
{
    Vector<Real> R(2);
    R(1)=sin(p*pi_*P(1));
    R(2)=cos(q*pi_*P(2));
    return R;
}

void sys_eigen_elasticity(int argc, char* argv[], bool check)
{
  String rootname = "sys_eigen_elasticity";
  trace_p->push(rootname);

  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;

  // Strings sn11("yd1","x=1/2-","y=1/2-","xl1");
  // Rectangle om11(_xmin=0., _xmax=1., _ymin=0., _ymax=0.5, _hsteps=0.03, _domain_name="Omega11", _side_names=sn11);
  // Strings sn21("yd2","xr1","y=1/2-","x=1/2+");
  // Rectangle om21(_xmin=1., _xmax=2., _ymin=0., _ymax=0.5, _hsteps=0.03, _domain_name="Omega21", _side_names=sn21);
  // Strings sn22("y=1/2+","xr2","yu2","x=1/2+");
  // Rectangle om22(_xmin=1., _xmax=2., _ymin=0.5, _ymax=1., _hsteps=0.03, _domain_name="Omega22", _side_names=sn22);
  // Strings sn12("y=1/2+","x=1/2-","yu1","xl2");
  // Rectangle om12(_xmin=0., _xmax=1., _ymin=0.5, _ymax=1., _hsteps=0.03, _domain_name="Omega12", _side_names=sn12);

  // Geometry cellper=om11+om12+om22+om21;
  // Mesh m11(cellper, _shape=triangle, _order=1, _generator=gmsh);

  Mesh m11;
  if (!check)
  {
    Strings sn11("yd1","x=1/2-","y=1/2-","xl1");
    Rectangle om11(_xmin=0., _xmax=1., _ymin=0., _ymax=0.5, _hsteps=0.03, _domain_name="Omega11", _side_names=sn11);
    Strings sn21("yd2","xr1","y=1/2-","x=1/2+");
    Rectangle om21(_xmin=1., _xmax=2., _ymin=0., _ymax=0.5, _hsteps=0.03, _domain_name="Omega21", _side_names=sn21);
    Strings sn22("y=1/2+","xr2","yu2","x=1/2+");
    Rectangle om22(_xmin=1., _xmax=2., _ymin=0.5, _ymax=1., _hsteps=0.03, _domain_name="Omega22", _side_names=sn22);
    Strings sn12("y=1/2+","x=1/2-","yu1","xl2");
    Rectangle om12(_xmin=0., _xmax=1., _ymin=0.5, _ymax=1., _hsteps=0.03, _domain_name="Omega12", _side_names=sn12);
    Geometry cellper=om11+om12+om22+om21;
    m11=Mesh(cellper, _shape=triangle, _order=1, _generator=gmsh);
    m11.saveToFile(inputsPathTo(rootname+"/m11.msh"), msh);
  }
  else
  {
    m11=Mesh(inputsPathTo(rootname+"/m11.msh"), _name="Mesh m11");
  }

  Domain omega = m11.domain("Omega");
  Domain omega11 = m11.domain("Omega11"); Domain omega12 = m11.domain("Omega12");
  Domain omega21 = m11.domain("Omega21"); Domain omega22 = m11.domain("Omega22");
  Domain sigmaM1=m11.domain("xl1"), sigmaP1=m11.domain("xr1");
  Domain gammaM1=m11.domain("yd1"), gammaP1=m11.domain("yu1");
  Domain sigmaM2=m11.domain("xl2"), sigmaP2=m11.domain("xr2");
  Domain gammaM2=m11.domain("yd2"), gammaP2=m11.domain("yu2");
  defineMap(sigmaP1, sigmaM1, maps);  //useful to periodic condition
  defineMap(gammaP1, gammaM1, mapg);  //useful to periodic condition
  defineMap(sigmaP2, sigmaM2, maps);  //useful to periodic condition
  defineMap(gammaP2, gammaM2, mapg);  //useful to periodic condition

  // create P1 Lagrange interpolation
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u", _dim=2);  TestFunction v(u, _name="v");

  // create bilinear form and linear form
  BilinearForm auv = lambda*intg(omega11, epsilon(u) % epsilon(v)) + 2*mu*intg(omega11, div(u)*div(v))
                   + lambda*intg(omega22, epsilon(u) % epsilon(v)) + 2*mu*intg(omega22, div(u)*div(v))
                   + lambda*intg(omega12, epsilon(u) % epsilon(v)) + 2*mu*intg(omega12, div(u)*div(v))
                   + lambda*intg(omega21, epsilon(u) % epsilon(v)) + 2*mu*intg(omega21, div(u)*div(v)),
               muv = rho*intg(omega11,u|v) + rho*intg(omega22,u|v) + rho*intg(omega12,u|v) + rho*intg(omega21,u|v);

  EigenElements eigs;
  ComplexVector vc;

  // no essential condition
  // ----------------------
  elapsedTime();
  TermMatrix A(auv, _name="auv"), M(muv, _name="muv");
  elapsedTime("compute A and M", thePrintStream);

  eigs = eigenSolve(A, M, _nev=10, _which="SM");
  thePrintStream << "eigenSolve(A, M, _nev=10, _which=""""SM"""") -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  eigs = eigenSolve(A, M, _nev=10, _sigma=0.);
  thePrintStream << "eigenSolve(A, M, _nev=10, _sigma=0) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  eigs = arpackSolve(A, M, _nev=10, _which="SM");
  thePrintStream << "arpackSolve(A, M, _nev=10, _which=""""SM"""") -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  eigs = arpackSolve(A, M, _nev=10, _sigma=0.);
  thePrintStream << "arpackSolve(A, M, _nev=10, _sigma=0.) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

//  INTERN EIGEN SOLVERS DO NOT WORK !!!!!
//  eigs = eigenInternSolve(A, M, _nev=10, _which="SM");
//  ssout << "eigenInternSolve(A, M, _nev=10, _which=""""SM"""") -> eigs="<<eigs.values<<eol;
//
//  eigs = eigenSolve(A, M, _solver=_intern, _nev=10, _which="SM");
//  ssout << "eigenInternSolve(A, M, _nev=10, _which=""""SM"""") -> eigs="<<eigs.values<<eol;
//
//  eigs = eigenInternSolve(A, M, _nev=10, _sigma=0.);
//  ssout << "eigenInternSolve(A, M, _nev=10, _sigma=0.) -> eigs="<<eigs.values<<eol;


  // with Dirichlet boundary condition
  // ---------------------------------
  EssentialConditions ecsd = ((u|gammaP1) = 0) & ((u|gammaM1) = 0) & ((u|sigmaP1) = 0) & ((u|sigmaM1) = 0)
                           & ((u|gammaP2) = 0) & ((u|gammaM2) = 0) & ((u|sigmaP2) = 0) & ((u|sigmaM2) = 0);

  // pseudo-reduction
  TermMatrix Ad(auv, ecsd, _reduction=ReductionMethod(_pseudoReduction,0.,10000.), _name="auv");
  TermMatrix Md(muv, ecsd, _reduction=ReductionMethod(_pseudoReduction,0.,1.), _name="muv");
  elapsedTime("compute Ad and Md", thePrintStream);

  eigs = eigenSolve(Ad, Md, _nev=10, _sigma=0.);
  thePrintStream << "eigenSolve(Ad, Md, _nev=10, _sigma=0) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  // full reduction
  TermMatrix Adr(auv, ecsd, _realReductionMethod, _name="Adr");
  TermMatrix Mdr(muv, ecsd, _realReductionMethod, _name="Mdr");
  elapsedTime("compute Adr and Mdr", thePrintStream);

  eigs = eigenSolve(Adr, Mdr, _nev=10, _sigma=0.);
  thePrintStream << "eigenSolve(Adr, Mdr, _nev=10, _sigma=0) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  // with Periodic boundary condition
  // --------------------------------
  EssentialConditions ecsp = ((u|gammaP1) - (u|gammaM1) = 0) & ((u|sigmaP1) - (u|sigmaM1) = 0)
                           & ((u|gammaP2) - (u|gammaM2) = 0) & ((u|sigmaP2) - (u|sigmaM2) = 0);
  //test periodic solver
  TermMatrix As(auv+muv, ecsp, _name="auv");
  TermVector B(intg(omega,f|v), _name="B");
  TermVector U=directSolve(As,B);
  TermVector Uex(u,omega,uex);
  TermVector E=U-Uex;
  thePrintStream<<"|U-Uex|2="<<sqrt(abs(M*E|E)/rho)<<eol;
  nbErrors+=checkValue(sqrt(abs(M*E|E)/rho), 0., 0.01, errors, "|U-Uex|2");
  saveToFile("U",U,_vtu); saveToFile("Uex",Uex,_vtu);

  elapsedTime();
  // pseudo-reduction
  TermMatrix Ap(auv, ecsp, _reduction=ReductionMethod(_pseudoReduction,0.,10000.), _name="auv");
  TermMatrix Mp(muv, ecsp, _reduction=ReductionMethod(_pseudoReduction,0.,1.), _name="muv");
  elapsedTime("compute Ap and Mp", thePrintStream);

  eigs = eigenSolve(Ap, Mp, _nev=10, _which="SM");  //move to sigma=0; UNSTABLE because A is ill conditioned
  thePrintStream << "eigenSolve(Ap, Mp, _nev=10, _which=SM) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  eigs = eigenSolve(Ap, Mp, _nev=10, _sigma=1.);    //STABLE because A-B is well conditioned
  thePrintStream << "eigenSolve(Ap, Mp, _nev=10, _sigma=1) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);
  //eigs.vectors.applyEssentialConditions(ecsp);

  TermMatrix Ap2(auv + muv, ecsp, _reduction=ReductionMethod(_pseudoReduction,0.,10000.), _name="auv");
  eigs = eigenSolve(Ap2, Mp, _nev=10, _which="SM");  //move to sigma=0; STABLE because A+B is well conditioned
  thePrintStream << "eigenSolve(Ap2, Mp, _nev=10, _which=SM) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  eigs = arpackSolve(Ap, Mp, _nev=10, _which="SM");  //STABLE because do not use the inverse of A but give only 2 eigen values
  thePrintStream << "arpackSolve(Ap, Mp, _nev=10, _which=SM) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);

  //full reduction
  TermMatrix Apr(auv, ecsp, _realReductionMethod, _name="Apr");
  TermMatrix Mpr(muv, ecsp, _realReductionMethod, _name="Mpr");
  elapsedTime("compute Apr and Mpr", thePrintStream);
  eigs = eigenSolve(Apr, Mpr, _nev=10, _sigma=1.);
  thePrintStream << "eigenSolve(Apr, Mpr, _nev=10, _sigma=1) -> eigs="<<eigs.values<<eol;
  elapsedTime("compute eigen", thePrintStream);
  //eigs.vectors.applyEssentialConditions(ecsp);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
