/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_IE_Helmholtz3D_NDC.cpp
	\since 20 jun 2014
	\date 20 jun 2014
	\author Eric Lunéville

	Solve Helmholtz 3D problem using Neumann double layer IE = Hyper-singular
	P1-P2 approximation and Sauter Schwab integration method
	This test use the mesh of a sphere (surface mesh in R3)
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_IE_Helmholtz3D_NDC
{

// incident plane wave
Complex uinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky"), kz=pa("kz");
  Real kp=kx*p(1)+ky*p(2)+kz*p(3);
  return exp(i_*kp);
}

// normal derivative of incident plane wave on sphere
Complex druinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky"), kz=pa("kz");
  Real kp=kx*p(1)+ky*p(2)+kz*p(3);
  return i_*kp*exp(i_*kp);
}

Complex g(const Point& p, Parameters& pa = defaultParameters)
{
  Real k=pa("k");
  Real r=norm(p);
  return exp(i_*k*r)/(4.*pi_*r);
}

// normal derivative of Hemholtz kernel on sphere r=R
Complex drg(const Point& p, Parameters& pa = defaultParameters)
{
  Real k=pa("k");
  return exp(i_*k)*(i_*k-1.)*over4pi_;
}

Complex gun(const Point& p,const Point& q,Parameters& pa = defaultParameters)
{
  return Complex(1.,0.);
}

void sys_IE_Helmholtz3D_NDC(int argc, char* argv[], bool check)
{
  String rootname = "sys_IE_Helmholtz3D_NDC";
  trace_p->push(rootname);

  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  bool saveF=false;

  //numberOfThreads(1);

  //computing parameters
  bool doP1=true, doP2=false;
  Number npa=15;   //nb of points by diameter of sphere
  Real k=3., k2=k*k;

  //define parameters and functions
  Parameters pars;
  pars<<Parameter(k,"k");       // wave number
  pars<<Parameter(k,"kx")<<Parameter(0.,"ky")<<Parameter(0.,"kz");      // x,y,z wave number of incident wave
  pars<<Parameter(1.,"radius");        // sphere radius
  Kernel G = Helmholtz3dKernel(pars);  //load Helmholtz3D kernel
  Function ginc(uinc,pars);
  Function finc(druinc,pars);
  Function scatSol(scatteredFieldSphereNeumann,pars);

  //meshing
  Sphere sp(_center=Point(0,0,0),_radius=1.,_nnodes=npa,_domain_name="sphere");
  Mesh m1(sp, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain sphere = m1.domain(0);
  elapsedTime("mesh");

  //Sauter-Schwab integration method
  Number order=5;
  IntegrationMethods ims(_SauterSchwabIM,order,0.,_defaultRule,4,2.,_defaultRule,3);

  //used by any interpolation
  Space V1(_domain=sphere, _interpolation=P1, _name="V1", _notOptimizeNumbering);
  Unknown u1(V1, _name="u1"); TestFunction v1(u1, _name="v1");
  TermMatrix M11(intg(sphere,u1*v1), _name="M11");

  //for integral representation on x=0 plane
  Number npp=20, npc=8*npp/10;
//  Rectangle recx(_v1=Point(0.,-4.,-4.),_v2=Point(0.,4.,-4.),_v4=Point(0.,-4.,4.),_nnodes=npp,_domain_name="x=0");
//  Disk dx(_center=Point(0.,0.,0.),_v1=Point(0.,1.25,0.),_v2=Point(0.,0.,1.25),_nnodes=npc,_domain_name="x=0");
//  Mesh mx0(recx-dx, _shape=_triangle, _order=1, _generator=_gmsh);
//  Domain planx0 = mx0.domain(0);
//  Space Wx(_domain=planx0, _interpolation=P1, _name="Wx");
//  Unknown wx(Wx, _name="wx");
//  TermMatrix Rx(wx, planx0, u1, sphere, grad_y(G)|_ny, _name="Rx");  //(grady(G)|ny)(xi,yj)
//  TermVector Uix0(wx,planx0,ginc);
//  TermVector soldx0(wx,planx0,scatSol);
//  TermVector soltx0=Uix0+soldx0;
//  TermMatrix Mx0(intg(planx0, wx*wx), _name="Mx0");  //mass matrix on x=0
//  //for integral representation on y=0 plane
//  Rectangle recy(_v1=Point(-4.,0.,-4.),_v2=Point(4.,0.,-4.),_v4=Point(-4.,0.,4.),_nnodes=npp,_domain_name="y=0");
//  Disk dy(_center=Point(0.,0.,0.),_v1=Point(1.25,0.,0.),_v2=Point(0.,0.,1.25),_nnodes=npc,_domain_name="y=0");
//  Mesh my0(recy-dy, _shape=_triangle, _order=1, _generator=_gmsh);
//  Domain plany0 = my0.domain(0);
//  Space Wy(_domain=plany0, _interpolation=P1, _name="Wy");
//  Unknown wy(Wy,"wy");
//  TermMatrix Ry(wy, plany0, u1, sphere, grad_y(G)|_ny, _name="Ry");  //(grady(G)|ny)(xi,yj)
//  TermVector Uiy0(wy,plany0,ginc);
//  TermVector soldy0(wy,plany0,scatSol);
//  TermVector solty0=Uiy0+soldy0;
//  TermMatrix My0(intg(plany0, wy*wy), _name="My0");  //mass matrix on y=0
//  //integral representation on z=0 plane
  Rectangle recz(_v1=Point(-4.,-4.,0.),_v2=Point(4.,-4.,0.),_v4=Point(-4.,4.,0.),_nnodes=npp,_domain_name="z=0");
  Disk dz(_center=Point(0.,0.,0.),_v1=Point(1.25,0.,0.),_v2=Point(0.,1.25,0.),_nnodes=npc,_domain_name="z=0");
  Mesh mz0(recz-dz, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain planz0 = mz0.domain(0);
  Space Wz(_domain=planz0, _interpolation=P1, _name="Wz");
  Unknown wz(Wz, _name="wz");
//  TermMatrix Rz(wz, planz0, u1, sphere, grad_y(G)|_ny, _name="Rz");  //(grady(G)|ny)(xi,yj)
  TermVector Uiz0(wz,planz0,ginc);
  TermVector soldz0(wz,planz0,scatSol);
  TermVector soltz0=Uiz0+soldz0;
  TermMatrix Mz0(intg(planz0, wz*wz), _name="Mz0");  //mass matrix on z=0
//
//  //save exact diffracted field on x,y,z planes
//  saveToFile("sold_x=0.vtk",soldx0,_format=_vtk);
//  saveToFile("sold_y=0.vtk",soldy0,_format=_vtk);
  if (saveF) saveToFile("sold_z=0.vtk",soldz0,_format=_vtk);
//  saveToFile("solt_x=0.vtk",soltx0,_format=_vtk);
//  saveToFile("solt_y=0.vtk",solty0,_format=_vtk);
  if (saveF) saveToFile("solt_z=0.vtk",soltz0,_format=_vtk);
//
//  out<<"test IE double layer Neumann problem for Hemholtz equation on a sphere"<<eol;
//  out<<" P1 mesh : number of triangles  = "<<m1.nbOfElements()<<" number of vertices = "<<m1.nbOfVertices()<<eol;
//  out<<" L2 norm of solution on planes : "<<sqrt(abs((Mx0*soldx0|soldx0)+(My0*soldy0|soldy0)+(Mz0*soldz0|soldz0))/3.)<<eol;

  TermVector udif_ex=TermVector(u1,sphere,scatSol);
  TermVector utot_ex=udif_ex+TermVector(u1,sphere,ginc);
//  saveToFile("udif_ex",udif_ex,_format=_vtk);
//  saveToFile("utot_ex",utot_ex,_format=_vtk);

  Real tA, tS;

  TermVector Un(u1,sphere,1.);
  Kernel Gun(gun);
//  TermMatrix D(intg(sphere,sphere,u1*(Gun*(_nx|_ny))*v1));
//  thePrintStream<<"norminfty(D*Un)="<<norminfty(D*Un);
//  TermMatrix E(intg(sphere,sphere,(ncrossgrad(u1)*Gun)|ncrossgrad(v1)));
//  thePrintStream<<" norminfty(E*Un)="<<norminfty(E*Un)<<eol;


  //=================  P1 computation =========================
  if(doP1)
    {
      //define forms
      BilinearForm bf1= intg(sphere,sphere,(ncrossgrad(u1)*G)|ncrossgrad(v1),_method=ims)
                       - k2*intg(sphere,sphere,u1*(G*(_nx|_ny))*v1,_method=ims);

      BilinearForm mlf1 = intg(sphere,u1*v1);
      LinearForm fv1 = intg(sphere,finc*v1,_quad=GaussLegendre,_order=4);
      //compute matrix and right hand side
      TermMatrix A1(bf1, _name="A1");
      tA=elapsedTime("compute A");
      TermVector B1(fv1, _name="B1");
      //solve the system using Gauss elimination
      TermVector U1 = directSolve(A1, B1);
      tS=elapsedTime("solve AU=B");
      if (saveF) saveToFile("U1.vtk",U1,_format=_vtk);
      ssout<<"U1 = "<<U1<<eol;
      clear(A1);

      TermVector U1dz0IR=integralRepresentation(wz,planz0,intg(sphere,(grad_y(G)|_ny)*U1,_quad=_symmetricalGaussRule,_order=6));
      //TermVector U1dz0IR=integralRepresentation(wz,planz0,intg(sphere,(grad_y(G)|_ny)*utot_ex,_quad=_symmetricalGaussRule,_order=6));
      if (saveF) saveToFile("U1d_zIR=0.vtk",U1dz0IR,_format=_vtk);
      TermVector  ec1z0=U1dz0IR-soldz0;
      Real  el1z0= sqrt(abs((Mz0*ec1z0) | ec1z0));
      ssout<<"   L2 error on z=0 = "<<el1z0<<eol;
      ssout<<"   Linf error on z=0 = "<<norm(ec1z0,0)<<eol;
      nbErrors+=checkValue(el1z0, 0., 0.02, errors, "L2 error on z=0");
      nbErrors+=checkValue(norm(ec1z0,0), 0., 0.01, errors, "Linf error on z=0");



      //integral representation on x, y, z=0 plane
//      TermVector U1dx0=Rx*(M11*U1);
//      TermVector U1tx0=U1dx0+Uix0;
//      saveToFile("U1d_x=0.vtk",U1dx0,_format=_vtk);
//      saveToFile("U1t_x=0.vtk",U1tx0,_format=_vtk);
//      TermVector U1dy0=Ry*(M11*U1);
//      TermVector U1ty0=U1dy0+Uiy0;
//      saveToFile("U1d_y=0.vtk",U1dy0,_format=_vtk);
//      saveToFile("U1t_y=0.vtk",U1ty0,_format=_vtk);
//      TermVector U1dz0=Rz*(M11*U1);
//      TermVector U1tz0=U1dz0+Uiz0;
//      saveToFile("U1d_z=0.vtk",U1dz0,_format=_vtk);
//      saveToFile("U1t_z=0.vtk",U1tz0,_format=_vtk);

      //compute errors on diffracted field
//      TermVector ec1x0=U1dx0-soldx0, ec1y0=U1dy0-soldy0, ec1z0=U1dz0-soldz0;
//      Real el1x0= sqrt(abs((Mx0*ec1x0) | ec1x0)),
//           el1y0= sqrt(abs((My0*ec1y0) | ec1y0)),
//           el1z0= sqrt(abs((Mz0*ec1z0) | ec1z0));
//      out<<"=== P1 approximation, number of dofs = "<<V1.nbDofs()<<"==="<<eol;
//      out<<"   L2 error on x=0 = "<<el1x0<<eol;
//      out<<"   L2 error on y=0 = "<<el1y0<<eol;
//      out<<"   L2 error on z=0 = "<<el1z0<<eol;
//      out<<"   Linf error on x=0 = "<<norm(ec1x0,0)<<eol;
//      out<<"   Linf error on y=0 = "<<norm(ec1y0,0)<<eol;
//      out<<"   Linf error on z=0 = "<<norm(ec1z0,0)<<eol;
//      out<<"   time compute A = "<<tA<<" time solve = "<<tS<<eol;
    }

  //=================  P2 computation =========================
  if(doP2)
    {
      //define forms
      Space V2(_domain=sphere, _interpolation=P2, _name="V2");
      Unknown u2(V2, _name="u2"); TestFunction v2(u2, _name="v2");
      BilinearForm bf2=k2*intg(sphere,sphere,u2*(G*(_nx|_ny))*v2,_method=ims)
                       - intg(sphere,sphere,(ncrossgrad(u2)*G)|ncrossgrad(v2),_method=ims);
      LinearForm fv2 = intg(sphere,finc*v2,_quad=GaussLegendre,_order=4);
      //compute matrix and right hand side
      TermMatrix A2(bf2, _name="A2");
      tA=elapsedTime("compute A");
      TermVector B2(fv2, _name="B2");
      //solve the system using Gauss elimination
      TermVector U2 = directSolve(A2, B2);
      tS=elapsedTime("solve AU=B");
      if (saveF) saveToFile("U2.vtk",U2,_format=_vtk);
      ssout<<"U2 = "<<U2<<eol;
      clear(A2);
//      //integral representation on x, y, z=0 plane
//      TermMatrix M12(intg(sphere,u2*v1), _name="M12");
//      TermVector U2dx0=Rx*(M12*U2);
//      TermVector U2tx0=U2dx0+Uix0;
//      saveToFile("U2d_x=0.vtk",U2dx0,_format=_vtk);
//      saveToFile("U2t_x=0.vtk",U2tx0,_format=_vtk);
//      TermVector U2dy0=Ry*(M12*U2);
//      TermVector U2ty0=U2dy0+Uiy0;
//      saveToFile("U2d_y=0.vtk",U2dy0,_format=_vtk);
//      saveToFile("U2t_y=0.vtk",U2ty0,_format=_vtk);
//      TermVector U2dz0=Rz*(M12*U2);
//      TermVector U2tz0=U2dz0+Uiz0;
//      saveToFile("U2d_z=0.vtk",U2dz0,_format=_vtk);
//      saveToFile("U2t_z=0.vtk",U2tz0,_format=_vtk);
//
//      //compute errors on diffracted field
//      TermVector ec2x0=U2dx0-soldx0, ec2y0=U2dy0-soldy0, ec2z0=U2dz0-soldz0;
//      Real el2x0= sqrt(abs((Mx0*ec2x0) | ec2x0)),
//           el2y0= sqrt(abs((My0*ec2y0) | ec2y0)),
//           el2z0= sqrt(abs((Mz0*ec2z0) | ec2z0));
//      out<<"=== P2 approximation, number of dofs = "<<V2.nbDofs()<<"==="<<eol;
//      out<<"   L2 error on x=0 = "<<el2x0<<eol;
//      out<<"   L2 error on y=0 = "<<el2y0<<eol;
//      out<<"   L2 error on z=0 = "<<el2z0<<eol;
//      out<<"   Linf error on x=0 = "<<norm(ec2x0,0)<<eol;
//      out<<"   Linf error on y=0 = "<<norm(ec2y0,0)<<eol;
//      out<<"   Linf error on z=0 = "<<norm(ec2z0,0)<<eol;
//      out<<"   time compute A = "<<tA<<" time solve = "<<tS<<eol;
    }

  std::cout<<"U saved, that's all folks"<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
