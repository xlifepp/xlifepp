/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_ExplicitIntegralRepresentation.cpp
\author N. Salles
\since 6 April 2016
\date 6 April 2016
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

//using namespace std;
using namespace xlifepp;

namespace sys_ExplicitIntegralRepresentation
{

const Real k=3.35;
Kernel Gh, G;
// Number nbErrors = 0;
String errors;

Real gr(const Point& M, Parameters& pa = defaultParameters)
{
  Point x0(0.,0.,0.);
  return over4pi_/M.distance(x0);
}

Complex gc(const Point& M, Parameters& pa = defaultParameters)
{
  Point x0(0.,0.,0.);
  return Complex(over4pi_/M.distance(x0),0.);
}

Real gr2(const Point& M, Parameters& pa = defaultParameters)
{
  Point x0(0.,0.);
  return -over2pi_*std::log(M.distance(x0));
}

Complex gc2(const Point& M, Parameters& pa = defaultParameters)
{
  Point x0(0.,0.);
  return Complex(-over2pi_*std::log(M.distance(x0)),0.);
}

template<typename T>
Number testRep(Domain gamma, Space& V, Kernel& K, const String& eqtype,
             std::vector<Point>& points, Function &f, Number quadorder, IntegrationMethods& ims, IntegrationMethods& imsir, Real tol, bool print=false)
{
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");
  BilinearForm bf;
  if(eqtype=="SL") bf=intg(gamma,gamma,u*K*v,_method=ims);
  if(eqtype=="DL") bf=0.5*intg(gamma,u*v)+intg(gamma,gamma,u*ndotgrad_y(K)*v,_method=ims);
  LinearForm lf=intg(gamma,f*v);
  TermMatrix lhs(bf);
  TermVector rhs(lf);
  TermVector sol=gaussSolve(lhs,rhs);
  Vector<T> vsol;
  // thePrintStream<<"sol ="<<sol.asVector(vsol)<<eol;
  Number Np=points.size();
  Vector<T> valnum(Np,0.), valexp(Np,0.);
  Real tn,te;
  Number E=0;
  if (eqtype=="SL")
  {
    elapsedTime();
    integralRepresentation(points,intg(gamma,K*sol,_quad=_GaussLegendreRule, _order=quadorder),valnum);
    tn=elapsedTime();
    integralRepresentation(points,intg(gamma,K*sol,_method=imsir),valexp);
    te=elapsedTime();
  }
  if (eqtype=="DL")
  {
    elapsedTime();
    integralRepresentation(points,intg(gamma,ndotgrad_y(K)*sol,_quad=_GaussLegendreRule,_order=quadorder),valnum);
    tn=elapsedTime();
    integralRepresentation(points,intg(gamma,ndotgrad_y(K)*sol,_method=imsir),valexp);
    te=elapsedTime();
  }
  if (print)
  {
    thePrintStream << "points = "<< points<<eol;
    thePrintStream << "numeric  val = " << valnum << eol;
    thePrintStream << "explicit val = " << valexp << eol;
  }
  Real diff=0, nv=0.;
  for (Number l=0; l<Np; l++)
  {
    diff+=std::abs(valexp[l]-valnum[l])*std::abs(valexp[l]-valnum[l]);
    nv+=std::abs(valnum[l])*std::abs(valnum[l]);
  }
  //diff=std::sqrt(diff)/std::sqrt(nv);
  diff=std::sqrt(diff)/std::sqrt(Np);
  thePrintStream << " difference = " << diff << eol;
  E+=checkValue(diff, 0., tol, errors, "diff");
  return E;
}

void sys_ExplicitIntegralRepresentation(int argc, char* argv[], bool check)
{
  String rootname = "sys_ExplicitIntegralRepresentation";
  trace_p->push(rootname);
  verboseLevel(0);
  numberOfThreads(1);
  String errors;
  // Number nbErrors = 0;
  Real tol1=0.0001;
  Real tol2=0.2;
  Number nbErrors = 0;


  bool doLaplace=true, doHelmholtz=true;
  bool do2D=true, do3D=true;
  bool doSL=true, doDL=true;
  bool doP0=true, doP1=true;
  Number nmesh;
  bool printRep=false;
  Number q=6;  //quadrature order

  if (do2D) // 2D test
  {
    nmesh=21;
    Mesh m2;
    if (!check)
    {
      Disk ds(_center=Point(0.,0.),_radius=0.9,_nnodes=nmesh);
      m2=Mesh(ds, _shape=_segment, _order=1, _generator=_gmsh);
      m2.saveToFile(inputsPathTo(rootname+"/m2.msh"),msh);
    }
    else
    {
      m2=Mesh(inputsPathTo(rootname+"/m2.msh"), _name="m2");
    }
    Domain disk = m2.domain(0);
    disk.setNormalOrientation(_towardsInfinite);

    // Define spaces
    Space V02(_domain=disk, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
    Space V12(_domain=disk, _interpolation=_P1, _name="V1", _notOptimizeNumbering);

    // Points for IR
    Mesh m2ir;
    if (!check)
    {
      Disk dsir(_center=Point(0.,0.),_radius=1.2,_nnodes=10);
      m2ir=Mesh(dsir, _shape=_segment, _order=1, _generator=_gmsh);
      m2ir.saveToFile(inputsPathTo(rootname+"/m2ir.msh"),msh);
    }
    else
    {
      m2ir=Mesh(inputsPathTo(rootname+"/m2ir.msh"), _name="/m2ir");
    }
    std::vector<Point> evalPoints2=m2ir.nodes;

    thePrintStream<<"======================================================="<<eol;
    thePrintStream <<" 2D test "<<V02.nbDofs()<< " dofs in P0, "<<V12.nbDofs()<< " dofs in P1, "  << evalPoints2.size()<<" points for IR"<<eol;
    thePrintStream<<"======================================================="<<eol;

    // Define IntegrationMethods for bilinear forms
    IntegrationMethods ims2(_method=Duffy, _order=5, _quad=_defaultRule, _order=5, _bound=2., _quad=_defaultRule, _order=4, _bound=4., _quad=_defaultRule, _order=3);
    if (doLaplace)
    {
      //====================================
      // Test LAPLACE
      //====================================
      Kernel Gl2d=Laplace2dKernel();
      Function fl2d(gr2);
      IntegrationMethods imsir2(_method=LenoirSalles2dIR(),_functionPart=_allFunction, _bound=theRealMax);
      if (doP0)
      {
        if (doSL)
        {
          thePrintStream << "  LAPLACE SL P0"<<eol;
          nbErrors+=testRep<Real>(disk,V02,Gl2d,"SL", evalPoints2,fl2d,q,ims2,imsir2,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  LAPLACE DL P0"<<eol;
          nbErrors+=testRep<Real>(disk,V02,Gl2d,"DL", evalPoints2,fl2d,q,ims2,imsir2,tol1,printRep);
        }
      }
      if (doP1)
      {
        if (doSL)
        {
          thePrintStream << "  LAPLACE SL P1"<<eol;
          nbErrors+=testRep<Real>(disk,V12,Gl2d,"SL", evalPoints2,fl2d,q,ims2,imsir2,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  LAPLACE DL P1"<<eol;
          nbErrors+=testRep<Real>(disk,V12,Gl2d,"DL", evalPoints2,fl2d,q,ims2,imsir2,tol1,printRep);
        }
      }
    }

    if (doHelmholtz)
    {
      //====================================
      // Test HELMHOLTZ
      //====================================
      Kernel Gh2d=Helmholtz2dKernel(k);
      Function fh2d(gc2);
      IntegrationMethods imsirh2(_method=LenoirSalles2dIR(), _functionPart=_singularPart, _bound=theRealMax,
                                 _quad=QuadratureIM(_GaussLegendreRule,q), _functionPart=_regularPart, _bound=theRealMax);
      if (doP0)
      {
        if (doSL)
        {
          thePrintStream << "  HELMHOLTZ SL P0"<<eol;
          nbErrors+=testRep<Complex>(disk,V02,Gh2d, "SL", evalPoints2,fh2d,q,ims2,imsirh2,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  HELMHOLTZ DL P0"<<eol;
          nbErrors+=testRep<Complex>(disk,V02,Gh2d, "DL", evalPoints2,fh2d,q,ims2,imsirh2,tol1,printRep);
        }
      }
      if (doP1)
      {
        if (doSL)
        {
          thePrintStream << "  HELMHOLTZ SL P1"<<eol;
          nbErrors+=testRep<Complex>(disk,V12,Gh2d, "SL", evalPoints2,fh2d,q,ims2,imsirh2,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  HELMHOLTZ DL P1"<<eol;
          nbErrors+=testRep<Complex>(disk,V12,Gh2d, "DL", evalPoints2,fh2d,q,ims2,imsirh2,tol1,printRep);
        }
      }
    }
  }

  if (do3D) // 3D test
  {
    nmesh=3;
    Mesh m1,mir;
    if (!check)
    {
      Ball sp(_center=Point( 0.,0.,0.),_radius=1.,_nboctants=8,_nnodes=nmesh);
      m1=Mesh(sp, _shape=_triangle, _order=1, _generator=_subdiv);
      m1.saveToFile(inputsPathTo(rootname+"/m1.msh"),msh);   
    }
    else
    {
      m1=Mesh(inputsPathTo(rootname+"/m1.msh"), _name="m1");
    }
    Domain sphere = m1.domain(0);
    sphere.setNormalOrientation(_towardsInfinite);

    // Define spaces
    Space V0(_domain=sphere, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
    Space V1(_domain=sphere, _interpolation=_P1, _name="V1", _notOptimizeNumbering);

    // Points for IR
    if(!check)
    {
      Ball spir(_center=Point(0.,0.,0.),_radius=1.2,_nboctants=8,_nnodes=3);
      mir=Mesh(spir,_shape=_triangle, _order=1, _generator=gmsh);
      mir.saveToFile(inputsPathTo(rootname+"/mir.msh"),msh);
    }
    else
    {
      mir=Mesh(inputsPathTo(rootname+"/mir.msh"),"mir",msh);
    }
    std::vector<Point> evalPoints=mir.nodes;

    thePrintStream<<"======================================================="<<eol;
    thePrintStream <<" 3D test    "<<V0.nbDofs()<< " dofs in P0, "<<V1.nbDofs()<< " dofs in P1, "  << evalPoints.size()<<" points for IR"<<eol;
    thePrintStream<<"======================================================="<<eol;

    // Define IntegrationMethods for bilinear forms
    IntegrationMethods ims(_method=SauterSchwab, _order=5, _quad=_defaultRule, _order=5, _bound=2., _quad=_defaultRule, _order=4, _bound=4., _quad=_defaultRule, _order=3);

    if (doLaplace)
    {
      //====================================
      // Test LAPLACE
      //====================================
      printRep=false;
      Kernel Gl3d=Laplace3dKernel();
      Function fl3d(gr);
      IntegrationMethods imsir(_method=LenoirSalles3dIR(), _functionPart=_allFunction, _bound=theRealMax);
      if (doP0)
      {
        if (doSL)
        {
          thePrintStream << "  LAPLACE SL P0"<<eol;
          nbErrors+=testRep<Real>(sphere,V0,Gl3d,"SL", evalPoints,fl3d,q,ims,imsir,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  LAPLACE DL P0"<<eol;
          nbErrors+=testRep<Real>(sphere,V0,Gl3d,"DL", evalPoints,fl3d,q,ims,imsir,tol2,printRep);
        }
      }
      if (doP1)
      {
        if (doSL)
        {
          thePrintStream << "  LAPLACE SL P1"<<eol;
          nbErrors+=testRep<Real>(sphere,V1,Gl3d,"SL", evalPoints,fl3d,q,ims,imsir,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  LAPLACE DL P1"<<eol;
          nbErrors+=testRep<Real>(sphere,V1,Gl3d,"DL", evalPoints,fl3d,q,ims,imsir,tol2,printRep);
        }
      }
    }
    if (doHelmholtz)
    {
      //====================================
      // Test HELMHOLTZ
      //====================================
      Kernel Gh3d=Helmholtz3dKernel(k);
      Function fh3d(gc);
      IntegrationMethods imsirh(_method=LenoirSalles3dIR(), _functionPart=_singularPart, _bound=theRealMax,
                                _quad=QuadratureIM(_GaussLegendreRule,q), _functionPart=_regularPart, _bound=theRealMax);
      if (doP0)
      {
        if (doSL)
        {
          thePrintStream << "  HELMHOLTZ SL P0"<<eol;
          nbErrors+=testRep<Complex>(sphere,V0,Gh3d, "SL", evalPoints,fh3d,q,ims,imsirh,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  HELMHOLTZ DL P0"<<eol;
          nbErrors+=testRep<Complex>(sphere,V0,Gh3d, "DL", evalPoints,fh3d,q,ims,imsirh,tol2,printRep);
        }
      }
      if (doP1)
      {
        if (doSL)
        {
          thePrintStream << "  HELMHOLTZ SL P1"<<eol;
          nbErrors+=testRep<Complex>(sphere,V1,Gh3d, "SL", evalPoints,fh3d,q,ims,imsirh,tol1,printRep);
        }
        if (doDL)
        {
          thePrintStream << "  HELMHOLTZ DL P1"<<eol;
          nbErrors+=testRep<Complex>(sphere,V1,Gh3d, "DL", evalPoints,fh3d,q,ims,imsirh,tol1,printRep);
        }
      }
    }
  }
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
