/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_Stokes2D.cpp
\author E. Lunéville
\since 1 March 2014
\date 1 March 2014
*/
#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_Stokes2D
{

Real fs(const Point& P, Parameters& pa = defaultParameters)
{ Real x=P(1), y=P(2);
  return 32*(x*(1-x)+y*(1-y));}

class opStokes
{
  public :
  TermMatrix *pA;         //pointer to TermMatrix
  mutable Number nbprod;
  ValueType valueType() const  {return xlifepp::_real;} //mandatory function
  opStokes(TermMatrix& A) : pA(&A){nbprod=0;}
};
void multMatrixVector ( const opStokes& O, const TermVector& X, TermVector& R)  //mandatory
{
  R = (*O.pA)*X; O.nbprod++;
}

void sys_Stokes2D(int argc, char* argv[], bool check)
{
  String rootname = "sys_Stokes2D";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  String errors;
  Number nbErrors = 0;
  Real eps=0.0001;
  verboseLevel(11);

  //======================= Stokes direct and GMRES  ======================
  SquareGeo sq(_origin=Point(0.,0.), _length=1, _nnodes=21);
  Mesh ms(sq, _shape=triangle, _order=1, _generator=structured);
  Domain omega=ms.domain("Omega");
  Space H(_domain=omega, _interpolation=P0, _name="H", _notOptimizeNumbering);
  Space RT(_domain=omega, _FE_type=RaviartThomas, _order=1, _name="RT");
  Unknown p(RT, _name="p"); TestFunction q(p, _name="q");  //p=grad(u)
  Unknown r(H, _name="r"); TestFunction t(r, _name="t");
  //create problem (Poisson problem)
  TermMatrix A(intg(omega, p|q) + intg(omega, r*div(q)) - intg(omega, div(p)*t));
  TermVector b(intg(omega, fs*t));
  //solve and save solution
  elapsedTime();
  TermVector X=directSolve(A, b, true);
  elapsedTime();
  saveToFile("r", X(r), _format=vtu);
  opStokes os(A);
  GmresSolver gmres;
  elapsedTime("direct solve");
  TermVector Xg=gmres(os,b,b);
  elapsedTime("gmres");
  theCout<<Xg;
  theCout<<"nbprod="<<os.nbprod<<eol;
  saveToFile("rg", Xg(t), _format=vtu);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
