/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_1d.cpp
  \author E. Lunéville
	\since 14 nov 2013
	\date 14 nov 2013

*/
#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_1d
{

typedef GeomDomain& Domain;

Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Int n=2;
  return std::sin(n*pi_*P(1));
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  Int n=2;
  return n*n*pi_*pi_*std::sin(n*pi_*P(1));
}

Real fp(const Point& P, Parameters& pa = defaultParameters)
{
  Int n=2;
  return (n*n*pi_*pi_+1)*std::sin(n*pi_*P(1));
}

Vector<Real> mapLtoR(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> Q(P);
  Q(1)+=1;
  return Q;
}

Real uexn(const Point& P, Parameters& pa = defaultParameters)
{
  Int n=2;
  return std::cos(n*pi_*P(1));
}

Real fn(const Point& P, Parameters& pa = defaultParameters)
{
  Int n=2;
  return (n*n*pi_*pi_+1)*std::cos(n*pi_*P(1));
}

void sys_1d(int argc, char* argv[], bool check)
{
  String rootname = "sys_1d";
  trace_p->push(rootname);
  verboseLevel(0);
  Number nbErrors=0;
  String errors;
  // Real tol=0.00001;
  int OrderMax=9;
  Vector<Real> tol(OrderMax, 0.00001); tol(1)=0.001;tol(2)=0.0001;tol(3)=0.0001;
  Real ErrL2;
  bool SaveToFile=false;

  numberOfThreads(1);

  //  mesh and domains
  Segment S01(_xmin=0,_xmax=1,_nnodes=10,_domain_name="Omega",_side_names=Strings("x=0","x=1"));
  Mesh m1(S01, _order=1, _generator=_structured, _name="P1-mesh of [0,1], step = 0.01");
  Domain omega = m1.domain("Omega"), sigmaL = m1.domain("x=0"), sigmaR = m1.domain("x=1");
  defineMap(sigmaL, sigmaR, mapLtoR);  //useful to periodic condition
  
  for (Number order=1; order<=OrderMax; order++)
  {
    thePrintStream<<"====================== Lagrange order "<<order<<" ==============================="<<eol;
    // space, unknowns, and test functions
    Space Vh(_domain=omega, _FE_type=Lagrange, _order=order, _name="Vh", _optimizeNumbering);
    Unknown u(Vh, _name="u"); TestFunction v(u, _name="v");

    // bilinear form and linear form
    BilinearForm a = intg(omega, grad(u)|grad(v));
    LinearForm l = intg(omega, f*v);
    EssentialConditions ecs = (u|sigmaL = 0) & (u|sigmaR = 0);
    TermMatrix M(intg(omega,u*v), _name="M");
    TermMatrix A(a, ecs, _name="A");
    TermVector F(l, _name="F");

    // solve linear system AX=F using LU factorization
    TermVector U=directSolve(A,F);
    if (SaveToFile) saveToFile("U1d_dir"+tostring(order),U,_format=vtu);
    TermVector Uex(u,omega,uex,_name="Uex");
    TermVector E=U-Uex;
    ErrL2=std::sqrt(std::abs((M*E)|E));
    thePrintStream<<"  Laplace with Dirichlet condition, err L2 = "<<ErrL2<<eol; 
    nbErrors+= checkValue(ErrL2, 0, tol(order), errors, "Laplace with Dirichlet condition, err L2");

    //periodic condition
    
    BilinearForm ap = intg(omega, grad(u)|grad(v))+intg(omega, u*v);
    EssentialConditions ecp = (u|sigmaL) - (u|sigmaR) = 0;
    TermMatrix Ap(ap, ecp, _name="Ap");
    TermVector Fp(intg(omega, fp*v), _name="Fp");
    TermVector Up=directSolve(Ap,Fp);
    if (SaveToFile) saveToFile("U1d_per"+tostring(order),Up,_format=_vtu);
    TermVector Ep=Up-Uex;
    ErrL2=std::sqrt(std::abs((M*Ep)|Ep));
    thePrintStream<<"  Laplace with periodic condition, err L2 = "<<ErrL2<<eol; 
    nbErrors+= checkValue(ErrL2, 0, tol(order), errors, "Laplace with periodic condition, err L2");

    //Neumann problem
    BilinearForm an = intg(omega, grad(u)|grad(v))+intg(omega, u*v);
    TermMatrix An(an, _name="An");
    TermVector Fn(intg(omega, fn*v), _name="Fn");
    TermVector Un=directSolve(An,Fn);
    TermVector Uexn(u,omega,uexn,_name="Uexn");
    TermVector En=Un-Uexn;
    ErrL2=std::sqrt(std::abs((M*En)|En));
    thePrintStream<<"  Laplace with Neumann, err L2 = "<<ErrL2<<eol; 
    nbErrors+= checkValue(ErrL2, 0, tol(order), errors, "Laplace with Neumann condition, err L2");
  }
  //transmission problem
  Segment S12(_xmin=1,_xmax=2,_nnodes=10,_domain_name="Omega2",_side_names=Strings("x=1","x=2"));
  Mesh m2(S12, _order=1, _generator=_structured, _name="P1-mesh of [0,2], step = 0.01");
  Domain omega2 = m2.domain("Omega2");
  Domain sigma1 = m2.domain("x=1"), sigma2 = m2.domain("x=2");
  Number order=1;
  Space V1(_domain=omega, _FE_type=Lagrange, _order=order, _name="V1", _optimizeNumbering);
  Space V2(_domain=omega2, _FE_type=Lagrange, _order=order, _name="V2", _optimizeNumbering);
  Unknown u1(V1, _name="u1"); TestFunction v1(u1, _name="v1");
  Unknown u2(V2, _name="u2"); TestFunction v2(u2, _name="v2");
  // bilinear form and linear form
  BilinearForm atr = intg(omega, grad(u1)|grad(v1))+intg(omega2, grad(u2)|grad(v2));
  EssentialConditions ecst = ((u1|sigmaR) - (u2|sigma1) = 0);
  TermMatrix At(atr, ecst, _name="At");
  TermVector F1(intg(omega, fn*v1), _name="F1");
  TermVector Ut=directSolve(At,F1);
  if (SaveToFile)
  {
    saveToFile("U1dtr",Ut,_format=_vtu);
    nbErrors+=checkValues("U1dtr_V1_Omega.vtu", rootname+"/U1dtr_V1.in", tol(OrderMax), errors, "TermVector U1dtr_V1", check);
    nbErrors+=checkValues("U1dtr_V2_Omega.vtu", rootname+"/U1dtr_V2.in", tol(OrderMax), errors, "TermVector U1dtr_V1", check);
  }
  // TermVector Uex(u,omega,uex,"Uex");
  // TermVector E=U-Uex;
  // ssout<<"  Laplace with Dirichlet condition, err L2 = "<<std::sqrt(std::abs((M*E)|E))<<eol;
  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  trace_p->pop();
  
}

}
