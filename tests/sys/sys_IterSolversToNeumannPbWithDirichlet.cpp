/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_IterSolversToNeumannPbWithDirichlet.cpp
\authors E. Luneville
\since 13 jan 2018
\date 13 jan 2018

Test   all iterative solvers with a matrix using operand \f$-\Delta+Id\f$
  A = intg( omg, grad(u)|grad(v) + u * v)
  solve A*X =S
  ans compare to results with DirectSolve
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using namespace xlifepp;

namespace sys_IterSolversToNeumannPbWithDirichlet
{

 Real K=10.;
 Number nbErrors=0;
// sol exacte
Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real Uex=K*sin(pi_*x)*sin(pi_*y);
  return Uex;
}
Real uex0(const Point& P, Parameters& pa = defaultParameters)
{
  return uex(P);
}
Real uex2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real Uex2=(2.+ K*sin(pi_*x)*sin(pi_*y));
  return Uex2;
}
//Dirichlet conditions
Real gDir0f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return 0.;
}
Real gDir2f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real epsilon=1.0e-5;
  Real cd1=2.;
  return cd1;
}
Real gDir0(const Point& P, Parameters& pa = defaultParameters)
{
  return gDir0f(P);
}
Real gDir2(const Point& P, Parameters& pa = defaultParameters)
{
  return gDir2f(P);
}

//RHS
Real g(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return (2*pi_*pi_+1)*(sin(pi_*x)*sin(pi_*y));
}
Real g0(const Point& P, Parameters& pa = defaultParameters)
{
  return g(P);
}
Real g2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real G2=K*(2*pi_*pi_+1)*(sin(pi_*x)*sin(pi_*y)) + 2;
  return G2;
}

void checkSolver(const String& solver, TermMatrix& A, TermVector& B, TermVector& X0, TermVector& Uex, Real eps, Number maxit,
                 Preconditioner& pdiag, Preconditioner& pildlt,
                 Vector<Vector<String>>& FinalResults, bool withTime=false)
{
  TermVector X;
  Vector<String> OneTest(5);
  String errors;
  Real et=0, epsc=10*eps;
  Number ms=37;
  const Number krylovDim = 3*sqrt(A.numberOfCols());
  theCout << "================================= "<<solver<<" ================================="<<eol;
  theCout << "* without Preconditionner" << eol;
  String m=solver+" without preconditionner", m1="test "+m+" / Direct Solve", YN;
  if (m.size()<ms) m.append(ms-m.size(),' ');
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  nbErrors+=checkValuesN2(Uex, X, epsc, errors, m1);
  printCompDirectIter(YN, m, A, B, X, Uex, epsc, OneTest, et);
  FinalResults.push_back(OneTest);
  theCout <<"* with Preconditionner (diag)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  m=solver+" with preconditionner diag", m1="test "+m+" / Direct Solve";
  if (m.size()<ms) m.append(ms-m.size(),' ');
  nbErrors+=checkValuesN2(Uex, X, epsc, errors, m1);
  printCompDirectIter(YN, m, A, B, X, Uex, epsc, OneTest, et);
  FinalResults.push_back(OneTest);
  theCout <<"* with Preconditionner (ildlt)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  m=solver+" with preconditionner ildlt", m1="test "+m+" / Direct Solve";
  if (m.size()<ms) m.append(ms-m.size(),' ');
  nbErrors+=checkValuesN2(Uex, X, epsc, errors, m1);
  printCompDirectIter(YN, m, A, B, X, Uex, epsc, OneTest, et);
  FinalResults.push_back(OneTest);
}

Number Neumann_Pk(const GeomDomain& omg, const GeomDomain& gamma, int k,  Real h, Number Dir, std::ostream& out)
{
  stringstream s;
  trace_p->push("Neumann_Pk");
  verboseLevel(0);
  Number nbErrors;
  String errors;
  Vector<String> OneTest(5);
  Vector<Vector<String> > FinalResults;
  OneTest(1)="Nom                   ";
  OneTest(2)="     X?~=?U_direct";
  OneTest(3)= "   err=||X-UD||/||UD||  ";
  OneTest(4)="  _nbIterations  ";
  OneTest(5)=" time";
  String YN="Y";
  bool withTime=true;
  //create Lagrange Pind space and unknown
  Space Vk(_domain=omg, _FE_type=Lagrange, _order=k, _name="Vk", _optimizeNumbering);
  Unknown u(Vk, _name="u");  TestFunction v(u, _name="v");
  BilinearForm auv=intg(omg,  grad(u)|grad(v))+ intg(omg, u * v);
  EssentialConditions ecs;
  // Create and compute term matrix according to Dichlet conditions:
  // Dir=0 <=> homogeneous
  // Dir=1 <=> non homogneous (=gDir0)
  // Dir=2 <=> non homogneous (=gDir)
  TermMatrix A;
  LinearForm f;
  TermVector B, Uex;
  String TypeTest;
  if (Dir == 2)
  {
    TypeTest="NON HOMOGENE";
    ecs = (u|gamma = 2.);
    A = TermMatrix(auv, ecs, _name="a(u,v)+ cdtn ess");
    f= intg(omg, g2*v);
    B= TermVector(f, _name="B");
    Uex= TermVector(u,omg,uex2,_name="Uex2");
  }
  else if (Dir == 1)
  {
    TypeTest="FAUSSEMENT HOMOGENE";
    ecs = (u|gamma = gDir0);
    A = TermMatrix(auv, ecs, _name="a(u,v) + cdtn ess");
    f= intg(omg, g0*v);
    B= TermVector(f,"B");
    Uex= TermVector(u,omg,uex0,_name="Uex0");
  }
  else if (Dir == 0)
  {
    TypeTest="HOMOGENE";
    A = TermMatrix(auv, _name="a(u,v)");
    f= intg(omg, g0*v);
    B= TermVector(f, _name="B");
    Uex= TermVector(u,omg,uex0,_name="Uex0");
  }
  out<<"Matrice"<<A.numberOfRows()<<"x"<<A.numberOfCols();
  out<<" nb non-zero coeficients of the matrix: "<<A.nnz()<<eol<<eol;
  Real et=0;
  const Real epsilon=1.e-6;
  const Number nbIteration = 5000;
  if (withTime) et = elapsedTime();
//================================================================================================
//           Direct solver
//================================================================================================
  theCout << "* direct solver " << eol;
  TermVector U=directSolve(A, B, true);
  if (withTime) et = elapsedTime();
  String m="direct Solver";
  if (m.size()<37) m.append(37-m.size(),' ');
  printCompDirectIter(YN,m, A, B, U, U, epsilon,  OneTest, et);
  FinalResults.push_back(OneTest);
//================================================================================================
//     Iterative solvers
//================================================================================================
  TermVector X0(u,omg,0.,_name="initial guess");
  PreconditionerTerm preDiag(A, _diagPrec);
  PreconditionerTerm preIldlt(A, _ildltPrec);
  checkSolver("CG",A,B,X0,U,epsilon, nbIteration, preDiag, preIldlt, FinalResults, withTime);
  checkSolver("BICG",A,B,X0,U,epsilon, nbIteration, preDiag, preIldlt, FinalResults, withTime);
  checkSolver("BICGSTAB",A,B,X0,U,epsilon, nbIteration, preDiag, preIldlt, FinalResults, withTime);
  checkSolver("CGS",A,B,X0,U,epsilon, nbIteration, preDiag, preIldlt, FinalResults, withTime);
  checkSolver("GMRES",A,B,X0,U,epsilon, nbIteration, preDiag, preIldlt, FinalResults, withTime);
  checkSolver("QMR",A,B,X0,U,epsilon, nbIteration, preDiag, preIldlt, FinalResults, withTime);

  theCout << "\nCas "<<TypeTest<<eol;
  for(auto & r: FinalResults)
  {
    if (r.size()>1) theCout << r[0] << " "<<r[1] << " "<< std::setw(14)<<r[2] << " "<<std::setw(5)<< r[3] << " " << std::setw(6)<<r[4] <<eol;
  }

  trace_p->pop();
  return nbErrors;
}

// Programme principal:
void sys_IterSolversToNeumannPbWithDirichlet(int argc, char* argv[], bool check)
{
  String rootname = "sys_IterSolversToNeumannPbWithDirichlet";
  trace_p->push(rootname);

  //numberOfThreads(1);

  std::stringstream out;
  out.precision(testPrec);
  string meshInfo;
  verboseLevel(2);
  Number nbDivMin, nbDivMax, dd=1;
  Number ordMin, ordMax;
  Real h;
  string nmFich;
  Number NbLignes;
  Number NbSolvers = 2;
  Number nbErrors=0;
  String errors;

  // out << "==========================================" << eol;
  // out << "=                                    TEST 2D                                       =" << eol;
  // out << "==========================================" << eol<<eol;
  //create a mesh and Domains TRIANGLES
  std::stringstream NmEps1;
  meshInfo= " Triangle mesh of [0,1]x[0,1] with nbStep =  ";
  nbDivMin =6, nbDivMax=6;
  ordMin = 1, ordMax=1;
  NbLignes= (nbDivMax - nbDivMin + 1) * (ordMax - ordMin +1) * (NbSolvers+2)+1;
  for (Number div = nbDivMin; div <= nbDivMax; div+=1)
  {
    meshInfo += tostring(div);
    Real h=1./(dd*div);
    Number nn=std::pow(2,div)+1;
    Mesh mesh2d(SquareGeo(_origin=Point(0.,0.), _length=1, _nnodes=nn, _domain_name="Omega", _side_names="Gamma"), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[1,2]");
    Domain omega = mesh2d.domain("Omega");
    Domain gamma = mesh2d.domain("Gamma");
    out <<eol<<eol<< "===========================================" << eol;
    out << "# Triangle mesh:  " << div << " sudivisions, h = " <<h <<eol;
    out << "===========================================" << eol;
    for (Number ord=ordMin; ord<=ordMax; ord++)
    {
      out<<"--------------------------" <<eol;
      out << " -> interpolation P" <<ord<< eol;
      out<<"--------------------------" <<eol;
      thePrintStream<<eol<<"***************************************"<<std::endl;
      thePrintStream<<"******  Dirichlet homogene  ***********"<<std::endl;
      thePrintStream<<"***************************************"<<std::endl;
      out<<eol<<"***************************************"<<std::endl;
      out<<"******  Dirichlet homogene  ***********"<<std::endl;
      out<<"***************************************"<<std::endl;
      nbErrors+=Neumann_Pk(omega, gamma, ord, h, 0, out);
      // std::cout<<eol<<"*******************************************"<<std::endl;
      // std::cout<<"******  Dirichlet faussement NON homogene  ***********"<<std::endl;
      // std::cout<<"*******************************************"<<std::endl;
      // out<<eol<<"*******************************************"<<std::endl;
      // out<<"******  Dirichlet faussement NON homogene  ***********"<<std::endl;
      // out<<"*******************************************"<<std::endl;
      //   nbErrors+=Neumann_Pk(omega, gamma, ord, h, 1, out);
      thePrintStream<<eol<<"*************************************************"<<std::endl;
      thePrintStream<<"**********  Dirichlet   NON homogene  ***********"<<std::endl;
      thePrintStream<<"*************************************************"<<std::endl;
      out<<eol<<"*************************************************"<<std::endl;
      out<<"**********  Dirichlet   NON homogene  ***********"<<std::endl;
      out<<"***********************************************"<<std::endl;
      nbErrors+=Neumann_Pk(omega, gamma, ord, h, 2, out);
    }
  }

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
