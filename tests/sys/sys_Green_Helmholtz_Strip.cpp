/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lun�ville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file sys_Green_Helmholtz_Strip.cpp
  \author E. Lun�ville
  \since 22 jun 2018
  \date 22 jun 2018

  test of Helmholtz Green function in a 2D strip

*/
#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_Green_Helmholtz_Strip
{

void sys_Green_Helmholtz_Strip(int argc, char* argv[], bool check)
{
  String rootname = "sys_Green_Helmholtz_Strip";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00008;

  Real k=20, h=1., l=0.1,eps=1E-8;
  Number n=1000000;

  Mesh mesh2d(Rectangle(_xmin=-0.5,_xmax=0.5,_ymin=0,_ymax=1,_nnodes=Numbers(500,500),_domain_name="Omega"), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Space V(_domain=omega, _interpolation=_P1, _name="V"); Unknown u(V,"u");

  //test Helmholtz kernel in a strip
  Complex r;
  Point S(0.,0.5);
  Kernel Kd=Helmholtz2dStripKernel(_Dirichlet,k,h,n,l,eps);

  ssout<<"Full modal method"<<eol;
  Kd("l")=0;
  elapsedTime();
  TermVector Td1(u,omega,Kd.kernel(S,_y));
  elapsedTime("modal method");
  saveToFile("kerDir_modal",Td1,_format=_vtu);
  ssout<<"Full image method"<<eol;
  Kd("l")=0.6; Kd("N")=100;
  elapsedTime();
  TermVector Td2(u,omega,Kd.kernel(S,_y));
  elapsedTime("image method");
  saveToFile("kerDir_image",Td2,_format=_vtu);
  ssout<<"hybrid method l="<<l<<eol;
  Kd("l")=0.1;
  elapsedTime();
  TermVector Td3(u,omega,Kd.kernel(S,_y));
  elapsedTime("hybrid method");
  saveToFile("kerDir_hybrid",Td3,_format=_vtu);

  ssout<<"|image-modal|="<<norm2(Td2-Td1)<<" |hybrid-modal|="<<norm2(Td3-Td1)<<" |hybrid-image|="<<norm2(Td3-Td2)<<eol;
  nbErrors+=checkValue(norm2(Td2-Td1), 0., tol, errors, "|image-modale|");
  nbErrors+=checkValue(norm2(Td3-Td1), 0., tol, errors, "|hybrid-modale|");
  nbErrors+=checkValue(norm2(Td3-Td2), 0., tol, errors, "|hybrid-modale|");
  nbErrors+=checkValue(ssout, rootname+"/Green_Helmoholtz_Strip.in", errors, "Green Helmholtz Strip", check);
  

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
