/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
Test Laplace problem with Neumann condition
   -delta(u)+u=f
   du/dn=0

\file sys_Laplace_Neumann.cpp
\author E. Lunéville
\since 16 mai 2013
\date 16 mai 2013
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_Laplace_Neumann {
//#define _TermVector(x,y) TermVector(x)((y),#x)
//#define _TermVector(x,y) TermVector x(y);x.name()=#x;
//      _TermVector(Er,U-Uex);
//#define _(x,y,z1) x y(z1);y.name()=#y;
//#define _(x,y,...) x y(__VA_ARGS__);y.name()=#y;
//      _(TermVector,Er,U-Uex);
//#define _Term(x,y,...) Term ##x y(__VA_ARGS__);y.name()=#y;
//       _Term(Vector,Er,U-Uex);

Real uno(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

Real cosx1(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1);
  return cos(pi_ * x);
}

Real cosx2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return cos(pi_ * x) * cos(pi_ * y);
}

Real cosx3(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  return cos(pi_ * x) * cos(pi_ * y) * cos(pi_ * z);
}

Real fx(const Point& P, Parameters& pa=defaultParameters)
{
  return P(1);
}
Real fy(const Point& P, Parameters& pa=defaultParameters)
{
  return P(2);
}
Real fz(const Point& P, Parameters& pa=defaultParameters)
{
  return P(3);
}

Real fx2(const Point& P, Parameters& pa=defaultParameters)
{
  return P(1)*P(1);
}
Real fy2(const Point& P, Parameters& pa=defaultParameters)
{
  return P(2)*P(2);
}
Real fz2(const Point& P, Parameters& pa=defaultParameters)
{
  return P(3)*P(3);
}

ofstream fout;

void Laplace_Neumann_Pk(const GeomDomain& omg, int k,  Real h, const String& E, Vector<Real>& errRel, CoutStream& out)
{
  stringstream s;
  trace_p->push("Laplace_Neumann_Pk");

  bool save=false;
  //create Lagrange Pind space and unknown
  Space Vk(_domain=omg, _FE_type=Lagrange, _order=k, _name="Vk", _notOptimizeNumbering); //Pb with mesh pyramidl et ord=2
  Unknown u(Vk, _name="u");  TestFunction v(u, _name="v");

  //create bilinear form and linear form
  BilinearForm muv=intg(omg, u * v);
  BilinearForm auv = intg(omg, grad(u) | grad(v)) + intg(omg, u * v);
  BilinearForm ruv=intg(omg, grad(u) | grad(v));

  LinearForm fv;
  Real a;
  TermVector Uex;
  TermVector un(u,omg,1., _name="un");

  if (omg.dim()==1)
  {
    fv=intg(omg, cosx1 * v);
    Uex = TermVector(u, omg, cosx1);
    a=1. + pi_ * pi_;
  }

  if(omg.dim()==2)
  {
    fv=intg(omg, cosx2 * v);
    Uex = TermVector(u, omg, cosx2);
    a=1. + 2.*pi_ * pi_;
  }
  if(omg.dim()==3)
  {
    fv=intg(omg, cosx3 * v);
    Uex=TermVector(u, omg, cosx3);
    a = 1. + 3.*pi_ * pi_;
  }
  Uex /=a;

  elapsedTime();

  // create FEterm
  TermMatrix A(auv, _name="a(u,v)");
  TermMatrix R(ruv, _name="R");
  TermMatrix M(muv, _name="M");
  TermVector B(fv, _name="f(v)");
  elapsedTime("compute  matrices "+tostring(omg.dim())+"D");

  // check FE computation
  if(omg.dim()==2)
  {
    TermVector X(u,omg,fx, _name="x");
    TermVector Y(u,omg,fy, _name="y");
    TermVector X2(u,omg,fx2, _name="X2");
    TermVector Y2(u,omg,fy2, _name="Y2");
    out<<"-----------------------------------------------------------------------------------------"<<eol;
    out<<"FE computation check "<< E << k << ", h=" << h << ", nb dl=" << Uex.size() << eol;
    out<<"   norminfty(R*1) = "<<norminfty(R*un);
    out<<", (M*1|1) = "<<(M*un|un);
    out<<", (A*1|1) = "<<(A*un|un)<<eol;
    out<<"  (R*x|x) = "<<(R*X|X);
    out<<", (R*y|y) = "<<(R*Y|Y);
    out<<", (R*x2|x2) = "<<(R*X2|X2);
    out<<", (R*y2|y2) = "<<(R*Y2|Y2);
    out<<", (B|1) = "<<(B|un)<<eol;
    out<<"-----------------------------------------------------------------------------------------"<<eol;
  }

  if(omg.dim()==3)
  {
    TermVector X(u,omg,fx, _name="x");
    TermVector Y(u,omg,fy, _name="y");
    TermVector Z(u,omg,fz, _name="z");
    TermVector X2(u,omg,fx2, _name="X2");
    TermVector Y2(u,omg,fy2, _name="Y2");
    TermVector Z2(u,omg,fz2, _name="Z2");
    out<<"-----------------------------------------------------------------------------------------"<<eol;
    out<<"FE computation check "<< E << k << ", h=" << h << ", nb dl=" << Uex.size() << eol;
    out<<"   norminfty(R*1) = "<<norminfty(R*un);
    out<<", (M*1|1) = "<<(M*un|un);
    out<<", (A*1|1) = "<<(A*un|un)<<eol;
    out<<"  (R*x|x) = "<<(R*X|X);
    out<<", (R*y|y) = "<<(R*Y|Y);
    out<<", (R*z|z) = "<<(R*Z|Z);
    out<<", (R*x2|x2) = "<<(R*X2|X2);
    out<<", (R*y2|y2) = "<<(R*Y2|Y2);
    out<<", (R*z2|z2) = "<<(R*Z2|Z2);
    out<<", (B|1) = "<<(B|un)<<eol;
    out<<"-----------------------------------------------------------------------------------------"<<eol;
  }

  SuTermMatrix* suterm = A.begin()->second;

  LargeMatrix<Complex>* cA = suterm->entries()->cEntries_p;
  Number cRow, cCol, rRow, rCol,nZA;
  if (0 != cA)
  {
    out << "Complex matrix ";
    out << " storage :" << words("storage type",cA->storageType());
    cRow = cA->nbRows;
    cCol = cA->nbCols;
    out << " matrix size " << cRow << " x " << cCol ;
    out << " mumber non-zero " << cA->nbNonZero() << std::endl;
  }

  LargeMatrix<Real>* rA = suterm->entries()->rEntries_p;
  if (0 != rA)
  {
    out << "Real matrix ";
    out << " storage :" << words("storage type",rA->storageType());
    rRow = rA->nbRows;
    rCol = rA->nbCols;
    out << " size " <<  rRow << " x " << rCol;
    out << " number non-zero " << rA->nbNonZero() << std::endl;
    nZA=rA->nbNonZero();
  }
  //solve linear systesm AX=F using direct method
  TermVector U = directSolve(A, B);

  TermVector res=A*U-B;
  out<<"residu = "<<norminfty(res)<<eol;

  //compute errors
  TermVector Dq = U - Uex;
  Real errL2, errH1, errC0, nL2, nH1, nC0, cpu;
  errL2 = sqrt(abs(M * Dq | Dq)) ;
  errH1 = sqrt(abs(A * Dq | Dq)) ;
  errC0= norminfty(Dq);
  nL2 = sqrt(abs(M * Uex | Uex));
  nH1 = sqrt(abs(A * Uex | Uex));
  nC0 = norminfty(Uex);
  cpu = elapsedTime();
  errRel(1)=errL2 / nL2;
  errRel(2)=errH1 / nH1;
  errRel(3)=errC0 / nC0;

  out << "--------------- "<<E << k << ", h=" << h << ", nb dl=" << U.size() << " ---------------" << eol;
  out << "L2 norm Uex = "<<nL2<<", L2 Error = " << errL2 << ", Rel. L2 error = " << errRel(1) << eol;
  out << "H1 norm Uex = "<<nH1<<", H1 error = " << errH1 << ", Rel. H1 error = " << errRel(2) << eol;
  out << "C0 norm Uex = "<<nC0<<", C0 error = " << errC0 << ", Rel. C0 error = " << errRel(3) << eol;
  out << " -> cpu time = " << cpu << eol;

  fout<<k<<" "<<h<<" "<<U.size()<<" "<<rA->nbNonZero()<<" "<<errL2<<" "<<errH1<<" "<<cpu<<eol;

  if (save)
  {
    saveToFile("U",U,_format=_vtu);
    saveToFile("Uex",Uex,_format=_vtu);
  }
  trace_p->pop();
}


//
// Programme principal:
void sys_Laplace_Neumann(int argc, char* argv[], bool check)
{
  String rootname = "sys_Laplace_Neumann";
  trace_p->push(rootname);

  //numberOfThreads(1);

   bool doSegment     = false,
        doTriangle    = false,
        doQuadrangle  = false,
        doTetrahedron = false,
        doHexahedron  = false,
        doPrism       = false,
        doPyramid     = false,
        doAll         = true;

   string meshInfo;
   verboseLevel(0);
   String errors;
   Number nbErrors = 0;
   Real tol=0.0001;
   Number nbDivMin = 1, nbDivMax=3;
   Number ordMin =1, ordMax=3;
   std::vector<String> sidenames(4, "");
   Real h;
   Vector<Real> errRel(3);
   Number nbCases=18;
   Matrix<Real> MErrRel(3,nbCases);
   Number iCase=0;

  if (doSegment || doAll)
  {
    nbDivMax=1;
    ordMin=1;
    ordMax=3;
    Number fac=8;
    thePrintStream <<"====================================================================================" << eol;
    thePrintStream << "=                                TEST 1D Segment                                   =" << eol;
    thePrintStream << "====================================================================================" << eol<<eol;
    // create a mesh and Domains SEGMENT
    meshInfo= " mesh of [0,1] with nbStep =  ";

    fout.open("segment_errors.dat");
    for(Number div = nbDivMin; div <= nbDivMax; div++)
    {
      Number n=fac*div;
      meshInfo += tostring(n);
      h=1./n;
      Mesh mesh1d;
      if (!check)
      {
        mesh1d=Mesh(Segment(_xmin=0,_xmax=1,_nnodes=n+1,_domain_name="Omega"), 1, _structured, meshInfo);
        mesh1d.saveToFile(inputsPathTo(rootname+"/mesh1d_"+tostring(div)+".msh"),_msh);
      }
      else
      {
        mesh1d=Mesh(inputsPathTo(rootname+"/mesh1d_"+tostring(div)+".msh"),"Segment",_msh);
      }
      Domain omega = mesh1d.domain("Omega");
      thePrintStream << "===========================================" << eol;
      thePrintStream << "# SEGMENT mesh:Segment : " << n << " sudivisions, h = " <<h <<eol;
      thePrintStream << "===========================================" << eol;

      for (Number ord=ordMin; ord<=ordMax; ord++)
      {
        thePrintStream << " -> Segments P" <<ord<< "-------------------" <<eol;
        Laplace_Neumann_Pk(omega, ord, h,"P", errRel, theCout);
        // nbErrors+= checkValues(errRel, rootname+"/errRelRef_Seg_"+tostring(div)+"_"+tostring(ord)+".in",  tol, errors, "Rectangle(Segment)", check);
        iCase++;
        MErrRel.column(iCase, errRel);
      }
    }
    fout.close();
  }
  thePrintStream << eol;

  if (doTriangle || doAll)
  {
    nbDivMin = 2;
    nbDivMax = 2;
    ordMin=1;
    ordMax=3;
    thePrintStream << "====================================================================================" << eol;
    thePrintStream << "=                             TEST 2D Triangle                                     =" << eol;
    thePrintStream << "====================================================================================" << eol<<eol;
    // create a mesh and Domains TRIANGLES
    meshInfo= " TRIANGULAR mesh of [0,1]x[0,1] with nbStep =  ";

    fout.open("triangle_errors.dat");
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      meshInfo += tostring(div);
      h=1./(4*div);
      // Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=4*div+1), _shape=_triangle, _order=1, _generator=_structured, _name=meshInfo);
      // mesh2d.saveToFile(inputsPathTo(rootname+"/mesh2d_trg_"+tostring(div)+".msh"),_msh);
      // Mesh mesh2d(inputsPathTo(rootname+"/mesh2d_trg_"+tostring(div)+".msh"), _name="Rectangle(triangle)");

      Mesh mesh2dT;
      if (!check)
      {
        mesh2dT=Mesh(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=4*div+1), _shape=_triangle, _order=1, _generator=_structured, _name=meshInfo);
        mesh2dT.saveToFile(inputsPathTo(rootname+"/mesh12_trg_"+tostring(div)+".msh"),_msh);
      }
      else
      {
        mesh2dT=Mesh(inputsPathTo(rootname+"/mesh2d_trg_"+tostring(div)+".msh"), _name="Rectangle(triangle)");
      }
      Domain omega = mesh2dT.domain("Omega");
      thePrintStream << "===========================================" << eol;
      thePrintStream << "# TRIANGULAR mesh:Rectangle(Triangle) : " << div << " sudivisions, h = " <<h <<eol;
      thePrintStream << "===========================================" << eol;
      for (Number ord=ordMin; ord<=ordMax; ord++)
      {
        thePrintStream << " -> Triangles P" <<ord<< "-------------------" <<eol;
        Laplace_Neumann_Pk(omega, ord, h,"P", errRel, theCout);
        // nbErrors+= checkValues(errRel, rootname+"/errRelRef_Trg_"+tostring(div)+"_"+tostring(ord)+".in",  tol, errors, "Rectangle(Triangle)", check);
        iCase++;
        MErrRel.column(iCase, errRel);
      }
    }
    fout.close();
  }
  thePrintStream << eol;

  if (doQuadrangle || doAll)
  {
    nbDivMin = 2;
    nbDivMax = 2;
    ordMin=1;
    ordMax=3;
    thePrintStream << "====================================================================================" << eol;
    thePrintStream << "=                           TEST 2D Quadrangle                                     =" << eol;
    thePrintStream << "====================================================================================" << eol<<eol;
    // create a mesh and Domains QUADRANGLES sidenames[0] = "x=0";
    meshInfo= " QUADRANGULAR mesh of [0,1]x[0,1] with nbStep =  ";
    fout.open("quadrangle_errors.dat");
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      meshInfo += tostring(div);
      h=1./(4*div);
      // Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=4*div+1), _shape=_quadrangle, _order=1, _generator=_structured, _name=meshInfo);
      // mesh2d.saveToFile(inputsPathTo(rootname+"/mesh2d_quad_"+tostring(div)+".msh"),_msh);
      // Mesh mesh2d(inputsPathTo(rootname+"/mesh2d_quad_"+tostring(div)+".msh"), _name="Rectangle(quadrangle)");

      Mesh mesh2d;
      if (!check)
      {
        mesh2d=Mesh(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=4*div+1), _shape=_quadrangle, _order=1, _generator=_structured, _name=meshInfo);
        mesh2d.saveToFile(inputsPathTo(rootname+"/mesh2d_quad_"+tostring(div)+".msh"),_msh);
      }
      else
      {
        mesh2d=Mesh(inputsPathTo(rootname+"/mesh2d_quad_"+tostring(div)+".msh"), _name="Rectangle(quadrangle)");
      }

      Domain omega=mesh2d.domain("Omega");
      thePrintStream << "=============================================" << eol;
      thePrintStream << " #  QUADRANGULAR mesh  Rectangle(quadrangle): " << div << " sudivisions, h = " <<h <<eol;
      thePrintStream << "==============================================" << eol;
      for (Number ord=ordMin; ord<=ordMax; ord++)
      {
        thePrintStream << " -> Quadrangles Q" <<ord<< eol;
        Laplace_Neumann_Pk(omega, ord, h,"Q", errRel, theCout);
        // nbErrors+= checkValues(errRel, rootname+"/errRelRef_Quad_"+tostring(div)+"_"+tostring(ord)+".in",  tol, errors, "Rectangle(quadrangle)", check);
        iCase++;
        MErrRel.column(iCase, errRel);
      }
    }
    fout.close();
  }
  thePrintStream << eol;

  // create a mesh and Domains TETRAHEDRON
  if (doTetrahedron || doAll)
  {
    thePrintStream << "====================================================================================" << eol;
    thePrintStream << "=                           TEST 3D Tetrahedron                                    =" << eol;
    thePrintStream << "====================================================================================" << eol<<eol;
    meshInfo=" TETRAHEDRAL mesh of cube ";
    nbDivMin = 2;
    nbDivMax = 2;
    ordMin=1;
    ordMax=3;
    fout.open("tetrahedron_errors.dat");
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      // Mesh mesh3d(Cube(_center=Point(0.5,0.5,0.5),_length=1.,_nnodes=4*div), _shape=_tetrahedron, _order=1, _generator=_subdiv, _name=meshInfo);
      // mesh3d.saveToFile(inputsPathTo(rootname+"/mesh3d_tetra_"+tostring(div)+".msh"),_msh);
      // Mesh mesh3d(inputsPathTo(rootname+"/mesh3d_tetra_"+tostring(div)+".msh"), _name="Cube(Tetrahedron)");

      Mesh mesh3d;
      if (!check)
      {
        mesh3d=Mesh(Cube(_center=Point(0.5,0.5,0.5),_length=1.,_nnodes=4*div), _shape=_tetrahedron, _order=1, _generator=_subdiv, _name=meshInfo);
        mesh3d.saveToFile(inputsPathTo(rootname+"/mesh3d_tetra_"+tostring(div)+".msh"),_msh);
      }
      else
      {
        mesh3d=Mesh(inputsPathTo(rootname+"/mesh3d_tetra_"+tostring(div)+".msh"), _name="Cube(Tetrahedron)");
      }

      mesh3d.renameDomain(0,"Omega");
      Domain omega = mesh3d.domain("Omega");
      h=1./(4*div);
      //h = 2. / std::pow(2,static_cast<int>(div));
      meshInfo += tostring(div);
      thePrintStream << "=========================================" << eol;
      thePrintStream << "# TETRAHEDRAL mesh Cube(tetrahedron):  " << div << " sudivisions, h = " <<h <<eol;
      thePrintStream << "=========================================" << eol;
      for(Number ord = ordMin; ord <= ordMax; ord++)
      {
        thePrintStream << " -> Tetrahedra P" <<ord<< eol;
        Laplace_Neumann_Pk(omega, ord, h, "P", errRel, theCout);
        // nbErrors+= checkValues(errRel, rootname+"/errRelRef_Tetra_"+tostring(div)+"_"+tostring(ord)+".in",  tol, errors, "Cube(tetrahedron)", check);
        iCase++;
        MErrRel.column(iCase, errRel);
      }
    }
    fout.close();
  }
  thePrintStream << eol;

  // create a hexaedron mesh and domains
  if (doHexahedron || doAll)
  {
    nbDivMin=2;
    nbDivMax=2;
    ordMin=1;
    ordMax=3;
    thePrintStream << "====================================================================================" << eol;
    thePrintStream << "=                           TEST 3D Hexahedron                                     =" << eol;
    thePrintStream << "====================================================================================" << eol<<eol;
    meshInfo=" HEXAHEDRAL mesh of cube [0,1]x[0,1]x[0,1] ";
    fout.open("hexahedron_errors.dat");
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      // Mesh mesh3d(Cuboid(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_zmin=0,_zmax=1,_nnodes=4*div+1), _shape=_hexahedron, _order=1, _generator=_structured, _name=meshInfo);
      // mesh3d.saveToFile(inputsPathTo(rootname+"/mesh3d_hexa_"+tostring(div)+".msh"),_msh);
      // Mesh mesh3d(inputsPathTo(rootname+"/mesh3d_hexa_"+tostring(div)+".msh"), _name="Cuboid hexa");

      Mesh mesh3d;
      if (!check)
      {
        mesh3d=Mesh(Cuboid(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_zmin=0,_zmax=1,_nnodes=4*div+1), _shape=_hexahedron, _order=1, _generator=_structured, _name=meshInfo);
        mesh3d.saveToFile(inputsPathTo(rootname+"/mesh3d_hexa_"+tostring(div)+".msh"),_msh);
      }
      else
      {
        mesh3d=Mesh(inputsPathTo(rootname+"/mesh3d_hexa_"+tostring(div)+".msh"), _name="Cuboid hexa");
      }


      Domain omega = mesh3d.domain("Omega");
      h =1./(4*div);
      meshInfo += tostring(div);
      thePrintStream << "==============================================" << eol;
      thePrintStream << " #  HEXAHEDRAL mesh Cuboid hexaedron:   " << div << " sudivisions, h = " <<h <<eol;
      thePrintStream << "==============================================" << eol;
      for (Number ord = ordMin; ord <= ordMax; ord++)
      {
        thePrintStream << " -> Hexahedra Q" <<ord<< eol;
        Laplace_Neumann_Pk(omega, ord, h, "Q", errRel, theCout);
        // nbErrors+= checkValues(errRel, rootname+"/errRelRef_hexa_"+tostring(div)+"_"+tostring(ord)+".in",  tol, errors, "Cuboid hexaedron", check);
        iCase++;
        MErrRel.column(iCase, errRel);
      }
    }
    fout.close();
  }
  thePrintStream << eol;

  if (doPrism || doAll)
  {
    nbDivMin=2;
    nbDivMax=2;
    ordMin=1;
    ordMax=2;
    thePrintStream << "====================================================================================" << eol;
    thePrintStream << "=                           TEST 3D Prism order 1/2                                =" << eol;
    thePrintStream << "====================================================================================" << eol<<eol;
    meshInfo=" mesh of cube [0,1]x[0,1]x[0,1] from 2D extrusion";
    fout.open("prism_errors.dat");
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      h =1./(4*div);
      // Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=4*div+1), _shape=_triangle, _order=1, _generator=_structured, _name=meshInfo);
      // Mesh mesh2d(inputsPathTo(rootname+"/mesh2d_trg_"+tostring(div)+".msh"), _name="Rectangle(triangle)");

      // Mesh mesh3d=extrude(mesh2d,Reals(0.,0.,1.),4*div);
      // mesh3d.saveToFile(inputsPathTo(rootname+"/mesh3d_prisma_"+tostring(div)+".msh"),_msh);
      Mesh mesh3d;
      if (!check)
      {
        Mesh mesh2d;
        if (!doTriangle || !doAll)
        {
          mesh2d=Mesh(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=4*div+1), _shape=_triangle, _order=1, _generator=_structured, _name=meshInfo);
          mesh2d.saveToFile(inputsPathTo(rootname+"/mesh2d_trg_"+tostring(div)+".msh"),_msh);
        }
        else
        {
          mesh2d=Mesh(inputsPathTo(rootname+"/mesh2d_trg_"+tostring(div)+".msh"), _name="Cuboid hexa");
        }
        mesh3d=extrude(mesh2d, Translation(0.,0.,1./(4*div)), _layers=4*div);
        mesh3d.saveToFile(inputsPathTo(rootname+"/mesh3d_prisma_"+tostring(div)+".msh"),_msh);
      }
      else
      {
        mesh3d=Mesh(inputsPathTo(rootname+"/mesh3d_prisma_"+tostring(div)+".msh"), _name="Prismatic");
      }
      Domain omega = mesh3d.domain("Omega");
      mesh3d.saveToFile("mesh3d_prism_"+tostring(div),_msh);

      meshInfo += tostring(div);
      thePrintStream << "==============================================" << eol;
      thePrintStream << " #  PRISMATIC mesh: Prismatic  " << div << " sudivisions, h = " <<h <<eol;
      thePrintStream << "==============================================" << eol;
      for (Number ord = ordMin; ord <= ordMax; ord++)
      {
        thePrintStream << " -> Prism order " <<ord<< eol;
        Laplace_Neumann_Pk(omega, ord, h, "Pr", errRel, theCout);
        // nbErrors+= checkValues(errRel, rootname+"/errRelRef_Prisma_"+tostring(div)+"_"+tostring(ord)+".in",  tol, errors, "Prismatic", check);
        iCase++;
        MErrRel.column(iCase, errRel);
      }
    }
    fout.close();
  }
  thePrintStream << eol;

  if (doPyramid || doAll)
  {
    nbDivMin=2;
    nbDivMax=2;
    ordMin=1;
    ordMax=1;
    thePrintStream << "====================================================================================" << eol;
    thePrintStream << "=                           TEST 3D Pyramid order 1/2                                =" << eol;
    thePrintStream << "====================================================================================" << eol<<eol;
    meshInfo=" mesh of cube [0,1]x[0,1]x[0,1]";
    fout.open("prism_errors.dat");
    for(Number div = nbDivMin; div <= nbDivMax; div++)
    {
      h =1./(4*div);
      // Mesh mesh3d(Cuboid(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_zmin=0,_zmax=1,_nnodes=4*div+1), _shape=_hexahedron, _order=1, _generator=_structured, _name=meshInfo);
      // Mesh mesh3d(inputsPathTo(rootname+"/mesh3d_hexa_"+tostring(div)+".msh"), _name="Cuboid");
      // Mesh mesh3dPy(mesh3d, _pyramid);
      // mesh3dPy.saveToFile(inputsPathTo(rootname+"/mesh3d_pyramid_"+tostring(div)+".msh"), _msh);
      // Mesh mesh3dPy(inputsPathTo(rootname+"/mesh3d_pyramid_"+tostring(div)+".msh"), _name="Pyramid");


      Mesh mesh3dPy;
      if (!check)
      {
        Mesh mesh3d;
        if( !doHexahedron || !doAll)
        {
          mesh3d=Mesh(Cuboid(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_zmin=0,_zmax=1,_nnodes=4*div+1), _shape=_hexahedron, _order=1, _generator=_structured, _name=meshInfo);
          mesh3d.saveToFile(inputsPathTo(rootname+"/mesh3d_hexa_"+tostring(div)+".msh"),_msh);
        }
        else
        {
          mesh3d=Mesh(inputsPathTo(rootname+"/mesh3d_hexa_"+tostring(div)+".msh"), _name="Cuboid hexa");
        }
        mesh3dPy=Mesh(mesh3d, _pyramid);
        mesh3dPy.saveToFile(inputsPathTo(rootname+"/mesh3d_pyramid_"+tostring(div)+".msh"), _msh);
      }
      else
      {
        mesh3dPy=Mesh(inputsPathTo(rootname+"/mesh3d_pyramid_"+tostring(div)+".msh"), _name="Pyramid");
      }

      Domain omega = mesh3dPy.domain("Omega");
      mesh3dPy.saveToFile("mesh3d_pyramid_"+tostring(div),_msh);

      meshInfo += tostring(div);
      thePrintStream << "==============================================" << eol;
      thePrintStream << " ###  PYRAMIDAL mesh Cube(Pyramid):   " << div << " sudivisions, h = " <<h <<eol;
      for(Number ord = ordMin; ord <= ordMax; ord++)
      {
        thePrintStream << " -> Pyramid order " <<ord<< eol;
        Laplace_Neumann_Pk(omega, ord, h, "Py", errRel, theCout);
        // nbErrors+= checkValues(errRel, rootname+"/errRelRef_Pyram_"+tostring(div)+"_"+tostring(ord)+".in",  tol, errors, "Cube(Pyramid)", check);
        iCase++;
        MErrRel.column(iCase, errRel);
      }
    }
    fout.close();
  }
  thePrintStream << eol;
  nbErrors+= checkValues(MErrRel, rootname+"/errRelRef_AllCases.in",  tol, errors, "Complete results", check);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some reference values in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

}

}
