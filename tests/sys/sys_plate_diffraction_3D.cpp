/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_plate_diffraction_3D.cpp
	\since 17 sep 2014
	\date 17 jun 2014
	\author Eric Lunéville

	anisotropic plate with hybrid DtN - waveguide diffraction
	2D geometry - 3D unknown

	solve the elastic diffraction problem in a 2D section of 3D plate (z invariant)
	multi-layer anisotropic plate with stiffener

	                                      ...
	                  ______________________________________
                     /                y=r2(x)               \
                   _/                 layer 2                \_
	              |    ____________________________________    |
	              |   /               y=r1(x)              \   |
	              |__/                layer 1               \__|    reinforcment
	              |     ______________________________ ___     |
	              |    /              y=r0(x)             \    |
	              |___/                                    \___|
	         ---------------------------------------------------------            y=nh
	sigmaM_n |    x1 x2               layer n              x3  x4    | sigmaP_n
             ---------------------------------------------------------
	         |                          ...                          |
             ---------------------------------------------------------            y=2h
	sigmaM_2 |                        layer 2                        | sigmaP_2
             ---------------------------------------------------------            y=h
	sigmaM_1 |                        layer 1                        | sigmaP_1
	         ----------------------------------------------------------------->   y=0
            x=x0                                                   x=x0+L

	Main computation steps :
	 - compute modes in cross section of the infinite plate
	 - compute hybrid DtN operator
	 - compute solution
    Main parameters
     - k : frequency
     - theta : angle of incident plane waves

*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_plate_diffraction_3D
{

std::map<Int, Matrix<Real> > Cs, Cs2d;
std::vector<Int> orientations;

/* construct C_angle matrix when applying rotation around x axis (c=cos(t), s=sin(t))
           | 1   0   0    0    0  0 |
           | 0  c2  s2  -2cs   0  0 |
     R1 =  | 0  s2  c2   2cs   0  0 |    R2=inv(R1)=R1(-t)
           | 0  cs -cs  c2-s2  0  0 |
           | 0   0   0    0    c  s |
           | 0   0   0    0   -s  c |
 Warning : we do not use the standard Voigt notation e=(e11,e22,e33,2e23,2e13,2e12) but e=(e11,e22,e33,e23,e13,e12) (no factor 2)
*/
Matrix<Real> C_oriented_x(const Matrix<Real>& C, Real angle)
{
  Real t=pi_*angle/180, s= std::sin(t), c=std::cos(t);
  Real c2=c*c, s2=s*s, cs=c*s;
  Matrix<Real> R1(6,6), R2(6,6);
  R1(1,1)=1;
  R1(2,2)=c2; R1(2,3)=s2; R1(2,4)=-2*cs;
  R1(3,2)=s2; R1(3,3)=c2; R1(3,4)=2*cs;
  R1(4,2)=cs; R1(4,3)=-cs; R1(4,4)=c2-s2;
  R1(5,5)=c; R1(5,6)=s;
  R1(6,5)=-s; R1(6,6)=c;
  R2(1,1)=1;
  R2(2,2)=c2; R2(2,3)=s2; R2(2,4)=2*cs;
  R2(3,2)=s2; R2(3,3)=c2; R2(3,4)=-2*cs;
  R2(4,2)=-cs; R2(4,3)=cs; R2(4,4)=c2-s2;
  R2(5,5)=c; R2(5,6)=-s;
  R2(6,5)=s; R2(6,6)=c;
  Matrix<Real> Co= R1*C*R2;
  for (Matrix<Real>::iterator it=Co.begin(); it!=Co.end(); it++)
    if (abs(*it)<theTolerance) *it=0;
  return Co;
}

/* construct C_angle matrix when applying rotation around y axis (c=cos(t), s=sin(t))
           | c2   0  s2  0   2cs   0 |
           |  0   1  0   0    0    0 |
     R1 =  | s2   0  c2  0  -2cs   0 |    R2=inv(R1)=R1(-t)
           |  0   0  0   c    0   -s |
           | -cs  0  cs  0  c2-s2  0 |
           |  0   0  0  -s    0    c |
 Warning : we do not use the standard Voigt notation e=(e11,e22,e33,2e23,2e13,2e12) but e=(e11,e22,e33,e23,e13,e12) (no factor 2)
*/
Matrix<Real> C_oriented_y(const Matrix<Real>& C, Real angle)
{
  Real t=pi_*angle/180, s= std::sin(t), c=std::cos(t);
  Real c2=c*c, s2=s*s, cs=c*s;
  Matrix<Real> R1(6,6), R2(6,6);
  R1(1,1)=c2; R1(1,3)=s2; R1(1,5)=2*cs;
  R1(2,2)=1;
  R1(3,1)=s2; R1(3,3)=c2; R1(3,5)=-2*cs;
  R1(4,4)=c; R1(4,6)=-s;
  R1(5,1)=-cs; R1(5,3)=cs; R1(5,5)=c2-s2;
  R1(6,4)=s; R1(6,6)=c;
  R2(1,1)=c2; R2(1,3)=s2; R2(1,5)=-2*cs;
  R2(2,2)=1;
  R2(3,1)=s2; R2(3,3)=c2; R2(3,5)=2*cs;
  R2(4,4)=c; R2(4,6)=s;
  R2(5,5)=c2-s2; R2(5,1)=cs; R2(5,3)=-cs;
  R2(6,4)=-s; R2(6,6)=c;
  Matrix<Real> Co= R1*C*R2;
  for (Matrix<Real>::iterator it=Co.begin(); it!=Co.end(); it++)
    if (abs(*it)<theTolerance) *it=0;
  return Co;
}
/* construct C_angle matrix when applying rotation around z axis (c=cos(t), s=sin(t))
           | c2  s2   0   0   0   2cs  |
           | s2  c2   0   0   0  -2cs  |
     R1 =  |  0   0   1   0   0    0   |    R2=inv(R1)=R1(-t)
           |  0   0   0   c  -s    0   |
           |  0   0   0   s   c    0   |
           | -cs  cs  0   0   0  c2-s2 |
 Warning : we do not use the standard Voigt notation e=(e11,e22,e33,2e23,2e13,2e12) but e=(e11,e22,e33,e23,e13,e12) (no factor 2)
*/
Matrix<Real> C_oriented_z(const Matrix<Real>& C, Real angle)
{
  Real t=pi_*angle/180, s= std::sin(t), c=std::cos(t);
  Real c2=c*c, s2=s*s, cs=c*s;
  Matrix<Real> R1(6,6), R2(6,6);
  R1(1,1)=c2; R1(1,2)=s2; R1(1,6)=2*cs;
  R1(2,1)=s2; R1(2,2)=c2; R1(2,6)=-2*cs;
  R1(3,3)=1;
  R1(4,4)=c; R1(4,5)=-s;
  R1(5,4)=s; R1(5,5)=c;
  R1(6,1)=-cs; R1(6,2)=cs; R1(6,6)=c2-s2;
  R2(1,1)=c2; R2(1,2)=s2; R2(1,6)=-2*cs;
  R2(2,1)=s2; R2(2,2)=c2; R2(2,6)=2*cs;
  R2(3,3)=1;
  R2(4,4)=c; R2(4,5)=s;
  R2(5,4)=-s; R2(5,5)=c;
  R2(6,1)=cs; R2(6,2)=-cs; R2(6,6)=c2-s2;
  Matrix<Real> Co= R1*C*R2;
  for (Matrix<Real>::iterator it=Co.begin(); it!=Co.end(); it++)
    if (abs(*it)<theTolerance) *it=0;
  return Co;
}

// return elasticity tensor (1D) regarding material Id (index starts from 1)
Matrix<Real> C(const Point& P, Parameters& pars=defaultParameters)
{
  Number mat=materialIdFromParameters(pars);
  if (mat==0) mat=1;
  return Cs[orientations[mat-1]];
}

// return elasticity tensor (2D) regarding material Id (index starts from 1) and local fiber orientation
Matrix<Real> C2(const Point& P, Parameters& pars=defaultParameters)
{
  const GeomElement* g=geomElementFromParameters(pars);
  if (g!=0)
  {
    Number mat=g->materialId;
    if(mat==0) mat=1;
    Real t=g->theta;
    if(abs(t)>theTolerance) return C_oriented_z(Cs2d[orientations[mat-1]],t);
    else return Cs2d[orientations[mat-1]];
  }
  else 
  {
    return Cs2d[orientations[0]];
  }
}

//map sigmaP (2D) -> [0,1] (1D)  return a vector of size 1 !
Vector<Real> mapS(const Point& P, Parameters& pa = defaultParameters)
{
  return Vector<Real>(1,P(2));
}

void sys_plate_diffraction_3D(int argc, char* argv[], bool check)
{
  String rootname = "sys_plate_diffraction_3D";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(30);
  String errors;
  Number nbErrors = 0;

  /*====================================================================================
                                        elaticity data

      Length in mm
      Time in s
      Elasticity tensor in GPa
      Density in mg/mm3
      Frequency in Mhz
    ====================================================================================*/
  /*
                                        | 2m+l    l     l           |    | E11 |
                                        |   l   2m+l    l           |    | E22 |
  Voigt form in sigma=C*epsilon, C  =  |   l     l   2m+l          |  * | E33 |   isotropic case
                                        |                  2m       |    | E23 |
                                        |                     2m    |    | E13 |
                                        |                        2m |    | E12 |
  */

  //---------------------  isotropic material (Vahan comparison)--------------------------------
  // Matrix<Real> C0(6,6);
  // Real lambda=112.134, mu=83.53, rho=7.86;
  // Real dmpl=2*mu+lambda;
  // C0(1,1)=dmpl; C0(2,2)=dmpl; C0(3,3)=dmpl;
  // C0(1,2)=lambda; C0(2,1)=lambda; C0(2,3)=lambda; C0(3,2)=lambda; C0(1,3)=lambda; C0(3,1)=lambda;
  // C0(4,4)=2*mu; C0(5,5)=2*mu; C0(6,6)=2*mu;
  // Cs[0] = C0;
  // Cs2d[0] = C0;
  // Number np=1;  //number of plies in plate support
  // Number nr=1;  //number of plies in stiffener (0: no stiffener)
  // orientations.resize(1); orientations[0]=0;

  //-------------  carbone epoxy plies (values from Taupin), orthotropic material --------------
  Real rho=1.590;
  Matrix<Real> C0(6,6);
  C0(1,1)=11.5; C0(2,2)=11.5; C0(3,3)=145.5;
  C0(1,2)=5.258; C0(2,1)=5.258; C0(2,3)=5; C0(3,2)=5; C0(1,3)=5; C0(3,1)=5;
  C0(4,4)=5.2; C0(5,5)=5.2; C0(6,6)=3.5;
  //create C matrix for different fiber orientations in d° and store them in Cs map
  Cs[0] = C0;
  Cs[45]= C_oriented_x(C0,45);
  Cs[-45]= C_oriented_x(C0,-45);
  Cs[90]= C_oriented_x(C0,90);
  Cs[135]= C_oriented_x(C0,135);
  //define also matrices for 2D computation because x,y,z -> y,z,x
  Matrix<Real> C02(6,6);
  C02(1,1)=145.5; C02(2,2)=11.5; C02(3,3)=11.5;
  C02(1,2)=5; C02(2,1)=5; C02(2,3)=5.258; C02(3,2)=5.258; C02(1,3)=5; C02(3,1)=5;
  C02(4,4)=3.5; C02(5,5)=5.2; C02(6,6)=5.2;
  Cs2d[0] = C02;
  Cs2d[45]= C_oriented_y(C02,45);
  Cs2d[-45]= C_oriented_y(C02,-45);
  Cs2d[90]= C_oriented_y(C02,90);
  Cs2d[135]= C_oriented_y(C02,135);

  Number np=1;  //number of plies in plate support
  Number nr=1;  //number of plies in stiffener (0: no stiffener)
  orientations.resize(4); orientations[0]=0; orientations[1]=45; orientations[2]=90;orientations[3]=135;

  /*====================================================================================
                                        create mesh
    ====================================================================================*/
  Real a=53.7, h=1.5625;       //h : layer width
  Real x0=-a, L=2*a;         //x origin and lenght of plate support
  Real x1= -a+10, x2= x1 + 10, x3=-x2, x4=-x1;   //x coordinates intersection of plate and stiffener
  Number my = 5, mx=Number(my*L/h);
  Real y;
  Point P1,P2,P3,P4;
  Segment S1,S2,S3,S4;
  Mesh meshP;
  Mesh meshS;
  String nmd="plate1";
  String nmdp="plate";
  if(np==1) nmdp="plate1";
  std::vector<const GeomDomain*> doms(np);
  if (!check)
  {
    if (np>1)
    {
      P1=Point(x0,0); P2=Point(x0+L,0); P3=Point(x0+L,h); P4=Point(x0,h);
      S1=Segment(_v1=P1,_v2=P2,_nnodes=mx+1);
      S2=Segment(_v1=P2,_v2=P3,_nnodes=my+1,_domain_name="sigmaP_1");
      S3=Segment(_v1=P3,_v2=P4,_nnodes=mx+1);
      S4=Segment(_v1=P4,_v2=P1,_nnodes=my+1,_domain_name="sigmaM_1");
      meshP=Mesh(surfaceFrom(S1+S2+S3+S4,"plate1"),_shape=_triangle, _order=1, _generator=gmsh, _name="mesh plate 1");
      meshP.domain("sigmaP_1").setMaterialId(1);
      meshP.domain("sigmaM_1").setMaterialId(1);
      meshP.domain("plate1").setMaterialId(1);
      for (Number k=1; k<np-1; k++)
      {
        y=k*h;
        Point P1(x0,y), P2(x0+L,y), P3(x0+L,y+h), P4(x0,y+h);
        S1=Segment(_v1=P1,_v2=P2,_nnodes=mx+1);
        S2=Segment(_v1=P2,_v2=P3,_nnodes=my+1,_domain_name="sigmaP_"+tostring(k+1));
        S3=Segment(_v1=P3,_v2=P4,_nnodes=mx+1);
        S4=Segment(_v1=P4,_v2=P1,_nnodes=my+1,_domain_name="sigmaM_"+tostring(k+1));
        nmd+="&"+tostring(k+1);
        meshP.merge(Mesh(surfaceFrom(S1+S2+S3+S4,"plate"+tostring(k+1)),_shape=_triangle, _order=1, _generator=gmsh, _name="mesh plate "+tostring(k+1)),true,nmd);
        meshP.domain("sigmaP_"+tostring(k+1)).setMaterialId(k+1);
        meshP.domain("sigmaM_"+tostring(k+1)).setMaterialId(k+1);
        meshP.domain("plate"+tostring(k+1)).setMaterialId(k+1);
      }
    }
    //last layer
    y=(np-1)*h;
    Number mx2=Number(my*(x2-x1)/h), mx3=Number(my*(x1-x0)/h),  mx1=mx-2*(mx2+mx3);
    P1=Point(x0,y); P2=Point(x0+L,y); P3=Point(x0+L,y+h); P4=Point(x0,y+h);
    Point P5(x4,y+h), P6(x3,y+h), P7(x2,y+h), P8(x1,y+h);
    S1 = Segment(_v1=P1,_v2=P2,_nnodes=mx+1);
    S2 = Segment(_v1=P2,_v2=P3,_nnodes=my+1,_domain_name="sigmaP_"+tostring(np));
    S4 = Segment(_v1=P4,_v2=P1,_nnodes=my+1,_domain_name="sigmaM_"+tostring(np));
    Segment S31(_v1=P3,_v2=P5,_nnodes=mx3+1);
    Segment S32(_v1=P5,_v2=P6,_nnodes=mx2+1);
    Segment S33(_v1=P6,_v2=P7,_nnodes=mx1+1);
    Segment S34(_v1=P7,_v2=P8,_nnodes=mx2+1);
    Segment S35(_v1=P8,_v2=P4,_nnodes=mx3+1);
    meshP.merge(Mesh(surfaceFrom(S1+S2+S31+S32+S33+S34+S35+S4,"plate"+tostring(np)),_shape=_triangle, _order=1, _generator=gmsh, _name="mesh plate "+tostring(np)), true, nmdp);
    meshP.domain("sigmaP_"+tostring(np)).setMaterialId(np);
    meshP.domain("sigmaM_"+tostring(np)).setMaterialId(np);
    meshP.domain("plate"+tostring(np)).setMaterialId(np);

    /* stiffener :  multi layers, each boundary is given by
         segment[P1,P2] + arc(R1,[0,theta]) + segment[P3,P4] + arc(R2,[-theta,theta])
       + segmentsegment[P5,P6] + arc(R1,[0,theta]) + segment[P7,P8]

       nr : number of layers
       h  : the layer width
       R1 : radius of first and last arcs of first boundary
       R2 : radius of middle arc of first boundary
       theta : angle of arcs
       x1,x2 : x coordinates of first segment of boundaries

       every thing is computed from this data, in particular the amplitude of the central bump :
                y5=-((1-cos(theta))*(r1+r2)+sin(theta)*x2)/ct;

       every meh steps are set from my the number of segment along h

       R1 decreases with the layer : R1->R1-kh and R2 increases : R2->R2+kh
    */
    //Vector<Real> tets; //for validation
    if (nr>0)
    {
      y=np*h;
      Real theta=pi_/3, st=sin(theta) ,ct=cos(theta), R1=10, R2=10;
      Real hk=0.;
      Real yk=y, r1=R1, r2=R2, y5=-((1-ct)*(r1+r2)+st*x2)/ct;
      Point Pi1(x1,yk), Pi2(x2,yk), Pi3(x2+r1*st,yk+r1*(1-ct)), Pi4(-r2*st,yk+y5-r2*(1-ct));
      Point Pi5(r2*st,yk+y5-r2*(1-ct)), Pi6(-x2-r1*st,yk+r1*(1-ct)),Pi7(-x2,yk), Pi8(-x1,yk);
      Point Ci1(x2,yk+r1), Ci2(0.,yk+y5-r2), Ci3(-x2,yk+r1);
      Number nai1=Number(theta*r1*my/h), nai2=Number(2*theta*r2*my/h), nli1=Number(dist(Pi3,Pi4)*my/h);
      Mesh meshS;
      nmd="stiffener1";
      String lastnmd=nmd;
      for (Number k=0; k<nr; k++)
      {
        hk+=h; yk=y+hk;
        r1=R1-hk; r2=R2+hk; y5=-((1-ct)*(r1+r2)+st*x2)/ct;
        Point Ps1(x1,yk), Ps2(x2,yk), Ps3(x2+r1*st,yk+r1*(1-ct)), Ps4(-r2*st,yk+y5-r2*(1-ct));
        Point Ps5(r2*st,yk+y5-r2*(1-ct)), Ps6(-x2-r1*st,yk+r1*(1-ct)),Ps7(-x2,yk), Ps8(-x1,yk);
        Point Cs1(x2,yk+r1), Cs2(0.,yk+y5-r2), Cs3(-x2,yk+r1);
        Number nas1=Number(theta*r1*my/h), nas2=Number(2*theta*r2*my/h), nls1=Number(dist(Ps3,Ps4)*my/h);
        Geometry G = Segment(_v1=Ps1,_v2=Pi1,_nnodes=my+1)
                     + Segment(_v1=Pi1,_v2=Pi2,_nnodes=mx2+1) + CircArc(_center=Ci1,_v1=Pi2,_v2=Pi3,_nnodes=nai1+1)
                     + Segment(_v1=Pi3,_v2=Pi4,_nnodes=nli1+1) + CircArc(_center=Ci2,_v1=Pi4,_v2=Pi5,_nnodes=nai2+1)
                     + Segment(_v1=Pi5,_v2=Pi6,_nnodes=nli1+1) + CircArc(_center=Ci3,_v1=Pi6,_v2=Pi7,_nnodes=nai1+1)
                     + Segment(_v1=Pi7,_v2=Pi8,_nnodes=mx2+1) + Segment(_v1=Pi8,_v2=Ps8,_nnodes=my+1)
                     + Segment(_v1=Ps8,_v2=Ps7,_nnodes=mx2+1) + CircArc(_center=Cs3,_v1=Ps7,_v2=Ps6,_nnodes=nas1+1)
                     + Segment(_v1=Ps6,_v2=Ps5,_nnodes=nls1+1) + CircArc(_center=Cs2,_v1=Ps5,_v2=Ps4,_nnodes=nas2+1)
                     + Segment(_v1=Ps4,_v2=Ps3,_nnodes=nls1+1) + CircArc(_center=Cs1,_v1=Ps3,_v2=Ps2,_nnodes=nas1+1)
                     + Segment(_v1=Ps2,_v2=Ps1,_nnodes=mx2+1);
        String nak="stiffener"+tostring(k+1);
        Mesh MSk(surfaceFrom(G,nak),_shape=_triangle, _order=1, _generator=gmsh, _name="mesh stiffener "+tostring(k+1));

        //define the angle with x axis for all element of the layer k, considering the center of element
        Domain dsk=MSk.domain(nak);
        dsk.setMaterialId(k+1);

        //tets.resize(dsk.numberOfElements()); //for validation
        for (Number e=1; e<=dsk.numberOfElements(); e++)
        {
          GeomElement* g=dsk.element(e);
          Point C=g->meshElement()->center();
          Real x=C(1),y=C(2), th=0;
          if (x<0)
          {
            Real x11=x-Ci1(1), y11=y-Ci1(2);
            if (x11>0 && abs(y11)>theTolerance)
            {
              if (ct*x11+st*y11 <=theTolerance) { th=-std::atan(x11/y11); } //area 2
              else
              {
                Real x21=x-Ci2(1), y21=y-Ci2(2);
                if(ct*x21+st*y21<=theTolerance) th=theta; //area 3
                else th=-std::atan(x21/y21); //area  4
              }
            }
          }
          else if (x<Ci3(1))//0<x<Ci(3)
          {
            Real x21=x-Ci2(1), y21=y-Ci2(2);
            if(ct*x21-st*y21<=theTolerance && abs(y21)>theTolerance) th=-std::atan(x21/y21); //area 5
            else
            {
              Real x31=x-Ci3(1), y31=y-Ci3(2);
              if(ct*x31-st*y31<=theTolerance) th=-theta; //area 6
              else if(x<Cs3(1) && abs(y31)>theTolerance)  th=-std::atan(x31/y31); //area 7
            }
          }
          g->theta=th;
          //tets(e)=th;  //for validation
        }

        // merge layer k
        meshS.merge(MSk, true, nmd);
        Pi1=Ps1; Pi2=Ps2; Pi3=Ps3; Pi4=Ps4; Pi5=Ps5; Pi6=Ps6; Pi6=Ps6; Pi6=Ps6; Pi7=Ps7; Pi8=Ps8; Ci1=Cs1; Ci2=Cs2; Ci3=Cs3;
        nai1=nas1; nai2=nas2; nli1=nls1;
        lastnmd=nmd;
        nmd+="&"+tostring(k+2);
      }

      //merge mesh of plate and stiffener
      meshP.merge(meshS,true,"Omega");
      meshP.renameDomain(lastnmd,"stiffener");

    }
    else meshP.renameDomain(nmd,"Omega");

    //remove additional domains created by merge process
    String nap="plate1", nar="stiffener1";
    for (Number k=2; k<np; k++) { nap+="&"+tostring(k); meshP.removeDomain(nap);}
    for (Number k=2; k<nr; k++) { nar+="&"+tostring(k); meshP.removeDomain(nar);}

    //merge boundaries SigmaM_k and merge boundaries SigmaP_k
    // std::vector<const GeomDomain*> doms(np);
    // for(Number k=0; k<np; k++) doms[k]=&meshP.domain("sigmaM_"+tostring(k+1));
    // Domain sigmaM=merge(doms,"sigmaM");
    // for(Number k=0; k<np; k++) doms[k]=&meshP.domain("sigmaP_"+tostring(k+1));
    // Domain sigmaP=merge(doms,"sigmaP");
    // Domain Omega=meshP.domain("Omega");
    cpuTime("meshing plate and stiffener");

    meshP.saveToFile(inputsPathTo(rootname+"/meshP.msh"),_msh, true);
    meshP.saveToFile(inputsPathTo(rootname+"/composite_mesh.vtk"),_vtk, true);
  
    //validation of theta
    // Domain stiffener=meshP.domain("stiffener");
    // Space sp0(stiffener,_P0,"P0 on stiffener");
    // Unknown tu(sp0,"tu",1);
    // TermVector Tets(tu,stiffener,tets,"Tets");
    // saveToFile("Tets.vtk",Tets,_format=_vtk);

    /*====================================================================================
                                            diffraction data
        ====================================================================================*/
    // Real beta= 0., k=1.;    //incidence angle in Oy, Oz, value of k
    // Real gamma=k*sin(beta); //ky
    // Real freq=0.2;
    // Real omg2=4*pi_*pi_*freq*freq;

    /*====================================================================================
                    compute eigen vectors in cross section S (SAFE-gamma method)
      ====================================================================================*/
    //compute mode on 1D segment S [0, np*h]
    // the coordinate axis system is :
    //
    //          x
    //          |  y
    //          | /
    //          |/
    //          |-------- z
    Real x=0;
    my=19;   //not the same step definition in structured meshing
    for (Number k=0; k<np; k++, x+=h)
    {
      Segment Sk(_v1=Point(x),_v2=Point(x+h),_nnodes=my+1,_domain_name="S"+tostring(k+1));
      if (k==0) nmd="S"+tostring(k+1);
      else nmd+="&"+tostring(k+1);
      meshS.merge(Mesh(Sk,1,_structured,"Section "+tostring(k+1)),true, nmd);
    }
    meshS.saveToFile(inputsPathTo(rootname+"/meshS.msh"),_msh, true);
  } //fin edit
  else
  {
    meshP=Mesh(inputsPathTo(rootname+"/meshP.msh"),"MeshP",msh);
    meshS=Mesh(inputsPathTo(rootname+"/meshS.msh"),"MeshS",msh);
    std::cout << "YOUP"<<eol;
  }

  for (Number k=0; k<np; k++) doms[k]=&meshP.domain("sigmaM_"+tostring(k+1));
  Domain sigmaM=merge(doms,"sigmaM");
  for (Number k=0; k<np; k++) doms[k]=&meshP.domain("sigmaP_"+tostring(k+1));
  Domain sigmaP=merge(doms,"sigmaP");
  Domain Omega=meshP.domain("Omega");
  cpuTime("meshing plate and stiffener");

  Real beta= 0., k=1.;    //incidence angle in Oy, Oz, value of k
  Real gamma=k*sin(beta); //ky
  Real freq=0.2;
  Real omg2=4*pi_*pi_*freq*freq;
  //rename main domain and remove additional domains created by merge process
  meshS.renameDomain(nmd,"S");
  nmd="S1";
  for(Number k=2; k<np; k++) { nmd+="&"+tostring(k); meshS.removeDomain(nmd);}
  Domain S=meshS.domain("S");
  //assign material id
  if (np>1)
    for (Number k=1; k<=np; k++) { meshS.domain("S"+tostring(k)).setMaterialId(k);}

  /*
                                | 0    M  |     | M    0  |
    matrices of 1D eigen system  |         | = l |         |
                                | A0   A1 |     | 0    A2 |
  */
  Space V1(_domain=S, _interpolation=_P2, _name="V1");       //P1 Lagrange interpolation
  Unknown U(V1, _name="U", _dim=3);        //3 vector unknown U
  Unknown gU(V1, _name="gU", _dim=3);      //3 vector unknown gamma.u
  TestFunction V(U, _name="V");
  TestFunction gV(gU, _name="gV");

  BilinearForm avp = omg2*intg(S,(rho*U)|gV)
                     - intg(S,(C%epsilonG(U,1,1,0,0))%epsilonG(gV,1,1,0,0))
                     - gamma*gamma*intg(S,(C%epsilonG(U,0,0,1,0))%epsilonG(gV,0,0,1,0))
                     - i_*gamma*intg(S,(C%epsilonG(U,0,0,1,0))%epsilonG(gV,1,1,0,0))
                     + i_*gamma*intg(S,(C%epsilonG(U,1,1,0,0))%epsilonG(gV,0,0,1,0))     //A0 part
                     - gamma*intg(S,(C%epsilonG(gU,0,0,1,0))%epsilonG(gV,0,0,0,1))
                     - gamma*intg(S,(C%epsilonG(gU,0,0,0,1))%epsilonG(gV,0,0,1,0))
                     - i_*intg(S,(C%epsilonG(gU,0,0,0,1))%epsilonG(gV,1,1,0,0))
                     + i_*intg(S,(C%epsilonG(gU,1,1,0,0))%epsilonG(gV,0,0,0,1))           // A1 part
                     + intg(S,(rho*gU)|V);                                               // M Part
  BilinearForm bvp = intg(S,(rho*U)|V,_symmetry=_symmetric)
                     + intg(S,(C%epsilonG(gU,0,0,0,1))%epsilonG(gV,0,0,0,1),_symmetry=_symmetric); // A2 part

  TermMatrix A(avp, _name="A"), B(bvp, _name="B");
  cpuTime("SAFE : compute(A,B)");

  //compute eigen values and eigen vectors, using arpack
  Number n=20;
  EigenElements eigs = arpackSolve(A, B, _nev=n, _sigma=Complex(0.), _tolerance=1.0e-12);

  cpuTime("SAFE : arpackSolver");
  if (eigs.numberOfEigenValues()==0)
  {
    std::cout<<"========================================================================="<<eol;
    std::cout<<"                        ARPACK solver fails"<<eol;
    std::cout<<"========================================================================="<<eol;
    if (check)
    {
      if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
      else { error("test_report", rootname, nbErrors, ":\n"+errors); }
    }
    else { theCout << "Data updated " << eol; }
  }

  /*==============================================================================================
    keep only eigenvalues with positive real part or positive imaginary part and sort them :
    first real eigenvalues (descending), then other eigenvalues sorted by ascending imaginary part
  ===============================================================================================*/
  std::map<Real,Number> sortedEigsR, sortedEigsC;
  for (Number k=1; k<=n; k++)
  {
    Complex lk=eigs.value(k);
    if(abs(lk.imag())<=theTolerance)  //real eigenvalue
    {if(lk.real()>= 0) sortedEigsR[lk.real()]=k;}  // later add criterium (im(Xk)|re(Yk)) < 0
    else
    {if(lk.imag()>= 0) sortedEigsC[lk.imag()]=k;}
  }
  n=sortedEigsR.size()+sortedEigsC.size();
  TermVectors evs(n);
  Vector<Complex> betas(n);
  std::map<Real,Number>::reverse_iterator rit=sortedEigsR.rbegin();
  Number m=1; String modetype;
  for (; rit!=sortedEigsR.rend(); m++, rit++)
  {
    betas(m) = eigs.value(rit->second);
    evs(m)=eigs.vector(rit->second)(U);
  }
  std::map<Real,Number>::iterator it=sortedEigsC.begin();
  for (; it!=sortedEigsC.end(); m++, it++)
  {
    betas(m) = eigs.value(it->second);
    if(abs(betas(m).real())<=theTolerance) betas(m)=Complex(0.,betas(m).imag());
    evs(m)=eigs.vector(it->second)(U);
  }

  //compute sigma=(s_11, s_22, s_33, s_23, s_13, s_12) from U, using Voigt to matrix operator
  Unknown sigma(V1, _name="sigma", _dim=6);       //6_vector unknown sigma
  TestFunction tau(sigma, _name="tau");     //6_vector test function tau
  TermMatrix Ms(intg(S,voigtToM(sigma)%voigtToM(tau)), _name="Ms");
  TermMatrix Exy(intg(S,C%epsilonG(U,1,1,0,0)%voigtToM(tau))+gamma*intg(S,C%epsilonG(U,0,0,1,0)%voigtToM(tau)), _name="Exy");
  TermMatrix Ez(intg(S,C%epsilonG(U,0,0,0,1)%voigtToM(tau)), _name="Ez");
  TermMatrix M12(intg(S,U[1]*sigma[5]) + intg(S,U[2]*sigma[4]), _name="M12");
  TermMatrix M3(intg(S,U[3]*sigma[3]),"M3");

  TermMatrix Msf; factorize(Ms, Msf);
  TermVectors svs(n);
  Number ns=0;  //number of propagative modes
  for (Number k=1; k<=n; k++)
  {
    TermVector vk = Exy*evs(k) + i_*betas(k)*(Ez*evs(k));
    svs(k)=factSolve(Msf,vk);
    TermVector m12k=M12*evs(k), m3k=M3*evs(k);
    Complex jk = std::sqrt(dotRC(m12k,svs(k))-dotRC(m3k,svs(k)));
    Complex dwdb = dotRC(imag(m12k),real(svs(k)))-dotRC(real(m3k),imag(svs(k)));
    modetype=" (evanescent ";
    if(abs(imag(betas(k))) < theTolerance) {ns++; modetype=" (propagative ";}
    evs(k)/=jk;  svs(k)/=jk;
    if(norm(evs(k)(U[2])) < 1.E-12) modetype+=" Lamb mode)"; else modetype+="accoustic mode)";
    thePrintStream<<"beta_"<<tostring(k)<<" = "<<betas(k)<<modetype<<" jk="<<jk;
    if(abs(imag(betas(k))) < theTolerance) thePrintStream<<" dwdb="<<dwdb<<eol;
    thePrintStream<<eol;
    // thePrintStream<<"evs("<<tostring(k)<<")="<<evs(k);
    // thePrintStream<<"svs("<<tostring(k)<<")="<<svs(k)<<eol;
  }

// orthogonality modes matrix
  Matrix<Complex> Omn(n,n);
  for (Number k=1; k<=n; k++)
  {
    TermVector m12k=M12*evs(k);
    for(Number q=1; q<=n; q++)
    {
      Omn(k,q) = dotRC(m12k,svs(q))-dotRC(M3*evs(q),svs(k));  //transpose or not ?
      if(abs(Omn(k,q))<=theTolerance) Omn(k,q)=0.;
      thePrintStream<<" j_"<<tostring(k)<<"_"<<tostring(q)<<" = "<<Omn(k,q);
    }
    thePrintStream<<eol;
  }

  //clean
  clear(A, B, Ms, Msf);
  clear(Exy, Ez, M12, M3);
  cpuTime("SAFE : prepare spectral basis");
  /*====================================================================================
                            solve 2D diffraction problem
    ====================================================================================
    the coordinate axis system is :

            y
            |  z
            | /
            |/
            |-------- x
  */
  //space definitions
  Space V2(_domain=Omega, _interpolation=_P2, _name="V2");                         //Pk Lagrange interpolation on Omega
  Unknown u(V2, _name="u", _dim=3); TestFunction v(u, _name="v");       //3_vector displacement unknown u(x,y)
  Space Vtp(_domain=sigmaP, _interpolation=_P2, _name="Vtp", _notOptimizeNumbering);                //Pk Lagrange interpolation
  Unknown tp(Vtp, _name="tp", _dim=1); TestFunction qp(tp, _name="qp"); //vector strain unknown t = tau_3 on sigma+
  Space Vtm(_domain=sigmaM, _interpolation=_P2, _name="Vtm", _notOptimizeNumbering);               //Pk Lagrange interpolation
  Unknown tm(Vtm, _name="tm", _dim=1); TestFunction qm(tm, _name="qm"); //vector strain unknown t = tau_3 on sigma-
  //construct tensor kernels (change components  x,y,z -> y,z,x )
  TermVectors tsn(n), t3n(n), usn(n), u3n(n);
  Unknown s3(V2, _name="s3", _dim=3);
  Vector<Number>ci(3); ci(1)=0; ci(2)=5; ci(3)=4;    //map unknown components 0 5 4
  
  for (Number k=1; k<=n; k++) //extract transverse component of displacement and longitudinal component of Sigma
  {
    usn(k)=evs(k); usn(k).changeUnknown(u,0,1,2); usn(k).name()="us_"+tostring(k);
    u3n(k)=evs(k)(U[3]); u3n(k).name()="u3_"+tostring(k);
    t3n(k)=svs(k)(sigma[3]); t3n(k).name()="t3_"+tostring(k);
    tsn(k)=svs(k); tsn(k).changeUnknown(u,0,5,4); tsn(k).name()="ts_"+tostring(k); //to be compatible with inner product by a 3-vector, ci={0,5,4} corresponds to 0 s13 s23 vectors
  }
  Matrix<Complex> Omn_inv=inverse(Omn);    //anisotropic case
  //Vector<Complex> Omn_inv(n,Complex(1.,0.)); //isotropic case

  Function fmaps(mapS);
  TensorKernel ts_ts(tsn,Omn_inv); ts_ts.xmap=fmaps; ts_ts.ymap=fmaps; ts_ts.name="ts_ts";
  TensorKernel u3_u3(u3n,Omn_inv); u3_u3.xmap=fmaps; u3_u3.ymap=fmaps; u3_u3.name="u3_u3";
  TensorKernel u3_ts(u3n,Omn_inv,tsn); u3_ts.xmap=fmaps; u3_ts.ymap=fmaps; u3_ts.name="u3_ts";  //may be transpose Omn_inv in anisotropic case
  TensorKernel ts_u3(tsn,Omn_inv,u3n); ts_u3.xmap=fmaps; ts_u3.ymap=fmaps; ts_u3.name="ts_u3";

  cpuTime("DIFFRACTION : prepare tensor kernels");
  // construct bilinear forms  with transverse components (2,3) NOTE: only gamma=0 has been validated
  BilinearForm aut = intg(Omega,(C2%epsilonG(u,1,1,1,0))%epsilonG(v,1,1,1,0))
                     + i_*gamma*intg(Omega,(C2%epsilonG(u,0,0,0,1))%epsilonG(v,1,1,1,0))
                     - i_*gamma*intg(Omega,(C2%epsilonG(u,1,1,1,0))%epsilonG(v,0,0,0,1))
                     + gamma*gamma*intg(Omega,(C2%epsilonG(u,0,0,0,1))%epsilonG(v,0,0,0,1))
                     - omg2*intg(Omega,(rho*u)|v)                                        //omega part
                     - intg(sigmaP,sigmaP,u|ts_ts|v)  - intg(sigmaP,sigmaP,tp*u3_ts|v)  + intg(sigmaP,tp*v[1])
                     - intg(sigmaP,sigmaP,u|ts_u3*qp) - intg(sigmaP,sigmaP,tp*u3_u3*qp) + intg(sigmaP,u[1]*qp)
                     - intg(sigmaM,sigmaM,u|ts_ts|v)  - intg(sigmaM,sigmaM,tm*u3_ts|v)  - intg(sigmaM,tm*v[1])
                     - intg(sigmaM,sigmaM,u|ts_u3*qm) - intg(sigmaM,sigmaM,tm*u3_u3*qm) - intg(sigmaM,u[1]*qm);
  TermMatrix Aut(aut, _name="Aut");

  //construct right hand side related to mode diffraction from mode computation
  Unknown tsm(Vtm,"tsm",3); Unknown tsp(Vtp,"tsp",3);
  BilinearForm blm = intg(sigmaM,sigmaM,u|ts_ts|v)  + intg(sigmaM,sigmaM,tm*u3_ts|v)  + intg(sigmaM,tsm|v)
                     + intg(sigmaM,sigmaM,u|ts_u3*qm) + intg(sigmaM,sigmaM,tm*u3_u3*qm) + intg(sigmaM,u[1]*qm);
  BilinearForm blp = intg(sigmaP,sigmaP,u|ts_ts|v)  + intg(sigmaP,sigmaP,tp*u3_ts|v)  - intg(sigmaP,tsp|v)
                     + intg(sigmaP,sigmaP,u|ts_u3*qp) + intg(sigmaP,sigmaP,tp*u3_u3*qp) - intg(sigmaP,u[1]*qp);

  TermMatrix Blm(blm," _name=Blm"), Blp(blp, _name="Blp");
  cpuTime("DIFFRACTION : construct linear system");

  //factorize matrix Aut
  TermMatrix LU;
  factorize(Aut,LU);
  cpuTime("DIFFRACTION : LU factorisation");

  DomainMap dmapm(sigmaM,S,mapS);
  DomainMap dmapp(sigmaP,S,mapS);
  //construct mode projectors Pm and Pp
  Space Sm(_basis=tsn, _name="Sm"), Um(_basis=u3n, _name="Um");
  Unknown sm(Sm, _name="sm"), um(Um, _name="um");
  BilinearForm prm=intg(sigmaM,u|sm)+intg(sigmaM,tm*um),
              prp=intg(sigmaP,u|sm)+intg(sigmaP,tp*um);
  TermMatrix Pm(prm, _name="Pm"), Pp(prp, _name="Pp");

  //solve systems and construct scatering matrix for non evanescent modes
  Matrix<Complex> scat(2*ns,2*ns); //[R-- T-+; T+- R++]
  GeomDomain* plate=&Omega;
  if (nr>0) plate= &meshP.domain(nmdp);
  IOFormat iof=_vtu;
  for (Number k=1;k<=ns;k++)
  {
    //compute rhs with mode k as left incident mode
    //----------------------------------------------
    TermVector tsk=tsn(k).mapTo(sigmaM,tsm); tsk.name()="ts_"+tostring(k)+"map";
    TermVector t3k=t3n(k).mapTo(sigmaM,tm); t3k.name()="t3_"+tostring(k)+"map";
    TermVector usk=usn(k).mapTo(sigmaM,u); usk.name()="us_"+tostring(k)+"map";
    TermVector u3k=u3n(k).mapTo(sigmaM,u[1]); u3k.name()="u3_"+tostring(k)+"map";
    TermVector f=tsk-t3k+usk+u3k; f.name()="f"+tostring(k);
    //f*=-exp(-i*betas(k)*a);  // 0 phase en x=0
    f*=-1;                     // 0 phase at sigmaM (x=-a)
    TermVector rhs=Blm*f;
    //solve system
    TermVector sol=factSolve(LU, rhs);
    saveToFile("u-_"+tostring(k),sol(u),_format=iof);
    //construt uinc = mode k
    TermVector uinc(u,*plate,Vector<Complex>(3,0.),"uinc");
    TermVector uM=evs(k).mapTo(sigmaM,u); uM.changeUnknown(u,3,1,2);
    for (Number n=1; n<=uinc.nbDofs(u); n++)
    {
      Point p=uinc.dof(u,n).coords();
      Vector<Complex> vc=uM.evaluate(u,Point(-a,p(2))).value<Vector<Complex> >();
      //uinc.setValue(u,n,vc*exp(i*betas(k)*p(1)));  // 0 phase en x=0
      uinc.setValue(u,n,vc*exp(i_*betas(k)*(p(1)+a)));// 0 phase at sigmaM (x=-a)
    }
    TermVector t3kM=t3n(k).mapTo(sigmaM,tm), t3kP=t3n(k).mapTo(sigmaP,tp);
    //TermVector solinc = uinc - exp(-i*betas(k)*a)*t3kM - exp(i*betas(k)*a)*t3kP;  // 0 phase en x=0
    TermVector solinc = uinc - t3kM - exp(2*i_*betas(k)*a)*t3kP;                     // 0 phase at sigmaM (x=-a)
    saveToFile("mode-_"+tostring(k),uinc,_format=iof);

    //construct scattering coefficients
    TermVector Skm=Pm*sol, Skp=Pp*sol;
    Vector<Complex> S1, S2, Sm, Sp;
    Sm = Skm.subVector(sm).asVector(S1) + Skm.subVector(um).asVector(S2);
    Sp = Skp.subVector(sm).asVector(S1) + Skp.subVector(um).asVector(S2);
    Sm=Omn_inv*Sm; Sp=Omn_inv*Sp;     //general case
    scat.set(k,k,1,ns,Sm(1,ns));      //R--
    scat.set(k,k,ns+1,2*ns,Sp(1,ns)); //T-+
    scat(k,k)-=1; //substract incident field

    //compute rhs with mode k as right incident mode
    //----------------------------------------------
    tsk=tsn(k).mapTo(sigmaP,tsp);
    t3k=t3n(k).mapTo(sigmaP,tp);
    usk=usn(k).mapTo(sigmaP,u);
    u3k=u3n(k).mapTo(sigmaP,u[1]);
    f=usk-u3k-tsk-t3k;
    //f*=-exp(-i*betas(k)*a);  // 0 phase en x=0
    f*=-1;                     // 0 phase at sigmaP (x=a)
    rhs=Blp*f;
    //solve system
    sol=factSolve(LU, rhs);
    saveToFile("u+_"+tostring(k),sol(u),_format=iof);
    //construt uinc = mode k
    TermVector em=evs(k)(U[1])+evs(k)(U[2])-evs(k)(U[3]);
    uM=em.mapTo(sigmaP,u); uM.changeUnknown(u,3,1,2);
    for(Number n=1; n<=uinc.nbDofs(u); n++)
    {
      Point p=uinc.dof(u,n).coords();
      Vector<Complex> vc=uM.evaluate(u,Point(a,p(2))).value<Vector<Complex> >();
      //uinc.setValue(u,n,vc*exp(-i*betas(k)*p(1)));    // 0 phase en x=0
      uinc.setValue(u,n,vc*exp(-i_*betas(k)*(p(1)-a)));  // 0 phase at sigmaP (x=a)
    }
    //solinc = uinc - exp(i*betas(k)*a)*t3kM - exp(-i*betas(k)*a)*t3kP;  // 0 phase en x=0
    solinc = uinc - exp(2*i_*betas(k)*a)*t3kM - t3kP;                     // 0 phase at sigmaP (x=a)
    saveToFile("mode+_"+tostring(k),uinc,_format=iof);
    //construct scattering coefficients
    Skm=Pm*sol; Skp=Pp*sol;
    Sm = Skm.subVector(sm).asVector(S1) + Skm.subVector(um).asVector(S2);
    Sp = Skp.subVector(sm).asVector(S1) + Skp.subVector(um).asVector(S2);
    Sm=Omn_inv*Sm; Sp=Omn_inv*Sp;     //general case
    scat.set(ns+k,ns+k,1,ns,Sm(1,ns));       //T+-
    scat.set(ns+k,ns+k,ns+1,2*ns,Sp(1,ns));  //R++
    scat(ns+k,ns+k)-=1; //substract incident field
  }

  Matrix<Real> absscat=abs(scat);
  absscat.roundToZero(1.E-08);
  thePrintStream<<"scat="<<eol<<scat<<eol;
  thePrintStream<<"abs(scat)="<<eol<< absscat<<eol;
  scat.saveToFile("scat.dat");
  absscat.saveToFile("absscat.dat");
  cpuTime("DIFFRACTION : solve linear systems");

  std::cout<<"U saved, that's all folks"<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
   
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
}

}
