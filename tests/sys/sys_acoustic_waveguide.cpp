/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_acoustic_waveguide.cpp
\author E. Lunéville
\since 16 sep 2015
\date 16 sep 2015

 deal with Helmholtz equation in a semi-infinite waveguide

                                                Gamma
          y=h  ------------------------------------------------------------
                 |                                    |                  |
                 |                                    |                  |
    Sigma_0|   Omega_a      Sigma_a|  Omega_b  |Sigma_b
                 |                                    |                  |
                 |                                    |                  |
          y=0  ------------------------------------------------------------
          x=0               Gamma              x=a         x=b


          (delta+k^2)u=0  in Omega = ]0,+inf[ x ]0,h[
          du/dn=0         on Gamma
          du/dn=g         on Sigma_0
          u(x,y)= sum_n a_n*phi_n(y)*exp(i*beta_n*x)  for any x>x1

          with phi_n(y) = c_n cos(n*pi*y/h)
          and  beta_n = sqrt(k^2-n^2*pi^2/h^2) Re(beta_n)>0 and Im(beta_n)>=0

 four methods are considered :

     - low frequency approximation :   du/dn = iku on Sigma_a
     - standard DtN approximation  :   du/dn = sum i*beta_n*(u,phi_n)|Sigma1*phi_n  on Sigma_a
                                               n<N
     - thick DtN approximation     :   du/dn = sum i*beta_n*exp(i*beta_n(x2-x1))*(u,phi_n)|Sigma1*phi_n  on Sigma_b
                                               n<N
     - the PML technique in Omega2 :   d2u/dy2 + alpha^2 d2u/dx2 = 0 in Omega_b


*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

using namespace std;

namespace sys_acoustic_waveguide
{

Real k=20;
Real h=1;

//data function on Sigma0
Complex g(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Number n=1;
  Complex betan=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));
  return -i_*betan*sqrt(2./h)*cos(n*pi_*y/h);         //normal derivative on Sigma0 of mode n
}

//exact solution
Complex uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Number n=1;
  Complex betan=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));
  return sqrt(2./h)*cos(n*pi_*y/h)*exp(i_*betan*x);
}

//cos basis
Real cosn(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                   //get the parameter h (user definition)
  Int  n=pa("basis index")-1;       //get the index of function to compute
  if(n==0) return std::sqrt(1./h);
  else     return std::sqrt(2./h)*std::cos(n*pi_*y/h);
}

void sys_acoustic_waveguide(int argc, char* argv[], bool check)
{

  String rootname = "sys_acoustic_waveguide";
  trace_p->push("rootname");
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.0001;

  //define some parameters
  Real a=1,       // abcissa of Sigma_a
       b=1.2,     // abcissa of Sigma_b
       k2=k*k;
  Parameters params;
  params << Parameter(h,"h")<< Parameter(k,"k");

//create Mesh
  Mesh  meshWG;
  if (!check)
  {
  Number ny=30, na=Number(ny*a/h), nb=Number(ny*(b-a)/h);
  Rectangle Ra(_xmin=0.,_xmax=a,_ymin=0.,_ymax=h, _nnodes=Numbers(na, ny),_domain_name="Omega_a",_side_names=Strings("Gamma","Sigma_a","Gamma","Sigma_0"));
  Rectangle Rb(_xmin=a,_xmax=b,_ymin=0.,_ymax=h, _nnodes=Numbers(nb, ny),_domain_name="Omega_b",_side_names=Strings("Gamma","Sigma_b","Gamma","Sigma_a"));
  Real r=0.1;
  Number nd=Number(0.5*ny*pi_*r/h);
  Disk D1(_center=Point(0.1,0.1),_radius=0.05,_nnodes=nd,_domain_name="D1");
  // Mesh meshWG(Ra+Rb-D1,_shape=_triangle, _order=1, _generator=gmsh);
  meshWG=Mesh(Ra+Rb, _shape=_triangle, _order=1, _generator=gmsh);
  meshWG.saveToFile(inputsPathTo(rootname+"/meshWG.msh"), _msh);
  }
  else
  {
    meshWG=Mesh(inputsPathTo(rootname+"/meshWG.msh"), _name="meshWG");
  }
  Domain Omega_a = meshWG.domain("Omega_a"), Omega_b = meshWG.domain("Omega_b");
  Domain Sigma_0 = meshWG.domain("Sigma_0"),
         Sigma_a = meshWG.domain("Sigma_a"),
         Sigma_b = meshWG.domain("Sigma_b");


  // create interpolation space on Omega
  Domain Omega=merge(Omega_a,Omega_b,"Omega");
  Space V(_domain=Omega, _interpolation=_P2, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");

  //create mass matrix
  TermMatrix M1(intg(Omega_a,u*v));
  TermVector Uex(u,Omega_a,uex);

  // create spectral space on Sigma_a and dtn kernel
  Number N=7;  //number of modes in DtN
  Complexes beta(N);
  for(Number n=0; n<N; n++) beta[n]=sqrt(Complex(k2-n*n*pi_*pi_/(h*h)));
  Space Sa(_domain=Sigma_a, _basis=Function(cosn,params), _dim=N, _name="cos(n*pi*y)");
  Unknown phi_a(Sa,"phi_a");
  TensorKernel K1(phi_a,beta);

  //create scattering matrix
  TermMatrix C(intg(Sigma_a,u*phi_a), _name="C");

  bool doError = true;
  bool save = false;
  bool scat_coeff = false;
  //Real t=0;

  cpuTime("mesh and space creation");

  //----------------   solve low frequency approximation ----------------
  // create system and solve it
  BilinearForm alf = intg(Omega_a,grad(u)|grad(v)) - k2*intg(Omega_a,u*v)
                     - i_*k*intg(Sigma_a,u*v);
  LinearForm f= intg(Sigma_0,g*v);
  TermMatrix Alf(alf, _name="Alf");
  TermVector B(f, _name="B");
  TermVector Ulf=directSolve(Alf,B);

  thePrintStream<<"======================= low frequency approximation ======================="<<eol;
  thePrintStream<<" cpu time : "<<cpuTime()<<eol;
  if (scat_coeff)
  {
    TermVector clf=C*Ulf;
    thePrintStream<<" scattering coefficients : "<<clf.asComplexVector()<<eol;
  }
  if (doError) //compute L2 error
  {
    TermVector Erlf=Ulf-Uex;
    Real erlf=sqrt(abs((M1*Erlf|Erlf)));
    thePrintStream<<" L2 error = "<<erlf<<eol;
    nbErrors+=checkValue(erlf, 0.00952224, tol, errors, "Erlf");
  }
   if (save) saveToFile("Ulf",Ulf,_format=_vtu);
   // nbErrors+=checkValues("Ulf.vtu", rootname+"/Ulf.in", tol, errors, "Ulf", check);

  //---------------- solve standard DtN approximation ----------------
  cpuTime();
  // create system and solve it
  BilinearForm adtn = intg(Omega_a,grad(u)|grad(v)) - k2*intg(Omega_a,u*v)
                      - i_*intg(Sigma_a,Sigma_a,u*K1*v);
  TermMatrix Adtn(adtn, _name="Adtn");
  TermVector Udtn=directSolve(Adtn,B);

  thePrintStream<<"======================= standard DtN approximation ======================="<<eol;
  thePrintStream<<" cpu time : "<<cpuTime()<<eol;
  if (scat_coeff)
  {
    TermVector cdtn=C*Udtn;
    thePrintStream<<" scattering coefficients  : "<<cdtn.asComplexVector()<<eol;
  }

  if (doError) //compute L2 error
  {
    TermVector Erdtn=Udtn-Uex;
    Real erdtn=sqrt(abs((M1*Erdtn|Erdtn)));
    thePrintStream<<" L2 error = "<<erdtn<<eol;
    nbErrors+=checkValue(erdtn, 0.00102525, tol, errors, "Erdtn");
  }
  if (save) saveToFile("Udtn",Udtn,_format=_vtu);
  // nbErrors+=checkValues("Udtn.vtu", rootname+"/Udtn.in", tol, errors, "Udtn", check);

  //---------------- solve thick DtN approximation ----------------
  // create spectral space on Sigma_b and dtn kernel
  cpuTime();
  Real L=b-a;
  for (Number n=0; n<N; n++)
  {
    Complex bn=sqrt(Complex(k2-n*n*pi_*pi_/(h*h)));
    beta[n]=bn*exp(i_*bn*L);
  }
  Space Sb(_domain=Sigma_b, _basis=Function(cosn,params), _dim=N, _name="cos(n*pi*y)");
  Unknown phi_b(Sb, _name="phi_b");
  TensorKernel K2(phi_a,beta,phi_b);
//  TensorKernel K2(phin,beta,phin);

  //create system and solve it
  BilinearForm adtn2 = intg(Omega,grad(u)|grad(v)) - k2*intg(Omega,u*v)
                       - i_*intg(Sigma_a,Sigma_b,u*K2*v);
  TermMatrix Adtn2(adtn2, _name="Adtn2");
  TermVector Udtn2=directSolve(Adtn2,B);

  thePrintStream<<"======================= thick DtN approximation ======================="<<eol;
  thePrintStream<<" cpu time : "<<cpuTime()<<eol;
  if(scat_coeff)
  {
    TermVector cdtn2=C*Udtn2;
    thePrintStream<<" scattering coefficients     : "<<cdtn2.asComplexVector()<<eol;
  }

  if (doError) //compute L2 error
  {
    TermVector Erdtn2=Udtn2-Uex;
    Real erdtn2=sqrt(abs((M1*Erdtn2|Erdtn2)));
    thePrintStream<<" L2 error = "<<erdtn2<<eol;
    nbErrors+=checkValue(erdtn2, 0.000878007, tol, errors, "Erdtn2");
  }
  if (save) saveToFile("Udtn2",Udtn2,_format=_vtu);
  // nbErrors+=checkValues("Udtn2.vtu", rootname+"/Udtn2.in", tol, errors, "Udtn2", check);


  //---------------- solve using PML method ----------------
  cpuTime();
  Complex alpha=(1.-i_)/10.;    //PML coefficient
  Complex am=1./alpha;
  //create system and solve it
  BilinearForm apml =  intg(Omega_a,grad(u)|grad(v)) - k2*intg(Omega_a,u*v)
                       +  am*intg(Omega_b,dy(u)|dy(v)) + alpha*intg(Omega_b,dx(u)|dx(v))- (k2*am)*intg(Omega_b,u*v);
  TermMatrix Apml(apml, _name="Apml");
  TermVector Upml=directSolve(Apml,B);

  thePrintStream<<"======================= PML approximation ======================="<<eol;
  thePrintStream<<" cpu time : "<<cpuTime()<<eol;
  if (scat_coeff)
  {
    TermVector cpml=C*Upml;
    thePrintStream<<" scattering coefficients           : "<<cpml.asComplexVector()<<eol;
  }
  if (doError) //compute L2 error
  {
    TermVector Erpml=Upml-Uex;
    Real erpml=sqrt(abs((M1*Erpml|Erpml)));
    thePrintStream<<" L2 error = "<<erpml<<eol;
    Real erRef=0.0943;
    nbErrors+=checkValue(erpml, erRef, tol, errors, "Erpml");
  }
  if (save) saveToFile("Upml",Upml,_format=_vtu);
  // nbErrors+=checkValues("Upml.vtu", rootname+"/Upml.in", tol, errors, "Upml", check);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
