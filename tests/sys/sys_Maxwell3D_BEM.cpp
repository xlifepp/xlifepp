/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_Maxwell3D_BEM.cpp
\author E. Lunéville
\since 18 November 2017
\date 18 November 2017
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_Maxwell3D_BEM
{
Vector<Complex> Ei(const Point& P, Parameters& pars)
{
  Vector<Real> incPol(3,0.); incPol(1)=1.;
  Point incDir(0.,0.,1.) ;
  Real k = pars("k");
  return  incPol*exp(i_*k * dot(P,incDir));
}

Vector<Complex> Eixn(const Point& P, Parameters& pars)
{
  Vector<Real> n=P;
  Real n2=norm2(n);
  if(n2>theEpsilon) n/=n2;
  return  crossProduct(Ei(P,pars),n);
}

Vector<Real> gc(const Point& P, const Point& Q,Parameters& pars)
{
    return Vector<Real>(3,1.);
}

Vector<Complex> Eixnxn(const Point& P, Parameters& pars)
{
  Vector<Real> n=P;
  Real n2=norm2(n);
  if(n2>theEpsilon) n/=n2;
  return crossProduct(crossProduct(Ei(P,pars),n),n);
}

void sys_Maxwell3D_BEM(int argc, char* argv[], bool check)
{
  String rootname = "sys_Maxwell3D_BEM";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  //numberOfThreads(1);
  verboseLevel(20);
  out.precision(5);
  out.setf(ios::scientific);
  String errors;
  Number nbErrors = 0;

  //define parameters and functions
  Real k= 1, radius=1.;
  Parameters pars;
  pars << Parameter(k, "k") << Parameter(radius, "radius");
  Kernel H = Helmholtz3dKernel(pars);      // load Helmholtz3D kernel
  Function fi(Ei, pars);                   // define right hand side function
  Function fixn(Eixn, pars);               // define right hand side function
  Function fixnxn(Eixnxn, pars);           // define right hand side function
  Function Eex(scatteredFieldMaxwellExn,pars);

  // stuff to do integral representation and compute error on segment [(0,0,1.5),(0,0,20)]
  Mesh mseg;
  if (!check)
  {
    mseg=Mesh(Segment(_v1=Point(0,0,5),_v2=Point(0,0,20),_nnodes=100,_domain_name="S"), _shape=triangle);
    mseg.saveToFile(inputsPathTo(rootname+"/mseg.msh"),msh);
  }
  else
  {
     mseg=Mesh(inputsPathTo(rootname+"/mseg.msh"), _name="Segment");
  }
  Domain S = mseg.domain("S");
  Space VS(_domain=S,_interpolation=_P1, _name="VS");
  Unknown WS(VS, _name="WS", _dim=3);
  TermVector Eexa(WS, S, Eex);
  TermMatrix M(intg(S,WS|WS));
  Real nex=sqrt(real(M*Eexa|Eexa));

  // meshing the unit sphere
  Number npa=10;                             //nb of points by diameter/4 of sphere
  theCout<<"npa="<<npa<<eol;
  Mesh meshSh;
  if (!check)
  {
    Sphere sphere(_center=Point(0,0,0), _radius=radius, _nnodes=npa, _domain_name="Gamma");
    meshSh=Mesh(sphere, _shape=triangle, _order=1, _generator=gmsh);
    meshSh.saveToFile(inputsPathTo(rootname+"/mSh.msh"),msh);
  }
  else
  {
     meshSh=Mesh(inputsPathTo(rootname+"/mSh.msh"), _name="Sphere");
  }
  Domain Gamma = meshSh.domain("Gamma");
  //define FE-RT1 space and unknown
  Space RTh(_domain=Gamma, _interpolation=_RT_1, _name="RTh");
  Unknown U(RTh, _name="U"); TestFunction V(U, _name="V");
  Space Vh(_domain=Gamma, _interpolation=_P1, _name="Vh"); Unknown W(Vh,"W",3);

  IntegrationMethods ims(_SauterSchwabIM,4,0.,_defaultRule,5,2.,_defaultRule,3);
  IntegrationMethods im(_defaultRule,10,1.,_defaultRule, 5);
  TermVector Fx(W,Gamma,fixn); TermVector FxRT=projection(Fx,RTh);FxRT.name("FxRT");

  //compute indirect EFIE system and solve it
  BilinearForm as((1./k)*intg(Gamma,Gamma, div(U)*H*div(V),ims)-k*intg(Gamma,Gamma, U*H|V,_method=ims));
  TermMatrix SS(as, _name="S");
  TermVector BIE(intg(Gamma,fi|V), _name="BIE");
  TermVector LambdaIE = directSolve(SS,-BIE,true);
  TermVector EIE=-(1./k)*integralRepresentation(WS, S, intg(Gamma,grad_x(H)*div(U),_method=im), LambdaIE)
                 -k*integralRepresentation(WS, S, intg(Gamma,H*U,_method=im), LambdaIE);
  TermVector erIE=EIE-Eexa;
  Real nerIE=sqrt(real(M*erIE|erIE));
  theCout<<"indirect EFIE L2 error = "<<nerIE<<" relative error = "<<nerIE/nex<<eol;

  //compute direct EFIE system and solve it
  TermMatrix C(intg(Gamma,Gamma,U|(grad_x(H)^V),_method=ims), _name="C");
  TermVector BDE=C*FxRT-0.5*BIE;             //intg(Gamma,Gamma,fixn|(grad_x(H)^V),ims))-0.5*intg(Gamma,fixnxn|V))
  TermVector LambdaDE = directSolve(SS,BDE,true);
  TermVector EDE= integralRepresentation(WS, S, intg(Gamma,grad_x(H)^U,_method=im),FxRT)
                   -(1./k)*integralRepresentation(WS, S, intg(Gamma,grad_x(H)*div(U),_method=im), LambdaDE)
                   -k*integralRepresentation(WS, S, intg(Gamma,H*U,_method=im), LambdaDE);
  TermVector erDE=EDE-Eexa;
  Real nerDE=sqrt(real(M*erDE|erDE));
  theCout<<"  direct EFIE L2 error = "<<nerDE<<" relative error = "<<nerDE/nex<<eol;

  //compute indirect MFIE system and solve it
  TermMatrix N(intg(Gamma,(U^_n)|V), _name="N");
  TermMatrix AIM=0.5*N-C;
  TermVector LambdaIM = directSolve(AIM,BIE,true);
  TermVector EIM= -integralRepresentation(WS, S, intg(Gamma,grad_x(H)^U,_method=im),LambdaIM);
  TermVector erIM=EIM-Eexa;
  Real nerIM=sqrt(real(M*erIM|erIM));
  theCout<<"indirect MFIE L2 error = "<<nerIM<<" relative error = "<<nerIM/nex<<eol;

  //compute direct MFIE system and solve it
  TermMatrix ADM=0.5*N+C;
  TermVector LambdaDM = directSolve(ADM,SS*FxRT,true);
  TermVector EDM= integralRepresentation(WS, S, intg(Gamma,grad_x(H)^U,_method=im),FxRT)
                   -(1./k)*integralRepresentation(WS, S, intg(Gamma,grad_x(H)*div(U),_method=im), LambdaDM)
                   -k*integralRepresentation(WS, S, intg(Gamma,H*U,_method=im), LambdaDM);
  TermVector erDM=EDM-Eexa;
  Real nerDM=sqrt(real(M*erDM|erDM));
  theCout<<"  direct MFIE L2 error = "<<nerDM<<" relative error = "<<nerDM/nex<<eol;

  //compute CFIE system and solve it
  Unknown theta(RTh,"theta"); TestFunction xi(theta,"xi");
  Real eta=0.25; Complex ieta=i_*eta;
  BilinearForm ac= (ieta/k)*intg(Gamma,Gamma, div(U)*H*div(V),_method=ims)-(ieta*k)*intg(Gamma,Gamma, U*H|V,_method=ims)
                 + 0.5*intg(Gamma,(theta^_n)|V)-intg(Gamma,Gamma,theta|(grad_x(H)^V),_method=ims)
                 + intg(Gamma,theta|xi)+intg(Gamma,div(theta)*div(xi))+intg(Gamma,(U^_n)|xi);
  TermMatrix AC(ac, _name="AC");
  TermVector BC(intg(Gamma,fi|V));
  TermVector UT=directSolve(AC,BC);
  TermVector LambdaC=UT(U), Theta=UT(theta);
  TermVector EC =(ieta/k)*integralRepresentation(WS, S, intg(Gamma,grad_x(H)*div(U),_method=im), LambdaC)
                +(ieta*k)*integralRepresentation(WS, S, intg(Gamma,H*U,_method=im), LambdaC)
                - integralRepresentation(WS, S, intg(Gamma,grad_x(H)^theta,_method=im),Theta);
  TermVector erC=EC-Eexa;
  Real nerC=sqrt(real(M*erC|erC));
  theCout<<"         CFIE L2 error = "<<nerC<<" relative error = "<<nerC/nex<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

}

}
