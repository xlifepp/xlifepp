/* 
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file sys_Laplace_var.cpp
  \author E. Lunéville
  \since 14 nov 2013
  \date 14 nov 2013

*/
#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_Laplace_var
{

Matrix<Real> M(const Point& P, Parameters& pa = defaultParameters)
{
 Real x=P(1) , y=P(2);
 Real r=sqrt(x*x+y*y);
 Matrix<Real> rM(2,_idMatrix);
 if((1<r)&&(r<=2))
  {
   rM(1,1)=((r-1)*(r-1)/(r*r));
   rM(2,2)=2;
  }
  return rM;
}

void sys_Laplace_var(int argc, char* argv[], bool check)
{
   String rootname = "sys_Laplace_var";
   trace_p->push(rootname);
   std::stringstream out;
   out.precision(testPrec);
   verboseLevel(0);
   String errors;
   Number nbErrors = 0;

  //create mesh of square
  Strings sidenames("y=0","y=1","x=0","x=1");
  Mesh mesh2d;
  if (!check)
  {
     mesh2d=Mesh(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=11), _shape=_triangle, _order=1, _generator=_structured);
     mesh2d.saveToFile(inputsPathTo(rootname+"/mesh2d.msh"),_msh);
  }
  else
  {
     mesh2d=Mesh(inputsPathTo(rootname+"/mesh2d.msh"), _name="Sphere");
  }
  mesh2d.printInfo();
  Domain omega=mesh2d.domain("Omega");
//  Domain sigmaM=mesh2d.domain("x=0"),  sigmaP=mesh2d.domain("x=1");

  // create interpolation
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u");  TestFunction v(u, _name="v");

  // create bilinear form, linear form and their algebraic representation
  BilinearForm auv=intg(omega,(M*grad(u))|grad(v));
  TermMatrix A(auv, _name="A");
//  EssentialConditions ecs= (u|sigmaM = 1) & (u|sigmaP = 1);
//  TermMatrix A(auv, ecs, _name="A");

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  trace_p->pop();
  
}

}
