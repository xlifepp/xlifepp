/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
    \file sys_EigenSolverSparse.cpp
    \author ManhHa NGUYEN
    \since 28 May 2013
    \date  23 July 2013

    Tests of eigen solver with method:
    +) Block Davidson (Hermitian problem)
    +) Block KrylovSchur (Hermitian and non-hermitian problem)
     The matrices MatrixMarket and their structure are used to make corresponding positive definite matrices:
     URL: http://math.nist.gov/MatrixMarket/data/NEP/mhd/mhd1280b.html
     Size: 1280 x 1280
     NNZ: 22778 entries
     URL: http://math.nist.gov/MatrixMarket/data/NEP/bfwave/bfw782b.html
     Size: 782 x 782
     NNZ: 5982 entries

    Almost functionalities are checked.
    This function may either creates a reference file storing the results (check=false)
    or compares results to those stored in the reference file (check=true)
    It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_EigenSolverSparse
{

Real unoEigenSparse(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

Real cosx2EigenSparse(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return cos(pi_ * x) * cos(pi_ * y);
}

Real cosx3EigenSparse(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  return cos(pi_ * x) * cos(pi_ * y) * cos(pi_ * z);
}

Real fxEigenSparse(const Point& P, Parameters& pa=defaultParameters)
{
  return P(1);
}

Real fyEigenSparse(const Point& P, Parameters& pa=defaultParameters)
{
  return P(2);
}

Real fzEigenSparse(const Point& P, Parameters& pa=defaultParameters)
{
  return P(3);
}

Real fx2EigenSparse(const Point& P, Parameters& pa=defaultParameters)
{
  return P(1)*P(1);
}

template<typename ST>
bool testStandardBlockDavidsonEigenSolver(LargeMatrix<ST>& matK, LargeMatrix<ST>& matPrec, bool isPre,
        Parameters& pl, int blockSize, int nev, Real tol, std::stringstream& out, std::string testName)
{
  typedef NumTraits<ST>              SCT;
  typedef MultiVec<ST>               MV;
  typedef Operator<ST>               OP;
  typedef MultiVecTraits<ST,MV>      MVT;
  typedef OperatorTraits<ST,MV,OP>   OPT;
  ST one = SCT::one();

  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > K(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matK));
  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > Prec(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matPrec));

  SmartPtr<MV> ivec = SmartPtr<MultiVecAdapter<ST> >(new MultiVecAdapter<ST>(matK.nbCols, blockSize));
  ivec->mvRandom();

  SmartPtr<EigenProblem<ST,MV,OP> > problem =
          SmartPtr<EigenProblem<ST,MV,OP> >(new EigenProblem<ST,MV,OP>(K,ivec));


  // Inform the eigenproblem that the operator cK is symmetric
  problem->setHermitian(true);
  //
  // Set the number of eigenvalues requested
  problem->setNEV(nev);

  if (isPre) problem->setPrec(Prec);
  //
  // Inform the eigenproblem that you are done passing it information
  bool testPassed = true;
  bool boolret = problem->setProblem();
  if (boolret)
  {
    out << "\n" << "\n"
        << "BlockDavidson Eigen Solver Test : " << testName << "\n"
        << "-----------------------------------------------------------------------------------" << "\n";

    BlockDavidsonSolMgr<ST,MV,OP> bdSolverMan(problem, pl);
    ComputationInfo returnCode = bdSolverMan.solve();


    if (_success == returnCode)
    {
      EigenSolverSolution<ST,MV> sol = problem->getSolution();
      SmartPtr<MV> evecs = sol.evecs;
      int numev = sol.numVecs;

      // Compute the direct residual to verify the results
      std::vector<Real> normV(numev);
      MatrixEigenDense<ST> t(numev,numev);
      for (int i=0; i<numev; i++)
      {
        t.coeffRef(i,i) = sol.evals[i].realpart;
      }
      SmartPtr<MV> kVecs = MVT::clone(*evecs, numev);

      OPT::apply(*K, *evecs, *kVecs);

      MVT::mvTimesMatAddMv(-one, *evecs, t, one, *kVecs);
      MVT::mvNorm(*kVecs, normV);

      out << "  Direct residual norms computed" << "\n"
          << std::setw(20) << "Eigenvalue" << std::setw(20) << "Residual(M)" << "\n"
          << "----------------------------------------" << "\n";
      for (int i=0; i<numev; i++)
      {
        if (SCT::magnitude(sol.evals[i].realpart) != SCT::zero())
        {
          normV[i] = SCT::magnitude(normV[i]/sol.evals[i].realpart);
        }
        out << std::setw(20) << sol.evals[i].realpart << std::setw(20) << normV[i] << "\n";
        if (normV[i] > tol)
        {
            testPassed = false;
        }
      }
      if (!testPassed) { out << "BlockDavidson eigensolver test failed" << "\n"; }
      return testPassed;
    }
    else {
      out << "BlockDavidson EigenSolver doesn't converge " << "\n";
      return testPassed;
    }
  }
  else out << "BlockDavidson problem cant be set! Please verify " << "\n";
  return testPassed;
}

template<typename ST>
bool testStandardBlockKrylovSchurSolver(LargeMatrix<ST>& matK, LargeMatrix<ST>& matPrec, bool isPre,
        Parameters& pl, int blockSize, int nev, Real tol, std::stringstream& out, std::string testName)
{
  typedef NumTraits<ST>              SCT;
  typedef MultiVec<ST>               MV;
  typedef Operator<ST>               OP;
  typedef MultiVecTraits<ST,MV>      MVT;
  typedef OperatorTraits<ST,MV,OP>   OPT;
  Complex one = SCT::one();

  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > K(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matK));
  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > Prec(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matPrec));

  SmartPtr<MV> ivec = SmartPtr<MultiVecAdapter<ST> >(new MultiVecAdapter<ST>(matK.nbCols, blockSize));
  ivec->mvRandom();

  SmartPtr<EigenProblem<ST,MV,OP> > problem =
          SmartPtr<EigenProblem<ST,MV,OP> >(new EigenProblem<ST,MV,OP>(K,ivec));


  // Inform the eigenproblem that the operator cK is symmetric
  problem->setHermitian(false);
  //
  // Set the number of eigenvalues requested
  problem->setNEV(nev);

  if (isPre) problem->setPrec(Prec);
  //
  // Inform the eigenproblem that you are done passing it information
  bool boolret = problem->setProblem();
  bool testPassed = true;
  if (boolret)
  {
    out << "\n" << "\n"
        << "BlockKrylovSchur Eigen Solver Test : " << testName << "\n"
        << "-----------------------------------------------------------------------------------" << "\n";

    BlockKrylovSchurSolMgr<ST,MV,OP> bkrSolverMan(problem, pl);
    ComputationInfo returnCode = bkrSolverMan.solve();

    if (_success == returnCode)
    {
      EigenSolverSolution<ST,MV> sol = problem->getSolution();
      SmartPtr<MV> evecs = sol.evecs;
      int numev = sol.numVecs;
      int size = matK.nbCols;

      std::vector<int> index = sol.index;
      SmartPtr<MultiVec<Complex> > eigVecs = SmartPtr<MultiVecAdapter<Complex> >(new MultiVecAdapter<Complex>(matK.nbCols, numev));
      if (NumTraits<ST>::isReal())
      {
        for (int j = 0; j < numev; j++)
        {
          for (int i = 0; i < size; i++)
          {
            if (0 == index[j])
            {
//                            ((*eigVecs)(i,j)).real() = (NumTraits<ST>::real((*evecs)(i,j)));
              (*eigVecs)(i,j)=Complex((NumTraits<ST>::real((*evecs)(i,j))), ((*eigVecs)(i,j)).imag());
            }
            else if (1 == index[j])
            {
//                            (*eigVecs)(i,j).real() = NumTraits<ST>::real((*evecs)(i,j));
//                            (*eigVecs)(i,j).imag() = NumTraits<ST>::real((*evecs)(i,j+1));
              (*eigVecs)(i,j)=Complex(NumTraits<ST>::real((*evecs)(i,j)), NumTraits<ST>::real((*evecs)(i,j+1)));
            }
            else
            {
//                            (*eigVecs)(i,j).real() = NumTraits<ST>::real((*evecs)(i,j-1));
//                            (*eigVecs)(i,j).imag() = NumTraits<ST>::real(-(*evecs)(i,j));
              (*eigVecs)(i,j)=Complex(NumTraits<ST>::real((*evecs)(i,j-1)), NumTraits<ST>::real(-(*evecs)(i,j)));
            }
          }
        }
      }
      else
      {
          //eigVecs = sol.evecs;
      }


      // Compute the direct residual to verify the results
      std::vector<Real> normV(numev);
      MatrixEigenDense<Complex> t(numev,numev);
      for (int i=0; i<numev; i++) {
        t.coeffRef(i,i) = Complex(sol.evals[i].realpart,sol.evals[i].imagpart);
      }
//            SmartPtr<MV> kVecs = MVT::clone(*evecs, numev);
//            OPT::apply(*K, *evecs, *kVecs);
//
//            MVT::mvTimesMatAddMv(-one, *evecs, t, one, *kVecs);

      SmartPtr<MultiVec<Complex> > kVecs = MultiVecTraits<Complex,MultiVec<Complex> >::clone(*eigVecs, numev);
      multMatVecLargeMatrixAdapter(*K,*eigVecs, *kVecs);

      MultiVecTraits<Complex,MultiVec<Complex> >::mvTimesMatAddMv(-one, *eigVecs, t, one, *kVecs);
      MultiVecTraits<Complex,MultiVec<Complex> >::mvNorm(*kVecs, normV);

      out << "  Direct residual norms computed" << "\n"
          << std::setw(20) << "Eigenvalue" << std::setw(20) << "Residual(M)" << "\n"
          << "----------------------------------------" << "\n";
      for (int i=0; i<numev; i++) {
        if (NumTraits<Complex>::magnitude(t.coeff(i,i)) != SCT::zero()) {
          normV[i] = NumTraits<Complex>::magnitude(normV[i]/t.coeff(i,i));
        }
        out << std::setw(20) << t.coeff(i,i) << std::setw(20) << normV[i] << "\n";
        if (normV[i] > tol) {
          testPassed = false;
        }
      }
      if (!testPassed) {
        out << "BlockKrylovSchur eigensolver test failed" << "\n";
      }
      return testPassed;
    }
    else {
        out << "BlockKrylovSchur EigenSolver doesn't converge " << "\n";
        return testPassed;
    }
  } else out << "BlockKrylovSchur problem cant be set! Please verify " << "\n";
  return testPassed;
}

template<typename ST>
bool testGeneralizedBlockDavidsonEigenSolver(LargeMatrix<ST>& matK, LargeMatrix<ST>& matM, LargeMatrix<ST>& matPrec,
        bool isPre, Parameters& pl, int blockSize, int nev, Real tol, std::stringstream& out, std::string testName)
{
  typedef NumTraits<ST>              SCT;
  typedef MultiVec<ST>               MV;
  typedef Operator<ST>               OP;
  typedef MultiVecTraits<ST,MV>      MVT;
  typedef OperatorTraits<ST,MV,OP>   OPT;
  ST one = SCT::one();

  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > K(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matK));
  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > M(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matM));
  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > Prec(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matPrec));

//    matK.toSkyline();
//    matK.luFactorize();
//    SmartPtr< const LargeMatrixAdapterInverseGeneralized<LargeMatrix<ST>, ST> > M(new LargeMatrixAdapterInverseGeneralized<LargeMatrix<ST>, ST>(&matK, &matM, _lu));

  SmartPtr<MV> ivec = SmartPtr<MultiVecAdapter<ST> >(new MultiVecAdapter<ST>(matK.nbCols, blockSize));
  ivec->mvRandom();

  SmartPtr<EigenProblem<ST,MV,OP> > problem = SmartPtr<EigenProblem<ST,MV,OP> >(new EigenProblem<ST,MV,OP>(K,M,ivec));


//    SmartPtr<EigenProblem<ST,MV,OP> > problem = SmartPtr<EigenProblem<ST,MV,OP> >(new EigenProblem<ST,MV,OP>(M,ivec));

  problem->setM(M);

  // Inform the eigenproblem that the operator cK is symmetric
  problem->setHermitian(true);
  //
  // Set the number of eigenvalues requested
  problem->setNEV(nev);

//    if (isPre) problem->setPrec(Prec);
  //
  // Inform the eigenproblem that you are done passing it information
  bool boolret = problem->setProblem();
  bool testPassed = true;
  if (boolret)
  {
    out << "\n" << "\n"
        << "BlockDavidson Eigen Solver Test : " << testName << "\n"
        << "-----------------------------------------------------------------------------------" << "\n";

    BlockDavidsonSolMgr<ST,MV,OP> bdSolverMan(problem, pl);
    ComputationInfo returnCode = bdSolverMan.solve();

    if (_success == returnCode)
    {
      EigenSolverSolution<ST,MV> sol = problem->getSolution();
      SmartPtr<MV> evecs = sol.evecs;
      int numev = sol.numVecs;

      // Compute the direct residual to verify the results
      std::vector<Real> normV(numev);
      MatrixEigenDense<ST> t(numev,numev);
      for (int i=0; i<numev; i++) { t.coeffRef(i,i) = sol.evals[i].realpart; }
      SmartPtr<MV> kVecs = MVT::clone(*evecs, numev);
      SmartPtr<MV> mVecs = MVT::clone(*evecs, numev);

      OPT::apply(*K, *evecs, *kVecs);
      OPT::apply(*M, *evecs, *mVecs);

      MVT::mvTimesMatAddMv(-one, *mVecs, t, one, *kVecs);
      MVT::mvNorm(*kVecs, normV);

      out << "  Direct residual norms computed" << "\n"
         << std::setw(20) << "Eigenvalue" << std::setw(20) << "Residual(M)" << "\n"
         << "----------------------------------------" << "\n";
      for (int i=0; i<numev; i++)
      {
        if (SCT::magnitude(sol.evals[i].realpart) != SCT::zero())
          { normV[i] = SCT::magnitude(normV[i]/sol.evals[i].realpart); }
        out << std::setw(20) << sol.evals[i].realpart << std::setw(20) << normV[i] << "\n";
        if (normV[i] > tol) { testPassed = false; }
      }
      if (!testPassed) { out << "BlockDavidson eigensolver test failed" << "\n"; }
      return testPassed;
    }
    else
    {
      out << "BlockDavidson EigenSolver doesn't converge " << "\n";
      return testPassed;
    }
  } else out << "BlockDavidson problem cant be set! Please verify " << "\n";
  return testPassed;
}

template<typename ST>
bool testGeneralizedBlockKrylovSchurEigenSolver(LargeMatrix<ST>& matK, LargeMatrix<ST>& matM, LargeMatrix<ST>& matPrec,
        bool isPre, Parameters& pl, int blockSize, int nev, Real tol, std::stringstream& out, std::string testName)
{
  typedef NumTraits<ST>              SCT;
  typedef MultiVec<ST>               MV;
  typedef Operator<ST>               OP;
  typedef MultiVecTraits<ST,MV>      MVT;
  typedef OperatorTraits<ST,MV,OP>   OPT;
  ST one = SCT::one();

  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > K(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matK));
  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > M(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matM));
  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > Prec(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&matPrec));

  SmartPtr<MV> ivec = SmartPtr<MultiVecAdapter<ST> >(new MultiVecAdapter<ST>(matK.nbCols, blockSize));
  ivec->mvRandom();

  SmartPtr<EigenProblem<ST,MV,OP> > problem =
          SmartPtr<EigenProblem<ST,MV,OP> >(new EigenProblem<ST,MV,OP>(K,M,ivec));

  problem->setM(M);

  // Inform the eigenproblem that the operator cK is symmetric
  problem->setHermitian(true);
  //
  // Set the number of eigenvalues requested
  problem->setNEV(nev);

  if (isPre) problem->setPrec(Prec);
  //
  // Inform the eigenproblem that you are done passing it information
  bool boolret = problem->setProblem();
  bool testPassed = true;
  if (boolret)
  {
    out << "\n" << "\n"
        << "BlockKrylovSchur Eigen Solver Test : " << testName << "\n"
        << "-----------------------------------------------------------------------------------" << "\n";

    BlockKrylovSchurSolMgr<ST,MV,OP> bkrSolverMan(problem, pl);
    ComputationInfo returnCode = bkrSolverMan.solve();

    if (_success == returnCode)
    {
      EigenSolverSolution<ST,MV> sol = problem->getSolution();
      SmartPtr<MV> evecs = sol.evecs;
      int numev = sol.numVecs;

      // Compute the direct residual to verify the results
      std::vector<Real> normV(numev);
      MatrixEigenDense<ST> t(numev,numev);
      for (int i=0; i<numev; i++) { t.coeffRef(i,i) = sol.evals[i].realpart; }
      SmartPtr<MV> kVecs = MVT::clone(*evecs, numev);
      SmartPtr<MV> mVecs = MVT::clone(*evecs, numev);

      OPT::apply(*K, *evecs, *kVecs);
      OPT::apply(*M, *evecs, *mVecs);

      MVT::mvTimesMatAddMv(-one, *mVecs, t, one, *kVecs);
      MVT::mvNorm(*kVecs, normV);

      out << "  Direct residual norms computed" << "\n"
          << std::setw(20) << "Eigenvalue" << std::setw(20) << "Residual(M)" << "\n"
          << "----------------------------------------" << "\n";
      for (int i=0; i<numev; i++)
      {
        if (SCT::magnitude(sol.evals[i].realpart) != SCT::zero())
        {
          normV[i] = SCT::magnitude(normV[i]/sol.evals[i].realpart);
        }
        out << std::setw(20) << sol.evals[i].realpart << std::setw(20) << normV[i] << "\n";
        if (normV[i] > tol) {
            testPassed = false;
        }
      }
      if (!testPassed) { out << "BlockKrylovSchur eigensolver test failed" << "\n"; }
      return testPassed;
    }
    else
    {
      out << "BlockKrylovSchur EigenSolver doesn't converge " << "\n";
      return testPassed;
    }
  } else out << "BlockKrylovSchur problem cant be set! Please verify " << "\n";
  return testPassed;
}

void Laplace_Neumann_Eigen_Pk(const GeomDomain& omg, int k,  Real h, const String& Elt, std::ostream& out, bool intern)
{
  std::stringstream s;

  //create Lagrange Pind space and unknown
  Space Vk(_domain=omg, _FE_type=Lagrange, _order=k, _name="Vk", _optimizeNumbering);
  Unknown u(Vk, _name="u");
  TestFunction v(u, _name="v");

  //create bilinear form and linear form
  BilinearForm muv=intg(omg, u * v);
  BilinearForm auv = intg(omg, grad(u) | grad(v)) + intg(omg, u * v);
  BilinearForm ruv=intg(omg, grad(u) | grad(v));

  LinearForm fv;
  Real a;
  TermVector Uex;
  TermVector un(u,omg,1., _name="un");
  Real cpu = 0;

  if (omg.dim()==2)
  {
    fv=intg(omg, cosx2EigenSparse * v);
    Uex = TermVector(u, omg, cosx2EigenSparse);
    a=1 + 2.*pi_ * pi_;
  }
  else if (omg.dim()==3)
  {
    fv=intg(omg, cosx3EigenSparse * v);
    Uex=TermVector(u, omg, cosx3EigenSparse);
    a = 1 + 3.*pi_ * pi_;
  }
  Uex /=a;

  // create FEterm
  TermMatrix A(auv, _name="a(u,v)");
  TermMatrix R(ruv, _name="R");
  TermMatrix M(muv, _name="M");
  TermVector B(fv, _name="f(v)");

  // check FE computation
  if (omg.dim()==3)
  {
    TermVector X(u,omg,fxEigenSparse, _name="x");
    TermVector Y(u,omg,fyEigenSparse, _name="y");
    TermVector Z(u,omg,fzEigenSparse, _name="z");
    TermVector X2(u,omg,fx2EigenSparse, _name="x2");
    out<<"-----------------------------------------------------------------------------------------"<<eol;
    out<<"FE computation check "<< Elt << k << ", h=" << h << ", nb dl=" << Uex.size() << eol;
    out<<"   norminfty(R*1) = "<<norminfty(R*un);
    out<<", (M*1|1) = "<<(M*un|un);
    out<<", (A*1|1) = "<<(A*un|un)<<eol;
    out<<"  (R*x|x) = "<<(R*X|X);
    out<<", (R*y|y) = "<<(R*Y|Y);
    out<<", (R*z|z) = "<<(R*Z|Z);
    out<<", (R*x2|x2) = "<<(R*X2|X2);   // =32/3
    out<<", (B|1) = "<<(B|un)<<eol;
    out<<"-----------------------------------------------------------------------------------------"<<eol;
  }

  SuTermMatrix* suterm = A.begin()->second;

  LargeMatrix<Complex>* cA = suterm->entries()->cEntries_p;
  Number cRow, cCol, rRow, rCol,nZA;
  if (0 != cA)
  {
    out << "Complex matrix " << std::endl;
    out << "StorageType " << cA->storageType() << std::endl;
    cRow = suterm->entries()->cEntries_p->nbRows;
    cCol = suterm->entries()->cEntries_p->nbCols;
    out << "Matrix size " << "row x col " << cRow << " x " << cCol << std::endl;
    out << "Number non-zero " << cA->nbNonZero() << std::endl;
  }

  LargeMatrix<Real>* rA = suterm->entries()->rEntries_p;// *pB = 0;
  if (0 != rA)
  {
    out << "Real matrix " << std::endl;
    out << "StorageType " << rA->storageType() << std::endl;
    rRow = suterm->entries()->rEntries_p->nbRows;
    rCol = suterm->entries()->rEntries_p->nbCols;
    out << "Matrix size " << "row x col " << rRow << " x " << rCol << std::endl;
    out << "Number non-zero " << rA->nbNonZero() << std::endl;
    nZA=rA->nbNonZero();
  }

  EigenElements eigenPair;

  Number nev = 10;

  // In each case, compute eigen values and eigen vectors, then compute errors

  out << "Testing with STANDARD problem ---------------------------------- " << std::endl;
  out << "Testing with Largest Magnitude " << std::endl;
  out << "--------------------------------" << std::endl;

  if (intern) {
  cpu = cpuTime("Begin Standard KrylovSchur");
  eigenPair = eigenInternSolve(A, _mode=_krylovSchur, _nev=nev);
  cpu = cpuTime("End Standard Krylov Schur");
  out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
  for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
  {
    out << "Eigenvalue " << eigenPair.value(i+1);
    TermVector eigVec(eigenPair.vector(i+1));
    eigVec *= eigenPair.value(i+1);
    eigVec -= A*eigenPair.vector(i+1);
    out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
  }

  cpu = cpuTime("Begin Standard Davidson");
  eigenPair = eigenInternSolve(A, _mode=_davidson, _nev=nev, _which="LM");
  cpu = cpuTime("End Standard Davidson");
  out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
  for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
  {
    out << "Eigenvalue " << eigenPair.value(i+1);
    TermVector eigVec(eigenPair.vector(i+1));
    eigVec *= eigenPair.value(i+1);
    eigVec -= A*eigenPair.vector(i+1);
    out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
  }

  } else {
  cpu = cpuTime("Begin Standard Arpack");
  eigenPair = arpackSolve(A, _nev=nev, _which="LM");
  cpu = cpuTime("End Standard Arpack");
  out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
  for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
  {
    TermVector eigVec(eigenPair.vector(i+1));
    out << "Eigenvalue " << eigenPair.value(i+1);
    eigVec *= eigenPair.value(i+1);
    eigVec -= A*eigenPair.vector(i+1);
    out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
  }
  }// else

  out << "Testing with Smallest Magnitude " << std::endl;
  out << "--------------------------------" << std::endl;

  if (intern) {
    cpu = cpuTime("Begin Standard Krylov Schur");
    eigenPair = eigenInternSolve(A, _mode=_krylovSchur, _nev=nev, _which="SM");
    cpu = cpuTime("End Standard Krylov Schur");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      out << "Eigenvalue " << eigenPair.value(i+1);
      TermVector eigVec(eigenPair.vector(i+1));
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }

    cpu = cpuTime("Begin Standard Davidson");
    eigenPair = eigenInternSolve(A, _mode=_davidson, _nev=nev, _which="SM");
    cpu = cpuTime("End Standard Davidson");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      out << "Eigenvalue " << eigenPair.value(i+1);
      TermVector eigVec(eigenPair.vector(i+1));
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }
  }
  else
  {
    cpu = cpuTime("Begin Standard Arpack");
    eigenPair = arpackSolve(A, _nev=nev, _which="SM");
    cpu = cpuTime("End Standard Arpack");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      TermVector eigVec(eigenPair.vector(i+1));
      out << "Eigenvalue " << eigenPair.value(i+1);
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }
  }// else

  out << "Testing with GENERALIZED problem ---------------------------------- " << std::endl;
  out << "Testing with Largest Magnitude " << std::endl;
  out << "--------------------------------" << std::endl;

  if (intern)
  {
    cpu = cpuTime("Begin Generalized Krylov Schur");
    eigenPair = eigenInternSolve(A, M, _mode=_krylovSchur, _nev=nev);
    cpu = cpuTime("End Generalized Krylov Schur");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      out << "Eigenvalue " << eigenPair.value(i+1);
      TermVector eigVec;
      multMatrixVector(M, eigenPair.vector(i+1), eigVec);
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }

    cpu = cpuTime("Begin Generalized Davidson");
    eigenPair = eigenInternSolve(A, M, _mode=_davidson, _nev=nev, _which="LM");
    cpu = cpuTime("End Generalized Davidson");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      out << "Eigenvalue " << eigenPair.value(i+1);
      TermVector eigVec;
      multMatrixVector(M, eigenPair.vector(i+1), eigVec);
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }
  }
  else
  {
    cpu = cpuTime("Begin Generalized Arpack");
    eigenPair = arpackSolve(A, M, _nev=nev);
    cpu = cpuTime("End Generalized Arpack");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      TermVector eigVec;
      multMatrixVector(M, eigenPair.vector(i+1), eigVec);
      out << "Eigenvalue " << eigenPair.value(i+1);
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }
  }// else

  out << "Testing with Smallest Magnitude " << std::endl;
  out << "--------------------------------" << std::endl;

  if (intern)
  {
    cpu = cpuTime("Begin Generalized Krylov Schur");
    eigenPair = eigenInternSolve(A, M, _mode=_krylovSchur, _nev=nev, _which="SM");
    cpu = cpuTime("End Generalized Krylov Schur");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      out << "Eigenvalue " << eigenPair.value(i+1);
      TermVector eigVec;
      multMatrixVector(M, eigenPair.vector(i+1), eigVec);
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }

    cpu = cpuTime("Begin Generalized Davidson");
    eigenPair = eigenInternSolve(A, M, _mode=_davidson, _nev=nev, _which="SM");
    cpu = cpuTime("End Generalized Davidson");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      out << "Eigenvalue " << eigenPair.value(i+1);
      TermVector eigVec;
      multMatrixVector(M, eigenPair.vector(i+1), eigVec);
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }
  }
  else
  {
    cpu = cpuTime("Begin Generalized Arpack");
    eigenPair = arpackSolve(A, M, _nev=nev, _which="SM");
    cpu = cpuTime("End Generalized Arpack");
    out << "Number of found eigen value " << eigenPair.numberOfEigenValues() << std::endl;
    for (Number i = 0 ; i < eigenPair.numberOfEigenValues(); ++i)
    {
      TermVector eigVec;
      multMatrixVector(M, eigenPair.vector(i+1), eigVec);
      out << "Eigenvalue " << eigenPair.value(i+1);
      eigVec *= eigenPair.value(i+1);
      eigVec -= A*eigenPair.vector(i+1);
      out << " \tError of eigen vector " << i+1 << " = " << norm2(eigVec) << std::endl;
    }
  }// else

  clear(R, A, B, M);
  clear(Uex);
}

void test_EigenTerm(std::stringstream& out, bool intern)
{
  std::vector<int> Param(4);
  Param[0]=1;      // with triangles (=1)
  Param[1]=0;      // with quadrangles (=1)
  Param[2]=0;      // with tetrahedra (=1)
  Param[3]=0;      // with hexahedra (=1)

  std::string meshInfo;
  verboseLevel(1);
  Number nbDivMin = 1, nbDivMax=5;
  Number ordMin =1, ordMax=2;
  std::vector<String> sidenames(4, "");
  Real h;

  if (Param[0] == 1)
  {
    out << "==========================================" << eol;
    out << "=                  TEST 2D               =" << eol;
    out << "==========================================" << eol<<eol;
    // create a mesh and Domains TRIANGLES
    meshInfo= " TRIANGULAR mesh of [0,1]x[0,1] with nbStep =  ";
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      meshInfo += tostring(div);
      Real h=1./(5*div);
      Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=5*div+1), _shape=_triangle, _order=1, _generator=_structured, _name=meshInfo);
      Domain omega = mesh2d.domain("Omega");
      out << "# TRIANGULAR mesh:  " << div << " sudivisions, h = " <<h <<eol;
      out << "===========================================" << eol;
      for (Number ord=ordMin; ord<=ordMax; ord++)
      {
        std::cout << " -> Triangles P" <<ord<<eol;
              out << " -> Triangles P" <<ord<<eol;
        Laplace_Neumann_Eigen_Pk(omega, ord, h, "P", out, intern);
      }
    }
  }

  if (Param[1] == 1)
  {
    out << "==========================================" << eol;
    out << "=                  TEST 2D               =" << eol;
    out << "==========================================" << eol<<eol;
    // create a mesh and Domains QUADRANGLES sidenames[0] = "x=0";
    meshInfo= " QUADRANGULAR mesh of [0,1]x[0,1] with nbStep =  ";
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      meshInfo += tostring(div);
      Real h=1./(5*div);
      Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=5*div+1), _shape=_quadrangle, _order=1, _generator=_structured, _name=meshInfo);
      Domain omega=mesh2d.domain("Omega");
      out << " #  QUADRANGULAR mesh           " << div << " sudivisions, h = " <<h <<eol;
      out << "==============================================" << eol;
      for (Number ord=ordMin; ord<=ordMax; ord++)
      {
        out << " -> Quadrangles Q" <<ord<< eol;
        Laplace_Neumann_Eigen_Pk(omega, ord, h, "Q", out, intern);
      }
    }
  }

  nbDivMin =4, nbDivMax=5;
  // create a mesh and Domains TETRAHEDRON
  if (Param[2] == 1)
  {
    out << eol<<"========================================== " << eol;
    out << "=                  TEST 3D               = " << eol;
    out << "========================================== " << eol;
    meshInfo=" TETRAHEDRAL mesh of cube ";

    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      Mesh mesh3d(Cube(_origin=Point(0.,0.,0.),_length=1.,_nnodes=div-1), _shape=_tetrahedron, _order=1, _generator=_subdiv, _name=meshInfo);
      mesh3d.renameDomain(0,"Omega");
      Domain omega = mesh3d.domain("Omega");
      h = 2. / std::pow(2,static_cast<int>(div));
      meshInfo += tostring(div);
      out << "# TETRAHEDRAL mesh :  " << div << " sudivisions, h = " <<h <<eol;
      out << "=========================================" << eol;
      for (Number ord = ordMin; ord <= ordMax; ord++)
      {
        out << " -> Tetrahedra P" <<ord<< eol;
        Laplace_Neumann_Eigen_Pk(omega, ord, h, "P", out, intern);
      }
    }

  }
  // create a hexaedron mesh and domains
  if (Param[3] == 1)
  {
    out << eol<<"========================================== " << eol;
    out << "=                  TEST 3D               = " << eol;
    out << "========================================== " << eol;
    meshInfo=" HEXAHEDRAL mesh of cube [0,1]x[0,1]x[0,1] ";
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      Mesh mesh3d(Cuboid(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_zmin=0,_zmax=1,_nnodes=2*div+1), _shape=_hexahedron, _order=1, _generator=_structured, _name=meshInfo);
      Domain omega = mesh3d.domain("Omega");
      h =1./(2*div);
      meshInfo += tostring(div);
      out << " #  HEXAHEDRAL mesh:   " << div << " sudivisions, h = " <<h <<eol;
      out << "==============================================" << eol;
      for (Number ord = ordMin; ord <= ordMax; ord++)
      {
        out << " -> Hexahedra Q" <<ord<< eol;
        Laplace_Neumann_Eigen_Pk(omega, ord, h, "Q", out, intern);
      }
    }
  }
}

void test_EigenMat(std::stringstream& out)
{
  const std::string rMatrixSym("bfw782b.coo");
  LargeMatrix<Real> rMatSym(inputsPathTo(rMatrixSym), _coo, 782, _cs, _symmetric);
  const std::string rMatrixSymPos("bfw782c.coo");
  LargeMatrix<Real> rMatSymPos(inputsPathTo(rMatrixSymPos), _coo, 782, _cs, _symmetric);
  const std::string rMatrixNonSym("bfw782a.coo");
  LargeMatrix<Real> rMatNonSym(inputsPathTo(rMatrixNonSym), _coo, 782, 782, _cs, _dual);
  const std::string rMatrixSymPrec("bfw782d.coo");
  LargeMatrix<Real> rMatSymPrec(inputsPathTo(rMatrixSymPrec), _coo, 782, _cs, _symmetric);

  const std::string cMatrixSym("mhd1280b.coo");
  LargeMatrix<Complex> cMatSym(inputsPathTo(cMatrixSym), _coo, 1280, _cs, _selfAdjoint);
  const std::string cMatrixSymPos("mhd1280c.coo");
  LargeMatrix<Complex> cMatSymPos(inputsPathTo(cMatrixSymPos), _coo, 1280, _cs, _selfAdjoint);
  const std::string cMatrixNonSym("mhd1280a.coo");
  LargeMatrix<Complex> cMatNonSym(inputsPathTo(cMatrixNonSym), _coo, 1280, 1280, _cs, _dual);
  const std::string cMatrixSymPrec("mhd1280d.coo");
  LargeMatrix<Complex> cMatSymPrec(inputsPathTo(cMatrixSymPrec), _coo, 1280, _cs, _selfAdjoint);

  // Eigensolver parameters
  const Number nev = 6; //6
  int blockSize = 5;//6;
  int numBlocks = 10;//14;
  int maxRestarts = 25;//8;
  Real tol = 1.0e-6;
  String which("SM"), ortho("SVQB");
  bool locking = true;
  bool insitu = false;
  MsgEigenType verbosity = _errorsEigen;
  //
  // Create parameter list to pass into the solver manager
  Parameters pl;
  pl.set("Verbosity", (int)verbosity);
  pl.set("Which", which);
  pl.set("Orthogonalization", ortho);
  pl.set("Block Size", blockSize);
  pl.set("Num Blocks", numBlocks);
  pl.set("Maximum Restarts", maxRestarts);
  pl.set("Convergence Tolerance", tol);
  pl.set("Use Locking", locking);
  pl.set("Locking Tolerance", tol/10);
  pl.set("In Situ Restarting", insitu);
  //pl.set("Write Out Result", (void*)&out);

  which = "SM";
  pl.set("Which", which);
  testStandardBlockDavidsonEigenSolver(rMatSym, rMatSymPrec, true,
              pl, blockSize, nev, tol, out, std::string("Standard symmetric matrix test with Smallest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testStandardBlockDavidsonEigenSolver(rMatSym, rMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Standard symmetric matrix test with Largest Magnitude"));


  which = "SM";
  pl.set("Which", which);
  testGeneralizedBlockDavidsonEigenSolver(rMatSym, rMatSymPos, rMatSymPrec, true,
              pl, blockSize, nev, tol, out, std::string("Generalized symmetric matrix test with Smallest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testGeneralizedBlockDavidsonEigenSolver(rMatSym, rMatSymPos, rMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Generalized symmetric matrix test with Largest Magnitude"));


  which = "SM";
  pl.set("Which", which);
  testStandardBlockDavidsonEigenSolver(cMatSym, cMatSymPrec, true,
              pl, blockSize, nev, tol, out, std::string("Standard Hermititian matrix test with Smallest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testStandardBlockDavidsonEigenSolver(cMatSym, cMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Standard Hermititian matrix test with Largest Magnitude"));

  which = "SM";
  pl.set("Which", which);
  testGeneralizedBlockDavidsonEigenSolver(cMatSym, cMatSymPos, cMatSymPrec, true,
              pl, blockSize, nev, tol, out, std::string("Generalized Hermitian matrix test with Smallest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testGeneralizedBlockDavidsonEigenSolver(cMatSym, cMatSymPos, cMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Generalized Hermititian matrix test with Largest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testStandardBlockKrylovSchurSolver(rMatSym, rMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Standard symmetric matrix test with Largest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testStandardBlockKrylovSchurSolver(rMatNonSym, rMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Standard non-symmetric matrix test with Largest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testStandardBlockKrylovSchurSolver(cMatSym, cMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Standard symmetric matrix test with Largest Magnitude"));

  which = "LM";
  pl.set("Which", which);
  testStandardBlockKrylovSchurSolver(cMatNonSym, cMatSymPrec, false,
              pl, blockSize, nev, tol, out, std::string("Standard non-symmetric matrix test with Largest Magnitude"));
}

void sys_EigenSolverSparse(int argc, char* argv[], bool check)
{
  String rootname = "sys_EigenSolverSparse";
  trace_p->push(rootname);
  std::stringstream ssout; // string stream receiving results
  ssout.precision(testPrec);
  Number nbErrors=0;
  String errors;
  verboseLevel(0);

  // std::cout << "Begin: Computing eigen values of matrices from Matrix Market" << std::endl;
  // test_EigenMat(ssout);
  // std::cout << "End  : Computing eigen values of matrices from Matrix Market" << std::endl;

  // std::cout << "Begin: Computing eigen values of matrices from Xlife++ (intern solver)" << std::endl;
  // test_EigenTerm(ssout,true);
  // std::cout << "End  : Computing eigen values of matrices from Xlife++ (intern solver)" << std::endl;

  thePrintStream << "Begin: Computing eigen values of matrices from Xlife++ (arpack solver)" << std::endl;
  test_EigenTerm(ssout,false);
  nbErrors+=checkValue(ssout, rootname+"/ArpackSolver.in", errors, "EigenTerm", check);
  thePrintStream << "End  : Computing eigen values of matrices from Xlife++ (arpack solver)" << std::endl;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------

  if (check)
  {
    if (nbErrors == 0 ){theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}

