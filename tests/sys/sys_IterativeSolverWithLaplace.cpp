/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_IterativeSolverWithLaplace.cpp
\authors E. Luneville
\since 13 jan 2018
\date 13 jan 2018

Test   all iterative solvers with a matrix using operand \f$-\Delta+Id\f$
  A = intg( omg, grad(u)|grad(v) + u * v)
  solve A*X =S
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using namespace xlifepp;

namespace sys_IterativeSolverWithLaplace
{
Number NE=0;
void printResultsOfCV(const TermMatrix& MAT, const TermVector& B, const TermVector& X, const TermVector& Uex, Real epsilon, std::ostream& out, Real et=0.)
{
  TermVector E=B-MAT*X, D=X-Uex;
  double_t res=norm2(E), err=norm2(D), nB=norm2(B), nUex=norm2(Uex);
  std::stringstream ss;
  if (res< epsilon*nB) ss << "CV with epsilon = " << epsilon ;
  else {ss << "NO CV with epsilon = " << epsilon; NE++;}
  ss << " ||residu|| = " << res/nB << " ||Uex-Unum|| = " << err <<" ";
  if (et!=0.) ss<<" time  = "<<et;
  ss<<eol<<eol;
  out<<ss.str();
  theCout<<ss.str();
}

void checkSolver(const String& solver, TermMatrix& A, TermVector& B, TermVector& X0, TermVector& Uex, Real eps, Number maxit,
                 Preconditioner& pdiag, Preconditioner& pildlt, Preconditioner& pldlt, std::ostream& out, bool withTime=false)
{
  TermVector X;
  Real et=0;
  const Number krylovDim = 2*sqrt(A.numberOfCols());
  out << "================================= "<<solver<<" ================================="<<eol;
  theCout << "================================= "<<solver<<" ================================="<<eol;
  out<<"* without Preconditionner" << eol;
  theCout << "* without Preconditionner" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
  out << "* with Preconditionner (diag)" << eol;
  theCout <<"* with Preconditionner (diag)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
  out << "* with Preconditionner (ildlt)" << eol;
  theCout <<"* with Preconditionner (ildlt)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
  out << "* with Preconditionner (ldlt)" << eol;
  theCout << "* with Preconditionner (ldlt)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
}

void Laplace_Pk(const GeomDomain& omg, int k,  Real h, const std::stringstream& nmFich, std::ostream& out)
{
  stringstream s;
  trace_p->push("Laplace_Pk");
  verboseLevel(0);

  //create Lagrange Pind space and unknown
  Space Vk(_domain=omg, _FE_type=Lagrange, _order=k, _name="Vk", _optimizeNumbering);
  Unknown u(Vk, _name="u");  TestFunction v(u, _name="v");

  // Create bilinear form
  BilinearForm auv=intg(omg,  grad(u)|grad(v))+ intg(omg, u * v);

  // Create and compute term matrix
  TermMatrix A(auv, _name="a(u,v)");
  TermMatrix Atmp=A;
  out<<A<<eol;
  out<<"Matrice "<<A.numberOfRows()<<"x"<<A.numberOfCols();
  out<<" nb non-zero coeficients of the matrix: "<<A.nnz()<<eol<<eol;
  theCout<<"Matrice "<<A.numberOfRows()<<"x"<<A.numberOfCols();
  theCout<<" nb non-zero coeficients of the matrix: "<<A.nnz()<<eol<<eol;

//================================================================================================
  TermVector X0(u,omg,0.,_name="vecteur initial");
  TermVector X1(u,omg,1.5,_name="vecteur initial");
  TermVector B, UN(u,omg,2.,_name="un");
  B = A*UN;
  // Parameters
  const Real epsilon = 1.e-6;
  const Number nbIteration = 5000;
  bool withTime=false;
  if (withTime) elapsedTime("preparation", theCout);
  PreconditionerTerm preDiag(A, _diagPrec);
  if (withTime) elapsedTime("diagonal preconditioning", theCout);
  PreconditionerTerm preLdLt(A, _ldltPrec);
  if (withTime) elapsedTime("ILdLt preconditioning", theCout);
  PreconditionerTerm preILdLt(A, _ildltPrec);
  if (withTime) elapsedTime("LdLt preconditioning", theCout);

  checkSolver("CG",A,B,X0,UN,epsilon, nbIteration, preDiag, preILdLt, preLdLt, out, withTime);
  checkSolver("BICG",A,B,X0,UN,epsilon, nbIteration, preDiag, preILdLt, preLdLt, out, withTime);
  checkSolver("BICGSTAB",A,B,X0,UN,epsilon, nbIteration, preDiag, preILdLt, preLdLt, out, withTime);
  checkSolver("CGS",A,B,X0,UN,epsilon, nbIteration, preDiag, preILdLt, preLdLt, out, withTime);
  checkSolver("GMRES",A,B,X0,UN,epsilon, nbIteration, preDiag, preILdLt, preLdLt, out, withTime);
  checkSolver("QMR",A,B,X0,UN,epsilon, nbIteration, preDiag, preILdLt, preLdLt, out, withTime);

  trace_p->pop();
}

// Programme principal:
void sys_IterativeSolverWithLaplace(int argc, char* argv[], bool check)
{
  String rootname = "sys_IterativeSolverWithLaplace";
  trace_p->push(rootname);

  bool withTriangles   = true;
  bool withQuadrangles = false;
  bool withTetrahedra  = false;
  bool withHexahedra   = false;

  //numberOfThreads(1);

  std::stringstream out;
  out.precision(testPrec);
  string meshInfo;
  verboseLevel(1);
  Number nbErrors=0;
  String errors;
  Number nbDivMin, nbDivMax, dd=1;
  Number ordMin, ordMax;
  std::vector<String> sidenames(4, "");
  Real h;
  string nmFich;
  Number NbLignes;
  Number NbSolvers = 2;

//   out << "==========================================" << eol;
//   out << "=                                    TEST 2D                                       =" << eol;
//   out << "==========================================" << eol<<eol;
  if (withTriangles)
  {
    //create a mesh and Domains TRIANGLES
    // out << "# -> Triangles P" <<1<< eol;
    std::stringstream NmEps1;
    meshInfo= " Triangle mesh of [0,1]x[0,1] with nbStep =  ";
    nbDivMin =5, nbDivMax=5;
    ordMin = 1, ordMax=1;
    NbLignes= (nbDivMax - nbDivMin + 1) * (ordMax - ordMin +1) * (NbSolvers+2)+1;
    for(Number div = nbDivMin; div <= nbDivMax; div+=1)
    {
      meshInfo += tostring(div);
      Real h=1./(dd*div);
      Number nn=std::pow(2,div)+1;
      Mesh mesh2d(SquareGeo(_origin=Point(0.,0.), _length=1, _nnodes=nn), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[1,2]");
      Domain omega = mesh2d.domain("Omega");
      theCout <<eol<<eol<< "===========================================" << eol;
      theCout << "# Triangle mesh:  " << div << " sudivisions, h = " <<h <<eol;
      theCout << "===========================================" << eol;
      for (Number ord=ordMin; ord<=ordMax; ord++)
      {
        theCout<<"--------------------------" <<eol;
        theCout << " -> interpolation P" <<ord<< eol;
        theCout<<"--------------------------" <<eol;
        Laplace_Pk(omega, ord, h, NmEps1, out);
      }
    }
  }
  if (withQuadrangles)
  {
    //create a mesh and Domains Quadrangles
    meshInfo= " quadrangle mesh of [0,1]x[0,1] with nbStep =  ";
    nbDivMin =5, nbDivMax=5;
    ordMin = 1, ordMax=2;
    std::stringstream NmEps2;
    NmEps2 << "Epsi";
    NbLignes= (nbDivMax - nbDivMin + 1) * (ordMax - ordMin +1) * (NbSolvers+2)+1;
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      meshInfo += tostring(div);
      Real h=1./(dd*div);
      Number nn=std::pow(2,div)+1;
      Mesh mesh2d(SquareGeo(_origin=Point(0.,0.), _length=1, _nnodes=nn), _shape=_quadrangle, _order=1, _generator=_structured, _name="Q1 mesh of [0,1]x[0,1]");
      Domain omega = mesh2d.domain("Omega");
      theCout <<eol<<eol<< "===========================================" << eol;
      theCout << "# Quadrangle mesh:  " << div << " sudivisions, h = " <<h <<eol;
      theCout << "===========================================" << eol;
      for (Number ord=ordMin; ord<=ordMax; ord++)
      {
        theCout<<"--------------------------" <<eol;
        theCout << " -> interpolation P" <<ord<< eol;
        theCout<<"--------------------------" <<eol;
        Laplace_Pk(omega, ord, h, NmEps2, out);
      }
    }
  }

  // create a mesh and Domains TETRAHEDRON
  if (withTetrahedra)
  {
    // out << eol<<"========================================== " << eol;
    // out << "=                                    TEST 3D                                       = " << eol;
    // out << "========================================== " << eol;
    meshInfo=" TETRAHEDRONS mesh of cube ";
    nbDivMin =2, nbDivMax=2;
    ordMin = 1, ordMax=2;

    std::stringstream NmEps3;
    NmEps3 << "Epsi";
    NbLignes= (nbDivMax - nbDivMin + 1) * (ordMax - ordMin +1) * (NbSolvers+2)+1;
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      Number nn=std::pow(2,div)+1;
      Mesh mesh3d(Cube(_origin=Point(0.,0.,0.), _length=1., _nnodes=nn), _shape=_tetrahedron, _order=1, _generator=_subdiv, _name=meshInfo);
      Domain omega = mesh3d.domain("Omega");
      h = 2. / std::pow(2,div);
      meshInfo += tostring(div);
      theCout <<eol<<eol<< "=========================================" << eol;
      theCout << "# TETRAHEDRONS mesh :  " << div << " sudivisions, h = " <<h <<eol;
      theCout << "=========================================" << eol;
      for (Number ord = ordMin; ord <= ordMax; ord++)
      {
        theCout<<"--------------------------" <<eol;
        theCout << " -> interpolation P" <<ord<< eol;
        theCout<<"--------------------------" <<eol;
        Laplace_Pk(omega, ord, h, NmEps3, out);
      }
    }
  }

  // create a mesh and Domains HEXAHEDRON
  if (withHexahedra)
  {
    meshInfo=" HEXAHEDRONS mesh of cube ";
    nbDivMin =2, nbDivMax=2;
    ordMin = 1, ordMax=2;

    std::stringstream NmEps4;
    NmEps4 << "Epsi";
    NbLignes= (nbDivMax - nbDivMin + 1) * (ordMax - ordMin +1) * (NbSolvers+2)+1;
    for (Number div = nbDivMin; div <= nbDivMax; div++)
    {
      Number nn=std::pow(2,div)+1;
      Mesh mesh3d(Cube(_origin=Point(0.,0.,0.), _length=1., _nnodes=nn), _shape=_hexahedron, _order=1, _generator=_structured, _name=meshInfo);
      Domain omega = mesh3d.domain("Omega");
      h = 2. / std::pow(2,div);
      meshInfo += tostring(div);
      theCout <<eol<<eol<< "=========================================" << eol;
      theCout << "# HEXAHEDRONS mesh :  " << div << " sudivisions, h = " <<h <<eol;
      theCout << "=========================================" << eol;
      for (Number ord = ordMin; ord <= ordMax; ord++)
      {
        theCout<<"--------------------------" <<eol;
        theCout << " -> interpolation P" <<ord<< eol;
        theCout<<"--------------------------" <<eol;
        Laplace_Pk(omega, ord, h, NmEps4, out);
      }
    }
  }

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  nbErrors=NE;
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
