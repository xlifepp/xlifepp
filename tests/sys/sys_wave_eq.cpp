/*ssss
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file sys_wave_eq.cpp 
  \since 22 nov 2014
  \date  22 nov 2014

  Test on wave equation with P1 Lagrange and leap-frog time scheme;

     d2u/dt2 -c2*Delta(u)=h(t)g(x)   on Omega
       du/dn=0 on dOmega
       u(0)=du/dt(0)=0  on Omega
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_wave_eq {


Real g(const Point& P, Parameters& pa = defaultParameters)
{
  Real d=P.distance(Point(0.5,0.5));
  Real R= 0.02;//source radius
  Real amp= 1./(pi_*R*R);//source amplitude (constant power)
  if(d<=0.02) return amp; else return 0.;
}

Real h(const Real& t)
{
  Real a=10000; //gaussian slope
  Real t0=0.04; //gaussian time center
  return exp(-a*(t-t0)*(t-t0));
}


void sys_wave_eq(int argc, char* argv[], bool check)
{
  String rootname = "sys_wave_eq";
  trace_p->push(rootname);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;
  bool save=false;
  //create a mesh and Domains
  Strings sidenames(4,"Gamma");

  Mesh mesh2d;
  if (!check)
  {
    mesh2d=Mesh(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=70,_side_names=sidenames),_shape=_triangle, _order=1, _generator=gmsh);
    mesh2d.saveToFile(inputsPathTo(rootname + "/mesh2d70.msh"),_msh);
  }
  else
  {
    mesh2d=Mesh(inputsPathTo(rootname+"/mesh2d70.msh"), _name="mesh of the unit square");
  }
  Domain omega=mesh2d.domain("Omega");
  Domain gamma=mesh2d.domain("Gamma");

  //create interpolation
  Space V(_domain=omega, _interpolation=P1, _name="V");
  Unknown u(V, _name="u");  TestFunction v(u, _name="v");

  // define problem (a=1)
  BilinearForm a = intg(omega, grad(u)|grad(v)), m=intg(omega, u * v);
  LinearForm lg = intg(omega, g*v);

  // compute matrix and rhs
  TermMatrix A(a, _name="A"), M(m, _name="M");
  TermVector G(lg, _name="G");

  // leap-frog scheme
  Real c=1, dt=0.004, dt2=dt*dt, cdt2=c*c*dt2;
  Number nbt=200;
  TermMatrix L;
  ldltFactorize(M,L);
  thePrintStream<<"factorization cputime="<<cpuTime()<<eol;
  TermVectors U(nbt);
  TermVector zeros(u,omega,0.);
  Number nbZ=zeros.size();
  Matrix<Real> MErrRel(nbZ,nbt);
  U(1)=zeros;  U(2)=zeros;
  MErrRel.column(1, U(1).asRealVector() );
  MErrRel.column(2, U(2).asRealVector());
  Real t=dt;
  Vector<Real> vtmp;
  for (Number n=2; n<nbt; n++, t+=dt)
  {
    U(n+1)=2*U(n)-U(n-1)-factSolve(L,cdt2*(A*U(n))-dt2*h(t)*G);
    if (save) saveToFile("U"+tostring(n+1),U(n+1),_format=vtu);
    MErrRel.column(n+1, U(n+1).asVector(vtmp));
  }
  Real cpt=cpuTime();
  nbErrors+= checkValues(MErrRel.roundToZero(0.2), rootname+"/U.in",  tol, errors, "U", check);
  thePrintStream<<"Neumann leap frog cputime="<<cpt<<" (by step "<<cpt/nbt<<")"<<eol;
  //saveToFile("U",U,_format=vtu);

  // same problem with Dirichlet condition u=0 on Gamma
  TermMatrix A0(a,u|gamma=0, _name="A"), M0(m,u|gamma=0, _name="M");
  TermMatrix L0; ldltFactorize(M0,L0);
  TermVectors U0(nbt);
  U0(1)=zeros;  U0(2)=zeros;
  Matrix<Real> MErrRel0(nbZ,nbt);
  MErrRel0.column(1, U0(1).asRealVector());
  MErrRel0.column(2, U0(2).asRealVector());
  t=dt;
  cpuTime();
  for (Number n=2; n<nbt; n++, t+=dt)
  {
    U0(n+1)=2*U0(n)-U0(n-1)-factSolve(L0,cdt2*(A*U0(n))-dt2*h(t)*G);
    U0(n+1).setValue(gamma,0.);
    if (save) saveToFile("U0"+tostring(n+1),U0(n+1),_format=vtu);
    MErrRel0.column(n+1, U0(n+1).asVector(vtmp));
  }
  cpt=cpuTime();
  nbErrors+= checkValues(MErrRel0.roundToZero(0.2), rootname+"/U0.in",  tol, errors, "U0", check);

  thePrintStream<<"Dirichlet leap frog cputime="<<cpt<<" (by step "<<cpt/nbt<<")"<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
