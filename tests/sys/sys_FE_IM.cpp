/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_FE_IM.cpp
\author E. Lunéville
\since 1 March 2014
\date 1 March 2014

 Test of FEM-IEM method on 2D Helmholtz problem on a disk

*/

#include "xlife++-libs.h"
#include "testUtils.hpp"
#include <exception>

using namespace xlifepp;

namespace sys_FE_IM
{

Complex data_g(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), k=pa("k");
  Vector<Complex> g(2,0.);
  g(1) = i_*k*exp(i_*k*x);
  return dot(g,P/norm2(P));  //dr(e^{ikx}
}

Complex u_inc(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), k=pa("k");
  return exp(i_*k*x);
}

void sys_FE_IM(int argc, char* argv[], bool check)
{
  String rootname = "sys_FE_IM";
  trace_p->push(rootname);
  // std::stringstream out;
  // out.precision(testPrec);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;
  bool save=false;

  //numberOfThreads(8);

  //parameters
  Number nh = 20;               // number of elements on Gamma
  Real h=2.*pi_/nh;             // size of mesh
  Real re=1.+2.*h;              // exterior radius
  Number ne=Number(2*pi_*re/h); // number of elements on Sigma
  Real l = 4.*re;               // length of exterior square
  Number nr=Number(4*l/h);      // number of elements on exterior square
  Real k= 4, k2=k*k;            // wavenumber
  Parameters pars;
  pars << Parameter(k,"k") << Parameter(k2,"k2");
  Kernel H=Helmholtz2dKernel(pars);
  Function g(data_g,pars);
  Function ui(u_inc,pars);
  //Mesh and domains definition
  Mesh mesh;
  if (!check)
  {
    Disk d1(_center=Point(0.,0.),_radius=1,_nnodes=nh,
            _side_names=Strings(4,"Gamma"));
    Disk d2(_center=Point(0.,0.),_radius=re,_nnodes=ne,_domain_name="Omega",
            _side_names=Strings(4,"Sigma"));
    SquareGeo sq(_center=Point(0.,0.),_length=l,_nnodes=nr,_domain_name="Omega_ext");
    mesh=Mesh(sq+(d2-d1),_shape=_triangle, _order=1, _generator=gmsh);
    mesh.saveToFile(inputsPathTo(rootname+"/mesh.msh"),msh);
  }
  else
  {
    mesh=Mesh(inputsPathTo(rootname+"/mesh.msh"), _name="Mesh mesh");
  }
  Domain omega=mesh.domain("Omega");
  Domain sigma=mesh.domain("Sigma");
  Domain gamma=mesh.domain("Gamma");
  Domain omega_ext=mesh.domain("Omega_ext");  //for integral representation
//  sigma.setNormalOrientation( outwardsDomain , omega ) ; // outwards normals
//  gamma.setNormalOrientation( outwardsDomain , omega ) ;

  //create P2 Lagrange interpolation
  Space V(_domain=omega, _interpolation=_P2, _name="V");
  Unknown u(V, _name="u");  TestFunction v(u, _name="v");

  // create bilinear form and linear form
  Complex lambda=-i_*k;
  BilinearForm auv =
    intg(omega,grad(u)|grad(v))-k2*intg(omega,u*v)+lambda*intg(sigma,u*v)
    +intg(sigma,gamma,u*(grad_x(grad_y(H)|_ny)|_nx)*v)
    +lambda*intg(sigma,gamma,u*(grad_y(H)|_ny)*v);
  BilinearForm alv =
    intg(sigma,gamma,u*(grad_x(H)|_nx)*v)+lambda*intg(sigma,gamma,u*H*v);
  TermMatrix ALV(alv), A(auv);
  TermVector B(intg(gamma,g*v));
  TermVector G(u,gamma,g);
  B+=ALV*G;

  //solve linear system AU=F
  TermVector U=directSolve(A,B);
  if (save) 
  { 
    saveToFile("U.vtk",U,vtk);
    sigma.saveNormalsToFile("n2_sigma",_vtk);
    gamma.saveNormalsToFile("n2_gamma",_vtk);
  }
  //integral representation on omega_ext
  Space Vext(_domain=omega_ext, _interpolation=_P2, _name="Vext", _notOptimizeNumbering);
  Unknown uext(Vext, _name="uext");
  TermVector Uext =-integralRepresentation(uext, omega_ext, intg(gamma,(grad_y(H)|_ny)*U))
                   +integralRepresentation(uext, omega_ext, intg(gamma,H*G));
  if (save) saveToFile("Uext.vtk",Uext,vtk);
  nbErrors+=checkValues("U_Omega.vtk", rootname+"/U_Omega.in", tol, errors, "U", check);
  nbErrors+=checkValues("n2_gamma.vtk", rootname+"/n2_gamma.in", tol, errors, "n2_gamma", check);
  nbErrors+=checkValues("n2_sigma.vtk", rootname+"/n2_sigma.in", tol, errors, "n2_sigma", check);
  nbErrors+=checkValues("Uext_Omega_ext.vtk", rootname+"/Uext_Omega_ext.in", tol, errors, "Uext", check);
  //total field
  TermVector Ui(u, omega, ui), Utot=Ui+U;
  TermVector Uiext(uext, omega_ext, ui), Utotext=Uiext+Uext;
  if (save) 
  {
    saveToFile("Utot.vtk",Utot,vtk);
    saveToFile("Utotext.vtk",Utotext,vtk);
  }
  nbErrors+=checkValues("Utot_Omega.vtk", rootname+"/Utot_Omega.in", tol, errors, "Utot", check);
  nbErrors+=checkValues("Utotext_Omega_ext.vtk", rootname+"/Utotext_Omega_ext.in", tol, errors, "Utotext", check);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
