/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file sys_isotropic_plate.cpp
  \since 17 sep 2014
  \date 17 jun 2014
  \author Eric Lunéville

  isotropic plate with hybrid DtN - waveguide diffraction
  2D geometry - 3D unknown

  solve the elastic diffraction problem in a 2D section of 3D plate (z invariant)
  isotropic plate with square stiffener

                   -b  -b+s        b-s  b
                    ---------------------               y=h+e+a
                    |      stringer     |
                    |   ------------    |
                    |   |           |   |
                    |   |           |   |
                a   |   |           |   |
                    |   |           |   |
                    |   ------------    |
                    |                   |
    glue     -----------------------------------         y=h+e
             -----------------------------------         y=h
    sigmaM   |              plate              | sigmaP
             --------------------------------------->    y=0
            x=-L   x=-b                x= b   x=L



	Wilcox bench
	   height of plate    h = 3 mm
	   stringer length    a = 25.4 mm
	   stringer thickness s = 3.5 mm
	   glue thickness     e = 0.25mm
       plate/stringer in aluminum :  rho=2.7, E=70,   nu=1/3 --> lambda=52.5   mu=26.25
                             glue :  rho=1.2, E=2.64, nu=1/3 --> lambda=1.98   mu=0.99

	Main computation steps :
	 - compute modes in cross section of the infinite plate
	 - compute hybrid DtN operator
	 - compute solution
    Main parameters
     - k : frequency
     - theta : angle of incident plane waves

*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_isotropic_plate
{

std::map<Int, Matrix<Real> > Cs;
std::map<Int, Real> rhos;

// return density regarding material Id (index starts from 1)
Real rho(const Point& P, Parameters& pars=defaultParameters)
{
  Number mat = materialIdFromParameters(pars);
  return rhos[mat-1];
}

// return elasticity tensor (2D) regarding material Id (index starts from 1)
Matrix<Real> C(const Point& P, Parameters& pars=defaultParameters)
{
  Number mat = materialIdFromParameters(pars);
  if (mat==0) mat=1;
  return Cs[mat-1];
}

//map sigmaP/M (2D) -> [0,h] (1D)  return a vector of size 1 !
Vector<Real> mapS(const Point& P, Parameters& pa = defaultParameters)
{
  Real y1=-100.082779;
  return Vector<Real>(1,P(2)-y1);
}

void sys_isotropic_plate(int argc, char* argv[], bool check)
{
  String rootname = "sys_isotropic_plate";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(0);
  numberOfThreads(1);    //use only one thread
  String errors;
  Number nbErrors = 0;
  Real tol=0.001;
  bool saveF=false;

  /*====================================================================================
                                        elaticity data
      Length in mm
      Time in s
      Elasticity tensor in GPa
      Density in mg/mm3
      Frequency in Mhz
    ====================================================================================*/
  /*
                                        | 2m+l    l     l           |    | E11 |
                                        |   l   2m+l    l           |    | E22 |
  Voigt form in sigma=C*epsilon, C  =  |   l     l   2m+l          |  * | E33 |   isotropic case
                                        |                  2m       |    | E23 |
                                        |                     2m    |    | E13 |
                                        |                        2m |    | E12 |
  */

  //---------------------  isotropic material --------------------------------
  //wilcox test
//   Mesh mesh2d(inputsPathTo("wilcox.msh"), _name="wilcox mesh");
//   Domain sigmaP=mesh2d.domain("Sigma+"), sigmaM=mesh2d.domain("Sigma-");
//   Domain plate=mesh2d.domain("Plate"), glue=mesh2d.domain("Glue"), stringer=mesh2d.domain("Stringer");
//   plate.setMaterialId(1);stringer.setMaterialId(1);glue.setMaterialId(2);
//   Domain Omega=merge(plate,glue,stringer,"omega");
//   Matrix<Real> C0(6,6), C1(6,6);
//   Real lambda=52.5, mu=26.25; rhos[0]=2.7;
//   Real dmpl=2*mu+lambda;
//   C0(1,1)=dmpl; C0(2,2)=dmpl; C0(3,3)=dmpl;
//   C0(1,2)=lambda; C0(2,1)=lambda; C0(2,3)=lambda; C0(3,2)=lambda; C0(1,3)=lambda; C0(3,1)=lambda;
//   C0(4,4)=2*mu; C0(5,5)=2*mu; C0(6,6)=2*mu;
//   Cs[0] = C0;
//   lambda=1.98;mu=0.99; dmpl=2*mu+lambda; rhos[1]=1.2;
//   C1(1,1)=dmpl; C1(2,2)=dmpl; C1(3,3)=dmpl;
//   C1(1,2)=lambda; C1(2,1)=lambda; C1(2,3)=lambda; C1(3,2)=lambda; C1(1,3)=lambda; C1(3,1)=lambda;
//   C1(4,4)=2*mu; C1(5,5)=2*mu; C1(6,6)=2*mu;
//   Cs[1] = C1;
//   Real h=3;               //plate height
//   Real a=15;              //plate length
//   Real beta= 0, k=1.;     //incidence angle in Oy, Oz, value of k
//   Real gamma=k*sin(beta); //ky
//   Real freq=0.2;
//   Real omg2=4*pi*pi*freq*freq;

  //vahan  comparison
  Mesh mesh2d(inputsPathTo(rootname+"/vahan_plate.msh"), _name="vahan plate");
  Domain Omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("IT2"), sigmaM=mesh2d.domain("IT1");
  Omega.setMaterialId(1);

  //isotropic
  Matrix<Real> C0(6,6);
  Real lambda=112.134, mu=83.53, rho0=7.86;
  Real dmpl=2*mu+lambda;
  C0(1,1)=dmpl; C0(2,2)=dmpl; C0(3,3)=dmpl;
  C0(1,2)=lambda; C0(2,1)=lambda; C0(2,3)=lambda; C0(3,2)=lambda; C0(1,3)=lambda; C0(3,1)=lambda;
  C0(4,4)=2*mu; C0(5,5)=2*mu; C0(6,6)=2*mu;
  Cs[0] = C0; rhos[0]=rho0;

  Real h=6.25;            //plate height
  //Real x1=-355.009 ,x2=-249.075;  //abscissa of left/right end of plate (use to set 0 phase at x1)
  Real beta= 0, k=1.;     //incidence angle in Oy, Oz, value of k
  Real gamma=k*sin(beta); //ky
  Real freq=0.2;          //frequency in MHz
  Real omg2=4.*pi_*pi_*freq*freq;

  /*====================================================================================
                                          diffraction data
      ====================================================================================*/


  /*====================================================================================
                compute eigen vectors in cross section S (SAFE-gamma method)
    ====================================================================================*/
  //compute mode on 1D segment S [0, np*h]
  // the coordinate axis system is :
  //
  //          x
  //          |  y
  //          | /
  //          |/
  //          |-------- z

  Real x=0.;
  Number my=19;
  Mesh meshS(Segment(_xmin=0.,_xmax=h,_nnodes=my+1,_domain_name="S"), _order=1, _generator=_structured, _name="Section mesh");
  Domain S=meshS.domain("S");
  /*
                                | 0    M  |     | M    0  |
    matrices of 1D eigen system  |         | = l |         |
                                | A0   A1 |     | 0    A2 |
  */
  Space V1(_domain=S,_interpolation=_P2, _name="V1");       //P1 Lagrange interpolation
  Unknown U(V1, _name="U", _dim=3);        //3 vector unknown U
  Unknown gU(V1, _name="gU", _dim=3);      //3 vector unknown gamma.u
  TestFunction V(U, _name="V");
  TestFunction gV(gU, _name="gV");

  BilinearForm avp = omg2*intg(S,(rho0*U)|gV)
                     - intg(S,(C0%epsilonG(U,1,1,0,0))%epsilonG(gV,1,1,0,0))
                     - gamma*gamma*intg(S,(C0%epsilonG(U,0,0,1,0))%epsilonG(gV,0,0,1,0))
                     - i_*gamma*intg(S,(C0%epsilonG(U,0,0,1,0))%epsilonG(gV,1,1,0,0))
                     + i_*gamma*intg(S,(C0%epsilonG(U,1,1,0,0))%epsilonG(gV,0,0,1,0))     //A0 part
                     - gamma*intg(S,(C0%epsilonG(gU,0,0,1,0))%epsilonG(gV,0,0,0,1))
                     - gamma*intg(S,(C0%epsilonG(gU,0,0,0,1))%epsilonG(gV,0,0,1,0))
                     - i_*intg(S,(C0%epsilonG(gU,0,0,0,1))%epsilonG(gV,1,1,0,0))
                     + i_*intg(S,(C0%epsilonG(gU,1,1,0,0))%epsilonG(gV,0,0,0,1))          // A1 part
                     + intg(S,(rho0*gU)|V);                                               // M Part
  BilinearForm bvp = intg(S,(rho0*U)|V,_symmetry=_symmetric)
                     + intg(S,(C0%epsilonG(gU,0,0,0,1))%epsilonG(gV,0,0,0,1),_symmetry=_symmetric); // A2 part

  TermMatrix A(avp, _name="A"), B(bvp, _name="B");
  cpuTime("SAFE : compute(A,B)");

  //compute eigen values and eigen vectors, using arpack
  Number n=20;
  EigenElements eigs = eigenSolve(A, B, _nev=n, _which="SM", _tolerance=1.0e-12);

  cpuTime("SAFE : eigenSolver");
  if (eigs.numberOfEigenValues()==0)
  {
    thePrintStream<<"========================================================================="<<eol;
    thePrintStream<<"                        ARPACK solver fails"<<eol;
    thePrintStream<<"========================================================================="<<eol;
    trace_p->pop();
  }

  /*==============================================================================================
    keep only eigenvalues with positive real part or positive imaginary part and sort them :
    first real eigenvalues (descending), then other eigenvalues sorted by ascending imaginary part
  ===============================================================================================*/
  std::map<Real,Number> sortedEigsR, sortedEigsC;
  for (Number k=1; k<=n; k++)
  {
    Complex lk=eigs.value(k);
    if (abs(lk.imag())<=theTolerance)  //real eigenvalue
    {if (lk.real()>= 0) sortedEigsR[lk.real()]=k;}  // later add criterium (im(Xk)|re(Yk)) < 0
    else
    {if (lk.imag()>= 0) sortedEigsC[lk.imag()]=k;}
  }
  n=sortedEigsR.size()+sortedEigsC.size();
  TermVectors evs(n);
  Vector<Complex> betas(n);
  std::map<Real,Number>::reverse_iterator rit=sortedEigsR.rbegin();
  Number m=1; String modetype;
  for (; rit!=sortedEigsR.rend(); m++, rit++)
  {
    betas(m) = eigs.value(rit->second);
    evs(m)=eigs.vector(rit->second)(U);
  }
  std::map<Real,Number>::iterator it=sortedEigsC.begin();
  for (; it!=sortedEigsC.end(); m++, it++)
  {
    betas(m) = eigs.value(it->second);
    if(abs(betas(m).real())<=theTolerance) betas(m)=Complex(0.,betas(m).imag());
    evs(m)=eigs.vector(it->second)(U);
  }

  //compute sigma=(s_11, s_22, s_33, s_23, s_13, s_12) from U, using Voigt to matrix operator
  Unknown sigma(V1, _name="sigma", _dim=6); // 6_vector unknown sigma
  TestFunction tau(sigma, _name="tau");     // 6_vector test function tau
  TermMatrix Ms(intg(S,voigtToM(sigma)%voigtToM(tau)), _name="Ms");
  TermMatrix Exy(intg(S,C0%epsilonG(U,1,1,0,0)%voigtToM(tau))+gamma*intg(S,C0%epsilonG(U,0,0,1,0)%voigtToM(tau)), _name="Exy");
  TermMatrix Ez(intg(S,C0%epsilonG(U,0,0,0,1)%voigtToM(tau)), _name="Ez");
  TermMatrix M12(intg(S,U[1]*sigma[5]) + intg(S,U[2]*sigma[4]), _name="M12");
  TermMatrix M3(intg(S,U[3]*sigma[3]), _name="M3");

  TermMatrix Msf; factorize(Ms, Msf);
  TermVectors svs(n);
  Number ns=0;  //number of propagative modes
  for (Number k=1; k<=n; k++)
  {
    TermVector vk = Exy*evs(k) + i_*betas(k)*(Ez*evs(k));
    svs(k)=factSolve(Msf,vk);
    TermVector m12k=M12*evs(k), m3k=M3*evs(k);
    Complex jk = std::sqrt(dotRC(m12k,svs(k))-dotRC(m3k,svs(k)));
    Complex dwdb = dotRC(imag(m12k),real(svs(k)))-dotRC(real(m3k),imag(svs(k)));
    modetype=" (evanescent ";
    if (abs(imag(betas(k))) < theTolerance) {ns++; modetype=" (propagative ";}
    evs(k)/=jk;  svs(k)/=jk;
    if (norm(evs(k)(U[2])) < 1.E-12) modetype+=" Lamb mode)"; else modetype+="accoustic mode)";
    thePrintStream<<"beta_"<<tostring(k)<<" = "<<betas(k)<<modetype<<" jk="<<jk;
    if (abs(imag(betas(k))) < theTolerance) thePrintStream<<" dwdb="<<dwdb<<eol;
    thePrintStream<<eol;
//      thePrintStream<<"evs("<<tostring(k)<<")="<<evs(k);
//      thePrintStream<<"svs("<<tostring(k)<<")="<<svs(k)<<eol;
  }

// orthogonality modes matrix
  Matrix<Complex> Omn(n,n);
  for (Number k=1; k<=n; k++)
  {
    TermVector m12k=M12*evs(k);
    for (Number q=1; q<=n; q++)
    {
      Omn(k,q) = dotRC(m12k,svs(q))-dotRC(M3*evs(q),svs(k));  //transpose or not ?
      if (abs(Omn(k,q))<=theTolerance) Omn(k,q)=0.;
      thePrintStream<<" j_"<<tostring(k)<<"_"<<tostring(q)<<" = "<<Omn(k,q);
    }
    // nbErrors+=checkValues(Omn, rootname + "/Omn.in", tol, errors, "orthogonality modes matrix", check);
    thePrintStream<<eol;
  }

  //clean
  clear(A, B, Ms, Msf);
  clear(Exy, Ez, M12, M3);
  cpuTime("SAFE : prepare spectral basis");

  /*====================================================================================
                            solve 2D diffraction problem
    ====================================================================================
    the coordinate axis system is :

            y
            |  z
            | /
            |/
            |-------- x
  */
  //space definitions
  Space V2(_domain=Omega, _interpolation=_P2, _name="V2"); // Pk Lagrange interpolation on Omega
  Unknown u(V2, _name="u", _dim=3); TestFunction v(u, _name="v"); // 3_vector displacement unknown u(x,y)
  Space Vtp(_domain=sigmaP, _interpolation=_P2, _name="Vtp", _notOptimizeNumbering); // Pk Lagrange interpolation
  Unknown tp(Vtp, _name="tp", _dim=1); TestFunction qp(tp, _name="qp"); //vector strain unknown t = tau_3 on sigma+
  Space Vtm(_domain=sigmaM, _interpolation=_P2, _name="Vtm", _notOptimizeNumbering); // Pk Lagrange interpolation
  Unknown tm(Vtm, _name="tm", _dim=1); TestFunction qm(tm, _name="qm"); //vector strain unknown t = tau_3 on sigma-

  //construct tensor kernels (change components  x,y,z -> y,z,x )
  TermVectors tsn(n), t3n(n), usn(n), u3n(n);
  Unknown s3(V2, _name="s3", _dim=3);
  Vector<Number>ci(3); ci(1)=0; ci(2)=5; ci(3)=4;    //map unknown components 0 5 4
  for (Number k=1; k<=n; k++) //extract transverse component of displacement and longitudinal component of Sigma
  {
    usn(k)=evs(k); usn(k).changeUnknown(u,0,1,2); usn(k).name()="us_"+tostring(k);
    u3n(k)=evs(k)(U[3]); u3n(k).name()="u3_"+tostring(k);
    t3n(k)=svs(k)(sigma[3]); t3n(k).name()="t3_"+tostring(k);
    tsn(k)=svs(k); tsn(k).changeUnknown(u,0,5,4); tsn(k).name()="ts_"+tostring(k); //to be compatible with inner product by a 3-vector, ci={0,5,4} corresponds to 0 s13 s23 vectors
  }
  Vector<Complex> Omn_inv(n,Complex(1.,0.)); //isotropic case
  Function fmaps(mapS);
  TensorKernel ts_ts(tsn,Omn_inv); ts_ts.xmap=fmaps; ts_ts.ymap=fmaps; ts_ts.name="ts_ts";
  TensorKernel u3_u3(u3n,Omn_inv); u3_u3.xmap=fmaps; u3_u3.ymap=fmaps; u3_u3.name="u3_u3";
  TensorKernel u3_ts(u3n,Omn_inv,tsn); u3_ts.xmap=fmaps; u3_ts.ymap=fmaps; u3_ts.name="u3_ts";  //may be transpose Omn_inv in anisotropic case
  TensorKernel ts_u3(tsn,Omn_inv,u3n); ts_u3.xmap=fmaps; ts_u3.ymap=fmaps; ts_u3.name="ts_u3";

  cpuTime("DIFFRACTION : prepare tensor kernels");

  //construct bilinear forms  with transverse components (2,3) NOTE: only gamma=0 has been validated
  BilinearForm aut = intg(Omega,(C%epsilonG(u,1,1,1,0))%epsilonG(v,1,1,1,0))
                     + i_*gamma*intg(Omega,(C%epsilonG(u,0,0,0,1))%epsilonG(v,1,1,1,0))
                     - i_*gamma*intg(Omega,(C%epsilonG(u,1,1,1,0))%epsilonG(v,0,0,0,1))
                     + gamma*gamma*intg(Omega,(C%epsilonG(u,0,0,0,1))%epsilonG(v,0,0,0,1))
                     - omg2*intg(Omega,(rho*u)|v)                                        //omega part
                     - intg(sigmaP,sigmaP,u|ts_ts|v)  - intg(sigmaP,sigmaP,tp*u3_ts|v)  + intg(sigmaP,tp*v[1])
                     - intg(sigmaP,sigmaP,u|ts_u3*qp) - intg(sigmaP,sigmaP,tp*u3_u3*qp) + intg(sigmaP,u[1]*qp)
                     - intg(sigmaM,sigmaM,u|ts_ts|v)  - intg(sigmaM,sigmaM,tm*u3_ts|v)  - intg(sigmaM,tm*v[1])
                     - intg(sigmaM,sigmaM,u|ts_u3*qm) - intg(sigmaM,sigmaM,tm*u3_u3*qm) - intg(sigmaM,u[1]*qm);
  TermMatrix Aut(aut, _name="Aut");

  //construct right hand side related to mode diffraction from mode computation
  Unknown tsm(Vtm, _name="tsm", _dim=3); Unknown tsp(Vtp, _name="tsp", _dim=3);
  BilinearForm blm = intg(sigmaM,sigmaM,u|ts_ts|v)  + intg(sigmaM,sigmaM,tm*u3_ts|v)  + intg(sigmaM,tsm|v)
                     + intg(sigmaM,sigmaM,u|ts_u3*qm) + intg(sigmaM,sigmaM,tm*u3_u3*qm) + intg(sigmaM,u[1]*qm);
  BilinearForm blp = intg(sigmaP,sigmaP,u|ts_ts|v)  + intg(sigmaP,sigmaP,tp*u3_ts|v)  - intg(sigmaP,tsp|v)
                     + intg(sigmaP,sigmaP,u|ts_u3*qp) + intg(sigmaP,sigmaP,tp*u3_u3*qp) - intg(sigmaP,u[1]*qp);

  TermMatrix Blm(blm, _name="Blm"), Blp(blp, _name="Blp");
  cpuTime("DIFFRACTION : construct linear system");

  //factorize matrix Aut
  TermMatrix LU;
  factorize(Aut,LU);
  cpuTime("DIFFRACTION : LU factorisation");

  DomainMap dmapm(sigmaM,S,mapS);
  DomainMap dmapp(sigmaP,S,mapS);

  //construct mode projectors Pm and Pp
  Space Sm(_basis=tsn, _name="Sm"), Um(_basis=u3n, _name="Um");
  Unknown sm(Sm, _name="sm"), um(Um, _name="um");
  BilinearForm prm=intg(sigmaM,u|sm)+intg(sigmaM,tm*um),
               prp=intg(sigmaP,u|sm)+intg(sigmaP,tp*um);
  TermMatrix Pm(prm, _name="Pm"), Pp(prp, _name="Pp");

  //solve systems and construct scattering matrix for non evanescent modes
  Matrix<Complex> scat(2*ns,2*ns); //[R-- T-+; T+- R++]
  IOFormat iof=_vtu;
  for (Number k=1;k<=ns;k++)
  {
    //compute rhs with mode k as left incident mode
    //----------------------------------------------
    TermVector tsk=tsn(k).mapTo(sigmaM,tsm); tsk.name()="ts_"+tostring(k)+"map";
    TermVector t3k=t3n(k).mapTo(sigmaM,tm); t3k.name()="t3_"+tostring(k)+"map";
    TermVector usk=usn(k).mapTo(sigmaM,u); usk.name()="us_"+tostring(k)+"map";
    TermVector u3k=u3n(k).mapTo(sigmaM,u[1]); u3k.name()="u3_"+tostring(k)+"map";
    TermVector f=tsk-t3k+usk+u3k; f.name()="f"+tostring(k);
    //f*=-exp(i*betas(k)*(x1-x2)/2.);  // 0 phase at middle
    f*=-1;                             // 0 phase at sigmaM (x=x1)
    TermVector rhs=Blm*f;
    //solve system
    TermVector sol=factSolve(LU, rhs);
    if (saveF) saveToFile("u-_"+tostring(k),sol(u),_format=iof);
    //construct scattering coefficients
    TermVector Skm=Pm*sol, Skp=Pp*sol;
    Vector<Complex> S1, S2, Sm, Sp;
    Sm = Skm.subVector(sm).asVector(S1) + Skm.subVector(um).asVector(S2);
    Sp = Skp.subVector(sm).asVector(S1) + Skp.subVector(um).asVector(S2);
    scat.set(k,k,1,ns,Sm(1,ns));      //R--
    scat.set(k,k,ns+1,2*ns,Sp(1,ns)); //T-+
    scat(k,k)-=1; //substract incident field

    //compute rhs with mode k as right incident mode
    //----------------------------------------------
    tsk=tsn(k).mapTo(sigmaP,tsp); t3k=t3n(k).mapTo(sigmaP,tp);
    usk=usn(k).mapTo(sigmaP,u);   u3k=u3n(k).mapTo(sigmaP,u[1]);
    f=usk-u3k-tsk-t3k;
    //f*=-exp(i*betas(k)*(x1-x2)/2.);  // 0 phase at middle
    f*=-1;                             // 0 phase at sigmaP (x=x2)
    rhs=Blp*f;
    //solve system
    sol=factSolve(LU, rhs);
    if (saveF) saveToFile("u+_"+tostring(k),sol(u),_format=iof);
    //construct scattering coefficients
    Skm=Pm*sol; Skp=Pp*sol;
    Sm = Skm.subVector(sm).asVector(S1) + Skm.subVector(um).asVector(S2);
    Sp = Skp.subVector(sm).asVector(S1) + Skp.subVector(um).asVector(S2);
    scat.set(ns+k,ns+k,1,ns,Sm(1,ns));      //T+-
    scat.set(ns+k,ns+k,ns+1,2*ns,Sp(1,ns)); //R++
    scat(ns+k,ns+k)-=1; //substract incident field
  }

  Matrix<Real> absscat=abs(scat);
  absscat.roundToZero(1.E-08);
  thePrintStream<<"scat="<<eol<<scat<<eol;
  thePrintStream<<"abs(scat)="<<eol<< absscat<<eol;
  if (saveF) scat.saveToFile("scat.dat");
  if (saveF) absscat.saveToFile("absscat.dat");
  cpuTime("DIFFRACTION : solve linear systems");
  // nbErrors+=checkValues(absscat, rootname + "/absscat.in", tol, errors, "absscat", check);

  thePrintStream<<"U saved, that's all folks"<<eol;
  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
}

}
