/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_IterSolversHemholtzDtN.cpp
\authors E. Luneville
\since 31 jan 2024
\date 31 jan 2024

Test all iterative solvers on Helmholtz problem with DtN


and compare to results with DirectSolve
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using namespace xlifepp;

namespace sys_IterSolversHelmholtzDtN
{

Real k=20;    // wave number
Real h=1;     // waveguide height
Real a=1;     // waveguide length
Real r=0.05;  // obstacle radius
Number ni=1;  // incident mode index
Number NE=0;  // number of errors

//cos basis
Real cosn(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Int n=pa("basis index")-1;       //get the index of function to compute
  if (n==0) return std::sqrt(1./h);
  else      return std::sqrt(2./h)*std::cos(n*pi_*y/h);
}

//data function on Sigma0
Complex g(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Complex betan=sqrt(Complex(k*k-ni*ni*pi_*pi_/(h*h)));
  return -i_*betan*sqrt(2./h)*cos(ni*pi_*y/h);         //normal derivative on Sigma0 of mode ni
}

class myOp2
{public :
  TermMatrix * pA,*pT;         //pointer to store TermMatrix
  mutable Number nbprod;
  ValueType valueType() const  //mandatory function
  {if(pA->valueType()==xlifepp::_complex || pT->valueType()==xlifepp::_complex)
      return xlifepp::_complex; else return xlifepp::_real;}
  myOp2(TermMatrix& A, TermMatrix& T) : pA(&A), pT(&T) {nbprod=0;}

};
void multMatrixVector ( const myOp2& O, const TermVector& X, TermVector& R)  //mandatory
{ R = X-factSolve(*O.pA,*O.pT*X);O.nbprod++;}  //(I-inv(A)*T)*X

class myOp3
{public :
  TermMatrix * pA,*pL,*pD;         //pointer to store TermMatrix
  mutable Number nbprod;
  ValueType valueType() const  {return xlifepp::_complex;} //mandatory function
  myOp3(TermMatrix& A, TermMatrix& L, TermMatrix& D ) : pA(&A), pL(&L), pD(&D) {nbprod=0;}
};
void multMatrixVector ( const myOp3& O, const TermVector& X, TermVector& R)  //mandatory
{ TermVector S=((*O.pD)*(*O.pL*X))*(*O.pL);
  R = X-factSolve(*O.pA,S);O.nbprod++;}  //(I-inv(A)*Lt*D*L)*X

class myOp4
{public :
  TermMatrix *pA,*pM;         //pointer to TermMatrix
  Vector<Complex> *plambda;
  TermVectors *pPhin;
  mutable Number nbprod;
  ValueType valueType() const  {return xlifepp::_complex;} //mandatory function
  myOp4(TermMatrix& A, TermMatrix& M, Vector<Complex>& lambda, TermVectors& P) : pA(&A), pM(&M), plambda(&lambda), pPhin(&P) {nbprod=0;}
};
void multMatrixVector ( const myOp4& O, const TermVector& X, TermVector& R)  //mandatory
{ TermVector Mx=*O.pM*X, Y;
  TermVector S;
  for(Number n=1;n<=O.pPhin->size();n++)
  {
    TermVector& phi_n = (*O.pPhin)(n);
    S+=(i_*(*O.plambda)[n-1]*(Mx|phi_n))*phi_n;   //sum_n i*lambda_n*(M*X|phi_n)*phi_n
  }
  R = X-factSolve(*O.pA,*O.pM*S);O.nbprod++;
}

void printResultsOfCV(const TermMatrix& MAT, const TermVector& B, const TermVector& X, const TermVector& Uex, Real epsilon, std::ostream& out, Real et=0.)
{
  TermVector E=B-MAT*X, D=X-Uex;
  double_t res=norm2(E), err=norm2(D), nB=norm2(B), nUex=norm2(Uex);
  std::stringstream ss;
  if(res< epsilon*nB) ss << "CV with epsilon = " << epsilon ;
  else {ss << "NO CV with epsilon = " << epsilon; NE++;}
  ss << " ||residu|| = " << res/nB << " ||Uex-Unum|| = " << err <<" ";
  if(et!=0.) ss<<" time  = "<<et;
  ss<<eol<<eol;
  out<<ss.str();
  theCout<<ss.str();
}

void checkSolver(const String& solver, TermMatrix& A, TermVector& B, TermVector& X0, TermVector& Uex, Real eps, Number maxit,
                 Preconditioner& pdiag, Preconditioner& pildlt, Preconditioner& pldlt, std::ostream& out, bool withTime=false)
{
  TermVector X;
  Real et=0;
  const Number krylovDim = 3*sqrt(A.numberOfCols());
  out << "================================= "<<solver<<" ================================="<<eol;
  theCout << "================================= "<<solver<<" ================================="<<eol;
  out<<"* without Preconditionner" << eol;
  theCout << "* without Preconditionner" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
  out << "* with Preconditionner (diag)" << eol;
  theCout <<"* with Preconditionner (diag)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pdiag, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
  out << "* with Preconditionner (ldlt)" << eol;
  theCout <<"* with Preconditionner (ldlt)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pildlt, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
  out << "* with Preconditionner (bf)" << eol;
  theCout << "* with Preconditionner (bf)" << eol;
  if (withTime) elapsedTime();
  if (solver=="CG")   X = cgSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="CGS")  X = cgsSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICG")  X = bicgSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="BICGSTAB")  X = bicgStabSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (solver=="GMRES")  X = gmresSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit, _krylovDim=krylovDim);
  if (solver=="QMR")  X = qmrSolve(A, B, X0, pldlt, _tolerance=eps, _maxIt=maxit);
  if (withTime) et=elapsedTime();
  printResultsOfCV(A, B, X, Uex, eps, out, et);
}

// Programme principal:
void sys_IterSolversHelmholtzDtN(int argc, char* argv[], bool check)
{
  String rootname = "sys_IterSolversHelmholtzDtN";
  trace_p->push(rootname);
  //numberOfThreads(1);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(1);
  Number nbErrors=0;
  String errors;
  bool withTime=true;
  Real k2=k*k;
  Parameters params;
  params << Parameter(h,"h")<< Parameter(k,"k");
  //create Mesh [0,a]x[0,h]
  Number ny=30, na=Number(ny*a/h), nd=Number(0.5*ny*pi_*r/h);
  Rectangle R(_xmin=0.,_xmax=a,_ymin=0.,_ymax=h, _nnodes=Numbers(na, ny),_domain_name="Omega",_side_names=Strings("Gamma","Sigma","Gamma","Sigma0"));
  Disk D(_center=Point(0.1,0.1),_radius=r,_nnodes=nd,_domain_name="D");
  Mesh me(R, _shape=_triangle, _order=1, _generator=_structured);
  //saveToFile("mesh",me,_format=_vtk);
  //Mesh me(R-D,_shape=_triangle, _order=1, _generator=gmsh);
  Domain Omega = me.domain("Omega"), Sigma = me.domain("Sigma"), Sigma0 = me.domain("Sigma0");
  //create FE space and FE unknown
  Space V(_domain=Omega, _interpolation=_P2, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");
  // create spectral space on Sigma_a and dtn kernel
  Number N=10;         //number of modes in DtN
  Complexes beta(N);
  for(Number n=0; n<N; n++) beta[n]=sqrt(Complex(k2-n*n*pi_*pi_/(h*h)));
  Space S(_domain=Sigma, _basis=Function(cosn,params), _dim=N, _name="cos(n*pi*y)");
  Unknown phi(S,_name="phi");
  TensorKernel K(phi,beta);
  LinearForm f= intg(Sigma0,g*v);
  TermVector B(f,_name="B");
  // create problem
  elapsedTime();
  BilinearForm auv = intg(Omega,grad(u)|grad(v)) - k2*intg(Omega,u*v) - i_*intg(Sigma,Sigma,u*K*v);
  TermMatrix A(auv,_name="A");
  TermVector U=directSolve(A,B,true);
  if(withTime) elapsedTime("direct solver", theCout);
  saveToFile("U",U,_format=_vtu);

  // BF problem
  BilinearForm abf = intg(Omega,grad(u)|grad(v)) - k2*intg(Omega,u*v) - (i_*k)*intg(Sigma,u*v);
  TermMatrix Abf(abf,_name="Abf");
  factorize(Abf,_ldlt);
  if(withTime) elapsedTime("build Abf",theCout);
  TermVector X0(u,Omega,1.,_name="X0");
  // check iterative solvers
  Real epsilon=1.e-6;
  Number nbIteration = 5000;

  PreconditionerTerm preDiag(A, _diagPrec);
  if(withTime)elapsedTime("diagonal preconditioning", theCout);
  PreconditionerTerm preLdLt(A, _ldltPrec);
  if(withTime)elapsedTime("LdLt preconditioning", theCout);
  PreconditionerTerm preBF(Abf, _ildltPrec);
  if(withTime)elapsedTime("BF preconditioning", theCout);

  checkSolver("CG",A,B,X0,U,epsilon, nbIteration, preDiag, preLdLt, preBF, out, withTime);
  checkSolver("BICG",A,B,X0,U,epsilon, nbIteration, preDiag, preLdLt, preBF, out, withTime);
  checkSolver("BICGSTAB",A,B,X0,U,epsilon, nbIteration, preDiag, preLdLt, preBF, out, withTime);
  checkSolver("CGS",A,B,X0,U,epsilon, nbIteration, preDiag, preLdLt, preBF, out, withTime);
  checkSolver("GMRES",A,B,X0,U,epsilon, nbIteration, preDiag, preLdLt,preBF, out, withTime);
  checkSolver("QMR",A,B,X0,U,epsilon, nbIteration, preDiag, preLdLt, preBF, out, withTime);

  TermVector Un(u,Omega,1.,_name="Un");
  for(Number n=0; n<N; n++) beta[n]-=k;
  Number np=100;
  // solve (I-inv(A)*T)X=inv(A)B with A the low-frequency matrix and T the modified DtN operator
  theCout <<"=================== solving (I-inv(A)*T)X=inv(A)B using GMRES ===================" << eol;
  elapsedTime();
  verboseLevel(0);
  TensorKernel K2(phi,beta);
  BilinearForm tuv = i_*intg(Sigma,Sigma,u*K2*v);
  TermMatrix T(tuv, _name="T");
  BilinearForm a2 = intg(Omega,grad(u)|grad(v)) - k2*intg(Omega,u*v) - (i_*k)*intg(Sigma,u*v);
  TermMatrix A2(a2,_name="A2");
  factorize(A2,_ldlt);
  TermVector B2=factSolve(A2,B);
  myOp2 O2(A2,T);
  elapsedTime("build O2",theCout);
  verboseLevel(1);
  GmresSolver gmres2;
  TermVector U2=gmres2(O2,B2,B2);
  elapsedTime("solve",theCout);
  //theCout<<"nbprod="<<O2.nbprod<<eol;
  TermVector E2=U2-U;
  theCout<<"||U2-U||="<<norm2(E2)<<eol;
  saveToFile("U2",U2,_format=_vtu);
  saveToFile("E2",E2,_format=_vtu);
//  for(Number j=0;j<np;j++) multMatrixVector(O2,Un,U2);
//  elapsedTime("product");

  // solve (I-inv(A)*Lt*D*L)*X=inv(A)B with A the low-frequency matrix and Lt*D*L the modified DtN operator
  theCout <<"=================== solving ((I-inv(A)*Lt*D*L)*X=inv(A)B using GMRES ===================" << eol;
  elapsedTime();
  verboseLevel(0);
  BilinearForm lpv = intg(Sigma,u*phi);
  TermMatrix L(lpv, _name="L");
  TermMatrix Db(phi,Sigma,beta,_name="Db");  //diagonal matrix
  BilinearForm a3 = intg(Omega,grad(u)|grad(v)) - k2*intg(Omega,u*v) - (i_*k)*intg(Sigma,u*v);
  TermMatrix A3(a3,_name="A3");
  factorize(A3,_ldlt);
  TermVector B3=factSolve(A3,B);
  myOp3 O3(A3,L,Db);
  verboseLevel(1);
  GmresSolver gmres3;
  elapsedTime("build O3",theCout);
  TermVector U3=gmres3(O3,B3,B3);
  elapsedTime("solve",theCout);
  //theCout<<"nbprod="<<O3.nbprod<<eol;
  TermVector E3=U3-U;
  theCout<<"||U3-U||="<<norm2(E3)<<eol;
  saveToFile("U3",U3,_format=_vtu);
  saveToFile("E3",E3,_format=_vtu);
//  for(Number j=0;j<np;j++) multMatrixVector(O3,Un,U3);
//  elapsedTime("product");
//
  //solving (O*X=inv(A)B with O*X = X-inv(A)*sum_n ib_n(MX,phi_n)Mphi_n, phi_n interpolated
  theCout <<"=================== solving (O*X=inv(A)B using GMRES ===================" << eol;
  elapsedTime();
  verboseLevel(0);
  Function fcosn(cosn, params);
  TermVectors cos_int(N);
  for(Number n=1; n<=N; n++)
  {
    fcosn.parameter("basis index")=n;
    cos_int(n)=TermVector(u,Sigma,fcosn,_name="c"+tostring(n-1));
  }
  TermMatrix Ms(intg(Sigma,u*v), _name="Ms");
  BilinearForm a4 = intg(Omega,grad(u)|grad(v)) - k2*intg(Omega,u*v) - (i_*k)*intg(Sigma,u*v);
  TermMatrix A4(a4,_name="A4");
  factorize(A4,_ldlt);
  TermVector B4=factSolve(A3,B);
  myOp4 O4(A4,Ms,beta,cos_int);
  verboseLevel(1);
  GmresSolver gmres4;
  elapsedTime("build O4",theCout);
  TermVector U4=gmres4(O4,B4,B4);
  elapsedTime("solve",theCout);
  //theCout<<"nbprod="<<O4.nbprod<<eol;
  TermVector E4=U4-U;
  theCout<<"||U4-U||="<<norm2(E4)<<eol;
  saveToFile("U4",U4,_format=_vtu);
  saveToFile("E4",E4,_format=_vtu);
  elapsedTime();
//  for(Number j=0;j<np;j++) multMatrixVector(O4,Un,U4);
//  elapsedTime("product");

  nbErrors=NE;
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  trace_p->pop();
  return;
}

} // end namespace
