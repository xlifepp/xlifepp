/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_HMatrix.cpp
	\author E. Lunéville
	\since 17 may 2016
	\date 17 may 2016

	Low level tests of Hmatrix class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=false)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "hierarchicalMatrix.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace sys_HMatrix
{

void sys_HMatrix(int argc, char* argv[], bool check)
{
  String rootname = "sys_HMatrix";
  trace_p->push(rootname);
  std::stringstream out;                  // string stream receiving results
  out.precision(testPrec);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);
  verboseLevel(0);
  //Trace::trackingMode=true;
  numberOfThreads(4);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;

  Real eps=1.E-08;
  Real eps2=1.E-04;
  Number nr=5;
  Number order=5;
  IntegrationMethods ims(_method=_SauterSchwabIM,_order=order,_bound=0.,_quad=_defaultRule,_order=4,_bound=2.,_quad=_defaultRule,_order=3);
  bool laplace_sl=true, laplace_dl=true, helmholtz_sl=true;
  bool hm_exact = false, hm_exact_no_sym=false,
       hm_svd=false, hm_svd_rank=false, hm_svd_eps=false,
       hm_aca = false, hm_aca_part=false, hm_aca_plus=true,
       hm_rsvd_rank = false, hm_rsvd_eps=false, hm_r3svd=false;
  MeshGenerator mg=_gmsh;  //_subdiv

  for (Number k=5; k<30; k+=5)
  {
    //test on a sphere
    Mesh meshd;
    if (!check)
    {
      meshd=Mesh(Sphere(_center=Point(0.,0.,0.),_radius=1.,_nnodes=k,_domain_name="Omega"), _shape=_triangle, _order=1, _generator=mg);
      meshd.saveToFile(inputsPathTo(rootname+"/meshd"+tostring(k)+".msh"),msh);
    }
    else
    {
      meshd=Mesh(inputsPathTo(rootname+"/meshd"+tostring(k)+".msh"), _name="meshd"+tostring(k)+" Sphere");
    }
    Domain omega=meshd.domain("Omega");
    Space W(_domain=omega, _interpolation=_P0, _name="V", _notOptimizeNumbering);
    Unknown u(W, _name="u"); TestFunction v(u, _name="v");
    Vector<Real> x(W.dimSpace(),1.);
    Number sb=std::max(Number(10),W.dimSpace()/100); // minimum size of block matrix -> not split if nbrow < sb and nbcol < sb
    Number nbp=50;  //nb product
    Kernel Gl=Laplace3dKernel();
    HMatrixIM him0(_clustering_method=cardinalityBisection, _min_block_size=sb, _threshold=0., _method=ims);
    HMatrixIM him (_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=svdCompression, _threshold=0., _method=ims);
    HMatrixIM him1(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=svdCompression, _threshold=eps, _method=ims);
    HMatrixIM him2(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=svdCompression, _max_rank=nr, _method=ims);
    HMatrixIM him3(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=acaFull, _threshold=eps2, _method=ims);
    HMatrixIM him4(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=acaPartial, _threshold=eps2, _method=ims);
    HMatrixIM him5(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=acaPlus, _threshold=eps2, _method=ims);
    HMatrixIM him6(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=rsvdCompression, _max_rank==nr, _method=ims);
    HMatrixIM him8(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=rsvdCompression, _threshold=eps2, _method=ims);
    HMatrixIM him7(_clustering_method=cardinalityBisection, _min_block_size=sb, _approximation_method=r3svdCompression, _threshold=eps, _method=ims);
    thePrintStream<<"============================================================="<<eol;
    thePrintStream<<"       sphere mesh k = "<<k<<", nbdofs = "<<W.dimSpace()<<", block size = "<<sb<<eol;
    thePrintStream<<"============================================================="<<eol;

    if (laplace_sl)
    {
      thePrintStream<<"============================================================="<<eol;
      thePrintStream<<"         Test for Laplace kernel, single layer"<<eol;
      thePrintStream<<"============================================================="<<eol;

      BilinearForm bf=intg(omega,omega,u*Gl*v,_method=ims);
      elapsedTime();
      TermMatrix A(bf, _name="A");
      elapsedTime("build full matrix");
      LargeMatrix<Real>& Alm=A.getLargeMatrix<Real>();
      Vector<Real> Ax;
      elapsedTime();
      for (Number k=0; k<nbp; ++k) Ax=Alm*x;
      thePrintStream<<"|A*x| = "<<norm(Ax)<<" nnz = "<<Alm.nbNonZero()<<eol;
      elapsedTime("full matrix x vector");
      clear(A);

      if (hm_exact) //no approximation sym
      {
        thePrintStream<<"------------------- Hmatrix no approximation -------------------"<<eol;
        BilinearForm bfhm0=intg(omega,omega,u*Gl*v,_method=him0);
        elapsedTime();
        TermMatrix Ahm0(bfhm0, _name="Ahm0");
        elapsedTime("build HMatrix exact ");
        HMatrix<Real,FeDof>& hmA0=Ahm0.getHMatrix<Real>();
        Vector<Real> Ahm0x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm0x=hmA0*x;
        elapsedTime("exact Hmatrix x vector");
        thePrintStream<<"exact |A*x- hmA0*x| = "<<norm(Ax-Ahm0x)<<eol;
      }

      if (hm_exact_no_sym) // no approximation no symmetry
      {
        thePrintStream<<"------------------- Hmatrix no approximation -------------------"<<eol;
        BilinearForm bfhm00=intg(omega,omega,u*Gl*v,_method=him0,_symmetry=_noSymmetry);
        elapsedTime();
        TermMatrix Ahm00(bfhm00, _name="Ahm00");
        elapsedTime("build HMatrix exact non sym");
        HMatrix<Real,FeDof>& hmA00=Ahm00.getHMatrix<Real>();
        Vector<Real> Ahm00x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm00x=hmA00*x;
        elapsedTime("exact Hmatrix non sym x vector");
        thePrintStream<<"exact non sym |A*x- hmA00*x| = "<<norm(Ax-Ahm00x)<<eol;
      }

      if (hm_svd)//full svd
      {
        thePrintStream<<"------------------- Hmatrix full svd -------------------"<<eol;
        BilinearForm bfhm=intg(omega,omega,u*Gl*v,_method=him);
        elapsedTime();
        TermMatrix Ahm(bfhm, _name="Ahm");
        elapsedTime("build HMatrix full svd");
        HMatrix<Real,FeDof>& hmA=Ahm.getHMatrix<Real>();
        Vector<Real> Ahmx;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahmx=hmA*x;
        elapsedTime("full svd Hmatrix x vector");
        thePrintStream<<"full svd |A*x-hmA*x| = "<<norm(Ax-Ahmx)<<eol;
      }

      if (hm_svd_eps) //svd with truncation of singular values sigma_i> eps
      {
        thePrintStream<<"------------------- Hmatrix truncated svd, sigma_n > "<<eps<<eol;
        BilinearForm bfhm1=intg(omega,omega,u*Gl*v,_method=him1);
        elapsedTime();
        TermMatrix Ahm1(bfhm1, _name="Ahm1");
        elapsedTime("build HMatrix eps truncated svd");
        HMatrix<Real,FeDof>& hmA1=Ahm1.getHMatrix<Real>();
        Vector<Real> Ahm1x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm1x=hmA1*x;
        elapsedTime("eps truncated svd Hmatrix x vector");
        thePrintStream<<"truncated svd (eps="<<eps<<") |A*x-hmA1*x| = "<<norm(Ax-Ahm1x)<<" average rank = "<<hmA1.averageRank()<<eol;
      }

      if (hm_svd_rank) //svd with truncation of n first singular values
      {
        thePrintStream<<"------------------- Hmatrix truncated svd, n < "<<nr<<eol;
        BilinearForm bfhm2=intg(omega,omega,u*Gl*v,_method=him2);
        elapsedTime();
        TermMatrix Ahm2(bfhm2, _name="Ahm2");
        elapsedTime("build HMatrix n truncated svd");
        HMatrix<Real,FeDof>& hmA2=Ahm2.getHMatrix<Real>();
        Vector<Real> Ahm2x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm2x=hmA2*x;
        elapsedTime("n truncated svd Hmatrix x vector");
        thePrintStream<<"truncated svd (n="<<nr<<") |A*x-hmA2*x| = "<<norm(Ax-Ahm2x)<<" average rank = "<<hmA2.averageRank()<<eol;
      }

      if (hm_aca) //compression ACA full
      {
        thePrintStream<<"------------------- Hmatrix ACA full -------------------"<<eol;
        BilinearForm bfhm3=intg(omega,omega,u*Gl*v,_method=him3);
        elapsedTime();
        TermMatrix Ahm3(bfhm3, _name="Ahm3");
        elapsedTime("build HMatrix ACA full");
        HMatrix<Real,FeDof>& hmA3=Ahm3.getHMatrix<Real>();
        Vector<Real> Ahm3x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm3x=hmA3*x;
        elapsedTime("ACA full Hmatrix x vector");
        thePrintStream<<"ACA full (eps="<<eps<<") |A*x-hmA3*x| = "<<norm(Ax-Ahm3x)<<" average rank = "<<hmA3.averageRank()<<eol;
      }

      if (hm_aca_part)//compression ACA partial
      {
        thePrintStream<<"------------------- Hmatrix ACA partial -------------------"<<eol;
        BilinearForm bfhm4=intg(omega,omega,u*Gl*v,_method=him4);
        elapsedTime();
        TermMatrix Ahm4(bfhm4, _name="Ahm4");
        elapsedTime("build HMatrix ACA partial");
        HMatrix<Real,FeDof>& hmA4=Ahm4.getHMatrix<Real>();
        Vector<Real> Ahm4x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm4x=hmA4*x;
        elapsedTime("ACA partial Hmatrix x vector");
        thePrintStream<<"ACA partial (eps="<<eps<<") |A*x-hmA4*x| = "<<norm(Ax-Ahm4x)<<" average rank = "<<hmA4.averageRank()<<eol;
      }

      if (hm_aca_plus)//compression ACA plus
      {
        thePrintStream<<"------------------- Hmatrix ACA + -------------------"<<eol;
        BilinearForm bfhm5=intg(omega,omega,u*Gl*v,_method=him5);
        elapsedTime();
        TermMatrix Ahm5(bfhm5, _name="Ahm5");
        elapsedTime("build HMatrix ACA+");
        HMatrix<Real,FeDof>& hmA5=Ahm5.getHMatrix<Real>();
        Vector<Real> Ahm5x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm5x=hmA5*x;
        elapsedTime("ACA+ Hmatrix x vector");
        thePrintStream<<"ACA+ (eps="<<eps<<") |A*x-hmA5*x| = "<<norm(Ax-Ahm5x)<<" average rank = "<<hmA5.averageRank()<<" nnz = "<<hmA5.nbNonZero()<<eol;
        nbErrors+=checkValue(norm2(norm(Ax-Ahm5x)), 0., eps2, errors, "|A*x-hmA5*x|");

        // nbErrors+=checkValue(hmA5.averageRank(), 7, errors, "average rank ");
      }

      if (hm_rsvd_rank) //rsvd compression rank
      {
        thePrintStream<<"------------------- Hmatrix rsvd rank -------------------"<<eol;
        BilinearForm bfhm6=intg(omega,omega,u*Gl*v,_method=him6);
        elapsedTime();
        TermMatrix Ahm6(bfhm6, _name="Ahm6");
        elapsedTime("build HMatrix rsvd_rank");
        HMatrix<Real,FeDof>& hmA6=Ahm6.getHMatrix<Real>();
        Vector<Real> Ahm6x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm6x=hmA6*x;
        elapsedTime("rsvd_rank Hmatrix x vector");
        thePrintStream<<"rsvd (nr="<<nr<<") |A*x-hmA6*x| = "<<norm(Ax-Ahm6x)<<" average rank = "<<hmA6.averageRank()<<eol;
      }


      if (hm_rsvd_eps) //rsvd compression eps
      {
        thePrintStream<<"------------------- Hmatrix rsvd eps -------------------"<<eol;
        BilinearForm bfhm8=intg(omega,omega,u*Gl*v,_method=him8);
        elapsedTime();
        TermMatrix Ahm8(bfhm8, _name="Ahm8");
        elapsedTime("build HMatrix rsvd_eps");
        HMatrix<Real,FeDof>& hmA8=Ahm8.getHMatrix<Real>();
        Vector<Real> Ahm8x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm8x=hmA8*x;
        elapsedTime("rsvd_eps Hmatrix x vector");
        thePrintStream<<"rsvd (eps="<<eps2<<") |A*x-hmA8*x| = "<<norm(Ax-Ahm8x)<<" average rank = "<<hmA8.averageRank()<<eol;
      }

      if (hm_r3svd) //r3svd compression
      {
        thePrintStream<<"------------------- Hmatrix r3svd -------------------"<<eol;
        BilinearForm bfhm7=intg(omega,omega,u*Gl*v,_method=him7);
        elapsedTime();
        TermMatrix Ahm7(bfhm7, _name="Ahm7");
        elapsedTime("build HMatrix r3svd");
        HMatrix<Real,FeDof>& hmA7=Ahm7.getHMatrix<Real>();
        Vector<Real> Ahm7x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Ahm7x=hmA7*x;
        elapsedTime("r3svd Hmatrix x vector");
        thePrintStream<<"r3svd (eps="<<eps<<") |A*x-hmA7*x| = "<<norm(Ax-Ahm7x)<<" average rank = "<<hmA7.averageRank()<<eol;
      }
    }

    //test with a combination of a HMatrix and a LargeMatrix
    //======================================================
    if (laplace_dl)
    {
      thePrintStream<<"============================================================="<<eol;
      thePrintStream<<"     Test for Laplace kernel, double layer, hybrid HM/LM"<<eol;
      thePrintStream<<"============================================================="<<eol;
      thePrintStream<<"----- Test for Laplace kernel, double layer, hybrid HM/LM ------"<<eol;
      thePrintStream<<"------------------ Dense matrix computation --------------------"<<eol;
      BilinearForm blm=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=ims)-0.5*intg(omega,u*v);
      elapsedTime();
      TermMatrix B(blm, _name="B");
      elapsedTime("build full matrix");
      Vector<Real> Bx;
      LargeMatrix<Real>& Blm=B.getLargeMatrix<Real>();
      elapsedTime();
      for (Number k=0; k<nbp; ++k) Bx=Blm*x;
      thePrintStream<<"|B*x| = "<<norm(Bx)<<" nnz = "<<Blm.nbNonZero()<<eol;
      elapsedTime("full matrix x vector");
      clear(B);

      if (hm_exact) //no approximation
      {
        thePrintStream<<"------------------- Hmatrix no approximation -------------------"<<eol;
        BilinearForm blm0=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him0)-0.5*intg(omega,u*v);
        TermMatrix B0(blm0, _name="B0");
        elapsedTime("build exact HM/LM matrix");
        Vector<Real> B0x;
        HMatrix<Real,FeDof>& B0hm=B0.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B0x=B0hm*x;
        thePrintStream<<"exact HM/LM |B*x-B0*x| = "<<norm(Bx-B0x)<<eol;
        elapsedTime("exact HM/LM matrix x vector");
      }

      if (hm_svd_eps) //svd with truncation of singular values sigma_i> eps
      {
        thePrintStream<<"------------------- Hmatrix truncated svd, sigma_n > "<<eps<<eol;
        BilinearForm blm1=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him1)-0.5*intg(omega,u*v);
        TermMatrix B1(blm1, _name="B1");
        elapsedTime("build SVD eps HM/LM matrix");
        Vector<Real> B1x;
        HMatrix<Real,FeDof>& B1hm=B1.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B1x=B1hm*x;
        thePrintStream<<"truncated svd (eps="<<eps<<") |B*x-B1*x| = "<<norm(Bx-B1x)<<" average rank = "<<B1hm.averageRank()<<eol;
        elapsedTime("SVD eps HM/LM matrix x vector");
      }

      if (hm_svd_rank) //svd with truncation of n first singular values
      {
        thePrintStream<<"------------------- Hmatrix truncated svd, n < "<<nr<<eol;
        BilinearForm blm2=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him2)-0.5*intg(omega,u*v);
        TermMatrix B2(blm2, _name="B2");
        elapsedTime("build SVD n HM/LM matrix");
        Vector<Real> B2x;
        HMatrix<Real,FeDof>& B2hm=B2.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B2x=B2hm*x;
        thePrintStream<<"truncated svd (n="<<nr<<") |B*x-B2*x| = "<<norm(Bx-B2x)<<" average rank = "<<B2hm.averageRank()<<eol;
        elapsedTime("SVD eps HM/LM matrix x vector");
      }

      if (hm_aca) //ACA compression
      {
        thePrintStream<<"------------------- Hmatrix ACA full -------------------"<<eol;
        BilinearForm blm3=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him3)-0.5*intg(omega,u*v);
        TermMatrix B3(blm3, _name="B3");
        elapsedTime("build ACA matrix");
        Vector<Real> B3x;
        HMatrix<Real,FeDof>& B3hm=B3.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B3x=B3hm*x;
        thePrintStream<<"ACA full (eps="<<eps<<") |B*x-B3*x| = "<<norm(Bx-B3x)<<" average rank = "<<B3hm.averageRank()<<eol;
        elapsedTime("ACA full HM/LM matrix x vector");
      }


      if (hm_aca_part) //partial ACA compression
      {
        thePrintStream<<"------------------- Hmatrix ACA partial -------------------"<<eol;
        BilinearForm blm4=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him4)-0.5*intg(omega,u*v);
        TermMatrix B4(blm4, _name="B4");
        elapsedTime("build ACA Part matrix");
        Vector<Real> B4x;
        HMatrix<Real,FeDof>& B4hm=B4.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B4x=B4hm*x;
        thePrintStream<<"ACA partial (eps="<<eps<<") |B*x-B4*x| = "<<norm(Bx-B4x)<<" average rank = "<<B4hm.averageRank()<<eol;
        elapsedTime("ACA Part HM/LM matrix x vector");
      }

      if (hm_aca_plus)//ACA+ compression
      {
        thePrintStream<<"------------------- Hmatrix ACA + -------------------"<<eol;
        BilinearForm blm5=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him5)-0.5*intg(omega,u*v);
        TermMatrix B5(blm5, _name="B5");
        elapsedTime("build ACA Part matrix");
        Vector<Real> B5x;
        HMatrix<Real,FeDof>& B5hm=B5.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B5x=B5hm*x;
        thePrintStream<<"ACA plus (eps="<<eps<<")|B*x-B5*x| = "<<norm(Bx-B5x)<<" average rank = "<<B5hm.averageRank()<<" nnz = "<<B5hm.nbNonZero()<<eol;
        nbErrors+=checkValue(norm2(norm(Bx-B5x)), 0., eps2, errors, "|B*x-B5*x|");
        elapsedTime("ACA+ HM/LM matrix x vector");
      }

      if (hm_rsvd_rank)//rsvd compression
      {
        thePrintStream<<"------------------- Hmatrix rsvd_rank -------------------"<<eol;
        BilinearForm blm6=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him6)-0.5*intg(omega,u*v);
        TermMatrix B6(blm6, _name="B6");
        elapsedTime("build RSVD_rank matrix");
        Vector<Real> B6x;
        HMatrix<Real,FeDof>& B6hm=B6.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B6x=B6hm*x;
        thePrintStream<<"rsvd_rank (nr="<<nr<<") |B*x-B6*x| = "<<norm(Bx-B6x)<<" average rank = "<<B6hm.averageRank()<<eol;
        elapsedTime("rsvd_rank matrix x vector");
      }

      if (hm_rsvd_eps)//rsvd_eps compression
      {
        thePrintStream<<"------------------- Hmatrix rsvd_eps-------------------"<<eol;
        BilinearForm blm8=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him8)-0.5*intg(omega,u*v);
        TermMatrix B8(blm8, _name="B8");
        elapsedTime("build R3SVD matrix");
        Vector<Real> B8x;
        HMatrix<Real,FeDof>& B8hm=B8.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B8x=B8hm*x;
        thePrintStream<<"rsvd (eps="<<eps2<<") |B*x-B8*x| = "<<norm(Bx-B8x)<<" average rank = "<<B8hm.averageRank()<<eol;
        elapsedTime("rsvd_eps matrix x vector");
      }

      if (hm_r3svd) //r3svd compression
      {
        thePrintStream<<"------------------- Hmatrix r3svd -------------------"<<eol;
        BilinearForm blm7=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him7)-0.5*intg(omega,u*v);
        TermMatrix B7(blm7, _name="B7");
        elapsedTime("build R3SVD matrix");
        Vector<Real> B7x;
        HMatrix<Real,FeDof>& B7hm=B7.getHMatrix<Real>();
        elapsedTime();
        for (Number k=0; k<nbp; ++k) B7x=B7hm*x;
        thePrintStream<<"r3svd (eps="<<eps<<") |B*x-B7*x| = "<<norm(Bx-B7x)<<" average rank = "<<B7hm.averageRank()<<eol;
        elapsedTime("R3SVD HM/LM matrix x vector");
      }
    }

// test for Helmholtz Kernel
// ============================================================================
    if (helmholtz_sl)
    {
      thePrintStream<<"============================================================="<<eol;
      thePrintStream<<"         Test for Helmhotz kernel, single layer"<<eol;
      thePrintStream<<"============================================================="<<eol;
      thePrintStream<<"---------- Test for Helmhotz kernel, single layer ------------"<<eol;
      thePrintStream<<"----------------- Dense matrix computation -------------------"<<eol;
      Kernel Gh=Helmholtz3dKernel(2.);
      Vector<Complex> z(W.dimSpace(),Complex(1.));
      BilinearForm cblm=intg(omega,omega,u*Gh*v,_method=ims);
      elapsedTime();
      TermMatrix C(cblm, _name="C");
      elapsedTime("build full matrix");
      Vector<Complex> Cx;
      LargeMatrix<Complex>& Clm=C.getLargeMatrix<Complex>();
      elapsedTime();
      for (Number k=0; k<nbp; ++k) Cx=Clm*z;
      thePrintStream<<"|C*x| = "<<norm(Cx)<<" nnz = "<<Clm.nbNonZero()<<eol;
      elapsedTime("full matrix x vector");
      clear(C);

      if (hm_exact) //no approximation sym
      {
        thePrintStream<<"------------------- Hmatrix no approximation -------------------"<<eol;
        BilinearForm cbfhm0=intg(omega,omega,u*Gh*v,_method=him0);
        elapsedTime();
        TermMatrix Chm0(cbfhm0, _name="Chm0");
        elapsedTime("build HMatrix exact");
        HMatrix<Complex,FeDof>& hmC0=Chm0.getHMatrix<Complex>();
        Vector<Complex> Chm0x;
        elapsedTime();
        for(Number k=0; k<nbp; ++k) Chm0x=hmC0*z;
        elapsedTime("exact Hmatrix x vector");
        thePrintStream<<"exact |C*x- hmC0*x| = "<<norm(Cx-Chm0x)<<eol;
      }

      if (hm_exact_no_sym) //no approximation no sym
      {
        thePrintStream<<"------------------- Hmatrix no approximation no symmetry -------------------"<<eol;
        BilinearForm cbfhm00=intg(omega,omega,u*Gh*v,_method=him0,_symmetry=_noSymmetry);
        elapsedTime();
        TermMatrix Chm00(cbfhm00, _name="Chm00");
        elapsedTime("build HMatrix exact non sym");
        HMatrix<Complex,FeDof>& hmC00=Chm00.getHMatrix<Complex>();
        Vector<Complex> Chm00x;
        elapsedTime();
        for (Number k=00; k<nbp; ++k) Chm00x=hmC00*z;
        elapsedTime("exact Hmatrix non sym x vector");
        thePrintStream<<"exact non sym |C*x- hmC00*x| = "<<norm(Cx-Chm00x)<<eol;
      }

      if (hm_svd) //full svd
      {
        thePrintStream<<"------------------- Hmatrix full svd  -------------------"<<eol;
        BilinearForm cbfhm=intg(omega,omega,u*Gh*v,_method=him);
        elapsedTime();
        TermMatrix Chm(cbfhm, _name="Chm");
        elapsedTime("build HMatrix full svd");
        HMatrix<Complex,FeDof>& hmC=Chm.getHMatrix<Complex>();
        Vector<Complex> Chmx;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chmx=hmC*z;
        elapsedTime("full svd Hmatrix x vector");
        thePrintStream<<"full svd |C*x-hmC*x| = "<<norm(Cx-Chmx)<<eol;
      }

      if (hm_svd_eps) //svd with truncation of singular values sigma_i> eps
      {
        thePrintStream<<"------------------- Hmatrix trucated svd, sigma_n > "<<eps<<" -------------------"<<eol;
        BilinearForm cbfhm1=intg(omega,omega,u*Gh*v,_method=him1);
        elapsedTime();
        TermMatrix Chm1(cbfhm1, _name="Chm1");
        elapsedTime("build HMatrix eps truncated svd");
        HMatrix<Complex,FeDof>& hmC1=Chm1.getHMatrix<Complex>();
        Vector<Complex> Chm1x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm1x=hmC1*z;
        elapsedTime("eps truncated svd Hmatrix x vector");
        thePrintStream<<"truncated svd (eps="<<eps<<") |C*x-hmC1*x| = "<<norm(Cx-Chm1x)<<" average rank = "<<hmC1.averageRank()<<eol;
      }


      if (hm_svd_rank) //  svd with truncation of singular values (n<=nr)
      {
        thePrintStream<<"------------------- Hmatrix truncated svd, n < "<<nr<<" -------------------"<<eol;
        BilinearForm cbfhm2=intg(omega,omega,u*Gh*v,_method=him2);
        elapsedTime();
        TermMatrix Chm2(cbfhm2, _name="Chm2");
        elapsedTime("build HMatrix n truncated svd");
        HMatrix<Complex,FeDof>& hmC2=Chm2.getHMatrix<Complex>();
        Vector<Complex> Chm2x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm2x=hmC2*z;
        elapsedTime("n truncated svd Hmatrix x vector");
        thePrintStream<<"truncated svd (n="<<nr<<") |C*x-hmC2*x| = "<<norm(Cx-Chm2x)<<" average rank = "<<hmC2.averageRank()<<eol;
      }

      if (hm_aca) //compression ACA full
      {
        thePrintStream<<"------------------- Hmatrix ACA full -------------------"<<eol;
        BilinearForm cbfhm3=intg(omega,omega,u*Gh*v,_method=him3);
        elapsedTime();
        TermMatrix Chm3(cbfhm3, _name="Chm3");
        elapsedTime("build HMatrix ACA full");
        HMatrix<Complex,FeDof>& hmC3=Chm3.getHMatrix<Complex>();
        Vector<Complex> Chm3x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm3x=hmC3*z;
        elapsedTime("ACA full Hmatrix x vector");
        thePrintStream<<"ACA full (eps="<<eps<<") |C*x-hmC3*x| = "<<norm(Cx-Chm3x)<<" average rank = "<<hmC3.averageRank()<<eol;
      }

      if (hm_aca_part)//compression ACA partial
      {
        thePrintStream<<"------------------- Hmatrix ACA partial -------------------"<<eol;
        BilinearForm cbfhm4=intg(omega,omega,u*Gh*v,_method=him4);
        elapsedTime();
        TermMatrix Chm4(cbfhm4, _name="Chm4");
        elapsedTime("build HMatrix ACA partial");
        HMatrix<Complex,FeDof>& hmC4=Chm4.getHMatrix<Complex>();
        Vector<Complex> Chm4x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm4x=hmC4*z;
        elapsedTime("ACA partial Hmatrix x vector");
        thePrintStream<<"ACA partial (eps="<<eps<<") |C*x-hmC4*x| = "<<norm(Cx-Chm4x)<<" average rank = "<<hmC4.averageRank()<<eol;
      }

      if (hm_aca_plus) //compression ACA plus
      {
        thePrintStream<<"------------------- Hmatrix ACA + -------------------"<<eol;
        BilinearForm cbfhm5=intg(omega,omega,u*Gh*v,_method=him5);
        elapsedTime();
        TermMatrix Chm5(cbfhm5, _name="Chm5");
        elapsedTime("build HMatrix ACA plus");
        HMatrix<Complex,FeDof>& hmC5=Chm5.getHMatrix<Complex>();
        Vector<Complex> Chm5x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm5x=hmC5*z;
        elapsedTime("ACA plus Hmatrix x vector");
        thePrintStream<<"ACA plus (eps="<<eps<<") |C*x-hmC5*x| = "<<norm(Cx-Chm5x)<<" average rank = "<<hmC5.averageRank()<<" nnz = "<<hmC5.nbNonZero()<<eol;
        nbErrors+=checkValue(norm2(norm(Cx-Chm5x)), 0., eps2, errors, "|C*x-hmC5*x|");
      }

      if (hm_rsvd_rank)//rsvd rank compression
      {
        thePrintStream<<"------------------- Hmatrix rsvd -------------------"<<eol;
        BilinearForm cbfhm6=intg(omega,omega,u*Gh*v,_method=him6);
        elapsedTime();
        TermMatrix Chm6(cbfhm6, _name="Chm6");
        elapsedTime("build HMatrix rsvd_rank");
        HMatrix<Complex,FeDof>& hmC6=Chm6.getHMatrix<Complex>();
        Vector<Complex> Chm6x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm6x=hmC6*z;
        elapsedTime("rsvd_rank Hmatrix x vector");
        thePrintStream<<"rsvd (nr="<<nr<<") |C*x-hmC6*x| = "<<norm(Cx-Chm6x)<<" average rank = "<<hmC6.averageRank()<<eol;
      }

      if (hm_rsvd_eps)//rsvd eps compression
      {
        thePrintStream<<"------------------- Hmatrix rsvd eps -------------------"<<eol;
        BilinearForm cbfhm8=intg(omega,omega,u*Gh*v,_method=him8);
        elapsedTime();
        TermMatrix Chm8(cbfhm8, _name="Chm8");
        elapsedTime("build HMatrix rsvd_eps");
        HMatrix<Complex,FeDof>& hmC8=Chm8.getHMatrix<Complex>();
        Vector<Complex> Chm8x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm8x=hmC8*z;
        elapsedTime("rsvd_eps Hmatrix x vector");
        thePrintStream<<"rsvd (eps="<<eps2<<") |C*x-hmC8*x| = "<<norm(Cx-Chm8x)<<" average rank = "<<hmC8.averageRank()<<eol;
      }

      if (hm_r3svd) //r3svd compression
      {
        thePrintStream<<"------------------- Hmatrix r3svd -------------------"<<eol;
        BilinearForm cbfhm7=intg(omega,omega,u*Gh*v,_method=him7);
        elapsedTime();
        TermMatrix Chm7(cbfhm7, _name="Chm7");
        elapsedTime("build HMatrix r3svd");
        HMatrix<Complex,FeDof>& hmC7=Chm7.getHMatrix<Complex>();
        Vector<Complex> Chm7x;
        elapsedTime();
        for (Number k=0; k<nbp; ++k) Chm7x=hmC7*z;
        elapsedTime("r3svd Hmatrix x vector");
        thePrintStream<<"r3svd (eps="<<eps<<") |C*x-hmC7*x| = "<<norm(Cx-Chm7x)<<" average rank = "<<hmC7.averageRank()<<eol;
      }
    }
  }
  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
