/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_IE_Helmholtz3D_DSC.cpp
	\since 20 jun 2014
	\date 20 jun 2014
	\author Eric Lunéville

	Solve Helmholtz 3D problem using Dirichlet single layer IE
	P0-P1-P2 approximation and Sauter Schwab integration method
	This test use the mesh of a sphere (surface mesh in R3)
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_IE_Helmholtz3D_DSC {

// incident plane wave
Complex uinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky"), kz=pa("kz");
  Real kp=kx*p(1)+ky*p(2)+kz*p(3);
  return exp(i_*kp);
}

Complex Gun(const Point& x, const Point& y, Parameters& pa = defaultParameters)
{
    Real t=dot(x-y,x-y);
    return Complex(t,0);
}

void sys_IE_Helmholtz3D_DSC(int argc, char* argv[], bool check)
{
  String rootname = "sys_IE_Helmholtz3D_DSC";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(100);
  String errors;
  Number nbErrors = 0;
  Real tol=0.01;
  bool saveF=false;

  Options opts ;
  opts.addOption("npa",3);
  opts.addOption("nbt",1);
  opts.addOption("nmax",50);
  opts.parse(argc,argv);
  Number npa=opts("npa");   //nb of points by diameter of sphere
  Number nbt=opts("nbt");   //nb of threads
  Number nmax=opts("nmax"); //nb of terms in exact solution expansion
  numberOfThreads(nbt);

  //computing parameters
  bool doP0=true, doP1=true, doP2=true;

  //define parameters and functions
  Parameters pars;
  pars<<Parameter(1.,"k");       // wave number
  pars<<Parameter(1.,"kx")<<Parameter(0.,"ky")<<Parameter(0.,"kz");      // x,y,z wave number of incident wave
  pars<<Parameter(1.,"radius");        // sphere radius
  pars<<Parameter(nmax,"nmax");        // number of terms in expansion
  Kernel G = Helmholtz3dKernel(pars);  //load Helmholtz3D kernel
  Kernel G1(Gun,"G1",_r_,-1,_symmetric);
  Function finc(uinc,pars);
  Function scatSol(scatteredFieldSphereDirichlet,pars);

  //meshing
  Sphere sp(_center=Point(0,0,0),_radius=1.,_nnodes=npa, _nboctants=8);
  Mesh m1(sp, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain sphere = m1.domain(0);
  elapsedTime("mesh");

  //Sauter-Schwab integration method
  IntegrationMethods ims(_method=SauterSchwab, _order=9, _quad=_defaultRule, _order=7, _bound=2., _quad=_defaultRule, _order=6, _bound=4., _quad=_defaultRule, _order=5);

  //used by any interpolation
  Space V1(_domain=sphere, _interpolation=P1, _name="V1");
  Unknown u1(V1, _name="u1"); TestFunction v1(u1, _name="v1");
  TermMatrix M11(intg(sphere,u1*v1), _name="M11");
  //thePrintStream<<V1<<eol;

  //for integral representation on x=0 plane
  Number npp=40, npc=8*npp/10;
  Rectangle recx(_v1 = Point(0.,-4.,-4.), _v2 = Point(0.,4.,-4.),_v4 = Point(0.,-4.,4.), _nnodes=npp, _domain_name="x=0");
  Disk dx(_center = Point(0.,0.,0.), _v1=Point(0.,0.,1.25), _v2=Point(0.,1.25,0.),_nnodes=npc, _domain_name="x=0");
  Mesh mx0(recx-dx, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain planx0 = mx0.domain(0);
  Space Wx(_domain=planx0, _interpolation=P1, _name="Wx");
  Unknown wx(Wx, _name="wx");
  TermMatrix Rx(wx, planx0, u1, sphere, G, _name="Rx");  //G(xi,yj)
  TermVector Uix0(wx,planx0,finc);
  TermVector soldx0(wx,planx0,scatSol);
  TermVector soltx0=Uix0+soldx0;
  TermMatrix Mx0(intg(planx0, wx*wx), _name="Mx0");  //mass matrix on x=0

  //for integral representation on y=0 plane
  Rectangle recy(_v1=Point(-4.,0.,-4.), _v2=Point(4.,0.,-4.),_v4=Point(-4.,0.,4.), _nnodes=npp, _domain_name="y=0");
  Disk dy(_center=Point(0.,0.,0.), _v1=Point(1.25,0.,0.),_v2=Point(0.,0.,1.25), _nnodes=npc, _domain_name="y=0");
  Mesh my0(recy-dy, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain plany0 = my0.domain(0);
  Space Wy(_domain=plany0, _interpolation=P1, _name="Wy");
  Unknown wy(Wy, _name="wy");
  TermMatrix Ry(wy, plany0, u1, sphere, G, _name="Ry");  //G(yi,yj)
  TermVector Uiy0(wy,plany0,finc);
  TermVector soldy0(wy,plany0,scatSol);
  TermVector solty0=Uiy0+soldy0;
  TermMatrix My0(intg(plany0, wy*wy), _name="My0");  //mass matrix on y=0

  //integral representation on z=0 plane
  Rectangle recz(_v1=Point(-4.,-4.,0.), _v2=Point(4.,-4.,0.),_v4=Point(-4.,4.,0.), _nnodes=npp, _domain_name="z=0");
  Disk dz(_center=Point(0.,0.,0.), _v1=Point(1.25,0.,0.),_v2=Point(0.,1.25,0.), _nnodes=npc, _domain_name="z=0");
  Mesh mz0(recz-dz, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain planz0 = mz0.domain(0);
  Space Wz(_domain=planz0, _interpolation=P1, _name="Wz");
  Unknown wz(Wz, _name="wz");
  TermMatrix Rz(wz, planz0, u1, sphere, G, _name="Rz");  //G(zi,zj)
  TermVector Uiz0(wz,planz0,finc);
  TermVector soldz0(wz,planz0,scatSol);
  TermVector soltz0=Uiz0+soldz0;
  TermMatrix Mz0(intg(planz0, wz*wz), _name="Mz0");  //mass matrix on z=0

  //save exact diffracted field on x,y,z planes
  saveToFile("sold_x=0.vtu",soldx0,_format=_vtu);
  saveToFile("sold_y=0.vtu",soldy0,_format=_vtu);
  saveToFile("sold_z=0.vtu",soldz0,_format=_vtu);
  saveToFile("solt_x=0.vtu",soltx0,_format=_vtu);
  saveToFile("solt_y=0.vtu",solty0,_format=_vtu);
  saveToFile("solt_z=0.vtu",soltz0,_format=_vtu);
  Real tA,tS;

  thePrintStream<<"test IE single layer Dirichlet problem for Hemholtz equation on a sphere"<<eol;
//  thePrintStream<<" P1 mesh : number of triangles  = "<<m1.nbOfElements()<<" number of vertices = "<<m1.nbOfVertices()<<eol;
//  thePrintStream<<" L2 norm of solution on planes : "<<sqrt(abs((Mx0*soldx0|soldx0)+(My0*soldy0|soldy0)+(Mz0*soldz0|soldz0))/3.)<<eol;
//
//  //=================  P0 computation =========================
////  if(doP0)
////    {
//      //create Lagrange P0 space and unknown
//      Space V0(_domain=sphere, _interpolation=P0, _name="V0", _notOptimizeNumbering);
//      Unknown u0(V0, _name="u0"); TestFunction v0(u0, _name="v0");
//      thePrintStream<<" P0 - P1 mesh aproximation : "<<V0.nbOfElements()<<" elements, "<<V0.nbDofs()<<" dofs"<<eol;
//      BilinearForm bf0=intg(sphere,sphere,u0*G*v0,_method=ims);
//      LinearForm fv0 = -intg(sphere,finc*v0);
//      elapsedTime();
//      TermMatrix A0(bf0, _name="A0");
//      tA=elapsedTime("compute A0");
////      thePrintStream<<A0<<eol;
//      TermVector B0(fv0, _name="B0");
////      TermVector un0(u0,sphere,1.);
////      thePrintStream<<"sqrt(abs((A0*un0|un0))="<<sqrt(abs((A0*un0|un0)))<<eol;
//      TermVector U0 = directSolve(A0, B0);
//      tS=elapsedTime("solve A0*U=B");
//      saveToFile("U0.vtu",U0,_format=_vtu);
//      clear(A0);
//      //P1 projection of P0 solution
//      TermMatrix M10(intg(sphere,u0*v1), _name="M10");
//      TermMatrix M11f;
//      factorize(M11,M11f);
//      TermVector U01=directSolve(M11f,M10*U0);
//      elapsedTime("P1 projection");
//      saveToFile("U01.vtu",U01,_format=_vtu);
////      thePrintStream<<"U0 = "<<U0<<eol;
////      thePrintStream<<"U01 = "<<U01<<eol;
//      //integral representation on x,y,z planes
//      TermVector U0dx0=Rx*(M11*U01);
//      TermVector U0tx0=U0dx0+Uix0;
//      TermVector U0dy0=Ry*(M11*U01);
//      TermVector U0ty0=U0dy0+Uiy0;
//      TermVector U0dz0=Rz*(M11*U01);
//      TermVector U0tz0=U0dz0+Uiz0;
//      //compute errors on diffracted field
//      TermVector ec0x0=U0dx0-soldx0, ec0y0=U0dy0-soldy0, ec0z0=U0dz0-soldz0;
//      Real el0x0= sqrt(abs((Mx0*ec0x0) | ec0x0)),
//           el0y0= sqrt(abs((My0*ec0y0) | ec0y0)),
//           el0z0= sqrt(abs((Mz0*ec0z0) | ec0z0));
//      thePrintStream<<"=== P0 - P1 mesh approximation, number of dofs = "<<V0.nbDofs()<<"==="<<eol;
//      thePrintStream<<"   L2 error on x=0 = "<<el0x0<<eol;
//      thePrintStream<<"   L2 error on y=0 = "<<el0y0<<eol;
//      thePrintStream<<"   L2 error on z=0 = "<<el0z0<<eol;
//      thePrintStream<<"   mean L2 error = "<<(el0x0+el0y0+el0z0)/3.<<eol;
//      thePrintStream<<"   Linf error on x=0 = "<<norm(ec0x0,0)<<eol;
//      thePrintStream<<"   Linf error on y=0 = "<<norm(ec0y0,0)<<eol;
//      thePrintStream<<"   Linf error on z=0 = "<<norm(ec0z0,0)<<eol;
////      TermMatrix C0(intg(sphere,sphere,u0*G1*v0,_method=ims));
////      thePrintStream<<"sqrt(abs((C0*un0|un0))="<<sqrt(abs(C0*un0|un0))<<eol;
//
////      nbErrors+=checkValue(el0x0, 0., 0.01, errors, "L2 error on x=0");
////      nbErrors+=checkValue(el0y0, 0., 0.01, errors, "L2 error on y=0");
////      nbErrors+=checkValue(el0z0, 0., 0.01, errors, "L2 error on z=0");
////      nbErrors+=checkValue(norm(ec0x0,0), 0., 0.01, errors, "Linf error on x=0");
////      nbErrors+=checkValue(norm(ec0y0,0), 0., 0.01, errors, "Linf error on y=0");
////      nbErrors+=checkValue(norm(ec0z0,0), 0., 0.01, errors, "Linf error on z=0");
//      saveToFile("U0d_x=0.vtu",U0dx0,_format=_vtu);
//      saveToFile("Ui_x=0.vtu",Uix0,_format=_vtu);
//      saveToFile("U0t_x=0.vtu",U0tx0,_format=_vtu);
//      saveToFile("U0d_y=0.vtu",U0dy0,_format=_vtu);
//      saveToFile("Ui_y=0.vtu",Uiy0,_format=_vtu);
//      saveToFile("U0t_y=0.vtu",U0ty0,_format=_vtu);
//      saveToFile("U0d_z=0.vtu",U0dz0,_format=_vtu);
//      saveToFile("Ui_z=0.vtu",Uiz0,_format=_vtu);
//      saveToFile("U0t_z=0.vtu",U0tz0,_format=_vtu);
//
//      //test integral representation using direct quadrature
////      elapsedTime();
////      TermMatrix Rx0(wx, planx0, v0, sphere, G, _name="Rx0");  //G(xi,yj)
////      TermMatrix Ma00(intg(sphere,u0*v0), _name="Ma00");
////      TermVector U0dxi=Rx0*(Ma00*U0);
////      elapsedTime("compute IR by interpolation");
////      TermVector U0dxq=integralRepresentation(wx,planx0,intg(sphere,G*u0,_quad=GaussLegendre,_order=3),U0);
////      elapsedTime("compute IR by integralRepresentation");
////      TermVector ec0xi=U0dxi-soldx0, ec0xq=U0dxq-soldx0;
////      Real erL2Itrp=sqrt(abs((Mx0*ec0xi) | ec0xi));
////      Real erL2Intg=sqrt(abs((Mx0*ec0xq) | ec0xq));
////      thePrintStream<<"   P0 interpolation L2 error on x=0 = "<<erL2Itrp<<eol;
////      thePrintStream<<"   P0 intg. rep     L2 error on x=0 = "<<erL2Intg<<eol;
//////      nbErrors+=checkValue(erL2Itrp, 0., tol, errors, "P0 interpolation L2 error on x=0");
//////      nbErrors+=checkValue(erL2Intg, 0., tol, errors, "P0 intg. rep     L2 error on x=0 ");
////    }
//
//  //=================  P1 computation =========================
//  if(doP1)
//    {
//      thePrintStream<<" P1 - P1 mesh aproximation : "<<V1.nbOfElements()<<" elements, "<<V1.nbDofs()<<" dofs"<<eol;
//      //define forms
//      BilinearForm bf1=intg(sphere,sphere,u1*G*v1,_method=ims,_symmetry=_noSymmetry);
//      BilinearForm mlf1=intg(sphere,u1*v1);
//      LinearForm fv1 = -intg(sphere,finc*v1);
//      //LinearForm fv1 = intg(sphere,ginc*v1,_quad=Gauss_Legendre,_order=4);
//      //compute matrix and right hand side
//      TermMatrix A1(bf1, _name="A1");
//      //thePrintStream<<A1<<eol;
//      // saveToFile("A1.dat",A1,_dense);
//      tA=elapsedTime("compute A1");
//      TermVector B1(fv1, _name="B1");
////      saveToFile("B1.vtu",B1,_format=_vtu);
////      TermVector un1(u1,sphere,1.);
////      thePrintStream<<"sqrt(abs((A1*un1|un1))="<<sqrt(abs((A1*un1|un1)))<<eol;
//      //solve the system using Gauss elimination
//      TermVector U1 = directSolve(A1, B1);
//      tS=elapsedTime("solve A1*U1=B1",thePrintStream);
//      clear(A1);
//      saveToFile("U1.vtu",U1,_format=_vtu);
//      //thePrintStream<<" U1= "<<U1<<eol;
//      //integral representation on x, y, z=0 plane
//      TermVector U1dx0=Rx*(M11*U1);
//      TermVector U1tx0=U1dx0+Uix0;
//      TermVector U1dy0=Ry*(M11*U1);
//      TermVector U1ty0=U1dy0+Uiy0;
//      TermVector U1dz0=Rz*(M11*U1);
//      TermVector U1tz0=U1dz0+Uiz0;
//      //compute errors on diffracted field
//      TermVector ec1x0=U1dx0-soldx0, ec1y0=U1dy0-soldy0, ec1z0=U1dz0-soldz0;
//      Real el1x0= sqrt(abs((Mx0*ec1x0) | ec1x0)),
//           el1y0= sqrt(abs((My0*ec1y0) | ec1y0)),
//           el1z0= sqrt(abs((Mz0*ec1z0) | ec1z0));
//      thePrintStream<<"=== P1 approximation, number of dofs = "<<V1.nbDofs()<<"==="<<eol;
//      thePrintStream<<"   L2 error on x=0 = "<<el1x0<<eol;
//      thePrintStream<<"   L2 error on y=0 = "<<el1y0<<eol;
//      thePrintStream<<"   L2 error on z=0 = "<<el1z0<<eol;
//      thePrintStream<<"   mean L2 error = "<<(el1x0+el1y0+el1z0)/3.<<eol;
//      thePrintStream<<"   Linf error on x=0 = "<<norm(ec1x0,0)<<eol;
//      thePrintStream<<"   Linf error on y=0 = "<<norm(ec1y0,0)<<eol;
//      thePrintStream<<"   Linf error on z=0 = "<<norm(ec1z0,0)<<eol;
////      TermMatrix C1(intg(sphere,sphere,u1*G1*v1,_method=ims));
////      thePrintStream<<"abs(M11*un1|un1)="<<abs(M11*un1|un1)<<eol;
////      thePrintStream<<"sqrt(abs(C1*un1|un1))="<<sqrt(abs(C1*un1|un1))<<eol;
//
//      saveToFile("U1d_x=0.vtu",U1dx0,_format=_vtu);
//      saveToFile("U1t_x=0.vtu",U1tx0,_format=_vtu);
//      saveToFile("U1d_y=0.vtu",U1dy0,_format=_vtu);
//      saveToFile("U1t_y=0.vtu",U1ty0,_format=_vtu);
//      saveToFile("U1d_z=0.vtu",U1dz0,_format=_vtu);
//      saveToFile("U1t_z=0.vtu",U1tz0,_format=_vtu);
//    }
//
//  //=================  P2 computation =========================
  if(doP2)
    { // ----------------------  using P1 mesh --------------------------------
//      Space V21(_domain=sphere,_interpolation=P2,_name="V21",_notOptimizeNumbering);
//      Unknown u21(V21,"u21"); TestFunction v21(u21,"v21");
//      thePrintStream<<" P2 - P1 mesh aproximation : "<<V21.nbOfElements()<<" elements, "<<V21.nbDofs()<<" dofs"<<eol<<std::flush;
//      BilinearForm bf21=intg(sphere,sphere,u21*G*v21,_method=ims,_symmetry=_noSymmetry);
//      LinearForm fv21 = -intg(sphere,finc*v21);
//      //compute matrix and right hand side
//      TermMatrix A21(bf21, _storage=denseDual, _name="A21");
//      tA=elapsedTime("compute A21"); theCout<<std::flush;
//      //thePrintStream<<A21<<eol;
//      TermVector B21(fv21, _name="B21");
//      TermVector un21(u21,sphere,1.);
//      //thePrintStream<<"sqrt(abs((A21*un21|un21))="<<sqrt(abs(A21*un21|un21))<<eol;
//      //solve the system using Gauss elimination
//      TermVector U21 = directSolve(A21, B21);
//      tS=elapsedTime("solve A21*U21=B21"); theCout<<std::flush;
//      saveToFile("U21.vtu",U21,_format=_vtu);
//      //thePrintStream<<"U2 = "<<U2<<eol;
//      clear(A21);
//      //integral representation on x, y, z=0 plane
//      TermVector U21dx0=integralRepresentation(wx,planx0,intg(sphere,G*u21,_quad=GaussLegendre,_order=5),U21,"IR21x");
//      TermVector U21tx0=U21dx0+Uix0;
//      TermVector U21dy0=integralRepresentation(wy,plany0,intg(sphere,G*u21,_quad=GaussLegendre,_order=5),U21,"IR21y");
//      TermVector U21ty0=U21dy0+Uiy0;
//      TermVector U21dz0=integralRepresentation(wz,planz0,intg(sphere,G*u21,_quad=GaussLegendre,_order=5),U21,"IR21z");
//      TermVector U21tz0=U21dz0+Uiz0;
//      saveToFile("U21d_x=0.vtu",U21dx0,_format=_vtu);
//      saveToFile("U21t_x=0.vtu",U21tx0,_format=_vtu);
//      saveToFile("U21d_y=0.vtu",U21dy0,_format=_vtu);
//      saveToFile("U21t_y=0.vtu",U21ty0,_format=_vtu);
//      saveToFile("U21d_z=0.vtu",U21dz0,_format=_vtu);
//      saveToFile("U21t_z=0.vtu",U21tz0,_format=_vtu);
//      //compute errors on diffracted field
//      TermVector ec21x0=U21dx0-soldx0, ec21y0=U21dy0-soldy0, ec21z0=U21dz0-soldz0;
//      Real el21x0= sqrt(abs((Mx0*ec21x0) | ec21x0)),
//           el21y0= sqrt(abs((My0*ec21y0) | ec21y0)),
//           el21z0= sqrt(abs((Mz0*ec21z0) | ec21z0));
//      thePrintStream<<"=== P2 - P1 mesh approximation, number of dofs = "<<V21.nbDofs()<<"==="<<eol;
//      thePrintStream<<"   L2 error on x=0 = "<<el21x0<<eol;
//      thePrintStream<<"   L2 error on y=0 = "<<el21y0<<eol;
//      thePrintStream<<"   L2 error on z=0 = "<<el21z0<<eol;
//      thePrintStream<<"   mean L2 error = "<<(el21x0+el21y0+el21z0)/3.<<eol;
//      thePrintStream<<"   Linf error on x=0 = "<<norm(ec21x0,0)<<eol;
//      thePrintStream<<"   Linf error on y=0 = "<<norm(ec21y0,0)<<eol;
//      thePrintStream<<"   Linf error on z=0 = "<<norm(ec21z0,0)<<eol;
////      TermMatrix C21(intg(sphere,sphere,u21*G1*v21,_method=ims));
////      thePrintStream<<"sqrt(abs(C21*un21|un21))="<<sqrt(abs(C21*un21|un21))<<eol;
//

      // ----------------------  using P2 mesh --------------------------------
      // =================  P2 approximation  =========================
      Sphere sp2(_center=Point(0,0,0),_radius=1.,_nnodes=npa, _nboctants=8);
      Mesh m2(sp2, _shape=_triangle, _order=2, _generator=_gmsh);
      Domain sphere2 = m2.domain(0);
      //define forms
      Space V2(_domain=sphere2,_interpolation=P2,_name="V2",_notOptimizeNumbering);
      Unknown u2(V2, _name="u2"); TestFunction v2(u2, _name="v2");
      thePrintStream<<" P2 - P2 mesh aproximation : "<<V2.nbOfElements()<<" elements, "<<V2.nbDofs()<<" dofs"<<eol<<std::flush;
      BilinearForm bf2=intg(sphere2,sphere2,u2*G*v2,_method=ims,_symmetry=_noSymmetry);
      LinearForm fv2 = -intg(sphere2,finc*v2,_quad=_GaussLegendreRule,_order=7);
      //compute matrix and right hand side
      TermMatrix A2(bf2, _storage=denseDual, _name="A2");
      //thePrintStream<<A2<<eol;
      tA=elapsedTime("compute A2"); theCout<<std::flush;
      TermVector B2(fv2, _name="B2");
      //thePrintStream<<"B2 = "<<B2<<eol;
//      TermVector un2(u2,sphere2,1.);
//      thePrintStream<<"sqrt(abs((A2*un2|un2))="<<sqrt(abs((A2*un2|un2)))<<eol;
      //solve the system using Gauss elimination
      TermVector U2 = directSolve(A2, B2);
      tS=elapsedTime("solve A2*U2=B2"); theCout<<std::flush;
      saveToFile("U2.vtu",U2,_format=_vtu);
      //thePrintStream<<"U2 = "<<U2<<eol;
      clear(A2);
      //integral representation on x, y, z=0 plane
      TermVector U2dx0=integralRepresentation(wx,planx0,intg(sphere2,G*u2,_quad=GaussLegendre,_order=7),U2,"IR2x");
      TermVector U2tx0=U2dx0+Uix0;
      TermVector U2dy0=integralRepresentation(wy,plany0,intg(sphere2,G*u2,_quad=GaussLegendre,_order=7),U2,"IR2y");
      TermVector U2ty0=U2dy0+Uiy0;
      TermVector U2dz0=integralRepresentation(wz,planz0,intg(sphere2,G*u2,_quad=GaussLegendre,_order=7),U2,"IR2z");
      TermVector U2tz0=U2dz0+Uiz0;
      saveToFile("U2d_x=0.vtu",U2dx0,_format=_vtu);
      saveToFile("U2t_x=0.vtu",U2tx0,_format=_vtu);
      saveToFile("U2d_y=0.vtu",U2dy0,_format=_vtu);
      saveToFile("U2t_y=0.vtu",U2ty0,_format=_vtu);
      saveToFile("U2d_z=0.vtu",U2dz0,_format=_vtu);
      saveToFile("U2t_z=0.vtu",U2tz0,_format=_vtu);
      //compute errors on diffracted field
      TermVector ec2x0=U2dx0-soldx0, ec2y0=U2dy0-soldy0, ec2z0=U2dz0-soldz0;
      Real el2x0= sqrt(abs((Mx0*ec2x0) | ec2x0)),
           el2y0= sqrt(abs((My0*ec2y0) | ec2y0)),
           el2z0= sqrt(abs((Mz0*ec2z0) | ec2z0));
      thePrintStream<<"=== P2 - P2 mesh approximation, number of dofs = "<<V2.nbDofs()<<"==="<<eol;
      thePrintStream<<"   L2 error on x=0 = "<<el2x0<<eol;
      thePrintStream<<"   L2 error on y=0 = "<<el2y0<<eol;
      thePrintStream<<"   L2 error on z=0 = "<<el2z0<<eol;
      thePrintStream<<"   mean L2 error = "<<(el2x0+el2y0+el2z0)/3.<<eol;
      thePrintStream<<"   Linf error on x=0 = "<<norm(ec2x0,0)<<eol;
      thePrintStream<<"   Linf error on y=0 = "<<norm(ec2y0,0)<<eol;
      thePrintStream<<"   Linf error on z=0 = "<<norm(ec2z0,0)<<eol;
//      TermMatrix C22(intg(sphere2,sphere2,u2*G1*v2,_method=ims));
//      thePrintStream<<"sqrt(abs(C22*un2|un2))="<<sqrt(abs(C22*un2|un2))<<eol;

      // =================  P1 approximation  =========================
      Space V12(_domain=sphere2,_interpolation=P1,_name="V12",_notOptimizeNumbering);
      Unknown u12(V12, _name="u12"); TestFunction v12(u12, _name="v12");
      thePrintStream<<" P1 - P2 Mesh aproximation : "<<V12.nbOfElements()<<" elements, "<<V12.nbDofs()<<" dofs"<<eol<<std::flush;
      //thePrintStream<<V12<<eol;
      BilinearForm bf12=intg(sphere2,sphere2,u12*G*v12,_method=ims,_symmetry=_noSymmetry);
      LinearForm fv12 = -intg(sphere2,finc*v12,_quad=_GaussLegendreRule,_order=7);
      //compute matrix and right hand side
      TermMatrix A12(bf12, _name="A12");
      //thePrintStream<<A12<<eol;
      tA=elapsedTime("compute A12"); theCout<<std::flush;
      TermVector B12(fv12, _name="B12");
      //thePrintStream<<B12<<eol;
      //saveToFile("B12.vtu",B12,_format=_vtu);
//      TermMatrix M12(intg(sphere2,u12*v12,_quad=_GaussLegendreRule,_order=5),_name="M12");
//      TermVector F12(u12,sphere2,finc,_name="F12"); F12*=-1;
//      saveToFile("F12.vtu",F12,_format=_vtu);
//      saveToFile("M12F12.vtu",M12*F12,_format=_vtu);
//      TermVector un12(u12,sphere2,1.);
//      thePrintStream<<"sqrt(abs((A12*un12|un12))="<<sqrt(abs((A12*un12|un12)))<<eol;
//      thePrintStream<<"abs(M12*un12|un12)="<<abs(M12*un12|un12)<<eol;
      //solve the system using Gauss elimination
      TermVector U12 = directSolve(A12, B12);
      tS=elapsedTime("solve A12*U12=B12"); theCout<<std::flush;
      saveToFile("U12.vtu",U12,_format=_vtu);
      //thePrintStream<<"U12 = "<<U12<<eol;
      clear(A12);
      //integral representation on x, y, z=0 plane
      TermVector U12dx0=integralRepresentation(wx,planx0,intg(sphere2,G*u12,_quad=GaussLegendre,_order=7),U12,"IR2x");
      TermVector U12tx0=U12dx0+Uix0;
      TermVector U12dy0=integralRepresentation(wy,plany0,intg(sphere2,G*u12,_quad=GaussLegendre,_order=7),U12,"IR2y");
      TermVector U12ty0=U12dy0+Uiy0;
      TermVector U12dz0=integralRepresentation(wz,planz0,intg(sphere2,G*u12,_quad=GaussLegendre,_order=7),U12,"IR2z");
      TermVector U12tz0=U12dz0+Uiz0;
      saveToFile("U12d_x=0.vtu",U12dx0,_format=_vtu);
      saveToFile("U12t_x=0.vtu",U12tx0,_format=_vtu);
      saveToFile("U12d_y=0.vtu",U12dy0,_format=_vtu);
      saveToFile("U12t_y=0.vtu",U12ty0,_format=_vtu);
      saveToFile("U12d_z=0.vtu",U12dz0,_format=_vtu);
      saveToFile("U12t_z=0.vtu",U12tz0,_format=_vtu);
      //compute errors on diffracted field
      TermVector ec12x0=U12dx0-soldx0, ec12y0=U12dy0-soldy0, ec12z0=U12dz0-soldz0;
      Real el12x0= sqrt(abs((Mx0*ec12x0) | ec12x0)),
           el12y0= sqrt(abs((My0*ec12y0) | ec12y0)),
           el12z0= sqrt(abs((Mz0*ec12z0) | ec12z0));
      thePrintStream<<"=== P1 - P2 mesh approximation, number of dofs = "<<V12.nbDofs()<<"==="<<eol;
      thePrintStream<<"   L2 error on x=0 = "<<el12x0<<eol;
      thePrintStream<<"   L2 error on y=0 = "<<el12y0<<eol;
      thePrintStream<<"   L2 error on z=0 = "<<el12z0<<eol;
      thePrintStream<<"   mean L2 error = "<<(el12x0+el12y0+el12z0)/3.<<eol;
      thePrintStream<<"   Linf error on x=0 = "<<norm(ec12x0,0)<<eol;
      thePrintStream<<"   Linf error on y=0 = "<<norm(ec12y0,0)<<eol;
      thePrintStream<<"   Linf error on z=0 = "<<norm(ec12z0,0)<<eol;
//      TermMatrix C12(intg(sphere2,sphere2,u12*G1*v12,_method=ims));
//      thePrintStream<<"sqrt(abs(C12*un12|un12))="<<sqrt(abs(C12*un12|un12))<<eol;

      // =================  P0 approximation  =========================
//      Space V02(_domain=sphere2,_interpolation=P0,_name="V02",_notOptimizeNumbering);
//      Unknown u02(V02,"u02"); TestFunction v02(u02,"v02");
//      thePrintStream<<" P0 - P2 mesh aproximation : "<<V02.nbOfElements()<<" elements, "<<V02.nbDofs()<<" dofs"<<eol<<std::flush;
//      BilinearForm bf02=intg(sphere2,sphere2,u02*G*v02,_method=ims);
//      LinearForm fv02 = -intg(sphere2,finc*v02);
//      //compute matrix and right hand side
//      TermMatrix A02(bf02, _storage=denseDual, _name="A02");
//      tA=elapsedTime("compute A02"); theCout<<std::flush;
//      TermVector B02(fv02, _name="B02");
//      TermVector un02(u02,sphere2,1.);
//      TermMatrix C02(intg(sphere2,sphere2,u02*G1*v02,_method=ims,_symmetry=_noSymmetry));
//      thePrintStream<<"sqrt(abs(C02*un02|un02))="<<sqrt(abs(C02*un02|un02))<<eol;
    }

  std::cout<<"U saved, that's all folks"<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

//#else
//  warning("free_warning","test_IE_Helmholtz3D_DSC skipped");
//#endif
}

}
