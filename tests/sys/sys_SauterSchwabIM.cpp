/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_SauterSchwabIM.cpp 
	\since 19 jun 2014
	\date 19 jun 2014
	\authors N. Salles, Eric Lunéville

	Test Sauter-Schwab integration method for integral equation in P0 interpolation
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_SauterSchwabIM
{

void sys_SauterSchwabIM(int argc, char* argv[], bool check)
{
  String rootname = "sys_SauterSchwabIM";
  trace_p->push(rootname);
  verboseLevel(0);
  //numberOfThreads(4);
  String errors;
  Number nbErrors = 0;
  Real tol=0.000001;

  //meshing
  Mesh m1;
  if (!check)
  {
    Sphere sp(_center=Point(0,0,0),_radius=1.,_nnodes=8,_domain_name="sphere");
    m1=Mesh(sp, _shape=_triangle, _order=1, _generator=_gmsh);
    m1.saveToFile(inputsPathTo(rootname+"/meshsp.msh"),msh);
  }
  else
  {
     m1=Mesh(inputsPathTo(rootname+"/meshsp.msh"), _name="mesh of the unit ball");
  }

  Domain sphere = m1.domain(0);
  elapsedTime("mesh");

  //create Lagrange P0 space and unknown
  Space V0(_domain=sphere, _interpolation=_P1, _name="V0", _notOptimizeNumbering);
  Unknown u(V0, _name="u");
  TestFunction v(u, _name="v");

  Kernel G=Laplace3dKernel(); //Laplace Kernel in 3D

  //use explicit SauterSchwabIM (old fashion)
  Number order=6;
  IntegrationMethods ssim(_method=_SauterSchwabIM, _order=order+2, _quad=_defaultRule, _order=10, _bound=2., _quad=_defaultRule, _order=5);
  BilinearForm bfss=intg(sphere,sphere,u*G*v,_method=ssim);
  LinearForm fv = intg(sphere,v);
  elapsedTime();
  TermMatrix Ass(bfss, _name="Ass");
  elapsedTime("compute Ass", theCout);
  thePrintStream << "||Ass|| = "<<norm2(Ass) << std::endl;

  IntegrationMethods ims1(SauterSchwabIM(order), symmetricalGauss,order);
  theCout<<ims1<<eol;
  BilinearForm bfss1=intg(sphere,sphere,u*G*v,_method=ims1);
  elapsedTime();
  TermMatrix Ass1(bfss1, _name="Ass1");
  elapsedTime("compute Ass1", theCout);
  thePrintStream << "||Ass1|| = "<<norm2(Ass1) << std::endl;
  Real Err=norm2(Ass1-Ass);
  nbErrors+= checkValue(Err , 0., tol, errors, "||Ass1-Ass||");
  thePrintStream << "||Ass1-Ass|| = "<<Err << std::endl;

  IntegrationMethods ims2(SauterSchwabSymIM(order), symmetricalGauss,order);
  theCout<<ims2<<eol;
  BilinearForm bfss2=intg(sphere,sphere,u*G*v,_method=ims2);
  elapsedTime();
  TermMatrix Ass2(bfss2, _name="Ass2");
  elapsedTime("compute Ass2", theCout);
  thePrintStream << "||Ass2|| = "<<norm2(Ass2) << std::endl;Err=norm2(Ass1-Ass);
  nbErrors+= checkValue(Err , 0., tol, errors, "||Ass2-Ass||");
  thePrintStream << "||Ass2-Ass|| = "<<Err << std::endl;

  // IntegrationMethods ims3(_method=LenoirSalles3d, _quad=symmetricalGauss, _order=order);
  // BilinearForm bfss3=intg(sphere,sphere,u*G*v,_method=ims3);
  // elapsedTime();
  // TermMatrix Ass3(bfss3, _name="Ass3");
  // elapsedTime("compute Ass3", theCout);
  // thePrintStream << "||Ass3|| = "<<norm2(Ass3) << std::endl;
  // Err=norm2(Ass3-Ass);
  // nbErrors+= checkValue(Err , 0., tol, errors, "||Ass3-Ass||");
  // thePrintStream << "||Ass3-Ass|| = "<<Err << std::endl;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  // if (nbWarn != 0 ){ warning("free_warning","warning in test solver BEM"); }
      
  trace_p->pop();
  
}

}

