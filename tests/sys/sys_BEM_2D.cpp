/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_BEM_2D.cpp
\author N. Salles
\since 26 May 2016
\date 17 August 2016
*/

/* Test BEM 2D :
- Laplace by comparing with explicit formulas
- Helmholtz by comparing with exact solution for a scattering problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_BEM_2D
{

//polar parametrization for unit circle u in [0,1], t=2pi*u, x=cos(t), y=sin(t)
Vector<Real> polar(const Point& u, Parameters& pars, DiffOpType d=_id)
{
  Vector<Real> xy(2);
  Real t=2*pi_*u[0];
  switch (d)
  {
    case _id: xy[0]=cos(t);  xy[1]=sin(t);break;
    case _d1: xy[0]=-sin(t); xy[1]=cos(t);break;
    case _d11:xy[0]=-cos(t); xy[1]=-sin(t);break;
    default:
      parfun_error("Polar parametrization", d);
  }
  return xy;
}
Vector<Real> invpolar(const Point& xy, Parameters& pars, DiffOpType d=_id)
{
  Vector<Real> u(1);
  switch (d)
  {
    case _id: u[0]=(std::atan2(xy[1],xy[0])+pi_)/(2*pi_);break;
    default:
      parfun_error("inv polar parametrization", d);
  }
  return u;
}


Real laplace_ref;
Real k;
Complex helmholtz_ref;
vector<Point> evalPoints;
Real maxrelerr;

Complex uinc(const Point& M, Parameters& pa = defaultParameters)
{
  Real k=real(pa("k"));
  return exp(i_*k*M[0]);
}


template<typename K>
Real computeRelErrLaplace(BilinearForm& bf, TermVector& rhs,LinearForm& lf, Vector<K>& val)
{
  TermMatrix LaplaceTM(bf, _name="bf");
  TermVector sol=gmresSolve(LaplaceTM, rhs, _tolerance=1.0e-6, _maxIt=500);
  clear(LaplaceTM);
  integralRepresentation(evalPoints,lf,sol,val);
  theCout << "ref= " << laplace_ref << " val= " << val <<eol;
  return std::abs((val[0]-laplace_ref)/laplace_ref);
}

template<typename K>
Real computeRelErrDirect(BilinearForm& bf, BilinearForm& bfrhs, LinearForm& lf1, LinearForm& lf2, Vector<K>& val, TermVector& tg, K ref)
{
  TermMatrix lhs(bf, _name="bf");
  TermMatrix I2pD(bfrhs, _name="bfrhs");
  TermVector rhs=I2pD*tg;
  TermVector sol=gmresSolve(lhs, rhs, _tolerance=1.0e-6, _maxIt=500);
  integralRepresentation(evalPoints,lf1,sol,val);
  Vector<K> val2;
  integralRepresentation(evalPoints,lf2,tg,val2);
  val-=val2;
  theCout << "ref= " << ref << " val= " << val <<eol;
  return std::abs((val[0]-ref)/ref);
}

Real computeRelErrHelmholtz(BilinearForm& bf, TermVector& rhs,LinearForm& lf, Vector<Complex>& val)
{
  TermMatrix HelmholtzTM(bf, _name="bf");
  TermVector sol=gmresSolve(HelmholtzTM, rhs, _tolerance=1.0e-6);
  clear(HelmholtzTM);
  integralRepresentation(evalPoints,lf,sol,val);
  theCout << "ref= " << helmholtz_ref << " val= " << val <<eol;
  return std::abs((val[0]-helmholtz_ref)/helmholtz_ref);
}

// Function performing the validation of Laplace 2D P0 and P1
Number test_Laplace2D(Domain disk)
{
  Number valid=0;
  verboseLevel(0);
  // P0 definition
  Space V0(_domain=disk, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
  theCout << "Number of Dofs in P0 = " << V0.nbDofs() << eol;
  Unknown u0(V0, _name="u0");
  TestFunction v0(u0, _name="v0");

  Space V1(_domain=disk, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  theCout << "Number of Dofs in P1 = " << V1.nbDofs() << eol;
  Unknown u1(V1, _name="u1");
  TestFunction v1(u1, _name="v1");

  theCout << "Number of Dofs in P0 and in P1 = " << V0.nbDofs() << eol;

  // Define IntegrationMethod DuffyIM and Lenoir-Salles
  IntegrationMethods ims(_method=Duffy, _order=8, _quad=_defaultRule, _order=6, _bound=2., _quad=_defaultRule, _order=4, _bound=4., _quad=_defaultRule, _order=3);
  IntegrationMethods ims2(_method=_LenoirSalles2dIM, _quad=_defaultRule, _order=15);

  Kernel Gl=Laplace2dKernel();

  // ===== P0 Case =====
  theCout << "Testing LAPLACE SL P0..." << eol;
  BilinearForm bfLapSLP0=intg(disk,disk,u0*Gl*v0,_method=ims);
  TermMatrix mLapSLP0(bfLapSLP0, _name="mLapSLP0");
  BilinearForm bfLapSLP0b=intg(disk,disk,u0*Gl*v0,_method=ims2);
  TermMatrix mLapSLP0b(bfLapSLP0b, _name="mLapSLP0b");
  // Number numcols=mLapSLP0.numberOfCols();
  // Number numrows=mLapSLP0.numberOfRows();
  TermMatrix diffLapSLP0=mLapSLP0-mLapSLP0b;
  std::vector<Real> & diffval =diffLapSLP0.matrixData()->rEntries_p->values();
  // std::vector<Real> & valexpl=mLapSLP0b.matrixData()->rEntries_p->values();

  std::vector<Real>::iterator it = diffval.begin();
  Real abserr=0.;
  Real abserr2=0.;

  for (; it != diffval.end(); it++) //,ref++)
  {
    Real tmp=std::abs(*it);
    abserr2+=tmp*tmp;
    if(abserr < tmp)
      abserr=tmp;
  }
  abserr2=std::sqrt(abserr2);
  theCout << "Laplace SL P0 case, max abs err = " << abserr << eol;
  theCout << "Laplace SL P0 case, norm 2 err = " << abserr2 << eol;

  clear(diffLapSLP0);
  clear(mLapSLP0b);
  clear(mLapSLP0);

  if (abserr2 > 0.01)
  {
    warning("free_warning","Error in test BEM 2D Laplace SL P0");
    return 1; // test failed
  }

  theCout << "Testing LAPLACE DL P0..." << eol;
  BilinearForm bfLapDLP0=intg(disk,disk,u0*ndotgrad_y(Gl)*v0,_method=ims);
  TermMatrix mLapDLP0(bfLapDLP0, _name="mLapDLP0");
  BilinearForm bfLapDLP0b=intg(disk,disk,u0*ndotgrad_y(Gl)*v0,_method=ims2);
  TermMatrix mLapDLP0b(bfLapDLP0b, _name="mLapDLP0b");

  TermMatrix diffLapDLP0=mLapDLP0-mLapDLP0b;

  std::vector<Real> & diffval2 =diffLapDLP0.matrixData()->rEntries_p->values();
  it = diffval2.begin();
  abserr=0.;
  abserr2=0.;
  for (; it != diffval2.end(); it++) //,ref++)
  {
    Real tmp=std::abs(*it);
    if (abserr < tmp)
      abserr=tmp;
    abserr2+=tmp*tmp;
  }
  abserr2=std::sqrt(abserr2);
  theCout << "Laplace DL P0 case, max abs err = " << abserr << eol;
  theCout << "Laplace DL P0 case, norm 2 err = " << abserr2 << eol;
  if (abserr2 > 0.01)
  {
    warning("free_warning","Error in test BEM 2D Laplace DL P0");
    return 1; // test failed
  }

  clear(mLapDLP0);
  clear(mLapDLP0b);
  clear(diffLapDLP0);

  // ===== P1 Case =====
  theCout << "Testing LAPLACE SL P1..." << eol;
  BilinearForm bfLapSLP1=intg(disk,disk,u1*Gl*v1,_method=ims);
  TermMatrix mLapSLP1(bfLapSLP1, _name="mLapSLP1");
  BilinearForm bfLapSLP1b=intg(disk,disk,u1*Gl*v1,_method=ims2);
  TermMatrix mLapSLP1b(bfLapSLP1b, _name="mLapSLP1b");
  TermMatrix diffLapSLP1=mLapSLP1-mLapSLP1b;

  std::vector<Real> & diffval3 =diffLapSLP1.matrixData()->rEntries_p->values();
  //std::vector<Real> & valexpl=mLapSLP1b.matrixData()->rEntries_p->values();

  it = diffval3.begin();
  abserr=0.;
  abserr2=0.;
  for (; it != diffval3.end(); it++) //,ref++)
  {
    Real tmp=std::abs(*it);
    if(abserr < tmp)
      abserr=tmp;
    abserr2+=tmp*tmp;
  }
  abserr2=std::sqrt(abserr2);
  theCout << "Laplace SL P1 case, max abs err = " << abserr << eol;
  theCout << "Laplace SL P1 case, norm 2 err = " << abserr2 << eol;
  if (abserr2 > 0.01)
  {
    warning("free_warning","Error in test BEM 2D Laplace SL P1");
    return 1; // test failed
  }

  theCout << "Testing LAPLACE DL P1..." << eol;
  theCout << "Test Laplace DL P1 not yet implemented for Lenoir-Salles quadrature" << eol;
//  BilinearForm bfLapDLP1=intg(disk,disk,u1*ndotgrad_y(Gl)*v1,_method=ims);
//  TermMatrix mLapDLP1(bfLapDLP1, _name="mLapDLP1");
//  BilinearForm bfLapDLP1b=intg(disk,disk,u1*ndotgrad_y(Gl)*v1,_method=ims2);
//  TermMatrix mLapDLP1b(bfLapDLP1b, _name="mLapDLP1b");
//  TermMatrix diffLapDLP1=mLapDLP1-mLapDLP1b;
//  std::vector<Real> & diffval4 =diffLapDLP1.matrixData()->rEntries_p->values();
//
//  it = diffval4.begin();
//  abserr=0.;
//  abserr2=0.;
//  for (;it != diffval4.end();it++) //,ref++)
//  {
//    Real tmp=std::abs(*it);
//    if(abserr < tmp) abserr=tmp;
//    abserr2+=tmp*tmp;
//  }
//  abserr2=std::sqrt(abserr2);
//  theCout << "Laplace DL P1 case, max abs err = " << abserr << eol;
//  theCout << "Laplace DL P1 case, norm 2 err = " << abserr2 << eol;
//  if (abserr2 > 0.01)
//  {
//    warning("free_warning","Error in test BEM 2D Laplace DL P1");
//    //theCout << "\t \t Failed test BEM 2D Laplace DL P1" << eol;
//    return 1; // test failed
//  }

  return valid;
}

Number test_Helmholtz2D(Domain disk, Real k, Real radius, Domain omega_ext, bool doSL, bool doDL, bool doP0, bool doP1,
                        Number dosx, Number dosy, Number doax, Number doay, Number depth, Real tol, bool useIso=false )
{
  Number invalid=0;
  Number ord=disk.order();
  //verboseLevel(0);
  // P0 definition
  Space V0(_domain=disk, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
  Unknown u0(V0, _name="u0");
  TestFunction v0(u0, _name="v0");

  // P1 definition
  Space V1(_domain=disk, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u1(V1, _name="u1");
  TestFunction v1(u1, _name="v1");
  String mo="mesh P"+tostring(ord);
  // Define IntegrationMethod DuffyIM and Lenoir-Salles
  DuffyIM dufim(_GaussLegendreRule,dosx,_GaussLegendreRule,dosy,_GaussLegendreRule,doax,_GaussLegendreRule,doay,depth);
  //dufim.print(thePrintStream);
  IntegrationMethods ims(_method=dufim,_bound=0.,_quad=_defaultRule,_order=8,_bound=2.,_quad=_defaultRule,_order=6,_bound=4.,_quad=_defaultRule,_order=4);
  Parameters pars;
  pars << Parameter(k,"k") << Parameter(1.,"radius")<<Parameter(100,"nmax");
  Kernel Gh=Helmholtz2dKernel(pars);
  Function Uinc(uinc,pars); // Incident plane wave

  // Space on Omega_ext
  Space VIR(_domain=omega_ext, _interpolation=P1, _name="VIR", _notOptimizeNumbering);
  Unknown uir(VIR, _name="uir");TestFunction vir(uir, _name="vir");
  TermMatrix MIR(intg(omega_ext,uir*vir), _name="MIR");

  // Exact solution
  Function ScatSolExact(scatteredFieldDiskDirichlet, pars);
  TermVector UScatEx(uir,omega_ext,ScatSolExact);
  Real normref=std::sqrt(std::abs(MIR*UScatEx|UScatEx));

  // Quadratures for IR
  std::set<Quadrature*> quad;
  Quadrature* myquad=findQuadrature(_segment,_GaussLegendreRule,10);
  quad.insert(myquad);
  myquad=findQuadrature(_segment,_GaussLegendreRule,11);
  quad.insert(myquad);
  preComputationIE(disk,&V0,quad,u0.nbOfComponents(),false,false,false,false);
  // ===== P0 Case =====
  if(doP0 && doSL)
  {
    theCout << "Testing HELMHOLTZ SL P0 with "<<mo<<", number of dofs = "<<V0.nbDofs();
    BilinearForm bfHelSLP0=intg(disk,disk,u0*Gh*v0,_method=ims);
    LinearForm lfHelSLP0=-intg(disk,Uinc*v0);
    TermMatrix mHelSLP0(bfHelSLP0, _name="mHelSLP0");
    TermVector vHelSLP0(lfHelSLP0, _name="vHelSLP0");
    TermVector solSLP0=directSolve(mHelSLP0,vHelSLP0);
    clear(mHelSLP0);
    TermVector IRsolSLP0=integralRepresentation(uir,omega_ext,intg(disk,u0*Gh,_quad=_GaussLegendreRule,_order=12),solSLP0);
    saveToFile("IRsolSLP0_"+mo,IRsolSLP0,_format=_vtu);
    TermVector diffScatSolSLP0=IRsolSLP0-UScatEx;
    saveToFile("diffScatSolSLP0_"+mo,diffScatSolSLP0,_format=_vtu);
    Real abserr=std::sqrt(std::abs(MIR*diffScatSolSLP0|diffScatSolSLP0))/normref;
    theCout << ", norm 2 err = " <<  abserr << eol;
    if (abserr > 0.01)
    {
      warning("free_warning","Error in test BEM 2D Helmholtz SL P0");
      invalid++;
    }
  }

  if (doP0 && doDL)
  {
    theCout << "Testing HELMHOLTZ DL P0 with "<<mo<<", number of dofs = "<<V0.nbDofs();
    BilinearForm bfHelDLP0=0.5*intg(disk,u0*v0)+intg(disk,disk,u0*ndotgrad_y(Gh)*v0,_method=ims);
    LinearForm lfHelDLP0=-intg(disk,Uinc*v0);
    TermMatrix mHelDLP0(bfHelDLP0, _name="mHelDLP0");
    TermVector vHelDLP0(lfHelDLP0, _name="vHelDLP0");
    TermVector solDLP0=directSolve(mHelDLP0,vHelDLP0);
    clear(mHelDLP0);
    TermVector IRsolDLP0=integralRepresentation(uir,omega_ext,intg(disk,ndotgrad_y(Gh)*u0,_quad=_GaussLegendreRule,_order=10),solDLP0);
    saveToFile("IRsolDLP0_"+mo,IRsolDLP0,_format=_vtu);
    saveToFile("ExactSol",UScatEx,_format=_vtu);
    TermVector diffScatSolDLP0=IRsolDLP0-UScatEx;
    saveToFile("diffScatSolDLP0_"+mo,diffScatSolDLP0,_format=_vtu);
    Real abserr2=std::sqrt(std::abs(MIR*diffScatSolDLP0|diffScatSolDLP0))/normref;
    theCout << ", norm2 err = " <<  abserr2 << eol;
    if (abserr2 > 0.01)
    {
      warning("free_warning","Error in test BEM 2D Helmholtz DL P0");
      invalid++;
    }
  }

  // ===== P1 Case =====
  if (doP1 && doSL)
  {
    theCout << "Testing HELMHOLTZ SL P1 with "<<mo<<", number of dofs = "<<V1.nbDofs();
    BilinearForm bfHelSLP1=intg(disk,disk,u1*Gh*v1,_method=ims);
    LinearForm lfHelSLP1=-intg(disk,Uinc*v1,_quad=_GaussLegendreRule,_order=5);
    TermMatrix mHelSLP1(bfHelSLP1, _name="mHelSLP1");
    TermVector vHelSLP1(lfHelSLP1, _name="vHelSLP1");
    theCout<<mHelSLP1<<vHelSLP1<<eol;
    TermVector solSLP1=directSolve(mHelSLP1,vHelSLP1);
    theCout<<solSLP1<<eol;
    clear(mHelSLP1);
    TermVector IRsolSLP1=integralRepresentation(uir,omega_ext,intg(disk,u1*Gh,_quad=_GaussLegendreRule,_order=5),solSLP1);
    TermVector diffScatSolSLP1=IRsolSLP1-UScatEx;
    //thePrintStream<<IRsolSLP1<<UScatEx<<diffScatSolSLP1<<eol;
    saveToFile("IRsolSLP1_"+mo,IRsolSLP1,_format=_vtu);
    saveToFile("diffScatSolSLP1_"+mo,diffScatSolSLP1,_format=_vtu);
    Real abserr3=std::sqrt(std::abs(MIR*diffScatSolSLP1|diffScatSolSLP1))/normref;
    theCout << ", norm 2 err = " <<  abserr3 << eol;
    if (abserr3 > 0.01)
    {
      warning("free_warning","Error in test BEM 2D Helmholtz SL P1");
      invalid++;
    }
    // isogeo
    theCout << "using iso-geometric approach"<<eol;
    bool isogeo=true;
    BilinearForm bfHelSLP1i=intg(disk,disk,u1*Gh*v1,_method=ims,_isogeo);
    LinearForm lfHelSLP1i=-intg(disk,Uinc*v1,_quad=_GaussLegendreRule,_order=5, _isogeo);
    TermMatrix mHelSLP1i(bfHelSLP1i, _name="mHelSLP1i");
    TermVector vHelSLP1i(lfHelSLP1i, _name="vHelSLP1i");
    theCout<<mHelSLP1i<<vHelSLP1i<<eol;
    TermVector solSLP1i=directSolve(mHelSLP1i,vHelSLP1i);
    theCout<<solSLP1i<<eol;
    clear(mHelSLP1i);
    TermVector IRsolSLP1i=integralRepresentation(uir,omega_ext,intg(disk,u1*Gh,_quad=_GaussLegendreRule, _order=5, _isogeo),solSLP1i);
    TermVector diffScatSolSLP1i=IRsolSLP1i-UScatEx;
    //thePrintStream<<IRsolSLP1i<<UScatEx<<diffScatSolSLP1i<<eol;
    saveToFile("IRsolSLP1_iso_"+mo,IRsolSLP1i,_format=_vtu);
    saveToFile("diffScatSolSLP1_iso_"+mo,diffScatSolSLP1i,_format=_vtu);
    Real abserr3i=std::sqrt(std::abs(MIR*diffScatSolSLP1i|diffScatSolSLP1i))/normref;
    theCout << ", norm 2 err = " <<  abserr3i << eol;
  }

  if (doP1 && doDL)
  {
    theCout << "Testing HELMHOLTZ DL P1 with "<<mo<<", number of dofs = "<<V1.nbDofs();
    BilinearForm bfHelDLP1=0.5*intg(disk,u1*v1)+intg(disk,disk,u1*ndotgrad_y(Gh)*v1,_method=ims);
    LinearForm lfHelDLP1=-intg(disk,Uinc*v1);
    TermMatrix mHelDLP1(bfHelDLP1, _name="mHelDLP1");
    TermVector vHelDLP1(lfHelDLP1, _name="vHelDLP1");
    TermVector solDLP1=directSolve(mHelDLP1,vHelDLP1);
    clear(mHelDLP1);
    TermVector IRsolDLP1=integralRepresentation(uir,omega_ext,intg(disk,ndotgrad_y(Gh)*u1,_quad=_GaussLegendreRule,_order=10),solDLP1);
    saveToFile("IRsolDLP1_"+mo,IRsolDLP1,_format=_vtu);
    TermVector diffScatSolDLP1=IRsolDLP1-UScatEx;
    saveToFile("diffScatSolDLP1_"+mo,diffScatSolDLP1,_format=_vtu);
    Real abserr4=std::sqrt(std::abs(MIR*diffScatSolDLP1|diffScatSolDLP1))/normref;
    theCout << ", norm2 err = " <<  abserr4 << eol;
    if (abserr4 > 0.01)
    {
      warning("free_warning","Error in test BEM 2D Helmholtz DL P1");
      invalid++;
    }
  }
  return invalid;
}

void sys_BEM_2D(int argc, char* argv[], bool check)
{
  String rootname = "sys_BEM_2D";
  trace_p->push(rootname);
  std::stringstream out;
  //out.precision(fullPrec);
  isTestMode = false;

  Options opts ;
  opts.addOption("H",0.1);
  opts.addOption("NBT",1);
  opts.addOption("R",1);
  opts.addOption("ORD",1);
  opts.addOption("BEM","SL");
  opts.addOption("MESH",1);
  opts.addOption("V",1);
  opts.addOption("DOSX",20);
  opts.addOption("DOSY",20);
  opts.addOption("DOAX",20);
  opts.addOption("DOAY",20);
  opts.addOption("DEPTH",0);
  opts.addOption("TOL",1.e-6);
  opts.parse(argc,argv);
  Real hstep=opts("H");   //nb of points by diameter of disk
  Number nbt=opts("NBT");   //nb of threads
  Real radius=opts("R"); //nb of terms in exact solution expansion
  Number ord=opts("ORD");
  bool doP0 = (ord == 0 || ord==10);
  bool doP1 = (ord == 1 || ord==10);
  String bem=opts("BEM");
  bool doSL = (bem.find("SL")!=String::npos);
  bool doDL = (bem.find("DL")!=String::npos);
  Number meshord=opts("MESH");   //mesh order
  Number vb=opts("V");
  bool doMeshP1 = meshord==1 || meshord==21 || meshord==12;
  bool doMeshP2 = meshord==2 || meshord==21 || meshord==12;
  Number dosx = opts("DOSX");
  Number dosy = opts("DOSY");
  Number doax = opts("DOAX");
  Number doay = opts("DOAY");
  Real tol=opts("TOL");
  Number depth=opts("DEPTH");
  verboseLevel(vb);

  numberOfThreads(nbt);

  laplace_ref=-over2pi_*std::log(2.);
  k=1.35;
  evalPoints.resize(1);

  // Domain for the integral representation, not too close from Gamma.
  SquareGeo sq(_center=Point(0.,0.), _length=4.,_hsteps=0.1,_domain_name="Omega_ext");
  Disk dsk2(_center=Point(0.,0.),_radius=1.3,_hsteps=0.1);
  Mesh m2(sq-dsk2,_shape=_triangle, _order=1, _generator=gmsh);
  Domain omega_ext=m2.domain("Omega_ext");

  Disk sp(_center=Point(0.,0.),_radius=radius,_hsteps=hstep);
  //Mesh m1("mesh_BEM2D.msh", _name="Omega", _nodesDim=2);
  Mesh m1(sp, _shape=_segment, _order=1, _generator=_gmsh);
  Domain disk = m1.domain(0);
  Parameters pars;
  Parametrization parc(0.,1.,polar, invpolar, pars,"polar parametrization");
  disk.setParametrization(parc);
//  Number validLaplace=test_Laplace2D(disk);
//  if(validLaplace!=0)
//    error("free_error","Error in tests for BEM 2D Laplace");

   Number invalidHelmholtz=0;
  // We compare the solution of the scattering of a disk by a plane wave with the exact solution
  if (doMeshP1)
     invalidHelmholtz=test_Helmholtz2D(disk,k,radius,omega_ext,doSL,doDL,doP0,doP1,dosx,dosy,doax,doay,depth,tol);
//  if(validHelmholtz!=0)
//    error("free_error","Error in tests for BEM 2D Helmholtz");

  if (doMeshP2)
  {
    Mesh mp2(sp, _shape=_segment, _order=2, _generator=_gmsh);
    Domain diskp2 = mp2.domain(0);
    invalidHelmholtz+=test_Helmholtz2D(diskp2,k,radius,omega_ext,doSL,doDL,doP0,doP1,dosx,dosy,doax,doay,depth,tol);
  }

  theCout << "All tests passed ..." << eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  trace_p->pop();

}

}
