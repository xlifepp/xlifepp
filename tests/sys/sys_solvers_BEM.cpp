/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*! 

\file sys_solvers_BEM.cpp
\author E. Lunéville
\since 1 March 2014
\date 1 March 2014
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_solvers_BEM
{

Real g1(const Point& M, Parameters& pa = defaultParameters)
{
  Point x0(0., 0., 0.);
  return over4pi_/M.distance(x0);
}

void sys_solvers_BEM(int argc, char* argv[], bool check)
{
  String rootname = "sys_solvers_BEM";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Number nbWarn = 0;
  Real tolRelErr=0.002;
  Real ErrRef=0.007;

  elapsedTime();

  numberOfThreads(4);

  Number nmesh=15;
  Mesh m1;
  if (!check)
  {
    Ball sp(_center=Point(0,0,0),_radius=1.,_nnodes=8,_domain_name="Ball");
    m1=Mesh(sp, _shape=_triangle, _order=1, _generator=_gmsh);
    m1.saveToFile(inputsPathTo(rootname+"/meshsp.msh"),msh);
  }
  else
  {
     m1=Mesh(inputsPathTo(rootname+"/meshsp.msh"), _name="mesh of the unit ball");
  }
  Domain sphere = m1.domain(0);

  Space V0(_domain=sphere, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
  Unknown u0(V0, _name="u0");   TestFunction v0(u0, _name="v0");
  thePrintStream << "Number of Dofs = " << V0.nbDofs() << eol;

  String pb="Laplace";
  //String pb="Helmholtz";
  //String bem="single";
  String bem="double";

  Kernel G;
  if (pb=="Laplace") G=Laplace3dKernel();
  else G=Helmholtz3dKernel(1.);
  Number order=3;
  IntegrationMethods ims(_SauterSchwabIM,4,0.,_defaultRule,4,2.,_defaultRule,3);
  cpuTime("mesh",ssout);

  BilinearForm bfss;
  if (bem=="single")  bfss=intg(sphere,sphere,u0*G*v0,_method=ims);
  else bfss=-0.5*intg(sphere,u0*v0)+intg(sphere,sphere,u0*ndotgrad_y(G)*v0,_method=ims);
  elapsedTime("mesh-space",ssout);
  TermMatrix lhs(bfss, _storage=denseRow, _name="lhs");
  //lhs.saveToFile("lhs.txt",_dense);
  Function dirichlet_data(g1);
  LinearForm fv = intg(sphere,dirichlet_data*v0);
  TermVector rhs(fv);

  thePrintStream<<"======================================================================================"<<eol;
  thePrintStream<<"                  "<<pb<<" Dirichlet with "<<bem<<" layer BEM"<<eol;
  thePrintStream<<"======================================================================================"<<eol;

  elapsedTime("BEM matrix",ssout);

  TermVector solgmres = gmresSolve(lhs,rhs);
  elapsedTime("GMRES solver",ssout);
  TermVector solbicg= bicgSolve(lhs,rhs);
  cpuTime("BiCG solver",ssout);
  TermVector solbicgs = bicgStabSolve(lhs,rhs);
  cpuTime("BiCGStab solver",ssout);
  TermVector solqmr = qmrSolve(lhs,rhs);
  cpuTime("QMR solver",ssout);
  TermVector solcgs = cgsSolve(lhs,rhs);
  cpuTime("CGS solver",ssout);
  TermVector solcg = cgSolve(lhs,rhs);
  cpuTime("CG solver",ssout);
  TermVector sol = directSolve(lhs,rhs);
  cpuTime("Direct solver",ssout);

  // Integral representation at a point
  if (pb=="Laplace")
  {
    vector<Point> myPoints(1);
    myPoints[0]=Point(0.,0.,0.);
    Vector<Complex> val;
    LinearForm test;
    if (bem=="single") test=intg(sphere,G*u0,_quad=_symmetricalGaussRule,_order=7);
    else test = intg(sphere,ndotgrad_y(G)*u0,_quad=_symmetricalGaussRule,_order=7);

    integralRepresentation(myPoints,test,solgmres,val);
    thePrintStream << "gmres val = " << val << "exact val = " << over4pi_
                   << " difference = "  <<abs(val-over4pi_)<<" relative error = "<<abs(val-over4pi_)/over4pi_<<eol;
    nbErrors += checkValue( norm2(val-over4pi_)/over4pi_, ErrRef, tolRelErr, errors, "Relative error Gmres");
    integralRepresentation(myPoints,test,solbicg,val);
    thePrintStream << "bicg val = " << val << "exact val = " << over4pi_
                   << " difference = "  <<abs(val-over4pi_)<<" relative error = "<<abs(val-over4pi_)/over4pi_<<eol;
    nbErrors += checkValue( norm2(val-over4pi_)/over4pi_, ErrRef, tolRelErr, errors, "Relative error bicg");
    integralRepresentation(myPoints,test,solbicgs,val);
    thePrintStream << "bicgstab val = " << val << "exact val = " << over4pi_
                   << " difference = "  <<abs(val-over4pi_)<<" relative error = "<<abs(val-over4pi_)/over4pi_<<eol;
    nbErrors += checkValue( norm2(val-over4pi_)/over4pi_, ErrRef, tolRelErr, errors, "Relative error bicgstab");
    integralRepresentation(myPoints,test,solqmr,val);
    thePrintStream << "qmr val = " << val << "exact val = " << over4pi_
                   << " difference = "  <<abs(val-over4pi_)<<" relative error = "<<abs(val-over4pi_)/over4pi_<<eol;
    nbErrors += checkValue( norm2(val-over4pi_)/over4pi_, ErrRef, tolRelErr, errors, "Relative error qmr");
    integralRepresentation(myPoints,test,solcgs,val);
    thePrintStream << "cgs val = " << val << "exact val = " << over4pi_
                   << " difference = "  <<abs(val-over4pi_)<<" relative error = "<<abs(val-over4pi_)/over4pi_<<eol;
    nbErrors += checkValue( norm2(val-over4pi_)/over4pi_, ErrRef, tolRelErr, errors, "Relative error cgs");
    integralRepresentation(myPoints,test,solcg,val);
    thePrintStream << "cg val = " << val << "exact val = " << over4pi_
                   << " difference = "  <<abs(val-over4pi_)<<" relative error = "<<abs(val-over4pi_)/over4pi_<<eol;
    nbErrors += checkValue( norm2(val-over4pi_)/over4pi_, ErrRef, tolRelErr, errors, "Relative error cg");
    integralRepresentation(myPoints,test,sol,val);
    thePrintStream << "direct val = " << val << "exact val = " << over4pi_
                   << " difference = "  <<abs(val-over4pi_)<<" relative error = "<<abs(val-over4pi_)/over4pi_<<eol;
    nbErrors += checkValue( norm2(val-over4pi_)/over4pi_, ErrRef, tolRelErr, errors, "Relative error directSolve");
  }

  //test integral representation on plane z=1+a;
  // Real a=0.5;
  // SquareGeo splan(_center=Point(0,0,1+a),_length=2,_nnodes=100,_domain_name="plan_z");
  // Mesh mplan(splan, _shape=_triangle, _order=1, _generator=gmsh);
  // Domain plan=mplan.domain("plan_z");
  // Space Vp(plan,P1,"Vp",false);
  // Unknown up(Vp,"up");
  // TermVector Up;
  // G.regPart = &G;    G.singPart = 0;
  // IntgRepresentationIM ir(_symmetricalGaussRule,2,_symmetricalGaussRule,2,4.);
  // elapsedTime();
  // if(bem=="single") Up=integralRepresentation(up,plan,intg(sphere,G*solgmres,ir));
  // else              Up=integralRepresentation(up,plan,intg(sphere,ndotgrad_y(G)*solgmres,ir));
  // ssout<<"number of points int intg. representation : "<<Vp.nbDofs()<<eol<<ir;
  // elapsedTime("Intg. Rep. reg+sing", thePrintStream);
  // G.regPart = 0;
  // elapsedTime();
  // if(bem=="single") Up=integralRepresentation(up,plan,intg(sphere,G*solgmres,_quad=_symmetricalGaussRule,_order=2));
  // else              Up=integralRepresentation(up,plan,intg(sphere,ndotgrad_y(G)*solgmres,_quad=_symmetricalGaussRule,_order=2));
  // thePrintStream<<"number of points int intg. representation : "<<Vp.nbDofs()<<eol;
  // elapsedTime("Intg. Rep. quad", thePrintStream);

  // saveToFile("Up",Up,_format=_vtk);
  // saveToFile("U",solgmres,_format=_vtk);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  // nbWarn=checkValue(ssout, rootname + "/sys_solvers_BEM.in", errors, "sys_solvers_BEM.in", check);
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated. " << eol; }
      
  trace_p->pop();
  
}

}
