/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file sys_high_order_mesh.cpp
  \author E. Lunéville
  \since 16 mai 2024
  \date 16 mai 2024

  check high order mesh and high order Lagrange interpolation on a disk sector [0,pi/2]x[0,1]

*/
#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_high_order_mesh
{
/*
  u(r,t) = r^m sin(nt)

  delta(u) = r^(m-2)sin(nt)(m^2-n^2) = -f
  u=0 at t=0
  dt(u)= n r^m cos(nt) = 0 at t=pi/2 in n is odd
  u = sin(nt) at r=1
  dr(u) = m sin(nt) = g  at r=1

  f=0 when m = n and g=0 when m = 0

*/

Real m=3, n=3;  // f=0

Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real r = norm(P), t = atan2(P(2),P(1));
  return pow(r,m)*sin(n*t);
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  if(m==n) return 0.;
  Real r = norm(P), t = atan2(P(2),P(1));
  return -pow(r,m-2)*sin(n*t)*(m*m-n*n);
}
Real g(const Point& P, Parameters& pa = defaultParameters)
{
  if(m==0.) return 0.;
  Real t = atan2(P(2),P(1));
  return m*sin(n*t);
}
Real ur1(const Point& P, Parameters& pa = defaultParameters)
{
  Real t = atan2(P(2),P(1));
  return sin(n*t);
}
Real utpis2(const Point& P, Parameters& pa = defaultParameters)
{
  Real t = atan2(P(2),P(1));
  return sin(n*t);
}

void sys_high_order_mesh(int argc, char* argv[], bool check)
{
  String rootname = "sys_high_order_mesh";
  trace_p->push(rootname);

  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;
  bool SaveToFile=false;
   //numberOfThreads(1);
  Parameters params;

  Number P=5, K=3;
  Real hmin=0.02, hmax=0.42, dh=(hmax-hmin)/20;
  std::ofstream erout;
  for(Number p=1;p<=P; p++)
  {
    erout.open("Error_Laplace_Ellipse2D_mo="+tostring(p)+".dat");
    std::cout<<"mesh order "<<p<<eol;
    for(Real h=hmin; h<=hmax; h+=dh)
    {
     std::cout<<"  mesh step "<<h<<eol;
     //mesh domain
//     Mesh mesh2d(Disk(_center=Point(0.,0.),_radius=1.,_hsteps=h,_side_names={"theta=0","r=1","theta=pi/2"}),
//                      _shape=_triangle, _order=p, _generator=_gmsh);
//     Domain omega=mesh2d.domain("Omega");
//     Domain gamma0=mesh2d.domain("theta=0"), gamma1=mesh2d.domain("theta=pi/2"),sigma=mesh2d.domain("r=1");
     Mesh mesh2d(Ellipse(_center=Point(0.,0.),_xradius=1.4,_yradius=0.7, _hsteps=h,_side_names={"gamma"}),
                      _shape=_triangle, _order=p, _generator=_gmsh);
     Domain omega=mesh2d.domain("Omega");
     Domain gamma=mesh2d.domain("gamma");
     for(Number k=1;k<=p+1; k++)
     {
         elapsedTime();
         std::cout<<"    FE order "<<k<<eol<<std::flush;
         Space V(_domain=omega,_FE_type=Lagrange,_order=k,_name="V");
         Unknown u(V,_name="u");  TestFunction v(u,_name="v");
         BilinearForm auv = intg(omega,grad(u)|grad(v));
//         LinearForm fv;
//         if(m!=n) fv=intg(omega,Function(f,params)*v);
//         if(m!=0) fv+=intg(sigma,Function(g,params)*v);
//         EssentialConditions ecs= (u|gamma0=0);
//         TermMatrix A(auv,ecs,_name="A");
//         TermVector B(fv, _name="B");
         EssentialConditions ecs= (u|gamma=uex);
         TermMatrix A(auv,ecs,_name="A");
         TermVector B(v,omega,0.);
         TermVector U=directSolve(A,B);
         TermVector Uex(u,omega,uex);
         TermMatrix M(intg(omega,u*v));
         TermVector E=Uex-U;
         Real erl2=sqrt(real((M*E|E))), erc0=norminfty(E);
         Real t=elapsedTime();
         theCout<<std::scientific <<std::setprecision(5)<<"mesh order "<<p<<", Lagrange order "<<k<<", nb dofs = "<< V.nbDofs()<<", L2 error = "<<erl2<<", C0 error = "<<erc0<<" time = "<<t;
         erout<<k<<" "<<h<<" "<<erl2<<" "<<erc0<<" "<<V.nbDofs()<<" "<<t<<eol;
         if (SaveToFile)
         {
           saveToFile("U_m"+tostring(p)+"_P"+tostring(k),U,_format=_vtu);
           saveToFile("Uex_m"+tostring(p)+"_P"+tostring(k),Uex,_format=_vtu);
         }
         theCout<<eol;
     }
    }
    erout.close();
  }
  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
