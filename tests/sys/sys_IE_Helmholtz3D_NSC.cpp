/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_IE_Helmholtz3D_NSC.cpp
	\since 20 jun 2014
	\date 20 jun 2014
	\author Eric Lunéville

	Solve Helmholtz 3D problem using Dirichlet double layer IE = Neumann double layer
	P0-P1-P2 approximation and Sauter Schwab integration method
	This test use the mesh of a sphere (surface mesh in R3)
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_IE_Helmholtz3D_NSC {

// incident plane wave
Complex uinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky"), kz=pa("kz");
  Real kp=kx*p(1)+ky*p(2)+kz*p(3);
  return exp(i_*kp);
}

// normal derivative of incident plane wave on sphere
Complex druinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky"), kz=pa("kz");
  Real kp=kx*p(1)+ky*p(2)+kz*p(3);
  return i_*kp*exp(i_*kp);
}

void sys_IE_Helmholtz3D_NSC(int argc, char* argv[], bool check)
{
  String rootname = "sys_IE_Helmholtz3D_NSC";
//#ifdef XLIFEPP_WITH_GMSH
  trace_p->push(rootname);

  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.05;
  bool saveF=false;

  //computing parameters
  bool doP0=true, doP1=true, doP2=true;
  Number npa=16;   //nb of points by diameter of sphere

  //define parameters and functions
  Parameters pars;
  pars<<Parameter(4.,"k");       // wave number
  pars<<Parameter(4.,"kx")<<Parameter(0.,"ky")<<Parameter(0.,"kz");      // x,y,z wave number of incident wave
  pars<<Parameter(1.,"radius");        // sphere radius
  Kernel G = Helmholtz3dKernel(pars);  //load Helmholtz3D kernel
  Function finc(druinc,pars);
  Function ginc(uinc,pars);
  Function scatSol(scatteredFieldSphereNeumann,pars);

  //meshing
  Sphere sp(_center=Point(0,0,0),_radius=1.,_nnodes=npa,_domain_name="sphere");
  Mesh m1(sp, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain sphere = m1.domain(0);

  elapsedTime("mesh");

  //Sauter-Schwab integration method
  Number order=5;
  IntegrationMethods ims(_SauterSchwabIM,order,0.,_defaultRule,4,2.,_defaultRule,3);

  //used by any interpolation
  Space V1(_domain=sphere, _interpolation=P1, _name="V1");
  Unknown u1(V1, _name="u1"); TestFunction v1(u1, _name="v1");
  TermMatrix M11(intg(sphere,u1*v1), _name="M11");

  //for integral representation on x=0 plane
  Rectangle recx(_v1=Point(0.,-4.,-4.),_v2=Point(0.,4.,-4.),_v4=Point(0.,-4.,4.),_nnodes=80,_domain_name="x=0");
  Disk dx(_center=Point(0.,0.,0.),_v1=Point(0.,1.25,0.),_v2=Point(0.,0.,1.25),_nnodes=64,_domain_name="x=0");
  Mesh mx0(recx-dx, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain planx0 = mx0.domain(0);
  Space Wx(_domain=planx0, _interpolation=P1, _name="Wx");
  Unknown wx(Wx,"wx");
  //TermMatrix Rx(wx, planx0, u1, sphere, grad_y(G)|_ny, _name="R");  //(grady(G)|ny)(xi,yj)
  TermMatrix Rx(wx, planx0, u1, sphere, G, "R");  //G(xi,yj)
  TermVector Uix0(wx,planx0,ginc);
  TermVector soldx0(wx,planx0,scatSol);
  TermVector soltx0=Uix0+soldx0;
  TermMatrix Mx0(intg(planx0, wx*wx), _name="Mx0");  //mass matrix on x=0

  //for integral representation on y=0 plane
  Rectangle recy(_v1=Point(-4.,0.,-4.),_v2=Point(4.,0.,-4.),_v4=Point(-4.,0.,4.),_nnodes=80,_domain_name="y=0");
  Disk dy(_center=Point(0.,0.,0.),_v1=Point(1.25,0.,0.),_v2=Point(0.,0.,1.25),_nnodes=64,_domain_name="y=0");
  Mesh my0(recy-dy, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain plany0 = my0.domain(0);
  Space Wy(_domain=plany0, _interpolation=P1, _name="Wy");
  Unknown wy(Wy, _name="wy");
  //TermMatrix Ry(wy, plany0, u1, sphere, grad_y(G)|_ny, _name="R");  //(grady(G)|ny)(xi,yj)
  TermMatrix Ry(wy, plany0, u1, sphere, G, _name="R");  //G(xi,yj)
  TermVector Uiy0(wy,plany0,ginc);
  TermVector soldy0(wy,plany0,scatSol);
  TermVector solty0=Uiy0+soldy0;
  TermMatrix My0(intg(plany0, wy*wy), _name="My0");  //mass matrix on y=0

  //integral representation on z=0 plane
  Rectangle recz(_v1=Point(-4.,-4.,0.),_v2=Point(4.,-4.,0.),_v4=Point(-4.,4.,0.),_nnodes=80,_domain_name="z=0");
  Disk dz(_center=Point(0.,0.,0.),_v1=Point(1.25,0.,0.),_v2=Point(0.,1.25,0.),_nnodes=64,_domain_name="z=0");
  Mesh mz0(recz-dz, _shape=_triangle, _order=1, _generator=_gmsh);
  Domain planz0 = mz0.domain(0);
  Space Wz(_domain=planz0, _interpolation=P1, _name="Wz");
  Unknown wz(Wz, _name="wz");
  //TermMatrix Rz(wz, planz0, u1, sphere, grad_y(G)|_ny, _name="R");  //(grady(G)|ny)(xi,yj)
  TermMatrix Rz(wz, planz0, u1, sphere, G, _name="R");  //G(xi,yj)
  TermVector Uiz0(wz,planz0,ginc);
  TermVector soldz0(wz,planz0,scatSol);
  TermVector soltz0=Uiz0+soldz0;
  TermMatrix Mz0(intg(planz0, wz*wz), _name="Mz0");  //mass matrix on z=0

  //save exact diffracted field on x,y,z planes
  if (saveF) saveToFile("sold_x=0.vtk",soldx0,_format=_vtk);
  if (saveF) saveToFile("sold_y=0.vtk",soldy0,_format=_vtk);
  if (saveF) saveToFile("sold_z=0.vtk",soldz0,_format=_vtk);
  if (saveF) saveToFile("solt_x=0.vtk",soltx0,_format=_vtk);
  if (saveF) saveToFile("solt_y=0.vtk",solty0,_format=_vtk);
  if (saveF) saveToFile("solt_z=0.vtk",soltz0,_format=_vtk);

  Real Nl2S=sqrt(abs((Mx0*soldx0|soldx0)+(My0*soldy0|soldy0)+(Mz0*soldz0|soldz0))/3.);
  thePrintStream<<"test IE single layer Neumann problem for Hemholtz equation on a sphere"<<eol;
  thePrintStream<<" P1 mesh : number of triangles  = "<<m1.nbOfElements()<<" number of vertices = "<<m1.nbOfVertices()<<eol;
  thePrintStream<<" L2 norm of solution on planes : "<<Nl2S<<eol;
  nbErrors+=checkValue(Nl2S, 1.903, 0.02, errors, "L2 norm of solution on plane");
 
  Real tA, tS;

  //=================  P0 computation =========================
  if (doP0)
  {
    //create Lagrange P0 space and unknown
    Space V0(_domain=sphere, _interpolation=P0, _name="V0", _notOptimizeNumbering);
    Unknown u0(V0, _name="u0");
    TestFunction v0(u0, _name="v0");
    //define forms
    BilinearForm bf0=0.5*intg(sphere,u0*v0)-intg(sphere,sphere,u0*ndotgrad_x(G)*v0,_method=ims);
    LinearForm fv0 = intg(sphere,finc*v0,_quad=GaussLegendre,_order=4);
    //compute matrix and right hand side
    TermMatrix A0(bf0, _name="A0");
    //A0.saveToFile("A0.dat",_dense,false);
    tA=elapsedTime("compute A");
    TermVector B0(fv0, _name="B0");
    //B0.saveToFile("B0.dat",false);

    //solve the system using gmres
    //solve the system using direct method
    TermVector U0 = directSolve(A0,B0);
    tS=elapsedTime("solve AU=B");
    clear(A0);

    //P1 projection of P0 solution
    TermMatrix M10(intg(sphere,u0*v1), _name="M10");
    TermMatrix M11f;
    factorize(M11,M11f);
    TermVector U01=directSolve(M11f,M10*U0);
    elapsedTime("P1 projection");
    if (saveF) saveToFile("U01.vtk",U01,_format=_vtk);
    thePrintStream<<"U0 = "<<U0<<eol;
    thePrintStream<<"U01 = "<<U01<<eol;

    //integral representation on x,y,z planes
    TermVector U0dx0=Rx*(M11*U01);
    TermVector U0tx0=U0dx0+Uix0;
    if (saveF) saveToFile("U0d_x=0.vtk",U0dx0,_format=_vtk);
    if (saveF) saveToFile("Ui_x=0.vtk",Uix0,_format=_vtk);
    if (saveF) saveToFile("U0t_x=0.vtk",U0tx0,_format=_vtk);
    TermVector U0dy0=Ry*(M11*U01);
    TermVector U0ty0=U0dy0+Uiy0;
    if (saveF) saveToFile("U0d_y=0.vtk",U0dy0,_format=_vtk);
    if (saveF) saveToFile("Ui_y=0.vtk",Uiy0,_format=_vtk);
    if (saveF) saveToFile("U0t_y=0.vtk",U0ty0,_format=_vtk);
    TermVector U0dz0=Rz*(M11*U01);
    TermVector U0tz0=U0dz0+Uiz0;
    if (saveF) saveToFile("U0d_z=0.vtk",U0dz0,_format=_vtk);
    if (saveF) saveToFile("Ui_z=0.vtk",Uiz0,_format=_vtk);
    if (saveF) saveToFile("U0t_z=0.vtk",U0tz0,_format=_vtk);

    //compute errors on diffracted field
    TermVector ec0x0=U0dx0-soldx0, ec0y0=U0dy0-soldy0, ec0z0=U0dz0-soldz0;
    Real el0x0= sqrt(abs((Mx0*ec0x0) | ec0x0)),
          el0y0= sqrt(abs((My0*ec0y0) | ec0y0)),
          el0z0= sqrt(abs((Mz0*ec0z0) | ec0z0));
    thePrintStream<<"=== PO approximation, number of dofs = "<<V0.nbDofs()<<"==="<<eol;
    thePrintStream<<"   L2 error on x=0 = "<<el0x0<<eol;
    thePrintStream<<"   L2 error on y=0 = "<<el0y0<<eol;
    thePrintStream<<"   L2 error on z=0 = "<<el0z0<<eol;
    thePrintStream<<"   Linf error on x=0 = "<<norm(ec0x0,0)<<eol;
    thePrintStream<<"   Linf error on y=0 = "<<norm(ec0y0,0)<<eol;
    thePrintStream<<"   Linf error on z=0 = "<<norm(ec0z0,0)<<eol;
    thePrintStream<<"   time compute A = "<<tA<<" time solve = "<<tS<<eol;
    nbErrors+=checkValue(el0x0, 0., tol, errors, "L2 error on x=0");
    nbErrors+=checkValue(el0y0, 0., tol, errors, "L2 error on y=0");
    nbErrors+=checkValue(el0z0, 0., tol, errors, "L2 error on z=0");
    nbErrors+=checkValue(norm(ec0x0,0), 0., tol, errors, "Linf error on x=0");
    nbErrors+=checkValue(norm(ec0y0,0), 0., tol, errors, "Linf error on y=0");
    nbErrors+=checkValue(norm(ec0z0,0), 0., tol, errors, "Linf error on z=0");
  }

  //=================  P1 computation =========================
  if (doP1)
  {
    //define forms
    BilinearForm bf1=0.5*intg(sphere,u1*v1)-intg(sphere,sphere,u1*ndotgrad_x(G)*v1,_method=ims);
    BilinearForm mlf1=intg(sphere,u1*v1);
    LinearForm fv1 = intg(sphere,finc*v1,_quad=GaussLegendre,_order=4);
    //compute matrix and right hand side
    TermMatrix A1(bf1, _name="A1");
    tA=elapsedTime("compute A");
    TermVector B1(fv1, _name="B1");

    //solve the system using Gauss elimination
    TermVector U1 = directSolve(A1, B1);
    tS=elapsedTime("solve AU=B");
    if (saveF) saveToFile("U1.vtk",U1,_format=_vtk);
    if (saveF) saveToFile("U1.dat",U1,_format=_raw);
    thePrintStream<<"U1 = "<<U1<<eol;
    clear(A1);
    //integral representation on x, y, z=0 plane
    TermVector U1dx0=Rx*(M11*U1);
    TermVector U1tx0=U1dx0+Uix0;
    if (saveF) saveToFile("U1d_x=0.vtk",U1dx0,_format=_vtk);
    if (saveF) saveToFile("U1t_x=0.vtk",U1tx0,_format=_vtk);
    TermVector U1dy0=Ry*(M11*U1);
    TermVector U1ty0=U1dy0+Uiy0;
    if (saveF) saveToFile("U1d_y=0.vtk",U1dy0,_format=_vtk);
    if (saveF) saveToFile("U1t_y=0.vtk",U1ty0,_format=_vtk);
    TermVector U1dz0=Rz*(M11*U1);
    TermVector U1tz0=U1dz0+Uiz0;
    if (saveF) saveToFile("U1d_z=0.vtk",U1dz0,_format=_vtk);
    if (saveF) saveToFile("U1t_z=0.vtk",U1tz0,_format=_vtk);
    //compute errors on diffracted field
    TermVector ec1x0=U1dx0-soldx0, ec1y0=U1dy0-soldy0, ec1z0=U1dz0-soldz0;
    Real el1x0= sqrt(abs((Mx0*ec1x0) | ec1x0)),
          el1y0= sqrt(abs((My0*ec1y0) | ec1y0)),
          el1z0= sqrt(abs((Mz0*ec1z0) | ec1z0));
    thePrintStream<<"=== P1 approximation, number of dofs = "<<V1.nbDofs()<<"==="<<eol;
    thePrintStream<<"   L2 error on x=0 = "<<el1x0<<eol;
    thePrintStream<<"   L2 error on y=0 = "<<el1y0<<eol;
    thePrintStream<<"   L2 error on z=0 = "<<el1z0<<eol;
    thePrintStream<<"   Linf error on x=0 = "<<norm(ec1x0,0)<<eol;
    thePrintStream<<"   Linf error on y=0 = "<<norm(ec1y0,0)<<eol;
    thePrintStream<<"   Linf error on z=0 = "<<norm(ec1z0,0)<<eol;
    thePrintStream<<"   time compute A = "<<tA<<" time solve = "<<tS<<eol;
    nbErrors+=checkValue(el1x0, 0., tol, errors, "L2 error on x=0");
    nbErrors+=checkValue(el1y0, 0., tol, errors, "L2 error on y=0");
    nbErrors+=checkValue(el1z0, 0., tol, errors, "L2 error on z=0");
    nbErrors+=checkValue(norm(ec1x0,0), 0., tol, errors, "Linf error on x=0");
    nbErrors+=checkValue(norm(ec1y0,0), 0., tol, errors, "Linf error on y=0");
    nbErrors+=checkValue(norm(ec1z0,0), 0., tol, errors, "Linf error on z=0");
  }

  //=================  P2 computation =========================
  if (doP2)
  {
    //define forms
    Space V2(_domain=sphere, _interpolation=P2, _name="V2");
    Unknown u2(V2,"u2"); TestFunction v2(u2,"v2");
    BilinearForm bf2=0.5*intg(sphere,u2*v2)-intg(sphere,sphere,u2*(grad_x(G)|_nx)*v2,_method=ims);
    LinearForm fv2 = intg(sphere,finc*v2,_quad=GaussLegendre,_order=4);

    //compute matrix and right hand side
    TermMatrix A2(bf2, _name="A2");
    tA=elapsedTime("compute A");
    TermVector B2(fv2, _name="B2");
    //solve the system using Gauss elimination
    TermVector U2 = directSolve(A2, B2);
    tS=elapsedTime("solve AU=B");
    if (saveF) saveToFile("U2.vtk",U2,_format=_vtk);
    thePrintStream<<"U2 = "<<U2<<eol;
    clear(A2);
    //integral representation on x, y, z=0 plane
    TermMatrix M12(intg(sphere,u2*v1), _name="M12");
    TermVector U2dx0=Rx*(M12*U2);
    TermVector U2tx0=U2dx0+Uix0;
    if (saveF) saveToFile("U2d_x=0.vtk",U2dx0,_format=_vtk);
    if (saveF) saveToFile("U2t_x=0.vtk",U2tx0,_format=_vtk);
    TermVector U2dy0=Ry*(M12*U2);
    TermVector U2ty0=U2dy0+Uiy0;
    if (saveF) saveToFile("U2d_y=0.vtk",U2dy0,_format=_vtk);
    if (saveF) saveToFile("U2t_y=0.vtk",U2ty0,_format=_vtk);
    TermVector U2dz0=Rz*(M12*U2);
    TermVector U2tz0=U2dz0+Uiz0;
    if (saveF) saveToFile("U2d_z=0.vtk",U2dz0,_format=_vtk);
    if (saveF) saveToFile("U2t_z=0.vtk",U2tz0,_format=_vtk);

    //compute errors on diffracted field
    TermVector ec2x0=U2dx0-soldx0, ec2y0=U2dy0-soldy0, ec2z0=U2dz0-soldz0;
    Real el2x0= sqrt(abs((Mx0*ec2x0) | ec2x0)),
          el2y0= sqrt(abs((My0*ec2y0) | ec2y0)),
          el2z0= sqrt(abs((Mz0*ec2z0) | ec2z0));
    thePrintStream<<"=== P2 approximation, number of dofs = "<<V2.nbDofs()<<"==="<<eol;
    thePrintStream<<"   L2 error on x=0 = "<<el2x0<<eol;
    thePrintStream<<"   L2 error on y=0 = "<<el2y0<<eol;
    thePrintStream<<"   L2 error on z=0 = "<<el2z0<<eol;
    thePrintStream<<"   Linf error on x=0 = "<<norm(ec2x0,0)<<eol;
    thePrintStream<<"   Linf error on y=0 = "<<norm(ec2y0,0)<<eol;
    thePrintStream<<"   Linf error on z=0 = "<<norm(ec2z0,0)<<eol;
    thePrintStream<<"   time compute A = "<<tA<<" time solve = "<<tS<<eol;
    nbErrors+=checkValue(el2x0, 0., tol, errors, "L2 error on x=0");
    nbErrors+=checkValue(el2y0, 0., tol, errors, "L2 error on y=0");
    nbErrors+=checkValue(el2z0, 0., tol, errors, "L2 error on z=0");
    nbErrors+=checkValue(norm(ec2x0,0), 0., tol, errors, "Linf error on x=0");
    nbErrors+=checkValue(norm(ec2y0,0), 0., tol, errors, "Linf error on y=0");
    nbErrors+=checkValue(norm(ec2z0,0), 0., tol, errors, "Linf error on z=0");
  }

  std::cout<<"U saved, that's all folks"<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
