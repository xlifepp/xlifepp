/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file sys_2d.cpp
  \author E. Lunéville
  \since 14 nov 2013
  \date 14 nov 2013

*/
#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace sys_2d
{

typedef GeomDomain& Domain;

//data on sigma-
Complex gp(const Point& P, Parameters& pa = defaultParameters)
{
  Real k=pa("k");
  return -i_*k;
}
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2), h=pa("h");            //get the parameter h (user definition)
  Int n=pa("basis index")-1;         //get the index of function to compute
  if(n==0) return sqrt(1./h);
  else     return sqrt(2./h)*cos(n*pi_*(y+h*0.5)/h);
}

void sys_2d(int argc, char* argv[], bool check)
{
   String rootname = "sys_2d";
   trace_p->push(rootname);  
   verboseLevel(0);
   String errors;
   Number nbErrors = 0; 
   Real tol=0.00001;
   bool SaveToFile=false;

   numberOfThreads(1);

  //mesh domain
  Strings sidenames("y=0","x=pi","y=h","x=0");
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=pi_,_ymin=-0.5,_ymax=0.5,_nnodes=Numbers(15,5),_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=pi"), sigmaM=mesh2d.domain("x=0");

  // create P2 lagrange interpolation
  Space V(_domain=omega,_interpolation=_P2, _name="V");
  Unknown u(V,"u");  TestFunction v(u,"v");

  // parameters
  Real h=1, k=1;
  Parameters params(h,"h");
  params << Parameter(k,"k");

  // create spectral space and dtn kernel
  Number N=5;
  Space Sp(_domain=sigmaP, _basis=Function(cosny,params), _dim=N, _name="cos(n*pi*y)");
  Unknown phiP(Sp, _name="phiP");
  Vector<Complex> lambda(N);
  for (Number n=0; n<N; n++) lambda[n]=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));
  TensorKernel tkp(phiP,lambda);

  // create bilinear form and linear form
  BilinearForm auv = intg(omega,grad(u)|grad(v)) - k*k*intg(omega,u*v)
                   - i_*intg(sigmaP,sigmaP,u*tkp*v);
  LinearForm fv=intg(sigmaM,Function(gp,params)*v);
  TermMatrix A(auv, _storage=csDual, _name="A");
  TermVector B(fv, _name="B");

  // solve linear system AX=B using LU factorization
  TermMatrix LU; luFactorize(A,LU);
  TermVector U=directSolve(LU,B);
  if (SaveToFile) saveToFile("U",U,_format=_vtk);
  nbErrors+=checkValues("U_Omega.vtk", rootname+"/U_Omega.in", tol, errors, "U", check);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
