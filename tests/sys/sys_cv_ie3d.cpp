/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_cv_ie3d.cpp
	\since 20 jun 2014
	\date 20 jun 2014
	\author Eric Lunéville

	convergence test for various integral equations
	for 3d acoustic diffraction by sphere R=1
    L2 error on sphere R=2
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_cv_ie3d
{
 // t in [0,1] -> (cos(2pi.(t-0.5)) sin(2pi.(t-0.5)))
 Vector<Real> polar(const Point& pt, Parameters& pars, DiffOpType d=_id)
 {
  Real dp=2*pi_, t = (pt[0]-0.5)*dp;
  Real c=std::cos(t), s=std::sin(t);
  switch (d)
  {
    case _id: return Vector<Real>{c,s};
    case _d1: return Vector<Real>{-s*dp, c*dp};
    case _d11: return Vector<Real>{-c*dp*dp,-s*dp*dp};
    default: parfun_error("polar parametrization", d);
  }
  return Vector<Real>();  //dummy return
 }

 Vector<Real> invPolar(const Point& pt, Parameters& pars, DiffOpType d=_id)
 {
  if (d != _id) { parfun_error("invPolar parametrization", d); }
  return Vector<Real>{(std::atan2(pt[1],pt[0])+pi_)/(2*pi_)};
 }

 // incident plane wave 2D
Complex uinc2(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky");
  Real kp=kx*p(1)+ky*p(2);
  return exp(i_*kp);
}

// normal derivative of incident plane wave on disk (-dr)
Complex druinc2(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky");
  Real kp=kx*p(1)+ky*p(2);
  return i_*kp*exp(i_*kp);
}
// incident plane wave 3D
Complex uinc3(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky"), kz=pa("kz");
  Real kp=kx*p(1)+ky*p(2)+kz*p(3);
  return exp(i_*kp);
}

// normal derivative of incident plane wave on sphere (-dr)
Complex druinc3(const Point& p, Parameters& pa = defaultParameters)
{
  Real kx=pa("kx"), ky=pa("ky"), kz=pa("kz");
  Real kp=kx*p(1)+ky*p(2)+kz*p(3);
  return i_*kp*exp(i_*kp);
}

enum IEType { DirichletSL, DirichletDL, NeumannSL, NeumannDL};

String tostring(IEType iet)
{
   switch(iet)
   {
    case DirichletSL: return "DirichletSL";
    case DirichletDL: return "DirichletDL";
    case NeumannSL  : return "NeumannSL";
    case NeumannDL  : return "NeumannDL";
    default : break;
   }
   return "";
}

void sys_cv_ie3d(int argc, char* argv[], bool check)
{
  String rootname = "sys_cv_ie3d";
//#ifdef XLIFEPP_WITH_GMSH
  trace_p->push(rootname);

  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;
  bool save=false;
  numberOfThreads(4);

  //computing parameters
  Number nnmin=4, nnmax=4, nnstep=4; // nb of nodes on on circle/sphere R=1;
  Real hmin=0.1, hmax=0.71, hstep=0.1;// h on sphere
  Number momin=1, momax=5;
  Number omin=1, omax=5;
  Number imo0=30, imo1=20, imo2=16;  //order of quadratures for singular integral
  Number ibo=16;  // order of quadrature for right hand side
  Number iro=16;  // order of quadrature for intg. rep.
  bool readMsh=true;
  bool isoflag=true;   // activate isogeo approximation
  bool omaxflag=false;   // omax = mo if omaxflag
  String geo="disk";

  //define parameters and functions
  Parameters pars;
  Real k=1, k2=k*k;
  pars<<Parameter(k,"k");       // wave number
  pars<<Parameter(k,"kx")<<Parameter(0.,"ky")<<Parameter(0.,"kz");      // x,y,z wave number of incident wave
  pars<<Parameter(1.,"radius");        // sphere/circle radius
  Kernel G;
  IEType typeIE = DirichletSL;
  Function f, scatSol;
  if(geo=="sphere")
  {
    G = Helmholtz3dKernel(pars);  //load Helmholtz3D kernel
    switch(typeIE)
    {
     case DirichletSL : //EFIE
     case DirichletDL :
       f = Function(uinc3,"uinc", pars);
       scatSol = Function(scatteredFieldSphereDirichlet,pars);
       break;
     case NeumannSL :  //MFIE
     case NeumannDL :
       f = Function(druinc3,"druinc",pars);
       scatSol = Function(scatteredFieldSphereNeumann,pars);
    }
  }
  else
  {
    G = Helmholtz2dKernel(pars);   //load Helmholtz2D kernel
    switch(typeIE)
    {
     case DirichletSL : //EFIE
     case DirichletDL :
       f = Function(uinc2,"uinc", pars);
       scatSol = Function(scatteredFieldDiskDirichlet,pars);
       break;
     case NeumannSL :  //MFIE
     case NeumannDL :
       f = Function(druinc2,"druinc",pars);
       scatSol = Function(scatteredFieldDiskNeumann,pars);
    }
  }

  String filename=geo+" "+tostring(typeIE)+" error";

  std::vector<Real> nh;
  Number nbn;
  if(!readMsh)
  {
    nbn=1+(nnmax-nnmin)/nnstep;
    nh.resize(nbn);
    Number nn=nnmin;
    for(Number i=0;i<nbn; i++,nn+=nnstep) nh[i]=nn;
  }
  else
  {
//    nbn=1+(hmax-hmin)/hstep;
//    nh.resize(nbn);
//    Real h=hmin;
//    for(Number i=0;i<nbn; i++,h+=hstep) nh[i]=h;
     nh={0.01,0.03,0.05,0.75,0.1,0.2,0.3,0.4,0.5,0.6,0.7};
     nbn=nh.size();
  }

  Number nbpts=1;
  vector<Point> pts(nbpts);
  if(geo=="sphere") pts[0]=Point(0.,0.,2.);
  else pts[0]=Point(0.,2.);
  Vector<Complex> val(nbpts), sol(nbpts);
  Complex cv;
  for(Number i=0;i<nbpts;i++)
     sol[i]=scatSol(pts[i],cv);

  //loop initialization
  Real tA, tS, tR;
  std::ofstream eout(filename.c_str());

  std::cout<<"Solving Helmholtz on "<<geo<<", using BEM "<< tostring(typeIE)<<", quadratic error at "<<nbpts<<" point(s)"<<eol;

  //loop on mesh size of shpere R=1
  String ext, fnmesh;
  Mesh msp;
  for(Number mo=momin;mo<=momax+1; mo++)  //loop on mesh order
  {
   Number mo_order=mo;
   bool isogeo = false;
   if(isoflag && mo==momax+1)
   {
     isogeo=true;
     mo_order=1;
     thePrintStream<<"=============== mesh order = isogeo ==============="<<eol;
     std::cout<<"mesh order isogeo"<<eol;
     ext="_isogeo";
   }
   else
   {
     thePrintStream<<"=============== mesh order = "<<mo<<" ==============="<<eol;
     std::cout<<"mesh order "<<mo<<eol;
     ext="_mo="+xlifepp::tostring(mo);
   }
   std::ofstream eout((filename+ext+".dat").c_str());  // error file
   Real h;
   for (Number i=0; i<nbn; i++) // loop on h=2pi/nn
   {
    Mesh msp;
    if(!readMsh)
    {
      Number nn=nh[i];
      h=2*pi_/(nn-1);
      thePrintStream<<std::fixed<<"-------------- nn = "<<nn<<" (h="<<h<<")------------"<<eol;
      std::cout<<" nnode "<<nn<<eol;
      if(geo=="sphere")
      {
        Sphere sp(_center=Point(0,0,0),_radius=1,_nnodes=Number(nh[i]),_domain_name="sphere R=1");
        msp=Mesh(sp, _shape=_triangle, _order=mo_order, _generator=_gmsh);
      }
      else
      {
        Circle sp(_center=Point(0,0,0),_radius=1,_nnodes=Number(nh[i]),_domain_name="circle R=1");
        msp=Mesh(sp, _shape=_segment, _order=mo_order, _generator=_gmsh);
      }
    }
    else
    {
      h=nh[i];
      thePrintStream<<std::fixed<<"-------------- (h="<<h<<")------------"<<eol;
      std::cout<<" h "<<h<<eol;
      if(geo=="sphere")
      {
        fnmesh="sphere_o"+xlifepp::tostring(mo_order)+"_l="+xlifepp::tostring(h)+".msh";
        msp=Mesh(fnmesh,_name=fnmesh);
        msp.geometry_p = new EllipsoidSidePart(Sphere(_center=Point(0,0,0),_radius=1,_hsteps=h,_domain_name="sphere R=1"), -pi_,pi_,-pi_/2,pi_/2, _bispherical);
        msp.domain(0).setGeometry(msp.geometry_p);
      }
      else
      {
        fnmesh="disk_o"+xlifepp::tostring(mo_order)+"_l="+xlifepp::tostring(h)+".msh";
        msp=Mesh(fnmesh,_name=fnmesh);
        Parametrization parCircle(0., 1., polar, invPolar,Parameters(), "polar");
        parCircle.periods={1.};
        msp.geometry_p = new ParametrizedArc(_parametrization = parCircle, _partitioning=_linearPartition, _nbParts=100,_hsteps=0.1,_domain_name="Circle R=1");
        msp.domain(0).setGeometry(msp.geometry_p);
      }
    }
    ext+="_h="+xlifepp::tostring(h);
    tA=elapsedTime();
    Domain omega = msp.domain(0);
    if(isogeo)
    {
        omega.meshDomain()->buildIsoNodes();
        omega.meshDomain()->exportIsoNodes("isoNodes",10);
    }
    if(omaxflag) omax=mo;
    for(Number order = omin; order<= omax; order++) // loop on FE order
    {
      std::cout<<"   FE order "<<order<<std::flush;
      ext+="_P="+xlifepp::tostring(order);
      Space V(_domain=omega, _FE_type=Lagrange, _order=order, _name="V", _notOptimizeNumbering);
      Unknown u(V, _name="u"); TestFunction v(u, _name="v");
      thePrintStream<<std::setprecision(3)<<"    FE order "<<order<<" ("<<std::setw(6)<<V.dimSpace()<<") : " ;
      //solve IE
      IntegrationMethods ssim;
      if(geo=="sphere") ssim=IntegrationMethods(_method=_SauterSchwabIM,_order=imo0,_bound=0.,_method=_defaultRule,_order=imo1, _bound=2.,_method=_defaultRule,_order=imo2);
      else ssim=IntegrationMethods(_method=Duffy, _order = imo0, _bound=0., _method=_defaultRule, _order=imo1, _bound=2.,_method=_defaultRule,_order=imo2);
      BilinearForm bf;
      LinearForm fv;
      if(isogeo) fv= -intg(omega,f*v,_quad=_GaussLegendreRule,_order=2*ibo,_isogeo);
      else fv= -intg(omega,f*v,_quad=_GaussLegendreRule,_order=ibo);
      switch (typeIE)  //define IE
      {
       case DirichletSL:
           if(isogeo) bf = intg(omega,omega,u*G*v,_method=ssim,_isogeo);
           else bf = intg(omega,omega,u*G*v,_method=ssim);
           break;
       case DirichletDL:
           if(isogeo) bf = intg(omega,omega,u*(grad_x(G)|_nx)*v,_method=ssim,_isogeo) + 0.5*intg(omega,u*v,_quad=_defaultRule,_order=iro,_isogeo);
           else bf = intg(omega,omega,u*(grad_x(G)|_nx)*v,_method=ssim) + 0.5*intg(omega,u*v,_quad=_defaultRule,_order=iro);
           break;
       case NeumannSL:
           if(isogeo) bf = intg(omega,omega,u*(grad_y(G)|_ny)*v,_method=ssim,_isogeo) - 0.5*intg(omega,u*v, _quad=_defaultRule,_order=iro,_isogeo);
           else bf = intg(omega,omega,u*(grad_y(G)|_ny)*v,_method=ssim) - 0.5*intg(omega,u*v, _quad=_defaultRule,_order=iro);
           break;
       case NeumannDL:
           if(isogeo) bf = k2*intg(omega,omega,u*(G*(_nx|_ny))*v,_method=ssim,_isogeo) - intg(omega,omega,(ncrossgrad(u)*G)|ncrossgrad(v),_method=ssim,_isogeo);
           else bf = k2*intg(omega,omega,u*(G*(_nx|_ny))*v,_method=ssim) - intg(omega,omega,(ncrossgrad(u)*G)|ncrossgrad(v),_method=ssim);
      }
      tA=elapsedTime();
      TermMatrix A(bf, _name="A");
      TermVector B(fv, _name="B");
      tA=elapsedTime();
//      thePrintStream<<std::scientific<<A<<eol;
//      thePrintStream<<B<<eol;
      thePrintStream<<" assembly t="<<std::fixed<<std::setw(6)<<tA;
      std::cout<<" assembly t="<<std::fixed<<tA<<std::flush;
      TermVector U = directSolve(A, B);
      tS=elapsedTime();
      thePrintStream<<" solving t="<<std::setw(8)<<tS;
      std::cout<<" solving t="<<tS<<std::flush;
      //thePrintStream<<U<<eol;
      if (save) saveToFile("U"+ext+".vtu",U);
      //integral representation using interpolation method
//      TermMatrix Gi;
//      switch (typeIE)  //define IE
//      {
//       case DirichletSL:
//       case NeumannSL: Gi=TermMatrix(u2, omega2, u, omega, G, _name="Gi");  break;
//       case DirichletDL:
//       case NeumannDL: Gi=TermMatrix(u2, omega2, u, omega, grad_y(G)|_ny, _name="dnGi");
//      }
//      TermMatrix M1(intg(omega,u*v), _name="M1");
//      TermVector U2 = Gi*(M1*U);
//      tR=cpuTime();
//      std::cout<<" rep. intg. t="<<tR;
//      TermVector ec2=U2-sol2;
//      Real err=sqrt(abs((M2*ec2) | ec2));
//      eout<<V.dimSpace()<<" "<<h<<" "<<err<<" "<<tA<<" "<<tS<<" "<<tR<<eol;
//      std::cout<<" t = "<<tA+tS+tR<<" L2 error =  "<<err<<eol;
//      if (save) saveToFile("U2.vtk",U2,_format=_vtk);
//      if (save) saveToFile("sol2.vtk",sol2,_format=_vtk);

      //error on some points and compute mean of quadratic errors
      LinearForm irf;
      switch (typeIE)  //define IE
      {
       case DirichletSL:
       case NeumannSL:
           if(isogeo) irf=intg(omega,G*u, _quad=_defaultRule,_order=iro,_isogeo);
           else  irf=intg(omega,G*u, _quad=_defaultRule,_order=iro);
           break;
       case DirichletDL:
       case NeumannDL:
           if(isogeo) irf=intg(omega,ndotgrad_y(G)*u, _quad=_defaultRule,_order=iro,_isogeo);
           else irf=intg(omega,ndotgrad_y(G)*u, _quad=_defaultRule,_order=iro);
      }
      integralRepresentation(pts,irf,U,val);
      tR=elapsedTime();
      thePrintStream<<" rep. intg. t="<<std::setw(8)<<tR;
      std::cout<<" rep. intg. t="<<tR<<std::flush<<eol;
      Real err=0.;
      for(Number i=0;i<nbpts;i++)
        err+=abs(val[i]-sol[i]);
      err/=nbpts;
      eout<<V.dimSpace()<<" "<<h<<" "<<err<<" "<<tA<<" "<<tS<<" "<<tR<<eol;
      thePrintStream<<" t="<<std::setw(8)<<tA+tS+tR<<std::scientific<<" error =  "<<err<<eol;

    } //end loop order
   } //end loop h
    eout.close();
  } //end loop mo




  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
