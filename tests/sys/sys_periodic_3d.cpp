
/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file sys_periodic_3d.cpp
  \author E. Lunéville
	\since 3 dec 2014
	\date 3 dec 2014

	Test periodic condition in 3D (parallepiped)
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_periodic_3d {

Complex g(const Point& p, Parameters& pa = defaultParameters)
{
  Point dd(5.,10.,5.);dd-=p;
  Complex res=0.;
  if(abs(p(2)-10.)<theTolerance)
  {
    res=-exp(i_*dd(3));
  }
  return res;
}

Vector<Real> mapxPM(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> Q(P);
  Q(1)+=10.;
  return Q;
}

Vector<Real> mapyPM(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> Q(P);
  Q(2)+=10.;
  return Q;
}

void sys_periodic_3d(int argc, char* argv[], bool check)
{
  String rootname = "sys_periodic_3d";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(1);
  String errors;
  Number nbErrors = 0;
  Real tol=0.0001;
  bool save=false;
  if(check) save=false;

  //mesh parallelipiped
  Strings sn(6);
  sn[0]="zm";sn[1]="zp";sn[2]="ym";sn[3]="yp";sn[4]="xm";sn[5]="xp";
  Number npa=5;

  Mesh m1;
  if (!check)
  {
     Cuboid cu(_xmin=0.,_xmax=10.,_ymin=0.,_ymax=10.,_zmin=0.,_zmax=10.,_nnodes=npa,_side_names=sn);
     m1=Mesh(cu, _shape=_tetrahedron, _order=1, _generator=_gmsh);
     if (save) m1.saveToFile("m1",_vtk);
     m1.saveToFile(inputsPathTo(rootname+"/m1.msh"),_msh);
  }
  else
  {
    m1=Mesh(inputsPathTo(rootname+"/m1.msh"), _name="Parallelepipede(tetrahedron)");
  }
  Domain gxm = m1.domain(1), gxp = m1.domain(2), gym = m1.domain(3);
  Domain gyp = m1.domain(4), gzm = m1.domain(5), gzp = m1.domain(6);
  Domain cube= merge(gxm,gxp,gym,gyp,gzm,gzp,"cube");
  Space V1(_domain=cube, _interpolation=_P1, _name="V1");
  Unknown q(V1, _name="q"); TestFunction qt(q, _name="qt");

  //define parameters and functions
  Parameters pars;
  pars<<Parameter(1.23,"k");       // wave number
  Kernel G = Helmholtz3dKernel(pars);  //load Helmholtz3D kernel
  Function ginc(g,pars);

  //Sauter-Schwab integration method
  Number order=3;
  IntegrationMethods ssim(_method=_SauterSchwabIM, _order=order, _quad=_defaultRule, _order=4, _bound=2., _quad=_defaultRule, _order=3);

  //define periodic conditions
  defineMap(gxm,gxp,mapxPM);
  defineMap(gym,gyp,mapyPM);
  EssentialConditions ecs=((q|gxm)-(q|gxp)=0) & ((q|gym)-(q|gyp)=0);

  //define forms and terms
  BilinearForm bf0=intg(cube,cube,q*G*qt,_method=ssim);
  LinearForm fv0 = intg(cube,ginc*qt);
  TermMatrix A0(bf0, ecs, _name="A0");
  TermVector B0(fv0, _name="B0");
  TermVector Q1=directSolve(A0,B0);

  TermMatrix M11(intg(cube,q*qt), _name="M11");
  Real lz=7;
  Number NPlan=100;

  Mesh mz0;
  if (!check)
  {
    Rectangle recz(_v1=Point(0.1,0.1,lz),_v2=Point(9.9,0.1,lz),_v4=Point(0.1,9.9,lz),_nnodes=NPlan,_domain_name="z7");
    mz0=Mesh(recz, _shape=_triangle, _order=1, _generator=_gmsh);
    if(save) mz0.saveToFile("mz0",_vtk);
    mz0.saveToFile(inputsPathTo(rootname+"/mz0.msh"),_msh);
  }
  else
  {
    mz0=Mesh(inputsPathTo(rootname+"/mz0.msh"), _name="Parallelepipede(tetrahedron)");
  }

  Domain planz0 = mz0.domain(0);
  Space Wz(_domain=planz0, _interpolation=_P1, _name="Wz");
  Unknown wz(Wz, _name="wz");
  TermMatrix Rz(wz, planz0, q, cube, G, "R");
  TermVector U1dz7=Rz*(M11*Q1);
  if(save) saveToFile("u1",U1dz7,_format=_vtu);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  trace_p->pop();
  
}

}
