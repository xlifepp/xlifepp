/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_BEM_Laplace_Helmholtz_Dirichlet.cpp
\author N. Salles
\since 18 November 2015
\date 18 November 2015
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace sys_BEM_Laplace_Helmholtz_Dirichlet
{

Real laplace_ref;
Real k;
Complex helmholtz_ref;
vector<Point> evalPoints;
Real maxrelerr;


Real g1(const Point& M, Parameters& pa = defaultParameters)
{
  Point x0(0.,0.,0.);
  return over4pi_/M.distance(x0);
}

Complex g2(const Point& M, Parameters& pa = defaultParameters)
{
  Point x0(0.,0.,0.);
  Real r=M.distance(x0);
  Complex ikr=r*i_*k;
  return over4pi_*exp(ikr)/r;
}

bool VerifCriterion(Real relative_error)
{
  return relative_error<maxrelerr;
}

template<typename K>
Real computeRelErrLaplace(BilinearForm& bf, TermVector& rhs,LinearForm& lf, Vector<K>& val)
{
  TermMatrix LaplaceTM(bf, _name="bf");
  TermVector sol=gmresSolve(LaplaceTM, rhs, _tolerance=1.0e-6, _maxIt=500);
  clear(LaplaceTM);
  integralRepresentation(evalPoints,lf,sol,val);
  return std::abs((val[0]-laplace_ref)/laplace_ref);
}

template<typename K>
Real computeRelErrDirect(BilinearForm& bf, BilinearForm& bfrhs, LinearForm& lf1, LinearForm& lf2, Vector<K>& val, TermVector& tg, K ref)
{
  TermMatrix lhs(bf, _name="bf");
  TermMatrix I2pD(bfrhs, _name="bfrhs");
  TermVector rhs=I2pD*tg;
  TermVector sol=gmresSolve(lhs, rhs, _tolerance=1.0e-6, _maxIt=500);
  integralRepresentation(evalPoints,lf1,sol,val);
  Vector<K> val2;
  integralRepresentation(evalPoints,lf2,tg,val2);
  val-=val2;
  return std::abs((val[0]-ref)/ref);
}

Real computeRelErrHelmholtz(BilinearForm& bf, TermVector& rhs,LinearForm& lf, Vector<Complex>& val)
{
  TermMatrix HelmholtzTM(bf, _name="bf");
  TermVector sol=gmresSolve(HelmholtzTM, rhs, _tolerance=1.0e-6);
  clear(HelmholtzTM);
  integralRepresentation(evalPoints,lf,sol,val);
  return std::abs((val[0]-helmholtz_ref)/helmholtz_ref);
}

void sys_BEM_Laplace_Helmholtz_Dirichlet(int argc, char* argv[], bool check)
{
  String rootname = "sys_BEM_Laplace_Helmholtz_Dirichlet";
  trace_p->push(rootname);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;

  bool check2;
  k=1.35;
  helmholtz_ref=k*over4pi_*std::exp(i_*k);
  laplace_ref=over4pi_;
  evalPoints.resize(1);
  maxrelerr=0.05;

  Number nmesh=9;
  Mesh m1;
  if (!check)
  {
    Ball sp(_center=Point(0.,0.,0.),_radius=1.,_nboctants=8,_nnodes=nmesh);
    m1=Mesh(sp, _shape=_triangle, _order=1, _generator=_subdiv);
    m1.saveToFile(inputsPathTo(rootname+"/m1.msh"), msh);
  }
  else
  {
  	m1=Mesh(inputsPathTo(rootname+"/m1.msh"), _name="Unity Ball");
  }
  Domain sphere = m1.domain(0);

  // Define spaces
  Space V0(_domain=sphere, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
  thePrintStream << "Number of Dofs in P0 = " << V0.nbDofs() << eol;
  Unknown u0(V0, _name="u0");
  TestFunction v0(u0, _name="v0");
  Space V1(_domain=sphere, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  thePrintStream << "Number of Dofs in P1 = " << V1.nbDofs() << eol;
  Unknown u1(V1, _name="u1");
  TestFunction v1(u1, _name="v1");

  // Define IntegrationMethod Sauter-Schwab
  Number order=5;
  IntegrationMethods ims(_method=_SauterSchwabIM, _order=order, _bound=0.,
                         _quad=_defaultRule, _order=4, _bound=2.,
                         _quad=_defaultRule, _order=3);

  // Define variables and Kernels
  Kernel Gl=Laplace3dKernel();
  evalPoints[0]=Point(0.,0.,0.);
  Vector<Real> val;
  Vector<Complex> valc;
  Real laplace_ref = over4pi_;
  Real rel_err=0.;

  //====================================
  // Test LAPLACE P0
  //====================================
  thePrintStream << "Tests LAPLACE P0 started" << eol;
  Function laplace_bc(g1);

  LinearForm laplace_fv = intg(sphere,laplace_bc*v0);
  TermVector laplace_rhs(laplace_fv);

  // Single Layer potential
  BilinearForm bfLapDirSL=intg(sphere,sphere,u0*Gl*v0,_method=ims);
  LinearForm lfLapDirSL(intg(sphere,Gl*u0,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrLaplace(bfLapDirSL,laplace_rhs,lfLapDirSL,val);
  thePrintStream << "Laplace Dirichlet P0 SL, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Laplace Dirichlet P0 SL";
  }

  // Double layer potential
  BilinearForm bfLapDirDL=-0.5*intg(sphere,u0*v0)+intg(sphere,sphere,u0*ndotgrad_y(Gl)*v0,_method=ims);
  LinearForm lfLapDirDL(intg(sphere,ndotgrad_y(Gl)*u0,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrLaplace(bfLapDirDL,laplace_rhs,lfLapDirDL,val);
  thePrintStream << "Laplace Dirichlet P0 DL, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Laplace Dirichlet P0 DL";
  }

  thePrintStream << "Tests LAPLACE P0 finished" << eol;
  clear(laplace_rhs);

  //====================================
  // Test LAPLACE P1
  //====================================

  TermVector laplace_rhsP1(intg(sphere,laplace_bc*v1));
  // Single Layer potential
  BilinearForm bfLapDirSLP1=intg(sphere,sphere,u1*Gl*v1,_method=ims);
  LinearForm lfLapDirSLP1(intg(sphere,Gl*u1,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrLaplace(bfLapDirSLP1,laplace_rhsP1,lfLapDirSLP1,val);
  thePrintStream << "Laplace Dirichlet P1 SL, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Laplace Dirichlet P1 SL";
  }

  // Double layer potential
  BilinearForm bfLapDirDLP1=-0.5*intg(sphere,u1*v1)+intg(sphere,sphere,u1*ndotgrad_y(Gl)*v1,_method=ims);
  LinearForm lfLapDirDLP1(intg(sphere,ndotgrad_y(Gl)*u1,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrLaplace(bfLapDirDLP1,laplace_rhsP1,lfLapDirDLP1,val);
  thePrintStream << "Laplace Dirichlet P1 DL, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Laplace Dirichlet P1 DL";
  }

  // Direct formulation
  BilinearForm bfLapDirDLP1rhs=0.5*intg(sphere,u1*v1)+intg(sphere,sphere,u1*ndotgrad_y(Gl)*v1,_method=ims);
  TermVector tg_lap(u1,sphere,g1);
  rel_err=computeRelErrDirect(bfLapDirSLP1, bfLapDirDLP1rhs, lfLapDirSLP1,lfLapDirDLP1, val, tg_lap,laplace_ref);
  thePrintStream << "Laplce Dirichlet P1 Direct, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Laplace Dirichlet P1 Direct";
  }

  //====================================
  // Test HELMHOLTZ P0
  //====================================
  Kernel Gh=Helmholtz3dKernel(k);

  // Test Helmholtz  P0
  thePrintStream << "Tests Helmholtz P0 started" << eol;
  Function helmholtz_bc(g2);
  LinearForm helmholtz_fv = intg(sphere,helmholtz_bc*v0);
  TermVector helmholtz_rhs(helmholtz_fv);

  // Single Layer potential
  BilinearForm bfHelDirSL=intg(sphere,sphere,u0*Gh*v0,_method=ims);
  LinearForm lfHelDirSL(intg(sphere,Gh*u0,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrHelmholtz(bfHelDirSL,helmholtz_rhs,lfHelDirSL,valc);
  thePrintStream << "Helmholtz Dirichlet P0 SL, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Helmoltz Dirichlet P0 SL";
  }

  // Double layer potential
  BilinearForm bfHelDirDL=-0.5*intg(sphere,u0*v0)+intg(sphere,sphere,u0*ndotgrad_y(Gh)*v0,_method=ims);
  LinearForm lfHelDirDL(intg(sphere,ndotgrad_y(Gh)*u0,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrHelmholtz(bfHelDirDL,helmholtz_rhs,lfHelDirDL,valc);
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Helmoltz Dirichlet P0 DL";
  }

  //====================================
  // Test HELMHOLTZ P1
  //====================================
  thePrintStream << "Tests Helmholtz P1 started" << eol;
  LinearForm helmholtz_fvP1 = intg(sphere,helmholtz_bc*v1);
  TermVector helmholtz_rhsP1(helmholtz_fvP1);

  // Single Layer potential

  BilinearForm bfHelDirSLP1=intg(sphere,sphere,u1*Gh*v1,_method=ims);
  LinearForm lfHelDirSLP1(intg(sphere,Gh*u1,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrHelmholtz(bfHelDirSLP1,helmholtz_rhsP1,lfHelDirSLP1,valc);
  thePrintStream << "Helmholtz Dirichlet P1 SL, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Helmoltz Dirichlet P1 SL";
  }

  // Double layer potential
  BilinearForm bfHelDirDLP1=-0.5*intg(sphere,u1*v1)+intg(sphere,sphere,u1*ndotgrad_y(Gh)*v1,_method=ims);
  LinearForm lfHelDirDLP1(intg(sphere,ndotgrad_y(Gh)*u1,_quad=_symmetricalGaussRule,_order=6));
  rel_err=computeRelErrHelmholtz(bfHelDirDLP1,helmholtz_rhsP1,lfHelDirDLP1,valc);
  thePrintStream << "Helmholtz Dirichlet P1 DL, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Helmoltz Dirichlet P1 DL";
  }

  // Direct formulation
  BilinearForm bfHelDirDLP1rhs=0.5*intg(sphere,u1*v1)+intg(sphere,sphere,u1*ndotgrad_y(Gh)*v1,_method=ims);
  TermVector tg_helm(u1,sphere,g2);
  rel_err=computeRelErrDirect(bfHelDirSLP1, bfHelDirDLP1rhs, lfHelDirSLP1,lfHelDirDLP1, valc, tg_helm,helmholtz_ref);
  thePrintStream << "Helmholtz Dirichlet P1 Direct, rel err = " << rel_err << eol;
  check2=VerifCriterion(rel_err);
  if (!check2)
  {
    thePrintStream << "No convergence" << std::endl;
    nbErrors++;
    errors+="No convergence Helmoltz Dirichlet P1 Direct";
  }

  thePrintStream << "Test BEM finished" << eol;
  clear(helmholtz_rhsP1);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
