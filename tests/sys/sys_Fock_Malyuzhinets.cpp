/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file sys_Fock_Malyuzhinets.cpp
\author E. Lunéville
\since  december 2019
\date  january 2020
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"
#include "../../src/mathsResources/specialFunctions/MalyuzhinetsFunction.hpp"


using namespace std;
using namespace xlifepp;


namespace sys_Fock_Malyuzhinets
{

Real airyR (const Real& x){return real(airy(x));}
Real airyRp(const Real& x){return real(airy(x,_dt));}
Real airyRpp(const Real& x){return x*real(airy(x));}

String computeWedge(const std::vector<Real>& krs, const std::vector<Real>& phis, Real Phi, Real phi0,
                  const String& meth, bool current, FieldPart fp, Real kra,
                  EcType ect, const Complex& cf = 0.)
{
   std::ofstream of; 
   std::vector<Complex> res;
   String name="wedge";
   String nom="wedge";
   if(ect==_NeumannEc) name+="_Neumann";
   if(ect==_DirichletEc) name+="_Dirichlet";
   if(ect==_FourierEc) name+="_Fourier";
   name+="_Phi="+tostring(round(Phi*180/pi_));
   name+="_phi0="+tostring(round(phi0*180/pi_));
   if(fp==_totalField) name+="_total_field";
   if(fp==_diffractedField) name+="_diffracted_field";
   if(fp==_wedgeField) name+="_wedge_field";
   if(meth=="SOM") name+="_SOM";  else name+="_SD";
   elapsedTime();
   if(meth=="SOM") res=wedgeDiffractionSOM(krs,phis,Phi,phi0,fp,ect,ect,cf,cf);
   else res=wedgeDiffractionSD(krs,phis,Phi,phi0,fp,kra,ect,ect,cf,cf);
   elapsedTime(name);
   nom=name;
   name+=".dat";
   of.open(name.c_str());
   std::vector<Complex>::iterator itr=res.begin();
   std::vector<Real>::const_iterator itkr;
   std::vector<Real>::const_iterator itp=phis.begin();
   for(;itp!=phis.end();++itp)
      for(itkr=krs.begin();itkr!=krs.end();++itkr, ++itr)
         of<<*itp<<" "<<*itkr<<" "<<real(*itr)<<" "<<imag(*itr)<<eol;
   of.close();
   return nom;
}

void sys_Fock_Malyuzhinets(int argc, char* argv[], bool check)
{
  String rootname = "sys_Fock_Malyuzhinets";
  trace_p->push(rootname);
  //numberOfThreads(1);
  verboseLevel(0);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;
  std::vector<String> nomFic(19);

  std::ofstream of;
  String fn;
  bool save=true;

  String rn="";
  if (save)
  {
    rn=rootname;
  }

  //test Fock function
  Fock fock(0,10,200,0,-2.55,1);
  Real dx=0.01;
  Complex e4=exp(i_*pi_/4);
  for(Real a=0; a<100; a+=100)
    {
      fock.q = a*0.1*e4;
      fock.q=-i_*0.759842;
      fock.initFilonMP(-100,10,1000,100, true);
      fock.initFilon(-15.,5,200,true);
      fock.initResidue(10,true);
      fn=rn+"fock_q"+tostring(a)+".dat";
      of.open(fn.c_str());
      for(Real x=-4.; x<=4.; x+=dx)
        {
          Complex fa=fock(x), ff=fock(x,_filonCal), fs=2*std::exp(i_*x*x*x/3)*x/(x-i_*fock.q);
          of<<x<<" "<<real(fa)<<" "<<imag(fa)<<" "<<real(ff)<<" "<<imag(ff)<<" "<<real(fs)<<" "<<imag(fs)<<eol;
        }
      of.close();
      fn=rn+"integrand_"+tostring(a)+".dat";
      of.open(fn.c_str());
      for(Real x=-100.; x<=10.; x+=dx)
      {
         Complex g=j_s(x, 0, fock.q);
         of<<x<<" "<<real(g)<<" "<<imag(g)<<eol;
      }
      of.close();
      thePrintStream<<"poles="<<fock.poles<<eol;
      thePrintStream<<"vpoles="<<fock.vpoles<<eol;
    }
  dx=0.001;
  fock.q = e4;
  elapsedTime();
  fock.initResidue(2,true);
  fock.initFilon(-15.,5,200,true);
  for(Real x=-5.; x<=5.; x+=dx) fock(x);
  elapsedTime("approximation");
  fock.initFilonMP(-100,10,1000,100, true);
  for(Real x=-5.; x<=5.; x+=dx) fock(x,_filonCal);
  elapsedTime("Filon quadrature");


  fock.createTable(-5,5,1000);
  fock.saveTable(rn+"Fock_table_5.dat");
  Fock fock2(rn+"Fock_table_5.dat");
  dx=0.003;
  of.open(rn+"fock2.dat");
  for(Real x=-3.; x<=3.; x+=dx)
  {
      Complex f=fock(x,_interpCal);
      of<<x<<" "<<real(f)<<" "<<imag(f)<<eol;
  }
  of.close();


 //test wedge diffraction using Malyuzhinets approach
   fn="Malyuzhinets_wa=120_ng=2.dat";
   of.open(fn.c_str());
   Malyuzhinets mal(2*pi_/3,2,_defaultCal);
   dx=0.001;
   std::vector<Complex> ms(1+ceil(8*pi_/dx));
   Number i=0;
   elapsedTime();
   for(Real x=0; x<8*pi_; x+=dx)
   {
       of<<x<<" "<<real(mal(x))<<eol;
   }
   elapsedTime("malyuzinets");
   of.close();

   Real Phi=3*pi_/4, phi0=5*pi_/8;
   Real kra=10000;
   std::vector<Real> krs=linSpace(0.,10.,100);
   std::vector<Real> phis=linSpace(-Phi,Phi,100);

   // TEST NEUMANN WEDGE with SD PATH
   nomFic[1]=computeWedge(krs,phis,Phi,phi0,"SD",false,_totalField,kra,_NeumannEc);
   nomFic[2]=computeWedge(krs,phis,Phi,phi0,"SD",false,_diffractedField,kra,_NeumannEc);
   nomFic[3]=computeWedge(krs,phis,Phi,phi0,"SD",false,_wedgeField,kra,_NeumannEc);

   // TEST DIRICHLET WEDGE with SD PATH
   nomFic[4]=computeWedge(krs,phis,Phi,phi0,"SD",false,_totalField,kra,_DirichletEc);
   nomFic[5]=computeWedge(krs,phis,Phi,phi0,"SD",false,_diffractedField,kra,_DirichletEc);
   nomFic[6]=computeWedge(krs,phis,Phi,phi0,"SD",false,_wedgeField,kra,_DirichletEc);

   // TEST FOURIER WEDGE with SD PATH
   Complex cf=(1-i_/100)*pi_/2;
   nomFic[7]=computeWedge(krs,phis,Phi,phi0,"SD",false,_totalField,kra,_FourierEc,cf);
   nomFic[8]=computeWedge(krs,phis,Phi,phi0,"SD",false,_diffractedField,kra,_FourierEc,cf);
   nomFic[9]=computeWedge(krs,phis,Phi,phi0,"SD",false,_wedgeField,kra,_FourierEc,cf);

   // TEST NEUMANN WEDGE with SOM PATH
   nomFic[10]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_totalField,kra,_NeumannEc);
   nomFic[11]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_diffractedField,kra,_NeumannEc);
   nomFic[12]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_wedgeField,kra,_NeumannEc);

   // TEST DIRICHLET WEDGE with SOM PATH
   nomFic[13]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_totalField,kra,_DirichletEc);
   nomFic[14]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_diffractedField,kra,_DirichletEc);
   nomFic[15]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_wedgeField,kra,_DirichletEc);

   // TEST FOURIER WEDGE with SOM PATH
   nomFic[16]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_totalField,kra,_FourierEc,cf);
   nomFic[17]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_diffractedField,kra,_FourierEc,cf);
   nomFic[18]=computeWedge(krs,phis,Phi,phi0,"SOM",false,_wedgeField,kra,_FourierEc,cf);

   for (int i=1; i<19; i++)
   { 
      nbErrors+=checkValues(nomFic[i]+".dat", rootname+"/"+nomFic[i]+".in", tol,errors, "*", check);
   }
  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
 if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

} // end of namespace dev_other
