/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file testUtils.hpp
  \authors E. Lunéville, C. Chambeyron
  \since 3 dec 2011
  \date 20 may 2021

  Utilities to manage tests.

*/
#ifndef TEST_UTILS_HPP
#define TEST_UTILS_HPP

// #include "config.h"
#include "xlife++-libs.h"

namespace xlifepp
{

String resPathTo(const String& basename);                          //!< return full path to filename in data directory
String inputsPathTo(const String& basename, bool checkExist=false); //!< return full path to filename in inputs directory
String diffResFile(std::stringstream &R, const String& N);         //!< compares result R to the file N.res containing the reference result
String saveResToFile(std::stringstream& R, const String& N);       //!< saves result R to the file N.res
String& normalizeResData(String& data);                            //!< normalize output for res files
void testClean();                                                      //!< clean all global lists of objects
//! utility for printing on the standard output and also on another stream
void printTestLabel(CoutStream& out, const String& label);
//! utility for printing on the standard output and also on another stream, and with a numbering
void printTestLabel(CoutStream& out, Number index, const String& label);

//@{
//! print utilities
void print_Dom(std::ostream & out, const GeomDomain& dom);
inline void print_Dom(CoutStream& cs, const GeomDomain& dom)
{ return print_Dom(*(cs.stringStream), dom); }
void print_Composite(std::ostream& out, const GeomDomain& dom);
inline void print_Composite(CoutStream& cs, const GeomDomain& dom)
{ return print_Composite(*(cs.stringStream), dom); }
void print_Domain(std::ostream& out, const GeomDomain& dom); 
inline void print_Domain(CoutStream& cs, const GeomDomain& dom)
{ return print_Domain(*(cs.stringStream), dom); }
void printTheDomains(std::ostream& out , std::vector<const GeomDomain*> doms);
inline void printTheDomains(CoutStream& cs , std::vector<const GeomDomain*> doms)
{ return printTheDomains(*(cs.stringStream), doms); }
void printCompDirectIter(String& YN, const String Nom, const TermMatrix& MAT, const TermVector& B, const TermVector& X, const TermVector& UD, Real epsilon, Vector<String>&, Real et);
//@}

//! round to "0" the string CVar  
void roundToZero(const String& Cvar, String& ligneI);

//@{
//! test a single integer value without tolerance
Number checkValue(Int SI, Int SR, String& errorMsg, const String& info);
inline Number checkValue(Int SI, Int SR, String& errorMsg, const char* info)
{ return checkValue(SI, SR, errorMsg, String(info)); }
//@}

//@{
//! test a single size_t value without tolerance
Number checkValue(Number SI, Number SR, String& errorMsg, const String& info);
inline Number checkValue(Number SI, Number SR, String& errorMsg, const char* info)
{ return checkValue(SI, SR, errorMsg, String(info)); }
//@}

//@{
//! test a single boolean value without tolerance
Number checkValue(bool SI, bool SR, String& errorMsg, const String& info);
inline Number checkValue(bool SI, bool SR, String& errorMsg, const char* info)
{ return checkValue(SI, SR, errorMsg, String(info)); }
//@}

//@{
//! test a single real value with a tolerance tol
Number checkValue(Real SI, Real SR, Real tol, String& errorMsg, const String& info);
inline Number checkValue(Real SI, Real SR, Real tol, String& errorMsg, const char* info)
{ return checkValue(SI, SR, tol, errorMsg, String(info)); }
//@}

//@{
//! test a single real value without tolerance
inline Number checkValue(Real SI, Real SR, String& errorMsg, const String& info)
{ return checkValue(SI,  SR, 0., errorMsg, info); }
inline Number checkValue(Real SI, Real SR, String& errorMsg, const char* info)
{ return checkValue(SI,  SR, 0., errorMsg, String(info)); }
//@}

//@{
//! test a single comlex value with a tolerance tol
Number checkValue(Complex SI, Complex SR, Real tol, String& errorMsg, const String& info);
inline Number checkValue(Complex SI, Complex SR, Real tol, String& errorMsg, const char* info)
{ return checkValue(SI, SR, tol, errorMsg, String(info)); }
//@}

//@{
//! test a single complex value without tolerance
inline Number checkValue(Complex SI, Complex SR, String& errorMsg, const String& info)
{ return checkValue(SI, SR, 0., errorMsg, info); }
inline Number checkValue(Complex SI, Complex SR, String& errorMsg, const char* info)
{ return checkValue(SI, SR, 0., errorMsg, String(info)); }
//@}

//@{
//! test the value of a parameter or a list of parameters
Number checkValue(Parameter SI, Int SR, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, Int SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, Int SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, Int SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, String(nom), errorMsg, String(info)); }

inline Number checkValue(Parameter SI, int SR, const String& nom, String& errorMsg, const String& info)
{ return checkValue(SI, Int(SR), nom, errorMsg, info); }
inline Number checkValue(Parameter SI, int SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, Int(SR), nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, int SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, Int(SR), String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, int SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, Int(SR), String(nom), errorMsg, String(info)); }

Number checkValue(Parameter SI, Real SR, Real tol, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, Real SR, Real tol, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, tol, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, Real SR, Real tol, const char*& nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, tol, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, Real SR, Real tol, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, tol, String(nom), errorMsg, String(info)); }

Number checkValue(Parameter SI, Complex SR, Real tol, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, Complex SR, Real tol, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, tol, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, Complex SR, Real tol, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, tol, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, Complex SR, Real tol, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, tol, String(nom), errorMsg, String(info)); }

Number checkValue(Parameter SI, const String& SR, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, const String& SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, const String& SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, const String& SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, nom, errorMsg, String(info)); }

inline Number checkValue(Parameter SI, const char* SR, const String& nom, String& errorMsg, const String& info)
{ return checkValue(SI, String(SR), nom, errorMsg, info); }
inline Number checkValue(Parameter SI, const char* SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, String(SR), nom, errorMsg, info); }
inline Number checkValue(Parameter SI, const char* SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, String(SR), nom, errorMsg, info); }
inline Number checkValue(Parameter SI, const char* SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, String(SR), nom, errorMsg, info); }

Number checkValue(Parameter SI, const String& fname, Real tol, const String& nom, String& errorMsg, const String& info, bool check);
inline Number checkValue(Parameter SI, const String& fname, Real tol, const String& nom, String& errorMsg, const char* info, bool check)
{ return checkValue(SI, fname, tol, nom, errorMsg, String(info), check); }
inline Number checkValue(Parameter SI, const String& fname, Real tol, const char* nom, String& errorMsg, const String& info, bool check)
{ return checkValue(SI, fname, tol, String(nom), errorMsg, info, check); }
inline Number checkValue(Parameter SI, const String& fname, Real tol, const char* nom, String& errorMsg, const char* info, bool check)
{ return checkValue(SI, fname, tol, String(nom), errorMsg, String(info), check); }

Number checkValue(Parameter SI, bool SR, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, bool SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, bool SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, bool SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, String(nom), errorMsg, String(info)); }

Number checkValue(Parameter SI, std::vector<String> SR, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, std::vector<String> SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, std::vector<String> SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, std::vector<String> SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, String(nom), errorMsg, String(info)); }

Number checkValue(Parameter SI, std::vector<Int> SR, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, std::vector<Int> SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, std::vector<Int> SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, std::vector<Int> SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, String(nom), errorMsg, String(info)); }

Number checkValue(Parameter SI, std::vector<Real> SR, const String& nom, String& errorMsg, const String& info);
inline Number checkValue(Parameter SI, std::vector<Real> SR, const String& nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, nom, errorMsg, String(info)); }
inline Number checkValue(Parameter SI, std::vector<Real> SR, const char* nom, String& errorMsg, const String& info)
{ return checkValue(SI, SR, String(nom), errorMsg, info); }
inline Number checkValue(Parameter SI, std::vector<Real> SR, const char* nom, String& errorMsg, const char* info)
{ return checkValue(SI, SR, String(nom), errorMsg, String(info)); }

Number checkValues(Parameters SI, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(Parameters SI, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(SI, refFilename, tol, errorMsg, String(info), check); }
//@}

//@{
//! test Vector or Matrix values with  a file containing reference values
Number checkValues(const Vector<Number>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const Vector<Number>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const Vector<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const Vector<Real>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const Vector<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const Vector<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const std::vector<Real>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<Complex>& C, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const std::vector<Complex>& C, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(C, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<Vector<Real> >& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const std::vector<Vector<Real> >& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<Vector<Complex> >& R,  const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const std::vector<Vector<Complex> >& R,  const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const Matrix<Real>& M, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const Matrix<Real>& M, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(M, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const Matrix<Complex>& M, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const Matrix<Complex>& M, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(M, refFilename, tol, errorMsg, String(info), check); }
//@}

//@{
//! test Vector[Matrix] values with the reference Vector[Matrix].
Number checkValues(const Vector<Real>& R, const Vector<Real>& E, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const Vector<Real>& R, const Vector<Real>& E, Real tol, String& errorMsg, const char* info)
{ return checkValues(R, E, tol, errorMsg, String(info)); }

Number checkValues(const Vector<Complex>& R, const Vector<Complex>& E, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const Vector<Complex>& R, const Vector<Complex>& E, Real tol, String& errorMsg, const char* info)
{ return checkValues(R, E, tol, errorMsg, String(info)); }

Number checkValues(const Vector<Vector<Real> >& R, const Matrix<Real>& E, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const Vector<Vector<Real> >& R, const Matrix<Real>& E, Real tol, String& errorMsg, const char* info)
{ return checkValues(R, E, tol, errorMsg, String(info)); }

Number checkValues(const Vector<Vector<Complex> >& R, const Matrix<Complex>& E, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const Vector<Vector<Complex> >& R, const Matrix<Complex>& E, Real tol, String& errorMsg, const char* info)
{ return checkValues(R, E, tol, errorMsg, String(info)); }

Number checkValues(const Matrix<Real>& M, const Matrix<Real>& Mref, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const Matrix<Real>& M, const Matrix<Real>& Mref, Real tol, String& errorMsg, const char* info)
{ return checkValues(M, Mref, tol, errorMsg, String(info)); }

Number checkValues(const Matrix<Complex>& M, const Matrix<Complex>&  Mref, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const Matrix<Complex>& M, const Matrix<Complex>&  Mref, Real tol, String& errorMsg, const char* info)
{ return checkValues(M, Mref, tol, errorMsg, String(info)); }

Number checkValues(const Matrix<Complex>& M, const Matrix<Real>&  Mref, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const Matrix<Complex>& M, const Matrix<Real>&  Mref, Real tol, String& errorMsg, const char* info)
{ return checkValues(M, Mref, tol, errorMsg, String(info)); }
//@}

//@{
//! test kinds of std::vector with a reference file
Number checkValues(const std::vector<std::pair<Number, Number> > VP, const String& refFilename, Real tol, String& errorMsg,
                     const String& info, bool check);
inline Number checkValues(const std::vector<std::pair<Number, Number> > VP, const String& refFilename, Real tol, String& errorMsg,
                            const char* info, bool check)
{ return checkValues(VP, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<VectorEigenDense<Real> >& R, const String& refFilename, Real tol, String& errorMsg,
                     const String& info, bool check);
inline Number checkValues(const std::vector<VectorEigenDense<Real> >& R, const String& refFilename, Real tol, String& errorMsg,
                            const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<VectorEigenDense<Complex> >& R, const String& refFilename, Real tol, String& errorMsg,
                     const String& info, bool check);
inline Number checkValues(const std::vector<VectorEigenDense<Complex> >& R, const String& refFilename, Real tol, String& errorMsg,
                            const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<MatrixEigenDense<Real> >& R, const String& refFilename, Real tol, String& errorMsg,
                     const String& info, bool check);
inline Number checkValues(const std::vector<MatrixEigenDense<Real> >& R, const String& refFilename, Real tol, String& errorMsg,
                            const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const std::vector<MatrixEigenDense<Complex> >& R, const String& refFilename, Real tol, String& errorMsg,
                     const String& info, bool check);
inline Number checkValues(const std::vector<MatrixEigenDense<Complex> >& R, const String& refFilename, Real tol, String& errorMsg,
                            const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }
//@}

//@{
//! test values of a Vector in quadratic norm
Number checkValuesN2(const Vector<Real>& R, const Vector<Real>& E, Real tol, String& errorMsg, const String& info);
inline Number checkValuesN2(const Vector<Real>& R, const Vector<Real>& E, Real tol, String& errorMsg, const char* info)
{ return checkValuesN2(R, E, tol, errorMsg, String(info)); }

Number checkValuesN2(const Vector<Complex>& R, const Vector<Complex>& E, Real tol, String& errorMsg, const String& info);
inline Number checkValuesN2(const Vector<Complex>& R, const Vector<Complex>& E, Real tol, String& errorMsg, const char* info)
{ return checkValuesN2(R, E, tol, errorMsg, String(info)); }
//@}

//@{
//! test an Eigen dense real or complex vector with a reference file
Number checkValues(const VectorEigenDense<Real> & R,  const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const VectorEigenDense<Real> & R,  const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const VectorEigenDense<Complex> & R,  const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const VectorEigenDense<Complex> & R,  const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }
//@}

//! test values of a TermVector in quadratic norm
Number checkValuesN2(const TermVector& R, const TermVector& E, Real tol, String& errorMsg, const String& info);
//! test values of a TermVector in L2 norm
Number checkValuesL2(const TermVector& R, const TermVector& E, const TermMatrix& M, Real tol, String& errorMsg, const String& info);
//! test values of a TermVector in infinity norm
Number checkValuesNinfty(const TermVector& R, const TermVector& E, Real tol, String& errorMsg, const String& info);

//! Test values of a TermVector with a reference TermVector
Number checkValues(const TermVector& E, const TermVector& R, Real tol, String& errorMsg, const String& info);
inline Number checkValues(const TermVector& E, const TermVector& R, Real tol, String& errorMsg, const char* info)
{ return checkValues(E, R, tol, errorMsg, String(info)); }

//! test a TermVector with a reference file with tolerance
Number checkValues(const TermVector& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const TermVector& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

//@{
//! test a TermMatrix with a file or EigenElements reference
Number checkValues(const TermMatrix& M, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const TermMatrix& M, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(M, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const TermMatrix& TM, const EigenElements& EV, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const TermMatrix& TM, const EigenElements& EV, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(TM, EV, tol, errorMsg, String(info), check); }
//@}

//@{
//! Test a LargeMatrix with a reference file with tolerance
Number checkValues(const LargeMatrix<Real>& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const LargeMatrix<Real>& LM, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(LM, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const LargeMatrix<Complex>& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const LargeMatrix<Complex>& LM, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(LM, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const LargeMatrix<Matrix<Real> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const LargeMatrix<Matrix<Real> >& LM, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(LM, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const LargeMatrix<Matrix<Complex> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const LargeMatrix<Matrix<Complex> >& LM, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(LM, refFilename, tol, errorMsg, String(info), check); }
//@}

//@{
//! test a LargeMatrix with a single value and tolerance
Number checkValues(const LargeMatrix<Real>& LM, Real rV, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const LargeMatrix<Real>& LM, Real rV, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(LM, rV, tol, errorMsg, String(info), check); }

Number checkValues(const LargeMatrix<Complex>& LM, Complex zV, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const LargeMatrix<Complex>& LM, Complex zV, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(LM, zV, tol, errorMsg, String(info), check); }
//@}

//@{
//! Test a Polynomial[s] Basis with a refrence file and tolerance
Number checkValues(const PolynomialBasis& PB, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const PolynomialBasis& PB, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(PB, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const PolynomialsBasis& PB, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const PolynomialsBasis& PB, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(PB, refFilename, tol, errorMsg, String(info), check); }
//@}

//@{
//! Test a a multiVecAdaptater real or complex with a refrence file and tolerance
Number checkValues(const MultiVecAdapter<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const MultiVecAdapter<Real>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const MultiVecAdapter<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const MultiVecAdapter<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }
//@}

//@{
//! Test an eigen dense Matrix with a refrence file and tolerance
Number checkValues(const MatrixEigenDense<Real>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const MatrixEigenDense<Real>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

Number checkValues(const MatrixEigenDense<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const MatrixEigenDense<Complex>& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }
//@}

//! Test a Quadrature with a refrence file and tolerance
Number checkValues(const Quadrature& R, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const Quadrature& R, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(R, refFilename, tol, errorMsg, String(info), check); }

//! Test a low rank matrix with a refrence file and tolerance
Number checkValues(const LowRankMatrix<Real>& M, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const LowRankMatrix<Real>& M, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(M, refFilename, tol, errorMsg, String(info), check); }

//@{
//! Test a Strings with a reference Strings
Number checkValue(const Strings& SI, const Strings& SR, String& errorMsg, const String& info, bool printData=true);
inline Number checkValue(const Strings& SI, const Strings& SR, String& errorMsg, const char* info, bool printData=true)
{ return checkValue(SI, SR, errorMsg, String(info), printData); }
//@}

//@{
//! Test a string with a reference string
Number checkValue(const String& SI, const String& SR, String& errorMsg, const String& info);
inline Number checkValue(const String& SI, const String& SR, String& errorMsg, const char* info)
{ return checkValue(SI, SR, errorMsg, String(info)); }
//@}

//@{
//! test ostream with a reference file
Number checkValue(std::stringstream& ssi, const String& refFilename, String& errorMsg, const String& info, bool check, bool printData=true);
inline Number checkValue(std::stringstream& ssi, const String& refFilename, String& errorMsg, const char* info, bool check, bool printData=true)
{ return checkValue(ssi, refFilename, errorMsg, String(info), check, printData); }
//@}

inline Number checkValue(CoutStream& cs, const String& refFilename, String& errorMsg, const String& info, bool check, bool printData=true)
{ return checkValue(*(cs.stringStream), refFilename, errorMsg, info, check, printData); }
inline Number checkValue(CoutStream& cs, const String& refFilename, String& errorMsg, const char* info, bool check, bool printData=true)
{ return checkValue(*(cs.stringStream), refFilename, errorMsg, info, check, printData); }

//@{
//! Test std::vector of String with a refernce file
Number checkValues(const std::vector<String>& VS, const String& refFilename, String& errorMsg, const String& info , bool check);
inline Number checkValues(const std::vector<String>& VS, const String& refFilename, String& errorMsg, const char* info , bool check)
{ return checkValues(VS, refFilename, errorMsg, String(info), check); }
//@}

//@{
//! Test a file with a reference file
Number checkValues(const String& filename, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
inline Number checkValues(const char* filename, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check)
{ return checkValues(String(filename), refFilename, tol, errorMsg, info, check); }
inline Number checkValues(const String& filename, const char* refFilename, Real tol, String& errorMsg, const String& info, bool check)
{ return checkValues(filename, String(refFilename), tol, errorMsg, info, check); }
inline Number checkValues(const char* filename, const char* refFilename, Real tol, String& errorMsg, const String& info, bool check)
{ return checkValues(String(filename), String(refFilename), tol, errorMsg, info, check); }
inline Number checkValues(const String& filename, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(filename, refFilename, tol, errorMsg, String(info), check); }
inline Number checkValues(const char* filename, const String& refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(String(filename), refFilename, tol, errorMsg, String(info), check); }
inline Number checkValues(const String& filename, const char* refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(filename, String(refFilename), tol, errorMsg, String(info), check); }
inline Number checkValues(const char* filename, const char* refFilename, Real tol, String& errorMsg, const char* info, bool check)
{ return checkValues(String(filename), String(refFilename), tol, errorMsg, String(info), check); }
//@}

//@{
//!Test visual of different type of matrices with a reference file with tolerance
Number checkVisual(const TermMatrix& TM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
Number checkVisual(const MatrixStorage& MS, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
Number checkVisual(const LargeMatrix<Real>& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
Number checkVisual(const LargeMatrix<Complex>& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
Number checkVisual(const LargeMatrix<Matrix<Real> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
Number checkVisual(const LargeMatrix<Matrix<Complex> >& LM, const String& refFilename, Real tol, String& errorMsg, const String& info, bool check);
//@}

//@{
//! internal functions for checkIncompleteFacto
void giveValue(const LargeMatrix<Real>& M0, LargeMatrix<Real>& M1);
void giveValue(const LargeMatrix<Complex>& M0, LargeMatrix<Complex>& M1);
void getPart(FactorizationType Part, LargeMatrix<Real>& MIF, LargeMatrix<Real>& MM);
void getPart(FactorizationType Part, LargeMatrix<Complex>& MIF, LargeMatrix<Complex>& MM);
//!@}

//!@{
//! test an incomplete factorization of a LargeMatrix
Number checkIncompleteFacto(FactorizationType Part, LargeMatrix<Real>& M0, LargeMatrix<Real>& MIF, Real eps, String& errorMsg, const String& info);
Number checkIncompleteFacto(FactorizationType Part, LargeMatrix<Complex>& M0, LargeMatrix<Complex>& MIF, Real eps, String& errorMsg, const String& info);
//@}

//! test an incomplete factorization of a TermMatrix
Number checkIncompleteFacto(FactorizationType Part, TermMatrix& H0, TermMatrix& HIF, Real eps, String& errorMsg, const String& info);

} // end of namespace xlifepp

#endif // TEST_UTILS_HPP
