/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_LcOperatorOnUnknowns.cpp
  \authors É. Peillon
  \since 13 jan 2022
  \date  13 jan 2022

  Low level tests of LcOperatorOnUnknowns class and related classes.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting informations in a string
*/

//===============================================================================
//library dependences
#include "testUtils.hpp"
#include "xlife++-libs.h"

//===============================================================================
//stl dependences
#include <fstream>
#include <iostream>

//===============================================================================

using namespace xlifepp;

namespace unit_LcOperatorOnUnknowns
{

void unit_LcOperatorOnUnknowns(int argc, char *argv[], bool check)
{
  String rootname = "unit_LcOperatorOnUnknowns";
  trace_p->push(rootname);
  verboseLevel(5);

  Number nbErrors = 0;
  String errors;

  // Basic test domain
  Sphere B(_center = Point(0, 0, 0), _radius = 1, _nnodes = 11, _domain_name = "Omega");
  Mesh mesh2d(B, _shape=_triangle, _order=1);
  Domain Omega = mesh2d.domain("Omega");

  Space V1(_domain=Omega, _interpolation=P1, _name="P1");
  Unknown u1(V1, _name="u1"), u2(V1, _name="u2");
  TestFunction v1(u1, _name="v1"), v2(u2, _name="v2");

  Space V2(_domain=Omega, _interpolation=RT_1, _name="RT1");
  Unknown p1(V2, _name="p1"), p2(V2, _name="p2");
  TestFunction q1(p1, _name="q1"), q2(p2, _name="q2");

  ///////////////////////
  // test Constructors //
  ///////////////////////

  theCout << "Basic constructors : " << eol
          << LcOperatorOnUnknowns() << eol
          << "2*u1*v1 :" << eol << LcOperatorOnUnknowns(u1 * v1, 2) << eol
          << "i*u1*v1 :" << eol << LcOperatorOnUnknowns(u1 * v1, i_) << eol;
  LcOperatorOnUnknowns LcUnkS1;
  LcUnkS1 = u1 * v1;
  LcOperatorOnUnknowns LcUnkS2(LcUnkS1);
  theCout << "u1*v1 twice :" << eol
          << LcUnkS1 << eol
          << LcUnkS2 << eol;
  nbErrors += checkValue(theCout, rootname + "/constructors.in", errors, "Constructors", check);

  /////////////////////////
  // Manipulations tests //
  /////////////////////////

  // OperatoronUnknowns and OperatorOnUnknowns
  theCout << "OperatorOnUnknowns and OperatorOnUnknowns : " << eol
          << "u1*v1 + u2*v2 :" << eol << (u1 * v1) + (u2 * v2) << eol
          << "u1*v1 - p1|q1 :" << eol << (u1 * v1) - (p1 | q1) << eol;
  nbErrors += checkValue(theCout, rootname + "/OpUnksOpUnks.in", errors, "OperatorOnUnknowns and OperatorOnUnknowns", check);

  // LcOperatorOnUnknowns and LcOperatorOnUnknowns
  LcUnkS1 = u1 * v1;
  LcUnkS2 = u2 * v2;
  theCout << "LcOperatorOnUnknowns and LcOperatorOnUnknowns :" << eol
          << "u1*v1" << eol << LcUnkS1 << eol
          << "-u1*v1" << eol << -LcUnkS1 << eol
          << "u1*v1 + u2*v2" << eol << LcUnkS1 + LcUnkS2 << eol
          << "u1*v1 - u2*v2" << eol << LcUnkS1 - LcUnkS2 << eol;
  nbErrors += checkValue(theCout, rootname + "/LcOpUnksLcOpUnks.in", errors, "LcOperatorOnUnknowns and LcOperatorOnUnknowns", check);

  // LcOperatorOnUnknowns and OperatorOnUnknowns
  theCout << "LcOperatorOnUnknowns and OperatorOnUnknowns :" << eol
          << "u1*v1 + p1|q1" << eol << LcUnkS1 + (p1 | q1) << eol
          << "u1*v1 - p1|q1" << eol << LcUnkS1 - (p1 | q1) << eol
          << "p1|q1 + u1*v1" << eol << (p1 | q1) + LcUnkS1 << eol
          << "p1|q1 - u1*v1" << eol << (p1 | q1) - LcUnkS1 << eol;
  nbErrors += checkValue(theCout, rootname + "/LcOpUnksOpUnks.in", errors, "LcOperatorOnUnknowns and OperatorOnUnknowns", check);

  // LcOperatorOnUnknowns and scalar
  theCout << "LcOperatorOnUnknowns and scalar : " << eol
          << "2*u1*v1" << eol << 2 * LcUnkS1 << eol
          << "2i*u1*v1" << eol << 2 * i_ * LcUnkS1 << eol
          << "2*u1*v1" << eol << LcUnkS1 * 2 << eol
          << "2i*u1*v1" << eol << LcUnkS1 * 2 * i_ << eol
          << "0.5*u1*v1" << eol << LcUnkS1 / 2 << eol
          << "-0.5i*u1*v1" << eol << LcUnkS1 / (2 * i_) << eol;
  nbErrors += checkValue(theCout, rootname + "/LcOpUnksSc.in", errors, "LcOperatorOnUnknowns and scalar", check);

  // LcOperatorOnUnknown and OperatorOnUnkown
  theCout << "LcOperatorOnUnknown and OperatorOnUnkown : " << eol
          << "u1*dx(v1) + u2*dx(v1)" << eol << (u1 + u2) * dx(v1) << eol
          << "dx(u1)*v1 - dx(u1)*2*v2" << eol << dx(u1) * (v1 - 2 * v2) << eol
          << "grad(p1) % grad(q1) + grad(p2) % grad(q1)) " << eol << (grad(p1) + grad(p2)) % grad(q1) << eol
          << "grad(p1) % grad(q1) - grad(p1) % grad(q2)) " << eol << grad(p1) % (grad(q1) - grad(q2)) << eol
          << "p1|grad(v1) + i*p2|grad(v1)" << eol << ((p1 + i_ * p2) | grad(v1)) << eol
          << "grad(u1)|grad(v1) - grad(u1)|q1" << eol << (grad(u1) | (grad(v1) - q1)) << eol;
  // << "p1^grad(v1) + grad(u2)^grad(v1)" << eol << ((p1 + grad(u2)) ^ grad(v1)) << eol
  // << "p1^q1+p1^q2" << eol << (id(p1) ^ (q1 + q2)) << eol;
  nbErrors += checkValue(theCout, rootname + "/LcOpUnkOpUnk.in", errors, "LcOperatorOnUnknown and OperatorOnUnkown", check);

  // LcOperatorOnUnknown and Unkown
  theCout << "LcOperatorOnUnknown and Unkown : " << eol
          << "u1*v1 + u2*v1" << eol << (u1 + u2) * v1 << eol
          << "u1*v1 - u1*2*v2" << eol << u1 * (v1 - 2 * v2) << eol
          << "p1|q2 + i*p2|q2" << eol << ((p1 + i_ * p2) | q2) << eol
          << "p1|grad(v1) - p1|q1" << eol << (p1 | (grad(v1) - q1)) << eol;
  nbErrors += checkValue(theCout, rootname + "/LcOpUnkUnk.in", errors, "LcOperatorOnUnknown and Unkown", check);

  // LcOperatorOnUnknown and LcOperatorOnUnkown
  theCout << "LcOperatorOnUnknown and LcOperatorOnUnknown : " << eol
          << "u1*v1 + u1*v2 + u2*v1 + u2*v2" << eol << (u1 + u2) * (v1 + v2) << eol
          << "grad(p1)%grad(q1) + grad(p1)%grad(q2) + grad(p2)%grad(q1) + grad(p2)%grad(q2)" << eol << ((grad(p1) + grad(p2)) % (grad(q1) + grad(q2))) << eol
          << "p1|q1 + p1|q2 + p2|q1 + p2|q2" << eol << ((p1 + p2) | (q1 + q2)) << eol;
  nbErrors += checkValue(theCout, rootname + "/LcOpUnkLcOpUnk.in", errors, "LcOperatorOnUnknown and LcOperatorOnUnknown", check);

  // Linear and Bilinear form construction
  theCout << "Linear and Bilinear form construction : " << eol
          << "LinearForm on Omega of v1+v2 : " << eol << intg(Omega, v1 + v2) << eol
          << "LinearForm on Omega of v1+v2 : " << eol << intg(Omega, (u1 + u2) * (v1 + v2)) << eol;
  nbErrors += checkValue(theCout, rootname + "/LinBilinformLcOp.in", errors, "Linear and Bilinear form construction", check);

  if (check)
  {
    if (nbErrors == 0)
    {
      theCout << message("test_report", rootname, 0, "");
    }
    else
    {
      error("test_report", rootname, nbErrors, ":\n" + errors);
    }
  }
  else
  {
    theCout << "Data updated " << eol;
  }

  trace_p->pop();
  
}

} // namespace unit_LcOperatorOnUnknowns
