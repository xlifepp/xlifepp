/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_NedelecTriangle.cpp
	\author E. Lunéville
	\since 31 jan 2015
	\date 31 jan 2015

	Low level tests of Nedelec's element on triangle
	and a global test on Maxwell problem on unit square

	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_NedelecTriangle
{

typedef GeomDomain& Domain;

Real omg=1, eps=1, mu=1, a=pi_, ome=omg* omg* mu* eps;

Vector<Real> f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Vector<Real> res(2);
  Real c=2*a*a-ome;
  res(1)=-c*cos(a*x)*sin(a*y);
  res(2)= c*sin(a*x)*cos(a*y);
  return res;
}

Vector<Real> solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Vector<Real> res(2);
  res(1)=-cos(a*x)*sin(a*y);
  res(2)= sin(a*x)*cos(a*y);
  return res;
}

const Element* elt;
Number index=1;
Vector<Real> w(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> r(2);
  ShapeValues shv = elt->computeShapeValues(P, false, false, 0);
  Number i=index-1;
  r[0]=shv.w[2*i];r[1]=shv.w[2*i+1];
  return r;
}

//compute dof_i(w_j) in physical space, check computeShapeValue and dof::operator()
void checkPhysicalShapeValue(const Space& V)
{
  theCout<<V<<eol;
  elt=V.element_p(Number(0));
  Function fw(w,2);
  theCout<<" -------- checkPhysicalShapeValue -------------"<<eol;
  for(Number d=1;d<=V.nbDofs();d++)
  {
    theCout<<"dof "<<d<<": ";
    for(Number j=1;j<=V.nbDofs();j++)
    {
      index=j;
      theCout<<roundToZero(V.dof(d)(fw).asReal(),1.E-10)<<" ";
     }
   theCout<<eol;
  }
}

/*! test : solve Maxwell harmonic equation on unit square Omega=]0,1[x]0,1[
                  curl curl(E) -w^2.mu.eps E = -i.w.mu.J=fJ   in Omega
                  E x n = 0                                   on dOmega
            Solution (a=k.pi) :
                     Ex = - cos(ax)sin(ay)
                     Ey =   cos(ay)sin(ax)
         -> fJ= (2a^2-w^2.mu.eps)E
  */
void checkMaxwell(FESubType fs, Number ord, Domain omega, Domain gamma, const String& rootname, std::stringstream& ssout, bool check, double tol, String& errors, Number& nbErrors)
{
  Number fa=(fs == _firstFamily ? 1 : 2);
  //create spaces
  Space W(omega,interpolation(_Lagrange,_standard,ord,H1),"W",true);
  Unknown u(W, _name="u", _dim=2); TestFunction v(u, _name="v");
  TermMatrix M(intg(omega,u|v), _name="M");
  Space V(omega,interpolation(_NedelecEdge,fs,ord,Hrot),"V",false);
  Unknown e(V, _name="E"); TestFunction q(e, _name="q");
  //create FE terms
  BilinearForm aev=intg(omega,curl(e)|curl(q))-ome*intg(omega,e|q);
  LinearForm l=intg(omega,f|q);
  EssentialConditions ecs = (ncross(e) | gamma)=0;
  TermMatrix A(aev, ecs, _name="A");
  TermVector b(l, _name="B");
  //solve system
  TermVector E=directSolve(A,b);
  E.print(ssout);
  //P1 interpolation, L2 projection
  TermMatrix N(intg(omega,e|v), _name="N");
  TermVector EP1=directSolve(M,N*E,true);  //L2 projection
  String fn="E"+tostring(fa)+tostring(ord)+"_P1";
  saveToFile(fn,EP1,_format=_vtu,_aUniqueFile);
  nbErrors+= checkValues(fn+".vtu",rootname+"/"+fn+".in", tol, errors, fn, check);
  //compute error
  TermVector E_ex(u,omega,solex);
  TermVector E_er=E_ex-EP1;
  Real ErrL2=std::sqrt(real((M*E_er|E_er)));
  nbErrors+=checkValue(ErrL2, 0., tol, errors, "ErrL2 NT"+tostring(fa)+tostring(ord));
  //P1 interpolation, direct interpolation using local shape functions
  TermVector EI=interpolate(u,omega,E,"E_int");
  fn="E"+tostring(fa)+tostring(ord)+"_int";
  saveToFile(fn, EI,_format=_vtu, _aUniqueFile);
  nbErrors+= checkValues(fn+".vtu",rootname+"/"+fn+".in", tol, errors,fn, check);
  //other method
  TermMatrix Mp(intg(omega,u|q), _name="Mp");
  TermVector bG=Mp*TermVector(u,omega,f, _name="G");
  TermVector Eb=directSolve(A,bG);
  TermVector EbP1=directSolve(M,N*Eb);  //L2 projection
  fn="E"+tostring(fa)+tostring(ord)+"_P1b";
  saveToFile(fn, EbP1,_format=_vtu,_aUniqueFile);
  nbErrors+= checkValues(fn+".vtu",rootname+"/"+fn+".in", tol, errors, "EbP1", check);
  theCout<<"\n \n Nedelec NT"<<fa<<ord<<"  err L2 ="<<ErrL2<<eol;
}


void unit_NedelecTriangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_NedelecTriangle";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(10);
  //numberOfThreads(1);

  Number nbErrors = 0;
  String errors;
  Real tol=0.005;

  Mesh me(_triangle); Domain omg=me.domain("Omega");

  Point x(0.5,0.5);
  ShapeValues shv;
  Interpolation* interp;
  interp=findInterpolation(Nedelec, _firstFamily, 1, Hrot);
  ssout<<"\n\n================  Nedelec standard triangle, order 1 (NT1) ======================"<<eol;
  NedelecFirstTriangleP1 NT1(interp);
  NT1.print(ssout);
  shv = ShapeValues(NT1);
  NT1.computeShapeValues(x.begin(),shv,true);
  NT1.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NT1.in", errors, "NT1", check);

  ssout<<"\n\n================  Nedelec first family, triangle, order 1 (NT11) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 1, Hrot);
  NedelecFirstTrianglePk NT11(interp);
  NT11.print(ssout);
  shv = ShapeValues(NT11);
  NT11.computeShapeValues(x.begin(),shv);
  NT11.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NT11.in", errors, "NT11", check);
  Space V11(omg,*interp,"V11",false);
  checkPhysicalShapeValue(V11);

  ssout<<"\n\n ================  Nedelec first family, triangle, order 2 (NT12) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 2, Hrot);
  NedelecFirstTrianglePk NT12(interp);
  NT12.print(ssout);
  shv = ShapeValues(NT12);
  NT12.computeShapeValues(x.begin(),shv,true);
  NT12.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NT12.in", errors, "NT12", check);
  Space V12(omg,*interp,"V12",false);
  checkPhysicalShapeValue(V12);

  ssout<<"\n\n================  Nedelec first family, triangle, order 3 (NT13) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 3, Hrot);
  NedelecFirstTrianglePk NT13(interp);
  NT13.print(ssout);
  shv = ShapeValues(NT13);
  NT13.computeShapeValues(x.begin(),shv,true);
  NT13.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NT13.in", errors, "NT13", check);
  Space V13(omg,*interp,"V13",false);
  checkPhysicalShapeValue(V13);

  ssout<<"\n\n================  Nedelec second family, triangle, order 1 (NT21) ======================"<<eol;
  interp=findInterpolation(Nedelec, _secondFamily, 1, Hrot);
  NedelecSecondTrianglePk NT21(interp);
  NT21.print(ssout);
  shv = ShapeValues(NT21);
  NT21.computeShapeValues(x.begin(),shv);
  NT21.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NT21.in", errors, "NT21", check);
  Space V21(omg,*interp,"V21",false);
  checkPhysicalShapeValue(V21);

  ssout<<"\n\n ================  Nedelec second family, triangle, order 2 (NT22) ======================"<<eol;
  interp=findInterpolation(Nedelec, _secondFamily, 2, Hrot);
  NedelecSecondTrianglePk NT22(interp);
  NT22.print(ssout);
  shv = ShapeValues(NT22);
  NT22.computeShapeValues(x.begin(),shv,true);
  NT22.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NT22.in", errors, "NT22", check);
  Space V22(omg,*interp,"V22",false);
  checkPhysicalShapeValue(V22);

  ssout<<"\n\n================  Nedelec second family, triangle, order 3 (NT23) ======================"<<eol;
  interp=findInterpolation(Nedelec, _secondFamily, 3, Hrot);
  NedelecSecondTrianglePk NT23(interp);
  NT23.print(ssout);
  shv = ShapeValues(NT23);
  NT23.computeShapeValues(x.begin(),shv,true);
  NT23.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NT23.in", errors, "NT23", check);
  Space V23(omg,*interp,"V23",false);
  checkPhysicalShapeValue(V23);

  //check Maxwell problem
  Number nh=21;
  Strings sidenames(4,"Gamma");
  Mesh  mesh2d(inputsPathTo(rootname+"/mesh2d"+tostring(nh)+".msh"), _name="mesh of the unit square");
  Domain omega=mesh2d.domain("Omega"), gamma=mesh2d.domain("Gamma");
  checkMaxwell (_firstFamily, 1, omega, gamma, rootname, ssout, check, tol, errors, nbErrors);
  checkMaxwell (_firstFamily, 2, omega, gamma, rootname, ssout, check, tol, errors, nbErrors);
  checkMaxwell (_firstFamily, 3, omega, gamma, rootname, ssout, check, tol, errors, nbErrors);
  checkMaxwell(_secondFamily, 1, omega, gamma, rootname, ssout, check, tol, errors, nbErrors);
  checkMaxwell(_secondFamily, 2, omega, gamma, rootname, ssout, check, tol, errors, nbErrors);
  checkMaxwell(_secondFamily, 3, omega, gamma, rootname, ssout, check, tol, errors, nbErrors);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
