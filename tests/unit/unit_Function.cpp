/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
	\file unit_Function.cpp
	\author E. Lunéville
	\since 1 nov 2011
	\date 11 may 2012

	Low level tests of Function class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <map>

using namespace xlifepp;

namespace unit_Function {

//!============================================================================
//!       different user functions (scalar form, Point as argument)
//!============================================================================

//! function of a point returning a real scalar
Real sinf(const Point& P, Parameters& pa = defaultParameters)
{return sin(P[0]);}

//! function of a point returning a real vector
Vector<Real> vsinf(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> V(3);
  V[0] = sin(P[0]); V[1] = 2 * sin(P[0]); V[2] = 3 * sin(P[0]);
  return V;
}

//! function of a point returning a real matrix
Matrix<Real> msinf(const Point& P, Parameters& pa = defaultParameters)
{
  Matrix<Real> M(Dimen(3), Dimen(3));
  M(1, 1) = sin(P[0]); M(2, 2) = 2 * sin(P[0]); M(3, 3) = 3 * sin(P[0]);
  return M;
}

//! function of a point returning a complex scalar
Complex isinf(const Point& P, Parameters& pa = defaultParameters)
{ return i_ * sin(P[0]);}

//! function of a point returning a complex vector
Vector<Complex> visinf(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Complex> V(3);
  V[0] = i_ * sin(P[0]); V[1] = 2.*i_ * sin(P[0]); V[2] = 3.*i_ * sin(P[0]);
  return V;
}

//! function of a point returning a complex matrix
Matrix<Complex> misinf(const Point& P, Parameters& pa = defaultParameters)
{
  Matrix<Complex> M(Dimen(3), Dimen(3));
  M(1, 1) = i_ * sin(P[0]); M(2, 2) = 2.*i_ * sin(P[0]); M(3, 3) = 3.*i_ * sin(P[0]);
  return M;
}

//! kernel function of a point returning a real scalar
Real green(const Point& x, const Point& y, Parameters& pa = defaultParameters)
{return sin(x[0]) * exp(y[0]);}

//! kernel function of a point returning a real vector
Vector<Real> vgreen(const Point& x, const Point& y, Parameters& pa = defaultParameters)
{
  Vector<Real> V(3);
  V[0] = sin(x[0]) * exp(y[0]); V[1] = 2 * sin(x[0]) * exp(y[0]); V[2] = 3 * sin(x[0]) * exp(y[0]);
  return V;
}

//! kernel function of a point returning a real matrix
Matrix<Real> mgreen(const Point& x, const Point& y, Parameters& pa = defaultParameters)
{
  Matrix<Real> M(Dimen(3), Dimen(3));
  M(1, 1) = sin(x[0]) * exp(y[0]); M(2, 2) = 2.*sin(x[0]) * exp(y[0]); M(3, 3) = 3.*sin(x[0]) * exp(y[0]);
  return M;
}

//! kernel function of a point returning a complex scalar
Complex cgreen(const Point& x, const Point& y, Parameters& pa = defaultParameters)
{ return i_ * sin(x[0]) * exp(y[0]); }

//! kernel function of a point returning a complex vector
Vector<Complex> vcgreen(const Point& x, const Point& y, Parameters& pa = defaultParameters)
{
  Vector<Complex> V(3);
  V[0] = i_ * sin(x[0]) * exp(y[0]); V[1] = 2.*i_ * sin(x[0]) * exp(y[0]); V[2] = 3.*i_ * sin(x[0]) * exp(y[0]);
  return V;
}

//! kernel function of a point returning a complex matrix
Matrix<Complex> mcgreen(const Point& x, const Point& y, Parameters& pa = defaultParameters)
{
  Matrix<Complex> M(Dimen(3), Dimen(3));
  M(1, 1) = sin(x[0]) * exp(y[0]); M(2, 2) = 2.*i_ * sin(x[0]) * exp(y[0]); M(3, 3) = 3.*i_ * sin(x[0]) * exp(y[0]);
  return M;
}

//!============================================================================
//! different user functions (vector form, Vector<Point> as argument)
//!============================================================================

//! function of a point returning a real scalar
Vector<Real> sinf_v(const Vector<Point>& P, Parameters& pa = defaultParameters)
{
  Vector<Real> res(P.size());
  for(Number j = 0; j < P.size(); j++) { res[j] = sinf(P[j]); }
  return res;
}

//! function of a point returning a real vector
Vector<Vector<Real> > vsinf_v(const Vector<Point>& P, Parameters& pa = defaultParameters)
{
  Vector<Vector<Real> > VV(P.size());
  for(Number j = 0; j < P.size(); j++)
  {
    Vector<Real> W(3);
    W[0] = sin(P[j][0]); W[1] = 2 * sin(P[j][0]); W[2] = 3 * sin(P[j][0]);
    VV[j] = W;
  }
  return VV;
}

//! function of a point returning a real matrix
Vector<Matrix<Real> > msinf_v(const Vector<Point>& P, Parameters& pa = defaultParameters)
{
  Vector<Matrix<Real> > VM(P.size());
  for(Number j = 0; j < P.size(); j++)
  {
    Matrix<Real> M(Dimen(3), Dimen(3));
    M(1, 1) = sin(P[j][0]); M(2, 2) = 2 * sin(P[j][0]); M(3, 3) = 3 * sin(P[j][0]);
    VM[j] = M;
  }
  return VM;
}

//! function of a point returning a complex scalar
Vector<Complex> isinf_v(const Vector<Point>& P, Parameters& pa = defaultParameters)
{
  Vector<Complex> res(P.size());
  for(Number j = 0; j < P.size(); j++) { res[j] = i_ * sinf(P[j]); }
  return res;
}

//! function of a point returning a complex vector
Vector<Vector<Complex> > visinf_v(const Vector<Point>& P, Parameters& pa = defaultParameters)
{
  Vector<Vector<Complex> > VV(P.size());
  for(Number j = 0; j < P.size(); j++)
  {
    Vector<Complex> W(3);
    W[0] = i_ * sin(P[j][0]); W[1] = 2.*i_ * sin(P[j][0]); W[2] = 3.*i_ * sin(P[j][0]);
    VV[j] = W;
  }
  return VV;
}

//! function of a point returning a complex matrix
Vector<Matrix<Complex> > misinf_v(const Vector<Point>& P, Parameters& pa = defaultParameters)
{
  Vector<Matrix<Complex> > VM(P.size());
  for(Number j = 0; j < P.size(); j++)
  {
    Matrix<Complex> M(Dimen(3), Dimen(3));
    M(1, 1) = i_ * sin(P[j][0]); M(2, 2) = 2.*i_ * sin(P[j][0]); M(3, 3) = 3.*i_ * sin(P[j][0]);
    VM[j] = M;
  }
  return VM;
}

//! kernel function of a point returning a real scalar
Vector<Real> green_v(const Vector<Point>& Vx, const Vector<Point>& Vy, Parameters& pa = defaultParameters)
{
  // do not check that Vy.size()>=Vx.size()
  Vector<Real> V(Vx.size());
  for(Number j = 0; j < Vx.size(); j++)  { V[j] = sin(Vx[j][0]) * exp(Vy[j][0]); }
  return V;
}

//! kernel function of a point returning a real vector
Vector<Vector<Real> > vgreen_v(const Vector<Point>& Vx, const Vector<Point>& Vy, Parameters& pa = defaultParameters)
{
  //! do not check that Vy.size()>=Vx.size()
  Vector<Vector<Real> > VV(Vx.size());
  for(Number j = 0; j < Vx.size(); j++)
  {
    Vector<Real> V(3);
    V[0] = sin(Vx[j][0]) * exp(Vy[j][0]);
    V[1] = 2 * sin(Vx[j][0]) * exp(Vy[j][0]);
    V[2] = 3 * sin(Vx[j][0]) * exp(Vy[j][0]);
    VV[j] = V;
  }
  return VV;
}

//! kernel function of a point returning a real vector
Vector<Matrix<Real> > mgreen_v(const Vector<Point>& Vx, const Vector<Point>& Vy, Parameters& pa = defaultParameters)
{
  // do not check that Vy.size()>=Vx.size()
  Vector<Matrix<Real> > VM(Vx.size());
  for(Number j = 0; j < Vx.size(); j++)
  {
    Matrix<Real> M(Dimen(3), Dimen(3));
    M(1, 1) = sin(Vx[j][0]) * exp(Vy[j][0]);
    M(2, 2) = 2 * sin(Vx[j][0]) * exp(Vy[j][0]);
    M(3, 3) = 3 * sin(Vx[j][0]) * exp(Vy[j][0]);
    VM[j] = M;
  }
  return VM;
}

//! kernel function of a point returning a complex scalar
Vector<Complex> igreen_v(const Vector<Point>& Vx, const Vector<Point>& Vy, Parameters& pa = defaultParameters)
{
  // do not check that Vy.size()>=Vx.size()
  Vector<Complex> V(Vx.size());
  for(Number j = 0; j < Vx.size(); j++)  { V[j] = i_ * sin(Vx[j][0]) * exp(Vy[j][0]); }
  return V;
}

//! kernel function of a point returning a complex vector
Vector<Vector<Complex> > vigreen_v(const Vector<Point>& Vx, const Vector<Point>& Vy, Parameters& pa = defaultParameters)
{
  //! do not check that Vy.size()>=Vx.size()
  Vector<Vector<Complex> > VV(Vx.size());
  for(Number j = 0; j < Vx.size(); j++)
  {
    Vector<Complex> V(3);
    V[0] = i_ * sin(Vx[j][0]) * exp(Vy[j][0]);
    V[1] = 2.*i_ * sin(Vx[j][0]) * exp(Vy[j][0]);
    V[2] = 3.*i_ * sin(Vx[j][0]) * exp(Vy[j][0]);
    VV[j] = V;
  }
  return VV;
}

//! kernel function of a point returning a complex vector
Vector<Matrix<Complex> > migreen_v(const Vector<Point>& Vx, const Vector<Point>& Vy, Parameters& pa = defaultParameters)
{
  //! do not check that Vy.size()>=Vx.size()
  Vector<Matrix<Complex> > VM(Vx.size());
  for(Number j = 0; j < Vx.size(); j++)
  {
    Matrix<Complex> M(Dimen(3), Dimen(3));
    M(1, 1) = sin(Vx[j][0]) * exp(Vy[j][0]);
    M(2, 2) = 2.*i_ * sin(Vx[j][0]) * exp(Vy[j][0]);
    M(3, 3) = 3.*i_ * sin(Vx[j][0]) * exp(Vy[j][0]);
    VM[j] = M;
  }
  return VM;
}

//! complex function, using a parameter list
Complex planewave(const Point& x, Parameters& pa = defaultParameters)
{
  Real kx = pa("kx"), ky = pa("ky");
  Complex r = exp(i_ * kx * x[0] + i_ * ky * x[1]);
  return r;
}

//!============================================================================
//! test function
//!============================================================================
void unit_Function(int argc, char* argv[], bool check)
{
  String rootname = "unit_Function";
  trace_p->push(rootname);
  std::stringstream out;                  // string stream receiving results
  out.precision(testPrec);
  std::stringstream ssout;  
  ssout.precision(testPrec);   
  String errors;
  Number nbErrors = 0;
  Real eps=0.005;    

  //! some data used by the test
  Real ps2 = 3.1415926 / 2., res = 0.;
  Point x(ps2, ps2);
  Vector<Point> vx(3, x);
  Vector<Real> vres;
  Vector<Vector<Real> > vvres;
  Matrix<Real> mres;
  Vector<Matrix<Real> >vmres;
  Complex ces(0., 0.);
  Vector<Complex> vces;
  Vector<Vector<Complex> > vvces;
  Matrix<Complex> mces;
  Vector<Matrix<Complex> >vmces;

  //! test of static data initialisation
  std::map<String, std::pair<ValueType, StrucType> >::iterator it;
  for(it = Function::returnTypes.begin(); it != Function::returnTypes.end(); ++it)
  {thePrintStream << "name=" << it->first << " values=" << (it->second).first << "," << (it->second).second << eol;}
  for(it = Function::returnArgs.begin(); it != Function::returnArgs.end(); ++it)
  {thePrintStream << "name=" << it->first << " values=" << (it->second).first << "," << (it->second).second << eol;}
  // nbErrors+=checkValue(theCout, rootname+"/StaticDataInitialisation.in", errors, " Static data initialisation", check);

  //! test of scalar form of function (point as input argument)
  //!---------------------------------------------------------

  //! test of real scalar function
  Function f(sinf, "sinus");                         // object function, naming is optional
  theCout << f << eol;                                 // print information of the function
  f.checkTypeOn();                                 // activate the type checking only for the next call
  theCout << "safe call sin(x)=" << f(x, res) << eol;           // call using operator () with type checking
  theCout << "unsafe call sin(x)=" << f.funSR(x) << eol; // direct call, no type checking (never)!
  f.checkTypeOn();
  theCout << "safe call sin(vx)=" << f(vx, vres) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/RealScalarFunction.in", errors, "Real Scalar Function", check); 

  //! test of real vector function
  Function vf(vsinf, "vector sinus");                // object vector function, naming is optional
  theCout << vf << eol;                                // print information of the function
  vf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call vsin(x)=" << vf(x, vres) << eol;        // call using operator () with type checking
  theCout << "unsafe call vsin(x)=" << vf.funVR(x) << eol; // direct call, no type checking (never)!
  vf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call vsin(vx)=" << vf(vx, vvres) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/RealVectorFunction.in", errors, "Real Vector Function", check); 

  //! test of real matrix function
  Function mf(msinf, "matrix sinus");                // object matrix function, naming is optional
  theCout << mf << eol;                                // print information of the function
  mf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call msin(x)=" << mf(x, mres) << eol;        // call using operator () with type checking
  theCout << "unsafe call vsin(x)=" << mf.funMR(x) << eol; // direct call, no type checking (never)!
  mf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call vsin(vx)=" << mf(vx, vmres) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/RealMatrixFunction.in", errors, "Real Matrix Function", check); 

  //! test of complex scalar function
  Function cf(isinf, "isinus");                      // object function, naming is optional
  theCout << cf << eol;                                // print information of the function
  cf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call isin(x)=" << cf(x, ces) << eol;         // call using operator () with type checking
  theCout << "unsafe call isin(x)=" << cf.funSC(x) << eol; // direct call, no type checking (never)!
  cf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call isin(vx)=" << cf(vx, vces) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/ComplexScalarFunction.in", errors, "Complex Scalar Function", check); 

  //! test of complex vector function
  Function vcf(visinf, "vector isinus");              // object vector function, naming is optional
  theCout << vcf << eol;                                // print information of the function
  vcf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call(visin(x)=" << vcf(x, vces) << eol;       // call using operator () with type checking
  theCout << "unsafe call(visin(x)=" << vcf.funVC(x) << eol; // direct call, no type checking (never)!
  vcf.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call(visin(vx)=" << vcf(vx, vvces) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/ComplexVectorFunction.in", errors, "Complex Vector Function", check); 

  //! test of complex matrix function
  Function mcf(misinf, "matrix isinus");               // object matrix function, naming is optional
  theCout << mcf << eol;                                 // print information of the function
  mcf.checkTypeOn();                                 // activate the type checking only for the next call
  theCout << "safe call misin(x)=" << mcf(x, mces) << eol;        // call using operator () with type checking
  theCout << "unsafe call misin(x)=" << mcf.funMR(x) << eol; // direct call, no type checking (never)!
  mcf.checkTypeOn();                                 // activate the type checking only for the next call
  theCout << "safe call misin(vx)=" << mcf(vx, vmces) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/ComplexMatrixFunction.in", errors, "Complex Matrix Function", check); 

  //! test of real kernel
  Function k(green, "Green");                           // object kernel function, naming is optional
  theCout << k << eol;                                    // print information of the function
  k.checkTypeOn();                                    // activate the type checking only for the next call
  theCout << "safe call green(x,y)=" << k(x, x, res) << eol;       // call using operator () with type checking
  theCout << "unsafe call green(x,y)=" << k.kerSR(x, x) << eol; // direct call, no type checking (never)!
  k.checkTypeOn();                                    // activate the type checking only for the next call
  theCout << "safe call green(vx,vx)=" << k(vx, vx, vres) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/RealKernelFunction.in", errors, "Real Kernel Function", check); 

  //! test of real vector kernel
  Function vk(vgreen, "vGreen");                          // object kernel function, naming is optional
  theCout << vk << eol;                                     // print information of the function
  vk.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call vgreen(x,y)=" << vk(x, x, vres) << eol;      // call using operator () with type checking
  theCout << "unsafe call vgreen(x,y)=" << vk.kerVR(x, x) << eol; // direct call, no type checking (never)!
  vk.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call vgreen(vx,vx)=" << vk(vx, vx, vvres) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/RealVectorKernelFunction.in", errors, "Real Vector Kernel Function", check); 

  //! test of real matrix kernel
  Function mk(mgreen, "mGreen");                          // object kernel function, naming is optional
  theCout << mk << eol;                                     // print information of the function
  mk.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call mgreen(x,y)=" << mk(x, x, mres) << eol;      // call using operator () with type checking
  theCout << "unsafe call mgreen(x,y)=" << mk.kerMR(x, x) << eol; // direct call, no type checking (never)!
  mk.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call mgreen(vx,vx)=" << mk(vx, vx, vmres) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/RealMatrixKernelFunction.in", errors, "Real Matrix Kernel Function", check); 

  //! test of complex kernel
  Function ck(cgreen, "cGreen");                          // object kernel function, naming is optional
  theCout << ck << eol;                                     // print information of the function
  ck.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call cgreen(x,y)=" << ck(x, x, ces) << eol;       // call using operator () with type checking
  theCout << "unsafe call cgreen(x,y)=" << ck.kerSC(x, x) << eol; // direct call, no type checking (never)!
  ck.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call cgreen(vx,vx)=" << ck(vx, vx, vces) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/ComplexKernelFunction.in", errors, "Complex Kernel Function", check); 

  //! test of complex vector kernel
  Function vck(vcgreen, "vcGreen");                         // object kernel function, naming is optional
  theCout << vck << eol;                                      // print information of the function
  vck.checkTypeOn();                                      // activate the type checking only for the next call
  theCout << "safe call vcgreen(x,y)=" << vck(x, x, vces) << eol;      // call using operator () with type checking
  theCout << "unsafe call vcgreen(x,y)=" << vck.kerVC(x, x) << eol; // direct call, no type checking (never)!
  vck.checkTypeOn();                                      // activate the type checking only for the next call
  theCout << "safe call vcgreen(vx,vx)=" << vck(vx, vx, vvces) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/ComplexVectorKernelFunction.in", errors, "Complex Vector Kernel Function", check); 

  //! test of complex matrix kernel
  Function mck(mcgreen, "mcGreen");                        // object kernel function, naming is optional
  theCout << mck << eol;                                     // print information of the function
  mck.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call mgreen(x,y)=" << mck(x, x, mces) << eol;      // call using operator () with type checking
  theCout << "unsafe call mgreen(x,y)=" << mck.kerMC(x, x) << eol; // direct call, no type checking (never)!
  mck.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call mgreen(vx,vx)=" << mck(vx, vx, vmces) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/ComplexMatrixKernelFunction.in", errors, "Complex Matrix Kernel Function", check); 


  //! test of vector form of function (points vector as input argument)
  //!-----------------------------------------------------------------

  //! test of real scalar function
  Function f_v(sinf_v, "sinus vector");                  // object function, naming is optional
  theCout << f_v << eol;                                   // print information of the function
  f_v.checkTypeOn();                                   // activate the type checking only for the next call
  theCout << "safe call sin_v(vx)=" << f_v(vx, vres) << eol;        // call using operator () with type checking
  theCout << "unsafe call sin_v(vx)=" << f_v.vfunSR(vx) << eol; // direct call, no type checking (never)!
  f_v.checkTypeOn();                                   // activate the type checking only for the next call
  theCout << "safe call sin_v(x)=" << f_v(x, res) << eol;   // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFRealScalarFunction.in", errors, "Real vector Real Scalar Function", check); 

  //! test of real vector function
  Function vf_v(vsinf_v, "vector sinus vector");           // object vector function, naming is optional
  theCout << vf_v << eol;                                    // print information of the function
  vf_v.checkTypeOn();                                    // activate the type checking only for the next call
  theCout << "safe call vsin_v(vx)=" << vf_v(vx, vvres)  << eol;       // call using operator () with type checking
  theCout << "unsafe call vsin_v(vx)=" << vf_v.vfunVR(vx) << eol; // direct call, no type checking (never)!
  vf_v.checkTypeOn();                                      // activate the type checking only for the next call
  theCout << "safe call vsin_v(x)=" << vf_v(x, vres) << eol;  // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFRealVectorFunction.in", errors, "Real vector Real Vector Function", check);

  //! test of real matrix function
  Function mf_v(msinf_v, "matrix sinus vector");           // object matrix function, naming is optional
  theCout << mf_v << eol;                                    // print information of the function
  mf_v.checkTypeOn();                                    // activate the type checking only for the next call
  theCout << "safe call msin_v(vx)=" << mf_v(vx, vmres)  << eol;       // call using operator () with type checking
  theCout << "unsafe call msin_v(vx)=" << mf_v.vfunMR(vx) << eol; // direct call, no type checking (never)!
  mf_v.checkTypeOn();                                      // activate the type checking only for the next call
  theCout << "safe call msin_v(x)=" << mf_v(x, mres) << eol;  // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFRealMatrixFunction.in", errors, "Real vector Real Matrix Function", check);

  //! test of complex scalar function
  Function cf_v(isinf_v, "isinus vector");                 // object function, naming is optional
  theCout << cf_v << eol;                                    // print information of the function
  cf_v.checkTypeOn();                                    // activate the type checking only for the next call
  theCout << "safe call isin_v(vx)=" << cf_v(vx, vces) << eol;        // call using operator () with type checking
  theCout << "unsafe call isin_v(vx)=" << cf_v.vfunSC(vx) << eol; // direct call, no type checking (never)!
  cf_v.checkTypeOn();                                    // activate the type checking only for the next call
  out << "safe call isin_v(x)=" << cf_v(x, ces) << eol;   // call for a points vector using operator 
  nbErrors+=checkValue(theCout, rootname+"/VFComplexScalarFunction.in", errors, "Real vector Complex Scalar Function", check);

  //! test of complex vector function
  Function vcf_v(visinf_v, "vector isinus vector");          // object vector function, naming is optional
  theCout << vcf_v << eol;                                     // print information of the function
  vcf_v.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call(visin_v(vx)=" << vcf_v(vx, vvces)  << eol;       // call using operator () with type checking
  theCout << "unsafe call(visin_v(vx)=" << vcf_v.vfunVC(vx) << eol; // direct call, no type checking (never)!
  vcf_v.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call(visin_v(x)=" << vcf_v(x, vces) << eol;  // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFComplexVectorFunction.in", errors, "Real vector Complex Vector Function", check);


  //! test of complex matrix function
  Function mcf_v(misinf_v, "matrix isinus vector");          // object matrix function, naming is optional
  theCout << mcf_v << eol;                                     // print information of the function
  mcf_v.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call misin_v(vx)=" << mcf_v(vx, vmces) << eol;       // call using operator () with type checking
  theCout << "unsafe call misin_v(vx)=" << mcf_v.vfunMC(vx) << eol; // direct call, no type checking (never)!
  mcf_v.checkTypeOn();                                       // activate the type checking only for the next call
  theCout << "safe call misin_v(x)=" << mcf_v(x, mces) << eol;  // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFComplexMatrixFunction.in", errors, "Real vector Complex Matrix Function", check);

  //! test of real scalar kernel function
  Function k_v(green_v, "green vector");                         // object kernel function, naming is optional
  theCout << k_v << eol;                                           // print information of the kernel function
  k_v.checkTypeOn();                                           // activate the type checking only for the next call
  theCout << "safe call green_v(vx,vx)=" << k_v(vx, vx, vres) << eol;       // call using operator () with type checking
  theCout << "unsafe call green_v(vx,vx)=" << k_v.vkerSR(vx, vx) << eol; // direct call, no type checking (never)!
  k_v.checkTypeOn();                                           // activate the type checking only for the next call
  theCout << "safe call green_v(x)=" << k_v(x, x, res) << eol;      // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFRealScalarKernel.in", errors, "Real Scalar Kernel Function", check);

  //! test of real vector kernel function
  Function vk_v(vgreen_v, "vector green vector");                  // object vector kernel function, naming is optional
  theCout << vk_v << eol;                                            // print information of the kernel function
  vk_v.checkTypeOn();                                            // activate the type checking only for the next call
  theCout << "safe call vgreen_v(vx,vx)=" << vk_v(vx, vx, vvres) << eol;      // call using operator () with type checking
  theCout << "unsafe call vgreen_v(vx,vx)=" << vk_v.vkerVR(vx, vx) << eol; // direct call, no type checking (never)!
  vk_v.checkTypeOn();                                            // activate the type checking only for the next call
  theCout << "safe call vgreen_v(x,x)=" << vk_v(x, x, vres) << eol;   // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFRealVectorKernel.in", errors, "Real Vector Kernel Function", check);


  //! test of real matrix kernel function
  Function mk_v(mgreen_v, "matrix green vector");                  // object matrix kernel function, naming is optional
  theCout << mk_v << eol;                                            // print information of the kernel function
  mk_v.checkTypeOn();                                            // activate the type checking only for the next call
  theCout << "safe call mgreen_v(vx,vx)=" << mk_v(vx, vx, vmres) << eol;      // call using operator () with type checking
  theCout << "unsafe call mgreen_v(vx,vx)=" << mk_v.vkerMR(vx, vx) << eol; // direct call, no type checking (never)!
  mk_v.checkTypeOn();                                            // activate the type checking only for the next call
  theCout << "safe call mgreen_v(x,x)=" << mk_v(x, x, mres) << eol;   // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFRealMatrixKernel.in", errors, "Real matrix Kernel Function", check);


  //! test of complex scalar kernel function
  Function ck_v(igreen_v, "igreen vector");                        // object kernel function, naming is optional
  theCout << ck_v << eol;                                            // print information of the kernel function
  ck_v.checkTypeOn();                                            // activate the type checking only for the next call
  theCout << "safe call igreen_v(vx,vx)=" << ck_v(vx, vx, vces) << eol;       // call using operator () with type checking
  theCout << "unsafe call igreen_v(vx,vx)=" << ck_v.vkerSC(vx, vx) << eol; // direct call, no type checking (never)!
  ck_v.checkTypeOn();                                            // activate the type checking only for the next call
  out << "safe call igreen_v(x,x)=" << ck_v(x, x, ces) << eol;    // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFComplexScalarKernel.in", errors, "Complex Scalar Kernel Function", check);


  //! test of complex vector kernel function
  Function vck_v(vigreen_v, "vector igreen vector");                 // object vector kernel function, naming is optional
  theCout << vck_v << eol;                                             // print information of the kernel function
  vck_v.checkTypeOn();                                             // activate the type checking only for the next call
  theCout << "safe call vigreen_v(vx,vx)=" << vck_v(vx, vx, vvces) << eol;      // call using operator () with type checking
  theCout << "unsafe call vigreen_v(vx,vx)=" << vck_v.vkerVC(vx, vx) << eol; // direct call, no type checking (never)!
  vck_v.checkTypeOn();                                             // activate the type checking only for the next call
  theCout << "safe call vigreen_v(x,x)=" << vck_v(x, x, vces) << eol;   // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFComplexVectorKernel.in", errors, "Complex Vector Kernel Function", check);


  //! test of complex matrix kernel function
  Function mck_v(migreen_v, "matrix igreen vector");                 // object matrix kernel function, naming is optional
  theCout << mck_v << eol;                                             // print information of the kernel function
  mck_v.checkTypeOn();                                             // activate the type checking only for the next call
  theCout << "safe call migreen_v(vx,vx)=" << mck_v(vx, vx, vmces) << eol;      // call using operator () with type checking
  theCout << "unsafe call migreen_v(vx,vx)=" << mck_v.vkerMC(vx, vx) << eol; // direct call, no type checking (never)!
  mck_v.checkTypeOn();                                             // activate the type checking only for the next call
  theCout << "safe call migreen_v(x,x)=" << mck_v(x, x, mces) << eol;   // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/VFComplexMatrixKernel.in", errors, "Complex Matrix Kernel Function", check);

  //! test one object function with user parameter list
  Parameter kx(3., "kx"), ky(0., "ky");              // (kx,ky) wave vector
  Parameters pars; pars << kx << ky;                 // parameter list
  Function pw(planewave, "plane wave", pars);        // object function with a parameter
  theCout << pw << eol;                                // print information of the function
  pw.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call pw(x)=" << pw(x, ces) << eol;           // call using operator () with type checking
  theCout << "unsafe call pw(x)=" << pw.funSC(x) << eol; // direct call, no type checking (never)!
  pw.checkTypeOn();                                // activate the type checking only for the next call
  theCout << "safe call pw(vx)=" << pw(vx, vces) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/OneOnjectFunction.in", errors, "one object function with user parameter list", check);

  //! test of safety of default copy constructor
  Function fun(f);                                   // new fun copy of real scalar Function f
  theCout << fun << eol;                               // print information of the function
  fun.checkTypeOn();                               // activate the type checking only for the next call
  theCout << "safe call fun(x)=" << fun(x, res) << eol;         // call using operator () with type checking
  theCout << "unsafe call fun(x)=" << fun.funSR(x) << eol; // direct call, no type checking (never)!
  fun.checkTypeOn();                               // activate the type checking only for the next call
  theCout << "safe call fun(vx)=" << fun(vx, vres) << eol; // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/SafetyDefaultCopyConstructor.in", errors, "safety of default copy constructor", check);


  //! test of safety of default assignment operator =
  fun = mck_v;                                               // new assignmant of fun=mck_v
  theCout << fun << eol;                                       // print information of the kernel function
  mck_v.checkTypeOn();                                     // activate the type checking only for the next call
  theCout << "safe call fun(vx,vx)=" << fun(vx, vx, vmces) << eol;      // call using operator () with type checking
  theCout << "unsafe call fun(vx,vx)=" << fun.vkerMC(vx, vx) << eol; // direct call, no type checking (never)!
  fun.checkTypeOn();                                       // activate the type checking only for the next call
  theCout << "safe call migreen_v(x,x)=" << fun(x, x, mces) << eol;     // call for a points vector using operator ()
  nbErrors+=checkValue(theCout, rootname+"/SafetyDefaultOperatorEgal.in", errors, "safety of default assignment operator =", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
