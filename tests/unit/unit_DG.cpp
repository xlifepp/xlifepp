/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Fourier; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file unit_DG.cpp
\author E. Lunéville
\since 7 april 2022
\date 3 april 2022

    check DG approximation
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_DG
{
Real al=1.;

Real uex(const Point& p, Parameters& pars=defaultParameters)
{
  return std::sin(al*pi_*p(1))*std::sin(al*pi_*p(2));
}

Real f(const Point& p, Parameters& pars=defaultParameters)
{
  return 2*al*al*pi_*pi_*std::sin(al*pi_*p(1))*std::sin(al*pi_*p(2));
}

Real eta=1;
Real fmu(const Point& p, Parameters& pars=defaultParameters)
{
    GeomElement* gelt=getElementP();
    if(gelt!=nullptr) return eta/gelt->measure();
    return 0.;
}

/* compute intg(somega,mean(grad(u)|_n)*jump(v)) elementary matrices
     M11 =  0.5 intg(S,grad(u1).n1*v1)  M12 =  0.5 intg(S,grad(u2).n1*v1)
     M21 = -0.5 intg(S,grad(u1).n1*v2)  M22 = -0.5 intg(S,grad(u2).n1*v2)
     intg_S grad(wj).n1*wi = 2*mes(S) * J^-t grad(tj|S).n1 * intg_[0,1]ti|S
                           =  mes(S) * J^-t grad(tj|S).n1 if i on side S else 0
*/
void meanJumpP1(BFComputationData& bfd)
{
  if(bfd.matels.size()!=4) bfd.matels= std::vector<Matrix<Real>>(4,Matrix<Real>(3,3));
  const Element *E1 = bfd.elt_u, *E2 = bfd.elt_u2;
  const GeomElement* S=bfd.sidelt;
  if(E1==nullptr) return;   //no computation, to get valuetype
  const MeshElement* melt=S->meshElement();
  GeomMapData* mds =melt ->geomMapData_p;
  if(mds==nullptr) {mds= new GeomMapData(melt);melt->geomMapData_p=mds;}
  if(mds->differentialElement==0) {mds->computeJacobianMatrix(Point(0.));mds->computeJacobianDeterminant();} // update differentialElement
  Real c = 0.25*mds->differentialElement; //  mean coef * ds * intg_[0,1] ti = 0.25*ds
  Matrix<Real> G(3,2,0.);G(1,1)=1;G(2,2)=1;G(3,1)=-1;G(3,2)=-1;  // reference shape function grad
  Number s1 = S->parentSide(0).second, s2=0;
  const MeshElement* melt1 = E1->geomElt_p->meshElement();
  GeomMapData *md1=melt1->geomMapData_p;
  if(md1==nullptr) {md1= new GeomMapData(melt1,true,true,false);melt1->geomMapData_p=md1;}
  if(md1->inverseJacobianMatrix.size()==0) {md1->computeJacobianMatrix(Point(0.,0.));md1->invertJacobianMatrix();} // update differentialElement
  Vector<Real> n1 = melt1->normalVector(s1);
  n1/=norm(n1);
  Vector<Real> G1n1 = G*md1->inverseJacobianMatrix*n1, G2n1;
  Vector<Real> t1(3,c), t2;
  t1[(s1+1)%3]=0;  // wi|s = 0 if i not on S
  bool withE2 = (E2!=nullptr && S->parentSides().size()>1);
  if(withE2)
  {
    s2 = S->parentSide(1).second;
    const MeshElement* melt2 = E2->geomElt_p->meshElement();
    GeomMapData *md2=melt2->geomMapData_p;
    if(md2==nullptr) {md2= new GeomMapData(melt2,true,true,false);melt2->geomMapData_p=md2;}
    if(md2->inverseJacobianMatrix.size()==0) {md2->computeJacobianMatrix(Point(0.,0.));md2->invertJacobianMatrix();} // update differentialElement
    G2n1=G*md2->inverseJacobianMatrix*n1;
    t2=Vector<Real>(3,c);
    t2[(s2+1)%3]=0;
  }
  else t1*=2;  //not shared {u}=u
  Number k;
  for(Number i=0;i<3;i++)
  {
    for(Number j=0;j<3;j++)
    {
      k = 3*i+j;
      bfd.matels[0][k] =  t1[i]*G1n1[j];    // M11
      if(withE2)
      {
        bfd.matels[1][k] =  t1[i]*G2n1[j];  // M12
        bfd.matels[2][k] = -t2[i]*G1n1[j];  // M21
        bfd.matels[3][k] = -t2[i]*G2n1[j];  // M22
      }
    }
  }
}

void unit_DG(int argc, char* argv[], bool check)
{
  String rootname = "unit_DG";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(5);
  //numberOfThreads(1);

  String errors;
  Number nbErrors = 0;
  Real eps=0.005;
  bool isTM= true;

  Rectangle R(_origin=Point(0.,0.),_xlength=1., _ylength=1.,_nnodes=31,_domain_name="Omega",_side_names="Gamma");
  Mesh mR(R, _shape=_triangle, _order=1, _generator=_structured, _split_direction=_alternate);
  Domain omega=mR.domain("Omega"), gamma=mR.domain("Gamma");
  Domain somega=sides(omega);
  thePrintStream<<somega<<eol;
  Domain isomega=somega-gamma;
  thePrintStream<<isomega<<eol;
  Interpolation int1(Lagrange,standard,1,H1),  int0(Lagrange,standard,1,L2);
  Space V1(omega,int1,"V1");
  thePrintStream<<V1;
  Space V(omega,int0,"V");
  thePrintStream<<V;
  Unknown u(V, _name="u");TestFunction v(u, _name="v");

  //check DG Matrix
  TermMatrix M(intg(omega,u*v), _name="M");
  TermVector un(u,omega,1.,_name="un");
  thePrintStream<<"(M*un|un)="<<(M*un|un)<<eol;
  TermMatrix K(intg(omega,grad(u)|grad(v)), _name="K");
  TermVector tx(u,omega,_x);
  thePrintStream<<"(K*tx|tx)="<<(K*tx|tx)<<eol;
  BilinearForm auv=intg(somega,jump(u)*jump(v));
  thePrintStream<<auv<<eol<<auv.asString()<<eol;
  TermMatrix J(auv, _name="J");
  thePrintStream<<J<<eol;
  BilinearForm auvi=intg(isomega,jump(u)*jump(v));
  thePrintStream<<auvi<<eol<<auvi.asString()<<eol;
  TermMatrix Ji(auvi, _name="Ji");
  thePrintStream<<Ji<<eol;
  thePrintStream<<"Ji*U="<<Ji*un<<eol;
  TermMatrix MJ(intg(somega,mean(grad(u)|_n)*jump(v)), _name="MJ");
  thePrintStream<<MJ<<eol;
  BilinearForm umj=userBilinearForm(somega,u,v,meanJumpP1,_DGComputation,_noSymmetry, true, true);
  TermMatrix UMJ(umj, _name="UMJ");
  thePrintStream<<UMJ<<eol;
  theCout<<"|MJ-UMJ|="<<norm2(MJ-UMJ)<<eol;

  // solve Laplace problem -lap(u)=f on Omega, u=0 on Gamma (boundary of Omega)
  // Baumann-Oden method (see XLiFE++ doc) (weekly stable, not convergent in P1)
  BilinearForm abo=intg(omega,grad(u)|grad(v))-intg(somega,mean(grad(u)|_n)*jump(v))+intg(somega,jump(u)*mean(grad(v)|_n));
  TermMatrix Abo(abo, _name="Abo");
  TermVector F(intg(omega,f*v), _name="F");
  TermVector U=directSolve(Abo,F);
  TermVector Uex(u,omega,uex);
  saveToFile("Ubo",U,_format=_vtu);
  saveToFile("Uex",Uex,_format=_vtu);
  TermVector E=U-Uex;
  theCout<<"Baumann-Oden : |U-uex|L2 = "<<std::sqrt(M*E|E)<<eol;
  TermVector Ff(u, omega, f, _name="Ff");
  TermVector F2=M*Ff;
  TermVector U2=directSolve(Abo,F2);
  E=U2-Uex;
  theCout<<"               |U2-uex|L2= "<<std::sqrt(M*E|E)<<eol;

  // NIPG method (see XLiFE++ doc) (stable, convergent in P1)
  Function mu(fmu); mu.require("element");
  BilinearForm ani=intg(omega,grad(u)|grad(v))-intg(somega,mean(grad(u)|_n)*jump(v))+intg(somega,jump(u)*mean(grad(v)|_n))+intg(somega,mu*jump(u)*jump(v));
  TermMatrix Ani(ani, _name="Ani");
  U=directSolve(Ani,F);
  saveToFile("Uni",U,_format=_vtu);
  E=U-Uex;
  theCout<<"NIPG : |U-uex|L2 = "<<std::sqrt(M*E|E)<<eol;
  U2=directSolve(Ani,F2);
  E=U2-Uex;
  theCout<<"       |U2-uex|L2= "<<std::sqrt(M*E|E)<<eol;

  // IP method (see XLiFE++ doc) (stable, adjoint consistent, convergent in P1)
  BilinearForm aip=intg(omega,grad(u)|grad(v))-intg(somega,mean(grad(u)|_n)*jump(v))-intg(somega,jump(u)*mean(grad(v)|_n))+intg(somega,mu*jump(u)*jump(v));
  TermMatrix Aip(aip, _name="Aip");
  U=directSolve(Aip,F);
  saveToFile("Uip",U,_format=_vtu);
  E=U-Uex;
  theCout<<"  IP : |U-uex|L2 = "<<std::sqrt(M*E|E)<<eol;
  U2=directSolve(Aip,F2);
  E=U2-Uex;
  theCout<<"       |U2-uex|L2= "<<std::sqrt(M*E|E)<<eol;
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
