/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_SymbolicFunction.cpp
  \author E. Lunéville
  \since 11 feb 2017
  \date 11 feb 2017

	Low level tests of Function class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>

using namespace xlifepp;

namespace unit_SymbolicFunction {

//============================================================================
// test symbolic functions
//============================================================================
void unit_SymbolicFunction(int argc, char* argv[], bool check)
{
  String rootname = "unit_SymbolicFunction";
  trace_p->push(rootname);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;
  Real cm=1000.0*theEpsilon;

  //!test constructors/destructor
  SymbolicFunction fs1(x_1);
  theCout<<"fs1 = "<<fs1<<eol;
  SymbolicFunction fs2(fs1,_sqrt, 2.);
  theCout<<"fs2 = "<<fs2<<eol;
  SymbolicFunction fs3(fs1,fs2,_plus);
  theCout<<"fs3 = "<<fs3<<eol;
  SymbolicFunction fs4=fs3;
  theCout<<"fs4 = "<<fs4<<eol;
  fs1=fs4;
  theCout<<"fs1=fs4, fs1 = "<<fs1<<eol;
  SymbolicFunction* fsp= new SymbolicFunction(fs1);
  fs2=*fsp; delete fsp;
  theCout<<"copy pointer and delete pointer, fs2 = "<<fs2<<eol;
  nbErrors+=checkValue(theCout, rootname+"/Constructors.in", errors, "constructors/destructor", check);

  //!test syntax
  theCout<<"==================== algebraic symbolic functions ===================="<<eol;
  SymbolicFunction fs;
  fs = SymbolicFunction(3);
  theCout<<" fs = SymbolicFunction(3); -> "<<fs<<" fs(1) - f(1) ="<<fs(1)-3;
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol;

  fs = x_1+x_2-1;
  theCout<<" fs = x_1+x_2-1; -> "<<fs <<"  fs(1,0)-f(1,0) = "<<fs(1,0);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs = 2*x_1-3*x_2;
  theCout<<" fs = 2*x_1-3*x_2; -> "<<fs<<"  fs(1,0)-f(1,0) = "<<fs(1,0)-2;
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs =5*(2*x_1-3*x_2);
  theCout<<" fs = 5*(2*x_1-3*x_2); -> "<<fs<<"     fs(1,0)-f(1,0) = "<<fs(1,0)-10;
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs = 4*sqrt(2*x_1-3*x_2)/x_2;
  theCout<<" fs = 4*sqrt(2*x_1-3*x_2)/x_2; -> "<<fs<<"  fs(2,0.5)-f(2,0.5) = "<<fs(2,0.5)-(4*std::sqrt(2.5)/0.5);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs = pow(squared(x_1), 0.5);
  theCout<<" fs = pow(squared(x_1), 0.5); -> "<<fs<<" fs(3) - f(3) ="<<fs(3)-3;
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol;

  nbErrors+=checkValue(theCout, rootname+"/AlgebraicSF.in", errors, "= algebraic symbolic functions =", check);

  theCout<<"==================== transcendent symbolic functions ===================="<<eol;
  fs = -sin(x_1);
  theCout<<" fs = -sin(x_1); -> "<<fs<<" |fs(1) - f(1)| ="<<roundToZero(std::abs(fs(1)+std::sin(1)), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol;

  fs = +sin(-x_1);
  theCout<<" fs = +sin(-x_1); -> "<<fs<<" |fs(1) - f(1)| ="<<roundToZero(std::abs(fs(1)+std::sin(1)), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol;

  fs = 2*sqrt(x_1*x_2);
  theCout<<" fs = 2*sqrt(x_1*x_2); -> "<<fs<<" |fs(1,2) - f(1,2)| ="<<roundToZero(std::abs(fs(1,2)-2*std::sqrt(2)), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1);
  theCout<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs = cos(x_2)-2*sqrt(x_1*x_2);
  theCout<<" fs = cos(x_2)-2*sqrt(x_1*x_2); -> "<<fs<<" |fs(1,2) - f(1,2)| ="<<roundToZero(std::abs(fs(1,2)-(std::cos(2)-2*std::sqrt(2))), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs = abs(x_1)*sin(x_2);
  theCout<<" fs = abs(x_1)*sin(x_2); -> "<<fs <<" |fs(1)-f(1)| = "<<roundToZero(std::abs(fs(1,1)-(abs(1)*sin(1))), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs = (1.-sin(x_1))*(cos(1.+x_1)/5 + 2*sin(3*x_1));
  theCout<<" fs = (1.-sin(x_1))*(cos(1.+x_1)/5 + 2*sin(3*x_1)); -> "<<fs
      <<" |fs(1)-f(1)| = "<<roundToZero(std::abs(fs(1)-(1.-sin(1))*(cos(2)/5 + 2*sin(3))), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  fs = sin(cos(sqrt(abs(log(x_1*x_1+x_2*x_2+x_3*x_3)))));
  theCout<<" fs = sin(cos(sqrt(abs(log(x_1*x_1+x_2*x_2+x_3*x_3))))); -> "<<fs
      <<" |fs(1,0,1)-f(1,0,1)| = "<<roundToZero(std::abs(fs(1,0,1)-sin(cos(sqrt(abs(log(2)))))), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol<<"    dx3(fs) = "<<derivative(fs,_x3)<<eol;

  fs = (1-x_1+cos(x_2))^3;
  theCout<<" fs = (1-x_1+cos(x_2))^3; -> "<<fs<<" |fs(_i,0) - f(i,0)| = "<<roundToZero(std::abs(fs(i_,0)-std::pow(2.-i_,3)), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2);
  theCout<<eol<<"    dx1(dx1(fs)) = "<<derivative(derivative(fs,_x1),_x1)<<eol;

  fs = power(sin(x_1),cos(x_2));
  theCout<<" fs = power(sin(x_1),cos(x_2)); -> "<<fs<<" |fs(pi/3,pi/6) - f(pi/3,pi/6)| = "<<roundToZero(std::abs(fs(pi_/3,pi_/6)-std::pow(sin(pi_/3),cos(pi_/6))), cm);
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;

  nbErrors+=checkValue(theCout, rootname+"/TranscendentSF.in", errors, "= Transcendent symbolic functions =", check);


  theCout<<"==================== boolean symbolic functions ===================="<<eol;
  fs= x_1 < 1;
  theCout<<" fs =  x_1 < 1; -> "<<fs<<" fs(2) = "<<fs(2)<<eol;

  fs= 1 <= x_1;
  theCout<<" fs = 1 <= x_1; -> "<<fs<<" fs(2) = "<<fs(2)<<eol;

  fs=(x_1+x_2)<=1 && exp(x_1)>2;
  theCout<<" fs = (x_1+x_2)<1 && exp(x_1)>3; -> "<<fs<<" fs(1,0) = "<<fs(1,0)<<eol;

  fs=(x_1+x_2)^3;
  theCout<<" fs = (x_1+x_2)^3; -> "<<fs<<" fs(1,2) - f(1,2) = "<<fs(1,2)-27<<eol;

  fs = (x_1<=0)*exp(x_1)+(x_1>0);
  theCout<<" fs = (x_1<=0)*exp(x_1)+(x_1>0); -> "<<fs<<" fs(1) - f(1) = "<<fs(1)-1<<" fs(-1) - f(-1) = "<<fs(-1)-std::exp(-1)<<eol;

  fs=sign(x_1)*x_1;
  theCout<<" fs = sign(x_1)*x1; -> "<<fs<<" fs(-1) = "<<fs(-1)<<eol;

  fs=x_1*abs(x_1+0.5*x_2);
  theCout<<" fs=x_1*abs(x_1+0.5*x_2); -> "<<fs<<" fs(-2,1) -f(-2,1) = "<<fs(-2,1)+3;
  theCout<<eol<<"    dx1(fs) = "<<derivative(fs,_x1)<<eol<<"    dx2(fs) = "<<derivative(fs,_x2)<<eol;
  theCout<<"    dx1(fs) = "<<derivative(fs,_x1).asString()<<eol;

  nbErrors+=checkValue(theCout, rootname+"/BooleanSF.in", errors, "= Boolean symbolic functions =", check);

  theCout<<"==================== r,theta,phi symbolic functions ===================="<<eol;
  fs = (1-r_*cos(2.*theta_))^3;
  theCout<<" fs =  (1-r_*cos(2.*theta_))^3; -> "<<fs<<" fs(0.5,0.5) = "<<fs(0.5,0.5)<<eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
