/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_randomGenerators.cpp
  \author E. Lunéville
  \since 20 jun 2020
  \date 20 jun 2020

	Low level tests of random generator functions
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>

using namespace xlifepp;

namespace unit_randomGenerators {

void checkMoments(const String& title, const std::vector<Real>& xs, Real mean, Real var, String& errors,  Number& nbErrors, Real tol)
{
  std::vector<Real>::const_iterator it;
  Number n=xs.size();
  Real e=std::accumulate(xs.begin(),xs.end(), 0.)/n;  // empirical average
  Real s=0.;
  for( it=xs.begin();it!=xs.end();++it)  s+=(*it-e)*(*it-e);
  s/=(n-1); //unbiaised variance
  Real scalemean=std::max(mean,1.);
  nbErrors+=checkValue(e, mean, scalemean*tol, errors,title+", empirical average");
  nbErrors+=checkValue(s, var,  var*tol, errors,title+", unbiased variance");
}


void unit_randomGenerators(int argc, char* argv[], bool check)
{
  String rootname = "unit_randomGenerators";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);

  Number nbErrors = 0;
  String errors;
  Real tol=0.01;

  Number n=10000000;
  Real a=0.,b=1.;
  std::vector<Real> xs(n);
  Real ave=(a+b)/2., var=(b-a)*(b-a)/12.;

  //!test default uniform generator on [0,1[
  uniformDistribution(&xs[0],n);
  checkMoments("uniform generator on [0,1[",xs,ave,var,errors,nbErrors,tol);

  //!test C uniform generator on [0,1[ (based on rand())
  uniformDistributionC(&xs[0],n);
  checkMoments("C uniform generator on [a,b[",xs,ave,var,errors,nbErrors,tol);

  //!test default uniform generator on [a,b[
  a=-1.;b=1.;ave=(a+b)/2;var=(b-a)*(b-a)/12.;
  uniformDistribution(&xs[0],a,b,n);
  checkMoments("C uniform generator on [a,b[",xs,ave,var,errors,nbErrors,tol);

  //!test C uniform generator on [a,b[ (based on rand())
  uniformDistributionC(&xs[0],a,b,n);
  checkMoments("C uniform generator on [a,b[",xs,ave,var,errors,nbErrors,tol);

  //!test default normal distribution (0.,1.)
  Real mu=0.,sigma=1.;
  normalDistribution(&xs[0],mu,sigma,n);
  checkMoments("normal distribution (0,1)",xs,mu,sigma,errors,nbErrors,tol);

  //!test C normal distribution (0.,1.) using Marsiglia (default in C mode)
  normalDistributionC(&xs[0],mu,sigma,n);
  checkMoments("normal distribution (0,1), using Marsiglia generator",xs,mu,sigma*sigma,errors,nbErrors,tol);

  //!test C normal distribution (0.,1.) using Box-Muller
  normalDistributionC(&xs[0],mu,sigma,_BoxMullerGenerator,n);
  checkMoments("normal distribution (0,1), using Box-Muller generator",xs,mu,sigma*sigma,errors,nbErrors,tol);

  //!test default normal distribution (2.,4.)
  mu=2.,sigma=4.;
  normalDistribution(&xs[0],mu,sigma,n);
  checkMoments("normal distribution (2,4)",xs,mu,sigma*sigma,errors,nbErrors,tol);

  //!test C normal distribution (0.,1.) using Marsiglia (default in C mode)
  normalDistributionC(&xs[0],mu,sigma,n);
  checkMoments("normal distribution (2,4), using Marsiglia generator",xs,mu,sigma*sigma,errors,nbErrors,tol);

  //!test C normal distribution (0.,1.) using Box-Muller
  normalDistributionC(&xs[0],mu,sigma,_BoxMullerGenerator,n);
  checkMoments("normal distribution (2,4), using Box-Muller generator",xs,mu,sigma*sigma,errors,nbErrors,tol);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
