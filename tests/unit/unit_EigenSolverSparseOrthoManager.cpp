/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!
  \file unit_EigenSolverSparseOrthoManager.cpp
  \author ManhHa NGUYEN
  \since 23 March 2013
  \date 22 May 2013

  Tests of orthogonalization method for sparse eigen problem.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_EigenSolverSparseOrthoManager {
// this is the tolerance that all tests are performed against
const Real tol = 1.0e-12;
const Real atol = 10;

// some forward declarations
template<typename ScalarType, typename MV>
int testProject(SmartPtr<BasicOutputManager<ScalarType> >& theCout,
                SmartPtr<OrthoManager<ScalarType,MV> > om, SmartPtr<MV> S,
                SmartPtr<MV> X1, SmartPtr<MV> X2);
template<typename ScalarType, typename MV>
int testNormalize(SmartPtr<BasicOutputManager<ScalarType> >& theCout,
                  SmartPtr<OrthoManager<ScalarType,MV> > om, SmartPtr<MV> S);
template<typename ScalarType, typename MV>
int testProjectAndNormalize(SmartPtr<BasicOutputManager<ScalarType> >& theCout,
                SmartPtr<OrthoManager<ScalarType,MV> > om, SmartPtr<MV> S,
                SmartPtr<MV> X1, SmartPtr<MV> X2);

template<typename ScalarType, typename MVT, typename MV>
Real MVDiff(const MV& X, const MV& Y)
{
  typedef NumTraits<ScalarType> SCT;
  const ScalarType ONE = SCT::one();

  const int sizeX = MVT::getNumberVecs(X);
  MatrixEigenDense<ScalarType> xTmx(sizeX,sizeX);

  // tmp <- X
  SmartPtr<MV> tmp = MVT::cloneCopy(X);

  // tmp <- tmp - Y
  MVT::mvAddMv(-ONE,Y,ONE,*tmp,*tmp);
  MVT::mvTransMv(ONE,*tmp,*tmp,xTmx);

  Real err = 0;
  for (int i=0; i<sizeX; i++) {
    err += SCT::magnitude(xTmx.coeff(i,i));
  }
  return SCT::magnitude(SCT::squareroot(err));
}

template<typename ScalarType, typename MV>
void testOrthoMgr(SmartPtr<BasicOutputManager<ScalarType> >& theCout,
                 SmartPtr<OrthoManager<ScalarType,MV> > om,
                 int dim, int sizeS,
                 int sizeX1, int sizeX2);

void unit_EigenSolverSparseOrthoManager(int argc, char* argv[], bool check)
{
  String rootname = "unit_EigenSolverSparseOrthoManager";
  trace_p->push(rootname);
  std::stringstream theCout; // string stream receiving results
  theCout.precision(testPrec);
  verboseLevel(10);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  typedef Complex                              ST;
  typedef MultiVec<ST>                         MV;
  typedef Operator<ST>                         OP;

  typedef Real                                 RealST;

  bool verbose = false;

  int dim = 1280; //3200;
  int sizeS  = 5; //5
  int sizeX1 = 11;  //11 // MUST: sizeS + sizeX1 + sizeX2 <= elements[0]-1
  int sizeX2 = 13;  //13 // MUST: sizeS + sizeX1 + sizeX2 <= elements[0]-1

  // below we will assume that sizeX1 > sizeX2
  // this does not effect our testing, since we will test P_{X1,Y1} P_{X2,Y2} as well as P_{X2,Y2} P_{X1,Y1}
  // however, is does allow us to simplify some logic
  if (sizeX1 < sizeX2) {
    std::swap(sizeX1,sizeX2);
  }

  SmartPtr<std::ostream> osp = SmartPtr<std::ostream>(&theCout, false);

  const std::string cMatrixSym(rootname+"/mhd1280b.coo");
  LargeMatrix<ST> cMat(inputsPathTo(cMatrixSym), _coo, 1280, _cs, _symmetric);
  //  Problem information
  SmartPtr< const LargeMatrixAdapter<LargeMatrix<ST>, ST> > cM(new LargeMatrixAdapter<LargeMatrix<ST>, ST>(&cMat));

  // declare an theCoutput manager for handling local theCoutput
  SmartPtr<BasicOutputManager<ST> > cplxOM;

  cplxOM = SmartPtr<BasicOutputManager<ST> >(new BasicOutputManager<ST>(3, osp, 0) );
  if (verbose) {
    // theCoutput in this driver will be sent to _warningsEigen
    cplxOM->setVerbosity(_debugEigen);
  }

  SmartPtr<OrthoManager<ST,MV> > om;
  om = SmartPtr<SVQBOrthoManager<ST,MV,OP> >(new SVQBOrthoManager<ST,MV,OP>(cM, false) );
  testOrthoMgr(cplxOM, om, dim, sizeS, sizeX1, sizeX2);

  //om = SmartPtr<BasicOrthoManager<ST,MV,OP> >(new BasicOrthoManager<ST,MV,OP>(cM) );

  SmartPtr<BasicOutputManager<RealST> > realOM;
  
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

////////////////////////////////////////////////////////////////////////////
template<typename ST, typename MV>
int testProjectAndNormalize(SmartPtr<BasicOutputManager<ST> >& theCout,
                            SmartPtr<OrthoManager<ST,MV> > om,
                            SmartPtr<MV> S,
                            SmartPtr<MV> X1, SmartPtr<MV> X2)
{
  typedef NumTraits<ST> SCT;
  typedef typename SCT::magnitudeType MT;
  typedef MultiVecTraits<ST,MV> MVT;
  const ST ONE = SCT::one();
  const MT ZERO = SCT::magnitude(SCT::zero());
  const int sizeS = MVT::getNumberVecs(*S);
  const int sizeX1 = MVT::getNumberVecs(*X1);
  const int sizeX2 = MVT::getNumberVecs(*X2);
  int numerr = 0;
  std::ostringstream stheCout;

  //
  // theCoutput tests:
  //   <S_theCout,S_theCout> = I
  //   <S_theCout,X1> = 0
  //   <S_theCout,X2> = 0
  //   S_in = S_theCout B + X1 C1 + X2 C2
  //
  // we will loop over an integer specifying the test combinations
  // the bit pattern for the different tests is listed in parenthesis
  //
  // for the projectors, test the following combinations:
  // none              (00)
  // P_X1              (01)
  // P_X2              (10)
  // P_X1 P_X2         (11)
  // P_X2 P_X1         (11)
  // the latter two should be tested to give the same answer
  //
  // for each of these, we should test with C1, C2 and B
  //
  // if hasM:
  // with and without MX1   (1--)
  // with and without MX2  (1---)
  // with and without MS  (1----)
  //
  // as hasM controls the upper level bits, we need only run test cases 0-3 if hasM==false
  // otherwise, we run test cases 0-31
  //

  int numtests;
  numtests = 4;

  // test ortho error before orthonormalizing
  if (X1 != _smPtrNull) {
    MT err = om->orthogError(*S,*X1);
    stheCout << "   || <S,X1> || before     : " << err << "\n";
  }
  if (X2 != _smPtrNull) {
    MT err = om->orthogError(*S,*X2);
    stheCout << "   || <S,X2> || before     : " << err << "\n";
  }

  for (int t=0; t<numtests; t++) {

    std::vector<SmartPtr<const MV> > theX;
    SmartPtr<MatrixEigenDense<ST> > B = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeS,sizeS) );
    std::vector<SmartPtr<MatrixEigenDense<ST> > > C;
    if (t == 3 ) {
      // neither <X1,Y1> nor <X2,Y2>
      // C, theX and theY are already empty
    }
    else if (t == 1 ) { //((t && 3) == 1 ) {
      // X1
      theX.resize(1);
      theX[0] = X1;
      C.resize(1);
      C[0] =  SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX1,sizeS));
    }
    else if (t == 2 ) { //((t && 3) == 2 ) {
      // X2
       theX.resize(1);
       theX[0] = (X2);
       C.resize(1);
       C[0] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX2,sizeS));
    }
    else {
      // X1 and X2, and the reverse.
        theX.resize(2);
        theX[0] = X1;
        theX[1] = X2;
        C.resize(2);
        C[0] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX1,sizeS));
        C[1] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX2,sizeS));
//      theX = tuple(X1,X2);
//      C = tuple(SmartPtr(new MatrixEigenDense<ST>(sizeX1,sizeS)),
//                 SmartPtr(new MatrixEigenDense<ST>(sizeX2,sizeS)) );
    }

      // call rtheCoutine
      // if (t && 3) == 3, {
      //    call with reversed input: X2 X1
      // }
      // test all theCoutputs for correctness
      // test all theCoutputs for equivalence

      // here is where the theCoutputs go
      std::vector<SmartPtr<const MV> > S_theCouts;
      std::vector<std::vector<SmartPtr<MatrixEigenDense<ST> > > > C_theCouts;
      std::vector<SmartPtr<MatrixEigenDense<ST> > > B_theCouts;
      SmartPtr<MV> Scopy;
      std::vector<int> ret_theCout;

      // copies of S,MS
      Scopy = MVT::cloneCopy(*S);
      // randomize this data, it should be overwritten
      B->random();
      for (Number i=0; i<C.size(); i++) {
        C[i]->random();
      }
      // run test

      int ret = om->projectAndNormalize(*Scopy,theX,C,B);
      stheCout << "projectAndNormalize() returned rank " << ret << "\n";
      if (ret == 0) {
        stheCout << "   Cannot continue." << "\n";
        numerr++;
        break;
      }
      ret_theCout.push_back(ret);
      // projectAndNormalize() is only required to return a
      // basis of rank "ret"
      // this is what we will test:
      //   the first "ret" columns in Scopy
      //   the first "ret" rows in B
      // save just the parts that we want
      // we allocate S and MS for each test, so we can save these as views
      // however, save copies of the C and B
      if (ret < sizeS) {
        std::vector<int> ind(ret);
        for (int i=0; i<ret; i++) {
          ind[i] = i;
        }
        S_theCouts.push_back(MVT::cloneView(*Scopy,ind) );
        B_theCouts.push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*B) ) );
      }
      else {
        S_theCouts.push_back(Scopy );
        B_theCouts.push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*B) ) );
      }
      C_theCouts.push_back(std::vector<SmartPtr<MatrixEigenDense<ST> > >(0) );
      if (C.size() > 0) {
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[0]) ) );
      }
      if (C.size() > 1) {
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[1]) ) );
      }

      // do we run the reversed input?
      if ( t == 3 ) {
        theX.resize(2);
        theX[0] = X2;
        theX[1] = X1;
        C.resize(2);
        C[0] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX2,sizeS));
        C[1] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX1,sizeS));
        // copies of S,MS
        Scopy = MVT::cloneCopy(*S);
        // randomize this data, it should be overwritten
        B->random();
        for (Number i=0; i<C.size(); i++) {
          C[i]->random();
        }
        // flip the inputs
//        std::swap(theX[1], theX[0]);
        //theX = tuple(theX[1], theX[0] );
        // run test
        ret = om->projectAndNormalize(*Scopy,theX,C,B);
        stheCout << "projectAndNormalize() returned rank " << ret << "\n";
        if (ret == 0) {
          stheCout << "   Cannot continue." << "\n";
          numerr++;
          break;
        }
        ret_theCout.push_back(ret);
        // projectAndNormalize() is only required to return a
        // basis of rank "ret"
        // this is what we will test:
        //   the first "ret" columns in Scopy
        //   the first "ret" rows in B
        // save just the parts that we want
        // we allocate S and MS for each test, so we can save these as views
        // however, save copies of the C and B
        if (ret < sizeS) {
          std::vector<int> ind(ret);
          for (int i=0; i<ret; i++) {
            ind[i] = i;
          }
          S_theCouts.push_back(MVT::cloneView(*Scopy,ind) );
          B_theCouts.push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*B) ) );
        }
        else {
          S_theCouts.push_back(Scopy );
          B_theCouts.push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*B) ) );
        }
        C_theCouts.push_back(std::vector<SmartPtr<MatrixEigenDense<ST> > >() );
        // reverse the Cs to compensate for the reverse projectors
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[1]) ) );
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[0]) ) );
        // flip the inputs back
        std::swap(theX[1], theX[0] );
        //theX = tuple(theX[1], theX[0] );
      }


      // test all theCoutputs for correctness
      for (Number o=0; o<S_theCouts.size(); o++) {
        // S^T M S == I
        {
          MT err = om->orthonormError(*S_theCouts[o]);
          if (err > tol) {
            stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
          stheCout << "   || <S,S> - I || after  : " << err << "\n";
        }
        // S_in = X1*C1 + C2*C2 + S_theCout*B
        {
          SmartPtr<MV> tmp = MVT::clone(*S,sizeS);
          MVT::mvTimesMatAddMv(ONE,*S_theCouts[o],*B_theCouts[o],ZERO,*tmp);
          if (C_theCouts[o].size() > 0) {
            MVT::mvTimesMatAddMv(ONE,*theX[0],*C_theCouts[o][0],ONE,*tmp);
            if (C_theCouts[o].size() > 1) {
              MVT::mvTimesMatAddMv(ONE,*theX[1],*C_theCouts[o][1],ONE,*tmp);
            }
          }
          MT err = MVDiff<ST, MVT>(*tmp,*S);
          if (err > atol*tol) {
            stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
          stheCout << "  " << t << "|| S_in - X1*C1 - X2*C2 - S_theCout*B || : " << err << "\n";
        }
        // <X1,S> == 0
        if (theX.size() > 0 && theX[0] != _smPtrNull) {
          MT err = om->orthogError(*theX[0],*S_theCouts[o]);
          if (err > tol) {
            stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
          stheCout << "  " << t << "|| <X[0],S> || after      : " << err << "\n";
        }
        // <X2,S> == 0
        if (theX.size() > 1 && theX[1] != _smPtrNull) {
          MT err = om->orthogError(*theX[1],*S_theCouts[o]);
          if (err > tol) {
            stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
          stheCout << "  " << t << "|| <X[1],S> || after      : " << err << "\n";
        }
      }
  } // test for

  MsgEigenType type = _warningsEigen;
  if (numerr>0) type = _errorsEigen;
  theCout->stream(type) << stheCout.str();
  theCout->stream(type) << "\n";
  
            std::cout << " testProjectAndNormalizereurn numerr:"<< numerr;
  return numerr;
}

////////////////////////////////////////////////////////////////////////////
template<typename ST, typename MV>
int testNormalize(SmartPtr<BasicOutputManager<ST> >& theCout, SmartPtr<OrthoManager<ST,MV> > om, SmartPtr<MV> S)
{
  typedef NumTraits<ST> SCT;
  typedef typename SCT::magnitudeType MT;
  typedef MultiVecTraits<ST,MV> MVT;
  const ST ONE = SCT::one();
  const MT ZERO = SCT::magnitude(SCT::zero());
  const int sizeS = MVT::getNumberVecs(*S);
  int numerr = 0;
  std::ostringstream stheCout;

  //
  // theCoutput tests:
  //   <S_theCout,S_theCout> = I
  //   S_in = S_theCout B
  //
  // we will loop over an integer specifying the test combinations
  // the bit pattern for the different tests is listed in parenthesis
  //
  // for each of the following, we should test B
  //
  // if hasM:
  // with and without MS  (1)
  //
  // as hasM controls the upper level bits, we need only run test case 0 if hasM==false
  // otherwise, we run test cases 0-1
  //
  int numtests;
  numtests = 1;

  for (int t=0; t<numtests; t++) {

    // allocate the data separately from B, to allow it to persist as B is delete/reallocated
    SmartPtr<MatrixEigenDense<ST> > B = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeS,sizeS) );

      // call rtheCoutine
      // test all theCoutputs for correctness

      // here is where the theCoutputs go
      SmartPtr<MV> Scopy;
      int ret;

      // copies of S,MS
      Scopy = MVT::cloneCopy(*S);
      // randomize this data, it should be overwritten
      B->random();

      // run test
      ret = om->normalize(*Scopy,B);
      stheCout << "normalize() returned rank " << ret << "\n";
      if (ret == 0) {
        stheCout << "   Cannot continue." << "\n";
        numerr++;
        break;
      }
      // normalize() is only required to return a
      // basis of rank "ret"
      // this is what we will test:
      //   the first "ret" columns in Scopy
      //   the first "ret" rows in B
      // get pointers to the parts that we want
      if (ret < sizeS) {
        std::vector<int> ind(ret);
        for (int i=0; i<ret; i++) {
          ind[i] = i;
        }
        Scopy = MVT::cloneViewNonConst(*Scopy,ind);
      }

      // test all theCoutputs for correctness
      // S^T M S == I
      {
        MT err = om->orthonormError(*Scopy);
        if (err > tol) {
          stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
          numerr++;
        }
        stheCout << "   || <S,S> - I || after  : " << err << "\n";
      }

      // S_in = S_theCout*B
      {
        SmartPtr<MV> tmp = MVT::clone(*S,sizeS);
        MVT::mvTimesMatAddMv(ONE,*Scopy,*B,ZERO,*tmp);
        MT err = MVDiff<ST, MVT>(*tmp,*S);
        if (err > atol*tol) {
          stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
          numerr++;
        }
        stheCout << "  " << t << "|| S_in - S_theCout*B || : " << err << "\n";
      }
  } // test for

    MsgEigenType type = _warningsEigen;
    if (numerr>0) type = _errorsEigen;
  theCout->stream(type) << stheCout.str();
  theCout->stream(type) << "\n";

            std::cout << " testNormalize numerr:"<< numerr;
  return numerr;
}

////////////////////////////////////////////////////////////////////////////
template<typename ST, typename MV>
int testProject(SmartPtr<BasicOutputManager<ST> >& theCout, SmartPtr<OrthoManager<ST,MV> > om,
                SmartPtr<MV> S, SmartPtr<MV> X1, SmartPtr<MV> X2)
{
  typedef NumTraits<ST> SCT;
  typedef typename SCT::magnitudeType MT;
  typedef MultiVecTraits<ST,MV> MVT;
  const ST ONE = SCT::one();
  const int sizeS = MVT::getNumberVecs(*S);
  const int sizeX1 = MVT::getNumberVecs(*X1);
  const int sizeX2 = MVT::getNumberVecs(*X2);
  int numerr = 0;
  std::ostringstream stheCout;

  //
  // theCoutput tests:
  //   <S_theCout,X1> = 0
  //   <S_theCout,X2> = 0
  //   S_in = S_theCout + X1 C1 + X2 C2
  //
  // we will loop over an integer specifying the test combinations
  // the bit pattern for the different tests is listed in parenthesis
  //
  // for the projectors, test the following combinations:
  // none              (00)
  // P_X1              (01)
  // P_X2              (10)
  // P_X1 P_X2         (11)
  // P_X2 P_X1         (11)
  // the latter two should be tested to give the same answer
  //
  // for each of these, we should test
  // with C1 and C2
  //
  // if hasM:
  // with and without MX1   (1--)
  // with and without MX2  (1---)
  // with and without MS  (1----)
  //
  // as hasM controls the upper level bits, we need only run test cases 0-3 if hasM==false
  // otherwise, we run test cases 0-31
  //

  int numtests;
  numtests = 8;

  // test ortho error before orthonormalizing
  if (X1 != _smPtrNull) {
    MT err = om->orthogError(*S,*X1);
    stheCout << "   || <S,X1> || before     : " << err << "\n";
  }
  if (X2 != _smPtrNull) {
    MT err = om->orthogError(*S,*X2);
    stheCout << "   || <S,X2> || before     : " << err << "\n";
  }

  for (int t=0; t<numtests; t++) {
    std::vector<SmartPtr<const MV> > theX;
    std::vector<SmartPtr<MatrixEigenDense<ST> > > C;
    if (t == 0 ) { //((t && 3) == 0 ) {
      // neither X1 nor X2
      // C and theX are already empty
    }
    else if (t == 1 ) { //((t && 3) == 1 ) {
      // X1
      theX.resize(1);
      theX[0] = X1;
      C.resize(1);
      C[0] =  SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX1,sizeS));
    }
    else if (t == 2 ) { //((t && 3) == 2 ) {
      // X2
       theX.resize(1);
       theX[0] = (X2);
       C.resize(1);
       C[0] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX2,sizeS));
    }
    else {
      // X1 and X2, and the reverse.
        theX.resize(2);
        theX[0] = X1;
        theX[1] = X2;
        C.resize(2);
        C[0] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX1,sizeS));
        C[1] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX2,sizeS));
//      theX = tuple(X1,X2);
//      C = tuple(SmartPtr(new MatrixEigenDense<ST>(sizeX1,sizeS)),
//                 SmartPtr(new MatrixEigenDense<ST>(sizeX2,sizeS)) );
    }

      //    call with reversed input: X2 X1
      // test all theCoutputs for correctness
      // test all theCoutputs for equivalence
      // here is where the theCoutputs go
      std::vector<SmartPtr<MV> > S_theCouts;
      std::vector<std::vector<SmartPtr<MatrixEigenDense<ST> > > > C_theCouts;
      SmartPtr<MV> Scopy;

      // copies of S,MS
      Scopy = MVT::cloneCopy(*S);
      // randomize this data, it should be overwritten
      for (Number i=0; i<C.size(); i++) {
        C[i]->random();
      }
      // run test
      om->project(*Scopy,theX,C);
      // we allocate S and MS for each test, so we can save these as views
      // however, save copies of the C
      S_theCouts.push_back(Scopy );
      C_theCouts.push_back(std::vector<SmartPtr<MatrixEigenDense<ST> > >(0) );
      if (C.size() > 0) {
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[0]) ) );
      }
      if (C.size() > 1) {
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[1]) ) );
      }

      // do we run the reversed input?
      if (t == 3 ) {
        theX.resize(2);
        theX[0] = X2;
        theX[1] = X1;
        C.resize(2);
        C[0] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX2,sizeS));
        C[1] = SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(sizeX1,sizeS));
        // copies of S,MS
        Scopy = MVT::cloneCopy(*S);
        // randomize this data, it should be overwritten
        for (Number i=0; i<C.size(); i++) {
          C[i]->random();
        }
        // flip the inputs
        //theX = tuple(theX[1], theX[0] );
//        std::swap(theX[1], theX[0]);
        // run test
        om->project(*Scopy,theX,C);
        // we allocate S and MS for each test, so we can save these as views
        // however, save copies of the C
        S_theCouts.push_back(Scopy );
        // we are in a special case: P_X1 and P_X2, so we know we applied
        // two projectors, and therefore have two C[i]
        C_theCouts.push_back(std::vector<SmartPtr<MatrixEigenDense<ST> > >() );
        // reverse the Cs to compensate for the reverse projectors
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[1]) ) );
        C_theCouts.back().push_back(SmartPtr<MatrixEigenDense<ST> >(new MatrixEigenDense<ST>(*C[0]) ) );
//        // flip the inputs back
//        //theX = tuple(theX[1], theX[0] );
        std::swap(theX[1], theX[0]);
      }

      // test all theCoutputs for correctness
      for (Number o=0; o<S_theCouts.size(); o++) {
        // S_in = X1*C1 + C2*C2 + S_theCout
        {
          SmartPtr<MV> tmp = MVT::cloneCopy(*S_theCouts[o]);
          if (C_theCouts[o].size() > 0) {
            MVT::mvTimesMatAddMv(ONE,*theX[0],*C_theCouts[o][0],ONE,*tmp);
            if (C_theCouts[o].size() > 1) {
              MVT::mvTimesMatAddMv(ONE,*theX[1],*C_theCouts[o][1],ONE,*tmp);
            }
          }
          MT err = MVDiff<ST, MVT>(*tmp,*S);
          if (err > atol*tol) {
            stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
          stheCout << "  " << t << "|| S_in - X1*C1 - X2*C2 - S_theCout || : " << err << "\n";
        }
        // <X1,S> == 0
        if (theX.size() > 0 && theX[0] != _smPtrNull) {
          MT err = om->orthogError(*theX[0],*S_theCouts[o]);
          if (err > tol) {
            stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
          stheCout << "  " << t << "|| <X[0],S> || after      : " << err << "\n";
        }
        // <X2,S> == 0
        if (theX.size() > 1 && theX[1] != _smPtrNull) {
          MT err = om->orthogError(*theX[1],*S_theCouts[o]);
          if (err > tol) {
            stheCout << "         vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
          stheCout << "  " << t << "|| <X[1],S> || after      : " << err << "\n";
        }
      }

      // test all theCoutputs for equivalence
      // check all combinations:
      //    theCoutput 0 == theCoutput 1
      //    theCoutput 0 == theCoutput 2
      //    theCoutput 1 == theCoutput 2
      for (Number o1=0; o1<S_theCouts.size(); o1++) {
        for (Number o2=o1+1; o2<S_theCouts.size(); o2++) {
          // don't need to check MS_theCouts because we check
          //   S_theCouts and MS_theCouts = M*S_theCouts
          // don't need to check C_theCouts either
          //
          // check that S_theCouts[o1] == S_theCouts[o2]
          MT err = MVDiff<ST, MVT>(*S_theCouts[o1],*S_theCouts[o2]);
          if (err > tol) {
            stheCout << "    vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv         tolerance exceeded! test failed!" << "\n";
            numerr++;
          }
        }
      }
      } // test for

  MsgEigenType type = _warningsEigen;
  if (numerr>0) type = _errorsEigen;
  theCout->stream(type) << stheCout.str();
  theCout->stream(type) << "\n";
            std::cout << " testProject numerr:"<< numerr;

  return numerr;
}

template<typename ST, typename MV>
void testOrthoMgr(SmartPtr<BasicOutputManager<ST> >& theCout,
                 SmartPtr<OrthoManager<ST,MV> > om,
                 int dim, int sizeS,
                 int sizeX1, int sizeX2)
{
    int numFailed = 0;
    typedef NumTraits<ST> SCT;
    typedef MultiVecTraits<ST,MV> MVT;
    typedef typename SCT::magnitudeType MT;
    const ST ONE = SCT::one();
    const MT ZERO = SCT::magnitude(SCT::zero());

    // multivector to spawn off of
    SmartPtr<MV> S = SmartPtr<MultiVecAdapter<ST> >(new MultiVecAdapter<ST>(dim, sizeS) );

    // create X1, X2
    // they must be M-orthonormal and mutually M-orthogonal
    theCout->stream(_errorsEigen) << "Generating X1,X2 for testing... " << "\n";
    SmartPtr<MV> X1  = MVT::clone(*S,sizeX1),
                 X2  = MVT::clone(*S,sizeX2);
    {
      int dummy;
      MT err;

      // X1
      theCout->stream(_errorsEigen) << "tolerance " << tol << "\n";
      MVT::mvRandom(*X1);
      dummy = om->normalize(*X1);
      theCout->stream(_errorsEigen) << "normalize(X1) returned rank " << dummy << " from " << sizeX1 << " vectors." << "\n";
      err = om->orthonormError(*X1);
      theCout->stream(_errorsEigen) << "normalize(X1) did meet tolerance: orthonormError(X1) == " << err << "\n";
      theCout->stream(_warningsEigen) << "   || <X1,X1> - I || : " << err << "\n";
    }
    {
      // X2
      MVT::mvRandom(*X2);
      Real dummy = om->projectAndNormalize(*X2, std::vector<SmartPtr<const MV> >(1,X1));
      theCout->stream(_errorsEigen) << "projectAndNormalize(X2,X1) returned rank " << dummy << " from " << sizeX2 << " vectors." << "\n";
      Real err = om->orthonormError(*X2);
      if ( err > tol) {
          theCout->stream(_errorsEigen) << "projectAndNormalize(X2,X1) did not meet tolerance: orthonormError(X2) == " << err << "\n";
      }
      err = om->orthogError(*X2,*X1);
      theCout->stream(_warningsEigen) << "   || <X2,X2> - I || : " << err <<  "\n";
      if ( err > tol) {
          theCout->stream(_errorsEigen) << "projectAndNormalize(X2,X1) did not meet tolerance: orthogError(X2,X1) == " << err << "\n";
      }
      theCout->stream(_warningsEigen) << "   || <X2,X1> ||     : " << err <<  "\n";
    }
    theCout->stream(_warningsEigen) << "\n";

    {
      MVT::mvRandom(*S);
      theCout->stream(_errorsEigen) << " project(): testing on random multivector " << "\n";
      numFailed += testProject(theCout, om,S,X1,X2);
    }

    {
      // run a X1,Y2 range multivector against P_{X1,X1} P_{Y2,Y2}
      // note, this is allowed under the restrictions on project(),
      // because <X1,Y2> = 0
      // also, <Y2,Y2> = I, but <X1,X1> != I, so biOrtho must be set to false
      // it should require randomization, as
      // P_{X1,X1} P_{Y2,Y2} (X1*C1 + Y2*C2) = P_{X1,X1} X1*C1 = 0
      MatrixEigenDense<ST> C1(sizeX1,sizeS), C2(sizeX2,sizeS);
      C1.random();
      C2.random();
      MVT::mvTimesMatAddMv(ONE,*X1,C1,ZERO,*S);
      MVT::mvTimesMatAddMv(ONE,*X2,C2,ONE,*S);

      theCout->stream(_errorsEigen) << " project(): testing [X1 X2]-range multivector against P_X1 P_X2 " << "\n";
      numFailed += testProject(theCout,om,S,X1,X2);
    }

    if (sizeS > 2) {
      MVT::mvRandom(*S);
      SmartPtr<MV> mid = MVT::clone(*S,1);
      MatrixEigenDense<ST> c(sizeS,1);
      MVT::mvTimesMatAddMv(ONE,*S,c,ZERO,*mid);
      std::vector<int> ind(1);
      ind[0] = sizeS-1;
      MVT::setBlock(*mid,ind,*S);

      theCout->stream(_errorsEigen) << " normalize(): testing on rank-deficient multivector " << "\n";
      numFailed += testNormalize(theCout,om,S);
    }

    if (sizeS > 1) {
      // rank-1
      SmartPtr<MV> one = MVT::clone(*S,1);
      MVT::mvRandom(*one);
      // put multiple of column 0 in columns 0:sizeS-1
      for (int i=0; i<sizeS; i++) {
        std::vector<int> ind(1);
        ind[0] = i;
        SmartPtr<MV> Si = MVT::cloneViewNonConst(*S,ind);
        MVT::mvAddMv(SCT::random(),*one,ZERO,*one,*Si);
      }

      theCout->stream(_errorsEigen) << " normalize(): testing on rank-1 multivector " << "\n";
      numFailed += testNormalize(theCout, om,S);
    }

    {
      std::vector<int> ind(1);
      MVT::mvRandom(*S);

      theCout->stream(_errorsEigen) << " projectAndNormalize(): testing on random multivector " << "\n";
      numFailed += testProjectAndNormalize(theCout, om,S,X1,X2);
    }


    {
      // run a X1,X2 range multivector against P_X1 P_X2
      // this is allowed as <X1,X2> == 0
      // it should require randomization, as
      // P_X1 P_X2 (X1*C1 + X2*C2) = P_X1 X1*C1 = 0
      // and
      // P_X2 P_X1 (X2*C2 + X1*C1) = P_X2 X2*C2 = 0
      MatrixEigenDense<ST> C1(sizeX1,sizeS), C2(sizeX2,sizeS);
      C1.random();
      C2.random();
      MVT::mvTimesMatAddMv(ONE,*X1,C1,ZERO,*S);
      MVT::mvTimesMatAddMv(ONE,*X2,C2,ONE,*S);

      theCout->stream(_errorsEigen) << " projectAndNormalize(): testing [X1 X2]-range multivector against P_X1 P_X2 " << "\n";
      numFailed += testProjectAndNormalize(theCout, om,S,X1,X2);
    }


    if (sizeS > 2) {
      MVT::mvRandom(*S);
      SmartPtr<MV> mid = MVT::clone(*S,1);
      MatrixEigenDense<ST> c(sizeS,1);
      MVT::mvTimesMatAddMv(ONE,*S,c,ZERO,*mid);
      std::vector<int> ind(1);
      ind[0] = sizeS-1;
      MVT::setBlock(*mid,ind,*S);

      theCout->stream(_errorsEigen) << " projectAndNormalize(): testing on rank-deficient multivector " << "\n";
      numFailed += testProjectAndNormalize(theCout, om,S,X1,X2);
    }


    if (sizeS > 1) {
      // rank-1
      SmartPtr<MV> one = MVT::clone(*S,1);
      MVT::mvRandom(*one);
      // put multiple of column 0 in columns 0:sizeS-1
      for (int i=0; i<sizeS; i++) {
        std::vector<int> ind(1);
        ind[0] = i;
        SmartPtr<MV> Si = MVT::cloneViewNonConst(*S,ind);
        MVT::mvAddMv(SCT::random(),*one,ZERO,*one,*Si);
      }

      theCout->stream(_errorsEigen) << " projectAndNormalize(): testing on rank-1 multivector " << "\n";
      numFailed += testProjectAndNormalize(theCout, om,S,X1,X2);
    }

   if (0 < numFailed) {
      theCout->stream(_errorsEigen) << numFailed << " errors." << "\n";
      theCout->stream(_errorsEigen) << "End Result: TEST FAILED" << "\n";
   } else {
       // Default return value
       theCout->stream(_errorsEigen) << "End Result: TEST PASSED" << "\n";
   }

}

}
