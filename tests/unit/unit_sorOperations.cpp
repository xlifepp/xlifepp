/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
    \file unit_sorOperations.cpp
    \author E. Lunéville
    \since 27 Jan 2016
    \date 27 Jan 2016

    Low level tests related to sor operations
    Almost functionalities are checked.
    This function may either creates a reference file storing the results (check=true)
    or compares results to those stored in the reference file (check=true)
    It returns reporting information in a string

    matrix splitting A = D + L + U  (D diagonal, L strict lower triangular part, U upper triangular part)
    v : vector, w : real parameter

    sorDiagonalMatrixVector :  [w*D]*v
    sorLowerMatrixVector    :  [w*D+L]*v
    sorUpperMatrixVector    :  [w*D+U]*v
    sorDiagonalSolve        :  [D/w]*x = b
    sorLowerSolve           :  [D/w+L]*x = b
    sorUpperSolve           :  [D/w+U]*x = b
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using namespace xlifepp;

namespace unit_sorOperations {

void unit_sorOperations(int argc, char* argv[], bool check)
{
  String rootname = "unit_sorOperations";
  trace_p->push(rootname);
  std::stringstream out; 
  out.precision(testPrec);
  Number nbErrors = 0;
  String errors;
  Real tol=0.00001;
  verboseLevel(1);
  numberOfThreads(1);

  const int rowNum = 3, colNum=3;
  Real w = 0.5;

  const std::string rMatrixDataSym(inputsPathTo(rootname+"/matrix3x3Sym.data"));
  const std::string rMatrixDataSkewSym(inputsPathTo(rootname+"/matrix3x3SkewSym.data"));
  const std::string rMatrixDataNoSym(inputsPathTo(rootname+"/matrix3x3NoSym.data"));
  const std::string rMatrixDataSymPosDef(inputsPathTo(rootname+"/matrix3x3SymPosDef.data"));

  // Real Vector B
  Vector<Real> rv(rowNum, 1.);
  for(Number i=1;i<=rowNum; i++) rv(i)=i;

  //!------------------------------------------------------------------------------
  //! Test with Real value
  //!-----------------------------------------------------------------------------
  std::vector<Real> res(rowNum);
  const MatrixStorage* mst ;
  const std::vector<Real>* mat;

  //! Test with DENSE STORAGE
  LargeMatrix<Real> rMatDenseRow(rMatrixDataSym, _dense, rowNum, colNum, _dense, _row); 
  out << "================= Real dense row matrix =================" <<eol;
  out << " A = " << rMatDenseRow<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  nbErrors += checkValues(rv, rootname+"/rv.in", tol, errors, "Vector rv", check);
  mst = rMatDenseRow.storagep();
  mat = &rMatDenseRow.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  rootname+"/wDv_mdr.in", tol, errors, "case rMatDenseRow Vector [w*D]*v 1", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res, rootname+"/wDvpL_mdr.in", tol, errors, "case rMatDenseRow  Vecto [w*D+L]*v 1", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res, rootname+"/wDvpU_mdr.in", tol, errors, "case rMatDenseRow  Vecto [w*D+U]*v 1", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  rootname+"/invDswxv_mdr.in", tol, errors, "case rMatDenseRow  Vector [D/w]^-1*v 1", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res, rootname+"/invDswpLxv_mdr.in", tol, errors, "case rMatDenseRow  Vector [[D/w+L]^-1*v 1", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res, rootname+"/invDswpUxv_mdr.in", tol, errors, "case rMatDenseRow Vector  [D/w+U]^-1*v ", check);

  LargeMatrix<Real> rMatDenseCol(rMatrixDataNoSym, _dense, rowNum, colNum, _dense, _col);
  out << "================= Real dense col matrix =================" <<eol;
  out << " A = " << rMatDenseCol<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatDenseCol.storagep();
  mat = &rMatDenseCol.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mdc.in"), tol, errors, "case rMatDenseCol Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mdc.in"), tol, errors, "case rMatDenseCol Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mdc.in"), tol, errors, "case rMatDenseCol Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mdc.in"), tol, errors, "case rMatDenseCol Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mdc.in"), tol, errors, "case rMatDenseCol Vecto [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mdc.in"), tol, errors, "case rMatDenseCol Vector  [[D/w+U]^-1*v ", check);


  LargeMatrix<Real> rMatDenseDual(rMatrixDataNoSym, _dense, rowNum, colNum, _dense, _dual);
  out << "================= Real dense dual matrix =================" <<eol;
  out << " A = " << rMatDenseDual<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatDenseDual.storagep();
  mat = &rMatDenseDual.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mdd.in"), tol, errors, "case rMatDenseDual Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mdd.in"), tol, errors, "case rMatDenseDual Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mdd.in"), tol, errors, "case rMatDenseDual Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mdd.in"), tol, errors, "case rMatDenseDual Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mdd.in"), tol, errors, "case rMatDenseDual Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mdd.in"), tol, errors, "case rMatDenseDual Vector  [[D/w+U]^-1*v ", check);

  LargeMatrix<Real> rMatDenseSymSym(rMatrixDataSym, _dense, rowNum, _dense, _symmetric);
  out << "================= Real dense sym matrix (symmetric) =================" <<eol;
  out << " A = " << rMatDenseSymSym<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatDenseSymSym.storagep();
  mat = &rMatDenseSymSym.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mdss.in"), tol, errors, "case rMatDenseSymSym Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mdss.in"), tol, errors, "case rMatDenseSymSym Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w, _symmetric);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mdss.in"), tol, errors, "case rMatDenseSymSym Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mdss.in"), tol, errors, "case rMatDenseSymSym Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mdss.in"), tol, errors, "case rMatDenseSymSym Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w,_symmetric);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mdss.in"), tol, errors, "case rMatDenseSymSym Vector  [[D/w+U]^-1*v ", check);

  LargeMatrix<Real> rMatDenseSym(rMatrixDataNoSym, _dense, rowNum, colNum, _dense, _sym);
  out << "================= Real dense sym matrix (non symmetric) *=================" <<eol;
  out << " A = " << rMatDenseSym<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatDenseSym.storagep();
  mat = &rMatDenseSym.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mds.in"), tol, errors, "case rMatDenseSym Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mds.in"), tol, errors, "case rMatDenseSym Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mds.in"), tol, errors, "case rMatDenseSym Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mds.in"), tol, errors, "case rMatDenseSym Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mds.in"), tol, errors, "case rMatDenseSym Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mds.in"), tol, errors, "case rMatDenseSym Vector  [[D/w+U]^-1*v ", check);

  //! Test with CS STORAGE
  LargeMatrix<Real> rMatCsRow(rMatrixDataNoSym, _dense, rowNum, colNum, _cs, _row);
  out << "================= Real cs row matrix =================" <<eol;
  out << " A = " << rMatCsRow<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatCsRow.storagep();
  mat = &rMatCsRow.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mCsr.in"), tol, errors, "case rMatCsRow Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mCsr.in"), tol, errors, "case rMatCsRow Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mCsr.in"), tol, errors, "case rMatCsRow Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mCsr.in"), tol, errors, "case rMatCsRow Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mCsr.in"), tol, errors, "case rMatCsRow Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mCsr.in"), tol, errors, "case rMatCsRow Vector  [[D/w+U]^-1*v ", check);


  LargeMatrix<Real> rMatCsCol(rMatrixDataNoSym, _dense, rowNum, colNum, _cs, _col);
  out << "================= Real cs col matrix =================" <<eol;
  out << " A = " << rMatCsCol<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatCsCol.storagep();
  mat = &rMatCsCol.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mCsc.in"), tol, errors, "case rMatCsCol Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mCsc.in"), tol, errors, "case rMatCsCol Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mCsc.in"), tol, errors, "case rMatCsCol Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mCsc.in"), tol, errors, "case rMatCsCol Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mCsc.in"), tol, errors, "case rMatCsCol Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mCsc.in"), tol, errors, "case rMatCsCol Vector  [[D/w+U]^-1*v ", check);


  LargeMatrix<Real> rMatCsDual(rMatrixDataNoSym, _dense, rowNum, colNum, _cs, _dual);
  out << "================= Real cs dual matrix =================" <<eol;
  out << " A = " << rMatCsDual<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatCsDual.storagep();
  mat = &rMatCsDual.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mCsd.in"), tol, errors, "case rMatCsDual Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mCsd.in"), tol, errors, "case rMatCsDual Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mCsd.in"), tol, errors, "case rMatCsDual Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mCsd.in"), tol, errors, "case rMatCsDual Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mCsd.in"), tol, errors, "case rMatCsDual Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mCsd.in"), tol, errors, "case rMatCsDual Vector  [[D/w+U]^-1*v ", check);

  LargeMatrix<Real> rMatCsSymSym(rMatrixDataSym, _dense, rowNum, _cs, _symmetric);
  out << "================= Real Cs sym matrix (symmetric) =================" <<eol;
  out << " A = " << rMatCsSymSym<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatCsSymSym.storagep();
  mat = &rMatCsSymSym.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mCsss.in"), tol, errors, "case rMatCsSymSym Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mCsss.in"), tol, errors, "case rMatCsSymSym Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w, _symmetric);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mCsss.in"), tol, errors, "case rMatCsSymSym Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mCsss.in"), tol, errors, "case rMatCsSymSym Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mCsss.in"), tol, errors, "case rMatCsSymSym Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w,_symmetric);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mCsss.in"), tol, errors, "case rMatCsSymSym Vector  [[D/w+U]^-1*v ", check);

  LargeMatrix<Real> rMatCsSym(rMatrixDataNoSym, _dense, rowNum, colNum, _cs, _sym);
  out << "================= Real Cs sym matrix (non symmetric)=================" <<eol;
  out << " A = " << rMatCsSym<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatCsSym.storagep();
  mat = &rMatCsSym.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mCss.in"), tol, errors, "case rMatCsSym Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mCss.in"), tol, errors, "case rMatCsSym Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mCss.in"), tol, errors, "case rMatCsSym Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mCss.in"), tol, errors, "case rMatCsSym Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mCss.in"), tol, errors, "case rMatCsSym Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mCss.in"), tol, errors, "case rMatCsSym Vector  [[D/w+U]^-1*v ", check);

  //! Test with Skyline STORAGE
  LargeMatrix<Real> rMatSkylineDualSym(rMatrixDataSym, _dense, rowNum, colNum, _skyline, _dual);
  out << "================= Real skyline dual matrix (symmetric) =================" <<eol;
  out << " A = " << rMatSkylineDualSym<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatSkylineDualSym.storagep();
  mat = &rMatSkylineDualSym.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mSds.in"), tol, errors, "case rMatSkylineDualSym Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mSds.in"), tol, errors, "case rMatSkylineDualSym Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mSds.in"), tol, errors, "case rMatSkylineDualSym Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mSds.in"), tol, errors, "case rMatSkylineDualSym Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mSds.in"), tol, errors, "case rMatSkylineDualSym Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mSds.in"), tol, errors, "case rMatSkylineDualSym Vector  [[D/w+U]^-1*v ", check);

 LargeMatrix<Real> rMatSkylineSymSym(rMatrixDataSym, _dense, rowNum, _skyline, _symmetric);
  out << "================= Real skyline sym matrix (symmetric) =================" <<eol;
  out << " A = " << rMatSkylineSymSym<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatSkylineSymSym.storagep();
  mat = &rMatSkylineSymSym.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mSss.in"), tol, errors, "case rMatSkylineSymSym Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mSss.in"), tol, errors, "case rMatSkylineSymSym Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w,_symmetric);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mSss.in"), tol, errors, "case rMatSkylineSymSym Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mSss.in"), tol, errors, "case rMatSkylineSymSym Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mSss.in"), tol, errors, "case rMatSkylineSymSym Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w,_symmetric);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mSss.in"), tol, errors, "case rMatSkylineSymSym Vector  [[D/w+U]^-1*v ", check);

  LargeMatrix<Real> rMatSkylineDual(rMatrixDataNoSym, _dense, rowNum, colNum, _skyline, _dual);
  out << "================= Real skyline dual matrix (non symmetric) =================" <<eol;
  out << " A = " << rMatSkylineDual<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatSkylineDual.storagep();
  mat = &rMatSkylineDual.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mSd.in"), tol, errors, "case rMatSkylineDual Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mSd.in"), tol, errors, "case rMatSkylineDual Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mSd.in"), tol, errors, "case rMatSkylineDual Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mSd.in"), tol, errors, "case rMatSkylineDual Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mSd.in"), tol, errors, "case rMatSkylineDual Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mSd.in"), tol, errors, "case rMatSkylineDual Vector  [[D/w+U]^-1*v ", check);

  LargeMatrix<Real> rMatSkylineSym(rMatrixDataNoSym, _dense, rowNum, colNum, _skyline, _sym);
  out << "================= Real skyline sym matrix (non symmetric) =================" <<eol;
  out << " A = " << rMatSkylineSym<<eol;
  out << " v = "<<rv<<" w = "<<w<<eol;
  mst = rMatSkylineSym.storagep();
  mat = &rMatSkylineSym.values();
  mst->sorDiagonalMatrixVector(*mat, rv, res, w);
  out << "   [w*D]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDv_mSs.in"), tol, errors, "case rMatSkylineSym Vector wDv", check);
  mst->sorLowerMatrixVector(*mat,rv, res, w);
  out << "   [w*D+L]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpL_mSs.in"), tol, errors, "case rMatSkylineSym Vector [w*D+L]*v", check);
  mst->sorUpperMatrixVector(*mat,rv, res, w);
  out << "   [w*D+U]*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/wDvpU_mSs.in"), tol, errors, "case rMatSkylineSym Vector [w*D+U]*v ", check);
  mst->sorDiagonalSolver(*mat,rv, res, w);
  out << "   [D/w]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswxv_mSs.in"), tol, errors, "case rMatSkylineSym Vector [D/w]^-1*v ", check);
  mst->sorLowerSolver(*mat,rv, res, w);
  out << "   [D/w+L]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpLxv_mSs.in"), tol, errors, "case rMatSkylineSym Vector [[D/w+L]^-1*v ", check);
  mst->sorUpperSolver(*mat,rv, res, w);
  out << "   [D/w+U]^-1*v = "<<res<<eol;
  nbErrors += checkValues(res,  (rootname+"/invDswpUxv_mSs.in"), tol, errors, "case rMatSkylineSym Vector  [[D/w+U]^-1*v ", check);
 
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
