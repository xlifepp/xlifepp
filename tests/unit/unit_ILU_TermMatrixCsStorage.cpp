/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_ILU_TermMatrixCsStorage.cpp
	\authors C.Chambeyron
	\since 24 Mars 2017

	Low level tests of LargeMatrix  CsStorage ILU factorization.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_ILU_TermMatrixCsStorage
{

// void validILU(LargeMatrix<Real>& MILU, LargeMatrix<Real>& M);
// void validILDLT(LargeMatrix<Real>& MILDLT, LargeMatrix<Real>& M);
// void validILLT(LargeMatrix<Real>& MILLT, LargeMatrix<Real>& M);
// Number validIncompleteFacto(LargeMatrix<Real>& M0, const String& Name, LargeMatrix<Real>& MILF, Real eps, std::stringstream& out);


void unit_ILU_TermMatrixCsStorage(int argc, char* argv[], bool check)
{
  String rootname = "unit_ILU_TermMatrixCsStorage";
  trace_p->push(rootname);
  verboseLevel(9);
  String errors;
  Number nbErrors = 0;
  Real eps=0.0001;

  //! create a mesh and Domains
  Number nbNodes = 4;
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  verboseLevel(16);
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=nbNodes,_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");

  //! create Lagrange P1 space and unknown
  Space V1(_domain=omega, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u(V1, _name="u");
  TestFunction v(u, _name="v");
  cpuTime("time for initialisation, mesh and space definition");

  /*******************************************************************************/
  Real k=1;
  BilinearForm h=intg(omega,grad(u)|grad(v))-k*k*intg(omega,u*v);

  theCout <<std::endl<<"* test I.L.U DualCS Storage:"<<std::endl;
  TermMatrix H0(h, _storage=csDual, _name="H0");
  LargeMatrix<double>  H0LM = H0.getLargeMatrix<Real>();
  TermMatrix ILUDual = H0;
  iFactorize(H0,ILUDual,_ilu);
  LargeMatrix<Real> ILUDualH = ILUDual.getLargeMatrix<Real>();
  nbErrors += checkIncompleteFacto( _ilu, H0, ILUDual, eps, errors, "-> Test incomplete facto  iLU Dual \n");

  theCout <<std::endl<<"* test I.L.D.Lt SymCS Storage:"<<std::endl;
  TermMatrix H(h, _storage=csSym, _name="H");
  TermMatrix ILDLTHDual = H;
  iFactorize(H,ILDLTHDual,_ildlt);
  LargeMatrix<Real> ILDLTH = ILDLTHDual.getLargeMatrix<Real>();
  nbErrors += checkIncompleteFacto( _ildlt, H, ILDLTHDual, eps, errors, "-> Test incomplete facto  iLDLt SymCS \n");

  theCout <<std::endl<<"* test I.L.U RowCS Storage:"<<std::endl;
  TermMatrix HRCS(h, _storage=csRow, _name="HR");
  TermMatrix ILURow = HRCS;
  iFactorize(HRCS,ILURow,_ilu);
  LargeMatrix<Real> ILUHR = ILURow.getLargeMatrix<Real>();
  nbErrors += checkIncompleteFacto( _ilu, HRCS, ILURow, eps, errors, "-> Test incomplete facto  iLU Row CS \n");

  theCout <<std::endl<<"* test I.L.U ColCS Storage:"<<std::endl;
  TermMatrix HCCS(h, _storage=csCol, _name="HC");
  TermMatrix ILUCol = HCCS;
  iFactorize(HCCS,ILUCol,_ilu);
  LargeMatrix<Real> ILUHC = ILUCol.getLargeMatrix<Real>();
  nbErrors += checkIncompleteFacto( _ilu, HCCS, ILUCol, eps, errors, "-> Test incomplete facto  iLU Col CS \n");

  theCout <<std::endl<<"* test I.L.Lt SymCS Storage:"<<std::endl;
  TermMatrix HSym(h, _storage=csSym, _name="HSym");
  TermMatrix ILLtSym = HSym;
  iFactorize(HSym,ILLtSym,_illt);
  LargeMatrix<Real> ILLtH = ILLtSym.getLargeMatrix<Real>();
  nbErrors += checkIncompleteFacto( _illt, HSym, ILLtSym, eps, errors, "-> Test incomplete facto  iLLt Sym CS \n");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}


/********************************************************************************************************/
// void validILU(LargeMatrix<Real>& MILU, LargeMatrix<Real>& M)
// {
//   Number nbLignes = MILU.nbRows;
//   for(Number i=1; i <=nbLignes; i++)
//     for(Number j=1; j <=nbLignes; j++)
//       if(MILU.pos(i,j) != 0.)
//         {
//           if(i <= j)  M(i,j)= MILU(i,j);
//           else M(i,j) = 0.;
//         }
// }
// /********************************************************************************************************/
// void validILDLT(LargeMatrix<Real>& MILDLT, LargeMatrix<Real>& M)
// {
//   Number nbLignes = MILDLT.nbRows;
//   LargeMatrix<Real> Mat_D(M); Mat_D *= 0.;
//   LargeMatrix<Real> Mat_LT(M); Mat_LT *= 0.;
//   for(Number i=1; i <=nbLignes; i++)
//     for(Number j=i; j <= nbLignes; j++)
//       if(MILDLT.pos(i,j) != 0.)
//         {
//           if(i < j)  {Mat_LT(i,j)= MILDLT(i,j);}
//           else if(i==j) {Mat_D(i,i)=MILDLT(i,i); Mat_LT(i,i)=1;}
//         }
//   M = Mat_D*Mat_LT;
// }
// /********************************************************************************************************/
// void validILLT(LargeMatrix<Real>& MILLT, LargeMatrix<Real>& M)
// {
//   Number nbLignes = MILLT.nbRows;
//   for(Number i=1; i <=nbLignes; i++)
//     for(Number j=i; j <= nbLignes; j++)
//       if(MILLT.pos(i,j) != 0.)
//         {
//           if(i <= j)  {M(i,j)= MILLT(i,j);}
//         }
// }
// /*************************************************************************************************/
// 
// Number validIncompleteFacto(LargeMatrix<Real>& M0, const String& Name, LargeMatrix<Real>& MILF, Real eps, String& )
// {
//   Number OK =0;
//   Number nbLignes = M0.numberOfRows();
//   LargeMatrix<Real> Mat=M0*0.;
//   LargeMatrix<Real> Mat_L=M0*0.;
//   String Message=" ";
//   for(Number i=1; i <=nbLignes; i++)
//     for(Number j=1; j <= nbLignes; j++)
//       if(MILF.pos(i,j) != 0.)
//         {
//           if(i > j) { Mat_L(i,j)= MILF(i,j);}
//           else if(i==j)
//             {
//               if(Name == "LLT") {Mat_L(i,j) = MILF(i,j); }
//               else {Mat_L(i,j)=1.;   }
//             }
//           else { Mat_L(i,j) = 0;}
//         }
//   if(Name == "LU")     { validILU(MILF, Mat);  }
//   if(Name == "LDLT") { validILDLT(MILF, Mat); }
//   if(Name == "LLT")   { validILLT(MILF, Mat);   }
//   LargeMatrix<Real> MVerif=Mat_L*Mat;
// //   MVerif.saveToFile("MVerif"+Name+".mat", _dense);
//   Message = "\n -> test de factorisation " + Name + " d'une matrice stockee Cs" + words("access type",MILF.accessType()) +" a les memes coef non nuls que la matrice de depart a "  +  tostring(eps) +" pres \n";
// 
//   for(Number i = 1; i <= nbLignes ; i++)
//     {
//       for(Number j = 1; j <= nbLignes; j++)
//         {
//           if((MVerif.pos(i,j) != 0) && (M0.pos(i,j) != 0))
//             if(std::abs(M0(i,j) - MVerif(i,j)) > eps)
//               {
//                 Message = "non valide car " + tostring(M0(i,j)) + " \\neq " + tostring(MVerif(i,j)) ;
//                 Message =Message + " pour M(" + tostring(i) + "," + tostring(j) +") \n";
// //                 out << "car "<<M0(i,j)<<" \\neq "<<MVerif(i,j);
// //                 out<<" pour M("<<i<<","<<j<<")"<<std::endl;
//                 OK=1; //break;
//               }
//         }
//     }
// //   if(OK==0) out<<"        => Test incomplete facto "<<Name<<": OK VALIDE"<<std::endl<<std::endl;
//    return OK;
// }

}
