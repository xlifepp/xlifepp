/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_LowRankMatrix.cpp
	\author E. Lunéville
	\since 17 may 2016
	\date  17 may 2016

	Low level tests of LowRankMatrix class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "hierarchicalMatrix.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_LowRankMatrix
{

void unit_LowRankMatrix(int argc, char* argv[], bool check)
{
  String rootname = "unit_LowRankMatrix";
  trace_p->push(rootname);
  std::stringstream ssout;                  
  ssout.precision(testPrec);
  verboseLevel(20);
   
  Number nbErrors = 0;
  String errors;
  Real tol=0.001;
  numberOfThreads(4);

  //!test LowRankMatrix
  Number m=6, n=4, r=3;
  Matrix<Real> U(m,r), V(n,r);
  Vector<Real> D(r);
  for(Number i=1;i<=r;i++) D(i)=i;
  for(Number i=1;i<=m;i++)
    for(Number j=1;j<=r;j++) U(i,j)=i+j;
  for(Number i=1;i<=n;i++)
    for(Number j=1;j<=r;j++) V(i,j)=i+2*j;
  Vector<Real> xn(n), xm(m), y;
  for(Number i=1;i<=n;i++) xn(i)=i;
  for(Number i=1;i<=m;i++) xm(i)=i;

  ssout<<"======== construct LowRankMatrix from explicit matrices ========="<<eol;
  LowRankMatrix<Real> lrm(U,V,D,"lrm");
  ssout<<lrm<<eol;
  ssout<<"======== construct LowRankMatrix from iterators ========="<<eol;
  LowRankMatrix<Real> lrm2(m,n,r,U.begin(),V.begin(),D.begin(),"lrm2");
  ssout<<lrm2<<eol;
  ssout<<"======== construct LowRankMatrix from pointers ========="<<eol;
  LowRankMatrix<Real> lrm3(m,n,r,&U[0],&V[0],&D[0],"lrm3");
  ssout<<lrm3<<eol;
  nbErrors+=checkValues(lrm2, rootname+"/lrm2.in", tol, errors, "U(lrm_0)", check);
  nbErrors+=checkValues(lrm2, rootname+"/lrm3.in", tol, errors, "U(lrm_0)", check);
  nbErrors+=checkValues(lrm, rootname+"/lrm0.in", tol, errors, "lrm_0", check);

  ssout<<"======== clone a LowRankMatrix  ========="<<eol;
  ApproximateMatrix<Real>* am=lrm.clone();
  ssout<<*am<<eol;
  delete am;

  ssout<<"======== LowRankMatrix to dense row ========="<<eol;
  std::vector<Real> drv(m*n);
  lrm.toDenseRow(drv.begin());
  nbErrors+=checkValues(drv, rootname+"/drv_0.in", tol, errors, "drv_0", check);
  ssout<<drv<<eol;
  ssout<<"======== LowRankMatrix to Matrix (dense row) ========="<<eol;
  Matrix<Real> mat=lrm.toMatrix();
  ssout<<mat<<eol;
  ssout<<"========  get rows  ========="<<eol;
  std::vector<Real > lrm_ri(lrm.numberOfCols() );
  for(Number i=1;i<=lrm.numberOfRows();i++) 
  { 
    lrm_ri = lrm.row(i);
    nbErrors+=checkValues(lrm_ri, rootname+"/LrmRow_"+tostring(i)+".in", tol, errors, "lrm.row["+tostring(i)+"]", check);
    ssout<<"  row "<<i<<": "<<lrm.row(i)<<eol;
  }
  ssout<<"========  get cols  ========="<<eol;
  std::vector<Real > lrm_cj(lrm.numberOfRows() );
  for(Number j=1;j<=lrm.numberOfCols();j++)
  {
    lrm_cj = lrm.col(j);
    nbErrors+=checkValues(lrm_cj, rootname+"/LrmCol_"+tostring(j)+".in", tol, errors, "lrm.col["+tostring(j)+"]", check);
    ssout<<"  col "<<j<<": "<<lrm.col(j)<<eol;
  }
  ssout<<"======== LowRankMatrix to LargeMatrix (dense row) ========="<<eol;
  LargeMatrix<Real> lm=lrm.toLargeMatrix();
  nbErrors+=checkValues(lm, rootname+"/Lm.in", tol, errors, "lm", check);
  ssout<<lm<<eol;
  ssout<<"======== LowRankMatrix to LargeMatrix (dense col) ========="<<eol;
  LargeMatrix<Real> lmdc=lrm.toLargeMatrix(_dense,_col);
  nbErrors+=checkValues(lmdc, rootname+"/Lmdc.in", tol, errors, "lmdc", check);
  ssout<<lmdc<<eol;
  ssout<<"======== LowRankMatrix to LargeMatrix (dense dual) ========="<<eol;
  LargeMatrix<Real> lmdd=lrm.toLargeMatrix(_dense, _dual);
  nbErrors+=checkValues(lmdd, rootname+"/Lmdd.in", tol, errors, "lmdd", check);
  ssout<<lmdd<<eol;

#ifdef XLIFEPP_WITH_EIGEN
  ssout<<"======== construct LowRankMatrix from LargeMatrix(dense row) ========="<<eol;
  LowRankMatrix<Real> lrmflmfdr(lm,_svdCompression);
  Vector<Real> vu=lm*xn, vufl=lrmflmfdr*xn;
  ssout<<lrmflmfdr<<eol; //r3svd, eps=theTolerance
  ssout<<"lm*xn="<<vu<<" lrmfl*xn="<<vufl<<eol;
  nbErrors+=checkValues(lrmflmfdr, rootname+"/Lrmflmfdr.in", tol, errors, "lrmflmfdr", check);
  nbErrors+=checkValues(vu, rootname+"/LmXn.in", tol, errors, "lm*xn", check);
  nbErrors+=checkValues(vufl, rootname+"/LrmflmfdrXn.in", tol, errors, "lrmflmfdr*xn", check);
  ssout<<"======== construct LowRankMatrix from LargeMatrix(dense col) ========="<<eol;
  LowRankMatrix<Real> lrmflmfdc(lmdc, _rsvdCompression);
  Vector<Real> vcu=lm*xn, vcufl=lrmflmfdc*xn;
  ssout<<lrmflmfdc<<eol; //r3svd, eps=theTolerance
  ssout<<"lmdc*xn="<<lmdc*xn<<" lrmflmfdc*xn="<<lrmflmfdc*xn<<eol;
  nbErrors+=checkValues(lrmflmfdc, rootname+"/Lrmflmfdc.in", tol, errors, "lrmflmfdc", check);
  nbErrors+=checkValues(vcu, rootname+"/LmdcXn.in", tol, errors, "lmdc*xn", check);
  nbErrors+=checkValues(vcufl, rootname+"/LrmflmfdcXn.in", tol, errors, "lrmflmfdc*xn", check);
#endif

  ssout<<"======== extract a submatrix from LowRankMatrix ========="<<eol;
  std::vector<Number> rows(3), cols(2);
  rows[0]=2; rows[1]=3; rows[2]=6; cols[0]=1; cols[1]=3;
  am=lrm.extract(rows,cols);
  ssout<<*am<<eol;
  LargeMatrix<Real> LMam=am->toLargeMatrix();
  ssout<<LMam<< eol;//am->toLargeMatrix()<<eol;
  nbErrors+=checkValues(LMam, rootname+"/LMam.in", tol, errors, "lam->toLargeMatrix()", check);
  delete am;

  ssout<<"======== restrict a LowRankMatrix to row/col index ========="<<eol;
  LowRankMatrix<Real> lrmc=lrm;
  lrmc.restrict(rows,cols);
  nbErrors+=checkValues(lrmc, rootname+"/Lrmc_restrict.in", tol, errors, "lrmc.restrict(rows,cols)", check);
  ssout<<lrmc<<eol;

  ssout<<"======== exten a LowRankMatrix row/col index parent numbering ========="<<eol;
  lrmc.extend(rows,cols);
  nbErrors+=checkValues(lrmc, rootname+"/Lrmc_extend.in", tol, errors, "lrmc.extend(rows,cols)", check);
  ssout<<lrmc<<eol;

  ssout<<"======== LowRankMatrix *= s ========="<<eol;
  ssout<<(lrm2*=2)<<eol;
  nbErrors+=checkValues(lrm2, rootname+"/Lrm2fe2.in", tol, errors, "lrm2*=2", check);
  ssout<<"======== LowRankMatrix /= s ========="<<eol;
  ssout<<(lrm2/=2)<<eol;
  nbErrors+=checkValues(lrm2, rootname+"/Lrm2de2.in", tol, errors, "lrm2/=2", check);
  ssout<<"======== LowRankMatrix += LowRankMatrix ========="<<eol;
  ssout<<(lrm2+=lrm)<<eol;
  nbErrors+=checkValues(lrm2, rootname+"/Lrm2pelrm.in", tol, errors, "lrm2+=lrm", check);
  ssout<<"======== LowRankMatrix -= LowRankMatrix ========="<<eol;
  ssout<<(lrm2-=lrm)<<eol;
  nbErrors+=checkValues(lrm2, rootname+"/Lrm2melrm.in", tol, errors, "lrm2-=lrm", check);
  ssout<<"======== LowRankMatrix = LowRankMatrix ========="<<eol;
  ssout<<(lrm2=lrm)<<eol;
  nbErrors+=checkValues(lrm2, rootname+"/Lrm2elrm.in", tol, errors, "lrm2=lrm", check);
  ssout<<"======== LowRankMatrix + s*LowRankMatrix (no compression) ========="<<eol;
  lrm2.add(lrm, 2);
  nbErrors+=checkValues(lrm2, rootname+"/Lrm2add2_0.in", tol, errors, "lrm2.add(lrm, 2)", check);
  ssout<<lrm2<<eol;

  ssout<<"======== LowRankMatrix * vector ========="<<eol;
  ssout<<"lm*xn  = "<<lm*xn<<eol;
  lrm.multMatrixVector(xn,y);
  nbErrors+=checkValues(y, rootname+"/yElmfxn.in", tol, errors, "y=lm*xn", check);
  ssout<<"lrm*xn = "<<y<<eol;

  ssout<<"======== vector * LowRankMatrix  ========="<<eol;
  ssout<<"xm*lm  = "<<xm*lm<<eol;
  lrm.multVectorMatrix(xm,y);
  nbErrors+=checkValues(y, rootname+"/yExnflm.in", tol, errors, "y=xn*lm;", check);
  ssout<<"xm*lrm = "<<y<<eol;

  ssout<<"======== norm of LowRankMatrix  ========="<<eol;
  Real n2lm=lm.norm2();
  Real n2lrm=lrm.norm2();
  Real ninflm=lm.norminfty();
  Real ninflrm=lrm.norminfty();
  ssout<<"norm2(lm)="<<n2lm<<" norm2(lrm)="<<n2lrm<<eol;
  ssout<<"norminfty(lm)="<<ninflm<<" norminfty(lrm)="<<ninflrm<<eol;
  nbErrors+= checkValue(n2lm, 1326.906, tol, errors, "norm2(lm)");
  nbErrors+= checkValue(n2lrm, 1326.906, tol, errors, "norm2(lrm)");
  nbErrors+= checkValue(ninflm, 440, tol, errors, "norminfty(lm)");
  nbErrors+= checkValue(ninflrm, 440, tol, errors, "norminfty(lrm)");

  ssout<<"======== multMatrixCol L*MatCol   ========="<<eol;
  Number p=2;
  LargeMatrix<Real> Mcol(n,p,_dense,_col), Mcolr(m,p, _dense,_col);
  for(Number i=1;i<=n;i++)
    for(Number j=1;j<=p;j++) Mcol(i,j)=i+j;
  lrm.multMatrixCol(&Mcol.values()[1], &Mcolr.values()[1], 2);
  LargeMatrix<Real> lmMcol = lm*Mcol;
  nbErrors+=checkValues(lmMcol, rootname+"/lmfMcol.in", tol, errors, "lm*Mcol", check);
  nbErrors+=checkValues(Mcolr, rootname+"/Mcolr.in", tol, errors, "Mcolr", check);
  ssout<<"lm*Mcol="<<lmMcol<<eol<<"lrm*Mcol="<<Mcolr<<eol;

  ssout<<"======== multLeftMatrixCol MatCol*L   ========="<<eol;
  LargeMatrix<Real> Mcol2(p,m,_dense,_col), Mcolr2(p,n, _dense,_col);
  for(Number i=1;i<=p;i++)
    for(Number j=1;j<=m;j++) Mcol2(i,j)=i+j;
  lrm.multLeftMatrixCol(&Mcol2.values()[1], &Mcolr2.values()[1], p);
  LargeMatrix<Real> Mcol2lm = Mcol2*lm;
  nbErrors+=checkValues(Mcol2lm, rootname+"/Mcol2flm.in", tol, errors, "Mcol2*lm", check);
  nbErrors+=checkValues(Mcolr2, rootname+"/Mcolr2.in", tol, errors, "Mcolr2", check);
  ssout<<"Mcol*lm="<<Mcol2lm<<eol<<"Mcol * lrm="<<Mcolr2<<eol;

  ssout<<"======== multMatrixRow L*MatRow   ========="<<eol;
  LargeMatrix<Real> Mrow(n,p,_dense,_row), Mrowr(m,p, _dense,_row);
  for(Number i=1;i<=n;i++)
    for(Number j=1;j<=p;j++) Mrow(i,j)=i+j;
  lrm.multMatrixRow(&Mrow.values()[1], &Mrowr.values()[1], 2);
  LargeMatrix<Real> lmMrow = lm*Mrow;
  nbErrors+=checkValues(lmMrow, rootname+"/lmMrow.in", tol, errors, "lm*Mrow", check);
  nbErrors+=checkValues(Mrowr, rootname+"/Mrowr.in", tol, errors, "Mrowr", check);
  ssout<<"lm*Mrow="<<lm*Mrow<<eol<<"lrm*Mrow="<<Mrowr<<eol;

  ssout<<"======== multLeftMatrixRow MatRow*L   ========="<<eol;
  LargeMatrix<Real> Mrow2(p,m,_dense,_row), Mrowr2(p,n, _dense,_row);
  for(Number i=1;i<=p;i++)
    for(Number j=1;j<=m;j++) Mrow2(i,j)=i+j;
  lrm.multLeftMatrixRow(&Mrow2.values()[1], &Mrowr2.values()[1], p);
  LargeMatrix<Real> Mrow2lm = Mrow2*lm;
  nbErrors+=checkValues(Mrow2lm, rootname+"/Mrow2flm.in", tol, errors, "Mrow2*lm", check);
  nbErrors+=checkValues(Mrowr2, rootname+"/Mrowr2.in", tol, errors, "Mrowr2", check);
  ssout<<"Mrow*lm="<<Mrow2lm<<eol<<"Mrow * lrm="<<Mrowr2<<eol;

#ifdef XLIFEPP_WITH_EIGEN
  lrm2=lrm;
  ssout<<"======== compression svd rank = 2   ========="<<eol;
  ssout<<lrm2.compress(_svdCompression,2);
  ssout<<"lrm*xn = "<<lrm.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrmfxn_0.in", tol, errors, "0/ lrm.multMatrixVector(xn,y)", check);
  ssout<<"lrm2*xn = "<<lrm2.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrm2fxn_0.in", tol, errors, "0/ lrm2.multMatrixVector(xn,y)", check);

  lrm2=lrm;
  Real eps=0.0000001;
  ssout<<"======== compression svd eps = "<<eps<<"  ========="<<eol;
  ssout<<lrm2.compress(_svdCompression,0,eps);
  ssout<<"lrm*xn = "<<lrm.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrmfxn_1.in", tol, errors, "1/ lrm.multMatrixVector(xn,y)", check);
  ssout<<"lrm2*xn = "<<lrm2.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrm2fxn_1.in", tol, errors, "1/ lrm2.multMatrixVector(xn,y)", check);

  lrm2=lrm;
  ssout<<"======== compression rsvd rank = 2   ========="<<eol;
  ssout<<lrm2.compress(_rsvdCompression,2);
  ssout<<"lrm*xn = "<<lrm.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrmfxn_2.in", tol, errors, "2/ lrm.multMatrixVector(xn,y)", check);
  ssout<<"lrm2*xn = "<<lrm2.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrm2fxn_2.in", tol, errors, "2/ lrm2.multMatrixVector(xn,y)", check);

  lrm2=lrm;
  ssout<<"======== compression rsvd eps "<<eps<<"  ========="<<eol;
  ssout<<lrm2.compress(_rsvdCompression,0,eps);
  ssout<<"lrm*xn = "<<lrm.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrmfxn_3.in", tol, errors, "3/ lrm.multMatrixVector(xn,y)", check);
  ssout<<"lrm2*xn = "<<lrm2.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrm2fxn_3.in", tol, errors, "3/ lrm2.multMatrixVector(xn,y)", check);

  lrm2=lrm;
  ssout<<"======== compression r3svd eps = "<<eps<<"  ========="<<eol;
  ssout<<lrm2.compress(_r3svdCompression,0,eps);
  ssout<<"lrm*xn = "<<lrm.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrmfxn_4.in", tol, errors, "4/ lrm.multMatrixVector(xn,y)", check);
  ssout<<"lrm2*xn = "<<lrm2.multMatrixVector(xn,y)<<eol;
  nbErrors+=checkValues(y, rootname+"/lrm2fxn_4.in", tol, errors, "4/ lrm2.multMatrixVector(xn,y)", check);

  lrm2=lrm;
  ssout<<"======== LowRankMatrix + s*LowRankMatrix (with compression eps="<<eps<<") ========="<<eol;
  lrm2.add(lrm, 2, eps);
  nbErrors+=checkValues(lrm2, rootname+"/Lrm2add2_1.in", tol, errors, "lrm2.add(lrm, 2, eps)", check);
  ssout<<lrm2<<eol;
#endif

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
