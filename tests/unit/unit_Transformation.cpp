/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Transformation.cpp
	\author N. Kielbasiewicz
	\since 13 oct 2014
	\date 17 oct 2014

	Low level tests of Transformation classes
 */

//===============================================================================
//library dependences
#include "xlife++-libs.h"
#include "testUtils.hpp"

//===============================================================================
//stl dependences
#include <iostream>
#include <fstream>
#include <vector>

//===============================================================================


using namespace xlifepp;

namespace unit_Transformation {

void unit_Transformation(int argc, char* argv[], bool check)
{
  String rootname = "unit_Transformation";
  trace_p->push(rootname);
  verboseLevel(5);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  Point p1d(-1.);
  Point p2d(-1.,0.);
  Point p3d(-1.,0.,0.);

  std::cout << "***** Transformation constructors" << eol;
  Rotation2d r1(_center=Point(0.),_angle=-pi_/2.);
  theCout << r1 << eol;
  Rotation2d r2(_center=Point(0.,0.),_angle=-pi_/2.);
  theCout << r2 << eol;
  Rotation2d r3(_center=Point(0.,0.,0.),_angle=-pi_/2.);
  theCout << r3 << eol;
  Rotation3d r4(_center=Point(0.),_axis={0.,0.,2.},_angle=-pi_/2.);
  theCout << r4 << eol;
  Rotation3d r5(_center=Point(0.,0.),_axis={0.,0.,2.},_angle=-pi_/2.);
  theCout << r5 << eol;
  Rotation3d r6(_center=Point(0.,0.,0.),_axis={0.,0.,2.},_angle=-pi_/2.);
  theCout << r6 << eol;
  Translation t1(_direction=4.);
  theCout << t1 << eol;
  Translation t2(_direction={4.,2.});
  theCout << t2 << eol;
  Translation t3(_direction={4.,2.,3.});
  theCout << t3 << eol;
  Homothety h1(_center=Point(0.), _scale=3.);
  theCout << h1 << eol;
  Homothety h2(_center=Point(0.,0.), _scale=3.);
  theCout << h2 << eol;
  Homothety h3(_center=Point(0.,0.,0.), _scale=3.);
  theCout << h3 << eol;
  PointReflection pr1(_center=Point(0.));
  theCout << pr1 << eol;
  PointReflection pr2(_center=Point(0.,0.));
  theCout << pr2 << eol;
  PointReflection pr3(_center=Point(0.,0.,0.));
  theCout << pr3 << eol;
  Reflection2d rr1(_center=Point(0.,0.), _direction={1.,1.});
  theCout << rr1 << eol;
  Reflection3d rr2(_center=Point(0.,0.,0.),_normal={-1.,1.,0.});
  theCout << rr2 << eol;
  Transformation t = r3*t3*h3;
  theCout << t << eol;
  nbErrors += checkValue(theCout, rootname+"/Transformation.in" , errors, "basic transformations",check);

  //! test of elementary calculations of transformations
  std::cout << eol << "***** elementary calculations" << eol;
  theCout << "Apply transformations on " << p1d << ":" << std::endl;
  theCout << "* " << r1 << " -> " << r1.apply(p1d) << std::endl;
  theCout << "* " << r2 << " -> " << r2.apply(p1d) << std::endl;
  theCout << "* " << r3 << " -> " << r3.apply(p1d) << std::endl;
  theCout << "* " << r4 << " -> " << r4.apply(p1d) << std::endl;
  theCout << "* " << r5 << " -> " << r5.apply(p1d) << std::endl;
  theCout << "* " << r6 << " -> " << r6.apply(p1d) << std::endl;
  theCout << "* " << t1 << " -> " << t1.apply(p1d) << std::endl;
  theCout << "* " << t2 << " -> " << t2.apply(p1d) << std::endl;
  theCout << "* " << t3 << " -> " << t3.apply(p1d) << std::endl;
  theCout << "* " << h1 << " -> " << h1.apply(p1d) << std::endl;
  theCout << "* " << h2 << " -> " << h1.apply(p1d) << std::endl;
  theCout << "* " << h3 << " -> " << h1.apply(p1d) << std::endl;
  theCout << "* " << pr1 << " -> " << pr1.apply(p1d) << std::endl;
  theCout << "* " << pr2 << " -> " << pr2.apply(p1d) << std::endl;
  theCout << "* " << pr3 << " -> " << pr3.apply(p1d) << std::endl;
  theCout << "* " << rr1 << " -> " << rr1.apply(p1d) << std::endl;
  theCout << "* " << rr2 << " -> " << rr2.apply(p1d) << std::endl;
  theCout << "* " << t << " -> " << t.apply(p1d) << std::endl;
  theCout << "Apply transformations on " << p2d << ":" << std::endl;
  theCout << "* " << r1 << " -> " << r1.apply(p2d) << std::endl;
  theCout << "* " << r2 << " -> " << r2.apply(p2d) << std::endl;
  theCout << "* " << r3 << " -> " << r3.apply(p2d) << std::endl;
  theCout << "* " << r4 << " -> " << r4.apply(p2d) << std::endl;
  theCout << "* " << r5 << " -> " << r5.apply(p2d) << std::endl;
  theCout << "* " << r6 << " -> " << r6.apply(p2d) << std::endl;
  theCout << "* " << t1 << " -> " << t1.apply(p2d) << std::endl;
  theCout << "* " << t2 << " -> " << t2.apply(p2d) << std::endl;
  theCout << "* " << t3 << " -> " << t3.apply(p2d) << std::endl;
  theCout << "* " << h1 << " -> " << h1.apply(p2d) << std::endl;
  theCout << "* " << h2 << " -> " << h1.apply(p2d) << std::endl;
  theCout << "* " << h3 << " -> " << h1.apply(p2d) << std::endl;
  theCout << "* " << pr1 << " -> " << pr1.apply(p2d) << std::endl;
  theCout << "* " << pr2 << " -> " << pr2.apply(p2d) << std::endl;
  theCout << "* " << pr3 << " -> " << pr3.apply(p2d) << std::endl;
  theCout << "* " << rr1 << " -> " << rr1.apply(p2d) << std::endl;
  theCout << "* " << rr2 << " -> " << rr2.apply(p2d) << std::endl;
  theCout << "* " << t << " -> " << t.apply(p2d) << std::endl;
  theCout << "Apply transformations on " << p3d << ":" << std::endl;
  theCout << "* " << r1 << " -> " << r1.apply(p3d) << std::endl;
  theCout << "* " << r2 << " -> " << r2.apply(p3d) << std::endl;
  theCout << "* " << r3 << " -> " << r3.apply(p3d) << std::endl;
  theCout << "* " << r4 << " -> " << r4.apply(p3d) << std::endl;
  theCout << "* " << r5 << " -> " << r5.apply(p3d) << std::endl;
  theCout << "* " << r6 << " -> " << r6.apply(p3d) << std::endl;
  theCout << "* " << t1 << " -> " << t1.apply(p3d) << std::endl;
  theCout << "* " << t2 << " -> " << t2.apply(p3d) << std::endl;
  theCout << "* " << t3 << " -> " << t3.apply(p3d) << std::endl;
  theCout << "* " << h1 << " -> " << h1.apply(p3d) << std::endl;
  theCout << "* " << h2 << " -> " << h1.apply(p3d) << std::endl;
  theCout << "* " << h3 << " -> " << h1.apply(p3d) << std::endl;
  theCout << "* " << pr1 << " -> " << pr1.apply(p3d) << std::endl;
  theCout << "* " << pr2 << " -> " << pr2.apply(p3d) << std::endl;
  theCout << "* " << pr3 << " -> " << pr3.apply(p3d) << std::endl;
  theCout << "* " << rr1 << " -> " << rr1.apply(p3d) << std::endl;
  theCout << "* " << rr2 << " -> " << rr2.apply(p3d) << std::endl;
  theCout << "* " << t << " -> " << t.apply(p3d) << std::endl;

  //! test explicit linear transformation
  std::cout << eol << "***** Explicit linear transformation" << eol;
  Transformation trexp1d(1.,0.,0.,0.,0.,0.,0.,0.,0.,-1.,0.,0.);
  theCout<<trexp1d<<std::endl;
  theCout << "trexp1d(p1d) = "<< trexp1d.apply(p1d) << std::endl;
  Transformation trexp2d(0.,-1,0.,1.,0.,0.,0.,0.,0.,1.,0.,0.);
  theCout<<trexp2d<<std::endl;
  theCout << "trexp2d(p1d) = "<< trexp2d.apply(p2d) << std::endl;
  Transformation trexp3d(0.,1.,0.,1.,0.,0.,0.,0.,1.);
  theCout<<trexp3d<<std::endl;
  theCout << "trexp3d(p3d) = "<< trexp3d.apply(p3d) << std::endl;

  nbErrors += checkValue(theCout, rootname+"/ElementaryCalculationsOfTransformations.in" , errors, "elementary calculations of transformations",check);

  //! test of bounding box calculations of transformations
  std::cout << eol << "***** boundingbox transformations" << eol;
  BoundingBox bb(0.2,0.8,0.2,0.8,0.,0.);
  theCout << bb << std::endl;
  theCout << bb.translate(_direction={4., 2., 3.}) << std::endl;
  theCout << bb << std::endl;
  nbErrors += checkValue(theCout, rootname+"/BoundingBoxCalculationsOfTransformations.in" , errors, "bounding box calculations of transformations",check);

  //! test of minimal box calculations of transformations
  std::cout << eol << "***** minimal box transformations" << eol;
  MinimalBox mb(0.2,0.8,0.2,0.8,0.,0.);
  theCout << mb << std::endl;
  theCout << mb.translate(_direction={4., 2., 3.}) << std::endl;
  theCout << mb << std::endl;
  nbErrors += checkValue(theCout, rootname+"/MinimalBoxCalculationsOfTransformations.in" , errors, "Minimal box calculations of transformations",check);

  //! test of geometry calculations of transformations
  std::cout << eol << "***** geometry transformations" << eol;
  Segment seg(_v1=Point(0.2,0.2,0.),_v2=Point(0.8,0.8,0.),_nnodes=3);
  Segment seg2=seg;
  theCout << seg << std::endl;
  theCout << transform(seg, Translation(_direction={4., 2., 3.})) << std::endl;
  theCout << transform(seg , t3) << std::endl;
  theCout << translate(seg, _direction={4., 2., 3.}) << std::endl;
  theCout << seg.translate(_direction={4., 2., 3.}) << std::endl;
  theCout << seg << std::endl;

  Rectangle rect(_v1=Point(0.,0.,0.), _v2=Point(1.,0.,0.), _v4=Point(0.,1.,0.), _nnodes=3);
  Rectangle rect2=rect;
  theCout << rect << std::endl;
  theCout << transform(rect, Translation(_direction={4., 2., 3.})) << std::endl;
  theCout << transform(rect , t3) << std::endl;
  theCout << translate(rect, _direction={4., 2., 3.}) << std::endl;
  theCout << rect.translate(_direction={4., 2., 3.}) << std::endl;
  theCout << rect << std::endl;

  Geometry g=rect2+seg2;
  theCout << g << std::endl;
  theCout << transform(g, Translation(_direction={4., 2., 3.})) << std::endl;
  theCout << transform(g , t3) << std::endl;
  theCout << translate(g, _direction={4., 2., 3.}) << std::endl;
  theCout << g.translate(_direction={4., 2., 3.}) << std::endl;
  theCout << g << std::endl;

  //! test of mesh calculations of transformations
  std::cout << eol << "***** mesh transformations" << eol;
  Mesh m(rect2,_shape=_triangle, _order=1, _generator=_structured, _name="squareMesh");
  theCout << m << std::endl;
  theCout << transform(m,Translation(_direction={4., 2., 3.})) << std::endl;
  theCout << transform(m, t3) << std::endl;
  theCout << translate(m, _direction={4., 2., 3.}) << std::endl;
  theCout << m.translate(_direction={4., 2., 3.}) << std::endl;
  theCout << m << std::endl;
  nbErrors += checkValue(theCout, rootname+"/MeshCalculationsOfTransformations.in" , errors, "mesh calculations of transformations",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
