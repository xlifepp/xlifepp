/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_tetrahedron.cpp
	\author N. Kielbasiewicz
	\since 5 nov 2012
	\date 18 dec 2012

	Low level tests of refTetrahedron class.
 */

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_tetrahedron {

void unit_tetrahedron(int argc, char* argv[], bool check)
{
  String rootname = "unit_tetrahedron";
  trace_p->push(rootname);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!test GeomRefTetrahedron
  verboseLevel(12);
  theCout<<"----------------------------- GeomRefTetrahedron test ----------------------------------"<<eol;
  GeomRefTetrahedron grt;
  theCout<<grt;
  for(Number i=0;i<grt.nbSides();++i)
  {
      Number v1=grt.sideVertexNumbers()[i][0], v2=grt.sideVertexNumbers()[i][1], v3=grt.sideVertexNumbers()[i][2];
      theCout<<"sideWithVertices("<<v1<<","<<v2<<","<<v3<<") = "<<grt.sideWithVertices(v1,v2,v3)<<eol;
  }
  for(Number i=0;i<grt.nbSideOfSides();++i)
  {
      Number v1=grt.sideOfSideVertexNumbers()[i][0], v2=grt.sideOfSideVertexNumbers()[i][1];
      theCout<<"sideWithVertices("<<v1<<","<<v2<<") = "<<grt.sideWithVertices(v1,v2)<<eol;
  }
  nbErrors += checkValue(theCout, rootname+"/GeomRefTetrahedron.in" , errors, "Test Geom Ref Tetrahedron",check);

  //!test interpolation
  verboseLevel(5);
  theCout<<eol<<"----------------------------- interpolation test ----------------------------------"<<eol;
  Interpolation* p1 = findInterpolation(_Lagrange, _standard, 1, H1);
  Interpolation* p2 = findInterpolation(_Lagrange, _standard, 2, H1);
  Interpolation* p3 = findInterpolation(_Lagrange, _standard, 3, H1);
  Interpolation* p4 = findInterpolation(_Lagrange, _standard, 4, H1);
  Interpolation* p5 = findInterpolation(_Lagrange, _standard, 5, H1);

  RefElement* tetrahedronP1 = findRefElement(_tetrahedron, p1);
  RefElement* tetrahedronP2 = findRefElement(_tetrahedron, p2);
  RefElement* tetrahedronP3 = findRefElement(_tetrahedron, p3);
  RefElement* tetrahedronP4 = findRefElement(_tetrahedron, p4);
  RefElement* tetrahedronP5 = findRefElement(_tetrahedron, p5);

  verboseLevel(10);
  theCout << *(tetrahedronP1) << std::endl;
  theCout << *(tetrahedronP2) << std::endl;
  theCout << *(tetrahedronP3) << std::endl;
  theCout << *(tetrahedronP4) << std::endl;
  theCout << *(tetrahedronP5) << std::endl;
  nbErrors += checkValue(theCout, rootname+"/Interpolation.in" , errors, "Test Interpolation",check);

  //!test barycentric side dof map
  theCout<<"test of P3 barycentric side dofs"<<std::endl;
  LagrangeTetrahedron* lt=static_cast<LagrangeTetrahedron*>(tetrahedronP3);
  theCout << "  BarycentricSideDofs = "<<lt->barycentricSideDofs << std::endl;
  theCout << "  BarycentricSideDofMap ="<<lt->barycentricSideDofMap << std::endl;

  theCout<<"test of P4 barycentric side dofs"<<std::endl;
  lt=static_cast<LagrangeTetrahedron*>(tetrahedronP4);
  theCout << "  BarycentricSideDofs = "<<lt->barycentricSideDofs << std::endl;
  theCout << "  BarycentricSideDofMap ="<<lt->barycentricSideDofMap << std::endl;

  theCout<<"test of P5 barycentric side dofs"<<std::endl;
  lt=static_cast<LagrangeTetrahedron*>(tetrahedronP5);
  theCout << "  BarycentricSideDofs = "<<lt->barycentricSideDofs << std::endl;
  theCout << "  BarycentricSideDofMap ="<<lt->barycentricSideDofMap << std::endl;

  theCout<<"test of P6 barycentric side dofs"<<std::endl;
  Interpolation* p6 = findInterpolation(_Lagrange, _standard, 6, H1);
  RefElement* tetrahedronP6 = findRefElement(_tetrahedron, p6);
  lt=static_cast<LagrangeTetrahedron*>(tetrahedronP6);
  theCout << "  BarycentricSideDofs = "<<lt->barycentricSideDofs << std::endl;
  theCout << "  BarycentricSideDofMap ="<<lt->barycentricSideDofMap << std::endl;

  theCout<<"test of P7 barycentric side dofs"<<std::endl;
  Interpolation* p7 = findInterpolation(_Lagrange, _standard, 7, H1);
  RefElement* tetrahedronP7 = findRefElement(_tetrahedron, p7);
  lt=static_cast<LagrangeTetrahedron*>(tetrahedronP7);
  theCout << "  BarycentricSideDofs = "<<lt->barycentricSideDofs << std::endl;
  theCout << "  BarycentricSideDofMap ="<<lt->barycentricSideDofMap << std::endl;
  theCout << "  lt->sideDofsMap(1,1,2,3) = "<<lt->sideDofsMap(1,1,2,3) << std::endl;
  theCout << "  lt->sideDofsMap(1,2,1,3) = "<<lt->sideDofsMap(1,2,1,3) << std::endl;
  theCout << "  lt->sideDofsMap(10,2,1,3) = "<<lt->sideDofsMap(10,2,1,3) << std::endl;
  nbErrors += checkValue(theCout, rootname+"/barycentric.in" , errors, "barycentric side dof map",check);

//!test splitting EXPERIMENTAL
  theCout << "split P1 to P1 : "<<tetrahedronP1->splitLagrange3DToP1() << std::endl;
  theCout << "split P2 to P1 : "<<tetrahedronP2->splitLagrange3DToP1() << std::endl;
  theCout << "split P3 to P1 : "<<tetrahedronP3->splitLagrange3DToP1() << std::endl;
  theCout << "split P4 to P1 : "<<tetrahedronP4->splitLagrange3DToP1() << std::endl;
  theCout << "split P5 to P1 : "<<tetrahedronP5->splitLagrange3DToP1() << std::endl;
  nbErrors += checkValue(theCout, rootname+"/splittingEXPERIMENTAL.in" , errors, "Test splitting EXPERIMENTAL",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
