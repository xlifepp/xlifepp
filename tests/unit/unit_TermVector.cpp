/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_TermVector.cpp
	\author E. Lunéville
	\since 5 oct 2012
	\date 21 aug 2013

	Low level tests of TestVector class and related classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_TermVector {

//! scalar 2D function
Real one(const Point& P, Parameters& pa = defaultParameters)
{
	return 1.;
}
Real xy(const Point& P, Parameters& pa = defaultParameters)
{
	return P(1)*P(2);
}

Real x(const Point& P, Parameters& pa = defaultParameters)
{
	return P(1);
}

Real fn(const Point& P, Parameters& pa = defaultParameters)
{
	Vector<Real>& n = getN();  //new method
	return dot(P,Point(n));
}

Real ft(const Point& P, Parameters& pa = defaultParameters)
{
	Vector<Real>& t = getT();  //new method
	return dot(P,Point(t));
}

Real fsin(const Point& P,Parameters& pa = defaultParameters)
{
  return sin(pi_*P(1));
}

Vector<Real> m1(const Point& P,Parameters& pa = defaultParameters)
{
  return Point(P(1),0.);
}
Complex fexp(const Point& P,Parameters& pa = defaultParameters)
{
  return exp(4*i_*pi_*P(2));
}
Real fcos(const Point& P,Parameters& pa = defaultParameters)
{
  Real t=atan2(P(2),P(1));
  return cos(2*t);
}
Vector<Real> m2(const Point& P,Parameters& pa = defaultParameters)
{
  Real r=norm(P);
  if(r!=0.) return P/r;
  return P;
}

Complex rexp(const Point& P,Parameters& pa = defaultParameters)
{
  Real r=norm(P);
  if(r!=0.) return exp(i_*pi_*(r-1))/r;
  return 0.;
}

void unit_TermVector(int argc, char* argv[], bool check)
{
  String rootname = "unit_TermVector";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  std::cout.precision(testPrec);
  verboseLevel(9);
  String errors;
  Number nbErrors = 0;
  Real tol=0.0001;

 //! create a mesh and Domains
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  verboseLevel(10);
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=11,_side_names=sidenames),_shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");
  Domain gamma1=mesh2d.domain("Gamma_1");
  Domain gamma2=mesh2d.domain("Gamma_2");

  //!create Lagrange P1 space and unknown
  Space V1(_domain=omega,_FE_type=Lagrange, _order=1, _name="V1", _notOptimizeNumbering);
  Unknown u(V1, _name="u"); TestFunction v(u, _name="v");

  //!create TermVector from linear forms
  Function f1(one,"one");
  Function fxy(xy,"xy");
  TermVector un(v,omega,1., _name="un");
  LinearForm lf=intg(omega,f1*v);
  TermVector B1(lf, _name="B");
  ssout<<B1;
  nbErrors += checkValues(B1, rootname+"/B1.in", tol, errors, "Vec B1", check);
  ssout<<"\n B1|un="<<realPart(B1|un)<<"\n\n";
  nbErrors += checkValue(realPart(B1|un), 1., tol, errors, "Re(B1)|un");

  LinearForm lfxy=intg(omega,xy*v);
  TermVector Bxy(lfxy, _name="Bxy");
  ssout<<Bxy;
  ssout<<"\n Bxy|un="<<realPart(Bxy|un)<<"\n\n";
  nbErrors += checkValues(Bxy, rootname+"/Bxy.in", tol, errors, "Vec Bxy", check);
  nbErrors += checkValue(realPart(Bxy|un), 0.25, tol, errors, "Re(Bxy)|un");

  LinearForm lfb1=intg(gamma2,v);
  TermVector C1(lfb1, _name="C1");
  ssout<<C1;
  nbErrors += checkValues(C1, rootname+"/C1.in", tol, errors, "Vec C1", check);

  //!test product
  ssout<<TermVector(2*intg(omega,f1*v));
  ssout<<TermVector(pi_*intg(omega,f1*v));
  ssout<<TermVector(i_*intg(omega,f1*v));
  ssout<<TermVector(Complex(3,0)*intg(omega,f1*v));
  ssout<<TermVector(Complex(0,3)*intg(omega,f1*v));
  TermVector Vec2f1(2*intg(omega,f1*v));//.saveToFile(inputsPathTo(rootname+"/Vec2f1.dat"));
  TermVector Vecif1(i_*intg(omega,f1*v));//.saveToFile(inputsPathTo(rootname+"/Vecif1.dat"));
  TermVector Vec3cf1(Complex(3,0)*intg(omega,f1*v));//.saveToFile(inputsPathTo(rootname+"/Vec3cf1.dat"));
  TermVector Vec3if1(Complex(0,3)*intg(omega,f1*v));//.saveToFile(inputsPathTo(rootname+"/Vec3if1.dat"));
  nbErrors += checkValues(Vec2f1, rootname+"/Vec2f1.in", tol, errors, "Vec Vec2f1", check);
  nbErrors += checkValues(Vecif1, rootname+"/Vecif1.in", tol, errors, "Vec Vecif1", check);
  nbErrors += checkValues(Vec3cf1, rootname+"/Vec3cf1.in", tol, errors, "Vec Vec3cf1", check);
  nbErrors += checkValues(Vec3if1, rootname+"/Vec3if1.in", tol, errors, "Vec Vec3if1", check);

  //!create TermVector from function
  TermVector F1(u,omega,fxy, _name="F1");
  ssout<<"TermVector from Function"<<F1;
  nbErrors += checkValues(F1, rootname+"/F1.in", tol, errors, "Vec F1", check);

  //!create TermVector from symbolic function
  TermVector F2(u,omega,x_1*x_2, _name="F2");
  ssout<<"TermVector from SymbolicFunction"<<F2;
  nbErrors += checkValues(F2, rootname+"/F2.in", tol, errors, "Vec F2", check);

  //!create TermVector from function involving normal vector
  Function f_fn(fn);
  f_fn.require(_n);
  TermVector Fn(u,gamma2,f_fn, _name="Fn");
  ssout<<"TermVector from Function involving normal vector"<<Fn;
  nbErrors += checkValues(Fn, rootname+"/Fn.in", tol, errors, "Vec Fn", check);

  //!create TermVector from linear form with function involving tangent vector
  Function f_ft(ft);
  f_ft.require(_tau);
  TermVector Ft(intg(gamma2,f_ft*v), _name="Ft");
  ssout<<"TermVector from Function involving tangent vector"<<Ft;
  nbErrors += checkValues(Ft, rootname+"/Ft.in", tol, errors, "Vec Ft", check);

  //!examples of inner product
  TermVector ung2(v,gamma2,1., _name="ung2");
  TermVector ung1(v,gamma1,1., _name="ung1");
  ssout<<"\n C1|ung2="<<realPart(C1|ung2)<<"\n";
  ssout<<"\n C1|ung1="<<realPart(C1|ung1)<<"\n";
  ssout<<"\n C1|un="<<realPart(C1|un)<<"\n";
  nbErrors += checkValue(realPart(C1|ung2), 1., tol, errors, "C1|ung2");
  nbErrors += checkValue(realPart(C1|ung1), 0.05, tol, errors, "C1|ung1");
  nbErrors += checkValue(realPart(C1|un), 1, tol, errors, "C1|un");

  //!create Lagrange P2 space and unknown
  Interpolation* LagrangeP2=findInterpolation(Lagrange,standard,2,H1);
  Space V2(_domain=omega,_interpolation=P2, _name="V2", _notOptimizeNumbering);
  Unknown u2(V2, _name="u2");
  TestFunction v2(u2, _name="v2");

  TermVector un2(v2,omega,1., _name="un2");
  LinearForm lf2=intg(omega,f1*v2);
  TermVector B2(lf2, _name="B2");
  ssout<<B2;
  nbErrors += checkValues(B2, rootname+"/B2.in", tol, errors, "Vec B2", check);
  ssout<<"\n B2|un2="<<realPart(B2|un2)<<"\n\n";
  nbErrors += checkValue(realPart(B2|un2), 1., tol, errors, "Re(B2)|un2");

  LinearForm lfxy2=intg(omega,xy*v2);
  TermVector Bxy2(lfxy2, _name="Bxy2");
  ssout<<Bxy2;
  nbErrors += checkValues(Bxy2, rootname+"/Bxy2.in", tol, errors, "Vec Bxy2", check);
  ssout<<"\n Bxy2|un2="<<realPart(Bxy2|un2)<<"\n\n";
  nbErrors += checkValue(realPart(Bxy2|un2), 0.25, tol, errors, "Re(Bxy2)|un2");

  LinearForm lfb2=intg(gamma2,v2);
  TermVector C2(lfb2, _name="C2");
  ssout<<C2;
  nbErrors += checkValues(C2, rootname+"/C2.in", tol, errors, "Vec C2", check);

  //!examples of inner product
  TermVector ung22(v2,gamma2,1., _name="ung22");
  TermVector ung12(v2,gamma1,1., _name="ung12");
  ssout<<"\n C2|ung22="<<realPart(C2|ung22)<<"\n";
  ssout<<"\n C2|ung12="<<realPart(C2|ung12)<<"\n";
  ssout<<"\n C2|un2="<<realPart(C2|un2)<<"\n";
  nbErrors += checkValue(realPart(C2|ung22), 1., tol, errors, "Re(C2)|ung22");
  nbErrors += checkValue(realPart(C2|ung12), 0.0166667, tol, errors, "Re(C2)|ung12");
  nbErrors += checkValue(realPart(C2|un2), 1., tol, errors, "Re(C2)|un2");

  //!linear algebra on TermVector
  TermVector t1(u, omega, xy), t2(u,gamma1,one);
  TermVector t=t1+t2;t.name()="";
  ssout<<"\n t1 = "<<t1<<"\n";
  ssout<<"\n t2 = "<<t2<<"\n";
  ssout<<"\n t1 + t2 = "<<t<<"\n";
  nbErrors += checkValues(t1, rootname+"/t1.in", tol, errors, "Vec t1", check);
  nbErrors += checkValues(t2, rootname+"/t2.in", tol, errors, "Vec t2", check);
  nbErrors += checkValues(t, rootname+"/t1pt2.in", tol, errors, "Vec t1+t2", check);

  t=2*t1+Complex(0.,1.)*t2;
  ssout<<"\n 2*t1+Complex(0.,1.)*t2 = "<<t<<"\n";
  nbErrors += checkValues(t, rootname+"/t1x2pit2.in", tol, errors, "Vec 2t1+it2", check);

  t=2*t1-Complex(0.,1.)*t2;
  ssout<<"\n 2*t1-Complex(0.,1.)*t2 = "<<t<<"\n";
  nbErrors += checkValues(t, rootname+"/t1x2mit2.in", tol, errors, "Vec 2t1-it2", check);

  ssout<<"\n abs(t) = "<<abs(t)<<"\n";
  ssout<<"\n imag(t) = "<<imag(t)<<"\n";
  ssout<<"\n real(t) = "<<real(t)<<"\n";
  ssout<<"\n conj(t) = "<<conj(t)<<"\n";
  nbErrors += checkValues(abs(t), (rootname+"/abst.in"), tol, errors, "Vec abs(t)", check);
  nbErrors += checkValues(imag(t), (rootname+"/imagt.in"), tol, errors, "Vec imag(t)", check);
  nbErrors += checkValues(real(t), (rootname+"/realt.in"), tol, errors, "Vec real(t)", check);
  nbErrors += checkValues(conj(t), (rootname+"/conjt.in"), tol, errors, "Vec conj(t)", check);

  Real dt=2.;
  t=(t1-t2)/dt;
  ssout<<"\n (t1-t2)/dt = "<<t<<"\n";
  ssout<<"\n on the fly abs((t1-t2)/dt) = "<<abs((t1-t2)/dt)<<"\n";
  ssout<<"\n on the fly out<<(t1-t2)/dt = "<<(t1-t2)/dt<<"\n";
  nbErrors += checkValues(abs((t1-t2)/dt), rootname+"/absttddt.in", tol, errors, "Vec abs((t1-t2)/dt)", check);
  nbErrors += checkValues(t, (rootname+"/t1mt2ddt.in"), tol, errors, "Vec (t1-t2)/dt", check);

  //!interpolation of TermVector
  Point p(0.5,0.5), q(0.11,0.33);
  Real r=0.;
  ssout<<"t1.subVector(u)(p,r) = "<<t1.subVector(u)(p,r)<<"\n";
  ssout<<"t1.subVector(u)(q,r) = "<<t1.subVector(u)(q,r)<<"\n";
  ssout<<"t1(p,r) = "<<t1(p,r)<<"\n";
  ssout<<"t1(q,r) = "<<t1(q,r)<<"\n";
  ssout<<"t1(u,p,r) = "<<t1(u,p,r)<<"\n";
  ssout<<"t1(u,q,r) = "<<t1(u,q,r)<<"\n";
  nbErrors += checkValue( t1.subVector(u)(p,r), 0.25, tol, errors, "t1.subVector(u)(p,r)");
  nbErrors += checkValue( t1.subVector(u)(q,r), 0.036, tol, errors, "t1.subVector(u)(q,r)");
  nbErrors += checkValue( t1(p,r), 0.25, tol, errors, "t1(p,r)");
  nbErrors += checkValue( t1(q,r), 0.036, tol, errors, "t1(q,r)");
  nbErrors += checkValue( t1(u,p,r), 0.25, tol, errors, "t1(u,p,r)");
  nbErrors += checkValue( t1(u,q,r), 0.036, tol, errors, "t1(u,q,r)");

  //!assign constant value to a part of a TermVector
  TermVector ts(u2,omega,fxy);
  ts.setValue(gamma1,1.);
  ts.setValue(gamma2,2.);
  Domain gamma=merge(gamma1,gamma2,"Gamma");
  ts.setValue(gamma,0.);
  ts.setValue(gamma1,x); //assign function values to a part of a TermVector
  ts.setValue(gamma,0.);
  TermVector tg(u2,gamma1,x);
  ts.setValue(tg);      //assign TermVector values to a part of a TermVector

  //!interpolation on a domain (TermVector::mapTo)
  Mesh mdisk(Disk(_center = Point(0.5,0.5),_radius=0.5, _nnodes=11,_domain_name="Disk"),_shape=_triangle, _order=1, _generator=gmsh);
  Domain disk=mdisk.domain("Disk");
  Space Vd(_domain=disk, _interpolation=P1, _name="Vd"); Unknown vd(Vd, _name="vd");
  TermVector td=t1.mapTo(disk,vd, false);

  //!non linear operation on TermVector
  ssout<<"by fun t1*t1 = "<<TermVector(t1,t1,fun_productR)<<eol;      //using explicit function
  nbErrors += checkValues(TermVector(t1,t1,fun_productR), rootname+"/t1t1Fun.in", tol, errors, "Vec fun t1*t1", check);
  ssout<<"by symbolic function t1^2 = "<<TermVector(t1,x_1^2)<<eol;   //using symbolic function
  nbErrors += checkValues(TermVector(t1,t1,fun_productR), rootname+"/t1t1SymbFun.in", tol, errors, "Vec symb fun t1^2", check);
  ssout<<"t1*t1 = "<<t1*t1<<eol;
  nbErrors += checkValues(t1*t1, (rootname+"/t1xt1.in"), tol, errors, "Vec t1*t1", check);
  TermVector st1 = TermVector(t1,sin(x_1));
  ssout<<"st1 = "<<st1<<eol;
  nbErrors += checkValues(st1, (rootname+"/st1.in"), tol, errors, "Vec st1", check);
  ssout<<"abs(st1+t1) = "<<TermVector(st1,t1,abs(x_1+x_2))<<eol;
  nbErrors += checkValues(TermVector(st1,t1,abs(x_1+x_2)), (rootname+"/absst1pt1.in"), tol, errors, "Vec abs(st1+t1)", check);
  ssout<<"abs(sin(t1)+t1)= "<<abs(sin(t1)+t1)<<eol;
  nbErrors += checkValues(TermVector(abs(sin(t1)+t1)), (rootname+"/abssint1pt1.in"), tol, errors, "Vec abs(sin(t1)+t1)", check);

  //!linear form from TermVector
  ssout<<"TermVector(intg(omega,F1*v)) : "<<TermVector(intg(omega,F1*v))<<eol;
  nbErrors += checkValues(TermVector(intg(omega,F1*v)), (rootname+"/F1v.in"), tol, errors, "Vec TermVector(intg(omega,F1*v))", check);

  //! extension of TermVector
  TermVector tg1(u,gamma1,fsin, _name="t1");
  Function fm1(m1), fe(fexp);
  TermVector extg1=extension(tg1,fm1,omega,u);
  saveToFile("extg1",extg1,_format=_vtu);
  saveToFile("extg1",extg1,_format=_vizir4);
  TermVector extg2=extension(tg1,fm1,omega,u,fe);
  saveToFile("extg2",extg2,_format=_vtu);
  Point A(1.,0.,0.), B(2.,0.,0.), C(0.,2.,0.), D(0.,1.,0.), O(0.,0.,0.);
  Segment s1(_v1=A,_v2=B,_hsteps=0.1), s2(_v1=C,_v2=D,_hsteps=0.1);
  CircArc c1(_center=O,_v1=D, _v2=A, _hsteps=0.1,_domain_name="sigma1"),
          c2(_center=O,_v1=B, _v2=C, _hsteps=0.1,_domain_name="sigma2");
  Geometry cour=surfaceFrom(s1+c2+s2+c1,_domain_name="delta");
  Mesh meshc(cour,_shape=_triangle, _order=1, _generator=gmsh, _name="P1 mesh of quarter of crown");
  Domain delta=meshc.domain("delta"), sigma1=meshc.domain("sigma1"), sigma2=meshc.domain("sigma2");
  Space W(_domain=delta,_interpolation=P2, _name="Vd", _notOptimizeNumbering);
  Unknown ud(W, _name="ud");
  TermVector tv1(ud,sigma1,fcos, _name="fs1");
  Function fm2(m2), fr(rexp);
  TermVector extv1=extension(tv1,fm2,delta,ud,fr);
  saveToFile("extv1",extv1,_format=_vtu);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
