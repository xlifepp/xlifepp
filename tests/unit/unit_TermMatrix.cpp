/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_TermMatrix.cpp
	\author E. Lunéville
	\since 5 october 2012
	\date 11 september 2013

	Low level tests of TestMatrix class and related classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_TermMatrix {

// scalar 2D function
Real un(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

Real xfy(const Point& P, Parameters& pa = defaultParameters)
{
  return P(1)*P(2);
}

Real xf(const Point& P, Parameters& pa = defaultParameters)
{
  return P(1);
}

Real G(const Point& P, const Point& Q,Parameters& pa = defaultParameters)
{
    return 1.;
}

Real sin_n(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Number n = pa.basisIndex();       // get the index of function to compute (Parameters way)
  //Number n = getBasisIndex();     // get the index of function to compute (ThreadData way)
  return sqrt(2. / h) * sin(n * pi_ * x / h); // computation
}

void unit_TermMatrix(int argc, char* argv[], bool check)
{
  String rootname = "unit_TermMatrix";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;
  verboseLevel(30);
  numberOfThreads(1);

  //test 1D
  Mesh mesh1d(Segment(_xmin=0,_xmax=1,_nnodes=3,_side_names=Strings("zero","un")), _shape=_segment);
  mesh1d.printInfo(theCout);
  Domain S01=mesh1d.domain("Omega"), g0=mesh1d.domain("zero"), g1=mesh1d.domain("un");
  Space Vs(_domain=S01, _interpolation=_P1, _name="Vs");
  Unknown p(Vs, _name="p"); TestFunction q(p, _name="q");
  BilinearForm as=intg(S01,p*q);
  TermMatrix As(as, _name="As");
  theCout<<As<<eol;
  BilinearForm a0=intg(g0,p*q);
  TermMatrix A0(a0, _name="A0");
  theCout<<A0<<eol;
  BilinearForm a01=intg(g0,p*q)+intg(g1,p*q);
  TermMatrix A01(a01, _name="A01");
  theCout<<A01<<eol;

  // create a mesh and Domains
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=3,_side_names=sidenames),_shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");
  Domain gamma1=mesh2d.domain("Gamma_1");
  saveToFile("mesh2d.mshh", mesh2d);

  //! create Lagrange P1 space and unknown
  Interpolation *LagrangeP1=findInterpolation(Lagrange,standard,1,H1);
  Space V1(omega,*LagrangeP1,"V1",false);
  Unknown u(V1, _name="u"); TestFunction v(u, _name="v");
  Function f1(un,"one");
  Function fxy(xfy,"xy");
  Space V2(_domain=omega,_interpolation=P2,_name="V2",_notOptimizeNumbering);
  Unknown u2(V2, _name="u2"); TestFunction v2(u2, _name="v2");
  cpuTime("time for initialisation, mesh and space definition");

  //! useful TermVector
  TermVector un(u,omega,1., _name="un");
  TermVector unv(v,omega,1., _name="unv");

  //! test some volumic mass bilinear forms
  BilinearForm uv=intg(omega,u*v);
  TermMatrix M(uv, _name="UV");
  cpuTime("time for computation of M");
  M.viewStorage(ssout);
  ssout<<M;
  ssout<<"----> M*un|un="<<realPart(M*un|un)<<"\n\n";
  nbErrors+=checkVisual(M, rootname+"/MVis.in", tol, errors, "TermMatrix M(uv,UV); ", check);
  nbErrors+=checkValues(M, rootname+"/M.in", tol, errors, "TermMatrix M(uv,UV); ", check);
  nbErrors+=checkValue(realPart(M*un|un), 1., tol, errors, "realPart(M*un|un) ");

  TermMatrix Md(uv,_storage=csDual, _name="UV");
  cpuTime("time for computation of Md");
  Md.viewStorage(ssout);
  ssout<<Md;
  ssout<<"----> Md*un|un="<<realPart(Md*un|un)<<"\n\n";
  nbErrors+=checkVisual(Md, rootname+"/MdVis.in", tol, errors, "TermMatrix Md(uv,_csDualStorage); ", check);
  nbErrors+=checkValues(Md, rootname+"/Md.in", tol, errors, "TermMatrix Md(uv,_csDualStorage); ", check);
  nbErrors+=checkValue(realPart(Md*un|un), 1., tol, errors, "realPart(Md*un|un) ");
  clear(Md);

  BilinearForm uv1=intg(omega,f1*u*v);
  TermMatrix M1(uv1, _name="f1*UV");
  cpuTime("time for computation of M1");
  ssout<<M1;
  nbErrors+=checkValues(M1, rootname+"/M1.in", tol, errors, "TermMatrix M1(intg(omega,f1*u*v); ", check);

  BilinearForm xyuv=intg(omega,xfy*u*v);
  TermMatrix Mxy(xyuv, _name="xy*UV");
  cpuTime("time for computation of Mxy");
  ssout<<Mxy;
  ssout<<"----> Mxy*un|un="<<realPart(Mxy*un|un)<<"\n\n";
  nbErrors+=checkValues(Mxy, rootname+"/Mxy.in", tol, errors, "TermMatrix Md(intg(omega,xfy*u*v); ", check);
  nbErrors+=checkValue(realPart(Mxy*un|un), 0.25, tol, errors, "realPart(Mxy*un|un) ");
  clear(Mxy);

  BilinearForm uxyv=intg(omega,u*xfy*v);
  TermMatrix Mxy2(uxyv, _name="U*xy*V");
  cpuTime("time for computation of Mxy2");
  ssout<<Mxy2;
  nbErrors+=checkValues(Mxy2, rootname+"/Mxy2.in", tol, errors, "TermMatrix Mxy2(intg(omega,u*xfy*v); ", check);
  clear(Mxy2);

  BilinearForm uvxy=intg(omega,u*(v*xfy));
  TermMatrix Mxy3(uvxy, _name="UV*xy");
  cpuTime("time for computation of Mxy3");
  ssout<<Mxy3;
  nbErrors+=checkValues(Mxy3, rootname+"/Mxy3.in", tol, errors, "TermMatrix Mxy3(intg(omega,u*(v*xfy)); ", check);
  clear(Mxy3);

  //! test some volumic rigidity bilinear forms
  BilinearForm gugv=intg(omega,grad(u)|grad(v));
  TermMatrix K(gugv, _name="K");
  cpuTime("time for computation of K");
  ssout<<K;
  ssout<<"----> K*un="<<K*un<<"\n\n";
  nbErrors+=checkValues(K, rootname+"/K.in", tol, errors, "TermMatrix K intg(omega,grad(u)|grad(v)) ", check);
  nbErrors+=checkValues(K*un, rootname+"/Kfun.in", tol, errors, "TermVector K*un ", check);

  BilinearForm gugv1=intg(omega,f1*grad(u)|grad(v));
  TermMatrix K1(gugv1, _name="K1");
  cpuTime("time for computation of K1");
  ssout<<K1;
  nbErrors+=checkValues(K1, rootname+"/K1.in", tol, errors, "TermMatrix K1 intg(omega,f1*grad(u)|grad(v)) ", check);
  clear(K1);

  BilinearForm gugvxy=intg(omega,xfy*grad(u)|grad(v));
  TermMatrix Kxy(gugvxy, _name="Kxy");
  cpuTime("time for computation of Kxy");
  ssout<<Kxy;
  nbErrors+=checkValues(Kxy, rootname+"/Kxy.in", tol, errors, "TermMatrix Kxy intg(omega,xfy*grad(u)|grad(v)) ", check);
  clear(Kxy);

  BilinearForm dxuv=intg(omega,dx(u)*v);
  TermMatrix Dxuv(dxuv, _name="DxUV");
  cpuTime("time for computation of Dxuv");
  ssout<<Dxuv;
  ssout<<"----> DxUV*un="<<Dxuv*un<<"\n\n";
  nbErrors+=checkValues(Dxuv, rootname+"/Dxuv.in", tol, errors, "TermMatrix Dxuv intg(omega,intg(omega,dx(u)*v)) ", check);
  nbErrors+=checkValues(Dxuv*un, rootname+"/Dxuvfun.in", tol, errors, "TermVector Dxuv*un ", check);
  clear(Dxuv);

  TermMatrix Dxuvd(dxuv, _storage=skylineDual, _name="DxUVd");
  cpuTime("time for computation of Dxuvd");
  ssout<<Dxuvd;
  ssout<<"----> DxUVd*un="<<Dxuvd*un<<"\n\n";
  nbErrors+=checkValues(Dxuvd, rootname+"/Dxuvd.in", tol, errors, "TermMatrix Dxuvd _skylineDualStorage, DxUVd ", check);
  nbErrors+=checkValues(Dxuvd*un, rootname+"/Dxuvdfun.in", tol, errors, "TermVector Dxuvd*un ", check);
  clear(Dxuvd);

  BilinearForm udxv=intg(omega,u*dx(v));
  TermMatrix Dxvu(udxv, _name="DxVU");
  cpuTime("time for computation of Dxvu");
  ssout<<Dxvu;
  ssout<<"----> DxVU*un="<<Dxvu*un<<"\n\n";
  nbErrors+=checkValues(Dxvu, rootname+"/Dxvu.in", tol, errors, "TermMatrix Dxuvd _skylineDualStorage, DxVU ", check);
  nbErrors+=checkValues(Dxvu*un, rootname+"/Dxvufun.in", tol, errors, "TermVector Dxvu*un ", check);

  //! test some surfacic mass bilinear forms
  BilinearForm mg1=intg(gamma1,u*v);
  TermMatrix Mg1(mg1, _name="Mg1");
  cpuTime("time for computation of Mg1");
  ssout<<Mg1;
  TermVector ung(u,gamma1,1., _name="un_gamma1");
  ssout<<"----> Mg1*ung|ung="<<realPart(Mg1*ung|ung)<<"\n\n";
  nbErrors+=checkValues(Mg1, rootname+"/Mg1.in", tol, errors, "TermMatrix Mg1(gamma1,u*v) ", check);
  nbErrors+=checkValue(realPart(Mg1*ung|ung), 1., tol, errors, "realPart(Mg1*ung|ung) ");


  TermMatrix Mg1d(mg1, _storage=skylineSym, _name="Mg1d");
  cpuTime("time for computation of Mg1d");
  ssout<<Mg1d;
  ssout<<"----> Mg1d*ung|ung="<<realPart(Mg1d*ung|ung)<<"\n\n";
  nbErrors+=checkValues(Mg1d, rootname+"/Mg1d.in", tol, errors, "TermMatrix Mg1d(mg1,_skylineSymStorage,Mg1d) ", check);

  //! test some TermMatrix involving TermVector as coefficient
  TermVector TXY(u,omega,x_1*x_2);
  TermMatrix MXY(intg(omega,TXY*u*v), _name="MXY");
  ssout<<MXY;
  ssout<<"----> MXY*un|un="<<realPart(MXY*un|un)<<"\n\n";
  nbErrors+=checkValue(realPart(MXY*un|un), 0.229167, tol, errors, "realPart(MXY*un|un) ");
  clear(MXY);

  TermMatrix Mgx(intg(gamma1,xf*u*v), _name="Mgx");
  ssout<<Mgx;
  ssout<<"----> Mgx*un|un="<<realPart(Mgx*un|un)<<"\n\n";
  nbErrors+=checkValue(realPart(Mgx*un|un), 0.5, tol, errors, "realPart(Mgx*un|un) ");
  clear(Mgx);

  TermVector TX(u,omega,x_1);
  TermMatrix MgX(intg(gamma1,TX*u*v), _name="MgXY");
  ssout<<MgX;
  ssout<<"----> MgX*un|un="<<realPart(MgX*un|un)<<"\n\n";
  nbErrors+=checkValue(realPart(MgX*un|un), 0.5, tol, errors, "realPart(MgX*un|un) ");
  clear(MgX);

  //! test some surfacic bilinear forms requiring domain extension
  TermMatrix Dxuv1(intg(gamma1,dx(u)*v), _name="Dxuv1");
  cpuTime("time for computation of Dxuv1");
  ssout<<Dxuv1;
  nbErrors+=checkValues(Dxuv1, rootname+"/Dxuv1.in", tol, errors, "TermMatrix Dxuv1(intg(gamma1,dx(u)*v) ", check);

  TermMatrix Dxvu1(intg(gamma1,u*dx(v)), _name="Dxvu1");
  nbErrors+=checkValues(Dxvu1, rootname+"/Dxvu1.in", tol, errors, "TermMatrix Dxvu1(intg(gamma1,u*dx(v)) ", check);
  cpuTime("time for computation of Dxvu1");
  ssout<<Dxvu1;

  TermMatrix Dyuv1(intg(gamma1,dy(u)*v), _name="Dyuv1");
  cpuTime("time for computation of Dyuv1");
  ssout<<Dyuv1;
  nbErrors+=checkValues(Dyuv1, rootname+"/Dyuv1.in", tol, errors, "TermMatrix Dyuv1(intg(gamma1,dy(u)*v) ", check);

  TermMatrix Dxudxv1(intg(gamma1,dx(u)|dx(v)), _name="Dxudxv1");
  cpuTime("time for computation of Dxudxv1");
  ssout<<Dxudxv1;
  nbErrors+=checkValues(Dxudxv1, rootname+"/Dxudxv1.in", tol, errors, "TermMatrix Dxudxv1(intg(gamma1,dx(u)|dx(v)) ", check);

  TermMatrix Dtudtv(intg(gamma1,gradS(u)|gradS(v)), _name="Dtudtv");
  cpuTime("time for computation of Dtudtv");
  ssout<<Dtudtv;
  nbErrors+=checkValues(Dtudtv, rootname+"/Dtudtv.in", tol, errors, "TermMatrix Dtudtv(intg(gamma1,gradS(u)|gradS(v)) ", check);

  TermMatrix Dnuv1(intg(gamma1,ndotgrad(u)*v), _name="Dnuv1");
  cpuTime("time for computation of Dnuv1");
  ssout<<Dnuv1;
  nbErrors+=checkValues(Dnuv1, rootname+"/Dnuv1.in", tol, errors, "TermMatrix Dnuv1(intg(gamma1,ndotgrad(u)*v) ", check);

  TermMatrix DtuGdtv(intg(gamma1,gamma1,gradS(u)|(G*gradS(v))), _name="DtuGdtv");
  cpuTime("time for computation of DtuGdtv");
  ssout<<DtuGdtv;
  nbErrors+=checkValues(DtuGdtv, rootname+"/DtuGdtv.in", tol, errors, "TermMatrix DtuGdtv(intg(gamma1,gamma1,gradS(u)|(G*gradS(v))) ", check);

  // check P2
  TermMatrix Dxuv2(intg(gamma1,dx(u2)*v2), _name="Dxuv2");
  cpuTime("time for computation of Dxuv2");
  ssout<<Dxuv2;
  nbErrors+=checkValues(Dxuv2, rootname+"/Dxuv2.in", tol, errors, "TermMatrix Dxuv2(intg(gamma1,dx(u2)*v2) ", check);

  TermMatrix Dxvu2(intg(gamma1,u2*dx(v2)), _name="Dxvu2");
  nbErrors+=checkValues(Dxvu2, rootname+"/Dxvu2.in", tol, errors, "TermMatrix Dxvu2(intg(gamma1,u2*dx(v2)) ", check);
  cpuTime("time for computation of Dxvu2");
  ssout<<Dxvu2;

  TermMatrix Dyuv2(intg(gamma1,dy(u2)*v2), _name="Dyuv2");
  cpuTime("time for computation of Dyuv2");
  ssout<<Dyuv2;
  nbErrors+=checkValues(Dyuv2, rootname+"/Dyuv2.in", tol, errors, "TermMatrix Dyuv2(intg(gamma1,dy(u2)*v2) ", check);

  TermMatrix Dxudxv2(intg(gamma1,dx(u2)|dx(v2)), _name="Dxudxv2");
  cpuTime("time for computation of Dxudxv2");
  ssout<<Dxudxv2;
  nbErrors+=checkValues(Dxudxv2, rootname+"/Dxudxv2.in", tol, errors, "TermMatrix Dxudxv2(intg(gamma1,dx(u2)|dx(v2)) ", check);

  TermMatrix Dtudtv2(intg(gamma1,gradS(u2)|gradS(v2)), _name="Dtudtv2");
  cpuTime("time for computation of Dtudtv2");
  ssout<<Dtudtv2;
  nbErrors+=checkValues(Dtudtv2, rootname+"/Dtudtv2.in", tol, errors, "TermMatrix Dtudtv2(intg(gamma1,gradS(u2)|gradS(v2)) ", check);

  TermMatrix Dnuv2(intg(gamma1,ndotgrad(u2)*v2), _name="Dnuv2");
  cpuTime("time for computation of Dnuv2");
  ssout<<Dnuv2;
  nbErrors+=checkValues(Dnuv2, rootname+"/Dnuv2.in", tol, errors, "TermMatrix Dnuv2(intg(gamma1,ndotgrad(u2)*v2) ", check);

  TermMatrix DtuGdtv2(intg(gamma1,gamma1,gradS(u2)|(G*gradS(v2))), _name="DtuGdtv2");
  cpuTime("time for computation of DtuGdtv2");
  ssout<<DtuGdtv2;
  nbErrors+=checkValues(DtuGdtv2, rootname+"/DtuGdtv2.in", tol, errors, "TermMatrix DtuGdtv2(intg(gamma1,gamma1,gradS(u2)|(G*gradS(v2))) ", check);

  //!test some combination of bilinear forms
  Real k=1,alpha=1.;
  BilinearForm h=intg(omega,grad(u)|grad(v))-k*k*intg(omega,u*v)+alpha*intg(gamma1,u*v);
  TermMatrix H(h, _name="H");
  cpuTime("time for computation of H");
  ssout<<H;
  nbErrors+=checkValues(H, rootname+"/H.in", tol, errors, "TermMatrix h=intg(omega,grad(u)|grad(v))-k*k*intg(omega,u*v)+alpha*intg(gamma1,u*v) ", check);

  //! test storage conversion
  H.setStorage(_skyline,_sym);
  ssout<<H;
  nbErrors+=checkValues(H, rootname+"/H_skyline_sym.in", tol, errors, "TermMatrix  H.setStorage(_skyline,_sym) ", check);

  H.setStorage(_skyline,_dual);
  ssout<<H;
  nbErrors+=checkValues(H, rootname+"/H_skyline_dual.in", tol, errors, "TermMatrix  H.setStorage(_skyline,_dual) ", check);
  H.setStorage(_dense,_row);
  ssout<<H;
  nbErrors+=checkValues(H, rootname+"/H_dense_row.in", tol, errors, "TermMatrix  H.setStorage(_dense, _row) ", check);
  H.setStorage(_cs,_dual);
  ssout<<H;
  nbErrors+=checkValues(H, rootname+"/H_cs_dual.in", tol, errors, "TermMatrix  H.setStorage(_cs,_dual) ", check);

  //!test of linear combination of TermMatrix's
  TermMatrix Hlc=K-(k*k)*M+alpha*Mg1;
  cpuTime("time for computation of Hlc");
  ssout<<Hlc;
  nbErrors+=checkValues(Hlc, rootname+"/Hlc.in", tol, errors, "TermMatrix  Hlc=K-(k*k)*M+alpha*Mg1 ", check);

  clear(Hlc);

  //!test some combination of bilinear forms
  BilinearForm h2=intg(omega,grad(u)|grad(v))+intg(omega,u*dx(v))+alpha*intg(gamma1,u*v);
  TermMatrix H2(h2, _name="H2");
  cpuTime("time for computation of H2");
  ssout<<H2;
  nbErrors+=checkValues(H2, rootname+"/H2.in", tol, errors, "TermMatrix  H2=intg(omega,grad(u)|grad(v))+intg(omega,u*dx(v))+alpha*intg(gamma1,u*v)", check);

  //!test of linear combination of TermMatrix's
  TermMatrix H2lc=K+Dxvu+alpha*Mg1;
  cpuTime("time for computation of H2lc");
  ssout<<H2lc;
  nbErrors+=checkValues(H2lc, rootname+"/H2lc.in", tol, errors, "TermMatrix  H2lc=K+Dxvu+alpha*Mg1", check);

  //!H2lc=H2lc-H2;
  H2lc-=H2;
  ssout<<H2lc;
  nbErrors+=checkValues(H2lc, rootname+"/H2lcmeH2.in", tol, errors, "TermMatrix  H2lc-=H2", check);
  clear(H2lc);
  clear(H2);

  //! test product and inverse
  TermMatrix M12=M1*M1;
  thePrintStream<<"M12=M1*M1="<<M12;
  nbErrors+=checkValues(M12, rootname+"/M12.in", tol, errors, "TermMatrix  M12=M1*M1", check);
  TermMatrix invM1M1f=directSolve(M1,M1,_keep);
  ssout<<"invM1M1f=directSolve(M1,M1)="<<invM1M1f;
  nbErrors+=checkValues(invM1M1f, rootname+"/invM1M1f.in", tol, errors, "TermMatrix  invM1M1f=directSolve(M1,M1,_keep)", check );
  TermMatrix Id(M1,_idMatrix, _name="Id");
  ssout<<Id;
  ssout<<"directSolve(M1,Id,_keep)="<<eol; //directSolve(M1,Id,_keep);
  nbErrors+=checkValues(directSolve(M1,Id,_keep), rootname+"/directSolveM1Idkeep.in", tol, errors, "directSolve(M1,Id,_keep)", check);
  TermMatrix invM1=inverse(M1);
  ssout<<"invM1=inverse(M1)="<<invM1;
  ssout<<"invM1*M1="<<invM1*M1;
  ssout<<"invM1*M12="<<invM1*M12;
  nbErrors+=checkValues(invM1, rootname+"/invM1.in", tol, errors, "inv(M1)", check);
  nbErrors+=checkValues(invM1*M1, rootname+"/invM1M1.in", tol, errors, "invM1*M1", check);
  nbErrors+=checkValues(invM1*M12, rootname+"/invM1M12.in", tol, errors, "invM1*M12", check);
  TermMatrix MT=invM1*M12-M1;
  ssout<<"norm(invM1*M12-M1)="<<norm2(MT)<<eol;
  ssout<<"norm(invM1*M12-M1)="<<norm2(invM1*M12-M1)<<eol;
  ssout<<"out<<invM1*M12-M1="<<invM1*M12-M1<<eol;
  nbErrors+=checkValue(norm2(MT), 0., tol, errors, "norm2(MT)");
  nbErrors+=checkValue(norm2(invM1*M12-M1), 0., tol, errors, "norm2(invM1*M12-M1)");
  TermMatrix invM1M12mM1 = invM1*M12-M1;
  nbErrors+=checkValues(invM1M12mM1, rootname+"/invM1M12mM1.in", tol, errors, "invM1*M12-M1", check);

  thePrintStream<<"===== avant Mpq ====="<<eol;
  printMatrixStorages(thePrintStream);

  //!test SymbolicTermMatrix
  TermMatrix Mf;
  factorize(M,Mf);
  SymbolicTermMatrix S=~M+2*inv(M)*M*(~M+~M);
  ssout<<"S="<<S<<eol;
  nbErrors+=checkValue(S.asString (), tostring("(UV + ((2*inv(UV) x UV) x (UV + UV)))"), errors, "S=~M+2*inv(M)*M*(~M+~M)");
  TermVector X(u,omega,1.);
  TermVector SX=S*X;
  TermVector S5=5*M*X;
  ssout<<"|S5-SX|="<<norm2(S5-SX)<<eol;
  nbErrors+=checkValue(norm2(S5-SX), 0., tol, errors, "norm2(S5-SX)");

  //test TermMatrix from Matrix and Unknown/testFunction
  Parameters ps;
  Space W(_domain=S01, _basis=Function(sin_n, "sin_n", ps), _dim=2, _name="sinus basis");
  Unknown us(W, _name="us"); TestFunction vs(us, _name="vs");
  Matrix<Real> Ms(2,2);
  Ms(1,1)=1;Ms(1,2)=2;Ms(2,1)=3;Ms(2,2)=4;
  TermMatrix TMs(us,vs,Ms, _name="Ms");
  theCout<<TMs<<eol;

  //!change matrix value
  ssout<<M<<eol;
  nbErrors+=checkValues(M, rootname+"/M.in", tol, errors, "M change matrix value", check);
  Dof& dof=V1.locateDof(Point(1.,1.));
  ssout<<dof<<eol;
  Number i=M.rowRank(dof);
  theCout<<"i="<<i<<" M.getValue(i,i) -> "<<M.getValue(i,i);
  Real Mii=M.getValue(i,i).asReal();
  ssout<<"Mii="<<Mii<<eol;
  nbErrors+=checkValue(Mii, 0.0208333, tol, errors, "Mii");
  M.setValue(i,i,100.);
  ssout<<"after  M.setValue(i,i,100.) -> "<<M.getValue(i,i);
  nbErrors+=checkValue(M.getValue(i,i).asReal(), 100.,  errors, "M.getValue(i,i)");
  ssout<<"M.getValue(dof,dof) -> "<<M.getValue(dof,dof);
  M.setValue(dof,dof,200.);
  ssout<<"after M.setValue(dof,dof,200.) -> "<<M.getValue(dof,dof);
  nbErrors+=checkValue(M.getValue(dof,dof).asReal(), 200., errors, "M.getValue(dof,dof)");
  Dof& d=V1.locateDof(Point(0.5,0.5));
  ssout<<d<<eol;
  M.setRow(-10.,d);
  ssout<<"after M.setRow(-10.,d) "<<M<<eol;
  nbErrors+=checkValues(M, rootname+"/MsetRow-10d.in", tol, errors, "M.setRow(-10.,d)", check);
  M.setCol(33.,d);
  ssout<<"after M.setCol(33.,d) "<<M<<eol;
  nbErrors+=checkValues(M, rootname+"/MsetCol33d.in", tol, errors, "M.setCol(33.,d)", check);

  TermVector diag(v,omega,x_1,_name="x1");        //un TermVector compatible avec les colonnes de M
  SuTermMatrix& sM=*M.firstSut();                 //la SutermMatrix de M
  sM.actual_entries()->toUnsymmetric();           //passage en stockage non symmétrique
  MatrixStorage& sto=*sM.storagep();              //récupération du stockage
  for(int c=1;c<=diag.nbDofs();c++)               //boucle sur les composantes du vecteur diag et les colonnes de M
  {
    Real vc=diag.getValue(c).asReal();            // récupération de la composante c du vecteur diag
    std::set<Number> rows=sto.getRows(c);       // récupération des indices de lignes de la colonne C de M
    std::set<Number>::iterator its=rows.begin();
    for(;its!=rows.end();++its )                  // boucle sur les indices de lignes
    {
      Real nvc=sM.getValue(*its,c).asReal()*vc;   // récupération du coefficient (r,c) de M et produit avec vc
      sM.setValue(*its,c,nvc);                    // réaffectation du coefficient (r,c) de M
    }
  }

//  //other interpolations
//  Interpolation LagrangeP2(Lagrange,standard,2,H1);
//  Space V2(omega,LagrangeP2,"V2",false);
//  Unknown p(V2, _name="p");
//  TestFunction q(p, _name="q");
//  BilinearForm pq=intg(omega,p*q);
//  TermMatrix Mpq(pq, _name="pq");
//  cpuTime("time for computation of Mpq");
//  out<<Mpq;
//  TermVector pun(p,omega,1.,_name="pun");
//  TermVector qun(q,omega,1.,_name="qun");
//  out<<"\nMpq*un|un="<<realPart(Mpq*pun|qun)<<"\n\n";
//  clear(Mpq);
//
//  thePrintStream<<"===== avant Kpq ====="<<eol;
//  printMatrixStorages(thePrintStream);
//
//  BilinearForm gpgq=intg(omega,grad(p)|grad(q));
//  TermMatrix Kpq(gpgq, _name="Kpq");
//  cpuTime("time for computation of Kpq");
//  out<<Kpq;
//  out<<"\n"<<Kpq*pun<<"\n\n";
//  clear(Kpq);
//
//  thePrintStream<<"===== après Kpq ====="<<eol;
//  printMatrixStorages(thePrintStream);thePrintStream.flush();
//
//  Interpolation LagrangeP3(Lagrange,standard,3,H1);
//  Space V3(omega,LagrangeP3,"V3",false);
//  Unknown s(V3, _name="s");
//  TestFunction t(s, _name="t");
//  BilinearForm st=intg(omega,s*t);
//  TermMatrix Mst(st, _name="st");
//  cpuTime("time for computation of Mst");
//  out<<Mst;
//  TermVector sun(s,omega,1.,_name="pun");
//  TermVector tun(t,omega,1.,_name="qun");
//  out<<"\nMst*un|un="<<realPart(Mst*sun|tun)<<"\n\n";
//  clear(Mst);
//
//  BilinearForm gsgt=intg(omega,grad(s)|grad(t));
//  TermMatrix Kst(gsgt, _name="Kst");
//  cpuTime("time for computation of Kst");
//  out<<Kst;
//  out<<"\n"<<Kst*sun<<"\n";
//  clear(Kst);
//
//  Interpolation LagrangeP4(Lagrange,standard,4,H1);
//  Space V4(omega,LagrangeP4,"V4",false);
//  Unknown e(V4, _name="e");
//  TestFunction f(e, _name="f");
//  BilinearForm ef=intg(omega,e*f);
//  TermMatrix Mef(ef, _name="ef");
//  cpuTime("time for computation of Mef");
//  out<<Mef;
//  TermVector eun(e,omega,1.,_name="eun");
//  TermVector fun(f,omega,1.,_name="fun");
//  out<<"\nMef*un|un="<<realPart(Mef*eun|fun)<<"\n\n";
//  clear(Mef);
//
//  BilinearForm gegf=intg(omega,grad(e)|grad(f));
//  TermMatrix Kef(gegf, _name="Kef");
//  cpuTime("time for computation of Kef");
//  out<<Kef;
//  out<<"\n"<<Kef*eun<<"\n";
//  clear(Kef);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
