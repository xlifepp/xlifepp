/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
	\file unit_Interpolation.cpp
	\author E. Lunéville
	\since 16 feb 2012
	\date 11 may 2012

	Low level tests of Interpolation class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_Interpolation {

void unit_Interpolation(int argc, char* argv[], bool check)
{
  String rootname = "unit_Interpolation";
  trace_p->push(rootname);
  
  String errors;
  Number nbErrors = 0;
  
  //! constructor test and access operator
  Interpolation* fei1=findInterpolation(Lagrange, standard, 1, H1);
  theCout << "create Interpolation(Lagrange,standard,1,H1) : name=" << fei1->name;
  theCout << " subname=" << fei1->subname << " numtype=" << fei1->numtype;
  theCout << " space=" << fei1->conformSpaceName();
  theCout << " isContinuous()=" << fei1->isContinuous();
  theCout << " isScalar()=" << fei1->isScalar() << " maximumDegree()=" << fei1->maximumDegree();
  Interpolation* fei2=findInterpolation(Lagrange, GaussLobattoPoints, 1, H1);
  theCout << "\ncreate Interpolation(Lagrange,gaussLobatto,1,H1) : name=" << fei2->name;
  theCout << " subname=" << fei2->subname << " numtype=" << fei2->numtype;
  theCout << " space=" << fei2->conformSpaceName();
  theCout << " isContinuous()=" << fei2->isContinuous();
  theCout << " isScalar()=" << fei2->isScalar() << " maximumDegree()=" << fei2->maximumDegree();
  Interpolation* fei3=findInterpolation(Lagrange, GaussLobattoPoints, 1, H1);
  theCout << "\ncreate Interpolation(Hermite,standard,1,H2) : name=" << fei3->name;
  theCout << " subname=" << fei3->subname << " numtype=" << fei3->numtype;
  theCout << " space=" << fei3->conformSpaceName();
  theCout << " isContinuous()=" << fei3->isContinuous();
  theCout << " isScalar()=" << fei3->isScalar() << " maximumDegree()=" << fei3->maximumDegree();
  Interpolation* fei4=findInterpolation(Nedelec, firstFamily, 1, Hdiv);
  theCout << "\ncreate Interpolation(Nedelec,firstFamily,1,Hdiv) : name=" << fei4->name;
  theCout << " subname=" << fei4->subname << " numtype=" << fei4->numtype;
  theCout << " space=" << fei4->conformSpaceName();
  theCout << " isContinuous()=" << fei4->isContinuous();
  theCout << " isScalar()=" << fei4->isScalar() << " maximumDegree()=" << fei4->maximumDegree();
  Interpolation* fei5=findInterpolation(Lagrange, standard, 4, L2);
  theCout << "\ncreate Interpolation(Lagrange,standard,4,L2) : name=" << fei5->name;
  theCout << " subname=" << fei5->subname << " numtype=" << fei5->numtype;
  theCout << " space=" << fei5->conformSpaceName();
  theCout << " isContinuous()=" << fei5->isContinuous();
  theCout << " isScalar()=" << fei5->isScalar() << " maximumDegree()=" << fei5->maximumDegree();
  
  //! find an interpolation
  Interpolation* fei = findInterpolation(Lagrange, standard, 1, H1);
  theCout << "\nfindInterpolation(Lagrange,standard,1,H1): name=" << fei->name;
  theCout << " subname=" << fei->subname << " numtype=" << fei->numtype;
  theCout << " space=" << fei->conformSpaceName();
  theCout << " isContinuous()=" << fei->isContinuous();
  theCout << " isScalar()=" << fei->isScalar() << " maximumDegree()=" << fei->maximumDegree();
  fei = findInterpolation(RaviartThomas, standard, 1, Hdiv);
  theCout << "\nfindInterpolation(RaviartThomas,standard,1,Hdiv): name=" << fei->name;
  theCout << " subname=" << fei->subname << " numtype=" << fei->numtype;
  theCout << " space=" << fei->conformSpaceName();
  theCout << " isContinuous()=" << fei->isContinuous();
  theCout << " isScalar()=" << fei->isScalar() << " maximumDegree()=" << fei->maximumDegree();
  
  nbErrors+=checkValue(theCout, rootname+"/Interpolation.in", errors, "Interpollation", check);
  
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
