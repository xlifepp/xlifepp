/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_Intg_Rep.cpp
  \author E. Lunéville
  \since 16 feb 2018
  \date 16 feb 2012

  Low level tests of Integral representation
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"


using namespace xlifepp;

namespace unit_Intg_Rep
{
const Real k=1;
const Real lambda=1, mu=1;
Kernel L2d, L3d, H2d, H3d, N3d;
Point S2, S3;
Real EE;

Real l2d(const Point& P, Parameters& pa=defaultParameters)
{
  Real r;
  return L2d(S2,P,r);
}

Real dnl2d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Real> g(2);
  L2d.grady(S2,P,g);
  return P[0]*g[0]+P[1]*g[1];
}

Reals gradyl2d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Real> g(2);
  L2d.grady(S2,P,g);
  return g;
}

Reals gradxl2d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Real> g(2);
  L2d.gradx(S2,P,g);
  return g;
}

Real l3d(const Point& P, Parameters& pa=defaultParameters)
{
  Real r;
  return L3d(S3,P,r);
}

Real dnl3d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Real> g(3);
  L3d.grady(S3,P,g);
  return P[0]*g[0]+P[1]*g[1]+P[2]*g[2];
}

Complex h2d(const Point& P, Parameters& pa=defaultParameters)
{
  Complex r;
  return H2d(S2,P,r);
}

Complexes gradyh2d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Complex> g(2);
  H2d.grady(S2,P,g);
  return g;
}

Complex dnh2d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Complex> g(2);
  H2d.grady(S2,P,g);
  return P[0]*g[0]+P[1]*g[1];
}

Complex h3d(const Point& P, Parameters& pa=defaultParameters)
{
  Complex r;
  return H3d(S3,P,r);
}

Complex dnh3d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Complex> g(3);
  H3d.grady(S3,P,g);
  return P[0]*g[0]+P[1]*g[1]+P[2]*g[2];
}


Matrix<Complex> n3d(const Point& P, Parameters& pa=defaultParameters)
// Vector<Complex> n3d(const Point& P, Parameters& pa=defaultParameters)
// Vector<Complex> n3d(const Point& P, number_t k, Parameters& pa=defaultParameters)
{
  Matrix<Complex> rm;
  Vector<Complex> rv;
  bool diag=true;
  Real eps=0.0001;
  N3d(S3, P, rm);
  std::cout << "Sortie:"<<eol<<rm<<eol;
  for( dimen_t l=1; l< rm.numberOfRows()+1; l++)
    for( dimen_t c=l+1; c<rm.numberOfCols()+1;  c++)
    {
        if (( abs(rm(c,l)) != 0) || ( abs(rm(l,c)) !=0) )
        {
          diag=false;
        }
        else
        {
       //   std::cout <<eol << " RM "<<eol<< l<< ", "<< c <<" "<<rm(l,c)<<" "<<rm(c,l)<<eol;
        }
    }
  if (diag) rv=rm.diag();

  return rv;
  // return rm;// ?? peut etre faire *P?
  // return N3d(S3, P, rm);
}

Complex dnn3d(const Point& P, Parameters& pa=defaultParameters)
{
  Vector<Complex> g(3);
  N3d.grady(S3,P,g);
  return P[0]*g[0]+P[1]*g[1]+P[2]*g[2];
}


template <typename T>
Real intgRep(const IntegrationMethods &ims, const std::vector<Point>& xs, const Kernel& K,
             Domain Gamma, const Unknown& u, const TermVector& U, const TermVector& dnU,
             const Vector<T>& sol,const String& st="intgrep", bool withfile=false, bool withTime=false, bool check=true)
{
  Number np=xs.size();
  Vector<T> sl(np), dl(np);
  if(withTime) elapsedTime();
  integralRepresentation(xs, intg(Gamma,K*u,_method=ims), dnU, sl);
  integralRepresentation(xs, intg(Gamma,ndotgrad_y(K)*u,_method=ims), U, dl);
  if(withTime) elapsedTime(st);
  std::ofstream fl;
  String fn;
  if (check)
  {
      fn=st+".dat";
  }
  else
  {
      fn= inputsPathTo(st+".in");
  }
  if(withfile) {fl.open(fn.c_str()); }
  Real er=0.;
  for(Number k=0; k<np; ++k)
    {
      Real erk=std::abs(sol[k]-(dl[k]-sl[k]));
      er=std::max(er,erk);
      if(withfile) fl<<norm(xs[k])<<" "<<std::atan2(xs[k][1],xs[k][0])<<" "<<sol[k]<<" "<<dl[k]-sl[k]<<" "<<erk<<eol;
    }
  if(withfile) fl.close();
  theCout<<st<<" sup error = "<<er<<eol;

  return er;
}

template <typename T>
Real intgRepGrad(const IntegrationMethods &ims, const std::vector<Point>& xs, const Kernel& K,
                 Domain Gamma, const Unknown& u, const TermVector& U, const TermVector& dnU,
                 const Vector<T>& sol,const String& st="intgrep", bool withfile=false, bool withTime=false, bool check=true, bool isTM=false)
{
  Number np=xs.size();
  Vector<T> sl(np), dl(np);
  if(withTime) elapsedTime();
  integralRepresentation(xs, intg(Gamma,grad_x(K)*u,_method=ims), dnU, sl);
  integralRepresentation(xs, intg(Gamma,grad_x(ndotgrad_y(K))*u,_method=ims), U, dl);
  if(withTime) elapsedTime(st);
  String fn;
  if (check)
  {
      fn=st+".dat";
  }
  else
  {
      fn= inputsPathTo(st+".in");
  }
  std::ofstream fl;
  if(withfile) {fl.open(fn.c_str()); }
  Real er=0.;
  for(Number k=0; k<np; ++k)
    {
      Real erk=norm(sol[k]-(dl[k]-sl[k]));
      er=std::max(er,erk);
      if(withfile) fl<<norm(xs[k])<<" "<<std::atan2(xs[k][1],xs[k][0])<<" "<<norm(sol[k])<<" "<<norm(dl[k]-sl[k])<<" "<<erk<<eol;
      if (!isTestMode)
      {
         theCout<<norm(xs[k])<<"  dnsol="<<(sol[k][0]*xs[k][0]+sol[k][1]*xs[k][1])<<" dnapp="<<((dl[k][0]-sl[k][0])*xs[k][0]+(dl[k][1]-sl[k][1])*xs[k][1])
                <<" err="<<abs(((sol[k][0]-dl[k][0]+sl[k][0])*xs[k][0]+(sol[k][1]-dl[k][1]+sl[k][1])*xs[k][1]))<<eol;
         theCout<<norm(xs[k])<<"  dtsol="<<(sol[k][1]*xs[k][0]-sol[k][0]*xs[k][1])<<" dtapp="<<((dl[k][1]-sl[k][1])*xs[k][0]-(dl[k][0]-sl[k][0])*xs[k][1])
                <<" err="<<abs(((sol[k][1]-dl[k][1]+sl[k][1])*xs[k][0]-(sol[k][0]-dl[k][0]+sl[k][0])*xs[k][1]))<<eol;
      }
    }
  if(withfile) fl.close();
  theCout<<st<<" sup error = "<<er<<eol;
  return er;
}

void unit_Intg_Rep(int argc, char* argv[], bool check)
{
  String rootname = "unit_Intg_Rep";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(0);
  numberOfThreads(1);

  Number nbErrors = 0;
  String errors;
  Real tol=0.001;
  Real tolG=0.02;
  String RepInputs;
  if (check)
  {
      RepInputs="./";
  }
  else
  {
      RepInputs= rootname +"/";
  }
  L2d=Laplace2dKernel();
  L3d=Laplace3dKernel();
  H2d=Helmholtz2dKernel(k);
  H3d=Helmholtz3dKernel(k);
  N3d=Navier3dKernel(k, lambda, mu);

  bool lap2d=false, helm2d=false;
  //bool lap3d=true, helm3d=true, nav3d=true;
  bool lap3d=false, helm3d=false, nav3d=true;
  bool p0=true, p1=true;
  bool withFile=true, withTime=false;
  Number minord=10, maxord=10, stepord=2, expord=4;
  bool quad = false;
  String LS="_Lenoir-Salles";
  String GL="_Gauss-Legendre";
  String Ti;
  Real er, erg;
  IntegrationMethods ims;

  if (lap2d || helm2d)
  {
    //! =========================================================================
    //!                            Disk 2D
    //! =========================================================================
    Number nmesh=2000;
    Disk disk(_center=Point(0.,0.),_radius=1.,_nnodes=nmesh,_side_names="Gamma");
    Mesh mesh(disk, _shape=_segment, _order=1, _generator=_gmsh);
    Domain Gamma = mesh.domain("Gamma");
    Gamma.setNormalOrientation(_towardsInfinite);

    //! Define spaces, unknown and testfunction
    Space V0(_domain=Gamma, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
    Unknown u0(V0, _name="u0");
    Space V1(_domain=Gamma, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
    Unknown u1(V1, _name="u1");

    S2=Point(0.5,0.); //source point

    //! on a normal line crossing first element at center
    const MeshElement* melt=V0.element_p(98)->geomElt_p->meshElement();
    Point x0=melt->center();
    Point n=melt->normalVector();
    Number np=8;
    Real e=0, a=-0.02, b=0.02, d=(b-a)/np;
    std::vector<Point> xs(np);
    for (Number k=0; k<np; ++k) xs[k]=x0+(a+k*d)*n;

    if (lap2d)
    {
      IntegrationMethods ims(_method=_LenoirSalles2dIR); //analytical method
      Vector<Real> sol(np,0.);
      Vector<Vector<Real> > gsol(np,Vector<Real>(2,0.));
      for (Number k=0; k<np; ++k)
      {
        if (norm2(xs[k]) >= 0.9999)
        {
          sol[k]=l2d(xs[k]);
          gsol[k]=gradyl2d(xs[k]);
        }
      }

      //!=========================================================================
      //!                            Laplace 2D P0
      //! =========================================================================
      if (p0)
      {
        Ti="Laplace2d_P0";
        TermVector U0(u0,Gamma,l2d), dnU0(u0,Gamma,dnl2d);
        for (Number ord=minord; quad && ord<=maxord; ord+=stepord)  //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          er=intgRep<Real>(ims,xs,L2d,Gamma,u0,U0,dnU0,sol,RepInputs+Ti+GL+"_"+tostring(ord),withFile,withTime, check);
          erg=intgRepGrad<Vector<Real> >(ims,xs,L2d,Gamma,u0,U0,dnU0,gsol,RepInputs+"Grad_"+Ti+GL+"_"+tostring(ord),withFile,withTime, check, true);
          nbErrors +=checkValue(er, 0.6, tol, errors, "Laplace 2D P0"+GL+"_"+tostring(ord));
          nbErrors +=checkValue(erg, 0.00006, tol, errors, "Laplace 2D Grad P0"+GL+"_"+tostring(ord));
        }
        er=intgRep<Real>(ims,xs,L2d,Gamma,u0,U0,dnU0,sol,RepInputs+Ti+LS,withFile,withTime, check);
        erg=intgRepGrad<Vector<Real> >(ims,xs,L2d,Gamma,u0,U0,dnU0,gsol,RepInputs+"Grad_"+Ti+LS,withFile,withTime, check, true);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Laplace 2D P0"+LS);
        nbErrors +=checkValue(erg, 0.00001, tolG, errors, "Laplace 2D Grad P0"+LS);
      }

      //! =========================================================================
      //!                            Laplace 2D P1
      //! =========================================================================
      if (p1)
      {
        Ti="Laplace2d_P1";
        TermVector U1(u1,Gamma,l2d), dnU1(u1,Gamma,dnl2d);
        for (Number ord=minord; quad && ord<=maxord; ord+=stepord)  //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          er=intgRep<Real>(ims,xs,L2d,Gamma,u1,U1,dnU1,sol,RepInputs+Ti+GL+"_"+tostring(ord),withFile,withTime, check);
          erg=intgRepGrad<Vector<Real> >(ims,xs,L2d,Gamma,u1,U1,dnU1,gsol,"Grad_"+RepInputs+Ti+GL+"_"+tostring(ord),withFile,withTime, check, true);
          nbErrors +=checkValue(er, 0.0001, tol, errors, "Laplace 2D P1"+GL);
          nbErrors +=checkValue(erg, 0.00001, tolG, errors, "Laplace 2D Grad P1"+GL);
        }
        er=intgRep<Real>(ims,xs,L2d,Gamma,u1,U1,dnU1,sol, RepInputs+Ti+LS,withFile,withTime, check);
        erg=intgRepGrad<Vector<Real> >(ims,xs,L2d,Gamma,u1,U1,dnU1,gsol,RepInputs+"Grad_"+Ti+LS,withFile,withTime, check, true);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Laplace 2D P1"+LS);
        nbErrors +=checkValue(erg, 0.00001, tolG, errors, "Laplace 2D Grad P1"+LS);
      }
    }

    if (helm2d)
    {
      IntegrationMethods imsh(_method=LenoirSalles2dIR(),_functionPart=_singularPart,_bound=theRealMax,
                              _quad=QuadratureIM(_GaussLegendreRule,expord),_functionPart=_regularPart,_bound=theRealMax);
      Vector<Complex> sol(np,0.);
      Vector<Vector<Complex> > gsol(np,Vector<Complex>(2,0.));
      for (Number k=0; k<np; ++k)
      {
        if (norm2(xs[k]) >= 0.9999)
        {
          sol[k]=h2d(xs[k]);
          gsol[k]=gradyh2d(xs[k]);
        }
      }

      //! =========================================================================
      //!                            Helmholtz 2D P0
      //! =========================================================================
      if (p0)
      {
        Ti="Helmholtz2d_P0";
        TermVector Uh0(u0,Gamma,h2d), dnUh0(u0,Gamma,dnh2d);
        for (Number ord=minord; quad && ord<=maxord; ord+=stepord)  //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          er=intgRep<Complex>(ims,xs,H2d,Gamma,u0,Uh0,dnUh0,sol,RepInputs+Ti+GL+"_"+tostring(ord),withFile,withTime, check);
          erg=intgRepGrad<Vector<Complex> >(ims,xs,H2d,Gamma,u0,Uh0,dnUh0,gsol,RepInputs+"Grad_"+Ti+GL+"_"+tostring(ord),withFile,withTime, check, true);
          nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 2D P0"+GL);
          nbErrors +=checkValue(erg, 0.00001, tolG, errors, "Helmholtz 2D Grad P0"+GL);
        }
        er=intgRep<Complex>(imsh,xs,H2d,Gamma,u0,Uh0,dnUh0,sol,RepInputs+Ti+LS,withFile,withTime, check);
        erg=intgRepGrad<Vector<Complex> >(imsh,xs,H2d,Gamma,u0,Uh0,dnUh0,gsol,RepInputs+"Grad_"+Ti+LS,withFile,withTime, check, true);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 2D P0"+LS);
        nbErrors +=checkValue(erg, 0.00001, tolG, errors, "Helmholtz 2D Grad P0"+LS);
      }

      //! =========================================================================
      //!                            Helmholtz 2D P1
      //! =========================================================================
      if (p1)
      {
        TermVector Uh1(u1,Gamma,h2d), dnUh1(u1,Gamma,dnh2d);
        for (Number ord=minord; quad && ord<=maxord; ord+=stepord)  //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          er=intgRep<Complex>(ims,xs,H2d,Gamma,u1,Uh1,dnUh1,sol,"Helmholtz2d P1 Gauss-Legendre_"+tostring(ord),withFile,withTime, check);
          erg=intgRepGrad<Vector<Complex> >(ims,xs,H2d,Gamma,u1,Uh1,dnUh1,gsol,"Grad Helmholtz2d P1 Gauss-Legendre_"+tostring(ord),withFile,withTime, check, true);
          nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 2D P1"+GL);
          nbErrors +=checkValue(erg, 0.00001, tolG, errors, "Helmholtz 2D Grad P1"+GL);
        }
        er=intgRep<Complex>(imsh,xs,H2d,Gamma,u1,Uh1,dnUh1,sol,"Helmholtz2d P1 Lenoir-Salles",withFile,withTime, check);
        erg=intgRepGrad<Vector<Complex> >(imsh,xs,H2d,Gamma,u1,Uh1,dnUh1,gsol,"Grad Helmhotz2d P1 Lenoir-Salles",withFile,withTime, check, true);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 2D P1"+LS);
        nbErrors +=checkValue(erg, 0.00001, tolG, errors, "Helmholtz 2D Grad P1"+LS);
      }
    }
  }

  if (lap3d || helm3d || nav3d)
  {
    //! =========================================================================
    //!                            Sphere 3D
    //! =========================================================================
    S3=Point(0.5,0.,0.);
    Number nmesh=15;
    Sphere sphere(_center=Point(0.,0.,0.),_radius=1.,_nnodes=nmesh,_side_names="Sigma");
    Mesh mesh3d(sphere, _shape=_triangle, _order=1, _generator=_gmsh);
    Domain Sigma = mesh3d.domain("Sigma");
    Sigma.setNormalOrientation(_towardsInfinite);

    //! Define spaces, unknown and testfunction
    Space W0(_domain=Sigma, _interpolation=_P0, _name="W0", _notOptimizeNumbering);
    Unknown w0(W0, _name="w0");
    Unknown wv0(W0, _name="wv0", _dim=3);
    Space W1(_domain=Sigma, _interpolation=_P1, _name="W1", _notOptimizeNumbering);
    Unknown w1(W1, _name="w1");
    Unknown wv1(W1, _name="wv1", _dim=3);

    withTime=false; withFile=false;
    Number np=101;
    Real e=0, a=1.1, b=1.2;
    std::vector<Point> zs(np);
    for (Number k=0; k<np; ++k) zs[k]=Point(a+k*(b-a)/np,0.,0.);
    if (lap3d)
    {
      Vector<Real> sol(np,0.);
      for(Number k=0; k<np; ++k)
        if(norm2(zs[k]) >= 1.) sol[k]=l3d(zs[k]);

      //! =========================================================================
      //!                            Laplace 3D P0
      //! =========================================================================
      if (p0)
      {
        Ti="Laplace3d_P0";

        TermVector U0(w0,Sigma,l3d), dnU0(w0,Sigma,dnl3d);
        for (Number ord=minord; ord<=maxord; ord+=stepord) //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          er=intgRep<Real>(ims,zs,L3d,Sigma,w0,U0,dnU0,sol,RepInputs+Ti+GL+"_"+tostring(ord),withFile,withTime, check);
          nbErrors +=checkValue(er, 0.0001, tol, errors, "Laplace 3D P0"+GL);
        }
        IntegrationMethods ims(_method=_LenoirSalles3dIR); //analytical method
        er=intgRep<Real>(ims,zs,L3d,Sigma,w0,U0,dnU0,sol,RepInputs+Ti+LS,withFile, check);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Laplace 3D P0"+LS);
      }

      //! =========================================================================
      //!                            Laplace 3D P1
      //! =========================================================================
      if (p1)
      {
        Ti="Laplace3d_P1";
        TermVector U1(w1,Sigma,l3d), dnU1(w1,Sigma,dnl3d);
        for (Number ord=minord; ord<=maxord; ord+=stepord)  //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          er=intgRep<Real>(ims,zs,L3d,Sigma,w1,U1,dnU1,sol,RepInputs+Ti+GL+"_"+tostring(ord)+".in",withFile,withTime, check);
          nbErrors +=checkValue(er, 0.0001, tol, errors, "Laplace 3D P1"+GL);
        }
        er=intgRep<Real>(ims,zs,L3d,Sigma,w1,U1,dnU1,sol, RepInputs+Ti+LS+".in",withFile,withTime, check);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Laplace 3D P1"+LS);
      }
    }

    if (helm3d)
    {
      // IntegrationMethods imsh(LenoirSalles3dIR(),_singularPart,theRealMax,
      //                         QuadratureIM(_GaussLegendreRule,expord),_regularPart,theRealMax);
      // std::cout << imsh << eol;
      IntegrationMethods imsh(_method=LenoirSalles3dIR(),_functionPart=_singularPart,_bound=theRealMax,
                              _quad=QuadratureIM(_GaussLegendreRule,expord),_functionPart=_regularPart);
      Vector<Complex> sol(np,0.);
      for (Number k=0; k<np; ++k)
      { if (norm2(zs[k]) >= 1.) sol[k]=h3d(zs[k]); }

      //! =========================================================================
      //!                            Helmholtz 3D P0
      //! =========================================================================
      if (p0)
      {
        TermVector U0(w0,Sigma,h3d), dnU0(w0,Sigma,dnh3d);
        for (Number ord=minord; ord<=maxord; ord+=stepord)  //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          intgRep<Complex>(ims,zs,H3d,Sigma,w0,U0,dnU0,sol,"Helmholtz3d P0 Gauss-Legendre_"+tostring(ord),withFile,withTime, check);
          nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 3D P0"+GL);
        }

        er=intgRep<Complex>(imsh,zs,H3d,Sigma,w0,U0,dnU0,sol,"Helmholtz3d P0 Lenoir-Salles",withFile,withTime, check);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 3D P0"+LS);
      }

      //! =========================================================================
      //!                            Helmholtz 3D P1
      //! =========================================================================
      if (p1)
      {
        TermVector U1(w1,Sigma,h3d), dnU1(w1,Sigma,dnh3d);
        for (Number ord=minord; ord<=maxord; ord+=stepord)  //quadrature method
        {
          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
          er=intgRep<Complex>(ims,zs,H3d,Sigma,w1,U1,dnU1,sol,"Helmholtz3d P1 Gauss-Legendre_"+tostring(ord),withFile,withTime, check);
          nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 3D P1"+GL);
        }
        er=intgRep<Complex>(imsh,zs,H3d,Sigma,w1,U1,dnU1,sol,"Helmholtz3d P1 Lenoir-Salles",withFile,withTime, check);
        nbErrors +=checkValue(er, 0.0001, tol, errors, "Helmholtz 3D P1"+LS);
      }
    }

    if (nav3d)
    {
      //Vector<Matrix<Complex>> sol(np,Matrix<Complex>(3,3));
      Vector<Vector<Complex>> sol(np,Vector<Complex>(3));
      for(Number k=0; k<np; ++k)
      {
        if(norm2(zs[k]) >= 1.) sol[k]=n3d(zs[k]);
        // std::cout<<"zs[k]: "<<zs[k]<<eol<<"sol[k]="<<sol[k]<<eol<<eol;
        // for(Number i=1; i<4; ++i)
        //   for(Number j=i+1; j<4; j++)
        //     {
        //       if ( (abs(sol[k](i,j))!=0 ) || (abs(sol[k](j,i))!=0 ) ) std::cout <<"SOS "<<i<<" " <<j << sol[k](i,j)<<eol;
        //       Number toto;
        //       if ( (abs(sol[k](i,j))!=0 ) || (abs(sol[k](j,i))!=0 ) ) std::cin >> toto;
        //     }
      }
      //! =========================================================================
      //!                            Navier 3D P0
      //! =========================================================================
      if (p0)
      {
        Ti="Navier3d_P0";


      //  Matrix<Complex> MC(3,3);
      //  MC=sol[1];
      //  std::cout << "diag MC="<<eol<<MC.diag()<<eol;;
      //  std::cout << "1ere colonne de  MC="<<eol<<MC.column(1)<<eol;;
      //  TermVector U00(wv0,Sigma,MC.diag());
      //  std::cout << "U00:"<<eol<<U00.size()<<eol<<eol;


       //TermVector U0(wv0,Sigma,n3d), dnU0(w0,Sigma,dnn3d);
        // for (Number ord=minord; ord<=maxord; ord+=stepord) //quadrature method
        // {
        //   IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
        //   er=intgRep<Real>(ims,zs,N3d,Sigma,w0,U0,dnU0,sol,RepInputs+Ti+GL+"_"+tostring(ord),withFile,withTime, check);
        //   nbErrors +=checkValue(er, 0.0001, tol, errors, "Navier 3D P0"+GL);
        // }
        // IntegrationMethods ims(_method=_LenoirSalles3dIR); //analytical method
        // er=intgRep<Real>(ims,zs,N3d,Sigma,w0,U0,dnU0,sol,RepInputs+Ti+LS,withFile, check);
        // nbErrors +=checkValue(er, 0.0001, tol, errors, "Navier 3D P0"+LS);
      }

      //! =========================================================================
      //!                            Navier 3D P1
      //! =========================================================================
//      if (p1)
//      {
//        std::cout << "Navier P1"<<eol;
//        Ti="Navier3d_P1";
//        TermVector U1(w1,Sigma,l3d), dnU1(w1,Sigma,dnl3d);/////////////////////////////////
//        for (Number ord=minord; ord<=maxord; ord+=stepord)  //quadrature method
//        {
//          IntegrationMethods ims(_quad=_GaussLegendreRule, _order=ord);
//          er=intgRep<Real>(ims,zs,N3d,Sigma,w1,U1,dnU1,sol,RepInputs+Ti+GL+"_"+tostring(ord)+".in",withFile,withTime, check);
//          nbErrors +=checkValue(er, 0.0001, tol, errors, "Navier 3D P1"+GL);
//        }
//        er=intgRep<Real>(ims,zs,N3d,Sigma,w1,U1,dnU1,sol, RepInputs+Ti+LS+".in",withFile,withTime, check);
//        nbErrors +=checkValue(er, 0.0001, tol, errors, "Navier 3D P1"+LS);
//      }

    }
  }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

} // end of namespace dev_Intg_Rep
