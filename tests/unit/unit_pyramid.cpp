/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_pyramid.cpp
	\author N. Kielbasiewicz
	\since 5 nov 2012
	\date 5 nov 2012

	Low level tests of refPyramid class.
 */

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_pyramid {

void unit_pyramid(int argc, char* argv[], bool check)
{
  String rootname = "unit_pyramid";
  trace_p->push(rootname);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!test GeomRefPyramid
  verboseLevel(12);
  theCout<<"----------------------------- GeomRefPyramid test ----------------------------------"<<eol;
  GeomRefPyramid grt;
  theCout<<grt;
  for(Number i=0;i<grt.nbSides();++i)
  {
      Number v1=grt.sideVertexNumbers()[i][0], v2=grt.sideVertexNumbers()[i][1], v3=grt.sideVertexNumbers()[i][2];
      theCout<<"sideWithVertices("<<v1<<","<<v2<<","<<v3<<") = "<<grt.sideWithVertices(v1,v2,v3)<<eol;
  }
  for(Number i=0;i<grt.nbSideOfSides();++i)
  {
      Number v1=grt.sideOfSideVertexNumbers()[i][0], v2=grt.sideOfSideVertexNumbers()[i][1];
      theCout<<"sideWithVertices("<<v1<<","<<v2<<") = "<<grt.sideWithVertices(v1,v2)<<eol;
  }
  nbErrors += checkValue(theCout, rootname+"/GeomRefPyramid.in" , errors, "Test Geom Ref Pyramid",check);

  //!test interpolation
  verboseLevel(9);
  theCout<<eol<<"----------------------------- interpolation test ----------------------------------"<<eol;
  Interpolation* o1 = findInterpolation(_Lagrange, _standard, 1, H1);
  Interpolation* o2 = findInterpolation(_Lagrange, _standard, 2, H1);
  RefElement* pyramidO1 = findRefElement(_pyramid, o1);
  RefElement* pyramidO2 = findRefElement(_pyramid, o2);

  verboseLevel(10);
  theCout << *(pyramidO1) << std::endl;
  theCout << *(pyramidO2) << std::endl;
  nbErrors += checkValue(theCout, rootname+"/Interpolation.in" , errors, "Test Interpolation",check);

  //! test splitting
  theCout << "split Fast O1 to P1 : " << pyramidO1->splitP1() << std::endl;
  theCout << "split Fast O2 to P1 : " << pyramidO2->splitP1() << std::endl;
  theCout << "split Fast O1 to O1 : " << pyramidO1->splitO1() << std::endl;
  theCout << "split Fast O2 to O1 : " << pyramidO2->splitO1() << std::endl;
  nbErrors += checkValue(theCout, rootname+"/splitting.in" , errors, "Test splitting",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
