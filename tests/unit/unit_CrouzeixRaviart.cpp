/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Fourier; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file unit_CrouzeixRaviart.cpp
\author E. Lunéville
\since 9 mar 2018
\date 9 mar 2018

    Solve  the 2D Laplace Dirichlet problem using Crouzeix-Raviart element
        -lap(u) = f  on ]0,1[x]0,1[
        u=0 on boundary

    exact solution : uex=sin(n*pi*x)*sin(m*pi*y) -> f=(n*n+m*m)*pi*pi*uex

*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_CrouzeixRaviart
{

Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Number n=2, m=1;
  return sin(n*pi_*x)*sin(m*pi_*y);
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  Number n=2, m=1;
  return (n*n+m*m)*pi_*pi_*uex(P);
}

void unit_CrouzeixRaviart(int argc, char* argv[], bool check)
{
  String rootname = "unit_CrouzeixRaviart";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(1);
  String errors;
  Number nbErrors = 0;
  Real eps=0.005;

  // mesh
  SquareGeo sq(_origin=Point(0.,0.),_length=1., _nnodes=30,_domain_name="Omega",_side_names="Sigma");
  Mesh m(sq, _shape=_triangle, _order=1, _generator=_structured);
  Domain Omega=m.domain("Omega");
  Domain Sigma=m.domain("Sigma");
  // space and form
  Space V(_domain=Omega, _FE_type=CrouzeixRaviart, _order=1, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");
  BilinearForm a=intg(Omega,grad(u)|grad(v));
  EssentialConditions ec = (u|Sigma=0);
  // compute and solve
  TermMatrix A(a, ec, _name="A");
  TermVector F(intg(Omega,f*v));
  TermVector X=directSolve(A,F);
  // compute error
  TermVector Xex(u,Omega,uex);
  TermVector er=X-Xex;
  theCout<<"Sup error = "<<norminfty(er)<<eol;
  nbErrors += checkValuesNinfty(X , Xex , eps,  errors, "Sol test Crouzeix-Raviart");

  //! export in P1
  Space V1(_domain=Omega,_FE_type=Lagrange, _order=1, _name="V1");
  TermVector X1=projection(X,V1);
  nbErrors += checkValues(X1 , rootname+"/X1.in" , eps,  errors, "X1", check);
  saveToFile("X1.vtu",X1,_format=_vtu, _aUniqueFile);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
