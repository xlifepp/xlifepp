/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Dirichlet_Condition.cpp
  \author E. Lunéville
	\since 22 jan 2014
	\date  22 jan 2014

	Test Dirichlet condition on Laplace problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Dirichlet_Condition {

//data on sigma-
Real un(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  return -8.;
}


Real solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1);
  return 4.*(x-0.5)*(x-0.5);
}

void unit_Dirichlet_Condition(int argc, char* argv[], bool check)
{
  String rootname = "unit_Dirichlet_Condition";
  trace_p->push(rootname);
  verboseLevel(40);

  Number nbErrors = 0;
  String errors;
  Real eps=0.0001;

  //!create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=1";
  sidenames[2] = "y=1"; sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=10,_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaM=mesh2d.domain("x=0");
  Domain sigmaP=mesh2d.domain("x=1");

  //!create interpolation
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");

  //!create bilinear form and linear form
  BilinearForm auv=intg(omega,grad(u)|grad(v));
  LinearForm fv=intg(omega,f*v);
  EssentialConditions ecs= (u|sigmaM = 1.) & (u|sigmaP = 1.);
  TermMatrix A(auv, ecs, _name="A");
  TermVector B(fv, _name="B");

  //!solve linear system AX=F using LU factorization
  TermMatrix LLT;
  ldltFactorize(A,LLT);
  TermVector U=directSolve(LLT,B);

  //!compute error
  BilinearForm uv=intg(omega,u*v);
  TermMatrix M(uv, _name="M");
  TermVector Uex(u,omega,solex);
  nbErrors += checkValuesL2(U , Uex , M, eps,  errors, "pseudo-reduction, L2 error =");

  //!test penalization method
  TermMatrix Ap(auv, ecs, _reduction=ReductionMethod(_penalizationReduction,10000.), _name="Ap");
  TermVector Up=directSolve(Ap,B);
  nbErrors += checkValuesL2(Up , Uex , M, eps,  errors, "penalization reduction, L2 error =");

  TermMatrix A2(auv, _name="A2");
  A2.applyEssentialConditions(ecs);
  TermMatrix LLT2;
  ldltFactorize(A2,LLT2);
  TermVector U2=directSolve(LLT2,B);

  //!compute error
  nbErrors += checkValuesL2(U2 , Uex , M, eps,  errors, "pseudo-reduction, L2 error =");

  //!test real reduction method
  TermMatrix Ar(auv, ecs, _realReductionMethod, _name="Ar");
  thePrintStream<<Ar<<eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
