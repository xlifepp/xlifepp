/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Domain.cpp
	\author E. Lunéville
	\since 05 apr 2012
	\date 11 may 2012

	Low level tests of Space class and related classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_Domain
{

struct WordGreaterComparator
{
    bool operator()(const int & left, const int & right) const
    {
        return (left >right);
    }
};

Real f(const Point&x, Parameters& pa = defaultParameters)
{
  Vector<Real>& n=getVector(_n);
  theCout<<"n="<<n<<eol;
  return n(1);
}

Real g(const Point&x, const Point&y, Parameters& pa = defaultParameters)
{
  Vector<Real>& nx=getVector(_nx);
  Vector<Real>& ny=getVector(_ny);
  theCout<<"nx="<<nx<<eol;
  theCout<<"ny="<<ny<<eol;
  return nx(1);
}

void unit_Domain(int argc, char* argv[], bool check)
{
  String rootname = "unit_Domain";
  trace_p->push(rootname);
  verboseLevel(50);

  String errors;
  Number nbErrors = 0;

  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma"; sidenames[2]="Sigma";
  Mesh M(inputsPathTo(rootname+"/mailM.msh"), _name="P1 mesh of [0,1]x[0,1]");
  //! construction of  mesh domains
  Domain Omega=M.domain("Omega"); // a mesh domain
  theCout << "test Omega:" << eol;
  print_Domain(theCout, Omega);
  Domain Gamma=M.domain("Gamma"); // a mesh domain
  theCout << eol << eol << "test Gamma:" << eol;
  print_Domain(theCout, Gamma);
  Domain Sigma=M.domain("Sigma"); // a mesh domain
  theCout << eol << eol << "test Sigma:"  << eol;
  print_Domain(theCout, Sigma);

  //! check computeStatistics
  verboseLevel(2);
  Omega.computeStatistics(true);
  theCout << "\nupdate domain info Omega:" << eol;
  print_Domain(theCout, Omega);
  Gamma.computeStatistics();
  theCout << "\nupdate domain info Gamma:" << eol;
  print_Domain(theCout, Gamma);
  verboseLevel(50);

  //! test accessors
  theCout << eol << eol << "test accessors on Sigma: name=" << Sigma.name() << ", dim=" << Sigma.dim() << ", type=" << words("domain type", Sigma.domType());
  theCout << ", isUnion=" << words(Sigma.isUnion()) << ", isIntersection=" << words(Sigma.isIntersection());
  theCout << ", meshDomain=" << Sigma.meshDomain()->name() << eol;
  theCout << "measure(Omega)=" << measure(Omega) << " measure(Gamma)=" << measure(Gamma) << " measure(Sigma)=" << measure(Sigma) << eol;

  //! construction of composite domains
  GeomDomain OmgUGam(_union, Omega, Gamma);
  theCout << eol << "test union OmgUGam(_union,Omega,Gamma):" /*<< OmgUGam */<<eol;
  print_Domain(theCout, OmgUGam);
  GeomDomain OmgUSig(_union, Omega, Sigma);
  theCout << eol << eol << "test union OmgUSig(_union,Omega,Sigma):" /*<< OmgUSig */<< eol;
  print_Domain(theCout, OmgUSig);
  GeomDomain OmgUGamInterOmgUSig(_intersection, OmgUGam, OmgUSig);
  theCout << eol << eol << "test intersection Domain(_intersection,OmgUGam,OmgUSig):"  << eol;
  print_Domain(theCout, OmgUGamInterOmgUSig);
  //! test accessors
  theCout << eol << eol << "test accessors on OmgUSig: name=" << OmgUSig.name() << ", dim=" << OmgUSig.dim() << ", type=" << words("domain type", OmgUSig.domType());
  theCout << ", isUnion=" << words(OmgUSig.isUnion()) << ", isIntersection=" << words(OmgUSig.isIntersection());
  theCout << ", compositeDomain=" << OmgUSig.compositeDomain()->name() << eol;

  //!test of geometrical union of domains (internal tool)
  std::vector<const GeomDomain*> doms(2);
  doms[0]=&Gamma; doms[1]=&Sigma;
  const GeomDomain* du1=geomUnionOf(doms, &Omega);
  theCout << eol << "unionOf(Gamma,Sigma):" << eol;
  print_Domain(theCout, *du1);
  doms[0]=&Gamma; doms[1]=&Omega;
  const GeomDomain* du2=geomUnionOf(doms, &Omega);
  theCout << eol << eol << "unionOf(Gamma,Omega):"  << eol;
  print_Domain(theCout, *du2);

  doms[0]=&OmgUGam; doms[1]=&OmgUSig;
  const GeomDomain* du3=geomUnionOf(doms, &Omega);
  theCout << eol << eol << "unionOf(Omega+Gamma,Omega+Sigma):" << eol;
  print_Domain(theCout, *du3);

  //!test of merge
  doms[0]=&Gamma; doms[1]=&Sigma;
  const GeomDomain& md1=merge(doms, "Tau");
  theCout << eol << eol << "merge(Gamma,Sigma):"  << eol;
  print_Domain(theCout, md1);

  Domains domns;
  domns << Gamma << Sigma;
  const GeomDomain& md2=merge(doms, "Tau");
  theCout << eol << eol << "merge(Gamma,Sigma):" << eol;
  print_Domain(theCout, md2);


  //!test extension of a side domain
  Domain sigma_exts=Sigma.extendDomain(false);
  theCout << eol << eol << "test Sigma extension from sides:"  << eol;
  print_Domain(theCout, sigma_exts);
  Domain sigma_extv=Sigma.extendDomain(true);
  theCout << eol << eol << "test Sigma extension from vertices:" << eol;
  print_Domain(theCout, sigma_extv);
  theCout << eol;

  nbErrors += checkValue(theCout, rootname+"/mailM.in", errors, "Test mailM ~ Domain", check);

  Mesh M12(inputsPathTo(rootname+"/mailM12.msh"), _name="P1 mesh of [0,1]x[0,1]");

  Domain omega1=M12.domain("Omega1");
  Domain omega2=M12.domain("Omega2");
  Domain interface=M12.domain("x=1");
  interface.updateParentOfSideElements();
  print_Domain(theCout, interface);
  Domain int_s=interface.extendDomain(false);
  theCout << eol << eol << "test interface extension from sides:"  << eol;
  print_Domain(theCout, int_s);
  Domain int_v=interface.extendDomain(true);
  theCout << eol << eol << "test interface extension from vertices:"  << eol;
  print_Domain(theCout, int_v) ;

  Domain int_s1=interface.extendDomain(false, omega1);
  theCout << eol << eol << "test interface extension on omega1 from sides:" << eol;
  print_Domain(theCout, int_s1) ;
  Domain int_v1=interface.extendDomain(true, omega1);
  theCout << eol << eol << "test interface extension on omega1 from vertices:" << eol;
  print_Domain(theCout, int_v1) ;

  Domain int_s2=interface.extendDomain(false, omega2);
  theCout << eol << eol << "test interface extension on omega2 from sides:" << eol;
  print_Domain(theCout, int_s2) ;
  Domain int_v2=interface.extendDomain(true, omega2);
  theCout << eol << eol << "test interface extension on omega2 from vertices:" << eol;
  print_Domain(theCout, int_v2) ;

  //! test static find operation
  theCout << eol << eol;
  printTheDomains(theCout,GeomDomain::theDomains);
  std::vector<const GeomDomain*> vdom(2, &Omega); vdom[1] = &Gamma;
  theCout << eol << eol << "Domain::findCompositeDomain(_union,vdom):" << eol;
  print_Domain(theCout, *GeomDomain::findCompositeDomain(_union, vdom));
  theCout << eol;

  nbErrors += checkValue(theCout, rootname+"/mailM12.in", errors, "Test mailM12 ~ Domain", check);

  //! test sidesDomain (domain of all sides of a domain)
  // as algorithms are pointers dependant, output is not reproductible. Do not check domain outputs
  Mesh msq(SquareGeo(_origin=Point(0.,0.),_length=1.,_nnodes=2,_domain_name="sq",_side_names="bsq"), _shape=_triangle, _order=1, _generator=_structured);
  Domain sq=msq.domain("sq"), bsq=msq.domain("bsq");
  Domain ssq=sq.sidesDomain();
  thePrintStream <<eol<<"test sidesDomain(square):" << eol;
  thePrintStream<< ssq;
  Domain sssq=ssq.sidesDomain();
  thePrintStream <<eol<<"test sidesDomain(sides of square):" << eol;
  thePrintStream<<sssq ;
  Domain sbsq=bsq.sidesDomain();
  thePrintStream <<eol<<"test sidesDomain(boundary of square):" << eol;
  thePrintStream<<sbsq;

  Mesh mcu(Cube(_origin=Point(0.,0.,0.),_length=1.,_nnodes=2,_domain_name="cu",_side_names="bcu"), _shape=_hexahedron, _order=1, _generator=_structured);
  Domain cu=mcu.domain("cu"), bcu=mcu.domain("bcu");
  Domain scu=cu.sidesDomain();
  thePrintStream <<eol<<"test sidesDomain(cube):" << eol;
  thePrintStream<<scu;
  Domain sscu=scu.sidesDomain();
  thePrintStream <<eol<<"test sidesDomain(sides of cube):" << eol;
  thePrintStream<<sscu;
  Domain sbcu=bcu.sidesDomain();
  thePrintStream <<eol<<"test sidesDomain(boundary of cube):" << eol;
  thePrintStream<<sbcu;

  Mesh msp(Sphere(_center=Point(0.,0.,0.),_radius=1.,_nnodes=2,_domain_name="sph"), _shape=_triangle, _order=1, _generator=gmsh);
  Domain sph=msp.domain("sph");
  Domain ssph=sph.sidesDomain();
  thePrintStream <<eol<<"test sidesDomain(sphere):" << eol;
  thePrintStream <<ssph;

  //! test extension of a side domain living on a mesh to a domain living on an other mesh (ficticious domain)
  Mesh meshR(inputsPathTo(rootname+"/mailMeshR.msh"), _name="mesh of rectangle [-1,1]^2");
  Mesh meshS(inputsPathTo(rootname+"/mailMeshS.msh"), _name="mesh of segment (-2/3,1/3) -- (2/3,1/3)");

  Domain omega=meshR.domain("Omega"), gamma=meshS.domain("Gamma");
  Domain gammaF=gamma.ficticiousDomain(omega);
  theCout << "test ficticious domain of a side domain:" <<eol;
  print_Dom(theCout, gammaF) ;

  Mesh md(inputsPathTo(rootname+"/mailMd.msh"), _name="mesh of unit disk");

  Domain omega_N=md.domain("omega"), gamma_N=md.domain("gamma");
  gamma_N.setNormalOrientation(towardsInfinite); gamma_N.saveNormalsToFile("normal_gamma_N_towardsInfinite",_vtk);
  gamma_N.setNormalOrientation(outwardsInfinite); gamma_N.saveNormalsToFile("normal_gamma_N_outwardsInfinite",_vtk);
  gamma_N.setNormalOrientation(towardsDomain,omega_N); gamma_N.saveNormalsToFile("normal_gamma_N_towardsDomain",_vtk);
  gamma_N.setNormalOrientation(outwardsDomain,omega_N); gamma_N.saveNormalsToFile("normal_gamma_N_outwardsDomain",_vtk);
  nbErrors += checkValues("normal_gamma_N_towardsInfinite.vtk", rootname+"/normal_gamma_N_towardsInfinite.in", 0.1, errors, "normal_gamma_N_towardsInfinite", check);
  nbErrors += checkValues("normal_gamma_N_outwardsInfinite.vtk", rootname+"/normal_gamma_N_outwardsInfinite.in", 0.1, errors, "normal_gamma_N_outwardsInfinite", check);
  nbErrors += checkValues("normal_gamma_N_towardsDomain.vtk", rootname+"/normal_gamma_N_towardsDomain.in", 0.1, errors, "normal_gamma_N_towardsDomain", check);
  nbErrors += checkValues("normal_gamma_N_outwardsDomain.vtk", rootname+"/normal_gamma_N_outwardsDomain.in", 0.1, errors, "normal_gamma_N_outwardsDomain", check);

  Mesh mds(inputsPathTo(rootname+"/mailMds.msh"), _name="mesh of unit circle");

  Domain gamma_Ns=mds.domain("gamma");
  gamma_Ns.setNormalOrientation(towardsInfinite); gamma_Ns.saveNormalsToFile("normal_gamma_Ns_towardsInfinite",_vtk);
  gamma_Ns.setNormalOrientation(outwardsInfinite); gamma_Ns.saveNormalsToFile("normal_gamma_Ns_outwardsInfinite",_vtk);
  nbErrors += checkValues("normal_gamma_Ns_towardsInfinite.vtk", rootname+"/normal_gamma_Ns_towardsInfinite.in", 0.1, errors, "normal_gamma_Ns_towardsInfinite", check);
  nbErrors += checkValues("normal_gamma_Ns_outwardsInfinite.vtk", rootname+"/normal_gamma_Ns_outwardsInfinite.in", 0.1, errors, "normal_gamma_Ns_outwardsInfinite", check);

  theCout << eol << eol << "test getNormal of a side domain:" << eol;
  Real s=sqrt(2.)/2.; Point P(s,s);
  Vector<Real> n1=gamma_N.getNormal(P); theCout << "gamma_N.getNormal(P): " << n1 << eol;
  theCout << "gamma_N.getNormal(P,towardsDomain,omega_N): " << gamma_N.getNormal(P,towardsDomain,omega_N) << eol;
  theCout << "gamma_Ns.getNormal(P): " << gamma_Ns.getNormal(P) << eol;
  theCout << "gamma_Ns.getNormal(P,towardsInfinite): " << gamma_Ns.getNormal(P,towardsInfinite) << eol;

  //!test transmission of normal vector when computing function
  Vector<Real> n2(2,1.);
  theCout << eol << "test normal transmission in Function:" << eol;
  Function F(f), G(g); Real r;
  F.require(_n);
  setNx(n1);
  Real res=F(P,r);
  theCout << "setNx(n1); F(p,r): " << res << eol;
  G.require(_nx); G.require(_ny);
  setNx(n1); setNy(n2);
  res=G(P,P,r);
  theCout << "setNx(n1); setNy(n2); G(p,p,r): " << res << eol;

  nbErrors += checkValue(theCout, rootname+"/mailSRMds.in", errors, "Test mailSRMds ~ Domain", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
