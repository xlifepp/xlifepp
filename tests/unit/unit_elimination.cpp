
/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_elimination.cpp
  \author E. Lunéville
	\since 12 fev 2014
	\date  21 fev 2014

    tests of elimination of essential conditions in Terms's
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_elimination {

//!data function
Real g(const Point& P, Parameters& pa = defaultParameters)
{
  return -0.5;
}

void unit_elimination(int argc, char* argv[], bool check)
{
  String rootname = "unit_elimination";
  trace_p->push(rootname);

  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(40);

  //!create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=1";
  sidenames[2] = "y=1"; sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=6,_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=1");
  Domain sigmaM=mesh2d.domain("x=0");
  String errors;
  Number nbErrors = 0;
  Real tol=0.005;

  Function f1(g,"-1/2");

  //!create interpolation
  Interpolation* LagrangePk=findInterpolation(Lagrange,standard,1,H1);
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");

  BilinearForm auv=intg(omega,grad(u)|grad(v));
  LinearForm fv=intg(omega,v);
  ssout<<" ----------------------  single scalar Dirichlet condition --------------------------------"<<eol;
  EssentialConditions ecsu = (u|sigmaM = 0) & (u|sigmaP = f1);
  TermMatrix K(auv);
  nbErrors+=checkValues(K, rootname+"/K.in" , tol, errors, "TermMatrix without Essantial Conditions", check);
  TermMatrix A(auv,ecsu);
  nbErrors+=checkValues(A, rootname+"/A.in" , tol, errors, "TermMatrix with Essantial Conditions", check);
  TermVector b(fv);
  TermVector x=directSolve(A,b);
  ssout<<"x= "<<x<<eol;  
  nbErrors += checkValues(x, rootname+"/x.in", 0.001, errors, "x, solution of Ax=b", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  

}

}
