/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_BicgSolver.cpp
  \author ManhHa NGUYEN
  \since 13 Sep 2012
  \date 27 Sep 2012

  Low level tests of BicgSolver.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
  Test order:
  1. Real matrix and real vector
      1.1. WiththeCout a preconditioner
          a. DENSE STORAGE
          b. CS STORAGE
          c. SKYLINE STORAGE
      1.2 With a preconditioner
          a. DENSE STORAGE
          b. CS STORAGE
          c. SKYLINE STORAGE
  2. Complex matrix and complex vector
      The same as Real
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_CgSolver {

void unit_CgSolver(int argc, char* argv[], bool check)
{
  String rootname = "unit_CgSolver";
  trace_p->push(rootname);
  verboseLevel(3);

  std::stringstream ssout;                  // string stream receiving results
  String errors;
  Number nbErrors = 0;
  ssout.precision(testPrec);

  const int rowNum = 3;
  const int colNum = 3;

  const std::string rMatrixDataSym(rootname+"/matrix3x3Sym.data");
  const std::string rMatrixDataSkewSym(rootname+"/matrix3x3SkewSym.data");
  const std::string rMatrixDataNoSym(rootname+"/matrix3x3NoSym.data");
  const std::string rMatrixDataSymPosDef(rootname+"/matrix3x3SymPosDef.data");

  const std::string cMatrixDataSym(rootname+"/cmatrix3x3Sym.data");
  const std::string cMatrixDataNoSym(rootname+"/cmatrix3x3NoSym.data");
  const std::string cMatrixDataSymSelfAjoint(rootname+"/cmatrix3x3SymSelfAjoint.data");
  const std::string cMatrixDataSymSkewAjoint(rootname+"/cmatrix3x3SymSkewAjoint.data");
  const std::string cMatrixDataSymPosDef(rootname+"/cmatrix3x3SymPosDef.data");

  CgSolver cg, cgKV(_maxIt=defaultMaxIterations, _tolerance=theDefaultConvergenceThreshold), cgKV2(_maxIt=100, _tolerance=1e-6, _verbose=3);

  //------------ solvers ----------------
  theCout << cg << eol;
  theCout << cgKV << eol;
  theCout << cgKV2 << eol;
  nbErrors += checkValue(theCout , rootname+"/solvers.in" , errors, "Defining solvers", check);

  /*!
    Vectors
  */
  //! Real Vector B
  Vector<Real> rvB(rowNum, 1.);

  //! Real initial value
  Vector<Real> rvX0(rowNum, 0.);
  //! Vector real unknown
  Vector<Real> rvX(rowNum);
  Vector<Real> rvXe(rowNum);
  for(Number i=0;i<rowNum; i++) rvXe(i+1)=Real(i+1);

  //!-------------------------
  //! Vector complex B
  Vector<Complex> cvB(rowNum, 1.);

  //!  Vector initial value
  Vector<Complex> cvX0(rowNum, 0.);

  //! Vector complex unknown
  Vector<Complex> cvX(rowNum);
  Vector<Complex> cvXe(rowNum);
  for(Number i=0;i<rowNum; i++) cvXe(i+1)=Complex(1.,1.)*Real(i+1);

  //! Real Large Matrix (Dense Type)
  //! Symmetric
  LargeMatrix<Real> rMatDenseRowSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Real> rMatDenseColSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Real> rMatDenseDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Real> rMatDenseSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _dense, _symmetric);

  //!  Complex Large Matrix (Dense Type)
  //! Symmetric
  LargeMatrix<Complex> cMatDenseRowSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseColSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _dense, _symmetric);


  //! Real Large Matrix (CS Type)
  //! Symmetric
  LargeMatrix<Real> rMatCsRowSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Real> rMatCsColSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Real> rMatCsDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _cs, _symmetric);

  //!  Complex Large Matrix (Cs Type)
  //! Symmetric
  LargeMatrix<Complex> cMatCsRowSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsColSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _cs, _symmetric);

  //!  Real Large Matrix (Skyline Type)
  //! Symmetric
  LargeMatrix<Real> rMatSkylineDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _skyline, _symmetric);

  //!  Complex Large Matrix (Skyline Type)
  //! Symmetric
  LargeMatrix<Complex> cMatSkylineDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _skyline, _symmetric);

  Real eps=0.0001;
  
  /*!------------------------------------------------------------------------------
    Test with Real value
   -----------------------------------------------------------------------------
  */
  // I. Solver without a preconditioner
  //! Test with DENSE STORAGE
  //! Test with symmetric matrices
  rvB = rMatDenseRowSym *rvXe;
  rvX = cg(rMatDenseRowSym, rvB, rvX0, _real); 
  nbErrors += checkValuesN2( rvX , rvXe, eps ,  errors, "real dense row and sym matrix with real RHS");  

  rvX = cg(rMatDenseColSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real dense col and sym matrix with real RHS");  

  rvX = cg(rMatDenseDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real dense dual and sym matrix with real RHS");

  rvX = cg(rMatDenseSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real dense sym and sym matrix with real RHS");

  //!---------------------------------
  //! Test with CS STORAGE
  //! Test with symmetric matrices
  rvB = rMatCsRowSym *rvXe;
  rvX = cg(rMatCsRowSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real Cs row and sym matrix with real RHS");

  rvX = cg(rMatCsColSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real Cs col and sym matrix with real RHS");

  rvX = cg(rMatCsDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real Cs dual and sym matrix with real RHS");

  rvX = cg(rMatCsSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real Cs sym and sym matrix with real RHS");


  //!------------------------------
  //! Test with SKYLINE STORAGE
  //! Test with symmetric matrices
  rvB = rMatSkylineDualSym *rvXe;
  rvX = cg(rMatSkylineDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real Skyline dual and sym matrix with real RHS");

  rvX = cg(rMatSkylineSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "real Skyline sym and sym matrix with real RHS");

  /*! -----------------------------------------------------------------------------
      Test with Complex values (Complex x Complex = Complex)
   -----------------------------------------------------------------------------
  */
  // I. Solver without a preconditioner
  //! Test with DENSE STORAGE
  //! Test with symmetric matrices
  cvB = cMatDenseRowSym *cvXe;
  cvX = cg(cMatDenseRowSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex dense row and sym matrix with complex RHS");

  cvX = cg(cMatDenseColSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex dense col and sym matrix with complex RHS");

  cvX = cg(cMatDenseDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex dense dual and sym matrix with complex RHS");

  cvX = cg(cMatDenseSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex dense sym and sym matrix with complex RHS");

  //!---------------------------------
  //! Test with CS STORAGE
  //! Test with symmetric matrices
  cvB = cMatCsRowSym *cvXe;

  cvX = cg(cMatCsRowSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex Cs row and sym matrix with complex RHS");

  cvX = cg(cMatCsColSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex Cs col and sym matrix with complex RHS");

  cvX = cg(cMatCsDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex Cs dual and sym matrix with complex RHS");

  cvX = cg(cMatCsSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex Cs sym and sym matrix with complex RHS");

  //!---------------------------------
  //! Test with SKYLINE STORAGE
  //! Test with symmetric matrices
  cvB = cMatSkylineDualSym*cvXe;

  cvX = cg(cMatSkylineDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex Skyline dual and sym matrix with complex RHS");

  cvX = cg(cMatSkylineSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX , cvXe , eps,  errors, "Complex Skyline sym and sym matrix with complex RHS");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
      
  trace_p->pop();
  
}

}
