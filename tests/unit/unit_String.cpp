/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
	\file unit_String.cpp
	\author E. Lunéville
	\since 8 dec 2011
	\date 11 may 2012

	Low level tests of String class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_String {

void unit_String(int argc, char* argv[], bool check)
{
  String rootname = "unit_String";
  trace_p->push(rootname);
  String errors;
  Number nbErrors = 0;

  struct mystruct {Real values[20];};	// structure example
  std::stringstream out;		// string stream receiving results
  out.precision(testPrec);
  
  //! string to anything compatible with input stringstream operator >>
  String s = "1 2 3";
  theCout << "s=" << s << " stringto<int> " << stringto<int>(s) << std::endl;
  theCout << "s=" << s << " stringto<Real> " << stringto<Real>(s) << std::endl;
  theCout << "s=" << s << " stringto<Complex> " << stringto<Complex>(s) << std::endl;
  theCout << "s=" << s << " stringto<void*> " << stringto<void*>(s) << std::endl;
  theCout << "s=" << s << " stringto<String> " << stringto<String>(s) << std::endl;
  s = "(0,1)";
  theCout << "s=" << s << " stringto<Complex> " << stringto<Complex>(s) << std::endl;
  
  //! anything to string compatible with output stringstream operator <<
  int i = 1; Real r = 1.; Complex c(0., 1.);
  theCout << "int " << i << " tostring " << tostring(i) << std::endl;
  theCout << "real " << r << " tostring " << tostring(r) << std::endl;
  theCout << "complex=" << c << " tostring " << tostring(c) << std::endl;
  theCout << "string" << s << " tostring " << tostring(s) << std::endl;
  
  //! transformation utilities
  s = " Life is a long quiet river    ";
  theCout << "\"" << s << "\"" << " lowercase : \"" << lowercase(s) << "\"" << std::endl;
  theCout << "\"" << s << "\"" << " uppercase : \"" << uppercase(s) << "\"" << std::endl;
  theCout << "\"" << s << "\"" << " capitalize : \"" << capitalize(s) << "\"" << std::endl;
  theCout << "\"" << s << "\"" << " trimLeading : \"" << trimLeading(s) << "\"" << std::endl;
  theCout << "\"" << s << "\"" << " trimTrailing : \"" << trimTrailing(s) << "\"" << std::endl;
  theCout << "\"" << s << "\"" << " trim : \"" << trim(s) << "\"" << std::endl;
  theCout << "\"" << s << "\"" << " delSpace : \"" << delSpace(s) << "\"" << std::endl;
  theCout << "\"" << s << "\"" << " capitalize(trim(lowercase)): \"" << capitalize(trim(lowercase(s))) << "\"" << std::endl;
  
  //! find utilities
  std::vector<String> names(3);
  names[0] = "eric"; names[1] = "nicolas"; names[2] = "marc";
  s = "e"; i = findString(s, names);
  theCout << words("to find first item beginning with") << " \"" << s << "\" "
      << words("in vector") << " (" << names[0] << "," << names[1] << "," << names[2] << ") = " << words("index") << " " << i << std::endl;
  s = "nico"; i = findString(s, names);
  theCout << words("to find first item beginning with") << " \"" << s << "\" "
      << words("in vector") << " (" << names[0] << "," << names[1] << "," << names[2] << ") = " << words("index") << " " << i << std::endl;
  s = "a"; i = findString(s, names);
  theCout << words("to find first item beginning with") << " \"" << s << "\" "
      << words("in vector") << " (" << names[0] << "," << names[1] << "," << names[2] << ") = " << words("index") << " " << i << std::endl;
//!----------------------------------------------------
    
  nbErrors += checkValue(theCout, rootname+"/String.in" , errors, "Test String",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
 
 trace_p->pop();
 
}

}
