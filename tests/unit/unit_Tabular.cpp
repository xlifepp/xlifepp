/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Tabular.cpp
	\author E. Lunéville
	\since 2 apr 2019
	\date 2 apr 2019

	Low level tests of Tabular class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_Tabular {

Real  f1(Real x){return sin(x);}
Real f1p(Real x, Parameters& pars){return std::sin(pars(1).get_r()*x);}
Real  f2(Real x, Real y){return x+y;}
Real f2p(Real x, Real y, Parameters& pars){return x+pars(1).get_r()*y;}
Real  f3(Real x, Real y, Real z){return x+y+z;}
Complex g2(Real x,Real y) {return x+i_*y;}
Real h(Real r){return log(abs(r));}
Vector<Real> radial(const Point& P, Parameters& pars=defaultParameters){return Vector<Real>(1,norm2(P));}
Real  f1a(const Point& P, Parameters& pars=defaultParameters){return sin(P(1));}
Real  f2a(const Point& P, Parameters& pars=defaultParameters){return P(1)+P(2);}
Real  f3a(const Point& P, Parameters& pars=defaultParameters){return P(1)+P(2)+P(3);}

void unit_Tabular(int argc, char* argv[], bool check)
{
  String rootname = "unit_Tabular";
  trace_p->push(rootname);
  std::stringstream ssout;                  
  String errors;
  
  Number nbErrors = 0;
  Real tol = 0.0001;
  ssout.precision(testPrec);
  verboseLevel(20);

  typedef Tabular<Real> TabularR;

  ssout<<"---------------------------- test 1D grid ----------------------------"<<eol;
  TabularR t1(0.,0.01,100,"x");
  t1.createTable(f1);
  ssout<<t1;
  Parameters pars(1.,"k");
  t1.createTable(f1p,pars);
  ssout<<t1;
  TabularR t1d(0.,0.01,100,f1,"x");
  ssout<<t1d;
  TabularR t1p(0.,0.01,100,f1p,pars,"x");
  t1p.loadFromFile(inputsPathTo(rootname + "/t1p.tab"));
  ssout<<t1p;
  nbErrors+=checkValue(t1(0.5), f1(0.5), tol, errors, "t1(0.5)!=f1(0.5)");
  nbErrors+=checkValue(t1p(0.5), f1p(0.5,pars), tol, errors, "t1p(0.5)!=f1p(0.5,pars)");

  ssout<<"---------------------------- test 2D grid ----------------------------"<<eol;
  TabularR t2(0.,0.1,10,1.,0.1,10,"x","y");
  t2.createTable(f2);
  ssout<<t2;
  t2.createTable(f2p,pars);
  ssout<<t2;
  TabularR t2d(0.,0.1,10,1.,0.1,10,f2,"x","y");
  ssout<<t2d;
  TabularR t2p(0.,0.1,10,1.,0.1,10,f2p,pars,"x","y");
  t2p.loadFromFile(inputsPathTo(rootname + "/t2p.tab"));
  ssout<<t2p;
  ssout<<" eval  t2(0.5,1.25) = "<<t2(0.5,1.25)<<" exact = "<<f2(0.5,1.25)<<eol;
  ssout<<" eval t2p(0.5,1.25) = "<<t2p(0.5,1.25)<<" exact = "<<f2p(0.5,1.25,pars)<<eol;
  nbErrors+=checkValue(t2(0.5, 1.25), f2(0.5, 1.25), tol, errors, "t2(0.5, 1.25) != f2(0.5, 1.25)");
  nbErrors+=checkValue(t2p(0.5, 1.25), f2p(0.5, 1.25,  pars), tol, errors, "t2p(0.5, 1.25) != f2p(0.5, 1.25,  pars)");

  Tabular<Complex> t2c(0.,0.1,10,1.,0.1,10,g2,"x","y");
  ssout<<t2c;
  ssout<<" eval t2c(0.5,1.25) = "<<t2c(0.5,1.25)<<" exact = "<<g2(0.5,1.25)<<eol;
  nbErrors+=checkValue(t2c(0.5, 1.25), g2(0.5, 1.25), tol, errors, "t2c(0.5, 1.25)!= g2(0.5, 1.25)");

  ssout<<"---------------------------- test 3D grid ----------------------------"<<eol;
  TabularR t3(0.,0.1,10,"x");
  t3.addCoordinate(0.,0.2,5,"y");
  t3.addCoordinate(1.,0.5,2,"z");
  t3.createTable(f3);
  ssout<<t3;
  ssout<<"t3.bs="<<t3.bs<<eol;
  std::vector<Number> ijk(3);
  for(Number k=0;k<20;k++)
  {
      t3.globalToIndex(k, 1, ijk.begin());
      ssout<<k<<" -> "<<ijk<<eol;
  }
  TabularR t3d(0.,0.1,10,0.,0.2,5,1.,0.5,2,f3,"x","y","z");
  ssout<<t3d;
  // t3.loadFromFile(inputsPathTo(rootname+"/t3.tab"));
  ssout<<t3;
  ssout<<" eval  t3(0.5,0.5,1.25) = "<<t3(0.5,0.5,1.25)<<" exact = "<<f3(0.5,0.5,1.25)<<eol;
  nbErrors+=checkValue(t3(0.5, 0.5, 1.25), f3(0.5, 0.5, 1.25), tol, errors, "t3(0.5, 0.5, 1.25)!= f3(0.5, 0.5, 1.25)");

  ssout<<"---------------------------- test F-Tabular ----------------------------"<<eol;
  Function F1(t1,1);
  Real r;
  ssout<<"F-Tabular F1(0.5,r) = "<<F1(Point(0.5),r)<<" exact = "<<f1(0.5)<<eol;
  nbErrors+=checkValue(F1(Point(0.5),r), f1(0.5), tol, errors, "F1(Point(0.5),r) != f1(0.5)");
  Function F2(t2,2);
  ssout<<"F-Tabular F2(Point(0.5,1.25),r) ="<<F2(Point(0.5,1.25),r) <<" exact = "<<f2(0.5,1.25)<<eol;
  nbErrors+=checkValue(F2(Point(0.5, 1.25),r), f2(0.5, 1.25), tol, errors, "F2(Point(0.5, 1.25),r) != f2(0.5, 1.25)");
  Function F3(t3,3);
  ssout<<"F-Tabular F3(Point(0.5,0.5,1.25),r) = "<<F3(Point(0.5,0.5,1.25),r) <<" exact = "<<f3(0.5,0.5,1.25)<<eol;
  nbErrors+=checkValue(F3(Point(0.5, 0.5, 1.25),r), f3(0.5, 0.5, 1.25), tol, errors, "F3(Point(0.5, 0.5,1.25),r) != f3(0.5, 0.5, 1.25)");

  TabularR th(0.,0.01,100,h,"r");
  Function Rad(radial);
  Function Fh(th,2,Rad);
  ssout<<"F-Tabular Fh(Point(0.5,0.7),r) ="<<Fh(Point(0.5,0.7),r) <<" exact = "<<h(norm2(Point(0.5,0.7)))<<eol;
  nbErrors+=checkValue( Fh(Point(0.5, 0.7),r), h(norm2(Point(0.5, 0.7))), tol, errors, "Fh(Point(0.5, 0.7),r) != h(norm2(Point(0.5, 0.7)))");

  Function F1t(f1a,1);
  F1t.createTabular(0.,0.1,10);
  ssout<<"F-Tabular F1t(Point(0.5),r) = "<<F1t(Point(0.5),r)<<" exact = "<<f1(0.5)<<eol;
  nbErrors+=checkValue(F1t(Point(0.5),r), f1(0.5), tol, errors, "F1t(Point(0.5),r) != f1(0.5)");
  Function F2t(f2a,2);
  F2t.createTabular(0.,0.1,10,1.,0.1,10);
  ssout<<"F-Tabular F2t(Point(0.5,1.25),r) ="<<F2t(Point(0.5,1.25),r) <<" exact = "<<f2(0.5,1.25)<<eol;
  nbErrors+=checkValue(F2t(Point(0.5, 1.25),r), f2(0.5, 1.25), tol, errors, "F2t(Point(0.5, 1.25),r) != f2(0.5, 1.25)");
  Function F3t(f3a,3);
  F3t.createTabular(0.,0.1,10, 0.,0.2,5, 1.,0.5,2);
  ssout<<"F-Tabular F3t(Point(0.5,0.5,1.25),r) ="<<F3t(Point(0.5,0.5,1.25),r) <<" exact = "<<f3(0.5,0.5,1.25)<<eol;
  nbErrors+=checkValue( F3t(Point(0.5, 0.5, 1.25),r) , f3(0.5, 0.5, 1.25), tol, errors, "F3t(Point(0.5, 0.5, 1.25),r) != f3(0.5, 0.5, 1.25)");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
