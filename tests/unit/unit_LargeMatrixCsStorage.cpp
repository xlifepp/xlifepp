/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_LargeMatrixCsStorage.cpp
	\authors E. Lunéville, Ha NGUYEN
	\since 04 jul 2012
	\date  20 Oct 2012

	Low level tests of LargeMatrix class and CsStorage classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_LargeMatrixCsStorage {

void unit_LargeMatrixCsStorage(int argc, char* argv[], bool check)
{
  String rootname = "unit_LargeMatrixCsStorage";
  trace_p->push(rootname);
  
  std::stringstream ssout;                  
  ssout.precision(testPrec);
  std::stringstream out;                  
  out.precision(testPrec);
  verboseLevel(9);
  Number nbErrors = 0;
  String errors;
  Real tol=0.00001;

  //! EF numbering (regular Q1 of square)
  Number n = 3;
  std::vector< std::vector<Number> > elts((n - 1) * (n - 1), std::vector<Number>(4));
  for(Number j = 1; j <= n - 1; j++)
  for(Number i = 1; i <= n - 1; i++)
  {
    Number e = (j - 1) * (n - 1) + i - 1, p = (j - 1) * n + i;
    elts[e][0] = p; elts[e][1] = p + 1; elts[e][2] = p + n; elts[e][3] = p + n + 1;
  }
  Number n2 = n * n;
  std::vector<Real> x1(n * n, 0.);
  for(Number i = 0; i < n * n; i++) { x1[i] = i; }
  ssout << "x1 = " << x1;

  std::vector<Number> rs(9);for(Number r=0;r<9;r++) rs[r]=r+1;
  std::vector<Number> cs(2);cs[0]=1;cs[1]=9;

  ssout << "\n\n----------------------------------- test row cs storage -----------------------------------\n";
  RowCsStorage* csr = new RowCsStorage(n * n, n * n, elts, elts);
  csr->visual(ssout);
  LargeMatrix<Real> Ar1(csr, 1.);
  ssout << "rowcs real large matrix Ar1 : " << Ar1;
  nbErrors+=checkValues(Ar1,rootname+"/Ar1.in", tol, errors, "Ar1",check); 
  Ar1.viewStorage(ssout);
  nbErrors+=checkVisual(Ar1,  rootname+"/Ar1Vis.in", tol, errors,  "LargeMatrix<Real> Ar1", check);
  ssout << "product Ar1*x1 : " << Ar1* x1;
  ssout << "\nproduct x1*Ar1 : " << x1* Ar1;
  nbErrors+=checkValues(Ar1*x1,rootname+"/Ar1fx1.in", tol, errors, "Ar1*x1",check); 
  nbErrors+=checkValues(x1*Ar1,rootname+"/x1fAr1.in", tol, errors, "x1*Ar1",check);
  Ar1.saveToFile("Ardense.mat", _dense);
  ssout << "\nsave Ar1 to file Adense.mat in dense format\n";
  Ar1.saveToFile("Arcoo.mat", _coo);
  ssout << "save Ar1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Real> Ar1reload(n2, n2, _cs, _row, 0.);
  Ar1reload.loadFromFile("Ardense.mat", _dense);
  ssout << "matrix Ar1reload from Adense : " << Ar1reload << "\n";
  LargeMatrix<Real> Ar1reload2("Arcoo.mat", _coo, n2, n2, _cs, _row);
  ssout << "matrix Ar1reload2 from Acoo : " << Ar1reload2 << "\n";
  RowCsStorage csrb=RowCsStorage(*csr);
  ssout << "matrix storage updated with dense submatrix indices [1,2,3,4,5,6,7,8,9]x[1,9] :\n";
  csrb.visual(ssout);
  nbErrors+=checkVisual(csrb, rootname + "/csrb_1Vis.in", tol, errors,  "csrbAr1", check);
  csrb.addSubMatrixIndices(rs,cs);
  csrb.visual(ssout);
  nbErrors+=checkVisual(csrb, rootname + "/csrb_2Vis.in", tol, errors,  "csrbAr1 add", check); 
  ssout << "row 1 (col & adrs) : " <<Ar1.getRow(1) << "\n";
  ssout << "row 2 (col & adrs) : " <<Ar1.getRow(2) << "\n";
  ssout << "row 3 (col & adrs) : " <<Ar1.getRow(3) << "\n";
  ssout << "row 9 (col & adrs) : " <<Ar1.getRow(9) << "\n";
  ssout << "col 1 (row & adrs) : " <<Ar1.getCol(1) << "\n";
  ssout << "col 2 (row & adrs) : " <<Ar1.getCol(2) << "\n";
  ssout << "col 3 (row & adrs) : " <<Ar1.getCol(3) << "\n";
  ssout << "col 9 (row & adrs) : " <<Ar1.getCol(9) << "\n";
  nbErrors+=checkValues(Ar1.getRow(1), rootname+"/Ar1_getrow1.in", tol, errors, "Ar1.getRow(1)", check); 
  nbErrors+=checkValues(Ar1.getRow(2), rootname+"/Ar1_getrow2.in", tol, errors, "Ar1.getRow(2)", check); 
  nbErrors+=checkValues(Ar1.getRow(3), rootname+"/Ar1_getrow3.in", tol, errors, "Ar1.getRow(3)", check); 
  nbErrors+=checkValues(Ar1.getRow(9), rootname+"/Ar1_getrow9.in", tol, errors, "Ar1.getRow(9)", check);
  nbErrors+=checkValues(Ar1.getCol(1), rootname+"/Ar1_getcol1.in", tol, errors, "Ar1.getCol(1)", check); 
  nbErrors+=checkValues(Ar1.getCol(2), rootname+"/Ar1_getcol2.in", tol, errors, "Ar1.getCol(2)", check); 
  nbErrors+=checkValues(Ar1.getCol(3), rootname+"/Ar1_getcol3.in", tol, errors, "Ar1.getCol(3)", check); 
  nbErrors+=checkValues(Ar1.getCol(9), rootname+"/Ar1_getcol9.in", tol, errors, "Ar1.getCol(9)", check);

  RowCsStorage* csrc = new RowCsStorage(n * n, n * n, elts, elts);
  RowCsStorage* scsrc = csrc->toScalar(2,2);
  ssout << "csrc->toScalar(2,2) :\n";
  scsrc->visual(ssout);
  nbErrors+=checkVisual(*scsrc, rootname+"/scsrc_22Vis.in", tol, errors,  "scsrc Ar1 add 22", check);
  delete scsrc;
  scsrc = csrc->toScalar(3,2);
  ssout << "csrc->toScalar(3,2) :\n";
  scsrc->visual(ssout);
  nbErrors+=checkVisual(*scsrc, rootname+"/scsrc_32Vis.in", tol, errors,  "scsrc Ar1 add 3,2", check);
  delete scsrc;
  scsrc = csrc->toScalar(1,3);
  ssout << "csrc->toScalar(1,3) :\n";
  nbErrors+=checkVisual(*scsrc, rootname+"/scsrc_13Vis.in", tol, errors,  "scsrc Ar1 add 1,3", check);
  scsrc->visual(ssout);
  delete scsrc;

  LargeMatrix<Real> Ard1(csrc, 1.);
  ssout << "Ard1 = " << Ard1;
  Ard1.deleteCols(2,4);
  ssout << "Ard1 delete col 2,3,4 : " << Ard1;
  Ard1.deleteRows(5,8);
  ssout << "Ard1 delete row 5,6,7,8 : " << Ard1;
  Ard1.deleteRows(5,5);
  ssout << "Ard1 delete row 5 : " << Ard1;

  ssout << "\n----------------------- test row cs storage, complex large matrix -------------------------\n";
  LargeMatrix<Complex> Br1(csr, i_);
  ssout << "rowcs complex large matrix Br1 : " << Br1;
  nbErrors+=checkValues(Br1,rootname+"/Br1.in", tol, errors, "Br1",check); 
  Br1.viewStorage(ssout);
  nbErrors+=checkVisual(Br1,  rootname+"/Br1Vis.in", tol, errors,  "LargeMatrix<Real> Br1", check);
  ssout << "product Br1*x1 : " << Br1* x1;
  ssout << "\nproduct x1*Br1 : " << x1* Br1;
  nbErrors+=checkValues(Br1*x1,rootname+"/Br1fx1.in", tol, errors, "Br1*x1",check); 
  nbErrors+=checkValues(x1*Br1,rootname+"/x1fBr1.in", tol, errors, "x1*Br1",check); 
  Br1.saveToFile("Brdense.mat", _dense);
  ssout << "\nsave Br1 to file Adense.mat in dense format\n";
  Br1.saveToFile("Brcoo.mat", _coo);
  ssout << "save Br1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Complex> Br1reload("Brcoo.mat", _coo, n2, n2, _cs, _row);
  ssout << "rowcs complex large matrix Br1reload : " << Br1reload;

  Matrix<Real> I22(2, 2, _idMatrix);
  std::vector<Vector<Real> > x2(n * n, Vector<Real>(2, 1.));
  for(Number i = 0; i < n * n; i++) { x2[i] *= i; }

  ssout << "\n----------------- test row cs storage, large matrix of real matrices ------------------\n";
  LargeMatrix<Matrix<Real> > Cr1(csr, I22);
  ssout << "rowcs complex large matrix Cr1 : " << Cr1;
  nbErrors+=checkValues(Cr1,rootname+"/Cr1.in", tol, errors, "Cr1",check); 
  Cr1.viewStorage(out);
  nbErrors+=checkVisual(Cr1,  rootname+"/Cr1Vis.in", tol, errors,  "LargeMatrix<Real> Cr1", check);
  ssout << "product Cr1*x1 : " << Cr1* x2;
  ssout << "\nproduct x1*Cr1 : " << x2* Cr1;
  nbErrors+=checkValues(Cr1*x2,rootname+"/Cr1fx2.in", tol, errors, "Cr1*x2",check); 
  nbErrors+=checkValues(x2*Cr1,rootname+"/x2fCr1.in", tol, errors, "x2*Cr1",check);
  Cr1.saveToFile("Crdense.mat", _dense);
  ssout << "\nsave Cr1 to file Crdense.mat in dense format\n";
  Cr1.saveToFile("Crcoo.mat", _coo);
  ssout << "save Cr1 to file Crcoo.mat in coordinates format\n";
  LargeMatrix<Real> Cr1reload("Crcoo.mat", _coo, 2 * n2, 2 * n2, _cs, _row);
  ssout << "rowcs real large matrix Cr1reload in scalar form: " << Cr1reload;
  Cr1reload.viewStorage(ssout);
  Matrix<Real> M(Dimen(1),Dimen(2));M(1,1)=1.;M(1,2)=2.;
  LargeMatrix<Matrix<Real> > Arm(csr,M);
  ssout << "Arm(csr,M) : " << Arm;
  ssout << "Arm.toScalar : " << *Arm.toScalar(0.);
  nbErrors+=checkValues(Arm,rootname+"/Arm.in", tol, errors, "Arm",check); 
  nbErrors+=checkValues(*Arm.toScalar(0.),rootname+"/Arm_toscal0.in", tol, errors, "*Arm.toScalar(0.)",check); 

  ssout << "\n----------------- test row cs storage, large matrix of complex matrices ------------------\n";
  LargeMatrix<Matrix<Complex> > Dr1(csr, i_ * I22);
  ssout << "rowcs complex large matrix Dr1 : " << Dr1;
  nbErrors+=checkValues(Dr1,rootname+"/Dr1.in", tol, errors, "Dr1",check); 
  Dr1.viewStorage(ssout);
  nbErrors+=checkVisual(Dr1,  rootname+"/Dr1Vis.in", tol, errors,  "LargeMatrix<Matrix<Complex>> Dr1", check);
  ssout << "product Dr1*x1 : " << Dr1* x2;
  ssout << "\nproduct x1*Dr1 : " << x2* Dr1;
  nbErrors+=checkValues(Dr1*x2,rootname+"/Dr1fx2.in", tol, errors, "Dr1*x2",check); 
  nbErrors+=checkValues(x2*Dr1,rootname+"/x2fDr1.in", tol, errors, "x2*Dr1",check);
  Dr1.saveToFile("Drdense.mat", _dense);
  ssout << "\nsave Dr1 to file Drdense.mat in dense format\n";
  Dr1.saveToFile("Acoo.mat", _coo);
  ssout << "save Dr1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Complex> Dr1reload("Acoo.mat", _coo, 2 * n2, 2 * n2, _cs, _row);
  ssout << "rowcs complex large matrix Dr1reload in scalar form: " << Dr1reload;
  Dr1reload.viewStorage(ssout);
  Matrix<Complex> Mc(Dimen(1),Dimen(2)); Mc(1,1)=i_; Mc(1,2)=2*i_;
  LargeMatrix<Matrix<Complex> > Drm(csr,Mc);
  ssout << "Drm(csr,Mc) : " << Drm;
  ssout << "Drm.toScalar : " << *Drm.toScalar(i_);
  nbErrors+=checkValues(Drm,rootname+"/Drm.in", tol, errors, "Drm",check); 
  nbErrors+=checkValues(*Drm.toScalar(i_),rootname+"/Drmtoscli.in", tol, errors, "*Drm.toScalar(i_)",check); 


  ssout << "\n----------------------------------- test col cs storage -----------------------------------\n";
  ColCsStorage* csc = new ColCsStorage(n * n, n * n, elts, elts);
  csc->visual(ssout);
  nbErrors+=checkVisual(*csc, rootname+"/cscVis.in", tol, errors,  "ColCsStorage csc", check);
  LargeMatrix<Real> Ac1(csc, 1.);
  nbErrors+=checkValues(Ac1,rootname+"/Ac1.in", tol, errors, "Ac1",check); 
  ssout << "rowcs real large matrix Ac1 : " << Ac1;
  Ac1.viewStorage(ssout);
  ssout << "product Ac1*x1 : " << Ac1* x1;
  ssout << "\nproduct x1*Ac1 : " << x1* Ac1;
  nbErrors+=checkValues(Ac1*x1,rootname+"/Ac1fx1.in", tol, errors, "Ac1*x1",check); 
  nbErrors+=checkValues(x1*Ac1,rootname+"/x1fAc1.in", tol, errors, "x1*Ac1",check); 
  Ac1.saveToFile("Acdense.mat", _dense);
  ssout << "\nsave Ac1 to file Acdense.mat in dense format\n";
  Ac1.saveToFile("Accoo.mat", _coo);
  ssout << "save Ac1 to file Accoo.mat in coordinates format\n";
  LargeMatrix<Real> Ac1reload(n2, n2, _cs, _col, 0.);
  Ac1reload.loadFromFile("Acdense.mat", _dense);
  ssout << "matrix Ac1reload from Acdense : " << Ac1reload << "\n";
  Ac1reload.loadFromFile("Accoo.mat", _coo);
  ssout << "matrix Ac1reload from Accoo : " << Ac1reload << "\n";
  ColCsStorage cscb=ColCsStorage(*csc);
  ssout << "matrix storage updated with dense submatrix indices [1,2,3,4,5,6,7,8,9]x[1,9] :\n";
  cscb.visual(ssout);
  nbErrors+=checkVisual(cscb,  rootname+"/cscbVis.in", tol, errors,  "ColCsStorage cscb", check);
  cscb.addSubMatrixIndices(rs,cs);
  cscb.visual(ssout);
  nbErrors+=checkVisual(cscb,  rootname+"/cscb_addMIVis.in", tol, errors,  "ColCsStorage cscb.addSubMatrixIndices(rs,cs);", check);
  ssout << "row 1 (col & adrs) : " <<Ac1.getRow(1) << "\n";
  ssout << "row 2 (col & adrs) : " <<Ac1.getRow(2) << "\n";
  ssout << "row 3 (col & adrs) : " <<Ac1.getRow(3) << "\n";
  ssout << "row 9 (col & adrs) : " <<Ac1.getRow(9) << "\n";
  ssout << "col 1 (row & adrs) : " <<Ac1.getCol(1) << "\n";
  ssout << "col 2 (row & adrs) : " <<Ac1.getCol(2) << "\n";
  ssout << "col 3 (row & adrs) : " <<Ac1.getCol(3) << "\n";
  ssout << "col 9 (row & adrs) : " <<Ac1.getCol(9) << "\n";
  nbErrors+=checkValues(Ac1.getRow(1), rootname+"/Ac1_getrow1.in", tol, errors, "Ac1.getRow(1)", check); 
  nbErrors+=checkValues(Ac1.getRow(2), rootname+"/Ac1_getrow2.in", tol, errors, "Ac1.getRow(2)", check); 
  nbErrors+=checkValues(Ac1.getRow(3), rootname+"/Ac1_getrow3.in", tol, errors, "Ac1.getRow(3)", check); 
  nbErrors+=checkValues(Ac1.getRow(9), rootname+"/Ac1_getrow9.in", tol, errors, "Ac1.getRow(9)", check);
  nbErrors+=checkValues(Ac1.getCol(1), rootname+"/Ac1_getcol1.in", tol, errors, "Ac1.getCol(1)", check); 
  nbErrors+=checkValues(Ac1.getCol(2), rootname+"/Ac1_getcol2.in", tol, errors, "Ac1.getCol(2)", check); 
  nbErrors+=checkValues(Ac1.getCol(3), rootname+"/Ac1_getcol3.in", tol, errors, "Ac1.getCol(3)", check); 
  nbErrors+=checkValues(Ac1.getCol(9), rootname+"/Ac1_getcol9.in", tol, errors, "Ac1.getCol(9)", check);
  ColCsStorage* cscc = new ColCsStorage(n * n, n * n, elts, elts);
  ColCsStorage* scscc = cscc->toScalar(2,2);
  ssout << "cscc->toScalar(2,2) :\n";
  scscc->visual(out);
  nbErrors+=checkVisual(*scscc, rootname+"/pscscc_22Vis.in", tol, errors,  "scscc Ac1 add 1,3", check);
  delete scscc;
  LargeMatrix<Real> Acd1(cscc, 1.);
  ssout << "Acd1 = " << eol;
  nbErrors+=checkValues(Acd1,rootname+"/Acd1_1.in", tol, errors, "AcD1",check); 
  Acd1.deleteCols(2,4);
  ssout << "Acd1 delete col 2,3,4 : " << eol;
  nbErrors+=checkValues(Acd1,rootname+"/Acd1_2.in", tol, errors, "AcD1 delete col 2,3,4 :",check); 
  Acd1.deleteRows(5,8);
  ssout << "Acd1 delete row 5,6,7,8 : " << eol;
  nbErrors+=checkValues(Acd1,rootname+"/Acd1_3.in", tol, errors, "AcD1 delete row 5,6,7,8 :",check); 
  Acd1.deleteRows(6,6);
  ssout << "Acd1 delete row 6 > 5 ! : " << eol;
  nbErrors+=checkValues(Acd1,rootname+"/Acd1_4.in", tol, errors, "AcD1 delete row 6>5 :",check); 

  LargeMatrix<Matrix<Real> > Acm(csc,M);
  ssout << "Acm(csc,M) : " << eol;
  nbErrors+=checkValues(Acm,rootname+"/Acm.in", tol, errors, "Acm(csc,M)",check); 
  ssout << "Acm.toScalar : " << eol;
  nbErrors+=checkValues(*Acm.toScalar(0.),rootname+"/pAcmtoScal0.in", tol, errors, "*Acm.toScalar(0.)",check); 

  ssout << "\n\n----------------------------------- test sym cs storage -----------------------------------\n";
  SymCsStorage* cssym = new SymCsStorage(n * n, elts, elts);
  cssym->visual(ssout);
  nbErrors+=checkVisual(*cssym, rootname+"/cssymVis.in", tol, errors,  "SymCsStorage cssym", check);
  LargeMatrix<Real> As1(cssym, 1., _symmetric);
  ssout << "symcs real large matrix As1 : " << As1;
  nbErrors+=checkValues(As1,rootname+"/As1.in", tol, errors, "As1",check);
  As1.viewStorage(ssout);
  ssout << "product As1*x1 : " << As1* x1;
  ssout << "\nproduct x1*As1 : " << x1* As1;
  nbErrors+=checkValues(As1*x1,rootname+"/As1fx1.in", tol, errors, "As1*x1",check); 
  nbErrors+=checkValues(x1*As1,rootname+"/x1fAs1.in", tol, errors, "x1*As1",check);
  As1.saveToFile("Asdense.mat", _dense);
  ssout << "\nsave As1 to file Adense.mat in dense format\n";
  As1.saveToFile("Ascoo.mat", _coo);
  ssout << "save As1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Real> As1reload(n2, n2, _cs, _symmetric, 0.);
  As1reload.loadFromFile("Asdense.mat", _dense);
  ssout << "matrix As1reload from Adense : " << As1reload << "\n";
  As1reload.loadFromFile("Ascoo.mat", _coo);
  ssout << "matrix As1reload from Acoo : " << As1reload << "\n";
  SymCsStorage cssymb=SymCsStorage(*cssym);
  ssout << "matrix storage updated with dense submatrix indices [1,2,3,4,5,6,7,8,9]x[1,9] :\n";
  cssymb.visual(ssout);
  cssymb.addSubMatrixIndices(rs,cs);
  cssymb.visual(ssout);
  ssout << "row 1 (col & adrs) : " <<As1.getRow(1) << "\n";
  ssout << "row 2 (col & adrs) : " <<As1.getRow(2) << "\n";
  ssout << "row 3 (col & adrs) : " <<As1.getRow(3) << "\n";
  ssout << "row 9 (col & adrs) : " <<As1.getRow(9) << "\n";
  ssout << "col 1 (row & adrs) : " <<As1.getCol(1) << "\n";
  ssout << "col 2 (row & adrs) : " <<As1.getCol(2) << "\n";
  ssout << "col 3 (row & adrs) : " <<As1.getCol(3) << "\n";
  ssout << "col 9 (row & adrs) : " <<As1.getCol(9) << "\n";
  nbErrors+=checkValues(As1.getRow(1), rootname+"/As1_getrow1.in", tol, errors, "As1.getRow(1)", check); 
  nbErrors+=checkValues(As1.getRow(2), rootname+"/As1_getrow2.in", tol, errors, "As1.getRow(2)", check); 
  nbErrors+=checkValues(As1.getRow(3), rootname+"/As1_getrow3.in", tol, errors, "As1.getRow(3)", check); 
  nbErrors+=checkValues(As1.getRow(9), rootname+"/As1_getrow9.in", tol, errors, "As1.getRow(9)", check);
  nbErrors+=checkValues(As1.getCol(1), rootname+"/As1_getcol1.in", tol, errors, "As1.getCol(1)", check); 
  nbErrors+=checkValues(As1.getCol(2), rootname+"/As1_getcol2.in", tol, errors, "As1.getCol(2)", check); 
  nbErrors+=checkValues(As1.getCol(3), rootname+"/As1_getcol3.in", tol, errors, "As1.getCol(3)", check); 
  nbErrors+=checkValues(As1.getCol(9), rootname+"/As1_getcol9.in", tol, errors, "As1.getCol(9)", check);
  SymCsStorage* cssymc = new SymCsStorage(n * n, elts, elts);
  SymCsStorage* scssymc = cssymc->toScalar(2,2);
  ssout << "cssymc->toScalar(2,2) :\n";
  scssymc->visual(ssout);
  delete scssymc;
  scssymc = cssymc->toScalar(3,3);
  ssout << "cssymc->toScalar(3,3) :\n";
  scssymc->visual(ssout);
  nbErrors+=checkVisual(*scssymc, rootname+"/pscssymc_33Vis.in", tol, errors,  "*scssymc As1 add 33", check);
  delete scssymc;
  scssymc = cssymc->toScalar(1,1);
  ssout << "cssymc->toScalar(1,1) :\n";
  scssymc->visual(ssout);
  nbErrors+=checkVisual(*scssymc, rootname+"/pscssymc_11Vis.in", tol, errors,  "*scssymc As1 add 11", check);
  delete scssymc;

  Matrix<Real> Ms(Dimen(2),Dimen(2));Ms(1,1)=11;Ms(1,2)=12;Ms(2,1)=21;Ms(2,2)=22;
  LargeMatrix<Matrix<Real> > Asm(cssym,Ms);
  ssout << "Asm(cssym,M) : " << eol;
  nbErrors+=checkValues(Asm,rootname+"/Asm.in", tol, errors, "Asm(cssym,M)",check); 
  ssout << "Asm.toScalar : " << eol;
  nbErrors+=checkValues(*Asm.toScalar(0.),rootname+"/pAsmtoScal0.in", tol, errors, "*Asm.toScalar(0.)",check);

  ssout << "\n\n----------------------------------- test dual cs storage -----------------------------------\n";
  DualCsStorage* csdual = new DualCsStorage(n * n, n * n, elts, elts);
  csdual->visual(ssout);
  nbErrors+=checkVisual(*csdual, rootname+"/csdualVis.in", tol, errors,  "DualCsStorage dualsc", check);
  LargeMatrix<Real> Ad1(csdual, 1.);
  ssout << "dualcs real large matrix Ad1 : " << Ad1;
  nbErrors+=checkValues(Ad1,rootname+"/Ad1.in", tol, errors, "Ad1",check); 
  Ad1.viewStorage(ssout);
  ssout << "product Ad1*x1 : " << Ad1* x1;
  nbErrors+=checkValues(Ad1*x1,rootname+"/Ad1fx1.in", tol, errors, "Ad1*x1",check); 
  ssout << "\nproduct x1*Ad1 : " << x1* Ad1;
  nbErrors+=checkValues(x1*Ad1,rootname+"/x1fAd1.in", tol, errors, "x1*Ad1",check); 
  Ad1.saveToFile("Addense.mat", _dense);
  ssout << "\nsave Ad1 to file Adense.mat in dense format\n";
  Ad1.saveToFile("Adcoo.mat", _coo);
  ssout << "save Ad1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Real> Ad1reload(n2, n2, _cs, _dual, 0.);
  Ad1reload.loadFromFile("Addense.mat", _dense);
  ssout << "matrix Ad1reload from Adense : " << Ad1reload << "\n";
  Ad1reload.loadFromFile("Adcoo.mat", _coo);
  ssout << "matrix Ad1reload from Acoo : " << Ad1reload << "\n";
  DualCsStorage csdualb=DualCsStorage(*csdual);
  ssout << "matrix storage updated with dense submatrix indices [1,2,3,4,5,6,7,8,9]x[1,9] :\n";
  csdualb.visual(ssout);
  nbErrors+=checkVisual(csdualb,  rootname+"/csdualbVis.in", tol, errors,  "DualCsStorage csdualb", check);
  csdualb.addSubMatrixIndices(rs,cs);
  csdualb.visual(ssout);
  nbErrors+=checkVisual(csdualb,  rootname+"/csdualb_addMIVis.in", tol, errors,  "DualCsStorage csdualb.addSubMatrixIndices(rs,cs);", check);
  ssout << "row 1 (col & adrs) : " <<Ad1.getRow(1) << "\n";
  ssout << "row 2 (col & adrs) : " <<Ad1.getRow(2) << "\n";
  ssout << "row 3 (col & adrs) : " <<Ad1.getRow(3) << "\n";
  ssout << "row 9 (col & adrs) : " <<Ad1.getRow(9) << "\n";
  ssout << "col 1 (row & adrs) : " <<Ad1.getCol(1) << "\n";
  ssout << "col 2 (row & adrs) : " <<Ad1.getCol(2) << "\n";
  ssout << "col 3 (row & adrs) : " <<Ad1.getCol(3) << "\n";
  ssout << "col 9 (row & adrs) : " <<Ad1.getCol(9) << "\n";
  nbErrors+=checkValues(Ad1.getRow(1), rootname+"/Ad1_getrow1.in", tol, errors, "Ad1.getRow(1)", check); 
  nbErrors+=checkValues(Ad1.getRow(2), rootname+"/Ad1_getrow2.in", tol, errors, "Ad1.getRow(2)", check); 
  nbErrors+=checkValues(Ad1.getRow(3), rootname+"/Ad1_getrow3.in", tol, errors, "Ad1.getRow(3)", check); 
  nbErrors+=checkValues(Ad1.getRow(9), rootname+"/Ad1_getrow9.in", tol, errors, "Ad1.getRow(9)", check);
  nbErrors+=checkValues(Ad1.getCol(1), rootname+"/Ad1_getcol1.in", tol, errors, "Ad1.getCol(1)", check); 
  nbErrors+=checkValues(Ad1.getCol(2), rootname+"/Ad1_getcol2.in", tol, errors, "Ad1.getCol(2)", check); 
  nbErrors+=checkValues(Ad1.getCol(3), rootname+"/Ad1_getcol3.in", tol, errors, "Ad1.getCol(3)", check); 
  nbErrors+=checkValues(Ad1.getCol(9), rootname+"/Ad1_getcol9.in", tol, errors, "Ad1.getCol(9)", check);
  DualCsStorage* csDualc = new DualCsStorage(n * n, n * n,elts, elts);
  DualCsStorage* scsDualc = csDualc->toScalar(2,2);
  ssout << "csDualc->toScalar(2,2) :\n";
  scsDualc->visual(ssout);
  nbErrors+=checkVisual(*scsDualc, rootname+"/pscsDualc_22Vis.in", tol, errors,  "*scsDualc Ad1 add 22", check);
  delete scsDualc;
  scsDualc = csDualc->toScalar(3,2);
  nbErrors+=checkVisual(*scsDualc, rootname+"/scsDualc_32Vis.in", tol, errors,  "scsDualc Ad1 add 3,2", check);
  ssout << "csDualc->toScalar(3,2) :\n";
  scsDualc->visual(ssout);
  delete scsDualc;
  scsDualc = csDualc->toScalar(1,3);
  ssout << "csDualc->toScalar(1,3) :\n";
  scsDualc->visual(ssout);
  nbErrors+=checkVisual(*scsDualc, rootname+"/scsDualc_13Vis.in", tol, errors,  "scsDualc Ad1 add 1,3", check);
  delete scsDualc;
  LargeMatrix<Matrix<Real> > Adm(csdual,M);
  ssout << "Adm(csdual,M) : " << Adm;
  nbErrors+=checkValues(Adm,rootname+"/Adm.in", tol, errors, "Adm(csc,M)",check);
  ssout << "Adm.toScalar : " << *Adm.toScalar(0.);
  nbErrors+=checkValues(*Adm.toScalar(0.),rootname+"/pAdmtoScal0.in", tol, errors, "*Adm.toScalar(0.)",check); 

  ssout << "\n----------------------------------- test scaleMatrix -----------------------------------\n";
  Real sigmaR = 3.0;
  Complex sigmaC = Complex(3.0, 3.0);
  ssout << "product Ar1*sigmaR : " << Ar1* sigmaR << std::endl;
  ssout << "product sigmaR*Ar1 : " << sigmaR* Ar1 << std::endl;
  ssout << "product Ar1*sigmaC : " << Ar1* sigmaC << std::endl;
  ssout << "product sigmaC*Ar1 : " << sigmaC* Ar1 << std::endl;
  ssout << "product Ac1*sigmaR : " << Ac1* sigmaR << std::endl;
  ssout << "product sigmaR*Ac1 : " << sigmaR* Ac1 << std::endl;
  ssout << "product Ac1*sigmaC : " << Ac1* sigmaC << std::endl;
  ssout << "product sigmaC*Ac1 : " << sigmaC* Ac1 << std::endl;
  ssout << "product Ad1*sigmaR : " << Ad1* sigmaR << std::endl;
  ssout << "product sigmaR*Ad1 : " << sigmaR* Ad1 << std::endl;
  ssout << "product Ad1*sigmaC : " << Ad1* sigmaC << std::endl;
  ssout << "product sigmaC*Ad1 : " << sigmaC* Ad1 << std::endl;
  ssout << "product As1*sigmaR : " << As1* sigmaR << std::endl;
  ssout << "product sigmaR*As1 : " << sigmaR* As1 << std::endl;
  ssout << "product As1*sigmaC : " << As1* sigmaC << std::endl;
  ssout << "product sigmaC*As1 : " << sigmaC* As1 << std::endl;
  nbErrors+=checkValues(Ar1* sigmaR,rootname+"/Ar1sigmaR.in", tol, errors, "Ar1* sigmaR",check);
  nbErrors+=checkValues( sigmaR*Ar1,rootname+"/sigmaRAr1.in", tol, errors, "sigmaR*Ar1",check);
  nbErrors+=checkValues(Ar1* sigmaC,rootname+"/Ar1sigmaC.in", tol, errors, "Ar1* sigmaC",check);
  nbErrors+=checkValues( sigmaC*Ar1,rootname+"/sigmaCAr1.in", tol, errors, "sigmaC*Ar1",check);
  nbErrors+=checkValues(Ac1* sigmaR,rootname+"/Ac1sigmaR.in", tol, errors, "Ac1* sigmaR",check);
  nbErrors+=checkValues( sigmaR*Ac1,rootname+"/sigmaRAc1.in", tol, errors, "sigmaR*Ac1",check);
  nbErrors+=checkValues(Ac1* sigmaC,rootname+"/Ac1sigmaC.in", tol, errors, "Ac1* sigmaC",check);
  nbErrors+=checkValues( sigmaC*Ac1,rootname+"/sigmaCAc1.in", tol, errors, "sigmaC*Ac1",check); 
  nbErrors+=checkValues(Ad1* sigmaR,rootname+"/Ad1sigmaR.in", tol, errors, "Ad1* sigmaR",check);
  nbErrors+=checkValues( sigmaR*Ad1,rootname+"/sigmaRAd1.in", tol, errors, "sigmaR*Ad1",check);
  nbErrors+=checkValues(Ad1* sigmaC,rootname+"/Ad1sigmaC.in", tol, errors, "Ad1* sigmaC",check);
  nbErrors+=checkValues( sigmaC*Ad1,rootname+"/sigmaCAd1.in", tol, errors, "sigmaC*Ad1",check);  
  nbErrors+=checkValues(As1* sigmaR,rootname+"/As1sigmaR.in", tol, errors, "As1* sigmaR",check);
  nbErrors+=checkValues( sigmaR*As1,rootname+"/sigmaRAs1.in", tol, errors, "sigmaR*As1",check);
  nbErrors+=checkValues(As1* sigmaC,rootname+"/As1sigmaC.in", tol, errors, "As1* sigmaC",check);
  nbErrors+=checkValues( sigmaC*As1,rootname+"/sigmaCAs1.in", tol, errors, "sigmaC*As1",check); 

  ssout << "\n----------------------------------- test addMatrixMatrix -----------------------------------\n";
  LargeMatrix<Real> rCsRow(Ar1);
  LargeMatrix<Real> rCsCol(Ac1);
  LargeMatrix<Real> rCsDual(Ad1);
  LargeMatrix<Real> rCsSym(As1);
  ssout << "Sum of two row cs matrices : " << rCsRow + Ar1 << std::endl;
  ssout << "Sum of two col cs matrices : " << rCsCol + Ac1 << std::endl;
  ssout << "Sum of two dual cs matrices : " << rCsDual + Ad1 << std::endl;
  ssout << "Sum of two sym cs matrices : " << rCsSym + As1 << std::endl;

  ssout << "\n----------------------------------- test SOR solver -----------------------------------\n";
  const std::string rMatrixDataSym(rootname+"/matrix3x3Sym.data");
  const std::string rMatrixDataNoSym(rootname+"/matrix3x3NoSym.data");
  const Real w = 1.5;
  Vector<Real> rVecB(3, 1.);
  Vector<Real> rVecX(3, 1.);
  const Number rowNum = 3;
  const Number colNum = 3;
  LargeMatrix<Real> rMatCsDualNoSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _cs, _symmetric);
  LargeMatrix<Real> rMatCsSymNoSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, _cs, _noSymmetry);

  rMatCsSymSym.sorLowerSolve(rVecB, rVecX, w);
  ssout << "The result SOR lower solver of sym cs matrix is " << std::endl;
  nbErrors+=checkValues(rVecX,rootname+"/rVecXsorLower.in", tol, errors, "solution sor lower",check); 
  rMatCsSymSym.sorDiagonalSolve(rVecB, rVecX, w);
  ssout << "The result SOR diagonal solver of sym cs matrix is " << std::endl;
  nbErrors+=checkValues(rVecX,rootname+"/rVecXsorDiag.in", tol, errors, "solution sor Diagonal",check); 
  rMatCsSymSym.sorUpperSolve(rVecB, rVecX, w);
  ssout << "The result SOR upper solver of sym cs matrix is " << std::endl;
  nbErrors+=checkValues(rVecX,rootname+"/rVecXsorUpper.in", tol, errors, "solution sor Upper",check); 
  rMatCsSymSym.sorLowerMatrixVector(rVecB, rVecX, w);
  ssout << "The result SOR lower matrix vector of sym cs matrix is " << std::endl;
  nbErrors+=checkValues(rVecX,rootname+"/rVecXsorLowerMV.in", tol, errors, "solution sor Lower MV",check); 
  rMatCsSymSym.sorDiagonalMatrixVector(rVecB, rVecX, w);
  ssout << "The result SOR diagonal matrix vector of sym cs matrix is " << rVecX << std::endl;
  nbErrors+=checkValues(rVecX,rootname+"/rVecXsorDiagMV.in", tol, errors, "solution sor Diag MV",check);
  rMatCsSymSym.sorUpperMatrixVector(rVecB, rVecX, w);
  ssout << "The result SOR upper matrix vector of sym cs matrix is " << rVecX << std::endl;
  nbErrors+=checkValues(rVecX,rootname+"/rVecXsorUpperMV.in", tol, errors, "solution sor Upper MV",check);

  rMatCsSymSym.sorLowerSolve(rVecB, rVecX, w);
  rMatCsSymSym.sorDiagonalMatrixVector(rVecX, rVecX, w);
  rMatCsSymSym.sorUpperSolve(rVecX, rVecX, w);
  ssout << "The result SOR solver of sym cs matrix is " << rVecX << std::endl;
  nbErrors+=checkValues(rVecX,rootname+"/rVecXsor.in", tol, errors, "solution sor",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
