/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Point.cpp
	\author E. Lunéville
	\since 8 dec 2011
	\date 11 may 2012

	Low level tests of Point class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

//===============================================================================
// library dependencies
#include "xlife++-libs.h"
#include "testUtils.hpp"
//===============================================================================
// stl dependencies
#include <iostream>
#include <fstream>
//===============================================================================

using namespace xlifepp;

namespace unit_Point {

void unit_Point(int argc, char* argv[], bool check)
{
  String rootname = "unit_Point";
  trace_p->push(rootname);
  verboseLevel(10);
  String errors;
  Number nbErrors = 0;

  //! Point constructors
  theCout << words("constructors") << std::endl;
  Point p1(0., 0., 0.), p2(1., 1., 1.);
  theCout << "p1=" << p1 << " p2=" << p2 << std::endl;
  Point p3(1.), p4(1., 2.), p5(1., 2., 3.);
  theCout << "p3=" << p3 << " p4=" << p4 << " p5=" << p5 << std::endl;
  Real v1[] = {1, 2, 3};
  std::vector<Real> v2(3, std::sqrt(2));
  Point p6(3, v1), p7(v2);
  theCout << "p6=" << p6 << " p7=" << p7 << std::endl;
  p7 = p6;
  Point p8(p5);
  theCout << "p7=" << p7 << " p8=" << p8 << std::endl;
  Point p9{1.,2.,3.};
  Point p10={1.,2.};
  theCout << "p9=" << p9<<" p10="<<p10<<std::endl;

  //! accessors
  theCout << words("accessors") << std::endl;
  theCout << "p8 coordinates: " << p8(1) << " " << p8(2) << " " << p8(3) << std::endl;
  p8(1) = 0;
  theCout << "new p8 coordinates: " << p8(1) << " " << p8(2) << " " << p8(3) << std::endl;
  theCout << "p8 coordinates: " << p8.x() << " " << p8.y() << " " << p8.z() << std::endl;
  p8.z() = 0;
  theCout << "new p8 coordinates: " << p8.x() << " " << p8.y() << " " << p8.z() << std::endl;

  //! operations
  theCout << words("operations") << std::endl;
  theCout << "|p8-p7|^2 = " << p8.squareDistance(p7) << std::endl << "|p8-p7| = " << p8.distance(p7) << std::endl;
  theCout << "|p8-p7|^2 = " << squareDistance(p7, p8) << std::endl << "|p8-p7| = " << pointDistance(p7, p8) << std::endl;
  p5 = +p6; p6 = -p5; p7 = p5 += 1; p8 = p5 -= 1;
  theCout << "p5=+p6=" << p5 << " p6=-p5=" << p6 << " p7=p5+=1=" << p7 << " p8=p5-=1=" << p8 << std::endl;
  p5 += p6; p7 -= p8; p6 *= 2; p8 /= 3;
  theCout << "p5=p5+p6=" << p5 << " p6=2*p6=" << p6 << " p7=p7-p8=" << p7 << " p8=p8/3=" << p8 << std::endl;
  p1 = p5 + p6; p2 = p5 - p6; p3 = 3 * p6; p4 = p6 * 3; p5 = p6 / 3;
  theCout << "p1=p5+p6=" << p1 << " p2=p5-p6=" << p2 << " p3=3*p6=" << p3 << " p4=p6*3=" << p4 << " p5=p6/3=" << p5 << std::endl;
  p1 = (3 * (p2 + 3) - 8 * (1 - p3 + p5 * 2)) / 2;
  theCout << "p1=(3*(p2+3)-8*(1-p3+p5*2))/2=" << p1 << std::endl;

  //! comparison (tol=0)
  theCout << words("comparisons with tolerance") << "=0" << std::endl;
  Point p(1, 2, 3), q(1, 1, 3);
  theCout << "p=" << p << " q=" << q << " p==q: " << (p == q) << " p!=q: " << (p != q) << " p<q: " << (p < q);
  theCout << " p>q: " << (p > q) << " p<=q: " << (p <= q) << " p>=q: " << (p >= q) << std::endl;
  p = q;
  theCout << "p=" << p << " q=" << q << " p==q: " << (p == q) << " p!=q: " << (p != q) << " p<q: " << (p < q);
  theCout << " p>q: " << (p > q) << " p<=q: " << (p <= q) << " p>=q: " << (p >= q) << std::endl;

  //! comparison (tol=0)
  Real eps = .00001; Point::tolerance = eps;
  theCout << words("comparisons with tolerance") << "=" << eps << std::endl;
  q = p + eps / 2;
  theCout << "p=" << p << " q=p+eps/2=" << q << " p==q: " << (p == q) << " p!=q: " << (p != q) << " p<q: " << (p < q);
  theCout << " p>q: " << (p > q) << " p<=q: " << (p <= q) << " p>=q: " << (p >= q) << std::endl;
  q = p - eps / 2;
  theCout << "p=" << p << " q=p-eps/2=" << q << " p==q: " << (p == q) << " p!=q: " << (p != q) << " p<q: " << (p < q);
  theCout << " p>q: " << (p > q) << " p<=q: " << (p <= q) << " p>=q: " << (p >= q) << std::endl;
  q = p + 2 * eps;
  theCout << "p=" << p << " q=p+2*eps=" << q << " p==q: " << (p == q) << " p!=q: " << (p != q) << " p<q: " << (p < q);
  theCout << " p>q: " << (p > q) << " p<=q: " << (p <= q) << " p>=q: " << (p >= q) << std::endl;
  q = p - 2 * eps;
  theCout << "p=" << p << " q=p-2*eps=" << q << " p==q: " << (p == q) << " p!=q: " << (p != q) << " p<q: " << (p < q);
  theCout << " p>q: " << (p > q) << " p<=q: " << (p <= q) << " p>=q: " << (p >= q);

  //!check to2D (2D coordinates of 3D coplanar points)
  std::vector<Point> vs(4);
  vs[0]=Point(1,0,0);vs[1]=Point(0,1,0);vs[2]=Point(0,0.5,0.5);vs[3]=Point(0,0,1);
  theCout<<eol<<"2D coordinates: "<<to2D(vs)<<eol;
  theCout<<"splitInTriangles: "<<splitInTriangles(vs)<<eol;

  //!check earcut algorithm
  std::vector<std::vector<Point> > pol;
  std::vector<Point> ext(9), hole(4);
  ext[0]=Point(0., 0.); ext[1]=Point(12., 0.); ext[2]=Point(12., 8.); ext[3]=Point(8., 8.); ext[4]=Point(8., 4.);
  ext[5]=Point(6., 6.); ext[6]=Point(4., 4.); ext[7]=Point(4., 8.); ext[8]=Point(0., 8.);
  hole[0]=Point(2.,1.); hole[1]=Point(10.,1.);hole[2]=Point(10.,2.);hole[3]=Point(2.,2.);
  pol.push_back(ext);
  std::vector<Number> indices = earcut(pol);
  theCout<<"polygon earcut: "<<indices<<eol;
  theCout<<"splitInTriangles: "<<splitInTriangles(ext)<<eol;
  pol.push_back(hole);
  indices = earcut(pol);
  theCout<<"holed polygon earcut: "<<indices<<eol;

  nbErrors+=checkValue(theCout, rootname+"/Point.in", errors, "Test  Point", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();


}

}
