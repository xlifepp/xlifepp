/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_IncompleteFacto_TermMatrixCsStorage.cpp
	\authors C.Chambeyron
	\since 24 Mars 2017

	Low level tests of LargeMatrix  CsStorage ILU factorization.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_IncompleteFacto_TermMatrixCsStorage
{
   template<typename M>
void giveValue( const LargeMatrix<M>& M0, LargeMatrix<M>& M1);
    template<typename M>
void getPart( String Part, LargeMatrix<M>& MIF, LargeMatrix<M>& MM);
    template<typename M>
int validIncompleteFactoLM(FactorizationType Part, LargeMatrix<M>& M0, LargeMatrix<M>& MIF, Real eps);
int validIncompleteFacto(FactorizationType Part, TermMatrix& M0, TermMatrix& MIF, Real eps);
Real un(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

void unit_IncompleteFacto_TermMatrixCsStorage(int argc, char* argv[], bool check)
{
  String rootname = "unit_IncompleteFacto_TermMatrixCsStorage";
  trace_p->push(rootname);
  verboseLevel(3);
  String errors;
  Number nbErrors = 0;

  //! create a mesh and Domains
  Number nbNodes = 4;
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  verboseLevel(10);
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=nbNodes,_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");

  //! create Lagrange P1 space and unknown
  Space V1(_domain=omega,_interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u(V1, _name="u");
  TestFunction v(u, _name="v");
  cpuTime("time for initialisation, mesh and space definition");

  Real k=1;
  Real eps=0.0001;
  BilinearForm h=intg(omega,grad(u)|grad(v));

  theCout << "test de validation:"<<std::endl;
  theCout << " Le produit des matrices de la factorisation d'une matrice stockee Cs a les memes coef non nuls que la matrice de depart a "<<eps<<" pres?" << std::endl;

  theCout <<std::endl<<"* test I.L.U DualCS Storage:"<<std::endl;
  TermMatrix H0(h, _storage=csDual, _name="H0");
  TermMatrix ILUDual = H0;
  iFactorize(H0,ILUDual,_ilu);
  nbErrors += checkIncompleteFacto( _ilu, H0, ILUDual, eps, errors, "-> Test incomplete facto  iLU Dual \n");

  theCout <<std::endl<<"* test I.L.U DualCS Storage, symmetric matrix:"<<std::endl;
  TermMatrix H0S(h, _name="H0");
  TermMatrix ILUDualS = H0S;
  iFactorize(H0S,ILUDualS,_ilu);
  nbErrors += checkIncompleteFacto( _ilu, H0S, ILUDualS, eps, errors, "-> Test incomplete facto  iLU Dual Sym reelle\n");

  theCout <<std::endl<<"* test I.L.U RowCS Storage:"<<std::endl;
  TermMatrix HRCS(h, _storage=csRow, _name="HR");
  TermMatrix ILURow = HRCS;
  iFactorize(HRCS,ILURow,_ilu);
  nbErrors += checkIncompleteFacto( _ilu, HRCS, ILURow, eps, errors, "-> Test incomplete facto  iLU Row reelle\n");

  theCout <<std::endl<<"* test I.L.U ColCS Storage:"<<std::endl;
  TermMatrix HCCS(h, _storage=csCol, _name="HC");
  TermMatrix ILUCol = HCCS;
  iFactorize(HCCS,ILUCol,_ilu);
  nbErrors += checkIncompleteFacto( _ilu, HCCS, ILUCol, eps, errors, "-> Test incomplete facto  iLU Col reelle\n");

  theCout <<std::endl<<"* test I.L.D.Lt SymCS Storage:"<<std::endl;
  TermMatrix H(h, _storage=csSym, _name="H");
  TermMatrix ILDLTHDual = H;
  iFactorize(H,ILDLTHDual,_ildlt);
  nbErrors += checkIncompleteFacto( _ildlt, H, ILDLTHDual, eps, errors, "-> Test incomplete facto  iLDLt Dual \n");

  theCout <<std::endl<<"* test I.L.Lt SymCS Storage:"<<std::endl;
  TermMatrix HSym(h, _storage=csSym, _name="HSym");
  TermMatrix ILLTSym = HSym;
  iFactorize(HSym,ILLTSym,_illt);
  nbErrors += checkIncompleteFacto( _illt, HSym, ILLTSym, eps, errors, "-> Test incomplete facto  iLLt Sym reelle \n");

  //! cas COMPLEXE
  //!*******************************************************************************/
  Complex I(0,1);
  BilinearForm hsym=intg(omega,(grad(u)|grad(v))) +I*intg(omega,(grad(u)|grad(v)));

  TermMatrix H00(hsym, _storage=csDual, _name="H0");

  theCout <<std::endl<<"* test I.L.D.Lt SymCS Storage:"<<std::endl;
  TermMatrix Hsym(hsym, _storage=csSym, _name="Hsym");
  TermMatrix ILDLtHsym = Hsym;
  iFactorize(Hsym,ILDLtHsym,_ildlt);
  nbErrors += checkIncompleteFacto( _ildlt, Hsym, ILDLtHsym, eps, errors, "-> Test incomplete facto  iLDLt Sym complexe \n");

  Matrix<Complex> A(2,2);
  A(1,1) =1.; A(1,2)=-I;
  A(2,1)=I;    A(2,2)=1.;
  BilinearForm hadj= intg(omega,( ( A*grad(u)|grad(v)) ),  _selfAdjoint);

  theCout <<std::endl<<"* test I.L.D.L* SymCS Storage:"<<std::endl;
  TermMatrix Hadj(hadj , _storage=csSym, _name="Hadj");
  TermMatrix ILDLstarHadj = Hadj;
  iFactorize(Hadj,ILDLstarHadj,_ildlstar);
  nbErrors += checkIncompleteFacto( _ildlstar, Hadj, ILDLstarHadj, eps, errors, "-> Test incomplete facto  iLDL* Sym complexe LM\n");

  //!  Test factorization LLt and LL* with LargeMatrix 
  //!-------------------------------------------------------------------------
  theCout  << std::endl << "*** Test incomplete factorization LLt and LL* with LargeMatrix " << std::endl;
  Number NNN = 2;
  std::vector< std::vector<Number> > elts((NNN- 1) * (NNN - 1), std::vector<Number>(4));
  for(Number j = 1; j <= NNN - 1; j++)
  for(Number i = 1; i <= NNN - 1; i++)
  {
    Number e = (j - 1) * (NNN - 1) + i - 1, p = (j - 1) * NNN + i;
    elts[e][0] = p; elts[e][1] = p + 1; elts[e][2] = p + NNN; elts[e][3] = p + NNN + 1;
  }

  SymCsStorage* cssym = new SymCsStorage(NNN * NNN, elts, elts);
  LargeMatrix<Complex> MCS0(cssym, Complex(0.,0.), _symmetric);
  LargeMatrix<Complex> MatriceC(cssym, 0, _noSymmetry);

  theCout<< "*   LargeMatrix LLT complexe "<<std::endl;
  MatriceC(1,1)=2;
  MatriceC(2,1)=I;      MatriceC(2,2)=1;
  MatriceC(3,1)=-1-I;   MatriceC(3,2)=I;    MatriceC(3,3)=1+I;
  MatriceC(4,1)=-2;    MatriceC(4,2)=-I;   MatriceC(4,3)=1+I;   MatriceC(4,4)=4;

  giveValue(MatriceC,MCS0);
  LargeMatrix<Complex> MCST = MCS0;
  illtFactorize(MCST);
  nbErrors += checkIncompleteFacto( _illt, MatriceC, MCST, eps, errors, "-> Test incomplete facto  iLLt Sym complexe \n");

  nbErrors += checkIncompleteFacto( _illt, MatriceC, MCST, eps, errors, "-> Test incomplete facto  iLLt Sym complexe \n");

  LargeMatrix<Complex> MatriceCC(cssym, 0, _noSymmetry);
  LargeMatrix<Complex> MCS0C(cssym, Complex(0.,0.), _selfAdjoint);
  MatriceCC = MatriceC; MatriceCC(2,2)=10;MatriceCC(3,3)=10;MatriceCC(4,4)=10;
  giveValue(MatriceCC,MCS0C);
  LargeMatrix<Complex> MCSS = MCS0C;
  illstarFactorize(MCSS);
  nbErrors += checkIncompleteFacto( _illstar, MCS0C, MCSS, eps, errors, "-> Test incomplete facto  iLL* Sym complexe LM \n");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}


/*********************************************************************************************************/
/*
template<typename M>
void giveValue( const LargeMatrix<M>& M0, LargeMatrix<M>& M1)
{       // std::cout << "taille:" << M1.nbRows <<std::endl;
   // M1(i,j)=M0(i,j)
   for(Number j = 1; j <= M0.nbRows ; j++)
      for(Number i = j; i<= M0.nbRows; i++)
           if ( M0.pos(i,j) != 0 ) {M1(i,j)=M0(i,j);}  //M1(j,i)=M1(i,j);
}
*/
/*************************************************************************************************/
/*
template<typename M>
void getPart( FactorizationType Part, LargeMatrix<M>& MIF, LargeMatrix<M>& MM)
{
//Get Part U, Lt, L*, D.Lt, D.L* de MIF ->  MM
       Number nbLignes = MIF.nbRows;
        switch(Part)
       {
          case _ilu :
          case _illt :
            for (Number i=1; i <=nbLignes; i++)
                for (Number j=i; j <=nbLignes; j++)
                       if (MIF.pos(i,j) != 0.)
                            MM(i,j)= MIF(i,j);
          break;
          case _illstar :
            for (Number i=1; i <=nbLignes; i++)
                 for (Number j=i; j <= nbLignes; j++)
                       if (MIF.pos(i,j) != 0.)
                             {
                                MM(i,j)= conj(MIF(i,j));
                              }
          break;
          case _ildlt :
            { LargeMatrix<M> Mat_DLT(MM);Mat_DLT *= 0.;
             LargeMatrix<M> Mat_LT(MM); Mat_LT *= 0.;
             for (Number i=1; i <=nbLignes; i++)
                  for (Number j=i; j <= nbLignes; j++)
                      if (MIF.pos(i,j) != 0.)
                      {
                           if (i < j )
                           {
                               Mat_LT(i,j)= MIF(i,j);
                               Mat_DLT(i,j)=0.;
                            }
                           else if (i==j)
                           {
                               Mat_DLT(i,j)=MIF(i,j);
                               Mat_LT(i,j)=1;
                           }
                      }
             MM = Mat_DLT*Mat_LT;
          break; }
          case _ildlstar :
           {
             LargeMatrix<M> Mat_Dstar(MM);Mat_Dstar *= 0.;
             LargeMatrix<M> Mat_Lstar(MM); Mat_Lstar *= 0.;
             for (Number i=1; i <=nbLignes; i++)
                  for (Number j=i; j <= nbLignes; j++)
                      if (MIF.pos(i,j) != 0.)
                      {
                           if (i < j )
                           {
                               Mat_Lstar(i,j)= MIF(i,j);
                               Mat_Dstar(i,j)=0.;
                            }
                           else if (i==j)
                           {
                               Mat_Dstar(i,j)=MIF(i,j);
                               Mat_Lstar(i,j)=1;
                           }
                      }
             MM = Mat_Dstar*Mat_Lstar.toConj();
          break;
          }
          default :
             std::cout<< "Probleme: type de factorisation non prevu"<<std::endl;
       }
}
*/
/*************************************************************************************************/
/*
template<typename M>
int validIncompleteFactoLM( FactorizationType Part, LargeMatrix<M>& M0, LargeMatrix<M>& MIF, Real eps)
{
  Number OK =1;
  std::stringstream out;
   String Name;
   if (Part == _ilu) {Name="iLU";}
   if (Part == _illt) {Name="iLLt";}
   if (Part == _illstar) {Name="iLL*";}
   if (Part == _ildlt) {Name="iLDLt";}
   if (Part == _ildlstar) {Name="iLDL*";}
  MatrixStorage* StoTyp = M0.storagep();
  Number nbLignes = M0.numberOfRows();
  LargeMatrix<M> TESTM(StoTyp, 0, _noSymmetry); //TESTM = M0;
  LargeMatrix<M> Mat(TESTM);
   Mat*=0;//M0*0.;
  LargeMatrix<M> Mat_L(TESTM);
  Mat_L*=0;
  for (Number i=1; i <=nbLignes; i++)
       for (Number j=1; j <= i; j++)
               if (MIF.pos(i,j) != 0.)
                   {
                       if (i > j ) { Mat_L(i,j)= MIF(i,j);}
                       else if (i==j) {
                                             if ( (Part == _illt) || (Part == _illstar) )
                                                    Mat_L(i,j) = MIF(i,j);
                                             else {Mat_L(i,j)=1.;}
                                            }
                    }
  getPart( Part, MIF,  Mat);
  LargeMatrix<M> MVerif=Mat_L*Mat;
  for(Number j = 1; j <= nbLignes ; j++)
  {
       for(Number i = 1; i <= nbLignes; i++)
        {
                   if ( ( MVerif.pos(i,j) != 0 ) && ( M0.pos(i,j) != 0 ) )
                   {
                      switch (Part)
                      {
                          case _illt:
                              if ( i >= j)
                              {
                                  if   ( std::abs(M0(i,j) - MVerif(i,j)) > eps )
                                  {
                                       out << "non valide"<<std::endl;
                                       out << "car "<<M0(i,j)<<" \\neq "<<MVerif(i,j);
                                       out<<" pour L, M("<<i<<","<<j<<")"<<std::endl;
                                       OK=0; //break;
                                  }
                              }
                              if ( i < j)
                              {
                                  if   ( std::abs(M0(j,i) -MVerif(i,j)) > eps )
                                  {
                                       out << "non valide"<<std::endl;
                                       out << "car "<<M0(i,j)<<" \\neq "<<MVerif(i,j);
                                       out<<" pour U, M("<<i<<","<<j<<")"<<std::endl;
                                       OK=0; //break;
                                  }
                              }
                          break;
                          case _illstar:
                          case _ildlstar:
                             if ( i >= j)
                              {
                                  if   ( std::abs(M0(i,j) - MVerif(i,j)) > eps )
                                  {
                                      out << "  i >= j "<<std::endl;
                                      out << "non valide"<<std::endl;
                                      out << "car "<<M0(i,j)<<" \\neq "<<MVerif(i,j);
                                      out<<" pour L, M("<<i<<","<<j<<")"<<std::endl;
                                       OK=0; //break;
                                  }
                              }
                              if ( i < j)
                              {
                               // en attendant
                                  if   ( std::abs( conj(M0(i,j)) -MVerif(i,j) ) > eps )
// //                                   if   ( std::abs(M0(i,j) - MVerif(i,j)) > eps )
                                  {
                                       out << "  i < j "<<std::endl;
                                       out << std::abs(conj(M0(j,i)) - MVerif(i,j))<<std::endl ;
                                       out << "non valide"<<std::endl;
                                       out << "car "<<conj(M0(i,j)) <<" \\neq "<<MVerif(i,j);
                                       out<<" pour U, M("<<i<<","<<j<<")"<<std::endl;
                                       OK=0; //break;
                                  }
                              }
                              break;
                              default:
                              break;
                       }// fin switch
                   }
         }
   }
   //if (OK==0) out<<"-> Test incomplete facto "<<Name<<": OK,  valide"<<std::endl;
   return OK;
  }

*/
/*************************************************************************************************/
/*
int validIncompleteFacto( FactorizationType Part, TermMatrix& H0, TermMatrix& HIF, Real eps)
{/0/valueType()
    int OK=1;
    if (H0.valueType() == _real)
        {
            LargeMatrix<Real>   H0LM; H0LM = H0.getLargeMatrix<Real>();
            LargeMatrix<Real>   HIFLM = HIF.getLargeMatrix<Real>();
            OK=validIncompleteFactoLM(  Part, H0LM,  HIFLM, eps);
         }
    else if (H0.valueType() == xlifepp::_complex)
        {
            LargeMatrix<Complex>   H0LM; H0LM = H0.getLargeMatrix<Complex>();
            LargeMatrix<Complex>   HIFLM = HIF.getLargeMatrix<Complex>();
            OK = validIncompleteFactoLM(  Part, H0LM,  HIFLM, eps);
         }
     return OK;
}


*/

} //end namespace