/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lun�ville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
	\file unit_parametrization.cpp
	\since 23 may 2019
	\date 23 may 2019
	\author Eric Lunéville

	test the parametrization classes
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"
#include <limits>

using namespace std;
using namespace xlifepp;

Vector<Real> f(const Point& tp, Parameters& pars, DiffOpType d)
{
    Real t=tp[0],  a=pars("a"), b=pars("b");
    Vector<Real> v(2,0.);
    switch(d)
    {
        case _id  : v(1)=a*cos(t);v(2)=b*sin(t);break;
        case _d1  : v(1)=-a*sin(t);v(2)=b*cos(t);break;
        case _d11 : v(1)=-a*cos(t);v(2)=-b*sin(t);break;
        case _d111: v(1)=a*sin(t);v(2)=-b*cos(t);break;
        default  : parfun_error("f",d);
    }
    return v;
}

Vector<Real> invf(const Point& tp, Parameters& pars, DiffOpType d)
{
     if(d!=_id) parfun_error("invf",d);
     Real  a=pars("a"), b=pars("b");
     return Vector<Real>(1,atan2(tp(2)/b,tp(1)/a));
}

Vector<Real> length(const Point& tp, Parameters& pars, DiffOpType d)
{
     Real t=tp[0],  a=pars("a"), b=pars("b"), st=sin(t),ct=cos(t);
     return Vector<Real>(1,sqrt(a*a*st*st+b*b*ct*ct));
}

Vector<Real> curvature(const Point& tp, Parameters& pars, DiffOpType d)
{
     Real t=tp[0],  a=pars("a"), b=pars("b"), st=sin(t),ct=cos(t);
     return Vector<Real>(1,a*b*pow(a*a*st*st+b*b*ct*ct,-1.5));
}

Vector<Real> normal(const Point& tp, Parameters& pars, DiffOpType d)
{
     Real t=tp[0], a=pars("a"), b=pars("b");
     Vector<Real> n(2,0);
     n(1)=-a*sin(t);n(2)=b*cos(t);
     return n/norm2(n);
}

Vector<Real> tangent(const Point& tp, Parameters& pars, DiffOpType d)
{
     Real t=tp[0],  a=pars("a"), b=pars("b");
     Vector<Real> ta(2,0);
     ta(1)=b*cos(t);ta(2)=a*sin(t);
     return ta/norm2(ta);
}

//  test EllipsoidsidePart parametrizations
void checkParametrization(const Parametrization& psp, const Point& p)
{
  String ps=p.toString();
  theCout<<"-------------------------- at p="<<ps<<" --------------------------"<<eol;
  theCout<<"psp"<<ps<<"="<<psp(p)<<" psp.toParameter(psp"<<ps<<"="<<psp.toParameter(psp(p))<<eol;
  theCout<<"psp.lengths"<<ps<<"="<<psp.lengths(p)<<eol;
  theCout<<"psp.weingarten"<<ps<<"="<<eol<<psp.weingarten(p)<<eol;
  theCout<<"psp.curvatures"<<ps<<"="<<psp.curvatures(p)<<eol;
  theCout<<"psp.normal"<<ps<<"="<<psp.normal(p)<<eol;
  theCout<<"psp.tangents"<<ps<<"="<<psp.tangents(p)<<eol;
  theCout<<"psp.jacobian"<<ps<<"="<<eol<<psp.jacobian(p)<<eol;
  theCout<<"psp.metricTensor"<<ps<<"="<<psp.metricTensor(p)<<eol;
  theCout<<"psp.christoffel"<<ps<<"="<<psp.christoffel(p)<<eol;

//  Number nl=20;
//  Real du=1./nl, dv=du;
//  for(Real v=0;v<=1+theTolerance;v+=dv)
//  {
//    std::ofstream os("line_v="+tostring(v)+".dat");
//    for(Real u=0;u<=1.+theTolerance;u+=0.5*du)
//    {
//      Point P=psp(u,v);
//      os<<P[0]<<" "<<P[1]<<" "<<P[2]<<eol;
//    }
//    os.close();
//  }
//  for(Real u=0;u<=1+theTolerance;u+=du)
//  {
//    std::ofstream os("line_u="+tostring(u)+".dat");
//    for(Real v=0;v<=1.+theTolerance;v+=0.5*dv)
//    {
//      Point P=psp(u,v);
//      os<<P[0]<<" "<<P[1]<<" "<<P[2]<<eol;
//    }
//    os.close();
//  }
}

namespace unit_parametrization {

void unit_parametrization(int argc, char* argv[], bool check)
{
  String rootname = "unit_parametrization";
  trace_p->push(rootname);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(10);
  Parameters pars;
  pars<<Parameter(1.,"a")<<Parameter(1.,"b");
  Parametrization p1;
  ssout<<"Parametrization p1 : "<<p1<<eol;

Vector<Real> VP2(7); Vector<String> VSP2(2); Vector<Vector<Real> > VVP2(4);
  Parametrization p2(0,2*pi_,f,pars,"ellipse");
  VSP2[0]=tostring(p2); ssout<<"p2(0,2*pi_,f,pars,\"ellipse\") : "<<VSP2[0]<<eol;
  Point q=p2(pi_/4);
  VSP2[1]=tostring(q); ssout<<"p2(pi/4)="<<q<<eol;
  p2.setinvParametrization(invf);
  VP2[0]=p2.toParameter(q)[0]; ssout<<"p2.toParameter(q) = "<<VP2[0]<<eol;
  VP2[1]=p2.toRealParameter(q);ssout<<"p2.toRealParameter(q) = "<<VP2[1]<<eol;
  VP2[2]=p2.length(pi_/4);ssout<<"p2.length(pi/4) = "<<VP2[2]<<eol;
  VP2[3]=p2.curvature(pi_/4);ssout<<"p2.curvature(pi/4) = "<<VP2[3]<<eol;
  VVP2[0]=p2.normal(pi_/4);ssout<<"p2.normal(pi/4) = "<<VVP2[0]<<eol;
  p2.setLength(length);
  VP2[3]=p2.length(pi_/4);ssout<<"p2.length(pi/4) = "<<VP2[3]<<eol;
  p2.setCurvature(curvature);
  ssout<<"p2.curvature(pi/4) = "<<p2.curvature(pi_/4)<<eol;VP2[4]=p2.curvature(pi_/4);
  p2.setNormal(normal);
  VVP2[1]=p2.normal(pi_/4);ssout<<"p2.normal(pi/4) = "<<VVP2[1]<<eol;
  VVP2[2]=p2.tangent(pi_/4);ssout<<"p2.tangent(pi/4) = "<<VVP2[2]<<eol;
  VP2[5]=p2.curabc(pi_/4);ssout<<"p2.curabc(pi/4) = "<<VP2[5]<<eol;
  p2.setTangent(tangent);
  VVP2[3]=p2.tangent(pi_/4);ssout<<"p2.tangent(pi/4) = "<<VVP2[3]<<eol;
  nbErrors+=checkValues(VP2, rootname+"/VP2.in",  tol, errors, "VP2", check);
  nbErrors+=checkValues(VVP2, rootname+"/VVP2.in",  tol, errors, "VPP2", check);
  nbErrors+=checkValues(VSP2, rootname+"/VSP2.in", errors, "VSP2", check);

  ssout<<" ================= symbolic parametrization =================="<<eol;
  Vector<Real> VP3(4); Vector<String> VSP3(2); Vector<Vector<Real> > VVP3(2);
  Parametrization p3(-pi_,pi_,cos(x_1),sin(x_1),pars,"unit circle");
  ssout<<"Parametrization p3(-pi_,pi_,cos(x_1),sin(x_1),pars,\"unit circle\")"<<eol;
  VSP3[0]=tostring(p3); ssout<<"  Parametrization p3 : "<<p3<<eol;
  VSP3[1]=tostring(p3(pi_/4)); ssout<<"  p3(pi/4)="<<p3(pi_/4)<<eol;
  p3.setinvParametrization(atan2(x_2,x_1));
  VP3[0]=p3.toRealParameter(q);  ssout<<"  p3.toRealParameter(q) = "<<VP3[0]<<eol;
  VP3[1]=p3.length(pi_/4); ssout<<"  p3.length(pi/4) = "<<VP3[1]<<eol;
  VP3[2]=p3.curvature(pi_/4); ssout<<"  p3.curvature(pi/4) = "<<VP3[2]<<eol;
  VVP3[0]=p3.normal(pi_/4); ssout<<"  p3.normal(pi/4) = "<<VVP3[0]<<eol;
  VVP3[1]=p3.tangent(pi_/4); ssout<<"  p3.tangent(pi/4) = "<<VVP3[1]<<eol;
  VP3[3]=p3.curabc(pi_/4);ssout<<"  p3.curabc(pi/4) = "<<VP3[3]<<eol;
  nbErrors+=checkValues(VP3, rootname+"/VP3.in",  tol, errors, "VP3", check);
  nbErrors+=checkValues(VVP3, rootname+"/VVP3.in",  tol, errors, "VVP3", check);
  nbErrors+=checkValues(VSP3, rootname+"/VSP3.in", errors, "VSP3", check);

  Vector<Real> VP4(4); Vector<String> VSP4(3); Vector<Vector<Real> > VVP4(8);
  Parametrization p4(-pi_/2,pi_/2,-pi_,pi_,cos(x_1)*cos(x_2),cos(x_1)*sin(x_2),sin(x_1),pars,"unit sphere");
  ssout<<"Parametrization p4(-pi_/2,pi_/2,-pi_,pi_,cos(x_1)*cos(x_2),cos(x_1)*sin(x_2),sin(x_1),pars,\"unit sphere\")"<<eol;;
  VSP4[0]=tostring(p4);ssout<<"  Parametrization p4 : "<<p4<<eol;
  VSP4[1]=tostring(p4(0.,0.)); ssout<<"  p4(0,0)="<<VSP4[1]<<eol;
  VP4[0]=p4.length(Point(0.,0.));ssout<<"  p4.length(Point(0,0)) = "<<VP4[0]<<eol;
  VP4[1]=p4.curvature(Point(0.,0.)); ssout<<"  p4.curvature(Point(0,0)) = "<<VP4[1]<<eol;
  VVP4[0]=p4.curvatures(Point(0.,0.)); ssout<<"  p4.curvatures(Point(0,0)) = "<<VVP4[0]<<eol;
  VVP4[1]=p4.normal(Point(0.,0.));ssout<<"  p4.normal(Point(0,0)) = "<<VVP4[1]<<eol;
  VVP4[2]=p4.tangent(Point(0.,0.)); ssout<<"  p4.tangent(Point(0,0)) = "<<VVP4[2]<<eol;
  VVP4[3]=p4.bitangent(Point(0.,0.)); ssout<<"  p4.bitangent(Point(0,0)) = "<<VVP4[3]<<eol;
  Point M(pi_/4,pi_/2);
  ssout<<"  M="<<M<<eol;
  VSP4[2]=tostring(p4(M));ssout<<"  p4(M)="<<VSP4[2]<<eol;
  VP4[2]=p4.length(M); ssout<<"  p4.length(M) = "<<VP4[2]<<eol;
  VP4[3]=p4.curvature(M); ssout<<"  p4.curvature(M) = "<<VP4[3]<<eol;
  VVP4[4]=p4.curvatures(M); ssout<<"  p4.curvatures(M) = "<<VVP4[4]<<eol;
  VVP4[5]=p4.normal(M); ssout<<"  p4.normal(M) = "<<VVP4[5]<<eol;
  VVP4[6]=p4.tangent(M);ssout<<"  p4.tangent(M) = "<<VVP4[6]<<eol;
  VVP4[7]=p4.bitangent(M); ssout<<"  p4.bitangent(M) = "<<VVP4[7]<<eol;
  nbErrors+=checkValues(VP4, rootname+"/VP4.in",  tol, errors, "VP4", check);
  nbErrors+=checkValues(VVP4, rootname+"/VVP4.in",  tol, errors, "VVP4", check);
  nbErrors+=checkValues(VSP4, rootname+"/VSP4.in", errors, "VSP4", check);

  //!test with geometry
  ssout<<" ================= parametrization of Segment =================="<<eol;
  Vector<Real> VSA(3); Vector<String> VSSA(4); Vector<Vector<Real> > VVSA(2);
  Segment sa(_v1=Point(0.,0.), _v2=Point(1.,1.));
  VSSA[0]=tostring(sa.parametrization()(0.).roundToZero() ); ssout<<" sa.parametrization()(0.) = "<<VSSA[0]<<eol;
  VSSA[1]=tostring(sa.parametrization()(0.5).roundToZero()); ssout<<" sa.parametrization()(0.5) = "<<VSSA[1]<<eol;
  VSSA[2]=tostring(sa.parametrization()(1).roundToZero());ssout<<" sa.parametrization()(1) = "<<VSSA[2]<<eol;
  VSSA[3]=tostring(sa.parametrization().toParameter(Point(0.5,0.5))); ssout<<" sa.parametrization().toParameter(0.5,0.5) = "<<VSSA[3]<<eol;
  VSA[0]=sa.parametrization().length(0.5);ssout<<" sa.parametrization().length(0.5) = "<<VSA[0]<<eol;
  VSA[1]=sa.parametrization().curvature(0.5); ssout<<" sa.parametrization().curvature(0.5) = "<<VSA[1]<<eol;
  VSA[2]=sa.parametrization().curabc(0.5);ssout<<" sa.parametrization().curabc(0.5) = "<<VSA[2]<<eol;
  VVSA[0]=sa.parametrization().normal(0.5); ssout<<" sa.parametrization().normal(0.5) = "<<VVSA[0]<<eol;
  VVSA[1]=sa.parametrization().tangent(0.5); ssout<<" sa.parametrization().tangent(0.5) = "<<VVSA[1]<<eol;
  nbErrors+=checkValues(VSA, rootname+"/VSA.in",  tol, errors, "VSA", check);
  nbErrors+=checkValues(VVSA, rootname+"/VVSA.in",  tol, errors, "VVSA", check);
  nbErrors+=checkValues(VSSA, rootname+"/VSSA.in", errors, "VSSA", check);

  ssout<<" ================= parametrization of CircArc =================="<<eol;
  Vector<Real> VCA(3); Vector<String> VSCA(4); Vector<Vector<Real> > VVCA(2);
  CircArc ca(_center=Point(0.,0.), _v1=Point(1.,0.), _v2=Point(0.,1.), _nnodes=30,_domain_name="Omega");
  VSCA[0]=tostring(ca.parametrization()(0.).roundToZero()); ssout<<" ca.parametrization()(0.) = "<<VSCA[0]<<eol;
  VSCA[1]=tostring(ca.parametrization()(0.5).roundToZero()); ssout<<" ca.parametrization()(0.5) = "<<VSCA[1]<<eol;
  VSCA[2]=tostring(ca.parametrization()(1).roundToZero()); ssout<<" ca.parametrization()(1) = "<<VSCA[2]<<eol;
  Point P4=ca.parametrization()(0.5);
  VSCA[3]=tostring(ca.parametrization().toParameter(P4)); ssout<<" ca.parametrization().toParameter(P4) = "<<VSCA[3]<<eol;
  VCA[0]=ca.parametrization().length(0.5); ssout<<" ca.parametrization().length(0.5) = "<<VCA[0]<<eol;
  VCA[1]=ca.parametrization().curvature(0.5); ssout<<" ca.parametrization().curvature(0.5) = "<<VCA[1]<<eol;
  VCA[2]=ca.parametrization().curabc(0.5); ssout<<" ca.parametrization().curabc(0.5) = "<<VCA[2]<<eol;
  VVCA[0]=ca.parametrization().normal(0.5);ssout<<" ca.parametrization().normal(0.5) = "<<VVCA[0]<<eol;
  VVCA[1]=ca.parametrization().tangent(0.5); ssout<<" ca.parametrization().tangent(0.5) = "<<VVCA[1]<<eol;
  nbErrors+=checkValues(VCA, rootname+"/VCA.in",  tol, errors, "VCA", check);
  nbErrors+=checkValues(VVCA, rootname+"/VVCA.in",  tol, errors, "VVCA", check);
  nbErrors+=checkValues(VSCA, rootname+"/VSCA.in", errors, "VSCA", check);

  ssout<<" ================= parametrization of EllArc =================="<<eol;
  Vector<Real> VEA(3); Vector<String> VSEA(4); Vector<Vector<Real> > VVEA(2);
  EllArc ea(_center=Point(0.,0.), _v1=Point(2.,0.), _v2=Point(0.,1.), _nnodes=30,_domain_name="Omega");
  VSEA[0]=tostring(ea.parametrization()(0.).roundToZero()); ssout<<" ea.parametrization()(0.) = "<<VSEA[0]<<eol;
  VSEA[1]=tostring(ea.parametrization()(0.5).roundToZero()); ssout<<" ea.parametrization()(0.5) = "<<VSEA[1]<<eol;
  VSEA[2]=tostring(ea.parametrization()(1.).roundToZero()); ssout<<" ea.parametrization()(1.) = "<<VSEA[2]<<eol;
  Point Pe4=ea.parametrization()(0.5);
  VSEA[3]=tostring(ea.parametrization().toParameter(Pe4)); ssout<<" ea.parametrization().toParameter(Pe4) = "<<VSEA[3]<<eol;
  VEA[0]=ea.parametrization().length(0.5);ssout<<" ea.parametrization().length(0.5) = "<<VEA[0]<<eol;
  VEA[1]=ea.parametrization().curvature(0.5); ssout<<" ea.parametrization().curvature(0.5) = "<<VEA[1]<<eol;
  VEA[2]=ea.parametrization().curabc(0.5); ssout<<" ea.parametrization().curabc(0.5) = "<<VEA[2]<<eol;
  VVEA[0]=ea.parametrization().normal(0.5); ssout<<" ea.parametrization().normal(0.5) = "<<VVEA[0]<<eol;
  VVEA[1]=ea.parametrization().tangent(0.5); ssout<<" ea.parametrization().tangent(0.5) = "<<VVEA[1]<<eol;
  nbErrors+=checkValues(VEA, rootname+"/VEA.in",  tol, errors, "VEA", check);
  nbErrors+=checkValues(VVEA, rootname+"/VVEA.in",  tol, errors, "VVEA", check);
  nbErrors+=checkValues(VSEA, rootname+"/VSEA.in", errors, "VSEA", check);

  ssout<<" ================= parametrization of ParametrizedArc =================="<<eol;
  Vector<Real> VPA(3); Vector<String> VSPA(8); Vector<Vector<Real> > VVPA(2);
  Parametrization ps(0,2*pi_,x_1*cos(x_1),x_1*sin(x_1),Parameters(),"tcos(t),tsin(t)");
  ParametrizedArc pa(_parametrization = ps, _partitioning=_linearPartition, _nbParts=100,_hsteps=0.1,_domain_name="Gamma");
  VSPA[0]=tostring(pa);
  VSPA[1]=tostring(pa.parametrization()(0.).roundToZero());
  VSPA[2]=tostring(pa.parametrization()(0.5).roundToZero());
  VSPA[3]=tostring(pa.parametrization()(1.).roundToZero());
  VPA[0]=pa.parametrization().length(0.5); ssout<<" pa.parametrization().length(0.5) = "<<VPA[0]<<eol;
  VPA[1]=pa.parametrization().curvature(0.5); ssout<<" pa.parametrization().curvature(0.5) = "<<VPA[1]<<eol;
  VPA[2]=pa.parametrization().curabc(0.5); ssout<<" pa.parametrization().curabc(0.5) = "<<VPA[2]<<eol;
  VVPA[0]=pa.parametrization().normal(0.5) ; ssout<<" pa.parametrization().normal(0.5) = "<<VVPA[0]<<eol;
  VVPA[1]=pa.parametrization().tangent(0.5) ; ssout<<" pa.parametrization().tangent(0.5) = "<<VVPA[1]<<eol;
  nbErrors+=checkValues(VSPA, rootname+"/VSPA.in", errors, "VSPA", check);
  nbErrors+=checkValues(VPA, rootname+"/VPA.in",  tol, errors, "VPA", check);
  nbErrors+=checkValues(VVPA, rootname+"/VVPA.in",  tol, errors, "VVPA", check);
  ssout<<" ================= parametrization of SplineArc =================="<<eol;
  Number n=5; std::vector<Point> points(n+1);
  Real x=0, dx=pi_/n;
  for(Number i=0;i<=n;i++, x+=dx) points[i]=Point(x,sin(x));

  SplineArc spaC2(_splineType=_C2Spline,_vertices=points);
  Vector<Real> VC2(3); Vector<String> VSC2(4); Vector<Vector<Real> > VVC2(2);
  VSC2[0]=tostring(spaC2); ssout<<VSC2[0];
  VSC2[1]=tostring(spaC2.parametrization()(0.)); ssout<<" spaC2.parametrization()(0.) = "<<VSC2[1]<<eol;
  VSC2[2]=tostring(spaC2.parametrization()(0.5)); ssout<<" spaC2.parametrization()(0.5) = "<<VSC2[2]<<eol;
  VSC2[3]=tostring(spaC2.parametrization()(1.)); ssout<<" spaC2.parametrization()(1.) = "<<VSC2[3]<<eol;
  VC2[0]=spaC2.parametrization().length(0.5); ssout<<" spaC2.parametrization().length(0.5) = "<<VC2[0]<<eol;
  VC2[1]=spaC2.parametrization().curvature(0.5); ssout<<" spaC2.parametrization().curvature(0.5) = "<<VC2[1]<<eol;
  VC2[2]=spaC2.parametrization().curabc(0.5); ssout<<" spaC2.parametrization().curabc(0.5) = "<<VC2[2]<<eol;
  VVC2[0]=spaC2.parametrization().normal(0.5); ssout<<" spaC2.parametrization().normal(0.5) = "<<VVC2[0]<<eol;
  VVC2[1]=spaC2.parametrization().tangent(0.5); ssout<<" spaC2.parametrization().tangent(0.5) = "<<VVC2[1]<<eol;
  nbErrors+=checkValues(VSC2, rootname+"/VSC2.in", errors, "VSC2", check);
  nbErrors+=checkValues(VVC2, rootname+"/VVC2.in", tol, errors, "VVC2", check);
  nbErrors+=checkValues(VC2, rootname+"/VC2.in", tol, errors, "VC2", check);

  SplineArc spaCR(_splineType=_CatmullRomSpline,_vertices=points);
  Vector<Real> VCR(3); Vector<String> VSCR(4); Vector<Vector<Real> > VVCR(2);
  VSCR[0]=tostring(spaCR.parametrization()(0.).roundToZero()); ssout<<" spaCR.parametrization()(0.) = "<<VSCR[0]<<eol;
  VSCR[1]=tostring(spaCR.parametrization()(0.5).roundToZero()); ssout<<" spaCR.parametrization()(0.5) = "<<VSCR[1]<<eol;
  VSCR[2]=tostring(spaCR.parametrization()(1.).roundToZero()); ssout<<" spaCR.parametrization()(1.) = "<<VSCR[2]<<eol;
  VCR[0]=spaCR.parametrization().length(0.5); ssout<<" spaCR.parametrization().length(0.5) = "<<VCR[0]<<eol;
  VCR[1]=spaCR.parametrization().curvature(0.5); ssout<<" spaCR.parametrization().curvature(0.5) = "<<VCR[1]<<eol;
  VCR[2]=spaCR.parametrization().curabc(0.5); ssout<<" spaCR.parametrization().curabc(0.5) = "<<VCR[2]<<eol;
  VVCR[0]=spaCR.parametrization().normal(0.5); ssout<<" spaCR.parametrization().normal(0.5) = "<<VVCR[0]<<eol;
  VVCR[1]=spaCR.parametrization().tangent(0.5); ssout<<" spaCR.parametrization().tangent(0.5) = "<<VVCR[1]<<eol;
  nbErrors+=checkValues(VSCR, rootname+"/VSCR.in", errors, "VSCR", check);
  nbErrors+=checkValues(VVCR, rootname+"/VVCR.in", tol, errors, "VVCR", check);
  nbErrors+=checkValues(VCR, rootname+"/VCR.in", tol, errors, "VCR", check);

  SplineArc spaB(_splineType=_BSpline,_vertices=points);
  Vector<Real> VB(3); Vector<String> VSB(4); Vector<Vector<Real> > VVB(2);
  VSB[0]=tostring(spaB.parametrization()(0.).roundToZero()); ssout<<" spaB.parametrization()(0.) = "<<VSB[0]<<eol;
  VSB[1]=tostring(spaB.parametrization()(0.5).roundToZero());ssout<<" spaB.parametrization()(0.5) = "<<VSB[1]<<eol;
  VSB[2]=tostring(spaB.parametrization()(1).roundToZero());ssout<<" spaB.parametrization()(1) = "<<VSB[2]<<eol;
  VB[0]=spaB.parametrization().length(0.5); ssout<<" spaB.parametrization().length(0.5) = "<<VB[0]<<eol;
  VB[1]=spaB.parametrization().curvature(0.5); ssout<<" spaB.parametrization().curvature(0.5) = "<<VB[1]<<eol;
  VB[2]=spaB.parametrization().curabc(0.5); ssout<<" spaB.parametrization().curabc(0.5) = "<<VB[2]<<eol;
  VVB[0]=spaB.parametrization().normal(0.5); ssout<<" spaB.parametrization().normal(0.5) = "<<VVB[0]<<eol;
  VVB[1]=spaB.parametrization().tangent(0.5); ssout<<" spaB.parametrization().tangent(0.5) = "<<VVB[1]<<eol;
  nbErrors+=checkValues(VSB, rootname+"/VSB.in", errors, "VSB", check);
  nbErrors+=checkValues(VVB, rootname+"/VVB.in", tol, errors, "VVB", check);
  nbErrors+=checkValues(VB, rootname+"/VB.in", tol, errors, "VB", check);

  ssout<<" ================= parametrization of ParametrizedSurface =================="<<eol;
  Parametrization pss(0,2*pi_,0.,2*pi_, x_1, x_2,cos(x_1)*cos(x_2),Parameters(),"u,v,cos(u)cos(v)");
  ParametrizedSurface pass(_parametrization = pss, _partitioning=_linearPartition, _nbParts=9,_hsteps=0.2,_domain_name="Gamma");
  Vector<Real> VSS(3); Vector<String> VSSS(4); Vector<Vector<Real> > VVSS(2);
  VSSS[0]=tostring(pass); ssout<<" pass = "<<VSSS[0];
  VSSS[1]=tostring(pass.parametrization()(0.,0.).roundToZero()); ssout<<" pass.parametrization()(0.,0.) = "<<VSSS[1]<<eol;
  VSSS[2]=tostring(pass.parametrization()(pi_,pi_).roundToZero()); ssout<<" pass.parametrization()(pi_,pi_) = "<<VSSS[2]<<eol;
  VSSS[2]=tostring(pass.parametrization()(pi_/4,pi_/4).roundToZero()); ssout<<" pass.parametrization()(pi_/4,pi_/4) = "<<VSSS[3]<<eol;
  VSS[0]=pass.parametrization().length(pi_/4,pi_/4) ; ssout<<" pass.parametrization().length(pi_/4,pi_/4) = "<<VSS[0]<<eol;
  VSS[1]=pass.parametrization().curvature(pi_/4,pi_/4); ssout<<" pass.parametrization().curvature(pi_/4,pi_/4) = "<<VSS[1]<<eol;
  VVSS[0]=pass.parametrization().normal(pi_/4,pi_/4); ssout<<" pass.parametrization().normal(pi_/4,pi_/4)= "<<VVSS[0]<<eol;
  VVSS[1]=pass.parametrization().tangents(pi_/4,pi_/4); ssout<<" pass.parametrization().tangents(pi_/4,pi_/4) = "<<VVSS[1]<<eol;
  nbErrors+=checkValues(VSSS, rootname+"/VSSS.in", errors, "VSSS", check);
  nbErrors+=checkValues(VVSS, rootname+"/VVSS.in", tol, errors, "VVSS", check);
  nbErrors+=checkValues(VSS, rootname+"/VSS.in", tol, errors, "VSS", check);
  Mesh msp(pass, _shape=_triangle, _order=1);

   theCout<<" ================= parametrization of a composite curve =================="<<eol;
   Vector<Real> Vobs(3); Vector<String> VSobs(7); Vector<Vector<Real> > VVobs(2);
   Real dist=2., angle=7*pi_/8, hsize=0.1;
   Real R=dist*std::sin(angle);
   Point O(0.,0.), C(-dist,0.), A(-dist-R,0.);
   Point P(-dist+R*std::sin(angle),-R*std::cos(angle)), Q(-dist+R*std::sin(angle),R*std::cos(angle));
   Segment Sp(_v1=O,_v2=P,_hsteps=Reals(hsize/10,hsize),_domain_name="GammaP");
   Segment Sm(_v1=Q,_v2=O,_hsteps=Reals(hsize,hsize/10),_domain_name="GammaM");
   CircArc Cap(_center=C,_v1=P,_v2=A,_hsteps=Reals(hsize,hsize),_domain_name="GammaC");
   CircArc Cam(_center=C,_v1=A,_v2=Q,_hsteps=Reals(hsize,hsize),_domain_name="GammaC");
   Geometry obs=Sp+Cap+Cam+Sm; obs.domName()="obstacle";
   VSobs[0]=tostring(obs); theCout<<VSobs[0]<<eol;
   theCout<<VSobs[1]<<eol;
   VSobs[1]=tostring(obs.parametrization()(0.).roundToZero()); theCout<<" obs.parametrization()(0.)="<<VSobs[1]<<eol;
   VSobs[2]=tostring(obs.parametrization()(0.5).roundToZero()); theCout<<" obs.parametrization()(0.5)="<<VSobs[2]<<eol;
   VSobs[3]=tostring(obs.parametrization()(1.).roundToZero()); theCout<<" obs.parametrization()(1)="<<VSobs[3]<<eol;
   VSobs[4]=tostring(obs.parametrization().toParameter(obs.parametrization()(0.))); theCout<<" obs.parametrization().toParameter(obs.parametrization()(0.))  = "<<VSobs[4]<<eol;
   VSobs[5]=tostring(obs.parametrization().toParameter(obs.parametrization()(0.5))); theCout<<" obs.parametrization().toParameter(obs.parametrization()(0.5)) = "<<VSobs[5]<<eol;
   VSobs[6]=tostring(obs.parametrization().toParameter(obs.parametrization()(0.99))); theCout<<" obs.parametrization().toParameter(obs.parametrization()(0.99))  = "<<VSobs[6]<<eol;
   Vobs[0]=obs.parametrization().length(0.5); theCout<<" obs.parametrization().length(0.5) = "<<Vobs[0]<<eol;
   Vobs[1]=obs.parametrization().curvature(0.5); theCout<<" obs.parametrization().curvature(0.5) = "<<Vobs[1]<<eol;
   Vobs[2]=obs.parametrization().curabc(0.5); theCout<<" obs.parametrization().curabc(0.5) = "<<Vobs[2]<<eol;
   VVobs[0]=obs.parametrization().normal(0.5); theCout<<" obs.parametrization().normal(0.5) = "<<VVobs[0]<<eol;
   VVobs[1]=obs.parametrization().tangent(0.5); theCout<<" obs.parametrization().tangent(0.5) = "<<VVobs[1]<<eol;
   nbErrors+=checkValues(VSobs, rootname+"/VSobs.in", errors, "VSobs", check);
   nbErrors+=checkValues(VVobs, rootname+"/VVobs.in", tol, errors, "VVobs", check);
   nbErrors+=checkValues(Vobs, rootname+"/Vobs.in", tol, errors, "Vobs", check);

   theCout<<" ================= parametrization of a composite curve from surface=================="<<eol;
   Vector<Real> VGC(3); Vector<String> VSGC(7); Vector<Vector<Real> > VVGC(2);
   Geometry lobs=surfaceFrom(obs,"lobs",true);
   theCout<<lobs<<eol;
   Mesh mobs(lobs, _shape=_triangle);
   Domain gammaC=mobs.domain("GammaC");
   theCout<<gammaC<<eol;
   VSGC[0]=tostring(gammaC.parametrization());theCout<<VSGC[0]<<eol;
   VSGC[1]=tostring(gammaC.parametrization()(0.).roundToZero()); theCout<<" gammaC.parametrization()(0.)="<<VSGC[1]<<eol;
   VSGC[2]=tostring(gammaC.parametrization()(0.5).roundToZero()); theCout<<" gammaC.parametrization()(0.5)="<<VSGC[2]<<eol;
   VSGC[3]=tostring(gammaC.parametrization()(1.).roundToZero()); theCout<<" gammaC.parametrization()(1)="<<VSGC[3]<<eol;
   VGC[0]=gammaC.parametrization().length(0.5); theCout<<" gammaC.parametrization().length(0.5) = "<<VGC[0]<<eol;
   VGC[1]=gammaC.parametrization().curvature(0.5);   theCout<<" gammaC.parametrization().curvature(0.5) = "<<VGC[1]<<eol;
   VGC[2]=gammaC.parametrization().curabc(0.5); theCout<<" gammaC.parametrization().curabc(0.5) = "<<VGC[2]<<eol;
   VVGC[0]=gammaC.parametrization().normal(0.5); theCout<<" gammaC.parametrization().normal(0.5) = "<<VVGC[0]<<eol;
   VVGC[1]=gammaC.parametrization().tangent(0.5); theCout<<" gammaC.parametrization().tangent(0.5) = "<<VVGC[1]<<eol;
   VSGC[4]=tostring(gammaC.parametrization().toParameter(gammaC.parametrization()(0.))); theCout<<" gammaC.parametrization().toParameter(gammaC.parametrization()(0.))  = "<<VSGC[4]<<eol;
   VSGC[5]=tostring(gammaC.parametrization().toParameter(gammaC.parametrization()(0.5))); theCout<<" gammaC.parametrization().toParameter(gammaC.parametrization()(0.5)) = "<<VSGC[5]<<eol;
   VSGC[6]=tostring(gammaC.parametrization().toParameter(gammaC.parametrization()(0.99))); theCout<<" gammaC.parametrization().toParameter(gammaC.parametrization()(0.99))  = "<<VSGC[6]<<eol;

  theCout<<" ================= parametrization of a polygon =================="<<eol;
  std::vector<Point> vertices(5);
  vertices[0]=Point(0.,0.); vertices[1]=Point(2.,0.); vertices[2]=Point(3.,1.);
  vertices[3]=Point(1.,4.); vertices[4]=Point(-1.,2.);
  Polygon pol(_vertices=vertices,_side_names=Strings("S1","S2","S3","S4","S5"));
  theCout << pol << eol<<std::flush;
  theCout << pol.parametrization() << eol;

  Number np=10;
  vertices.resize(np);
  Real t=2*pi_/np;
  for(Number i=0;i<np;i++)  vertices[i]=Point(cos(i*t),sin(i*t));
  Polygon poln(_vertices=vertices);
  theCout << poln << eol;
  theCout << poln.parametrization() << eol;
  theCout<<" poln.parametrization()(0.,0.)="<<poln.parametrization()(0.,0.).roundToZero()<<eol;
  theCout<<" poln.parametrization()(0.6,0.6)="<<poln.parametrization()(0.6,0.6).roundToZero()<<eol;
  for(Real v=0.;v<=1.;v+=0.1)
  {
    String na="traj_v="+tostring(v)+".dat";
    std::ofstream of(na.c_str());
    for(Real u=0.;u<=1.;u+=0.01)
    {
     Point p=poln.parametrization()(u,v).roundToZero();
     if(p.size()>0) of<<p[0]<<" "<<p[1]<<eol;
    }
   of.close();
  }
  for(Real u=0.;u<=1.;u+=0.1)
  {
    String na="traj_u="+tostring(u)+".dat";
    std::ofstream of(na.c_str());
    for(Real v=0.;v<=1.;v+=0.01)
    {
     Point p=poln.parametrization()(u,v).roundToZero();
     if(p.size()>0) of<<p[0]<<" "<<p[1]<<eol;
    }
   of.close();
  }

  theCout<<" ================= EllipsoidSidePart theta-phi parametrization =================="<<eol;
  Sphere Sph(_center=Point(0,0,0),_radius=1.);
  EllipsoidSidePart Esp1(Sph,-pi_,pi_,-pi_/2,pi_/2);
  checkParametrization(Esp1.parametrization(),Point(0.5,0.5));
  checkParametrization(Esp1.parametrization(),Point(0.,0.25));
  checkParametrization(Esp1.parametrization(),Point(0.,0.));
  checkParametrization(Esp1.parametrization(),Point(0.,1E-10));
  theCout<<" ================= EllipsoidSidePart bi-stereographic parametrization =================="<<eol;
  EllipsoidSidePart Esp2(Sph,-pi_,pi_,-pi_/2,pi_/2,_bistereographic);
  checkParametrization(Esp2.parametrization(),Point(0,0.5));
  checkParametrization(Esp2.parametrization(),Point(0.25,0.5));
  checkParametrization(Esp2.parametrization(),Point(0.5,0.5));
  checkParametrization(Esp2.parametrization(),Point(0.75,0.5));
  checkParametrization(Esp2.parametrization(),Point(1,0.5));

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}
}
