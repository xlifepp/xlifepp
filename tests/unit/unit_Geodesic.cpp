/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHout ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Geodesic.cpp
	\author E. Lunéville
	\since 14 mar 2014
	\date 14 mar 2014

	Low level tests of Geodesic class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_Geodesic {

typedef std::list<std::pair<MeshedGeodesic*,Number> > listGeodNum;

void buildSegElt(GeomElement* gelt, Real v1, Real v2, Real v3, Point& P1, Point& P2, Number& s1, Number& s2)
{
  int s=0;
  if(v1>0) s+=1; else s-=1;
  if(v2>0) s+=1; else s-=1;
  if(v3>0) s+=1; else s-=1;
  if(std::abs(s)!=1)
  {
    s1=0;s2=0;return;
  }
  Real a;
  if(v1*v2>0) // segment [1,3]-[2,3]
  {
    a=v1/(v1-v3);
    P1 =(1-a)*gelt->vertex(1)+a*gelt->vertex(3);
    s1=3;
    a=v2/(v2-v3);
    P2 =(1-a)*gelt->vertex(2)+a*gelt->vertex(3);
    s2=2;
  }
  if(v1*v3>0) // segment [1,3]-[2,3]
  {
    a=v1/(v1-v2);
    P1 =(1-a)*gelt->vertex(1)+a*gelt->vertex(2);
    s1=1;
    a=v2/(v2-v3);
    P2 =(1-a)*gelt->vertex(2)+a*gelt->vertex(3);
    s2=2;
  }
  if(v2*v3>0) // segment [1,3]-[2,3]
  {
    a=v1/(v1-v2);
    P1 =(1-a)*gelt->vertex(1)+a*gelt->vertex(2);
    s1=1;
    a=v1/(v1-v3);
    P2 =(1-a)*gelt->vertex(1)+a*gelt->vertex(3);
    s2=3;
  }
}

/*! build a list of shadow/light curves, each curve being described as a list of SegElt
    assuming triangular element and use only P1 dof
    incn : TermVector handling the inc.normal vector (should be a single unknown scalar term)
    slcs: list of shadow/light curves built, each curve being a list<SegElt>
*/
void buildShadowLight3DP1(const TermVector& incn, std::list<std::list<SegElt> >& slcs)
{
  const Space* sp=incn.subVector().spacep();
  const FeSpace* fsp = sp->feSpace();
  const FeSubSpace* ssp = sp->feSubSpace();
  if(fsp == 0 && ssp == 0)
    {
      where("buildShadowLight3D");
      error("not_fe_space_type", sp->name());
    }
  std::set<const Element*> setgelts;
  if(fsp!=nullptr)
  {
    std::vector<Element>::const_iterator ite=fsp->elements.begin();
    for(;ite!=fsp->elements.end();++ite) setgelts.insert(&(*ite));
  }
  else
  {
    std::vector<Element*>::const_iterator ite=ssp->elements.begin();
    for(;ite!=ssp->elements.end();++ite) setgelts.insert(*ite);
  }
  const Vector<Real>& v= *incn.subVector().entries()->rEntries_p;
  Real v1,v2,v3;
  Point P1, P2, P2p;
  Number s1,s2;
  GeomElement *gelt,*gelt1,*gelt2;
  slcs.clear();
  std::set<const Element*>::iterator its;
  std::vector<Number>::const_iterator itd;
  //main loop on elements
  while(!setgelts.empty())
   {
     its = setgelts.begin();
     const Element* elt=*its;
     if(fsp!=nullptr) itd=elt->dofNumbers.begin();
     else itd=ssp->dofRanks[ssp->eltRanks[const_cast<Element*>(elt)]].begin();
     v1=v[*itd++-1]; v2=v[*itd++-1]; v3=v[*itd-1];
     gelt=elt->geomElt_p;
     buildSegElt(gelt, v1, v2, v3, P1,  P2,  s1, s2);
     //theCout<<"gelt="<<gelt<<" vertices : "<<gelt->vertex(1)<<" "<<gelt->vertex(2)<<" "<<gelt->vertex(3)<<" v1="<<v1<<" v2="<<v2<<" v3="<<v3<<" P1="<<P1<<" P2="<<P2<<" s1="<<s1<<" s2="<<s2<<eol<<std::flush;
     if(s1==0) setgelts.erase(its); // full light or full shadow
     else // start new curve
     {
       //theCout<<"========== new curve ==============="<<eol;
       slcs.push_back(std::list<SegElt>(1,SegElt(P1,P2,gelt,0,s1,s2)));
       P2p=P2;
       setgelts.erase(its);
       GeoNumPair pars1=gelt->elementSharingSide(s1), pars2=gelt->elementSharingSide(s2);
       gelt1=pars1.first; gelt2=pars2.first;
       //if(gelt1!=nullptr) theCout<<"   gelt1="<<gelt1<<" vertices : "<<gelt1->vertex(1)<<" "<<gelt1->vertex(2)<<" "<<gelt1->vertex(3)<<eol;
       //if(gelt2!=nullptr) theCout<<"   gelt2="<<gelt2<<" vertices : "<<gelt2->vertex(1)<<" "<<gelt2->vertex(2)<<" "<<gelt2->vertex(3)<<eol;
       if(fsp!=nullptr)
       {
         if(gelt1!=nullptr  && setgelts.find(fsp->element_p(gelt1))==setgelts.end()) gelt1=nullptr;
         if(gelt2!=nullptr  && setgelts.find(fsp->element_p(gelt2))==setgelts.end()) gelt2=nullptr;
       }
       else
       {
         if(gelt1!=nullptr  && setgelts.find(ssp->element_p(gelt1))==setgelts.end()) gelt1=nullptr;
         if(gelt2!=nullptr  && setgelts.find(ssp->element_p(gelt2))==setgelts.end()) gelt2=nullptr;
       }
       if(gelt1==0) {gelt1=gelt2;gelt2=nullptr;}
       //theCout<<" gelt1="<<gelt1<<" gelt2="<<gelt2<<eol<<std::flush;
       while(gelt1!=nullptr)
       {
         if(fsp!=nullptr) {elt = fsp->element_p(gelt1);itd=elt->dofNumbers.begin();}
         else       {elt = ssp->element_p(gelt1); itd=ssp->dofRanks[ssp->eltRanks[const_cast<Element*>(elt)]].begin();}
         v1=v[*itd++-1]; v2=v[*itd++-1]; v3=v[*itd-1];
         buildSegElt(gelt1, v1, v2, v3, P1,  P2,  s1, s2);
         //theCout<<"gelt1="<<gelt1<<" vertices : "<<gelt1->vertex(1)<<" "<<gelt1->vertex(2)<<" "<<gelt1->vertex(3)<<" v1="<<v1<<" v2="<<v2<<" v3="<<v3<<" P1="<<P1<<" P2="<<P2<<" s1="<<s1<<" s2="<<s2<<eol<<std::flush;
         if(s1!=0)
         {
           if(P1==P2p) {slcs.back().push_back(SegElt(P1,P2,gelt1,0,s1,s2));P2p=P2;}
           else        {slcs.back().push_back(SegElt(P2,P1,gelt1,0,s2,s1));P2p=P1;}

           setgelts.erase(elt);
           GeoNumPair pars1=gelt1->elementSharingSide(s1), pars2=gelt1->elementSharingSide(s2);
           gelt1=pars1.first; gelt2=pars2.first;
           //if(gelt1!=nullptr) theCout<<"   gelt1="<<gelt1<<" vertices : "<<gelt1->vertex(1)<<" "<<gelt1->vertex(2)<<" "<<gelt1->vertex(3)<<eol;
           //if(gelt2!=nullptr) theCout<<"   gelt2="<<gelt2<<" vertices : "<<gelt2->vertex(1)<<" "<<gelt2->vertex(2)<<" "<<gelt2->vertex(3)<<eol;
           if(fsp!=nullptr)
           {
            if(gelt1!=nullptr  && setgelts.find(fsp->element_p(gelt1))==setgelts.end()) gelt1=nullptr;
            if(gelt2!=nullptr  && setgelts.find(fsp->element_p(gelt2))==setgelts.end()) gelt2=nullptr;
           }
           else
           {
            if(gelt1!=nullptr  && setgelts.find(ssp->element_p(gelt1))==setgelts.end()) gelt1=nullptr;
            if(gelt2!=nullptr  && setgelts.find(ssp->element_p(gelt2))==setgelts.end()) gelt2=nullptr;
           }
           //theCout<<" gelt1="<<gelt1<<" gelt2="<<gelt2<<eol<<std::flush;
           if(gelt1==0) {gelt1=gelt2;gelt2=nullptr;}
         }
         else
         {
           setgelts.erase(elt);
           gelt1=gelt2;
           gelt2=nullptr;
         }
       }
        //end of curve
     }
   }
}

// find isoline 0 in triangle v1,v2,v3 with a1, a2, a3 real values
// if a line is found return true and the points P1,P2 and the side numbers s1,s2 are available
bool isoTri0(const Point& v1, const Point& v2, const Point& v3, Real a1, Real a2,Real a3, Point& P1, Point& P2, Number& s1, Number& s2)
{
  if(std::abs(a1)<theTolerance) a1=0;  //to deal with limit cases in a esay way
  if(std::abs(a2)<theTolerance) a2=0;
  if(std::abs(a3)<theTolerance) a3=0;
  if(a1==0 && a2==0 && a3==0) // special case, take middle relative to [v1,v2]
  {
    P1=0.5*(v1+v3); P2=0.5*(v2+v3);
    s1=3; s2=2;
    return true;
  }
  bool hasP1=false, hasP2=false;
  if((a1>=0 && a2<0)||(a1<0 && a2>=0))
  {
    P1=v1+a1/(a1-a2)*(v2-v1);s1=1;
    hasP1=true;
  }
  if((a2>=0 && a3<0)||(a2<0 && a3>=0))
  {
    Point Q =v2+a2/(a2-a3)*(v3-v2);
    if(hasP1){P2=Q;s2=2;hasP2=true;}
    else {P1=Q;s1=2;hasP1=true;}
  }
  if((a1>=0 && a3<0)||(a1<0 && a3>=0))
  {
    Point Q =v1+a1/(a1-a3)*(v3-v1);
    if(hasP2) return false; //should not be occured
    if(hasP1){P2=Q;s2=3;hasP2=true;}
    else {P1=Q;s1=3;hasP1=true;}
  }
  return hasP2;
}

/*! build a list of shadow/light curves, each curve being described as a list of SegElt
    assuming triangular element
    inc : TermVector handling the incident vector field (should be a single unknown scalar term)
    slc : list of shadow/light curves built, each curve being a list<SegElt>
    toP1: if true a build "P1" shadow/light interface else build a "P0" one
    fname : root filename to export shadow/light interface, if void no saving
    ds : step of the shadow/light interface; if 0, ds is computed as interface length/ number of elements crossed
    tau : a decreasing ratio used to adapt ds when "local curvature" is large; 0<tau<=1 (1 means no adaptation)
    Method :
      - build the color of element (-inc|n) where n is the outward normal (light: color>0, shadow color<=0)
      - change the color of elements surrounded by elements of the reverse color
      - build the P0 interface (SegElt's) between light elements and shadow elements (file 'fname_P0.dat')
      if toP1 option
      - build (inc|n) at vertices using a local interpolation based on the measure of neighbor elements of a vertex
      - modify P0 interface using local vertex approximate of (inc|n) -> P1 interface (file 'fname_P0.dat')
      - try to make continuous the P1 interface (file 'fname_P0_P1.dat')
      - reorder SegElt's (file 'fname_P0_P1_bis.dat')
      - make distribution of points of P1 interface uniform (file 'fname_P0_P1_Uni.dat')
*/
void buildShadowLight3D(const TermVector& inc, std::list<SegElt>& slc, bool toP1=true, const String& fname="",Real ds=0, Real tau=1)
{
  const Space* sp=inc.subVector().spacep();
  const FeSpace* fsp = sp->feSpace();
  const FeSubSpace* ssp = sp->feSubSpace();
  if(fsp == nullptr && ssp == nullptr)
  {
    where("buildShadowLight3D");
    error("not_fe_space_type", sp->name());
  }
  const Mesh* m=sp->domain()->mesh();
  if(m->sides().size()==0) const_cast<Mesh*>(m)->buildSides();

  std::ofstream os, oss;

  //set color of mesh elements regarding dot(inc,n)
  //-----------------------------------------------
  Number nelt=sp->nbOfElements();
  Vector<Vector<Real> >& vinc=*inc.subVector().entries()->rvEntries_p;
  Vector<Real> vint;
  std::vector<Number> dofnum;
  for(Number k=0;k<nelt;k++)
  {
     const Element& elt=sp->element(k);
     GeomElement* gelt=elt.geomElt_p;
     if(fsp!=nullptr)  elt.interpolate(vinc,gelt->center(),elt.dofNumbers,vint,_id);
     else elt.interpolate(vinc,gelt->center(),ssp->dofRanks[ssp->eltRanks[const_cast<Element*>(&elt)]],vint,_id);
     Real incn=dot(gelt->normalVector(),vint);
     gelt->color=-incn;  //light>0, shadow<=0
     if (std::abs(incn) < theTolerance) gelt->color=0.;
  }

  //clean shadow/light zones : "reverse" color of element if it is surrounded by element of reversed color
  //------------------------------------------------------------------------------------------------------
  for(Number k=0;k<nelt;k++)
  {
     const Element& elt=sp->element(k);
     GeomElement* gelt=elt.geomElt_p, *gelts;
     bool isLight=(gelt->color>0), isLights, sameColor=true;
     for(Number s=1;s<=3 && sameColor;s++)
     {
       const GeomElement* gs=gelt->sideElement(s);
       const std::vector<GeoNumPair>& pars=gs->parentSides();
       gelts=pars[0].first;
       if(gelts!=gelt)
       {
         if(s==1) isLights=gelts->color>0;
         else sameColor = isLights==gelts->color>0;
       }
       if(pars.size()>1)
       {
         gelts=pars[1].first;
         if(gelts!=gelt)
         {
           if(s==1) isLights=gelts->color>0;
           else sameColor = isLights==gelts->color>0;
         }
       }
     }
     if(sameColor && isLight!=isLights) gelt->color*=-1;  // reverse color, probably small color
  }

  if(fname!="")
  {
    os.open("light_elements.dat");
    oss.open("shadow_elements.dat");
    for(Number k=0;k<sp->nbOfElements();k++)
    {
      const Element& elt=sp->element(k);
      GeomElement* gelt=elt.geomElt_p;
      if(gelt->color>0)
      {
        for(Number i=1;i<=3;i++)
          os<<gelt->vertex(i)[0]<<" "<<gelt->vertex(i)[1]<<" "<<gelt->vertex(i)[2]<<eol;
      }
      else
      {
        for(Number i=1;i<=3;i++)
          oss<<gelt->vertex(i)[0]<<" "<<gelt->vertex(i)[1]<<" "<<gelt->vertex(i)[2]<<eol;
      }
    }
    os.close();
    oss.close();
  }

  //define shadow/light curve as union of side separating light element and shadow element
  //--------------------------------------------------------------------------------------
  const std::vector<GeomElement*>& sides=m->sides();
  std::vector<GeomElement*>::const_iterator its=sides.begin();
  for(;its!=sides.end();++its)
  {
     std::vector<GeoNumPair>& pars=(*its)->parentSides();
     //if(pars.size()>1 && pars[0].first->color!=pars[1].first->color)
     if(pars.size()>1) // shadow element is stored first
     {
       if(pars[0].first->color>0 && pars[1].first->color<=0)
        slc.push_back(SegElt((*its)->vertex(1),(*its)->vertex(2),pars[1].first,pars[0].first,pars[1].second,pars[0].second));
       if(pars[0].first->color<=0 && pars[1].first->color>0)
        slc.push_back(SegElt((*its)->vertex(1),(*its)->vertex(2),pars[0].first,pars[1].first,pars[0].second,pars[1].second));
     }
  }
  std::list<SegElt>::iterator itl;
  if(fname!="")
  {
    os.open(fname+"_P0.dat");
    for(itl=slc.begin();itl!=slc.end();++itl)
    {
     os<<itl->P1_(1)<<" "<<itl->P1_(2)<<" "<<itl->P1_(3)<<eol
       <<itl->P2_(1)<<" "<<itl->P2_(2)<<" "<<itl->P2_(3)<<eol;
    }
    os.close();
  }

  if(!toP1) return;

  //improve curve by local interpolation of incn, based on surface
  //--------------------------------------------------------------
  // local interpolation :  incn(P)=sum s_k*incn_k/sum s_k for elements containing P, s_k surface of elt_k, incn_k value on element
  const MeshDomain* mdom=sp->domain()->meshDomain();
  if(mdom->vertexElements.size()==0) mdom->buildVertexElements();
  std::map<Point,std::list<GeomElement*> >::iterator itm;
  std::list<GeomElement*>::iterator itgs;
  std::map<Point,Real> incns;
  std::list<Point> Ps;
  std::list<Point>::iterator itp;
  std::set<GeomElement*> sgelts;
  for(itl=slc.begin();itl!=slc.end();++itl)
  {
    if(itl->gelt2_!=nullptr && dot(itl->gelt1_->normalVector(),itl->gelt2_->normalVector())>0.9)
    {
      itm=mdom->vertexElements.find(itl->P1_);
      sgelts.insert(itm->second.begin(),itm->second.end());
      itm=mdom->vertexElements.find(itl->P2_);
      sgelts.insert(itm->second.begin(),itm->second.end());
    }
  }
  std::set<GeomElement*>::iterator it;
  for(it=sgelts.begin();it!=sgelts.end();++it)
  {
    for(Number i=1;i<=3;i++)
    {
      Point P=(*it)->vertex(i);
      if(incns.find(P)==incns.end())
      {
        itm=mdom->vertexElements.find(P);
        Real incn=0., s=0.;
        for(itgs=itm->second.begin();itgs!=itm->second.end();++itgs)
        {
          incn+=(*itgs)->color * (*itgs)->measure();
          s+=(*itgs)->measure();
        }
        incns[P]=incn/s;
      }
    }
  }

  // move the sl curve regarding incn values of neighbor elements
  Point v1,v2,v3,P1,P2;
  Number side, side2, s1,s2;
  Real inc1, inc2, inc3;
  Number i[3];
  bool hasFirst;
  GeomElement* gelt, *gelt2;
  std::map<GeomElement*,SegElt*> geoseg;  //map gelt->segelt
  std::map<GeomElement*,SegElt*>::iterator itmg;
  std::list<std::list<SegElt>::iterator> toErase;
  for(itl=slc.begin();itl!=slc.end();++itl)
  {
    //theCout<<(*itl)<<eol<<std::flush;
    gelt=itl->gelt1_;    //shadow element
    gelt2=itl->gelt2_;   //light element
    sgelts.erase(gelt);sgelts.erase(gelt2);
    if(gelt2!=nullptr && dot(gelt->normalVector(),gelt2->normalVector())>0.9) // a separating edge, not singular
    {
      hasFirst=false; s1=0;
      if(geoseg.find(gelt)==geoseg.end()) // gelt has not been analysed
      {
        side=itl->s1_; side2=itl->s2_;
        i[0]=side; i[1]=side+1; i[2]=side+2;
        if(side==2) i[2]=1; if(side==3) {i[1]=1;i[2]=2;}
        v1=gelt->vertex(i[0]); v2=gelt->vertex(i[1]); v3=gelt->vertex(i[2]);
        inc1=incns[v1]; inc2=incns[v2]; inc3=incns[v3];
        //theCout<<"  gelt1 :  inc1="<<inc1<<" inc2="<<inc2<<" inc3="<<inc3<<" v3="<<v3<<" side="<<side<<eol<<std::flush;
        if(isoTri0(v1,v2,v3,inc1,inc2,inc3,P1,P2,s1,s2)) // search isoline 0 using gelt1_
        {
          itl->P1_=P1; itl->P2_=P2; itl->s1_= i[s1-1]; itl->s2_=i[s2-1]; itl->gelt2_=nullptr;
          hasFirst=true;
          //theCout<<"  first segment "<<(*itl)<<" s1="<<s1<<" s2="<<s2<<eol<<std::flush;
          geoseg[gelt]=&(*itl);
        }
      }
      if(s1<2) // s1=0 means segment not found and 1 means segment crossing original segment, search using gelt2_
      {
        if(geoseg.find(gelt2)==geoseg.end())
        {
          gelt=gelt2; side=side2;  //light element
          i[0]=side; i[1]=side+1; i[2]=side+2;
          if(side==2) i[2]=1; if(side==3) {i[1]=1;i[2]=2;}
          v1=gelt->vertex(i[0]); v2=gelt->vertex(i[1]); v3=gelt->vertex(i[2]);
          inc1=incns[v1]; inc2=incns[v2]; inc3=incns[v3];inc3=incns[v3];
          //theCout<<"  gelt2 : inc1="<<inc1<<" inc2="<<inc2<<" inc3="<<inc3<<" v3="<<v3<<" side="<<side<<eol<<std::flush;
          if(isoTri0(v1,v2,v3,inc1,inc2,inc3,P1,P2,s1,s2)) // find isoline 0
          {
            if(hasFirst)
            {
              slc.push_back(SegElt(P1,P2,gelt,0,i[s1-1],i[s2-1]));
              geoseg[gelt]=&slc.back();
              //theCout<<"  second segment "<<slc.back()<<" s1="<<s1<<" s2="<<s2<<eol<<std::flush;
            }
            else
            {
              itl->P1_=P1; itl->P2_=P2; itl->s1_= i[s1-1]; itl->s2_=i[s2-1]; itl->gelt1_=gelt2; itl->gelt2_=nullptr;
              geoseg[gelt]=&(*itl);
              //theCout<<"  first segment "<<(*itl)<<" s1="<<s1<<" s2="<<s2<<eol<<std::flush;
            }
          }
          else if(!hasFirst) toErase.push_back(itl);  // remove current segelt because gelt2 is alredy filled
        }
        else if(!hasFirst) toErase.push_back(itl);  // remove current segelt because gelt2 is alredy filled
      }
    }
    else
    {
      geoseg[gelt]=&(*itl);
      if(gelt2!=nullptr) geoseg[gelt2]=&(*itl);
    }
  }
  for(std::list<std::list<SegElt>::iterator>::iterator ite=toErase.begin();ite!=toErase.end();++ite) slc.erase(*ite);
  toErase.clear();
  //deal with remaining element in segelts
  for(it= sgelts.begin();it!=sgelts.end();++it)
  {
    gelt=*it;
    if(geoseg.find(gelt)==geoseg.end())
    {
      v1=gelt->vertex(1); v2=gelt->vertex(2); v3=gelt->vertex(3);
      inc1=incns[v1]; inc2=incns[v2]; inc3=incns[v3];
      //theCout<<"  inc1="<<inc1<<" inc2="<<inc2<<" inc3="<<inc3<<" v1="<<v1<<" v2="<<v2<<" v3="<<v3<<eol<<std::flush;
      if(isoTri0(v1,v2,v3,inc1,inc2,inc3,P1,P2,s1,s2))
      {
        slc.push_back(SegElt(P1,P2,gelt,0,s1,s2));
        geoseg[gelt]=&slc.back();
        //theCout<<"  new segment "<<slc.back()<<eol<<std::flush;
      }
    }
  }
  geoseg.clear();
//  theCout<<"geoseg = "<<eol;
//  for(itmg=geoseg.begin();itmg!=geoseg.end();++itmg)
//    theCout<<"gelt="<<itmg->first<<" "<<itmg->first->number()<<" -> SegElt "<<itmg->second<<" "<<(*itmg->second)<<eol;
  //save to file
  if(fname!="")
  {
    os.open(fname+"_P0_P1.dat");
    for(itl=slc.begin();itl!=slc.end();++itl)
    {
      os<<itl->P1_(1)<<" "<<itl->P1_(2)<<" "<<itl->P1_(3)<<eol
        <<itl->P2_(1)<<" "<<itl->P2_(2)<<" "<<itl->P2_(3)<<eol;
    }
    os.close();
  }

  //reorder and try to make it continuous
  //-------------------------------------
  std::list<SegElt> slc0;
  std::map<Point,std::list<std::pair<SegElt*,bool> > > order;
  std::map<Point,std::list<std::pair<SegElt*,bool> > >::iterator ito;
  for(itl=slc.begin();itl!=slc.end();++itl)
  {
     ito=order.find(itl->P1_);
     if(ito==order.end()) order.insert(std::make_pair(itl->P1_,std::list<std::pair<SegElt*,bool> >(1,std::make_pair(&(*itl),true))));
     else ito->second.push_back(std::make_pair(&(*itl),true));
     ito=order.find(itl->P2_);
     if(ito==order.end()) order.insert(std::make_pair(itl->P2_,std::list<std::pair<SegElt*,bool> >(1,std::make_pair(&(*itl),false))));
     else ito->second.push_back(std::make_pair(&(*itl),false));
  }
   //locate isolated point and try to connect to a neigbor SegElt
  typedef std::map<Point,std::list<std::pair<SegElt*,bool> > >::iterator it_order;
  std::list<std::pair<it_order,it_order> > isolated;
  for(ito=order.begin();ito!=order.end();++ito)
  {
    if(ito->second.size()==1) isolated.push_back(std::make_pair(ito,ito)); //isolated point
  }
  std::list<std::pair<it_order,it_order> >::iterator iti=isolated.begin(), itj;
  for(Number i=0;i<isolated.size();i++, ++iti)
  {
    Real d=theRealMax, dij;
    const Point & pi=iti->first->first;
    itj=isolated.begin();
    for(Number j=0;j<isolated.size();j++, ++itj)
    {
      if(j!=i)
      {
        const Point & pj=itj->first->first;
        dij=norm(pi-pj);
        if(dij<d) {d=dij;iti->second=itj->first;}
      }
     }
     theCout<<i<<" -> P1="<<iti->first->first<<" P2="<<iti->second->first<<" d="<<d<<eol<<std::flush;
  }
  std::list<std::pair<SegElt*,bool> >::iterator itls;
  iti=isolated.begin();
  std::set<const Point*> spts;
  for(Number i=0;i<isolated.size() && iti!=isolated.end();i++, ++iti)
  {
    if(spts.find(&iti->second->first)==spts.end())
    {
      spts.insert(&iti->first->first);
      SegElt *selt1=iti->first->second.begin()->first,
             *selt2=iti->second->second.begin()->first;
      Real h=selt1->gelt1_->size()+selt2->gelt1_->size();
      if(norm(iti->first->first-iti->second->first)<h) // segelts are closed
      {
        bool isP11=iti->first->second.begin()->second,
             isP12=iti->second->second.begin()->second;
        Point *Pi1,*Pi2,*Pc1, *Pc2, *P0=0, *P3=0;
        if(isP11) {Pi1=&selt1->P1_; Pc1=&selt1->P2_;} else {Pi1=&selt1->P2_; Pc1=&selt1->P1_;}
        if(isP12) {Pi2=&selt2->P1_; Pc2=&selt2->P2_;} else {Pi2=&selt2->P2_; Pc2=&selt2->P1_;}
        ito=order.find(*Pc1);
        if(ito!=order.end())
        {
          itls=ito->second.begin();
          while(itls->first==selt1) itls++;
          if(itls->second) P0=&itls->first->P2_; else P0=&itls->first->P1_;
        }
        ito=order.find(*Pc2);
        if(ito!=order.end())
        {
          itls=ito->second.begin();
          while(itls->first==selt2) itls++;
          if(itls->second) P3=&itls->first->P2_; else P3=&itls->first->P1_;
        }
        //theCout<<"P0="<<*P0<<" Pc1="<<*Pc1<<" Pi1="<<*Pi1<<" Pi2="<<*Pi2<<" Pc2="<<*Pc2<<" P3="<<*P3;

        // choose the best way to connect segelts with an isolated end point : (P0 Pc1 Pi1 Pc2 P3) or (P0 Pc1 Pi2 Pc2 P3)
        // by comparing angle signs in the projection plane Pc1Pi1-Pc2Pi2 if available
        bool case2=false;  // default case (P0 Pc1 Pi1 Pc2 P3)

        if(P0!=nullptr && P3!=nullptr)
        {
           Point n=cross3D(*Pc1-*Pi1,*Pc2-*Pi2);
           Real no=norm(n);
           if(no>theTolerance)
           {
             Int sig=0;
             n/=no;
             Point u=*Pc1-*P0, v=*Pi2-*Pc1, w=*Pc2-*Pi2, t=*P3-*Pc2,
                   un=u-dot(u,n)*n, vn=v-dot(v,n)*n, wn=w-dot(w,n)*n, tn=t-dot(t,n)*n;
             Real a1=angle(un,vn,n), a2=angle(vn,wn,n), a3=angle(wn,tn,n);
             sig=(a1>=0?1:-1)+(a2>=0?1:-1)+(a3>=0?1:-1);
             //theCout<<" a1="<<a1<<" a2="<<a2<<" a3="<<a3<<" sig="<<sig<<eol<<std::flush;
             if(std::abs(sig)==3) case2=true;
             else // omit the smaller segment u or t
             {
                sig=0;
                if(norm(u)<norm(t)) sig=(a2>=0?1:-1)+(a3>=0?1:-1);
                else sig=(a1>=0?1:-1)+(a2>=0?1:-1);
                if(std::abs(sig)==2) case2=true;
             }
           }
        }
        if(case2) // P0 Pc1 Pi2 Pc2 P3
        {
          if(isP11) selt1->P1_=*Pi2; else selt1->P2_=*Pi2;
          iti->second->second.push_back(std::make_pair(selt1,isP11));
          order.erase(iti->first);
        }
        else // P0 Pc1 Pi1 Pc2 P3
        {
          if(isP12) selt2->P1_=*Pi1; else selt2->P2_=*Pi1;
          iti->first->second.push_back(std::make_pair(selt2,isP12));
          order.erase(iti->second);
        }
      }
    }
  }
  if(fname!="")
  {
    os.open(fname+"_P0_P1_bis.dat");
    for(itl=slc.begin();itl!=slc.end();++itl)
    {
      os<<itl->P1_(1)<<" "<<itl->P1_(2)<<" "<<itl->P1_(3)<<eol
        <<itl->P2_(1)<<" "<<itl->P2_(2)<<" "<<itl->P2_(3)<<eol;
    }
    os.close();
  }

//  for(ito=order.begin();ito!=order.end();++ito)
//  {
//      theCout<<ito->first<<": "<<eol;
//      for(std::list<std::pair<SegElt*,bool> >::iterator itb=ito->second.begin(); itb!=ito->second.end();++itb)
//        theCout<<"    "<<(*itb->first)<<" "<<" isP1="<<(itb->second?"true":"false")<<eol;
//  }
  //reordering such that (P1 P2) (P2 P3) ...
  std::set<SegElt*> vis;
  ito=order.begin();
  while(ito!=order.end() && ito->second.size()!=2) ++ito;
  if(ito==order.end()) ito=order.begin();
  SegElt* selt=ito->second.front().first;
  Point pdown=selt->P1_, pup=selt->P2_;
  slc0.push_back(*selt);
  vis.insert(selt);
  std::list<std::pair<SegElt*,bool> >::iterator itse;
  Number n=slc.size();
  bool up=true, down=true;

  bool stopbreak=false;
  while(n>0)
  {
     if(up)
     {
       ito=order.find(pup);
       if(ito==order.end()) up=false;
       else
       {
         itse=ito->second.begin();
         for(;itse!=ito->second.end();++itse)
         {
           selt=itse->first;
           if(vis.find(selt)==vis.end())
           {
              if(selt->P2_==pup) selt->reverse();
              pup=selt->P2_;
              vis.insert(selt);
              slc0.push_back(*selt);
              n--;
              break;
           }
         }
         order.erase(ito);
       }
     }
     if(down)
     {
       ito=order.find(pdown);
       if(ito==order.end()) down=false;
       else
       {
         itse=ito->second.begin();
         for(;itse!=ito->second.end();++itse)
         {
           selt=itse->first;
           if(vis.find(selt)==vis.end())
           {
              if(selt->P1_==pdown) selt->reverse();
              pdown=selt->P1_;
              vis.insert(selt);
              slc0.push_front(*selt);
              n--;
              break;
           }
         }
         order.erase(ito);
       }
     }
     if((!up && !down && n>0) || (pup==pdown))   //restart
     {
       if(order.size()==0) break;
       ito=order.begin();
       while(ito!=order.end() && ito->second.size()!=2) ++ito;
       if(ito==order.end()) ito=order.begin();
       selt=ito->second.front().first;
       pdown=ito->second.front().first->P1_;
       pup=ito->second.front().first->P2_;
      //  if (stopbreak) break;
       stopbreak=true;
     }
  }
  
  if(fname!="")
  {
   os.open(fname+"_P0_P1_C0.dat");
    for(itl=slc0.begin();itl!=slc0.end();++itl)
    {
      os<<itl->P1_(1)<<" "<<itl->P1_(2)<<" "<<itl->P1_(3)<<eol
        <<itl->P2_(1)<<" "<<itl->P2_(2)<<" "<<itl->P2_(3)<<eol;
    }
    os.close();
  }

  //uniform distribution
  slc.clear();
  std::list<SegElt>::iterator itln;
  Real l=0.; //compute total length
  for(itl=slc0.begin();itl!=slc0.end();++itl) l+=norm(itl->P2_-itl->P1_);
  if(ds==0) ds=l/slc0.size();
  itl=slc0.begin();
  Point P=itl->P1_, P12, P1P, P12n, Q;
  gelt=itl->gelt1_;
  Real a,b,c,d,t,dsi=ds,g;
  if(slc.size()>1)
  {
    itln=itl; ++itln;
    P12=itl->P2_-P; P12n=itln->P2_-P2;
    g=dot(P12,P12n)/(norm(P12)*norm(P12n)); g=1-g;  // 0<g<1 means obtuse angle and 1<g<2 means sharp angle
    dsi=2*tau*ds/((1-tau)*g+2*tau);   //for 0 angle (g=2) gives r*ds and pi angle (g=0) gives ds
  }
  bool initl=true;
  //theCout<<"uniform distribution ds="<<ds<<eol<<std::flush;
  while(itl!=slc0.end())
  {
    Point &P1=itl->P1_, &P2=itl->P2_;
    //theCout<<" in seg element ["<<P1(1)<<" "<<P2(1)<<"],["<<P1(2)<<" "<<P2(2)<<"],["<<P1(3)<<" "<<P2(3)<<"] initl="<<(initl?"true":"false")<<" dsi="<<dsi<<eol<<std::flush;
    if(initl) // P in itl
    {
      a=norm(P2-P);
      if(a>theTolerance) t=dsi/a; else t=2;
      if(t<1+theTolerance)
      {
        t=std::min(1.,t);
        Q=P+t*(P2-P);
        slc.push_back(SegElt(P,Q,itl->gelt1_,itl->gelt1_,0,0));
        //theCout<<"linear P=["<<P(1)<<"],["<<P(2)<<"],["<<P(3)<<"] Q=["<<Q(1)<<"],["<<Q(2)<<"],["<<Q(3)<<"] d="<<norm(P-Q)<<eol<<std::flush;
        P=Q;
        initl=true;
      }
      else {++itl;initl=false;}
    }
    else
    {// find 0<=t<=1 such that Q=P1+t(P2-P1) and |Q-P|=ds
     //   t+-=(-b+-sqrt(d))/a  with a=(P2-P1|P2-P1), b=(P2-P1|P1-P), c=(P1-P|P1-P)-ds*ds
      P12=P2-P1;P1P=P1-P;
      a=dot(P12,P12); b=dot(P12,P1P);c=dot(P1P,P1P)-dsi*dsi;
      d=b*b-a*c;
      if(d>-theTolerance)
      {
        d=std::max(0.,d); d=std::sqrt(d);
        if(b<=0) t=(d-b)/a; else t=-(b+d)/a;
        if(t<=-theTolerance || t>=1+theTolerance) t=c/(a*t);
        if(t>-theTolerance && t<1+theTolerance)
        {
          t=std::max(0.,std::min(1.,t));
          Q=P1+t*P12;
          slc.push_back(SegElt(P,Q,gelt,itl->gelt1_,0,0));
          //theCout<<"quadratic P=["<<P(1)<<"],["<<P(2)<<"],["<<P(3)<<"] Q=["<<Q(1)<<"],["<<Q(2)<<"],["<<Q(3)<<"] d="<<norm(P-Q)<<eol<<std::flush;
          P=Q; gelt=itl->gelt1_;
          initl=true;
          //adapt dsi
          itln=std::next(itl);
          if(itln==slc0.end()) itln=slc0.begin(); //ok for close curve
          P12n=itln->P2_-P2;
          g=dot(P12,P12n)/(norm(P12)*norm(P12n)); g=1-g;  // 0<g<1 means obtuse angle and 1<g<2 means sharp angle
          dsi=2*tau*ds/((1-tau)*g+2*tau);   //for 0 angle (g=2) gives r*ds and pi angle (g=0) gives ds
        }
        else {++itl;initl=false;}
      }
      else {++itl;initl=false;}
    }
  }
  itl=slc.end();--itl;
  if(norm(slc.front().P1_-slc.back().P2_)<0.5*ds) slc.erase(itl); //remove last is too close of first
  if(fname!="")
  {
   os.open(fname+"_P0_P1_C0_Uni.dat");
    for(itl=slc.begin();itl!=slc.end();++itl)
    {
      os<<itl->P1_(1)<<" "<<itl->P1_(2)<<" "<<itl->P1_(3)<<eol
        <<itl->P2_(1)<<" "<<itl->P2_(2)<<" "<<itl->P2_(3)<<eol;
    }
    os.close();
  }
}

/*compute field in shadow zone using UTD, du/dn=0 (Fock+creeping wave)
   requires a Parametrized geodesic with curAbcs computed, tangent computed
   k   : wave number
   ui  : incident field on geodesic points
   sb2 : shadow fock zone limit (Fock coordinate, sb2>0)
   sc  : limit of creeping waves (>s2) (curvilinear coordinate)
   spreadmax : spread limiter (1 no spread)
   wfs : width of the focal zone when used (in wavelength, good value : 1.75)
         if spreadmax <= 1 et dfs = 0. no spread correction
         if spreadmax > 1 use spread limiter : 1 <= spread <= spreadmax
         if spreadmax <= 1 et wfs > 0. use focal limiter
   sym : if true compute the sum of direct creeping ray and reverse creeping ray
   na  : root filename where fields are exported
   onMesh : if true, use MeshedGeodesic related to

   spread factors computation scheme solving h"(s)+g(s)h(s)=0; h(0)=1 and h'(0)=0; g(s) being the Gaussian curvature
    - forward Euler  : hnp1=hn+ds.dhn ; dhnp1=dhn-ds.gcn.hn
    - backward Euler : hnp1=hn+ds.dhn/(1+ds.gcnp1) ;  dhnp1=dh_n-ds.gcnp1.h_n+1
    - Neumark scheme with beta=1/4, gamma=1/2, (stable without any condition):
                     hnp1=(hn+ds*dhn-0.5*ds*ds*gcn*hn)/(1+0.25*ds*ds*gcnp1) ; dhnp1=dhn-0.5*ds*(gcn*hn+gcnp1*hnp1);
*/
void computeShadowField(const Geodesic& geod, const Vector<Complex>& ui, const Fock& fock, Real k,
                        Real sb2, Real sc, Real spreadmax, Real wfs, bool sym=false, const String& na="",bool onMesh=true)
{
   const std::vector<Point>* xs_p=&geod.xs_;
   const std::vector<Vector<Real> >* cus_p=&geod.curvatures_;
   const std::vector<Real>* cas_p=&geod.curAbcs_;
   Vector<Complex>* field_p = const_cast<Vector<Complex>*>(&geod.field_);
   std::vector<FieldType>* fieldTypes_p = const_cast<std::vector<FieldType>*>(&geod.fieldTypes_);
   if(onMesh)
   {
     if(geod.meshgeod_==0) error("free_error","computeLightField on mesh requires the mesh projection geodesic, call buildMeshedGeodesic before");
     xs_p=&geod.meshgeod_->xs_;
     cas_p=&geod.meshgeod_->curAbcs_;
     cus_p=&geod.meshgeod_->curvatures_;
     field_p = const_cast<Vector<Complex>*>(&geod.meshgeod_->field_);
     fieldTypes_p = const_cast<std::vector<FieldType>*>(&geod.meshgeod_->fieldTypes_);
   }
   field_p->clear(); fieldTypes_p->clear();
   Number nbf=cas_p->size();
   field_p->resize(nbf,Complex(0.)); fieldTypes_p->resize(nbf,_noFieldType);
   Vector<Complex>::iterator itf=field_p->begin();
   std::vector<FieldType>::iterator itt=fieldTypes_p->begin();
   std::vector<Point>::const_iterator itx=xs_p->begin();
   std::vector<Real>::const_iterator its=cas_p->begin();
   std::vector<Vector<Real> >::const_iterator itc=cus_p->begin();
   Vector<Complex>::const_iterator iti=ui.begin();
   Real  hn=0, dhn=1, hnp1, dhnp1, s0=*its, sn=(*its)-s0, snp1, ds,
           gcn=(*itc)[0], gcnp1,  //Gauss curvature
           cn=(*itc)[2], cnp1;    //normal curvature
   const Point *pn=&(*itx), *pnp1;
   Real sb, c, c0=0., a23= std::pow(2,-1./3.);
   Real epsf=1, sf=0, sf1=0, sf2=0, df=2*pi_*wfs/k;
   Real st, p3=std::pow(k,1./3.);
   Number nbr=fock.nbr;
   std::vector<Complex> xi(nbr), ai(nbr), psi(nbr);
   for(Number m=0;m<nbr;m++)
   {
     xi[m]=std::conj(fock.poles[m]);
     ai[m]=2*i_*sqrtOfpi_/std::conj(fock.vpoles[m]);
     psi[m]=0.;
   }

   //compute the spread factors and detect shadow focal point if exists
   Vector<Real> spread(nbf,1.);
   Vector<Real>::iterator itsp=spread.begin();
   while(its!=--cas_p->end())
   {
     ++its; ++itsp, ++itc;
     gcnp1=(*itc)[0]; snp1=*its-s0; ds=snp1-sn;
     hnp1=hn+ds*dhn; dhnp1=dhn-ds*gcn*hn;     //forward Euler scheme
     if(sf==0. && dhnp1<-theTolerance)        //shadow focal detection (sign change)
        sf = (snp1*dhn-sn*dhnp1)/(dhn-dhnp1); //linear inter/extra-polation
     if(spreadmax > 1. && std::abs(dhnp1)<1./(spreadmax*spreadmax)) *itsp=spreadmax;  //use spreadmax limiter
     else *itsp = std::sqrt(std::abs(1./dhnp1));
     gcn=gcnp1;hn=hnp1;dhn=dhnp1;sn=snp1;
   }
   if(wfs>0 && sf>0) {sf1=sf-df;sf2=sf+df;}  //focal zone

   Complex vi=*iti, zc(0.,0.), a;
   Number n=0, n1=0, n2=0, nf=0;
   Complex asf=std::conj(fock.vpoles[0]), bsf=asf*xi[0]*p3*a23;
   *itf=std::conj(fock(0))*vi;
   *itt=_terminator;
   its=cas_p->begin(); itsp=spread.begin(); itc=cus_p->begin();
   sn=*its-s0; cn=(*itc)[2];

   while(its!=--cas_p->end())
   {
     ++itx; ++itc; ++its; ++itf; ++itt;++itsp, n++;
     snp1=*its-s0;
     pnp1=&(*itx);
     cnp1=(*itc)[2];  //normal curvature
     sb=std::pow(0.5*k*cnp1*cnp1,1./3.)*snp1;
     if(sb<sb2)       //compute shadow Fock
        {*itt=_shadowFock; *itf=std::conj(fock(sb))*std::exp(i_*k*snp1)*vi**itsp;}
     else if(snp1<sc) // compute creeping wave
     {
       *itt=_creeping;
       if(c0==0.) c0=cn;
       for(Number m=0;m<nbr;m++)
       {
         psi[m]+=0.5*a23*xi[m]*norm(*pnp1-*pn)*(std::pow(cn,2./3.)+std::pow(cnp1,2./3.));  // 2^(-1/3) xi int_s2^s c(s)^(2/3)
         *itf+=std::exp(i_*(k*snp1+p3*psi[m]+sb2*xi[m]))*std::pow(cnp1/c0,1./6.)*ai[m]*vi**itsp;
       }
       if(wfs>0)     // use focal correction
       {
         st=sf-snp1;
         if(snp1>=sf1 && snp1<sf) //in first focal zone
         {
           if(n1==0) //enter in focal zone
           {
             n1=n;
             a=(*itf)/(asf*besselJ0(k*st)-bsf*std::pow(cnp1,2./3)*st*besselJ1(k*st));
           }
           else (*itf)=a*(asf*besselJ0(k*st)-bsf*std::pow(cnp1,2./3)*st*besselJ1(k*st));
         }
         else if(snp1>=sf && snp1<sf2) //in second focal zone
         {
           if(nf==0) nf=n;
           st*=-1;
           (*itf)=asf*besselJ0(k*st)-bsf*std::pow(cnp1,2./3)*st*besselJ1(k*st); //will be modified after
         }
         else if(snp1>sf)
         {
           if(n2==0) n2=n;      // first out
           *itf*=-i_;           // out focal zone, change phase
         }
         //theCout<<"n="<<n<<" n1="<<n1<<" nf="<<nf<<" n2="<<n2<<" s="<<snp1<<" a="<<a<<" u="<<(*itf)<<eol;
       }
       else if(sf>0 && snp1>sf) *itf*=-i_;   // change phase when crossing focal point in any case
     }
     cn=cnp1;sn=snp1;pn=pnp1;
   }

   // update field in focal zone if required
   if(sf>0)
   {
     a*=(*field_p)[n2]/(*field_p)[n1];
     for(Number n=nf;n<n2;n++) (*field_p)[n]*=a;
   }

   //save to file if requested
   std::ofstream os;
   bool svt=na!="";
   if(svt)
   {
      os.open(na);
      itf=field_p->begin();
      for(its=cas_p->begin(); its!=cas_p->end();++its, ++itf)
         os<<(*its)<<" "<<itf->real()<<" "<<itf->imag()<<eol;
      os.close();
   }

   if(sym) //symmetric field if requested
   {
     std::map<Real,Complex> field;
     std::ofstream oss;
     if(svt) {oss.open("sym_"+na); os.open("full_"+na);}
     itf=field_p->begin();
     for(its=cas_p->begin();its!=cas_p->end();++its, ++itf)
       field[*its]=*itf;
     std::map<Real,Complex>::iterator itmr, itmb=field.begin();
     Real t,t1,t2;
     Complex v1,v2, vs;
     if(sf==0) sf=cas_p->back()/2;
     itf=field_p->begin();
     for(its=cas_p->begin();its!=cas_p->end();++its, ++itf)
     {
       sn=*its;
       t=2*sf-sn;
       itmr=field.lower_bound(t);
       t2=itmr->first; v2=itmr->second;
       if(itmr!=itmb)
       {
        itmr--; t1=itmr->first; v1=itmr->second;
        if(t1<=sf && t2>sf) vs=v1;  //crossing sf
        else vs=((t2-t)*v1+(t-t1)*v2)/(t2-t1);
       }
       else vs=v2;
       *itf+=vs;
       if(svt)
       {
         oss<<sn<<" "<<vs.real()<<" "<<vs.imag()<<eol;
         os<<sn<<" "<<itf->real()<<" "<<itf->imag()<<eol;
       }
       //theCout<<"sn="<<sn<<" t="<<t<<" t1="<<t1<<" t2="<<t2<<" v1="<<v1<<" v2="<<v2<<" vs="<<vs<<eol;
     }
     if(svt) {oss.close();os.close();}
   }
}

/*compute field in light zone using UTD, du/dn=0 (Fock+OP)
   requires a Parametrized geodesic with curAbcs computed, tangent computed
   if onMesh true, use MeshedGeodesic related to
   k   : wave number
   ui  : incident field on geodesic points
   inc : incidence direction
   sb1 : light fock zone limit (Fock coordinate, sb1<0) Not used
   dsk : width of transition area Fock-Kirchoff : s1-ds < s < s1+ds (Fock coordinate) Not used!
*/
void computeLightField(const Geodesic& geod, const Vector<Complex>& ui, const Fock& fock,
                        Real k, Real sb1, Real dsk, const String& na="", bool onMesh=true)
{
   const std::vector<Point>* xs_p=&geod.xs_;
   const std::vector<Vector<Real> >* cus_p=&geod.curvatures_;
   const std::vector<Real>* cas_p=&geod.curAbcs_;
   Vector<Complex>* field_p = const_cast<Vector<Complex>*>(&geod.field_);
   std::vector<FieldType>* fieldTypes_p = const_cast<std::vector<FieldType>*>(&geod.fieldTypes_);
   if(onMesh)
   {
     if(geod.meshgeod_==nullptr) error("free_error","computeLightField on mesh requires the mesh projection geodesic, call buildMeshedGeodesic before");
     xs_p=&geod.meshgeod_->xs_;
     cas_p=&geod.meshgeod_->curAbcs_;
     cus_p=&geod.meshgeod_->curvatures_;
     field_p = const_cast<Vector<Complex>*>(&geod.meshgeod_->field_);
     fieldTypes_p = const_cast<std::vector<FieldType>*>(&geod.meshgeod_->fieldTypes_);
   }
   field_p->clear(); fieldTypes_p->clear();
   Number n=cas_p->size();
   field_p->resize(n,Complex(0.)); fieldTypes_p->resize(n,_noFieldType);
   Vector<Complex>::iterator itf=field_p->begin();
   std::vector<FieldType>::iterator itt=fieldTypes_p->begin();
   std::vector<Point>::const_iterator itx=xs_p->begin();
   Vector<Complex>::const_iterator iti=ui.begin();
   std::vector<Real>::const_iterator its=cas_p->begin();
   std::vector<Vector<Real> >::const_iterator itc=cus_p->begin();
   Real  s0=*its, s, sb, c;    //normal curvature
   std::ofstream os;
   bool save=(na!="");
   if(save) os.open(na);
   n=0;
   while(its!=cas_p->end())  //main loop on geodesic points
   {
     s=s0-*its;   //reverse s (<0)
     c=(*itc)[2]; //normal curvature
     sb=std::pow(0.5*k*c*c,1./3.)*s;
     if(sb>sb1) {*itt=_lightFock; *itf= std::conj(fock(sb))*std::exp(i_*k*c*c*s*s*s/6)**iti;} // Fock light
     else {*itt=_lightOP; *itf=2**iti;}  //kirchoff app
     if(save) os<<s<<" "<<itf->real()<<" "<<itf->imag()<<eol;
     ++itc; ++its; ++itf; ++iti; ++itt; n++;
   }
   if(save) os.close();
}

class dataPointGeod
{
 public :
 MeshedGeodesic* geod; // geodesic
 Number index;       // index of point in Geodesic
 Point* pt;            // Geodesic point
 Complex val;        // field value
 dataPointGeod(MeshedGeodesic* g, Number i, Point* p, const Complex& v)
  :geod(g), index(i), pt(p), val(v) {}
};
std::ostream& operator<<(std::ostream& out, const dataPointGeod& g)
{
  out<<g.geod<<" i="<<g.index;
  if(g.pt!=nullptr) out<<" p="<<(*g.pt);
  out<<" val="<<g.val;
  return out;
}

//tool for geodesic interpolation
Complex average(const Point& M, const std::list<dataPointGeod>& dsg)
{
  Complex num=0; Real den=0, d;
  for(auto itl=dsg.begin();itl!=dsg.end();++itl)
  {
    d=norm(M-*itl->pt);
    if(d>theTolerance) {num+=itl->val/d;den+=1/d;}
    else return itl->val;  // one geodesic crosses M, return it
  }
  if(den!=0) num/=den;
  return num;  // return average
}

/*! interpolate geodesic field ond dofs of a GeomDomain; produce a TermVector defined with unknown u on domain dom
    use GeomElement::userdata_p that contains the list of geodesics crossing the element
    and an index related to the geodesic allowing to recovery point and field value
    In order to be fast, the algorithm is based on a local interpolation :
       to average field we use the inverse distance weight mean : v = sum vi/di /sum 1/di (gives the standard mean if equidistant)
    For each dof
      - locate all GeomElements involving dof
      - analyse geodesic contributions, several cases are possible
         - only light contribution and creeping contributions :
             - proccess to an interpolation on light contribution  using inverse distance weight mean
             - add creeping contribution using inverse distance weight mean for each family of geodesics travelling in the "same" direction
         - only shadow fock and and creeping contributions : for each family of geodesics travelling in the "same" direction
             - proccess to an interpolation on shadow fock contribution using inverse distance weight mean
             - add creeping contribution using inverse distance weight mean
         - light and shadow fock contribution (means close to the terminator)
             - do interpolation
         - if no geodesics found, look after GeomElement color (see buildShadowLight3D)
             - if light zone far from terminator : set OP value
             - if light zone close to terminator : set approximation of Fock light based on "distance" to the terminator
             - if shadow zone far from terminator : set 0 (full shadow)
             - if shadow zone close to terminator : set approximation of Fock shadow based on "distance" to the terminator
 support parallel computing
*/
TermVector interpolatedGeodesicField(const GeomDomain& dom, const Unknown& u, const TermVector& uinc)
{
   Complex zc=0.;
   TermVector igf(u, dom, zc, _name="IGF");  //allocate Interpolated Geodesic Field TermVector (IGF)
   const MeshDomain* mdom=dom.meshDomain();
   const std::map<Point,std::list<GeomElement*> >& vertexElts = mdom->vertexElements;
   std::map<Point,std::list<GeomElement*> >::const_iterator itme=vertexElts.end();
   Number nbdof=igf.nbDofs(), npdiv10=nbdof/(10*numberOfThreads());

  #pragma omp parallel for
  for(Number n=0;n<nbdof; n++)  //loop on dofs
  {
     const Point& M=igf.coords(n+1);
     Complex vM=0.;
     //thePrintStream<<" deal with "<<n<<" M="<<M<<eol<<std::flush;
     std::map<Point,std::list<GeomElement*> >::const_iterator itm = vertexElts.find(M);
     std::map<Point, std::list<dataPointGeod> > lights, shadows, creepings; //to list geodesic values by direction and type
     std::map<Point, std::list<dataPointGeod> >::iterator itmd;
     std::list<dataPointGeod>::iterator itl;
     std::list<GeomElement*>::const_iterator itg;
     if(itm!=itme)
     {//collect geodesic values
       Real color=0; Number nbelt=0; Real mes=0., mm=0.;
       for(itg=itm->second.begin();itg!=itm->second.end();++itg) //loop on GeomElements having M as vertex
       {
         GeomElement* gelt=*itg;
         Complex v=0.; Number nv=0;
         for(Number nt=0; nt<gelt->userData_ps.size();nt++)
         {
          if(gelt->userData_ps[nt]!=0)
          {
            listGeodNum* geodata = reinterpret_cast<listGeodNum*>(gelt->userData_ps[nt]);
            for(auto itgd=geodata->begin();itgd!=geodata->end();++itgd) // loop on geodesics crossing GeomElement
            {
               MeshedGeodesic* gd=itgd->first;  //gd should not be zero !
               Number k=itgd->second;
               FieldType ft=gd->fieldTypes_[k];
               Point* p=&gd->xs_[k], *dir=&gd->dxs_[k];  //geodesic point and direction
               v+=gd->field_[k]; nv+=1;
            }
          }
         }
         if(nv!=0) //do not take into account, non visited gelt
         {
           v/=nv; //average on gelt
           mm=gelt->measure();
           vM+=v*mm;
           mes+=mm;
        }
         nbelt++; if((*itg)->color>0) color+=1;
       }
       if(mes!=0 && vM!=zc) igf.setValue(n+1,vM/=mes);
       else if(Number(color)==nbelt) igf.setValue(n+1,2*uinc.getValue(n+1).asComplex());
//       theCout<<" vM="<<vM<<" vc="<<igf.getValue(n+1).asComplex()<<" color="<<color;
//       theCout<<eol;
     }
     if(currentThread()==0 && n!=0 && n % npdiv10 == 0)  //progress status
     {std::cout<< n/npdiv10 <<"0% "<<std::flush; }
   }
   return igf;
}

//TermVector interpolatedGeodesicField(const GeomDomain& dom, const Unknown& u, const TermVector& uinc)
//{
//   Complex zc=0.;
//   TermVector igf(u, dom, zc, _name="IGF");  //allocate Interpolated Geodesic Field TermVector (IGF)
//   const MeshDomain* mdom=dom.meshDomain();
//   const std::map<Point,std::list<GeomElement*> >& vertexElts = mdom->vertexElements;
//   std::map<Point,std::list<GeomElement*> >::const_iterator itme=vertexElts.end();
//   Number nbdof=igf.nbDofs(), npdiv10=nbdof/(10*numberOfThreads());
//
//  #pragma omp parallel for
//  for(Number n=0;n<nbdof; n++)  //loop on dofs
//   {
//     const Point& M=igf.coords(n+1);
//     //thePrintStream<<" deal with "<<n<<" M="<<M<<eol<<std::flush;
//     std::map<Point,std::list<GeomElement*> >::const_iterator itm = vertexElts.find(M);
//     std::map<Point, std::list<dataPointGeod> > lights, shadows, creepings; //to list geodesic values by direction and type
//     std::map<Point, std::list<dataPointGeod> >::iterator itmd;
//     std::list<dataPointGeod>::iterator itl;
//     std::list<GeomElement*>::const_iterator itg;
//     if(itm!=itme)
//     {
//       //collect geodesic values
//       Real color=0; Number nbelt=0;
//       for(itg=itm->second.begin();itg!=itm->second.end();++itg) //loop on GeomElements having M as vertex
//       {
//         GeomElement* gelt=*itg;
//         for(Number nt=0; nt<gelt->userData_ps.size();nt++)
//         {
//          if(gelt->userData_ps[nt]!=0)
//           {
//            listGeodNum* geodata = reinterpret_cast<listGeodNum*>(gelt->userData_ps[nt]);
//            Real dmax=theRealMax, d;
//            for(auto itgd=geodata->begin();itgd!=geodata->end();++itgd) // loop on geodesics crossing GeomElement
//            {
//               MeshedGeodesic* gd=itgd->first;  //gd should not be zero !
//               Number k=itgd->second;
//               FieldType ft=gd->fieldTypes_[k];
//               Point* p=&gd->xs_[k], *dir=&gd->dxs_[k];  //geodesic point and direction
//               Complex* v=&gd->field_[k];
//               d=norm(M-*p);
//               if(d<dmax)
//               {
//                if(ft==_lightFock || ft==_lightOP)
//                {
////                 for(itmd=lights.begin();itmd!=lights.end();++itmd)
////                   if(dot(itmd->first,*dir)>0) break; // angle between current geodesic family and new geodesic < pi/10
////                 if(itmd==lights.end()) lights[*dir]=std::list<dataPointGeod>(1,dataPointGeod(gd,k,p,*v));
////                 else itmd->second.push_back(dataPointGeod(gd,k,p,*v));
//                 if(lights.size()==0) lights[*dir]=std::list<dataPointGeod>(1,dataPointGeod(gd,k,p,*v));
//                 else
//                 {
//                   if(dmax==theRealMax) lights.begin()->second.push_back(dataPointGeod(gd,k,p,*v));
//                   else *lights.begin()->second.begin()=dataPointGeod(gd,k,p,*v);
//                 }
//                }
//                else if(ft==_shadowFock||_terminator)
//                {
////                 for(itmd=shadows.begin();itmd!=shadows.end();++itmd)
////                   if(dot(itmd->first,*dir)>0.95) break; // angle between current geodesic family and new geodesic < pi/10
////                 if(itmd==shadows.end()) shadows[*dir]=std::list<dataPointGeod>(1,dataPointGeod(gd,k,p,*v));
////                 else itmd->second.push_back(dataPointGeod(gd,k,p,*v));
//                 if(shadows.size()==0) shadows[*dir]=std::list<dataPointGeod>(1,dataPointGeod(gd,k,p,*v));
//                 else
//                 {
//                    if(dmax==theRealMax) shadows.begin()->second.push_back(dataPointGeod(gd,k,p,*v));
//                    else *shadows.begin()->second.begin()=dataPointGeod(gd,k,p,*v);
//                 }
//                }
//               else if (ft==_creeping)
//                {
//                 for(itmd=creepings.begin();itmd!=creepings.end();++itmd)
//                    if(dot(itmd->first,*dir)>0) break; // angle between current geodesic family and new geodesic < pi/10
//                   //if(dot(itmd->first,*dir)>0.99*norm(itmd->first)*norm(*dir)) break; // angle between current geodesic family and new geodesic < pi/10
//                 if(itmd==creepings.end()) creepings[*dir]=std::list<dataPointGeod>(1,dataPointGeod(gd,k,p,*v));
//                 else
//                 {
//                   if(dmax==theRealMax) itmd->second.push_back(dataPointGeod(gd,k,p,*v));
//                   else *creepings.begin()->second.begin()=dataPointGeod(gd,k,p,*v);
//                 }
//                }
//                dmax=d;
//               }
//            }
//           }
//         }
//         nbelt++; if((*itg)->color>0) color+=1;
//       }
//     }
//     bool hasLight=lights.size()>0, hasShadows=shadows.size()>0, hasCreepings=creepings.size()>0;
//     Complex num, vM; Real den, d; bool numden;
//     //averaging geodesics considered in "same direction"
//     if(hasLight)
//       for(itmd=lights.begin();itmd!=lights.end();++itmd)
//       {
//         std::list<dataPointGeod>& dsg=itmd->second;
//         if(dsg.size()>1)
//            dsg.push_front(dataPointGeod(static_cast<MeshedGeodesic*>(0),0,static_cast<Point*>(0),average(M,dsg)));
//       }
//     if(hasShadows)
//       for(itmd=shadows.begin();itmd!=shadows.end();++itmd)
//       {
//         std::list<dataPointGeod>& dsg=itmd->second;
//         if(dsg.size()>1)
//            dsg.push_front(dataPointGeod(static_cast<MeshedGeodesic*>(0),0,static_cast<Point*>(0),average(M,dsg)));
//       }
//     if(hasCreepings)
//       for(itmd=creepings.begin();itmd!=creepings.end();++itmd)
//       {
//         std::list<dataPointGeod>& dsg=itmd->second;
//         if(dsg.size()>1)
//           dsg.push_front(dataPointGeod(static_cast<MeshedGeodesic*>(0),0,static_cast<Point*>(0),average(M,dsg)));
//       }
//     //add each contribution
//     //theCout<<"dof "<<n+1<<" M= "<<M;
//     vM=0.;
//     if(hasLight)
//     {
//       for(itmd=lights.begin();itmd!=lights.end();++itmd) vM=itmd->second.begin()->val;
//       //theCout<<", add light : ";
//     }
//     if(hasShadows)
//     {
//       for(itmd=shadows.begin();itmd!=shadows.end();++itmd) vM+=itmd->second.begin()->val;
//       if(hasLight) vM/=2;   //average shadow and light
//       //theCout<<", add shadow : ";
//     }
//     //add creeping (take average for the moment), no addition of creeping contribution
//     if(hasCreepings)
//     {
//      for(itmd=creepings.begin();itmd!=creepings.end();++itmd) vM+=itmd->second.begin()->val;
//      //theCout<<", add creeping : ";
//     }
//     if(vM!=zc) igf.setValue(n+1,vM);
//     //else if(Number(color)==nbelt) igf.setValue(n+1,2*uinc.getValue(n+1).asComplex());
////     theCout<<" vM="<<vM<<" vc="<<igf.getValue(n+1).asComplex()<<" color="<<color;
////     if(lights.size()>0)    theCout<<"\n     lights="<<lights;
////     if(shadows.size()>0)   theCout<<"\n     shadows="<<shadows;
////     if(creepings.size()>0) theCout<<"\n     creepings="<<creepings;
////     theCout<<eol;
//     if(currentThread()==0 && n!=0 && n % npdiv10 == 0)  //progress status
//     { std::cout<< n/npdiv10 <<"0% "<<std::flush; }
//   }
//   return igf;
//}


std::pair<Real,Real> computeErrors(const std::list<Point>& xs, const std::list<Point>& ys)
{
    std::list<Point>::const_iterator itx=xs.begin();
    std::list<Point>::const_iterator ityb=ys.begin(), itye=ys.end(), ity;
    Point p1, p2;
    Real d1=theRealMax, d2=theRealMax, d, esup=0, el2=0, h;
    for(;itx!=xs.end();++itx)
    {
      d1=theRealMax; d2=theRealMax;
      for(ity=ityb;ity!=itye;++ity)
      {
        d=norm(*itx-*ity);
        if(d<d1) {d2=d1;d1=d;p2=p1;p1=*ity;}
        else if(d<d2) {d2=d;p2=*ity;}
      }
      //theCout<<" x="<<(*itx)<<" p1="<<p1<<" p2="<<p2<<" q="<<projectionOnStraightLine(*itx,p1,p2,h)<<" h="<<h<<eol;
      projectionOnStraightLine(*itx,p1,p2,h);
      el2+=h*h;
      esup=std::max(esup,h);
    }
    return std::make_pair(std::sqrt(el2/xs.size()),esup);
}

Complex finc(const Point& p,Parameters& pars=defaultParameters)
{
   Real k=pars("k").get_r();
   Point pin=pars("in").get_pt();
   return exp(i_*k*dot(pin,p));
}

void cleanUserdata(GeomDomain& dom)
{
  std::vector<GeomElement*>::const_iterator itg=dom.meshDomain()->geomElements.begin();
  for(; itg!=dom.meshDomain()->geomElements.end(); ++itg)
  {
    if((*itg)->userData_p!=0)
    {
      delete reinterpret_cast<listGeodNum*>((*itg)->userData_p);
      (*itg)->userData_p=0;
    }
    for(Number n=0;n<(*itg)->userData_ps.size();n++)
    {
      if((*itg)->userData_ps[n]!=0)
        delete reinterpret_cast<listGeodNum*>((*itg)->userData_ps[n]);
      (*itg)->userData_ps[n]=0;
    }
  }
}

void unit_Geodesic(int argc, char* argv[], bool check)
{
  String rootname = "unit_Geodesic";
  trace_p->push(rootname);
  std::srand(1);
  verboseLevel(10);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;
//  Point x,dx;
//  Real l=0.;

//  //test ParametrizedGeodesic
//  Sphere sph(_center=Point(0.,0.,0.),_radius=1, _hsteps=0.1, _domain_name="Sphere");
//  Mesh msph(sph,_triangle);
//  theCout<<msph;
//  Domain dsph=msph.domain("Sphere");
//  theCout<<dsph<<eol<<std::flush;
//  ParametrizedGeodesic gsph(dsph,true,true);
//  x=Point(1.,0.,0.); dx=Point(0.,1.,0.); l=0.;
//  gsph.compute(x,dx,l,3*pi_,200, 0.01);
//  gsph.saveToFile("sph_pargeod1");
//  x=Point(1.,0.,0.);dx=Point(1,0,1);dx/=norm(dx); l=0.;
//  gsph.compute(x,dx,l,3*pi_,200, 0.01);
//  gsph.saveToFile("sph_pargeod2");
//  x=Point(1.,0.,0.);dx=Point(0,0,1); l=0.;
//  gsph.compute(x,dx,l,3*pi_,200, 0.01);
//  gsph.saveToFile("sph_pargeod3");
//  x=Point(1.,0.,0.);dx=Point(1,0,2);dx/=norm(dx); l=0.;
//  gsph.compute(x,dx,l,10, 200, 0.01);
//  gsph.saveToFile("sph_pargeod4");
//
//
//  Ellipsoid ell(_center=Point(0.,0.,0.),_xradius=3, _yradius=2, _zradius=1, _hsteps=0.1, _domain_name="Ellipsoid");
//  Mesh mell(ell,_triangle);
//  theCout<<mell;
//  Domain dell=mell.domain("Ellipsoid");
//  theCout<<dell<<eol<<std::flush;
//  ParametrizedGeodesic gell(dell,true,true);
//  x=Point(3,0,0); dx=Point(0,1,0.); l=0.;
//  gell.compute(x,dx,l,20 ,2000, 0.01);
//  gell.saveToFile("ell_pargeod1");
//  x=Point(3,0,0);dx=Point(1,0,1);dx/=norm(dx); l=0.;
//  gell.compute(x,dx,l,100 ,2000, 0.01);
//  gell.saveToFile("ell_pargeod2");
//  x=Point(3,0,0);dx=Point(0,0,1); l=0.;
//  gell.compute(x,dx,l,20, 2000, 0.01);
//  gell.saveToFile("ell_pargeod3");
//  x=Point(3,0,0.01);dx=Point(0,1,0); l=0.;
//  gell.compute(x,dx,l,100, 2000, 0.01);
//  gell.saveToFile("ell_pargeod4");

//check intersectionHalfLineEllipse
//  Point C(0.,0.,0.), A(1.,0.,0.), B(0.,1.,0.),M(2.,0.,0.), D(-1,0,0);
//  for(Real y=0.;y<=1.;y+=0.1)
//  {
//    D[1]=y;
//    for(Real t0=0;t0<2*pi_;t0+=pi_/2)
//      theCout<<"t0="<<t0<<" t1="<<t0+pi_/2<<" D="<<D<<" I="<<intersectionHalfLineEllipse(M,D,C,A,B,t0,t0+pi_/2)<<eol;
//  }
//  M=Point(1.,0.,0.);
//  for(Real y=0.;y<=1.;y+=0.1)
//  {
//    D[1]=y;
//    for(Real t0=0;t0<2*pi_;t0+=pi_/2)
//      theCout<<"t0="<<t0<<" t1="<<t0+pi_/2<<" D="<<D<<" I="<<intersectionHalfLineEllipse(M,D,C,A,B,t0,t0+pi_/2)<<eol;
//  }
//
//  M=Point(0.5,0.5,0.);
//  for(Real y=0.;y<=1.;y+=0.1)
//  {
//    D[1]=y;
//    for(Real t0=0;t0<2*pi_;t0+=pi_/2)
//      theCout<<"t0="<<t0<<" t1="<<t0+pi_/2<<" D="<<D<<" I="<<intersectionHalfLineEllipse(M,D,C,A,B,t0,t0+pi_/2)<<eol;
//  }

//  Cylinder cyl(_basis=Disk(_center=Point(0.,0.,0.),_radius=1.,_nnodes=40),_direction=Point(0.,0.,2.),_nnodes=Numbers(40,40,40),_domain_name="Cylinder");
//  Mesh mc(cyl,_triangle);
//  theCout<<mc;
//  Domain dc=mc.domain("Cylinder");
//  //theCout<<dc<<eol<<std::flush;
//  Space Vc(dc,_P1,"Vc"); Unknown uc(Vc, _name="uc");
//  saveToFile("cyl_1.m",TermVector(uc, dc, 1., _name="uc"),_format=_matlab);
  //theCout<<dcyl.boundaryParametrization()<<eol<<std::flush;

//  RevCone co(_center=Point(0,0,0),_radius=1.,_apex=Point(0.,0.,-2.),_nnodes=40,_domain_name="Cone");
//  Mesh mc(co, _shape=_triangle, _order=1, _generator=gmsh);
//  theCout<<mc;
//  Domain dc=mc.domain("Cone");
//  theCout<<dc<<eol<<std::flush;
//  Space V(dc,_P1,"V"); Unknown u(V, _name="u");
//  saveToFile("co_1.m",TermVector(u, dc, 1., _name="u"),_format=_matlab);

//  Point x,dx; Real l;
//
//  ParametrizedGeodesic pgcyl(dc,true,true);
//  x=Point(1,0,0.1); dx=Point(0,1,0.2);l=0;
//  pgcyl.compute(x,dx,l, 12 ,2000, 0.1);
//  pgcyl.saveToFile("cyl_pg1");
//
//  MeshedGeodesic mgcyl(dc,true,true);
//  x=Point(1,0.0,0.1); dx=Point(0,1,0.2);l=0;
//  mgcyl.compute(x,dx,l, 12 ,2000, 0.1);
//  mgcyl.saveToFile("cyl_mg1");
//
//  Geometry& bocyl=*dc.geometry()->boundary().components().at(8);
//  theCout<<bocyl;
//  AnalyticGeodesic agcyl(bocyl);
//  x=Point(1,0.0,0.1); dx=Point(0,1,0.2);l=0;
//  agcyl.compute(x,dx,l, 12 ,2000, 0.1);
//  agcyl.saveToFile("cyl_ag1");
//
//  x=Point(1,0.0,0.1); dx=Point(0,1,0.2);l=0;
//  CompositeGeodesic cgcyl(dc.geometry()->boundary());
//  cgcyl.compute(x,dx,l, 12 ,2000, 0.02);
//  cgcyl.toGlobal();
//  theCout<<eol<<cgcyl<<eol;
//  cgcyl.saveToFile("cyl_cg1");

//  //compute errors
//  std::ofstream fer("errors.dat");
//  std::ofstream ferm("errorm.dat");
//  Number n0=10, nf=50, dnm=2;
//  Real t0=0, tf=pi_/4, dt=pi_/40;
//  Real px0=1, pxf=5, dpx=0.5;
//  std::list<std::list<Point> > cgxs;
//  std::list<std::list<Point> >::iterator itcg;
//  for(Number nm=n0;nm<=nf;nm+=dnm)
//  {
//    Cylinder cylnm(_basis=Disk(_center=Point(0.,0.,0.),_radius=1.,_nnodes=nm),_direction=Point(0.,0.,2.),_nnodes=Numbers(nm,nm,std::floor(1.273*nm)),_domain_name="Cylinder");
//    Mesh mcylnm(cylnm,_triangle);
//    Domain dcylnm=mcylnm.domain("Cylinder");
//    MeshedGeodesic mgcylnm(dcylnm,false,false);
//    Real h=pi_/(2*nm);
//    theCout<<"h = "<<h<<" (n="<<nm<<")"<<eol;
//    Real er2=0.; Number k=0;
//    itcg=cgxs.begin();
//    for(Real t=t0;t<=tf;t+=dt)
//    {
//     for(Real px=px0;px<=pxf;px+=dpx,k++,itcg++)
//     {
//      x=Point(cos(t),sin(t),0.1); dx=Point(-sin(t),cos(t),px);l=0;
//      theCout<<"compute geodesic n="<<nm<<" x="<<x<<" dx="<<dx<<eol<<std::flush;
//      if(nm==n0)
//      {
//        cgcyl.compute(x,dx,l, 12 ,2000, 0.02);
//        cgcyl.toGlobal();
//        cgcyl.saveToFile("cyl_cg_x="+tostring(t)+"_px="+tostring(px));
//        cgxs.push_back(cgcyl.xs_);
//        itcg=--cgxs.end();
//        x=Point(cos(t),sin(t),0.1); dx=Point(-sin(t),cos(t),px);l=0;
//      }
//      mgcylnm.compute(x,dx,l, 12 ,2000, 0.01);
//      std::pair<Real,Real> ersnm=computeErrors(mgcylnm.xs_,*itcg);
//      theCout<<"  t="<<t<<" px="<<px<<" L2 error = "<<ersnm.first<<" Linf error  "<<ersnm.second<<eol;
//      fer<<h<<" "<<ersnm.first<<" "<<ersnm.second<<eol;
//      er2+=ersnm.first;
//      mgcylnm.saveToFile("cyl_mg_x="+tostring(t)+"_px="+tostring(px)+"_m="+tostring(nm));
//    }
//    }
//    er2/=k;
//    ferm<<h<<" "<<er2<<eol;
//  }
//  fer.close();
//  ferm.close();

// test shadow/light curve computation
//  if(mc.sides().size()==0) mc.buildSides();
//  Unknown u3(V, _name="u3", _dim=3);
//  Vector<Real> in(3,0.); in(1)=1;in(3)=1;
//  TermVector inc(u3, dc, in, _name="in");
//  std::list<SegElt>::iterator its;
//  theCout<<inc<<ns<<eol;
//  TermVector ns=normalsOn(dcyl,u3);
//  TermVector incns=tensorInnerProduct(inc,ns);
//  std::list<std::list<SegElt> > slcs;
//  buildShadowLight3DP1(incns,slcs);
//  std::list<std::list<SegElt> >::iterator itc=slcs.begin();
//  Number i=1;
//  for(;itc!=slcs.end();++itc, i++)
//  {
//     std::ofstream os("sl_curve_"+tostring(i)+".dat");
//     for(its=itc->begin();its!=itc->end();++its)
//     {
//        os<<its->P1_(1)<<" "<<its->P1_(2)<<" "<<its->P1_(3)<<eol
//          <<its->P2_(1)<<" "<<its->P2_(2)<<" "<<its->P2_(3)<<eol;
//     }
//     os.close();
//  }

//  in(3)=1;in(2)=1;
//  std::list<SegElt> slc;
//  inc=TermVector(u3, dc, in, _name="in100");
//  buildShadowLight3D(inc,slc,true,"sl_curve",0.01,1);
//  std::ofstream os;
//
//  Point P;Number k=0; Real lmax=2;

//  std::map<Point,MeshedGeodesic> geodesicField_mesh;
//  std::pair<std::map<Point,MeshedGeodesic>::iterator,bool> ins;
//  for(its=slc.begin();its!=slc.end();++its)
//  {
//   for(Number i=0;i<2;i++)
//   {
//     if(i==0) P=its->P1_; else P=its->P2_;
//     if(geodesicField_mesh.find(P)==geodesicField_mesh.end())
//     {
//       x=P;dx=in;l=0;k++;
//       ins=geodesicField_mesh.insert(std::make_pair(P,MeshedGeodesic(dc,false,false)));
//       if(k>=1)
//       {ins.first->second.compute(x,dx,l, 2 ,2000, 0.1);
//        ins.first->second.saveToFile("geod_mesh_"+tostring(k));
//        std::cout<<"meshed geodesic "<<k<<" x="<<x<<" computed"<<" length="<<l<<eol;
//        theCout<<"mesdhed geodesic "<<k<<" x="<<x<<" computed"<<" length="<<l<<eol;
//       }
//     }
//   }
//  }

//  std::map<Point,ParametrizedGeodesic> geodesicField_par;
//  P;k=0;

//  std::pair<std::map<Point,ParametrizedGeodesic>::iterator,bool> insp;
//  for(its=slc.begin();its!=slc.end();++its)
//  {
//   for(Number i=0;i<2;i++)
//   {
//     if(i==0) P=its->P1_; else P=its->P2_;
//     if(geodesicField_par.find(P)==geodesicField_par.end())
//     {
//       x=P;dx=in;l=0;k++;
//       if(k==1)
//       { insp=geodesicField_par.insert(std::make_pair(P,ParametrizedGeodesic(dc,true,true,true)));
//         ParametrizedGeodesic& geod=insp.first->second;
//         geod.compute(x,dx,l, lmax ,2000, 0.01);
//         geod.saveToFile("geod_par_"+tostring(k));
////        geod.buildMeshedGeodesic(dc);
////        insp.first->second.meshgeod_->saveToFile("geod_par_mesh_"+tostring(k));
////        std::cout<<"parametrized geodesic "<<k<<" x="<<x<<" computed"<<" length="<<l<<eol;
////        theCout<<"parametrized geodesic "<<k<<" x="<<x<<" computed"<<" length="<<l<<eol;
////        std::ofstream ft("triangles_"+tostring(k)+".dat");
////        for(Number e=1;e<=dc.numberOfElements();e++)
////        {
////          GeomElement* gelt=dc.element(e);
////          if(gelt->userData_p!=nullptr)
////           ft<<"plot3(["<<gelt->vertex(1)[0]<<" "<< gelt->vertex(2)[0]<<" "<<gelt->vertex(3)[0]<<" "<<gelt->vertex(1)[0]<<"],"
////                   <<"["<<gelt->vertex(1)[1]<<" "<< gelt->vertex(2)[1]<<" "<<gelt->vertex(3)[1]<<" "<<gelt->vertex(1)[1]<<"],"
////                   <<"["<<gelt->vertex(1)[2]<<" "<< gelt->vertex(2)[2]<<" "<<gelt->vertex(3)[2]<<" "<<gelt->vertex(1)[2]<<"],"
////                   <<"'Color',[0.7 0.7 0.7])"<<eol;
////        }
////        ft.close();
//       }
//     }
//   }
//  }

  Sphere sph(_center=Point(0.,0.,0.),_radius=1.,_nnodes=100,_domain_name="Sphere");
  Mesh ms(sph, _shape=_triangle);
  theCout<<ms;
  Domain ds=ms.domain("Sphere");
  theCout<<ds<<eol<<std::flush;
  Space V(_domain=ds,_interpolation=P1,_name="V"); Unknown u(V, _name="u"), u3(V, _name="u3", _dim=3);
  saveToFile("sphere_1.m", TermVector(u, ds, 1., _name="u"), _format=_matlab);
  elapsedTime("mesh",std::cout);
  if(ms.sides().size()==0) ms.buildSides();
  Real ko=50, l=0., lmax=pi_;
  Vector<Real> in(3,0.); in(1)=1;
  std::list<SegElt> slc;
  std::list<SegElt>::iterator its;
  TermVector inc(u3, ds, in, _name="in100");
  buildShadowLight3D(inc,slc,true,"sl_curve",0.005,1);
  //list of terminator starting point
  Number ni=0;  // number of intermediate points
  std::vector<Point> stp((1+ni)*slc.size()+1);
  std::vector<Point>::iterator itp=stp.begin();
  for(its=slc.begin();its!=slc.end();++its)
  {
      Point& P1=its->P1_, &P2=its->P2_;
      *itp++=P1;
      for(Number i=1;i<=ni;i++)  *itp++=((ni+1-i)*P1+i*P2)/(ni+1);
  }
  *itp=slc.back().P2_;
  elapsedTime("buildShadowLight",std::cout);
  std::ofstream fout("terminator.csv");
  fout<<"x,y,z"<<eol;
  for(itp=stp.begin();itp!=stp.end();++itp)
    fout<<itp->x()<<","<<itp->y()<<","<<itp->z()<<eol;
  fout.close();
  theCout<<"terminator starting points ("<<stp.size()<<"):"<<stp<<eol;
  elapsedTime("print terminator",std::cout);
  Number nbt=numberOfThreads();
  cleanUserdata(ds);
  void* vpz=0;
  std::vector<void*> vpzs(nbt,vpz);
  #pragma omp parallel for
  for(Number e=1;e<=ds.numberOfElements();e++)
  {
    GeomElement* gelt= ds.element(e);
    gelt->userData_ps=vpzs;
    for(Number j=0;j<nbt;j++) gelt->userData_ps[j]= static_cast<void *>(new listGeodNum());
    gelt->normalVector();
  }
  ds.meshDomain()->buildKdTree();
  ds.geometry()->boundaryParametrization();
  Point pin(in), pinm=-pin;
  Fock fock(0,10,200);
  //fock.createTable(-5,5,2500); //Fock table
  Number npdiv10=stp.size()/(10*nbt);
  std::vector<std::list<ParametrizedGeodesic> > geodesics(nbt);
  elapsedTime("initializing geodesic computation",std::cout);

  #pragma omp parallel for
  for(Number ng=0; ng<stp.size(); ng++)
  {
    Number nt=currentThread();
    //thePrintStream<<"ng="<<ng<<" nt="<<nt<<std::flush;
    Point& x=stp[ng]; Real l=0;
    geodesics[nt].push_back(ParametrizedGeodesic(ds,true,true,true));
    ParametrizedGeodesic* geod= &geodesics[nt].back();
    geod->compute(x,pin,l,lmax,5000,0.01);
    //thePrintStream<<" shadow geodesic geod="<<geod<<" l="<<l<<std::flush;
    geod->buildMeshedGeodesic(ds);
//    //if(ng<10) geod->meshgeod_->saveToFile("shadow_meshgeod_"+tostring(ng)+".csv");
//    //thePrintStream<<" meshed shadow geodesic meshgeod="<<geod->meshgeod_<<" l="<<geod->meshgeod_->length_<<std::flush;
    std::vector<Point>* xs=&geod->meshgeod_->xs_;
    Vector<Complex> gui(xs->size());
    Vector<Complex>::iterator iti=gui.begin();
    for(auto itx=xs->begin();itx!=xs->end();++itx,++iti) *iti=std::exp(i_*ko*dot(pin,*itx));
    String na="";
    //if(ng<10) na="shadow_field_"+tostring(ng)+".dat";
    computeShadowField(*geod,gui,fock,ko,1.5,pi_,0,1.75,true,na,true);
    //thePrintStream<<" shadow field"<<std::flush;
    l=0;
    geodesics[nt].push_back(ParametrizedGeodesic(ds,true,true,true));
    geod= &geodesics[nt].back();
    geod->compute(x,pinm,l,lmax,5000,0.01);
    //thePrintStream<<" light geodesic geod="<<geod<<" l="<<l<<std::flush;
    geod->buildMeshedGeodesic(ds);
    //if(ng<10) geod->meshgeod_->saveToFile("light_meshgeod_"+tostring(ng)+".csv");
    //thePrintStream<<" meshed light geodesic meshgeod="<<geod->meshgeod_<<" l="<<geod->meshgeod_->length_<<std::flush;
    xs=&geod->meshgeod_->xs_;
    gui.resize(xs->size());
    iti=gui.begin();
    for(auto itx=xs->begin();itx!=xs->end();++itx,++iti) *iti=std::exp(i_*ko*dot(pin,*itx));
    na="";
    //if(ng<10) na="light_field_"+tostring(ng)+".dat";
    computeLightField(*geod,gui,fock,ko,-2.5,0,na,true);
//    thePrintStream<<" light field"<<std::flush;
//    thePrintStream<<eol<<std::flush;
    if(currentThread()==0 && ng!=0 && ng % npdiv10 == 0)  //progress status
    { std::cout<< ng/npdiv10 <<"0% "<<std::flush; }
  }
  elapsedTime("geodesics and field",std::cout);

  //interolate geodesic fields
  Parameters pars(ko,"k");pars<<Parameter(Point(in),"in");
  Function Finc(finc,pars);
  TermVector ui(u, ds, Finc);
  std::cout<<"interpolating"<<eol;
  TermVector uf=interpolatedGeodesicField(ds, u, ui);
  elapsedTime("interpolating",std::cout);
  saveToFile("uf",uf,_format=_vtu);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
