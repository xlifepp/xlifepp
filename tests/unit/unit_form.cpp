/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_form.cpp
	\author E. Lunéville
	\since 23 feb 2012
	\date 30 octobre 2013

	Low level tests of Operator class and related classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>

using namespace xlifepp;

namespace unit_form {

//! scalar spectral function
Real Sn(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Int n = pa("basis index") -1;        // get the index of function to compute
  return sqrt(2. / h) * sin(n * pi_ * x / h); // computation
}

//! vector spectral function
Vector<Real> SCn(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Int n = pa("basis index")-1;       // get the index of function to compute
  Vector<Real> res(2);
  res(1) = sqrt(2. / h) * sin(n * pi_ * x / h); // computation
  res(2) = sqrt(2. / h) * cos(n * pi_ * x / h);
  return res;
}
Real f(const Point& M, Parameters& pa = defaultParameters)
{
  return norm(M);    // computation
}

Real Green(const Point& M, const Point& P, Parameters& pa = defaultParameters)
{
  Real r = M.distance(P);
  return log(r);    // computation
}
Real alpha(const Point& M, Parameters& pa = defaultParameters)
{
  return 2.;    // computation
}

//compute elementary matrix grad.grad in 2D-P1
void bfGradGrad(BFComputationData& bfd)
{
  if(bfd.matel().size()==0) bfd.matel()= Matrix<Real>(3,3);
  const Element* elt=bfd.elt_u;
  if(elt==0) return;//no computation, to get valuetype
  GeomMapData& mapdata = *elt->geomElt_p->meshElement()->geomMapData_p;
  Matrix<Real> C=(0.5*mapdata.differentialElement)*mapdata.inverseJacobianMatrix*tran(mapdata.inverseJacobianMatrix);
  Matrix<Real> G(2,3,0.);G(1,1)=1;G(2,2)=1;G(1,3)=-1;G(2,3)=-1;
  bfd.matel()=tran(G)*C*G;
}

void unit_form(int argc, char* argv[], bool check)
{
  String rootname = "unit_form";
  trace_p->push(rootname);
  std::stringstream out;                  // string stream receiving results
  out.precision(testPrec);
  String errors;
  Number nbErrors = 0;
  Real eps=0.005;

  verboseLevel(2);
  Rectangle r(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=11,_domain_name="Omega", _side_names=Strings("y=0","x=1","y=1","x=0"));
  Mesh mesh2d(r, _shape=_triangle, _order=1, _generator=_structured);
  Domain Omega=mesh2d.domain("Omega");

  Parameters ps;
  ps<<Parameter(1.,"h")<<Parameter(1,"basis index");
  Function Basis(Sn, "Sn", ps);
  Function VecBasis(SCn, "VecBasis", ps);
  Function G(Green, "green function");

  Space V(_domain=Omega, _basis=Basis, _dim=10, _name="sin basis");
  Unknown u(V, _name="u");

  Space W(_domain=Omega, _basis=VecBasis, _dim=10, _basis_dim=2, _name="(sin,cos) basis");
  Unknown w(W, _name="w");

  //! test of linear form
  LinearForm lf = intg(Omega, grad(u));
  theCout << "test copy constructor, lf=intg(Omega,grad(u)) : " << lf;
  nbErrors+=checkValue(theCout, rootname+"/lfgradu.in", errors, "lf = intg(Omega, grad(u))", check);
  theCout << "test intg(Omega,Sn*u) : " << intg(Omega, Basis * u);
  nbErrors+=checkValue(theCout, rootname+"/Snu.in", errors, " intg(Omega, Sn*u)", check);
  lf = intg(Omega, div(w) * Basis);
  theCout << "test operator =, lf=intg(Omega,div(w)*Sn); : " << lf;
  nbErrors+=checkValue(theCout, rootname+"/divwSn.in", errors, "intg(Omega, div(w)*Sn)", check);
  theCout << "test intg(Omega,u) : " << intg(Omega, u);
  nbErrors+=checkValue(theCout, rootname+"/u.in", errors, " intg(Omega, u)", check);
  theCout << "test intg(Omega,Sn*(_n*div(w))*SCn) : " << intg(Omega, Basis * (((_n * div(w))|VecBasis)));
  nbErrors+=checkValue(theCout, rootname+"/Sn_ndivwSCn.in", errors, "intg(Omega,Sn*(_n*div(w))*SCn)", check);
  theCout << "test intg(Omega,Omega,f*(grad(u)|_n)) : " << intg(Omega, f * (grad(u) | _n));
  nbErrors+=checkValue(theCout, rootname+"/fgradun.in", errors, "intg(Omega,Omega,f*(grad(u)|_n)", check);
  theCout << "test intg(Omega,w[1]) : " << intg(Omega, w[1]);
  nbErrors+=checkValue(theCout, rootname+"/w_1.in", errors, "intg(Omega,w[1])", check);
  theCout << "test 2*intg(Omega,u) : " << 2*intg(Omega, u);
  nbErrors+=checkValue(theCout, rootname+"/intg2u.in", errors, "2*intg(Omega, u)", check);
  theCout << "test pi_*intg(Omega,u) : " << pi_*intg(Omega, u);
  nbErrors+=checkValue(theCout, rootname+"/pi_u.in", errors, "pi_*intg(Omega, u)", check);
  theCout << "test i_*intg(Omega,u) : " << i_*intg(Omega, u);
  nbErrors+=checkValue(theCout, rootname+"/iu.in", errors, "i_*intg(Omega, u)", check);
  theCout << "test Complex(3,0)*intg(Omega,u) : " << Complex(3,0)*intg(Omega, u);
  nbErrors+=checkValue(theCout, rootname+"/c3u.in", errors, "Complex(3,0)*intg(Omega, u)", check);
  theCout << "test Complex(0,3)*intg(Omega,u) : " << Complex(0,3)*intg(Omega, u);
  nbErrors+=checkValue(theCout, rootname+"/c3iu.in", errors, "Complex(0,3)*intg(Omega, u)", check);

  //! test of explicit linear combination
  lf = intg(Omega, u);
  theCout << "test intg(Omega, u) : " << lf;
  nbErrors+=checkValue(theCout, rootname+"/lfu.in", errors, "intg(Omega, u)", check);
  LinearForm lfu = intg(Omega, VecBasis | grad(u));
  theCout << "test intg(Omega,SCn|grad(u)) : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/intgSCngradu.in", errors, "intg(Omega,SCn|grad(u))", check);

  //! test of operation on linear combination
  lfu = 2 * intg(Omega, VecBasis | grad(u));
  theCout << "test 2*intg(Omega,SCn|grad(u)) : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/intg2SCngradu.in", errors, "2*intg(Omega,SCn|grad(u))", check);
  lfu = intg( Omega, f * (grad(u) | _n)) * i_;
  theCout << "test intg(Omega,f*(grad(u)|_n))*i : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/fgradu_ni.in", errors, "intg(Omega,f*(grad(u)|_n))*i", check);
  lfu = intg(Omega, u) / 4;
  theCout << "test intg(u)/4 : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/usur4.in", errors, "intg(u)/4", check);
  lfu = 2 * intg(Omega, VecBasis | grad(u)) + intg(Omega, f * (grad(u) | _n)) * i_;
  theCout << "test 2*intg(Omega,SCn|grad(u))+intg(Omega,f*(grad(u)|_n))*i : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/CLgraduScn_n.in", errors, "2*intg(Omega,SCn|grad(u))+intg(Omega,f*(grad(u)|_n))*i", check);
  theCout << "test lfu-intg(Omega,u)/4 : " << (lfu - intg(Omega, u) / 4);
  nbErrors+=checkValue(theCout, rootname+"/lfumlus4.in", errors, "lfu-intg(Omega,u)/4", check);
  theCout << "test 2*intg(Omega,SCn|grad(u))+intg(Omega,f*(grad(u)|_n))*i-i*intg(Omega,u)/4 : "
      << 2 * intg(Omega, VecBasis | grad(u)) + intg(Omega, f * (grad(u) | _n))*i_ - i_* intg(Omega, u) / 4.;
  nbErrors+=checkValue(theCout, rootname+"/gonzo.in", errors, "2*intg(Omega,SCn|grad(u))+intg(Omega,f*(grad(u)|_n))*i-i*intg(Omega,u)/4", check);

  //! test multiple unknowns linear form
  lfu += intg(Omega, w) / 4;
  theCout << "test lfu+=intg(Omega,w)/4 : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/lfupews4.in", errors, "lfu+=intg(Omega,w)/4", check);
  lfu = lfu + intg(Omega, f * (grad(u) | _n)) * i_;
  theCout << "test lfu+intg(Omega,f*(grad(u)|_n))*i : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/lfupfgun.in", errors, "lfu+intg(Omega,f*(grad(u)|_n))*i", check);
  lfu = lfu + 4 * intg(Omega, Basis * div(w)) / i_;
  theCout << "test lfu=lfu+4*intg_(Omega,Omega,Sn*div(w)/i; : " << lfu;
  nbErrors+=checkValue(theCout, rootname+"/lfup4Sndivw.in", errors, "lfu=lfu+4*intg_(Omega,Omega,Sn*div(w)/i", check);
  LinearForm lw = intg(Omega, div(w)) + intg(Omega, i_ * w[1]);
  theCout << "lw=intg(Omega,div(w))+intg(Omega,i*w[1]);: " << lw;
  nbErrors+=checkValue(theCout, rootname+"/divwpdivx_1.in", errors, "intg(Omega,div(w))+intg(Omega,i*w[1])", check);

  //! test of simple bilinear form
  TestFunction v(u,"v");
  TestFunction q(w,"q");
  BilinearForm bf = intg(Omega, dx(u) * dy(v));
  theCout << "test bf=intg(Omega,dx(u)*dy(v)) : " << bf;
  nbErrors+=checkValue(theCout, rootname+"/dxudxv.in", errors, "bf=intg(Omega,dx(u)*dy(v))", check);
  theCout << "test intg(Omega,Omega,grad(u)|grad(v)) : " << intg(Omega, Omega, grad(u) | grad(v));
  nbErrors+=checkValue(theCout, rootname+"/gradugradv.in", errors, "intg(Omega,Omega,grad(u)|grad(v))", check);
  theCout << "test intg(Omega,dx(u)*v) : " << intg(Omega, dx(u)*v);
  nbErrors+=checkValue(theCout, rootname+"/dxuv.in", errors, "intg(Omega,dx(u)*v)", check);
  theCout << "test intg(Omega,u*dt(v)) : " << intg(Omega, u * dt(v));
  nbErrors+=checkValue(theCout, rootname+"/udtv.in", errors, "intg(Omega,u*dt(v))", check);
  theCout << "test intg(Omega,u*v) : " << intg(Omega, u * v);
  nbErrors+=checkValue(theCout, rootname+"/uv.in", errors, "intg(Omega,u*v)", check);
  theCout << "test intg(Omega,w|q) : " << intg(Omega, w | q);
  nbErrors+=checkValue(theCout, rootname+"/wscalq.in", errors, "intg(Omega,w|q)", check);
  theCout << "test intg(Omega,(SCn^w)*( q |SCn)) : " << intg(Omega, (VecBasis ^ w) * (q | VecBasis) );
  nbErrors+=checkValue(theCout, rootname+"/SCnwqSCn.in", errors, "intg(Omega,(SCn^w)*( q |SCn))", check);
  theCout << "test intg(Omega,w|conj(q)) : " << intg(Omega, w | conj(q));
  nbErrors+=checkValue(theCout, rootname+"/wconjq.in", errors, "intg(Omega,w|conj(q))", check);

  //! test of linear combination of bilinear forms
  bf = bf + intg(Omega, dx(u) * v);
  theCout << "bf=bf+intg(Omega,dx(u)*v) : " << bf;
  nbErrors+=checkValue(theCout, rootname+"/bfpdxuv.in", errors, "bf=bf+intg(Omega,dx(u)*v)", check);
  bf = bf - intg(Omega, u * dt(v));
  theCout << "bf=bf-intg(Omega,u*dt(v)) : " << bf;
  nbErrors+=checkValue(theCout, rootname+"/bfmdxuv.in", errors, "bf=bf-intg(Omega,dx(u)*v)", check);
  bf = i_ * bf;
  theCout << "i*bf : " << bf;
  nbErrors+=checkValue(theCout, rootname+"/ibf.in", errors, "i*bf ", check);
  bf = bf * 3;
  theCout << "bf*3 : " << bf;
  nbErrors+=checkValue(theCout, rootname+"/bf3.in", errors, "bf*3 ", check);
  bf = bf / (3. * i_);
  theCout << "bf/(3*i) : " << bf;
  nbErrors+=checkValue(theCout, rootname+"/bfs3i.in", errors, "bf/(3*i) ", check);
  theCout << "intg(Omega,dx(u)*dy(v))+5*intg(Omega,u*v)/3.-i*intg(Omega,Omega,grad(u)|grad(v)) : "
      << intg(Omega, dx(u)*dy(v)) + 5 * intg(Omega, (Basis * u)*v) / 3. - i_* intg(Omega, Omega, grad(u) | grad(v));
  nbErrors+=checkValue(theCout, rootname+"/gonzo_bf.in", errors, "intg(Omega,dx(u)*dy(v))+5*intg(Omega,u*v)/3.-i*intg(Omega,Omega,grad(u)|grad(v)) ", check);

  //! test multiple unknowns
  bf += 2.5 * intg(Omega, Omega, grad(u) | q) - i_ * intg(Omega, div(w) * v);
  theCout << "bf+=2.5*intg(Omega,Omega,grad(u)|q)-i*intg(Omega,div(w)*v) : " << bf;
  nbErrors+=checkValue(theCout, rootname+"/graduscalq25midivwv.in", errors, "bf+=2.5*intg(Omega,Omega,grad(u)|q);-i*intg(Omega,div(w)*v) :  ", check);

  //! test bilinearform with KernelOperatorOnUnknowns
  Domain Gamma=mesh2d.domain("x=0"), Sigma=mesh2d.domain("x=1");
  Kernel K=Helmholtz2dKernel(1.);
  BilinearForm bkf=intg(Gamma,Sigma,u*K*v);
  theCout<<"bkf=intg(Gamma,Sigma,u*K*v) : "<<bkf<<eol;
  bkf=intg(Gamma,Sigma,u*K*(alpha*v));
  theCout<<"bkf=intg(Gamma,Sigma,u*K*(alpha*v)) : "<<bkf<<eol;

  //! test bilinearform with LcKernelOperatorOnUnknowns
  BilinearForm bklf=intg(Gamma,Sigma,2*(u*K*v) - u*ndotgrad_x(K)*v);
  theCout<<"bklf=int(Omega,Omega,2*(u*K*v)-u*ndotgrad_x(K)*v) : "<<bklf<<eol;
  nbErrors+=checkValue(theCout, rootname+"/blkf.in", errors, "bklf=int(Omega,Omega,2*(u*K*v)-u*ndotgrad_x(K)*v) ", check);

  //! test user bilinearform
  verboseLevel(16);
  Mesh ms(Rectangle(_xmin=0.,_xmax=1., _ymin=0., _ymax=1.,_nnodes=10,_domain_name="Omega"), _shape=_triangle, _order=1, _generator=_structured, _split_direction=_alternate);
  Domain omega=ms.domain("Omega");
  Space H(_domain=omega, _FE_type=Lagrange, _order=1, _name="H");
  Unknown p(H, _name="p"); TestFunction t(p, _name="t");
  BilinearForm ubf=userBilinearForm(omega,p,t,bfGradGrad,_FEComputation,_symmetric, true, false);
  TermMatrix Ku(ubf, _name="Ku");
  TermMatrix Ks(intg(omega,grad(p)|grad(t)), _name="Ks");
  thePrintStream<<Ks<<Ku<<eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
