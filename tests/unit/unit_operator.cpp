/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_operator.cpp
	\author E. Lunéville
	\since 23 feb 2012
	\date 11 may 2012

	Low level tests of Operator class and related classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>

using namespace xlifepp;

namespace unit_operator {

// scalar spectral function
Real sinBasis(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Int n = pa("basis index");        // get the index of function to compute
  return std::sqrt(2. / h) * std::sin(n * pi_ * x / h); //computation
}

//!vector spectral function
Vector<Real> VecBasis(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Int n = pa("basis index");        // get the index of function to compute
  Vector<Real> res(2);
  res(1) = std::sqrt(2. / h) * std::sin(n * pi_ * x / h); // computation
  res(2) = std::sqrt(2. / h) * std::cos(n * pi_ * x / h);
  return res;
}

//!vector spectral function
Vector<Complex> VecBasis3(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Int n = pa("basis index");        // get the index of function to compute
  Vector<Complex> res(3);
  res(1) = std::sqrt(2. / h) * std::sin(n * pi_ * x / h);
  res(2) = std::sqrt(2. / h) * std::cos(n * pi_ * x / h);
  res(3) = Complex(0,1.);
  return res;
}

Real scalF(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  return x; //computation
}

Vector<Real> VecF(const Point& P, Parameters& pa = defaultParameters)
{
    return Vector<Real>(1,0.);
}


//! matrix function
Matrix<Real> MatF(const Point& P, Parameters& pa = defaultParameters)
{
  Matrix<Real> res(2, 2);
  Real x = P.x();
  res(1, 1) = x;   res(1, 2) = 0.;
  res(2, 1) = 0.5; res(2, 2) = 2 * x;
  return res;
}

Matrix<Complex> MatFc(const Point& P, Parameters& pa = defaultParameters)
{
  Matrix<Complex> res(3, 3);
  Real x = P.x();
  res(1, 1) = Complex(0,1); res(2, 2) = x; res(3, 3) = 1.;
  return res;
}

//! scalar kernel function
Real G(const Point& M, const Point& P, Parameters& pa = defaultParameters)
{
//  Real r = M.distance(P);
  return 1.;    // computation
}

void unit_operator(int argc, char* argv[], bool check)
{
  String rootname = "unit_operator";
  trace_p->push(rootname);
  String errors;
  Number nbErrors = 0;


  //! test of DifferentialOperator
  DifferentialOperator* do1_p = findDifferentialOperator(_id);
  theCout << "difop id: name=" << do1_p->name() << " order=" << do1_p->order()
      << " normal=" << words(do1_p->normalRequired())
      << " extension=" << words(do1_p->extensionRequired()) << "\n";
  findDifferentialOperator(_dt);
  findDifferentialOperator(_dx);
  findDifferentialOperator(_dy);
  findDifferentialOperator(_dz);
  findDifferentialOperator(_grad);
  findDifferentialOperator(_div);
  findDifferentialOperator(_curl);
  findDifferentialOperator(_gradS);
  findDifferentialOperator(_gradG);
  findDifferentialOperator(_divS);
  findDifferentialOperator(_divG);
  findDifferentialOperator(_curlS);
  findDifferentialOperator(_curlG);
  findDifferentialOperator(_epsilon);
  findDifferentialOperator(_epsilonG);
  findDifferentialOperator(_ntimes);
  findDifferentialOperator(_timesn);
  findDifferentialOperator(_ncrossntimes);
  findDifferentialOperator(_timesncrossn);
  findDifferentialOperator(_timesn);
  findDifferentialOperator(_ndot);
  findDifferentialOperator(_ndotgrad);
  findDifferentialOperator(_ndiv);
  findDifferentialOperator(_ncross);
  findDifferentialOperator(_ncrosscurl);
  findDifferentialOperator(_ncrossncross);
  verboseLevel(2);
  printListDiffOp(theCout);

  nbErrors += checkValue(theCout, rootname+"/findDifferentialOperator.in", errors, "Test findDifferentialOperator", check);

  //! test of Value
  Value::printValueTypeRTINames(std::cout);

  Real s = 1.;
  Vector<Real> vr(3); Vector<Complex> vc(3, i_);
  Matrix<Real> mr(3, 3, 1.); Matrix<Complex> mc(3, 3, i_);
  Value Vs(s); theCout << "real Value Vs(s): " << Vs;
  nbErrors += checkValue( Vs.asReal(), 1., 0.0000001, errors, "real Value Vs(s)");
  Value Vi(i_); theCout << "complex Value Vi(i): " << Vi;
  nbErrors += checkValue( Vi.asComplex(), i_, 0.0000001, errors, "complex Value Vc(s)");
  Value Vvr(vr); theCout << "real vector Value Vvr(vr): " << Vvr;
  nbErrors += checkValues( Vvr.asRealVector(), vr , 0.0000001, errors, "real vector Value Vvr");
  Value Vvc(vc); theCout << "complex vector Value Vvc(vc): " << Vvc;
  nbErrors += checkValues( Vvc.asComplexVector(), vc , 0.0000001, errors, "Complex vector Value Vvc");
  Value Vmr(mr); theCout << "real matrix Value Vmr(mr): " << Vmr;
  nbErrors += checkValues( Vmr.asRealMatrix(), mr , 0.0000001, errors, "real matrix Value mr");
  Value Vmc(mc); theCout << "complex matrix Value Vmc(mc): " << Vmc;
  nbErrors += checkValues( Vmc.asComplexMatrix(), mc , 0.0000001, errors, "Complex matrix Value mr");
  theCout << "get real Value s= " << Vs.value<Real>() << "\n";
  nbErrors += checkValue( Vs.value<Real>(), 1., 0.0000001, errors, "real get Value Vs(s)");
  theCout << "get complex Value i= " << Vi.value<Complex>() << "\n";
  nbErrors += checkValue( Vi.value<Complex>(), i_, 0.0000001, errors, "complex get Value Vi(s)");
  theCout << "get real vector Value vr= " << Vvr.value<Vector<Real> >() << "\n";
  nbErrors += checkValues( Vvr.value<Vector<Real> >() , vr, 0.0000001, errors, "real vector get Value Vi(s)");
  theCout << "get complex vector Value vc= " << Vvc.value<Vector<Complex> >() << "\n";
  nbErrors += checkValues( Vvc.value<Vector<Complex> >() , vc, 0.0000001, errors, "Complex vector get Value Vi(s)");
  theCout << "get real matrix Value mr= " << Vmr.value<Matrix<Real> >() << "\n";
  nbErrors += checkValues( Vmr.value<Matrix<Real> >() , mr, 0.0000001, errors, "real matrix get Value mr");
  theCout << "get complex matrix Value mc= " << Vmc.value<Matrix<Complex> >() << "\n";
  nbErrors += checkValues( Vmc.value<Matrix<Complex> >() , mc, 0.0000001, errors, "complex matrix get Value mc");

  //! test of OperatorOnUnknown
  Mesh msh;  //fake mesh
  GeomDomain Omega(msh, "Omega", 3);
  Parameters ps;
  ps<<Parameter(1.,"h")<<Parameter(1,"basis index");
  Function F(sinBasis, "sinBasis", ps);
  Function VF(VecBasis, "(sin,cos)", ps);
  Function MF(MatF, "mat function", ps);
  Space V(_domain=Omega, _basis=Function(sinBasis, "sinBasis", ps), _dim=10, _name="sinus basis");
  Unknown u(V, _name="u", _rank=1);
  theCout << "definition of unknown u: " << u;
  Unknown w(V, _name="w", _dim=3);
  theCout << "definition of unknown w: " << w;

  //! test constructor and assignment
  OperatorOnUnknown opu(u), opgu(grad(u));
  theCout << "test OperatorOnUnknown opu(u): " << opu;
  theCout << "test OperatorOnUnknown opgu(grad(u)): " << opgu;
  opu = d0(u);
  theCout << "test opu=d0(u): " << opu;
  theCout << "test opgu=opu: " << opgu;
  // test all primary operator
  theCout << "test id(u): " << id(u);
  theCout << "test d0(u): " << d0(u);
  theCout << "test dt(u): " << dt(u);
  theCout << "test d1(u): " << d1(u);
  theCout << "test dx(u): " << dx(u);
  theCout << "test d2(u): " << d2(u);
  theCout << "test dy(u): " << dy(u);
  theCout << "test d3(u): " << d3(u);
  theCout << "test dz(u): " << dz(u);
  theCout << "test grad(u): " << grad(u);
  theCout << "test nabla(u): " << nabla(u);
  theCout << "test div(w): " << div(w);
  theCout << "test curl(w): " << curl(w);
  theCout << "test rot(w): " << rot(w);
  theCout << "test nx(u): " << nx(u);
  theCout << "test ndot(w): " << ndot(w);
  theCout << "test ncross(w): " << ncross(w);
  theCout << "test ncrossncross(w): " << ncrossncross(w);
  theCout << "test ndotgrad(u): " << ndotgrad(u);
  theCout << "test ndiv(w): " << ndiv(w);
  theCout << "test ncrosscurl(w): " << ncrosscurl(w);
  theCout << "test epsilon(w): " << epsilon(w);
  theCout << "test epsilonG(w): " << epsilonG(w,1,0,0);
  theCout << "test epsilonG(w): " << epsilonG(w,1,0,1,0);
  theCout << "test gradG(u): " << gradG(u,1,0,1);
  theCout << "test gradG(w): " << gradG(w,1,0,1);
  theCout << "test divG(u): " << divG(u,1,0,1);
  theCout << "test divG(w): " << divG(w,1,0,1);
  theCout << "test curlG(u): " << curlG(u,1,0,1);
  theCout << "test curlG(w): " << curlG(w,1,0,1);

  nbErrors += checkValue(theCout, rootname+"/constructorPassignement.in", errors, "Test constructor and assignement", check);

  //!test aliases
  theCout << "test n*u: " << _n* u;
  theCout << "test u*n: " << u* _n;
  theCout << "test n|w: " << (_n | w);
  theCout << "test w|n: " << (w | _n);
  theCout << "test n^w: " << (_n ^ w);
  theCout << "test n^(n^w): " << (_n ^ (_n ^ w));
  theCout << "test n|grad(u): " << (_n | grad(u));
  theCout << "test grad(u)|n: " << (grad(u) | _n);
  theCout << "test n*div(w): " << (_n * div(w));
  theCout << "test div(w)*n: " << (div(w)*_n);
  theCout << "test n^curl(w): " << (_n ^ curl(w));
  theCout << "test n^rot(w): " << (_n ^ rot(w));

  nbErrors += checkValue(theCout, rootname+"/aliases.in", errors, "Test aliases", check);

  //! test left and right operations with function
  theCout << "test F*opu: " << (F * opu);
  theCout << "test opu*F: " << (opu * F);
  theCout << "test F*opgu*F: " << (F * opgu * F);
  theCout << "test F*u: " << (F * u);
  theCout << "test u*F: " << (u * F);
  theCout << "test F*grad(u)*F: " << (F * grad(u)*F);
  theCout << "test VF|grad(u)*F: " << (VF | grad(u)*F);
  theCout << "test VF^grad(u)*VF: " << ((VF ^ grad(u)) * VF);  //cross product in 2D returns a scalar !
  theCout << "test MF*grad(u)*F: " << (MF * grad(u)*F);
  theCout << "test (MF*grad(u))|VF: " << ((MF * grad(u)) | VF);

  nbErrors += checkValue(theCout, rootname+"/leftPrightOpWFun.in", errors, "leftPrightOpWFun", check);

  //! test using explicit C++ functions
  theCout << "test u*scalF: " << (u * scalF);
  theCout << "test scalF*u: " << (scalF * u);
  theCout << "test grad(u)*scalF: " << (grad(u)*scalF);
  theCout << "test (adj(MatF)*grad(u))|VecF: " << ((adj(MatF)*grad(u)) | VecF);

  nbErrors += checkValue(theCout, rootname+"/explicitCppFunFun.in", errors, "explicitCppFunFun", check);

  //! test left and right operations with constant
  theCout << "test u*vr: " << (u * vr);
  theCout << "test s*grad(u): " << (s * grad(u));
  theCout << "test i*grad(u): " << (i_ * grad(u));
  theCout << "test vr|grad(u): " << (vr | grad(u));
  theCout << "test vc|grad(u): " << (vc | grad(u));
  theCout << "test mr*grad(u): " << (mr * grad(u));
  theCout << "test mc*grad(u): " << (mc * grad(u));
  theCout << "test s*grad(u)*i: " << (s * grad(u)*i_);
  theCout << "test i*grad(u)|vr: " << (i_ * grad(u) | vr);
  theCout << "test (vr|grad(u))*mr: " << ((vr | grad(u))*mr);

  nbErrors += checkValue(theCout, rootname+"/LeftPRightOpWCst.in", errors, "test left and right operations with constant", check);

  //! test conjugate/transpose and adjoint operator
  theCout << "test grad(conj(u)): " << grad(conj(u));
  theCout << "test trans(mr)*grad(conj(u)): " << (trans(mr)*grad(conj(u)));
  theCout << "test grad(conj(u))|conj(vc): " << (grad(conj(u)) | conj(vc));
  theCout << "test (adj(mc)*grad(conj(u)))|conj(vc): " << (adj(mc)*grad(conj(u)) | conj(vc));
  theCout << "test (adj(mc)*grad(conj(u)))|adj(vc): " << (adj(mc)*grad(conj(u)) | adj(vc));

  nbErrors += checkValue(theCout, rootname+"/ConjTransAdjOp.in", errors, "ConjTransAdjOp", check);

  //! test expressions with component
  theCout << "test grad(w[1]): " << grad(w[1]);
  theCout << "test grad(w[2])*s: " << (grad(w[2])*s);
  theCout << "test (adj(mc)*grad(conj(w[2])))|adj(vc): " << (adj(mc)*grad(conj(w[2])) | adj(vc));
  theCout << "test adj(mc)*(grad(conj(w[2])))*adj(mc)): " << (adj(mc) * (grad(conj(w[2]))*adj(mc)));

  nbErrors += checkValue(theCout, rootname+"/ExpWComponent.in", errors, "ExpWComponent", check);

  //!test kernel form
  Kernel ker=Helmholtz3dKernel(1.);
  //Kernel ker=Laplace3dKernel();
  theCout << "\ntest ker*u: " << ker*u;
  theCout << "\ntest u*ker: " << u*ker;
  theCout << "\ntest u*ker*u: " << u*ker*u;

  //!theCout << "\ntest G*u: " << G*u;                      //not working onVis.inual studio, ambiguity ?
  theCout << "\ntest u*G: " << u*G;
  theCout << "\ntest u*G*u: " << u*G*u;
  theCout << "\ntest u*(grad_x(ker)|_nx)*u: " << u*(grad_x(ker)|_nx)*u;
  theCout << "\ntest u*grad_y(grad_x(ker)|_nx)*u: " << u*grad_y(grad_x(ker)|_nx)*u;
  theCout << "\ntest u*grad_y(grad_x(ker)|_nx)|ny*u: " << u*(grad_y(grad_x(ker)|_nx)|_ny)*u;
  theCout << "\ntest u*(_nx^(_ny^ker))*u: " << u*(_nx^(_ny^ker))*u;
  theCout << "\ntest u*(grad_y(grad_x(ker))*u: " << u*(grad_y(grad_x(ker)))*u;
  //!theCout << "\ntest u*(_nx^(_ny^G))*u: " << u*(_nx^(_ny^G))*u;  //ambiguity between ny^Function et ny^Kernel, G may be cast to both

  nbErrors += checkValue(theCout, rootname+"/KernelForm.in", errors, "KernelForm", check);

  //!test operator on function
  theCout << "\ntest id(F): " << id(F);
  theCout << "\ntest ntimes(F): " << ntimes(F);
  theCout << "\ntest ndot(VF): " << ndot(VF);
  theCout << "\ntest ncross(VF): " << ncross(VF);
  theCout << "\ntest ncrossncross(VF): " << ncrossncross(VF);
  theCout << "\ntest n*F: " << _n*F;
  theCout << "\ntest F*n: " << F *_n;
  theCout << "\ntest (n^n)*F: " << ((_n^_n)*F);
  theCout << "\ntest n|VF: " << (_n|VF);
  theCout << "\ntest VF|*n: " << (VF |_n);
  theCout << "\ntest n^VF: " << (_n^VF);
  theCout << "\ntest n^(n^VF): " << (_n^(_n^VF));
  theCout << "\ntest (n^n)^VF): " << ((_n^_n)^VF);

  nbErrors += checkValue(theCout, rootname+"/OpOnFun.in", errors, "Operator on functions", check);

  Vector<Real> rVecT(2, 0.); rVecT(2)=-1.41421356237;

  //!evaluate operator on function
  Point M(1.);
  Real val;
  Vector<Real> v_val, n(2); n[0]=1.; n[1]=1.;
  theCout << "\ntest id(F)(M): " << id(F).eval(M,val);
  nbErrors += checkValue( id(F).eval(M,val), 0., 0.0000001, errors, "id(F)(M)");

  theCout << "\ntest id(VF)(M): " << id(VF).eval(M,v_val);
  nbErrors += checkValues( id(VF).eval(M,v_val), rVecT, 0.0000001, errors, "id(VF).eval(M,v_val)");

  theCout << "\ntest (VF|_n)(M): " << (VF|_n).eval(M,val,&n);
  nbErrors += checkValue( (VF|_n).eval(M,val,&n), -1.41421356237, 0.0000001, errors, "VF|_n)(M)");

  theCout << "\ntest (_n^VF)(M): " << (_n^VF).eval(M,val,&n);  //2D ->scalar result
  nbErrors += checkValue( (_n^VF).eval(M,val,&n), -1.41421356237, 0.0000001, errors, "(_n^VF)");

  rVecT(1)=1.; rVecT(2)=2.5;
  theCout << "\ntest (MatF*_n)(M): " << (MatF*_n).eval(M,v_val,&n);
  nbErrors += checkValues( (MatF*_n).eval(M,v_val,&n), rVecT, 0.0000001, errors, "(MatF*_n).eval(M,v_val,&n)");

  rVecT(1)=1.5; rVecT(2)=2.;
  theCout << "\ntest (_n*MatF)(M): " << (_n*MatF).eval(M,v_val,&n);
  nbErrors += checkValues( (_n*MatF).eval(M,v_val,&n), rVecT, 0.0000001, errors, "(_n*MatF).eval(M,v_val,&n)");


  rVecT(1)=10; rVecT(2)=0.;
  Matrix<Real> A(2,2);A(1,1)=10;
  theCout << "\ntest (A*_n)(M): " << (A*_n).eval(M,v_val,&n);
  nbErrors += checkValues( (A*_n).eval(M,v_val,&n), rVecT, 0.0000001, errors, "(A*_n).eval(M,v_val,&n)");

  Function H(VecBasis3, "(sin,cos,1)", ps);
  n.resize(3);n[2]=0.;
  Complex cval;
  Vector<Complex> cv_val;
  Vector<Complex> cVecT(3, Complex(0., 0.)); cVecT(2)=Complex(-1.41421356237,0.); cVecT(3)=i_;
  theCout << "\ntest id(H)(M): " << id(H).eval(M,cv_val);
  nbErrors += checkValues( id(H).eval(M,cv_val), cVecT, 0.0000001, errors, "id(H).eval(M,cv_val)");

  theCout << "\ntest (H|_n)(M): " << (H|_n).eval(M,cval,&n);
  nbErrors += checkValue( (H|_n).eval(M,cval,&n), Complex(-1.41421356237,0.), 0.0000001, errors, "(H|_n).eval(M,cval,&n)");

  cVecT(1)=i_; cVecT(2)=-i_; cVecT(3)=Complex(-1.41421356237,0.);
  theCout << "\ntest (_n^H)(M): " << (_n^H).eval(M,cv_val,&n);
  nbErrors += checkValues( (_n^H).eval(M,cv_val,&n), cVecT, 0.0000001, errors, "(_n^H).eval(M,cv_val,&n)");

  cVecT(1)=i_; cVecT(2)=1.; cVecT(3)=0.;
  theCout << "\ntest (MatFc*_n)(M): " << (MatFc*_n).eval(M,cv_val,&n)<<eol;
  nbErrors += checkValues( (MatFc*_n).eval(M,cv_val,&n), cVecT, 0.0000001, errors, "(MatFc*_n).eval(M,cv_val,&n)");

  //!test operator involving TermVector
  verboseLevel(25);
  Mesh msq(SquareGeo(_origin=Point(0.,0.),_length=1.,_nnodes=5,_domain_name="Omega"), _shape=_triangle, _order=1, _generator=_structured, _name="msq");
  Domain omega=msq.domain("Omega");
  Space Vh(_domain=omega, _interpolation=_P1, _name="Vh");  Unknown uh(Vh, _name="uh", _rank=1);
  TermVector x1(uh,omega,_x1,_name="x1");
  theCout << "test x1*uh: " << x1*uh;
  theCout << "test x1*uh*x1: " << x1*uh*x1;
  theCout << "test (x1^3)*uh: " << (x1^3)*uh;
  theCout << "test intg(omega,(x1^2)*uh): " << intg(omega,(x1^2)*uh);

  nbErrors += checkValue(theCout, rootname+"/OpInvolvingTermVector.in", errors, "Operator involving TermVector", check);

  //!test LcKernelOperatorOnUnknowns
  TestFunction v(u,"v");
  theCout << "test LcKernelOperatorOnUnknowns(): " << LcKernelOperatorOnUnknowns()<<eol;
  KernelOperatorOnUnknowns uKv = u*ker*v;
  theCout << "test KernelOperatorOnUnknowns uKv = u*ker*v: " << uKv.asString()<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(uKv): " << LcKernelOperatorOnUnknowns(uKv)<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(u*ndotgrad_x(ker)*v): " << LcKernelOperatorOnUnknowns(u*ndotgrad_x(ker)*v)<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(uKv,2.): " << LcKernelOperatorOnUnknowns(uKv,2.)<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(uKv,i_): " << LcKernelOperatorOnUnknowns(uKv,i_)<<eol;
  LcKernelOperatorOnUnknowns lc1 = u*ker*v;
  theCout << "test LcKernelOperatorOnUnknowns lc1 = u*ker*v: " << lc1<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(lc1): " << LcKernelOperatorOnUnknowns(lc1)<<eol;
  theCout << "test lc1 += (u*ndotgrad_x(ker)*v): " << (lc1 += u*ndotgrad_x(ker)*v) <<eol;
  theCout << "test lc1 += lc1: " << (lc1 += lc1) <<eol;
  theCout << "test lc1 -= (u*ndotgrad_x(ker)*v): " << (lc1 -= u*ndotgrad_x(ker)*v) <<eol;
  theCout << "test lc1 -= lc1: " << (lc1 -= lc1) <<eol;
  theCout << "test 2*LcKernelOperatorOnUnknowns(uKv): " << 2*LcKernelOperatorOnUnknowns(uKv)<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(uKv)*2: " << LcKernelOperatorOnUnknowns(uKv)*2<<eol;
  theCout << "test i_*LcKernelOperatorOnUnknowns(uKv): " << i_*LcKernelOperatorOnUnknowns(uKv)<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(uKv)*i_: " << LcKernelOperatorOnUnknowns(uKv)*i_<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(uKv)/2: " << LcKernelOperatorOnUnknowns(uKv)/2<<eol;
  theCout << "test LcKernelOperatorOnUnknowns(uKv)/i_: " << LcKernelOperatorOnUnknowns(uKv)/i_<<eol;
  LcKernelOperatorOnUnknowns lc= (u*ker*v)/i_ -2*(u*ndotgrad_x(ker)*v);
  theCout << "test LcKernelOperatorOnUnknowns lc= (u*ker*v)/i_ -2*(u*ndotgrad_x(ker)*v): " << lc<<eol;

  nbErrors += checkValue(theCout, rootname+"/LcKernelOperatorOnUnknowns.in", errors, "LcKernelOperatorOnUnknowns", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
