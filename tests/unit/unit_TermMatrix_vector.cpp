/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_TermMatrix_vector.cpp
	\author E. Lunéville
	\since 5 october 2012
	\date 25 june 2013

	Low level tests of TestMatrix class and related classes for vector unknown case.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_TermMatrix_vector {

//! scalar 2D function
Real fun(const Point& P, Parameters& pa = defaultParameters)
{
	return 1.;
}

Real xfy(const Point& P, Parameters& pa = defaultParameters)
{
	return P(1)*P(2);
}

void unit_TermMatrix_vector(int argc, char* argv[], bool check)
{
  String rootname = "unit_TermMatrix_vector";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  std::cout.precision(testPrec);
  verboseLevel(9);
  numberOfThreads(1);

  String errors;
  Number nbErrors = 0;
  Real tol=0.0001;

	//!create a mesh and Domains
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  Mesh mesh2d(inputsPathTo(rootname + "/mesh2d.msh"), _name="P1 mesh of [0,1]x[0,1]");
  // if (!check)
  // {
  //   mesh2d=Mesh(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=2,_side_names=sidenames),_shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  //   saveToFile("mesh2d.msh",mesh2d, _aUniqueFile);
  // }
  // else
  // {
    // mesh2d=Mesh(inputsPathTo(rootname + "/mesh2d.msh"), "P1 mesh of [0,1]x[0,1]",msh);
  // }
  Domain omega=mesh2d.domain("Omega");
  Domain gamma1=mesh2d.domain("Gamma_1");

  //!create Lagrange P1xP1 space and unknown
  Space V1(_domain=omega, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u(V1, _name="u", _dim=_2D);
  TestFunction v(u, _name="v");

  cpuTime("time for initialisation, mesh and space definition");

  //!useful TermVector
  TermVector un(u,omega,1., _name="un");

  //!test some volumic mass bilinear forms
  BilinearForm uv=intg(omega,u|v);
  TermMatrix M(uv, _name="U|V");
  ssout<<M;
  ssout<<"\n Re(M*un|un)="<<realPart(M*un|un)<<"\n\n";
  nbErrors += checkValue( realPart(M*un|un), 2., tol, errors, "Re(M*un)|un");
  clear(M);

  BilinearForm uv1=intg(omega,(fun*u)|v);
  TermMatrix M1(uv1, _name="f1*U|V");
  ssout<<M1;
  ssout<<"\n M1*un|un="<<realPart(M1*un|un)<<"\n\n";
  nbErrors += checkValue( realPart(M1*un|un), 2., tol, errors, "Re(M1*un)|un");
  clear(M1);

  BilinearForm xyuv=intg(omega,(xfy*u)|v);
  TermMatrix Mxy(xyuv, _name="xy*U|V");
  ssout<<Mxy;
  ssout<<"\n Mxy*un|un="<<realPart(Mxy*un|un)<<"\n\n";
  nbErrors += checkValue( realPart(Mxy*un|un), 0.5, tol, errors, "Re(Mxy*un)|un");
  clear(Mxy);


  BilinearForm uxyv=intg(omega,u|(xfy*v));
  TermMatrix Mxy2(uxyv, _name="U|xy*V");
  ssout<<Mxy2;
  ssout<<"\n Mxy2*un|un="<<realPart(Mxy2*un|un)<<"\n\n";
  nbErrors += checkValue( realPart(Mxy2*un|un), 0.5, tol, errors, "Re(Mxy2*un)|un");
  clear(Mxy2);

  //! test some volumic div div bilinear forms
  BilinearForm divudivv=intg(omega,div(u)*div(v));
  TermMatrix DivDiv(divudivv, _name="DivDiv");
  ssout<<DivDiv;
  clear(DivDiv);

  BilinearForm divudivvf=intg(omega,fun*div(u)*div(v));
  TermMatrix DivDivf(divudivvf, _name="DivDivf");
  ssout<<DivDivf;
  ssout<<"\nDivDivf*un="<<DivDivf*un<<"\n\n";
  nbErrors += checkValues(DivDivf*un, rootname+"/DivDivfx1.in", tol, errors, "Vec DivDivf*un", check);
  clear(DivDivf);

  BilinearForm divudivvf2=intg(omega,div(u)*fun*div(v));
  TermMatrix DivDivf2(divudivvf2, _name="DivDivf2");
  ssout<<DivDivf2;
  ssout<<"\nDivDivf2*un="<<DivDivf2*un<<"\n\n";
  nbErrors += checkValues(DivDivf2*un, rootname+"/DivDivf2x1.in", tol, errors, "Vec DivDivf2*un", check);
  clear(DivDivf2);

  BilinearForm udxv=intg(omega,u|dx(v));
  TermMatrix Dxvu(udxv, _name="DxVU");
  ssout<<Dxvu;
  ssout<<"\nDxvu*un="<<Dxvu*un<<"\n\n";
  nbErrors += checkValues(Dxvu*un, rootname+"/Dxvux1.in", tol, errors, "Vec Dxvu*un", check);
  clear(Dxvu);

  BilinearForm dxuv=intg(omega,dx(u)|v);
  TermMatrix Dxuv(dxuv, _name="DxUV");
  ssout<<Dxuv;
  ssout<<"\nDxuv*un="<<Dxuv*un<<"\n\n";
  nbErrors += checkValues(Dxuv*un, rootname+"/Dxuvx1.in", tol, errors, "Vec Dxuv*un", check);
  clear(Dxuv);

  //! test some surfacic mass bilinear forms
  BilinearForm mg1=intg(gamma1,u|v);
  TermMatrix Mg1(mg1, _name="Mg1");
  ssout<<Mg1;
  TermVector ung(u,gamma1,1., _name="un_gamma1");
  ssout<<"\n Mg1*ung|ung="<<realPart(Mg1*ung|ung)<<"\n\n";
  nbErrors += checkValue( realPart(Mg1*ung|ung), 2., tol, errors, "Mg1|ung");
  clear(Mg1);

  //!test some partial bilinear forms
  BilinearForm m11=intg(omega,u[1]*v[1]);
  TermMatrix M11(m11, _name="M11");
  ssout<<M11;
  TermVector un1=un(u[1]);
  ssout<<"\n M11*un1|un1="<<realPart(M11*un1|un1)<<"\n\n";
  nbErrors += checkValue( realPart(M11*un1|un1), 0., tol, errors, "M11*un1|un1");
  clear(M11);

  //!test some combination bilinear forms
  Real k=1,alpha=1.;
  BilinearForm h=intg(omega,div(u)*div(v))-k*intg(omega,u|v)+alpha*intg(gamma1,u|v);
  TermMatrix H(h, _name="H");
  ssout<<H;
  clear(H);

  Space V2(_domain=omega, _interpolation=_P2, _name="V2");
  Unknown p(V2, _name="p", _dim=_2D);  //P2xP2
  TestFunction q(p, _name="q");
  BilinearForm pq=intg(omega,p|q);
  TermMatrix Mpq(pq, _name="pq");
  ssout<<Mpq;
  TermVector pun(p,omega,1., _name="un");
  ssout<<"\nMpq*un|un="<<realPart(Mpq*pun|pun)<<"\n\n";
  nbErrors += checkValue( realPart(Mpq*pun|pun), 2., tol, errors, "Mpq*un|un");
  clear(Mpq);

  BilinearForm divpdivq=intg(omega,div(p)*div(q));
  TermMatrix Divpdivq(divpdivq, _name="divpdivq");
  ssout<<Divpdivq;
  ssout<<"\n"<<Divpdivq*pun<<"\n\n";
  nbErrors += checkValues(Divpdivq*pun, rootname+"/Divpdivqxp1.in", tol, errors, "Vec Divpdivq*pun", check);
  clear(Divpdivq);

  Space V3(_domain=omega, _interpolation=_P3, _name="V3");
  Unknown p3(V3, _name="p3", _dim=_2D);  //P3xP3
  TestFunction q3(p3, _name="q3");
  BilinearForm pq3=intg(omega,p3|q3);
  TermMatrix Mpq3(pq3, _name="pq3");
  ssout<<Mpq3;
  TermVector pun3(p3,omega,1., _name="un");
  ssout<<"\nMpq3*un|un="<<realPart(Mpq3*pun3|pun3)<<"\n\n";
  nbErrors += checkValue(realPart(Mpq3*pun3|pun3), 2., tol, errors, "realPart(Mpq3*pun3|pun3)");
  clear(Mpq3);

  BilinearForm divpdivq3=intg(omega,div(p3)*div(q3));
  TermMatrix Divpdivq3(divpdivq3, _name="divpdivq");
  ssout<<Divpdivq3;
  ssout<<"\n"<<Divpdivq3*pun3<<"\n\n";
  nbErrors += checkValues(Divpdivq3*pun3, rootname+"/Divpdivq3xp1.in", tol, errors, "Vec Divpdivq3*pun3", check);
  clear(Divpdivq3);

  Space V4(_domain=omega, _interpolation=_P4, _name="V4");
  Unknown p4(V4, _name="p4", _dim=_2D);   //P4xP4
  TestFunction q4(p4, _name="q4");
  BilinearForm pq4=intg(omega,p4|q4);
  TermMatrix Mpq4(pq4, _name="pq4");
  ssout<<Mpq4;
  TermVector pun4(p4,omega,1., _name="un");
  ssout<<"\nMpq4*un|un="<<realPart(Mpq4*pun4|pun4)<<"\n\n";
  nbErrors += checkValue(realPart(Mpq4*pun4|pun4), 2., tol, errors, "realPart(Mpq4*pun4|pun4)");

  BilinearForm divpdivq4=intg(omega,div(p4)*div(q4));
  TermMatrix Divpdivq4(divpdivq4, _name="divpdivq");
  ssout<<Divpdivq4;
  ssout<<"\n"<<Divpdivq4*pun4<<"\n\n";
  nbErrors += checkValues(Divpdivq4*pun4, rootname+"/Divpdivq4xp1.in", tol, errors, "Vec Divpdivq4*pun4", check);

  ssout<<"Mpq4+2*Divpdivq4="<<Mpq4+2*Divpdivq4;
  clear(Mpq4);
  clear(Divpdivq4);

  // test with a matrix coeff
  Matrix<Real> mat22(2,2);
  mat22(2,1)=-1.; mat22(1,2)=1.;
  mat22(1,1)=0.; mat22(2,2)=0.;
  BilinearForm udotmatq4=intg(omega, p4|(mat22*q4));
  TermMatrix Udotmatq4(udotmatq4, _name="udotmatq4");
  ssout << Udotmatq4;
  ssout << "\n" << Udotmatq4*pun4 << "\n\n";
  nbErrors += checkValues(Udotmatq4*pun4, rootname+"/Udotmatq4xp1.in", tol, errors, "Vec Udotmatq4*pun4", check);

  BilinearForm udotmatgradq41=intg(omega, p4|(mat22*grad(q4[1])));
  TermMatrix Udotmatgradq41(udotmatgradq41, _name="udotmatgradq41");
  ssout << Udotmatgradq41;
  ssout << "\n" << Udotmatgradq41*pun4 << "\n\n";
  nbErrors += checkValues(Udotmatgradq41*pun4, rootname+"/Udotmatgradq41xp1.in", tol, errors, "Vec Udotmatgradq41*pun4", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
