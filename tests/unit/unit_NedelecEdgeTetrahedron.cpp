/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_NedelecEdgeTetrahedron.cpp
	\author E. Lunéville
	\since 16 sep 2015
	\date 16 sep 2015

	Low level tests of Nedelec's edge element on tetrahedron
	and a global test

	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_NedelecEdgeTetrahedron
{

typedef GeomDomain& Domain;

Real omg=1, eps=1, mu=1, a=pi_, ome=omg* omg* mu* eps;

Vector<Real> f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  Vector<Real> res(3);
  Real c=3*a*a-ome;
  res(1)=-2*cos(a*x)*sin(a*y)*sin(a*z)*c;
  res(2)=   sin(a*x)*cos(a*y)*sin(a*z)*c;
  res(3)=   sin(a*x)*sin(a*y)*cos(a*z)*c;
  return res;
}

Vector<Real> fg(const Point& P, Parameters& pa = defaultParameters)  // rot u
{
  Real x=P(1), y=P(2), z=P(3);
  Vector<Real> res(3);
  res(1)= 0.;
  res(2)=-3*a*cos(a*x)*sin(a*y)*cos(a*z);
  res(3)= 3*a*cos(a*x)*cos(a*y)*sin(a*z);
  Vector<Real>& n=getN();
  return crossProduct(res,n);
}

Vector<Real> solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  Vector<Real> res(3);
  res(1)=-2*cos(a*x)*sin(a*y)*sin(a*z);
  res(2)=   sin(a*x)*cos(a*y)*sin(a*z);
  res(3)=   sin(a*x)*sin(a*y)*cos(a*z);
  return res;
}

const Element* elt;
Number index=1;
Vector<Real> w(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> r(3);
  ShapeValues shv = elt->computeShapeValues(P, false, false, 0);
  Number i=index-1;
  r[0]=shv.w[3*i];r[1]=shv.w[3*i+1];r[2]=shv.w[3*i+2];
  return r;
}

//compute dof_i(w_j) in physical space, check computeShapeValue and dof::operator()
void checkPhysicalShapeValue(const Space& V)
{
  theCout<<V<<eol;
  elt=V.element_p(Number(0));
  Function fw(w,3);
  theCout<<" -------- checkPhysicalShapeValue -------------"<<eol;
  String s="dof  ";
  for(Number d=1;d<=V.nbDofs();d++)
  {
    if(d==10) s="dof ";
    theCout<<s<<d<<": ";
    for(Number j=1;j<=V.nbDofs();j++)
    {
      index=j;
      theCout<<roundToZero(V.dof(d)(fw).asReal(),1.E-10)<<" ";
     }
   theCout<<eol;
  }
}

void checkMaxwell(FESubType fs, Number ord, Domain omega, Domain gamma, const String& rootname, std::stringstream& ssout, bool isTM, bool check, double tol, String& errors, Number& nbErrors)
{
  Number fa=1;
  Real errL2;
  String fss = "first";
  if(fs==_secondFamily){fa=2;fss="second";}
  ssout<<"=============== test Nedelec edge "<<fss<<" family of order "<<ord<<" ==============="<<eol;
  //create spaces
  Space V(omega,interpolation(_Nedelec,_firstFamily,ord,Hcurl),"V",false);
  Unknown e(V, _name="E"); TestFunction q(e, _name="q");
  Space W(omega,interpolation(_Lagrange,_standard,ord,H1),"W",true);
  Unknown u(W, _name="u", _dim=3); TestFunction v(u, _name="v");  //for H1 projection
  ssout<<" number of dofs : "<<V.nbDofs()<<eol;
  TermVector E_ex(u,omega,solex);
  //solve Dirichlet problem
  //  intg(omega,curl(e)|curl(q))-ome*intg(omega,e|q)=intg(omega,f|v)
  //  u x n = 0 on gamma  with f=rot(rot(uex))- ome*uex
  BilinearForm aev=intg(omega,curl(e)|curl(q))-ome*intg(omega,e|q);
  LinearForm l=intg(omega,f|q);
  TermVector b(l, _name="B");
  EssentialConditions ecs = (ncross(e) | gamma)=0;
  TermMatrix A(aev, ecs, _name="A");
  TermVector E=directSolve(A,b);
//  //! Pk interpolation, L2 projection
  TermMatrix N(intg(omega,e|v), _name="N");
  TermMatrix M(intg(omega,u|v), _name="M");
  TermVector NE=N*E;
  TermVector EP1=directSolve(M,NE,true);  //L2 projection
  String fn="E"+tostring(fa)+tostring(ord)+"_P1";
  saveToFile(fn, EP1, _format=_vtu, _aUniqueFile);
  nbErrors+= checkValues(fn+".vtu", rootname+"/"+fn+".in", tol, errors, "Ep_"+tostring(ord), check);
  //!interpolate "edge" solution
  TermVector Ei=interpolate(u,omega,E,"Ei");
  fn="E"+tostring(fa)+tostring(ord)+"_int";
  saveToFile(fn+".vtu",Ei,_format=_vtu,_aUniqueFile);
  nbErrors+= checkValues(fn+".vtu", rootname+"/"+fn+".in", tol, errors, "Ei_"+tostring(ord), check);
  //!compute error
  TermVector E_er=E_ex-EP1;
  errL2=std::sqrt(real((M*E_er|E_er)));
  //theCout<<"\n \n Nedelec NT"<<fa<<ord<<" Dirichlet, err L2 ="<<errL2<<eol;
  nbErrors+=checkValue(errL2, 0., tol, errors, "Dirichlet, ErrL2 NT"+tostring(fa)+tostring(ord));

  //solve Neumann problem
  //  intg(omega,curl(e)|curl(q))-ome*intg(omega,e|q)=intg(omega,f|v)+intg(gamma,g|v)
  //  with f=rot(rot(uex))- ome*uex  and g=rot(uex) x n
  Function g(fg); g.require(_n);
  TermMatrix An(aev, _name="An");
  LinearForm lg=intg(gamma,g|q,_computation=_FEextComputation); // use extended domain
  TermVector bg(lg, _name="Bg");
  TermVector bn=b+bg;
  TermVector En=directSolve(An,bn);
  TermVector EnP1=directSolve(M,N*En,true);  //L2 projection
  TermVector En_er=E_ex-EnP1;
  errL2=std::sqrt(real((M*En_er|En_er)));
  //theCout<<"\n \n Nedelec NT"<<fa<<ord<<" Neumann, err L2 ="<<errL2<<eol;
  nbErrors+=checkValue(errL2, 0., tol, errors, "Neumann, ErrL2 NT"+tostring(fa)+tostring(ord));
  TermVector Eni=interpolate(u,omega,En, _name="Eni");
  fn="En"+tostring(fa)+tostring(ord)+"_int";
  saveToFile(fn+".vtu",Eni,_format=_vtu,_aUniqueFile);

  TermVector bg2(intg(gamma,g|q), _name="Bg2");   // use Nedelec FE trace
  //thePrintStream<<bg<<eol<<bg2<<eol;
  TermVector bn2=b+bg2;
  TermVector En2=directSolve(An,bn2);
  TermVector En2P1=directSolve(M,N*En2,true);  //L2 projection
  TermVector En2_er=E_ex-En2P1;
  errL2=std::sqrt(real((M*En2_er|En2_er)));
  //theCout<<"\n \n Nedelec NT"<<fa<<ord<<" Neumann 2, err L2 ="<<errL2<<eol;
  nbErrors+=checkValue(errL2, 0., tol, errors, "Neumann 2, ErrL2 NT"+tostring(fa)+tostring(ord));
  TermVector En2i=interpolate(u,omega,En2, _name="En2i");
  fn="En2"+tostring(fa)+tostring(ord)+"_int";
  saveToFile(fn+".vtu",En2i,_format=_vtu,_aUniqueFile);
}

void unit_NedelecEdgeTetrahedron(int argc, char* argv[], bool check)
{
  String rootname = "unit_NedelecEdgeTetrahedron";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(110);

  Number nbErrors = 0;
  String errors;
  Real tol=0.07;
  bool isTM=isTestMode;
  numberOfThreads(1);
  Mesh me(_tetrahedron); Domain omg=me.domain("Omega");

  Point x(1./3,1./3,1./3);
  ShapeValues shv;
  Interpolation* interp;
  interp=findInterpolation(Nedelec, _firstFamily, 1, Hrot);
  ssout<<"\n\n================  Nedelec edge tetrahedron, first family, order 1 (NE1_1) ======================"<<eol;
  NedelecEdgeFirstTetrahedronPk NE11(interp);
  NE11.print(ssout);  shv = ShapeValues(NE11);
  NE11.computeShapeValues(x.begin(),shv,true);
  NE11.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NE11.in", errors, "NE11", check,isTM);
  Space V11(omg,*interp,"V11",false);
  checkPhysicalShapeValue(V11);
  ssout<<"\n\n================  Nedelec edge tetrahedron, first family, order 2 (NE1_2) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 2, Hrot);
  NedelecEdgeFirstTetrahedronPk NE12(interp);
  NE12.print(ssout);
  shv = ShapeValues(NE12);
  NE12.computeShapeValues(x.begin(),shv,true);
  NE12.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NE12.in", errors, "NE12", check, isTM);
  Space V12(omg,*interp,"V12",false);
  checkPhysicalShapeValue(V12);
  ssout<<"\n\n================  Nedelec edge tetrahedron, first family, order 3 (NE1_3) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 3, Hrot);
  NedelecEdgeFirstTetrahedronPk NE13(interp);
  NE13.print(ssout);
  shv = ShapeValues(NE13);
  NE13.computeShapeValues(x.begin(),shv,true);
  NE13.printShapeValues(ssout,x.begin(),shv);
  Space V13(omg,*interp,"V13",false);
  checkPhysicalShapeValue(V13);
  nbErrors += checkValue(ssout, rootname+"/NE13.in", errors, "NE13", check, isTM);
  ssout<<"\n\n================  Nedelec edge tetrahedron, first family, order 4 (NE1_4) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 4, Hrot);
  NedelecEdgeFirstTetrahedronPk NE14(interp);
  NE14.print(ssout);
  shv = ShapeValues(NE14);
  NE14.computeShapeValues(x.begin(),shv,true);
  NE14.printShapeValues(ssout,x.begin(),shv);
  nbErrors += checkValue(ssout, rootname+"/NE14.in", errors, "NE14", check, isTM);
  Space V14(omg,*interp,"V14",false);
  checkPhysicalShapeValue(V14);

  // the Nedelec edge second family is not implemented, to do later

  //check Maxwell problem
  // Cube cub(_origin=Point(0.,0.,0.), _length=1.,_nnodes=11,_domain_name="Omega",_side_names="Gamma");
  // Mesh mesh3d(cub, _shape=_tetrahedron, _order=1, _generator=gmsh, _name="mesh of the unit cube");
  Mesh mesh3d(inputsPathTo(rootname+"/mesh3d.msh"), _name="mesh of the unit cube");
  Domain omega=mesh3d.domain("Omega"), gamma=mesh3d.domain("Gamma");
  checkMaxwell(_firstFamily, 1, omega, gamma, rootname, ssout, isTM, check, tol, errors, nbErrors);
//  checkMaxwell(_firstFamily, 2, omega, gamma, rootname, ssout, isTM, check, tol, errors, nbErrors);
//  checkMaxwell(_firstFamily, 3, omega, gamma, rootname, ssout, isTM, check, tol, errors, nbErrors);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
