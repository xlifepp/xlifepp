/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANQY; without even the implied warranty of
  MERCHANQABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_NedelecQuadrangle.cpp
	\author E. Lunéville
	\since 31 jan 2015
	\date 31 jan 2015

	Low level tests of Nedelec's element on quadrangle
	and a global test on Maxwell problem on unit square

	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_NedelecQuadrangle
{

typedef GeomDomain& Domain;

Real omg=1, eps=1, mu=1, a=pi_, ome=omg* omg* mu* eps;

Vector<Real> f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Vector<Real> res(2);
  Real c=2*a*a-ome;
  res(1)=-c*cos(a*x)*sin(a*y);
  res(2)= c*sin(a*x)*cos(a*y);
  return res;
}

Vector<Real> solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Vector<Real> res(2);
  res(1)=-cos(a*x)*sin(a*y);
  res(2)= sin(a*x)*cos(a*y);
  return res;
}

const Element* elt;
Number index=1;
Vector<Real> w(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> r(2);
  ShapeValues shv = elt->computeShapeValues(P, false, false, 0);
  Number i=index-1;
  r[0]=shv.w[2*i];r[1]=shv.w[2*i+1];
  return r;
}

//compute dof_i(w_j) in physical space, check computeShapeValue and dof::operator()
void checkPhysicalShapeValue(const Space& V)
{
  theCout<<V<<eol;
  elt=V.element_p(Number(0));
  Function fw(w,2);
  theCout<<" -------- checkPhysicalShapeValue -------------"<<eol;
  String s="dof  ";
  for(Number d=1;d<=V.nbDofs();d++)
  {
    if(d==10) s="dof ";
    theCout<<s<<d<<": ";
    for(Number j=1;j<=V.nbDofs();j++)
    {
      index=j;
      theCout<<roundToZero(V.dof(d)(fw).asReal(),1.E-10)<<" ";
     }
   theCout<<eol;
  }
}

void unit_NedelecQuadrangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_NedelecQuadrangle";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(20);
  numberOfThreads(1);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0004;
  bool isTM=isTestMode;

  Point x(0.5,0.5);
  ShapeValues shv;
  Interpolation* interp;
  Mesh me(_quadrangle); Domain omg=me.domain("Omega");

  ssout<<"\n\n ================  Nedelec first family, quadrangle, order 1 (NQ11) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 1, Hrot);
  NedelecEdgeFirstQuadranglePk NQ11(interp);
  NQ11.print(ssout);
  shv = ShapeValues(NQ11);
  NQ11.computeShapeValues(x.begin(),shv,true);
  NQ11.printShapeValues(ssout,x.begin(),shv);
  Space V11(omg,*interp,"V11",false);
  checkPhysicalShapeValue(V11);

  ssout<<"\n\n ================  Nedelec first family, quadrangle, order 2 (NQ12) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 2, Hrot);
  NedelecEdgeFirstQuadranglePk NQ12(interp);
  NQ12.print(ssout);
  shv = ShapeValues(NQ12);
  NQ12.computeShapeValues(x.begin(),shv,true);
  NQ12.printShapeValues(ssout,x.begin(),shv);
  Space V12(omg,*interp,"V12",false);
  checkPhysicalShapeValue(V12);

  ssout<<"\n\n ================  Nedelec first family, quadrangle, order 3 (NQ13) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 3, Hrot);
  NedelecEdgeFirstQuadranglePk NQ13(interp);
  NQ13.print(ssout);
  shv = ShapeValues(NQ13);
  NQ13.computeShapeValues(x.begin(),shv,true);
  NQ13.printShapeValues(ssout,x.begin(),shv);
  Space V13(omg,*interp,"V13",false);
  checkPhysicalShapeValue(V13);

  /*! test : solve Maxwell harmonic equation on unit square Omega=]0,1[x]0,1[
                  curl curl(E) -w^2.mu.eps E = -i.w.mu.J=fJ   in Omega
                  E x n = 0                                   on dOmega
            Solution (a=k.pi) :
                     Ex = - cos(ax)sin(ay)
                     Ey =   cos(ay)sin(ax)
         -> fJ= (2a^2-w^2.mu.eps)E
  */
  Number k=2, nh=30, nhmax=30;
  std::vector<Real> hs, erL2, times;
  for (Number nh=30; nh<=nhmax; nh+=10)
  {
    elapsedTime();
    //!create mesh
    Strings sidenames(4,"Gamma");
    // Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=nh,_domain_name="Omega",_side_names=sidenames), _shape=_quadrangle, _order=1, _generator=_structured);
    Mesh  mesh2d(inputsPathTo(rootname+"/mesh2d.msh"));
    Domain omega=mesh2d.domain("Omega");
    Domain gamma=mesh2d.domain("Gamma");
    //!create spaces
    Space V(omega,interpolation(_Nedelec,_firstFamily,k,Hrot),"V",false);
    Unknown e(V, _name="E"); TestFunction q(e, _name="q");
    elapsedTime("mesh and space", thePrintStream);
    //!create FE terms
    BilinearForm aev=intg(omega,curl(e)|curl(q))-ome*intg(omega,e|q);
    LinearForm l=intg(omega,f|q);
    EssentialConditions ecs = (ncross(e) | gamma)=0;
    TermMatrix A(aev, ecs, _name="A");
    TermVector b(l, _name="B");
    elapsedTime("FE computation", thePrintStream);
    //!solve system
    TermVector E=directSolve(A,b);
    elapsedTime("solving", thePrintStream);
    //! P1 interpolation, L2 projection
    Space W(omega,interpolation(_Lagrange,_standard,k,H1),"W",false);
    Unknown u(W, _name="u", _dim=2); TestFunction v(u, _name="v");  //for H1 projection
    TermMatrix N(intg(omega,e|v), _name="N"), M(intg(omega,u|v), _name="M");
    TermVector EP1=directSolve(M,N*E,true);  //L2 projection
    //!compute error
    TermVector E_ex(u,omega,solex);
    TermVector E_er=E_ex-EP1;
    hs.push_back(1./(nh-1));
    Real ErrL2=std::sqrt(real((M*E_er|E_er)));
    erL2.push_back(std::sqrt(real((M*E_er|E_er))));
    theCout<<"\nNedelec NQ1"<<k<<"  err L2 ="<<ErrL2<<eol;
    nbErrors+=checkValue(ErrL2, 0., tol, errors, "ErrL2 Nedelec Quadrangle");
    elapsedTime("error computation", thePrintStream);

    //!save to file
    saveToFile("E_P1", EP1,_format=_vtk);
    saveToFile("E_ex", E_ex,_format=_vtk);
    saveToFile("E_er", E_er,_format=_vtk);
    elapsedTime("save to vtk file",thePrintStream);

    //!P1 interpolation, direct interpolation using local shape functions
    TermVector EI=interpolate(u,omega,E,"E_int");
    saveToFile("E_int", EI,_format=_vtk);
    thePrintStream<<EI<<eol;

   //!other method
    TermMatrix Mp(intg(omega,u|e), _name="Mp");
    TermVector G(u,omega,f, _name="G");
    TermVector bG=Mp*G;
    TermVector E2=directSolve(A,bG);
    TermVector E2P1=directSolve(M,N*E2);  //L2 projection
    saveToFile("E2P1", E2P1,_format=_vtk);
    nbErrors+= checkValues("E_P1_Omega.vtk", rootname+"/E_P1"+".in", tol, errors, "EP1.vtk", check);
    nbErrors+= checkValues("E_ex_Omega.vtk", rootname+"/E_ex"+".in", tol, errors, "Eex.vtk", check);
    nbErrors+= checkValues("E_er_Omega.vtk", rootname+"/E_er"+".in", tol, errors, "Eer.vtk", check);
    nbErrors+= checkValues("E2P1_Omega.vtk", rootname+"/E2P1"+".in", tol, errors, "E2P1.vtk", check);
    nbErrors+= checkValues("E_int_Omega.vtk", rootname+"/E_int"+".in", tol, errors, "E_int.vtk", check);
  }

  ssout<<"\n \n Nedelec_"<<k<<"  err L2 ="<<erL2;
  String fn = "erreur_NQ1_"+tostring(k)+".dat";
  std::ofstream erssout(fn.c_str());
  erssout<<hs<<eol<<erL2<<eol<<times;
  erssout.close();
  nbErrors+= checkValues(fn, rootname+"/erreur_NQ1_"+tostring(k)+".in", tol, errors, "ErrNQ1_"+tostring(k), check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
