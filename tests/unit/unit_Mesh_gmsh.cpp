/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_Mesh_gmsh.cpp
  \author N. Kielbasiewicz
  \since 13 dec 2013
  \date 5 mar 2014

  Low level tests of Mesh class using system call to gmsh
*/


//===============================================================================
//library dependences
#include "xlife++-libs.h"
#include "testUtils.hpp"

//===============================================================================
//stl dependences
#include <iostream>
#include <fstream>
#include <vector>

//===============================================================================

using namespace xlifepp;

namespace unit_Mesh_gmsh {

void extractGeoLines(CoutStream& out, String ofname="")
{
  std::ifstream fin("xlifepp_script.geo");
  char s[200];
  Number it_l=0;
  // we ignore 2 first lines of geo files (the include line and the following blank line)
  while (fin.getline(s,200))
  {
    if (it_l > 1)
    {
      out << s << eol;
      // we have to add a virtual line defining the format version (not written for gmsh 2.X but present in reference files)
      std::vector<String> gmshVersionNumbers=split(GMSH_VERSION,'.');
      if (stringto<int>(gmshVersionNumbers[0]) < 4)
      {
        String str=s;
        if (str.find("Mesh.ElementOrder=")!=String::npos) { out << "Mesh.MshFileVersion = 2.2;" << eol; }
      }
    }
    ++it_l;
  }
  fin.close();
  if (ofname != "")
  {
    remove(ofname.c_str()); //because not renamed if file already exists
    std::rename("xlifepp_script.msh",ofname.c_str());
  }
}

void unit_Mesh_gmsh(int argc, char* argv[], bool check)
{
  String rootname = "unit_Mesh_gmsh";
#ifdef XLIFEPP_WITH_GMSH
  trace_p->push(rootname);
  verboseLevel(1);
  String errors;
  Number nbErrors = 0;

  setDefaultCharacteristicLength(0.5);
  bool meshfile = true;          // if true save all meshes
  String ofname;

  //! Mesh built from a .geo file, input file for Gmsh, which is called to create the Gmsh format file
  std::cout << "****** Testing loadGeo" << eol;
  Mesh xlifepp2D(inputsPathTo(rootname+"/xlifepp2D.geo"), _name="xlifepp2D");
  Mesh xlifepp2D3D(inputsPathTo(rootname+"/xlifepp2D3D.geo"), _name="xlifepp2D3D");
  Mesh xlifepp3D(inputsPathTo(rootname+"/xlifepp3D.geo"), _name="xlifepp3D");
  
  std::cout << "****** Testing canonical geometries" << eol;

  Mesh mseg1(Segment(_xmin=1., _xmax=2.), _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Segment1D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Segment(_xmin=1, _xmax=2)", check);

  Mesh mseg2(Segment(_v1=Point(1.,0.), _v2=Point(0.,2.), _nnodes=6, _side_names=Strings("Gamma_1","Gamma_2")), _order=2, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Segment2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Segment(_v1=Point(1,0), _v2=Point(0,2)) with sides", check);

  Mesh mseg3(Segment(_v1=Point(0.,0.,0.), _v2=Point(1.,2.,3.), _nnodes=6, _domain_name="Sigma", _side_names=Strings("Gamma_1","Gamma_2")), _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Segment3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Segment(_v1=Point(0,0,0), _v2=Point(1,2,3)) with sides", check);

  Mesh marc1(EllArc(_center=Point(0.,0.), _v1=Point(2.,0.), _v2=Point(0.,1.)), _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="EllArc2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "EllArc(_center=Point(0,0), _v1=Point(2,0), _v2=Point(0,1))", check);

  Mesh marc2(EllArc(_center=Point(0.,0.,0.), _v1=Point(2.,0.,0.), _v2=Point(0.,0.,1.)), _order=2, _generator=gmsh, _default_hstep={0.,10.});
  ofname="EllArc3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "EllArc(_center=Point(0,0,0), _v1=Point(2,0,0), _v2=Point(0,0,1))", check);

  Real coord=sqrt(2.);
  Mesh marc3(CircArc(_center=Point(0.,0.), _v1=Point(2.,0.), _v2=Point(coord,coord)), _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CircArc2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "CircArc(_center=Point(0,0), _v1=Point(2,0), _v2=Point(sqrt(2),sqrt(2)))", check);

  Mesh marc4(CircArc(_center=Point(0.,0.,0.), _v1=Point(2.,0.,0.), _v2=Point(coord,0.,coord)), _order=2, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CircArc3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "CircArc(_center=Point(0,0,0), _v1=Point(2,0,0), _v2=Point(sqrt(2),0,sqrt(2)))", check);

  std::vector<Point> vertices(5), vertices2(4);
  vertices[0]=Point(0.,0.);
  vertices[1]=Point(2.,0.);
  vertices[2]=Point(3.,1.);
  vertices[3]=Point(1.,4.);
  vertices[4]=Point(-1.,2.);
  Polygon pg1(_vertices=vertices, _side_names=Strings("S1","S2","S3","S4","S5"));
  Mesh mpg1(pg1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Polygon2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Polygon(Point(0,0), Point(2,0), Point(3,1), Point(1,4), Point(-1,2))", check);

  vertices[0]=Point(0.,0.,0.);
  vertices[1]=Point(2.,2.,0.);
  vertices[2]=Point(3.,3.,1.);
  vertices[3]=Point(1.,1.,4.);
  vertices[4]=Point(-1.,-1.,2.);
  Polygon pg2(_vertices=vertices, _side_names=Strings("S1","S2","S3","S4","S5")); // S1, S2, S1, S2, S1 does not work
  Mesh mpg2(pg2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Polygon3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Polygon(Point(0,0,0), Point(2,2,0), Point(3,3,1), Point(1,1,4), Point(-1,-1,2))", check);

  Mesh mtri1(Triangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(0.,3.,1.)), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Triangle3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Triangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(0.,3.,1.))", check);

  Mesh mqua1(Quadrangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(2.,3.,1.), _v4=Point(0.,3.,1.)), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Quadrangle3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Quadrangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(2.,3.,1.), _v4=Point(0.,3.,1.))", check);

  Mesh mpara1(Parallelogram(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v4=Point(0.,3.,1.)), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelogram3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelogram(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v4=Point(0.,3.,1.))", check);

  Mesh mrec1(Rectangle(_xmin=0, _xmax=2, _ymin=-1, _ymax=1, _nnodes=Numbers(20,10)), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Rectangle2DXY";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rectangle(_xmin=0, _xmax=2, _ymin=-1, _ymax=1) with triangles", check);

  Mesh mrec2(Rectangle(_center=Point(0.,0.), _xlength=3., _ylength=2., _nnodes=10), _shape=quadrangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Rectangle2Dcenter";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rectangle(_center=Point(0,0), _xlength=3, _ylength=2) with quadrangles", check);

  Mesh mrec3(Rectangle(_origin=Point(0.,0.), _xlength=3., _ylength=2., _nnodes=10), _shape=triangle, _order=2, _generator=gmsh, _pattern=structured, _split_direction=right);
  ofname="Rectangle2Dorigin";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rectangle(_origin=Point(0,0), _xlength=3, _ylength=2) with structured triangles", check);

  Mesh msq1(SquareGeo(_center=Point(0.,0.), _length=1., _nnodes=10), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Square2Dcenter";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "SquareGeo(_center=Point(0,0), _length=1) with triangles", check);

  Mesh msq2(SquareGeo(_origin=Point(0.,0.), _length=1., _nnodes=10), _shape=segment, _order=2, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Square2Dorigin";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "SquareGeo(_origin=Point(0,0), _length=1) with segments", check);

  Mesh mell1(Ellipse(_center=Point(0.,0.), _v1=Point(2.,0.), _v2=Point(0.,1.), _nnodes=10), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipse2Dgeneral";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse(_center=Point(0,0), _v1=Point(2,0), _v2=Point(0,1))", check);

  Mesh mell2(Ellipse(_center=Point(0.,0.), _xlength=4., _ylength=2., _nnodes=10), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipse2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse(_center=Point(0,0), _xlength=4, _ylength=2)", check);

  Mesh mell2sector1(Ellipse(_center=Point(0.,0.), _xlength=4., _ylength=2., _nnodes=10, _angle1=25*deg_, _angle2=90*deg_), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipse2Dsector";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse(_center=Point(0,0), _xlength=4, _ylength=2, _angle1=25, _angle2=90)", check);

  Mesh mell2sector2(Ellipse(_center=Point(0.,0.), _xlength=4., _ylength=2., _nnodes=10, _angle1=25*deg_, _angle2=-25*deg_), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipse2Dsectorinv";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse(_center=Point(0,0), _xlength=4, _ylength=2, _angle1=25, _angle2=-25)", check);

  Mesh mell2sectorNames(Ellipse(_center=Point(0.,0.), _xlength=4., _ylength=2., _nnodes=10, _angle1=25*deg_, _angle2=90*deg_, _domain_name="O", _side_names=Strings("A", "B", "C")), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipse2DsectorNames";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse(_center=Point(0,0), _xlength=4, _ylength=2, _angle1=25, _angle2=90, _domain_name=O, _sidenames={A,B,C})", check);
  
  Mesh mdisk1(Disk(_center=Point(0.,0.), _radius=2., _nnodes=10), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Disk2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Disk(_center=Point(0,0), _radius=2)", check);

  Mesh mdisk1sector1(Disk(_center=Point(0.,0.), _radius=2., _nnodes=10, _angle1=25*deg_, _angle2=90*deg_), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Disk2Dsector";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Disk(_center=Point(0,0), _radius=2, _angle1=25, _angle2=90)", check);

  Mesh mdisk1sector2(Disk(_center=Point(0.,0.), _radius=2., _nnodes=10, _angle1=25*deg_, _angle2=-25*deg_), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Disk2Dsectorinv";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Disk(_center=Point(0,0), _radius=2, _angle1=25, _angle2=-25)", check);

  vertices[0]=Point(0.,0.,0.);
  vertices[1]=Point(2.,0.,0.);
  vertices[2]=Point(3.,1.,0.);
  vertices[3]=Point(1.,4.,0.);
  vertices[4]=Point(-1.,2.,0.);
  Polygon pg3(_vertices=vertices, _domain_name="S1");
  vertices[0]=Point(0.,0.,1.);
  vertices[1]=Point(2.,0.,1.);
  vertices[2]=Point(3.,1.,1.);
  vertices[3]=Point(1.,4.,1.);
  vertices[4]=Point(-1.,2.,1.);
  Polygon pg4(_vertices=vertices, _domain_name="S2");
  vertices2[0]=Point(0.,0.,0.);
  vertices2[1]=Point(2.,0.,0.);
  vertices2[2]=Point(2.,0.,1.);
  vertices2[3]=Point(0.,0.,1.);
  Polygon pg5(_vertices=vertices2, _domain_name="S3"); // S1
  vertices2[0]=Point(2.,0.,0.);
  vertices2[1]=Point(3.,1.,0.);
  vertices2[2]=Point(3.,1.,1.);
  vertices2[3]=Point(2.,0., 1.);
  Polygon pg6(_vertices=vertices2, _domain_name="S4"); // S2
  vertices2[0]=Point(3.,1.,0.);
  vertices2[1]=Point(1.,4.,0.);
  vertices2[2]=Point(1.,4.,1.);
  vertices2[3]=Point(3.,1., 1.);
  Polygon pg7(_vertices=vertices2, _domain_name="S5"); // S1
  vertices2[0]=Point(1.,4.,0.);
  vertices2[1]=Point(-1.,2.,0.);
  vertices2[2]=Point(-1.,2.,1.);
  vertices2[3]=Point(1.,4., 1.);
  Polygon pg8(_vertices=vertices2, _domain_name="S6"); // S2
  vertices2[0]=Point(-1.,2.,0.);
  vertices2[1]=Point(0.,0.,0.);
  vertices2[2]=Point(0.,0.,1.);
  vertices2[3]=Point(-1.,2., 1.);
  Polygon pg9(_vertices=vertices2, _domain_name="S7"); // S7
  std::vector<Polygon> faces(7);
  faces[0]=pg3;
  faces[1]=pg4;
  faces[2]=pg5;
  faces[3]=pg6;
  faces[4]=pg7;
  faces[5]=pg8;
  faces[6]=pg9;
  Polyhedron ph1(_faces=faces);
  Mesh mph1(ph1, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Polyhedron";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Polyhedron type prism with pentagonal base", check);

  Mesh mtet1(Tetrahedron(_v1=Point(0.,-1.,3.), _v2=Point(2.,-1.,3.), _v3=Point(0.,1.,3.), _v4=Point(0.,-1.,4.)), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Tetrahedron";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Tetrahedron(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v3=Point(0,1,3), _v4=Point(0,-1,4))", check);

  Hexahedron hexa1(_v1=Point(0.,-1.,3.), _v2=Point(2.,-1.,3.), _v3=Point(2.,1.,3.), _v4=Point(0.,1.,3.), _v5=Point(0.,-1.,4.), _v6=Point(2.,-1.,4.), _v7=Point(2.,1.,4.), _v8=Point(0.,1.,4.));
  Mesh mhex1(hexa1, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Hexahedron";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Hexahedron(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v3=Point(2,1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _v6=Point(2,-1,4), _v7=Point(2,1,4), _v8=Point(0,1,4))", check);

  Mesh mparad1(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4)), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepipedtetrahedra";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4)) with tetrahedra", check);

  Mesh mparad2(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4)), _shape=hexahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepipedhexahedra";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4)) with hexahedra", check);

  Mesh mparad3(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4)), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepipedtriangles";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4)) with triangles", check);

  Mesh mparad4(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=7), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepiped7";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=7) with tetrahedra", check);

  Mesh mparad5(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=6), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepiped6";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=6) with tetrahedra", check);

  Mesh mparad6(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=5), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepiped5";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=5) with tetrahedra", check);

  Mesh mparad7(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=4), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepiped4";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=4) with tetrahedra", check);

  Mesh mparad8(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=3), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepiped3";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=3) with tetrahedra", check);

  Mesh mparad9(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=2), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepiped2";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=2) with tetrahedra", check);

  Mesh mparad10(Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=1), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Parallelepiped1";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=1) with tetrahedra", check);

  Mesh mcubd1(Cuboid(_xmin=0, _xmax=2, _ymin=-1, _ymax=1, _zmin=3, _zmax=4), _shape=tetrahedron, _order=2, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CuboidXYZ";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cuboid(_xmin=0, _xmax=2, _ymin=-1, _ymax=1, _zmin=3, _zmax=4)", check);

  Mesh mcubd2(Cuboid(_center=Point(1.,0.,3.5), _xlength=2, _ylength=2, _zlength=1), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Cuboidcenter";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cuboid(_center=Point(1,0,3.5), _xlength=2, _ylength=2, _zlength=1)", check);

  Mesh mcubd3(Cuboid(_origin=Point(0.,-1.,3), _xlength=2, _ylength=2, _zlength=1), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Cuboidorigin";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cuboid(_origin=Point(0,-1,3), _xlength=2, _ylength=2, _zlength=1)", check);

  Mesh mcub1(Cube(_center=Point(0.,0.,0.), _length=2, _side_names=Strings("S1","S2","S1","S2","S2","S2")), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Cube";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cube(_center=Point(0,0,0), _length=2) with sides", check);

  Mesh melld1(Ellipsoid(_center=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.), _v6=Point(0.,0.,1.), _nnodes=10), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoidgeneral";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,2,0), _v6=Point(0,0,1))", check);

  Mesh melld2(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1)", check);

  Mesh melld2o7(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10, _nboctants=7), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid7";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1, _nboctants=7)", check);

  Mesh melld2o6(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10, _nboctants=6), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid6";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1, _nboctants=6)", check);

  Mesh melld2o5(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10, _nboctants=5), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid5";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1, _nboctants=5)", check);

  Mesh melld2o4(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10, _nboctants=4), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid4";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1, _nboctants=4)", check);

  Mesh melld2o3(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10, _nboctants=3), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid3";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1, _nboctants=3)", check);

  Mesh melld2o2(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10, _nboctants=2), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid2";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1, _nboctants=2)", check);

  Mesh melld2o1(Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nnodes=10, _nboctants=1), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ellipsoid1";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1, _nboctants=1)", check);

  Mesh mball1(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2)", check);

  Mesh mball1o7(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10, _nboctants=7), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball7";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=7)", check);

  Mesh mball1o6(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10, _nboctants=6), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball6";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=6)", check);

  Mesh mball1o5(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10, _nboctants=5), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball5";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=5)", check);

  Mesh mball1o4(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10, _nboctants=4), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball4";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=4)", check);

  Mesh mball1o3(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10, _nboctants=3), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball3";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=3)", check);

  Mesh mball1o2(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10, _nboctants=2), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball2";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=2)", check);

  Mesh mball1o1(Ball(_center=Point(0.,0.,0.), _radius=2., _nnodes=10, _nboctants=1), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Ball1";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=1)", check);

  Trunk tr1(_basis=Rectangle(_origin=Point(0.,0.,0.), _xlength=2, _ylength=1.), _origin=Point(0.,1.,5.), _scale=0.5);
  Mesh mtr1(tr1, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="TrunkOriginTwice";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Trunk with key origin used twice and scale < 1", check);

  Trunk tr2(_basis=Rectangle(_origin=Point(0.,0.,0.), _xlength=2, _ylength=1.), _origin=Point(0.,1.,5.), _scale=2);
  ofname="TrunkOriginTwice2";
  Mesh mtr2(tr2, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Trunk with key origin used twice and scale > 1", check);

  std::vector<Real> dir(3,0.);
  dir[2]=1.;
  Mesh mcyl1(Cylinder(_basis=Triangle(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.)), _direction=dir), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CylinderbasisTriangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cylinder(_basis=Triangle(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0)), _direction=(0,0,1))", check);

  Mesh mcyl2(Cylinder(_basis=Ellipse(_center=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.)), _direction=dir), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CylinderbasisEllipse";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cylinder(_basis=Ellipse(_center=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,2,0)), _direction=(0,0,1))", check);

  Mesh mcyl3(Cylinder(_center1=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.), _center2=Point(0.,0.,1.)), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CylinderpointsEllipse";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cylinder(_center1=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,2,0), _center2=Point(0,0,1))", check);

  Mesh mcyl4(Cylinder(_center1=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,3.,0.), _center2=Point(0.,0.,1.)), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CylinderpointsDisk";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cylinder(_center1=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,3,0), _center2=Point(0,0,1))", check);

  Mesh mrcyl1(RevCylinder(_center1=Point(0.,0.,0.), _center2=Point(0.,0.,1.), _radius=3.), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RevCylinder";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "RevCylinder(_center1=Point(0,0,0), _center2=Point(0,0,1), _radius=3)", check);

  std::vector<Real> dir2(3,0.);
  dir2[0]=-1.;
  dir2[2]=4.;
  Prism pri1(_basis=Triangle(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.)), _direction=dir2,
            _side_names=Strings("S1","S2","S1","S2","S2"));
  Mesh mpri1(pri1, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="PrismbasisTriangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Prism(_basis=Triangle(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0)), _direction=(-1,0,4))", check);

  std::vector<Number> nprism1(9,3);
  Prism pri2(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.), _direction=dir2, _nnodes=nprism1, _domain_name="Omega",
               _side_names="Gamma");
  Mesh mpri2(pri2, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Prismpoints";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Prism(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0), _direction=(-1,0,4))", check);

  std::vector<Number> nprism2(12,3);
  Prism pri3(_basis=SquareGeo(_origin=Point(0.,0.,0.), _length=2.), _direction=dir, _nnodes=nprism2, _domain_name="Omega",
             _side_names="Gamma");
  Mesh mpri3(pri3, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="PrismbasisSquare";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Prism(_basis=SquareGeo(_origin=Point(0,0,0), _length=2), _direction=Point(0,0,1)", check);

  Mesh mcon1(Cone(_basis=Triangle(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.)), _apex=Point(-1.,0.,4.)), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="ConebasisTriangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cone(_basis=Triangle(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0)), _apex=Point(-1,0,4))", check);

  Mesh mcon2(Cone(_basis=SquareGeo(_origin=Point(0.,0.,0.), _length=2.), _apex=Point(0.,0.,1.)), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="ConebasisSquare";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cone(_basis=SquareGeo(_origin=Point(0,0,0), _length=2), _apex=Point(0,0,1))", check);

  RevCone revcon1(_center=Point(0.,0.,0.), _radius=2., _apex=Point(0.,0.,4.), _nnodes=11, _side_names="Gamma");
  Mesh mrcon1(revcon1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RevCone";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "RevCone(_center=Point(0,0,0), _radius=2, _apex=Point(0,0,4)) with triangles", check);

  Pyramid pyr1(_basis=Rectangle(_v1=Point(0.,0.,0.), _v2=Point(1.,0.,0.), _v4=Point(0.,1.,0.)), _apex=Point(0.5,0.5,1.), _side_names=Strings("S1","S2","S1","S2","S2"));
  Mesh mpyr1(pyr1, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="PyramidbasisRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Pyramid(_basis=Rectangle(_v1=Point(0,0,0), _v2=Point(1,0,0), _v4=Point(0,1,0)), _apex=Point(0.5,0.5,1))", check);
  
  Pyramid pyr2(_v1=Point(0.,0.,0.), _v2=Point(1.,0.,0.), _v3=Point(1.,1.,0.), _v4=Point(0.,1.,0.), _apex=Point(0.5,0.5,1.));
  Mesh mpyr2(pyr2, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Pyramidpoints";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Pyramid(_v1=Point(0,0,0), _v2=Point(1,0,0), _v3=Point(1,1,0), _v4=Point(0,1,0), _apex=Point(0.5,0.5,1))", check);

  std::cout << "****** Testing parametrized arcs" << eol;

  //! open parametrized arc
  Parametrization par1(0,2*pi_,x_1*cos(x_1),x_1*sin(x_1),Parameters(),"tcos(t),tsin(t)"); //open
  ParametrizedArc parc1(_parametrization = par1, _partitioning=_linearPartition, _nbParts=20, _hsteps=0.1, _domain_name="Gamma");
  Mesh mpar1(parc1);
  ofname="ParamArc";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Open parametrized arc with segments", check);

  Segment sarc1(_v1=parc1.p2(), _v2=parc1.p1(), _hsteps=0.1, _domain_name="Sigma");
  Mesh mparc1s(surfaceFrom(parc1+sarc1,"Omega"), _shape=_triangle);
  ofname="SFParamArc";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Surface from open parametrized arc and segment with triangles", check);

  //! closed parametrized arc
  Parametrization par2(0,2*pi_,2*cos(x_1),sin(x_1),Parameters(),"2cos(t),sin(t)");  //closed
  ParametrizedArc parc2(_parametrization = par2, _partitioning=_linearPartition, _nbParts=20, _hsteps=0.1, _domain_name="Gamma");
  Mesh mpar2(parc2);
  ofname="ClosedParamArc";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Closed parametrized arc with segments", check);

  ParametrizedArc parc2s(_parametrization = par2, _partitioning=_splinePartition, _nbParts=20, _hsteps=0.1, _domain_name="Gamma");
  Mesh mpar2s(parc2s);
  ofname="ClosedSplinedParamArc";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Closed parametrized arc with spline", check);

  Mesh mparc2t(surfaceFrom(parc2,"Omega"), _shape=_triangle);
  ofname="SFClosedParamArc";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Closed parametrized arc with triangles", check);

  Mesh mparc2ts(surfaceFrom(parc2s,"Omega"), _shape=_triangle);
  ofname="SFClosedSplinedParamArc";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Closed parametrized arc (splined) with triangles", check);

  std::cout << "****** Testing spline arcs" << eol;

  Number n=5; std::vector<Point> points(n+1);
  Real x=0, dx=pi_/n;
  for(Number i=0;i<=n;i++, x+=dx) points[i]=Point(x,sin(x));
  n=6; std::vector<Point> points2(n+1);
  x=0, dx=2*pi_/n;
  for(Number i=0;i<=n;i++, x+=dx) points2[i]=Point(2*cos(x),sin(x));

  //! Catmull-Rom spline
  SplineArc spac(_splineType=_CatmullRomSpline, _vertices=points, _domain_name="Gamma", _hsteps=0.1);
  Mesh mspac(spac);
  ofname="CRSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Open Catmull-Rom spline arc with segments", check);

  Segment sega(_v1=points[5], _v2=points[0], _hsteps=0.1, _domain_name="Sigma");
  Mesh mspacs(surfaceFrom(spac+sega), _shape=_triangle);
  ofname="SFCRSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Surface from open Catmull-Rom spline arc and segment with triangles", check);

  SplineArc spac2(_splineType=_CatmullRomSpline, _vertices=points2, _domain_name="Gamma", _hsteps=0.1);
  Mesh mspac2(spac2);
  ofname="ClosedCRSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Closed Catmull-Rom spline arc with segments", check);

  Mesh mspacs2(surfaceFrom(spac2), _shape=_triangle);
  ofname="SFClosedCRSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Surface from closed Catmull-Rom spline arc with triangles", check);

  //! B-spline
  SplineArc spab(_splineType=_BSpline, _splineSubtype=_SplineApproximation, _vertices=points, _domain_name="Gamma", _hsteps=0.1);
  Mesh mspab(spab);
  ofname="BSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Open B-spline arc with segments", check);

  Mesh mspabs(surfaceFrom(spab+sega), _shape=_triangle);
  ofname="SFBSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Surface from open B-spline arc and segment with triangles", check);

  SplineArc spab2(_splineType=_BSpline, _splineSubtype=_SplineApproximation, _vertices=points2, _domain_name="Gamma", _hsteps=0.1);
  Mesh mspab2(spab2);
  ofname="ClosedBSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Closed B-spline arc with segments", check);

  Mesh mspabs2(surfaceFrom(spab2), _shape=_triangle);
  ofname="SFClosedBSpline";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Surface from closed B-spline arc with triangles", check);

  std::cout << "****** Testing loop geometries" << eol;

  Point a1(-1.5,-4.,0.), b1(1.5,-4.,0.), c1(2.,-3.5,0.), d1(2.,3.5,0.);
  Point e1(1.5,4.,0.), f1(-1.5,4.,0.), g1(-2.,3.5,0.), h1(-2.,-3.5,0.);
  Segment seg1(_v1=a1, _v2=b1, _nnodes=11, _domain_name="AB");
  CircArc circ1(_center=Point(1.5,-3.5,0.), _v1=b1, _v2=c1, _nnodes=3, _domain_name="BC");
  Segment seg2(_v1=c1, _v2=d1, _nnodes=6, _domain_name="CD");
  CircArc circ2(_center=Point(1.5,3.5,0.), _v1=d1, _v2=e1, _nnodes=3, _domain_name="DE");
  Segment seg3(_v1=e1, _v2=f1, _nnodes=11, _domain_name="EF");
  CircArc circ3(_center=Point(-1.5,3.5,0.), _v1=f1, _v2=g1, _nnodes=3, _domain_name="FG");
  Segment seg4(_v1=g1, _v2=h1, _nnodes=6, _domain_name="GH");
  CircArc circ4(_center=Point(-1.5,-3.5,0.), _v1=h1, _v2=a1, _nnodes=3, _domain_name="HA");
  Mesh mloop1(surfaceFrom(seg1+circ1+seg2+circ2+seg3+circ3+seg4+circ4), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) with triangles", check);

  Geometry sf1=surfaceFrom(seg1+circ1+seg2+circ2+seg3+circ3+seg4+circ4, "Omega2");
  Mesh mloop2(sf1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectanglecopy";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle with triangles (copy)", check);

  Point a2(0,0,0), b2(2,0,0), c2(2,1,0), d2(0,1,0);
  Point e2(0,0,1), f2(2,0,1), g2(2,1,1), h2(0,1,1);
  Rectangle rrect1(_v1=a2, _v2=b2, _v4=d2, _nnodes=6, _domain_name="R1");
  Rectangle rrect2(_v1=e2, _v2=f2, _v4=h2, _nnodes=6, _domain_name="R2");
  Rectangle rrect3(_v1=a2, _v2=b2, _v4=e2, _nnodes=6, _domain_name="R3");
  Rectangle rrect4(_v1=b2, _v2=c2, _v4=f2, _nnodes=6, _domain_name="R4");
  Rectangle rrect5(_v1=c2, _v2=d2, _v4=g2, _nnodes=6, _domain_name="R5");
  Rectangle rrect6(_v1=d2, _v2=a2, _v4=h2, _nnodes=6, _domain_name="R6");
  Mesh mloop3(volumeFrom(rrect1+rrect2+rrect3+rrect4+rrect5+rrect6), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CubeVF";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cube (loop) with tetrahedra", check);

  Segment seg5(_v1=a2, _v2=b2, _nnodes=6, _domain_name="AB");
  Segment seg6(_v1=b2, _v2=c2, _nnodes=6, _domain_name="BC");
  Segment seg7(_v1=c2, _v2=d2, _nnodes=6, _domain_name="CD");
  Segment seg8(_v1=d2, _v2=a2, _nnodes=6, _domain_name="DA");
  Geometry sf4=surfaceFrom(seg5+seg6+seg7+seg8, "R1");
  Mesh mloop4(volumeFrom(sf4+rrect2+rrect3+rrect4+rrect5+rrect6), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CubeVFSF";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Cube (loop with border loop) with tetrahedra", check);

  Real csconeheight=8, csbaseradius=2, cssphereheight=csbaseradius*csbaseradius/csconeheight;
  Real cssphereradius=std::sqrt(csbaseradius*csbaseradius+cssphereheight*cssphereheight);
  Number nnodes=6;
  Point csbasecenter(0.,0.,0.);
  Point cscenter(0.,0.,-cssphereheight);
  Point cstop(0.,0.,-cssphereheight-cssphereradius);
  Point csb1(csbaseradius,0.,0.), csb2(0.,csbaseradius,0.), csb3(-csbaseradius,0.,0.), csb4(0.,-csbaseradius,0.);
  CircArc csarc1(_center=cscenter, _v1=cstop, _v2=csb1, _nnodes=nnodes);
  CircArc csarc2(_center=cscenter, _v1=cstop, _v2=csb2, _nnodes=nnodes);
  CircArc csarc3(_center=cscenter, _v1=cstop, _v2=csb3, _nnodes=nnodes);
  CircArc csarc4(_center=cscenter, _v1=cstop, _v2=csb4, _nnodes=nnodes);
  CircArc csarcb1(_center=csbasecenter, _v1=csb1, _v2=csb2, _nnodes=nnodes);
  CircArc csarcb2(_center=csbasecenter, _v1=csb2, _v2=csb3, _nnodes=nnodes);
  CircArc csarcb3(_center=csbasecenter, _v1=csb3, _v2=csb4, _nnodes=nnodes);
  CircArc csarcb4(_center=csbasecenter, _v1=csb4, _v2=csb1, _nnodes=nnodes);
  Geometry fs1=ruledSurfaceFrom(csarcb1+(~csarc2)+csarc1,"Sphere1");
  Geometry fs2=ruledSurfaceFrom(csarcb2+(~csarc3)+csarc2,"Sphere2");
  Geometry fs3=ruledSurfaceFrom(csarcb3+(~csarc4)+csarc3,"Sphere3");
  Geometry fs4=ruledSurfaceFrom(csarcb4+(~csarc1)+csarc4,"Sphere4");
  Geometry fs5=planeSurfaceFrom(csarcb1+csarcb2+csarcb3+csarcb4,"Base");
  Mesh mloop5(volumeFrom(fs1+fs2+fs3+fs4+fs5,"Omega"), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="HalfSphere";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Half-sphere with ruledSurfaceFrom/VolumeFrom", check);

  //! false conesphere
  Point csapex(0.,0.,csconeheight);
  Segment csseg1(_v1=csapex, _v2=csb1, _nnodes=3*nnodes);
  Segment csseg2(_v1=csapex, _v2=csb2, _nnodes=3*nnodes);
  Segment csseg3(_v1=csapex, _v2=csb3, _nnodes=3*nnodes);
  Segment csseg4(_v1=csapex, _v2=csb4, _nnodes=3*nnodes);
  Geometry fc1=ruledSurfaceFrom(csarcb1+(~csseg2)+csseg1,"Cone1");
  Geometry fc2=ruledSurfaceFrom(csarcb2+(~csseg3)+csseg2,"Cone2");
  Geometry fc3=ruledSurfaceFrom(csarcb3+(~csseg4)+csseg3,"Cone3");
  Geometry fc4=ruledSurfaceFrom(csarcb4+(~csseg1)+csseg4,"Cone4");
  Mesh mloop6(volumeFrom(fc1+fc2+fc3+fc4+fs1+fs2+fs3+fs4,"Omega"), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="ConesphereVF";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Conesphere with ruledSurfaceFrom/VolumeFrom", check);

  std::cout << "****** Testing composite geometries" << eol;

  Ellipse ell1(_center=Point(0.,0.,0.), _v1=Point(4,0.,0.), _v2=Point(0.,5.,0.), _nnodes=6, _domain_name="Omega1",
               _side_names=Strings("Gamma_1","Gamma_2","Gamma_3","Gamma_4"));
  Rectangle rect1(_xmin=-2., _xmax=2., _ymin=-4., _ymax=4., _nnodes=6, _domain_name="Omega2",
                  _side_names=Strings("Gamma_5","Gamma_6","Gamma_7","Gamma_8"));
  Ellipse ell2(_center=Point(1.,2.,0.), _v1=Point(1.5,2.,0.), _v2=Point(1.,3.,0.), _nnodes=6, _domain_name="Omega3",
               _side_names=Strings("Gamma_9","Gamma_10","Gamma_11","Gamma_12"));
  Ellipse ell3(_center=Point(0.,0.,0.), _v1=Point(0.5,0.,0.), _v2=Point(0.,1.,0.), _nnodes=6, _domain_name="Omega4",
               _side_names=Strings("Gamma_13","Gamma_14","Gamma_15","Gamma_16"));
  Rectangle rect2(_xmin=5., _xmax=6., _ymin=0., _ymax=1., _nnodes=6, _domain_name="Omega5",
                  _side_names=Strings("Gamma_17","Gamma_18","Gamma_19","Gamma_20"));
  Disk disk1(_center=Point(5.5,0.5,0.), _v1=Point(5.7,0.5,0.), _v2=Point(5.5,0.7,0.), _nnodes=6, _domain_name="Omega6",
             _side_names=Strings("Gamma_21","Gamma_22","Gamma_23","Gamma_24"));

  //! case canonical - canonical
  Mesh mcomp1(ell1-ell2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical-Canonical";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse - Ellipse with triangles", check);

  //! case canonical + canonical
  Mesh mcomp2(ell1+ell2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical+Canonical";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse + Ellipse with triangles", check);

  //! case composite - canonical
  Mesh mcomp3((ell1+rect1)-ell2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite-Canonical";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rectangle) - Ellipse with triangles", check);

  //! case composite + canonical
  Mesh mcomp4((ell1+rect1)+ell2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite+Canonical";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rectangle) + Ellipse with triangles", check);

  //! case canonical - composite
  Mesh mcomp5(rect1-(ell2+ell3), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical-Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rectangle - (Ellipse + Ellipse) with triangles", check);

  //! case canonical + composite
  Mesh mcomp6(rect1+(ell2+ell3), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical+Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rectangle + (Ellipse + Ellipse) with triangles", check);

  //! case composite - composite
  Mesh mcomp7((ell1+rect1)-(ell2+ell3), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite-Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rectangle) - (Ellipse + Ellipse) with triangles", check);

  //! case composite + composite
  Mesh mcomp8((ell1+rect1)+(ell2+ell3), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite+Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rectangle) + (Ellipse + Ellipse) with triangles", check);

  //! case multi-composite
  Mesh mcomp9((ell1+rect1)-(ell2+ell3)+rect2-disk1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rectangle) - (Ellipse + Ellipse) + Rectangle - Disk with triangles", check);

  //! case composite 3d
  Ellipsoid elld1(_center=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.), _v6=Point(0.,0.,1.), _nnodes=5, _domain_name="Omega");
  Parallelepiped para1(_v1=Point(-0.5,-0.5,-0.5), _v2=Point(0.5,-0.5,-0.5), _v4=Point(-0.5,0.5,-0.5), _v5=Point(-0.5,-0.5,0.5), _nnodes=3);
  Mesh mcomp10((elld1-para1), _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipsoid - Parallelepiped with tetrahedra", check);

  //! canonical - loop
  Geometry g=ell1-sf1;
  g.domName("Omega"); // BUG is ignored in this case, it does not replace the name of ell1 when analyzed
  Mesh mcomp11(g, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical-RoundedRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse - Rounded Rectangle (loop) with triangles", check);

  //! canonical + loop (loop inside canonical)
  Mesh mcomp12(ell1+sf1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical+RoundedRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse + Rounded Rectangle (loop) with triangles", check);

  //! loop - canonical
  Mesh mcomp13(sf1-ell2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle-Canonical";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) - Ellipse with triangles", check);

  //! loop + canonical (canonical inside loop : undetermined inclusion -> need to force)
  Mesh mcomp14(sf1+!ell2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle+Canonicalforce";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) + Ellipse with triangles (need to force inclusion)", check);

  //! loop + canonical (loop inside canonical)
  Mesh mcomp15(sf1+ell1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle+Canonical";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) + Ellipse with triangles", check);

  //! composite - loop
  Mesh mcomp16((ell1+rect2)-sf1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite-RoundedRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rectangle) - Rounded Rectangle (loop) with triangles", check);

  //! composite + loop (loop inside one component)
  Mesh mcomp17((ell1+rect2)+sf1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite+RoundedRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rectangle) + Rounded Rectangle (loop) with triangles", check);

  //! loop - composite
  Mesh mcomp18(sf1-(ell2+ell3), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle-Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) - (Ellipse + Ellipse) with triangles", check);

  //! loop + composite (composite inside loop : undetermined inclusions -> need to force)
  Mesh mcomp19(sf1+!(ell2+ell3), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle+Compositeforce";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) + (Ellipse + Ellipse) with triangles (need to force inclusion)", check);

  //! composite with loop - composite
  Mesh mcomp20((ell1+sf1)-(ell2+ell3), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CompositeSF-Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rounded Rectangle (loop)) - (Ellipse + Ellipse) with triangles", check);

  //! composite with loop + composite (composite inside loop : undetermined inclusions -> need to rewrite and force)
  //! (ell1+sf1)+(ell2+ell3) should be written as follows
  //! (sf1+!(ell2+ell3))+ell1 or ell1+(sf1+!(ell2+ell3))
  Mesh mcomp21((sf1+!(ell2+ell3))+ell1, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CompositeSF+Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Rounded Rectangle (loop) + (Ellipse + Ellipse)) + Ellipse with triangles (need to force inclusion)", check);

  Mesh mcomp22(ell1+(sf1+!(ell2+ell3)), _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical+CompositeSF";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse + (Rounded Rectangle (loop) + (Ellipse + Ellipse)) with triangles (need to force inclusion)", check);

  //! loop - loop
  Point a3(0,0,0.);
  Point b3(1,0,0.);
  Point c3(0,1,0.);
  Point d3(-1,0,0.);
  Segment seg9(_v1=d3, _v2=b3, _nnodes=6);
  CircArc circ5(_center=a3, _v1=b3, _v2=c3, _nnodes=3);
  CircArc circ6(_center=a3, _v1=c3, _v2=d3, _nnodes=3);
  Geometry sf2=surfaceFrom(seg9+circ5+circ6,"Omega7");
  Mesh mcomp23(sf1-sf2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle-HalfDisk";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) - Half Disk (loop) with triangles", check);

  //! loop + loop (undetermined inclusion -> need to force)
  Mesh mcomp24(sf1+!sf2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle+HalfDiskforce";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) + Half Disk (loop) with triangles (need to force inclusion)", check);

  //! loop + loop (no inclusion)
  Point a4(5.5,0.5,0.);
  Point b4(5.7,0.5,0.);
  Point c4(5.5,0.7,0.);
  Point d4(5.3,0.5,0.);
  Segment seg10(_v1=d4, _v2=b4, _nnodes=6);
  CircArc circ7(_center=a4, _v1=b4, _v2=c4, _nnodes=3);
  CircArc circ8(_center=a4, _v1=c4, _v2=d4, _nnodes=3);
  Geometry sf3=surfaceFrom(seg10+circ7+circ8, "Omega6");
  Mesh mcomp25(sf1+sf3, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="RoundedRectangle+HalfDisk";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Rounded Rectangle (loop) + Half Disk (loop) with triangles", check);

  //! composite with loop + loop (no inclusion)
  Mesh mcomp26((sf1+(!(ell2+ell3)))+sf3, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="CompositeSF+HalfDisk";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Rounded Rectangle (loop) + (Ellipse + Ellipse)) + Half Disk (loop) with triangles", check);

  Mesh mcomp27((ell1+sf1)-(ell2+ell3)+rect2-sf3, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "(Ellipse + Rounded Rectangle (loop)) - (Ellipse + Ellipse) + Rectangle - Half Disk (loop) with triangles", check);

  //! composite + loop (loop inside one component + test += operator)
  g=ell1+rect2;
  g+=sf1;
  Mesh mcomp28(g, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Composite+=RoundedRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse + Rectangle += Rounded Rectangle (loop) with triangles", check);

  //! canonical - loop with use of operator -=
  //! to preserve ell1, we have to convert ell1 int a composite geometry with one component
  g=toComposite(ell1);
  g-=sf1;
  Mesh mcomp29(g, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Canonical-=RoundedRectangle";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Ellipse -= Rounded Rectangle (loop) with triangles", check);

  std::cout << "****** Testing adjacency in composite geometries" << eol;

  Point i1(3,0,0);
  Point j1(3,1,0);
  Point k1(3,0,1);
  Point l1(3,1,1);
  Rectangle rrect7(_v1=b2, _v2=i1, _v4=c2, _nnodes=6, _domain_name="R7");
  Mesh madj1(rrect1+rrect7, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Adjacent2D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Adjacent Rectangles with triangles", check);

  Parallelepiped ppara1(_v1=a2, _v2=b2, _v4=d2, _v5=e2, _nnodes=6, _domain_name="P1");
  Parallelepiped ppara2(_v1=b2, _v2=i1, _v4=c2, _v5=f2, _nnodes=6, _domain_name="P2");
  Mesh madj2(ppara1+ppara2, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="Adjacent3D";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Adjacent Parallelepipeds with tetrahedra", check);

  Rectangle rbg(_xmin=-1, _xmax=1, _ymin=0, _ymax=1, _hsteps=0.05, _domain_name="Omega0", _side_names=Strings("y=0","x=1","y=1","x=-1"));
  g=toComposite(rbg);
  Point p1(0.2,0.8), p2(0.,0.9), p3(-0.2,0.8), p4(0.,0.7);
  Point p5(-0.2,0.3), p6(0.2,0.3), p7(0.2,0.4), p8(0.2,0.5);
  Point p9(0.2,0.7), p10(0.3,0.6), p11(0.4,0.6);
  CircArc ca1(_center=Point(0.2,0.6), _v1=p8, _v2=p10, _nnodes=7, _domain_name="arc1");
  CircArc ca2(_center=Point(0.2,0.6), _v1=p10, _v2=p9, _nnodes=7, _domain_name="arc2");
  CircArc ca3(_center=Point(0.2,0.6), _v1=p7, _v2=p11, _nnodes=7, _domain_name="arc3");
  CircArc ca4(_center=Point(0.2,0.6), _v1=p11, _v2=p1, _nnodes=7, _domain_name="arc4");
  EllArc ea1(_center=Point(0.,0.8), _apogee=p1, _v1=p1, _v2=p2, _nnodes=10, _domain_name="ell1");
  EllArc ea2(_center=Point(0.,0.8), _apogee=p1, _v1=p2, _v2=p3, _nnodes=10, _domain_name="ell2");
  EllArc ea3(_center=Point(0.,0.8), _apogee=p1, _v1=p3, _v2=p4, _nnodes=10, _domain_name="ell3");
  EllArc ea4(_center=Point(0.,0.8), _apogee=p1, _v1=p4, _v2=p1, _nnodes=10, _domain_name="ell4");
  Segment sg1(_v1=p3, _v2=p5, _nnodes=10), sg2(_v1=p5, _v2=p6, _nnodes=10), sg3(_v1=p6, _v2=p7, _nnodes=3),
          sg4(_v1=p7, _v2=p8, _nnodes=3), sg5(_v1=p8, _v2=p9, _nnodes=4), sg6(_v1=p9, _v2=p1, _nnodes=3);
  g-=surfaceFrom(ea1+ea2+ea3+ea4, "Omega_vide");
  g+=surfaceFrom(sg1+sg2+sg3+sg4+sg5+sg6+(~ea4)+(~ea3), "Omega1");
  g+=surfaceFrom(sg4+ca1+ca2+sg6+(~ca4)+(~ca3), "Omega2");
  g+=surfaceFrom(ca1+ca2+(~sg5), "Omega0");
  Mesh madj3(g, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="AdjacentComposite";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Composite surface with adjacent components", check);

  std::cout << "****** Testing extrusions" << eol;

  Geometry extr1=extrude(ell1, Translation(0.,0.,4.), 5, "ExtrOmega", "Gamma");
  Mesh mextr1(extr1, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionCanonicalTr";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Ellipse by Translation with tetrahedra", check);

  Geometry extr2=extrude(sf1, Rotation3d(Point(5.,0.,0.),0.,5.,0.,pi_/2), 10, "ExtrOmega2");
  Mesh mextr2(extr2, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionRoundedRectangleRot";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (pi_/2) with tetrahedra", check);

  Geometry extr3=extrude(ell1-ell2, Rotation3d(Point(5.,0.,0.),0.,5.,0.,pi_/2), 10, "ExtrOmega3", "Gamma");
  Mesh mextr3(extr3, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionCompositeRot";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Ellipse - Ellipse by Rotation (pi_/2) with tetrahedra", check);

  Geometry extr4=extrude(sf1, Rotation3d(Point(5.,0.,0.),0.,5.,0.,pi_), 10, "ExtrOmega4");
  Mesh mextr4(extr4, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionRoundedRectangleRot180";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (pi_) with tetrahedra", check);

  Geometry extr5=extrude(ell1-ell2, Rotation3d(Point(5.,0.,0.),0.,5.,0.,pi_), 10, "ExtrOmega5", "Gamma");
  Mesh mextr5(extr5, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionCompositeRot180";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Ellipse - Ellipse by Rotation (pi_) with tetrahedra", check);

  Geometry extr6=extrude(sf1, Rotation3d(Point(5.,0.,0.),0.,5.,0.,3.*pi_/2), 10, "ExtrOmega6");
  Mesh mextr6(extr6, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionRoundedRectangleRot270";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (3*pi_/2) with tetrahedra", check);

  Geometry extr7=extrude(ell1-ell2, Rotation3d(Point(5.,0.,0.),0.,5.,0.,3.*pi_/2), 10, "ExtrOmega7", "Gamma");
  Mesh mextr7(extr7, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionCompositeRot270";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Ellipse - Ellipse by Rotation (3*pi_/2) with tetrahedra", check);

  Geometry extr7KV=extrude(ell1-ell2, Rotation3d(Point(5.,0.,0.),0.,5.,0.,3.*pi_/2), _layers=10, _domain_name="ExtrOmega9KV", _side_names="Gamma", _base_names=Strings("SigmaM", "SigmaP"));
  Mesh mextr7KV(extr7KV, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionCompositeRot270KV";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Ellipse - Ellipse by Rotation (3*pi_/2) with tetrahedra (key/value)", check);
  
  Geometry extr8=extrude(sf1, Rotation3d(Point(5.,0.,0.),0.,5.,0.,2.*pi_), 10, "ExtrOmega8");
  Mesh mextr8(extr8, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionRoundedRectangleRot360";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (2*pi_) with tetrahedra", check);

  Geometry extr9=extrude(ell1-ell2, Rotation3d(Point(5.,0.,0.),0.,5.,0.,2.*pi_), 10, "ExtrOmega9", "Gamma");
  Mesh mextr9(extr9, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionCompositeRot360";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of Ellipse - Ellipse by Rotation (2*pi_) with tetrahedra", check);

  Geometry extr10=extrude(circ1, Translation(0.,0.,4.), 5, "ExtrOmega10");
  Mesh mextr10(extr10, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
  ofname="extrusionCircArcTr";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "extrusion of CircArc by Translation with triangles", check);

  //! true conesphere
  Real csbaseradiusint=2., csconeheightint=8.;
  Real cssphereheightint=csbaseradiusint*csbaseradiusint/csconeheightint;
  Real cssphereradiusint=sqrt(csbaseradiusint*csbaseradiusint+cssphereheightint*cssphereheightint);
  Real diff=2.;
  Real csbaseradiusext=2.+diff, csconeheightext=8.+2.*diff;
  Real cssphereheightext=csbaseradiusext*csbaseradiusext/csconeheightext;
  Real cssphereradiusext=sqrt(csbaseradiusext*csbaseradiusext+cssphereheightext*cssphereheightext);
  nnodes=10;

  Point csapexint(0.,0.,csconeheightint), csp1int(csbaseradiusint,0.,0.), csp2int(0.,0.,-cssphereheightint-cssphereradiusint);
  Point csapexext(0.,0.,csconeheightext), csp1ext(csbaseradiusext,0.,0.), csp2ext(0.,0.,-cssphereheightext-cssphereradiusext);

  Segment css1(_v1=csp1ext, _v2=csapexext, _nnodes=2*nnodes);
  Segment css2(_v1=csapexext, _v2=csapexint, _nnodes=2*nnodes);
  Segment css3(_v1=csapexint, _v2=csp1int, _nnodes=2*nnodes);
  CircArc csc1(_center=Point(0.,0.,-cssphereheightint), _v1=csp1int, _v2=csp2int, _nnodes=nnodes);
  Segment css4(_v1=csp2int, _v2=csp2ext, _nnodes=2*nnodes);
  CircArc csc2(_center=Point(0.,0.,-cssphereheightext), _v1=csp2ext, _v2=csp1ext, _nnodes=nnodes);

  Geometry csbase=planeSurfaceFrom(css1+css2+css3+csc1+css4+csc2, "Gamma");
  Geometry csextrude=extrude(csbase, Rotation3d(Point(0.,0.,0.),0.,0.,1.,2.*pi_), "Omega1",
                             Strings("Gamma1", "Gamma2", "Gamma3", "Gamma4", "Gamma5", "Gamma6", "Gamma7", "Gamma8",
                                     "Gamma9", "Gamma10", "Gamma11", "Gamma12", "Gamma13", "Gamma14", "Gamma15", "Gamma16"));

  Mesh mextr11(csextrude, _shape=_tetrahedron, _order=1, _generator=_gmsh);
  ofname="extrusionConesphere";
  if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
  nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "Conesphere with extrusion by rotation (2.*pi_)", check);

  std::vector<String> gmshVersionNumbers=split(GMSH_VERSION,'.');
  if (stringto<int>(gmshVersionNumbers[0]) != 3)
  {
    std::cout << "****** Testing cracks" << eol;

    //! crack 2D 1st version
    Point x1(0,0,0);
    Point x2(1,0,0);
    Point x3(1,1,0);
    Point x4(0,1,0);
    Point x5(0.2,0.2,0);
    Point x6(0.8,0.8,0);
    Point x7(0.2,0,0);
    Point x8(0.8,1,0);
    Segment ss1(_v1=x1, _v2=x7, _domain_name="Gamma");
    Segment ss2(_v1=x2, _v2=x7, _domain_name="Gamma");
    Segment ss3(_v1=x3, _v2=x2, _domain_name="Gamma");
    Segment ss4(_v1=x8, _v2=x3, _domain_name="Gamma");
    Segment ss5(_v1=x8, _v2=x4, _domain_name="Gamma");
    Segment ss6(_v1=x4, _v2=x1, _domain_name="Gamma");
    Segment ss7(_v1=x7, _v2=x5);
    Segment ss8(_v1=x5, _v2=x6, _nnodes=3, _domain_name="Crack");
    Segment ss9(_v1=x6, _v2=x8);
    crack(ss8);
    Geometry gsf1=surfaceFrom(ss7+ss8+ss9+ss5+ss6+ss1, "Omega1");
    Geometry gsf2=surfaceFrom(ss7+ss8+ss9+ss4+ss3+ss2, "Omega2");
    Mesh mcrack1(gsf1+gsf2, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
    ofname="crack1Dloop";
    if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
    nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "cracked Segment (loop version) with triangles", check);

    //! crack 2D 2st version
    Rectangle rrect8(_v1=x1, _v2=x2, _v4=x4, _nnodes=2, _domain_name="Omega", _side_names="Gamma");
    Segment scrack(_v1=x5, _v2=x6, _nnodes=3, _domain_name="Crack", _side_names="Sigma");
    openCrack(scrack,String("Sigma"));
    Mesh mcrack2(rrect8+scrack, _shape=triangle, _order=1, _generator=gmsh, _default_hstep={0.,10.});
    ofname="crack1D";
    if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
    nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "open cracked Segment (operator+ version) with triangles", check);

    //! crack 3D
    Ellipsoid elldcrack(_center=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.), _v6=Point(0.,0.,1.),
                        _nnodes=5, _domain_name="Omega");
    Rectangle rcrack(_v1=x1, _v2=x2, _v4=x4, _nnodes=2, _domain_name="Crack");
    crack(rcrack);
    Mesh mcrack3(elldcrack+rcrack, _shape=tetrahedron, _order=1, _generator=gmsh, _default_hstep={0.,10.});
    ofname="crack2D";
    if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
    nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "cracked Rectangle with tetrahedra", check);

    //! crack 3D
    Point p1ar(-0.3,0.), p2ar(-0.1,0.), p3ar(0.,0.), p4ar(0.3,0.);
    Point p5ar(-0.3,0.49), p6ar(-0.1,0.49), p7ar(0.,0.49), p8ar(0.3,0.49);
    Point p9ar(-0.3,0.51), p10ar(-0.1,0.51), p11ar(0.,0.51), p12ar(0.3,0.51);
    Point p13ar(-0.3,1.), p14ar(-0.1,1.), p15ar(0.,1.), p16ar(0.3,1.);
    Real h = 1., epColle = 0.02;
    Number MeshSize = 20;
    Segment s1ar(_v1=p1ar, _v2=p2ar, _nnodes=(Number)floor(0.2*MeshSize+1e-5)+1, _domain_name="GAMMAm");
    Segment s2ar(_v1=p2ar, _v2=p3ar, _nnodes=(Number)floor(0.1*MeshSize+1e-5)+1, _domain_name="GAMMAm");
    Segment s3ar(_v1=p3ar, _v2=p4ar, _nnodes=(Number)floor(0.3*MeshSize+1e-5)+1, _domain_name="GAMMAm");
    Segment s4ar(_v1=p5ar, _v2=p6ar, _nnodes=(Number)floor(0.2*MeshSize+1e-5)+1);
    Segment s5ar(_v1=p6ar, _v2=p7ar, _nnodes=(Number)floor(0.1*MeshSize+1e-5)+1);
    Segment s6ar(_v1=p7ar, _v2=p8ar, _nnodes=(Number)floor(0.3*MeshSize+1e-5)+1);
    Segment s7ar(_v1=p9ar, _v2=p10ar, _nnodes=(Number)floor(0.2*MeshSize+1e-5)+1);
    Segment s8ar(_v1=p10ar, _v2=p11ar, _nnodes=(Number)floor(0.1*MeshSize+1e-5)+1, _domain_name="DEFAUT"); crack(s8ar);
    Segment s9ar(_v1=p11ar, _v2=p12ar, _nnodes=(Number)floor(0.3*MeshSize+1e-5)+1);
    Segment s10ar(_v1=p13ar, _v2=p14ar, _nnodes=(Number)floor(0.2*MeshSize+1e-5)+1, _domain_name="GAMMAp");
    Segment s11ar(_v1=p14ar, _v2=p15ar, _nnodes=(Number)floor(0.1*MeshSize+1e-5)+1, _domain_name="GAMMAp");
    Segment s12ar(_v1=p15ar, _v2=p16ar, _nnodes=(Number)floor(0.3*MeshSize+1e-5)+1, _domain_name="GAMMAp");

    Segment s13ar(_v1=p1ar, _v2=p5ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1, _domain_name="SIGMAm");
    Segment s14ar(_v1=p5ar, _v2=p9ar, _nnodes=(Number)floor(epColle*MeshSize+1e-5)+1, _domain_name="SIGMAm");
    Segment s15ar(_v1=p9ar, _v2=p13ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1, _domain_name="SIGMAm");
    Segment s16ar(_v1=p2ar, _v2=p6ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1);
    Segment s17ar(_v1=p6ar, _v2=p10ar, _nnodes=(Number)floor(epColle*MeshSize+1e-5)+1);
    Segment s18ar(_v1=p10ar, _v2=p14ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1);
    Segment s19ar(_v1=p3ar, _v2=p7ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1);
    Segment s20ar(_v1=p7ar, _v2=p11ar, _nnodes=(Number)floor(epColle*MeshSize+1e-5)+1);
    Segment s21ar(_v1=p11ar, _v2=p15ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1);
    Segment s22ar(_v1=p4ar, _v2=p8ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1, _domain_name="SIGMAp");
    Segment s23ar(_v1=p8ar, _v2=p12ar, _nnodes=(Number)floor(epColle*MeshSize+1e-5)+1, _domain_name="SIGMAp");
    Segment s24ar(_v1=p12ar, _v2=p16ar, _nnodes=(Number)floor((h-epColle)/2*MeshSize+1e-5)+1, _domain_name="SIGMAp");

    Geometry g1ar=surfaceFrom(s1ar+s16ar+(~s4ar)+(~s13ar), "OMEGA1");
    Geometry g2ar=surfaceFrom(s2ar+s19ar+(~s5ar)+(~s16ar), "OMEGA1");
    Geometry g3ar=surfaceFrom(s3ar+s22ar+(~s6ar)+(~s19ar), "OMEGA1");
    Geometry g4ar=surfaceFrom(s4ar+s17ar+(~s7ar)+(~s14ar), "OMEGA2");
    Geometry g5ar=surfaceFrom(s5ar+s20ar+(~s8ar)+(~s17ar), "OMEGA2");
    Geometry g6ar=surfaceFrom(s6ar+s23ar+(~s9ar)+(~s20ar), "OMEGA2");
    Geometry g7ar=surfaceFrom(s7ar+s18ar+(~s10ar)+(~s15ar), "OMEGA1");
    Geometry g8ar=surfaceFrom(s8ar+s21ar+(~s11ar)+(~s18ar), "OMEGA1");
    Geometry g9ar=surfaceFrom(s9ar+s24ar+(~s12ar)+(~s21ar), "OMEGA1");
    Geometry gar=g1ar+g2ar+g3ar+g4ar+g5ar+g6ar+g7ar+g8ar+g9ar;
    Mesh mcrack4(gar, _shape=_quadrangle);
    ofname="crack1DQ1";
    if (meshfile) extractGeoLines(theCout, ofname+".msh"); else extractGeoLines(theCout);
    nbErrors += checkValue(theCout, rootname+"/"+ofname+".in", errors, "cracked Segment with quadrangles (Arnaud's mesh)", check);
  }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else
  { theCout << "Data updated " << eol; }

 trace_p->pop();
 
#else
  return String(rootname+" skipped");
#endif
}

}
