/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_quadrangle.cpp
	\author N. Kielbasiewicz
	\since 5 nov 2012
	\date 18 dec 2012

	Low level tests of refQuadrangle class.
 */

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_quadrangle {
using std::endl;

void unit_quadrangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_quadrangle";
  trace_p->push(rootname);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!test GeomRefQuadrangle
  verboseLevel(12);
  theCout<<"----------------------------- GeomRefQuadrangle test ----------------------------------"<<eol;
  GeomRefQuadrangle grt;
  theCout<<grt;
  for(Number i=0;i<grt.nbSides();++i)
  {
      Number v1=grt.sideVertexNumbers()[i][0], v2=grt.sideVertexNumbers()[i][1];
      theCout<<"sideWithVertices("<<v1<<","<<v2<<") = "<<grt.sideWithVertices(v1,v2)<<eol;
  }
  nbErrors += checkValue(theCout, rootname+"/GeomRefQuadrangle.in" , errors, "Test Geom Ref Quadrangle",check);

  //!test interpolation
  verboseLevel(5);
  theCout<<eol<<"----------------------------- interpolation test ----------------------------------"<<eol;
  Interpolation* q1 = findInterpolation(_Lagrange, _standard, 1, H1);
  Interpolation* q2 = findInterpolation(_Lagrange, _standard, 2, H1);
  Interpolation* q3 = findInterpolation(_Lagrange, _standard, 3, H1);
  Interpolation* q4 = findInterpolation(_Lagrange, _standard, 4, H1);
  Interpolation* q5 = findInterpolation(_Lagrange, _standard, 5, H1);
  RefElement* quadrangleQ1 = findRefElement(_quadrangle, q1);
  RefElement* quadrangleQ2 = findRefElement(_quadrangle, q2);
  RefElement* quadrangleQ3 = findRefElement(_quadrangle, q3);
  RefElement* quadrangleQ4 = findRefElement(_quadrangle, q4);
  RefElement* quadrangleQ5 = findRefElement(_quadrangle, q5);

  verboseLevel(10);
  theCout << *(quadrangleQ1) << endl;
  theCout << *(quadrangleQ2) << endl;
  theCout << *(quadrangleQ3) << endl;
  theCout << *(quadrangleQ4) << endl;
  theCout << *(quadrangleQ5) << endl;
  nbErrors += checkValue(theCout, rootname+"/Interpolation.in" , errors, "Test Interpolation",check);

  //!test splitting
  theCout << "split Fast P1 to P1 : " << quadrangleQ1->splitP1() << endl;
  theCout << "split Fast P2 to P1 : " << quadrangleQ2->splitP1() << endl;
  theCout << "split Fast P3 to P1 : " << quadrangleQ3->splitP1() << endl;
  theCout << "split P1 to P1 : " << quadrangleQ1->splitLagrange2DToP1() << endl;
  theCout << "split P2 to P1 : " << quadrangleQ2->splitLagrange2DToP1() << endl;
  theCout << "split P3 to P1 : " << quadrangleQ3->splitLagrange2DToP1() << endl;
  theCout << "split P4 to P1 : " << quadrangleQ4->splitLagrange2DToP1() << endl;
  theCout << "split P5 to P1 : " << quadrangleQ5->splitLagrange2DToP1() << endl;
  theCout << "split Q1 to Q1 : " << quadrangleQ1->splitO1() << endl;
  theCout << "split Q2 to Q1 : " << quadrangleQ2->splitO1() << endl;
  theCout << "split Q3 to Q1 : " << quadrangleQ3->splitO1() << endl;
  theCout << "split Q4 to Q1 : " << quadrangleQ4->splitO1() << endl;
  theCout << "split Q5 to Q1 : " << quadrangleQ5->splitO1() << endl;
  nbErrors += checkValue(theCout, rootname+"/splittingEXPERIMENTAL.in" , errors, "Test splitting EXPERIMENTAL",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
