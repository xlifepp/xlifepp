/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_Mesh.cpp
  \authors E. Lunéville, Y. Lafranche
  \since 10 may 2012
  \date  23 oct 2012

  Low level tests of Mesh class and related classes.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting informations in a string
*/

//===============================================================================
//library dependences
#include "xlife++-libs.h"
#include "testUtils.hpp"

//===============================================================================
//stl dependences
#include <iostream>
#include <fstream>
#include <vector>

//===============================================================================

using namespace xlifepp;

namespace unit_Mesh {

void test_SubdvMesh(CoutStream& out, bool meshfile);
void test_MeshFromFile(String rootname, CoutStream& out);

Reals gamma(const Point& P, Parameters& par, DiffOpType dop)
{
  Real t=P(1);
  if(dop==_dt) return Reals{16*pi_*std::sin(4*pi_*t), 0., 40.};
  return Reals{2*(1-std::cos(4*pi_*t)), 0., 40*t};
}

Vector<Real> ellPar(const Point& uv, Parameters& pars, DiffOpType d=_id)
{
  Real eps=1E-8, R=1+eps;
  Vector<Real> xy(2);
  Real u=2*uv[0]-1, v=2*uv[1]-1, u2=u*u, v2=v*v, ru=sqrt(1-0.5*u2), rv=sqrt(1-0.5*v2);
  xy[0]=u*rv*R; xy[1]=v*ru*R;
  return xy;
}

Vector<Real> invEllPar(const Point& xy, Parameters& pars, DiffOpType d=_id)
{
  Real eps=1E-8, R=1+eps, R2=R*R;
  Vector<Real> uv(2);
  Real x=xy[0], y=xy[1], x2=x*x, y2=y*y, r=2*sqrt(2);
  uv[0]=0.5*(1+0.5*sqrt(2+(x2-y2)/R2+r*x/R)-0.5*sqrt(2+(x2-y2)/R2-r*x/R));
  uv[1]=0.5*(1+0.5*sqrt(2+(y2-x2)/R2+r*y/R)-0.5*sqrt(2+(y2-x2)/R2-r*y/R));
  return uv;
}

void unit_Mesh(int argc, char* argv[], bool check)
{
  String rootname = "unit_Mesh";
  trace_p->push(rootname);
  verboseLevel(5);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;
  bool meshfile = true; // if true save all meshes

  std::vector<String> sidenames(2, "");
  sidenames[0] = "Sigma_1";
//
  //test P1 2D mesh
  Mesh mesh1dP1(Segment(_xmin=0, _xmax=1, _nnodes=11, _side_names=sidenames), _order=1, _generator=_structured, _name="P1 mesh of [0,1], step=0.01");
  theCout << "------------------- P1 mesh of a segment -------------------------\n";
  theCout << mesh1dP1 << std::endl;
  saveToFile("mesh1dP1.vtk", mesh1dP1);
  saveToFile("mesh1dP1.vtu", mesh1dP1);
  theCout << "\n------------------ P1 mesh of a rectangle ------------------------\n";
  sidenames.resize(4);
  sidenames[0] = "Gamma_1";
  sidenames[1] = "Gamma_2";
  sidenames[2] = "Gamma_3";
  sidenames[3] = "Gamma_4";

  Rectangle R(_xmin=0, _xmax=1, _ymin=1, _ymax=3, _nnodes={6,10}, _side_names=sidenames);
  Mesh mesh2dP1(R, _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[1,3]");
  theCout << mesh2dP1 << std::endl;
  theCout << "\n------------ P1 mesh of a rectangle with sides----------------------\n";
  mesh2dP1.buildSides();
  mesh2dP1.buildVertexElements();
  verboseLevel(9);
  theCout << mesh2dP1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dP1.in", errors, "mesh2dP1", check);
  saveToFile("mesh2dP1.vtk", mesh2dP1);
  saveToFile("mesh2dP1.vtu", mesh2dP1);

  Mesh mesh2dP1r(R, _shape=_triangle, _order=1, _generator=_structured, _split_direction=_right, _name="P1 mesh of [0,1]x[1,3]");
  theCout << mesh2dP1r << std::endl;
  theCout << "\n------------ P1 mesh of a rectangle with sides, right split----------------------\n";
  mesh2dP1r.buildSides();
  mesh2dP1r.buildVertexElements();
  verboseLevel(9);
  theCout << mesh2dP1r << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dP1r.in", errors, "mesh2dP1r", check);
  saveToFile("mesh2dP1r.vtk", mesh2dP1r);
  saveToFile("mesh2dP1r.vtu", mesh2dP1r);

  Mesh mesh2dP1a(R, _shape=_triangle, _order=1, _generator=_structured, _split_direction=_alternate, _name="P1 mesh of [0,1]x[1,3]");
  theCout << mesh2dP1a << std::endl;
  theCout << "\n------------ P1 mesh of a rectangle with sides, alternate split----------------------\n";
  mesh2dP1a.buildSides();
  mesh2dP1a.buildVertexElements();
  verboseLevel(9);
  theCout << mesh2dP1a << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dP1a.in", errors, "mesh2dP1a", check);
  saveToFile("mesh2dP1a.vtk", mesh2dP1a);
  saveToFile("mesh2dP1a.vtu", mesh2dP1a);

  Mesh mesh2dP1s(R, _shape=_triangle, _order=1, _generator=_structured, _split_direction=_random, _name="P1 mesh of [0,1]x[1,3]");
  thePrintStream << mesh2dP1s << std::endl;
  thePrintStream << "\n------------ P1 mesh of a rectangle with sides, random split----------------------\n";
  mesh2dP1s.buildSides();
  mesh2dP1s.buildVertexElements();
  verboseLevel(9);
  thePrintStream << mesh2dP1s << std::endl;
  // nbErrors+=checkValue(theCout, rootname+"/mesh2dP1s.in", errors, "mesh2dP1s", check);  - not checked because random -
  saveToFile("mesh2dP1s.vtk", mesh2dP1s);
  saveToFile("mesh2dP1s.vtu", mesh2dP1s);

  Mesh mesh2dP1c(R, _shape=_triangle, _order=1, _generator=_structured, _split_direction=_cross, _name="P1 mesh of [0,1]x[1,3]");
  thePrintStream << mesh2dP1c << std::endl;
  thePrintStream << "\n------------ P1 mesh of a rectangle with sides, cross split----------------------\n";
  mesh2dP1c.buildSides();
  mesh2dP1c.buildVertexElements();
  verboseLevel(9);
  theCout << mesh2dP1c << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dP1c.in", errors, "mesh2dP1c", check);
  saveToFile("mesh2dP1c.vtk", mesh2dP1c);
  saveToFile("mesh2dP1c.vtu", mesh2dP1c);

  //test Q1 2D mesh
  sidenames[0] = "Sigma_1";
  sidenames[2] = "Sigma_2";
  sidenames[1] = "";
  Mesh mesh2dQ1(Rectangle(_xmin=1, _xmax=2, _ymin=1, _ymax=3, _nnodes={6, 10}, _side_names=sidenames), _shape=_quadrangle, _order=1, _generator=_structured, _name="Q1 mesh of [1,2]x[1,3]");
  mesh2dQ1.domain("Omega").rename("OmegaQ");
  theCout << mesh2dQ1 << std::endl;
  theCout << "\n------------ Q1 mesh of a rectangle with sides----------------------\n";
  mesh2dQ1.buildSides();
  mesh2dQ1.buildVertexElements();
  theCout << mesh2dQ1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dQ1.in", errors, "mesh2dQ1", check);
  saveToFile("mesh2dQ1.vtk", mesh2dQ1);
  saveToFile("mesh2dQ1.vtu", mesh2dQ1);

  theCout << "\n------------ P1 mesh of a rectangle boundary ----------------------\n";
  Mesh mesh1DRect(R, _generator=structured, _shape = segment);
  theCout << mesh1DRect << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh1DRect.in", errors, "mesh1DRect", check);
  saveToFile("mesh1DRect.vtk", mesh1DRect);
  saveToFile("mesh1DRect.vtu", mesh1DRect);

  //merging meshes P1/Q1
  Mesh mesh2dP1Q1(mesh2dP1);
  theCout << "\n-------------------------- copy of P1 mesh --------------------------\n";
  theCout << mesh2dP1Q1 << std::endl;
  mesh2dP1Q1.merge(mesh2dQ1);
  verboseLevel(24);
  theCout << "\n----------------------- merge P1 and Q1 mesh ------------------------\n";
  theCout << mesh2dP1Q1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dP1Q1.in", errors, "mesh2dP1Q1", check);
  saveToFile("mesh2dP1Q1.vtk", mesh2dP1Q1);
  saveToFile("mesh2dP1Q1.vtu", mesh2dP1Q1);

  verboseLevel(12);
  Rectangle R1(_xmin=0, _xmax=1, _ymin=0, _ymax=1, _nnodes={3,3}, _domain_name="omega",_side_names={"g1","g2","g3","g4"});
  Mesh mR1P1(R1, _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Rectangle R2(_xmin=1, _xmax=2, _ymin=0, _ymax=1, _nnodes={3,3}, _domain_name="omega", _side_names={"s1","s2","s3","s4"});
  Mesh mR2Q1(R2, _shape=_quadrangle, _order=1, _generator=_structured, _name="Q1 mesh of [1,2]x[0,1]");
  Mesh mR1R2=merge(mR1P1,mR2Q1);
  thePrintStream<<mR1R2<<eol;
  saveToFile("mR1R2.msh", mR1R2);
  Mesh mR1R2f=merge(mR1P1,mR2Q1,false);
  thePrintStream<<mR1R2f<<eol;
  saveToFile("mR1R2f.msh", mR1R2f);
  R2.sideNames()={"s1","s2","s3","g2"};
  Mesh mR2Q12(R2, _shape=_quadrangle, _order=1, _generator=_structured, _name="Q1 mesh of [1,2]x[0,1]");
  Mesh mR1R2i=merge(mR1P1,mR2Q12);
  thePrintStream<<mR1R2i<<eol;
  saveToFile("mR1R2i.msh", mR1R2i);
  R1.sideNames()={"g","s","g","g"};
  R2.sideNames()={"g","g","g","s"};
  Mesh mR1P1_3(R1, _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Mesh mR2Q1_3(R2, _shape=_quadrangle, _order=1, _generator=_structured, _name="Q1 mesh of [1,2]x[0,1]");
  Mesh mR1R2_3=merge(mR1P1_3,mR2Q1_3);
  thePrintStream<<mR1R2_3<<eol;
  saveToFile("mR1R2_3.msh", mR1R2_3);

  verboseLevel(5);
  theCout << "\n------------------ Q1 mesh of a hexahedron ------------------------\n";
  sidenames.clear(); sidenames.resize(6);
  sidenames[0] = "z=1";  sidenames[1] = "z=5";  sidenames[2] = "y=1";
  sidenames[3] = "y=3";  sidenames[4] = "x=0";  sidenames[5] = "x=1";
  Cuboid cu(_xmin=0, _xmax=1, _ymin=1, _ymax=3, _zmin=1, _zmax=5, _nnodes={3, 5, 9}, _side_names=sidenames);
  Mesh mesh3dQ1(cu, _shape=_hexahedron, _order=1, _generator=_structured, _name="Q1 mesh of [0,1]x[1,3]x[1,5]");
  theCout << mesh3dQ1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh3dQ1.in", errors, "mesh3dQ1 Hexagone", check);
  saveToFile("mesh3dQ1.vtk", mesh3dQ1);
  saveToFile("mesh3dQ1.vtu", mesh3dQ1);
  saveToFile("mesh3dQ1.msh", mesh3dQ1);

  theCout << "\n--------- convert hexahedron mesh to pyramid mesh -------------\n";
  Mesh mesh3DPyramid=split(mesh3dQ1,_pyramid);
  theCout << mesh3DPyramid << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh3dPyramid.in", errors, "mesh3d Pyramid", check);
  saveToFile("mesh3DPyramid.msh", mesh3DPyramid, _aUniqueFile);

  theCout << "\n--------- mesh using pyramids -------------\n";
  Mesh mesh3DPyramid2(cu, _shape=_pyramid, _order=1, _generator=_structured, _name="Q1 mesh of [0,1]x[1,3]x[1,5]");
  theCout << mesh3DPyramid2 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh3dPyramid2.in", errors, "mesh3d Pyramid2", check);
  saveToFile("mesh3DPyramid2.msh", mesh3DPyramid2);

  theCout << "\n--------- mesh using prisms -------------\n";
  Mesh mesh3DPrism2(cu, _shape=_prism, _generator=_structured, _name="Q1 mesh of [0,1]x[1,3]x[1,5]");
  theCout << mesh3DPrism2 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh3dPrism2.in", errors, "mesh3d Prism2", check);
  saveToFile("mesh3DPrism2.msh", mesh3DPrism2);

  theCout << "\n------------ P1 mesh of a cuboid boundary ----------------------\n";
  Mesh mesh2DP1Cuboid(cu, _generator= structured, _shape =_triangle);
  theCout << mesh2DP1Cuboid << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2DP1Cuboid.in", errors, "mesh2DP1Cuboid", check);
  saveToFile("mesh2DP1Cuboid.vtk", mesh2DP1Cuboid);
  saveToFile("mesh2DP1Cuboid.vtu", mesh2DP1Cuboid);

  theCout << "\n------------ cross P1 mesh of a cuboid boundary ----------------------\n";
  Mesh mesh2DP1CrossCuboid(cu, _generator= structured, _shape =_triangle, _split_direction = cross);
  theCout << mesh2DP1CrossCuboid << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2DP1CrossCuboid.in", errors, "mesh2DP1CrossCuboid", check);
  saveToFile("mesh2DP1CrossCuboid.vtk", mesh2DP1CrossCuboid);
  saveToFile("mesh2DP1CrossCuboid.vtu", mesh2DP1CrossCuboid);

  theCout << "\n------------ Q1 mesh of a cuboid boundary ----------------------\n";
  Mesh mesh2DQ1Cuboid(cu, _generator= structured, _shape =_quadrangle);
  theCout << mesh2DQ1Cuboid << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2DQ1Cuboid.in", errors, "mesh2DQ1Cuboid", check);
  saveToFile("mesh2DQ1Cuboid.vtk", mesh2DQ1Cuboid);
  saveToFile("mesh2DQ1Cuboid.vtu", mesh2DQ1Cuboid);

  theCout << "\n------------------ extrusion of 1D mesh ------------------------\n";
  Mesh mesh1dExtrude=extrude(mesh1dP1, Translation(_direction=Reals(0.,0.1)), _layers=10, _naming_domain=1, _naming_section=1);
  theCout << mesh1dExtrude << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh1dExtrude.in", errors, "mesh1d Extrude", check);
  saveToFile("mesh1dExtrude.vtk", mesh1dExtrude);
  saveToFile("mesh1dExtrude.vtu", mesh1dExtrude);

  theCout << "\n------------------ extrusion of 2D mesh ------------------------\n";
  Mesh mesh2dExtrudeP1=extrude(mesh2dP1, Translation(_direction=Reals(0.,0.,0.1)), _layers=10, _naming_domain=1, _naming_section=1, _naming_side=1);
  theCout << mesh2dExtrudeP1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeP1.in", errors, "mesh2d Extrude P1", check);
  saveToFile("mesh2dExtrudeP1.vtk", mesh2dExtrudeP1);
  saveToFile("mesh2dExtrudeP1.vtu", mesh2dExtrudeP1);

  theCout << "\n------------------ extrusion of 2D mesh ------------------------\n";
  Mesh mesh2dExtrudeP1b=extrude(mesh2dP1, Translation(_direction=Reals(0.,0.,0.1)), _layers=10, _naming_domain=2, _naming_section=2, _naming_side=2);
  theCout << mesh2dExtrudeP1b << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeP1b.in", errors, "mesh2d Extrude P1b", check);
  saveToFile("mesh2dExtrudeP1b.vtk", mesh2dExtrudeP1b);
  saveToFile("mesh2dExtrudeP1b.vtu", mesh2dExtrudeP1b);

  theCout << "\n------------------ extrusion of 2D mesh ------------------------\n";
  Mesh mesh2dExtrudeQ1=extrude(mesh2dQ1, Translation(_direction=Reals(0.,0.,0.1)), _layers=10, _naming_domain=1, _naming_section=1, _naming_side=1);
  theCout << mesh2dExtrudeQ1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeQ1.in", errors, "mesh2d Extrude Q1", check);
  saveToFile("mesh2dExtrudeQ1.vtk", mesh2dExtrudeQ1);
  saveToFile("mesh2dExtrudeQ1.vtu", mesh2dExtrudeQ1);

  theCout << "\n------------------ extrusion/rotation of 2D mesh ------------------------\n";
  Mesh mesh2dExtrudeP1Rot=extrude(mesh2dP1, Rotation3d(_center=Point(3.,0.,0.), _axis=Reals(0.,1.,0.), _angle=pi_/10), _layers=10, _naming_domain=1, _naming_section=1, _naming_side=1);
  theCout << mesh2dExtrudeP1Rot << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeP1Rot.in", errors, "mesh2d Extrude P1, Rotation", check);
  saveToFile("mesh2dExtrudeP1Rot.vtk", mesh2dExtrudeP1Rot);
  saveToFile("mesh2dExtrudeP1Rot.vtu", mesh2dExtrudeP1Rot);

  theCout << "\n------------------ extrusion/rotation-translation of 2D mesh ------------------------\n";
  Mesh mesh2dExtrudeP1Hel=extrude(mesh2dP1, Translation(_direction={0.,0.1,0.})*Rotation3d(_center=Point(3.,0.,0.), _axis=Reals(0.,1.,0.), _angle=2.5*pi_/100), _layers=100, _naming_domain=1, _naming_section=1, _naming_side=1);
  theCout << mesh2dExtrudeP1Hel << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeP1Hel.in", errors, "mesh2d Extrude P1, Helix", check);
  saveToFile("mesh2dExtrudeP1Hel.vtk", mesh2dExtrudeP1Hel);
  saveToFile("mesh2dExtrudeP1Hel.vtu", mesh2dExtrudeP1Hel);

  theCout << "\n------------------ extrusion with full rotations of 2D mesh ------------------------\n";
  Mesh mdi(Disk(_center=Point(0.,0.),_v1=Point(1.,0.),_v2=Point(0.,1.),_nnodes=8),_shape=_triangle,_generator=subdiv);
  theCout<<mdi<<eol;
  Mesh mesh2dExtrudeP1T=extrude(mdi, Rotation3d(_center=Point(3.,0.,0.), _axis=Reals(0.,1.,0.), _angle=2*pi_/40), _layers=40, _naming_domain=1, _naming_section=1, _naming_side=1);
  theCout << mesh2dExtrudeP1T << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeP1T.in", errors, "mesh2d Extrude P1, Tore", check);
  saveToFile("mesh2dExtrudeP1T.vtk", mesh2dExtrudeP1T);

  theCout << "\n------------------ extrusion with list of transformations of 2D mesh ------------------------\n";
  Translation *tran=new Translation(_direction={0.2,0.,0.});
  Rotation3d *rot1=new Rotation3d(_center=Point(3.,0.,0.), _axis=Reals(0.,1.,0.), _angle=pi_/40);
  Rotation3d *rot2=new Rotation3d(_center=Point(5.,0.,6.), _axis=Reals(0.,1.,0.), _angle=-pi_/40);
  std::vector<Transformation*> trs(50);
  for(Number i=0;i<20;i++)  { trs[i]=rot1; trs[i+30]=rot2; }
  for(Number i=0;i<10;i++)  { trs[i+20]=tran; }
  Mesh mesh2dExtrudeP1L=extrude(mesh2dP1, trs, _naming_domain=1, _naming_section=1, _naming_side=1);
  theCout << mesh2dExtrudeP1L << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeP1L.in", errors, "mesh2d Extrude P1, list of transformations", check);
  saveToFile("mesh2dExtrudeP1L.vtk", mesh2dExtrudeP1L);

  theCout << "\n------------------ extrusion with a parametrization of 2D mesh ------------------------\n";
  //mdi.domain(0).setNormalOrientation(_towardsInfinite,Point(0.,0.,-1.));
  Mesh mesh2dExtrudeP1S=extrude(mdi, gamma, _layers=100);
  theCout << mesh2dExtrudeP1S << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dExtrudeP1S.in", errors, "mesh2d Extrude P1, Serpentine", check);
  saveToFile("mesh2dExtrudeP1S.vtk", mesh2dExtrudeP1S);

  verboseLevel(2); // because of a random numbering of side elements, decrease verbose level to not display element numbering
  theCout << "\n-------------- parallelepiped mesh with prisms -------------------\n";
  Mesh mesh3dPrism(Parallelepiped(_v1=Point(0.,0.,0.), _v2=Point(0.,0.,1.), _v4=Point(1.,0.,0.), _v5=Point(0.,1.,0.),
                   _nnodes=10, _side_names="Gamma"), _shape=_prism, _order=1, _generator=_structured, _name="Prism mesh of cube");
  theCout << mesh3dPrism << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh3dPrism.in", errors, "mesh3d prism", check);
  saveToFile("mesh3dPrism.vtk", mesh3dPrism);
  verboseLevel(5);

  //test mesh refinement
  theCout << "\n-------------- P1 disk mesh with refinement  -------------------\n";
  Mesh md(Disk(_center=Point(0.,0.),_radius=1.,_nnodes=5),_generator=gmsh,_shape=triangle);
  Mesh md1Refined(md,_refinement_depth=1);
  theCout << md1Refined << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/md1Refined.in", errors, "md1Refined", check);
  Mesh md2Refined(md,_refinement_depth=2);
  theCout << md2Refined << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/md2Refined.in", errors, "md2Refined", check);
  saveToFile("md.vtk", md);
  saveToFile("md1Refined.vtk", md1Refined);
  saveToFile("md2Refined.vtk", md2Refined);
  saveToFile("md.vtu", md);
  saveToFile("md1Refined.vtu", md1Refined);
  saveToFile("md2Refined.vtu", md2Refined);

  theCout << "\n------------------ creation of underlying P1 mesh ------------------------\n";
  //test creation of underlying P1 mesh
  Mesh* mesh2dQ1_P1= mesh2dQ1.createFirstOrderMesh();
  theCout << *mesh2dQ1_P1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dQ1_P1.in", errors, "mesh2dQ1_P1 ", check);

  //test creation of underlying first order mesh
  Mesh& mesh2dQ1ToP1= *mesh2dQ1.createFirstOrderMesh();
  theCout << mesh2dQ1ToP1 << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/mesh2dQ1toP1.in", errors, "mesh2dQ1 to P1 ", check);
  Mesh mesh1dFromBasic(Segment(_v1=Point(0.,0.,0.),_v2=Point(1.,2.,3.),_nnodes=12), _order=1, _generator=_structured);
  Mesh mesh2dP1FromBasic(Rectangle(_xmin=0,_xmax=2,_ymin=0,_ymax=4,_nnodes=12), _shape=_triangle, _order=1, _generator=_structured);
  Mesh mesh2dQ1FromBasic(Rectangle(_xmin=0.,_xmax=2.,_ymin=0.,_ymax=4.,_nnodes=12), _shape=_quadrangle, _order=1, _generator=_structured);
  Mesh mesh3dQ1FromBasic(Cuboid(_xmin=0.,_xmax=2.,_ymin=0.,_ymax=4.,_zmin=0.,_zmax=1.,_nnodes=12), _shape=_hexahedron, _order=1,_generator=_structured);

  // test of meshes built using a subdivision algorithm
  test_SubdvMesh(theCout, meshfile);
  nbErrors+=checkValue(theCout, rootname+"/SubdvMesh.in", errors, "Test SubdvMesh ", check);

  //test of meshes read from a file
  test_MeshFromFile(rootname, theCout);
  nbErrors+=checkValue(theCout, rootname+"/MeshFromFile.in", errors, "Test Mesh From File ", check);

  //test of meshes get from parametrization
  theCout << "\n------------------ creation of mesh from parametrization ------------------------\n";
  Parametrization pe(0,2*pi_,2*cos(x_1),sin(x_1),Parameters(),"2cos(t),sin(t)");
  ParametrizedArc parc(_parametrization = pe,_hsteps =0.01 ,_domain_name="Gamma");
  Mesh mparc(parc, _shape=_segment, _order=1, _generator=_fromParametrization);
  theCout<<mparc<<eol;
  saveToFile("mparc.msh", mparc);

  Disk d1(_center=Point(0.,0.),_radius=1, _nnodes=20);
  Mesh md1(d1, _shape=_triangle, _order=1, _generator=_fromParametrization);
  saveToFile("md1.msh", md1);

  Parameters pars;
  Parametrization par (0,1,0,1, ellPar, invEllPar, pars);
  d1.setParametrization(par);
  Mesh md1b(d1, _shape=_triangle, _order=1, _generator=fromParametrization, _pattern=structured, _split_direction=alternate);
  saveToFile("md1b.msh", md1b);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

//===============================================================================

/*!
   Test functions for subdivsion algorithm.
*/

//! Tests for the sphere
void test_Subdv_Sph(CoutStream& out, bool meshfile, Dimen nboctants = 8) {
  Number order;
  String fname;
  Ball sphu; // unit sphere by default

  out << "\n---------------- Volume P1 mesh of the unit sphere (subdiv 0) -------------------\n";
  order=1;
  fname="mesh3dP1S0VolSph";
  Mesh mesh3dP1S0VolSph(sphu, _shape=_tetrahedron, _order=order, _generator=_subdiv, _name=fname);
  out << mesh3dP1S0VolSph << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S0VolSph);

  out << "\n---------------- Volume P1 mesh of the unit sphere (subdiv 1) -------------------\n";
  sphu = Ball(_center=Point(0.,0.,0.),_radius=1,_nboctants=nboctants,_nnodes=3);
  fname="mesh3dP1S1VolSph";
  Mesh mesh3dP1S1VolSph(sphu, _shape=_tetrahedron, _order=order, _generator=_subdiv, _name="mesh3dP1S1VolSph");
  out << mesh3dP1S1VolSph << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S1VolSph);

  out << "\n---------------- Volume P2 mesh of the unit sphere (subdiv 1) -------------------\n";
  order=2;
  fname="mesh3dP2S1VolSph";
  Mesh mesh3dP2S1VolSph(sphu, _shape=_tetrahedron, _order=order, _generator=_subdiv, _name="mesh3dP2S1VolSph");
  out << mesh3dP2S1VolSph << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP2S1VolSph);

  out << "\n---------------- Surface P1 mesh of the unit sphere (subdiv 0) -------------------\n";
  order=1;
  sphu = Ball(_center=Point(0.,0.,0.),_radius=1.,_nboctants=nboctants,_nnodes=2);
  fname="mesh3dP1S0SurfSph";
  Mesh mesh3dP1S0SurfSph(sphu, _shape=_triangle, _order=order, _generator=_subdiv, _name="mesh3dP1S0SurfSph");
  out << mesh3dP1S0SurfSph << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S0SurfSph);

  out << "\n---------------- Surface P1 mesh of the unit sphere (subdiv 1) -------------------\n";
  sphu = Ball(_center=Point(0.,0.,0.),_radius=1.,_nboctants=nboctants,_nnodes=3);
  fname="mesh3dP1S1SurfSph";
  Mesh mesh3dP1S1SurfSph(sphu, _shape=_triangle, _order=order, _generator=_subdiv, _name="mesh3dP1S1SurfSph");
  out << mesh3dP1S1SurfSph << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S1SurfSph);

  out << "\n---------------- Surface P2 mesh of the unit sphere (subdiv 1) -------------------\n";
  order=2;
  fname="mesh3dP2S1SurfSph";
  Mesh mesh3dP2S1SurfSph(sphu, _shape=_triangle, _order=order, _generator=_subdiv, _name="mesh3dP2S1SurfSph");
  out << mesh3dP2S1SurfSph << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP2S1SurfSph);
}

//! Tests for the cone
void test_Subdv_Cone(CoutStream& out, bool meshfile) {
  std::string fname;
  Number order=1;

  fname = "P1VolMeshTetCone";
  Number n=5;
  Real radius=1.;
  RevCone cone(_center=Point(0.,0.,2.), _radius=radius, _apex=Point(-1.,-1.,0.), _nnodes=n);
  Mesh mesh3dP1S2VolConeT(cone, _shape=_tetrahedron, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Volume P1 mesh of the cone (subdiv 2) with tetrahedrons -------------------\n";
  out << mesh3dP1S2VolConeT << std::endl;
  fname = "P1VolMeshTetTrunk";
  Real radius1=0.6, radius2=1.;
  RevTrunk trunk1(_center1=Point(-1.,-1.,0.), _radius1=radius1, _center2=Point(0.,0.,2.), _radius2=radius2,
                 _end1_shape=_gesCone, _end1_distance=1.5, _end2_shape=_gesEllipsoid, _end2_distance=0.7, _nnodes=n);
  Mesh mesh3dP1S2VolTrunkTE(trunk1, _shape=_tetrahedron, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Volume P1 mesh of the trunk (subdiv 2) with tetrahedrons -------------------\n";
  out << mesh3dP1S2VolTrunkTE << std::endl;

  fname = "P1VolMeshHexTrunk";
  radius1=0.5;//radius1=1.e-15;
  RevTrunk trunk2(_center1=Point(-1.,-1.,0.), _radius1=radius1, _center2=Point(0.,0.,2.), _radius2=radius2, _nnodes=n);
  Mesh mesh3dP1S2VolTrunkH(trunk2, _shape=_hexahedron, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Volume P1 mesh of the trunk (subdiv 2) with hexahedrons -------------------\n";
  out << mesh3dP1S2VolTrunkH << std::endl;

  fname = "P1SurfMeshQuaTrunk";
  RevTrunk trunk3(_center1=Point(-1.,-1.,0.), _radius1=radius1, _center2=Point(0.,0.,2.), _radius2=radius2, _nnodes=n);
  {std::stringstream ss; ss << fname << trunk3.nbSubdomains() << ".tex"; trunk3.teXFilename(ss.str());}
  Mesh mesh3dP1S2SurfConeQ(trunk3, _shape=_quadrangle, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Surface P1 mesh of the cone (subdiv 2) with quadrangles -------------------\n";
  out << mesh3dP1S2SurfConeQ << std::endl;

  fname = "P1SurfMeshTriTrunk";
  {std::stringstream ss; ss << fname << trunk3.nbSubdomains() << ".tex"; trunk3.teXFilename(ss.str());}
  Mesh mesh3dP1S2SurfConeT(trunk3, _shape=_triangle, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Surface P1 mesh of the cone (subdiv 2) with triangles -------------------\n";
  out << mesh3dP1S2SurfConeT << std::endl;
}

//! Tests for the cube
void test_Subdv_Cub(CoutStream& out, bool meshfile, Dimen nboctants = 8) {
  Number order=1;
  Cube cube(_center=Point(0.,0.,0.),_length=2.,_nboctants=nboctants, _nnodes=2); cube.rotate3d(Point(0.,0.,0.), 1.,0.,0., pi_);

  Mesh mesh3dP1S0VolCubT(cube, _shape=_tetrahedron, _order=order, _generator=_subdiv, _name="mesh3dP1S0VolCubT");
  out << "\n---------------- Volume P1 mesh of the cube (subdiv 0) with tetrahedrons -------------------\n";
  out << mesh3dP1S0VolCubT << std::endl;

  Cube cube2(_center=Point(0.,0.,0.),_length=1., _nboctants=2, _nnodes=2);
  std::string fname("mesh3dP1S0VolCubH");
  Mesh mesh3dP1S0VolCubH(cube2, _shape=_hexahedron, _order=3, _generator=_subdiv, _name=fname);
  out << "\n---------------- Volume P1 mesh of the cube (subdiv 0) with hexahedrons -------------------\n";
  out << mesh3dP1S0VolCubH << std::endl;

  fname="mesh3dQ3S0SurfCubQ";
  Mesh mesh3dQ3S0SurfCubQ(cube2, _shape=_quadrangle, _order=3, _generator=_subdiv, _name=fname);
  out << "\n---------------- Volume Q3 mesh of the cube (subdiv 0) with quadrangles -------------------\n";
  out << mesh3dQ3S0SurfCubQ << std::endl;
}

//! Tests for the cylinder
void test_Subdv_Cyl(CoutStream& out, bool meshfile) {
  Number order=1;
  String fname;
  RevCylinder cyl;

  fname="mesh3dP1S0VolCylT";
  Mesh mesh3dP1S0VolCylT(cyl, _shape=_tetrahedron, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Volume P1 mesh of the cylinder (subdiv 0) with tetrahedra -------------------\n";
  out << mesh3dP1S0VolCylT << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S0VolCylT);

  fname="mesh3dP1S0VolCylH";
  Real radius=1.;
  Number n=3;
  RevCylinder cyl2(_center1=Point(0.,0.,0.), _center2=Point(0.,0.,1.), _radius=radius, _nnodes=n);
  Mesh mesh3dP1S0VolCylH(cyl2, _shape=_hexahedron, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Volume P1 mesh of the cylinder (subdiv 0) with hexahedra -------------------\n";
  out << mesh3dP1S0VolCylH << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S0VolCylH);

  fname="mesh3dP1S0SurfCylH";
  Mesh mesh3dP1S0SurfCylH(cyl2, _shape=_quadrangle, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Surface P1 mesh of the cylinder (subdiv 0) with quadrangles -------------------\n";
  out << mesh3dP1S0SurfCylH << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S0SurfCylH);

  fname="mesh3dP1S0SurfCylT";
  Mesh mesh3dP1S0SurfCylT(cyl2, _shape=_triangle, _order=order, _generator=_subdiv, _name=fname);
  out << "\n---------------- Surface P1 mesh of the cylinder (subdiv 0) with triangles -------------------\n";
  out << mesh3dP1S0SurfCylT << std::endl;
  if (meshfile) saveToFile(fname+".msh", mesh3dP1S0SurfCylT);
}

//! Tests for the set of elements
void test_Subdv_TrSet(CoutStream& out, bool meshfile) {
  std::vector<Point> pts;
  pts.push_back(Point(0.,0.,0.));
  pts.push_back(Point(1.,0.,0.));
  pts.push_back(Point(0.,1.,0.3));
  pts.push_back(Point(0.,-1.,0.3));

  std::vector<std::vector<Number> > elems;
  { Number T[]={1,2,3}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={1,4,2}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  std::vector<std::vector<Number> > bounds;
  std::vector<String> names;
  { Number L[]={1,4}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); names.push_back("S1"); }
  { Number L[]={4,2}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); names.push_back("S2"); }
  { Number L[]={2,3}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); names.push_back("S3"); }
  { Number L[]={1,3}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); names.push_back("S4"); }

  Number nbsubdiv(1), order(3);

  SetOfElems sot(pts, elems, bounds, names, _triangle, nbsubdiv);
  sot.teXFilename("P1SurfMeshTriSet.tex");
  Mesh meshTriSet(sot, _shape=_triangle, _order=order, _generator=_subdiv, _name="meshTriSet");
  out << "\n---------------- Surface P3 mesh starting from an initial mesh of triangles (subdiv 1) -------------------\n";
  out << meshTriSet << std::endl;

  pts.clear();
  pts.push_back(Point(0.,0.));
  pts.push_back(Point(0.,1.));
  pts.push_back(Point(1.,0.));
  pts.push_back(Point(1.,1.));
  pts.push_back(Point(1.5,0.5));
  pts.push_back(Point(1.5,1.5));
  pts.push_back(Point(2.,0.));
  pts.push_back(Point(2.,1.));
  pts.push_back(Point(3.,0.));
  pts.push_back(Point(3.,1.));
  pts.push_back(Point(0.,-1.));
  pts.push_back(Point(1.,-1.));
  pts.push_back(Point(1.5,-0.5));
  pts.push_back(Point(1.5,-1.5));
  pts.push_back(Point(2.,-1.));
  pts.push_back(Point(3.,-1.));

  elems.clear();
  { Number T[]={1,3,4,2}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={3,5,6,4}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={5,7,8,6}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={7,9,10,8}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={11,12,3,1}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={12,14,13,3}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={14,15,7,13}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  { Number T[]={15,16,9,7}; elems.push_back(std::vector<Number>(T,T+sizeof(T)/sizeof(Number))); }
  bounds.clear();
  names.clear();
  { Number L[]={1,2,11}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); }
  { Number L[]={11,12,14,15,16}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); }
  { Number L[]={16,9,10}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); }
  { Number L[]={2,4,6,8,10}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); }
  { Number L[]={3,5,7,13}; bounds.push_back(std::vector<Number>(L,L+sizeof(L)/sizeof(Number))); }

  SetOfElems soq(pts, elems, bounds, names, _quadrangle, nbsubdiv);
  soq.teXFilename("P1SurfMeshQuaSet.tex");
  Mesh meshQuaSet(soq, _shape=_quadrangle, _order=order, _generator=_subdiv, _name="meshQuaSet");
  out << "\n---------------- Surface Q3 mesh starting from an initial mesh of quadrangles (subdiv 1) -------------------\n";
  out << meshQuaSet << std::endl;
}

//! Tests for the disk
void test_Subdv_Disk_Sub(CoutStream& out, bool meshfile) {
  Real radius=1.;
  Number order=1;
  Disk pdisk(_center=Point(0.,1.), _radius=radius, _angle1=10*deg_, _angle2=300*deg_, _nnodes=5);
  pdisk.teXFilename("P1SurfMeshTriDisk.tex");
  Mesh meshTriDisk(pdisk, _shape=_triangle, _order=order, _generator=_subdiv, _name="meshTriDisk");
  out << "\n---------------- Surface mesh of a portion of disk (subdiv 1) -------------------\n";
  out << meshTriDisk << std::endl;

  pdisk.teXFilename("P1SurfMeshQuaDisk.tex");
  Mesh meshQuaDisk(pdisk, _shape=_quadrangle, _order=order, _generator=_subdiv, _name="meshQuaDisk");
  out << "\n---------------- Surface mesh of a portion of disk (subdiv 1) -------------------\n";
  out << meshQuaDisk << std::endl;

  Mesh submeshQuaDisk(meshQuaDisk, _refinement_depth=1);
  out << "\n---------------- Subdivision of the previous one -------------------\n";
  out << submeshQuaDisk << std::endl;
}

//! Global test function
void test_SubdvMesh(CoutStream& out, bool meshfile) {
  test_Subdv_Sph(out, meshfile,8); // nboctants = 8;
  test_Subdv_Cub(out, meshfile,7); // nboctants = 7;
  test_Subdv_Cyl(out, meshfile);
  test_Subdv_TrSet(out, meshfile);
  test_Subdv_Disk_Sub(out, meshfile);
  test_Subdv_Cone(out, meshfile);
}
//===============================================================================

/*!
   Test functions for mesh read from a file.
*/
void test_MeshFromFile(String rootname, CoutStream& out) {
  using std::vector;
  // Mesh in Gmsh 2.2 format
  String fn("concentric_sphere.msh");
  Mesh mesh3DGmsh2 = Mesh(inputsPathTo(rootname+"/"+fn), _name="mesh3DGmsh2");
  out << "\n---------------- Read file in Gmsh 2.2 format (file " << fn << ") -------------------\n";
  out << mesh3DGmsh2 << std::endl;

  // Mesh in Gmsh 4.1 format
  fn="concentric_sphere4.msh";
  Mesh mesh3DGmsh4 = Mesh(inputsPathTo(rootname+"/"+fn), _name="mesh3DGmsh4");
  out << "\n---------------- Read file in Gmsh 4.1 format (file " << fn << ") -------------------\n";
  out << mesh3DGmsh4 << std::endl;

  // Mesh in Melina format
  vector<String> vfn;
  vfn.push_back("tr4x4.mel");
  vfn.push_back("qu4x4.mel");
  for (vector<String>::const_iterator it_vfn=vfn.begin(); it_vfn < vfn.end(); it_vfn++) {
    Mesh mesh2DMel = Mesh(inputsPathTo(rootname+"/"+ *it_vfn), _name="mesh2DMel");
    out << "\n---------------- Read file in Melina format (file " << *it_vfn << ") -------------------\n";
    out << mesh2DMel << std::endl;
  }

  fn="unitCube_1e3.ply";
  out << "\n---------------- Read file in PLY format (file " << fn << ") -------------------\n";
  Mesh mesh2DPly(inputsPathTo(rootname+"/"+fn), _name="mesh PLY");
  out << mesh2DPly << std::endl;
  fn="square.vtk";
  out << "\n---------------- Read file in VTK format (file " << fn << ") -------------------\n";
  Mesh meshSquareVtk(inputsPathTo(rootname+"/"+fn), _name="mesh square VTK");
  out << meshSquareVtk << std::endl;
  fn="screen_4.msh";
  out << "\n---------------- Read file in Msh format (file " << fn << ") -------------------\n";
  Mesh mesh2DMsh(inputsPathTo(rootname+"/"+fn), _name="mesh Msh");
  out << mesh2DMsh << std::endl;
  fn="screen_4.mesh";
  out << "\n---------------- Read file in Vizir4 format (file " << fn << ") -------------------\n";
  out << "filename:"<<inputsPathTo(rootname+"/"+fn)<<eol;
  Mesh mesh2DVizir4(inputsPathTo(rootname+"/"+fn), _name="mesh Vizir4");
  out << mesh2DVizir4 << std::endl;
  out << "\n---------------- Read file in Medit format (file " << fn << ") -------------------\n";
  out << "filename:"<<inputsPathTo(rootname+"/"+fn)<<eol;
  Mesh mesh2DMedit(inputsPathTo(rootname+"/"+fn), _name="mesh Medit", _format=_medit);
  out << mesh2DMedit << std::endl;
}

}
