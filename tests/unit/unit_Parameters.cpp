/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Parameters.cpp
	\author E. Lunéville
	\since 1 nov 2011
	\date 11 may 2012

	Low level tests of Parameter and Parameters classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace xlifepp;

namespace unit_Parameters {


struct mystruct {Real values[20];};   // structure example

void unit_Parameters(int argc, char* argv[], bool check)
{
  String rootname = "unit_Parameters";
  trace_p->push(rootname);
  std::stringstream out;                  // string stream receiving results
  out.precision(testPrec);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!------------------------------------------------------------------------------------
  //! Parameter class tests
  //!------------------------------------------------------------------------------------

  //! constructors of Param
  Parameter n(3, "the int n");
  Parameter pi(Real(3.1415926), "the real pi");
  Parameter i(Complex(0., 1.), "the imaginary i");
  Parameter s1("que j'aime à faire apprendre ce nombre utile aux sages", "the string pi");
  mystruct data;
  Parameter pdata(&data, "the struct data");
  ssout << n << pi << i << s1 << std::endl;
  nbErrors += checkValue( n, 3, "the int n", errors, "Parameter n");
  nbErrors += checkValue( pi, pi_, tol, "the real pi", errors, "Parameter pi");
  nbErrors += checkValue( i, Complex(0.,1.), tol, "the imaginary i", errors, "le i complex");
  nbErrors += checkValue( s1, "que j'aime à faire apprendre ce nombre utile aux sages", "the string pi", errors, "la phrase pour pi");
  //! copy constructors
  Parameter m(n), r(pi), j(i), s2(s1), p2(pdata);
  ssout << words("copy constructors") << ":" << std::endl << m << r << j << s2 << std::endl;
  nbErrors += checkValue( n, 3, "the int n", errors, "Parameter m");
  nbErrors += checkValue( pi, pi_, tol, "the real pi", errors, "Parameter r");
  nbErrors += checkValue( i, Complex(0.,1.), tol, "the imaginary i", errors, "le j complex");
  nbErrors += checkValue( s1, "que j'aime à faire apprendre ce nombre utile aux sages", "the string pi", errors, "copie: la phrase pour pi");

  //! assignment operators and set name()
  ssout << words("assignment operator") << " = :" << std::endl;
  Parameter par;
  par = m; ssout << par; nbErrors += checkValue( par, 3, "the int n", errors, "Parameter par=m");
  par = r; ssout << par; nbErrors += checkValue( par, pi_, tol, "the real pi", errors, "Parameter par=pi");
  par = j; ssout << par;  nbErrors += checkValue( par, Complex(0.,1.), tol, "the imaginary i", errors, "par=le i complex");
  par = s2; ssout << par; nbErrors += checkValue( par, "que j'aime à faire apprendre ce nombre utile aux sages", "the string pi", errors, "par=la phrase pour pi");
  par = 1; par.name("un"); ssout << par; nbErrors += checkValue( par, 1, "un", errors, "Parameter par=UN");
  par = 3.1415926; par.name("pi"); ssout << par; nbErrors += checkValue( par, pi_, tol, "pi",errors, "Parameter par=pi");
  par = Complex(0, 1); par.name("i"); ssout << par; nbErrors += checkValue( par, Complex(0.,1.), tol, "i", errors, "Parameter par=i");
  par = "une chaine"; ssout << par; nbErrors += checkValue( par, "une chaine", "i", errors, "Parameter une chaine (pas le bon nom)");
  par = &data;
  ssout << std::endl;

  //! name(), type(), get_i(), get_r(), get_c(), get_s(), get_p()
  ssout << words("accessors") << ":" << std::endl;
  ssout << par.name() << " " << par.type() << " " << par.get_i() << " "
      << par.get_r() << " " << par.get_c() << " " << par.get_s() << "\n";
  nbErrors += checkValue( par.name(), "i", errors, "Par.name()");
  nbErrors += checkValue( tostring(par.type()), tostring(_pointer), errors, "Par.type()");
  nbErrors += checkValue( par.get_i(), 1, errors, "Par.get_i()");
  nbErrors += checkValue( par.get_r(), pi_, tol, errors, "Par.get_r()");
  nbErrors += checkValue( par.get_c(), Complex(0.,1.), tol, errors, "Par.get_c()");
  nbErrors += checkValue( par.get_s(), "une chaine", errors, "Par.get_s()");

  //! automatic parameter cast to int, real, complex, string or pointer
  //! illegal casting operations are not tested, they cause breakdown
  Int vn = n;
  Real vr = r;
  Complex vj = j;
  String vs2 = s2;
  ssout << words("automatic cast") << " :\n";
  nbErrors += checkValue( n, vn,errors, "Int vn=n");
  ssout << "vn=" << vn << " vr=" << vr << " vj=" << vj << " vs2=" << vs2 << "\n";
  nbErrors += checkValue( r, vr, tol,errors, "Real vr=r");
  nbErrors += checkValue( j, vj, tol,errors, "Complex vj=j");
  nbErrors += checkValue( s2, vs2, errors, "String vs2=s2");
  vr = n;            // autocast integer parameter to real
  Complex cn = n;  // autocast integer or real parameter to complex
  Complex ci = pi; // ci=parameter; with no type declaration does not work (ambiguity)
  ssout << "vr=" << vr << " cn=" << cn << " ci=" << ci << "\n";
  // parameter explicit cast to int, real, complex, string or pointer
  // illegal casting operations are not tested, they cause breakdown
  ssout << "vn=" << integer(n) << " vr=" << real(r) << " vj=" << cmplx(j) << " vs2=" << str(s2) << "\n";

  //! algebraic operation (only legal operations are checked)
  ssout << words("test of") << " " << words("assignment operator") << " +=\n";
  n += 1; ssout << n;  nbErrors += checkValue( n, 4,"the int n", errors, "n+=1");
  n += m; ssout << n; nbErrors += checkValue( n, 7,"the int n", errors, "n+=m");
  n += 1.5; ssout << n; nbErrors += checkValue( real(n), 8.5,tol,"the int n (Real)", errors, "n+=1.5");// as 1.5 is a real, n becomes a real parameter
  n += i; ssout << n; nbErrors += checkValue( n, Complex(8.5,1.), tol, errors, "n+=i"); // as i is a complex parameter, n becomes a complex parameter
  r += 1; ssout << r; nbErrors += checkValue( r, 4.14159, tol, errors, "r+=1");
  r += 1.5; ssout << r; nbErrors += checkValue( r, 5.64159, tol, errors, "r+=1.5");
  r += pi; ssout << r;  nbErrors += checkValue( r, 8.7831852, tol, errors, "r+=pi");
  r += Complex(0., 2.); ssout << r;  nbErrors += checkValue( r, Complex(8.7831852,2.) , tol, errors, "r+=2i");// add a complex, r becomes a complex parameter
  i += 1; ssout << i; nbErrors += checkValue( i, Complex(1,1),tol,"the imaginary i (Complex)", errors, "i+=1");
  i += 2.; ssout << i; nbErrors += checkValue( i, Complex(3,1),tol, "the imaginary i (Complex)", errors, "i+=2");
  i += Complex(1., 2.); ssout << i;  nbErrors += checkValue( i, Complex(4,3),tol, "the imaginary i (Complex)", errors, "i+= Complex(1., 2.)");
  i += i; ssout << i; nbErrors += checkValue( i, Complex(8,6),tol, "the imaginary i (Complex)", errors, "i+=i");
  s2 += " other chars"; ssout << s2;
  s2 += s2; ssout << s2;
  nbErrors += checkValue( s2, "que j'aime à faire apprendre ce nombre utile aux sages other charsque j'aime à faire apprendre ce nombre utile aux sages other chars" ,"the string pi", errors, "la phrase");
  ssout << std::endl;

  ssout << words("test of") << " " << words("assignment operator") << " -=\n";
  n = Parameter (3, "the int n"); ssout << n;  nbErrors += checkValue( n, 3,"the int n", errors, "n=3");
  r = Parameter (Real(3.1415926), "the real pi"); ssout << r; nbErrors += checkValue( r, 3.1415926,tol, "the real pi", errors, "r=pi");
  i = Parameter (Complex(0., 1.), "the imaginary i"); ssout << i; nbErrors += checkValue( i, Complex(0,1),tol, "the imaginary i", errors, "i=i");
  n -= m; ssout << n; nbErrors += checkValue( n, 0, tol, "the int n", errors, "n-=m");
  n -= 1.5; ssout << n; nbErrors += checkValue( n, -1.5, tol, "the int n", errors, "n-=1.5");// as 1.5 is a real, n becomes a real parameter
  n -= i; ssout << n; nbErrors += checkValue( n, Complex(-1.5,-1), tol, "the int n", errors, "n-=i");// as i is a complex parameter, n becomes a complex parameter
  r -= 1; ssout << r; nbErrors += checkValue( r, 2.1415926, tol, "the real pi", errors, "r-=1");
  r -= 1.5; ssout << r; nbErrors += checkValue( r, 0.6415926, tol, "the real pi", errors, "r-=1.5");
  r -= pi; ssout << r; nbErrors += checkValue( r, -2.5 , tol, "the real pi", errors, "r-=pi");
  r -= Complex(0., 2.); ssout << r;  nbErrors += checkValue( r, -Complex(2.5,2) , tol, "the real pi", errors, "r-=2i"); // add a complex, r becomes a complex parameter
  i -= 1; ssout << i; nbErrors += checkValue( i, Complex(-1,1), tol, "the imaginary i", errors, "i-=1");
  i -= 2.; ssout << i; nbErrors += checkValue( i, Complex(-3,1), tol, "the imaginary i", errors, "i-=2.");
  i -= Complex(1., 2.); ssout << i; nbErrors += checkValue( i, Complex(-4,-1), tol, "the imaginary i", errors, "i-=(1+2i).");
  i -= i; ssout << i; nbErrors += checkValue( i, Complex(0,0), tol, "the imaginary i", errors, "i-=i.");
  ssout << std::endl;


  ssout << words("test of") << " " << words("assignment operator") << " *=\n";
  n = Parameter (3, "the int n"); out << n; nbErrors += checkValue( n, 3,"the int n", errors, "n=3");
  r = Parameter (Real(3.1415926), "the real pi"); out << r; nbErrors += checkValue( r, 3.1415926,tol, "the real pi", errors, "r=pi");
  i = Parameter (Complex(0., 1.), "the imaginary i"); out << i;  nbErrors += checkValue( i, Complex(0,1),tol, "the imaginary i", errors, "i=i");
  n *= m; ssout << n;  nbErrors += checkValue( n, 9, "the int n", errors, "n*=m");
  n *= 1.5; ssout << n; nbErrors += checkValue( n, 13.5, tol, "the int n", errors, "n*=1.5");// as 1.5 is a real, n becomes a real parameter
  n *= i; ssout << n; nbErrors += checkValue( n, Complex(0,13.5), tol, "the int n", errors, "n*=i"); // as i is a complex parameter, n becomes a complex parameter
  r *= 1; ssout << r; nbErrors += checkValue( r, pi_, tol, "the real pi", errors, "r*=1");
  r *= 1.5; ssout << r; nbErrors += checkValue( r, 4.7123889, tol, "the real pi", errors, "r*=1.5");
  r *= pi; ssout << r;nbErrors += checkValue( r, 14.8044, tol, "the real pi", errors, "r*=pi");
  r *= Complex(0., 2.);  ssout << r; nbErrors += checkValue( r, Complex(0,29.6088), tol, "the real pi", errors, "r*=pi"); // add a complex, r becomes a complex parameter
  i *= 1; ssout << i; nbErrors += checkValue( i, Complex(0,1), tol, "the imaginary i", errors, "i*=1");
  i *= 2.; ssout << i; nbErrors += checkValue( i, Complex(0,2), tol, "the imaginary i", errors, "i*=2");
  i *= Complex(1., 2.); ssout << i; nbErrors += checkValue( i, Complex(-4,2) , tol, "the imaginary i", errors, "i*=(1+2i)");
  i *= i; ssout << i; nbErrors += checkValue( i, Complex(12,-16), tol, "the imaginary i", errors, "i*=i");

  ssout << words("test of") << " " << words("assignment operator") << " /=\n";
  n = Parameter (3, "the int n");  nbErrors += checkValue( n, 3,"the int n", errors, "n=3");
  r = Parameter (Real(3.1415926), "the real pi"); out << r; nbErrors += checkValue( r, 3.1415926,tol, "the real pi", errors, "r=pi");
  i = Parameter (Complex(0., 1.), "the imaginary i"); out << i; nbErrors += checkValue( i, Complex(0,1),tol, "the imaginary i", errors, "i=i");
  n /= m; ssout << n;nbErrors += checkValue( n, 1, "the int n", errors, "n/=m");
  n /= 1.5; ssout << n; nbErrors += checkValue( n, 0.666666667, tol, "the int n", errors, "n/=1.5");// as 1.5 is a real, n becomes a real parameter
  n /= i; ssout << n; nbErrors += checkValue( n, Complex(0, -0.666667), tol, "the int n", errors, "n/=i"); // as i is a complex parameter, n becomes a complex parameter
  r /= 1; ssout << r; nbErrors += checkValue( r, pi_, tol, "the real pi", errors, "r/=1");
  r /= 1.5; ssout << r; nbErrors += checkValue( r, 2.094395066, tol, "the real pi", errors, "r/=1.5");
  r /= pi; ssout << r; nbErrors += checkValue( r,0.666667, tol, "the real pi", errors, "r/=pi");
  r /= Complex(0., 2.); ssout << r; nbErrors += checkValue( r,Complex(0,-0.333333), tol, "the real pi", errors, "r/=2i");// add a complex, r becomes a complex parameter
  i /= 1; ssout << i; nbErrors += checkValue( i, Complex(0,1), tol, "the imaginary i", errors, "i/=1");
  i /= 2.; ssout << i; nbErrors += checkValue( i, Complex(0,0.5), tol, "the imaginary i", errors, "i/=2");
  i /= Complex(1., 2.); ssout << i; nbErrors += checkValue( i, Complex(0.2, 0.1) , tol, "the imaginary i", errors, "i/=(1+2i)");
  i /= i; ssout << i; nbErrors += checkValue( i, Complex(1,0), tol, "the imaginary i", errors, "i/=i");
  ssout << std::endl;

  ssout << words("test of operator") << " +\n";
  n = Parameter (3, "the int n"); ssout << n; nbErrors += checkValue( n, 3,"the int n", errors, "n=3");
  r = Parameter (Real(3.1415926), "the real pi"); ssout << r; nbErrors += checkValue( r, 3.1415926,tol, "the real pi", errors, "r=pi");
  i = Parameter (Complex(0., 1.), "the imaginary i");ssout << i; nbErrors += checkValue( i, Complex(0,1),tol, "the imaginary i", errors, "i=i");
  par = n + 1; ssout << par;  nbErrors += checkValue( par, 4, "the int n", errors, "n+1");          // par is a integer parameter
  par = n + m; ssout << par;  nbErrors += checkValue( par, 6, "the int n", errors, "n+m");           // par is a integer parameter
  par = n + 1.5; ssout << par;  nbErrors += checkValue( par, 4.5, tol,"the int n", errors, "n+1.5");        // par is a real parameter
  par = n + Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(3, 1) , tol, "the int n", errors, "n+i");// par is a complex parameter
  par = n + r; ssout << par; nbErrors += checkValue( par, 6.1415926 , tol, "the int n", errors, "n+pi");        // par is a real parameter
  par = n + i; ssout << par; out << par; nbErrors += checkValue( par, Complex(3, 1) , tol, "the int n", errors, "n+i");           // par is a complex parameter
  par = r + 1; ssout << par;  nbErrors += checkValue( par, 4.1415962 , tol, "the real pi", errors, "r+1");           // par is a real parameter
  par = r + m; ssout << par; nbErrors += checkValue( par, 6.1415962 , tol, "the real pi", errors, "r+m");           // par is a real parameter
  par = r + 1.5; ssout << par;  nbErrors += checkValue( par, 4.6415962 , tol, "the real pi", errors, "r+1.5");         // par is a real parameter
  par = r + Complex(0., 1.); out << par; nbErrors += checkValue( par, Complex(3.1415962, 1) , tol, "the real pi", errors, "r+Complex(0,1)");   // par is a complex parameter
  par = r + r; ssout << par; nbErrors += checkValue( par, 6.2831852 , tol, "the real pi", errors, "r+r");          // par is a real parameter
  par = r + i; ssout << par; nbErrors += checkValue( par, Complex(3.1415962, 1) , tol, "the real pi", errors, "r+i");           // par is a complex parameter
  par = i + 1; ssout << par; nbErrors += checkValue( par, Complex(1, 1) , tol, "the imaginary i", errors, "i+1");           // par is a complex parameter
  par = i + m; ssout << par; nbErrors += checkValue( par, Complex(3, 1) , tol, "the imaginary i", errors, "i+m");           // par is a complex parameter
  par = i + 1.5; ssout << par; nbErrors += checkValue( par, Complex(1.5, 1) , tol, "the imaginary i", errors, "i+1.5");         // par is a complex parameter
  par = i + Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(0, 2) , tol, "the imaginary i", errors, "i+Complex(0,1)");  // par is a complex parameter
  par = i + r; ssout << par;  nbErrors += checkValue( par, Complex(3.1415926, 1) , tol, "the imaginary i", errors, "i+r");          // par is a complex parameter
  par = i + i; ssout << par;  nbErrors += checkValue( par, Complex(0, 2) , tol, "the imaginary i", errors, "i+i");          // par is a complex parameter
  s2 = Parameter("a string", "s2");
  par = s2 + " " + s2; ssout << par; nbErrors += checkValue( par, "a string a string" , "s2", errors, "s2+s2");   // par is a string
  ssout << std::endl;

  ssout << words("test of operator") << " -\n";
  n = Parameter (3, "the int n"); ssout << n; nbErrors += checkValue( n, 3,"the int n", errors, "n=3");
  r = Parameter (Real(3.1415926), "the real pi"); ssout << r; nbErrors += checkValue( r, 3.1415926,tol, "the real pi", errors, "r=pi");
  i = Parameter (Complex(0., 1.), "the imaginary i"); ssout << i; nbErrors += checkValue( i, Complex(0,1),tol, "the imaginary i", errors, "i=i");
  par = n - 1; ssout << par; nbErrors += checkValue( par, 2, "the int n", errors, "n-1");           // par is a integer parameter
  par = n - m; ssout << par; nbErrors += checkValue( par, 0, "the int n", errors, "n-m");           // par is a integer parameter
  par = n - 1.5; ssout << par;  nbErrors += checkValue( par, 1.5, tol,"the int n", errors, "n-1.5");        // par is a real parameter
  par = n - Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(3, -1) , tol, "the int n", errors, "n-cmplex(0,1)");    //par is a complex parameter
  par = n - r; ssout << par;  nbErrors += checkValue( par, -0.1415962, tol,"the int n", errors, "n-r");          // par is a real parameter
  par = n - i; ssout << par;  nbErrors += checkValue( par, Complex(3, -1) , tol, "the int n", errors, "n-i");            // par is a complex parameter
  par = r - 1; ssout << par;  nbErrors += checkValue( par, 2.1415926, tol, "the real pi", errors, "r-1");          // par is a real parameter
  par = r - m; ssout << par;  nbErrors += checkValue( par, 0.1415926, tol, "the real pi", errors, "r-m");          // par is a real parameter
  par = r - 1.5; ssout << par;  nbErrors += checkValue( par, 1.6415926, tol, "the real pi", errors, "r-1.5");        // par is a real parameter
  par = r - Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(3.1415926,-1), tol, "the real pi", errors, "r-Complex(0,1)");// par is a complex parameter
  par = r - r; ssout << par; nbErrors += checkValue( par, 0., tol, "the real pi", errors, "r-r");           // par is a real parameter
  par = r - i; ssout << par;  nbErrors += checkValue( par, Complex(3.1415926,-1), tol, "the real pi", errors, "r-i");           // par is a complex parameter
  par = i - 1; ssout << par; nbErrors += checkValue( par, Complex(-1,1), tol, "the imaginary i", errors, "i-1");           // par is a complex parameter
  par = i - m; ssout << par; nbErrors += checkValue( par, Complex(-3,1), tol, "the imaginary i", errors, "i-m");           // par is a complex parameter
  par = i - 1.5; ssout << par;  nbErrors += checkValue( par, Complex(-1.5,1), tol, "the imaginary i", errors, "i-1.5");        // par is a complex parameter
  par = i - Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(0,0), tol, "the imaginary i", errors, "i-Complex(0,1)");  // par is a complex parameter
  par = i - r; ssout << par; nbErrors += checkValue( par, Complex(-3.1415926,1.), tol, "the imaginary i", errors, "i-r");           // par is a complex parameter
  par = i - i; ssout << par;  checkValue( par, Complex(0,0), tol, "the imaginary i", errors, "i-i");          // par is a complex parameter
  ssout << std::endl;

  ssout << words("test of operator") << " *\n";
  par = n * 1; ssout << par; nbErrors += checkValue( par, 3,"the int n", errors, "n*1");           // par is a integer parameter
  par = n * m; ssout << par; nbErrors += checkValue( par, 9,"the int n", errors, "n*m");           // par is a integer parameter
  par = n * 1.5; ssout << par; nbErrors += checkValue( par, 4.5, tol,"the int n", errors, "n*1.5");         // par is a real parameter
  par = n * Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(0,3), tol,"the int n", errors, "n*Complex(0,1)");// par is a complex parameter
  par = n * r; ssout << par; nbErrors += checkValue( par, 9.4247778 , tol,"the int n", errors, "n*r");           // par is a real parameter
  par = n * i; ssout << par; nbErrors += checkValue( par, Complex(0,3), tol,"the int n", errors, "n*i");           // par is a complex parameter
  par = r * 1; ssout << par; nbErrors += checkValue( par, 3.1415926, tol,"the real pi", errors, "r*1");            // par is a real parameter
  par = r * m; ssout << par; nbErrors += checkValue( par, 9.4247778, tol,"the real pi", errors, "r*m");           // par is a real parameter
  par = r * 1.5; ssout << par;  nbErrors += checkValue( par, 4.7123889, tol,"the real pi", errors, "r*1.5");        // par is a real parameter
  par = r * Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(0,3.1415926), tol,"the real pi", errors, "r*i"); // par is a complex parameter
  par = r * r; ssout << par;  nbErrors += checkValue( par, 9.86960406437, tol,"the real pi", errors, "r*r");          // par is a real parameter
  par = r * i; ssout << par; nbErrors += checkValue( par, Complex(0,3.1415926), tol,"the real pi", errors, "r*i");           // par is a complex parameter
  par = i * 1; ssout << par; nbErrors += checkValue( par, Complex(0,1), tol, "the imaginary i", errors, "i*1");           // par is a complex parameter
  par = i * m; ssout << par; nbErrors += checkValue( par, Complex(0,m), tol, "the imaginary i", errors, "i*m");            // par is a complex parameter
  par = i * 1.5; ssout << par; nbErrors += checkValue( par, Complex(0,1.5), tol, "the imaginary i", errors, "i*1.5");         // par is a complex parameter
  par = i * Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(-1,0), tol, "the imaginary i", errors, "i*Complex(0,1)");// par is a complex parameter
  par = i * r; ssout << par;  nbErrors += checkValue( par, Complex(0,3.1415926), tol, "the imaginary i", errors, "i*r");          // par is a complex parameter
  par = i * i; ssout << par;  nbErrors += checkValue( par, Complex(-1,0), tol, "the imaginary i", errors, "i*i");            // par is a complex parameter
  ssout << std::endl;

  ssout << words("test of operator") << " /\n";
  par = n / 1; ssout << par;  nbErrors += checkValue( par, 3,"the int n", errors, "n/1");           // par is a integer parameter
  par = n / m; ssout << par;  nbErrors += checkValue( par, 1,"the int n", errors, "n/3");          // par is a integer parameter
  par = n / 1.5; ssout << par;  nbErrors += checkValue( par, 2, tol,"the int n", errors, "n/1.5");        // par is a real parameter
  par = n / Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(0,-3), tol,"the int n", errors, "n/Complex(0,1)"); // par is a complex parameter
  par = n / r; ssout << par;  nbErrors += checkValue( par, 0.954929674, tol,"the int n", errors, "n/r");          // par is a real parameter
  par = n / i; ssout << par; nbErrors += checkValue( par, Complex(0,-3), tol,"the int n", errors, "n/i");            // par is a complex parameter
  par = r / 1; ssout << par; nbErrors += checkValue( par, 3.1415926, tol,"the real pi", errors, "r/1");           // par is a real parameter
  par = r / m; ssout << par; nbErrors += checkValue( par, 1.04719753333 , tol,"the real pi", errors, "r/m");           // par is a real parameter
  par = r / 1.5; ssout << par;  nbErrors += checkValue( par, 2.094395066 , tol,"the real pi", errors, "r/1.5");        // par is a real parameter
  par = r / Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(0,-3.1415926) , tol,"the real pi", errors, "r/Complex(0,1)");// par is a complex parameter
  par = r / r; ssout << par; nbErrors += checkValue( par, 1 , tol,"the real pi", errors, "r/r");           // par is a real parameter
  par = r / i; ssout << par; nbErrors += checkValue( par, Complex(0,-3.1415926) , tol,"the real pi", errors, "r/i");            // par is a complex parameter
  par = i / 1; ssout << par; nbErrors += checkValue( par, Complex(0,1) , tol,"the imaginary i", errors, "i/1");            // par is a complex parameter
  par = i / m; ssout << par; nbErrors += checkValue( par, Complex(0,0.3333333) , tol,"the imaginary i", errors, "i/m");          // par is a complex parameter
  par = i / 1.5; ssout << par;   nbErrors += checkValue( par, Complex(0,0.666667) , tol,"the imaginary i", errors, "i/1.5");       // par is a complex parameter
  par = i / Complex(0., 1.); ssout << par; nbErrors += checkValue( par, Complex(1,0) , tol,"the imaginary i", errors, "i/i");// par is a complex parameter
  par = i / r; ssout << par;  nbErrors += checkValue( par, Complex(0,0.318309891614) , tol,"the imaginary i", errors, "i/r");          // par is a complex parameter
  par = i / i; ssout << par;   nbErrors += checkValue( par, Complex(1,0) , tol,"the imaginary i", errors, "i/i" );         // par is a complex parameter
  ssout << std::endl;

  //! comparison operator ==
  Vector<Number> VboolEe(20);
  ssout << words("test of operator") << " ==\n";
  ssout << n << " == 1 ->" << (n == 1) << "\n"; VboolEe(1)=(n == 1);
  ssout << n << " == 3 ->" << (n == 3) << "\n"; VboolEe(2)=(n == 3);
  ssout << n << " == 1.5 ->" << (n == 1.5) << "\n"; VboolEe(3)=(n == 1.5);
  ssout << n << " == 3. ->" << (n == 3.) << "\n"; VboolEe(4)=(n == 3.);
  ssout << n << " == (0,1) ->" << (n == Complex(0., 1.)) << "\n"; VboolEe(5)=(n ==  Complex(0., 1.) );
  ssout << n << " == (3,0) ->" << (n == Complex(3, 0)) << "\n"; VboolEe(6)=(n ==  Complex(3, 0) );
  ssout << n << " == 'trois' ->" << (n == String("trois")) << "\n"; VboolEe(7)=(n == String("trois") );
  ssout << n << " == '3' ->" << (n == String("3")) << "\n"; VboolEe(8)=(n == String("3") );
  ssout << n << " == '3' ->" << (n == "3") << "\n"; VboolEe(9)=(n == "3");
  ssout << n << " == m ->" << (n == m) << "\n"; VboolEe(10)=(n == m);
  ssout << n << " == r ->" << (n == r) << "\n"; VboolEe(11)=(n == r);
  ssout << n << " == i ->" << (n == i) << "\n"; VboolEe(12)=(n == i);
  ssout << r << " == 3 ->" << (r == 3) << "\n"; VboolEe(13)=(r == 3);
  ssout << r << " == 1.5 ->" << (r == 1.5) << "\n"; VboolEe(14)=(r == 1.5);
  ssout << r << " == 3.1415926 ->" << (r == 3.1415926) << "\n"; VboolEe(15)=(r == 3.1415926);
  ssout << r << " == (0,1) ->" << (r == Complex(0., 1.)) << "\n"; VboolEe(16)=(r == Complex(0., 1.));
  ssout << r << " == (3.1415926,0) ->" << (r == Complex(3.1415926, 0)) << "\n"; VboolEe(16)=(r == Complex(3.1415926, 0));
  ssout << r << " == 'trois' ->" << (r == String("trois")) << "\n"; VboolEe(17)=(r == String("trois"));
  ssout << r << " == '3.1415926' ->" << (r == String("3.1415926")) << "\n"; VboolEe(18)=(r == String("3.1415926"));
  ssout << r << " == '3.1415926' ->" << (r == "3.1415926") << "\n"; VboolEe(19)=(r == "3.1415926");
  nbErrors+= checkValues(VboolEe, rootname+"/OpEE.in", 0, errors, "Operator ==", check);

  //! comparison operator <=
  Vector<Number> VboolLe(9);
  ssout << words("test of operator") << " <=\n";
  ssout << n << " <= 1 ->" << (n <= 1) << "\n"; VboolLe(1)=(n <= 1);
  ssout << n << " <= 3 ->" << (n <= 3) << "\n"; VboolLe(2)=(n <= 3);
  ssout << n << " <= 1.5 ->" << (n <= 1.5) << "\n"; VboolLe(3)=(n <= 1.5);
  ssout << n << " <= 3. ->" << (n <= 3.) << "\n"; VboolLe(4)=(n <= 3.);
  ssout << n << " <= m ->" << (n <= m) << "\n"; VboolLe(5)=(n <= m);
  ssout << n << " <= r ->" << (n <= r) << "\n"; VboolLe(6)=(n <= r);
  ssout << r << " <= 3 ->" << (r <= 3) << "\n"; VboolLe(7)=(r <= 3);
  ssout << r << " <= 1.5 ->" << (r <= 1.5) << "\n"; VboolLe(8)=(r <= 1.5);
  ssout << r << " <= 3.1415926 ->" << (r <= 3.1415926) << "\n"; VboolLe(9)=(r <= 3.1415926);
  nbErrors+= checkValues(VboolLe, rootname+"/OpLE.in", 0, errors, "Operator <=", check);

  //! comparison operator >=
  Vector<Number> VboolGe(9);
  ssout << words("test of operator") << " >=\n";
  ssout << n << " >= 1 ->" << (n >= 1) << "\n"; VboolGe(1)=(n >= 1);
  ssout << n << " >= 3 ->" << (n >= 3) << "\n"; VboolGe(2)=(n >= 3);
  ssout << n << " >= 1.5 ->" << (n >= 1.5) << "\n"; VboolGe(3)=(n >= 1.5);
  ssout << n << " >= 3. ->" << (n >= 3.) << "\n"; VboolGe(4)=(n >= 3.);
  ssout << n << " >= m ->" << (n >= m) << "\n"; VboolGe(5)=(n >= m);
  ssout << n << " >= r ->" << (n >= r) << "\n"; VboolGe(6)=(n >= r);
  ssout << r << " >= 3 ->" << (r >= 3) << "\n"; VboolGe(7)=(r >= 3);
  ssout << r << " >= 1.5 ->" << (r >= 1.5) << "\n"; VboolGe(8)=(r >= 1.5);
  ssout << r << " >= 3.1415926 ->" << (r >= 3.1415926) << "\n"; VboolGe(9)=(r >= 3.1415926);
  nbErrors+= checkValues(VboolGe, rootname+"/OpGE.in", 0, errors, "Operator >=", check);

  //! comparison operator <
  Vector<Number> VboolLt(9);
  ssout << words("test of operator") << " <\n";
  ssout << n << " < 1 ->" << (n < 1) << "\n"; VboolLt(1)=(n < 1);
  ssout << n << " < 3 ->" << (n < 3) << "\n"; VboolLt(2)=(n < 3);
  ssout << n << " < 1.5 ->" << (n < 1.5) << "\n"; VboolLt(3)=(n < 1.5);
  ssout << n << " < 3. ->" << (n < 3.) << "\n"; VboolLt(4)=(n < 3.);
  ssout << n << " < m ->" << (n < m) << "\n"; VboolLt(5)=(n < m);
  ssout << n << " < r ->" << (n < r) << "\n"; VboolLt(6)=(n < r);
  ssout << r << " < 3 ->" << (r < 3) << "\n"; VboolLt(7)=(r < 3);
  ssout << r << " < 1.5 ->" << (r < 1.5) << "\n"; VboolLt(8)=(r < 1.5);
  ssout << r << " < 3.1415926 ->" << (r < 3.1415926) << "\n";VboolLt(9)=(r < 3.1415926);
  nbErrors+= checkValues(VboolLt, rootname+"/OpLT.in", 0, errors, "Operator <", check);

  //! comparison operator >
  Vector<Number> VboolGt(9);
  ssout << words("test of operator") << " >\n";
  ssout << n << " > 1 ->" << (n > 1) << "\n"; VboolGt(1)=((n > 1));
  ssout << n << " > 3 ->" << (n > 3) << "\n"; VboolGt(2)=(n > 3);
  ssout << n << " > 1.5 ->" << (n > 1.5) << "\n"; VboolGt(3)=(n > 1.5);
  ssout << n << " > 3. ->" << (n > 3.) << "\n"; VboolGt(4)=(n > 3.);
  ssout << n << " > m ->" << (n > m) << "\n"; VboolGt(5)=(n > m);
  ssout << n << " > r ->" << (n > r) << "\n"; VboolGt(6)=(n > r);
  ssout << r << " > 3 ->" << (r > 3) << "\n"; VboolGt(7)=(r > 3);
  ssout << r << " > 1.5 ->" << (r > 1.5) << "\n"; VboolGt(8)=(r > 1.5);
  ssout << r << " > 3.1415926 ->" << (r > 3.1415926) << "\n"; VboolGt(9)=(r > 3.1415926);
  nbErrors+= checkValues(VboolGt, rootname+"/OpGT.in", 0, errors, "Operator >", check);

  //! test parameter of type vector or matrix
  std::vector<Real> vecr(10,1.);
  Parameter pvecr(vecr,"pvecr");
  ssout<<"vecr="<<vecr<<" -> "<<pvecr<<eol;
  nbErrors+=checkValue( pvecr,  rootname+"/pvecr.in", 0.0001, "pvecr", errors, "vecr", check);
  std::vector<Complex> vecc(10,Complex(0.,1.));
  Parameter pvecc(vecc,"pvecc");
  ssout<<"vecc="<<vecc<<" -> "<<pvecc<<eol;
  nbErrors+=checkValue( pvecc,  rootname+"/pvecc.in", 0.0001, "pvecc", errors, "vecc", check);
  Parameter pmatr_1(Matrix<Real>(3,_hilbertMatrix),"matr_1");
  Parameter pmatc_1(Matrix<Complex>(3,_hilbertMatrix),"matc_1");
  Parameter pvecr_1(RealVector(5,0.5),"vecr_1");
  Parameter pvecr_2(ComplexVector(5,Complex(0,1.)),"vecr_2");
  Parameter pmatr_2(RealMatrix(3,_hilbertMatrix),"matr_2");
  Parameter pmatc_2(ComplexMatrix(3,_hilbertMatrix),"matc_2");
  ssout<<pmatr_1<<eol;
  nbErrors+=checkValue( pmatr_1,  rootname+"/pmatr_1.in", 0.0001, "matr_1", errors, "matr_1", check);
  nbErrors+=checkValue( pmatc_1,  rootname+"/pmatc_1.in", 0.0001, "matc_1", errors, "matc_1", check);
  nbErrors+=checkValue( pvecr_1,  rootname+"/pvecr_1.in", 0.0001, "vecr_1", errors, "vecr_1", check);
  nbErrors+=checkValue( pvecr_2,  rootname+"/pvecr_2.in", 0.0001, "vecr_2", errors, "vecr_2", check);
  nbErrors+=checkValue( pmatr_2,  rootname+"/pmatr_2.in", 0.0001, "matr_2", errors, "matr_2", check);
  nbErrors+=checkValue( pmatc_2,  rootname+"/pmatc_2.in", 0.0001, "matc_2", errors, "matc_2", check);
  pvecr = std::vector<Complex>(10,Complex(1.,2.));
  ssout<<"pvecr = vector<Complex> -> "<<pvecr<<eol;
  pvecr = pvecc;
  ssout<<"pvecr_2 = pvecc -> "<<pvecr_2<<eol;
  ssout<<"pvecr_2.get_cv() -> "<<pvecr_2.get_cv()<<eol;

  #if __cplusplus >= 201103L    //c++11 enabled
  //!test initilalizer list
   Parameter names={"Gamma","Sigma","Sigma0"}; ssout<<names;
   nbErrors+=checkValue(names, {"Gamma","Sigma","Sigma0"}, "", errors, "names");
   names={"eric","nicolas"}; ssout<<" "<<names<<eol;
   std::vector<String> Noms(2); Noms[0]="eric"; Noms[1]="nicolas";
   nbErrors+=checkValue(names, Noms, "", errors, "names");
   Parameter nums={10,20,30,40};ssout<<nums<<eol;;
   std::vector<Int> Nums(4); Nums[0]=10; Nums[1]=20;Nums[2]=30; Nums[3]=40;
   nbErrors+=checkValue(nums, Nums, "", errors, "nums");
   nums={8,7,6}; ssout<<" "<<nums<<eol; Nums[0]=8; Nums[1]=7;Nums[2]=6;
   Parameter res={0.1,0.2,0.3}; ssout<<res;
   std::vector<Real> Res(3); Res[0]=0.1; Res[1]=0.2;Res[2]=0.3;
   nbErrors+=checkValue(res, Res, "", errors, "Res");
   res={3.1415,2.718};out<<" "<<res<<eol;
  #endif
  //!------------------------------------------------------------------------------------
  //!Parameters class tests
  //!------------------------------------------------------------------------------------

  //! Parameters constructors
  out << words("test of class") << " : Parameters\n";
  Parameters pars1;                                    // empty list
  Parameters pars2(1, "one"); ssout << pars2;            // contains an integer parameter
  Parameters pars3(3.1415926, "pi"); ssout << pars3;     // contains an real parameter
  Parameters pars4(Complex(0., 1.), "i"); ssout << pars4; // contains a complex parameter
  Parameters pars5("iron", "material"); ssout << pars5;  // contains a string parameter
  Parameters pars6(&data, "pointer to data");          // contains a void * parameter
  Parameters pars7(sqrt(2)); ssout << pars7;             // contains a real parameter, default name "parameter1"
  Parameter p(sqrt(2), "sqrt(2)");                     // define the real parameter p
  Parameters pars8(p);                                 // create par8 from the real parameter p
  Parameters pars9(true, "ok");            // contains a boolean parameter
  nbErrors += checkValue( pars2(1),1,"one", errors, "pars2");
  nbErrors += checkValue( pars3(1),3.1415926, tol,"pi", errors, "pars3");
  nbErrors += checkValue( pars4(1),Complex(0,1), tol,"i", errors, "pars4");
  nbErrors += checkValue( pars5(1),"iron","material", errors, "pars2");
  nbErrors += checkValue( pars7(1),sqrt(2), tol,"parameter1", errors, "pars7");
  nbErrors += checkValue( pars8(1),sqrt(2), tol,"sqrt(2)", errors, "pars8");
  if (!( pars9(1).get_b() == true) )
    {
    	nbErrors++; errors+= "Pb with pars9";
    };

  //! add parameters to the parameters list (append)
  //! no check for adresse pointer
  Vector<String> Vstring(8);
  pars1 << p; Vstring[0]=tostring(pars1);
  pars1 << sqrt(3) << Complex(1, 1);Vstring[1]=tostring(sqrt(3));Vstring[2]=tostring(Complex(1,1));     // append data with no name (default name parametern)
  pars1 << "aluminium" << &data; Vstring[3]=tostring("aluminium");/*Vstring[4]=tostring(&data)*/;   // append data with no name (default name parametern)
  pars1 << pars5(1); Vstring[4]=tostring(pars5(1));           // append first parameter of pars5 list
  pars1<<Parameter(RealVector(5,1.),"un"); Vstring[5]=tostring(Parameter(RealVector(5,1.),"un"));
  pars1<<Parameter(Complexes(5,i_),"i_"); Vstring[6]=tostring(Parameter(Complexes(5,i_),"i_"));
  pars1<<Parameter(Matrix<Complex>(3,_hilbertMatrix),"hilbert matrix"); Vstring[6]=tostring(Parameter(Matrix<Complex>(3,_hilbertMatrix)));
  nbErrors+=checkValues(Vstring, rootname+"/pars1.in", errors, "pars1", check);
  nbErrors+=checkValues(pars1, rootname+"/pars1CLS.in", tol, errors, "pars1", check);

  //! access to a parameter of the listpars1("hilbert matrix")
  Vector<String> VstringA(12);
  Parameter q1 = pars1(1); VstringA[0]=tostring(q1); ssout << q1;                       // using rank access
  Parameter q2 = pars1("parameter2"); VstringA[1]=tostring(q2); ssout << q2;            // using name index access
  Parameter q3 = pars1(p); VstringA[2]=tostring(q3); ssout << q3<<eol;                  // using parameter reference (use parameter name!)
  ssout<<"pars1(\"hilbert matrix\") : "<<pars1("hilbert matrix")<<eol; VstringA[3]=tostring(pars1("hilbert matrix"));
  ssout << "pars1 " << words("contains") << " p ? " << pars1.contains(p) << "\n"; VstringA[4]=tostring(pars1.contains(p));              // check if p is in list pars1 (pointer comparison!)
  ssout << "pars1 " << words("contains") << " 'pi' ? " << pars1.contains("pi") << "\n";  VstringA[5]=tostring(pars1.contains("pi"));       // check if parameter named pi is in list pars1
  ssout << "pars1 " << words("contains") << " 'pi' ? " << pars1.contains(String("pi")) << "\n"; VstringA[6]=tostring(pars1.contains(String("pi"))); // the same
  //! access with autocast parameter->value type
  Real x = pars1("sqrt(2)"); VstringA[7]=tostring(x); out << "pars1('sqrt(2)')=" << x << "\n"; // access to the real value of a real parameter
  String s = pars1("material"); VstringA[8]=s; out << "pars1('material')=" << s<<eol;    // access to the string value of a string parameter
  RealVector wr=pars1("un"); VstringA[9]=tostring(wr);
  Complexes wc=pars1("i_"); VstringA[10]=tostring(wc);
  Matrix<Complex> hmat=pars1("hilbert matrix"); VstringA[11]=tostring(hmat);
  nbErrors+=checkValues( VstringA ,  rootname+"/addParameters.in", errors, "Add parameters", check);

  ssout << "Testing set method" << "\n";
  pars2.set("logarithm nepen", 2.718);  // append a real parameter
  pars2.set("i", Complex(0.,1.));       // append a complex parameter
  pars2.set("tree", String("apple"));   // append a string parameter
  pars2.set("boolean", true);           // append a boolean parameter
  pars2.set("vec_un", Reals(5,1.));// append a real vector parameter
  pars2.set("vec_i_", Complexes(5,i_));// append a complex vector parameter
  nbErrors+=checkValues(pars2, rootname+"/pars2CLS.in", tol, errors, "pars2", check);


  ssout << "Testing get method" << "\n";
  Vector<String> VstringB(8);
  ssout << "pars2 " << pars2 << "\n";
  int one(1); one = pars2.get("one", one); ssout << one << "\n";VstringB[0]=tostring(one);
  Real logNep = 0.0; logNep = pars2.get("logarithm nepen", logNep); ssout << logNep << "\n"; VstringB[1]=tostring(logNep);
  Complex cValue; cValue = pars2.get("i", cValue); ssout << cValue << "\n";VstringB[2]=tostring(cValue);
  String tree;    tree = pars2.get("tree", tree); ssout << tree << "\n";VstringB[3]=tree;
  bool b = true; b = pars2.get("boolean", b); ssout << b << "\n";VstringB[4]=tostring(b);
  String plant("rose"); plant = pars2.get("Plant", plant); ssout << plant << "\n";VstringB[5]=plant;
  Reals vun;
  vun=pars2.get("vec_un",vun);
  ssout << "pars2.get(\"vec_un\",vun) -> vun = "<<vun << "\n"; VstringB[6]=tostring(vun);
  Complexes vi_;
  vi_=pars2.get("vec_i_",vi_);
  ssout << "pars2.get(\"vec_i_\",vi_) -> vi_ = "<<vi_ << "\n"; VstringB[7]=tostring(vi_);
  nbErrors+=checkValues( VstringB ,  rootname+"/getParameters.in", errors, "Get parameters", check);

  //!test high level function involving parameters
  std::vector<String> sidenames(4, "");
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=0.5,_ymin=0,_ymax=1,_nnodes=Numbers(3,6),_domain_name="Omega-",_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  Mesh mesh2d_p(Rectangle(_xmin=0.5,_xmax=1,_ymin=0,_ymax=1,_nnodes=Numbers(3,6),_domain_name="Omega+",_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  mesh2d.merge(mesh2d_p);
  mesh2d.printInfo();

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
