/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHout ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_KdTree.cpp
	\author E. Lunéville
	\since 14 mar 2014
	\date 14 mar 2014

	Low level tests of KdTree class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_KdTree {

void createPoints(std::vector<Point>& points, String refFilename, Number N, Number dim, bool check);

void unit_KdTree(int argc, char* argv[], bool check)
{
  String rootname = "unit_KdTree";
  trace_p->push(rootname);
  std::srand(1);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!test 1D points cloud - regular -
  Number N=20;
  std::vector<Point> points(N);
  std::vector<Point>::iterator itp=points.begin();
  for(Number i=0;i<N;i++, itp++) *itp=Point(Real(i));
  KdTree<Point> k1tree(points);
  theCout<<"--------------------------  K1Tree regular points ----------------------------"<<eol;
  theCout<<"depth tree = "<<k1tree.depth()<<eol;
  theCout<<k1tree;
  itp=points.begin();
  for(Number i=0;i<N;i++, itp++)
     {
         theCout<<"search "<<*itp<<" -> "<<*k1tree.searchNearest(*itp)<<eol;
         theCout<<"search "<<*itp+0.1<<" -> "<<*k1tree.searchNearest(*itp+0.1)<<eol;
         theCout<<"search "<<*itp+0.5<<" -> "<<*k1tree.searchNearest(*itp+0.5)<<eol;
         theCout<<"search "<<*itp+0.9<<" -> "<<*k1tree.searchNearest(*itp+0.9)<<eol;
     }
  nbErrors+=checkValue(theCout, rootname+"/K1tree.in", errors, " K1Tree regular points ", check);
  //!test 1D points cloud - random -
  createPoints( points, rootname+"/K1tree_r.dat",  N, 1, check);
  theCout << "random points 1D : "<< points << eol;
  KdTree<Point> k1tree_r(points);
  theCout<<"--------------------------  K1Tree random points ----------------------------"<<eol;
  theCout<<"depth tree = "<<k1tree_r.depth()<<eol;
  theCout<<k1tree_r;
  itp=points.begin();
  for(Number i=0;i<N;i++, itp++)
     {
         theCout<<"search "<<*itp<<" -> "<<*k1tree_r.searchNearest(*itp)<<eol;
         theCout<<"search "<<*itp+0.1<<" -> "<<*k1tree_r.searchNearest(*itp+0.1)<<eol;
         theCout<<"search "<<*itp+0.5<<" -> "<<*k1tree_r.searchNearest(*itp+0.5)<<eol;
         theCout<<"search "<<*itp+0.9<<" -> "<<*k1tree_r.searchNearest(*itp+0.9)<<eol;
     }
  nbErrors+=checkValue(theCout, rootname+"/K1tree_r.in", errors, " K1Tree random points ", check);
  //!test 2D points cloud - regular -
  N=5;
  points.resize(N*N);
  itp=points.begin();
  for(Number i=0;i<N;i++)
    for(Number j=0;j<N;j++, itp++)
      *itp=Point(Real(i),Real(j));
  KdTree<Point> k2tree(points);
  theCout<<"--------------------------  K2Tree regular points ----------------------------"<<eol;
  theCout<<"depth tree = "<<k2tree.depth()<<eol;
  theCout<<k2tree;
  nbErrors+=checkValue(theCout, rootname+"/K2tree.in", errors, " K2Tree regular points ", check);

  N=10;
  points.resize(N*N);
  itp=points.begin();
  createPoints( points, rootname+"/K2tree_r.dat",  N, 2, check);
  theCout << "random points 2D : "<<points<<eol;
  KdTree<Point> k2tree_r(points);
  theCout<<"--------------------------  K2Tree random points ----------------------------"<<eol;
  theCout<<"depth tree = "<<k2tree_r.depth()<<eol;
  theCout<<k2tree_r;
  std::ofstream ofs("points.dat");
  for(itp=points.begin();itp!=points.end(); itp++) ofs<<(*itp)[0]<<" "<<(*itp)[1]<<" \n";
  ofs.close();
  k2tree_r.printBoxes("boxes.dat", Box<Real>(0,2*N,0,2*N));
  nbErrors+= checkValues("boxes.dat", rootname+"/boxes.in", tol, errors, "boxes.dat", check);

  itp=points.begin();
  Number nbs=0;
  ofs.open("locate.dat");
  for(Number i=0;i<N*N;i++, itp++)
     {
         theCout<<"search "<<*itp<<" -> "<<*k2tree_r.searchNearest(*itp)<<eol;
         //nbs+=KdNode<Point>::countSearch;
         theCout<<"search "<<*itp + 0.1<<" -> "<<*k2tree_r.searchNearest(*itp+0.1)<<eol;
         //nbs+=KdNode<Point>::countSearch;
         theCout<<"search "<<*itp+0.5<<" -> "<<*k2tree_r.searchNearest(*itp+0.5)<<eol;
         //nbs+=KdNode<Point>::countSearch;
         Point P=*itp+0.9, Q=*k2tree_r.searchNearest(P);
         theCout<<"search "<<P<<" -> "<<Q<<eol;
         ofs<<P(1)<<" "<<P(2)<<" "<<Q(1)<<" "<<Q(2)<<eol;
         //nbs+=KdNode<Point>::countSearch;
     }
  ofs.close();
  // theCout<<"average of number of steps in search = "<<Real(nbs)/(4*N*N)<<eol;
  nbErrors+=checkValue(theCout, rootname+"/K2tree_r.in", errors, " K2Tree random points ", check);

  N=5;
  points.resize(N*N*N);
  itp=points.begin();
  createPoints( points, rootname+"/K3tree_r.dat",  N, 3, check);
  theCout << "random points 3D : "<< points << eol;
  KdTree<Point> k3tree_r(points);
  theCout<<"--------------------------  K3Tree random points ----------------------------"<<eol;
  theCout<<"depth tree = "<<k3tree_r.depth()<<eol;
  theCout<<k3tree_r;
  itp=points.begin();
  nbs=0;
  for (Number i=0; i<N*N*N; i++, itp++)
     {
         theCout<<"search "<<*itp<<" -> "<<*k3tree_r.searchNearest(*itp)<<eol;
         //nbs+=KdNode<Point>::countSearch;
         theCout<<"search "<<*itp + 0.1<<" -> "<<*k3tree_r.searchNearest(*itp+0.1)<<eol;
         //nbs+=KdNode<Point>::countSearch;
         theCout<<"search "<<*itp+0.5<<" -> "<<*k3tree_r.searchNearest(*itp+0.5)<<eol;
         //nbs+=KdNode<Point>::countSearch;
         Point P=*itp+0.9, Q=*k3tree_r.searchNearest(P);
         theCout<<"search "<<P<<" -> "<<Q<<eol;
         //nbs+=KdNode<Point>::countSearch;
     }
//  theCout<<"average of number of steps in search = "<<Real(nbs)/(4*N*N*N)<<eol;
  nbErrors+=checkValue(theCout, rootname+"/K3tree_r.in", errors, " K3Tree random points ", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}


void createPoints(std::vector<Point> &points, String refFilename, Number N, Number dim, bool check)
{
  std::vector<Point>::iterator itp=points.begin();
  Real PP,PQ,PR;
  if (check)
  {
  	String ligne;
    std::ifstream fin(inputsPathTo(refFilename).c_str());
    if (fin)
      {
       while ( !fin.eof() )
         {
       	 if (dim==1)
       	 {
              while ( std::getline( fin, ligne ) )
              {
                std::istringstream is(ligne);
                is >> PP;
               *itp=Point(PP);
                itp++;
              }
          }
          else if (dim==2)
          {
              while ( std::getline( fin, ligne ) )
              {
                std::istringstream is(ligne);
                is >>PP>>PQ;
               *itp=Point(PP,PQ);
                itp++;
               }
           }
           else
           {
              while ( std::getline( fin, ligne ) )
              {
                std::istringstream is(ligne);
                is >>PP>>PQ>>PR;
               *itp=Point(PP,PQ,PR);
                itp++;
               }


           }
       }
    }
    fin.close();
  }
  else
  {
     std::ofstream fout(inputsPathTo(refFilename).c_str());
     if (dim==1)
     {
     	for(Number i=0;i<N-1;i++, itp++)
     	{
       	    PP=Real(std::rand()%(2*N));
   		  	fout << PP<<eol;
     		*itp=Point(PP);
     	}
     	PP=Real(std::rand()%(2*N));
    	fout << PP;;
       *itp=Point(PP);
     }
     else if (dim==2)
     {
     	for(Number i=0;i<N;i++)
     	  for(Number j=0;j<N;j++, itp++)
      	  {
         	 PP=Real(std::rand()%(2*N));
        	 PQ=Real(std::rand()%(2*N));
       	 	 fout << PP<<" "<<PQ;
       	 	 if ((i!=N)&&(j!=N)) fout<<eol;
     	 	*itp=Point(PP,PQ);
       	  }
     }
     else
     {
     	for(Number i=0;i<N;i++)
      	   for(Number j=0;j<N;j++)
        	 for(Number k=0;k<N;k++, itp++)
         	 {
           		PP=Real(std::rand()%(2*N));
    	        PQ=Real(std::rand()%(2*N));
         	    PR=Real(std::rand()%(2*N));
       	   		fout << PP<<" "<<PQ<<" "<<PR;
           		if ((i!=N)&&(j!=N)&&(k!=N)) fout<<eol;
     	  	   *itp=Point(PP,PQ,PR);
       		 }
     }
     fout.close();
  }
}
}
