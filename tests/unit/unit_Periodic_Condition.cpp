/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Periodic_Condition.cpp
	\since 08 apr 2014
	\date  09 mar 2014

	Test periodic condition on Laplace problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Periodic_Condition
{

Complex alpha;

Real pc_f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return (4.*pi_*pi_*y*(y-1.)-2.)*sin(2.*pi_*x);
}

Real pc_solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return y*(y-1.)*sin(2.*pi_*x);
}

Vector<Real> mapPM(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> Q(P);
  Q(1)-=1.;
  return Q;
}

Vector<Real> mapPMy(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> Q(P);
  Q(2)-=1.;
  return Q;
}

Real pc_solex2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return (1+y*y)*sin(2.*pi_*x);
}

Real pc_f2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return (4.*pi_*pi_*(1+y*y)-2.)*sin(2.*pi_*x);
}

Real gM(const Point& P, Parameters& pa = defaultParameters)
{
  return pc_solex2(P);
}

Real gP(const Point& P, Parameters& pa = defaultParameters)
{
  return pc_solex2(P);
}

Real pc_solex3(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return cos(2*pi_*x)*sin(2.*pi_*y);
}

Real pc_f3(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return (1+8.*pi_*pi_)*cos(2*pi_*x)*sin(2.*pi_*y);
}

Complex pc_solex5(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Complex a=2*pi_*i_+log(alpha);
  return exp(a*x)*exp(a*y);
}
Complex pc_f5(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Complex a=2*pi_*i_+log(alpha);
  return (1.-2*a*a)*exp(a*x)*exp(a*y);
}

void unit_Periodic_Condition(int argc, char* argv[], bool check)
{
  String rootname = "unit_Periodic_Condition";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(40);
  String errors;
  Real eps=0.01;
  Number nbErrors= 0;

  //!create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=1";
  sidenames[2] = "y=1"; sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=20,_domain_name="omega",_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("omega");
  Domain sigmaM=mesh2d.domain("x=0");
  Domain sigmaP=mesh2d.domain("x=1");
  Domain gammaM=mesh2d.domain("y=0");
  Domain gammaP=mesh2d.domain("y=1");

  defineMap(sigmaP,sigmaM,mapPM);  //useful to periodic condition
  defineMap(gammaP,gammaM,mapPMy);

  //create interpolation
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");
  std::cout<<"mesh and space created cputime="<<cpuTime()<<eol;

  //!=========================================================================================
  //!   Periodic condition and homogeneous Dirichlet conditions
  //!=========================================================================================
  //!create bilinear form and linear form
  BilinearForm auv=intg(omega,grad(u)|grad(v));
  BilinearForm uv=intg(omega,u*v);
  TermMatrix M(uv, _name="M");
  LinearForm fv=intg(omega,pc_f*v);
  EssentialConditions ecs= (u|gammaM = 0) & (u|gammaP = 0) & ((u|sigmaP) - (u|sigmaM) = 0);
  TermMatrix A(auv, ecs, _name="A");
  TermVector B(fv, _name="B");
  //!solve linear system AX=F using factorization
  TermMatrix Af;
  factorize(A,Af);
  TermVector U=directSolve(Af,B);
  //!compute errorvtk
  TermVector Uex(u,omega,pc_solex);
  saveToFile("Uex_omega.vtk",Uex,_format=_vtk, _aUniqueFile);
  nbErrors += checkValuesL2( U, Uex, M, eps,  errors, "test scalar periodic condition (u|gammaM = 0) & (u|gammaP = 0) & ((u|sigmaP) - (u|sigmaM) = 0)");

  //! eigen problems with periodic condition
  TermMatrix Ae(auv, ecs, _reduction=ReductionMethod(_pseudoReduction,0.,1000.), _name="Ae");
  TermMatrix Me(uv, ecs, _reduction=ReductionMethod(_pseudoReduction,0.,1.), _name="Me");
  EigenElements eigs=eigenSolve(Ae,Me,_nev=10,_which="SM");
  out<<"   P1 first eigenvalue error : "<<abs(eigs.value(2)-4*pi_*pi_)/(4*pi_*pi_)<<eol;
  TermVector e=eigs.vector(2);  // second
  e.applyEssentialConditions(ecs);
  Complex v00=e.evaluate(Point(0.,0.)).asComplex(), v10=e.evaluate(Point(1.,0.)).asComplex(),
          v01=e.evaluate(Point(0.,1.)).asComplex(), v11=e.evaluate(Point(1.,1.)).asComplex(),
          va=e.evaluate(Point(0.5,0.)).asComplex(), vb=e.evaluate(Point(0.5,1.)).asComplex(),
          vc=e.evaluate(Point(0.,0.5)).asComplex(), vd=e.evaluate(Point(1.,0.5)).asComplex();
  Real er=abs(v00-v10)+abs(v00-v01)+abs(v00-v11)+abs(va-vb)+abs(vc-vd);
  out<<"   P1 first eigenvector, check condition at some points: "<<er<<eol;

  //!=========================================================================================
  //!   Periodic condition and non homogeneous Dirichlet conditions
  //!=========================================================================================
  EssentialConditions ecs2= (u|gammaM = gM) & (u|gammaP = gP) & ((u|sigmaP) - (u|sigmaM) = 0);
  TermMatrix A2(auv, ecs2, _name="A2");
  TermVector B2(intg(omega,pc_f2*v), _name="B2");
  TermVector U2=directSolve(A2,B2);
  TermVector Uex2(u,omega,pc_solex2);
  nbErrors += checkValuesL2( U2, Uex2, M, eps,  errors, "test scalar periodic condition (u|gammaM = gM) & (u|gammaP = gP) & ((u|sigmaP) - (u|sigmaM) =0)");

  //!=========================================================================================
  //!   Double periodic condition (homogenous)
  //!=========================================================================================
  EssentialConditions ecs3= ((u|gammaP) -(u|gammaM) = 0) & ((u|sigmaP) - (u|sigmaM) = 0);
  TermMatrix A3(auv+uv, ecs3, _name="A3");
  TermVector B3(intg(omega,pc_f3*v), _name="B3");
  TermVector U3=directSolve(A3,B3);
  TermVector Uex3(u,omega,pc_solex3);
  nbErrors += checkValuesL2( U3, Uex3, M, 10*eps,  errors, "test scalar double periodic condition ((u|gammaP) -(u|gammaM) = 0) & ((u|sigmaP) - (u|sigmaM) = 0)");

  TermMatrix A3e(auv,ecs3, _reduction=ReductionMethod(_pseudoReduction,0.,1000.), _name="A3e");
  TermMatrix M3e(uv, ecs3, _reduction=ReductionMethod(_pseudoReduction,0.,1.), _name="M3e");
  EigenElements eigs3=eigenSolve(A3e,M3e,_nev=10,_which="SM");
  out<<"   P1 first eigenvalue error : "<<abs(eigs3.value(2)-4*pi_*pi_)/(4*pi_*pi_)<<eol;
  TermVector e3=eigs3.vector(2);  // second
  e3.applyEssentialConditions(ecs3);
  Complex v300=e3.evaluate(Point(0.,0.)).asComplex(), v310=e3.evaluate(Point(1.,0.)).asComplex(),
          v301=e3.evaluate(Point(0.,1.)).asComplex(), v311=e3.evaluate(Point(1.,1.)).asComplex(),
          v3a=e3.evaluate(Point(0.5,0.)).asComplex(), v3b=e3.evaluate(Point(0.5,1.)).asComplex(),
          v3c=e3.evaluate(Point(0.,0.5)).asComplex(), v3d=e3.evaluate(Point(1.,0.5)).asComplex();
  Real er3=abs(v300-v310)+abs(v300-v301)+abs(v300-v311)+abs(v3a-v3b)+abs(v3c-v3d);
  nbErrors += checkValue(er3, 0., eps, errors, "second eigenvector, check condition at some points");
  nbErrors += checkValue(eigs3.value(2), 4.*pi_*pi_, eps*(4.*pi_*pi_), errors, "second eigenvalue");

  //!=========================================================================================
  //!   Double periodic condition (homogenous) in P2
  //!=========================================================================================
  Space V2(_domain=omega, _interpolation=_P2, _name="V2");
  Unknown u2(V2, _name="u2"); TestFunction v2(u2, _name="v2");
  EssentialConditions ecs4= ((u2|gammaP) -(u2|gammaM) = 0) & ((u2|sigmaP) - (u2|sigmaM) = 0);
  BilinearForm auv2=intg(omega,grad(u2)|grad(v2))+intg(omega,u2*v2);
  TermMatrix A4(auv2, ecs4, _name="A4");
  TermVector B4(intg(omega,pc_f3*v2), _name="B4");
  TermVector U4=directSolve(A4,B4);
  TermVector Uex4(u2,omega,pc_solex3);
  TermMatrix M2(intg(omega,u2*v2), _name="M2");
  nbErrors += checkValuesL2( U4, Uex4, M2, eps,  errors, "test scalar double periodic condition ((u|gammaP) -(u|gammaM) = 0) & ((u|sigmaP) - (u|sigmaM) = 0)");

  //!eigen problem
  TermMatrix A4e(intg(omega,grad(u2)|grad(v2)), ecs4, _reduction=ReductionMethod(_pseudoReduction,0.,1000.), _name="A4e");
  TermMatrix M4e(intg(omega,u2*v2), ecs4, _reduction=ReductionMethod(_pseudoReduction,0.,1.), _name="M4e");
  EigenElements eigs4=eigenSolve(A4e,M4e,_nev=10,_which="SM");
  out<<"   P2 first eigenvalue error : "<<abs(eigs4.value(2)-4*pi_*pi_)/(4*pi_*pi_)<<eol;
  TermVector e4=eigs4.vector(2);  // second
  e4.applyEssentialConditions(ecs4);
  Complex v400=e4.evaluate(Point(0.,0.)).asComplex(), v410=e4.evaluate(Point(1.,0.)).asComplex(),
          v401=e4.evaluate(Point(0.,1.)).asComplex(), v411=e4.evaluate(Point(1.,1.)).asComplex(),
          v4a=e4.evaluate(Point(0.5,0.)).asComplex(), v4b=e4.evaluate(Point(0.5,1.)).asComplex(),
          v4c=e4.evaluate(Point(0.,0.5)).asComplex(), v4d=e4.evaluate(Point(1.,0.5)).asComplex();
  Real er4=abs(v400-v410)+abs(v400-v401)+abs(v400-v411)+abs(v4a-v4b)+abs(v4c-v4d);
  nbErrors += checkValue(er4, 0., eps, errors, "second eigenvector, check condition at some points");
  nbErrors += checkValue(eigs4.value(2), 4.*pi_*pi_, eps*(4.*pi_*pi_), errors, "second eigenvalue");
  
  //!=========================================================================================
  //!   Double quasi-periodic condition (homogenous) in P1
  //!=========================================================================================
  alpha=1.;
  EssentialConditions ecs5= ((u|gammaP) -alpha*(u|gammaM) = 0) & ((u|sigmaP) - alpha*(u|sigmaM) = 0);
  BilinearForm auv5=intg(omega,grad(u)|grad(v))+intg(omega,u*v);
  TermMatrix A5(auv5, ecs5, _name="A5");
  TermVector B5(intg(omega,pc_f5*v), _name="B5");
  thePrintStream<<A5<<eol<<B5<<eol;
  TermVector U5=directSolve(A5,B5);
  TermVector Uex5(u,omega,pc_solex5);
  nbErrors += checkValuesL2( U5, Uex5, M, eps,  errors, "test scalar periodic condition P1,");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
