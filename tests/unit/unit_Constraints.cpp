/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Constraints.cpp
  \author E. Lunéville
	\since 02 mar 2014
	\date  02 mar 2014

  low level tests of Constraints class
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;


namespace unit_Constraints {

//data function
Real g(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}
Real g2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1);
  return (2*x-1)*(2*x-1);
}

Vector<Real> g3(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> v(2);
  v(1)=1.; v(2)=0.;
  return v;
}

void unit_Constraints(int argc, char* argv[], bool check)
{
  String rootname = "unit_Constraints";
  trace_p->push(rootname);
  
  Number nbErrors = 0;
  String errors;
  Real eps=0.0001;
  verboseLevel(40);

  //!create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=1";
  sidenames[2] = "y=1"; sidenames[3] = "x=0";
  //! mesh2d used : Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=6,_side_names=sidenames),_triangle,1,_structured
  Mesh  mesh2d(inputsPathTo(rootname+"/mesh2d.msh"));
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=1");
  Domain sigmaM=mesh2d.domain("x=0");
  Domain gammaM=mesh2d.domain("y=0");
  Domain gammaP=mesh2d.domain("y=1");

  Function f1(g,"g"), f2(g2,"g2");

  //!create interpolation
  Space V(_domain=omega,_FE_type=Lagrange, _order=1, _name="V");
  Unknown u(V, _name="u");

  theCout<<" ----------------------  single scalar Dirichlet condition --------------------------------"<<eol;
  EssentialConditions ssecs = (u|gammaM = f1);
  SetOfConstraints scons = buildConstraints(ssecs);
  theCout<<scons;
  nbErrors+=checkValue(theCout, rootname+"/SingleScalarDirichletCdtn.in", errors, "Single scalar Dirichlet condition", check);

  theCout<<" ----------------------  multiple scalar Dirichlet condition --------------------------------"<<eol;
  EssentialConditions msecs = (u|gammaM = f1) & (u|sigmaM = f1) & (u|sigmaP = f1) & (u|gammaP = f2);
  SetOfConstraints mcons = buildConstraints(msecs);
  theCout<<mcons;
  nbErrors+=checkValue(theCout, rootname+"/MultipleScalarDirichletCdtn.in", errors, "Multiple scalar Dirichlet condition", check);

  theCout<<" ----------------------  single vector Dirichlet condition --------------------------------"<<eol;
  Unknown v(V, _name="v", _dim=2);
  EssentialConditions svecs= (v|gammaM = g3);
  SetOfConstraints svcons = buildConstraints(svecs);
  theCout<<svcons;
  nbErrors+=checkValue(theCout, rootname+"/SingleVectorDirichletCdtn.in", errors, "Single Vector Dirichlet condition", check);

  theCout<<" ----------------------  scalar transmission condition --------------------------------"<<eol;
  Unknown w(V, _name="w");
  EssentialConditions stecs=((u|gammaM) - (w|gammaM) = 0);
  SetOfConstraints stcons = buildConstraints(stecs);
  theCout<<stcons;
  nbErrors+=checkValue(theCout, rootname+"/ScalarTransmissionCdtn.in", errors, "Scalar Transmission condition", check);

  theCout<<" ----------------------  mixed condition --------------------------------"<<eol;
  EssentialConditions svecs2= (v|gammaM = g3);
  EssentialConditions stecs2=((u|gammaM) - (w|gammaM) = 0);
  EssentialConditions miecs= svecs2 & stecs2;
  SetOfConstraints micons = buildConstraints(miecs);
  theCout<<micons;
  // nbErrors+=checkValue(theCout, rootname+"/MixedCdtn.in", errors, "Mixed condition", check);

  theCout<<" ----------------------  super mixed condition --------------------------------"<<eol;
  EssentialConditions msecs3 = (u|gammaM = f1) & (u|sigmaM = f1) & (u|sigmaP = f1) & (u|gammaP = f2);
  EssentialConditions svecs3= (v|gammaM = g3);
  EssentialConditions stecs3=((u|gammaM) - (w|gammaM) = 0);
  EssentialConditions smiecs= msecs3 & svecs3 & stecs3;
  SetOfConstraints smicons = buildConstraints(smiecs);
  theCout<<smicons;
  // nbErrors+=checkValue(theCout, rootname+"/SuperMixedCdtn.in", errors, "Super Mixed condition", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
