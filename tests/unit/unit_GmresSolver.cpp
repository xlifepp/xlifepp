/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_GmresSolver.cpp
  \author ManhHa NGUYEN
  \since 13 Sep 2012
  \date 06 Oct 2012

  Low level tests of GmresSolver.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_GmresSolver {

void unit_GmresSolver(int argc, char* argv[], bool check)
{
  String rootname = "unit_GmresSolver";
  trace_p->push(rootname);

  String errors;
  Number nbErrors = 0;

  const int rowNum = 3;
  const int colNum = 3;

  const std::string rMatrixDataSym(rootname+"/matrix3x3Sym.data");
  const std::string rMatrixDataSkewSym(rootname+"/matrix3x3SkewSym.data");
  const std::string rMatrixDataNoSym(rootname+"/matrix3x3NoSym.data");
  const std::string rMatrixDataSymPosDef(rootname+"/matrix3x3SymPosDef.data");

  const std::string cMatrixDataSym(rootname+"/cmatrix3x3Sym.data");
  const std::string cMatrixDataNoSym(rootname+"/cmatrix3x3NoSym.data");
  const std::string cMatrixDataSymSelfAjoint(rootname+"/cmatrix3x3SymSelfAjoint.data");
  const std::string cMatrixDataSymSkewAjoint(rootname+"/cmatrix3x3SymSkewAjoint.data");
  const std::string cMatrixDataSymPosDef(rootname+"/cmatrix3x3SymPosDef.data");

  const Number krylovDim = 3;
  const Real epsilon = 1.e-5;
  const Number noIteration = 1000;
  const Number vb = 0;


  GmresSolver gmres(krylovDim, epsilon, noIteration), gmresKV(_maxIt=noIteration, _tolerance=epsilon, _krylovDim=krylovDim), gmresKV2(_maxIt=100, _tolerance=1e-6, _verbose=3);

  //------------ solvers ----------------
  theCout << gmres << eol;
  theCout << gmresKV << eol;
  theCout << gmresKV2 << eol;
  nbErrors += checkValue(theCout , rootname+"/solvers.in" , errors, "Defining solvers", check);

  //!  Vectors
  Vector<Real> rvB(rowNum, 1.);

  //! Real initial value
  Vector<Real> rvX0(rowNum, 0.);

  //! Vector real unknown
  Vector<Real> rvX(rowNum);
  Vector<Real> rvXe(rowNum);
  for(Number i=0;i<rowNum; i++) rvXe(i+1)=Real(i+1);

  //!-------------------------
  //! Vector complex B
  Vector<Complex> cvB(rowNum, 1.);

  //!  Vector initial value
  Vector<Complex> cvX0(rowNum, 0.);

  //! Vector complex unknown
  Vector<Complex> cvX(rowNum);
  Vector<Complex> cvXe(rowNum);
  for(Number i=0;i<rowNum; i++) cvXe(i+1)=Complex(1.,1.)*Real(i+1);

  //!  Real Large Matrix (Dense Type)
  //! No symmetric
  LargeMatrix<Real> rMatDenseRow(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Real> rMatDenseCol(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Real> rMatDenseDual(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Real> rMatDenseSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _sym);

  //! Symmetric
  LargeMatrix<Real> rMatDenseRowSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Real> rMatDenseColSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Real> rMatDenseDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Real> rMatDenseSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _dense, _symmetric);

  //! Skew Symmetric
  LargeMatrix<Real> rMatDenseRowSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Real> rMatDenseColSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Real> rMatDenseDualSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Real> rMatDenseSymSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, _dense, _skewSymmetric);

  
  //!  Complex Large Matrix (Dense Type)
  //! No symmetric
  LargeMatrix<Complex> cMatDenseRow(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseCol(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDual(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSym(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _sym);

  //! Symmetric
  LargeMatrix<Complex> cMatDenseRowSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseColSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _dense, _symmetric);

  //! Self Ajoint Symmetric
  LargeMatrix<Complex> cMatDenseRowSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseColSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDualSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSymSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, _dense, _selfAdjoint);

  //! Skew Ajoint Symmetric
  LargeMatrix<Complex> cMatDenseRowSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseColSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDualSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSymSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, _dense, _skewAdjoint);

  //!  Real Large Matrix (CS Type)
  //! No symmetric
  LargeMatrix<Real> rMatCsRow(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Real> rMatCsCol(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Real> rMatCsDual(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _sym);

  //! Symmetric
  LargeMatrix<Real> rMatCsRowSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Real> rMatCsColSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Real> rMatCsDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _cs, _symmetric);

  //! Skew Symmetric
  LargeMatrix<Real> rMatCsRowSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Real> rMatCsColSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Real> rMatCsDualSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSymSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, _cs, _skewSymmetric);

  //!  Complex Large Matrix (Cs Type)
  //! No symmetric
  LargeMatrix<Complex> cMatCsRow(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsCol(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDual(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSym(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _sym);

  //! Symmetric
  LargeMatrix<Complex> cMatCsRowSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsColSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _cs, _symmetric);

  //! Self Ajoint Symmetric
  LargeMatrix<Complex> cMatCsRowSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsColSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDualSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSymSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, _cs, _selfAdjoint);

  //! Skew Ajoint Symmetric
  LargeMatrix<Complex> cMatCsRowSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsColSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDualSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSymSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, _cs, _skewAdjoint);

  //!  Real Large Matrix (Skyline Type)
  //! No symmetric
  LargeMatrix<Real> rMatSkylineDual(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _sym);

  //! Symmetric
  LargeMatrix<Real> rMatSkylineDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _skyline, _symmetric);

  //! Skew Symmetric
  LargeMatrix<Real> rMatSkylineDualSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSymSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, _skyline, _skewSymmetric);

  //!  Complex Large Matrix (Skyline Type)
  //! No symmetric
  LargeMatrix<Complex> cMatSkylineDual(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSym(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _sym);

  //! Symmetric
  LargeMatrix<Complex> cMatSkylineDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _skyline, _symmetric);

  //! Self Ajoint Symmetric
  LargeMatrix<Complex> cMatSkylineDualSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSymSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, _skyline, _selfAdjoint);

  //! Skew Ajoint Symmetric
  LargeMatrix<Complex> cMatSkylineDualSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSymSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint),	 _dense, rowNum, _skyline, _skewAdjoint);

  Real eps=0.0002;
  
  //!------------------------------------------------------------------------------
  //! Test with Real value
  //!-----------------------------------------------------------------------------
  //!
  // I. Solver without a preconditioner
  //! Test with DENSE STORAGE
  //! Test with symmetric matrices
  rvB = rMatDenseRowSym *rvXe;
  rvX = gmres(rMatDenseRowSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense row and sym matrix ");

  rvX = gmres(rMatDenseColSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense col and sym matrix ");

  rvX = gmres(rMatDenseDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense dual and sym matrix ");

  rvX = gmres(rMatDenseSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense sym and sym matrix ");  

  //! Test with skew symmetric matrices
  rvB = rMatDenseRowSkewSym *rvXe;

  rvX = gmres(rMatDenseRowSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense row and skew sym matrix ");

  rvX = gmres(rMatDenseColSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense col and skew sym matrix ");

  rvX = gmres(rMatDenseDualSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense dual and skew sym matrix ");

  rvX = gmres(rMatDenseSymSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense sym and skew sym matrix ");

  //! Test with non-symmetric matrices
  rvB = rMatDenseRow *rvXe;

  rvX = gmres(rMatDenseRow, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense row and non sym matrix ");

  rvX = gmres(rMatDenseCol, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense col and non sym matrix ");

  rvX = gmres(rMatDenseDual, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense dual and non sym matrix ");

  rvX = gmres(rMatDenseSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real dense sym and non sym matrix ");

  //!---------------------------------
  //! Test with CS STORAGE
  //! Test with symmetric matrices
  rvB = rMatCsRowSym *rvXe;

  rvX = gmres(rMatCsRowSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs row and sym matrix ");

  rvX = gmres(rMatCsColSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs col and sym matrix ");

  rvX = gmres(rMatCsDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs dual and sym matrix ");

  rvX = gmres(rMatCsSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs sym and sym matrix ");

  //! Test with skew symmetric matrices
  rvB = rMatCsRowSkewSym *rvXe;

  rvX = gmres(rMatCsRowSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs row and skew sym matrix ");

  rvX = gmres(rMatCsColSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs col and skew sym matrix ");

  rvX = gmres(rMatCsDualSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs dual and skew sym matrix ");

  rvX = gmres(rMatCsSymSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs sym and skew sym matrix ");


  //! Test with non symmetric matrices
  rvB = rMatCsRow *rvXe;

  rvX = gmres(rMatCsRow, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs row and non sym matrix ");

  rvX = gmres(rMatCsCol, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs col and non sym matrix ");

  rvX = gmres(rMatCsDual, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs dual and non sym matrix ");

  rvX = gmres(rMatCsSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Cs sym and non sym matrix ");

  //!------------------------------
  //! Test with SKYLINE STORAGE
  //! Test with symmetric matrices
  rvB = rMatSkylineDualSym *rvXe;

  rvX = gmres(rMatSkylineDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Skyline dual and  sym matrix ");

  rvX = gmres(rMatSkylineSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Skyline sym and  sym matrix ");

  //! Test with skew symmetric matrices
  rvB = rMatSkylineDualSkewSym *rvXe;

  rvX = gmres(rMatSkylineDualSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Skyline dual and  skew sym matrix ");

  rvX = gmres(rMatSkylineSymSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Skyline sym and  skew sym matrix ");

  //! Test with non symmetric matrices
  rvB = rMatSkylineDual *rvXe;

  rvX = gmres(rMatSkylineDual, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Skyline dual and  non sym matrix ");

  rvX = gmres(rMatSkylineSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: real Skyline sym and  non sym matrix ");

  //! -----------------------------------------------------------------------------
  //! Test with Complex values (Complex x Complex = Complex)
  //!-----------------------------------------------------------------------------
  //!
  //! Test with DENSE STORAGE
  //! Test with symmetric matrices
  cvB = cMatDenseRowSym *cvXe;

  cvX = gmres(cMatDenseRowSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense row and   sym matrix ");

  cvX = gmres(cMatDenseColSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense col and   sym matrix ");

  cvX = gmres(cMatDenseDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense dual and   sym matrix ");

  cvX = gmres(cMatDenseSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense sym and   sym matrix ");

  // Test with Sym SelfAjoint symmetric matrices
  cvB = cMatDenseRowSymSelfAjoint *cvXe;

  cvX = gmres(cMatDenseRowSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense row and SelfAjoint  sym matrix ");

  cvX = gmres(cMatDenseColSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense col and SelfAjoint  sym matrix ");

  cvX = gmres(cMatDenseDualSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense dual and SelfAjoint  sym matrix ");

  cvX = gmres(cMatDenseSymSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense sym and SelfAjoint  sym matrix ");

  // Test with Skew Ajoint symmetric matrices
  cvB = cMatDenseRowSymSkewAjoint *cvXe;

  cvX = gmres(cMatDenseRowSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense row and Skew Ajoint  sym matrix ");

  cvX = gmres(cMatDenseColSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense col and Skew Ajoint  sym matrix ");

  cvX = gmres(cMatDenseDualSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense dual and Skew Ajoint  sym matrix ");

  cvX = gmres(cMatDenseSymSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense sym and Skew Ajoint  sym matrix ");

  //! Test with no symmetric matrices
  cvB = cMatDenseRow *cvXe;

  cvX = gmres(cMatDenseRow, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense row and non-sym matrix ");

  cvX = gmres(cMatDenseCol, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense col and non-sym matrix ");

  cvX = gmres(cMatDenseDual, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense dual and non-sym matrix ");

  cvX = gmres(cMatDenseSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex dense sym and non-sym matrix ");

  //!---------------------------------
  //! Test with CS STORAGE
  //! Test with symmetric matrices
  cvB = cMatCsRowSym *cvXe;

  cvX = gmres(cMatCsRowSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs row and sym matrix ");

  cvX = gmres(cMatCsColSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs col and sym matrix ");

  cvX = gmres(cMatCsDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs dual and sym matrix ");

  cvX = gmres(cMatCsSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs sym and sym matrix ");

  //! Test with selfAjoint symmetric matrices
  cvB = cMatCsRowSymSelfAjoint *cvXe;

  cvX = gmres(cMatCsRowSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs row and selfajoint sym matrix ");

  cvX = gmres(cMatCsColSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs col and selfajoint sym matrix ");

  cvX = gmres(cMatCsDualSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs dual and selfajoint sym matrix ");

  cvX = gmres(cMatCsSymSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs sym and selfajoint sym matrix ");

  //! Test with skewAjoint symmetric matrices
  cvB = cMatCsRowSymSkewAjoint*cvXe;

  cvX = gmres(cMatCsRowSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs row and skewajoint sym matrix ");

  cvX = gmres(cMatCsColSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs col and skewajoint sym matrix ");

  cvX = gmres(cMatCsDualSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs dual and skewajoint sym matrix ");

  cvX = gmres(cMatCsSymSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs sym and skewajoint sym matrix ");

  //! Test with no symmetric matrices
  cvB = cMatCsRow*cvXe;

  cvX = gmres(cMatCsRow, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs row and non-sym matrix ");

  cvX = gmres(cMatCsCol, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs col and non-sym matrix ");

  cvX = gmres(cMatCsDual, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs dual and non-sym matrix ");

  cvX = gmres(cMatCsSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Cs sym and non-sym matrix ");

  //!---------------------------------
  //! Test with SKYLINE STORAGE
  //! Test with symmetric matrices
  cvB = cMatSkylineDualSym*cvXe;

  cvX = gmres(cMatSkylineDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline dual and sym matrix ");

  cvX = gmres(cMatSkylineSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline sym and sym matrix ");

  //! Test with self ajoint sym matrices
  cvB = cMatSkylineDualSymSelfAjoint*cvXe;

  cvX = gmres(cMatSkylineDualSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline dual and selfadjoint sym matrix ");

  cvX = gmres(cMatSkylineSymSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline sym and selfadjoint sym matrix \n ");

  //! Test with skew ajoint matrices
  cvB = cMatSkylineDualSymSkewAjoint*cvXe;

  cvX = gmres(cMatSkylineDualSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline dual and skewadjoint sym matrix ");

  cvX = gmres(cMatSkylineSymSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline sym and skewadjoint sym matrix ");

  //! Test with no symmetric matrices
  cvB = cMatSkylineDual*cvXe;

  cvX = gmres(cMatSkylineDual, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline dual and non-sym matrix ");

  cvX = gmres(cMatSkylineSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(rvX , rvXe , eps,  errors, "GMRES: complex Skyline sym and non-sym matrix ");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
