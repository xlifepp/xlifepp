/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Fourier; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file unit_MorleyTriangle.cpp
\author E. Lunéville
\since 3 may 2021
\date 3 may 2021

    check MorleyTriangle element functions and solve bilaplacian problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_MorleyTriangle
{

Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real k=1., kp=k*pi_;
  Real r=sin(kp*x)*sin(kp*y);
  return r*r;
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real k=1., dkp=2*k*pi_;
  Real cx=cos(dkp*x), cy=cos(dkp*y);
  return 0.25*dkp*dkp*dkp*dkp*(4*cx*cy-cx-cy);
}

Real f1(const Point& P, Parameters& pa = defaultParameters)
{return 1.;}
Vector<Real> g1(const Point& P, Parameters& pa = defaultParameters)
{return Vector<Real>(2,0.);}

Real fx(const Point& P, Parameters& pa = defaultParameters)
{return P(1);}
Vector<Real> gx(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(1)=1.;return g;}

Real fx2(const Point& P, Parameters& pa = defaultParameters)
{return P(1)*P(1);}
Vector<Real> gx2(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(1)=2*P(1);return g;}

Real fy(const Point& P, Parameters& pa = defaultParameters)
{return P(2);}
Vector<Real> gy(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(2)=1.;return g;}

Real fy2(const Point& P, Parameters& pa = defaultParameters)
{return P(2)*P(2);}
Vector<Real> gy2(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(2)=2*P(2);return g;}

Real fxy(const Point& P, Parameters& pa = defaultParameters)
{return P(1)*P(2);}
Vector<Real> gxy(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(1)=P(2); g(2)=P(1);return g;}

void unit_MorleyTriangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_MorleyTriangle";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(10);
  //numberOfThreads(1);

  String errors;
  Number nbErrors = 0;
  Real eps=0.005;
  bool isTM= true;

  Point x(0.,0.); x[0]=1.;
  theCout<<"\n\n================  Morley standard triangle, order 1 (MOR) ======================"<<eol;
  MorleyTriangle MORT;
  MORT.print(theCout);
  ShapeValues shv = ShapeValues(MORT,true,true);
  MORT.computeShapeValues(x.begin(),shv);
  MORT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.;x[1]=1.;
  MORT.computeShapeValues(x.begin(),shv);
  MORT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.;x[1]=0.;
  MORT.computeShapeValues(x.begin(),shv);
  MORT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.;x[1]=0.5;
  MORT.computeShapeValues(x.begin(),shv);
  MORT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.5;x[1]=0.;
  MORT.computeShapeValues(x.begin(),shv);
  MORT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.5;x[1]=0.5;
  MORT.computeShapeValues(x.begin(),shv);
  MORT.printShapeValues(theCout,x.begin(),shv);
  theCout<<eol;
  //nbErrors += checkValue(theCout, rootname+"/NT1.in", errors, "NT1", check, isTM);

  //check Morley space
  Rectangle rect(_xmin=0,_xmax=1,_ymin=0,_ymax=1.,_hsteps=0.05,_domain_name="Omega",_side_names="Gamma");
  Mesh mesh(rect,_shape=_triangle, _order=1, _generator=gmsh);
  Domain omega=mesh.domain("Omega"), gamma=mesh.domain("Gamma");

  Space V(_domain=omega, _FE_type=Morley, _order=1, _name="V");
  theCout<<V<<eol;

  //check affine transformation
  ShapeValues shw; shw.resize(6,2,true,true);
  for(Number i=0;i<std::min(Number(10),V.nbOfElements());i++)
  {
    theCout<<"shape values on elt "<<i+1<<": "<<eol;
    const Element& elt=V.element(i);
    GeomMapData geom(elt.geomElt_p->meshElement());
    geom.computeJacobianMatrix(Point(0.,0.));
    geom.invertJacobianMatrix();
    const std::vector<Number>& dofnums=elt.dofNumbers;
    Real prec=1E-4;
    for(Number d=0;d<dofnums.size();d++)
    {
       const FeDof& dof=static_cast<const FeDof&>(V.dof(dofnums[d]));
       const Vector<Real>& n=dof.projVector();
       Point x= dof.coords(), xr=geom.geomMapInverse(x);
       MORT.computeShapeValues(xr.begin(),shv);
       shw.Morley2dMap(shv, geom,true,true);
       String  sc =  format(tostring(round(x,prec)),17,_centerAlignment);
       String  scr=  format(tostring(round(xr,prec)),17,_centerAlignment);
       if(d<3) theCout<<"    w_"<<d+1<<" at "<<sc<<" / "<<scr<<": "<<roundToZero(shw.w,1E-10)<<eol;
       else    theCout<<"  dnw_"<<d+1<<" at "<<sc<<" / "<<scr<<": "<<roundToZero(n[0]*shw.dw[0]+n[1]*shw.dw[1],1E-10)<<"   n="<<n<<eol;
    }
  }
  theCout<<std::flush<<eol;


  // check FE matrix
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");

  TermMatrix K(intg(omega,grad(u)|grad(v)), _name="K");
  theCout<<K;
  TermMatrix L(intg(omega,dxx(u)*dxx(v)), _name="L");
  theCout<<L;
  TermMatrix M(intg(omega,u*v), _name="M");
  theCout<<M;

  Function F1(f1), G1(g1);
  TermVector un=interpolent(u,omega,F1,G1);
  theCout<<"interpolent(u,omega,F1,G1) :"<<un;
  theCout<<"(M*un|un)="<<(M*un|un)<<" ||K*un||="<<norm2(K*un)<<", ||L*un||="<<norm2(L*un)<<eol;

  Function Fx(fx), Gx(gx);
  TermVector ux=interpolent(u,omega,Fx,Gx);
  theCout<<"interpolent(u,omega,Fx,Gx) :"<<ux;
  theCout<<"(M*ux|ux)="<<(M*ux|ux)<<" (K*ux|ux)="<<(K*ux|ux)<<", ||L*ux||="<<norm2(L*ux)<<eol;

  Function Fx2(fx2), Gx2(gx2);
  TermVector ux2=interpolent(u,omega,Fx2,Gx2);
  theCout<<"interpolent(u,omega,Fx2,Gx2) :"<<ux2;
  theCout<<"(M*ux2|ux2)="<<(M*ux2|ux2)<<" (K*ux2|ux2)="<<(K*ux2|ux2)<<", (L*ux2|ux2)="<<(L*ux2|ux2)<<eol;

  Function Fy(fy), Gy(gy);
  TermVector uy=interpolent(u,omega,Fy,Gy);
  theCout<<"interpolent(u,omega,Fy,Gy) :"<<uy;
  theCout<<"(M*uy|uy)="<<(M*uy|uy)<<" (K*uy|uy)="<<(K*uy|uy)<<", ||L*uy||="<<norm2(L*uy)<<eol;

  Function Fy2(fy2), Gy2(gy2);
  TermVector uy2=interpolent(u,omega,Fy2,Gy2);
  theCout<<"interpolent(u,omega,Fy2,Gy2) :"<<uy2;
  theCout<<"(M*uy2|uy2)="<<(M*uy2|uy2)<<" (K*uy2|uy2)="<<(K*uy2|uy2)<<", (L*uy2|uy2)="<<(L*uy2|uy2)<<eol;

  Function Fxy(fxy), Gxy(gxy);
  TermVector uxy=interpolent(u,omega,Fxy,Gxy);
  theCout<<"interpolent(u,omega,Fxy,Gxy) :"<<uxy;
  theCout<<"(M*uxy|uxy)="<<(M*uxy|uxy)<<" (K*uxy|uxy)="<<(K*uxy|uxy)<<", (L*uxy|uxy)="<<(L*uxy|uxy)<<eol;

  //check bilaplacian problem delta^2 u=f on Omega, u=du/dn=0 on Gamma
  EssentialConditions ecs= (u|gamma = 0) & (ndotgrad(u)|gamma = 0);
  theCout<<ecs<<std::flush;
  TermMatrix A(intg(omega,dxx(u)*dxx(v))+intg(omega,dyy(u)*dyy(v))+2*intg(omega,dxy(u)*dxy(v)), ecs, _name="A");
  theCout<<A<<std::flush;
  LinearForm b=intg(omega,f*v);
  TermVector B(b, _name="B");
  theCout<<B<<std::flush;
  TermVector U=directSolve(A,B);
  theCout<<U<<std::flush;

  Space W(_domain=omega, _interpolation=_P1, _name="W", _notOptimizeNumbering);
  Unknown w(W, _name="W");
  TermVector ui1=interpolate(w,omega,U,"ui1");
  saveToFile("ui1", ui1,_format=_vtu, _aUniqueFile);

  TermVector ue(w,omega,uex, _name="uex");
  saveToFile("uex", ue,_format=_vtu, _aUniqueFile);

  TermVector up1=projection(U,W);
  saveToFile("up1",up1,_format=_vtu, _aUniqueFile);

  // convergence error
  Reals hs(0.25,0.1,0.075,0.05);
  std::vector<Real> el2(hs.size(),0.), el2r(hs.size(),0.);
  for (Number k=0;k<hs.size();k++)
  {
    Rectangle rectk(_xmin=0,_xmax=1,_ymin=0,_ymax=1.,_hsteps=hs[k],_domain_name="Omegak",_side_names="Gammak");
    Mesh meshk(rectk,_shape=_triangle, _order=1, _generator=gmsh);
    Domain omegak=meshk.domain("Omegak"), gammak=meshk.domain("Gammak");
    Space Vk(_domain=omegak, _FE_type=Morley, _order=1, _name="Vk");
    Unknown uk(Vk, _name="uk"); TestFunction vk(uk, _name="vk");
    // TermMatrix Ak(intg(omegak,dxx(uk)*dxx(vk))+intg(omegak,dyy(uk)*dyy(vk))+2*intg(omegak,dxy(uk)*dxy(vk)),
    //              (uk|gammak = 0) & (ndotgrad(uk)|gammak = 0), _name="Ak");
    EssentialConditions ecs= (uk|gammak = 0) & (ndotgrad(uk)|gammak = 0);
    TermMatrix Ak(intg(omegak,dxx(uk)*dxx(vk))+intg(omegak,dyy(uk)*dyy(vk))+2*intg(omegak,dxy(uk)*dxy(vk)), ecs, _name="Ak");
    TermVector Bk(intg(omegak,f*vk), _name="B");
    TermVector Uk=directSolve(Ak,Bk);
    Space Wk(_domain=omegak, _interpolation=_P1, _name="Wk", _notOptimizeNumbering);
    Unknown wk(Wk, _name="Wk"); TestFunction qk(wk, _name="qk");
    TermVector Uki=interpolate(wk,omegak,Uk,"uki");
    TermVector Uek(wk,omegak,uex, _name="uex");
    TermMatrix Mk(intg(omegak,wk*qk));
    TermVector ek=Uki-Uek;
    el2[k]=sqrt(real((Mk*ek|ek)));
    el2r[k]=el2[k]/sqrt(real((Mk*Uek|Uek)));
    saveToFile("u"+tostring(k), Uki,_format=_vtu, _aUniqueFile);
    saveToFile("e"+tostring(k), ek,_format=_vtu, _aUniqueFile);
  }
  std::ofstream err("error_L2.dat");
  theCout<<"L2 error versus h :"<<eol;
  for(Number k=0;k<hs.size();k++)
  {
    err<<hs[k]<<" "<<el2[k]<<" "<<el2r[k]<<eol;
    theCout<<"  "<<format(tostring(hs[k]),5,_leftAlignment)<<" -> "<<format(tostring(round(el2[k],1.E-6)),8,_leftAlignment)<<" ("<<round(100*el2r[k],0.01)<<"%)"<<eol;
  }
  err.close();

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
