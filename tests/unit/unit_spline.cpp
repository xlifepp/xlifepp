/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
	\file unit_spline.cpp
	\since 28 aug 2019
	\date 28 aug 2019
	\author Eric Lunéville

	test the spline classes
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_spline {

void checkSpline(const String& fn, const Spline& sp, bool is1D, bool withFile, CoutStream& cs)
{
  cs<<sp;
  Real dt=0.01, t0=0, tf=1.+0.1*theTolerance, tm=0.5;
  const Parametrization& pa=sp.parametrization();
  cs<<"pa(t0) = "<<pa(t0).roundToZero()<<eol;
  cs<<"pa(tm) = "<<pa(tm).roundToZero()<<eol;
  cs<<"pa(tf) = "<<pa(tf).roundToZero()<<eol;
  cs<<"pa.length(tm) = "<<roundToZero(pa.length(tm))<<eol;
  cs<<"pa.curvature(tm) = "<<roundToZero(pa.curvature(tm))<<eol;
  cs<<"pa.curabc(tm) = "<<roundToZero(pa.curabc(tm))<<eol;
  cs<<"pa.normal(tm) = "<<pa.normal(tm).roundToZero()<<eol;
  cs<<"pa.tangent(tm) = "<<pa.tangent(tm).roundToZero()<<eol;
  if(withFile)
  {
    std::ofstream of(fn.c_str());
    if(is1D)
      {
        for(Real t=0;t<tf;t+=dt)
            of<<t<<" "<<sp(t)[0]<<" "<<sp(t,_d1)[0]<<eol;
      }
      else
      {
        for(Real t=t0;t<tf;t+=dt)
        {
          Point pt=sp(t), dpt=sp(t,_dt);
          of<<pt[0]<<" "<<pt[1]<<" "<<dpt[0]<<" "<<dpt[1]<<eol;
        }
      }
      of.close();
  }
}

void unit_spline(int argc, char* argv[], bool check)
{
  String rootname = "unit_spline";
  trace_p->push(rootname);

  verboseLevel(4);
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  ofstream of;
  bool withFile=true;
  Real dt=0.01, t0=0, tf=1.+0.1*theTolerance, tm=0.5;

  Number n=5;
  std::vector<Point> points(n+1);
  Real x=0, dx=pi_/n;
  for(Number i=0;i<=n;i++, x+=dx) points[i]=Point(x,sin(x));

  theCout<<" ================= C2-spline =================="<<eol;
  C2Spline cs(points,_xParametrization);
  checkSpline("c2spline_nat.dat", cs, true, withFile, theCout);

  C2Spline csc(points,_xParametrization,_clampedBC,_clampedBC,Reals(1.,1.),Reals(1.,-1.));
  checkSpline("c2spline_cla.dat", csc, true, withFile, theCout);

  C2Spline csp(points,_xParametrization,_periodicBC,_periodicBC);
  checkSpline("c2spline_per.dat", csp, true, withFile, theCout);

  nbErrors += checkValue(theCout, rootname+"/C2Spline.in" , errors, "C2 Spline",check);

  theCout<<" ================= Catmull-Rom spline =================="<<eol;
  CatmullRomSpline ca(points);
  checkSpline("catmullspline_nat.dat", ca, false, withFile, theCout);

  Reals v0(2,1.), v1(2,1.); v1(2)=-1;
  CatmullRomSpline ca2(points,_chordalParametrization,0.5,_clampedBC,_clampedBC,v0,v1);
  checkSpline("catmullspline_cla.dat", ca2, false, withFile, theCout);

  Number ns=6;
  std::vector<Point> pts(ns+1);
  Real ss=0, dss=2*pi_/ns;
  for(Number i=0;i<=ns;i++,ss+=dss) pts[i]=Point(2*cos(ss),sin(ss));
  CatmullRomSpline ca3(pts);
  checkSpline("catmullspline_per.dat", ca3, false, withFile, theCout);

  nbErrors += checkValue(theCout, rootname+"/CatmullRom.in" , errors, "Catmull-Rom",check);

  theCout<<" ================= B-spline =================="<<eol;
  BSpline bsn(points,3,_naturalBC);
  checkSpline("bspline_nat.dat", bsn, false, withFile, theCout);

  std::vector<Point> points2(5,Point(0.,0.)); points2[1]=Point(pi_/4,sqrt(2)/2.); points2[2]=Point(pi_/2,1.);points2[3]=Point(3*pi_/4,sqrt(2)/2.);points2[4]=Point(pi_,0.);
  BSpline bsc(points2,3,_clampedBC);
  checkSpline("bspline_cla.dat", bsc, false, withFile, theCout);

  BSpline bsp(pts,3,_periodicBC);
  checkSpline("bspline_per.dat", bsp, false, withFile, theCout);

  std::vector<Real> we(pts.size(),1.);
  for(Number k=0;k<we.size();k+=2) we[k]=0.5;
  BSpline bspw(pts,3,_periodicBC,_periodicBC,we);
  checkSpline("bspline_perw.dat", bspw, false, withFile, theCout);

  nbErrors += checkValue(theCout, rootname+"/BSpline.in" , errors, "B-Spline",check);

  theCout<<" ================= interpolating B-spline =================="<<eol;
  BSpline bsni(_SplineInterpolation, points2, 3,_uniformParametrization, _naturalBC);
  checkSpline("bspline_int_nat.dat", bsni, false, withFile, theCout);

  BSpline bsni2(_SplineInterpolation, points2, 3,_centripetalParametrization, _naturalBC);
  checkSpline("bspline_int_nat_cen.dat", bsni2, false, withFile, theCout);

  Reals d0(2,1.),d1=d0; d1[1]=-1.;
  BSpline bsci(_SplineInterpolation, points2, 3,_uniformParametrization, _clampedBC,_clampedBC,d0,d1);
  checkSpline("bspline_int_cla.dat", bsci, false, withFile, theCout);

  nbErrors += checkValue(theCout, rootname+"/InterpolationBSpline.in" , errors, "Interpolation B-Spline",check);

  theCout<<" ================= Bezier spline =================="<<eol;
  BezierSpline bz(points);
  checkSpline("bezierspline.dat", bsci, false, withFile, theCout);

  nbErrors += checkValue(theCout, rootname+"/BezierSpline.in" , errors, "Bezier Spline",check);

  theCout<<" ================= Nurbs =================="<<eol;
  ns=4;Real ds=pi_/(2*ns);
  Real u=-pi_/2,v;
  std::vector<std::vector<Point> > pts2(2*ns+1,std::vector<Point>(ns+1));
  for(Number i=0;i<=2*ns;i++,u+=ds)
   {
     v=0;
     for(Number j=0;j<=ns;j++,v+=ds) pts2[i][j]=Point(cos(u)*cos(v),cos(u)*sin(v),sin(u));
   }
   Nurbs nuI(pts2);
   theCout<<nuI<<std::flush;
   Real du=1./20, dv=1./20;
   if(withFile)
   {
     of.open("nurbsI.dat");
     for(Real u=0.;u<=1.+theTolerance;u+=du)
       for(Real v=0.;v<=1.+theTolerance;v+=dv)
       {
        Point Q=nuI(std::min(u,1.),std::min(v,1.));
        of<<Q[0]<<" "<<Q[1]<<" "<<Q[2]<<eol;
       }
     of.close();
   }

   Nurbs nuA(_SplineApproximation,pts2);
   theCout<<nuA<<std::flush;
   if(withFile)
   {
     of.open("nurbsA.dat");
     for(Real u=0.;u<=1.+theTolerance;u+=du)
       for(Real v=0.;v<=1.+theTolerance;v+=dv)
       {
        Point Q=nuA(std::min(u,1.),std::min(v,1.));
        of<<Q[0]<<" "<<Q[1]<<" "<<Q[2]<<eol;
       }
     of.close();
   }

   Nurbs nu2(_SplineApproximation,pts2,3,3,_clampedBC,_naturalBC);
   theCout<<nu2<<std::flush;
   if(withFile)
   {
     of.open("nurbsA_cla_nat.dat");
     for(Real u=0.;u<=1.+theTolerance;u+=du)
       for(Real v=0.;v<=1.+theTolerance;v+=dv)
       {
         Point Q=nu2(std::min(u,1.),std::min(v,1.));
         of<<Q[0]<<" "<<Q[1]<<" "<<Q[2]<<eol;
       }
     of.close();
   }

  nbErrors += checkValue(theCout, rootname+"/Nurbs.in" , errors, "Nurbs",check);

  //!finalization
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}
}

