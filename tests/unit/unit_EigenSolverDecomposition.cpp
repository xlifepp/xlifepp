/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; withssout even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file unit_EigenSolverDecomposition.cpp
  \author ManhHa NGUYEN
  \since 23 Jan 2013
  \date 24 July 2013

  Low level tests of decompostion functionalities for (dense) eigen problem (eigenCore).
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_EigenSolverDecomposition {

void unit_EigenSolverDecomposition(int argc, char* argv[], bool check)
{
  String rootname = "unit_EigenSolverDecomposition";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(10);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;
  Real nF;
  bool isTM=true;

  const Number size = 5;

  const std::string rSym(inputsPathTo(rootname+"/rSym5.data"));
  const std::string rNonSym(inputsPathTo(rootname+"/rNonSym5.data"));
  const std::string cSym(inputsPathTo(rootname+"/cSelfAdjoint5.data"));
  const std::string cNonSym(inputsPathTo(rootname+"/cNonSym5.data"));

  typedef Tridiagonalization<MatrixEigenDense<Real> > RealTridiag;
  typedef Tridiagonalization<MatrixEigenDense<Complex> > CplxTridiag;
  typedef HessenbergDecomposition<MatrixEigenDense<Real> > RealHess;
  typedef HessenbergDecomposition<MatrixEigenDense<Complex> > CplxHess;
  typedef RealSchur<MatrixEigenDense<Real> > RealSch;
  typedef ComplexSchur<MatrixEigenDense<Real> > RealComplexSch;
  typedef ComplexSchur<MatrixEigenDense<Complex> > CmplxComplexSch;
  typedef HouseholderQR<MatrixEigenDense<Real> > RealHHQR;

  VectorEigenDense<Real> rDiag(size), rSubdiag(size-1), rHcoeffs(size - 1);
  VectorEigenDense<Complex> cDiag(size), cSubdiag(size - 1), cHcoeffs(size - 1);
  MatrixEigenDense<Real> rMat(size), rMatNonSym(size);
  MatrixEigenDense<Complex> cMat(size), cMatNonSym(size);

  MatrixEigenDense<Complex> cMatTest(size), cTmp(5,5),cQaq(5,5),cEye5(5);
  MatrixEigenDense<Real> rMatTest(size), rTmp(5,5),rQaq(5,5),rEye5(5);

  rMat.loadFromFile(rSym.c_str());
  cMat.loadFromFile(cSym.c_str());
  rMatNonSym.loadFromFile(rNonSym.c_str());
  cMatNonSym.loadFromFile(cNonSym.c_str());
  eyeMatrix(cEye5); eyeMatrix(rEye5);

  int sortInt[] = {1,2,0,4,3};
  std::vector<int> sortOrder(sortInt, sortInt + sizeof(sortInt) / sizeof(int) );

  ssout << "----------------------------Test Tridiagonalization-----------------------------------------------" << "\n";
  ssout << "----------------------------Real symmetric--------------------" << "\n";
  RealTridiag tridiag(size);
  ssout << "Real input matrix " << rMat << "\n";
  rMatTest = rMat;
  tridiag.compute(rMat);
  ssout << "Computing with tridiagonlization object" << "\n";
  ssout << "Tridiagonlized matrix " << rMat << "\n";
  ssout << "Diagonal " << rMat.diagonal().real() << "\n";
  ssout << "Sub-Diagonal " << rMat.subDiagonal().real() << "\n";
  ssout << "Reflection coeff " << tridiag.householderCoefficients() << "\n";
  ssout << "Unitary matrix " << tridiag.matrixQ() << "\n";
  nbErrors+=checkValues(rMat, rootname+"/rMat.in", tol, errors, "rMat", check);
  nbErrors+=checkValues(rMat.diagonal().real(), rootname+"/rMatDiagReal.in", tol, errors, "rMat.diagonal.real", check);
  nbErrors+=checkValues(rMat.subDiagonal().real(), rootname+"/rMatSubDiagReal.in", tol, errors, "rMat.diagonal.real", check);
  nbErrors+=checkValues(tridiag.householderCoefficients(), rootname+"/triDiagHHC.in", tol, errors, "tridiag.householderCoefficients()", check);
  nbErrors+=checkValues(tridiag.matrixQ(), rootname+"/triDiagMQ.in", tol, errors, "tridiag.matrixQ()", check);
  multMatMat(conj(transpose(tridiag.matrixQ())),tridiag.matrixQ(), rQaq);
  rQaq -= rEye5;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "norm Froebenius rQaq (1)");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";

  rMat.loadFromFile(rSym.c_str());
  rMatTest = rMat;
  internalEigenSolver::tridiagonalizationInplace(rMat, rHcoeffs);
  ssout << "Computing with tridiagonalization directly" << "\n";
  ssout << "Tridiagonlized matrix " << rMat << "\n";
  ssout << "Reflection coeff " << rHcoeffs << "\n";
  ssout << "Diagonal " << rMat.diagonal().real() << "\n";
  ssout << "Sub-Diagonal " << rMat.subDiagonal().real() << "\n";
  ssout << "Unitary matrix " << tridiag.matrixQ() << "\n";
  multMatMat(conj(transpose(tridiag.matrixQ())),tridiag.matrixQ(), rQaq);
  rQaq -= rEye5;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "norm Froebenius rQaq (2)");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";

  rMat.loadFromFile(rSym.c_str());
  rMatTest = rMat;
  internalEigenSolver::tridiagonalizationInplace(rMat, rDiag, rSubdiag, true);
  ssout << "Computing tridiagonalization directly and rewriting input matrix with unitary matrix" << "\n";
  ssout << "Diagonal " << rDiag << "\n";
  ssout << "Sub-Diagonal " << rSubdiag << "\n";
  ssout << "Unitary matrix " << rMat << "\n";
  multMatMat(conj(transpose(rMat)),rMat, rQaq);
  rQaq -= rEye5;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "norm Froebenius rQaq (3)");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";

  ssout << "----------------------------(Complex) selfAdjoint--------------------" << "\n";
  CplxTridiag ctridiag(size);

  ssout << "Complex input matrix " << cMat << "\n";
  cMatTest = cMat;
  ctridiag.compute(cMat);
  ssout << "Reflection coeff " << ctridiag.householderCoefficients() << "\n";
  ssout << "Diagonal " << cMat.diagonal().real() << "\n";
  ssout << "Sub-Diagonal " << cMat.subDiagonal().real() << "\n";
  ssout << "Unitary matrix " << ctridiag.matrixQ() << "\n";
  multMatMat(conj(transpose(ctridiag.matrixQ())),ctridiag.matrixQ(), cQaq);
  cQaq -= cEye5;
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "norm Froebenius cQaq (1)");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";

  cMat.loadFromFile(cSym.c_str());
  internalEigenSolver::tridiagonalizationInplace(cMat, cHcoeffs);
  ssout << "Computing with tridiagonalization directly" << "\n";
  ssout << "Tri-diagonlized matrix " << cMat << "\n";
  ssout << "Reflection coeff " << cHcoeffs << "\n";
  ssout << "Diagonal " << cMat.diagonal().real() << "\n";
  ssout << "Sub-Diagonal " << cMat.subDiagonal().real() << "\n";

  cMat.loadFromFile(cSym.c_str());
  cMatTest = cMat;
  internalEigenSolver::tridiagonalizationInplace(cMat, rDiag, rSubdiag, true);
  ssout << "Computing tridiagonalization directly and rewriting input matrix with unitary matrix" << "\n";
  ssout << "Unitary matrix " << cMat << "\n";
  ssout << "Diagonal " << rDiag << "\n";
  ssout << "Sub-Diagonal " << rSubdiag << "\n";
  multMatMat(conj(transpose(cMat)),cMat, cQaq);
  cQaq -= cEye5;
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "norm Froebenius cQaq (2)");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";

  ssout << "----------------------------Test Hessenberg decomposition------------------------------------------" << "\n";
  ssout << "----------------------------Real symmetric--------------------" << "\n";
  RealHess rHess(size);
  rMat.loadFromFile(rSym.c_str());
  ssout << "Symmetric matrix " << rMat << "\n";
  rHess.compute(rMat);
  ssout << "Hessenberg matrix " << rHess.matrixH() << "\n";
  ssout << "Unitary matrix " << rHess.matrixQ() << "\n";
  multMatMat(conj(transpose(rHess.matrixQ())),rHess.matrixQ(), rQaq);
  rQaq -= rEye5;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "norm Froebenius rQaq (4)");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";

  multMatMat(rHess.matrixQ(),rHess.matrixH(), rTmp);
  multMatMat(rTmp,conj(transpose(rHess.matrixQ())), rQaq);
  rQaq -= rMat;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "norm Froebenius ||Q*H*^T*Q-I||");
  ssout << "||Q*H*^T*Q-I|| = " << nF << "\n";


  ssout << "----------------------------real non-symmetric--------------------" << "\n";
  rMatNonSym.loadFromFile(rNonSym.c_str());
  ssout << "Non symmetric matrix " << rMatNonSym << "\n";
  rHess.compute(rMatNonSym);
  ssout << "Hessenberg matrix " << rHess.matrixH() << "\n";
  ssout << "Unitary matrix " << rHess.matrixQ() << "\n";
  multMatMat(conj(transpose(rHess.matrixQ())),rHess.matrixQ(), rQaq);
  rQaq -= rEye5;
  ssout << "||Q^T*Q-I|| = " << rQaq.normFrobenius() << "\n";
  multMatMat(rHess.matrixQ(),rHess.matrixH(), rTmp);
  multMatMat(rTmp,conj(transpose(rHess.matrixQ())), rQaq);
  rQaq -= rMatNonSym;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "Non Sym norm Froebenius ||Q*H*^T*Q-A||");
  ssout << "||Q*H*^T*Q-A|| = " << nF << "\n";

  ssout << "----------------------------Complex (selfAdjoint)--------------------" << "\n";
  CplxHess cHess(size);
  cMat.loadFromFile(cSym.c_str());
  ssout << "Symmetric matrix " << cMat << "\n";
  cHess.compute(cMat);
  ssout << "Hessenberg matrix " << cHess.matrixH() << "\n"; 
  ssout << "Unitary matrix " << cHess.matrixQ() << "\n";
  multMatMat(conj(transpose(cHess.matrixQ())),cHess.matrixQ(), cQaq);
  cQaq -= cEye5;
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "selfadjoint norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";
  multMatMat(cHess.matrixQ(),cHess.matrixH(), cTmp);
  multMatMat(cTmp,conj(transpose(cHess.matrixQ())),cQaq);
  cQaq -= cMat;
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "selfadjoint norm Froebenius ||Q*H*^T*Q-A||");
  ssout << "||Q*H*^T*Q-A|| = " << nF << "\n";

  ssout << "----------------------------Complex non-symmetric--------------------" << "\n";
  cMatNonSym.loadFromFile(cNonSym.c_str());
  ssout << "Non symmetric matrix " << cMatNonSym << "\n";
  cHess.compute(cMatNonSym);
  ssout << "Hessenberg matrix " << cHess.matrixH() << "\n";
  ssout << "Unitary matrix " << cHess.matrixQ() << "\n";
  multMatMat(conj(transpose(cHess.matrixQ())),cHess.matrixQ(), cQaq);
  cQaq -= cEye5;
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "selfadjoint norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q^T*Q-I|| = " << nF << "\n";
  multMatMat(cHess.matrixQ(),cHess.matrixH(), cTmp);
  multMatMat(cTmp,conj(transpose(cHess.matrixQ())),cQaq);
  cQaq -= cMatNonSym;
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, "selfadjoint norm Froebenius ||Q*H*^T*Q-A||");
  ssout << "||Q*H*^T*Q-A|| = " << nF << "\n";


  ssout << "----------------------------Test HouseHolder QR decomposition------------------------------------------" << "\n";
  RealHHQR rHhQR;
  MatrixEigenDense<Real> rMatQR(rMat,0,0,rMat.numOfRows(),rMat.numOfCols()-1), rTmpQr(rMat.numOfRows(),rMat.numOfCols()-1);
  rHhQR.compute(rMatQR);
  ssout << "Matrix " << rMatQR << "\n";
  ssout << "Matrix QR " << rHhQR.matrixQR() << "\n";
  ssout << "Householder reflector "<< rHhQR.hCoeffs() << "\n";
  ssout << "Unitary Matrix " << rHhQR.matrixQ() << "\n";

  multMatMat(rHhQR.matrixQ(),transpose(rHhQR.matrixQ()),rTmp);
  rTmp -= rEye5;
  nF=rTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(transpose(rHhQR.matrixQ()),rMatQR, rTmpQr);
  ssout << "Matrix R = " << rTmpQr << "\n";

  MatrixEigenDense<Real> rMat31QR(3,1);
  rMat31QR.coeffRef(0,0)=1.36;
  rMat31QR.coeffRef(1,0)=-0.816;
  rMat31QR.coeffRef(2,0)=0.521;
  rHhQR.compute(rMat31QR);
  ssout << "Unitary Matrix " << rHhQR.matrixQ() << "\n";

  ssout << "----------------------------Test Real Schur decomposition------------------------------------------" << "\n";
  ssout << "----------------------------real symmetric--------------------" << "\n";
  RealSch rSchur(size);
  rMat.loadFromFile(rSym.c_str());
  ssout << "Symmetric matrix " << rMat << "\n";
  rSchur.compute(rMat);
  ssout << "T matrix " << rSchur.matrixT() << "\n";
  ssout << "Unitary matrix " << rSchur.matrixU() << "\n";
  multMatMat(rSchur.matrixU(),transpose(rSchur.matrixU()),rTmp);
  rTmp -= rEye5;
  nF=rTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(rSchur.matrixU(),(rSchur.matrixT()),rTmp);
  multMatMat(rTmp, transpose(rSchur.matrixU()),rQaq);
  rQaq -= rMat;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*H*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";

  ssout << "Reordering diagonal block" << "\n";
  rSchur.swapSchur(0,1,true);
  ssout << "T matrix after being swapped " << rSchur.matrixT() << "\n";
  ssout << "Unitary matrix after being swapped " << rSchur.matrixU() << "\n";

  multMatMat(rSchur.matrixU(),transpose(rSchur.matrixU()),rTmp);
  rTmp -= rEye5;
  nF=rTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(rSchur.matrixU(),(rSchur.matrixT()),rTmp);
  multMatMat(rTmp, transpose(rSchur.matrixU()),rQaq);
  rQaq -= rMat;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";


  ssout << "----------------------------real non-symmetric--------------------" << "\n";
  rMatNonSym.loadFromFile(rNonSym.c_str());
  ssout << "Non symmetric matrix " << rMatNonSym << "\n";
  rSchur.compute(rMatNonSym);
  ssout << "T matrix " << rSchur.matrixT() << "\n";
  ssout << "Unitary matrix " << rSchur.matrixU() << "\n";
  multMatMat(rSchur.matrixU(),transpose(rSchur.matrixU()),rTmp);
  rTmp -= rEye5;
  nF=rTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(rSchur.matrixU(),(rSchur.matrixT()),rTmp);
  multMatMat(rTmp, transpose(rSchur.matrixU()),rQaq);
  rQaq -= rMatNonSym;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";

  ssout << "Reordering diagonal block" << "\n";
  rSchur.swapSchur(0,4,true);
  ssout << "T matrix after being swapped " << rSchur.matrixT() << "\n";
  ssout << "Unitary matrix after being swapped " << rSchur.matrixU() << "\n";

  multMatMat(rSchur.matrixU(),transpose(rSchur.matrixU()),rTmp);
  rTmp -= rEye5;
  nF=rTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(rSchur.matrixU(),(rSchur.matrixT()),rTmp);
  multMatMat(rTmp, transpose(rSchur.matrixU()),rQaq);
  rQaq -= rMatNonSym;
  nF=rQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";

  ssout << "----------------------------Test Complex Schur decomposition------------------------------------------" << "\n";
  ssout << "----------------------------real symmetric--------------------" << "\n";
  RealComplexSch rCplxSchur(size);
  rMat.loadFromFile(rSym.c_str());
  ssout << "Symmetric matrix " << rMat << "\n";
  rCplxSchur.compute(rMat);
  ssout << "T matrix " << rCplxSchur.matrixT() << "\n";
  ssout << "Unitary matrix " << rCplxSchur.matrixU() << "\n";
  multMatMat(rCplxSchur.matrixU(),transpose(rCplxSchur.matrixU()),cTmp);
  cTmp -= cEye5;
  nF=cTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(rCplxSchur.matrixU(),rCplxSchur.matrixT(), cTmp);
  multMatMat(cTmp, conj(transpose(rCplxSchur.matrixU())), cQaq);
  cQaq -= cmplx(rMat);
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";

  ssout << "----------------------------real non-symmetric--------------------" << "\n";
  rMatNonSym.loadFromFile(rNonSym.c_str());
  ssout << "Non symmetric matrix " << rMatNonSym << "\n";
  rCplxSchur.compute(rMatNonSym);
  ssout << "T matrix " << rCplxSchur.matrixT() << "\n";
  ssout << "Unitary matrix " << rCplxSchur.matrixU() << "\n";
  multMatMat(rCplxSchur.matrixU(),conj(transpose(rCplxSchur.matrixU())),cTmp);
  cTmp -= cEye5;
  nF=cTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(rCplxSchur.matrixU(),rCplxSchur.matrixT(), cTmp);
  multMatMat(cTmp, conj(transpose(rCplxSchur.matrixU())), cQaq);
  cQaq -= cmplx(rMatNonSym);
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";

  ssout << "----------------------------Test Complex Schur decomposition------------------------------------------" << "\n";
  ssout << "----------------------------Hermitian --------------------" << "\n";
  CmplxComplexSch cCplxSchur(size);
  cMat.loadFromFile(cSym.c_str());
  ssout << "Symmetric matrix " << cMat << "\n";
  cCplxSchur.compute(cMat);
  ssout << "T matrix " << cCplxSchur.matrixT() << "\n";
  ssout << "Unitary matrix " << cCplxSchur.matrixU() << "\n";
  multMatMat(rCplxSchur.matrixU(),conj(transpose(rCplxSchur.matrixU())),cTmp);
  cTmp -= cEye5;
  nF=cTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(cCplxSchur.matrixU(),cCplxSchur.matrixT(), cTmp);
  multMatMat(cTmp, conj(transpose(cCplxSchur.matrixU())), cQaq);
  cQaq -= cmplx(cMat);
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";
  multMatMat(cCplxSchur.matrixU(),conj(transpose(cCplxSchur.matrixU())), cTmp);
  ssout << "||Q*Q^T|| = " << cTmp.normOne() << "\n";
  nbErrors+= checkValue(cTmp.normOne(), 1., tol, errors, " norm Froebenius ||Q*Q^T||");

  ssout << "Sort complex Schur " << "\n";
  cCplxSchur.swapSchur(sortOrder);
  ssout << "Sort order " << sortOrder << "\n";
  ssout << "T matrix after being sorted " << cCplxSchur.matrixT() << "\n";
  ssout << "Unitary matrix after being sorted " << cCplxSchur.matrixU() << "\n";
  multMatMat(rCplxSchur.matrixU(),conj(transpose(rCplxSchur.matrixU())),cTmp);
  cTmp -= cEye5;
  nF=cTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(cCplxSchur.matrixU(),cCplxSchur.matrixT(), cTmp);
  multMatMat(cTmp, conj(transpose(cCplxSchur.matrixU())), cQaq);
  cQaq -= cmplx(cMat);
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";

  ssout << "----------------------------Non-Hermitian--------------------" << "\n";
  cMatNonSym.loadFromFile(cNonSym.c_str());
  ssout << "Non symmetric matrix " << cMatNonSym << "\n";
  cCplxSchur.compute(cMatNonSym);
  ssout << "T matrix " << cCplxSchur.matrixT() << "\n";
  ssout << "Unitary matrix " << cCplxSchur.matrixU() << "\n";
  multMatMat(cCplxSchur.matrixU(),conj(transpose(cCplxSchur.matrixU())), cTmp);
  cTmp -= cEye5;
  nF=cTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";
  multMatMat(cCplxSchur.matrixU(),cCplxSchur.matrixT(), cTmp);
  multMatMat(cTmp, conj(transpose(cCplxSchur.matrixU())), cQaq);
  cQaq -= cmplx(cMatNonSym);
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";
  cCplxSchur.swapSchur(sortOrder);
  ssout << "Sort order " << sortOrder << "\n";
  ssout << "T matrix after being sorted " << cCplxSchur.matrixT() << "\n";
  ssout << "Unitary matrix after being sorted " << cCplxSchur.matrixU() << "\n";
  multMatMat(cCplxSchur.matrixU(),cCplxSchur.matrixT(), cTmp);
  multMatMat(cTmp, conj(transpose(cCplxSchur.matrixU())), cQaq);
  cQaq -= cmplx(cMatNonSym);
  nF=cQaq.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q*T*^T*Q-A||");
  ssout << "||Q*T*Q^T-A|| = " << nF << "\n";
  multMatMat(cCplxSchur.matrixU(),conj(transpose(cCplxSchur.matrixU())), cTmp);
  cTmp -= cEye5;
  nF=cTmp.normFrobenius();
  nbErrors+= checkValue(nF, 0., tol, errors, " norm Froebenius ||Q^T*Q-I||");
  ssout << "||Q*Q^T-I|| = " << nF << "\n";

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
