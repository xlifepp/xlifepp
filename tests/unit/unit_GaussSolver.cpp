/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_GaussSolver.cpp
	\author E. Lunéville
	\since 19 aug 2014
	\date 19 aug 2014

	Low level tests of Gauss solver.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;


namespace unit_GaussSolver {

void unit_GaussSolver(int argc, char* argv[], bool check)
{
  String rootname = "unit_GaussSolver";
  trace_p->push(rootname);
  verboseLevel(30);

  //! create a mesh and Domains
  Strings sidenames(4,"");
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=3,_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");

  //! create Lagrange P1 space and unknown
  Space V1(_domain=omega, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u(V1, _name="u");
  TestFunction v(u, _name="v");

  //!define row dense matrix and vector
  BilinearForm uv=intg(omega,u*v);
  TermMatrix M(uv,_storage=denseRow, _name="uv");
  TermVector un(u,omega,1.,"un");
  TermVector b=M*un;
  Complex  z(2,1);
  TermVector bi=z*b;
  TermMatrix A=M;

  Number nbErrors = 0;
  String errors;
  Real eps=0.0001;

  //!solve using Gauss

  //!   "============== Gauss solver with real scalar row dense matrix and real rhs =============="
  TermVector x=gaussSolve(A,b);
  nbErrors += checkValuesN2( x , un, eps ,  errors, "real scalar row dense matrix and real rhs");

  A=M;
  //!   "============== Gauss solver with real scalar row dense matrix and complex rhs =============="
  x=gaussSolve(A,bi);
  nbErrors += checkValuesN2( x , z*un, eps ,  errors, "real scalar row dense matrix and complex rhs");

  //!define col dense matrix
  //!   "============== Gauss solver with real scalar col dense matrix and real rhs =============="
  M.setStorage(_dense,_col);
  A=M;
  x=gaussSolve(A,b);
  nbErrors += checkValuesN2( x , un, eps ,  errors, " real scalar col dense matrix and real rhs");

  //!define dual dense matrix
  //!   "============== Gauss solver with real scalar dual dense matrix and real rhs =============="
  M.setStorage(_dense,_dual);
  A=M; 
  x=gaussSolve(A,b);
  nbErrors += checkValuesN2( x , un, eps ,  errors, " real scalar dual dense matrix and real rhs");

  A=z*M;
  //!    "============== Gauss solver with complex scalar row dense matrix and real rhs =============="
  x=gaussSolve(A,b);
  nbErrors += checkValuesN2( z*x , un, eps ,  errors, " complex scalar row dense matrix and real rhs");

  A=z*M;
  //!    "============== Gauss solver with complex scalar row dense matrix and complex rhs =============="
  x=gaussSolve(A,bi);
  nbErrors += checkValuesN2( x , un, eps ,  errors, " complex scalar row dense matrix and complex rhs");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
