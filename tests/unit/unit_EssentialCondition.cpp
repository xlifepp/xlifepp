/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_EssentialCondition.cpp
	\since 14 jan 2014
	\date  14 jan 2014

    low level tests of EssentialCondition class
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_EssentialCondition {

Real k=1;

//!data on sigma-
Complex gp(const Point& P, Parameters& pa = defaultParameters)
{
  return -i_*k;
}

//!cos basis
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                    //get the parameter h (user definition)
  Int n=pa("basis index")-1;         //get the index of function to compute
  if(n==0) return sqrt(1./h);
  else     return sqrt(2./h)*cos(n*pi_*(y+h*0.5)/h);
}

void unit_EssentialCondition(int argc, char* argv[], bool check)
{
  String rootname = "unit_EssentialCondition";
  trace_p->push(rootname);
  verboseLevel(21);
  
  String errors;
  Number nbErrors = 0;

  //!parameters
  Real h=1;
  Parameters params(h,"h");
  params << Parameter(k,"k");

  //!create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=pi";
  sidenames[2] = "y=h"; sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=pi_,_ymin=-0.5,_ymax=0.5,_nnodes=Numbers(151,51),_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=pi");
  Domain sigmaM=mesh2d.domain("x=0");

  //!create Lagrange interpolation space
  Interpolation* LagrangePk=findInterpolation(Lagrange,standard,2,H1);
  Space V(_domain=omega, _interpolation=_P2, _name="V");
  Unknown u(V, _name="u");
  Unknown w(V, _name="w", _dim=_2D);

  //!create spectral space
  Number N=5;
  Space Sp(_domain=sigmaP, _basis=Function(cosny,params), _dim=N, _name="cos(n*pi*y)");
  Unknown phiP(Sp, _name="phiP");

  Function fp(gp,"gp",params);

  //!test LcOperatorOnUnknown!
  theCout << "LcOperatorOnUnknown() :" << LcOperatorOnUnknown() << eol;
  theCout << "LcOperatorOnUnknown(Unknown) : " << LcOperatorOnUnknown(u) << eol;
  theCout << "LcOperatorOnUnknown(Unknown, Real) : " << LcOperatorOnUnknown(u,2) << eol;
  theCout << "LcOperatorOnUnknown(Unknown, Complex) : " << LcOperatorOnUnknown(u,3.*i_) << eol;
  theCout << "LcOperatorOnUnknown(OperatorOnUnknown) : " << LcOperatorOnUnknown(dx(u)) << eol;
  theCout << "LcOperatorOnUnknown(OperatorOnUnknown, Real) : " << LcOperatorOnUnknown(dx(u),2) << eol;
  theCout << "LcOperatorOnUnknown(OperatorOnUnknown, Complex) : " << LcOperatorOnUnknown(dx(u),3.*i_) << eol;

  LcOperatorOnUnknown lc(dx(u)|sigmaP,2);
  theCout << "LcOperatorOnUnknown(LcOperatorOnUnknown) : " << LcOperatorOnUnknown(lc) << eol;
  LcOperatorOnUnknown lcb;
  theCout << "LcOperatorOnUnknown = LcOperatorOnUnknown : " << (lcb = lc) << " | ";
  theCout << "[" << tostring(lcb)+"] = ["+tostring(lc) << "]" << eol;
  
  lcb.setDomain(sigmaM);
  theCout << "LcOperatorOnUnknown.setDomain(sigmaM) : " << lcb << eol;
  theCout << "LcOperatorOnUnknown | Domain (sigmaP) : " << (lcb | sigmaP) << eol;
  
  theCout << "LcOperatorOnUnknown += OperatorOnUnknown : " << (lcb += u) <<eol;
  theCout << "LcOperatorOnUnknown += OperatorOnUnknown : " << (lcb += dy(u)) << eol;
  theCout << "LcOperatorOnUnknown += LcOperatorOnUnknown : " << (lcb += lc) << eol;
  
  lcb=lc;
  theCout << "LcOperatorOnUnknown -= Unknown : " << (lcb -= u) << eol;
  theCout << "LcOperatorOnUnknown -= OperatorOnUnknown : " << lcb - dy(u) << eol;
  theCout << "LcOperatorOnUnknown -= LcOperatorOnUnknown : " << (lcb - lc) << eol;
  
  lcb=lc;
  theCout << "LcOperatorOnUnknown *= Real (2) : " << (lcb *= 2) << eol;
  theCout << "LcOperatorOnUnknown /= Real (2) : " << (lcb /= 2) << eol;
  theCout << "LcOperatorOnUnknown *= Complex (i) : " << (lcb *=i_) << eol;
  theCout << "LcOperatorOnUnknown /= Complex (i) : " << (lcb /=i_) << eol;

  theCout << "+ LcOperatorOnUnknown : " << (+lcb) << eol;
  theCout << "- LcOperatorOnUnknown : " << (-lcb) << eol;
  theCout << "LcOperatorOnUnknown + Unknown : " << (lcb+u) << eol;
  theCout << "LcOperatorOnUnknown - Unknown : " << (lcb-u) << eol;
  theCout << "Unknown + LcOperatorOnUnknown : " << (u+lcb) << eol;
  theCout << "Unknown - LcOperatorOnUnknown : " << (u-lcb) << eol;
  
  theCout << "LcOperatorOnUnknown + OperatorOnUnknown : " << (lcb+dy(u)) << eol;
  theCout << "LcOperatorOnUnknown - OperatorOnUnknown : " << (lcb-dy(u)) << eol;
  
  theCout << "OperatorOnUnknown + LcOperatorOnUnknown : " << (dy(u)+lcb) << eol;
  theCout << "OperatorOnUnknown - LcOperatorOnUnknown : " << (dy(u)-lcb) << eol;         
  theCout << "LcOperatorOnUnknown + LcOperatorOnUnknown : " << (lcb+lc) << eol;          
  theCout << "LcOperatorOnUnknown - LcOperatorOnUnknown : " << (lcb-lc) << eol;

  theCout << "LcOperatorOnUnknown * Real : " << (lcb*2) << eol;
  theCout << "LcOperatorOnUnknown / Real : " << (lcb/2) << eol;            
  theCout << "LcOperatorOnUnknown * Complex : " << (lcb*3.*i_) << eol;
  theCout << "LcOperatorOnUnknown / Complex : " << (lcb/(3.*i_)) << eol;
            
  theCout << "Real * LcOperatorOnUnknown : " << (2*lcb) << eol;           
  theCout << "Complex * LcOperatorOnUnknown : " << (3.*i_*lcb) << eol;
            
  theCout << "OperatorOnUnknown + OperatorOnUnknown : " << (dx(u)+dy(u)) << eol;
  theCout << "OperatorOnUnknown - OperatorOnUnknown : " << (dx(u)-dy(u)) << eol;
            
  theCout << "OperatorOnUnknown + Unknown : " << (dx(u)+u) << eol;           
  theCout << "OperatorOnUnknown - Unknown : " << (dx(u)-u) << eol;
            
  theCout << "Unknown + OperatorOnUnknown : " << (u+dy(u)) << eol;          
  theCout << "Unknown - OperatorOnUnknown : " << (u-dy(u)) << eol;

  theCout << "Unknown + Unknown : " << (u+phiP) << eol;
  theCout << "Unknown - Unknown : " << (u-phiP) << eol;
 
  theCout << "Unknown | Domain : " << (u|sigmaP) << eol;          
  theCout << "OperatorOnUnknown | Domain : " << (dx(u)|sigmaP) << eol;
  
  nbErrors+=checkValue(theCout, rootname+"/LcOperatorOnUnknown.in", errors, "LcOperatorOnUnknown", check);
  
  theCout << "EssentialCondition(LcOperatorOnUnknown) : " << EssentialCondition(u) << eol;
  theCout << "EssentialCondition(Domain, Unknown) : " << EssentialCondition(sigmaP,u) << eol;
    
  theCout << "EssentialCondition(Domain, OperatorOnUnknown) : " << EssentialCondition(sigmaP,dx(u)) << eol;
  theCout << "EssentialCondition(Unknown, Function) : " << EssentialCondition(u,fp) << eol;
  
  theCout << "EssentialCondition(Unknown, function) : " << EssentialCondition(u,gp) << eol;
  theCout << "EssentialCondition(Domain, Unknown, Function) : " << EssentialCondition(sigmaP,u,fp) << eol;
  
  theCout << "EssentialCondition(Domain, Unknown, function) : " << EssentialCondition(sigmaP,u,gp) << eol;  
  theCout << "Unknown = 0 : " << (u=0) << eol; 
  
  theCout << "Unknown = Function : " << (u=fp) << eol;
  theCout << "Unknown = function : " << (u=gp) << eol;
  theCout << "OperatorOnUnknown = 0 : " << (dx(u)=0) << eol;
  
  theCout << " on(Domain, EssentialCondition) : " << on(sigmaP, dx(u)=0 ) << eol;
  theCout << "OperatorOnUnknown = Function : " << (dx(u)=fp) << eol;
  theCout << "OperatorOnUnknown = function : " << (dx(u)=gp) << eol;
  theCout << "LcOperatorOnUnknown = 0 : " << (lc=0) << eol;
  theCout << "LcOperatorOnUnknown = Function : " << (lc=fp) <<eol;
  theCout << "LcOperatorOnUnknown = function : " << (lc=gp) << eol;
  EssentialCondition bc = (dx(u)=0);
  bc.setDomain(sigmaM);
  theCout << "EssentialCondition.setDomain(sigmaM) : " << bc << eol;
  theCout << "EssentialCondition | Domain (sigmaP) : " << ((dx(u)=0) | sigmaP) << eol;
  
  theCout << "LcOperatorOnUnknown = 0 : " << (dx(u)|sigmaP = 0)  << eol;
  theCout << " on(Domain, EssentialCondition) : " << on(sigmaP, dx(u)=0 ) << eol;               
  theCout << "EssentialCondition | Domain (sigmaP) : " << ((u=fp) | sigmaP) << eol;
                                            
  theCout << "LcOperatorOnUnknown = 0 : " << (u|sigmaP = fp)  << eol;
  theCout << "on(Domain, EssentialCondition) : " << on(sigmaP, u=fp ) << eol;
  theCout << "EssentialCondition | Domain (sigmaP) : " << ((u=gp) | sigmaP) << eol;
  
  theCout << "LcOperatorOnUnknown = 0 : " << (u|sigmaP = gp)  << eol;
  theCout << "on(Domain, EssentialCondition) : " << on(sigmaP, u=gp ) << eol;
                                                                     
  bc= (u+phiP=0) | sigmaP;
  theCout << " bc= (u+phiP)=0 | sigmaP) : " << bc << eol;                                                                   
  bc= (u+(phiP*_n)=0) | sigmaP;
  theCout << " bc= (u+(phiP*_n)=0) | sigmaP) : bc.consitency = " << bc.consistency(false) << eol;
  
  TermVector tv(intg(sigmaP,u), _name="tv");
  theCout << "EssentialCondition(TermVector) : " << EssentialCondition(tv,0.) << eol;
  theCout << "EssentialCondition(intg(sigmaP,u)) : " << EssentialCondition(intg(sigmaP,u),1.) << eol;
  
  nbErrors+=checkValue(theCout, rootname+"/EssentialCondition.in", errors, "Test EssentialCondition", check);

  theCout << "EssentialConditions() : " << EssentialConditions() << eol;         
  theCout << "(u=0) |sigmaM :"<<   EssentialConditions( (u=0) |sigmaM ) << eol;
  EssentialConditions bcs( (u=0) |sigmaM ) ;
  theCout << "bcs = " << bcs; 
  theCout << "bcs & (u=0) |sigmaP : " <<  ( bcs & ( (u=0) |sigmaP) );
  theCout << "(u=0) |sigmaP & bcs : " <<  (( (u=0) |sigmaP ) & bcs) ;
  theCout << "(u=fp) |sigmaM & (u=0) |sigmaP : " <<  (( (u=fp) |sigmaM ) &( (u=0) |sigmaP ));

  nbErrors+=checkValue(theCout, rootname+"/EssentialConditions.in", errors, "Test EssentialConditionS", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
