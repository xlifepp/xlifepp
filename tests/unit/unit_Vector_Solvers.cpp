/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Vector_Solvers.cpp
  \author E. Lunéville
	\since 25 mar 2016
	\date  25 mar 2016

	Test all solvers on a vector problem (2D elasticity)
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Vector_Solvers {

//!data on sigma-

Real k=1.;
Real mu=1.;
Real lambda=1.;
Real omg=1.;

Vector<Real> uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Vector<Real> u(2,0.);
  u(1)= sin(k*pi_*x)*cos(k*pi_*y);
  u(2)=-sin(k*pi_*y)*cos(k*pi_*x);
  return u;
}

Vector<Real> f(const Point& P, Parameters& pa = defaultParameters)
{
   return (2.*mu*k*k*pi_*pi_+omg*omg)*uex(P);
}


void unit_Vector_Solvers(int argc, char* argv[], bool check)
{
  String rootname = "unit_Vector_Solvers";
  trace_p->push(rootname);
  verboseLevel(10);

  //!create problem
  Mesh mesh2d(Rectangle(_xmin=-0.5,_xmax=0.5,_ymin=-0.5,_ymax=0.5,_nnodes=15,_domain_name="Omega"),_shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u", _dim=2); TestFunction v(u, _name="v");
  TermMatrix M(intg(omega,u|v));
  TermVector Uex(u,omega,uex);
  Real omg2=omg*omg;
  BilinearForm auv=2.*mu*intg(omega,epsilon(u)%epsilon(v))+lambda*intg(omega,div(u)*div(v))+omg2*intg(omega,u|v);
  LinearForm fv=intg(omega,f|v);
  TermMatrix A(auv, _name="A");
  TermVector B(fv, _name="B");

  //!test solver
  TermVector U, E;
  TermMatrix ALU;
  Number nbErrors = 0;
  String errors;
  Real e,t;
  bool theCoutDetails = false;
  bool ok = true;
  Real eps = 0.05;  //waited error (below to)
  
  #ifdef XLIFEPP_WITH_UMFPACK
  elapsedTime(); U=umfpackSolve(A,B,true); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "UMFPACK Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  
  #endif // XLIFEPP_WITH_UMFPACK

  elapsedTime(); U=ldltSolve(A,B,ALU); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "LDLt Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  

  elapsedTime(); U=gaussSolve(A,B,true); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "Gauss Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  

  elapsedTime(); U=cgSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "CG Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  

  elapsedTime(); U=bicgSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "Bicg Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  

  elapsedTime(); U=bicgStabSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "BicgStab Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  

  elapsedTime(); U=gmresSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "Gmres Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  

  elapsedTime(); U=qmrSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "QMR Solver TermVector/TermMatrix, time:"+tostring(t)+"s");  
  
  if (check)
  {
   if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
   else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else
  { theCout << "Data updated " << eol; }
 
 trace_p->pop();
 
}

}
