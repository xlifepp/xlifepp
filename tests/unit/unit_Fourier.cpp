/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Fourier; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file unit_Fourier.cpp
\author E. Lunéville
\since 17 Juin 2015
\date 17 Juin 2015

    Solve  the 2D Laplace Fourier problem
        -lap(u) = u  on ]0,1[x]0,1[
        du/dn = u    on x=0 (Sigma)
        du/dn = 0    on x=1, y=0, y=1

    in weak form :
        find u in H1, for all v in H1
        intg_Omega grad(u)|grad(v) + intg_Sigma uv = intg_Omega fv

    exact solution : uex=((1-x)*(1-x)-3)*cos(n*pi*y) -> f=n*n*pi*pi*uex-2*cos(n*pi*y);

*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_Fourier
{

Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Number n=2;
  return ((1-x)*(1-x)-3)*std::cos(n*pi_*y);
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Number n=2;
  return n*n*pi_*pi_*uex(P)-2*std::cos(n*pi_*y);
}

void unit_Fourier(int argc, char* argv[], bool check)
{
  String rootname = "unit_Fourier";
  trace_p->push(rootname);
  verboseLevel(30);

  String errors;
  Number nbErrors = 0;
  Real eps=0.006;
  Number kmax=4, nx=21;

  theCout<<"Solving 2D Laplace Fourier Laplace problem on unit square with h="<<1./(nx-1)<<eol;

  for (Number sh=4; sh<=5; sh++)
  {
    Strings sidenames("y=ymin", "x=xmax", "y=ymax", "x=xmin");
    Mesh mesh2d(Rectangle(_xmin=0, _xmax=1, _ymin=0, _ymax=1, _nnodes=nx, _side_names=sidenames), _shape=ShapeType(sh), _order=1, _generator=structured);
    Domain Omega = mesh2d.domain("Omega");
    Domain Sigma = mesh2d.domain("x=xmin");

    String Mess;
    for (Number k=1; k<=kmax; k++)
    {
      for (Number t=0; t<=1; t++)
      {
        if ((sh==4 && t==0) || sh==5)
        {
          Interpolation&  interp=interpolation(Lagrange, FESubType(t), k, H1);
          Space Vk(_domain=Omega, _FE_type=Lagrange, _FE_subtype=t, _order=k, _name="Vk", _notOptimizeNumbering);
          Unknown u(Vk, _name="u"); TestFunction v(u, _name="v");
          TermMatrix A(intg(Omega, grad(u) | grad(v))+intg(Sigma, u * v), _name="A");
          TermMatrix M(intg(Omega, u * v), _name="M");
          TermVector F(intg(Omega, f * v), _name="F");
          TermVector U=directSolve(A,F);
          TermVector Uex(u,Omega,uex, _name="Uex");
          nbErrors += checkValuesL2(U, Uex,  M, eps,  errors, words("shape",sh) + tostring(k) + words("FE subname",t)  + "(nb dofs ="+tostring(U.size()) );
        }
      }
    }
  }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
