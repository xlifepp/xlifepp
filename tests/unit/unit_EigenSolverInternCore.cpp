/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file unit_EigenSolverInternCore.cpp
  \author ManhHa NGUYEN
  \since 23 Jan 2013
  \date 24 July 2013

  Tests of direct eigen solver for dense matrix.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_EigenSolverInternCore {

//// Function to verify the result of standard eigen solver
template<typename K, class Mat, class EigValue, class EigVector>
Vector<Real> verifyInternStdDenseResult(Mat& matA, EigValue& eigValue, EigVector& eigVector, Number size, Real epsilon = 1.0e-6)
{
  VectorEigenDense<K> lamdaX(size), Ax(size);
  Vector<K> eps(size, K());
  Vector<Real> result(size, -1.0);
  Vector<Real>::iterator itResult = result.begin();
  
  Indexing idx(4);
  idx[0] = idx[1] = 0;
  idx[2] = idx[3] = size;
  for (Number i = 0; i < size; i++, ++itResult)
  {
    matA.multSubMatVecVec(idx, eigVector.columnVector(i), Ax);
    lamdaX = eigValue[i] * eigVector.columnVector(i);
    eps = Ax - lamdaX;
    *itResult = (eps.norm2() < epsilon) ? 0.0 : -1.0;
  }
  
  return result;
}


//! Function to verify the result of generalized eigen solver
template<typename K, class Mat, class EigValue, class EigVector>
Vector<Real> verifyInternGenDenseResult(Mat& matA, Mat& matB, EigValue& eigValue, EigVector& eigVector, Number size, EigenSolverMode slvMode, Real epsilon = 1.0e-6)
{
  VectorEigenDense<K> Ax(size, K());
  VectorEigenDense<K> Bx(size, K());
  VectorEigenDense<K> lamdaX(size, K());
  Vector<K> eps(size, K());
  Vector<Real> result(size, -1.0);
  Vector<Real>::it_vk itResult = result.begin();
  Indexing idx(4);
  idx[0] = idx[1] = 0;
  idx[2] = idx[3] = size;
  
  if ( _Ax_lBx == slvMode)
  {
    for (Number i = 0; i < size; i++, itResult++)
    {
      matA.multSubMatVecVec(idx, eigVector.columnVector(i), Ax);
      matB.multSubMatVecVec(idx, eigVector.columnVector(i), Bx);
      lamdaX = eigValue[i] * Bx;
      eps = Ax - lamdaX;
      *itResult = (eps.norm2() < epsilon) ? 0.0 : -1.0;
    }
  }
  else if (_ABx_lx == slvMode)
  {
    Mat matC(size);
    multMatMat(matA, matB, matC);
    return verifyInternStdDenseResult<K>(matC, eigValue, eigVector, size);
  }
  else if (_BAx_lx == slvMode)
  {
    Mat matC(size);
    multMatMat(matB, matA, matC);
    return verifyInternStdDenseResult<K>(matC, eigValue, eigVector, size);
  }
  
  return result;
}

void unit_EigenSolverInternCore(int argc, char* argv[], bool check)
{
  String rootname = "unit_EigenSolverInternCore";
  trace_p->push(rootname);
  std::stringstream ssout; // string stream receiving results
  ssout.precision(testPrec);
  verboseLevel(10); 
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.001;
  bool isTM=true;
  
  const Number size = 20;
  
  const String rSym20DuplEigValue(inputsPathTo(rootname+"/rSym20DuplEigValue.data"));
  const String rSym20DiffEigValue(inputsPathTo(rootname+"/rSym20DiffEigValue.data"));
  const String rSymPos20(inputsPathTo(rootname+"/rSymPos20.data"));
  const String rNonSym20(inputsPathTo(rootname+"/rNonSym20.data"));
  const String cSelfAdjoint(inputsPathTo(rootname+"/cSelfAdjoint20.data"));
  const String cSymPos(inputsPathTo(rootname+"/cPosDef20.data"));
  const String cNonSym(inputsPathTo(rootname+"/cNonSym20.data"));
  
  MatrixEigenDense<Real> rMat(size), rMatDup(size), rMatPos(size), rMatNonSym(size, size), rEigVec(size, size),rIden(size), rTmp(size), rPseudoEigVal(size), rQaQ(size);
  MatrixEigenDense<Complex> cMat(size), cMatPos(size), cMatNonSym(size), cEigVec(size), cIden(size), cTmp(size);
  
  VectorEigenDense<Real> rEigVal(size);
  VectorEigenDense<Complex> cEigVal(size);
  
  typedef SelfAdjointEigenSolver<MatrixEigenDense<Real> > RealStd;
  typedef SelfAdjointEigenSolver<MatrixEigenDense<Complex> > ComplexStd;
  typedef GeneralizedSelfAdjointEigenSolver<MatrixEigenDense<Real> > RealGen;
  typedef GeneralizedSelfAdjointEigenSolver<MatrixEigenDense<Complex> > ComplexGen;
  typedef RealEigenSolver<MatrixEigenDense<Real> > RealStdIntern;
  typedef ComplexEigenSolver<MatrixEigenDense<Complex> > CplxStdIntern;
  
  rMat.loadFromFile(rSym20DiffEigValue.c_str());
  rMatDup.loadFromFile(rSym20DuplEigValue.c_str());
  rMatPos.loadFromFile(rSymPos20.c_str());
  rMatNonSym.loadFromFile(rNonSym20.c_str());
  cMat.loadFromFile(cSelfAdjoint.c_str());
  cMatPos.loadFromFile(cSymPos.c_str());
  cMatNonSym.loadFromFile(cNonSym.c_str());
  for (Number i = 0; i < size; ++i) {
      rIden.coeffRef(i,i) = NumTraits<Real>::one();
      cIden.coeffRef(i,i) = NumTraits<Complex>::one();
  }

  Real nF;
  Vector<Real> Vapprox;
  // ssout << "\n--------------------------- STANDARD selfAdjoint eigenvalue problem ---------------------------------------------------------------\n";
  // ssout << "\n--------------------------- Real symmetric -----------------------------------\n";
  

  RealStd eigStd(size);
  eigStd.compute(rMat);
  rEigVal = eigStd.eigenvalues();
  rEigVec = normalizeEigenVectors(eigStd.eigenvectors());
  // ssout << "Eigen values no duplication" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << rEigVec << "\n";
  nbErrors+=checkValues(rEigVec, rootname+"/rEigVec0.in", tol, errors, "rEigVec", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigVal0.in", tol, errors, "rEigVal", check); 
  multMatMat(conj(transpose(rEigVec)), rEigVec, rTmp);
  rTmp -= rIden;
  nF = rTmp.normFrobenius();
  Vapprox = verifyInternStdDenseResult<Real>(rMat, rEigVal, rEigVec, size);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Real Symetric case 1: error");
  nbErrors+= checkValue(nF, 0., tol, errors, "Real symmetric case 1: Frobenius norm");
  // ssout << "|| eigVec^T*eigVec - I|| = " << nF <<"\n";
  // ssout << "|| A*u - lamda*u|| = " << Vapprox <<"\n";
  
  eigStd.compute(rMatDup);
  rEigVal = eigStd.eigenvalues();
  rEigVec = normalizeEigenVectors(eigStd.eigenvectors());
  // ssout << "Duplicated eigen values" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << rEigVec << "\n";
  nbErrors+=checkValues(rEigVec, rootname+"/rEigVecDup0.in", tol, errors, "rEigVal", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigValDup0.in", tol, errors, "rEigVal", check);
  multMatMat(conj(transpose(rEigVec)), rEigVec, rTmp);
  rTmp -= rIden;
  nF = rTmp.normFrobenius();
  Vapprox = verifyInternStdDenseResult<Real>(rMatDup, rEigVal, rEigVec, size);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Real Symetric case 2: error");
  nbErrors+= checkValue(nF, 0., tol, errors, "Real symmetric case 2: norm Froebeniu");
  // ssout << "|| eigVec^T*eigVec - I|| = " << nF << "\n";
  // ssout << "|| A*u - lamda*u|| = " << Vapprox << "\n";
  
  // ssout << "\n--------------------------- Complex Selfadjoint -----------------------------------\n";
  ComplexStd ceigStd(cMat);
  rEigVal = ceigStd.eigenvalues();
  // theCout << ceigStd.eigenvectors() << eol; 
  cEigVec = normalizeEigenVectors(ceigStd.eigenvectors());
  // theCout << cEigVec << eol;
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << cEigVec << "\n";
  nbErrors+=checkValues(cEigVec, rootname+"/cEigVec1.in", tol, errors, "cEigVec", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigVal1.in", tol, errors, "rEigVal", check);
  multMatMat(conj(transpose(cEigVec)), cEigVec, cTmp);
  cTmp -= cIden;
  nF = cTmp.normFrobenius();
  Vapprox = verifyInternStdDenseResult<Complex>(cMat, rEigVal, cEigVec, size);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Complex selfadjoint: error");
  nbErrors+= checkValue(nF, 0., tol, errors, "Complex selfadjoint: Frobenius norm");
  // ssout << "|| eigVec^T*eigVec - I|| = " << nF << "\n";
  // ssout << "|| A*u - lamda*u|| = " << Vapprox << "\n";
  
  // ssout << "\n--------------------------- test GENERALIZED selfAdjoint eigenvalue problem ---------------------------------------------------------------\n";
  // ssout << "\n--------------------------- test real symmetric -----------------------------------\n";
  RealGen eigGen(size);
  eigGen.compute(rMat, rMatPos, _Ax_lBx | _computeEigenVector);
  rEigVal = eigGen.eigenvalues();
  rEigVec = normalizeEigenVectors(eigGen.eigenvectors());
  // ssout << "Computation mode AX = lamdaBX" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << rEigVec << "\n";
  nbErrors+=checkValues(cEigVec, rootname+"/rEigVecAxmlB.in", tol, errors, "cEigVec Ax-lB", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigValAxmlB.in", tol, errors, "rEigVal Ax-lB", check);
  Vapprox=verifyInternGenDenseResult<Real>(rMat, rMatPos, rEigVal, rEigVec, size, _Ax_lBx);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Generalized real symetric case 1: error");
  // ssout << "|| A*u - lamda*B*u|| = " << Vapprox  << "\n";
  
  eigGen.compute(rMat, rMatPos, _ABx_lx | _computeEigenVector);
  rEigVal = eigGen.eigenvalues();
  rEigVec = normalizeEigenVectors(eigGen.eigenvectors());
  // ssout << "Computation mode ABX = lamdaX" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << rEigVec << "\n";
  nbErrors+=checkValues(cEigVec, rootname+"/rEigVecABxml.in", tol, errors, "cEigVec ABx-l", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigValABxml.in", tol, errors, "rEigVal ABx-l", check);
  Vapprox=verifyInternGenDenseResult<Real>(rMat, rMatPos, rEigVal, rEigVec, size, _ABx_lx);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Generalized real symetric case 2 : error");
  // ssout << "|| A*B*u - lamda*u|| = " << Vapprox << "\n";
  
  eigGen.compute(rMat, rMatPos, _BAx_lx | _computeEigenVector);
  rEigVal = eigGen.eigenvalues();
  rEigVec = normalizeEigenVectors(eigGen.eigenvectors());
  // ssout << "Computation mode BAX = lamdaX" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << rEigVec << "\n";
  nbErrors+=checkValues(cEigVec, rootname+"/rEigVecBAxml.in", tol, errors, "cEigVec BAx-l", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigValBAxml.in", tol, errors, "rEigVal BAx-l", check);
  Vapprox=verifyInternGenDenseResult<Real>(rMat, rMatPos, rEigVal, rEigVec, size, _BAx_lx);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Generalized real symetric case 3: error");
  // ssout << "|| B*A*u - lamda*u|| = " <<  Vapprox<< "\n";
  
  // ssout << "\n--------------------------- test Complex selfAdjoint -----------------------------------\n";
  ComplexGen cEigGen(size);
  cEigGen.compute(cMat, cMatPos, _Ax_lBx | _computeEigenVector);
  rEigVal = cEigGen.eigenvalues();
  cEigVec = normalizeEigenVectors(cEigGen.eigenvectors());
  // ssout << "Computation mode AX = lamdaBX" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << cEigVec << "\n";
  nbErrors+=checkValues(cEigVec, rootname+"/cEigVecAxmlB.in", tol, errors, "cEigVec Ax-lB", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigValAxmlB3.in", tol, errors, "rEigVal Ax-lB", check);
  Vapprox=verifyInternGenDenseResult<Complex>(cMat, cMatPos, rEigVal, cEigVec, size, _Ax_lBx);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Generalized complex selfadjoint problem case 1: error");
  // ssout << "|| A*u - lamda*B*u|| = " << Vapprox << "\n";
  
  cEigGen.compute(cMat, cMatPos, _ABx_lx | _computeEigenVector);
  rEigVal = cEigGen.eigenvalues();
  cEigVec = normalizeEigenVectors(cEigGen.eigenvectors());
  // ssout << "Computation mode ABX = lamdaX" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << cEigVec << "\n";
  nbErrors+=checkValues(cEigVec, rootname+"/cEigVecABxml.in", tol, errors, "cEigVec ABx-lx", check);
  nbErrors+=checkValues(rEigVal, rootname+"/rEigValABxml3.in", tol, errors, "rEigVal ABx-lx", check);
  Vapprox=verifyInternGenDenseResult<Complex>(cMat, cMatPos, rEigVal, cEigVec, size, _ABx_lx);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Generalized complex symmetric case 2: error");
  // ssout << "|| A*B*u - lamda*u|| = " << Vapprox << "\n";

  cEigGen.compute(cMat, cMatPos, _BAx_lx | _computeEigenVector);
  rEigVal = cEigGen.eigenvalues();
  cEigVec = normalizeEigenVectors(cEigGen.eigenvectors());
  nbErrors+=checkValues(cEigVec, rootname+"/cEigVecBAxml.in", tol, errors, "real/complex cEigVec BAx-lx", check);
  nbErrors+=checkValues(rEigVal, rootname+"/cEigValBAxml.in", tol, errors, "real/complex rEigVal BAx-lx", check);
  // ssout << "Computation mode BAX = lamdaX" << "\n";
  // ssout << "Eigen values " << rEigVal << "\n";
  // ssout << "Eigen vectors " << cEigVec << "\n";
  Vapprox=verifyInternGenDenseResult<Complex>(cMat, cMatPos, rEigVal, cEigVec, size, _BAx_lx);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Generalized complex symmetric case 3: error");
  // ssout << "|| A*B*u - lamda*u|| = " << Vapprox << "\n";

  // ssout << "\n--------------------------- test STANDARD non-symmetric eigenvalue problem ---------------------------------------------------------------\n";
  RealStdIntern eigStdIntern(size);
  eigStdIntern.compute(rMatNonSym);
  cEigVal = eigStdIntern.eigenvalues();
  cEigVec = normalizeEigenVectors(eigStdIntern.eigenvectors());
  nbErrors+=checkValues(cEigVec, rootname+"/cEigVecAxml.in", tol, errors, "cEigVec Ax-lx", check);
  nbErrors+=checkValues(cEigVal, rootname+"/cEigValAxml.in", tol, errors, "cEigVal Ax-lx", check);
  // ssout << "Eigen values of non-symmetric real matrix" << "\n";
  // ssout << "Eigen values " << cEigVal << "\n";
  // ssout << "Eigen vectors " << cEigVec << "\n";
  Vector<Real> Vref(20,0.); Vref[13]=-1;Vref[14]=-1;Vref[15]=-1;Vref[17]=-1;Vref[18]=-1;
  Vapprox= verifyInternStdDenseResult<Complex>(rMatNonSym, cEigVal, cEigVec, size);
  nbErrors+=checkValues(Vapprox, Vref, tol, errors, "Real non-symmetric case: error");
  // ssout << "|| A*u - lamda*u|| = " << Vapprox << "\n";
  rPseudoEigVal = eigStdIntern.pseudoEigenvalueMatrix();
  rEigVec = eigStdIntern.pseudoEigenvectors();
  // ssout << "Pseudo Eigen values " << rPseudoEigVal << "\n";
  // ssout << "Pseudo Eigen vectors " << rEigVec << "\n";
  multMatMat(rMatNonSym, rEigVec, rTmp);
  multMatMat(rEigVec, rPseudoEigVal, rQaQ);
  rQaQ -= rTmp;
  nF = rQaQ.normFrobenius();
  // ssout << "|| A*V - V*D|| = " << nF << "\n";
  nbErrors+= checkValue(nF, 0., tol, errors, "Real non-symmetric case: Frobenius norm");
  
  CplxStdIntern cplxEigStdIntern(size);
  cplxEigStdIntern.compute(cMatNonSym);
  cEigVal = cplxEigStdIntern.eigenvalues();
  cEigVec = normalizeEigenVectors(cplxEigStdIntern.eigenvectors());
  // ssout << "Eigen values of complex non-symmetric matrix" << "\n";
  // ssout << "Eigen values " << cEigVal << "\n";
  // ssout << "Eigen vectors " << cEigVec << "\n";
  nbErrors+=checkValues(cEigVec, rootname+"/CcEigVecBAxml.in", tol, errors, "complex/complex cEigVec BAx-lx", check);
  nbErrors+=checkValues(cEigVal, rootname+"/CcEigValBAxml.in", tol, errors, "complex/complex rEigVal BAx-lx", check);
  Vapprox= verifyInternStdDenseResult<Complex>(cMatNonSym, cEigVal, cEigVec, size);
  nbErrors+=checkValue(Vapprox.norminfty(), 0., tol, errors, "Complex non-symmetric case: error");
  // ssout << "|| A*u - lamda*u|| = " << Vapprox << eol;
 
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
