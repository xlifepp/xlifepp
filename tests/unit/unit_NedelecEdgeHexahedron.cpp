/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_NedelecEdgeHexahedron.cpp
	\author E. Lunéville
	\since 16 sep 2015
	\date 16 sep 2015

	Low level tests of Nedelec's edge element on hexahedron
	and a global test

	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_NedelecEdgeHexahedron
{

typedef GeomDomain& Domain;

Real omeg, eps, mu, a, ome;

Vector<Real> f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  Vector<Real> res(3);
  Real c=3*a*a-ome;
  res(1)=-2*cos(a*x)*sin(a*y)*sin(a*z)*c;
  res(2)=   sin(a*x)*cos(a*y)*sin(a*z)*c;
  res(3)=   sin(a*x)*sin(a*y)*cos(a*z)*c;
  return res;
}

Vector<Real> solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  Vector<Real> res(3);
  res(1)=-2*cos(a*x)*sin(a*y)*sin(a*z);
  res(2)=   sin(a*x)*cos(a*y)*sin(a*z);
  res(3)=   sin(a*x)*sin(a*y)*cos(a*z);
  return res;
}

Vector<Real> f1(const Point& P, Parameters& pa = defaultParameters)
{
    return Vector<Real>(3,1.);
}

const Element* elt;
Number index=1;
Vector<Real> w(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> r(3);
  ShapeValues shv = elt->computeShapeValues(P, false, false, 0);
  Number i=index-1;
  r[0]=shv.w[3*i];r[1]=shv.w[3*i+1];r[2]=shv.w[3*i+2];
  return r;
}


//compute dof_i(w_j) in physical space, check computeShapeValue and dof::operator()
void checkPhysicalShapeValue(const Space& V, std::stringstream& ssout)
{
  ssout<<V<<eol;
  elt=V.element_p(Number(0));
  Function fw(w,3);
  ssout<<" -------- checkPhysicalShapeValue -------------"<<eol;
  String s="dof  ";
  for(Number d=1;d<=V.nbDofs();d++)
  {
    if(d==10) s="dof ";
    ssout<<s<<d<<": ";
    theCout<<s<<d<<": ";
    for(Number j=1;j<=V.nbDofs();j++)
    {
      index=j;
      ssout<<roundToZero(V.dof(d)(fw).asReal(),1.E-10)<<" ";
      theCout<<roundToZero(V.dof(d)(fw).asReal(),1.E-10)<<" ";
     }
   ssout<<eol;
   theCout<<eol;
  }
}

void unit_NedelecEdgeHexahedron(int argc, char* argv[], bool check)
{
 String rootname = "unit_NedelecEdgeHexahedron";
 trace_p->push(rootname);
 std::stringstream ssout;
 ssout.precision(testPrec);
 verboseLevel(110);
 numberOfThreads(1);
 Number nbErrors = 0;
 String errors;
 Real tol=0.0001;
 bool isTM=false;
 Mesh me(_hexahedron); Domain omg=me.domain("Omega");
 Point x(0.5,0.5,0.5);
 ShapeValues shv;
 Interpolation* interp;
 ssout<<"\n\n================  Nedelec edge hexahedron, first family, order 1 (NE1_1) ======================"<<eol;
 interp=findInterpolation(Nedelec, _firstFamily, 1, Hrot);
 NedelecEdgeFirstHexahedronPk NE11(interp);
 NE11.print(ssout);
 shv = ShapeValues(NE11);
 NE11.computeShapeValues(x.begin(),shv,true);
 NE11.printShapeValues(ssout,x.begin(),shv);
 Space V11(omg,*interp,"V11",false);
 checkPhysicalShapeValue(V11,ssout);
 theCout<<V11;
 //check matrix
 Unknown e11(V11, _name="e11"); TestFunction q11(e11, _name="q11");
 TermMatrix M11(intg(omg,e11|q11));
 TermVector F11(e11,omg,Function(f1),Function());
 theCout<<F11<<eol;
 theCout<<"(M11*F11|F11)="<<(M11*F11|F11)<<eol;
// nbErrors += checkValue(ssout, rootname+"/NE11.in", errors, "NE11", check, isTM);
 ssout<<"\n\n================  Nedelec edge hexahedron, first family, order 2 (NE1_2) ======================"<<eol;
 interp=findInterpolation(Nedelec, _firstFamily, 2, Hrot);
 NedelecEdgeFirstHexahedronPk NE12(interp);
 NE12.print(ssout);
 shv = ShapeValues(NE12);
 NE12.computeShapeValues(x.begin(),shv,true);
 NE12.printShapeValues(ssout,x.begin(),shv);
 Space V12(omg,*interp,"V12",false);
 theCout<<V12<<eol<<std::flush;
 checkPhysicalShapeValue(V12,ssout);
 //check matrix
 Unknown e12(V12, _name="e12"); TestFunction q12(e12, _name="q12");
 TermMatrix M12(intg(omg,e12|q12));
 TermVector F12(e12,omg,Function(f1),Function());
 theCout<<F12<<eol;
 theCout<<"(M12*F12|F12)="<<(M12*F12|F12)<<eol;
// nbErrors += checkValue(ssout, rootname+"/NE12.in", errors, "NE12", check, isTM);
 // Point x(0.5,0.5,0.5);
// ssout<<"\n\n================  Nedelec edge hexahedron, first family, order 3 (NE1_3) ======================"<<eol;
// interp=findInterpolation(Nedelec, _firstFamily, 3, Hrot);
// NedelecEdgeFirstHexahedronPk NE13(interp);
// NE13.print(ssout);
// shv = ShapeValues(NE13);
// NE13.computeShapeValues(x.begin(),shv,true);
// NE13.printShapeValues(ssout,x.begin(),shv);
// Space V13(omg,*interp,"V13",false);
// checkPhysicalShapeValue(V13,ssout);
// nbErrors += checkValue(ssout, rootname+"/NE13.in", errors, "NE13", check, isTM);
// ssout<<"\n\n================  Nedelec edge hexahedron, first family, order 4 (NE1_4) ======================"<<eol;
// interp=findInterpolation(Nedelec, _firstFamily, 4, Hrot);
// NedelecEdgeFirstHexahedronPk NE14(interp);
// NE14.print(ssout);
// shv = ShapeValues(NE14);
// NE14.computeShapeValues(x.begin(),shv,true);
// NE14.printShapeValues(ssout,x.begin(),shv);
// Space V14(omg,*interp,"V14",false);
// checkPhysicalShapeValue(V14,ssout);
// nbErrors += checkValue(ssout, rootname+"/NE14.in", errors, "NE14", check, isTM);
 ssout<<"\n================================================================================================"<<eol<<eol;

//
//  /* test : solve Maxwell harmonic equation on unit cube Omega=]0,1[x]0,1[x]0,1[
//                  curl curl(E) -w^2.mu.eps E = -i.w.mu.J=fJ   in Omega
//                  E x n = 0                                   on dOmega
//            Solution (a=k.pi) :
//                     Ex = -2cos(ax)sin(ay)sin(az)
//                     Ey =   cos(ay)sin(ax)sin(az)
//                     Ez =   cos(az)sin(ax)sin(ay)
//         -> fJ= (3a^2-w^2.mu.eps)E
//  */
//
  //numberOfThreads(1);
  omeg=1; eps=1; mu=1; a=pi_; ome=omeg* omeg* mu* eps;
  Number ordmin=1, ordmax=2;
  Number na=0;

  for(Number ord=ordmin; ord<=ordmax; ord++)
    {
      //Number nh=std::max(Number(3),1+(na/ord));
      Number nh=3;
      // Mesh mesh3d(Cuboid(_origin=Point(0.,0.,0.),_xlength=1,_ylength=1.,_zlength=1., _nnodes=Numbers(3,2,2), _domain_name="Omega",_side_names=Strings(6,"Gamma")), _shape=_hexahedron, _order=1, _generator=_structured);
      Mesh mesh3d(Cuboid(_origin=Point(0.,0.,0.),_xlength=1,_ylength=1.,_zlength=1., _nnodes=Numbers(5,5,5), _domain_name="Omega",_side_names=Strings(6,"Gamma")), _shape=_hexahedron, _order=1, _generator=_structured);
      // saveToFile(inputsPathTo(rootname+"/mesh3d.msh"), mesh3d);
      // Mesh mesh3d(inputsPathTo(rootname+"/mesh3d.msh"), _name="mesh of the unit cube");
      Domain omega=mesh3d.domain("Omega");
      Domain gamma=mesh3d.domain("Gamma");

      ssout<<"=============== test Nedelec edge first family of order "<<ord<<" ==============="<<eol;
      //create spaces
      Space V(_domain=omega, _FE_type=NedelecEdge, _order=ord, _name="V", _notOptimizeNumbering);
      thePrintStream<<V<<eol;
      Unknown e(V, _name="E"); TestFunction q(e, _name="q");
      Space W(omega,interpolation(_Lagrange,_standard,ord,H1),"W",true);
      Unknown u(W, _name="u",_dim=3); TestFunction v(u, _name="v");  //for H1 projection
      ssout<<"number of elements on edge : "<<nh-1;
      ssout<<" number of hexahedra : "<<mesh3d.nbOfElements();
      ssout<<" number of dofs : "<<V.nbDofs()<<eol;
      elapsedTime("mesh and space", ssout);

      //check mass matrix and dof interpolation
      TermMatrix MV(intg(omega,e|q));
      TermVector F1(e,omega,Function(f1),Function());
      theCout<<"(MV*F1|F1)="<<(MV*F1|F1)<<eol;
      TermVector Un(intg(omega,f1|q), _name="Un");
      TermVector F1p=directSolve(MV,Un,true);
      theCout<<"(MV*F1p|Fp1)="<<(MV*F1p|F1p)<<eol;

      //create FE terms
      BilinearForm aev=intg(omega,curl(e)|curl(q))-ome*intg(omega,e|q);
      LinearForm l=intg(omega,f|q);
      EssentialConditions ecs = (ncross(e) | gamma)=0;
      TermMatrix A(aev, ecs, _name="A");
      TermVector b(l, _name="B");
      elapsedTime("FE computation", std::cout);

      //solve system
      TermVector E=directSolve(A,b);
      elapsedTime("solving", std::cout);

      //interpolate "edge" solution
      TermVector Ei=interpolate(u,omega,E,"Ei");
      saveToFile("Ei_"+tostring(ord)+".vtu",Ei,_format=_vtu, _aUniqueFile);

      // Pk interpolation, L2 projection
      TermMatrix N(intg(omega,e|v), _name="N");
      TermMatrix M(intg(omega,u|v), _name="M");       //Qk mass matrix
      TermVector EQ=directSolve(M,N*E,true);   //L2 Qk_projection

      //compute L2 error in various way
      TermVector Ex(e,omega,solex,Function());
      TermVector Er=E-Ex;
      Real erl2 = sqrt(real((MV*Er|Er)));
      ssout<<"L2 error  = "<<erl2<<eol;
      theCout<<"L2 error  = "<<erl2<<eol;
      elapsedTime("error computation", std::cout);

      TermVector ExQ(u,omega,solex);
      TermVector ErQ=ExQ-EQ;
      Real erl2Q=sqrt(real((M*ErQ|ErQ)));
      ssout<<"L2 error on Q"<<ord<<" projection = "<<erl2Q<<eol;
      theCout<<"L2 error on Q"<<ord<<" projection = "<<erl2Q<<eol;

      TermVector Eri=ExQ-Ei;
      Real erl2i=sqrt(real((M*Eri|Eri)));
      ssout<<"L2 error on Q"<<ord<<" interpolation = "<<erl2i<<eol;
      theCout<<"L2 error on Q"<<ord<<" interpolation = "<<erl2i<<eol;

      saveToFile("Ep_"+tostring(ord),EQ,_format=_vtu, _aUniqueFile);
      saveToFile("Ex",ExQ,_format=_vtu, _aUniqueFile);
//      nbErrors+= checkValues("Ei_"+tostring(ord)+".vtu", rootname+"/Ei_"+tostring(ord)+".in", tol, errors, "vtu: Ei_"+tostring(ord), check, isTM);
//      nbErrors+= checkValues("Ep_"+tostring(ord)+".vtu", rootname+"/Ep_"+tostring(ord)+".in", tol, errors, "vtu: Ep_"+tostring(ord), check, isTM);
//      nbErrors+= checkValues("E_ex.vtu", rootname+"/E_ex.in", tol, errors, "Eex", check, isTM);
//      nbErrors+= checkValue(erl2, 0., tol, errors, "ErrL2 Nedelec EdgeHexahedron", isTM);
    }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
