/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_BuffaChristiansenTriangle.cpp
	\author E. Lunéville
	\since 31 dec 2017
	\date 31 dec 2017

	Low level tests of Buffa-Christiansen element on triangle
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_BuffaChristiansenTriangle
{

void unit_BuffaChristiansenTriangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_BuffaChristiansenTriangle";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);
  verboseLevel(20);

  String errors;
  Number nbErrors = 0;
  bool isTM =true;

  Interpolation& bcint=interpolation(_BuffaChristiansen, _standard, 1, _Hdiv);
  theCout<<bcint<<eol;
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=4), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Space RT(_domain=omega, _interpolation=_RT_1, _name="RT");
  theCout << RT;  
  nbErrors += checkValue(theCout, rootname+"/RT.in", errors, "Test RT ", check);

  Space BC(_domain=omega,_FE_type=BuffaChristiansen, _order=1, _name="BC");
  theCout << BC;
  nbErrors += checkValue(theCout, rootname+"/BuffaChristiansen.in", errors, "Test BuffaChristiansen", check);

  Unknown u(RT, _name="u");  TestFunction v(BC, _name="v");
  TermMatrix M(intg(omega,u|v));
  theCout << M << eol;
  nbErrors += checkValue(theCout, rootname+"/M.in", errors, "Test matrix M ", check);
  if (check)
  {
   if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
   else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else
  { theCout << "Data updated " << eol; }
 
 trace_p->pop();
 
}

} //end namespace
