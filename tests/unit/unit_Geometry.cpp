/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_Geometry.cpp
  \authors E. Lunéville, N. Kielbasiewicz
  \since 10 may 2012
  \date  10 may 2012

  Low level tests of Geometry class and related classes.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting informations in a string
*/

//===============================================================================
//library dependances
#include "xlife++-libs.h"
#include "testUtils.hpp"

//===============================================================================
//stl dependances
#include <iostream>
#include <fstream>
#include <vector>

//===============================================================================

using namespace xlifepp;

namespace unit_Geometry {

Real checkSpline(const SplineArc& sp,bool is1D, Real l, bool save=false, const String& name="")
{
  String na=name;
  if(save && na=="") na="splinearc.dat";
  Number np=100;
  Real t=0., dt=1./np, er=-theRealMax;
  std::ofstream out;
  if(save) out.open(na.c_str());
  if(is1D)
  {
    for(Number i=0;i<=np;i++,t+=dt)
    {
      Point q = sp.parametrization()(t);
      Real s=sin(t*l);
      if(save) out<<t*pi_<<" "<<q.y()<<" "<<s<<eol;
      er=std::max(std::abs(s-q.y()),er);
    }
  }
  else
  {
    for(Number i=0;i<=np;i++,t+=dt)
    {
      Point q = sp.parametrization()(t);
      Real s=sin(q.x());
      if(save) out<<q.x()<<" "<<q.y()<<" "<<s<<eol;
      er=std::max(std::abs(s-q.y()),er);
    }
  }
  if(save) out.close();
  return er;
}

void unit_Geometry(int argc, char* argv[], bool check)
{
  String rootname = "unit_Geometry";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);
  verboseLevel(1);
  String errors;
  Number nbErrors = 0;

  std::vector<String> sidenames(2, "");
  sidenames[0] = "Sigma-";

  //! test BoundingBox class
  BoundingBox S1(1, 2);
  theCout << "BoundingBox S1(1,2): " << S1 << eol;
  BoundingBox R1(0, 2, -1, 1);
  theCout << "BoundingBox R1(0,2,-1,1): " << R1 << eol;
  BoundingBox P1(0, 2, -1, 1, 3, 4);
  theCout << "BoundingBox P1(0,2,-1,1,3,4): " << P1 << eol;
  theCout << "test dim(): S1.dim()=" << S1.dim() << ", "
        << "R1.dim()=" << R1.dim() << ", " << "P1.dim()=" << P1.dim() << eol;
  theCout << "test xbounds(): S1.xbounds()=" << S1.xbounds() << ", "
        << "R1.xbounds()=" << R1.xbounds() << ", " << "P1.xbounds()=" << P1.xbounds() << eol;
  theCout << "test ybounds(): S1.ybounds()=" << S1.ybounds() << ", "
        << "R1.ybounds()=" << R1.ybounds() << ", " << "P1.ybounds()=" << P1.ybounds() << eol;
  theCout << "test zbounds(): S1.zbounds()=" << S1.zbounds() << ", "
        << "R1.zbounds()=" << R1.zbounds() << ", " << "P1.zbounds()=" << P1.zbounds() << eol;
  theCout << "test minPoint(): S1.minPoint()=" << S1.minPoint() << ", "
        << "R1.minPoint()=" << R1.minPoint() << ", " << "P1.minPoint()=" << P1.minPoint() << eol;
  theCout << "test maxPoint(): S1.maxPoint()=" << S1.maxPoint() << ", "
        << "R1.maxPoint()=" << R1.maxPoint() << ", " << "P1.maxPoint()=" << P1.maxPoint() << eol;

  nbErrors += checkValue(theCout, rootname+"/BoundingBox.in", errors, "Test of BoundingBox class", check);

  //! test Geometry class
  theCout << "Geometry(S1): " << Geometry(S1, S1.asString()) << eol;
  theCout << "Geometry(1): " << Geometry(1) << eol;
  theCout << "Geometry(R1): " << Geometry(R1, "rectangle " + R1.asString(), _rectangle, "r", "theta") << eol;
  theCout << "Geometry(2): " << Geometry(2) << eol;
  theCout << "Geometry(P): " << Geometry(P1, "parallelepiped " + P1.asString(), _parallelepiped) << eol;
  theCout << "Geometry(3): " << Geometry(3) << eol;
  Geometry gparad(P1, "parallelepiped " + P1.asString(), _parallelepiped);
  theCout << "test of g.dim() = " << gparad.dim() << ", g.boundingBox = " << gparad.boundingBox << eol;

  nbErrors += checkValue(theCout, rootname+"/Geometry.in", errors, "Test of Geometry class", check);

  //! test Geometry child classes
  theCout << "-------------- test of Geometry child classes ------------" << eol;

  theCout << Segment(_xmin = 1., _xmax = 2.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Segment1D.in", errors, "Segment(_xmin=1, _xmax=2)", check);

  theCout << Segment(_v1=Point(1.,0.), _v2=Point(0.,2.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Segment2D.in", errors, "Segment(_v1=Point(1,0), _v2=Point(0,2))", check);

  theCout << Segment(_v1=Point(0.,0.,0.), _v2=Point(1.,2.,3.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Segment3D.in", errors, "Segment(_v1=Point(0,0,0), _v2=Point(1,2,3))", check);

  theCout << EllArc(_center=Point(0.,0.), _v1=Point(2.,0.), _v2=Point(0.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/EllArc2D.in", errors, "EllArc(_center=Point(0,0), _v1=Point(2,0), _v2=Point(0,1))", check);

  theCout << EllArc(_center=Point(0.,0.,0.), _v1=Point(2.,0.,0.), _v2=Point(0.,0.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/EllArc3D.in", errors, "EllArc(_center=Point(0,0,0), _v1=Point(2,0,0), _v2=Point(0,0,1))", check);

  Real coord=sqrt(2.);
  theCout << CircArc(_center=Point(0.,0.), _v1=Point(2.,0.), _v2=Point(coord,coord)) << eol;
  nbErrors += checkValue(theCout, rootname+"/CircArc2D.in", errors, "CircArc(_center=Point(0,0), _v1=Point(2,0), _v2=Point(sqrt(2),sqrt(2)))", check);

  theCout << CircArc(_center=Point(0.,0.,0.), _v1=Point(2.,0.,0.), _v2=Point(coord,0.,coord)) << eol;
  nbErrors += checkValue(theCout, rootname+"/CircArc3D.in", errors, "CircArc(_center=Point(0,0,0), _v1=Point(2,0,0), _v2=Point(sqrt(2),0,sqrt(2)))", check);

  std::vector<Point> vertices(5), vertices2(4);
  vertices[0]=Point(0.,0.);
  vertices[1]=Point(2.,0.);
  vertices[2]=Point(3.,1.);
  vertices[3]=Point(1.,4.);
  vertices[4]=Point(-1.,2.);
  Polygon pol1(_vertices=vertices,_side_names=Strings("S1","S2","S3","S4","S5"));
  theCout << pol1 << eol;
  theCout << "extract side S3 from polygon:" << *pol1.find("S3") << eol;

  nbErrors += checkValue(theCout, rootname+"/Polygon2D.in", errors, "Polygon(Point(0,0), Point(2,0), Point(3,1), Point(1,4), Point(-1,2))", check);

  Points ptsVertices(5);
  ptsVertices(1)=Point(0.,0.,0.);
  ptsVertices(2)=Point(2.,2.,0.);
  ptsVertices(3)=Point(3.,3.,1.);
  ptsVertices(4)=Point(1.,1.,4.);
  ptsVertices(5)=Point(-1.,-1.,2.);
  Polygon pol2(_vertices=ptsVertices,_side_names=Strings("S1","S2","S1","S2","S1"));
  theCout << pol2 << eol;
  theCout << "extract side S1 from polygon:" << *pol2.find("S1") << eol;
  nbErrors += checkValue(theCout, rootname+"/Polygon3D.in", errors, "Polygon(Point(0,0,0), Point(2,2,0), Point(3,3,1), Point(1,1,4), Point(-1,-1,2))", check);

  theCout << Triangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(0.,3.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Triangle3D.in", errors, "Triangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(0.,3.,1.))", check);

  theCout << Quadrangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(2.,3.,1.), _v4=Point(0.,3.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Quadrangle3D.in", errors, "Quadrangle(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v3=Point(2.,3.,1.), _v4=Point(0.,3.,1.))", check);

  theCout << Parallelogram(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v4=Point(0.,3.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelogram3D.in", errors, "Parallelogram(_v1=Point(0.,0.,0.), _v2=Point(2.,0.,0.), _v4=Point(0.,3.,1.))", check);

  theCout << Rectangle(_xmin=0, _xmax=2, _ymin=-1, _ymax=1) << eol;
  nbErrors += checkValue(theCout, rootname+"/Rectangle2DXY.in", errors, "Rectangle(_xmin=0, _xmax=2, _ymin=-1, _ymax=1)", check);

  theCout << Rectangle(_center=Point(0.,0.), _xlength=3., _ylength=2.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Rectangle2Dcenter.in", errors, "Rectangle(_center=Point(0,0), _xlength=3, _ylength=2)", check);

  theCout << Rectangle(_origin=Point(0.,0.), _xlength=3., _ylength=2.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Rectangle2Dorigin.in", errors, "Rectangle(_origin=Point(0,0), _xlength=3, _ylength=2)", check);

  theCout << SquareGeo(_center=Point(0.,0.), _length=1.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Square2Dcenter.in", errors, "SquareGeo(_center=Point(0,0), _length=1)", check);

  theCout << SquareGeo(_origin=Point(0.,0.), _length=1.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Square2Dorigin.in", errors, "SquareGeo(_origin=Point(0,0), _length=1)", check);

  theCout << Ellipse(_center=Point(0.,0.), _xlength=4., _ylength=2.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipse2D.in", errors, "Ellipse(_center=Point(0,0), _xlength=4, _ylength=2)", check);

  theCout << Ellipse(_center=Point(0.,0.), _xlength=4., _ylength=2., _angle1=25*deg_, _angle2=90*deg_) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipse2Dsector.in", errors, "Ellipse(_center=Point(0,0), _xlength=4, _ylength=2, _angle1=25*deg_, _angle2=90*deg_)", check);

  theCout << Ellipse(_center=Point(0.,0.), _xlength=4., _ylength=2., _angle1=25*deg_, _angle2=-25*deg_) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipse2Dsectorinv.in", errors, "Ellipse(_center=Point(0,0), _xlength=4, _ylength=2, _angle1=25*deg_, _angle2=-25*deg_)", check);

  theCout << Disk(_center=Point(0.,0.), _radius=1.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Disk2D.in", errors, "Disk(_center=Point(0,0), _radius=1)", check);

  theCout << Disk(_center=Point(0.,0.), _radius=1., _angle1=25*deg_, _angle2=90*deg_) << eol;
  nbErrors += checkValue(theCout, rootname+"/Disk2Dsector.in", errors, "Disk(_center=Point(0,0), _radius=1, _angle1=25*deg_, _angle2=90*deg_)", check);

  theCout << Disk(_center=Point(0.,0.), _radius=1., _angle1=25*deg_, _angle2=-25*deg_) << eol;
  nbErrors += checkValue(theCout, rootname+"/Disk2Dsectorinv.in", errors, "Disk(_center=Point(0,0), _radius=1, _angle1=25*deg_, _angle2=-25*deg_)", check);

  vertices[0]=Point(0.,0.,0.);
  vertices[1]=Point(2.,0.,0.);
  vertices[2]=Point(3.,1.,0.);
  vertices[3]=Point(1.,4.,0.);
  vertices[4]=Point(-1.,2.,0.);
  Polygon pg1(_vertices=vertices,_domain_name="S1");
  vertices[0]=Point(0.,0.,1.);
  vertices[1]=Point(2.,0.,1.);
  vertices[2]=Point(3.,1.,1.);
  vertices[3]=Point(1.,4.,1.);
  vertices[4]=Point(-1.,2.,1.);
  Polygon pg2(_vertices=vertices,_domain_name="S2");
  vertices2[0]=Point(0.,0.,0.);
  vertices2[1]=Point(2.,0.,0.);
  vertices2[2]=Point(2.,0.,1.);
  vertices2[3]=Point(0.,0.,1.);
  Polygon pg3(_vertices=vertices2,_domain_name="S1");
  vertices2[0]=Point(2.,0.,0.);
  vertices2[1]=Point(3.,1.,0.);
  vertices2[2]=Point(3.,1.,1.);
  vertices2[3]=Point(2.,0., 1.);
  Polygon pg4(_vertices=vertices2,_domain_name="S2");
  vertices2[0]=Point(3.,1.,0.);
  vertices2[1]=Point(1.,4.,0.);
  vertices2[2]=Point(1.,4.,1.);
  vertices2[3]=Point(3.,1., 1.);
  Polygon pg5(_vertices=vertices2,_domain_name="S1");
  vertices2[0]=Point(1.,4.,0.);
  vertices2[1]=Point(-1.,2.,0.);
  vertices2[2]=Point(-1.,2.,1.);
  vertices2[3]=Point(1.,4., 1.);
  Polygon pg6(_vertices=vertices2,_domain_name="S2");
  vertices2[0]=Point(-1.,2.,0.);
  vertices2[1]=Point(0.,0.,0.);
  vertices2[2]=Point(0.,0.,1.);
  vertices2[3]=Point(-1.,2., 1.);
  Polygon pg7(_vertices=vertices2,_domain_name="S1");
  std::vector<Polygon> faces(7);
  faces[0]=pg1;
  faces[1]=pg2;
  faces[2]=pg3;
  faces[3]=pg4;
  faces[4]=pg5;
  faces[5]=pg6;
  faces[6]=pg7;
  Polyhedron pol3(_faces=faces);
  theCout << pol3 << eol;
  theCout << "extract side S2 from polyhedron:" << *pol3.find("S2") << eol;
  nbErrors += checkValue(theCout, rootname+"/Polyhedron.in", errors, "Polyhedron type prism with pentagonal base", check);

  theCout << Tetrahedron(_v1=Point(0.,-1.,3.), _v2=Point(2.,-1.,3.), _v3=Point(0.,1.,3.), _v4=Point(0.,-1.,4.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Tetrahedron.in", errors, "Tetrahedron(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v3=Point(0,1,3), _v4=Point(0,-1,4))", check);

  theCout << Hexahedron(_v1=Point(0.,-1.,3.), _v2=Point(2.,-1.,3.), _v3=Point(2.,1.,3.), _v4=Point(0.,1.,3.),
                      _v5=Point(0.,-1.,4.), _v6=Point(2.,-1.,4.), _v7=Point(2.,1.,4.), _v8=Point(0.,1.,4.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Hexahedron.in", errors, "Hexahedron(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v3=Point(2,1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _v6=Point(2,-1,4), _v7=Point(2,1,4), _v8=Point(0,1,4))", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4))", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=7) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped7.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=7)", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=6) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped6.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=6)", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=5) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped5.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=5)", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=4) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped4.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=4)", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=3) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped3.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=3)", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=2) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped2.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=2)", check);

  theCout << Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=1) << eol;
  nbErrors += checkValue(theCout, rootname+"/Parallelepiped1.in", errors, "Parallelepiped(_v1=Point(0,-1,3), _v2=Point(2,-1,3), _v4=Point(0,1,3), _v5=Point(0,-1,4), _nboctants=1)", check);

  theCout << Cuboid(_xmin=0, _xmax=2, _ymin=-1, _ymax=1, _zmin=3, _zmax=4) << eol;
  nbErrors += checkValue(theCout, rootname+"/CuboidXYZ.in", errors, "Cuboid(_xmin=0, _xmax=2, _ymin=-1, _ymax=1, _zmin=3, _zmax=4)", check);

  theCout << Cuboid(_center=Point(1.,0.,3.5), _xlength=2, _ylength=2, _zlength=1) << eol;
  nbErrors += checkValue(theCout, rootname+"/Cuboidcenter.in", errors, "Cuboid(_center=Point(1,0,3.5), _xlength=2, _ylength=2, _zlength=1)", check);

  theCout << Cuboid(_origin=Point(0.,-1.,3), _xlength=2, _ylength=2, _zlength=1) << eol;
  nbErrors += checkValue(theCout, rootname+"/Cuboidorigin.in", errors, "Cuboid(_origin=Point(0,-1,3), _xlength=2, _ylength=2, _zlength=1)", check);

  Cube cu(_center=Point(0.,0.,0.),_length=2,_side_names=Strings("S1","S2","S1","S2","S2","S2"));
  theCout << cu << eol;
  theCout << "extract side S1 from cube:" << *cu.find("S1");
  nbErrors += checkValue(theCout, rootname+"/Cube.in", errors, "Cube(_center=Point(0.,0.,0.), _length=2)", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.), _v6=Point(0.,0.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoidgeneral.in", errors, "Ellipsoid(_center=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,2,0), _v6=Point(0,0,1))", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1)", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nboctants=7) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid7.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1), _nboctants=7", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nboctants=6) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid6.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1), _nboctants=6", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nboctants=5) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid5.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1), _nboctants=5", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nboctants=4) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid4.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1), _nboctants=4", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nboctants=3) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid3.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1), _nboctants=3", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nboctants=2) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid2.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1), _nboctants=2", check);

  theCout << Ellipsoid(_center=Point(0.,0.,0.), _xlength=3., _ylength=2., _zlength=1., _nboctants=1) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ellipsoid1.in", errors, "Ellipsoid(_center=Point(0,0,0), _xlength=3, _ylength=2, _zlength=1), _nboctants=1", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2.) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball.in", errors, "Ball(_center=Point(0,0,0), _radius=2)", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2., _nboctants=7) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball7.in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=7)", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2., _nboctants=6) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball6.in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=6)", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2., _nboctants=5) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball5.in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=5)", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2., _nboctants=4) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball4.in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=4)", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2., _nboctants=3) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball3.in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=3)", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2., _nboctants=2) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball2.in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=2)", check);

  theCout << Ball(_center=Point(0.,0.,0.), _radius=2., _nboctants=1) << eol;
  nbErrors += checkValue(theCout, rootname+"/Ball1.in", errors, "Ball(_center=Point(0,0,0), _radius=2, _nboctants=1)", check);

  std::vector<Real> dir(3,0.);
  dir[2]=1.;
  theCout << Cylinder(_basis=Triangle(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.)),_direction=dir) << eol;
  nbErrors += checkValue(theCout, rootname+"/CylinderbasisTriangle.in", errors, "Cylinder(_basis=Triangle(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0)), _direction=(0,0,1))", check);

  theCout << Cylinder(_basis=Ellipse(_center=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.)),_direction=dir) << eol;
  nbErrors += checkValue(theCout, rootname+"/CylinderbasisEllipse.in", errors, "Cylinder(_basis=Ellipse(_center=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,2,0)),_direction=(0,0,1))", check);

  theCout << Cylinder(_center1=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.), _center2=Point(0.,0.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Cylinderpointsellipse.in", errors, "Cylinder(_center1=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,2,0), _center2=Point(0,0,1))", check);

  theCout << Cylinder(_center1=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,3.,0.), _center2=Point(0.,0.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Cylinderpointsdisk.in", errors, "Cylinder(_center1=Point(0,0,0), _v1=Point(3,0,0), _v2=Point(0,3,0), _center2=Point(0,0,1))", check);

  theCout << RevCylinder(_center1=Point(0.,0.,0.), _center2=Point(0.,0.,1.), _radius=3.) << eol;
  nbErrors += checkValue(theCout, rootname+"/RevCylinder.in", errors, "RevCylinder(_center1=Point(0,0,0), _center2=Point(0,0,1), _radius=3)", check);

  Prism pri1(_basis=Triangle(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.)),_direction=Point(0.,0.,1.),
             _side_names=Strings("S1","S2","S1","S2","S2"));
  theCout << pri1 << eol;
  theCout << "extract side S1 from prism:" << *pri1.find("S1");
  nbErrors += checkValue(theCout, rootname+"/PrismTriangle.in", errors, "Prism(_basis=Triangle(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0)), _direction=(0,0,1))", check);

  Prism pri2(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.),_direction=Point(0.,0.,1.),
             _side_names=Strings("S1","S2","S1","S2","S2"));
  theCout << pri2 << eol;
  nbErrors += checkValue(theCout, rootname+"/Prismpoints.in", errors, "Prism(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0), _direction=(0,0,1))", check);

  theCout << Cone(_basis=Triangle(_v1=Point(0.,0.,0.), _v2=Point(3.,0.,0.), _v3=Point(0.,2.,0.)),_apex=Point(-1.,0.,4.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/ConebasisTriangle.in", errors, "Cone(_basis=Triangle(_v1=Point(0,0,0), _v2=Point(3,0,0), _v3=Point(0,2,0)),_apex=Point(-1,0,4))", check);

  theCout << Cone(_basis=SquareGeo(_origin=Point(0.,0.,0.), _length=2.), _apex=Point(0.,0.,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/ConebasisSquare.in", errors, "Cone(_basis=SquareGeo(_origin=Point(0,0,0), _length=2), _apex=Point(0,0,1))", check);

  theCout << Cone(_center1=Point(0.,0.,0.), _v1=Point(2.,0.,0.), _v2=Point(0.,2.,0.), _apex=Point(0.,0.,4.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Conepoints.in", errors, "Cone(_center1=Point(0,0,0), _v1=Point(2,0,0), _v2=Point(0,2,0), _apex=Point(0,0,4))", check);

  theCout << RevCone(_center=Point(0.,0.,0.), _radius=2., _apex=Point(0.,0.,4.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/RevCone.in", errors, "RevCone(_center=Point(0,0,0), _radius=2, _apex=Point(0,0,4))", check);

  Pyramid pyr1(_basis=Rectangle(_v1=Point(0.,0.,0.), _v2=Point(1.,0.,0.), _v4=Point(0.,1.,0.)),_apex=Point(0.5,0.5,1.),
               _side_names=Strings("S1","S2","S1","S2","S2"));
  theCout << pyr1 << eol;
  theCout << "extract side S1 from pyramid:" << *pyr1.find("S1");
  nbErrors += checkValue(theCout, rootname+"/PyramidbasisRectangle.in", errors, "Pyramid(_basis=Rectangle(_v1=Point(0,0,0), _v2=Point(1,0,0), _v4=Point(0,1,0)), _apex=Point(0.5,0.5,1))", check);

  theCout << Pyramid(_v1=Point(0.,0.,0.), _v2=Point(1.,0.,0.), _v3=Point(1.,1.,0.), _v4=Point(0.,1.,0.),_apex=Point(0.5,0.5,1.)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Pyramidpoints.in", errors, "Pyramid(_v1=Point(0,0,0), _v2=Point(1,0,0), _v3=Point(1,1,0), _v4=Point(0,1,0), _apex=Point(0.5,0.5,1))", check);

  Point a1(-1.5,-4.,0.), b1(1.5,-4.,0.), c1(2.,-3.5,0.), d1(2.,3.5,0.);
  Point e1(1.5,4.,0.), f1(-1.5,4.,0.), g1(-2.,3.5,0.), h1(-2.,-3.5,0.);
  Segment seg1(_v1=a1, _v2=b1, _nnodes=11, _domain_name="AB");
  CircArc circ1(_center=Point(1.5,-3.5,0.), _v1=b1, _v2=c1, _nnodes=3, _domain_name="BC");
  Segment seg2(_v1=c1, _v2=d1, _nnodes=6, _domain_name="CD");
  CircArc circ2(_center=Point(1.5,3.5,0.), _v1=d1, _v2=e1, _nnodes=3, _domain_name="DE");
  Segment seg3(_v1=e1, _v2=f1, _nnodes=11, _domain_name="EF");
  CircArc circ3(_center=Point(-1.5,3.5,0.), _v1=f1, _v2=g1, _nnodes=3, _domain_name="FG");
  Segment seg4(_v1=g1, _v2=h1, _nnodes=6, _domain_name="GH");
  CircArc circ4(_center=Point(-1.5,-3.5,0.), _v1=h1, _v2=a1, _nnodes=3, _domain_name="HA");
  theCout << surfaceFrom(seg1+circ1+seg2+circ2+seg3+circ3+seg4+circ4) << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle.in", errors, "Rounded Rectangle (loop)", check);

  Geometry sf1=surfaceFrom(seg1+circ1+seg2+circ2+seg3+circ3+seg4+circ4, "Omega2");
  theCout << sf1 << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectanglecopy.in", errors, "Rounded Rectangle (copy)", check);

  Point a2(0,0,0), b2(2,0,0), c2(2,1,0), d2(0,1,0);
  Point e2(0,0,1), f2(2,0,1), g2(2,1,1), h2(0,1,1);
  Rectangle rrect1(_v1=a2, _v2=b2, _v4=d2, _nnodes=6, _domain_name="R1");
  Rectangle rrect2(_v1=e2, _v2=f2, _v4=h2, _nnodes=6, _domain_name="R2");
  Rectangle rrect3(_v1=a2, _v2=b2, _v4=e2, _nnodes=6, _domain_name="R3");
  Rectangle rrect4(_v1=b2, _v2=c2, _v4=f2, _nnodes=6, _domain_name="R4");
  Rectangle rrect5(_v1=c2, _v2=d2, _v4=g2, _nnodes=6, _domain_name="R5");
  Rectangle rrect6(_v1=d2, _v2=a2, _v4=h2, _nnodes=6, _domain_name="R6");
  theCout << volumeFrom(rrect1+rrect2+rrect3+rrect4+rrect5+rrect6) << eol;
  nbErrors += checkValue(theCout, rootname+"/CubeVF.in", errors, "Cube (loop)", check);

  Segment seg5(_v1=a2, _v2=b2, _nnodes=6, _domain_name="AB");
  Segment seg6(_v1=b2, _v2=c2, _nnodes=6, _domain_name="BC");
  Segment seg7(_v1=c2, _v2=d2, _nnodes=6, _domain_name="CD");
  Segment seg8(_v1=d2, _v2=a2, _nnodes=6, _domain_name="DA");
  Geometry sf4=surfaceFrom(seg5+seg6+seg7+seg8, "R1");
  theCout << volumeFrom(sf4+rrect2+rrect3+rrect4+rrect5+rrect6) << eol;
  nbErrors += checkValue(theCout, rootname+"/CubeVFSF.in", errors, "Cube (loop with border loop)", check);

  Real csconeheight=8, csbaseradius=2, cssphereheight=csbaseradius*csbaseradius/csconeheight;
  Real cssphereradius=std::sqrt(csbaseradius*csbaseradius+cssphereheight*cssphereheight);
  Number nnodes=6;
  Point csbasecenter(0.,0.,0.);
  Point cscenter(0.,0.,-cssphereheight);
  Point cstop(0.,0.,-cssphereheight-cssphereradius);
  Point csb1(csbaseradius,0.,0.), csb2(0.,csbaseradius,0.), csb3(-csbaseradius,0.,0.), csb4(0.,-csbaseradius,0.);
  CircArc csarc1(_center=cscenter, _v1=cstop, _v2=csb1, _nnodes=nnodes);
  CircArc csarc2(_center=cscenter, _v1=cstop, _v2=csb2, _nnodes=nnodes);
  CircArc csarc3(_center=cscenter, _v1=cstop, _v2=csb3, _nnodes=nnodes);
  CircArc csarc4(_center=cscenter, _v1=cstop, _v2=csb4, _nnodes=nnodes);
  CircArc csarcb1(_center=csbasecenter, _v1=csb1, _v2=csb2, _nnodes=nnodes);
  CircArc csarcb2(_center=csbasecenter, _v1=csb2, _v2=csb3, _nnodes=nnodes);
  CircArc csarcb3(_center=csbasecenter, _v1=csb3, _v2=csb4, _nnodes=nnodes);
  CircArc csarcb4(_center=csbasecenter, _v1=csb4, _v2=csb1, _nnodes=nnodes);
  Geometry fs1=ruledSurfaceFrom(csarcb1+(~csarc2)+csarc1,"Sphere1");
  Geometry fs2=ruledSurfaceFrom(csarcb2+(~csarc3)+csarc2,"Sphere2");
  Geometry fs3=ruledSurfaceFrom(csarcb3+(~csarc4)+csarc3,"Sphere3");
  Geometry fs4=ruledSurfaceFrom(csarcb4+(~csarc1)+csarc4,"Sphere4");
  Geometry fs5=planeSurfaceFrom(csarcb1+csarcb2+csarcb3+csarcb4,"Base");
  theCout << volumeFrom(fs1+fs2+fs3+fs4+fs5,"Omega") << eol;
  nbErrors += checkValue(theCout, rootname+"/HalfSphere.in", errors, "Half-sphere with ruledSurfaceFrom/VolumeFrom", check);

  //! false conesphere
  Point csapex(0.,0.,csconeheight);
  Segment csseg1(_v1=csapex, _v2=csb1, _nnodes=3*nnodes);
  Segment csseg2(_v1=csapex, _v2=csb2, _nnodes=3*nnodes);
  Segment csseg3(_v1=csapex, _v2=csb3, _nnodes=3*nnodes);
  Segment csseg4(_v1=csapex, _v2=csb4, _nnodes=3*nnodes);
  Geometry fc1=ruledSurfaceFrom(csarcb1+(~csseg2)+csseg1,"Cone1");
  Geometry fc2=ruledSurfaceFrom(csarcb2+(~csseg3)+csseg2,"Cone2");
  Geometry fc3=ruledSurfaceFrom(csarcb3+(~csseg4)+csseg3,"Cone3");
  Geometry fc4=ruledSurfaceFrom(csarcb4+(~csseg1)+csseg4,"Cone4");
  theCout << volumeFrom(fc1+fc2+fc3+fc4+fs1+fs2+fs3+fs4,"Omega") << eol;
  nbErrors += checkValue(theCout, rootname+"/ConesphereVF.in", errors, "Conesphere with ruledSurfaceFrom/VolumeFrom", check);

  Ellipse ell1(_center=Point(0.,0.,0.), _v1=Point(4,0.,0.), _v2=Point(0.,5.,0.), _domain_name="Omega1",
               _side_names=Strings("Gamma_1","Gamma_2","Gamma_3","Gamma_4"));
  Rectangle rect1(_xmin=-2., _xmax=2., _ymin=-4., _ymax=4., _nnodes=6, _domain_name="Omega2",
                  _side_names=Strings("Gamma_5","Gamma_6","Gamma_7","Gamma_8"));
  Ellipse ell2(_center=Point(1.,2.,0.), _v1=Point(1.5,2.,0.), _v2=Point(1.,3.,0.), _domain_name="Omega3",
               _side_names=Strings("Gamma_9","Gamma_10","Gamma_11","Gamma_12"));
  Ellipse ell3(_center=Point(0.,0.,0.), _v1=Point(0.5,0.,0.), _v2=Point(0.,1.,0.), _domain_name="Omega4",
               _side_names=Strings("Gamma_13","Gamma_14","Gamma_15","Gamma_16"));
  Rectangle rect2(_xmin=5., _xmax=6., _ymin=0., _ymax=1., _domain_name="Omega5",
                  _side_names=Strings("Gamma_17","Gamma_18","Gamma_19","Gamma_20"));
  Disk disk1(_center=Point(5.5,0.5,0.), _v1=Point(5.7,0.5,0.), _v2=Point(5.5,0.7,0.), _domain_name="Omega6",
             _side_names=Strings("Gamma_21","Gamma_22","Gamma_23","Gamma_24"));

  //! case canonical - canonical
  theCout << ell1-ell2 << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical-Canonical.in", errors, "Ellipse - Ellipse", check);

  //! case canonical + canonical
  theCout << ell1+ell2 << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical+Canonical.in", errors, "Ellipse + Ellipse", check);

  //! case composite - canonical
  theCout << (ell1+rect1)-ell2 << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite-Canonical.in", errors, "(Ellipse + Rectangle) - Ellipse", check);

  //! case composite + canonical
  theCout << (ell1+rect1)+ell2 << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite+Canonical.in", errors, "(Ellipse + Rectangle) + Ellipse", check);

  //! case canonical - composite
  theCout << rect1-(ell2+ell3) << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical-Composite.in", errors, "Rectangle - (Ellipse + Ellipse)", check);

  //! case canonical + composite
  printTestLabel(theCout, "Rectangle + (Ellipse + Ellipse)");
  theCout << rect1+(ell2+ell3) << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical+Composite.in", errors, "Rectangle + (Ellipse + Ellipse)", check);

  //! case composite - composite
  theCout << (ell1+rect1)-(ell2+ell3) << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite-Composite.in", errors, "(Ellipse + Rectangle) - (Ellipse + Ellipse)", check);

  //! case composite + composite
  theCout << (ell1+rect1)+(ell2+ell3) << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite+Composite.in", errors, "(Ellipse + Rectangle) + (Ellipse + Ellipse)", check);

  //! case multi-composite
  theCout << (ell1+rect1)-(ell2+ell3)+rect2-disk1 << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite2D.in", errors, "(Ellipse + Rectangle) - (Ellipse + Ellipse) + Rectangle - Disk", check);

  //! case composite 3d
  Ellipsoid elld1(_center=Point(0.,0.,0.), _v1=Point(3.,0.,0.), _v2=Point(0.,2.,0.), _v6=Point(0.,0.,1.), _nnodes=5,
                  _domain_name="Omega");
  Parallelepiped para1(_v1=Point(-0.5,-0.5,-0.5), _v2=Point(0.5,-0.5,-0.5), _v4=Point(-0.5,0.5,-0.5), _v5=Point(-0.5,-0.5,0.5),
                       _nnodes=3);
  theCout << elld1-para1 << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite3D.in", errors, "Ellipsoid - Parallelepiped", check);

  //! canonical - loop
  Geometry g=ell1-sf1;
  g.domName("Omega"); // BUG is ignored in this case, it does not replace the name of ell1 when analyzed
  theCout << g << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical-RoundedRectangle.in", errors, "Ellipse - Rounded Rectangle (loop)", check);

  //! canonical + loop (loop inside canonical)
  theCout << ell1+sf1 << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical+RoundedRectangle.in", errors, "Ellipse + Rounded Rectangle (loop)", check);

  //! loop - canonical
  theCout << sf1-ell2 << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle-Canonical.in", errors, "Rounded Rectangle (loop) - Ellipse", check);

  //! loop + canonical (canonical inside loop : undetermined inclusion -> need to force)
  theCout << sf1+!ell2 << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle+Canonicalforce.in", errors, "Rounded Rectangle (loop) + Ellipse (need to force inclusion)", check);

  //! loop + canonical (loop inside canonical)
  theCout << sf1+ell1 << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle+Canonical.in", errors, "Rounded Rectangle (loop) + Ellipse", check);

  //! composite - loop
  theCout << (ell1+rect2)-sf1 << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite-RoundedRectangle.in", errors, "(Ellipse + Rectangle) - Rounded Rectangle (loop)", check);

  // composite + loop (loop inside one component)
  theCout << (ell1+rect2)+sf1 << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite+RoundedRectangle.in", errors, "(Ellipse + Rectangle) + Rounded Rectangle (loop inside)", check);

  //! loop - composite
  theCout << sf1-(ell2+ell3) << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle-Composite.in", errors, "Rounded Rectangle (loop) - (Ellipse + Ellipse)", check);

  //! loop + composite (composite inside loop : undetermined inclusions -> need to force)
  theCout << sf1+!(ell2+ell3) << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle+Compositeforce.in", errors, "Rounded Rectangle (loop) + (Ellipse + Ellipse) (need to force inclusion)", check);

  //! composite with loop - composite
  theCout << (ell1+sf1)-(ell2+ell3) << eol;
  nbErrors += checkValue(theCout, rootname+"/CompositeSF-Composite.in", errors, "(Ellipse + Rounded Rectangle (loop)) - (Ellipse + Ellipse)", check);

  //! composite with loop + composite (composite inside loop : undetermined inclusions -> need to rewrite and force)
  //! (ell1+sf1)+(ell2+ell3) should be written as follows
  //! (sf1+!(ell2+ell3))+ell1 or ell1+(sf1+!(ell2+ell3))
  theCout << (sf1+!(ell2+ell3))+ell1 << eol;
  nbErrors += checkValue(theCout, rootname+"/CompositeSF+Composite.in", errors, "(Rounded Rectangle (loop) + (Ellipse + Ellipse)) + Ellipse (need to force inclusion)", check);

  theCout << ell1+(sf1+!(ell2+ell3)) << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical+CompositeSF.in", errors, "Ellipse + (Rounded Rectangle (loop) + (Ellipse + Ellipse)) (need to force inclusion)", check);

  //! loop - loop
  Point a3(0,0,0.);
  Point b3(1,0,0.);
  Point c3(0,1,0.);
  Point d3(-1,0,0.);
  Segment seg9(_v1=d3, _v2=b3, _nnodes=6);
  CircArc circ5(_center=a3, _v1=b3, _v2=c3, _nnodes=3);
  CircArc circ6(_center=a3, _v1=c3, _v2=d3, _nnodes=3);
  Geometry sf2=surfaceFrom(seg9+circ5+circ6,"Omega7");
  theCout << sf1-sf2 << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle-HalfDisk.in", errors, "Rounded Rectangle (loop) - Half Disk (loop)", check);

  //! loop + loop (undetermined inclusion -> need to force)
  theCout << sf1+!sf2 << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle+HalfDiskforce.in", errors, "Rounded Rectangle (loop) + Half Disk (loop) (need to force inclusion)", check);

  //! loop + loop (no inclusion)
  Point a4(5.5,0.5,0.);
  Point b4(5.7,0.5,0.);
  Point c4(5.5,0.7,0.);
  Point d4(5.3,0.5,0.);
  Segment seg10(_v1=d4, _v2=b4, _nnodes=6);
  CircArc circ7(_center=a4, _v1=b4, _v2=c4, _nnodes=3);
  CircArc circ8(_center=a4, _v1=c4, _v2=d4, _nnodes=3);
  Geometry sf3=surfaceFrom(seg10+circ7+circ8, "Omega6");
  theCout << sf1+sf3 << eol;
  nbErrors += checkValue(theCout, rootname+"/RoundedRectangle+HalfDisk.in", errors, "Rounded Rectangle (loop) + Half Disk (loop)", check);

  //! composite with loop + loop (no inclusion)
  theCout << (sf1+!(ell2+ell3))+sf3 << eol;
  nbErrors += checkValue(theCout, rootname+"/CompositeSF+HalfDisk.in", errors, "(Rounded Rectangle (loop) + (Ellipse + Ellipse)) + Half Disk (loop)", check);

  theCout << (ell1+sf1)-(ell2+ell3)+rect2-sf3 << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite.in", errors, "(Ellipse + Rounded Rectangle (loop)) - (Ellipse + Ellipse) + Rectangle - Half Disk (loop)", check);

  //! composite + loop (loop inside one component + test += operator)
  g=ell1+rect2;
  g+=sf1;
  theCout << g << eol;
  nbErrors += checkValue(theCout, rootname+"/Composite+=RoundedRectangle.in", errors, "Ellipse + Rectangle += Rounded Rectangle (loop)", check);

  //! canonical - loop with use of operator -=
  //! to preserve ell1, we have to convert ell1 int a composite geometry with one component
  g=toComposite(ell1);
  g-=sf1;
  theCout << g << eol;
  nbErrors += checkValue(theCout, rootname+"/Canonical-=RoundedRectangle.in", errors, "Ellipse -= Rounded Rectangle (loop)", check);

  Point i1(3,0,0);
  Point j1(3,1,0);
  Point k1(3,0,1);
  Point l1(3,1,1);
  Rectangle rrect7(_v1=b2, _v2=i1, _v4=c2, _nnodes=6, _domain_name="R7");
  theCout << rrect1+rrect7 << eol;
  nbErrors += checkValue(theCout, rootname+"/Adjacent2D.in", errors, "Adjacent Rectangles", check);

  Parallelepiped ppara1(_v1=a2, _v2=b2, _v4=d2, _v5=e2, _nnodes=6, _domain_name="P1");
  Parallelepiped ppara2(_v1=b2, _v2=i1, _v4=c2, _v5=f2, _nnodes=6, _domain_name="P2");
  theCout << ppara1+ppara2 << eol;
  nbErrors += checkValue(theCout, rootname+"/Adjacent3D.in", errors, "Adjacent Parallelepipeds", check);

  Rectangle rbg(_xmin=-1, _xmax=1, _ymin=0, _ymax=1, _hsteps=0.05, _domain_name="Omega0", _side_names=Strings("y=-h/2","x=L","y=h/2","x=-L"));
  g=toComposite(rbg);
  Point p1(0.2,0.8), p2(0.,0.9), p3(-0.2,0.8), p4(0.,0.7);
  Point p5(-0.2,0.3), p6(0.2,0.3), p7(0.2,0.4), p8(0.2,0.5);
  Point p9(0.2,0.7), p10(0.3,0.6), p11(0.4,0.6);
  CircArc ca1(_center=Point(0.2,0.6), _v1=p8, _v2=p10, _nnodes=7, _domain_name="arc1");
  CircArc ca2(_center=Point(0.2,0.6), _v1=p10, _v2=p9, _nnodes=7, _domain_name="arc2");
  CircArc ca3(_center=Point(0.2,0.6), _v1=p7, _v2=p11, _nnodes=7, _domain_name="arc3");
  CircArc ca4(_center=Point(0.2,0.6), _v1=p11, _v2=p1, _nnodes=7, _domain_name="arc4");
  EllArc ea1(_center=Point(0.,0.8), _apogee=p1, _v1=p1, _v2=p2, _nnodes=10, _domain_name="ell1");
  EllArc ea2(_center=Point(0.,0.8), _apogee=p1, _v1=p2, _v2=p3, _nnodes=10, _domain_name="ell2");
  EllArc ea3(_center=Point(0.,0.8), _apogee=p1, _v1=p3, _v2=p4, _nnodes=10, _domain_name="ell3");
  EllArc ea4(_center=Point(0.,0.8), _apogee=p1, _v1=p4, _v2=p1, _nnodes=10, _domain_name="ell4");
  Segment sg1(_v1=p3,_v2=p5,_nnodes=10), sg2(_v1=p5,_v2=p6,_nnodes=10), sg3(_v1=p6,_v2=p7,_nnodes=3),
          sg4(_v1=p7,_v2=p8,_nnodes=3), sg5(_v1=p8,_v2=p9,_nnodes=4), sg6(_v1=p9,_v2=p1,_nnodes=3);
  g+=surfaceFrom(ea1+ea2+ea3+ea4, "Omega_vide");
  g+=surfaceFrom(sg1+sg2+sg3+sg4+sg5+sg6+(~ea4)+(~ea3), "Omega1");
  g+=surfaceFrom(sg4+ca1+ca2+sg6+(~ca4)+(~ca3), "Omega2");
  g+=surfaceFrom(ca1+ca2+(~sg5), "Omega0");
  theCout << g << eol;
  nbErrors += checkValue(theCout, rootname+"/AdjacentComposite.in", errors, "Composite surface with adjacent components", check);

  Geometry extr1=extrude(ell1, Translation(_direction={0.,0.,4.}), _layers=5, _domain_name="ExtrOmega", _side_names="Gamma");
  theCout << extr1 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionCanonicalTr.in", errors, "extrusion of Ellipse by Translation", check);

  Geometry extr2=extrude(sf1, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=pi_/2), _layers=10, _domain_name="ExtrOmega2");
  theCout << extr2 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionRoundedRectangleRot.in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (pi_/2)", check);

  Geometry extr3=extrude(ell1-ell2, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=pi_/2), _layers=10, _domain_name="ExtrOmega3", _side_names="Gamma");
  theCout << extr3 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionCompositeRot.in", errors, "extrusion of Ellipse - Ellipse by Rotation (pi_/2)", check);

  Geometry extr4=extrude(sf1, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=pi_), _layers=10, _domain_name="ExtrOmega4");
  theCout << extr4 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionRoundedRectangleRot180.in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (pi_)", check);

  Geometry extr5=extrude(ell1-ell2, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=pi_), _layers=10, _domain_name="ExtrOmega5", _side_names="Gamma");
  theCout << extr5 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionCompositeRot180.in", errors, "extrusion of Ellipse - Ellipse by Rotation (pi_)", check);

  Geometry extr6=extrude(sf1, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=3.*pi_/2), _layers=10, _domain_name="ExtrOmega6");
  theCout << extr6 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionRoundedRectangleRot270.in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (3*pi_/2)", check);

  Geometry extr7=extrude(ell1-ell2, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=3.*pi_/2), _layers=10, _domain_name="ExtrOmega7", _side_names="Gamma");
  theCout << extr7 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionCompositeRot270.in", errors, "extrusion of Ellipse - Ellipse by Rotation (3*pi_/2)", check);

  Geometry extr8=extrude(sf1, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=2.*pi_), _layers=10, _domain_name="ExtrOmega8");
  theCout << extr8 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionRoundedRectangleRot360.in", errors, "extrusion of Rounded Rectangle (loop) by Rotation (2*pi_)", check);

  Geometry extr9=extrude(ell1-ell2, Rotation3d(_center=Point(5.,0.,0.), _axis={0.,5.,0.}, _angle=2.*pi_), _layers=10, _domain_name="ExtrOmega9", _side_names="Gamma");
  theCout << extr9 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionCompositeRot360.in", errors, "extrusion of Ellipse - Ellipse by Rotation (2*pi_)", check);

  Geometry extr10=extrude(circ1, Translation(_direction={0.,0.,4.}), _layers=5, _domain_name="ExtrOmega10");
  theCout << extr10 << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionCircArcTr.in", errors, "extrusion of CircArc by Translation", check);

  //! true conesphere
  Real csbaseradiusint=2., csconeheightint=8.;
  Real cssphereheightint=csbaseradiusint*csbaseradiusint/csconeheightint;
  Real cssphereradiusint=sqrt(csbaseradiusint*csbaseradiusint+cssphereheightint*cssphereheightint);
  Real diff=2.;
  Real csbaseradiusext=2.+diff, csconeheightext=8.+2.*diff;
  Real cssphereheightext=csbaseradiusext*csbaseradiusext/csconeheightext;
  Real cssphereradiusext=sqrt(csbaseradiusext*csbaseradiusext+cssphereheightext*cssphereheightext);
  nnodes=10;

  Point csapexint(0.,0.,csconeheightint), csp1int(csbaseradiusint,0.,0.), csp2int(0.,0.,-cssphereheightint-cssphereradiusint);
  Point csapexext(0.,0.,csconeheightext), csp1ext(csbaseradiusext,0.,0.), csp2ext(0.,0.,-cssphereheightext-cssphereradiusext);

  Segment css1(_v1=csp1ext, _v2=csapexext, _nnodes=2*nnodes);
  Segment css2(_v1=csapexext, _v2=csapexint, _nnodes=2*nnodes);
  Segment css3(_v1=csapexint,_v2=csp1int, _nnodes=2*nnodes);
  CircArc csc1(_center=Point(0.,0.,-cssphereheightint), _v1=csp1int, _v2=csp2int, _nnodes=nnodes);
  Segment css4(_v1=csp2int, _v2=csp2ext, _nnodes=2*nnodes);
  CircArc csc2(_center=Point(0.,0.,-cssphereheightext),_v1=csp2ext,_v2=csp1ext, _nnodes=nnodes);

  Geometry csbase=planeSurfaceFrom(css1+css2+css3+csc1+css4+csc2, "Gamma");
  Geometry csextrude=extrude(csbase, Rotation3d(_center=Point(0.,0.,0.), _axis={0.,0.,1.}, _angle=2.*pi_), _domain_name="Omega1",
                             _side_names=Strings("Gamma1", "Gamma2", "Gamma3", "Gamma4", "Gamma5", "Gamma6", "Gamma7", "Gamma8",
                                     "Gamma9", "Gamma10", "Gamma11", "Gamma12", "Gamma13", "Gamma14", "Gamma15", "Gamma16"));
  theCout << csextrude << eol;
  nbErrors += checkValue(theCout, rootname+"/ExtrusionConesphere.in", errors, "Conesphere with extrusion by rotation (2.*pi_)", check);

  //! parametrized arcs
  Parametrization par1(0,2*pi_,x_1*cos(x_1),x_1*sin(x_1),Parameters(),"tcos(t),tsin(t)"); //open
  ParametrizedArc parc1(_parametrization = par1, _partitioning=_linearPartition, _nbParts=100,_hsteps=0.1,_domain_name="Gamma");
  Segment sarc1(_v1=parc1.p2(), _v2=parc1.p1(),_hsteps=0.1,_domain_name="Sigma");
  Geometry  paresc=surfaceFrom(parc1+sarc1,"Omega");
  printTestLabel(theCout, "Parametrized open arc");
  theCout << parc1 << eol;
  printTestLabel(theCout, "surface from parametrized open arc and segment");
  theCout << paresc << eol;

  Parametrization par2(0,2*pi_,2*cos(x_1),sin(x_1),Parameters(),"2cos(t),sin(t)");  //closed
  ParametrizedArc parc2(_parametrization = par2, _partitioning=_linearPartition, _nbParts=50,_hsteps=0.1,_domain_name="Gamma");
  Geometry parell=surfaceFrom(parc2,"Omega");
  printTestLabel(theCout, "Parametrized closed arc");
  theCout << parc2 << eol;
  theCout << "parc2.parametrization()(0.5)=" << parc2.parametrization()(0.5).roundToZero() << eol;
  printTestLabel(theCout, "surface from parametrized closed arc");
  theCout << parell << eol;

  //! test transformation of parametrized arc
  parc2.translate(_direction={100.,100.,0.});
  printTestLabel(theCout, "Translated parametrized closed arc");
  theCout << parc2 << eol;
  theCout << "parc2.parametrization()(0.5) after translation=" << parc2.parametrization()(0.5).roundToZero() << eol;
  nbErrors += checkValue(theCout, rootname+"/ParametrizedArc.in", errors, "Parametrized arcs", check);

  //! spline arcs
  Number n=5; std::vector<Point> points(n+1);
  Real x=0, dx=pi_/n;
  for(Number i=0;i<=n;i++, x+=dx) points[i]=Point(x,sin(x));
  Real ersup;
  bool save=false;

  printTestLabel(theCout, "C2-Spline open arc from spline");
  SplineArc spac2(_spline=C2Spline(points)) ;
  ersup=checkSpline(spac2,true, pi_,save,"spac2.dat");
  theCout << spac2 << eol<<" sup error = "<<ersup<<eol;

  printTestLabel(theCout, "C2-Spline open arc from vertices");
  SplineArc spac2v(_splineType=_C2Spline,_vertices=points);
  ersup=checkSpline(spac2v,true,pi_,save,"spac2v.dat");
  theCout << spac2v << eol<<" sup error = "<<ersup<<eol;

  printTestLabel(theCout, "C2-Spline open arc from vertices with centripetalParametrization");
  SplineArc spac2vc(_splineType=_C2Spline,_vertices=points,_splineParametrization=_centripetalParametrization);
  ersup=checkSpline(spac2vc,false,pi_,save,"spac2vc.dat");
  theCout << spac2vc << eol<<" sup error = "<<ersup<<eol;

  Number m=10; std::vector<Point> points2(m+1);
  Real xx=0, dxx=2*pi_/m;
  for(Number i=0;i<=m;i++, xx+=dxx) points2[i]=Point(xx,sin(xx));
  printTestLabel(theCout, "periodic C2-Spline open arc from vertices");
  SplineArc spac2p(_splineType=_C2Spline,_vertices=points2,_splineBC=_periodicBC);
  ersup=checkSpline(spac2p,true,2*pi_,save,"spac2p.dat");
  theCout << spac2p << eol<<" sup error = "<<ersup<<eol;

  printTestLabel(theCout, "Catmull-Rom spline open arc from vertices");
  SplineArc spacr(_splineType=_CatmullRomSpline,_vertices=points,_domain_name="Gamma",_hsteps=0.1);
  ersup=checkSpline(spacr,false,pi_,save,"spacr.dat");
  theCout << spacr << eol<<" sup error = "<<ersup<<eol;

  Segment sega(_v1=points[5],_v2=points[0],_hsteps=0.1,_domain_name="Sigma");
  Geometry sspac=surfaceFrom(sega+spacr);
  printTestLabel(theCout, "surface from Catmull-Rom spline open arc and segment");
  theCout << sspac << eol;

  printTestLabel(theCout, "approximate B-spline open arc from vertices");
  SplineArc spaba(_splineType=_BSpline,_vertices=points, _splineSubtype=_SplineApproximation,
                  _splineParametrization=_centripetalParametrization, _domain_name="Gamma",_hsteps=0.1);
  ersup=checkSpline(spaba,false,pi_,save,"spaba.dat");
  theCout << spaba << eol<<" sup error = "<<ersup<<eol;

  printTestLabel(theCout, "interpolate B-spline open arc from vertices");
  SplineArc spab(_splineType=_BSpline,_vertices=points,
                 _tangent0=Reals(1.,1.),_tangent1=Reals(1.,-1.), _domain_name="Gamma",_hsteps=0.1);
  ersup=checkSpline(spab,false,pi_,save,"spab.dat");
  theCout << spab << eol<<" sup error = "<<ersup<<eol;

  printTestLabel(theCout, "surface from B-spline open arc and segment");
  Geometry sspab=surfaceFrom(sega+spab);
  theCout << sspab << eol;

  n=3; points.resize(n+1); x=0.;dx=pi_/n;
  for(Number i=0;i<=n;i++, x+=dx) points[i]=Point(x,sin(x));
  printTestLabel(theCout, "Bezier open arc from vertices");
  SplineArc spaz(_splineType=_BezierSpline,_vertices=points,_domain_name="Gamma",_hsteps=0.1);
  theCout << spaz << eol;

  printTestLabel(theCout, "surface from Bezier open arc and segment");
  Geometry sspaz=surfaceFrom(sega+spaz);
  theCout << sspaz << eol;

  n=6; points.resize(n+1); x=0, dx=2*pi_/n;
  for(Number i=0;i<=n;i++,x+=dx) points[i]=Point(2*cos(x),sin(x));
  printTestLabel(theCout, "Catmull-Rom spline closed arc from vertices");
  SplineArc spael(_splineType=_CatmullRomSpline,_vertices=points,_domain_name="Gamma",_hsteps=0.1);
  theCout << spael << eol;

  printTestLabel(theCout, "surface from Catmull-Rom spline closed arc");
  Geometry surfspael=surfaceFrom(spael,"Omega");
  theCout << surfspael << eol;

  printTestLabel(theCout, "B-spline closed arc from vertices");
  SplineArc spaelb(_splineType=_BSpline,_vertices=points,_domain_name="Gamma",_hsteps=0.1);
  theCout << spaelb << eol;

  printTestLabel(theCout, "surface from B-spline closed arc");
  Geometry surfspaelb=surfaceFrom(spaelb,"Omega");
  theCout << surfspaelb << eol;
  nbErrors += checkValue(theCout, rootname+"/SplineArc.in", errors, "Spline arcs", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
