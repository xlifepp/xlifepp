/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 /*!
	\file unit_normals.cpp
	\since 16 jul 2014
	\date 16 jul 2014
	\author Eric Lunéville

	test the computation of reference normals
	see the function GeomDomain::setRefNormals
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_normals {

void unit_normals(int argc, char* argv[], bool check)
{
  String rootname = "unit_normals";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(10);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!---------------- 2d test ----------------------------
  Disk d(_center=Point(0.,0.),_radius=1,_nnodes=11,_domain_name="Disk",_side_names=Strings(4,"Gamma"));
  SquareGeo sq(_center=Point(0.,0.),_length=4,_nnodes=15,_domain_name="Rect",_side_names=Strings(4,"Sigma"));
  
  Mesh m2d(inputsPathTo(rootname+"/m2d.msh"), _name="m2d.sh");
  
  Domain disk=m2d.domain("Disk");
  Domain sigma1=m2d.domain("Sigma");
  Domain gamma1=m2d.domain("Gamma");
  saveToFile("omega2d.vtk",m2d);
  nbErrors+= checkValues("omega2d.vtk", rootname+"/omega2d.in", tol, errors, "omega2d", check);

  //!set orientations
  sigma1.setNormalOrientation(_outwardsInfinite);
  sigma1.saveNormalsToFile("n_sigma_1d_outwardsInfinite.vtk",_vtk);
  nbErrors+= checkValues("n_sigma_1d_outwardsInfinite.vtk", rootname+"/n_sigma_1d_outwardsInfinite.in", tol, errors, "n_sigma_1d_outwardsInfinite", check);
  gamma1.setNormalOrientation(_towardsDomain,disk);
  gamma1.saveNormalsToFile("n_gamma_1d_towards_disk.vtk",_vtk);
  nbErrors+= checkValues("n_gamma_1d_towards_disk.vtk", rootname+"/n_gamma_1d_towards_disk.in", tol, errors, "n_gamma_1d_towards_disk", check);
  
  //!---------------- 3d test ----------------------------
  // Sphere s(_center=Point(0.,0.,0.),_radius=1,_nnodes=5,_domain_name="Sphere",_side_names=Strings(8,"Gamma"));
  // Cube c(_center=Point(0.,0.,0.),_length=4,_nnodes=5,_domain_name="Cube",_side_names=Strings(6,"Sigma"));
  // Mesh m3d(s+c, _shape=_tetrahedron, _order=1, _generator=_gmsh);
  Mesh m3d(inputsPathTo(rootname+"/m3d.msh"), _name="m3d.msh");
  Domain sphere=m3d.domain("Sphere");
  Domain cube=m3d.domain("Cube");
  Domain sigma2=m3d.domain("Sigma");
  Domain gamma2=m3d.domain("Gamma");
  saveToFile("omega3d.vtk",m3d);
  nbErrors+= checkValues("omega3d.vtk", rootname+"/omega3d.in", tol, errors, "omega3d", check);
  //!set orientations
  sigma2.setNormalOrientation(_towardsInfinite);
  gamma2.setNormalOrientation(_outwardsInfinite);
  sigma2.saveNormalsToFile("n_sigma_2d_towardsInfinite.vtk",_vtk);
  gamma2.saveNormalsToFile("n_gamma_2d_outwardsInfinite.vtk",_vtk);
  nbErrors+= checkValues("n_sigma_2d_towardsInfinite.vtk", rootname+"/n_sigma_2d_towardsInfinite.in", tol, errors, "n_sigma_2d_towardsInfinite", check);
  nbErrors+= checkValues("n_gamma_2d_outwardsInfinite.vtk", rootname+"/n_gamma_2d_outwardsInfinite.in", tol, errors, "n_gamma_2d_outwardsInfinite", check);
  
  //!create TermVector with normals
  Space Vd(_domain=disk, _interpolation=_P1, _name="Vd");  Unknown ud(Vd, _name="ud", _dim=2);
  TermVector n_gam1 = normalsOn(gamma1,ud);
  saveToFile("n_gam1.vtu",n_gam1,_format=_vtu);
  saveToFile("n_gam1.vtu",n_gam1,_format=_vizir4);
  nbErrors+= checkValues("n_gam1_Gamma.vtu", rootname+"/n_gam1_Gamma.in", tol, errors, "n_gam1_Gamma", check);
  Space Vs(_domain=cube, _interpolation=_P1, _name="Vs");  Unknown us(Vs, _name="us", _dim=3);
  TermVector n_sig2 = normalsOn(sigma2,us); 
  saveToFile("n_sig2.vtu",n_sig2,_format=_vtu);
  nbErrors+= checkValues("n_sig2_Sigma.vtu", rootname+"/n_sig2_Sigma.in", tol, errors, "n_sig2_Sigma", check);

  //---------------- test on manifolds ----------------------------
  // Mesh m2df(d, _shape=_segment, _order=1, _generator=_gmsh)  , saveToFile("m2df.msh",m2df); 
  Mesh m2df(inputsPathTo(rootname+"/m2df.msh"), _name="m2df.msh");
  Domain gamma1f=m2df.domain("Gamma");
  //!set orientation
  gamma1f.setNormalOrientation(_towardsInfinite);
  gamma1f.saveNormalsToFile("n_gamma_1f_towardsInfinite.vtk",_vtk);
  nbErrors+= checkValues("n_gamma_1f_towardsInfinite.vtk", rootname+"/n_gamma_1f_towardsInfinite.in", tol, errors, "n_gamma_1f_towardsInfinite", check);
  
  //!create normals on P1 vertices
  Space Vc(_domain=gamma1f, _interpolation=_P1, _name="Vc");  Unknown uc(Vc, _name="uc", _dim=2);
  TermVector n_gam1f = normalsOn(gamma1f,uc);
  saveToFile("n_gam1f.vtu",n_gam1f,_format=_vtu);
  nbErrors+= checkValues("n_gam1f_Gamma.vtu", rootname+"/n_gam1f_Gamma.in", tol, errors, "n_gam1f_Gamma", check);

  Mesh  m3df(inputsPathTo(rootname+"/m3df.msh"), _name="m3df.msh");
  Domain gamma2f=m3df.domain("Gamma");
  gamma2f.setNormalOrientation(_towardsInfinite);
  gamma2f.saveNormalsToFile("n_gamma_2f_towardsInfinite.vtk",_vtk);
  nbErrors+= checkValues("n_gamma_2f_towardsInfinite.vtk", rootname+"/n_gamma_2f_towardsInfinite.in", tol, errors, "n_gamma_2f_towardsInfinite", check);
  
  //!create normals on P1 vertices
  Space Vt(_domain=gamma2f,_interpolation=_P1, _name="Vt");  Unknown ut(Vt, _name="ut", _dim=3);
  TermVector n_gam2f = normalsOn(gamma2f,ut);
  saveToFile("n_gam2f.vtu",n_gam2f,_format=_vtu);
  nbErrors+= checkValues("n_gam2f_Gamma.vtu", rootname+"/n_gam2f_Gamma.in", tol, errors, "n_gam2f_Gamma", check);


    int n=3;
    Real aszero=1e-5;
    typedef typename std::vector<Real>::iterator it_vk;
    Vector<Vector<Real> >* TrvEntries_p=new Vector<Vector<Real> > (n);
    (*TrvEntries_p)(1)= Vector<Real>(2,11.);
    (*TrvEntries_p)(2)= Vector<Real>(3,1.e-13);
    (*TrvEntries_p)(3)= 1.e-6;
    ((*TrvEntries_p)(2))(1)=2.;


/*-------------------------------------------------------------------*/
  if (check)
  { 
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  

  
}

}
