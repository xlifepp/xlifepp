/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_OdeSolver.cpp
  \author E. Lunéville
  \since 21 jun 2021
  \date 21 jun 2021

	tests of OdeSolver
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>

using namespace xlifepp;

namespace unit_OdeSolver{

// real scalar example
Real& fr(Real t, const Real& y, Real& fyt)
{ return fyt=y*t;}
Real yr(Real t)
{ return exp(t*t/2);}

// complex scalar example
Complex& fc(Real t, const Complex& y, Complex& fyt)
{ return fyt=y*t;}
Complex yc(Real t)
{ return exp(t*t/2);}

// linearized pendulum (2D real EDO)
Real omg=1., th0=pi_/4, thp0=0.;
Reals& fvr(Real t, const Reals& y, Reals& fyt)
{
    fyt.resize(2);
    fyt[0]=y[1];
    fyt[1]=-omg*omg*y[0];   // linear
    return fyt;
}
Reals yvr(Real t)
{
    Reals y(2,0.);
    y[0] = thp0*sin(omg*t)/omg+ th0*cos(omg*t);
    y[1] = thp0*cos(omg*t)- th0*omg*sin(omg*t);
    return y;
}

// linearized pendulum (2D complex EDO)
Complexes& fvc(Real t, const Complexes& y, Complexes& fyt)
{
    fyt.resize(2);
    fyt[0]=y[1];
    fyt[1]=-omg*omg*y[0];   // linear
    return fyt;
}
Complexes yvc(Real t)
{
    Complexes y(2,0.);
    y[0] = thp0*sin(omg*t)/omg+ th0*cos(omg*t);
    y[1] = thp0*cos(omg*t)- th0*omg*sin(omg*t);
    return y;
}


void unit_OdeSolver(int argc, char* argv[], bool check)
{
  String rootname = "unit_OdeSolver";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);

  Number nbErrors = 0;
  String errors;
  Real tol=0.01;

  // real scalar ODE
  Real t0=0.,tf=1., dt=0.01, y0=exp(t0*t0/2), prec= 1E-6, errsup=0.;

  std::list<std::pair<Real,Real> > ty = Euler(fr,t0,tf,dt,y0).tys();
  std::list<std::pair<Real,Real> >::iterator itp;
  for(itp=ty.begin();itp!=ty.end();++itp) errsup=std::max(errsup,norm(itp->second-yr(itp->first)));
  theCout<<"Euler fr  : nb dt="<<format(tostring(ty.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  ty = RK4(fr,t0,tf,dt,y0).tys();
  errsup=0;
  for(itp=ty.begin();itp!=ty.end();++itp) errsup=std::max(errsup,norm(itp->second-yr(itp->first)));
  theCout<<"RK4 fr    : nb dt="<<format(tostring(ty.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  ty = Ode45(fr,t0,tf,0.1,y0,prec).tys();
  errsup=0;
  for(itp=ty.begin();itp!=ty.end();++itp) errsup=std::max(errsup,norm(itp->second-yr(itp->first)));
  theCout<<"Ode45 fr  : nb dt="<<format(tostring(ty.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  // real vector ODE
  Reals yv0(2,0.);yv0[0]=th0; yv0[1]=thp0;
  std::list<std::pair<Real,Reals> > tvy = EulerT<Reals>(fvr,t0,tf,dt,yv0).tys();
  std::list<std::pair<Real,Reals> >::iterator itv;
  for(itv=tvy.begin();itv!=tvy.end();++itv) errsup=std::max(errsup,norm(itv->second-yvr(itv->first)));
  theCout<<"Euler fvr : nb dt="<<format(tostring(tvy.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  tvy = RK4T<Reals>(fvr,t0,tf,dt,yv0).tys(); errsup=0.;
  for(itv=tvy.begin();itv!=tvy.end();++itv) errsup=std::max(errsup,norm(itv->second-yvr(itv->first)));
  theCout<<"RK4 fvr   : nb dt="<<format(tostring(tvy.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  tvy = Ode45T<Reals>(fvr,t0,tf,0.1,yv0,prec).tys(); errsup=0.;
  for(itv=tvy.begin();itv!=tvy.end();++itv) errsup=std::max(errsup,norm(itv->second-yvr(itv->first)));
  theCout<<"Ode45 fvr : nb dt="<<format(tostring(tvy.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  // complex scalar ODE
  Complex yc0=exp(t0*t0/2); errsup=0.;
  std::list<std::pair<Real,Complex> > tc = EulerT<Complex>(fc,t0,tf,dt,yc0).tys();
  std::list<std::pair<Real,Complex> >::iterator itc;
  for(itc=tc.begin();itc!=tc.end();++itc) errsup=std::max(errsup,norm(itc->second-yc(itc->first)));
  theCout<<"Euler fc  : nb dt="<<format(tostring(tc.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  tc = RK4T<Complex>(fc,t0,tf,dt,yc0).tys();
  errsup=0;
  for(itc=tc.begin();itc!=tc.end();++itc) errsup=std::max(errsup,norm(itc->second-yc(itc->first)));
  theCout<<"RK4 fc    : nb dt="<<format(tostring(tc.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  tc = Ode45T<Complex>(fc,t0,tf,0.1,yc0,prec).tys();
  errsup=0;
  for(itc=tc.begin();itc!=tc.end();++itc) errsup=std::max(errsup,norm(itc->second-yc(itc->first)));
  theCout<<"Ode45 fc  : nb dt="<<format(tostring(tc.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  // complex vector ODE
  Complexes yvc0(2,0.);yvc0[0]=th0; yvc0[1]=thp0;
  std::list<std::pair<Real,Complexes> > tvc = EulerT<Complexes>(fvc,t0,tf,dt,yvc0).tys();
  std::list<std::pair<Real,Complexes> >::iterator itvc;
  for(itvc=tvc.begin();itvc!=tvc.end();++itvc) errsup=std::max(errsup,norm(itvc->second-yvc(itvc->first)));
  theCout<<"Euler fvc : nb dt="<<format(tostring(tvc.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  tvc = RK4T<Complexes>(fvc,t0,tf,dt,yvc0).tys(); errsup=0.;
  for(itvc=tvc.begin();itvc!=tvc.end();++itvc) errsup=std::max(errsup,norm(itvc->second-yvr(itvc->first)));
  theCout<<"RK4 fvc   : nb dt="<<format(tostring(tvc.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  tvc = Ode45T<Complexes>(fvc,t0,tf,0.1,yvc0,prec).tys(); errsup=0.;
  for(itvc=tvc.begin();itvc!=tvc.end();++itvc) errsup=std::max(errsup,norm(itvc->second-yvr(itvc->first)));
  theCout<<"Ode45 fvc : nb dt="<<format(tostring(tvc.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  //check end-user scalar real function
  Reals sol = euler(fr,t0,tf,dt,y0);
  Real t=t0; errsup=0.;
  for(Number i=0;i<sol.size(); i++, t+=dt) errsup=std::max(errsup,norm(sol[i]-yr(t)));
  theCout<<"fun euler fr : nb dt="<<format(tostring(sol.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  sol = rk4(fr,t0,tf,dt,y0);
  t=t0; errsup=0.;
  for(Number i=0;i<sol.size(); i++, t+=dt) errsup=std::max(errsup,norm(sol[i]-yr(t)));
  theCout<<"fun rk4 fr   : nb dt="<<format(tostring(sol.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  std::pair<Reals,Reals> solp = ode45(fr,t0,tf,0.1,y0,prec);
  errsup=0.;
  Number n=solp.first.size();
  for(Number i=0;i<n; i++) errsup=std::max(errsup,norm(solp.second[i]-yr(solp.first[i])));
  theCout<<"fun ode45 fr : nb dt="<<format(tostring(n),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  //check end-user vector real function
  t=t0; errsup=0.;
  Vector<Reals> solv = euler(fvr,t0,tf,dt,yv0);
  for(Number i=0;i<solv.size(); i++, t+=dt) errsup=std::max(errsup,norm(solv[i]-yvr(t)));
  theCout<<"fun euler fvr : nb dt="<<format(tostring(solv.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  t=t0; errsup=0.;
  solv = rk4(fvr,t0,tf,dt,yv0);
  for(Number i=0;i<solv.size(); i++, t+=dt) errsup=std::max(errsup,norm(solv[i]-yvr(t)));
  theCout<<"fun rk4 fvr   : nb dt="<<format(tostring(solv.size()),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  t=t0; errsup=0.;
  std::pair<Reals,Vector<Reals> > solvp = ode45(fvr,t0,tf,0.1,yv0,prec);
  n=solvp.first.size();
  for(Number i=0;i<n; i++) errsup=std::max(errsup,norm(solvp.second[i]-yvr(solvp.first[i])));
  theCout<<"fun ode45 fvr : nb dt="<<format(tostring(n),3,_rightAlignment)<<" error sup = "<<errsup<<eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
