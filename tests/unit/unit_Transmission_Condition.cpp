/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Transmission_Condition.cpp
  \author E. Lunéville
	\since 09 mar 2014
	\date  09 mar 2014

	Test transmission condition on Laplace problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Transmission_Condition {

Real fm8(const Point& P, Parameters& pa = defaultParameters)
{
  return -8.;
}

Real Solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1);
  return 4.*(x-0.5)*(x-0.5);
}

Real Solex3p(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1);
  return 4.*x*x-3*x;
}

Real Solex3m(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1);
  return 4.*x*x-3*x+1;
}

Real g(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

void unit_Transmission_Condition(int argc, char* argv[], bool check)
{
  String rootname = "unit_Transmission_Condition";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(40);
  
  String errors;
  Real eps=0.0001;
  Number nbErrors= 0;

  //!test standard homogeneous transmission problem
  //!create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[1] = "x=1/2-"; sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=0.5,_ymin=0,_ymax=1,_nnodes=Numbers(10,20),_domain_name="Omega-",_side_names=sidenames),_shape=_triangle, _order=1, _generator=_structured);
  sidenames[1] = "x=1"; sidenames[3] = "x=1/2+";
  Mesh mesh2d_p(Rectangle(_xmin=0.5,_xmax=1,_ymin=0,_ymax=1,_nnodes=Numbers(10,20),_domain_name="Omega+",_side_names=sidenames),_shape=_triangle, _order=1, _generator=_structured);
  mesh2d.merge(mesh2d_p);
  mesh2d.printInfo();
  Domain omegaM=mesh2d.domain("Omega-");
  Domain omegaP=mesh2d.domain("Omega+");
  Domain sigmaM=mesh2d.domain("x=0");
  Domain sigmaP=mesh2d.domain("x=1");
  Domain gamma=mesh2d.domain("x=1/2-_x=1/2+");
  //!create interpolation
  Space VM(_domain=omegaM, _interpolation=_P1, _name="VM");
  Unknown uM(VM, _name="u-");
  TestFunction vM(uM, _name="v-");
  Space VP(_domain=omegaP, _interpolation=P1, _name="VP");
  Unknown uP(VP, _name="u+");
  TestFunction vP(uP, _name="v+");
  //!create bilinear form and linear form
  BilinearForm auv=intg(omegaM,grad(uM)|grad(vM))+intg(omegaP,grad(uP)|grad(vP));
  LinearForm fv=intg(omegaM,fm8*vM)+intg(omegaP,fm8*vP);
  EssentialConditions ecs= (uM|sigmaM = 1) & (uP|sigmaP = 1) & ((uM|gamma) - (uP|gamma) = 0);
  TermMatrix A(auv, ecs, _name="A");
  TermVector F(fv, _name="B");
  //!solve linear system AX=F
  TermVector U=directSolve(A,F);
  //saveToFile("U_T_2D",U,_format=_vtu);
  //!compute error
  BilinearForm uv=intg(omegaM,uM*vM)+intg(omegaP,uP*vP);
  TermMatrix M(uv, _name="M");
  TermVector Uex = TermVector(uM,omegaM,Solex)+ TermVector(uP,omegaP,Solex); 
  nbErrors += checkValuesL2( U, Uex, M, eps,  errors, "standard homogeneous transmission" );

  //!test transmission using general transmission condition involving a boundary domain not defined on the support mesh
  Mesh mseg(Segment(_v1=Point(0.5,0.),_v2=Point(0.5,1.),_nnodes=30,_domain_name="x=1/2"));
  Domain seg=mseg.domain("x=1/2");
  EssentialConditions ecs2= (uM|sigmaM = 1) & (uP|sigmaP = 1) & ((uM|seg) - (uP|seg) = 0);
  TermMatrix A2(auv, ecs2, _name="A2");
  //!solve linear system A2X=F
  TermVector U2=directSolve(A2,F);
  nbErrors += checkValuesL2( U2, Uex, M, eps,  errors, "transmission using general transmission condition involving a boundary domain not defined on the support mesh");

  //!test non homogeneous transmission problem
  EssentialConditions ecs3= (uM|sigmaM = 1) & (uP|sigmaP = 1) & ((uM|gamma) - (uP|gamma) = g);
  TermMatrix A3(auv, ecs3, _name="A3");
  //!solve linear system A2X=F
  TermVector U3=directSolve(A3,F);
  TermVector Uex3 = TermVector(uM,omegaM,Solex3m)+ TermVector(uP,omegaP,Solex3p);
  nbErrors += checkValuesL2( U3, Uex3, M, eps,  errors, "non homogeneous transmission problem");

  if (check)
  {
   if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
   else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else
  { theCout << "Data updated " << eol; }
 
 trace_p->pop();
 
}

}
