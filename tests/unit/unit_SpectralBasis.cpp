/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_SpectralBasis.cpp
  \author E. Lunéville
	\since 2 apr 2014
	\date 2 apr 2014

	low level test of SpectralBasis class
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_SpectralBasis {

Real k=1;

//!cos basis
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                    //get the parameter h (user definition)
  Int n=pa("basis index")-1;         //get the index of function to compute
  if (n==0) return sqrt(1./h);
  else     return sqrt(2./h)*cos(n*pi_*(y+h*0.5)/h);
}

//!id map
Vector<Real> mapP(const Point& P, Parameters& pa = defaultParameters)
{
  return P;
}


void unit_SpectralBasis(int argc, char* argv[], bool check)
{
  String rootname = "unit_SpectralBasis";
  trace_p->push(rootname);

  std::stringstream ssout;                  // string stream receiving results
  std::stringstream ssref;                   // string stream receiving reference data
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;
  verboseLevel(1);

  //!create a mesh and Domains
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=pi_,_ymin=-0.5,_ymax=0.5,_nnodes=Numbers(16,6),_side_names=Strings("y=0","x=pi","y=h","x=0")),_shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=pi");
  Domain sigmaM=mesh2d.domain("x=0");

  //!create interpolation
  Space V(_domain=omega, _interpolation=_P2, _name="V");
  Unknown u(V, _name="u");

  //!parameters
  Real h=1;
  Parameters params(h,"h");
  params<<Parameter(k,"k");
  Function cos_basis(cosny, params);
  Number N=5;

  //!create spectral basis from Function
  SpectralBasisFun cos(sigmaP, cos_basis, N);
  ssout <<"---------- SpectralBasisFun ----------------"<<eol;
  ssout <<"  number of functions : "<<cos.numberOfFun()<<eol;
  ssout <<"  functions dimension : "<<cos.dimFun()<<eol;
  ssout <<"  space dimension : "<<cos.dimSpace()<<eol;
  ssout <<"  domain support : "<<cos.domain()->name()<<eol;
  ssout <<"  function type : "<<words("function type",cos.funcFormType())<<eol;
  ssout <<"  value type : "<<words("value",cos.valueType())<<eol;
  ssout <<"  structure type : "<<words("structure",cos.strucType())<<eol;
  ssout <<"  cos : "<<cos<<eol;
  nbErrors += checkValue( cos.numberOfFun(), 5, tol, errors, "number of functions :");
  nbErrors += checkValue( cos.dimFun(), 1, tol, errors, "functions dimension:");
  nbErrors += checkValue( cos.dimSpace(), 5, tol, errors, "space dimension:");
  nbErrors += checkValue( cos.domain()->name(), "x=pi", errors, "domain support:");
  nbErrors += checkValue( words("function type",cos.funcFormType()), "analytical", errors, "function type:");
  nbErrors += checkValue( words("value",cos.valueType()) , "real", errors, "value type:");
  nbErrors += checkValue( words("structure",cos.strucType()), "scalar", errors, "structure type:");

  //!create space from spectral basis
  Space Sp(cos, "cos(n*pi*y)");
  ssout <<"  Sp(sigmaP, sb, N, ""cos(n*pi*y)"") : "<<Sp<<eol;
  Space Spf(_domain=sigmaP, _basis=cos_basis, _dim=N, _name="cos(n*pi*y)");
  ssout <<"  Spf(sigmaP, cos_basis, N, ""cos(n*pi*y)"") : "<<Spf<<eol;
  //!compute basis at point pi/3
  Point P(pi_,0.1);
  Real r;
  Vector<Real> refCos(5);
  refCos(1)=1.;
  refCos(2)=-0.437016024449;
  refCos(3)=-1.14412280564;
  refCos(4)=1.14412280564;
  refCos(5)=0.437016024449;
  for(Number n=1;n<=N;n++)
   {
     ssout <<"  cos.function("<<tostring(n)<<",P,r) = "<<cos.function(n,P,r)<<eol;
     nbErrors += checkValue( cos.function(n,P,r), refCos(n), tol, errors, "cos.function("+tostring(n)+",P,r) ");
   }
  Vector<Real> R;
  ssout <<"  cos.function(P,R) = "<<cos.functions(P,R)<<eol;
  nbErrors += checkValues(cos.functions(P,R), refCos, tol, errors, " cos.function(P,R)");

  //!create spectral space from interpolated functions (TermVectors)
  TermVectors cos_int(N);
  for(Number n=1; n<=N; n++)
    {
      cos_basis.parameter("basis index")=n;
      cos_int(n)=TermVector(u, sigmaP, cos_basis, "c"+tostring(n-1));
    }
    SpectralBasisInt sb_int(cos_int);
  ssout <<"---------- SpectralBasisInt ----------------"<<eol;
  ssout <<"  number of functions : "<<sb_int.numberOfFun()<<eol;
  ssout <<"  functions dimension : "<<sb_int.dimFun()<<eol;
  ssout <<"  space dimension : "<<sb_int.dimSpace()<<eol;
  ssout <<"  domain support : "<<sb_int.domain()->name()<<eol;
  ssout <<"  function type : "<<words("function type",sb_int.funcFormType())<<eol;
  ssout <<"  value type : "<<words("value",sb_int.valueType())<<eol;
  ssout <<"  structure type : "<<words("structure",sb_int.strucType())<<eol;
  ssout <<"  sb_int : "<<sb_int<<eol;
  nbErrors += checkValue( sb_int.numberOfFun(), 5, tol, errors, "number of functions :");
  nbErrors += checkValue( sb_int.dimFun(), 1, tol, errors, "functions dimension:");
  nbErrors += checkValue( sb_int.dimSpace(), 5, tol, errors, "space dimension:");
  nbErrors += checkValue( sb_int.domain()->name(), "x=pi", errors, "domain support:");
  nbErrors += checkValue( words("function type",sb_int.funcFormType()), "interpolated", errors, "function type:");
  nbErrors += checkValue( words("value",sb_int.valueType()) , "real", errors, "value type:");
  nbErrors += checkValue( words("structure",sb_int.strucType()), "scalar", errors, "structure type:");
  Space Sp1(sb_int, "interpolated cos(n*pi*y)");
  ssout <<" Sp1(sb, ""interpolated cos(n*pi*y)"") : "<<Sp1<<eol;
  Space Sp_int(_basis=cos_int, _name="interpolated cos(n*pi*y)");
  ssout <<" Sp_int(cos_int, ""interpolated cos(n*pi*y)"") : "<<Sp_int<<eol;
  for(Number n=1;n<=N;n++)
    {
     ssout <<"  sb_int.function("<<tostring(n)<<",P,r) = "<<sb_int.function(n,P,r)<<eol;
     nbErrors += checkValue( sb_int.function(n,P,r), refCos(n), tol, errors, "sb_int.function("+tostring(n)+",P,r) ");
    }
  ssout <<"  sb_int.function(P,R) = "<<sb_int.functions(P,R)<<eol;
  nbErrors += checkValues(sb_int.functions(P,R), refCos, tol, errors, " sb_int.function(P,R)");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
