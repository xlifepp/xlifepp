/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Laplace_EigenValues.cpp
  \author E. Lunéville
	\since 14 apr 2014
	\date  14 apr 2014

	test eigenvalues of Laplace problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Laplace_EigenValues {

void unit_Laplace_EigenValues(int argc, char* argv[], bool check)
{
  String rootname = "unit_Laplace_EigenValues";
  trace_p->push(rootname);

  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(40);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;


  //!create a mesh and Domains
  //! Mesh: Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=11,_side_names=sidenames),_triangle,1,_structured) -> mesh2d
  Mesh mesh2d(inputsPathTo(rootname+"/mesh2d.msh"), _name="mesh2d.msh");

  Domain omega=mesh2d.domain("Omega");

  //!create interpolation
  Space V(_domain=omega,_interpolation=_P1, _name="V");
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");
  std::cout<<"mesh and space created cputime="<<cpuTime()<<eol;

  //!create bilinear form and linear form
  BilinearForm auv=intg(omega,grad(u)|grad(v))+intg(omega,u*v);
  TermMatrix A(auv, _name="A");
  ssout<<A<<eol;
  nbErrors+=checkValues(A,rootname+"/A.in", tol, errors, "A", check);
    std::cout<<"TermMatrix A computed cputime="<<cpuTime()<<eol;

  //!compute eigen elements
  EigenElements eigs=eigenInternSolve(A, _nev=60, _mode=_krylovSchur, _which="SM");
  ssout<<"eigen values computation cputime="<<cpuTime()<<eol;
  ssout<<eigs;
  nbErrors += checkValues( A, eigs, tol, errors, "Laplace Eigen Vector/Value ", check);
 
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
 
}

}
