/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; withssout even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file unit_EigenSolverSparseUtils.cpp
  \author ManhHa NGUYEN
  \since 23 March 2013
  \date 22 May 2013

  Tests of some utils for sparse matrix.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_EigenSolverSparseUtils {

void unit_EigenSolverSparseUtils(int argc, char* argv[], bool check)
{
  String rootname = "unit_EigenSolverSparseUtils";
  trace_p->push(rootname);
  std::stringstream ssout; // string stream receiving results
  ssout.precision(testPrec);
  verboseLevel(10);
  Number nbErrors = 0;
  String errors;
  Real tol=0.001;
  bool isTM=true;

  ssout << "--------------------TESTING with Real---------------------------------" << "\n \n";
  ssout << "--------------------MultiVecAdapter---------------------" << "\n";

  typedef MultiVecAdapter<Real>                 RealMV;
  typedef LargeMatrixAdapter<LargeMatrix<Real>,Real>              RealOP;

  Real alpha = 2.0, beta = 3.0;
  int vecLength = 20;
  int vecNum    = 5;

  int blockSize = 3;

  // Load MatrixEigenDense
  const std::string rMatrixDataSym(inputsPathTo(rootname+"/rSym5.data"));
  MatrixEigenDense<Real> rMat(vecNum, vecNum), rMatB(vecNum, vecNum);
  rMat.loadFromFile(rMatrixDataSym.c_str());

  std::vector<int> idx(blockSize);
  idx[0] = 1;
  idx[1] = 3;
  idx[2] = 4;

  std::vector<Real> dotProduct(vecNum);
  std::vector<Real> norm(vecNum);
  std::vector<Real> scale(vecNum, 6);

  std::vector<Real> initVec(vecLength, 4);
  std::vector< std::vector<Real>* > initVecVec(vecNum);
  std::vector< std::vector<Real> >  arrayVec(vecNum, initVec);
  for (int i = 0; i < vecNum; ++i) {
      initVecVec[i] = &arrayVec[0]+i;
  }

  ssout << "-------MultiVec-------------" << "\n";
  ssout << "Initialize multi-vector " << "\n";
  RealMV rmvA(vecLength, vecNum);
  ssout << rmvA << "\n";
  ssout << "Initialize multi-vector from std vector" << "\n";
  RealMV rmvB(vecLength, initVecVec);
  ssout << rmvB << "\n";
  ssout << "Initialize multi-vector from another multi-vector" << "\n";
  RealMV rmvC(rmvB);
  ssout << rmvC << "\n";
  nbErrors+=checkValues(rmvA, rootname+"/rmvA.in", tol, errors, "MultiVecAdapter<Real> rmvA", check);
  nbErrors+=checkValues(rmvB, rootname+"/rmvB.in", tol, errors, "MultiVecAdapter<Real> rmvB", check);
  nbErrors+=checkValues(rmvC, rootname+"/rmvC.in", tol, errors, "MultiVecAdapter<Real> rmvC", check);
  ssout << "CloneView with index from another multi-vector" << "\n";
  RealMV* cloneRmvB = const_cast<RealMV*>(rmvB.cloneView(idx));
  ssout << *cloneRmvB << "\n";
  ssout << "CloneCopy with index from another multi-vector" << "\n";
  RealMV* cloneRmvC = rmvC.cloneViewNonConst(idx);
  ssout << *cloneRmvC << "\n";
  nbErrors+=checkValues(*cloneRmvB, rootname+"/cloneRmvBcopy.in", tol, errors, "copy *cloneRmvB", check);
  nbErrors+=checkValues(*cloneRmvC, rootname+"/cloneRmvCcopy.in", tol, errors, "copy *cloneRmvC", check);


  ssout << "Clone from another multi-vector" << "\n";
  RealMV* cloneRmvA = rmvA.clone(vecNum);
  ssout << *cloneRmvA << "\n";
  ssout << "CloneCopy from another multi-vector" << "\n";
  cloneRmvB = rmvB.cloneCopy();
  ssout << *cloneRmvB << "\n";
  ssout << "CloneCopy with index from another multi-vector" << "\n";
  cloneRmvC = rmvC.cloneCopy(idx);
  ssout << *cloneRmvC << "\n";
  nbErrors+=checkValues(*cloneRmvA, rootname+"/cloneRmvA.in", tol, errors, "Clone from another multi-vector *cloneRmvA", check);
  nbErrors+=checkValues(*cloneRmvB, rootname+"/cloneRmvB.in", tol, errors, "Clone from another multi-vector *cloneRmvB", check);
  nbErrors+=checkValues(*cloneRmvC, rootname+"/cloneRmvC.in", tol, errors, "CloneCopy with index from another multi-vector *cloneRmvC", check);

  ssout << "MvTimesMatAddMv " << "\n";
  rmvB.mvTimesMatAddMv(alpha, *cloneRmvB, rMat, beta);
  ssout << rmvB << "\n";
  nbErrors+=checkValues(rmvB, rootname+"/rmvB_2.in", tol, errors, "rmvB=MvTimesMatAddMv(alpha, *cloneRmvB, rMat, beta)", check);
  ssout << "MvAddMv " << "\n";
  rmvB.mvAddMv(alpha, *cloneRmvA, beta, *cloneRmvB);
  ssout << rmvB << "\n";
  nbErrors+=checkValues(rmvB, rootname+"/rmvB_3.in", tol, errors, "mvAddMv(alpha, *cloneRmvA, beta, *cloneRmvB)", check);
  ssout << "MvAddMv " << "\n";
  rmvB.mvTransMv(alpha, *cloneRmvB, rMatB);
  ssout << rMatB << "\n";
  nbErrors+=checkValues(rMatB, rootname+"/rMatB.in", tol, errors, "rmvB.mvTransMv(alpha, *cloneRmvB, rMatB);", check);
  ssout << "MvDot " << "\n";
  rmvB.mvDot(*cloneRmvB, dotProduct);
  ssout << dotProduct << "\n";
  nbErrors+=checkValues(dotProduct, rootname+"/rmvBDotProduct.in", tol, errors, "rmvB.mvDot(*cloneRmvB, dotProduct)", check);
  ssout << "MvNorm " << "\n";
  rmvB.mvNorm(norm);
  ssout << norm << "\n";
  nbErrors+=checkValues(norm, rootname+"/rmvBnorm.in", tol, errors, "rmvB.mvnorm(norm)", check);
  ssout << "SetBlock " << "\n";
  rmvC.setBlock(rmvB, idx);
  ssout << rmvC << "\n";
  nbErrors+=checkValues(rmvC, rootname+"/rmvC_2.in", tol, errors, "rmvC.setBlock(rmvB, idx)", check);
  ssout << "MvScale " << "\n";
  rmvC.mvScale(alpha);
  ssout << rmvC << "\n";
  nbErrors+=checkValues(rmvC, rootname+"/rmvC_3.in", tol, errors, "rmvC.mvScale(alpha)", check);
  ssout << "MvScale with a scale vector" << "\n";
  rmvC.mvScale(scale);
  ssout << rmvC << "\n";
  nbErrors+=checkValues(rmvC, rootname+"/rmvC_4.in", tol, errors, "rmvC.mvScale(scale)", check);
  ssout << "Initialize a multi-vector" << "\n";
  rmvC.mvInit(Real(1.0));
  ssout << rmvC << "\n";
  ssout << *(rmvC[0]) << "\n";
  nbErrors+=checkValues(rmvC, rootname+"/rmvC_5.in", tol, errors, "rmvC.mvScale(scale)", check);
  nbErrors+=checkValues(*(rmvC[0]), rootname+"/rmvC0.in", tol, errors, "*(rmvC[0])", check);
  RealMV rmvD(5,5);
  rmvD.mvLoadFromFile(rMatrixDataSym.c_str());
  ssout << "Load from file" << "\n";
  ssout << rmvD << "\n";
  nbErrors+=checkValues(rmvD, rootname+"/rmvD.in", tol, errors, "rmvD.loadFromFile(...))", check);

  ssout << "--------------------LargeMatrixAdapter---------------------" << "\n";
  const std::string rLMtrixData(inputsPathTo(rootname+"/rSym20.data"));
  LargeMatrix<Real> rLargeMatrix(rLMtrixData, _dense, vecLength, vecLength, _cs, _row);
  RealOP rLMat(&rLargeMatrix);
  ssout << "Apply Op: Ax = y " << "\n";
  rLMat.apply(rmvB,rmvA);
  ssout << rmvA << "\n";
  nbErrors+=checkValues(rmvA, rootname+"/rmvALM.in", tol, errors, "rLMat.apply(rmvB,rmvA)", check);


  ssout << "--------------------TESTING with Complex---------------------------------" << "\n \n";
  ssout << "--------------------MultiVecAdapter---------------------" << "\n";

  typedef MultiVecAdapter<Complex>                 CplxMV;
  typedef LargeMatrixAdapter<LargeMatrix<Complex>, Complex>              CplxOP;

  Complex calpha = Complex(2.0,1.0), cbeta = Complex(3.0,1.0);

  //! Load MatrixEigenDense
  const std::string cMatrixDataSym(inputsPathTo(rootname+"/cSelfAdjoint5.data"));
  MatrixEigenDense<Complex> cMat(vecNum, vecNum), cMatB(vecNum, vecNum);
  cMat.loadFromFile(cMatrixDataSym.c_str());

  std::vector<Complex> cdotProduct(vecNum);
  std::vector<Complex> cnorm(vecNum);
  std::vector<Complex> cscale(vecNum, 6);

  std::vector<Complex> cinitVec(vecLength, 4);
  std::vector< std::vector<Complex>* > cinitVecVec(vecNum);
  std::vector< std::vector<Complex> >  carrayVec(vecNum, cinitVec);
  for (int i = 0; i < vecNum; ++i) {
      cinitVecVec[i] = &carrayVec[0]+i;
  }

  ssout << "-------MultiVec-------------" << "\n";
  ssout << "Initialize multi-vector " << "\n";
  CplxMV cmvA(vecLength, vecNum);
  ssout << cmvA << "\n";
  ssout << "Initialize multi-vector from std vector" << "\n";
  CplxMV cmvB(vecLength, cinitVecVec);
  ssout << cmvB << "\n";
  ssout << "Initialize multi-vector from another multi-vector" << "\n";
  CplxMV crmvC(cmvB);
  ssout << crmvC << "\n";
  nbErrors+=checkValues(cmvA, rootname+"/cmvA.in", tol, errors, "cmvA", check);
  nbErrors+=checkValues(cmvB, rootname+"/cmvB.in", tol, errors, "cmvB", check);
  nbErrors+=checkValues(crmvC, rootname+"/crmvC.in", tol, errors, "crmvC", check);

  ssout << "CloneView with index from another multi-vector" << "\n";
  CplxMV* cplxcloneRmvB = const_cast<CplxMV*>(cmvB.cloneView(idx));
  ssout << *cplxcloneRmvB << "\n";
  ssout << "CloneCopy with index from another multi-vector" << "\n";
  CplxMV* cplxcloneRmvC = crmvC.cloneViewNonConst(idx);
  ssout << *cplxcloneRmvC << "\n";
  nbErrors+=checkValues(*cplxcloneRmvB, rootname+"/cplxcloneRmvBcopy.in", tol, errors, "*cplxcloneRmvB", check);
  nbErrors+=checkValues(*cplxcloneRmvC, rootname+"/cplxcloneRmvCcopy.in", tol, errors, "*cplxcloneRmvC", check);

  ssout << "Clone from another multi-vector" << "\n";
  CplxMV* cplxcloneRmvA = cmvA.clone(vecNum);
  ssout << *cplxcloneRmvA << "\n";
  ssout << "CloneCopy from another multi-vector" << "\n";
  cplxcloneRmvB = cmvB.cloneCopy();
  ssout << *cplxcloneRmvB << "\n";
  ssout << "CloneCopy with index from another multi-vector" << "\n";
  cplxcloneRmvC = crmvC.cloneCopy(idx);
  ssout << *cplxcloneRmvC << "\n";
  nbErrors+=checkValues(*cplxcloneRmvA, rootname+"/cplxcloneRmvA.in", tol, errors, "clone *cplxcloneRmvA", check);
  nbErrors+=checkValues(*cplxcloneRmvB, rootname+"/cplxcloneRmvB.in", tol, errors, "clone *cplxcloneRmvB", check);
  nbErrors+=checkValues(*cplxcloneRmvC, rootname+"/cplxcloneRmvC.in", tol, errors, "clone *cplxcloneRmvC", check);

  ssout << "MvTimesMatAddMv " << "\n";
  cmvB.mvTimesMatAddMv(calpha, *cplxcloneRmvB, cMat, cbeta);
  ssout << cmvB << "\n";
  nbErrors+=checkValues(cmvB, rootname+"/cmvB_2.in", tol, errors, "cmvB=MvTimesMatAddMv(alpha, *cplxcloneRmvB, cMat, cbeta)", check);
  ssout << "MvAddMv " << "\n";
  cmvB.mvAddMv(calpha, *cplxcloneRmvA, cbeta, *cplxcloneRmvB);
  ssout << cmvB << "\n";
  nbErrors+=checkValues(cmvB, rootname+"/cmvB_3.in", tol, errors, "mvAddMv(alpha, *cplxcloneRmvA, cbeta, *cplxcloneRmvB)", check);
  ssout << "MvAddMv " << "\n";
  cmvB.mvTransMv(calpha, *cplxcloneRmvB, cMatB);
  ssout << cMatB << "\n";
  nbErrors+=checkValues(cMatB, rootname+"/cMatB.in", tol, errors, "rmvB.mvTransMv(calpha, *cplxcloneRmvB, cMatB);", check);
  ssout << "MvDot " << "\n";
  cmvB.mvDot(*cplxcloneRmvB, cdotProduct);
  ssout << cdotProduct << "\n";
  nbErrors+=checkValues(cdotProduct, rootname+"/cmvBdotProduct.in", tol, errors, "cmvB.mvDot(*cplxcloneRmvB, cdotProduct)", check);
  ssout << "MvNorm " << "\n";
  cmvB.mvNorm(norm);
  ssout << cnorm << "\n";
  nbErrors+=checkValues(cnorm, rootname+"/cmvBnorm.in", tol, errors, "cmvB.mvnorm(norm)", check);
  ssout << "SetBlock " << "\n";
  crmvC.setBlock(cmvB, idx);
  ssout << crmvC << "\n";
  ssout << "MvScale " << "\n";
  nbErrors+=checkValues(crmvC, rootname+"/crmvC_2.in", tol, errors, "crmvC.setBlock(cmvB, idx)", check);
  crmvC.mvScale(calpha);
  ssout << crmvC << "\n";
  ssout << "MvScale with a cscale vector" << "\n";
  crmvC.mvScale(cscale);
  ssout << crmvC << "\n";
  nbErrors+=checkValues(crmvC, rootname+"/crmvC_4.in", tol, errors, "cmvC.mvScale(cscale)", check);
  ssout << "Initialize a multi-vector" << "\n";
  crmvC.mvInit(Real(1.0));
  ssout << crmvC << "\n";
  ssout << *(crmvC[0]) << "\n";
  nbErrors+=checkValues(crmvC, rootname+"/crmvC_5.in", tol, errors, "crmvC.mvScale(scale)", check);
  nbErrors+=checkValues(*(crmvC[0]), rootname+"/crmvC0.in", tol, errors, "*(crmvC[0])", check);
  CplxMV crmvD(5,5);
  crmvD.mvLoadFromFile(cMatrixDataSym.c_str());
  ssout << "Load from file" << "\n";
  ssout << crmvD << "\n";
  nbErrors+=checkValues(crmvD, rootname+"/crmvD.in", tol, errors, "crmvD.mvLoadFromFile(...)", check);


  ssout << "--------------------LargeMatrixAdapter---------------------" << "\n";
  const std::string cLMtrixData(inputsPathTo(rootname+"/c20.data"));
  LargeMatrix<Complex> cLargeMatrix(cLMtrixData, _dense, vecLength, vecLength, _cs, _row);
  CplxOP cLMat(&cLargeMatrix);
  ssout << "Apply Op: Ax = y " << "\n";
  cLMat.apply(cmvB,cmvA);
  ssout << cmvA << "\n";
  nbErrors+=checkValues(cmvA, rootname+"/cmvALM.in", tol, errors, "cLMat.apply(cmvB,cmvA)", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
