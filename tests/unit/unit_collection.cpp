/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Collection.cpp
	\author E. Lunéville
	\since 18 dec 2017
	\date 18 dec 2017

	Low level tests of Collection and PCollection classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include<cstdio>

using namespace xlifepp;

namespace unit_collection {


void unit_collection(int argc, char* argv[], bool check)
{
  String rootname = "unit_collection";
  trace_p->push(rootname);
  std::stringstream ssout;                  
  std::stringstream ssref;                   
  String errors;
  Number nbErrors = 0;
  
  bool W = false;

  //--------------------------------------------------------------------------------------------------------------------------
  // test Strings~Collection<String>
  //--------------------------------------------------------------------------------------------------------------------------

  Strings ss0; theCout << "ss0 :" << eol << ss0 << eol << eol;
  Strings ss1("s1"); theCout << "ss1 :" << eol << ss1 << eol << eol;
  Strings ss2("s1","s2"); theCout << "ss2 :" << eol << ss2 << eol << eol;
  Strings ss3(3,"s3"); theCout << "ss3 :" << eol << ss3 << eol << eol;
  Strings ss4;
  for (Number i=0;i<4;i++) ss4<<"s"+tostring(i+1);
  theCout << "ss4 :" << eol << ss4 << eol << eol;
  Strings ss5(5);
  for (Number i=1;i<=5;i++) ss5(i)="s"+tostring(i);
  theCout << "ss5 :" << eol << ss5 << eol << eol;
  theCout << "join(ss5,',') : " << join(ss5,",") << eol;
  nbErrors+=checkValue(theCout, rootname+"/Strings.in", errors, "Test Strings: collection", check);

  #if __cplusplus >= 201103L
    Strings ssi={"si1","si2","si3"};
    theCout << "C++11 initializer list :" << eol << ssi << eol;
    nbErrors+=checkValue(theCout, rootname+"/Strings11.in", errors, "Test Strings: collection<Strings> C++11", check);
  #endif

  //--------------------------------------------------------------------------------------------------------------------------
  // test Numbers~Collection<Number>
  //--------------------------------------------------------------------------------------------------------------------------

  Numbers ns0;  theCout << "ns0 :" << eol << ns0 << eol << eol;
  Numbers ns1(1); theCout << "ns1 :" << eol << ns1 << eol << eol;
  Numbers ns2(1,2);  theCout << "ns2 :" << eol << ns2 << eol << eol;
  Numbers ns3(1,2,3); theCout << "ns3 :" << eol << ns3 << eol << eol;
  Numbers ns4;
  for (Number i=0;i<4;i++) ns4<<(i+1);
  theCout << "ns4 :" << eol << ns4 << eol << eol;
  Numbers ns5; ns5.resize(5);
  for (Number i=1;i<=5;i++) ns5(i)=i;
  theCout << "ns5 :" << eol << ns5 << eol;
  nbErrors+=checkValue(theCout, rootname+"/Numbers.in", errors, "Test  Numbers ~ Collection<Number>", check);

  #if __cplusplus >= 201103L
    Numbers nsi={1,2,3};
    theCout << "C++11 initializer list :" << eol << nsi << eol;
    nbErrors+=checkValue(theCout, rootname+"/Numbers11.in", errors, "Test  Numbers ~ Collection<Number> C++11", check);
  #endif

  //--------------------------------------------------------------------------------------------------------------------------
  // test Ints~Collection<Int>
  //--------------------------------------------------------------------------------------------------------------------------

  Ints is0; theCout << "is0 :" << eol << is0 << eol << eol;
  Ints is1(1);  theCout << "is1 :" << eol << is1 << eol << eol;
  Ints is2(1,2);  theCout << "is2 :" << eol << is2 << eol << eol;
  Ints is3(1,2,3);  theCout << "is3 :" << eol << is3 << eol << eol;
  Ints is4;
  for (Int i=0;i<4;i++) is4 << (i+1);
  theCout << "is4 :" << eol << is4 << eol << eol;
  Ints is5; is5.resize(5);
  for (Int i=1;i<=5;i++) is5(i)=i;
  theCout << "is5 :" << eol << is5 << eol;
  nbErrors+=checkValue(theCout, rootname+"/Ints.in", errors, "Test  Ints ~ Collection<Int>", check);

  #if __cplusplus >= 201103L
    Ints isi={1,2,3};
    theCout << "C++11 initializer list :" << eol << isi << eol;
    nbErrors+=checkValue(theCout, rootname+"/Ints11.in", errors, "Test  Ints ~ Collection<Int> C++11", check);
  #endif

  //--------------------------------------------------------------------------------------------------------------------------
  // test Domains~PCollection<GeomDomain>
  //--------------------------------------------------------------------------------------------------------------------------

  Strings sn("Gamma1","Gamma2","Gamma3","Gamma4");
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=0.5,_ymin=0,_ymax=1,_nnodes=Numbers(3,6),_domain_name="Omega",_side_names=sn), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega"),
         gamma1=mesh2d.domain("Gamma1"), gamma2=mesh2d.domain("Gamma2"),
         gamma3=mesh2d.domain("Gamma3"), gamma4=mesh2d.domain("Gamma4");
  Domains ds0; ssout << "ds0 :" << eol << ds0 << eol << eol;
  Domains ds1(omega); ssout << "ds1 :" << eol << ds1 << eol << eol;
  Domains ds2(gamma1,gamma2); ssout << "ds2 :" << eol << ds2 << eol << eol;
  Domains ds3(gamma1,gamma2,gamma3); ssout << "ds3 :" << eol << ds3 << eol << eol;
  Domains ds4;
  for (Number i=0;i<4;i++) ds4 << mesh2d.domain(i+1);
  theCout << "ds4 :" << eol << ds4 << eol << eol;
  Domains ds5(5);
  for (Number i=1;i<=5;i++) ds5(i)=mesh2d.domain(i-1);
    theCout << "ds5 :" << eol << ds5 << eol;
  ds3[0]->setMaterialId(10);
  nbErrors+=checkValue(theCout, rootname+"/Domains.in", errors, "Test Domains ~ Collection<Domain> ", check);

  #if __cplusplus >= 201103L
    Domains dsi={gamma1,gamma2,gamma3};
    theCout << "C++11 initializer list :" << eol << dsi << eol;
    nbErrors+=checkValue(theCout, rootname+"/Domains11.in", errors, "Test  Domains ~ Collection<Domain> C++11", check);
  #endif

  //--------------------------------------------------------------------------------------------------------------------------
  // test Spaces~PCollection<Spaces>
  //--------------------------------------------------------------------------------------------------------------------------

  Space V1(_domain=omega, _interpolation=_P1, _name="V1"),
        V2(_domain=omega, _interpolation=_P2, _name="V2"),
        V3(_domain=omega, _interpolation=_P3, _name="V3");
  Spaces Vs0;  ssout << "Vs0 :" << eol << Vs0 << eol << eol;
  Spaces Vs1(V1);  ssout << "Vs1 :" << eol << Vs1 << eol << eol;
  Spaces Vs2(V1,V2);  ssout << "Vs2 :" << eol << Vs2 << eol << eol;
  Spaces Vs3(V1,V2,V3);  ssout << "Vs3 :" << eol << Vs3 << eol << eol;
  Spaces Vs4;
  Vs4 << V1 << V2 << V3 << V1;
  theCout << "Vs4 :" << eol << Vs4 << eol << eol;
  Spaces Vs5(5);
  for(Number i=1;i<=5;i++)
    Vs5(i)=Space(_domain=omega, _FE_type=Lagrange, _FE_subtype=standard, _order=i, _Sobolev_type=H1, _name="V_"+tostring(i));
  theCout << "Vs5 :" << eol << Vs5 << eol << eol;

  Spaces Ws1=spaces(ds3,interpolation(Lagrange,_standard,1,H1));
  theCout << "test spaces(Domains,Interpolation) :" << eol << Ws1 << eol << eol;
  Interpolations ints(3);
  for(Number i=1;i<=3;i++) ints(i)=interpolation(Lagrange,_standard,i,H1);
  Spaces Ws2=spaces(omega,ints);
  theCout << "test spaces(Domain,Interpolations) :" << eol << Ws2 << eol << eol;
  Spaces Ws3=spaces(ds3,ints);
  theCout << "test spaces(Domains,Interpolations) :" << eol << Ws3 << eol;
  nbErrors+=checkValue(theCout, rootname+"/Spaces.in", errors, "Test  Spaces ~ Collection<Space>", check);

  #if __cplusplus >= 201103L
    Spaces Vsi={V1,V2,V3};
    theCout << "C++11 initializer list :" << eol << Vsi << eol;
    nbErrors += checkValue(theCout, rootname+"/Spaces11.in" , errors, "Test Spaces ~ Collection<Space> C++11", check);
  #endif

  //--------------------------------------------------------------------------------------------------------------------------
  // test Unknowns~PCollection<Unknown>
  //--------------------------------------------------------------------------------------------------------------------------

  Unknown u1(V1, _name="u1"), u2(V2, _name="u2"), u3(V3, _name="u3");
  Unknowns us1(u1,u2,u3); ssout << "us1 :" << eol << us1 << eol << eol;
  Unknowns us2;
  us2 << u1 << u2 << u3;
  theCout << "us2 :" << eol << us2 << eol << eol;
  Unknowns us5(5);
  for (Number i=1;i<=5;i++) { us5(i)=Unknown(Vs5(i), _name="u_"+tostring(i)); }
  theCout << "us5 :" << eol << us5 << eol << eol;

  TestFunctions ts5=dualOf(us5);
  theCout << "ts5 :" << eol << ts5 << eol;

  nbErrors += checkValue(theCout, rootname+"/Unknowns.in" , errors, "Test: Unknowns ~ Collection<Unknown>", check);

  #if __cplusplus >= 201103L
    Unknowns usi={u1,u2,u3};
    theCout << "C++11 initializer list :" << eol << usi << eol;
    nbErrors += checkValue(theCout, rootname+"/Unknowns11.in" , errors, "Test: Unknowns ~ Collection<Unknown> C++11", check);
  #endif

  //--------------------------------------------------------------------------------------------------------------------------
  // test Reals
  //--------------------------------------------------------------------------------------------------------------------------

  Number sizen=10;
  int sizei=10;
  Reals rn(sizen), ri(sizei), rci(10);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
