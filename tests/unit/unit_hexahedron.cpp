/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_hexahedron.cpp
	\author N. Kielbasiewicz
	\since 5 nov 2012
	\date 18 dec 2012

	Low level tests of refHexahedron class.
 */

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_hexahedron {

void unit_hexahedron(int argc, char* argv[], bool check)
{
  String rootname = "unit_hexahedron";
  trace_p->push(rootname);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //! test GeomRefHexahedron
  verboseLevel(12);
  theCout<<"----------------------------- GeomRefHexahedron test ----------------------------------"<<eol;
  GeomRefHexahedron grt;
  theCout<<grt;
  for(Number i=0;i<grt.nbSides();++i)
  {
      Number v1=grt.sideVertexNumbers()[i][0], v2=grt.sideVertexNumbers()[i][1], v3=grt.sideVertexNumbers()[i][2];
      theCout<<"sideWithVertices("<<v1<<","<<v2<<","<<v3<<") = "<<grt.sideWithVertices(v1,v2,v3)<<eol;
  }
  for(Number i=0;i<grt.nbSideOfSides();++i)
  {
      Number v1=grt.sideOfSideVertexNumbers()[i][0], v2=grt.sideOfSideVertexNumbers()[i][1];
      theCout<<"sideWithVertices("<<v1<<","<<v2<<") = "<<grt.sideWithVertices(v1,v2)<<eol;
  }
  nbErrors += checkValue(theCout, rootname+"/GeomRefHexahedron.in" , errors, "Test Geom Ref Hexahedron",check);

  //! test interpolation
  verboseLevel(9);
  theCout<<eol<<"----------------------------- interpolation test ----------------------------------"<<eol;
  Interpolation* q1 = findInterpolation(_Lagrange, _standard, 1, H1);
  Interpolation* q2 = findInterpolation(_Lagrange, _standard, 2, H1);
  Interpolation* q3 = findInterpolation(_Lagrange, _standard, 3, H1);
  Interpolation* q4 = findInterpolation(_Lagrange, _standard, 4, H1);
  Interpolation* q5 = findInterpolation(_Lagrange, _standard, 5, H1);
  RefElement* hexahedronQ1 = findRefElement(_hexahedron, q1);
  RefElement* hexahedronQ2 = findRefElement(_hexahedron, q2);
  RefElement* hexahedronQ3 = findRefElement(_hexahedron, q3);
  RefElement* hexahedronQ4 = findRefElement(_hexahedron, q4);
  RefElement* hexahedronQ5 = findRefElement(_hexahedron, q5);
  theCout << *(hexahedronQ1) << std::endl;
  theCout << *(hexahedronQ2) << std::endl;
  theCout << *(hexahedronQ3) << std::endl;
  theCout << *(hexahedronQ4) << std::endl;
  theCout << *(hexahedronQ5) << std::endl;
  nbErrors += checkValue(theCout, rootname+"/Interpolation.in" , errors, "Test Interpolation",check);

  //! test splitting
  theCout << "split Fast Q1 to P1 : " << hexahedronQ1->splitP1() << std::endl;
  theCout << "split Fast Q2 to P1 : " << hexahedronQ2->splitP1() << std::endl;
  theCout << "split Fast Q3 to P1 : " << hexahedronQ3->splitP1() << std::endl;
  theCout << "split Gen Q1 to P1 : " << hexahedronQ1->splitLagrange3DToP1() << std::endl;
  theCout << "split Gen Q2 to P1 : " << hexahedronQ2->splitLagrange3DToP1() << std::endl;
  theCout << "split Gen Q3 to P1 : " << hexahedronQ3->splitLagrange3DToP1() << std::endl;
  theCout << "split Gen Q4 to P1 : " << hexahedronQ4->splitLagrange3DToP1() << std::endl;
  theCout << "split Gen Q5 to P1 : " << hexahedronQ5->splitLagrange3DToP1() << std::endl;
  theCout << "split Fast Q1 to Q1 : " << hexahedronQ1->splitO1() << std::endl;
  theCout << "split Fast Q2 to Q1 : " << hexahedronQ2->splitO1() << std::endl;
  theCout << "split Fast Q3 to Q1 : " << hexahedronQ3->splitO1() << std::endl;
  theCout << "split Gen Q1 to Q1 : " << hexahedronQ1->splitLagrange3DToQ1() << std::endl;
  theCout << "split Gen Q2 to Q1 : " << hexahedronQ2->splitLagrange3DToQ1() << std::endl;
  theCout << "split Gen Q3 to Q1 : " << hexahedronQ3->splitLagrange3DToQ1() << std::endl;
  theCout << "split Gen Q4 to Q1 : " << hexahedronQ4->splitLagrange3DToQ1() << std::endl;
  theCout << "split Gen Q5 to Q1 : " << hexahedronQ5->splitLagrange3DToQ1() << std::endl;
  nbErrors += checkValue(theCout, rootname+"/splitting.in" , errors, "Test splitting",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
