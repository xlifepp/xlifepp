/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Fourier; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file unit_ArgyrisTriangle.cpp
\author E. Lunéville
\since 3 may 2021
\date 3 may 2021

    check ArgyrisTriangle element functions and solve bilaplacian problem
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_ArgyrisTriangle
{
Real kex=1., p=1., q=1.;

Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), kp=kex*pi_;
  Real r=sin(kp*x)*sin(kp*y);
  return r*r;
}

Vector<Real> guex(const Point& P, Parameters& pa = defaultParameters)
 {
  Real x=P(1), y=P(2), kp=kex*pi_;
  Real r=sin(kp*x)*sin(kp*y);
  Vector<Real> g(2,0.);
  g[0]=kp*cos(kp*x)*sin(kp*y); g[1]=kp*sin(kp*x)*cos(kp*y);
  return g;
 }

Vector<Real> g2uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), kp=kex*pi_;
  Real r=sin(kp*x)*sin(kp*y);
  Vector<Real> g2(3,0.);
  g2[0]=-kp*kp*sin(kp*x)*sin(kp*y); g2[1]=-kp*kp*sin(kp*x)*sin(kp*y);g2[2]=kp*kp*cos(kp*x)*cos(kp*y);
  return g2;
  }

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real k=1., dkp=2*k*pi_;
  Real cx=cos(dkp*x), cy=cos(dkp*y);
  return 0.25*dkp*dkp*dkp*dkp*(4*cx*cy-cx-cy);
}

Real uex2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real r=sin(p*pi_*x)*sin(q*pi_*y);
  return r*r*r*r;
}

Vector<Real> guex2(const Point& P, Parameters& pa = defaultParameters)
 {
  Real x=P(1), y=P(2);
  Real pp=p*pi_, qp=q*pi_, sp=sin(pp*x), sq=sin(qp*y);
  Vector<Real> g(2,0.);
  g[0]=pp*sq*sq*sq*sq*(2*sin(2*pp*x)-sin(4*pp*x));
  g[1]=qp*sp*sp*sp*sp*(2*sin(2*qp*y)-sin(4*qp*y));
  return g;
 }

Vector<Real> g2uex2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real pp=p*pi_, qp=q*pi_, sp=sin(pp*x), sq=sin(qp*y);
  Vector<Real> g2(3,0.);
  g2[0]=4*pp*pp*sq*sq*sq*sq*(cos(2*pp*x)-cos(4*pp*x));
  g2[1]=4*qp*qp*sp*sp*sp*sp*(cos(2*qp*y)-cos(4*qp*y));
  g2[2]=pp*qp*(2*sin(2*pp*x)-sin(4*pp*x))*(2*sin(2*qp*y)-sin(4*qp*y));
  return g2;
  }

Real f2(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real pp=p*pi_, qp=q*pi_, ppqp=pp*qp, sp=sin(pp*x), sq=sin(qp*y), ppsq=pp*sq, qpsp=qp*sp,
       cp2=cos(2*pp*x), cp4=cos(4*pp*x),cq2=cos(2*qp*y), cq4=cos(4*qp*y);
  Real f2=2*ppqp*ppqp*(cp2-cp4)*(cq2-cq4)+ppsq*ppsq*ppsq*ppsq*(4*cp4-cp2)+qpsp*qpsp*qpsp*qpsp*(4*cq4-cq2);
  return 16*f2+100*uex2(P);
}

Real f1(const Point& P, Parameters& pa = defaultParameters)
{return 1.;}
Vector<Real> g1(const Point& P, Parameters& pa = defaultParameters)
{return Vector<Real>(2,0.);}
Vector<Real> g2(const Point& P, Parameters& pa = defaultParameters)
{return Vector<Real>(3,0.);}

Real fx(const Point& P, Parameters& pa = defaultParameters)
{return P(1);}
Vector<Real> gx(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(1)=1.;return g;}
Vector<Real> g2x(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(3,0.);return g;}

Real fx2(const Point& P, Parameters& pa = defaultParameters)
{return P(1)*P(1);}
Vector<Real> gx2(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(1)=2*P(1);return g;}
Vector<Real> g2x2(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(3,0.);g(1)=2;return g;}

Real fy(const Point& P, Parameters& pa = defaultParameters)
{return P(2);}
Vector<Real> gy(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(2)=1.;return g;}
Vector<Real> g2y(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(3,0.);return g;}

Real fy2(const Point& P, Parameters& pa = defaultParameters)
{return P(2)*P(2);}
Vector<Real> gy2(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(2)=2*P(2);return g;}
Vector<Real> g2y2(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(3,0.);g(2)=2;return g;}

Real fxy(const Point& P, Parameters& pa = defaultParameters)
{return P(1)*P(2);}
Vector<Real> gxy(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(2,0.);g(1)=P(2); g(2)=P(1);return g;}
Vector<Real> g2xy(const Point& P, Parameters& pa = defaultParameters)
{Vector<Real> g(3,0.);g(3)=1;return g;}


void unit_ArgyrisTriangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_ArgyrisTriangle";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(50);
  //numberOfThreads(1);

  String errors;
  Number nbErrors = 0;
  Real eps=0.005;

  Point x(0.,0.); x[0]=1.;
  theCout<<"\n\n================  Argyris standard triangle, (ARGT) ======================"<<eol;
  ArgyrisTriangle ARGT;
  ARGT.print(theCout);
  ShapeValues shv = ShapeValues(ARGT,true,true);
  ARGT.computeShapeValues(x.begin(),shv);
  ARGT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.;x[1]=1.;
  ARGT.computeShapeValues(x.begin(),shv);
  ARGT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.;x[1]=0.;
  ARGT.computeShapeValues(x.begin(),shv);
  ARGT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.;x[1]=0.5;
  ARGT.computeShapeValues(x.begin(),shv);
  ARGT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.5;x[1]=0.;
  ARGT.computeShapeValues(x.begin(),shv);
  ARGT.printShapeValues(theCout,x.begin(),shv);
  x[0]=0.5;x[1]=0.5;
  ARGT.computeShapeValues(x.begin(),shv);
  ARGT.printShapeValues(theCout,x.begin(),shv);
  theCout<<eol;
  // nbErrors += checkValue(theCout, rootname+"/NT1.in", errors, "NT1", check);

  Mesh mesh0(_triangle);
  Domain omega0=mesh0.domain("Omega");
  Space V0(_domain=omega0, _FE_type=Argyris, _order=1, _name="V0");
  theCout<<V0<<eol;
  Unknown u0(V0, _name="u0"); TestFunction v0(u0, _name="v0");
  std::cout << "**************************" << eol;
  std::cout << u0 << eol;
  std::cout << "**************************" << eol;
  std::cout << v0 << eol;
  std::cout << "**************************" << eol;
  TermMatrix M0(intg(omega0,u0*v0),"M0");
  theCout<<M0 << eol;
  TermMatrix K0(intg(omega0,grad(u0)|grad(v0)), _name="K0");
  theCout<<K0 << eol;
  TermMatrix L0(intg(omega0,dxx(u0)*dxx(v0)), _name="L0");
  theCout<<L0 << eol;
  Function Fx(fx), Gx(gx), G2x(g2x);
  TermVector u0x(u0,omega0,Fx,Gx,G2x);
  theCout<<"TermVector(u0,omega0,Fx,Gx,G2x) :"<<u0x;
  theCout<<"L0*u0x : "<<L0*u0x<<eol;
  theCout<<"(M0*ux|ux)="<<(M0*u0x|u0x)<<" (K0*u0x|u0x)="<<(K0*u0x|u0x)<<", ||L0*u0x||="<<norm2(L0*u0x)<<eol;
  {
    ShapeValues shw; shw.resize(21,2,true,true);
    const Element& elt=V0.element(Number(0));
    GeomMapData geom(elt.geomElt_p->meshElement());
    geom.computeJacobianMatrix(Point(0.,0.));
    geom.invertJacobianMatrix();

    const std::vector<Number>& dofnums=elt.dofNumbers;
    theCout<<"dofnums="<<dofnums<<eol;
    Real prec=1E-4;
    for(Number d=0;d<dofnums.size();d++)
    {
      const FeDof& dof=static_cast<const FeDof&>(V0.dof(dofnums[d]));
      const Vector<Real>& n=dof.projVector();
      Point x= dof.coords(), xr=geom.geomMapInverse(x);
      DiffOpType dif=dof.refDofP()->diffop();
      ARGT.computeShapeValues(xr.begin(),shv);
      String  sc =  format(tostring(round(Vector<Real>(x),prec)),17,_centerAlignment);
      String  scr=  format(tostring(round(Vector<Real>(xr),prec)),17,_centerAlignment);
      if (dif==_id) theCout << "  ref w   " << " at " << sc << " / " << scr << ": " << roundToZero(shv.w, 1E-10) << eol;
      if (dif==_dx) theCout << "  ref dxw " << " at " << sc << " / " << scr << ": " << roundToZero(shv.dw[0], 1E-10) << eol;
      if (dif==_dy) theCout << "  ref dyw " << " at " << sc << " / " << scr << ": " << roundToZero(shv.dw[1], 1E-10) << eol;
      if (dif==_dxx) theCout << "  ref dxxw" << " at " << sc << " / " << scr << ": " << roundToZero(shv.d2w[0], 1E-10) << eol;
      if (dif==_dyy) theCout << "  ref dyyw" << " at " << sc << " / " << scr << ": " << roundToZero(shv.d2w[1], 1E-10) << eol;
      if (dif==_dxy) theCout << "  ref dxyw" << " at " << sc << " / " << scr << ": " << roundToZero(shv.d2w[2], 1E-10) << eol;
      if (dif==_ndotgrad) theCout << "  ref dnw " << " at " << sc << " / " << scr << ": "<<roundToZero(n[0]*shv.dw[0]+n[1]*shv.dw[1], 1E-10) << "   n=" << n << eol;
      shw.Argyris2dMap(shv, geom,true,true);
      if (dif==_id) theCout << "  map w   " << " at " << sc << " / " << scr << ": " << roundToZero(shw.w, 1E-10) << eol;
      if (dif==_dx) theCout << "  map dxw " << " at " << sc << " / " << scr << ": " << roundToZero(shw.dw[0], 1E-10) << eol;
      if (dif==_dy) theCout << "  map dyw " << " at " << sc << " / " << scr << ": " << roundToZero(shw.dw[1], 1E-10) << eol;
      if (dif==_dxx) theCout << "  map dxxw" << " at " << sc << " / " << scr << ": " << roundToZero(shw.d2w[0], 1E-10) << eol;
      if (dif==_dyy) theCout << "  map dyyw" << " at " << sc << " / " << scr << ": " << roundToZero(shw.d2w[1], 1E-10) << eol;
      if (dif==_dxy) theCout << "  map dxyw" << " at " << sc << " / " << scr << ": " << roundToZero(shw.d2w[2], 1E-10) << eol;
      if (dif==_ndotgrad) theCout << "  map dnw " << " at " << sc << " / " << scr << ": " << roundToZero(n[0]*shw.dw[0]+n[1]*shw.dw[1], 1E-10) << "   n=" << n << eol;
    }
  }

  //check Argyris space
  Rectangle rect(_xmin=0,_xmax=1,_ymin=0,_ymax=1.,_hsteps=0.5,_domain_name="Omega",_side_names="Gamma");
  Mesh mesh(rect,_shape=_triangle, _order=1, _generator=gmsh);
  Domain omega=mesh.domain("Omega"), gamma=mesh.domain("Gamma");

  Space V(_domain=omega, _FE_type=Argyris, _order=1, _name="V");
  theCout << V << eol;

  //check affine transformation
  ShapeValues shw; shw.resize(21,2,true,true);
  for (Number i=0;i<std::min(Number(10),V.nbOfElements());i++)
  {
    theCout << "shape values on elt " << i+1 << ": " << eol;
    const Element& elt=V.element(i);
    GeomMapData geom(elt.geomElt_p->meshElement());
    geom.computeJacobianMatrix(Point(0.,0.));
    geom.invertJacobianMatrix();
    const std::vector<Number>& dofnums=elt.dofNumbers;
    Real prec=1E-4;
    for(Number d=0;d<dofnums.size();d++)
    {
      const FeDof& dof=static_cast<const FeDof&>(V.dof(dofnums[d]));
      const Vector<Real>& n=dof.projVector();
      Point x= dof.coords(), xr=geom.geomMapInverse(x);
      DiffOpType dif=dof.refDofP()->diffop();
      ARGT.computeShapeValues(xr.begin(),shv);
      shw.Argyris2dMap(shv, geom,true,true);
      String  sc =  format(tostring(round(Vector<Real>(x),prec)),17,_centerAlignment);
      String  scr=  format(tostring(round(Vector<Real>(xr),prec)),17,_centerAlignment);
      if (dif==_id) theCout << "  map w   " << " at " << sc << " / " << scr << ": " << roundToZero(shw.w, 1E-10) << eol;
      if (dif==_dx) theCout << "  map dxw " << " at " << sc << " / " << scr << ": " << roundToZero(shw.dw[0], 1E-10) << eol;
      if (dif==_dy) theCout << "  map dyw " << " at " << sc << " / " << scr << ": " << roundToZero(shw.dw[1], 1E-10) << eol;
      if (dif==_dxx) theCout << "  map dxxw" << " at " << sc << " / " << scr << ": " << roundToZero(shw.d2w[0], 1E-10) << eol;
      if (dif==_dyy) theCout << "  map dyyw" << " at " << sc << " / " << scr << ": " << roundToZero(shw.d2w[1], 1E-10) << eol;
      if (dif==_dxy) theCout << "  map dxyw" << " at " << sc << " / " << scr << ": " << roundToZero(shw.d2w[2], 1E-10) << eol;
      if (dif==_ndotgrad) theCout << "  map dnw " << " at " << sc << " / " << scr << ": " << roundToZero(n[0]*shw.dw[0]+n[1]*shw.dw[1], 1E-10) << "   n=" << n << eol;
    }
  }
  theCout << std::flush << eol;

//  // check FE matrix
//  Unknown u(V, _name="u"); TestFunction v(u,_name="v");
//  TermMatrix M(intg(omega,u*v),_name="M");
//  TermMatrix K(intg(omega,grad(u)|grad(v)),_name="K");
//  TermMatrix Lxx(intg(omega,dxx(u)*dxx(v)),_name="Lxx");
//  TermMatrix Lyy(intg(omega,dyy(u)*dyy(v)),_name="Lyy");
//  TermMatrix Lxy(intg(omega,dxy(u)*dxy(v)),_name="Lxy");
//  Real lmax=real(eigenSolve(M,_nev=2,_which="LM").value(1)), lmin=real(eigenSolve(M,_nev=2,_ncv=10,_sigma=0.).value(1));
//  theCout<<"M : lambda min = "<<lmin<<", lambda max = "<<lmax<<" cond = "<<lmax/lmin<<eol;
//  theCout<<K<<eol;
//  lmax=real(eigenSolve(K,_nev=2,_which="LM").value(1)), lmin=real(eigenSolve(K,_nev=2,_ncv=10,_sigma=0.).value(1));
//  theCout<<"K : lambda min = "<<lmin<<", lambda max = "<<lmax<<" cond = "<<lmax/lmin<<eol;
//  //check global matrices
//  Function F1(f1), G1(g1), G2(g2);
//  TermVector un(u,omega,F1,G1,G2);
//  theCout<<"(M*un|un)="<<(M*un|un)<<" ||K*un||="<<norm2(K*un)<<", ||Lxx*un||="<<norm2(Lxx*un)<<", ||Lyy*un||="<<norm2(Lyy*un)<<", ||Lxy*un||="<<norm2(Lxy*un)<<eol;
//  Function Fx(fx), Gx(gx), G2x(g2x);
//  TermVector ux(u,omega,Fx,Gx,G2x);
//  theCout<<"(M*ux|ux)="<<(M*ux|ux)<<" (K*ux|ux)="<<(K*ux|ux)<<", ||Lxx*ux||="<<norm2(Lxx*ux)<<", ||Lyy*ux||="<<norm2(Lyy*ux)<<", ||Lxy*ux||="<<norm2(Lxy*ux)<<eol;
//  Function Fx2(fx2), Gx2(gx2), G2x2(g2x2);
//  TermVector ux2(u,omega,Fx2,Gx2,G2x2);
//  theCout<<"(M*ux2|ux2)="<<(M*ux2|ux2)<<" (K*ux2|ux2)="<<(K*ux2|ux2)<<", (Lxx*ux2|ux2)="<<(Lxx*ux2|ux2)<<", ||Lyy*ux2||="<<norm2(Lyy*ux2)<<", ||Lxy*ux2||="<<norm2(Lxy*ux2)<<eol;
//  Function Fy(fy), Gy(gy), G2y(g2y);
//  TermVector uy(u,omega,Fy,Gy,G2y);
//  theCout<<"(M*uy|uy)="<<(M*uy|uy)<<" (K*uy|uy)="<<(K*uy|uy)<<", ||Lxx*uy||="<<norm2(Lxx*uy)<<", ||Lyy*uy||="<<norm2(Lyy*uy)<<", ||Lxy*uy||="<<norm2(Lxy*uy)<<eol;
//  Function Fy2(fy2), Gy2(gy2), G2y2(g2y2);
//  TermVector uy2(u,omega,Fy2,Gy2,G2y2);
//  theCout<<"(M*uy2|uy2)="<<(M*uy2|uy2)<<" (K*uy2|uy2)="<<(K*uy2|uy2)<<", ||Lxx*uy2||="<<norm2(Lxx*uy2)<<", (Lyy*uy2|uy2)="<<(Lyy*uy2|uy2)<<", ||Lxy*uy2||="<<norm2(Lxy*uy2)<<eol;
//  Function Fxy(fxy), Gxy(gxy), G2xy(g2xy);
//  TermVector uxy(u,omega,Fxy,Gxy,G2xy);
//  theCout<<"(M*uxy|uxy)="<<(M*uxy|uxy)<<" (K*uxy|uxy)="<<(K*uxy|uxy)<<", ||Lxx*uxy||="<<norm2(Lxx*uxy)<<", ||Lyy*uxy||="<<norm2(Lyy*uxy)<<", (Lxy*uxy|uxy)="<<(Lxy*uxy|uxy)<<eol;
//
//  //check bilaplacian problem delta^2 u=f on Omega, u=du/dn=0 on Gamma
//  EssentialConditions ecs= (u|gamma = 0) & (ndotgrad(u)|gamma = 0);
//  theCout<<ecs<<std::flush;
//  TermMatrix A(intg(omega,dxx(u)*dxx(v))+intg(omega,dyy(u)*dyy(v))+2*intg(omega,dxy(u)*dxy(v)), ecs, _name="A");
//  saveToFile("A.mat",A,_coo);
//  theCout<<"A lambda max = "<<eigenSolve(A,_nev=2,_which="LM").value(1)<<eol;
//  theCout<<"A lambda min = "<<eigenSolve(A,_nev=2,_ncv=10,_sigma=0.).value(1)<<eol;
//  //theCout<<A<<std::flush;
//  LinearForm b=intg(omega,f*v);
//  TermVector B(b, _name="B");
//  //theCout<<B<<std::flush;
//  TermVector U=directSolve(A,B);
//  theCout<<U<<std::flush;
//  Space W(omega,_P1,"W",false);
//  Unknown w(W, _name="W");
//  TermVector ui1=interpolate(w,omega,U,"ui1");
//  saveToFile("ui1", ui1,_format=_vtu, _aUniqueFile);
//  TermVector ue(w,omega,uex,"uex");
//  saveToFile("uex", ue,__format=vtu, _aUniqueFile);
//  TermVector up1=projection(U,W);
//  saveToFile("up1",up1,_format=_vtu, _aUniqueFile);
////
////  //check bilaplacian problem delta^2 u + u = f on Omega, delta u = dn delta u = 0 on Gamma (natural conditions)
//  Real nu=1;
//  BilinearForm a2=intg(omega,dxx(u)*dxx(v))+intg(omega,dyy(u)*dyy(v))+nu*intg(omega,dxx(u)*dyy(v))
//               +nu*intg(omega,dyy(u)*dxx(v))+2*(1-nu)*intg(omega,dxy(u)*dxy(v))+100.*intg(omega,u*v);
//  a2.symType()=_symmetric;
//  TermMatrix A2(a2, _name="A2");
//  TermVector B2(intg(omega,f2*v), _name="B2");
//  theCout<<A2<<B2<<eol;
//  theCout<<"lambda min = "<<eigenSolve(A2,_nev=2,_ncv=20,_sigma=0).value(1)<<" lambda max = "<<eigenSolve(A2,_nev=2,_ncv=20,_which="LM").value(1)<<eol;
//  TermVector U2=directSolve(A2,B2);
//  theCout<<"U2 : "<<U2<<eol;
//  TermVector Ue2=interpolent(u,omega,Function(uex2),Function(guex2),Function(g2uex2));
//  theCout<<"Uex interpolated :"<<Ue2<<eol;
//  TermVector e2=Ue2-U2;
//  theCout<<"error : "<<e2<<eol;
//  Real er2=sqrt(real((M*e2|e2)));
//  theCout<<"L2 error = "<<er2<<eol;
//  theCout<<"L2 relative error = "<<er2/sqrt(real((M*Ue2|Ue2)))<<eol;
//  saveToFile("uex2",TermVector(w,omega,uex2,"uex2"),_format=_vtu,_aUniqueFile);
//  saveToFile("u2",interpolate(w,omega,U2,"U2i"),_format=_vtu,_aUniqueFile);

//  // convergence error
//  std::vector<Real> hs={0.25,0.1,0.075,0.05,0.025};
//  //std::vector<Real> hs={0.5};
//  std::vector<Real> el2(hs.size(),0.), el2r(hs.size(),0.);
//  std::vector<Real> epl2(hs.size(),0.), epl2r(hs.size(),0.);
//  for(Number k=0;k<hs.size();k++)
//  {
//    Rectangle rectk(_xmin=0,_xmax=1,_ymin=0,_ymax=1.,_hsteps=hs[k],_domain_name="Omegak",_side_names="Gammak");
//    Mesh meshk(rectk,_shape=_triangle, _order=1, _generator=gmsh);
//    Domain omegak=meshk.domain("Omegak"), gammak=meshk.domain("Gammak");
//    Space Vk(omegak,_Argyris,1,"Vk");
//    Unknown uk(Vk, _name="uk"); TestFunction vk(uk, _name="vk");
//    EssentialConditions ecs= (uk|gammak = 0) & (ndotgrad(uk)|gammak = 0);
//    TermMatrix Ak(intg(omegak,dxx(uk)*dxx(vk))+intg(omegak,dyy(uk)*dyy(vk))+2*intg(omegak,dxy(uk)*dxy(vk)),
//                 (uk|gammak = 0) & (ndotgrad(uk)|gammak = 0), _name="Ak");
//    TermVector Bk(intg(omegak,f*vk), _name="B");
//    TermVector Uk=directSolve(Ak,Bk);
//    //errors
//    TermMatrix Mk(intg(omegak,uk*vk));
//    TermVector Bexk(intg(omegak,Function(uex)*vk));
//    TermVector Uexk=directSolve(Mk,Bexk,true);
//    theCout<<Uexk<<eol;
//       //TermVector Uek=interpolent(uk,omegak,Function(uex),Function(guex),Function(g2uex));
//    TermVector ek=Uexk-Uk;
//    el2[k]=sqrt(real((Mk*ek|ek)));
//    el2r[k]=8*el2[k]/3;
//    //check projection
//    epl2[k]=sqrt(abs(9./64.-real(Bexk|Uexk))); //|pu-u|^2=(pu,pu-u)-(u,pu-u)=|u|^2-(u,pu)
//    epl2r[k]=8.*epl2[k]/3;
//    //vtu export
//    Space Wk(omegak,P1,"Wk"); Unknown wk(Wk, _name="wk");
//    saveToFile("u"+tostring(k), interpolate(wk,omegak,Uk),_format=_vtu, _aUniqueFile);
//    saveToFile("ue"+tostring(k), interpolate(wk,omegak,Uexk),_format=_vtu, _aUniqueFile);
//  }
//  std::ofstream err("error_L2_argyris.dat");
//  theCout<<"L2 error versus h :"<<eol;
//  for(Number k=0;k<hs.size();k++)
//  {
//    err<<hs[k]<<" "<<el2[k]<<" "<<el2r[k]<<eol;
//    theCout<<"  "<<format(tostring(hs[k]),5,_leftAlignment)<<" -> "<<format(tostring(round(el2[k],1.E-7)),15,_leftAlignment)<<" ("<<round(100*el2r[k],1.E-5)<<"%)"<<eol;
//    err<<hs[k]<<" "<<epl2[k]<<" "<<epl2r[k]<<eol;
//    theCout<<"  "<<format(tostring(hs[k]),5,_leftAlignment)<<" -> "<<format(tostring(round(epl2[k],1.E-15)),15,_leftAlignment)<<" ("<<round(100*epl2r[k],1.E-10)<<"%)"<<eol;
//  }
//  err.close();

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
