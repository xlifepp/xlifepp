/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_TermVector_vector.cpp
	\author E. Lunéville
	\since 12 juin 2013
	\date 12 juin 2013

	Low level tests of TestVector class and related classes for vector unknowns

	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_TermVector_vector {

//! scalar 2D function
Real one(const Point& P, Parameters& pa = defaultParameters)
{return 1.;}

Real xy(const Point& P, Parameters& pa = defaultParameters)
{return P(1)*P(2);}

Real fx(const Point& P, Parameters& pa = defaultParameters)
{return P(1);}

Real fy(const Point& P, Parameters& pa = defaultParameters)
{return P(2);}

Vector<Real> fex(const Point& P, Parameters& pa = defaultParameters)
{return Vector<Real>(2,1.);}

Vector<Complex> fcex(const Point& P, Parameters& pa = defaultParameters)
{return Vector<Complex>(2,Complex(0.,1.));}

Matrix<Real> MatF(const Point& P, Parameters& pa = defaultParameters)
{
  Matrix<Real> res(2, 2);
  Real x = P.x();
  res(1, 1) = x;   res(1, 2) = 0.;
  res(2, 1) = 0.5; res(2, 2) = 2 * x;
  return res;
}

void unit_TermVector_vector(int argc, char* argv[], bool check)
{
  String rootname = "unit_TermVector_vector";
  trace_p->push(rootname);
  std::stringstream ssout;                  
  std::stringstream ssref;                   
  String errors;
  Number nbErrors = 0;
  ssout.precision(testPrec);
  std::cout.precision(testPrec);
  ssref.precision(testPrec);
  Real tol=0.0001;
  
  verboseLevel(2);
	//!create a mesh and Domains
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  Mesh mesh2d;
  if (!check)
  {
    mesh2d=Mesh(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=3,_side_names=sidenames),_shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
    saveToFile(inputsPathTo(rootname+"/mesh2d.msh"),mesh2d, _aUniqueFile);
  }
  else
  {
    mesh2d=Mesh(inputsPathTo(rootname+"/mesh2d.msh"), _name="P1 mesh of [0,1]x[0;1]");
  }
  Domain omega=mesh2d.domain("Omega");
  Domain gamma2=mesh2d.domain("Gamma_2");

  //!create Lagrange P1 space and unknown
  Space V1(_domain=omega, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u(V1, _name="u", _dim=_2D);  //2D unknown
  TestFunction v(u, _name="v");

  verboseLevel(10);
  //!create "nodal" vectors
  TermVector un(v,omega,1., _name="un"); //from constant real vector value
  ssout <<un;
  Vector<Complex> e1i(2,0.);e1i(1)=Complex(0.,1.);
  TermVector ue1i(v,omega,e1i,"ue1i");  //from constant complex vector value
  ue1i.saveToFile("ue1i.dat");
  ssout <<ue1i;
  nbErrors += checkValues(ue1i, rootname+"/ue1i.in", tol, errors, "Vec ue1i", check);
  TermVector uex(v,omega,fex, _name="uex");  //from real vector function fex
  ssout <<uex;
  TermVector ucex(v,omega,fcex, _name="ucex");  //from complex vector function fex
  ssout <<ucex;

  //!create  vectors from linear forms
  LinearForm lf=intg(omega,fex|v);
  TermVector Lex(lf, _name="Lex");
  ssout <<Lex;
  ssout <<"\n Lex|un="<<realPart(Lex|un)<<"\n\n";
  nbErrors += checkValue( realPart(Lex|un), 2., tol, errors, "Lex|un");

  TermVector Lex_1=Lex(v[1]);  //extract first component of unknown
  TermVector un_1=un(v[1]);
  ssout <<Lex_1;
  ssout <<"\n Lex_1|un_1="<<realPart(Lex_1|un_1)<<"\n\n";
  nbErrors += checkValue( realPart(Lex_1|un_1), 1., tol, errors, "Lex_1|un_1");

  LinearForm lfc=intg(omega,fcex|v);
  TermVector Lexc(lfc, _name="Lexc");
  ssout <<Lexc;
  ssout <<"\n Lexc|un="<<(Lexc|un)<<"\n\n";
  nbErrors += checkValue( (Lexc|un), Complex(0.,2.), tol, errors, "Lexc|un");

  LinearForm lfb1=intg(gamma2,fex|v);
  TermVector C1(lfb1, _name="C1");
  ssout <<C1<<eol;

  Matrix<Real> A(2,_idMatrix);
  LinearForm lfA=intg(omega,A*v |fex);
  TermVector LexA(lfA, _name="LexA");
  ssout <<LexA;
  ssout <<"\n LexA|un="<<realPart(LexA|un)<<"\n\n";
  nbErrors += checkValue( realPart(LexA|un), 2., tol, errors, "LexA|un");

  Matrix<Complex> Ac=Complex(0,1)*A;
  LinearForm lfAc=intg(omega,Ac*v |fex);
  TermVector LexAc(lfAc, _name="LexAc");
  ssout <<LexAc;
  ssout <<"\n LexAc|un="<<(LexAc|un)<<"\n\n";
  nbErrors += checkValue( (LexAc|un), Complex(0.,2.), tol, errors, "LexAc|un");

  LinearForm lfAn=intg(gamma2,(MatF*_n)|v);
  TermVector LexAn(lfAn, _name="LexAn");
  ssout <<LexAn;
  ssout <<"\n LexAn|un="<<realPart(LexAn|un)<<"\n\n";
  nbErrors += checkValue( realPart(LexAn|un), -0.5, tol, errors, "LexAn|un");


  //!create Lagrange P2 space and unknown
  Space V2(_domain=omega, _interpolation=_P2, _name="V2", _notOptimizeNumbering);
  Unknown u2(V2, _name="u2", _dim=_2D); TestFunction v2(u2, _name="v2");

  TermVector un2(v2,omega,1.,"un2"); //from constant real vector value
  ssout <<un2;

  //!create  vectors from linear forms
  LinearForm lf2=intg(omega,fex|v2);
  TermVector Lex2(lf2, _name="Lex2");
  ssout <<Lex2;
  ssout <<"\n Lex2|un2="<<realPart(Lex2|un2)<<"\n\n";
  nbErrors += checkValue( realPart(Lex2|un2), 2., tol, errors, "Lex2|un2");

  TermVector Lex_12=Lex2(v2[1]);  //extract first component of unknown
  TermVector un_12=un2(v2[1]);
  ssout <<Lex_12;
  ssout <<"\n Lex_12|un_12="<<realPart(Lex_12|un_12)<<"\n\n";
  nbErrors += checkValue( realPart(Lex_12|un_12), 1., tol, errors, "Lex_12|un_12");

  LinearForm lfc2=intg(omega,fcex|v2);
  TermVector Lexc2(lfc2, _name="Lexc2");
  ssout <<Lexc2;
  ssout <<"\n Lexc2|un2="<<(Lexc2|un2)<<"\n\n";
  nbErrors += checkValue( (Lexc2|un2), Complex(0.,2.), tol, errors, "Lexc2|un2");

  LinearForm lfb12=intg(gamma2,fex|v2);
  TermVector C12(lfb12, _name="C12");
  ssout <<C12<<eol;

  LinearForm lfA2=intg(omega,A*v2 |fex);
  TermVector LexA2(lfA2, _name="LexA2");
  ssout <<LexA2;
  ssout <<"\n LexA2|un2="<<realPart(LexA2|un2)<<"\n\n";
  nbErrors += checkValue( realPart(LexA2|un2), 2., tol, errors, "LexA2|unA2");

  LinearForm lfAc2=intg(omega,Ac*v2 |fex);
  TermVector LexAc2(lfAc2, _name="LexAc2");
  ssout <<LexAc2;
  ssout <<"\n LexAc2|un2="<<(LexAc2|un2)<<"\n\n";
  nbErrors += checkValue( (LexAc2|un2), Complex(0.,2.), tol, errors, "LexAc2|un2");

  ssout <<" on the fly  LexA2+LexAc2 = "<<LexA2+LexAc2;
  TermVector tt=LexA2+LexAc2;
  nbErrors += checkValues(tt, rootname+"/tt.in", tol, errors, "Vec tt", check);
  TermVector t=LexAc2+i_*LexAc2;
  nbErrors += checkValues(t,rootname+"/t.in", tol, errors, "Vec t", check);


  ssout <<"\n t = "<<t<<"\n";
  ssout <<"\n abs(t) = "<<abs(t)<<"\n";
  ssout <<"\n imag(t) = "<<imag(t)<<"\n";
  ssout <<"\n real(t) = "<<real(t)<<"\n";
  ssout <<"\n conj(t) = "<<conj(t)<<"\n";

  //!test non linear expression
  TermVector y=abs(t(v2[1]))*conj(t);
  ssout <<"\n y = "<<y<<"\n";
  nbErrors += checkValues(y, rootname+"/y.in", tol, errors, "Vec y", check);

  //!concatenate scalar TermVector to vector TermVector
  Unknown w(V1, _name="w");  //1D unknown
  TermVector wx(w,omega,fx,_name="wx"), wy(w,omega,fy,_name="wy"),wun(w,omega,1.,_name="wun"),wi(w,omega,i_,_name="wi");
  TermVector xyu(u,wx,wy,_name="xyu");
  ssout <<"\n xyu = "<<xyu<<"\n";
  nbErrors += checkValues(xyu, rootname+"/xyu.in", tol, errors, "Vec xyu", check);

  Unknown w3(V1, _name="w3", _dim=3);  //3D unknown
  TermVector xuny(w3,wx,wun,wy,_name="xuny");
  ssout <<"\n xuny = "<<xuny<<"\n";
  xuny.saveToFile("xuny.dat");
  nbErrors += checkValues(xuny, rootname+"/xuny.in", tol, errors, "Vec xuny", check);
  TermVector xyi(w3,wx,wy,wi,_name="xyi");
  ssout <<"\n xyi = "<<xyi<<"\n";
  nbErrors += checkValues(xyi, rootname+"/xyi.in", tol, errors, "Vec xyi", check);
  Unknown w4(V1, _name="w4", _dim=4);  //4D unknown
  TermVector xyiun(w4,wx,wy,wi,wun,_name="xyiun");
  ssout <<"\n xyiun = "<<xyiun<<"\n";
  nbErrors += checkValues(xyiun, rootname+"/xyiun.in", tol, errors, "Vec xyiun", check);
	
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
