/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_OpenCascade.cpp
  \authors E. Lunéville
  \since 23 apr 2020
  \date  23 apr 2020

  Low level tests of OpenCascade stuff.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=false)
  or compares results to those stored in the reference file (check=true)
  It returns reporting informations in a string
*/

//===============================================================================
//library dependences
#include "xlife++-libs.h"
#include "testUtils.hpp"
//===============================================================================
//stl dependences
#include <iostream>
#include <fstream>
#include <vector>
//===============================================================================

using namespace xlifepp;

namespace unit_OpenCascade {

#ifdef XLIFEPP_WITH_OPENCASCADE
void title(const String& t)
{
    theCout<<"======================================================================================"<<eol
           <<"                         OpenCascade "<<t<<eol
           <<"======================================================================================"<<eol;
}

void testMesh(const Geometry& geo, const String& name, ShapeType mshape, bool printOC, bool saveGeo, bool saveBrep, bool saveMsh, Real hmin=0., Real hmax=theRealMax)
{
  // if (printOC) geo.ocData().print(theCout);
  Mesh m(geo, _shape=mshape, _order=1, _generator=_gmshOC, _default_hstep={hmin,hmax});
  if (saveBrep) {remove((name+".brep").c_str());rename("xlifepp_script.brep",(name+".brep").c_str());}
  if (saveGeo)
  {
    remove((name+".geo").c_str());
    if (saveBrep)
    {
      String line;
      std::ifstream in("xlifepp_script.geo");
      std::getline(in,line);
      std::ofstream out((name+".geo").c_str());
      out<<"Merge \""<<name+".brep"<<"\";"<<eol;
      while(!in.eof())
      {
        getline(in,line);
        out<<line<<eol;
      }
      in.close();
      out.close();
      remove("xlifepp_script.geo");
    }
    else rename("xlifepp_script.geo",(name+".geo").c_str());
  }
  if (saveMsh) {remove((name+".msh").c_str());rename("xlifepp_script.msh",(name+".msh").c_str());}
}
#endif // XLIFEPP_WITH_OPENCASCADE

void unit_OpenCascade(int argc, char* argv[], bool check)
{
  String rootname = "unit_OpenCascade";
  trace_p->push(rootname);
  std::stringstream out;                  //string stream receiving results
  out.precision(testPrec);
  verboseLevel(6);
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  #ifndef XLIFEPP_WITH_OPENCASCADE
   theCout << message("test_report", rootname, 0, "");
   trace_p->pop();
  #else

  String sh;
  bool saveGeo=true, saveBrep=true, saveMsh=true, printOC=true;

  //-------------------------------------------------------------------------
  //                             1D Geometries
  //-------------------------------------------------------------------------
  sh="Segment";  title(sh);
  Segment Se(_v1=Point(0.,0.),_v2=Point(1.,2.),_nnodes=30,_domain_name=sh,_side_names=Strings("Gamma0","Gamma1"));
  testMesh(Se,sh,_segment,printOC,saveGeo,saveBrep,saveMsh);

  sh="CircularArc";  title(sh);
  CircArc Ca(_center=Point(0.,0.),_v1=Point(1.,0.),_v2=Point(0.,1.),_hsteps=Reals(0.1,0.05),_domain_name=sh);
  testMesh(Ca,sh,_segment,printOC,saveGeo,saveBrep,saveMsh);

  sh="EllipseArc";  title(sh);
  EllArc Ea(_center=Point(0.,0.,0.),_apogee=Point(2.,0.,0.),_v1=Point(2.,0.,0.),_v2=Point(0.,1.,0.),_hsteps=0.1,_domain_name=sh);
  testMesh(Ea,sh,_segment,printOC,saveGeo,saveBrep,saveMsh);

  sh="BSplineArcI";  title(sh);
  Number n=5;
  std::vector<Point> spts(n+1);
  Real x=0, dx=pi_/n;
  for(Number i=0;i<=n;i++, x+=dx) spts[i]=Point(x,sin(x));
  SplineArc spai(_splineType=_BSpline,_vertices=spts,_tangent0=Reals(1.,1.),_tangent1=Reals(1.,-1.), _domain_name="Gamma",_hsteps=0.1);
  testMesh(spai,sh,_segment,printOC,saveGeo,saveBrep,saveMsh);

  sh="BSplineArcA";  title(sh);
  BSpline bspa(_SplineApproximation, spts, 3, _uniformParametrization,_naturalBC);
  SplineArc spaa(_spline=bspa, _domain_name="Gamma",_hsteps=0.1);
  testMesh(spaa,sh,_segment,printOC,saveGeo,saveBrep,saveMsh);

  sh="ParametrizedArc";  title(sh);
  Real dpi=2.*pi_;
  Parametrization par(0,1,dpi*x_1,dpi*x_1*sin(dpi*x_1),Parameters(),"2pi.t,2pi.t.sin(2pi.t)"); //open
  ParametrizedArc parc(_parametrization = par, _partitioning=_splinePartition, _nbParts=10,_hsteps=0.1,_domain_name="Gamma");
  testMesh(parc,sh,_segment,printOC,saveGeo,saveBrep,saveMsh);

  //-------------------------------------------------------------------------
  //                             2D Geometries
  //-------------------------------------------------------------------------
  sh="Polygon";  title(sh);
  std::vector<Point> p(5);
  p[0]=Point(0.,0.);
  p[1]=Point(8.,0.);
  p[2]=Point(9.,4.);
  p[3]=Point(5.,2.);
  p[4]=Point(1.,4.);
  Polygon Po(_vertices=p , _nnodes=Numbers(15,10,8,8,10), _domain_name=sh,_side_names=Strings("Sigma1","Sigma2","Sigma3","Sigma4","Sigma5"));
  testMesh(Po,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Triangle";  title(sh);
  Triangle Tr(_v1=Point(0.,0.), _v2=Point(1.,0.), _v3=Point(0.,1.), _hsteps=0.1, _domain_name=sh, _side_names=Strings("Sigma","Gamma","Sigma"));
  testMesh(Tr,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Quadrangle";  title(sh);
  Quadrangle Qu(_v1=Point(0.,0.), _v2=Point(1.,0.), _v3=Point(1.,2.), _v4=Point(0.,1.), _hsteps=0.1, _domain_name=sh, _side_names=Strings("Sigma1","Gamma","Sigma2","Gamma"));
  testMesh(Qu,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Parallelogram";  title(sh);
  Parallelogram Pa(_v1=Point(0.,0.), _v2=Point(2.,0.), _v4=Point(1.,1.), _hsteps=0.1, _domain_name=sh, _side_names=Strings("Sigma1","Gamma","Sigma2","Gamma"));
  testMesh(Pa,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Rectangle"; title(sh);
  Rectangle Re(_origin=Point(0.,0.), _xlength=2., _ylength=1., _hsteps=0.1, _domain_name=sh, _side_names=Strings("Sigma1","Gamma","Sigma2","Gamma"));
  testMesh(Re,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Square"; title(sh);
  SquareGeo Sq(_origin=Point(0.,0.), _length=2., _hsteps=0.1, _domain_name=sh, _side_names=Strings("Sigma1","Gamma","Sigma2","Gamma"));
  testMesh(Sq,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipse";  title(sh);
  Ellipse El(_center=Point(0.,0.,0.), _v1=Point(1.,0.,0.), _v2=Point(0.,2.,0.), _hsteps=0.1, _domain_name=sh);
  testMesh(El,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipse4";  title(sh);
  Ellipse El4(_center=Point(0.,0.,0.), _v1=Point(1.,0.,0.), _v2=Point(0.,2.,0.), _hsteps=Reals(0.1,0.05,0.1,0.02), _domain_name=sh,_side_names=Strings("Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(El4,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="EllipseSector";  title(sh);
  Ellipse Els(_center=Point(0.,0.,0.), _v1=Point(1.,0.,0.), _v2=Point(0.,2.,0.), _angle1=90*deg_, _angle2=120*deg_, _hsteps=Reals(0.01,0.05,0.05), _domain_name=sh, _side_names=Strings("Gamma1","Sigma","Gamma2"));
  testMesh(Els,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Disk"; title(sh);
  Disk Di(_center=Point(1.,0.,0.), _radius=2, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Di,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="DiskSector"; title(sh);
  Disk Ds(_center=Point(1.,0.,0.), _radius=2, _angle1=90*deg_, _angle2=315*deg_, _hsteps=Reals(0.01,0.05,0.01,0.05), _domain_name=sh, _side_names=Strings("Gamma1","Sigma1","Sigma2","Gamma2"));
  testMesh(Ds,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="ParametrizedSurfaceLinear";
  Parametrization pars(0,2*pi_,0.,2*pi_, x_1, x_2,cos(x_1)*cos(x_2),Parameters(),"u,v,cos(u)cos(v)");
  ParametrizedSurface Psl(_parametrization = pars, _partitioning=_linearPartition, _nbParts=50,_hsteps=0.2,
                          _domain_name=sh,_side_names=Strings("Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(Psl,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="ParametrizedSurfaceSpline";
  ParametrizedSurface Pss(_parametrization = pars, _partitioning=_splinePartition, _nbParts=16,_hsteps=0.2,
                          _domain_name=sh,_side_names=Strings("Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(Pss,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  Number nbu=7;Real ds=pi_/(2*nbu);
  Real u=-pi_/2,v;
  std::vector<std::vector<Point> > pts2(2*nbu+1,std::vector<Point>(nbu+1));
  for (Number i=0;i<=2*nbu;i++,u+=ds)
  {
    v=0;
    for (Number j=0;j<=nbu;j++,v+=ds) pts2[i][j]=Point(cos(u)*cos(v),cos(u)*sin(v),sin(u));
  }

  sh="SplineSurfaceI";
  Nurbs nuI(pts2);  // interpolation nurbs
  SplineSurface spsI(_spline=nuI, _hsteps=0.2, _domain_name=sh,_side_names=Strings("Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(spsI,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);
  nbErrors += checkValues(sh+".msh", rootname+"/"+sh+".in", 1e-8, errors, "test SplineSurfaceI", check);

  sh="SplineSurfaceA";
  Nurbs nuA(_SplineApproximation, pts2); // approximation nurbs
  SplineSurface spsA(_spline=nuA, _hsteps=0.2, _domain_name=sh,_side_names=Strings("Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(spsA,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);
  nbErrors += checkValues(sh+".msh", rootname+"/"+sh+".in", 1e-8, errors, "test SplineSurfaceA", check);

  Number m= 2*nbu+1; n=nbu+1;
  std::vector<Point> vpts2(m*n);
  for (Number i=0;i<m;i++)
  {
    for (Number j=0;j<n;j++) vpts2[i*n+j]= pts2[i][j];
  }

  sh="SplineSurface"; // check direct construction (interpolation)
  SplineSurface sps(_vertices=vpts2, _nbu=m, _hsteps=0.2, _domain_name=sh,_side_names=Strings("Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(sps,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);
  nbErrors += checkValues(sh+".msh", rootname+"/"+sh+".in", 1e-8, errors, "test SplineSurface", check);

  //-------------------------------------------------------------------------
  //                             Canonical 3D Geometries
  //-------------------------------------------------------------------------
  sh="Cube"; title(sh);
  Cube Cu(_origin=Point(0.,0.,0.), _length=1, _hsteps=Reals(0.05,0.05,0.05,0.05,0.1,0.1,0.1,0.1),
          _domain_name=sh, _side_names=Strings("Sigma1","Sigma2","Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(Cu,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cuboid"; title(sh);
  Cuboid Cb(_origin=Point(0.,0.,0.), _xlength=2, _ylength=1, _zlength=0.5, _hsteps=Reals(0.05,0.05,0.05,0.05,0.1,0.1,0.1,0.1),
          _domain_name=sh, _side_names=Strings("Sigma1","Sigma2","Gamma1","Gamma2","Gamma3","Gamma4"));
  testMesh(Cb,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="FicheraCube"; title(sh);
  Cube Cuf(_origin=Point(0.,0.,0.), _length=1, _nboctants=7, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Cuf,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cube6octants"; title(sh);
  Cube Cu6(_origin=Point(0.,0.,0.), _length=1, _nboctants=6, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Cu6,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cube5octants"; title(sh);
  Cube Cu5(_origin=Point(0.,0.,0.), _length=1, _nboctants=5, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Cu5,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cube4octants"; title(sh);
  Cube Cu4(_origin=Point(0.,0.,0.), _length=1, _nboctants=4, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Cu4,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cube3octants"; title(sh);
  Cube Cu3(_origin=Point(0.,0.,0.), _length=1, _nboctants=3, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Cu3,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cube2octants"; title(sh);
  Cube Cu2(_origin=Point(0.,0.,0.), _length=1, _nboctants=2, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Cu2,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cube1octants"; title(sh);
  Cube Cu1(_origin=Point(0.,0.,0.), _length=1, _nboctants=1, _hsteps=0.1, _domain_name=sh,_side_names="Gamma");
  testMesh(Cu1,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Tetrahedron";
  Point a(1.,0.,0.),b(0.,1.,0.),c(0.,0.,1.),d(0.,0.,0.);
  Tetrahedron Tet (_v1=a, _v2=b, _v3=c, _v4=d, _nnodes=10, _domain_name=sh);
  testMesh(Tet,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Hexahedron";
  Point a1(0.,0.,0.),b1(4.,0.,0.),c1(4.,2.,0.),d1(0.,2.,0.);
  Point a2(-0.5,0.,0.5),b2(4.,0.,2.),c2(4.,2.,2.),d2(-0.5,2.,0.5);
  Hexahedron Hex(_v1=a1, _v2=b1, _v3=c1, _v4=d1, _v5=a2, _v6=b2, _v7=c2, _v8=d2, _nnodes=10, _domain_name=sh);
  testMesh(Hex,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Parallelepiped ";
  Point a3(0.,0.,0.),b3(4.,0.,0.),c3(0.,2.,0.), d3(2.,0.,1.);
  Parallelepiped Par(_v1=a3,_v2=b3,_v4=c3,_v5=d3,_hsteps=0.1,_domain_name=sh);
  testMesh(Par,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Parallelepiped7octants";
  Parallelepiped Par7(_v1=a3,_v2=b3,_v4=c3,_v5=d3,_nboctants=7,_hsteps=0.1,_domain_name=sh);
  testMesh(Par7,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Polyhedron"; title(sh);
  std::vector<Point> vp(5), vp2(4);
  vp[0]=Point(0.,0.,0.);
  vp[1]=Point(2.,0.,0.);
  vp[2]=Point(3.,1.,0.);
  vp[3]=Point(1.,4.,0.);
  vp[4]=Point(-1.,2.,0.);
  Polygon pg1(_vertices=vp,_domain_name="Gamma1");
  vp[0]=Point(0.,0.,1.);
  vp[1]=Point(2.,0.,1.);
  vp[2]=Point(3.,1.,1.);
  vp[3]=Point(1.,4.,1.);
  vp[4]=Point(-1.,2.,1.);
  Polygon pg2(_vertices=vp,_domain_name="Gamma2");
  vp2[0]=Point(0.,0.,0.);
  vp2[1]=Point(2.,0.,0.);
  vp2[2]=Point(2.,0.,1.);
  vp2[3]=Point(0.,0.,1.);
  Polygon pg3(_vertices=vp2,_domain_name="Gamma3");
  vp2[0]=Point(2.,0.,0.);
  vp2[1]=Point(3.,1.,0.);
  vp2[2]=Point(3.,1.,1.);
  vp2[3]=Point(2.,0.,1.);
  Polygon pg4(_vertices=vp2,_domain_name="Gamma4");
  vp2[0]=Point(3.,1.,0.);
  vp2[1]=Point(1.,4.,0.);
  vp2[2]=Point(1.,4.,1.);
  vp2[3]=Point(3.,1.,1.);
  Polygon pg5(_vertices=vp2,_domain_name="Gamma5");
  vp2[0]=Point(1.,4.,0.);
  vp2[1]=Point(-1.,2.,0.);
  vp2[2]=Point(-1.,2.,1.);
  vp2[3]=Point(1.,4.,1.);
  Polygon pg6(_vertices=vp2,_domain_name="Gamma6");
  vp2[0]=Point(-1.,2.,0.);
  vp2[1]=Point(0.,0.,0.);
  vp2[2]=Point(0.,0.,1.);
  vp2[3]=Point(-1.,2.,1.);
  Polygon pg7(_vertices=vp2,_domain_name="Gamma7");
  std::vector<Polygon>faces(7);
  faces[0]=pg1;faces[1]=pg2;faces[2]=pg3;faces[3]=pg4;faces[4]=pg5;faces[5]=pg6;faces[6]=pg7;
  Polyhedron Pol(_faces=faces,_domain_name=sh);
  testMesh(Pol,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_H";
  Ball BalH(_center=Point(0.,0.,0.),_radius=2.,_hsteps=0.5,_domain_name=sh);
  testMesh(BalH,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_N";
  Ball BalN(_center=Point(0.,0.,0.),_radius=2.,_nnodes=10,_domain_name=sh,_side_names="Gamma");
  testMesh(BalN,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant8_H";
  Ball Bal8H(_center=Point(0.,0.,0.),_radius=2.,_hsteps=0.5,
             _domain_name=sh,_side_names=Strings("S1","S2","S3","S4","S5","S6","S7","S8"));
  testMesh(Bal8H,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant8_N";
  Ball Bal8N(_center=Point(0.,0.,0.),_radius=2.,_nnodes=Numbers(20,20,20,20,10,10,10,10,30,30,30,30),
             _domain_name=sh,_side_names=Strings("S1","S2","S3","S4","S5","S6","S7","S8"));
  testMesh(Bal8N,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant1_H";
  Ball Bal1H(_center=Point(0.,0.,0.),_radius=2.,_nboctants=1,_hsteps=Reals(0.1,0.2,0.2,0.2),_domain_name=sh,_side_names=Strings("Sigma","Gamma_yz","Gamma_xz","Gamma_xy"));
  testMesh(Bal1H,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant1_N";
  Ball Bal1N(_center=Point(0.,0.,0.),_radius=2.,_nboctants=1,_nnodes=Numbers(10,10,10,20,20,20),_domain_name=sh,_side_names=Strings("Sigma","Gamma_yz","Gamma_xz","Gamma_xy"));
  testMesh(Bal1N,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant2";
  Ball Bal2(_center=Point(0.,0.,0.),_radius=2.,_nboctants=2,_hsteps=0.1,_domain_name=sh,_side_names=Strings("S1","S2","S3","S4","P5","P6"));
  testMesh(Bal2,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant3";
  Ball Bal3(_center=Point(0.,0.,0.),_radius=2.,_nboctants=3,_hsteps=0.1,_domain_name=sh,_side_names=Strings("S1","S2","S3","P4","P5","P6","P7","P8"));
  testMesh(Bal3,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant4";
  Ball Bal4(_center=Point(0.,0.,0.),_radius=2.,_nboctants=4,_hsteps=0.1,_domain_name=sh,_side_names=Strings("S1","S2","S3","S4","P5","P6","P7","P8"));
  testMesh(Bal4,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant5";
  Ball Bal5(_center=Point(0.,0.,0.),_radius=2.,_nboctants=5,_hsteps=0.1,_domain_name=sh,_side_names=Strings("S1","S2","S3","S4","S5","P6","P7","P8","P9","P10"));
  testMesh(Bal5,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant6";
  Ball Bal6(_center=Point(0.,0.,0.),_radius=2.,_nboctants=6,_hsteps=0.1,_domain_name=sh,_side_names=Strings("S1","S2","S3","S4","S5","S6","P7","P8","P9","P10"));
  testMesh(Bal6,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ball_octant7";
  Ball Bal7(_center=Point(0.,0.,0.),_radius=2.,_nboctants=7,_hsteps=0.1,_domain_name=sh,_side_names=Strings("S1","S2","S3","S4","S5","S6","S7","P8","P9","P10"));
  testMesh(Bal7,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid";
  Ellipsoid Ell(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid_octant1";
  Ellipsoid Ell1(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_nboctants=1,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell1,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid_octant2";
  Ellipsoid Ell2(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_nboctants=2,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell2,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid_octant3";
  Ellipsoid Ell3(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_nboctants=3,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell3,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid_octant4";
  Ellipsoid Ell4(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_nboctants=4,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell4,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid_octant5";
  Ellipsoid Ell5(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_nboctants=5,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell5,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid_octant6";
  Ellipsoid Ell6(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_nboctants=6,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell6,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Ellipsoid_octant7";
  Ellipsoid Ell7(_center=Point(0.,0.,0.),_xradius=2.,_yradius=1., _zradius=3.,_nboctants=7,_hsteps=0.1,_domain_name=sh);
  testMesh(Ell7,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Trunk_Triangle";
  Trunk TrT(_basis=Triangle(_v1=Point(0.,0.,0.),_v2=Point(3.,0.,0.),_v3=Point(0.,2.,0.)),_origin=Point(0.,1.,1.),_scale=0.5,_hsteps=0.1,_domain_name=sh);
  testMesh(TrT,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Trunk_Rectangle";
  Trunk TrR(_basis=Rectangle(_origin=Point(0.,0.,0.),_xlength=2,_ylength=1.),_origin=Point(0.,1.,1.),_scale=0.5,_hsteps=0.1,_domain_name=sh);
  testMesh(TrR, sh, _tetrahedron, printOC, saveGeo, saveBrep, saveMsh);

  sh="Trunk_Hexagone";
  std::vector<Point> vh(6);
  for (Number i=0; i<6; i++) vh[i]=Point(std::cos(i*pi_/3.),std::sin(i*pi_/3.),0.);

  Trunk TrH(_basis=Polygon(_vertices=vh), _scale=1., _origin=Point(0.,0.,3.), _hsteps=0.1, _domain_name=sh);
  testMesh(TrH, sh, _tetrahedron, printOC, saveGeo, saveBrep, saveMsh);

  sh="Trunk_Hexagone2";
  Trunk TrH2(_basis=Polygon(_vertices=vh), _scale=0.5, _origin=Point(1.,0.,3.), _hsteps=0.1, _domain_name=sh);
  testMesh(TrH2, sh, _tetrahedron, printOC, saveGeo, saveBrep, saveMsh);

  sh="Trunk_Disk";
  Trunk TrD(_center1=Point(0.,0.,0.),_v1=Point(1.,0.,0.),_v2=Point(0.,1.,0.),_scale=1,_center2=Point(0.,1.,3.), _hsteps=0.1,_domain_name=sh);
  testMesh(TrD,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Trunk_Disk2";
  Trunk TrD2(_center1=Point(0.,0.,0.),_v1=Point(1.,0.,0.),_v2=Point(0.,1.,0.),_scale=0.5,_center2=Point(0.,1.,3.), _hsteps=0.1,_domain_name=sh);
  testMesh(TrD2,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Trunk_Ellipse";
  Trunk TrE(_center1=Point(0.,0.,0.),_v1=Point(2.,0.,0.),_v2=Point(0.,1.,0.),_scale=1,_center2=Point(0.,1.,3.), _hsteps=0.1,_domain_name=sh);
  testMesh(TrE,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Trunk_Ellipse2";
  Trunk TrE2(_center1=Point(0.,0.,0.),_v1=Point(2.,0.,0.),_v2=Point(0.,1.,0.),_scale=0.5,_center2=Point(2.,0.,3.), _hsteps=0.1,_domain_name=sh);
  testMesh(TrE2,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cylinder";
  Cylinder Cyl(_center1=Point(0.,0.,0.),_v1=Point(2.,0.,0.),_v2=Point(0.,2.,0.),_center2=Point(1.,0.,3.),_hsteps=0.2,
               _domain_name=sh,_side_names=Strings("b1","b2","s1","s2","s3","s4"));
  testMesh(Cyl,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cylinder_quadrangle";
  Cylinder CylQ(_basis=Quadrangle(_v1=Point(0.,0.,0.),_v2=Point(1.,0.,0.),_v3=Point(2.,2.,0.),_v4= Point(1.,2.,0.)),_direction={0.,2.,1.},_nnodes=10,
                _domain_name=sh,_side_names=Strings("b1","b2","s1","s2","s3","s4"));
  testMesh(CylQ,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cone";
  Cone Con(_center1=Point(0.,0.,0.),_v1=Point(2.,0.,0.),_v2=Point(0.,2.,0.),_apex=Point(0.,0.,5.),_hsteps=0.1,
           _domain_name=sh, _side_names=Strings("b1","s1","s2","s3","s4"));
  testMesh(Con,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Pyramid";
  Pyramid Pyr(_basis=Polygon(_vertices=vh),_apex=Point(0.,0.,3.),_hsteps=0.1,
              _domain_name=sh, _side_names=Strings("b1","s1","s2","s3","s4","s5","s6"));
  testMesh(Pyr,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevCylinder";
  RevCylinder Rcyl(_center1=Point(0.,0.,0.),_center2=Point(3.,3.,0.),_radius=1,_hsteps=0.2,_domain_name=sh,_side_names=Strings("s1","s2","s3","s4","b1","b2"));
  testMesh(Rcyl,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevCylinder_Open";
  RevCylinder Rcylo(_center1=Point(0.,0.,0.),_center2=Point(3.,3.,0.),_radius=1,_end1_shape=_gesNone,_end2_shape=_gesNone, _hsteps=0.2,_domain_name=sh,
                    _side_names=Strings("s1","s2","s3","s4"));
  testMesh(Rcylo,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevCone";
  RevCone Rcon(_center=Point(0.,0.,0.),_apex=Point(0.,0.,2.),_radius=1,_domain_name=sh, _hsteps=0.2, _side_names=Strings("s1","s2","s3","s4","b1"));
  testMesh(Rcon,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevCone_Open";
  RevCone Rcono(_center=Point(0.,0.,0.),_apex=Point(0.,0.,2.),_radius=1,_end_shape=_gesNone, _domain_name=sh, _hsteps=0.2, _side_names=Strings("s1","s2","s3","s4"));
  testMesh(Rcono,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevCylinder_ConeSphere";
  RevCylinder RcylCS(_center1=Point(0.,0.,0.),_center2=Point(3.,3.,0.),_radius=1,
                     _end1_shape=_gesCone, _end1_distance=3, _end2_shape=_gesSphere,_end2_distance=2,_hsteps=0.2,_domain_name=sh,
                     _side_names=Strings("st1","st2","st3","st4","sc1","sc2","sc3","sc4","ss1","ss2","ss3","ss4"));
  testMesh(RcylCS,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevCone_Ellipsoid";
  RevCone RconE(_center=Point(0.,0.,0.),_apex=Point(0.,0.,2.),_radius=1,_end_shape=_gesEllipsoid, _end_distance=4,_hsteps=0.2,_domain_name=sh,
                _side_names=Strings("sc1","sc2","sc3","sc4","se1","se2","se3","se4"));
  testMesh(RconE,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevTrunk";
  RevTrunk Rtru(_center1=Point(0.,0.,0.),_center2=Point(0.,0.,3.),_radius1=1,_radius2=0.5,_hsteps=0.2,_domain_name=sh,
                _side_names=Strings("st1","st2","st3","st4","b1","b2"));
  testMesh(Rtru,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="RevTrunk_Sphere_Open";
  RevTrunk RtruSO(_center1=Point(0.,0.,0.),_center2=Point(0.,0.,3.),_radius1=1,_radius2=0.5,
                  _end1_shape=_gesSphere, _end1_distance=2, _end2_shape=_gesNone,_nnodes=10,_domain_name=sh,
                  _side_names=Strings("st1","st2","st3","st4","ss1","ss2","ss3","ss4"));
  testMesh(RtruSO,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  //-------------------------------------------------------------------------
  //                      Composite and loop geometries
  //-------------------------------------------------------------------------
  sh="Composite_SegArc";
  Segment S1(_v1=Point(0.,0.),_v2=Point(1.,0.),_hsteps=0.1,_domain_name="S1");
  Segment S2(_v1=Point(1.,1.),_v2=Point(0.,1.),_hsteps=0.1,_domain_name="S2");
  CircArc C1a(_center=Point(1.,0.5),_v1=Point(1.,0.),_v2=Point(1.5,0.5),_hsteps=0.1,_domain_name="C1a");
  CircArc C1b(_center=Point(1.,0.5),_v1=Point(1.5,0.5),_v2=Point(1,1),_hsteps=0.1,_domain_name="C1b");
  CircArc C2a(_center=Point(0.,0.5),_v1=Point(0.,1.),_v2=Point(-0.5,0.5),_hsteps=0.1,_domain_name="C2a");
  CircArc C2b(_center=Point(0.,0.5),_v1=Point(-0.5,0.5),_v2=Point(0,0.),_hsteps=0.1,_domain_name="C2b");
  Geometry ComSC=S1+C1a+C1b+S2+C2a+C2b;
  testMesh(ComSC,sh,_segment,printOC,saveGeo,saveBrep,saveMsh);

  sh="SurfaceFrom_SegArc";
  Geometry SurfSA=surfaceFrom(ComSC,sh);
  testMesh(SurfSA,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Composite_Triangles";
  Triangle Trxy(_v1=Point(0.,0.,0.), _v2=Point(1.,0.,0.),_v3=Point(0.,1.,0.),_hsteps=0.1,_domain_name="Trxy");
  Triangle Trxz(_v1=Point(0.,0.,0.), _v2=Point(1.,0.,0.),_v3=Point(0.,0.,1.),_hsteps=0.1,_domain_name="Trxz");
  Triangle Tryz(_v1=Point(0.,0.,0.), _v2=Point(0.,1.,0.),_v3=Point(0.,0.,1.),_hsteps=0.1,_domain_name="Tryz");
  Triangle Trxyz(_v1=Point(1.,0.,0.), _v2=Point(0.,1.,0.),_v3=Point(0.,0.,1.),_hsteps=0.1,_domain_name="Trxyz");
  Geometry Txyz=Trxy+Trxz+Tryz+Trxyz;
  testMesh(Txyz,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="VolumeFrom_Triangles";
  Geometry VolTrs=volumeFrom(Txyz,sh);
  testMesh(VolTrs,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Composite_squares_plus";
  Rectangle R1(_origin=Point(0.,0.),_xlength=1.,_ylength=1.,_hsteps=0.05,_domain_name="R1",_side_names=Strings("R1_1","R1_2","R1_3","R1_4"));
  Rectangle R2(_origin=Point(0.5,0.5),_xlength=1.,_ylength=1.,_hsteps=0.1,_domain_name="R2",_side_names=Strings("R2_1","R2_2","R2_3","R2_4"));
  Geometry R1pR2=R1+R2;
  testMesh(R1pR2,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Composite_squares_minus";
  Geometry R1mR2=R1-R2;
  testMesh(R1mR2,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Rect-Disk1";
  Rectangle ReC(_origin=Point(0.,0.), _xlength=1., _ylength=1., _hsteps=0.1, _domain_name="ReC",_side_names="Gamma");
  Disk DiC(_center=Point(0.5,0.5), _radius=0.25, _hsteps=0.1, _domain_name="DiC",_side_names="Sigma");
  Geometry ReCMinusDiC = ReC-DiC;
  testMesh(ReCMinusDiC ,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Rect-Disk2";
  Disk DiC2(_center=Point(1,0.5), _radius=0.25, _hsteps=0.1, _domain_name="DiC",_side_names="Sigma2");
  Geometry ReCMinusDiC2 = ReC-DiC2;
  testMesh(ReCMinusDiC2 ,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Rect+Disk2";
  Geometry ReCPlusDiC2 = ReC+(+DiC2);
  testMesh(ReCPlusDiC2 ,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cylinder-Sphere";
  RevCylinder CylC(_center1=Point(0.,0.,-2),_center2=Point(0.,0.,2.),_radius=1,_hsteps=0.1,_domain_name="cylinder",_side_names="Gamma");
  Sphere SphC(_center=Point(1.,0,0.),_radius=0.5,_hsteps=0.1,_domain_name="sphere1",_side_names="Sigma1");
  Geometry CylCMinusSphC = CylC-SphC;
  CylCMinusSphC.domName(sh);
  testMesh(CylCMinusSphC ,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  Real an=45, xc=1.05*cos(an*pi_/180), yc=1.05*sin(an*pi_/180);  //NOT WORKING IF SPHERE2 LOCATED ON CYLINDER BOUNDARY (OC BUG!!!)
  sh="Cylinder-Sphere2";
  Sphere SphC2(_center=Point(xc,yc,0.),_radius=0.5,_hsteps=0.1,_domain_name="sphere2",_side_names="Sigma2");
  Geometry CylCMinusSphC2 = CylC-SphC2;
  CylCMinusSphC2.domName(sh);
  testMesh(CylCMinusSphC2 ,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cylinder+Sphere2";
  Geometry CylCPlusSphC2 = CylC+SphC2;
  CylCPlusSphC2.domName(sh);
  testMesh(CylCPlusSphC2 ,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cylinder+Sphere2-Sphere1";
  Geometry CylPlusSphCMinusSphC2 = (CylC+SphC2)-SphC;
  theCout<<CylPlusSphCMinusSphC2.geoNode_->asString()<<eol;
  GeoNode gen=expand(*CylCMinusSphC2.geoNode_);
  theCout<<gen.asString()<<eol;
  CylPlusSphCMinusSphC2.domName(sh);
  testMesh(CylPlusSphCMinusSphC2,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Cylinder+Sphere1-Sphere2";
  Geometry CylPlusSph2CMinusSph1 = (CylC+SphC)-SphC2;
  theCout<<CylPlusSph2CMinusSph1.geoNode_->asString()<<eol;
  CylPlusSph2CMinusSph1.domName(sh);
  testMesh(CylPlusSph2CMinusSph1,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="sphere^sphere";
  Sphere SphC0(_center=Point(1.,0,0.),_radius=1,_hsteps=0.02,_domain_name="sphere",_side_names="Sigma");
  Sphere SphCt=translate(SphC0,_direction={1.5,0.,0.});
  Geometry ComSS = SphC0^SphCt;
  testMesh(ComSS,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="sphere^sphere-cyl";
  RevCylinder Cyl0(_center1=Point(1,0.,0),_center2=Point(2.5,0.,0.),_radius=0.3,_hsteps=0.02,_domain_name="cylinder",_side_names="Gamma");
  Geometry ComSSMC = (SphC0^SphCt)-Cyl0;
  testMesh(ComSSMC,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  //-------------------------------------------------------------------------
  //                     Extruded geometries
  //-------------------------------------------------------------------------
  sh="Extruded_PerforatedHexagoneT";
  Polygon Hex2(_vertices=vh,_hsteps=0.1,_domain_name="Hex");
  Disk DiH(_center=Point(0.,0.,0.),_radius=0.5,_nnodes=10,_domain_name="DiH",_side_names=Strings("D1","D2","D3","D4"));
  Geometry HexMDi = Hex2-DiH;
  Geometry ExtHexT = extrude(HexMDi,Translation(0.,0.,4.),_layers=1, _hsteps=0.1,
                             _domain_name="omega",_base_names=Strings("base","top"),
                             _side_names=Strings("sigma","sigma","sigma","sigma","sigma","sigma","gamma"));
  testMesh(ExtHexT,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Extruded_PerforatedHexagoneR";
  Geometry ExtHexR = extrude(HexMDi,Rotation3d(Point(0,3,0),1,0,0,pi_/2),_layers=1,_hsteps=0.1,
                             _domain_name="omega",_base_names=Strings("base","top"),
                             _side_names=Strings("sigma","sigma","sigma","sigma","sigma","sigma","gamma"));
  testMesh(ExtHexR,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="Extruded_PerforatedHexagoneR2";
  Disk Di1(_center=Point(0.,0.5,0.),_radius=0.25,_nnodes=10,_domain_name="Di1");
  Disk Di2(_center=Point(0.,-0.5,0.),_radius=0.25,_nnodes=10,_domain_name="Di2");
  Geometry HexMDi2 = Hex2-Di1-Di2;
  Geometry ExtHexR2 = extrude(HexMDi2,Rotation3d(Point(0,3,0),1,0,0,pi_/2),_layers=1,_hsteps=0.1,
                              _domain_name="omega",_base_names=Strings("base","top"),
                              _side_names=Strings("sigma","sigma","sigma","sigma","sigma","sigma","gamma","gamma"));
  testMesh(ExtHexR2,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

////  -------------------------------------------------------------------------
////                              test Corona
////  -------------------------------------------------------------------------
////   std::srand(std::time(NULL));
////   sh="corona virus";
////   Cone co(_center1=Point(0.,0.,0.),_v1=Point(1.,0.,0.),_v2=Point(0.,1.,0.),_apex=Point(0.,0.,1),_hsteps=0.3,_side_names="gamma");
////   Cylinder cy(_center1=Point(0.,0.,0.),_v1=Point(0.7,0.,0.),_v2=Point(0.,0.7,0.),_center2=Point(0.,0.,7.),_hsteps=1,_side_names="gamma");
////   Sphere sp(_center=Point(0.,0.,7.),_radius=1.,_hsteps=0.3,_side_names="gamma");
////   Geometry ant=cy+co+sp;
////   testMesh(ant,"ant",_triangle,printOC,saveGeo,saveBrep,saveMsh);
////   Real R=30., aR=0.999*R, d=0.2;
////   Sphere spcor(_center=Point(0.,0.,0.),_radius=R,_hsteps=2,_side_names="sigma");
////   Geometry cor=toComposite(spcor);
////   Number n=8, m=7;
////   for(Number i=0;i<n;i++)
////    for(Number j=0;j<m;j++)
////     {
////      Real ir = i+d*(2*std::rand() * (1.0 / RAND_MAX)-1),
////             jr=  j+d*(2*std::rand() * (1.0 / RAND_MAX)-1);
////      Real t=pi_*(2*ir/n-1), p=pi_/8*(jr-3);
////      Transformation tf=Translation(Point(aR*cos(t)*cos(p),aR*sin(t)*cos(p),aR*sin(p)))*Rotation3d(Point(0,0,0),Point(0,0,1),t);
////      tf*=Rotation3d(Point(0,0,0),Point(0,1,0),pi_/2-p);
////      cor+=transform(ant,tf);
////    }
////    testMesh(cor,sh,_triangle,printOC,saveGeo,saveBrep,saveMsh);
////
//  -------------------------------------------------------------------------
//                     test mesh from loading brep file
//  -------------------------------------------------------------------------

  sh="naca";
  Geometry naca(inputsPathTo(rootname+"/NACA63-412.brep"));
  naca.setOCName(_solid,1,"naca");
  naca.setOCName(_face,1,"extrados"); //extrados face
  naca.setOCName(_face,2,"intrados"); //intrados face
  naca.setOCName(_face,Numbers(3,4),"lateral"); //lateral faces
  naca.setOCHstep(Numbers(1,3),0.2);
  naca.setOCHstep(Numbers(2,4),0.1);
  testMesh(naca,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh);

  sh="io1cm214";
  Geometry brep(inputsPathTo(rootname+"/io1-cm-214.brep"));
  brep.printOCInfo(theCout);
  brep.setOCName(_face,Numbers(3,18),"Sigma"); // face 3,18 named Sigma
  Numbers numv;numv.resize(24);
  for(Number i=0;i<12;i++) {numv[i]=i+7;numv[i+12]=i+35;}
  brep.setOCHstep(numv,1.);//vertices 7->18 and 35->46 with hstep=1.
  testMesh(brep,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh,0.,1.);

  sh="rafale";
  Geometry raf(inputsPathTo(rootname+"/RafaleStep.brep"));
  raf.setOCName(_face,0,"Gamma"); //all OC face named Gamma
  testMesh(raf,sh,_tetrahedron,printOC,saveGeo,saveBrep,saveMsh,0.,5.);

  if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
  else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  trace_p->pop();

  #endif
}

}
