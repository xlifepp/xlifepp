/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_LargeMatrixSkylineStorage.cpp
	\author E. Lunéville
	\since 04 jul 2012
	\date  12 jul 2012

	Low level tests of LargeMatrix class and SkylineStorage classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_LargeMatrixSkylineStorage {

void unit_LargeMatrixSkylineStorage(int argc, char* argv[], bool check)
{
  String rootname = "unit_LargeMatrixSkylineStorage";
  trace_p->push(rootname);
  std::stringstream ssout;                 
  ssout.precision(testPrec);
  verboseLevel(10);
  Number nbErrors = 0;
  String errors;
  Real tol=0.001;

  Matrix<Real> m22(2, 1, -5.);

  //!EF numbering (regular Q1 of square)
  Number n = 3;
  std::vector< std::vector<Number> > elts((n - 1) * (n - 1), std::vector<Number>(4));
  for(Number j = 1; j <= n - 1; j++)
    for(Number i = 1; i <= n - 1; i++)
    {
      Number e = (j - 1) * (n - 1) + i - 1, p = (j - 1) * n + i;
      elts[e][0] = p; elts[e][1] = p + 1; elts[e][2] = p + n; elts[e][3] = p + n + 1;
    }
  Number n2 = n * n;
  std::vector< std::vector<Number> >::iterator it;
  std::vector<Real> x1(n * n, 0.);
  for(Number i = 0; i < n * n; i++) { x1[i] = i; }
  ssout << "x1 = " << x1;

  std::vector<Number> rs(4);rs[0]=1;rs[1]=2;rs[2]=8;rs[3]=9;

  ssout << "\n----------------------------------- test sym skyline storage -----------------------------------\n";
  SymSkylineStorage* skysym = new SymSkylineStorage(n * n, elts, elts);
  skysym->visual(ssout);
  nbErrors += checkVisual(*skysym,  rootname+"/skysymVis.in", tol, errors, "skysym", check);
  LargeMatrix<Real> As1(skysym, 0., _symmetric);
  //!fill matrix
  for(Number i = 1; i <= n * n; i++)
    for(Number j = 1; j <= i; j++)
      if(As1.pos(i, j) > 0) { As1(i, j) = 10 * i + j; }
  ssout << "symsky real large matrix As1 : " << As1;
  nbErrors += checkValues(As1, rootname+"/As1.in", tol, errors, "As1", check);
  As1.viewStorage(ssout);
  nbErrors += checkVisual(As1,  rootname+"/As1Vis.in", tol, errors, "Storage As1", check);
  std::vector<Real> As1x1=As1* x1;
  std::vector<Real> x1As1=x1* As1;
  ssout << "product As1*x1 : " << As1x1;
  ssout << "\nproduct x1*As1 : " << x1As1;
  nbErrors+=checkValues(As1x1, rootname+"/As1x1.in", tol, errors, "As1*x1", check);
  nbErrors+=checkValues(x1As1, rootname+"/x1As1.in", tol, errors, "x1*As1", check);
  As1.saveToFile("As1dense.mat", _dense);
  ssout << "\nsave As1 to file Adense.mat in dense format\n";
  As1.saveToFile("As1coo.mat", _coo);
  ssout << "save As1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Real> As1reload(n2, n2, _skyline, _symmetric, 0.);
  As1reload.loadFromFile("As1dense.mat", _dense);
  nbErrors += checkValues(As1reload, rootname+"/As1reloaddense.in", tol, errors, "As1reload dense", check);
  ssout << "matrix As1reload from Adense : " << As1reload << "\n";
  As1reload.loadFromFile("As1coo.mat", _coo);
  ssout << "matrix As1reload from Acoo : " << As1reload << "\n";
  nbErrors += checkValues(As1reload, rootname+"/As1reloadcoo.in", tol, errors, "As1reload coo", check );
  SymSkylineStorage skysymb=SymSkylineStorage(*skysym);
  ssout << "matrix storage updated with dense submatrix indices [1,2,8,9]x[1,2,8,9] :\n";
  skysymb.visual(ssout);
  nbErrors += checkVisual(skysymb,  rootname+"/skysymbVis.in", tol, errors, "Storage skysumb", check);
  skysymb.addSubMatrixIndices(rs,rs);
  skysymb.visual(ssout);
  nbErrors += checkVisual(skysymb,  rootname+"/skysymb_2Vis.in", tol, errors, "Storage skysumb_2", check);
  ssout << "row 1 (col & adrs) : " <<As1.getRow(1) << "\n";
  ssout << "row 2 (col & adrs) : " <<As1.getRow(2) << "\n";
  ssout << "row 3 (col & adrs) : " <<As1.getRow(3) << "\n";
  ssout << "row 9 (col & adrs) : " <<As1.getRow(9) << "\n";
  ssout << "col 1 (col & adrs) : " <<As1.getCol(1) << "\n";
  ssout << "col 2 (col & adrs) : " <<As1.getCol(2) << "\n";
  ssout << "col 3 (col & adrs) : " <<As1.getCol(3) << "\n";
  ssout << "col 9 (col & adrs) : " <<As1.getCol(9) << "\n";
  nbErrors += checkValues(As1.getRow(1),  rootname+"/As1getRow1.in", tol, errors, "As1.getRow(1)", check);
  nbErrors += checkValues(As1.getRow(2),  rootname+"/As1getRow2.in", tol, errors, "As1.getRow(2)", check);
  nbErrors += checkValues(As1.getRow(3),  rootname+"/As1getRow3.in", tol, errors, "As1.getRow(3)", check);
  nbErrors += checkValues(As1.getRow(9),  rootname+"/As1getRow9.in", tol, errors, "As1.getRow(9)", check);
  nbErrors += checkValues(As1.getCol(1),  rootname+"/As1getCol1.in", tol, errors, "As1.getCol(1)", check);
  nbErrors += checkValues(As1.getCol(2),  rootname+"/As1getCol2.in", tol, errors, "As1.getCol(2)", check);
  nbErrors += checkValues(As1.getCol(3),  rootname+"/As1getCol3.in", tol, errors, "As1.getCol(3)", check);
  nbErrors += checkValues(As1.getCol(9),  rootname+"/As1getCol9.in", tol, errors, "As1.getCol(9)", check);

  ssout << "\n---------------test sym skyline storage and non symmetric matrix -------------------\n";
  skysym->visual(ssout);
  nbErrors += checkVisual(*skysym,  rootname+"/skysymnsmVis.in", tol, errors, "skysym non sym matrix", check);
  LargeMatrix<Real> Ans1(skysym, 0., _noSymmetry);
  //!fill matrix
  for(Number i = 1; i <= n * n; i++)
    for(Number j = 1; j <= i; j++)
      if(Ans1.pos(i, j) > 0) {Ans1(i, j) = 10 * i + j; Ans1(j, i) = Ans1(i, j);}
  ssout << "symsky real large non symmetric matrix Ans1 : " << Ans1;
  Ans1.viewStorage(ssout);
  nbErrors += checkVisual(Ans1,  rootname+"/Ans1Vis.in", tol, errors, "Ans1 storage", check);
  nbErrors += checkValues(Ans1,  rootname+"/Ans1.in", tol, errors, "Ans1 ", check);
  std::vector<Real> Ans1x1=Ans1* x1;
  std::vector<Real> x1Ans1=x1* Ans1;
  ssout << "product Ans1*x1 : " << Ans1x1;
  ssout << "\nproduct x1*Ans1 : " << x1Ans1;
  Ans1.saveToFile("Ans1dense.mat", _dense);
  ssout << "\nsave Ans1 to file Adense.mat in dense format\n";
  Ans1.saveToFile("Ans1coo.mat", _coo);
  ssout << "save Ans1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Real> Ans1reload(n2, n2, _skyline, _sym, 0.);
  Ans1reload.loadFromFile("Ans1dense.mat", _dense);
  ssout << "matrix Ans1reload from Adense : " << Ans1reload << "\n";
  nbErrors += checkValues(Ans1reload, rootname+"/Ans1reloaddense.in", tol, errors, "Ans1reload dense", check);
  Ans1reload.loadFromFile("Ans1coo.mat", _coo);
  ssout << "matrix Ans1reload from Acoo : " << Ans1reload << "\n";
  nbErrors += checkValues(Ans1reload, rootname+"/Ans1reloadcoo.in", tol, errors, "Ans1reload coo", check);

  ssout << "\n\n----------------------------------- test dual skyline storage -----------------------------------\n";
  DualSkylineStorage* skydual = new DualSkylineStorage(n * n, n * n, elts, elts);
  skydual->visual(ssout);
  nbErrors += checkVisual(*skydual,  rootname+"/skydualVis.in", tol, errors, "skydual", check);
  LargeMatrix<Real> Ad1(skydual, 0.);
  //!fill matrix Aij='ij', and symmetric in fact
  for(Number i = 1; i <= n * n; i++)
    for(Number j = 1; j <= i; j++)
      if(Ad1.pos(i, j) > 0) {Ad1(i, j) = 10 * i + j; Ad1(j, i) = Ad1(i, j);}
  ssout << "dualsky real large matrix Ad1 : " << Ad1;
  Ad1.viewStorage(ssout);
  nbErrors += checkVisual(Ad1,  rootname+"/Ad1Vis.in", tol, errors, "Ad1 storage", check);
  nbErrors += checkValues(Ad1,  rootname+"/Ad1.in", tol, errors, "Ad1 ", check);
  ssout << "product Ad1*x1 : " << Ad1* x1;
  ssout << "\nproduct x1*Ad1 : " << x1* Ad1;
  Ad1.saveToFile("Ad1dense.mat", _dense);
  ssout << "\nsave Ad1 to file Adense.mat in dense format\n";
  Ad1.saveToFile("Ad1coo.mat", _coo);
  ssout << "save Ad1 to file Acoo.mat in coordinates format\n";
  LargeMatrix<Real> Ad1reload(n2, n2, _skyline, _dual, 0.);
  Ad1reload.loadFromFile("Ad1dense.mat", _dense);
  ssout << "matrix Ad1reload from Adense : " << Ad1reload << "\n";
  nbErrors += checkValues(Ad1reload, rootname+"/Ad1reloaddense.in", tol, errors, "Ad1reload dense", check);
  Ad1reload.loadFromFile("Ad1coo.mat", _coo);
  ssout << "matrix Ad1reload from Acoo : " << Ad1reload << "\n";
  nbErrors += checkValues(Ad1reload, rootname+"/Ad1reloadcoo.in", tol, errors, "Ad1reload coo", check);

  DualSkylineStorage skydualb=DualSkylineStorage(*skydual);
  ssout << "matrix storage updated with dense submatrix indices [1,2,8,9]x[1,2,8,9] :\n";
  skydualb.visual(ssout);
  nbErrors += checkVisual(skydualb,  rootname+"/skydualbVis.in", tol, errors, "skydualb storage", check);
  skydualb.addSubMatrixIndices(rs,rs);
  skydualb.visual(ssout);
  nbErrors += checkVisual(skydualb,  rootname+"/skydualb_2Vis.in", tol, errors, "skydualb_2 storage", check);

  ssout << "row 1 (col & adrs) : " <<Ad1.getRow(1) << "\n";
  ssout << "row 2 (col & adrs) : " <<Ad1.getRow(2) << "\n";
  ssout << "row 3 (col & adrs) : " <<Ad1.getRow(3) << "\n";
  ssout << "row 9 (col & adrs) : " <<Ad1.getRow(9) << "\n";
  ssout << "col 1 (col & adrs) : " <<Ad1.getCol(1) << "\n";
  ssout << "col 2 (col & adrs) : " <<Ad1.getCol(2) << "\n";
  ssout << "col 3 (col & adrs) : " <<Ad1.getCol(3) << "\n";
  ssout << "col 9 (col & adrs) : " <<Ad1.getCol(9) << "\n";
  nbErrors += checkValues(Ad1.getRow(1),  rootname+"/Ad1getRow1.in", tol, errors, "Ad1.getRow(1)", check);
  nbErrors += checkValues(Ad1.getRow(2),  rootname+"/Ad1getRow2.in", tol, errors, "Ad1.getRow(2)", check);
  nbErrors += checkValues(Ad1.getRow(3),  rootname+"/Ad1getRow3.in", tol, errors, "Ad1.getRow(3)", check);
  nbErrors += checkValues(Ad1.getRow(9),  rootname+"/Ad1getRow9.in", tol, errors, "Ad1.getRow(9)", check);
  nbErrors += checkValues(Ad1.getCol(1),  rootname+"/Ad1getCol1.in", tol, errors, "Ad1.getCol(1)", check);
  nbErrors += checkValues(Ad1.getCol(2),  rootname+"/Ad1getCol2.in", tol, errors, "Ad1.getCol(2)", check);
  nbErrors += checkValues(Ad1.getCol(3),  rootname+"/Ad1getCol3.in", tol, errors, "Ad1.getCol(3)", check);
  nbErrors += checkValues(Ad1.getCol(9),  rootname+"/Ad1getCol9.in", tol, errors, "Ad1.getCol(9)", check);

  const Number rowNum = 3;
  const Number colNum = 3;

  const std::string rMatrixDataSym(rootname+"/matrix3x3Sym.data");
  const std::string rMatrixDataSymPosDef(rootname+"/matrix3x3SymPosDef.data");
  const std::string rMatrixDataNoSym(rootname+"/matrix3x3NoSym.data");
  const std::string cMatrixDataSymPosDef(rootname+"/cmatrix3x3SymPosDef.data");

  LargeMatrix<Real> rMatSkylineSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _skyline, _symmetric);
  LargeMatrix<Real> rMatSkylineSymNoSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, _skyline, _noSymmetry);
  LargeMatrix<Real> rMatSkylineDualNoSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSymSymPosDef(inputsPathTo(rMatrixDataSymPosDef), _dense, rowNum, _skyline, _symmetric);
  LargeMatrix<Real> rResSymSkylineLdlt(rMatSkylineSymSymPosDef);
  LargeMatrix<Real> rResSymSkylineLu(rMatSkylineSymNoSym);
  LargeMatrix<Real> rResDualSkylineLu(rMatSkylineDualNoSym);
  Vector<Real> rVecB(3, 1.);
  Vector<Real> rVecX(3, 1.);
  LargeMatrix<Complex> cMatSkylineSymSymPosDef(inputsPathTo(cMatrixDataSymPosDef), _dense, rowNum, _skyline, _selfAdjoint);
  LargeMatrix<Complex> cResLdlStar(cMatSkylineSymSymPosDef);
  Vector<Complex> cVecB(3, 1.);
  Vector<Complex> cVecX(3, 1.);

  ssout << "\n----------------------------------- test LDLt and LU factorization -----------------------------------\n";
  //! I. Real Matrix
  //!    SYMSKYLINE STORAGE
  ldltFactorize(rResSymSkylineLdlt);
  ssout << "The result of LDLt factorizing sym skyline matrix is " << rResSymSkylineLdlt << std::endl;
  nbErrors += checkValues(rResSymSkylineLdlt ,  rootname+"/rResSymSkylineLdlt.in", tol, errors, "rResSymSkylineLdlt ", check);
  luFactorize(rResSymSkylineLu);
  ssout << "The result LU factorizing sym skyline matrix is " << rResSymSkylineLu << std::endl;
  nbErrors += checkValues(rResSymSkylineLu ,  rootname+"/rResSymSkylineLu.in", tol, errors, "rResSymSkylineLu ", check);

  //!  DUALSKYLINE STORAGE
  luFactorize(rResDualSkylineLu);
  ssout << "The result of LU factorizing dual skyline matrix is " << rResDualSkylineLu << std::endl;
  nbErrors += checkValues(rResDualSkylineLu ,  rootname+"/rResDualSkylineLu.in", tol, errors, "rResDualSkylineLu ", check);

  //! II. Complex Matrix
  //!    SYMSKYLINE STORAGE
  ldlstarFactorize(cResLdlStar);
  ssout << "The result ldlstar factorized matrix is " << cResLdlStar << std::endl;
  nbErrors += checkValues(cResLdlStar , rootname+"/cLdlStar.in", tol, errors, "cResLdlStar ", check);

  ssout << "----------------------------------- test solver with factorized matrix -----------------------------------\n";
  rResSymSkylineLdlt.ldltSolve(rVecB, rVecX);
  ssout << "The result LDLt solver of sym skyline matrix is " << rVecX << std::endl;
  nbErrors += checkValues(rVecX , rootname+"/rVecX_ssldlt.in", tol, errors, "rVecX  ssldlt", check);
  cResLdlStar.ldlstarSolve(cVecB, cVecX);
  ssout << "The result LDL* solver of sym skyline matrix is " << cVecX << std::endl;
  nbErrors += checkValues(cVecX , rootname+"/cVecX_ldls.in", tol, errors, "cVecX_ldls", check);
  rResSymSkylineLu.luSolve(rVecB, rVecX);
  ssout << "The result LU solver of sym skyline matrix is " << rVecX << std::endl;
  nbErrors += checkValues(rVecX , rootname+"/rVecX_sslu.in", tol, errors, "rVecX_sslu ", check);
  rResDualSkylineLu.luSolve(rVecB, rVecX);
  ssout << "The result LU solver of dual skyline matrix is " << rVecX << std::endl;
  nbErrors += checkValues(rVecX , rootname+"/rVecX_dslu.in", tol, errors, "rVecX_dslu ", check);

  ssout << "----------------------------------- test diagonalMatrix -----------------------------------\n";
  ssout << "Diagonal DualSkyline matrix : " << diagonalMatrix (Ad1, 3.) << std::endl;
  ssout << "Diagonal SymSkyline matrix : " << diagonalMatrix(As1, 3.) << std::endl;
  nbErrors += checkValues( diagonalMatrix(As1, 3.) , rootname+"/As1_3.in", tol, errors, "diagonalMatrix(As1, 3.) ", check);
  nbErrors += checkValues( diagonalMatrix(Ad1, 3.) , rootname+"/Ad1_3.in", tol, errors, "diagonalMatrix(Ad1, 3.) ", check);

  ssout << "----------------------------------- test multMatrixScalar -----------------------------------\n";
  Real sigmaR = 3.0;
  Complex sigmaC = Complex(3.0, 3.0);
  LargeMatrix<Complex> Ad1Sc=Ad1* sigmaC;
  LargeMatrix<Complex> ScAd1=sigmaC* Ad1;
  LargeMatrix<Complex> As1Sc=As1* sigmaC;
  LargeMatrix<Complex> ScAs1=sigmaC* As1;
  LargeMatrix<Real> Ad1Sr=Ad1* sigmaR;
  LargeMatrix<Real> SrAd1=sigmaR* Ad1;
  LargeMatrix<Real> As1Sr=As1* sigmaR;
  LargeMatrix<Real> SrAs1=sigmaR* As1;

  ssout << "product Ad1*sigmaR : " << Ad1Sr << std::endl;
  ssout << "product sigmaR*Ad1 : " << SrAd1 << std::endl;
  ssout << "product Ad1*sigmaC : " << Ad1Sc << std::endl;
  ssout << "product sigmaC*Ad1 : " << ScAd1 << std::endl;
  ssout << "product As1*sigmaR : " << As1Sr << std::endl;
  ssout << "product sigmaR*As1 : " << SrAs1 << std::endl;
  ssout << "product As1*sigmaC : " << As1Sc << std::endl;
  ssout << "product sigmaC*As1 : " << ScAs1 << std::endl;
  nbErrors += checkValues( Ad1Sr , rootname+"/Ad1Sr.in", tol, errors, "Ad1*sigmaR ", check);
  nbErrors += checkValues( SrAd1 , rootname+"/SrAd1.in", tol, errors, "sigmaR*Ad1 ", check);
  nbErrors += checkValues( Ad1Sc , rootname+"/Ad1Sc.in", tol, errors, "Ad1*sigmaC ", check);
  nbErrors += checkValues( ScAd1 , rootname+"/ScAd1.in", tol, errors, "sigmaC*Ad1 ", check);
  nbErrors += checkValues( As1Sr , rootname+"/As1Sr.in", tol, errors, "As1*sigmaR ", check);
  nbErrors += checkValues( SrAs1 , rootname+"/SrAs1.in", tol, errors, "sigmaR*As1 ", check);
  nbErrors += checkValues( As1Sc , rootname+"/As1Sc.in", tol, errors, "As1*sigmaC ", check);
  nbErrors += checkValues( ScAs1 , rootname+"/ScAs1.in", tol, errors, "sigmaC*As1 ", check);


  ssout << "----------------------------------- test addMatrixMatrix -----------------------------------\n";
  LargeMatrix<Real> rSkylineDual(Ad1); LargeMatrix<Real> rSkylineSym(As1);
  LargeMatrix<Real> rSkylineDualpAd1 = rSkylineDual + Ad1;
  LargeMatrix<Real> rSkylineSympAs1 = rSkylineSym + As1;
  ssout << "Sum of two dual skyline matrices : " << rSkylineDualpAd1 << std::endl;
  ssout << "Sum of two sym skyline matrices : " << rSkylineSympAs1 << std::endl;
  nbErrors += checkValues( rSkylineDualpAd1 , rootname+"/rSkylineDualpAd1.in", tol, errors, "rSkylineDual + Ad1 ", check);
  nbErrors += checkValues( rSkylineSympAs1 , rootname+"/rSkylineSympAs1.in", tol, errors, "rSkylineSym + As1 ", check);

  Vector<Real> rVec0(3, 0.);
  Vector<Real> rVec1 = Reals(rMatSkylineSymSymPosDef*rVecB)-Reals(rResSymSkylineLdlt*rVecB);
  Vector<Real> rVec2 = Reals(rMatSkylineDualNoSym*rVecB)-Reals(rResSymSkylineLu*rVecB);
  Vector<Real> rVec3 = Reals(rMatSkylineDualNoSym*rVecB)-Reals(rResDualSkylineLu*rVecB);
  ssout<<"symSkyline factorization LLt : A*x-Af*x = "<<Reals(rMatSkylineSymSymPosDef*rVecB)-Reals(rResSymSkylineLdlt*rVecB)<<eol;
  ssout<<"symSkyline factorization  LU : A*x-Af*x = "<<Reals(rMatSkylineDualNoSym*rVecB)-Reals(rResSymSkylineLu*rVecB)<<eol;
  ssout<<"dualSkyline factorization LU : A*x-Af*x = "<<Reals(rMatSkylineDualNoSym*rVecB)-Reals(rResDualSkylineLu*rVecB)<<eol;
  nbErrors += checkValues( rVec1 , rVec0, tol, errors, "rVec1 rVec0 ");
  nbErrors += checkValues( rVec2 , rVec0, tol, errors, "rVec2-rVec0 ");
  nbErrors += checkValues( rVec3 , rVec0, tol, errors, "rVec3-rVec0 ");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
