/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_elasticity.cpp
  \author E. Lunéville
	\since 25 mar 2014
	\date  25 mar 2014

	Test elasticity problem - free traction condition
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_elasticity {

//data on sigma-

Real k=1.;
Real mu=1.;
Real lambda=1.;
Real omg=1.;

Vector<Real> uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Vector<Real> u(2,0.);
  u(1)= sin(k*pi_*x)*cos(k*pi_*y);
  u(2)=-sin(k*pi_*y)*cos(k*pi_*x);
  return u;
}

Vector<Real> f(const Point& P, Parameters& pa = defaultParameters)
{
   return (2*mu*k*k*pi_*pi_-omg*omg)*uex(P);
}


void unit_elasticity(int argc, char* argv[], bool check)
{
  String rootname = "unit_elasticity";
  trace_p->push(rootname);
  verboseLevel(40);

  String errors;
  Number nbErrors = 0;
  Real eps=0.01;

  //!create a mesh and Domains
  Mesh mesh2d(Rectangle(_xmin=-0.5,_xmax=0.5,_ymin=-0.5,_ymax=0.5,_nnodes=30,_domain_name="Omega"), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");

  //create interpolation
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u", _dim=2);       //displacement field
  TestFunction v(u, _name="v");

  Real omg2=omg*omg;

  //!test standard elasticity
  BilinearForm auv=2*mu*intg(omega,epsilon(u)%epsilon(v))+lambda*intg(omega,div(u)*div(v))-omg2*intg(omega,u|v);
  LinearForm fv=intg(omega,f|v);
  TermMatrix A(auv, _name="A");
  TermVector B(fv, _name="B");
  TermVector U=directSolve(A,B);  //solve linear system AX=F using direct method
  U.toVector(false);
  TermMatrix M(intg(omega,u|v));
  TermVector Uex(u,omega,uex);
  nbErrors += checkValuesL2(U , Uex , M, eps, errors, "2D elasticity, Standard formulation");

  //!test elasticity Voigt tensor form
  Matrix<Real> C(3,_idMatrix);  //Voigt form of C in sigma=C*epsilon
  C*=2*mu;C(1,1)+=lambda;C(2,1)=lambda;C(1,2)=lambda;C(2,2)+=lambda;
  BilinearForm auv2=intg(omega,(C%epsilon(u))%epsilon(v))-omg2*intg(omega,u|v);
  TermMatrix A2(auv2, _name="A2");
  TermVector U2=directSolve(A2,B);  //solve linear system AX=F using direct method
  nbErrors += checkValuesL2(U2 , Uex , M, eps, errors, "2D elasticity, Voigt formulation");

  //!test elasticity Voigt tensor form using generalized epsilon form
  BilinearForm auv3=intg(omega,(C%epsilonG(u,1,1,1))%epsilonG(v,1,1,1))-omg2*intg(omega,u|v);
  TermMatrix A3(auv3, _name="A3");
  TermVector U3=directSolve(A3,B);  //solve linear system AX=F using LU factorization
  nbErrors += checkValuesL2(U3 , Uex, M, eps, errors, "2D elasticity, generalized formulation");

  //!check partial generalized epsilon form
  TermMatrix G( intg(omega,epsilonG(u,1,1,1)%epsilonG(v,1,1,1)), _name="G");
  TermMatrix G1(intg(omega,epsilonG(u,1,1,0)%epsilonG(v,1,1,1)), _name="G1");
  TermMatrix G2(intg(omega,epsilonG(u,1,0,1)%epsilonG(v,1,1,1)), _name="G2");
  TermMatrix G12=G1+G2;
  nbErrors +=checkValue(norminfty(G-G12), 0., 10*theEpsilon, errors, "partial generalized epsilon");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
