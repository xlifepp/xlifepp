/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_triangle.cpp
	\author N. Kielbasiewicz
	\since 5 nov 2012
	\date 18 dec 2012

	Low level tests of refTriangle class.
 */

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_triangle {

void unit_triangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_triangle";
  trace_p->push(rootname);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!test GeomRefTriangle
  verboseLevel(12);
  theCout<<"----------------------------- GeomRefTriangle test ----------------------------------"<<eol;
  GeomRefTriangle grt;
  theCout<<grt;
  for (Number i=0;i<grt.nbSides();++i)
  {
    Number v1=grt.sideVertexNumbers()[i][0], v2=grt.sideVertexNumbers()[i][1];
    theCout<<"sideWithVertices("<<v1<<","<<v2<<") = "<<grt.sideWithVertices(v1,v2)<<eol;
  }
  nbErrors += checkValue(theCout, rootname+"/GeomRefTrg.in" , errors, "Test Geom Ref Triangle",check);


  //!test interpolation
  verboseLevel(5);
  theCout<<eol<<"----------------------------- interpolation test ----------------------------------"<<eol;
  Interpolation* p1 = findInterpolation(_Lagrange, _standard, 1, H1);
  Interpolation* p2 = findInterpolation(_Lagrange, _standard, 2, H1);
  Interpolation* p3 = findInterpolation(_Lagrange, _standard, 3, H1);
  Interpolation* p4 = findInterpolation(_Lagrange, _standard, 4, H1);
  Interpolation* p5 = findInterpolation(_Lagrange, _standard, 5, H1);
  RefElement* triangleP1 = findRefElement(_triangle, p1);
  RefElement* triangleP2 = findRefElement(_triangle, p2);
  RefElement* triangleP3 = findRefElement(_triangle, p3);
  RefElement* triangleP4 = findRefElement(_triangle, p4);
  RefElement* triangleP5 = findRefElement(_triangle, p5);

  verboseLevel(10);
  theCout << *(triangleP1) << std::endl;
  theCout << *(triangleP2) << std::endl;
  theCout << *(triangleP3) << std::endl;
  theCout << *(triangleP4) << std::endl;
  theCout << *(triangleP5) << std::endl;
  nbErrors += checkValue(theCout, rootname+"/Interpolation.in" , errors, "Test Interpolation",check);

  //!test splitting EXPERIMENTAL
  theCout << "split P1 to P1 : "<<triangleP1->splitLagrange2DToP1() << std::endl;
  theCout << "split P2 to P1 : "<<triangleP2->splitLagrange2DToP1() << std::endl;
  theCout << "split P3 to P1 : "<<triangleP3->splitLagrange2DToP1() << std::endl;
  theCout << "split P4 to P1 : "<<triangleP4->splitLagrange2DToP1() << std::endl;
  theCout << "split P5 to P1 : "<<triangleP5->splitLagrange2DToP1() << std::endl;
  nbErrors += checkValue(theCout, rootname+"/splittingEXPERIMENTAL.in" , errors, "Test splitting EXPERIMENTAL",check);

  //!test Lagrange element using polynomials representation
  theCout<<"\n\n================  Lagrange triangle Pk, k=1 ======================"<<eol;
  LagrangeStdTrianglePk P1t(p1);
  P1t.print(theCout);
  theCout<<"\n\n================  Lagrange triangle Pk, k=2 ======================"<<eol;
  LagrangeStdTrianglePk P2t(p2);
  P2t.print(theCout);
  theCout<<"\n\n================  Lagrange triangle Pk, k=3 ======================"<<eol;
  LagrangeStdTrianglePk P3t(p3);
  P3t.print(theCout);
  theCout<<"\n\n================  Lagrange triangle Pk, k=4 ======================"<<eol;
  LagrangeStdTrianglePk P4t(p4);
  P4t.print(theCout);
  theCout<<"\n\n================  Lagrange triangle Pk, k=5 ======================"<<eol;
  LagrangeStdTrianglePk P5t(p5);
  P5t.print(theCout);
  nbErrors += checkValue(theCout, rootname+"/LagrangeElementUsingPolynomialsRepresentation.in" , errors, "Test Lagrange element using polynomials representation",check);

  if (check)
  {
   if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
   else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else
  { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
