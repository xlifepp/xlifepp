/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_NedelecFaceTetrahedron.cpp
	\author E. Lunéville
	\since 16 sep 2015
	\date 16 sep 2015

	Low level tests of Nedelec's face element on tetrahedron
	and a global test

	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_NedelecFaceTetrahedron
{

typedef GeomDomain& Domain;

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  return 128*(x*(1-x)*y*(1-y) + x*(1-x)*z*(1-z) + z*(1-z)*y*(1-y));
}

Real solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2), z=P(3);
  return 64*x*(1-x)*y*(1-y)*z*(1-z);
}

const Element* elt;
Number index=1;
Vector<Real> w(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> r(3);
  ShapeValues shv = elt->computeShapeValues(P, false, false, 0);
  Number i=index-1;
  r[0]=shv.w[3*i];r[1]=shv.w[3*i+1];r[2]=shv.w[3*i+2];
  return r;
}

//compute dof_i(w_j) in physical space, check computeShapeValue and dof::operator()
void checkPhysicalShapeValue(const Space& V, std::stringstream& ssout)
{
  theCout<<V<<eol;
  elt=V.element_p(Number(0));
  Function fw(w,3);
  ssout<<" -------- checkPhysicalShapeValue -------------"<<eol;
  String s="dof  ";
  for(Number d=1;d<=V.nbDofs();d++)
  {
    if(d==10) s="dof ";
    ssout<<s<<d<<": ";
    for(Number j=1;j<=V.nbDofs();j++)
    {
      index=j;
      ssout<<roundToZero(V.dof(d)(fw).asReal(),1.E-10)<<" ";
     }
   ssout<<eol;
  }
}

//!solve laplace problem using mixed formulation

void checkLaplaceMixed(FESubType fs, Number ord, Domain omega, const String& rootname, std::stringstream& ssout,
                       bool check, double tol, String& errors, Number& nbErrors)
{
 Number fa=1;
 String fss = "first";
 if(fs==_secondFamily){fa=2;fss="second";}
 theCout<<eol<<"=============== test Nedelec face "<<fss<<" family of order "<<ord<<" ==============="<<eol;
 Space V(omega,interpolation(_Nedelec,_firstFamily,ord+1,Hdiv),"V",false);
 Space H(omega,interpolation(_Lagrange,_standard,ord,H1),"H",false);
 Unknown p(V, _name="p");  TestFunction q(p, _name="q");
 Unknown u(H, _name="u");  TestFunction v(u, _name="v");
 //!create problem (Poisson problem)
 BilinearForm a=intg(omega,p|q)+intg(omega,u*div(q))-intg(omega,div(p)*v);
 LinearForm l=intg(omega,f*v);
 TermMatrix A(a, _name="A");
 TermVector b(l, _name="B");
 //!solve and save solution
 TermVector X=directSolve(A,b);
 String fn="u"+tostring(fa)+tostring(ord);
 saveToFile("u"+tostring(ord+1), X(u),_format=_vtu, _aUniqueFile);
 nbErrors+= checkValues("u"+tostring(ord+1)+".vtu", rootname+"/u"+tostring(ord+1)+".in", tol, errors, "u"+tostring(ord+1)+".vtk", check);
//!compute error
 TermVector u_ex(u,omega,solex);
 TermVector u_er=u_ex-X(u);
 TermMatrix M(intg(omega,u*v), _name="M");
 Real e=std::sqrt(real((M*u_er|u_er)));
 theCout<<" Laplace problem with NF1_"<<ord<<" ("<<V.nbDofs()<<" dofs) and P"<<ord<<" ("<<H.nbDofs()<<" dofs)";
 theCout<<" nb dofs total = "<<X.nbDofs()<<", L2 error = "<<e<<eol;
 nbErrors+=checkValue(e, 0., tol, errors, "ErrL2 NF"+tostring(fa)+tostring(ord));
}

void unit_NedelecFaceTetrahedron(int argc, char* argv[], bool check)
{
  String rootname = "unit_NedelecFaceTetrahedron";
  trace_p->push(rootname);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(10);

  Number nbErrors = 0;
  String errors;
  Real tol=0.02;

  //numberOfThreads(1);
  Mesh me(_tetrahedron); Domain omg=me.domain("Omega");

  Point x(1./3,1./3,1./3);
  ShapeValues shv;
  Interpolation* interp;
  interp=findInterpolation(Nedelec, _firstFamily, 1, Hdiv);
  ssout<<"\n\n================  Nedelec face tetrahedron, first family, order 1 (NF1_1) ======================"<<eol;
  NedelecFaceFirstTetrahedronPk NF11(interp);
  NF11.print(ssout);
  shv = ShapeValues(NF11);
  NF11.computeShapeValues(x.begin(),shv,true);
  NF11.printShapeValues(ssout,x.begin(),shv);
  Space V11(omg,*interp,"V11",false);
  checkPhysicalShapeValue(V11,ssout);
  nbErrors += checkValue(ssout, rootname+"/NF11.in", errors, "NF11", check);
  ssout<<"\n\n================  Nedelec face tetrahedron, first family, order 2 (NF1_2) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 2, Hdiv);
  NedelecFaceFirstTetrahedronPk NF12(interp);
  NF12.print(ssout);
  shv = ShapeValues(NF12);
  NF12.computeShapeValues(x.begin(),shv,true);
  NF12.printShapeValues(ssout,x.begin(),shv);
  Space V12(omg,*interp,"V12",false);
  checkPhysicalShapeValue(V12,ssout);
  nbErrors += checkValue(ssout, rootname+"/NF12.in", errors, "NF12", check);
  ssout<<"\n\n================  Nedelec face tetrahedron, first family, order 3 (NF1_3) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 3, Hdiv);
  NedelecFaceFirstTetrahedronPk NF13(interp);
  NF13.print(ssout);
  shv = ShapeValues(NF13);
  NF13.computeShapeValues(x.begin(),shv,true);
  NF13.printShapeValues(ssout,x.begin(),shv);
  Space V13(omg,*interp,"V13",false);
  checkPhysicalShapeValue(V13,ssout);
  nbErrors += checkValue(ssout, rootname+"/NF13.in", errors, "NF13", check);
  ssout<<"\n\n================  Nedelec face tetrahedron, first family, order 4 (NF1_4) ======================"<<eol;
  interp=findInterpolation(Nedelec, _firstFamily, 4, Hdiv);
  NedelecFaceFirstTetrahedronPk NF14(interp);
  NF14.print(ssout);
  shv = ShapeValues(NF14);
  NF14.computeShapeValues(x.begin(),shv,true);
  NF14.printShapeValues(ssout,x.begin(),shv);
  Space V14(omg,*interp,"V14",false);
  checkPhysicalShapeValue(V14,ssout);
  nbErrors += checkValue(ssout, rootname+"/NF14.in", errors, "NF14", check);
  ssout<<"\n================================================================================================"<<eol<<eol;

  // the Nedelec face second family is not implemented, to do later

  //check laplace problem using mixed formulation
  Number n=5;
  Mesh  mesh3d(inputsPathTo(rootname+"/mesh3d.msh"), _name="mesh of the unit cube");
  Domain omega=mesh3d.domain("Omega");
  checkLaplaceMixed (_firstFamily, 1, omega, rootname, ssout, check, 0.06, errors, nbErrors);
  checkLaplaceMixed (_firstFamily, 2, omega, rootname, ssout, check, 0.005, errors, nbErrors);
  // checkLaplaceMixed (_firstFamily, 3, omega, rootname, ssout, check, 0.0005, errors, nbErrors); // is working, suspended in systematic tests because too long

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
