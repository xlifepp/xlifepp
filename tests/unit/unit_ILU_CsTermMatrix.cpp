/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_ILU_CsTermMatrix.cpp
	\author C.Chambeyron
	\since 18 Avril 2017
	\date 18 Avril 2017

	Low level tests of Cs TermMatrix .
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_ILU_CsTermMatrix {

// scalar 2D function
Real un(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

Real xfy(const Point& P, Parameters& pa = defaultParameters)
{
  return P(1)*P(2);
}

void unit_ILU_CsTermMatrix(int argc, char* argv[], bool check)
{
  String rootname = "unit_ILU_CsTermMatrix";
  trace_p->push(rootname);
  std::stringstream out;                  // string stream receiving results
  out.precision(testPrec);
  std::stringstream ssout;     
  ssout.precision(testPrec);
  String errors;
  Number nbErrors = 0;
  Real tol=0.005;

  verboseLevel(30);

  //! create a mesh and Domains
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  verboseLevel(10);
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=3,_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");
  Domain gamma1=mesh2d.domain("Gamma_1");

  //! create Lagrange P1 space and unknown
  Interpolation *LagrangeP1=findInterpolation(Lagrange,standard,1,H1);
  Space V1(_domain=omega,_interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u(V1, _name="u");
  TestFunction v(u, _name="v");
  Function f1(un,"one");
  Function fxy(xfy,"xy");
  cpuTime("time for initialisation, mesh and space definition");

  //! useful TermVector
  TermVector un(u,omega,1.,"un");
  TermVector unv(v,omega,1.,"unv");
  
  //! test some surfacic bilinear forms requiring domain extension
  TermMatrix Dxuv1(intg(gamma1,dx(u)*v), _name="Dxuv1");
  cpuTime("time for computation of Dxuv1");
  ssout<<Dxuv1;
  nbErrors+=checkValues(Dxuv1, rootname+"/Dxuv1.in", tol, errors, " intg(gamma1,dx(u)*v)", check);
  TermMatrix Dxvu1(intg(gamma1,u*dx(v)), _name="Dxvu1");
  cpuTime("time for computation of Dxvu1");
  ssout<<Dxvu1;
  nbErrors+=checkValues(Dxvu1, rootname+"/Dxvu1.in", tol, errors, " intg(gamma1,u*dx(v)", check);
  TermMatrix Dyuv1(intg(gamma1,dy(u)*v), _name="Dyuv1");
  cpuTime("time for computation of Dyuv1");
  ssout<<Dyuv1;
  nbErrors+=checkValues(Dyuv1, rootname+"/Dyuv1.in", tol, errors, " intg(gamma1,dy(u)*v)", check);
  TermMatrix Dnuv1(intg(gamma1,ndotgrad(u)*v), _name="Dnuv1");
  cpuTime("time for computation of Dnuv1");
  ssout<<Dnuv1;
  nbErrors+=checkValues(Dnuv1, rootname+"/Dnuv1.in", tol, errors, " intg(gamma1,ndotgrad(u)*v)", check);

  //!test some combination of bilinear forms
  Real k=1,alpha=1.;
  BilinearForm h=intg(omega,grad(u)|grad(v))-k*k*intg(omega,u*v)+alpha*intg(gamma1,u*v);
  TermMatrix H(h, _name="H");
  cpuTime("time for computation of H");
  ssout<<H;
  nbErrors+=checkValues(H, rootname+"/H.in", tol, errors, " intg(omega,grad(u)|grad(v))-k*k*intg(omega,u*v)+alpha*intg(gamma1,u*v)", check);

  H.setStorage(_cs,_dual);
  out<<H;
  nbErrors+=checkValues(H, rootname+"/HcsDual.in", tol, errors, " H.setStorage(_cs,_dual)", check);

  // ILU factorization of H:
  
//   //test of linear combination of TermMatrix's
//   TermMatrix Hlc=K-(k*k)*M+alpha*Mg1;
//   //Hlc.name()="Hlc";
//   cpuTime("time for computation of Hlc");
//   out<<Hlc;
// 
//   clear(Hlc);
// 
//   //test some combination of bilinear forms
//   BilinearForm h2=intg(omega,grad(u)|grad(v))+intg(omega,u*dx(v))+alpha*intg(gamma1,u*v);
//   TermMatrix H2(h2, _name="H2");
//   cpuTime("time for computation of H2");
//   out<<H2;
// 
//   //test of linear combination of TermMatrix's
//   TermMatrix H2lc=K+Dxvu+alpha*Mg1;
//   //H2lc.name()="H2lc";
//   cpuTime("time for computation of H2lc");
//   out<<H2lc;
//   //H2lc=H2lc-H2;
//   H2lc-=H2;
//   out<<H2lc;
//   clear(H2lc);
//   clear(H2);
// 
//   // test product and inverse
//   TermMatrix M12=M1*M1;
//   thePrintStream<<"M12=M1*M1="<<M12;
//   TermMatrix invM1M1f=directSolve(M1,M1,_keep);
//   out<<"invM1M1f=directSolve(M1,M1)="<<invM1M1f;
//   TermMatrix Id(M1,_idMatrix,"Id");
//   out<<Id;
//   out<<"directSolve(M1,Id,_keep)="<<directSolve(M1,Id,_keep);
//   TermMatrix invM1=inverse(M1);
//   out<<"invM1=inverse(M1)="<<invM1;
//   out<<"invM1*M1="<<invM1*M1;
//   out<<"invM1*M12="<<invM1*M12;
//   TermMatrix MT=invM1*M12-M1;
//   out<<"norm(invM1*M12-M1)="<<norm2(MT)<<eol;
//   out<<"norm(invM1*M12-M1)="<<norm2(invM1*M12-M1)<<eol;
//   out<<"out<<invM1*M12-M1="<<invM1*M12-M1<<eol;

  thePrintStream<<"===== avant Mpq ====="<<eol;
  printMatrixStorages(thePrintStream);

//  //other interpolations
//  Interpolation LagrangeP2(Lagrange,standard,2,H1);
//  Space V2(omega,LagrangeP2,"V2",false);
//  Unknown p(V2, _name="p");
//  TestFunction q(p, _name="q");
//  BilinearForm pq=intg(omega,p*q);
//  TermMatrix Mpq(pq, _name="pq");
//  cpuTime("time for computation of Mpq");
//  out<<Mpq;
//  TermVector pun(p,omega,1.,"pun");
//  TermVector qun(q,omega,1.,"qun");
//  out<<"\nMpq*un|un="<<realPart(Mpq*pun|qun)<<"\n\n";
//  clear(Mpq);
//
//  thePrintStream<<"===== avant Kpq ====="<<eol;
//  printMatrixStorages(thePrintStream);
//
//  BilinearForm gpgq=intg(omega,grad(p)|grad(q));
//  TermMatrix Kpq(gpgq, _name="Kpq");
//  cpuTime("time for computation of Kpq");
//  out<<Kpq;
//  out<<"\n"<<Kpq*pun<<"\n\n";
//  clear(Kpq);
//
//  thePrintStream<<"===== après Kpq ====="<<eol;
//  printMatrixStorages(thePrintStream);thePrintStream.flush();
//
//  Interpolation LagrangeP3(Lagrange,standard,3,H1);
//  Space V3(omega,LagrangeP3,"V3",false);
//  Unknown s(V3, _name="s");
//  TestFunction t(s, _name="t");
//  BilinearForm st=intg(omega,s*t);
//  TermMatrix Mst(st, _name="st");
//  cpuTime("time for computation of Mst");
//  out<<Mst;
//  TermVector sun(s,omega,1.,"pun");
//  TermVector tun(t,omega,1.,"qun");
//  out<<"\nMst*un|un="<<realPart(Mst*sun|tun)<<"\n\n";
//  clear(Mst);
//
//  BilinearForm gsgt=intg(omega,grad(s)|grad(t));
//  TermMatrix Kst(gsgt, _name="Kst");
//  cpuTime("time for computation of Kst");
//  out<<Kst;
//  out<<"\n"<<Kst*sun<<"\n";
//  clear(Kst);
//
//  Interpolation LagrangeP4(Lagrange,standard,4,H1);
//  Space V4(omega,LagrangeP4,"V4",false);
//  Unknown e(V4, _name="e");
//  TestFunction f(e, _name="f");
//  BilinearForm ef=intg(omega,e*f);
//  TermMatrix Mef(ef, _name="ef");
//  cpuTime("time for computation of Mef");
//  out<<Mef;
//  TermVector eun(e,omega,1.,"eun");
//  TermVector fun(f,omega,1.,"fun");
//  out<<"\nMef*un|un="<<realPart(Mef*eun|fun)<<"\n\n";
//  clear(Mef);
//
//  BilinearForm gegf=intg(omega,grad(e)|grad(f));
//  TermMatrix Kef(gegf, _name="Kef");
//  cpuTime("time for computation of Kef");
//  out<<Kef;
//  out<<"\n"<<Kef*eun<<"\n";
//  clear(Kef);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
