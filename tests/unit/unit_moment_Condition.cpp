/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_moment_Condition.cpp
  \author E. Lunéville
	\since 22 jan 2014
	\date  22 jan 2014

	Test on Laplace problem the moment condition intg_sigma u=0;
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_moment_Condition {

//data on sigma-
Real un(const Point& P, Parameters& pa = defaultParameters)
{
  return 1.;
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  return -8.;
}


Real solex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1);
  return 4*x*(x-2);
}

void unit_moment_Condition(int argc, char* argv[], bool check)
{
  String rootname = "unit_moment_Condition";
  trace_p->push(rootname);

  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(40);
  
  Number nbErrors = 0;
  String errors;
  Real eps=0.0001;

  //create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=1";
  sidenames[2] = "y=1"; sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=10,_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaM=mesh2d.domain("x=0");

  //!create interpolation
  Space V(_domain=omega, _interpolation=_P2, _name="V");
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");
  ssout<<"mesh and space created cputime="<<cpuTime()<<eol;

  //!create bilinear form and linear form
  BilinearForm auv=intg(omega,grad(u)|grad(v));
  LinearForm fv=intg(omega,f*v);
  EssentialConditions ecs= (intg(sigmaM,u) = 0);
  TermMatrix A(auv, ecs, _name="A");
  TermVector B(fv, _name="B");
  ssout<<"TermMatrix A computed cputime="<<cpuTime()<<eol;

  //!solve linear system AX=F using LU factorization
  TermVector U=directSolve(A,B);
  ssout<<"matrix factorized cputime="<<cpuTime()<<eol;

  //!compute error
  BilinearForm uv=intg(omega,u*v);
  TermMatrix M(uv, _name="M");
  TermVector Uex(u,omega,solex);
  nbErrors += checkValuesL2( U , Uex , M, eps,  errors, "Error L2 Moment Condition");  

  ssout << "\n There are " << nbErrors << " errors:" << eol;
  
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
