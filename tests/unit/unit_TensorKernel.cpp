/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_TensorKernel.cpp
	\author E. Lunéville
	\since 14 nov 2013
	\date 14 nov 2013

	Basic test of TensorKernel
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_TensorKernel {

Real k=5;

//!cos basis
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                    //get the parameter h (user definition)
  Int n=pa("basis index")-1;         //get the index of function to compute
  if(n==0) return sqrt(1./h);
  else     return sqrt(2./h)*cos(n*pi_*(y+h*0.5)/h);
}

//!map  sigma+ -> sigma-
Vector<Real> mapP(const Point& P, Parameters& pa = defaultParameters)
{
    Point P2(P.x(),P.y());
    Real theta=pi_/3, h=0.5, l1=3., l2=1.5;
    Real c=cos(theta), s=sin(theta);
    Point A(0.,0.);
    Point B=A; B(1)+=l1;
    Point C=B; C(1)+=l2*c;C(2)+=l2*s;
    Point C1=C; C1(1)+=h*s; C1(2)-=h*c;
    Point Q=P2-C1;
    Vector<Real> R(2,0.);
    R(1)=Q(1)*c+Q(2)*s; R(2)=Q(2)*c-Q(1)*s;
    R(2)-=h;
    return R;
}

void unit_TensorKernel(int argc, char* argv[], bool check)
{
  String rootname = "unit_TensorKernel";
  trace_p->push(rootname);

  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(21);
  String errors;
  
  Number nbErrors = 0;
  Real tol=0.0001;
  //!create a mesh and Domains
  Mesh mesh2d(inputsPathTo(rootname + "/coude60_court_moyen.msh"), _name="coude60deg");
  Domain omega=mesh2d.domain("omega");
  Domain sigmaP=mesh2d.domain("sigmaP");

  //!create interpolation
  Interpolation* LagrangePk=findInterpolation(Lagrange,standard,2,H1);
  Space V(omega,*LagrangePk,"V",true);
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");

  //!parameters
  Real h=1;
  Parameters params(h,"h");
  params << Parameter(k,"k");

  //!create spectral space and dtn kernel
  Number N=5;
  Space Sp(_domain=sigmaP, _basis=Function(cosny,params), _dim=N, _name="cos(n*pi*y)");
  Unknown phiP(Sp, _name="phiP");
  Vector<Complex> lambda(N);
  for (Number n=0; n<N; n++) lambda[n]=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));
  TensorKernel tkp(phiP,lambda);
  tkp.xmap=Function(mapP);
  tkp.ymap=tkp.xmap;

  ssout << "tkp:"<<eol <<tkp << eol;
  TermMatrix T1(intg(sigmaP,sigmaP,u*tkp*v), _name="T1");
  nbErrors += checkValues(T1, rootname+"/T1.in", 0.001, errors, " T1", check);
  ssout << "T1 : " << eol << T1 << eol;

  Matrix<Complex> mambda(N,N);
  for (Number n=0; n<N; n++) mambda(n+1,n+1)=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));
  TensorKernel mkp(phiP,mambda);
  mkp.xmap=Function(mapP);
  mkp.ymap=tkp.xmap;
  ssout << "mkp: " << eol << mkp << eol;
  TermMatrix T2(intg(sigmaP,sigmaP,u*tkp*v), _name="T2");
  nbErrors += checkValues(T2, rootname+"/T2.in", 0.001, errors, " T1", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
 
 trace_p->pop();
 
}

}
