/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
Test elasticity problem in 2D and 3D

\file unit_epsilon.cpp
\author E. Lunéville
\since 16 mai 2013
\date 16 mai 2013
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>


using namespace std;
using namespace xlifepp;

namespace unit_epsilon {

Number nbErrors = 0;
Real eps=0.0001;
String errors;

int Epsilon_Pk(const GeomDomain& omg, int k,  Real h, Real eps, std::ostream& out)
{
  stringstream s;
  trace_p->push("Epsilon_Pk");

  //!create Lagrange Pk space and unknown
  Interpolation& Pk=interpolation(Lagrange, standard, k, H1);
  Space Vk(_domain=omg, _FE_type=Lagrange, _order=k, _name="Vk");
  Unknown u(Vk,  _name="u", _dim=2); TestFunction v(u,  _name="v");
  TermVector UN(u,omg,1.,"un");
  
  //! Create bilinear form
  TermMatrix M(intg(omg, u | v), _name="M");
  TermMatrix S(intg( omg,  epsilon(u)%epsilon(v) ), _name="S");
  TermMatrix D(intg( omg,  div(u)*div(v) ), _name="D");
  BilinearForm auv=intg( omg,  epsilon(u)%epsilon(v) ) + intg(omg, div(u) * div(v) ) + intg(omg, u | v);
  TermMatrix A(auv, _name="a(u,v)");

  Number NbP  = A.begin()->second->entries()->rmEntries_p->nbRows;
  Number NbPs = A.begin()->second->entries()->rmEntries_p->nbRowsSub;

  TermVector B = A*UN;
  TermVector E=directSolve(A,B);

  nbErrors += checkValuesL2(E , UN , M, eps,  errors, "solve elasticity pb 2D/3D");
  trace_p->pop();
  return nbErrors;
}

void unit_epsilon(int argc, char* argv[], bool check)
{
  String rootname = "unit_epsilon";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  string meshInfo;
  verboseLevel(100);
  Number nbDivMin , nbDivMax;
  Number ordMin, ordMax;

  //!create a mesh and Domains TRIANGLES
  out << " =====================================================================================" << eol;
  out << "                           2D test on square" << eol;
  out << " =====================================================================================" << eol;
  meshInfo= " Triangle mesh of square";
  nbDivMin =5, nbDivMax=5;
  ordMin = 1, ordMax=1;
  for(Number div = nbDivMin; div <= nbDivMax; div+=5)
  {
    Real h=1./div;
    Mesh mesh2d(Rectangle(_xmin=0, _xmax=1, _ymin=0, _ymax=1, _nnodes=div+1, _domain_name = "Omega"), _shape=_triangle, _order=1, _generator=_structured, _name=meshInfo);
    Domain omega = mesh2d.domain("Omega");
    for(Number ord=ordMin; ord<=ordMax; ord++)
    {
      nbErrors += Epsilon_Pk(omega, ord, h, eps, out);
    }
  }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
