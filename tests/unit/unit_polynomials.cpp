/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_polynomials.cpp
	\author E. Lunéville
	\since 31 jan 2015
	\date 31 jan 2015

	Low level tests of polynomials stuff
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

using namespace xlifepp;

namespace unit_polynomials
{

void unit_polynomials(int argc, char* argv[], bool check)
{
  String rootname = "unit_polynomials";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  std::stringstream ssout;
  ssout.precision(testPrec);
  verboseLevel(10);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!basic construction
  Monomial un(0,0,0), x(1,0,0), y(0,1,0), z(0,0,1), x2(2,0,0),y2(0,2,0),xy(1,1,0);
  Polynomial p(un,1.); p.push_back(2.,x); p.push_back(-3.,x2);
  ssout<<"p(x,y) = "<<p<<" degree = "<<p.degree()<<eol;
  ssout<<"p(0)="<<p(0)<<" p(1)="<<p(1)<<" p(2)="<<p(2)<<eol;
  PolynomialBasis basis(2,Monomial(0),Monomial(1));
  basis.push_back(p);
  ssout<<basis<<eol;
  nbErrors+= checkValues(basis,  rootname+"/basis.in", 0, errors, "Basis",check);
  Polynomial x2py2(Monomial(2,0,0),1.,Monomial(0,2,0),1.); //x2+y2
  ssout<<"x2py2="<<x2py2<<eol;
  nbErrors+= checkValue(tostring(p), "1+2x-3x^2", errors, "p(x,y)");
  nbErrors+= checkValue(p.degree(), 2, tol, errors, "p.degree()");
  nbErrors+= checkValue(p(0), 1, tol, errors, "p(0))");
  nbErrors+= checkValue(p(1), 0, tol, errors, "p(1))");
  nbErrors+= checkValue(p(2), -7, tol, errors, "p(2))");
  nbErrors+= checkValue(tostring(x2py2), "x^2+y^2", errors, "x2py2");
  //! fctn checkbasis avec ...

  //!derivatives
  Monomial xyz2(1,1,2);
  ssout<<"dx(xyz2)="<<dx(xyz2)<<" dy(xyz2)="<<dy(xyz2)<<" dz(xyz2)="<<dz(xyz2)<<eol;
  ssout<<"grad(xyz2)="<<grad(xyz2)<<eol;
  ssout<<"curl2d(xyz2)="<<curl(xyz2,2)<<eol;
  ssout<<"curl3d(xyz2)="<<curl(xyz2,3)<<eol;
  nbErrors+= checkValue(tostring(dx(xyz2)), "y.z^2", errors, "dx(xyz2)");
  nbErrors+= checkValue(tostring(dy(xyz2)), "x.z^2", errors, "dy(xyz2)");
  nbErrors+= checkValue(tostring(dz(xyz2)), "2x.y.z", errors, "dz(xyz2)");
  nbErrors+= checkValue(tostring(grad(xyz2)), "[ y.z^2 x.z^2 2x.y.z ]", errors, "grad(xyz2)");
  nbErrors+= checkValue(tostring(curl(xyz2,2)), "[ x.z^2 -y.z^2 ]", errors, "curl2d(xyz2)");
  nbErrors+= checkValue(tostring(curl(xyz2,3)), "[ x.z^2-2x.y.z 2x.y.z-y.z^2 y.z^2-x.z^2 ]", errors, "curl3d(xyz2)");
  ssout<<eol;

  //!integrals
  ssout<<"integral(_x,x)="<<integral(_x,x)<<eol;
  ssout<<"integral(_y,x)="<<integral(_y,x)<<eol;
  ssout<<"integral(_z,x)="<<integral(_z,x)<<eol;
  ssout<<"integral(_x,x2py2)="<<integral(_x,x2py2)<<eol;
  ssout<<"integral(_y,integral(_x,x2py2))="<<integral(_y,integral(_x,x2py2))<<eol;
  nbErrors+= checkValue(tostring(integral(_x,x)), "0.5x^2", errors, "integral(_x,x)");
  nbErrors+= checkValue(tostring(integral(_y,x)), "x.y", errors, "integral(_y,x)");
  nbErrors+= checkValue(tostring(integral(_z,x)), "x.z", errors, "integral(_z,x)");
  nbErrors+= checkValue(tostring(integral(_x,x2py2)), "0.333333x^3+x.y^2", errors, "integral(_x,x2py2)");
  nbErrors+= checkValue(tostring(integral(_y,integral(_x,x2py2))), "0.333333x^3.y+0.333333x.y^3", errors, "integral(_y,_integral(_x,x2py2))");

  //!chaining
  Polynomial xz(Monomial(1,0,1),1.); //xz
  Polynomial xmz(Monomial(1,0,0),1.,Monomial(0,0,1),-1.); //x-z
  Polynomial r = x2py2(xz,xmz);  //(xz)^2+(x-z)^2 = x^2*z^2+x^2+z^2-2x*z
  ssout<<"x2py2="<<x2py2<<eol;
  ssout<<"xz="<<xz<<" xmz="<<xmz<<eol;
  ssout<<"x2py2(xz,xmz)="<<r<<eol;
  ssout<<"r(2z,y,z)="<<r(2.*z,y,z)<<eol;
  nbErrors+= checkValue(tostring(x2py2), "x^2+y^2", errors, "x2py2");
  nbErrors+= checkValue(tostring(xz), "x.z", errors, "xz");
  nbErrors+= checkValue(tostring(xmz), "x-z", errors, "xmz");
  nbErrors+= checkValue(tostring(r), "x^2.z^2+x^2-2x.z+z^2", errors, "x2py2(xz,xmz)");
  nbErrors+= checkValue(tostring(r(2.*z,y,z)), "4z^4+z^2", errors, "r(2.*z,y,z)");
  ssout<<eol;

  //!particular polynomial spaces
  ssout<<PolynomialBasis(_Pk,1,3)<<eol;
  ssout<<PolynomialBasis(_Pk,2,3)<<eol;
  ssout<<PolynomialBasis(_Pk,3,3)<<eol;
  nbErrors+= checkValues(PolynomialBasis(_Pk,1,3), rootname+"/Pk13.in",0., errors, "Pk13", check);
  nbErrors+= checkValues(PolynomialBasis(_Pk,2,3), rootname+"/Pk23.in",0., errors, "Pk23", check);
  nbErrors+= checkValues(PolynomialBasis(_Pk,3,3), rootname+"/Pk33.in",0., errors, "Pk33", check);

  PolynomialBasis pk3(_Pk,2,3);
  pk3.swapVar(2,3,1);
  ssout<<pk3<<eol;
  nbErrors+= checkValues(pk3, rootname+"/Pk13swap.in",0., errors, "Pk13 swaped", check);

  ssout<<PolynomialBasis(_PHk,1,3)<<eol;
  ssout<<PolynomialBasis(_PHk,2,3)<<eol;
  ssout<<PolynomialBasis(_PHk,3,3)<<eol;
  nbErrors+= checkValues(PolynomialBasis(_PHk,1,3), rootname+"/Phk13.in",0., errors, "Phk13", check);
  nbErrors+= checkValues(PolynomialBasis(_PHk,2,3), rootname+"/Phk23.in",0., errors, "Phk23", check);
  nbErrors+= checkValues(PolynomialBasis(_PHk,3,3), rootname+"/Phk33.in",0., errors, "Phk33", check);

  ssout<<PolynomialBasis(_Qk,1,3)<<eol;
  ssout<<PolynomialBasis(_Qk,2,3)<<eol;
  ssout<<PolynomialBasis(_Qk,3,3)<<eol;
  nbErrors+= checkValues(PolynomialBasis(_Qk,1,3), rootname+"/Qk13.in",0., errors, "Qk13", check);
  nbErrors+= checkValues(PolynomialBasis(_Qk,2,3), rootname+"/Qk23.in",0., errors, "Qk23", check);
  nbErrors+= checkValues(PolynomialBasis(_Qk,3,3), rootname+"/Qk33.in",0., errors, "Qk33", check);


  ssout<<PolynomialBasis(_Qks,3,1,2,3)<<eol;
  nbErrors+= checkValues(PolynomialBasis(_Qks,3,1,2,3), rootname+"/Qks3123.in",0., errors, "Qks3123", check);

  PolynomialBasis P2(_Pk,2,2);
  PolynomialBasis Q2(_Qk,1,2);
  ssout<<P2<<eol<<Q2<<eol;
  ssout<<P2* Q2<<eol;
  nbErrors+= checkValues(P2, rootname+"/P2.in",0., errors, "P2", check);
  nbErrors+= checkValues(Q2, rootname+"/Q2.in",0., errors, "Q2", check);
  nbErrors+= checkValues(P2* Q2, rootname+"/P2Q2.in",0., errors, "P2Q2", check);
  Q2.swapVar(3,2,1);
  ssout<<P2* Q2<<eol;
  nbErrors+= checkValues(P2* Q2, rootname+"/P2Q2_swapvar.in",0., errors, "P2Q2 swapvar", check);

  ssout<<PolynomialsBasis(_SHk,2,1)<<eol;
  ssout<<PolynomialsBasis(_SHk,3,1)<<eol;
  ssout<<PolynomialsBasis(_SHk,2,2)<<eol;
  ssout<<PolynomialsBasis(_SHk,3,2)<<eol;
  nbErrors+= checkValues(PolynomialsBasis(_SHk,2,1), rootname+"/SHk21.in",0., errors, "SHK 2,1", check);
  nbErrors+= checkValues(PolynomialsBasis(_SHk,3,1), rootname+"/SHk31.in",0., errors, "SHK 3,1", check);
  nbErrors+= checkValues(PolynomialsBasis(_SHk,2,2), rootname+"/SHk22.in",0., errors, "SHK 2,2", check);
  nbErrors+= checkValues(PolynomialsBasis(_SHk,3,2), rootname+"/SHk32.in",0., errors, "SHK 3,2", check);

  ssout<<PolynomialsBasis(_SHk,3,4)<<eol;
  nbErrors+= checkValues(PolynomialsBasis(_SHk,3,4), rootname+"/SHk34.in",0., errors, "SHK 3,4", check);

  ssout<<PolynomialsBasis(_Dk,2,2)<<eol;
  ssout<<PolynomialsBasis(_Dk,3,1)<<eol;
  ssout<<PolynomialsBasis(_Dk,3,2)<<eol;
  nbErrors+= checkValues(PolynomialsBasis(_Dk,2,2), rootname+"/Dk22.in",0., errors, "Dk 2,2", check);
  nbErrors+= checkValues(PolynomialsBasis(_Dk,3,1), rootname+"/Dk31.in",0., errors, "Dk 3,1", check);
  nbErrors+= checkValues(PolynomialsBasis(_Dk,3,2), rootname+"/Dk32.in",0., errors, "Dk 3,2", check);


  ssout<<PolynomialsBasis(PolynomialBasis(_Pk,2,1),PolynomialBasis(_Pk,2,2))<<eol;
  ssout<<PolynomialsBasis(PolynomialBasis(_Pk,3,2),PolynomialBasis(_Pk,3,1))<<eol;
  nbErrors+= checkValues(PolynomialsBasis(PolynomialBasis(_Pk,2,1),PolynomialBasis(_Pk,2,2)), rootname+"/P1xP2.in",0., errors, "P1xP2", check);
  nbErrors+= checkValues(PolynomialsBasis(PolynomialBasis(_Pk,3,2),PolynomialBasis(_Pk,3,1)), rootname+"/P2xP1.in",0., errors, "P2xP1", check);

  ssout<<PolynomialsBasis(_DQk,2,2)<<eol;
  ssout<<PolynomialsBasis(_DQk,3,2)<<eol;
  nbErrors+= checkValues(PolynomialsBasis(_DQk,2,2), rootname+"/DQk22.in",0., errors, "DQk 2,2", check);
  nbErrors+= checkValues(PolynomialsBasis(_DQk,3,2), rootname+"/DQk32.in",0., errors, "DQk 3,2", check);

  ssout<<PolynomialsBasis(_DQ2k,2,1)<<eol;
  ssout<<PolynomialsBasis(_DQ2k,2,2)<<eol;
  ssout<<PolynomialsBasis(_DQ2k,3,1)<<eol;
  ssout<<PolynomialsBasis(_DQ2k,3,2)<<eol;
  nbErrors+= checkValues(PolynomialsBasis(_DQ2k,2,1), rootname+"/DQ2k21.in",0., errors, "DQ2k 2,1", check);
  nbErrors+= checkValues(PolynomialsBasis(_DQ2k,2,2), rootname+"/DQ2k22.in",0., errors, "DQ2k 2,2", check);
  nbErrors+= checkValues(PolynomialsBasis(_DQ2k,3,1), rootname+"/DQ2k31.in",0., errors, "DQ2k 3,1", check);
  nbErrors+= checkValues(PolynomialsBasis(_DQ2k,3,2), rootname+"/DQ2k32.in",0., errors, "DQ2k 3,2", check);
  ssout<<PolynomialBasis(_Pk,2,2)<<eol;
  ssout<<dx(PolynomialBasis(_Pk,2,2))<<eol;
  ssout<<dy(PolynomialBasis(_Pk,2,2))<<eol;
  ssout<<dz(PolynomialBasis(_Pk,2,2))<<eol;
  nbErrors+= checkValues(PolynomialBasis(_Pk,2,2), rootname+"/Pk22_2.in",0., errors, "Pk 2,2 , bis", check);
  nbErrors+= checkValues(dx(PolynomialBasis(_Pk,2,2)), rootname+"/dxPk22_2.in",0., errors, "dx(Pk 2,2 , bis)", check);
  nbErrors+= checkValues(dy(PolynomialBasis(_Pk,2,2)), rootname+"/dyPk22_2.in",0., errors, "dy(Pk 2,2 , bis)", check);
  nbErrors+= checkValues(dz(PolynomialBasis(_Pk,2,2)), rootname+"/dzPk22_2.in",0., errors, "dz(Pk 2,2 , bis)", check);

  ssout<<grad(PolynomialBasis(_Pk,1,2))<<eol;
  ssout<<grad(PolynomialBasis(_Pk,2,2))<<eol;
  ssout<<grad(PolynomialBasis(_Pk,3,2))<<eol;
  nbErrors+= checkValues( grad(PolynomialBasis(_Pk,1,2)), rootname+"/gradPk12.in",0., errors, "grad(Pk 1,2 )", check);
  nbErrors+= checkValues( grad(PolynomialBasis(_Pk,2,2)), rootname+"/gradPk22.in",0., errors, "grad(Pk 2,2 )", check);
  nbErrors+= checkValues( grad(PolynomialBasis(_Pk,3,2)), rootname+"/gradPk32.in",0., errors, "grad(Pk 3,2 )", check);

  ssout<<PolynomialsBasis(_SHk,3,2)<<eol;
  ssout<<curl(PolynomialsBasis(_SHk,3,2))<<eol;
  ssout<<div(PolynomialsBasis(_SHk,3,2))<<eol;
  nbErrors+= checkValues(PolynomialsBasis(_SHk,3,2), rootname+"/SHk32_2.in",0., errors, "SHK 3,2 bis", check);
  nbErrors+= checkValues(curl(PolynomialsBasis(_SHk,3,2)), rootname+"/curlSHk32_2.in",0., errors, "curl(SHK 3,2 bis)", check);
  nbErrors+= checkValues(div(PolynomialsBasis(_SHk,3,2)), rootname+"/divSHk32_2.in",0., errors, "div(SHK 3,2 bis)", check);

  ssout<<"evaluate SHk_3_2 at point(1,2,3)"<<eol<<PolynomialsBasis(_SHk,3,2)(1,2,3)<<eol;
  ssout<<"evaluate curl(SHk_3_2) at point(1,2,3)"<<eol<<curl(PolynomialsBasis(_SHk,3,2))(1,2,3)<<eol;
  nbErrors+= checkValue(tostring(PolynomialsBasis(_SHk,3,2)(1,2,3)), "[ [ 4 -2 0 ] [ 2 -1 0 ] [ 9 0 -3 ] [ 6 0 -2 ] [ 3 0 -1 ] [ 0 9 -6 ] [ 0 3 -2 ] [ 0 6 -4 ] ]", errors, "evaluate SHk_3_2) at point(1,2,3)");
  nbErrors+= checkValue(tostring(curl(PolynomialsBasis(_SHk,3,2))(1,2,3) ), "[ [ 0 0 -6 ] [ 0 0 -3 ] [ 0 9 0 ] [ -1 4 -3 ] [ 0 3 0 ] [ -9 0 0 ] [ -2 2 3 ] [ -6 0 0 ] ]", errors, "evaluate curl(SHk_3_2) at point(1,2,3)");
 
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
