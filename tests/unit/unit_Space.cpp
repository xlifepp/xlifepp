/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
   \file unit_Space.cpp
   \author E. Lunéville
   \since 23 feb 2012
   \date 11 may 2012

   Low level tests of Space class and related classes.
   Almost functionalities are checked.
   This function may either creates a reference file storing the results (check=true)
   or compares results to those stored in the reference file (check=true)
   It returns reporting informations in a string
*/

//===============================================================================
//library dependencies
#include "xlife++-libs.h"
#include "testUtils.hpp"
//===============================================================================
//stl dependencies
#include <iostream>
#include <fstream>
#include <vector>
//===============================================================================

using namespace xlifepp;

namespace unit_Space {

//! spectral functions
Real sin_n(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Number n = pa.basisIndex();       // get the index of function to compute (Parameters way)
  //Number n = getBasisIndex();     // get the index of function to compute (ThreadData way)
  return sqrt(2. / h) * sin(n * pi_ * x / h); // computation
}

//! spectral functions
Vector<Real> sin_cos_n(const Point& P, Parameters& pa = defaultParameters)
{
  Real x = P.x();
  Real h = pa("h");                 // get the parameter h (user definition)
  Number n = pa.basisIndex();       // get the index of function to compute (Parameters way)
  //Number n = getBasisIndex();     // get the index of function to compute (ThreadData way)
  Vector<Real> S(2);
  S(1) = sqrt(2. / h) * sin(n * pi_ * x / h); // computation
  S(2) = sqrt(2. / h) * cos(n * pi_ * x / h);
  return S;
}

void unit_Space(int argc, char* argv[], bool check)
{
  String rootname = "unit_Space";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);

  Number nbErrors = 0;
  String errors;
  Real eps=0.0001;

  //! test of spectral space given by a Function
  //!------------------------------------------!
  SquareGeo sq(_center=Point(0.,0.), _length=1., _nnodes=4, _domain_name="Omega");
  Mesh M(sq, _shape=triangle, _order=1);
  // GeomDomain Omega(M, "Omega", 2);
  Domain Omega=M.domain("Omega");
  Parameters ps(1., "h");
  int n = 10;
  Space V(_domain=Omega, _basis=Function(sin_n, "sin_n", ps), _dim=n, _name="sinus basis");
  const SpSpace& spV=*V.spSpace();
  ssout << V << std::endl;
  Real r = 0;
  Point x(0.25); ssout << "p=" << x << std::endl;
  ssout << "V.spectralFun(0,x,r)=" << spV.spectralFun(0, x, r) << std::endl;
  ssout << "V.spectralFun(1,x,r)=" << spV.spectralFun(2, x, r) << std::endl;
  ssout << "V.spectralFun(2,x,r)=" << spV.spectralFun(3, x, r) << std::endl;
  Vector<Real> vr;
  ssout << "V.spectralFun(x,vr)=" << spV.spectralFun(x, vr) << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spectralspacegivenbyaFunction.in", errors, "spectral space given by a Function", check);

  //! test of spectral space given by a TermVectors
  //!------------------------------------------
  Space Vk(_domain=Omega, _interpolation=_P1, _name="Vk", _notOptimizeNumbering);
  Unknown utvs(Vk, _name="utvs");
  TermVectors tvs(n);
  for (Number i=1; i <= n; i++)
  {
    Parameters psn(i-1, "basis index");
    psn << Parameter(1., "h");
    Function fct_n(sin_n, "sin_n", psn);
    tvs(i)=TermVector(utvs, Omega, fct_n);
  }
  Space Vtvs(_basis=tvs, _name="sinus tvs basis");
  const SpSpace& spVtvs=*Vtvs.spSpace();
  ssout << Vtvs << std::endl;
  // ssout << "p=" << x << std::endl;
  // ssout << "Vtvs.spectralFun(0,x,r)=" << spVtvs.spectralFun(0, x, r) << std::endl;
  // ssout << "Vtvs.spectralFun(1,x,r)=" << spVtvs.spectralFun(2, x, r) << std::endl;
  // ssout << "Vtvs.spectralFun(2,x,r)=" << spVtvs.spectralFun(3, x, r) << std::endl;
  // ssout << "V.spectralFun(x,vr)=" << spVtvs.spectralFun(x, vr) << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spectralspacegivenbyaTermVectors.in", errors, "spectral space given by a TermVectors", check);

  //! test of Unknown and TestFunction
  Unknown p(V, _name="p");
  ssout << "Unknown p : " << p << std::endl;
  TestFunction q(p, _name="q");
  ssout << "TestFunction q : " << q << std::endl;

  Space W(_domain=Omega, _basis=Function(sin_cos_n, "sin_cos", ps), _dim=n, _basis_dim=2, _name="(sinus cosinus)");
  Unknown u(W, _name="u");
  ssout << "Test u[1]: " << u[1] << std::endl;
  ssout << "Test u[2]: " << u[2] << std::endl;

  nbErrors+=checkValue(ssout, rootname+"/unknowntestFunction.in", errors, "Unknown and TestFunction", check);

  //! test of Fe space
  //!------------------
  //!create a mesh and Domain
  std::vector<String> sidenames(4,"");
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  Mesh mesh2d(inputsPathTo(rootname+"/mesh2d.msh"), _name="mesh of [0,1]x[0,3]");
  ssout << mesh2d << std::endl;
  ssout <<"\n------------ P1 mesh of a rectangle with sides----------------------\n";
  mesh2d.buildSides();
  mesh2d.buildVertexElements();
  verboseLevel(9);
  ssout<<mesh2d << std::endl;

  ssout<<"\n------------------------------ domains------------------------------\n";
  Domain omega=mesh2d.domain("Omega");
  Domain gamma1=mesh2d.domain("Gamma_1");
  Domain gamma2=mesh2d.domain("Gamma_2");
  ssout<<"Domain omega : " << omega << std::endl;
  ssout<<"Domain gamma1 : " << gamma1 << std::endl;
  ssout<<"Domain gamma2 : " << gamma2 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/MeshP1.in", errors, "Mesh P1", check);

  ssout<<"\n------------------------- space definition P1 --------------------------\n";
  verboseLevel(5);
  Interpolation* LP1=findInterpolation(Lagrange,standard,1,H1);
  Space V1(_domain=omega, _FE_type=Lagrange, _FE_subtype=standard, _order=1, _Sobolev_type=H1, _name="V1", _notOptimizeNumbering);
  ssout << V1 << std::endl;
  ssout << "\n----------------------- space P1 with renumbering ----------------------\n";
  std::pair<Number,Number> skys=V1.renumberDofs();
  ssout << " skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  ssout << V1 << std::endl;
  ssout << " ----- subspaces on Gamma1 and Gamma2-----\n";
  Space V1_G1(gamma1,V1,"V1|gamma1");
  ssout << V1_G1 << std::endl;
  Space V1_G2(gamma2,V1,"V1|gamma2");
  ssout << V1_G2 << std::endl;
  verboseLevel(5);
  GeomDomain gamma12(_union,gamma1,gamma2);
  ssout << "test union of domains : " << gamma12 << std::endl;
  Space V1_G12(gamma12,V1,"V1|gamma12");
  ssout << V1_G12 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceMeshP1.in", errors, "Space of Mesh P1", check);

  verboseLevel(5);
  ssout<<"\n----------------------- P1/Q1 mesh  ------------------------\n";
  //! Mesh mesh2dP1Q1(Rectangle(_xmin=0,_xmax=1,_ymin=1,_ymax=3,_nnodes=Numbers(3,5),_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[1,3]");
  Mesh mesh2dP1Q1(inputsPathTo(rootname+"/mesh2dP1Q1.msh"), _name="mesh of [0,1]x[0,3]");
  ssout << mesh2dP1Q1 << std::endl;
  sidenames[0]="Sigma_1";
  sidenames[3]="Sigma_2";
  //! Mesh mesh2dQ1(Rectangle(_xmin=1,_xmax=2,_ymin=1,_ymax=3,_nnodes=Numbers(3,5),_side_names=sidenames), _shape=_quadrangle, _order=1, _generator=_structured, _name="Q1 mesh of [1,2]x[1,3]");
  Mesh mesh2dQ1(inputsPathTo(rootname+"/mesh2dQ1.msh"), _name="Q1 mesh of [1,2]x[1,3]");
  Mesh mesh2dP1Q1merged(inputsPathTo(rootname+"/mesh2dP1Q1merged.msh"), _name="Q1 mesh of [0,2]x[0,3]");
  ssout << mesh2dP1Q1merged << std::endl;
  Domain omgP1Q1=mesh2dP1Q1merged.domain("Omega");
  ssout << omgP1Q1 << std::endl;
  Space H(_domain=omgP1Q1, _interpolation=_P1, _name="H", _notOptimizeNumbering);
  ssout << H << std::endl;
  Space H_omega(mesh2dP1Q1merged.domain("Omega"),H,"H|omega");
  ssout << H_omega << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceMeshP1Q1.in", errors, "Space of Mesh P1/Q1", check);

  ssout<<"\n------------------------- space definition P2 --------------------------\n";
  Space V2(_domain=omega, _interpolation=_P2, _name="V2", _notOptimizeNumbering);
  ssout << V2 << std::endl;
  ssout<<"\n----------------------- space P2 with renumbering ----------------------\n";
  skys=V2.renumberDofs();
  ssout<<" skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  skys=V2.renumberDofs();
  ssout<<" skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  ssout << V2 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceP2.in", errors, "Space P2", check);

  ssout<<"\n----------------------- space P3 with renumbering ----------------------\n";
  Space V3(_domain=omega, _interpolation=_P3, _name="V3");  //with dof numbering optimisation (third argument is true by default)
  ssout << V3 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceP3.in", errors, "Space P3", check);


  ssout<<"\n------------------------- space definition P4 --------------------------\n";
  verboseLevel(9);
  Space V4(_domain=omega, _FE_type=Lagrange, _order=4, _name="V4", _notOptimizeNumbering);
  ssout << V4 << std::endl;
  ssout<<"\n----------------------- space P4 with renumbering ----------------------\n";
  skys=V4.renumberDofs();
  ssout<<" skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  ssout << V4 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceP4.in", errors, "Space P4", check);


  //!Qk test
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  Mesh  mesh2dQ(inputsPathTo(rootname+"/mesh2dQ.msh"), _name="Q1 mesh of [0,1]x[0,3]");
  ssout<<"\n\n"<<mesh2dQ;
  ssout<<"\n------------ Q1 mesh of a rectangle with sides----------------------\n";
  mesh2dQ.buildSides();
  mesh2dQ.buildVertexElements();
  ssout << mesh2dQ << std::endl;
  ssout<<"\n------------------------------ domains------------------------------\n";
  Domain omg=mesh2dQ.domain("Omega");
  Domain gam1=mesh2dQ.domain("Gamma_1");
  Domain gam2=mesh2dQ.domain("Gamma_2");
  ssout << "Domain omg : " << omg << std::endl;
  ssout << "Domain gam1 : " << gam1 << std::endl;
  ssout << "Domain gam2 : " << gam2 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceMeshQ1.in", errors, "Space Mesh Q1", check);


  ssout<<"\n------------------------- space definition Q1 --------------------------\n";
  verboseLevel(5);
  Interpolation* LQ1=findInterpolation(Lagrange,standard,1,H1);
  Space W1(_domain=omg, _interpolation=_Q1, _name="W1", _notOptimizeNumbering);
  ssout<<W1 << std::endl;
  ssout<<"\n----------------------- space Q1 with renumbering ----------------------\n";
  skys=W1.renumberDofs();
  ssout<<" skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  ssout<<W1 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceQ1.in", errors, "Space Q1", check);


  ssout<<"\n------------------------- space definition Q2 --------------------------\n";
  Space W2(_domain=omg, _interpolation=_Q2, _name="W2", _notOptimizeNumbering);  //using shortcut constructor
  ssout<<W2 << std::endl;
  ssout<<"\n----------------------- space Q2 with renumbering ----------------------\n";
  skys=W2.renumberDofs();
  ssout<<" skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  skys=W2.renumberDofs();
  ssout<<" skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  ssout<<W2 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceQ2.in", errors, "Space Q2", check);

  ssout<<"\n----------------------- space Q3 with renumbering ----------------------\n";
  Space W3(_domain=omg, _interpolation=_Q3, _name="W3");  //with dof numbering optimisation (third argument is true by default)
  ssout<<W3 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceQ3.in", errors, "Space Q3", check);

  ssout<<"\n------------------------- space definition Q4 --------------------------\n";
  verboseLevel(9);
  Space W4(_domain= omg, _FE_type=Lagrange, _order=4, _name="W4", _notOptimizeNumbering);
  ssout<<W4 << std::endl;
  ssout<<"\n----------------------- space Q4 with renumbering ----------------------\n";
  skys=W4.renumberDofs();
  ssout<<" skyline size before optimisation = "<<skys.first<<" after optimisation = "<<skys.second<<"\n";
  ssout<<W4 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceQ4.in", errors, "Space Q4", check);


  //!Qk test in 3D
  sidenames.resize(6);
  sidenames[0]="Gamma_1";
  sidenames[3]="Gamma_2";
  //! Mesh mesh3dQ(Cuboid(_xmin=0,_xmax=1,_ymin=1,_ymax=3,_zmin=2,_zmax=4,_nnodes=Numbers(3,5,3),_side_names=sidenames), _shape=_hexahedron, _order=1, _generator=_structured, _name="Q1 mesh of [0,1]x[1,3]x[2,4]");
  Mesh mesh3dQ(inputsPathTo(rootname+"/mesh3dQ.msh"), _name="Q1 mesh of [0,1]x[1,3]x[2,4]");
  ssout<<"\n\n"<<mesh3dQ;
  ssout<<"\n------------ Q1 mesh of a rectangle with sides----------------------\n";
  mesh3dQ.buildSides();
  mesh3dQ.buildVertexElements();
  ssout<<mesh3dQ << std::endl;
  ssout<<"\n------------------------------ domains------------------------------\n";
  Domain o=mesh3dQ.domain("Omega");
  Domain g1=mesh3dQ.domain("Gamma_1");
  Domain g2=mesh3dQ.domain("Gamma_2");
  ssout<<"Domain omg : " <<o << std::endl;
  ssout<<"Domain gam1 : " <<g1 << std::endl;
  ssout<<"Domain gam2 : " <<g2 << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/spaceMesh3D.in", errors, "Space Mesh 3D", check);

  //! test of merging (sub)spaces
  verboseLevel(5);
  Space* V1_g1p=&V1_G1, * V1_g2p=&V1_G2;
  ssout<< "V1_G1 : " << *V1_g1p << std::endl;
  ssout<< "V1_G2 : " << *V1_g2p << std::endl;
  Space* V1_g1g2p=mergeSubspaces(V1_g1p,V1_g2p, true);
  ssout<< "merge V1_G1 et V1_G2 : " << *V1_g1g2p << std::endl;
  ssout<< "V1_G1 : " << *V1_g1p << std::endl;
  ssout<< "V1_G2 : " << *V1_g2p << std::endl;

  Space * V1p= &V1;
  Space * W1p= mergeSubspaces(V1p,V1_g1p);
  ssout<< "merge V1 et V1_G1 : "<<*W1p << std::endl;
  nbErrors+=checkValue(ssout, rootname+"/mergeSubSpaces.in", errors, "Merge Subspaces", check);

  ssout<<"\n------------------------------ 3D space ------------------------------\n";
  std::vector<String> siden(6,"");
  siden[0]="x=0";
  Mesh mesh3d(inputsPathTo(rootname+"/mesh3d.msh"), _name="mesh of [0,1]^3");
  verboseLevel(30);
  ssout<<"\n"<<mesh3d << std::endl;
  Domain omg3=mesh3d.domain("Omega");
  Domain sigx0=mesh3d.domain("x=0");
  ssout<<omg3 << std::endl <<sigx0<<eol;
  nbErrors+=checkValue(ssout, rootname+"/DomainGmsh.in", errors, "Domain GMSH", check);

  Space W3P1(_domain=omg3, _interpolation=_P1, _name="W3P1");
  ssout<<"\n------------------------------ 3D space P1 ------------------------------\n"<<W3P1<<eol;
  nbErrors+=checkValue(ssout, rootname+"/W3P1.in", errors, "Space W3 P1 (gmsh)", check);
  Space W3P2(_domain=omg3, _interpolation=_P2, _name="W3P2");
  ssout<<"\n------------------------------ 3D space P2 ------------------------------\n"<<W3P2<<eol;
  nbErrors+=checkValue(ssout, rootname+"/W3P2.in", errors, "Space W3 P2 (gmsh)", check);
  Space W3P3(_domain=omg3, _interpolation=_P3, _name="W3P3");
  ssout<<"\n------------------------------ 3D space P3 ------------------------------\n"<<W3P3<<eol;
  nbErrors+=checkValue(ssout, rootname+"/W3P3.in", errors, "Space W3 P3 (gmsh)", check);
  Space W3P4(_domain=omg3, _interpolation=_P4, _name="W3P4");
  ssout<<"\n------------------------------ 3D space P4 ------------------------------\n"<<W3P4<<eol;
  nbErrors+=checkValue(ssout, rootname+"/W3P4.in", errors, "Space W3 P4 (gmsh)", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
