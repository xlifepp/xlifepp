/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_HMatrix.cpp
	\author E. Lunéville
	\since 17 may 2016
	\date 17 may 2016

	Low level tests of Hmatrix class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "hierarchicalMatrix.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

#undef __STRICT_ANSI__ // _controlfp is a non-standard function documented in MSDN
#include <float.h>
#include <stdio.h>

using namespace xlifepp;

namespace unit_HMatrix
{

void unit_HMatrix(int argc, char* argv[], bool check)
{
  String rootname = "unit_HMatrix";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);
  verboseLevel(5);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!test on a sphere
  Mesh  meshd(inputsPathTo(rootname+"/meshd.msh"), _name="mesh of the unit sphere"); 
  Domain omega=meshd.domain("Omega");
  Space W(_domain=omega, _interpolation=_P0, _name="V", _notOptimizeNumbering);
  Unknown u(W, _name="u"); TestFunction v(u, _name="v");
  Vector<Real> x(W.dimSpace(),1.);
  Kernel Gl=Laplace3dKernel();
  std::cout << "**********************" << eol;
  IntegrationMethods ims(_method=SauterSchwab, _order=5, _bound=0.,_quad=_defaultRule, _order=5, _bound=2.,_quad=_defaultRule, _order=4, _bound=4.,_quad=_defaultRule, _order=3);
  std::cout << ims << eol;
  IntegrationMethods ims2(_method=SauterSchwab, _order=5, _quad=_defaultRule, _order=5, _bound=2., _quad=_defaultRule, _order=4, _bound=4., _quad=_defaultRule, _order=3);
  std::cout << ims2 << eol;
  IntegrationMethods ims3(_method=SauterSchwab, _order=5, _quad=_defaultRule, _order=4, _bound=4., _quad=_defaultRule, _order=5, _bound=2., _quad=_defaultRule, _order=3);
  std::cout << ims3 << eol;
  IntegrationMethods ims4(_quad=_defaultRule, _order=5, _bound=2., _method=SauterSchwab, _order=5, _quad=_defaultRule, _order=4, _bound=4., _quad=_defaultRule, _order=3);
  std::cout << ims4 << eol;
  IntegrationMethods ims5(_quad=_defaultRule, _order=5, _bound=2., _quad=_defaultRule, _order=4, _bound=4., _quad=_defaultRule, _order=3, _method=SauterSchwab, _order=5);
  std::cout << ims5 << eol;
  std::cout << "**********************" << eol;
  // return;
  Number nbp=50;

  ssout<<"----------------- Laplace kernel single layer, dense matrix computation -------------------"<<eol;
  BilinearForm bf=intg(omega,omega,u*Gl*v,_method=ims,_symmetry=_noSymmetry);
  elapsedTime();
  TermMatrix A(bf,_storage=denseRow, _name="A");
  elapsedTime("build dense matrix");
  LargeMatrix<Real>& Alm=A.getLargeMatrix<Real>();
  Vector<Real> Ax;
  elapsedTime();
  for(Number k=0; k<nbp; ++k) Ax=Alm*x;
  ssout<<"dense matrix ("<<Alm.nbNonZero()<<") |A*x| = "<<norm(Ax)<<eol;
  elapsedTime("dense matrix x vector");
  nbErrors+=checkValue( numToInt(Alm.nbNonZero()), 1638400,errors, "non zero of Alm");
  nbErrors+=checkValue(norm(Ax), 0.350193049045, 0.0001, errors, "|A*x|");

  Number nbox=10;
  ClusterTree<FeDof> ct(W.feSpace()->dofs,_cardinalityBisection,nbox);
  elapsedTime("create Cluster tree");

  HMatrix<Real,FeDof> hm(ct,ct,nbox,nbox);
  elapsedTime("create HMatrix");
  //! load HMatrix from LargeMatrix, ct.saveToFile("cluster_sphere.txt"), hm.saveStructureToFile("hmatrix.dat");
  hm.load(Alm);  
  elapsedTime("load HMatrix from LargeMatrix");

  //!test matrix vector product
  //!==========================
  elapsedTime();
  Vector<Real> amx;
  for(Number k=0; k<50; k++) amx=Alm*x;
  elapsedTime("A*x");
  HMatrixNode<Real,FeDof>::initCounter();
  Vector<Real> hmx;
  for(Number k=0; k<5; k++)  hmx = hm*x;
  elapsedTime("hm*x");

  HMatrixNode<Real,FeDof>::stopCounter();
  ssout<<"|amx-hmx| = "<<norm(amx-hmx)<<" matrixVectorProductCount = "<<HMatrixNode<Real,FeDof>::counter<<eol;
  //!test get leaves
  std::list<HMatrixNode<Real,FeDof>* > leaves=hm.getLeaves();
  std::list<HMatrixNode<Real,FeDof>* >::iterator itl=leaves.begin();
  ssout<<" number of leaves = "<<leaves.size()<<eol;
  nbErrors+=checkValue(norm(amx-hmx), 4.27055661404e-16, 1e-8, errors, "|amx-hmx|");
  nbErrors+=checkValue( numToInt(leaves.size()), 3952,errors, "number of leaves");

  Number nt=20;
  Number unsafe=0;
  std::list<HMatrixNode<Real,FeDof>* > oleaves=hm.getLeaves(_row,nt,unsafe);
  ssout<<"list of leaves with nt="<<nt<<" unsafe = "<<unsafe
     <<" number of safe packets = "<<(oleaves.size()-unsafe)/nt<<eol;
  itl=oleaves.begin();

  ssout<<"norm2(am)="<<norm2(Alm)<<" norm2(hm)="<<hm.norm2()<<eol;
  nbErrors+=checkValue( numToInt(nt), 20,errors, "nt for list of leaves");
  nbErrors+=checkValue( numToInt(unsafe), 32, errors, "unsafe");
  nbErrors+=checkValue( numToInt((oleaves.size()-unsafe)/nt) , 196, errors, "number of safe pockets");
  nbErrors+=checkValue(norm2(Alm), 0.0155367669251, 1e-8, errors, "||Alm||");
  nbErrors+=checkValue(hm.norm2(), 0.0155367669251, 1e-8, errors, "||hm||");

  //!test HMatrixNode stuff
  //!======================
  verboseLevel(20);

  Vector<Real>::iterator ity;
  HMatrixNode<Real,FeDof>* hno=hm.child(5);
  ssout<<"----------------- HNode product -------------------"<<eol;

  Vector<Real> y0=Ax, y=Ax;
  y0*=0; hno->multMatrixVector(x,y0);

  ssout<<"norm2(hno*x)="<<norm2(y0)<<eol;
  ssout<<eol;
  nbErrors+=checkValue(norm2(y0), 0.0184721373876, 1e-6, errors, "||hno*x||");

  ssout<<"----------------- HNode hierarchical to LargeMatrix -------------------"<<eol;
  hno->toLargeMatrix();
  y*=0; hno->multMatrixVector(x,y);
  ssout<<"norm2(hno*x-Ax)="<<norm2(y-y0)<<eol;
  nbErrors+=checkValue(norm2(y-y0), 2.07986038258e-18, 1e-6, errors, "||hno*x-Ax||_1");

  ssout<<"----------------- HNode LargeMatrix to Hierarchical -------------------"<<eol;
  hno->toHierarchical();
  y*=0; hno->multMatrixVector(x,y);
  ssout<<"norm2(hno*x-Ax)="<<norm2(y-y0)<<eol;
  nbErrors+=checkValue(norm2(y-y0), 1.65140754931e-18, 1e-6, errors, "||hno*x-Ax||_2");

#ifdef XLIFEPP_WITH_EIGEN
  ssout<<"----------------- HNode hierarchical to LowRankMatrix -------------------"<<eol;
  hno->toApproximateMatrix(_lowRankApproximation,0,1.E-04);
  y*=0; hno->multMatrixVector(x,y);
  ssout<<"norm2(hno*x-Ax)="<<norm2(y-y0)<<eol;
  nbErrors+=checkValue(norm2(y-y0), 0.000186832, 1e-3, errors, "||hno*x-Ax||_3");

  ssout<<"----------------- HNode LowRankMatrix to hierarchical -------------------"<<eol;
  hno->toHierarchical();
  y*=0; hno->multMatrixVector(x,y);
  ssout<<"norm2(hno*x-Ax)="<<norm2(y-y0)<<eol;
  nbErrors+=checkValue(norm2(y-y0),0.001, 0.006, errors, "||hno*x-Ax||_4");
#endif

  //!test building Hmatrix from bilinear form
  //!========================================
  Number sb=10;
  HMatrixIM him0(_clustering_method=cardinalityBisection, _min_block_size={sb, sb}, _threshold=0., _method=ims);

  //! no approximation
  ssout<<"----------------- Laplace kernel single layer, Hmatrix computation -------------------"<<eol;
  theCout<<"----------------- Laplace kernel single layer, Hmatrix computation -------------------"<<eol;
  BilinearForm bfhm0=intg(omega,omega,u*Gl*v,_method=him0);
  elapsedTime();
  TermMatrix Ahm0(bfhm0, _name="Ahm0");
  elapsedTime("build HMatrix exact");
  HMatrix<Real,FeDof>& hmA0=Ahm0.getHMatrix<Real>();
  Vector<Real> Ahm0x;
  elapsedTime();
  for(Number k=0; k<nbp; ++k) Ahm0x=hmA0*x;
  elapsedTime("exact Hmatrix x vector");
  ssout<<"exact HM "; hmA0.printSummary(ssout); ssout<<" |A*x- hmA0*x| = "<<norm(Ax-Ahm0x)<<eol;
  nbErrors+=checkValue(numToInt(hmA0.nbNonZero() ), 825600, errors, "hmA0.nbNonZero");
  nbErrors+=checkValue(numToInt(hmA0.depth), 7, errors, "hmA0.depth");


#ifdef XLIFEPP_WITH_EIGEN
  //!test compression methods
  DofCluster rowCluster=him0.rowCluster(), colCluster=him0.colCluster();
  HMatrixIM him_svd(_approximation_method=svdCompression, _threshold=0.000001, _method=ims, _row_cluster=rowCluster, _col_cluster=colCluster);
  BilinearForm bfhm_svd=intg(omega,omega,u*Gl*v,_method=him_svd);
  elapsedTime();
  TermMatrix Ahm_svd(bfhm_svd, _name="Ahm_svd");
  elapsedTime("build HMatrix svd compression");
  HMatrix<Real,FeDof>& hmA_svd=Ahm_svd.getHMatrix<Real>();
  Vector<Real> Ahm_svdx;
  elapsedTime();
  for(Number k=0; k<nbp; ++k) Ahm_svdx=hmA_svd*x;
  elapsedTime("exact Hmatrix svd x vector");
  ssout<<"svd HM "; hmA_svd.printSummary(ssout); ssout<<" |A*x- Ahm_svd*x| = "<<norm(Ax-Ahm_svdx)<<eol;
#endif
  /*
  //!test with a combination of a HMatrix and a LargeMatrix
  ssout<<"----------------- Laplace kernel double layer - Id, dense matrix computation -------------------"<<eol;
  theCout<<"----------------- Laplace kernel double layer - Id, dense matrix computation -------------------"<<eol;
  BilinearForm blm=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=ims)-0.5*intg(omega,u*v);
  TermMatrix B(blm, _name="B");
  elapsedTime("build dense matrix");
  Vector<Real> Bx;
  LargeMatrix<Real>& Blm=B.getLargeMatrix<Real>();
  elapsedTime();
  for(Number k=0; k<nbp; ++k) Bx=Blm*x;
  ssout<<"|B*x| = "<<norm(Bx)<<eol;
  nbErrors+=checkValue(norm(Bx), 0.350862422869, 1e-6, errors, "||Bx||");
  elapsedTime("dense matrix x vector");

  ssout<<"----------------- Laplace kernel double layer - Id, Hmatrix computation -------------------"<<eol;
  theCout<<"----------------- Laplace kernel double layer - Id, Hmatrix computation -------------------"<<eol;
  BilinearForm blm0=intg(omega,omega,u*ndotgrad_y(Gl)*v,_method=him0)-0.5*intg(omega,u*v);
  TermMatrix B0(blm0, _name="B0");
  elapsedTime("build exact HM/LM matrix");
  Vector<Real> B0x;
  HMatrix<Real,FeDof>& B0hm=B0.getHMatrix<Real>();
  elapsedTime();
  for(Number k=0; k<nbp; ++k) B0x=B0hm*x;
  ssout<<"exact HM/LM |B*x-B0*x| = "<<norm(Bx-B0x)<<eol;
  nbErrors+=checkValue(norm(Bx-B0x), 9.03223055164e-17, 1e-6, errors, "exact HM/LM  |Bx-B0x|");
  elapsedTime("exact HM/LM matrix x vector");

  //!test for Helmholtz Kernel
  ssout<<"----------------- Helmholtz kernel single layer, dense matrix computation -------------------"<<eol;
  theCout<<"----------------- Helmholtz kernel single layer, dense matrix computation -------------------"<<eol;
  Kernel Gh=Helmholtz3dKernel(2.);
  Vector<Complex> z(W.dimSpace(),Complex(1.));
  BilinearForm cblm=intg(omega,omega,u*Gh*v,_method=ims);
  TermMatrix C(cblm, _name="B");
  elapsedTime("build full matrix");
  Vector<Complex> Cx;
  LargeMatrix<Complex>& Clm=C.getLargeMatrix<Complex>();
  elapsedTime();
  for(Number k=0; k<nbp; ++k) Cx=Clm*z;
  ssout<<"|C*x| = "<<norm(Cx)<<eol;
  elapsedTime("full matrix x vector");
  nbErrors+=checkValue(norm(Cx), 0.160097752465, 1e-6, errors, "|Cx|");

  //! no approximation
  ssout<<"----------------- Helmholtz kernel single layer, Hmatrix computation -------------------"<<eol;
  theCout<<"----------------- Helmholtz kernel single layer, Hmatrix computation -------------------"<<eol;
  BilinearForm cbfhm0=intg(omega,omega,u*Gh*v,_method=him0);
  elapsedTime();
  TermMatrix Chm0(cbfhm0, _name="Chm0");
  elapsedTime("build HMatrix exact");
  HMatrix<Complex,FeDof>& hmC0=Chm0.getHMatrix<Complex>();
  Vector<Complex> Chm0x;
  elapsedTime();
  for(Number k=0; k<nbp; ++k) Chm0x=hmC0*z;
  elapsedTime("exact Hmatrix x vector");
  ssout<<"exact HM/LM |C*x- hmC0*x| = "<<norm(Cx-Chm0x)<<eol;
  nbErrors+=checkValue(norm(Cx-Chm0x), 2.903733444e-08, 1e-6, errors, "exact HM/LM  |Cx-Chm0x|");
*/
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
