/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_DtN_scalar.cpp
	\since 14 nov 2013
	\date 14 nov 2013

	Scalar DtN test using Helmholtz operator

                         dn(u)=0
     y=1/2  --------------------------------
	        |                              |
            |                              |
  dn(u)=-ik |      delta(u)+k2.u=0         | dn(u)=Tu
	        |                              |
	        |                              |
	 y=-1/2 --------------------------------
           x=0           dn(u)=0          x=pi
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_DtN_scalar {

Real k=1;

//!data on sigma-
Complex gp(const Point& P, Parameters& pa = defaultParameters)
{
  return -i_*k;
}

//!cos basis
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                   // get the parameter h (user definition)
  Number n=getBasisIndex()-1;       // get the index of function to compute - NEW METHOD -
  if (n==0) return std::sqrt(1./h);
  else      return std::sqrt(2./h)*std::cos(n*pi_*(y+h*0.5)/h);
}

//!3D-vector cos basis
Vector<Real> vcosn(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                    //get the parameter h (user definition)
  Number n=pa.basisIndex()-1;         //get the index of function to compute
  Vector<Real> res(3,0.);
  if (n==0) res(1) = sqrt(1./h);
  else      res(1) = sqrt(2./h)*std::cos(n*pi_*(y+h*0.5)/h);
  res(2)=2*res(1);
  res(3)=3*res(1);
  return res;
}

//!2D-vector cos basis
Vector<Real> v2cosn(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                    //get the parameter h (user definition)
  Int n=pa.basisIndex()-1;         //get the index of function to compute
  Vector<Real> res(2,0.);
  if(n==0) res(1) = sqrt(1./h);
  else     res(1) = sqrt(2./h)*std::cos(n*pi_*(y+h*0.5)/h);
  res(2)=2*res(1);
  return res;
}

//!map  sigma+ -> sigma-
Vector<Real> mapP(const Point& P, Parameters& pa = defaultParameters)
{
  Point P2(P.x(),P.y());
  Real theta=pi_/3, h=0.5, l1=3., l2=1.5;
  Real c=std::cos(theta), s=std::sin(theta);
  Point A(0.,0.);
  Point B=A;
  B(1)+=l1;
  Point C=B;
  C(1)+=l2*c;
  C(2)+=l2*s;
  Point C1=C;
  C1(1)+=h*s;
  C1(2)-=h*c;
  Point Q=P2-C1;
  Vector<Real> R(P.dim(),0.);
  R(1)=Q(1)*c+Q(2)*s;
  R(2)=Q(2)*c-Q(1)*s;
  R(2)-=h;
  return R;
}

//!map  sigmaP -> section [-0.5,0.5] (1D!)
Vector<Real> mapS(const Point& P, Parameters& pa = defaultParameters)
{
  Point P2(P.x(),P.y());
  Real theta=pi_/3, h=0.5, l1=3., l2=1.5;
  Real c=std::cos(theta), s=std::sin(theta);
  Point A(0.,0.);
  Point B=A;
  B(1)+=l1;
  Point C=B;
  C(1)+=l2*c;
  C(2)+=l2*s;
  Point C1=C;
  C1(1)+=h*s;
  C1(2)-=h*c;
  Point Q=P2-C1;
  Vector<Real> R(P.dim(),0.);
  R(1)=Q(1)*c+Q(2)*s;
  R(2)=Q(2)*c-Q(1)*s;
  R(2)-=h;
  return Vector<Real>(1,R(2));
}


Complex modeplan(const Point& P, Parameters& pa = defaultParameters)
{
  return exp(i_*k*P(1));
}

void unit_DtN_scalar(int argc, char* argv[], bool check)
{
  String rootname = "unit_DtN_scalar";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(10);
  String errors;
  Number nbErrors = 0;
  numberOfThreads(1);
  Real eps=0.0001;

  //!create a mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0";
  sidenames[1] = "x=pi";
  sidenames[2] = "y=h";
  sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=pi_,_ymin=-0.5,_ymax=0.5,_nnodes=Numbers(99,33),_side_names=sidenames), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=pi");
  Domain sigmaM=mesh2d.domain("x=0");

  //!create interpolation
  Space V(_domain=omega,_FE_type=Lagrange, _FE_subtype=standard, _order=2, _Sobolev_type=H1, _name="V");
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");

  //!parameters
  Real h=1;
  Parameters params(h,"h");
  params<<Parameter(k,"k");
  params<<Parameter(1,"basis index");

  //!create spectral space and dtn kernel
  Number N=10;
  Vector<Complex> lambda(N);
  for(Number n=0; n<N; n++) lambda[n]=sqrt(Complex(k*k-n*n*pi_*pi_/(h*h)));
  Space Sp(_domain=sigmaP, _basis=Function(cosny,params), _dim=N, _name="cos(n*pi*y)");
  Unknown phiP(Sp, _name="phiP");
  TensorKernel tkp(phiP,lambda);

  //!================= analytic kernel and full bilinear form approach =========================
  //!create bilinear form and linear form
  BilinearForm auv=intg(omega,grad(u)|grad(v)) - k*k*intg(omega,u*v) - i_*intg(sigmaP,sigmaP,u*tkp*v);
  TermMatrix A(auv, _name="A");
  LinearForm fv=intg(sigmaM,gp*v);
  TermVector B(fv, _name="B");
  TermVector U=directSolve(A,B);
  BilinearForm uv=intg(omega,u*v);
  TermMatrix M(uv, _name="M");
  TermVector Uex(u,omega,modeplan);
  nbErrors += checkValuesL2( U, Uex,  M, eps,  errors, "solve Linear system with analytic kernel and full bilinear form approach");

  clear(A);

  //!================= analytic kernel and matrix product approach =========================
  BilinearForm auv2=intg(omega,grad(u)|grad(v)) - k*k*intg(omega,u*v);
  TermMatrix H(auv2, _storage=csDual, _name="H");
  BilinearForm euv=intg(sigmaP,phiP*v,_quad=GaussLegendre,_order=5);
  BilinearForm fuv=intg(sigmaP,u*phiP,_quad=GaussLegendre,_order=5);
  TermMatrix E(euv, _name="E");
  TermMatrix F(fuv, _name="F");
  TermMatrix L(phiP, sigmaP, lambda, _name="L");  //diagonal matrix
  TermMatrix ELF=E*L*F;
  TermMatrix A2=H-i_*ELF;
  TermMatrix LU2;
  luFactorize(A2,LU2);
  TermVector U2=directSolve(LU2,B);
  nbErrors += checkValuesL2( U, U2,  M, eps,  errors, "Comparison  analytic kernel and full bilinear form approach / matrix product approach");

  clear(H,E,F,L);
  clear(A2,LU2);

//!================= interpolated kernel and full bilinear form approach =========================
  Function cos(cosny, params);
  TermVectors cos_int(N);
  for (Number n=1; n<=N; n++)
  {
    // cos.parameter("basis index")=n;  //OLD METHOD
    setBasisIndex(n);                // NEW METHOD
    cos_int(n)=TermVector(u,sigmaP,cos, _name="c"+tostring(n-1));
  }
  Space Sp_int(_basis=cos_int, _name="interpolated cos(n*pi*y)");
  Unknown phiP_int(Sp_int, _name="phiP_int");
  TensorKernel tkp_int(phiP_int,lambda);
  BilinearForm auv_int=intg(omega,grad(u)|grad(v)) - k*k*intg(omega,u*v) - i_*intg(sigmaP,sigmaP,u*tkp_int*v,_quad=GaussLegendre,_order=5,_symmetry=_symmetric);
  LinearForm fv_int=intg(sigmaM,gp*v);
  TermMatrix A_int(auv_int, _storage=csDual, _name="A_int");
  TermVector B_int(fv_int, _name="B_int");
  TermMatrix LU;
  luFactorize(A_int,LU);
  TermVector U_int=directSolve(LU,B_int);
  nbErrors += checkValuesL2( U, U_int,  M, 5*eps,  errors, "Comparison  analytic / interpolated kernel and full bilinear form approach ");

  //!----------------------------------------------------------------------------------
  //!   Test on an elbow waveguide using map
  //!----------------------------------------------------------------------------------
  Mesh coude(inputsPathTo(rootname+"/coude60_court_moyen.msh"), _name="coude60deg");
  Domain comega=coude.domain("omega");
  Domain csigmaP=coude.domain("sigmaP");
  Domain csigmaM=coude.domain("sigmaM");
  //!create interpolation
  Space W(_domain=comega, _interpolation=_P2, _name="W");
  Unknown p(W, _name="p");  TestFunction q(p, _name="q");

  //!create spectral space and dtn kernel
  Space cSp(_domain=csigmaP, _basis=Function(cosny,params), _dim=N, _name="cos(n*pi*y)");
  Unknown cphiP(cSp, _name="cphiP");
  TensorKernel ctkp(cphiP,lambda);
  ctkp.xmap=Function(mapP);    //!use map from sigma+ to sigma- where are defined spectral functions
  ctkp.ymap=ctkp.xmap;

  //!================= map analytic kernel and full bilinear form approach =========================
  BilinearForm cpq=intg(comega,p*q);
  TermMatrix Mc(cpq, _name="Mc");
  BilinearForm apq=intg(comega,grad(p)|grad(q)) - k*k*intg(comega,p*q) - i_*intg(csigmaP,csigmaP,p*ctkp*q);
  LinearForm fq=intg(csigmaM,gp*q);
  TermMatrix cA(apq, _storage=csDual, _name="cA");
  TermVector cB(fq, _name="cB");
  TermMatrix cLU;
  luFactorize(cA,cLU);
  TermVector P=directSolve(cLU,cB);
  saveToFile("elbow1",P,_format=_vtu);

  clear(cA,cLU);

  //!================= map interpolated kernel and full bilinear form approach =========================
  TermVectors ccos_int(N);
  for(Number n=1; n<=N; n++)
  {
    //cos.parameter("basis index")=n; //OLD METHOD
    setBasisIndex(n);                 // NEW METHOD
    ccos_int(n)=TermVector(p,csigmaM,cos, _name="c"+tostring(n-1));
  }
  Space cSp_int(_basis=ccos_int, _name="interpolated cos(n*pi*y)");
  Unknown cphiP_int(cSp_int, _name="cphiP_int");
  TensorKernel ctkp_int(cphiP_int,lambda);
  ctkp_int.xmap=Function(mapP);
  ctkp_int.ymap=ctkp_int.xmap;
  BilinearForm apq_int=intg(comega,grad(p)|grad(q)) - k*k*intg(comega,p*q) - i_*intg(csigmaP,csigmaP,p*ctkp_int*q);
  LinearForm fq_int=intg(csigmaM,gp*q);
  TermMatrix cA_int(apq_int, _name="cA_int");
  TermVector cB_int(fq_int, _name="cB_int");
  luFactorize(cA_int,cLU);
  TermVector P_int=directSolve(cLU,cB_int);
  nbErrors += checkValuesL2( P, P_int,  Mc, 800*eps,  errors, "Elbow: Comparison  analytic / interpolated kernel and full bilinear form approach ");

  //!================= kernel from computed eigen elements and full bilinear form approach =========================
  //!compute eigen elements on cross section
  Mesh mesh1d(Segment(_xmin=-0.5,_xmax=0.5,_nnodes=21), _order=1);
  Domain section=mesh1d.domain("Omega");
  Space S(_domain=section, _interpolation=_P2, _name="S");
  Unknown r(S, _name="r");
  TestFunction s(r, _name="s");
  BilinearForm as=intg(section,grad(r)|grad(s));
  BilinearForm ms=intg(section,r*s);
  TermMatrix As(as, _name="As");
  TermMatrix Ms(ms, _name="Ms");
  As+=Ms;
  EigenElements eigs = eigenInternSolve(As, Ms, _nev=N, _mode=_krylovSchur, _which="SM");
  lambda=sqrt(k*k-(eigs.values-1));
  for (Number n=1;n<=N;n++)  //L2 normalisation
  {
    if (lambda(n).imag()<0) lambda(n)*=-1;  //choose img part >0
    Real nl2=sqrt(real(Ms*eigs.vector(n)|eigs.vector(n)));
    eigs.vector(n)/=nl2;
  }
  Space sSp_int(_basis=eigs.vectors, _name="computed eigenvectors cos(n*pi*y)");
  Unknown sphiP_int(sSp_int, _name="sphiP_int");
  TensorKernel stkp_int(sphiP_int,lambda);
  stkp_int.xmap=Function(mapS);
  stkp_int.ymap=stkp_int.xmap;
  BilinearForm sapq_int=intg(comega,grad(p)|grad(q)) - k*k*intg(comega,p*q) - i_*intg(csigmaP,csigmaP,p*stkp_int*q);
  LinearForm sfq_int=intg(csigmaM,gp*q);
  TermMatrix scA_int(sapq_int, _storage=csDual, _name="scA_int");
  TermVector scB_int(sfq_int, _name="scB_int");
  TermMatrix scLU;
  luFactorize(scA_int,scLU);
  TermVector sP_int=directSolve(scLU,scB_int);
  nbErrors += checkValuesL2( P, sP_int,  Mc, 5*eps,  errors, "Elbow: Comparison  analytic / kernel from computed eigen elements and full bilinear form approach ");

  //!================= vector tensor kernel  =========================
  Unknown u3(V, _name="u3", _dim=3); TestFunction v3(u3, _name="v3");
  TensorKernel tk3(Function(vcosn,params),lambda);
  BilinearForm auv3=intg(sigmaP,sigmaP,u3|(tk3*v3));
  TermMatrix A3(auv3, _name="A3");
  nbErrors += checkValue(norminfty(A3), 0.967482 , 1.E-5, errors,"vector tensor kernel, norminfty(A3)");

//  DOES NOT WORK, REQUIRE UPDATE OF SPECTRAL COMPUTATION ON EXTENDED DOMAIN
//  Space W(_domain=omega, _FE_type=Nedelec , _order=1 , _name="N" ) ;
//  Unknown p(W, _name="p"); TestFunction q(p, _name="q");
//  TensorKernel tk2(Function(v2cosn,params),lambda);
//  BilinearForm auv2=intg(sigmaP,sigmaP,p|(tk2*q));
//  TermMatrix A2(auv2, _name="A2");
//  thePrintStream<<A2<<eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
