/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_segment.cpp
	\author N. Kielbasiewicz
	\since 5 nov 2012
	\date 18 dec 2012

	Low level tests of refSegment class.
 */

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_segment {

void unit_segment(int argc, char* argv[], bool check)
{
  String rootname = "unit_segment";
  trace_p->push(rootname);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  Interpolation* p1 = findInterpolation(_Lagrange, _standard, 1, H1);
  Interpolation* p2 = findInterpolation(_Lagrange, _standard, 2, H1);
  Interpolation* p3 = findInterpolation(_Lagrange, _standard, 3, H1);
  Interpolation* p4 = findInterpolation(_Lagrange, _standard, 4, H1);
  Interpolation* p5 = findInterpolation(_Lagrange, _standard, 5, H1);
  RefElement* segmentP1 = findRefElement(_segment, p1);
  RefElement* segmentP2 = findRefElement(_segment, p2);
  RefElement* segmentP3 = findRefElement(_segment, p3);
  RefElement* segmentP4 = findRefElement(_segment, p4);
  RefElement* segmentP5 = findRefElement(_segment, p5);

  verboseLevel(10);
  theCout << *(segmentP1) << std::endl;
  theCout << *(segmentP2) << std::endl;
  theCout << *(segmentP3) << std::endl;
  theCout << *(segmentP4) << std::endl;
  theCout << *(segmentP5) << std::endl;

  nbErrors += checkValue(theCout, rootname+"/Segment.in" , errors, "Segment",check);

  //!test polynomials representation
  theCout<<"===================== with polynomials representation ========================"<<eol;
  segmentP1->computeShapeFunctions();theCout << *(segmentP1) << std::endl;
  segmentP2->computeShapeFunctions();theCout << *(segmentP2) << std::endl;
  segmentP3->computeShapeFunctions();theCout << *(segmentP3) << std::endl;
  segmentP4->computeShapeFunctions();theCout << *(segmentP4) << std::endl;
  segmentP5->computeShapeFunctions();theCout << *(segmentP5) << std::endl;

  nbErrors += checkValue(theCout, rootname+"/SegmentPolynomialsRepresentation.in" , errors, "Segment: Polynomials Representation",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
