/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_SPComputation.cpp
	\author E. Lunéville
	\since 14 nov 2013
	\date 14 nov 2013

	Basic test of TensorKernel
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_SPComputation {

Real k=5;

//!cos basis
Real cosny(const Point& P, Parameters& pa = defaultParameters)
{
  Real y=P(2);
  Real h=pa("h");                    //get the parameter h (user definition)
  Int n=pa("basis index")-1;         //get the index of function to compute
  if(n==0) return sqrt(1./h);
  else     return sqrt(2./h)*cos(n*pi_*(y+h*0.5)/h);
}

void unit_SPComputation(int argc, char* argv[], bool check)
{
  String rootname = "unit_SPComputation";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(21);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  //!create a 2D mesh
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=pi_,_ymin=-0.5,_ymax=0.5,_nnodes=Numbers(16,6),_side_names=Strings("y=0","x=pi","y=h","x=0")), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=pi");

  //!parameters
  Real h=1;
  Parameters params(h,"h");
  params<<Parameter(k,"k");

  //!-------------------------------------------
  //!spectral computation from analytic function
  //!-------------------------------------------
  Function cn(cosny, params);
  Number N=5;
  Space Sp(_domain=sigmaP, _basis=cn, _dim=N, _name="cos(n*pi*y)");
  Unknown u(Sp, _name="u"); TestFunction v(u, _name="v");
  //!scalar case
  TermMatrix A1(intg(sigmaP,u*v), _name="A1"); thePrintStream<<A1;
  TermMatrix A2(intg(sigmaP,u*conj(v)), _name="A2"); thePrintStream<<A2;
  //!vector case
  Unknown u3(Sp, _name="u3", _dim=3); TestFunction v3(u3, _name="v3");
  TermMatrix A3(intg(sigmaP,u3|v3), _name="A3"); thePrintStream<<A3;
  nbErrors+=checkValues(A1, rootname+"/A1.in", tol, errors, "TermMatrix A1(intg(sigmaP,u*v)) ", check);
  nbErrors+=checkValues(A2, rootname+"/A2.in", tol, errors, "TermMatrix A2(intg(sigmaP,u*conj(v)) ", check);
  A3.toScalar();
  nbErrors+=checkValues(A3, rootname+"/A3.in", tol, errors, "TermMatrix A3(intg(sigmaP,u3*v3) /vector case/", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
