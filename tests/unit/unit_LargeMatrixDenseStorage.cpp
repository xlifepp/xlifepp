/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_LargeMatrixDenseStorage.cpp
	\author E. Lunéville
	\since 26 jun 2012
	\date  26 jun 2012

	Low level tests of LargeMatrix class and DenseStorage classes.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_LargeMatrixDenseStorage
{

void unit_LargeMatrixDenseStorage(int argc, char* argv[], bool check)
{
  String rootname = "unit_LargeMatrixDenseStorage";
  trace_p->push(rootname);
  std::stringstream out;                  // string stream receiving results
  out.precision(testPrec);
  Number nbErrors = 0;
  String errors;
  Real tol=0.00001;
  verboseLevel(3);
  numberOfThreads(1);

  std::vector<Real> x1(2, 2), x2(3, -3), x3(6, 3);
  std::vector<Complex> c1(2, 2), c2(3, -3), c3(6, 3);
  std::vector<Number> rows(2, 1), cols(1, 1);
  rows[1] = 3;
  Matrix<Real> m22(2, 1, -5.);
  //!dense matrix
  LargeMatrix<Real> A0;
  theCout << "matrix A0 : " << A0 << std::endl;
  theCout << std::endl << "----------------------------------- test row dense storage -----------------------------------" << std::endl;
  LargeMatrix<Real> A1(3, 2, _dense, _row, 1.);
  theCout << "matrix A1 : " << A1 << std::endl;
  nbErrors+=checkValues( A1, rootname+"/A1.mat", tol, errors,  "LargeMatrix<Real> A1", check);
  theCout << "access A1(1,1)= " << A1(1, 1) << " A1(2,1)=" << A1(2, 1) << " A1(1,2)=" << A1(1, 2) << std::endl;
  nbErrors+=checkValue(A1(1,1), 1, errors, "A1(1,1)"); 
  nbErrors+=checkValue(A1(1,2), 1, errors, "A1(1,2)"); 
  nbErrors+=checkValue(A1(2,1), 1, errors, "A1(2,1)"); 
  theCout << "product A1*x1 : " << A1* x1 << std::endl;
  theCout << "product A1*c1 : " << A1* c1 << std::endl;
  theCout << "product x2*A1 : " << x2* A1 << std::endl;
  theCout << "product c2*A1 : " << c2* A1 << std::endl;
  nbErrors+=checkValues(A1*x1,rootname+"/A1fx1.in", tol, errors, "A1*x1",check); 
  nbErrors+=checkValues(A1*c1,rootname+"/A1fc1.in", tol, errors, "A1*c1", check); 
  nbErrors+=checkValues(x2*A1,rootname+"/x2fA1.in", tol, errors, "x2*A1",check); 
  nbErrors+=checkValues(c2*A1,rootname+"/c2fA1.in", tol, errors, "c2*A1", check); 
  theCout << "add constant to submatrix A1.add(5,rows,cols) : "<< eol;
  nbErrors+=checkValues(A1.add(5., rows, cols),rootname+"/A1_add5.in", tol, errors, "A1.add(5., rows, cols)", check); 
  theCout << "add submatrix to A1 A1.add(m22,rows,cols) : " << eol;
  nbErrors+=checkValues(A1.add(m22, rows, cols) ,rootname+"/A1_addm22.in", tol, errors, "A1.add(m22, rows, cols)", check);  
  A1.saveToFile("A1dense.mat", _dense);
  theCout << "\nsave A1 to file A1dense.mat in dense format" << std::endl;
  A1.saveToFile("A1coo.mat", _coo);
  LargeMatrix<Real> A1reload(3, 2, _dense, _row, 0.);
  A1reload.loadFromFile("A1dense.mat", _dense); 
  theCout << "matrix A1reload from Adense.mat : " << A1reload << std::endl;
  A1reload.loadFromFile("A1coo.mat", _coo);
  theCout << "matrix A1reload from A1coo.mat : " << A1reload << std::endl;

  theCout << "matrix A1reload from Acoo.mat : " << A1reload << std::endl;
  std::vector<std::pair<Number, Number> > row1=A1.getRow(1);
  theCout << "row 1 (col & adrs) : " << row1 << std::endl;
  theCout << "row 2 (col & adrs) : " << A1.getRow(2) << std::endl;
  theCout << "row 3 (col & adrs) : " << A1.getRow(3) << std::endl;
  theCout << "col 1 (col & adrs) : " << A1.getCol(1) << std::endl;
  theCout << "col 2 (col & adrs) : " << A1.getCol(2) << std::endl;
  nbErrors+=checkValues(A1.getRow(1), rootname+"/A1_getrow1.in", tol, errors, "A1.getRow(1)", check); 
  nbErrors+=checkValues(A1.getRow(2), rootname+"/A1_getrow2.in", tol, errors, "A1.getRow(2)", check); 
  nbErrors+=checkValues(A1.getRow(3), rootname+"/A1_getrow3.in", tol, errors, "A1.getRow(3)", check); 
  nbErrors+=checkValues(A1.getCol(1), rootname+"/A1_getcol1.in", tol, errors, "A1.getCol(1)", check); 
  nbErrors+=checkValues(A1.getCol(2), rootname+"/A1_getcol2.in", tol, errors, "A1.getCol(2)", check); 
  nbErrors+=checkValue(theCout, rootname+"/A1.in", errors, "LargeMatrix<Real>  A1", check); 

  LargeMatrix<Complex> B1(3, 2, _dense, _row, 1.);
  theCout << "matrix B1 : " << B1 << std::endl;
  nbErrors+=checkValues( B1, rootname+"/B1.mat", tol, errors,  "LargeMatrix<Real> B1", check);
  theCout << "access B1(1,1)= " << B1(1, 1) << " B1(2,1)=" << B1(2, 1) << " B1(1,2)=" << B1(1, 2) << std::endl;
  nbErrors+=checkValue(B1(1,1), Complex (1,0), errors, "B1(1,1)"); 
  nbErrors+=checkValue(B1(1,2), Complex (1,0), errors, "B1(1,2)"); 
  nbErrors+=checkValue(B1(2,1), Complex (1,0), errors, "B1(2,1)"); 
  theCout << "product B1*x1 : " << B1* x1 << std::endl;
  theCout << "product B1*c1 : " << B1* c1 << std::endl;
  theCout << "product x2*B1 : " << x2* B1 << std::endl;
  theCout << "product c2*B1 : " << c2* B1 << std::endl;
  nbErrors+=checkValues(B1*x1,rootname+"/B1fx1.in", tol, errors, "B1*x1",check); 
  nbErrors+=checkValues(B1*c1,rootname+"/B1fc1.in", tol, errors, "B1*c1", check); 
  nbErrors+=checkValues(x2*B1,rootname+"/x2fB1.in", tol, errors, "x2*B1",check); 
  nbErrors+=checkValues(c2*B1,rootname+"/c2fB1.in", tol, errors, "c2*B1", check);  
  theCout << "add constant to submatrix B1.add(5,rows,cols) : "<< eol;
  nbErrors+=checkValues(B1.add(5., rows, cols),rootname+"/B1_add5.in", tol, errors, "B1.add(5., rows, cols)", check); 
  theCout << "add submatrix to B1 B1.add(m22,rows,cols) : " << eol;
  nbErrors+=checkValues(B1.add(cmplx(m22), rows, cols) ,rootname+"/B1_addm22.in", tol, errors, "B1.add(m22, rows, cols)", check);
  B1.saveToFile("B1Adense.mat", _dense);
  theCout << "save B1 to file B1Adense.mat in dense format" << std::endl;
  B1.saveToFile("B1Acoo.mat", _coo);
  theCout << "save B1 to file B1Acoo.mat in coordinates format" << std::endl;
  LargeMatrix<Complex> B1reload(3, 2, _dense, _row, 0.);
  B1reload.loadFromFile("B1Adense.mat", _dense);
  theCout << "matrix B1reload from Adense : " << B1reload << std::endl;
  B1reload.loadFromFile("B1Acoo.mat", _coo);
  theCout << "matrix B1reload from Acoo : " << B1reload << std::endl;
  B1reload.loadFromFile("B1Adense.mat", _dense);
  theCout << "matrix B1reload from Adense : " << B1reload << std::endl;
  B1reload.loadFromFile("B1Acoo.mat", _coo);
  theCout << "matrix B1reload from Acoo : " << B1reload << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/B1.in", errors, "LargeMatrix<Complex>  B1", check); 

  Matrix<Real> I1(2, _idMatrix);
  Vector<Real> e1(2, 0); e1(1) = 1;
  Vector<Complex> f1(2, 0); f1(1) = 1;
  std::vector<Vector<Real> > X1(2, e1), X2(3, -3.*e1), X3(6, 3.*e1);
  theCout << "vector of Vectors  X1 : " << X1 << std::endl;
  theCout << "vector of Vectors  X2 : " << X2 << std::endl;
  theCout << "vector of Vectors  X3 : " << X3 << std::endl;
  nbErrors+=checkValues(X1,rootname+"/X1.in", tol, errors, "X1", check);
  nbErrors+=checkValues(X2,rootname+"/X2.in", tol, errors, "X2", check);
  nbErrors+=checkValues(X3,rootname+"/X3.in", tol, errors, "X3", check);
  std::vector<Vector<Complex> > C1(2, f1), C2(3, -3.*f1), C3(6, 3.*f1);
  theCout << "vector of Vectors  C1 : " << C1 << std::endl;
  theCout << "vector of Vectors  C2 : " << C2 << std::endl;
  theCout << "vector of Vectors  C3 : " << C3 << std::endl;
  nbErrors+=checkValues(C1,rootname+"/C1.in", tol, errors, "C1", check);
  nbErrors+=checkValues(C2,rootname+"/C2.in", tol, errors, "C2", check);
  nbErrors+=checkValues(C3,rootname+"/C3.in", tol, errors, "C3", check);

  LargeMatrix<Matrix<Real> > D1(3, 2, _dense, _row, I1);
  theCout << "matrix D1 : " << D1 << std::endl;
  nbErrors+=checkValues( D1, rootname+"/D1.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  D1", check);
  theCout << "access \nD1(1,1)= " << D1(1, 1) << " \nD1(2,1)=" << D1(2, 1) << " \nD1(1,2)=" << D1(1, 2) << std::endl;
  nbErrors+=checkValues(D1(1,1), I1, 0., errors, "D1(1,1)"); 
  nbErrors+=checkValues(D1(1,2), I1, 0., errors, "D1(1,2)"); 
  nbErrors+=checkValues(D1(2,1), I1, 0., errors, "D1(2,1)"); 
  theCout << "product D1*X1 : " << D1* X1 << std::endl;
  theCout << "product D1*C1 : " << D1* C1 << std::endl;
  theCout << "product X2*D1 : " << X2* D1 << std::endl;
  theCout << "product C2*D1 : " << C2* D1 << std::endl;
  nbErrors+=checkValues(D1*X1,rootname+"/D1fX1.in", tol, errors, "D1*X1",check); 
  nbErrors+=checkValues(D1*C1,rootname+"/D1fC1.in", tol, errors, "D1*C1", check); 
  nbErrors+=checkValues(X2*D1,rootname+"/X2fD1.in", tol, errors, "X2*D1",check); 
  nbErrors+=checkValues(C2*D1,rootname+"/C2fD1.in", tol, errors, "C2*D1", check);
  theCout << "add constant to submatrix D1.add(5*I1,rows,cols) : " <<eol; //<< D1.add(5 * I1, rows, cols) << std::endl;
  nbErrors+=checkValues(D1.add(5 *I1, rows, cols),rootname+"/D1_add5I1.in", tol, errors, "D1.add(5.*I1, rows, cols)", check);
  D1.saveToFile("D1Adense.mat", _dense);
  theCout << "save D1 to file D1Adense.mat in dense format" << std::endl;
  D1.saveToFile("D1Acoo.mat", _coo);
  theCout << "save D1 to file D1Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/D1.in", errors, "LargeMatrix<Real> D1", check); 

  Matrix<Complex> J1(2, _idMatrix);
  LargeMatrix<Matrix<Complex> > E1(3, 2, _dense, _row, J1);
  theCout << "matrix E1 : " << E1 << std::endl;
  nbErrors+=checkValues( E1, rootname+"/E1.mat", tol, errors,  "LargeMatrix<Matrix<Complex> >  E1", check);
  theCout << "access \nE1(1,1)= " << E1(1, 1) << " \nE1(2,1)=" << E1(2, 1) << " \nE1(1,2)=" << E1(1, 2) << std::endl;
  nbErrors+=checkValues(E1(1,1), J1, 0., errors, "D1(1,1)"); 
  nbErrors+=checkValues(E1(1,2), J1, 0., errors, "D1(1,2)"); 
  nbErrors+=checkValues(E1(2,1), J1, 0., errors, "D1(2,1)"); 
  theCout << "product E1*X1 : " << E1* X1 << std::endl;
  theCout << "product E1*C1 : " << E1* C1 << std::endl;
  theCout << "product X2*E1 : " << X2* E1 << std::endl;
  theCout << "product C2*E1 : " << C2* E1 << std::endl;
  nbErrors+=checkValues(E1*X1,rootname+"/E1fX1.in", tol, errors, "E1*X1",check); 
  nbErrors+=checkValues(E1*C1,rootname+"/E1fC1.in", tol, errors, "E1*C1", check); 
  nbErrors+=checkValues(X2*E1,rootname+"/X2fE1.in", tol, errors, "X2*E1",check); 
  nbErrors+=checkValues(C2*E1,rootname+"/C2fE1.in", tol, errors, "C2*E1", check);
  E1.saveToFile("E1Adense.mat", _dense);
  theCout << "save E1 to file E1Adense.mat in dense format" << std::endl;
  E1.saveToFile("E1Acoo.mat", _coo);
  theCout << "save E1 to file E1Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/E1.in", errors, "Matrix<Real> E1", check); 


  out << std::endl << "----------------------------------- test col dense storage -----------------------------------" << std::endl;
  LargeMatrix<Real> A2(6, 3, _dense, _col, 2.);
  theCout << "matrix A2 : " << A2 << std::endl;
  nbErrors+=checkValues( A2, rootname+"/A2.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  A2", check);
  theCout << "access A2(1,1)= " << A2(1, 1) << " A2(2,1)=" << A2(2, 1) << " A2(1,2)=" << A2(1, 2) << std::endl;
  nbErrors+=checkValue(A2(1,1), 2., errors, "A2(1,1)"); 
  nbErrors+=checkValue(A2(1,2), 2., errors, "A2(1,2)"); 
  nbErrors+=checkValue(A2(2,1), 2, errors, "A2(2,1)"); 
  theCout << "product A2*x2 : " << A2* x2 << std::endl;
  theCout << "product A2*c2 : " << A2* c2 << std::endl;
  theCout << "product x3*A2 : " << x3* A2 << std::endl;
  theCout << "product c3*A2 : " << c3* A2 << std::endl;
  nbErrors+=checkValues(A2*x2,rootname+"/A2fx2.in", tol, errors, "A2*x2",check); 
  nbErrors+=checkValues(A2*c2,rootname+"/A2fc2.in", tol, errors, "A2*c2", check); 
  nbErrors+=checkValues(x3*A2,rootname+"/x3fA2.in", tol, errors, "x3*A2",check); 
  nbErrors+=checkValues(c3*A2,rootname+"/c3fA2.in", tol, errors, "c3*A2", check);
  theCout << "add constant to submatrix A2.add(5,rows,cols) : " <<eol;//<< A2.add(5., rows, cols) << std::endl;
  nbErrors+=checkValues(A2.add(5., rows, cols),rootname+"/A2_add5.in", tol, errors, "A2.add(5., rows, cols)", check); 
  theCout << "add submatrix to A2 A2.add(m22,rows,cols) : " <<eol; //<< A2.add(m22, rows, cols) << std::endl;
  nbErrors+=checkValues(A2.add(m22, rows, cols) ,rootname+"/A2_addm22.in", tol, errors, "A2.add(m22, rows, cols)", check);
  A2.saveToFile("A2Adense.mat", _dense);
  theCout << "\nsave A2 to file A2Adense.mat in dense format" << std::endl;
  A2.saveToFile("A2Acoo.mat", _coo);
  theCout << "save A2 to file A2Acoo.mat in coordinates format" << std::endl;
  row1=A2.getRow(1);
  theCout << "row 1 (col & adrs) : " << row1 << std::endl;
  theCout << "row 2 (col & adrs) : " << A2.getRow(2) << std::endl;
  theCout << "row 3 (col & adrs) : " << A2.getRow(3) << std::endl;
  theCout << "col 1 (col & adrs) : " << A2.getCol(1) << std::endl;
  theCout << "col 2 (col & adrs) : " << A2.getCol(2) << std::endl;
  nbErrors+=checkValues(row1, rootname+"/A2_row1.in", tol, errors, "A2.row1", check); 
  nbErrors+=checkValues(A2.getRow(2), rootname+"/A2_getrow2.in", tol, errors, "A2.getRow(2)", check); 
  nbErrors+=checkValues(A2.getRow(3), rootname+"/A2_getrow3.in", tol, errors, "A2.getRow(3)", check); 
  nbErrors+=checkValues(A2.getCol(1), rootname+"/A2_getcol1.in", tol, errors, "A2.getCol(1)", check); 
  nbErrors+=checkValues(A2.getCol(2), rootname+"/A2_getcol2.in", tol, errors, "A2.getCol(2)", check); 
  nbErrors+=checkValue(theCout, rootname+"/A2.in", errors, "LargeMatrix<Real> A2", check);

  LargeMatrix<Complex> B2(6, 3, _dense, _col, 2.);
  theCout << "matrix B2 : " << B2  << std::endl;
  nbErrors+=checkValues( B2, rootname+"/B2.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  B2", check);
  theCout << "access B2(1,1)= " << B2(1, 1) << " B2(2,1)=" << B2(2, 1) << " B2(1,2)=" << B2(1, 2) << std::endl;
  nbErrors+=checkValue(B1(1,1), Complex (1,0), errors, "B1(1,1)"); 
  nbErrors+=checkValue(B1(1,2), Complex (1,0), errors, "B1(1,2)"); 
  nbErrors+=checkValue(B1(2,1), Complex (1,0), errors, "B1(2,1)"); 
  theCout << "product B2*x2 : " << B2* x2 << std::endl;
  theCout << "product B2*c2 : " << B2* c2 << std::endl;
  theCout << "product x3*B2 : " << x3* B2 << std::endl;
  theCout << "product c3*B2 : " << c3* B2 << std::endl;
  nbErrors+=checkValues(B2*x2,rootname+"/B2fx2.in", tol, errors, "B2*x2",check); 
  nbErrors+=checkValues(B2*c2,rootname+"/B2fc2.in", tol, errors, "B2*c2", check); 
  nbErrors+=checkValues(x3*B2,rootname+"/x2fB2.in", tol, errors, "x2*B2",check); 
  nbErrors+=checkValues(c3*B2,rootname+"/c3fB2.in", tol, errors, "c2*B2", check);  
  theCout << "add constant to submatrix B2.add(5,rows,cols) : " <<eol; //<< B2.add(5., rows, cols) << std::endl;
  nbErrors+=checkValues(B2.add(5., rows, cols),rootname+"/B2_add5.in", tol, errors, "B2.add(5., rows, cols)", check);
  theCout << "add submatrix to B2 B2.add(cmplx(m22),rows,cols) : " << eol; //<< B2.add(cmplx(m22), rows, cols) << std::endl;
  nbErrors+=checkValues(B2.add(cmplx(m22), rows, cols) ,rootname+"/B2_addm22.in", tol, errors, "B2.add(m22, rows, cols)", check);
  B2.saveToFile("B2Adense.mat", _dense);
  theCout << "save B2 to file B2Adense.mat in dense format" << std::endl;
  B2.saveToFile("B2Acoo.mat", _coo);
  theCout << "save B2 to file B2Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/B2.in", errors, "LargeMatrix<Complex> B2", check);

  LargeMatrix<Matrix<Real> > D2(6, 3, _dense, _col, 2.*I1);
  theCout << "matrix D2 : " << D2 << std::endl;
  nbErrors+=checkValues( D2, rootname+"/D2.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  D2", check);
  theCout << "access \nD2(1,1)= " << D2(1, 1) << " \nD2(2,1)=" << D2(2, 1) << " \nD2(1,2)=" << D2(1, 2) << std::endl;
  nbErrors+=checkValues(D2(1,1), 2*I1, 0., errors, "D2(1,1)"); 
  nbErrors+=checkValues(D2(1,2), 2*I1, 0., errors, "D2(1,2)"); 
  nbErrors+=checkValues(D2(2,1), 2*I1, 0., errors, "D2(2,1)"); 
  theCout << "product D2*X2 : " << D2* X2 << std::endl;
  theCout << "product D2*C2 : " << D2* C2 << std::endl;
  theCout << "product X3*D2 : " << X3* D2 << std::endl;
  theCout << "product C3*D2 : " << C3* D2 << std::endl;
  nbErrors+=checkValues(D2*X2,rootname+"/D2fX2.in", tol, errors, "D2*X2",check); 
  nbErrors+=checkValues(D2*C2,rootname+"/D2fC2.in", tol, errors, "D2*C2", check); 
  nbErrors+=checkValues(X3*D2,rootname+"/X3fD2.in", tol, errors, "X3*D2",check); 
  nbErrors+=checkValues(C3*D2,rootname+"/C3fD2.in", tol, errors, "C3*D2", check);
  theCout << "add constant to submatrix D2.add(5*I1,rows,cols) : " <<eol; //<< D2.add(5 * I1, rows, cols) << std::endl;
  nbErrors+=checkValues(D2.add(5 *I1, rows, cols),rootname+"/D2_add5I1.in", tol, errors, "D2.add(5.*I1, rows, cols)", check);
  D2.saveToFile("D2Adense.mat", _dense);
  theCout << "save D2 to file D2Adense.mat in dense format" << std::endl;
  D2.saveToFile("D2Acoo.mat", _coo);
  theCout << "save D2 to file D2Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/D2.in", errors, "LargeMatrix<Real> D2", check);

  LargeMatrix<Matrix<Complex> > E2(6, 3, _dense, _col, 2.*J1);
  theCout << "matrix E2 : " << E2 << std::endl;
  nbErrors+=checkValues( E2, rootname+"/E2.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  E2", check);
  theCout << "access \nE2(1,1)= " << E2(1, 1) << " \nE2(2,1)=" << E2(2, 1) << " \nE2(1,2)=" << E2(1, 2) << std::endl;
  nbErrors+=checkValues(E2(1,1), 2*J1, 0., errors, "E2(1,1)"); 
  nbErrors+=checkValues(E2(1,2), 2*J1, 0., errors, "E2(1,2)"); 
  nbErrors+=checkValues(E2(2,1), 2*J1, 0., errors, "E2(2,1)"); 
  theCout << "product E2*X2 : " << E2* X2 << std::endl;
  theCout << "product E2*C2 : " << E2* C2 << std::endl;
  theCout << "product X3*E2 : " << X3* E2 << std::endl;
  theCout << "product C3*E2 : " << C3* E2 << std::endl;
  nbErrors+=checkValues(E2*X2,rootname+"/E2fX2.in", tol, errors, "E2*X2",check); 
  nbErrors+=checkValues(E2*C2,rootname+"/E2fC2.in", tol, errors, "E2*C2", check); 
  nbErrors+=checkValues(X3*E2,rootname+"/X3fE2.in", tol, errors, "X3*E2",check); 
  nbErrors+=checkValues(C3*E2,rootname+"/C3fE2.in", tol, errors, "C3*E2", check);
  E2.saveToFile("E2Adense.mat", _dense);
  theCout << "save E2 to file E2Adense.mat in dense format" << std::endl;
  E2.saveToFile("E2Acoo.mat", _coo);
  theCout << "save E2 to file E2Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/E2.in", errors, "LargeMatrix<Comlex> E2", check);

  out << std::endl << "----------------------------------- test dual dense storage -----------------------------------" << std::endl;
  LargeMatrix<Real> A3(6, 3, _dense, _dual, 2.);
  theCout << "matrix A3 : " << A3 << std::endl;
  nbErrors+=checkValues( A3, rootname+"/A3.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  A3", check);
  theCout << "access A3(1,1)= " << A3(1, 1) << " A3(2,1)=" << A3(2, 1) << " A3(1,2)=" << A3(1, 2) << std::endl;
  nbErrors+=checkValue(A3(1,1), 2, errors, "A3(1,1)"); 
  nbErrors+=checkValue(A3(1,2), 2, errors, "A3(1,2)"); 
  nbErrors+=checkValue(A3(2,1), 2, errors, "A3(2,1)");
  theCout << "product A3*x2 : " << A3* x2 << std::endl;
  theCout << "product A3*c2 : " << A3* c2 << std::endl;
  theCout << "product x3*A3 : " << x3* A3 << std::endl;
  theCout << "product c3*A3 : " << c3* A3 << std::endl;
  nbErrors+=checkValues(A3*x2,rootname+"/A3fx2.in", tol, errors, "A3*x2",check); 
  nbErrors+=checkValues(A3*c2,rootname+"/A2fc2.in", tol, errors, "A3*c2", check); 
  nbErrors+=checkValues(x3*A3,rootname+"/x3fA3.in", tol, errors, "x3*A3",check); 
  nbErrors+=checkValues(c3*A3,rootname+"/c3fA3.in", tol, errors, "c3*A3", check);
  theCout << "add constant to submatrix A3.add(5,rows,cols) : "<<eol; // << A3.add(5., rows, cols) << std::endl;
  nbErrors+=checkValues(A3.add(5., rows, cols),rootname+"/A3_add5.in", tol, errors, "A3.add(5., rows, cols)", check); 
  theCout << "add submatrix to A3 A3.add(m22,rows,cols) : " << eol; //<< A3.add(m22, rows, cols) << std::endl;
  nbErrors+=checkValues(A3.add(m22, rows, cols) ,rootname+"/A3_addm22.in", tol, errors, "A3.add(m22, rows, cols)", check); 
  A3.saveToFile("A3Adense.mat", _dense);
  theCout << "save A3 to file A3Adense.mat in dense format" << std::endl;
  A3.saveToFile("A3Acoo.mat", _coo);
  theCout << "save A3 to file A3Acoo.mat in coordinates format" << std::endl;
  row1=A3.getRow(1);
  theCout << "row 1 (col & adrs) : " << row1 << std::endl;
  theCout << "row 2 (col & adrs) : " << A3.getRow(2) << std::endl;
  theCout << "row 3 (col & adrs) : " << A3.getRow(3) << std::endl;
  theCout << "row 4 (col & adrs) : " << A3.getRow(4) << std::endl;
  theCout << "row 5 (col & adrs) : " << A3.getRow(5) << std::endl;
  theCout << "row 6 (col & adrs) : " << A3.getRow(6) << std::endl;
  theCout << "col 1 (col & adrs) : " << A3.getCol(1) << std::endl;
  theCout << "col 2 (col & adrs) : " << A3.getCol(2) << std::endl;
  theCout << "col 3 (col & adrs) : " << A3.getCol(3) << std::endl;
  nbErrors+=checkValues(A3.getRow(1), rootname+"/A3getRow1.in", tol, errors, "A3.getRow(1)", check);
  nbErrors+=checkValues(A3.getRow(2), rootname+"/A3getRow2.in", tol, errors, "A3.getRow(2)", check);
  nbErrors+=checkValues(A3.getRow(3), rootname+"/A3getRow3.in", tol, errors, "A3.getRow(3)", check);
  nbErrors+=checkValues(A3.getRow(4), rootname+"/A3getRow4.in", tol, errors, "A3.getRow(4)", check);
  nbErrors+=checkValues(A3.getRow(5), rootname+"/A3getRow5.in", tol, errors, "A3.getRow(5)", check);
  nbErrors+=checkValues(A3.getRow(6), rootname+"/A3getRow6.in", tol, errors, "A3.getRow(6)", check);
  nbErrors+=checkValues(A3.getCol(1), rootname+"/A3getCol1.in", tol, errors, "A3.getCol(1)", check);
  nbErrors+=checkValues(A3.getCol(2), rootname+"/A3getCol2.in", tol, errors, "A3.getCol(2)", check);
  nbErrors+=checkValues(A3.getCol(3), rootname+"/A3getCol3.in", tol, errors, "A3.getCol(3)", check); 
  nbErrors+=checkValue(theCout, rootname+"/A3.in", errors, "LargeMatrix<Real> A3", check);

  LargeMatrix<Complex> B3(6, 3, _dense, _dual, 2.);
  theCout << "matrix B3 : " << B3 << std::endl;
  nbErrors+=checkValues( B3, rootname+"/B3.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  B3", check);
  theCout << "access B3(1,1)= " << B3(1, 1) << " B3(2,1)=" << B3(2, 1) << " B3(1,2)=" << B3(1, 2) << std::endl;
  nbErrors+=checkValue(B3(1,1), Complex (2,0), errors, "B3(1,1)"); 
  nbErrors+=checkValue(B3(1,2), Complex (2,0), errors, "B3(1,2)"); 
  nbErrors+=checkValue(B3(2,1), Complex (2,0), errors, "B3(2,1)");
  theCout << "product B3*x2 : " << B3* x2 << std::endl;
  theCout << "product B3*c2 : " << B3* c2 << std::endl;
  theCout << "product x3*B3 : " << x3* B3 << std::endl;
  theCout << "product c3*B3 : " << c3* B3 << std::endl;
  nbErrors+=checkValues(B2*x2,rootname+"/B2fx2.in", tol, errors, "B2*x2",check); 
  nbErrors+=checkValues(B3*c2,rootname+"/B3fc2.in", tol, errors, "B3*c2", check); 
  nbErrors+=checkValues(x3*B3,rootname+"/x3fB3.in", tol, errors, "x3*B3",check); 
  nbErrors+=checkValues(c3*B3,rootname+"/c3fB3.in", tol, errors, "c3*B3", check); 
  theCout << "add constant to submatrix B3.add(5,rows,cols) : " << eol; //<< B3.add(5., rows, cols) << std::endl;
  nbErrors+=checkValues(B3.add(5., rows, cols),rootname+"/B3_add5.in", tol, errors, "B3.add(5., rows, cols)", check); 
  theCout << "add submatrix to B3 B3.add(cmplx(m22),rows,cols) : "<<eol;// << B3.add(cmplx(m22), rows, cols) << std::endl;
  nbErrors+=checkValues(B3.add(cmplx(m22), rows, cols) ,rootname+"/B3_addm22.in", tol, errors, "B3.add(m22, rows, cols)", check);
  B3.saveToFile("B3Adense.mat", _dense);
  theCout << "save B3 to file B3Adense.mat in dense format" << std::endl;
  B3.saveToFile("B3Acoo.mat", _coo);
  theCout << "save B3 to file B3Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/B3.in", errors, "LargeMatrix<Real> B3", check);

  LargeMatrix<Matrix<Real> > D3(6, 3, _dense, _dual, 2.*I1);
  theCout << "matrix D3 : " << D3 << std::endl;
  nbErrors+=checkValues( D3, rootname+"/D3.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  D3", check);
  theCout << "access \nD3(1,1)= " << D3(1, 1) << " \nD3(2,1)=" << D3(2, 1) << " \nD3(1,2)=" << D3(1, 2) << std::endl;
  nbErrors+=checkValues(D3(1,1), 2*I1, 0., errors, "D3(1,1)"); 
  nbErrors+=checkValues(D3(1,2), 2*I1, 0., errors, "D3(1,2)"); 
  nbErrors+=checkValues(D3(2,1), 2*I1, 0., errors, "D3(2,1)"); 
  theCout << "product D3*X2 : " << D3* X2 << std::endl;
  theCout << "product D3*C2 : " << D3* C2 << std::endl;
  theCout << "product X3*D3 : " << X3* D3 << std::endl;
  theCout << "product C3*D3 : " << C3* D3 << std::endl;
  nbErrors+=checkValues(D3*X2,rootname+"/D3fX2.in", tol, errors, "D3*X2",check); 
  nbErrors+=checkValues(D3*C2,rootname+"/D3fC.in", tol, errors, "D3*C2", check); 
  nbErrors+=checkValues(X3*D3,rootname+"/X3fD3.in", tol, errors, "X3*D3",check); 
  nbErrors+=checkValues(C3*D3,rootname+"/C3fD3.in", tol, errors, "C3*D3", check);
  theCout << "add constant to submatrix D3.add(5*I1,rows,cols) : " << eol;  //<< D3.add(5 * I1, rows, cols) << std::endl;
  nbErrors+=checkValues(D3.add(5 *I1, rows, cols),rootname+"/D3_add5I1.in", tol, errors, "D3.add(5.*I1, rows, cols)", check);
  D3.saveToFile("D3Adense.mat", _dense);
  theCout << "save D3 to file D3Adense.mat in dense format" << std::endl;
  D3.saveToFile("D3Acoo.mat", _coo);
  theCout << "save D3 to file D3Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/D3.in", errors, "LargeMatrix<Matrix<Real> > D3", check);


  LargeMatrix<Matrix<Complex> > E3(6, 3, _dense, _dual, 2.*J1);
  theCout << "matrix E3 : " << E3 << std::endl;
  nbErrors+=checkValues( E3, rootname+"/E3.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  E3", check);
  theCout << "access \nE3(1,1)= " << E3(1, 1) << " \nE3(2,1)=" << E3(2, 1) << " \nE3(1,2)=" << E3(1, 2) << std::endl;
  nbErrors+=checkValues(E3(1,1), 2*J1, 0., errors, "E3(1,1)"); 
  nbErrors+=checkValues(E3(1,2), 2*J1, 0., errors, "E3(1,2)"); 
  nbErrors+=checkValues(E3(2,1), 2*J1, 0., errors, "E3(2,1)");
  theCout << "product E3*X2 : " << E3* X2 << std::endl;
  theCout << "product E3*C2 : " << E3* C2 << std::endl;
  theCout << "product X3*E3 : " << X3* E3 << std::endl;
  theCout << "product C3*E3 : " << C3* E3 << std::endl;
  nbErrors+=checkValues(E3*X2,rootname+"/E3fX2.in", tol, errors, "E3*X2",check); 
  nbErrors+=checkValues(E3*C2,rootname+"/E3fC2.in", tol, errors, "E3*C2", check); 
  nbErrors+=checkValues(X3*E3,rootname+"/X3fE3.in", tol, errors, "X3*E3",check); 
  nbErrors+=checkValues(C3*E3,rootname+"/C3fE3.in", tol, errors, "C3*E3", check);
  E3.saveToFile("E3Adense.mat", _dense);
  theCout << "save E3 to file E3Adense.mat in dense format" << std::endl;
  E3.saveToFile("E3Acoo.mat", _coo);
  theCout << "save E3 to file E3Acoo.mat in coordinates format" << std::endl;
  nbErrors+=checkValue(theCout, rootname+"/E3.in", errors, "LargeMatrix<Matrix<Complex> > E3", check);

  out << std::endl << "----------------------------------- test sym dense storage -----------------------------------" << std::endl;
  LargeMatrix<Real> A4(3, 3, _dense, _symmetric, 2.);
  theCout << "matrix A4 : " << A4 << std::endl;
  nbErrors+=checkValues( A4, rootname+"/A4.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  A4", check);
  theCout << "access A4(1,1)= " << A4(1, 1) << " A4(2,1)=" << A4(2, 1) << " A4(1,2)=" << A4(1, 2) << std::endl; 
  nbErrors+=checkValue(A4(1,1), 2, errors, "A4(1,1)"); 
  nbErrors+=checkValue(A4(1,2), 2, errors, "A4(1,2)"); 
  nbErrors+=checkValue(A4(2,1), 2, errors, "A4(2,1)"); 
  theCout << "product A4*x2 : " << A4* x2 << std::endl;
  theCout << "product A4*c2 : " << A4* c2 << std::endl;
  theCout << "product x2*A4 : " << x2* A4 << std::endl;
  theCout << "product c2*A4 : " << c2* A4 << std::endl;
  nbErrors+=checkValues(A4*x2,rootname+"/A4fx2.in", tol, errors, "A4*x2",check); 
  nbErrors+=checkValues(A4*c2,rootname+"/A4fC2.in", tol, errors, "A4*c2", check); 
  nbErrors+=checkValues(x2*A4,rootname+"/x2fA4.in", tol, errors, "x2*A4",check); 
  nbErrors+=checkValues(c2*A4,rootname+"/c2fA4.in", tol, errors, "c2*A4", check);
  theCout << "add constant to submatrix A4.add(5,rows,cols) : " << eol; //<< A4.add(5., rows, cols) << std::endl;
  nbErrors+=checkValues(A4.add(5., rows, cols),rootname+"/A4_add5.in", tol, errors, "A4.add(5., rows, cols)", check);
  theCout << "add submatrix to A4 A4.add(m22,rows,cols) : " << A4.add(m22, rows, cols) << std::endl;
  nbErrors+=checkValues(A4.add(m22, rows, cols) ,rootname+"/A4_addm22.in", tol, errors, "A4.add(m22, rows, cols)", check); 
  theCout << "row 1 (col & adrs) : " << A4.getRow(1) << std::endl;
  theCout << "row 2 (col & adrs) : " << A4.getRow(2) << std::endl;
  theCout << "row 3 (col & adrs) : " << A4.getRow(3) << std::endl;
  theCout << "col 1 (col & adrs) : " << A4.getCol(1) << std::endl;
  theCout << "col 2 (col & adrs) : " << A4.getCol(2) << std::endl;
  theCout << "col 3 (col & adrs) : " << A4.getCol(3) << std::endl;
  nbErrors+=checkValues(A4.getRow(1), rootname+"/A4getRow1.in", tol, errors, "A4.getRow(1)", check);
  nbErrors+=checkValues(A4.getRow(2), rootname+"/A4getRow2.in", tol, errors, "A4.getRow(2)", check);
  nbErrors+=checkValues(A4.getRow(3), rootname+"/A4getRow3.in", tol, errors, "A4.getRow(3)", check);
  nbErrors+=checkValues(A4.getCol(1), rootname+"/A4getCol1.in", tol, errors, "A4.getCol(1)", check);
  nbErrors+=checkValues(A4.getCol(2), rootname+"/A4getCol2.in", tol, errors, "A4.getCol(2)", check);
  nbErrors+=checkValues(A4.getCol(3), rootname+"/A4getCol3.in", tol, errors, "A4.getCol(3)", check);
  nbErrors+=checkValue(theCout, rootname+"/A4.in", errors, "LargeMatrix<Real> A4", check);

  LargeMatrix<Complex> B4(3, 3, _dense, _symmetric, 2.);
  theCout << "matrix B4 : " << B4 << std::endl;
  nbErrors+=checkValues( B4, rootname+"/B4.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  B4", check);
  theCout << "access B4(1,1)= " << B4(1, 1) << " B4(2,1)=" << B4(2, 1) << " B4(1,2)=" << B4(1, 2) << std::endl;
  nbErrors+=checkValue(B4(1,1), Complex (2,0), errors, "B4(1,1)"); 
  nbErrors+=checkValue(B4(1,2), Complex (2,0), errors, "B4(1,2)"); 
  nbErrors+=checkValue(B4(2,1), Complex (2,0), errors, "B4(2,1)"); 
  theCout << "product B4*x2 : " << B4* x2 << std::endl;
  theCout << "product B4*c2 : " << B4* c2 << std::endl;
  theCout << "product x2*B4 : " << x2* B4 << std::endl;
  theCout << "product c2*B4 : " << c2* B4 << std::endl;
  nbErrors+=checkValues(B4*x2,rootname+"/B4fx2.in", tol, errors, "B4*x2",check); 
  nbErrors+=checkValues(B4*c2,rootname+"/B4fc2.in", tol, errors, "B4*c2", check); 
  nbErrors+=checkValues(x2*B4,rootname+"/x2fB4.in", tol, errors, "x2*B4",check); 
  nbErrors+=checkValues(c2*B4,rootname+"/c2fB4.in", tol, errors, "c2*B4", check);
  theCout << "add constant to submatrix B4.add(5,rows,cols) : " << eol; // << B4.add(5., rows, cols) << std::endl;
  nbErrors+=checkValues(B4.add(5., rows, cols),rootname+"/B4_add5.in", tol, errors, "B4.add(5., rows, cols)", check);
  theCout << "add submatrix to B4 B4.add(cmplx(m22),rows,cols) : " << eol; //<< B4.add(cmplx(m22), rows, cols) << std::endl;
  nbErrors+=checkValues(B4.add(cmplx(m22), rows, cols) ,rootname+"/B4_addm22.in", tol, errors, "B4.add(m22, rows, cols)", check); 
  nbErrors+=checkValue(theCout, rootname+"/B4.in", errors, "LargeMatrix<Complex> B4", check);

  LargeMatrix<Matrix<Real> > D4(3, 3, _dense, _symmetric, 2.*I1);
  theCout << "matrix D4 : " << D4 << std::endl;
  nbErrors+=checkValues( D4, rootname+"/D4.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  D4", check); 
  theCout << "access \nD4(1,1)= " << D4(1, 1) << " \nD4(2,1)=" << D4(2, 1) << " \nD4(1,2)=" << D4(1, 2) << std::endl;
  nbErrors+=checkValues(D4(1,1), 2*I1, 0, errors, "D4(1,1)"); 
  nbErrors+=checkValues(D4(1,2), 2*I1, 0, errors, "D4(1,2)"); 
  nbErrors+=checkValues(D4(2,1), 2*I1, 0, errors, "D4(2,1)"); 
  theCout << "product D4*X2 : " << D4* X2 << std::endl;
  theCout << "product D4*C2 : " << D4* C2 << std::endl;
  theCout << "product X2*D4 : " << X2* D4 << std::endl;
  theCout << "product C2*D4 : " << C2* D4 << std::endl;
  nbErrors+=checkValues(D4*X2,rootname+"/D4fX2.in", tol, errors, "D4*X2",check); 
  nbErrors+=checkValues(D4*C2,rootname+"/D4fC2.in", tol, errors, "D4*X2", check); 
  nbErrors+=checkValues(X2*D4,rootname+"/X2fD4.in", tol, errors, "X2*D4",check); 
  nbErrors+=checkValues(C2*D4,rootname+"/C2fD4.in", tol, errors, "C2*D4", check);
  theCout << "add constant to submatrix D4.add(5*I1,rows,cols) : " <<eol; //<< D4.add(5 * I1, rows, cols) << std::endl;
  nbErrors+=checkValues(D4.add(5*I1, rows, cols) ,rootname+"/D4_addm22.in", tol, errors, "D4.add(m22, rows, cols)", check); 
  nbErrors+=checkValue(theCout, rootname+"/D4.in", errors, "LargeMatrix<Matrix<Real> > D4", check);

  LargeMatrix<Matrix<Complex> > E4(3, 3, _dense, _symmetric, 2.*J1);
  theCout << "matrix E4 : " << E4 << std::endl;
  nbErrors+=checkValues( E4, rootname+"/E4.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  E4", check);
  theCout << "access \nE4(1,1)= " << E4(1, 1) << " \nE4(2,1)=" << E4(2, 1) << " \nE4(1,2)=" << E4(1, 2) << std::endl;
  nbErrors+=checkValues(E4(1,1), 2*J1, 0, errors, "E4(1,1)"); 
  nbErrors+=checkValues(E4(1,2), 2*J1, 0, errors, "E4(1,2)"); 
  nbErrors+=checkValues(E4(2,1), 2*J1, 0, errors, "E4(2,1)"); 
  theCout << "product E4*X2 : " << E4* X2 << std::endl;
  theCout << "product E4*C2 : " << E4* C2 << std::endl;
  theCout << "product X2*E4 : " << X2* E4 << std::endl;
  theCout << "product C2*E4 : " << C2* E4 << std::endl;
  nbErrors+=checkValues(E4*X2,rootname+"/E4fX2.in", tol, errors, "E4*X2",check); 
  nbErrors+=checkValues(E4*C2,rootname+"/E4fC2.in", tol, errors, "E4*X2", check); 
  nbErrors+=checkValues(X2*E4,rootname+"/X2fE4.in", tol, errors, "X2*E4",check); 
  nbErrors+=checkValues(C2*E4,rootname+"/C2fE4.in", tol, errors, "C2*E4", check);
  nbErrors+=checkValue(theCout, rootname+"/E4.in", errors, "LargeMatrix<Matrix<Complex> > E4", check);

  out << std::endl << "----------------------------------- test skew symetric dense storage -----------------------------------" << std::endl;
  LargeMatrix<Real> A4skew(3, 3, _dense, _skewSymmetric, 2.);
  theCout << "matrix A4skew : " << A4skew << std::endl;
  nbErrors+=checkValues( A4skew, rootname+"/A4skew.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  A4skew", check);
  theCout << "access A4skew(1,1)= " << A4skew(1, 1) << " A4skew(2,1)=" << A4skew(2, 1) << " A4skew(1,2)=" << A4skew(1, 2) << std::endl;
  nbErrors+=checkValue(A4skew(1,1), 0, errors, "A4skew(1,1)");
  nbErrors+=checkValue(A4skew(2,1), 2, errors, "A4skew(2,1)");
  nbErrors+=checkValue(A4skew(1,2), 2, errors, "A4skew(1,2)");
  theCout << "product A4skew*x2 : " << A4skew* x2 << std::endl;
  theCout << "product A4skew*c2 : " << A4skew* c2 << std::endl;
  theCout << "product x2*A4skew : " << x2* A4skew << std::endl;
  theCout << "product c2*A4skew : " << c2* A4skew << std::endl;
  nbErrors+=checkValues(A4skew*x2,rootname+"/A4skewfx2.in", tol, errors, "A4skew*x2",check); 
  nbErrors+=checkValues(A4skew*c2,rootname+"/A4skewfC2skew.in", tol, errors, "A4skew*c2", check); 
  nbErrors+=checkValues(x2*A4skew,rootname+"/x2fA4skew.in", tol, errors, "x2*A4skew",check); 
  nbErrors+=checkValues(c2*A4skew,rootname+"/c2fA4skew.in", tol, errors, "c2*A4skew", check);
  nbErrors+=checkValue(theCout, rootname+"/A4skew.in", errors, "LargeMatrix<Real > A4skew", check);

  LargeMatrix<Complex> B4skew(3, 3, _dense, _skewSymmetric, 2.);
  theCout << "matrix B4skew : " << B4skew << std::endl;
  nbErrors+=checkValues( B4skew, rootname+"/B4skew.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  B4skew", check);
  theCout << "access B4skew(1,1)= " << B4skew(1, 1) << " B4skew(2,1)=" << B4skew(2, 1) << " B4skew(1,2)=" << B4skew(1, 2) << std::endl;
  nbErrors+=checkValue(B4skew(1,1), Complex (0,0), errors, "B4skew(1,1)");
  nbErrors+=checkValue(B4skew(2,1), Complex (2,0), errors, "B4skew(2,1)");
  nbErrors+=checkValue(B4skew(1,2), Complex (2,0), errors, "B4skew(1,2)");
  theCout << "product B4skew*x2 : " << B4skew* x2 << std::endl;
  theCout << "product B4skew*c2 : " << B4skew* c2 << std::endl;
  theCout << "product x2*B4skew : " << x2* B4skew << std::endl;
  theCout << "product c2*B4skew : " << c2* B4skew << std::endl;
  nbErrors+=checkValues(B4skew*x2,rootname+"/B4skewfx2.in", tol, errors, "B4skew*x2",check); 
  nbErrors+=checkValues(B4skew*c2,rootname+"/B4skewfC2skew.in", tol, errors, "B4skew*c2", check); 
  nbErrors+=checkValues(x2*B4skew,rootname+"/x2fB4skew.in", tol, errors, "x2*B4skew",check); 
  nbErrors+=checkValues(c2*B4skew,rootname+"/c2fB4skew.in", tol, errors, "c2*B4skew", check);
  nbErrors+=checkValue(theCout, rootname+"/B4skew.in", errors, "LargeMatrix<Complex > B4skew", check);

  LargeMatrix<Matrix<Real> > D4skew(3, 3, _dense, _skewSymmetric, 2.*I1);
  theCout << "matrix D4skew : " << D4skew << std::endl;
  nbErrors+=checkValues( D4skew, rootname+"/D4skew.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  D4skew", check);
  theCout << "access \nD4skew(1,1)= " << D4skew(1, 1) << " \nD4skew(2,1)=" << D4skew(2, 1) << " \nD4skew(1,2)=" << D4skew(1, 2) << std::endl;
  nbErrors+=checkValues(D4skew(1,1), 0*I1, 0., errors, "D4skew(1,1)"); 
  nbErrors+=checkValues(D4skew(1,2), 2*I1, 0., errors, "D4skew(1,2)"); 
  nbErrors+=checkValues(D4skew(2,1), 2*I1, 0., errors, "D4skew(2,1)"); 
  theCout << "product D4skew*X2 : " << D4skew* X2 << std::endl;
  theCout << "product D4skew*C2 : " << D4skew* C2 << std::endl;
  theCout << "product X2*D4skew : " << X2* D4skew << std::endl;
  theCout << "product C2*D4skew : " << C2* D4skew << std::endl;
  nbErrors+=checkValues(D4skew*X2,rootname+"/D4skewfX2.in", tol, errors, "D4skew*X2",check); 
  nbErrors+=checkValues(D4skew*C2,rootname+"/D4skewfC2.in", tol, errors, "D4skew*X2", check); 
  nbErrors+=checkValues(X2*D4skew,rootname+"/X2fD4skew.in", tol, errors, "X2*D4skew",check); 
  nbErrors+=checkValues(C2*D4skew,rootname+"/C2fD4skew.in", tol, errors, "C2*D4skew", check);
  nbErrors+=checkValue(theCout, rootname+"/D4skew.in", errors, "LargeMatrix<Matrix<Real> > D4skew", check);

  LargeMatrix<Matrix<Complex> > E4skew(3, 3, _dense, _skewSymmetric, 2.*J1);
  theCout << "matrix E4skew : " << E4skew << std::endl;
  nbErrors+=checkValues( E4skew, rootname+"/E4skew.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  E4skew", check);
  theCout << "access \nE4skew(1,1)= " << E4skew(1, 1) << " \nE4skew(2,1)=" << E4skew(2, 1) << " \nE4skew(1,2)=" << E4skew(1, 2) << std::endl;
  nbErrors+=checkValues(E4skew(1,1), 0*J1, 0, errors, "E4skew(1,1)"); 
  nbErrors+=checkValues(E4skew(1,2), 2*J1, 0, errors, "E4skew(1,2)"); 
  nbErrors+=checkValues(E4skew(2,1), 2*J1, 0, errors, "E4skew(2,1)"); 
  theCout << "product E4skew*X2 : " << E4skew* X2 << std::endl;
  theCout << "product E4skew*C2 : " << E4skew* C2 << std::endl;
  theCout << "product X2*E4skew : " << X2* E4skew << std::endl;
  theCout << "product C2*E4skew : " << C2* E4skew << std::endl;
  nbErrors+=checkValues(E4skew*X2,rootname+"/E4skewfX2.in", tol, errors, "E4skew*X2",check); 
  nbErrors+=checkValues(E4skew*C2,rootname+"/E4skewfC2.in", tol, errors, "E4skew*X2", check); 
  nbErrors+=checkValues(X2*E4skew,rootname+"/X2fE4skew.in", tol, errors, "X2*E4skew",check); 
  nbErrors+=checkValues(C2*E4skew,rootname+"/C2fE4skew.in", tol, errors, "C2*E4skew", check);
  nbErrors+=checkValue(theCout, rootname+"/E4skew.in", errors, "LargeMatrix<Matrix<Complex> > E4skew", check);

  out << std::endl << "----------------------------------- test self adjoint dense storage -----------------------------------" << std::endl;
  LargeMatrix<Real> A4adj(3, 3, _dense, _selfAdjoint, 2.);
  theCout << "matrix A4adj : " << A4adj << std::endl;
  nbErrors+=checkValues( A4adj, rootname+"/A4adj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  A4adj", check);
  theCout << "access A4adj(1,1)= " << A4adj(1, 1) << " A4adj(2,1)=" << A4adj(2, 1) << " A4adj(1,2)=" << A4adj(1, 2) << std::endl;
  nbErrors+=checkValue(A4adj(1,1), 2, errors, "A4adj(1,1)");
  nbErrors+=checkValue(A4adj(2,1), 2, errors, "A4adj(2,1)");
  nbErrors+=checkValue(A4adj(1,2), 2, errors, "A4adj(1,2)");
  theCout << "product A4adj*x2 : " << A4adj* x2 << std::endl;
  theCout << "product A4adj*c2 : " << A4adj* c2 << std::endl;
  theCout << "product x2*A4adj : " << x2* A4adj << std::endl;
  theCout << "product c2*A4adj : " << c2* A4adj << std::endl;
  nbErrors+=checkValues(A4adj*x2,rootname+"/A4adjfx2.in", tol, errors, "A4adj*x2",check); 
  nbErrors+=checkValues(A4adj*c2,rootname+"/A4adjC2skew.in", tol, errors, "A4adj*c2", check); 
  nbErrors+=checkValues(x2*A4adj,rootname+"/x2fA4adj.in", tol, errors, "x2*A4adj",check); 
  nbErrors+=checkValues(c2*A4adj,rootname+"/c2fA4adj.in", tol, errors, "c2*A4adj", check);
  nbErrors+=checkValue(theCout, rootname+"/A4adj.in", errors, "LargeMatrix<Real > A4adj", check);

  LargeMatrix<Complex> B4adj(3, 3, _dense, _selfAdjoint, 2.);
  theCout << "matrix B4adj : " << B4adj << std::endl;
  nbErrors+=checkValues( B4adj, rootname+"/B4adj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  B4adj", check);
  theCout << "access B4adj(1,1)= " << B4adj(1, 1) << " B4adj(2,1)=" << B4adj(2, 1) << " B4adj(1,2)=" << B4adj(1, 2) << std::endl;
  nbErrors+=checkValue(B4adj(1,1), Complex(2,0), errors, "B4adj(1,1)");
  nbErrors+=checkValue(B4adj(2,1), Complex(2,0), errors, "B4adj(2,1)");
  nbErrors+=checkValue(B4adj(1,2), Complex(2,0), errors, "B4adj(1,2)");
  theCout << "product B4adj*x2 : " << B4adj* x2 << std::endl;
  theCout << "product B4adj*c2 : " << B4adj* c2 << std::endl;
  theCout << "product x2*B4adj : " << x2* B4adj << std::endl;
  theCout << "product c2*B4adj : " << c2* B4adj << std::endl;
  nbErrors+=checkValues(B4skew*x2,rootname+"/B4skewfx2.in", tol, errors, "B4skew*x2",check); 
  nbErrors+=checkValues(B4skew*c2,rootname+"/B4skewfC2skew.in", tol, errors, "B4skew*c2", check); 
  nbErrors+=checkValues(x2*B4skew,rootname+"/x2fB4skew.in", tol, errors, "x2*B4skew",check); 
  nbErrors+=checkValues(c2*B4skew,rootname+"/c2fB4skew.in", tol, errors, "c2*B4skew", check);
  nbErrors+=checkValue(theCout, rootname+"/B4adj.in", errors, "LargeMatrix<Complex > B4adj", check);

  LargeMatrix<Matrix<Real> > D4adj(3, 3, _dense, _selfAdjoint, 2.*I1);
  theCout << "matrix D4adj : " << D4adj << std::endl;
  nbErrors+=checkValues( D4adj, rootname+"/D4adj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  D4adj", check);
  theCout << "access \nD4adj(1,1)= " << D4adj(1, 1) << " \nD4adj(2,1)=" << D4adj(2, 1) << " \nD4adj(1,2)=" << D4adj(1, 2) << std::endl;
  nbErrors+=checkValues(D4adj(1,1), 2*I1, 0., errors, "D4adj(1,1)");
  nbErrors+=checkValues(D4adj(2,1), 2*I1, 0., errors, "D4adj(2,1)");
  nbErrors+=checkValues(D4adj(1,2), 2*I1, 0., errors, "D4adj(1,2)");
  theCout << "product D4adj*X2 : " << D4adj* X2 << std::endl;
  theCout << "product D4adj*C2 : " << D4adj* C2 << std::endl;
  theCout << "product X2*D4adj : " << X2* D4adj << std::endl;
  theCout << "product C2*D4adj : " << C2* D4adj << std::endl;
  nbErrors+=checkValues(D4adj*X2,rootname+"/D4adjfX2.in", tol, errors, "D4adj*X2",check); 
  nbErrors+=checkValues(D4adj*C2,rootname+"/D4adjfC2.in", tol, errors, "D4adj*X2", check); 
  nbErrors+=checkValues(X2*D4adj,rootname+"/X2fD4adj.in", tol, errors, "X2*D4adj",check); 
  nbErrors+=checkValues(C2*D4adj,rootname+"/C2fD4adj.in", tol, errors, "C2*D4adj", check);
  nbErrors+=checkValue(theCout, rootname+"/D4adj.in", errors, "LargeMatrix<Matrix<Real> > D4adj", check);

  LargeMatrix<Matrix<Complex> > E4adj(3, 3, _dense, _selfAdjoint, 2.*J1);
  theCout << "matrix E4adj : " << E4adj << std::endl;
  nbErrors+=checkValues( E4adj, rootname+"/E4adj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  E4adj", check);
  theCout << "access \nE4adj(1,1)= " << E4adj(1, 1) << " \nE4adj(2,1)=" << E4adj(2, 1) << " \nE4adj(1,2)=" << E4adj(1, 2) << std::endl;
  nbErrors+=checkValues(E4adj(1,1), 2*J1, 0., errors, "E4adj(1,1)");
  nbErrors+=checkValues(E4adj(2,1), 2*J1, 0., errors, "E4adj(2,1)");
  nbErrors+=checkValues(E4adj(1,2), 2*J1, 0., errors, "E4adj(1,2)");
  theCout << "product E4adj*X2 : " << E4adj* X2 << std::endl;
  theCout << "product E4adj*C2 : " << E4adj* C2 << std::endl;
  theCout << "product X2*E4adj : " << X2* E4adj << std::endl;
  theCout << "product C2*E4adj : " << C2* E4adj << std::endl;
  nbErrors+=checkValues(E4adj*X2,rootname+"/E4adjfX2.in", tol, errors, "E4adj*X2",check); 
  nbErrors+=checkValues(E4adj*C2,rootname+"/E4adjfC2.in", tol, errors, "E4adj*X2", check); 
  nbErrors+=checkValues(X2*E4adj,rootname+"/X2fE4adj.in", tol, errors, "X2*E4adj",check); 
  nbErrors+=checkValues(C2*E4adj,rootname+"/C2fE4adj.in", tol, errors, "C2*E4adj", check);
  nbErrors+=checkValue(theCout, rootname+"/E4adj.in", errors, "LargeMatrix<Matrix<Complex> > E4adj", check);

  out << std::endl << "----------------------------------- test skew adjoint dense storage -----------------------------------" << std::endl;
  LargeMatrix<Real> A4skewadj(3, 3, _dense, _skewAdjoint, 2.);
  theCout << "matrix A4skewadj : " << A4skewadj << std::endl;
  nbErrors+=checkValues( A4skewadj, rootname+"/A4skewadj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  A4skewadj", check);
  theCout << "access A4skewadj(1,1)= " << A4skewadj(1, 1) << " A4skewadj(2,1)=" << A4skewadj(2, 1) << " A4skewadj(1,2)=" << A4skewadj(1, 2) << std::endl;
  nbErrors+=checkValue(A4skewadj(1,1), 0, errors, "A4skewadj(1,1)");
  nbErrors+=checkValue(A4skewadj(2,1), 2, errors, "A4skewadj(2,1)");
  nbErrors+=checkValue(A4skewadj(1,2), 2, errors, "A4skewadj(1,2)");
  theCout << "product A4skewadj*x2 : " << A4skewadj* x2 << std::endl;
  theCout << "product A4skewadj*c2 : " << A4skewadj* c2 << std::endl;
  theCout << "product x2*A4skewadj : " << x2* A4skewadj << std::endl;
  theCout << "product c2*A4skewadj : " << c2* A4skewadj << std::endl;
  nbErrors+=checkValues(A4skewadj*x2,rootname+"/A4skewadjfx2.in", tol, errors, "A4skewadj*x2",check); 
  nbErrors+=checkValues(A4skewadj*c2,rootname+"/A4skewadjC2skew.in", tol, errors, "A4skewadj*c2", check); 
  nbErrors+=checkValues(x2*A4skewadj,rootname+"/x2fA4skewadj.in", tol, errors, "x2*A4skewadj",check); 
  nbErrors+=checkValues(c2*A4skewadj,rootname+"/c2fA4skewadj.in", tol, errors, "c2*A4skewadj", check);
  nbErrors+=checkValue(theCout, rootname+"/A4skewadj.in", errors, "LargeMatrix<Real > A4skewadj", check);


  LargeMatrix<Complex> B4skewadj(3, 3, _dense, _skewAdjoint, 2.);
  theCout << "matrix B4skewadj : " << B4skewadj << std::endl;
  nbErrors+=checkValues( B4skewadj, rootname+"/B4skewadj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  B4skewadj", check);
  theCout << "access B4skewadj(1,1)= " << B4skewadj(1, 1) << " B4skewadj(2,1)=" << B4skewadj(2, 1) << " B4skewadj(1,2)=" << B4skewadj(1, 2) << std::endl;
  nbErrors+=checkValue(B4skewadj(1,1), Complex(0,0), errors, "B4skewadj(1,1)");
  nbErrors+=checkValue(B4skewadj(2,1), Complex(2,0), errors, "B4skewadj(2,1)");
  nbErrors+=checkValue(B4skewadj(1,2), Complex(2,0), errors, "B4kewadj(1,2)");
  theCout << "product B4skewadj*x2 : " << B4skewadj* x2 << std::endl;
  theCout << "product B4skewadj*c2 : " << B4skewadj* c2 << std::endl;
  theCout << "product x2*B4skewadj : " << x2* B4skewadj << std::endl;
  theCout << "product c2*B4skewadj : " << c2* B4skewadj << std::endl;
  nbErrors+=checkValues(B4skewadj*x2,rootname+"/B4skewadjfx2.in", tol, errors, "B4skewadj*x2",check); 
  nbErrors+=checkValues(B4skewadj*c2,rootname+"/B4skewadjfC2skew.in", tol, errors, "B4skewadj*c2", check); 
  nbErrors+=checkValues(x2*B4skewadj,rootname+"/x2fB4skewadj.in", tol, errors, "x2*B4skewadj",check); 
  nbErrors+=checkValues(c2*B4skewadj,rootname+"/c2fB4skewadj.in", tol, errors, "c2*B4skewadj", check);
  nbErrors+=checkValue(theCout, rootname+"/B4skewadj.in", errors, "LargeMatrix<Complex > B4skewadj", check);

  LargeMatrix<Matrix<Real> > D4skewadj(3, 3, _dense, _skewAdjoint, 2.*I1);
  theCout << "matrix D4skewadj : " << D4skewadj << std::endl;
  nbErrors+=checkValues( D4skewadj, rootname+"/D4skewadj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  D4skewadj", check);
  theCout << "access \nD4skewadj(1,1)= " << D4skewadj(1, 1) << " \nD4skewadj(2,1)=" << D4skewadj(2, 1) << " \nD4skewadj(1,2)=" << D4skewadj(1, 2) << std::endl;
  nbErrors+=checkValues(D4skewadj(1,1), 0*I1, 0., errors, "D4skewadj(1,1)");
  nbErrors+=checkValues(D4skewadj(2,1), 2*I1, 0., errors, "D4skewadj(2,1)");
  nbErrors+=checkValues(D4skewadj(1,2), 2*I1, 0., errors, "D4skewadj(1,2)");
  theCout << "product X2*D4skewadj : " << X2* D4skewadj << std::endl;
  theCout << "product D4skewadj*C2 : " << D4skewadj* C2 << std::endl;
  theCout << "product D4skewadj*X2 : " << D4skewadj* X2 << std::endl;
  theCout << "product C2*D4skewadj : " << C2* D4skewadj << std::endl;
  nbErrors+=checkValues(D4skewadj*X2,rootname+"/D4skewadjfX2.in", tol, errors, "D4skewadj*X2",check); 
  nbErrors+=checkValues(D4skewadj*C2,rootname+"/D4skewadjfC2.in", tol, errors, "D4skewadj*X2", check); 
  nbErrors+=checkValues(X2*D4skewadj,rootname+"/X2fD4skewadj.in", tol, errors, "X2*D4skewadj",check); 
  nbErrors+=checkValues(C2*D4skewadj,rootname+"/C2fD4skewadj.in", tol, errors, "C2*D4skewadj", check);
  nbErrors+=checkValue(theCout, rootname+"/D4skewadj.in", errors, "LargeMatrix<Matrix<Real> > D4skewadj", check);

  LargeMatrix<Matrix<Complex> > E4skewadj(3, 3, _dense, _skewAdjoint, 2.*J1);
  theCout << "matrix E4skewadj : " << E4skewadj << std::endl;
  nbErrors+=checkValues( E4skewadj, rootname+"/E4skewadj.mat", tol, errors,  "LargeMatrix<Matrix<Real> >  E4skewadj", check);
  theCout << "access \nE4skewadj(1,1)= " << E4skewadj(1, 1) << " \nE4skewadj(2,1)=" << E4skewadj(2, 1) << " \nE4skewadj(1,2)=" << E4skewadj(1, 2) << std::endl;
  nbErrors+=checkValues(E4skewadj(1,1), 0*J1, 0., errors, "E4skewadj(1,1)");
  nbErrors+=checkValues(E4skewadj(2,1), 2*J1, 0., errors, "E4skewadj(2,1)");
  nbErrors+=checkValues(E4skewadj(1,2), 2*J1, 0., errors, "E4skewadj(1,2)");
  theCout << "product E4skewadj*X2 : " << E4skewadj* X2 << std::endl;
  theCout << "product E4skewadj*C2 : " << E4skewadj* C2 << std::endl;
  theCout << "product X2*E4skewadj : " << X2* E4skewadj << std::endl;
  theCout << "product C2*E4skewadj : " << C2* E4skewadj << std::endl;
  nbErrors+=checkValues(E4skewadj*X2,rootname+"/E4skewadjfX2.in", tol, errors, "E4skewadj*X2",check); 
  nbErrors+=checkValues(E4skewadj*C2,rootname+"/E4skewadjfC2.in", tol, errors, "E4skewadj*X2", check); 
  nbErrors+=checkValues(X2*E4skewadj,rootname+"/X2fE4skewadj.in", tol, errors, "X2*E4skewadj",check); 
  nbErrors+=checkValues(C2*E4skewadj,rootname+"/C2fE4skewadj.in", tol, errors, "C2*E4skewadj", check);
  nbErrors+=checkValue(theCout, rootname+"/E4skewadj.in", errors, "LargeMatrix<Matrix<Real> > E4skewadj", check);

  //!--------------------------------------------------------------------------
  //! Test Dense Storage with different kinds of matrix (sym, nosym, selfadjoint, ..)
  //!---------------------------------------------------------------------------
  const Number rowNum = 3;
  const Number colNum = 3;
  LargeMatrix<Real> rMatDenseRow(rowNum, colNum, _dense, _row, 1.);
  LargeMatrix<Real> rMatDenseCol(rowNum, colNum, _dense, _col, 1.);
  LargeMatrix<Real> rMatDenseDual(rowNum, colNum, _dense, _dual, 1.);
  LargeMatrix<Real> rMatDenseSym(rowNum, colNum, _dense, _symmetric, 1.);
  LargeMatrix<Complex> cMatDenseRow(rowNum, colNum, _dense, _row, 1.);
  LargeMatrix<Complex> cMatDenseCol(rowNum, colNum, _dense, _col, 1.);
  LargeMatrix<Complex> cMatDenseDual(rowNum, colNum, _dense, _dual, 1.);
  LargeMatrix<Complex> cMatDenseSym(rowNum, colNum, _dense, _symmetric, 1.);

  thePrintStream << std::endl << "----------------------------------- test diagonalMatrix -----------------------------------" << std::endl;
  thePrintStream << "Diagonal RowDense matrix : " << diagonalMatrix(rMatDenseRow, 3.) << std::endl;
  thePrintStream << "Diagonal ColDense matrix : " << diagonalMatrix(rMatDenseCol, 3.) << std::endl;
  thePrintStream << "Diagonal DualDense matrix : " << diagonalMatrix(rMatDenseDual, 3.) << std::endl;
  thePrintStream << "Diagonal SymDense matrix : " << diagonalMatrix(rMatDenseSym, 3.) << std::endl;
  thePrintStream << "Diagonal RowDense matrix : " << diagonalMatrix(cMatDenseRow, cmplx(3.0)) << std::endl;
  thePrintStream << "Diagonal ColDense matrix : " << diagonalMatrix(cMatDenseCol, cmplx(3.0)) << std::endl;
  thePrintStream << "Diagonal DualDense matrix : " << diagonalMatrix(cMatDenseDual, cmplx(3.0)) << std::endl;
  thePrintStream << "Diagonal SymDense matrix : " << diagonalMatrix(cMatDenseSym, cmplx(3.0)) << std::endl;
  nbErrors+=checkValues(diagonalMatrix(rMatDenseRow, 3.), 3., tol, errors, "LargeMatrix(diagonalMatrix(rMatDenseRow, 3.) != 3id", check);
  nbErrors+=checkValues(diagonalMatrix(rMatDenseCol, 3.), 3., tol, errors, "LargeMatrix(diagonalMatrix(rMatDenseCol, 3.) != 3id", check);
  nbErrors+=checkValues(diagonalMatrix(rMatDenseDual, 3.), 3., tol, errors, "LargeMatrix(diagonalMatrix(rMatDenseDual, 3.) != 3id", check);
  nbErrors+=checkValues(diagonalMatrix(rMatDenseSym, 3.), 3., tol, errors, "LargeMatrix(diagonalMatrix(rMatDenseSym, 3.) != 3id", check);
  nbErrors+=checkValues(diagonalMatrix(cMatDenseRow, cmplx(3.0)), cmplx(3.0), tol, errors, "LargeMatrix(diagonalMatrix(cMatDenseRow, 3.) != 3id", check);
  nbErrors+=checkValues(diagonalMatrix(cMatDenseCol, cmplx(3.0)), cmplx(3.0), tol, errors, "LargeMatrix(diagonalMatrix(cMatDenseCol, 3.) != 3id", check);
  nbErrors+=checkValues(diagonalMatrix(cMatDenseDual, cmplx(3.0)), cmplx(3.0), tol, errors, "LargeMatrix(diagonalMatrix(cMatDenseDual, 3.) != 3id", check);
  nbErrors+=checkValues(diagonalMatrix(cMatDenseSym, cmplx(3.0)), cmplx(3.0), tol, errors, "LargeMatrix(diagonalMatrix(cMatDenseSym, 3.) != 3id", check);

  thePrintStream << std::endl << "----------------------------------- test identityMatrix -----------------------------------" << std::endl;
  thePrintStream << "Identity RowDense matrix : " << identityMatrix(rMatDenseRow) << std::endl;
  thePrintStream << "Identity ColDense matrix : " << identityMatrix(rMatDenseCol) << std::endl;
  thePrintStream << "Identity DualDense matrix : " << identityMatrix(rMatDenseDual) << std::endl;
  thePrintStream << "Identity SymDense matrix : " << identityMatrix(rMatDenseSym) << std::endl;

  thePrintStream << "Identity RowDense matrix : " << identityMatrix(cMatDenseRow) << std::endl;
  thePrintStream << "Identity ColDense matrix : " << identityMatrix(cMatDenseCol) << std::endl;
  thePrintStream << "Identity DualDense matrix : " << identityMatrix(cMatDenseDual) << std::endl;
  thePrintStream << "Identity SymDense matrix : " << identityMatrix(cMatDenseSym) << std::endl;

  nbErrors+=checkValues(identityMatrix(rMatDenseRow), 1., tol, errors, "LargeMatrix(identityMatrix(rMatDenseRow, 3.) != id", check);
  nbErrors+=checkValues(identityMatrix(rMatDenseCol), 1., tol, errors, "LargeMatrix(identityMatrix(rMatDenseCol, 3.) != id", check);
  nbErrors+=checkValues(identityMatrix(rMatDenseDual), 1., tol, errors, "LargeMatrix(identityMatrix(rMatDenseDual, 3.) != id", check);
  nbErrors+=checkValues(identityMatrix(rMatDenseSym), 1., tol, errors, "LargeMatrix(identityMatrix(rMatDenseSym, 3.) != id", check);
  nbErrors+=checkValues(identityMatrix(cMatDenseRow), cmplx(1.0), tol, errors, "LargeMatrix(identityMatrix(cMatDenseRow, 3.) != id", check);
  nbErrors+=checkValues(identityMatrix(cMatDenseCol), cmplx(1.0), tol, errors, "LargeMatrix(identityMatrix(cMatDenseCol, 3.) != id", check);
  nbErrors+=checkValues(identityMatrix(cMatDenseDual), cmplx(1.0), tol, errors, "LargeMatrix(identityMatrix(cMatDenseDual, 3.) != id", check);
  nbErrors+=checkValues(identityMatrix(cMatDenseSym), cmplx(1.0), tol, errors, "LargeMatrix(identityMatrix(cMatDenseSym, 1.) != id", check);

  thePrintStream << std::endl << "----------------------------------- test scaleMatrix -----------------------------------" << std::endl;
  Real sigmaR = 3.0;
  Complex sigmaC = Complex(3.0, 3.0);
  thePrintStream << "product RowDense matrix : " <<  rMatDenseRow* sigmaR  << std::endl;
  thePrintStream << "product RowDense matrix : " <<  sigmaR* rMatDenseRow  << std::endl;
  thePrintStream << "product RowDense matrix : " <<  rMatDenseRow* sigmaC  << std::endl;
  thePrintStream << "product RowDense matrix : " <<  sigmaC* rMatDenseRow  << std::endl;
  thePrintStream << "product ColDense matrix : " <<  rMatDenseCol* sigmaR << std::endl;
  thePrintStream << "product ColDense matrix : " <<  sigmaR* rMatDenseCol << std::endl;
  thePrintStream << "product ColDense matrix : " <<  rMatDenseCol* sigmaC << std::endl;
  thePrintStream << "product ColDense matrix : " <<  sigmaC* rMatDenseCol << std::endl;
  thePrintStream << "product DualDense matrix : " << rMatDenseDual* sigmaR << std::endl;
  thePrintStream << "product DualDense matrix : " << sigmaR* rMatDenseDual << std::endl;
  thePrintStream << "product DualDense matrix : " << rMatDenseDual* sigmaC << std::endl;
  thePrintStream << "product DualDense matrix : " << sigmaC* rMatDenseDual << std::endl;
  thePrintStream << "product SymDense matrix : " <<  rMatDenseSym* sigmaR << std::endl;
  thePrintStream << "product SymDense matrix : " <<  sigmaR* rMatDenseSym << std::endl;
  thePrintStream << "product SymDense matrix : " <<  rMatDenseSym* sigmaC << std::endl;
  thePrintStream << "product SymDense matrix : " <<  sigmaC* rMatDenseSym << std::endl;
  thePrintStream << "product RowDense matrix : " <<  cMatDenseRow* sigmaR << std::endl;
  thePrintStream << "product RowDense matrix : " <<  sigmaR* cMatDenseRow << std::endl;
  thePrintStream << "product RowDense matrix : " <<  cMatDenseRow* sigmaC << std::endl;
  thePrintStream << "product RowDense matrix : " <<  sigmaC* cMatDenseRow << std::endl;
  thePrintStream << "product ColDense matrix : " <<  cMatDenseCol* sigmaR << std::endl;
  thePrintStream << "product ColDense matrix : " <<  sigmaR* cMatDenseCol << std::endl;
  thePrintStream << "product ColDense matrix : " <<  cMatDenseCol* sigmaC << std::endl;
  thePrintStream << "product ColDense matrix : " <<  sigmaC* cMatDenseCol << std::endl;
  thePrintStream << "product DualDense matrix : " << cMatDenseDual* sigmaR << std::endl;
  thePrintStream << "product DualDense matrix : " << sigmaR* cMatDenseDual << std::endl;
  thePrintStream << "product DualDense matrix : " << cMatDenseDual* sigmaC << std::endl;
  thePrintStream << "product DualDense matrix : " << sigmaC* cMatDenseDual << std::endl;
  thePrintStream << "product SymDense matrix : " <<  cMatDenseSym* sigmaR << std::endl;
  thePrintStream << "product SymDense matrix : " <<  sigmaR* cMatDenseSym << std::endl;
  thePrintStream << "product SymDense matrix : " <<  cMatDenseSym* sigmaC << std::endl;
  thePrintStream << "product SymDense matrix : " <<  sigmaC* cMatDenseSym << std::endl;
  nbErrors+=checkValues(rMatDenseRow* sigmaR ,rootname+"/rMatDenseRowFsigmaR.in", tol, errors, "rMatDenseRow* sigmaR", check);
  nbErrors+=checkValues(sigmaR* rMatDenseRow ,rootname+"/sigmaRFrMatDenseRow.in", tol, errors, "sigmaR *rMatDenseRow", check);
  nbErrors+=checkValues(rMatDenseRow* sigmaC ,rootname+"/rMatDenseRowFsigmaC.in", tol, errors, "rMatDenseRow* sigmaC", check);
  nbErrors+=checkValues(sigmaC* rMatDenseRow ,rootname+"/sigmaCFrMatDenseRow.in", tol, errors, "sigmaC *rMatDenseRow", check);
  nbErrors+=checkValues(rMatDenseCol* sigmaR ,rootname+"/rMatDenseColFsigmaR.in", tol, errors, "rMatDenseCol* sigmaR", check);
  nbErrors+=checkValues(sigmaR* rMatDenseCol ,rootname+"/sigmaRFrMatDenseCol.in", tol, errors, "sigmaR *rMatDenseCol", check);
  nbErrors+=checkValues(rMatDenseCol* sigmaC ,rootname+"/rMatDenseColFsigmaC.in", tol, errors, "rMatDenseCol* sigmaC", check);
  nbErrors+=checkValues(sigmaC* rMatDenseCol ,rootname+"/sigmaCFrMatDenseCol.in", tol, errors, "sigmaC *rMatDenseCol", check);
  nbErrors+=checkValues(cMatDenseRow* sigmaR ,rootname+"/cMatDenseRowFsigmaR.in", tol, errors, "cMatDenseRow* sigmaR", check);
  nbErrors+=checkValues(sigmaR* cMatDenseRow ,rootname+"/sigmaRFcMatDenseRow.in", tol, errors, "sigmaR *cMatDenseRow", check);
  nbErrors+=checkValues(cMatDenseRow* sigmaC ,rootname+"/cMatDenseRowFsigmaC.in", tol, errors, "cMatDenseRow* sigmaC", check);
  nbErrors+=checkValues(sigmaC* cMatDenseRow ,rootname+"/sigmaCFcMatDenseRow.in", tol, errors, "sigmaC *cMatDenseDual", check);
  nbErrors+=checkValues(cMatDenseSym* sigmaR ,rootname+"/cMatDenseSymFsigmaR.in", tol, errors, "cMatDenseSym* sigmaR", check);
  nbErrors+=checkValues(sigmaR* cMatDenseSym ,rootname+"/sigmaRFcMatDenseSym.in", tol, errors, "sigmaR *cMatDenseSym", check);
  nbErrors+=checkValues(cMatDenseSym* sigmaC ,rootname+"/cMatDenseSymFsigmaC.in", tol, errors, "cMatDenseSym* sigmaC", check);
  nbErrors+=checkValues(sigmaC* cMatDenseSym ,rootname+"/sigmaCFcMatDenseSym.in", tol, errors, "sigmaC *cMatDenseSym", check);
  nbErrors+=checkValues(cMatDenseRow* sigmaR ,rootname+"/cMatDenseRowFsigmaR.in", tol, errors, "cMatDenseRow* sigmaR", check);
  nbErrors+=checkValues(sigmaR* cMatDenseRow ,rootname+"/sigmaRFcMatDenseRow.in", tol, errors, "sigmaR *cMatDenseRow", check);
  nbErrors+=checkValues(cMatDenseRow* sigmaC ,rootname+"/cMatDenseRowFsigmaC.in", tol, errors, "cMatDenseRow* sigmaC", check);
  nbErrors+=checkValues(sigmaC* cMatDenseRow ,rootname+"/sigmaCFcMatDenseRow.in", tol, errors, "sigmaC *cMatDenseRow", check);
  nbErrors+=checkValues(cMatDenseCol* sigmaR ,rootname+"/cMatDenseColFsigmaR.in", tol, errors, "cMatDenseCol* sigmaR", check);
  nbErrors+=checkValues(sigmaR* cMatDenseCol ,rootname+"/sigmaRFcMatDenseCol.in", tol, errors, "sigmaR *cMatDenseCol", check);
  nbErrors+=checkValues(cMatDenseCol* sigmaC ,rootname+"/cMatDenseColFsigmaC.in", tol, errors, "cMatDenseCol* sigmaC", check);
  nbErrors+=checkValues(sigmaC* cMatDenseCol ,rootname+"/sigmaCFcMatDenseCol.in", tol, errors, "sigmaC *cMatDenseCol", check);
  nbErrors+=checkValues(cMatDenseRow* sigmaR ,rootname+"/cMatDenseRowFsigmaR.in", tol, errors, "cMatDenseRow* sigmaR", check);
  nbErrors+=checkValues(sigmaR* cMatDenseRow ,rootname+"/sigmaRFcMatDenseRow.in", tol, errors, "sigmaR *cMatDenseRow", check);
  nbErrors+=checkValues(cMatDenseRow* sigmaC ,rootname+"/cMatDenseRowFsigmaC.in", tol, errors, "cMatDenseRow* sigmaC", check);
  nbErrors+=checkValues(sigmaC* cMatDenseRow ,rootname+"/sigmaCFcMatDenseRow.in", tol, errors, "sigmaC *cMatDenseDual", check);
  nbErrors+=checkValues(cMatDenseSym* sigmaR ,rootname+"/cMatDenseSymFsigmaR.in", tol, errors, "cMatDenseSym* sigmaR", check);
  nbErrors+=checkValues(sigmaR* cMatDenseSym ,rootname+"/sigmaRFcMatDenseSym.in", tol, errors, "sigmaR *cMatDenseSym", check);
  nbErrors+=checkValues(cMatDenseSym* sigmaC ,rootname+"/cMatDenseSymFsigmaC.in", tol, errors, "cMatDenseSym* sigmaC", check);
  nbErrors+=checkValues(sigmaC* cMatDenseSym ,rootname+"/sigmaCFcMatDenseSym.in", tol, errors, "sigmaC *cMatDenseSym", check);


  thePrintStream << std::endl << "----------------------------------- test addMatrixMatrix -----------------------------------" << std::endl;
  LargeMatrix<Real> rDenseRow(rMatDenseRow); LargeMatrix<Real> rDenseCol(rMatDenseCol); LargeMatrix<Real> rDenseDual(rMatDenseDual); LargeMatrix<Real> rDenseSym(rMatDenseSym);
  LargeMatrix<Complex> cDenseRow(cMatDenseRow); LargeMatrix<Complex> cDenseCol(cMatDenseCol); LargeMatrix<Complex> cDenseDual(cMatDenseDual);  LargeMatrix<Complex> cDenseSym(cMatDenseSym);

  thePrintStream << "Sum of two row dense matrices : " << eol; //<< rDenseRow + rMatDenseRow << std::endl;
  nbErrors+=checkValues(rDenseRow + rMatDenseRow ,rootname+"/rDenseRowPrMatDenseRow.in", tol, errors, "rDenseRow + rMatDenseRow", check);
  thePrintStream << "Sum of two col dense matrices : "<< eol; // << rDenseCol + rMatDenseCol << std::endl;
  nbErrors+=checkValues(rDenseCol + rMatDenseCol ,rootname+"/rDenseColPrMatDenseCol.in", tol, errors, "rDenseCol + rMatDenseCol", check);
  thePrintStream << "Sum of two dual dense matrices : " << eol; // << rDenseDual + rMatDenseDual << std::endl;
  nbErrors+=checkValues(rDenseDual + rMatDenseDual ,rootname+"/rDenseDualPrMatDenseDual.in", tol, errors, "rDenseDual + rMatDenseDual", check);
  thePrintStream << "Sum of two sym dense matrices : " <<eol;   //<< rDenseSym + rMatDenseSym << std::endl;
  nbErrors+=checkValues(rDenseSym + rMatDenseSym ,rootname+"/rDenseSymPrMatDenseSym.in", tol, errors, "rDenseSym + rMatDenseSym", check);

  thePrintStream << std::endl << "----------------------------------- test Matrix product -----------------------------------" << std::endl;

  thePrintStream << "product of two row dense matrices : "  << eol; //  << rDenseRow* rDenseRow  << std::endl;
  thePrintStream << "product of row dense by a col dense : " << eol; //  << rDenseRow* rDenseCol  << std::endl;
  thePrintStream << "product of row dense by a dual dense : "<< eol; //  << rDenseRow* rDenseDual << std::endl;
  thePrintStream << "product of row dense by a sym dense : "<< eol; //   << rDenseRow* rDenseSym  << std::endl;  // !!!!
  nbErrors+=checkValues(rDenseRow* rDenseRow ,rootname+"/rDenseRowFrDenseRow.in", tol, errors, "rDenseRow* rDenseRow",check);
  nbErrors+=checkValues(rDenseRow* rDenseCol ,rootname+"/rDenseRowFrDenseCol.in", tol, errors, "rDenseRow* rDenseCol",check);
  nbErrors+=checkValues(rDenseRow* rDenseDual ,rootname+"/rDenseRowFrDenseDual.in", tol, errors, "rDenseRow* rDenseDual",check);
  nbErrors+=checkValues(rDenseRow* rDenseSym ,rootname+"/rDenseRowFrDenseSym.in", tol, errors, "rDenseRow* rDenseSym",check);

  thePrintStream << "product of two col dense matrices : "    << eol; // << rDenseCol* rDenseCol  << std::endl;
  thePrintStream << "product of col dense by a row dense : " << eol; //  << rDenseCol* rDenseRow  << std::endl;
  thePrintStream << "product of col dense by a dual dense : " << eol; // << rDenseCol* rDenseDual << std::endl;
  thePrintStream << "product of col dense by a sym dense : "  << eol; // << eol; // << rDenseCol* rDenseSym  << std::endl;  // !!!!
  nbErrors+=checkValues(rDenseCol* rDenseRow ,rootname+"/rDenseColFrDenseRow.in", tol, errors, "rDenseCol* rDenseRow",check);
  nbErrors+=checkValues(rDenseCol* rDenseCol ,rootname+"/rDenseColFrDenseCol.in", tol, errors, "rDenseCol* rDenseCol",check);
  nbErrors+=checkValues(rDenseCol* rDenseDual ,rootname+"/rDenseColFrDenseDual.in", tol, errors, "rDenseCol* rDenseDual",check);
  nbErrors+=checkValues(rDenseCol* rDenseSym ,rootname+"/rDenseColFrDenseSym.in", tol, errors, "rDenseCol* rDenseSym",check);

  thePrintStream << "product of two sym dense matrices : "    << eol; // << rDenseSym*   rDenseSym  << std::endl;
  thePrintStream << "product of sym dense by a row dense : "  << eol; // << rDenseSym*   rDenseRow  << std::endl;
  thePrintStream << "product of sym dense by a col dense : "  << eol; // << rDenseSym*   rDenseCol  << std::endl;
  thePrintStream << "product of sym dense by a dual dense : " << eol; // << rDenseSym*   rDenseDual << std::endl;
  nbErrors+=checkValues(rDenseSym* rDenseRow ,rootname+"/rDenseSymFrDenseRow.in", tol, errors, "rDenseSym* rDenseRow",check);
  nbErrors+=checkValues(rDenseSym* rDenseCol ,rootname+"/rDenseSymFrDenseCol.in", tol, errors, "rDenseSym* rDenseCol",check);
  nbErrors+=checkValues(rDenseSym* rDenseDual ,rootname+"/rDenseSymFrDenseDual.in", tol, errors, "rDenseSym* rDenseDual",check);
  nbErrors+=checkValues(rDenseSym* rDenseSym ,rootname+"/rDenseSymFrDenseSym.in", tol, errors, "rDenseSym* rDenseSym",check);

  thePrintStream << "product of two dual dense matrices : "   << eol; // << rDenseDual* rDenseDual << std::endl;
  thePrintStream << "product of dual dense by a row dense : " << eol; // << rDenseDual* rDenseRow  << std::endl;
  thePrintStream << "product of dual dense by a col dense : " << eol; // << rDenseDual* rDenseCol  << std::endl;
  thePrintStream << "product of dual dense by a sym dense : " << eol; // << rDenseDual* rDenseSym  << std::endl;  // !!!!
  nbErrors+=checkValues(rDenseDual* rDenseRow ,rootname+"/rDenseDualFrDenseRow.in", tol, errors, "rDenseDual* rDenseRow",check);
  nbErrors+=checkValues(rDenseDual* rDenseCol ,rootname+"/rDenseDualFrDenseCol.in", tol, errors, "rDenseDual* rDenseCol",check);
  nbErrors+=checkValues(rDenseDual* rDenseDual ,rootname+"/rDenseDualFrDenseDual.in", tol, errors, "rDenseDual* rDenseDual",check);
  nbErrors+=checkValues(rDenseDual* rDenseSym ,rootname+"/rDenseDualFrDenseSym.in", tol, errors, "rDenseDual* rDenseSym",check);

  thePrintStream << "product of complex row dense by a real row dense : "  << eol; // << cDenseRow*   rDenseRow  << std::endl;
  thePrintStream << "product of complex col dense by a real row dense : "  << eol; // << cDenseCol*   rDenseRow  << std::endl;
  thePrintStream << "product of real row dense by a complex row dense : "  << eol; // << rDenseRow*   cDenseRow  << std::endl;
  thePrintStream << "product of real col dense by a complex row dense : "  << eol; // << rDenseCol*   cDenseRow  << std::endl;
  nbErrors+=checkValues(cDenseRow* rDenseRow ,rootname+"/cDenseRowFrDenseRow.in", tol, errors, "cDenseDual* rDenseRow",check);
  nbErrors+=checkValues(cDenseCol* rDenseRow ,rootname+"/cDenseColFrDenseRow.in", tol, errors, "cDenseCol* rDenseRow",check);
  nbErrors+=checkValues(rDenseRow* cDenseRow ,rootname+"/rDenseRowFcDenseRow.in", tol, errors, "rDenseRow* cDenseRow",check);
  nbErrors+=checkValues(rDenseCol* rDenseRow ,rootname+"/rDenseColFcDenseRow.in", tol, errors, "rDenseCol* cDenseCol",check);

  //!--------------------------------------------------------------------------
  //! Test Dense Storage matrix x vector in openmp if available
  //!---------------------------------------------------------------------------
 Number n=9;
 std::vector<Real> x(n,1.);
 #ifdef XLIFEPP_WITH_OMP

  LargeMatrix<Real> Ard(n, n, _dense, _row, 1.);
  LargeMatrix<Real> Acd(n, n, _dense, _col, 1.);
  LargeMatrix<Real> Asd(n, n, _dense, _symmetric, 1.);
  LargeMatrix<Real> Add(n, n, _dense, _dual, 1.);
  std::vector<Real> ys(n,0.), yns(n,0.), y(n), zs(n), zns(n);

//!test for small matrices
  for(Number i=1; i<=n; i++)
    {
      x[i-1]=i;
      Real& tnsi = yns[i-1];
      for(Number j=1; j<=n; j++)
        {
          Real aij = 10*i+j;
          Ard(i,j)=aij;
          Acd(i,j)=aij;
          Add(i,j)=aij;
          tnsi+=aij*j;
          zns[j-1]+=aij*i;
        }
      Real& tsi = ys[i-1];
      for(Number j=1; j<=i; j++)
        {
          Real aij = 10*i+j;
          Asd(i,j)=aij;
          tsi+= aij*j;
          zs[j-1]+=aij*i;
          if(i!=j) { ys[j-1]+=aij*i; zs[i-1]+=aij*j;}

        }
    }
  thePrintStream<<"\n==== parallel matrix * vector, vector * matrix, result should be [0 0 .. 0] ===="<<eol;
  Number nth = 1;
  #pragma omp parallel for lastprivate(nth)
  for(Number i = 0; i < 1; i++) {nth = omp_get_num_threads();}

//!  row_dense
  thePrintStream<<"==== row dense product ===="<<eol;
  thePrintStream<<"A="<<Ard<<eol;
  Environment::parallel(false);
  y=Ard*x;
  thePrintStream<<"   no omp    : Ard*x-yex = "<<y-yns<<eol;
  y=x*Ard;
  out<<"   no omp    : x*Ard-yex = "<<y-zns<<eol;
  Environment::parallel(true);
  for(Number n=1; n<=nth; n++)
    {
      numberOfThreads(n);
      y=Ard*x;
      out<<tostring(n)+" thread(s)  : Ard*x-yex = "<<y-yns<<eol;
      y=x*Ard;
      out<<tostring(n)+" thread(s)  : x*Ard-yex = "<<y-zns<<eol;
    }

//!  col_dense
  thePrintStream<<"==== col dense product ===="<<eol;
  thePrintStream<<"A="<<Acd<<eol;
  Environment::parallel(false);
  y=Acd*x;
  thePrintStream<<"   no omp    : Acd*x-yex = "<<y-yns<<eol;
  y=x*Acd;
  thePrintStream<<"   no omp    : x*Acd-yex = "<<y-zns<<eol;
  Environment::parallel(true);
  for(Number n=1; n<=nth; n++)
    {
      numberOfThreads(n);
      y=Acd*x;
      out<<tostring(n)+" thread(s)  : Acd*x-yex = "<<y-yns<<eol;
      y=x*Acd;
      out<<tostring(n)+" thread(s)  : x*Acd-yex = "<<y-zns<<eol;
    }

//!  sym dense storage
  thePrintStream<<"==== sym dense product ===="<<eol;
  thePrintStream<<"A="<<Asd<<eol;
  Environment::parallel(false);
  y=Asd*x;
  thePrintStream<<"   no omp    : Asd*x-yex = "<<y-ys<<eol;
  y=x*Asd;
  thePrintStream<<"   no omp    : x*Asd-yex = "<<y-zs<<eol;
  Environment::parallel(true);
  for(Number n=1; n<=nth; n++)
    {
      numberOfThreads(n);
      y=Asd*x;
      thePrintStream<<tostring(n)+" thread(s)  : Asd*x-yex = "<<y-ys<<eol;
      y=x*Asd;
      thePrintStream<<tostring(n)+" thread(s)  : x*Asd-yex = "<<y-zs<<eol;
    }

//!  dual dense storage
  thePrintStream<<"==== dual dense product ===="<<eol;
  thePrintStream<<"A="<<Add<<eol;
  Environment::parallel(false);
  y=Add*x;
  thePrintStream<<"   no omp    : Add*x-yex = "<<y-yns<<eol;
  y=x*Add;
  thePrintStream<<"   no omp    : x*Add-yex = "<<y-zns<<eol;
  Environment::parallel(true);
  for(Number n=1; n<=nth; n++)
    {
      numberOfThreads(n);
      y=Add*x;
      thePrintStream<<tostring(n)+" thread(s)  : Add*x-yex = "<<y-yns<<eol;
      y=x*Add;
      thePrintStream<<tostring(n)+" thread(s)  : x*Add-yex = "<<y-zns<<eol;
    }

  //test product for very large matrix (omp)
//  n=10000;
//  x.resize(n,1.); y.resize(n);
//  Number nloop=40;
//  thePrintStream<<eol<<"==== test for large matrices ("<<n<<"x"<<n<<"), cpu time for 1 to "<<nth<<" threads ===="<<eol;
//  //row_dense
//  std::ofstream fout("rowTime.txt");
//
//  for(Number p=50; p<5000; p+=50)
//    {
//      nloop=100000/p;
//      std::vector<Real> ts,rs;
//      LargeMatrix<Real> Brd(p, n, _dense, _row, 1.);
//      thePrintStream<<"==== row dense product ===="<<eol;
//      elapsedTime();
//      Environment::parallel(false);
//      for(Number k=0; k<nloop; k++)  y=Brd*x;
//      //elapsedTime("   no omp  ",thePrintStream);
//      ts.push_back(elapsedTime());
//      rs.push_back(1.);
//      Environment::parallel(true);
//      for(Number n=1; n<=nth; n++)
//        {
//          numberOfThreads(n);
//          for(Number k=0; k<nloop; k++)  y=Brd*x;
//          //elapsedTime(tostring(n)+" thread(s)",thePrintStream);
//          Real t=elapsedTime();
//          ts.push_back(t);
//          rs.push_back(ts[0]/t);
//        }
//      Brd.clearall();
//      fout<<p;
//      for(Number p=0; p<ts.size(); p++) fout<<" "<<ts[p];
//      for(Number p=0; p<ts.size(); p++) fout<<" "<<rs[p];
//      fout<<eol;
//    }
//  fout.close();
//
//  //col_dense
//  LargeMatrix<Real> Bcd(n, n, _dense, _col, 1.);
//  thePrintStream<<"==== col dense product ===="<<eol;
//  elapsedTime();
//  Environment::parallel(false);
//  for(Number k=0; k<nloop; k++)  y=Bcd*x;
//  elapsedTime("   no omp  ",thePrintStream);
//  Environment::parallel(true);
//  elapsedTime();
//  for(Number n=1; n<=nth; n++)
//    {
//      numberOfThreads(n);
//      for(Number k=0; k<nloop; k++)  y=Bcd*x;
//      elapsedTime(tostring(n)+" thread(s)",thePrintStream);
//    }
//  Bcd.clearall();
//
//  //sym dense storage
//  LargeMatrix<Real> Bsd(n, n, _dense, _symmetric, 1.);
//  thePrintStream<<"==== sym dense product ===="<<eol;
//  elapsedTime();
//  Environment::parallel(false);
//  for(Number k=0; k<nloop; k++)  y=Bsd*x;
//  elapsedTime("   no omp  ",thePrintStream);
//  Environment::parallel(true);
//  elapsedTime();
//  for(Number n=1; n<=nth; n++)
//    {
//      numberOfThreads(n);
//      for(Number k=0; k<nloop; k++)  y=Bsd*x;
//      elapsedTime(tostring(n)+" thread(s)",thePrintStream);
//    }
//  Bsd.clearall();
//
//  //dual_dense
//  LargeMatrix<Real> Bdd(n, n, _dense, _dual, 1.);
//  thePrintStream<<"==== dual dense product ===="<<eol;
//  elapsedTime();
//  Environment::parallel(false);
//  for(Number k=0; k<nloop; k++)  y=Bdd*x;
//  elapsedTime("   no omp  ",thePrintStream);
//  Environment::parallel(true);
//  elapsedTime();
//  for(Number n=1; n<=nth; n++)
//    {
//      numberOfThreads(n);
//      for(Number k=0; k<nloop; k++)  y=Bdd*x;
//      elapsedTime(tostring(n)+" thread(s)",thePrintStream);
//    }
//  Bdd.clearall();
#endif

//!---------------------------------------------------------------------------
//! Test product by a dense row matrix or a dense col matrix
//!---------------------------------------------------------------------------
 Number p=3, q=2;
 thePrintStream <<"\n----------------------------------- test multMatrixRow -----------------------------------\n";
 LargeMatrix<Real> Arow(p, p, _dense, _row, 0.);
 LargeMatrix<Real> Brow(p, q, _dense, _row, 0.);
 LargeMatrix<Real> Crow(p, q, _dense, _row, 0.);
 LargeMatrix<Real> Acol(p, p, _dense, _col, 0.);
 LargeMatrix<Real> Adual(p, p, _dense, _dual, 0.);
 LargeMatrix<Real> Asym(p, p, _dense, _sym, 0.);
 for(Number i=0;i<p;i++)
    for(Number j=0;j<p;j++) Arow(i+1,j+1)= p*i+j+1;
 thePrintStream<<"Arow = "<< eol; 
 nbErrors+=checkValues(Arow ,rootname+"/Arow.in", tol, errors, "Arow",check);
 for(Number i=0;i<p;i++)
    for(Number j=0;j<q;j++) Brow(i+1,j+1)= q*i+j+1;
 thePrintStream<<"Brow = "<< eol; 
 nbErrors+=checkValues(Brow ,rootname+"/Brow.in", tol, errors, "Brow",check);
 Real *pB=&Brow.values()[1];
 Real *pC=&Crow.values()[1];
 Arow.multMatrixRow(pB, pC, q);
 thePrintStream<<"Crow = "<< eol; 
  nbErrors+=checkValues(Crow ,rootname+"/Crow_1.in", tol, errors, "Crow_1",check);
 thePrintStream<<" norm2(Arow*Brow-Arow*pBrow)="<<eol;  
 nbErrors+=checkValue(norm2(Arow*Brow-Crow) , 0., tol, errors, "norm2(Acol*Brow-Crow)");
 for(Number i=0;i<p;i++)
    for(Number j=0;j<p;j++) Acol(i+1,j+1)= p*i+j+1;
 thePrintStream<<"Acol = "<< eol; 
 nbErrors+=checkValues(Acol ,rootname+"/Acol.in", tol, errors, "Acol",check);
 Acol.multMatrixRow(pB, pC, q);
 thePrintStream<<"Crow = "<< eol; 
 nbErrors+=checkValues(Crow ,rootname+"/Crow_2.in", tol, errors, "Crow_2",check);
 thePrintStream<<" norm2(Acol*Brow-Acol*pBrow)="<< eol;   
 nbErrors+=checkValue(norm2(Acol*Brow-Crow) , 0.  , tol, errors, "norm2(Acol*Brow-Crow)");
 for(Number i=0;i<p;i++)
    for(Number j=0;j<p;j++) Adual(i+1,j+1)= p*i+j+1;
 thePrintStream<<"Adual = "<< eol; 
 nbErrors+=checkValues(Adual ,rootname+"/Adual.in", tol, errors, "Adual",check);
 Adual.multMatrixRow(pB, pC, q);
 thePrintStream<<"Crow = "<< eol; 
 nbErrors+=checkValues(Crow ,rootname+"/Crow_3.in", tol, errors, "Crow_3",check);
 thePrintStream<<" norm2(Adual*Brow-Adual*pBrow)="<<eol;  
 nbErrors+=checkValue(norm2(Adual*Brow-Crow) , 0.  , tol, errors, "norm2(Adual*Brow-Crow)");
 for(Number i=0;i<p;i++)
    for(Number j=0;j<p;j++) Asym(i+1,j+1)= p*i+j+1;
 thePrintStream<<"Asym = "<<Asym;
 Asym.multMatrixRow(pB, pC, q);
 thePrintStream<<"Crow = "<< eol;
 nbErrors+=checkValues(Crow ,rootname+"/Crow_4.in", tol, errors, "Crow_4",check);
 thePrintStream<<" norm2(Asym*Brow-Asym*pBrow)="<<eol;  
 nbErrors+=checkValue(norm2(Asym*Brow-Crow) , 0.  , tol, errors, "norm2(Asym*Brow-Crow)");

 thePrintStream <<"\n----------------------------------- test multMatrixCol -----------------------------------\n";
 LargeMatrix<Real> Bcol(p, q, _dense, _col, 0.);
 LargeMatrix<Real> Ccol(p, q, _dense, _col, 0.);
 for(Number i=0;i<p;i++)
    for(Number j=0;j<q;j++) Bcol(i+1,j+1)= q*i+j+1;
 thePrintStream<<"Bcol = " << eol; 
 nbErrors+=checkValues(Bcol ,rootname+"/Bcol.in", tol, errors, "Bcol",check);
 pB=&Bcol.values()[1];
 pC=&Ccol.values()[1];
 Arow.multMatrixCol(pB, pC, q);
 thePrintStream<<"Arow*pBcol = " << eol; 
 nbErrors+=checkValues(Ccol ,rootname+"/Ccol_1.in", tol, errors, "Ccol_1",check);
 Acol.multMatrixCol(pB, pC, q);
 thePrintStream<<"Acol*pBcol = " << eol; 
 nbErrors+=checkValues(Ccol ,rootname+"/Ccol_2.in", tol, errors, "Ccol_2",check);
 Adual.multMatrixCol(pB, pC, q);
 thePrintStream<<"Adual*pBcol = "<< eol; 
 nbErrors+=checkValues(Ccol ,rootname+"/Ccol_3.in", tol, errors, "Ccol_3",check);
 Asym.multMatrixCol(pB, pC, q);
 thePrintStream<<"Asym*pBcol = "<< eol; 
 nbErrors+=checkValues(Ccol ,rootname+"/Ccol_4.in", tol, errors, "Ccol_4",check);

 thePrintStream <<"\n----------------------------------- test multLeftMatrixRow -----------------------------------\n";
 LargeMatrix<Real> Crowt(q, p, _dense, _row, 0.);
 LargeMatrix<Real> Browt(q, p, _dense, _row, 0.);
 for(Number i=0;i<q;i++)
    for(Number j=0;j<p;j++) Browt(i+1,j+1)= p*i+j+1;
 thePrintStream<<"Browt = "<<Browt;
 pB=&Browt.values()[1];
 pC=&Crowt.values()[1];
 Arow.multLeftMatrixRow(pB, pC, q);
 thePrintStream<<"Crowt = "; 
 nbErrors+=checkValues(Crowt ,rootname+"/Crowt_1.in", tol, errors, "Crowt_1",check);
 thePrintStream<<" norm2(Browt*Arow-pBrowt*Arow)="<<eol; 
 nbErrors+=checkValue(norm2(Browt*Arow-Crowt) , 0.  , tol, errors, "norm2(Browt*Arow-Crowt)");
 Acol.multLeftMatrixRow(pB, pC, q);
 thePrintStream<<"Crowt = "<<eol; 
 nbErrors+=checkValues(Crowt ,rootname+"/Crowt_2.in", tol, errors, "Crowt_2",check);
 thePrintStream<<" norm2(Browt*Acol-pBrowt*Acol)="<<eol;
 nbErrors+=checkValue(norm2(Browt*Acol-Crowt) , 0.  , tol, errors, "norm2(Browt*Acol-Crowt)");
 Adual.multLeftMatrixRow(pB, pC, q);
 thePrintStream<<"Crowt = "<<eol; 
 nbErrors+=checkValues(Crowt ,rootname+"/Crowt_3.in", tol, errors, "Crowt_3",check);
 thePrintStream<<" norm2(Browt*Adual-pBrowt*Adual)="<< eol; 
 nbErrors+=checkValue(norm2(Browt*Adual-Crowt) , 0.  , tol, errors, "norm2(Browt*Adual-Crowt)");
 Asym.multLeftMatrixRow(pB, pC, q);
 thePrintStream<<"Crowt = "<<eol; //<<Crowt;
 nbErrors+=checkValues(Crowt ,rootname+"/Crowt_4.in", tol, errors, "Crowt_4",check);
 thePrintStream<<" norm2(Browt*Adual-pBrowt*Asym)="<<eol;   
 nbErrors+=checkValue(norm2(Browt*Asym-Crowt) , 0.  , tol, errors, "norm2(Browt*Asym-Crowt)");

 thePrintStream <<"\n----------------------------------- test multLeftMatrixCol -----------------------------------\n";
 LargeMatrix<Real> Ccolt(q, p, _dense, _col, 0.);
 LargeMatrix<Real> Bcolt(q, p, _dense, _col, 0.);
 for(Number i=0;i<q;i++)
    for(Number j=0;j<p;j++) Bcolt(i+1,j+1)= p*i+j+1;
 thePrintStream<<"Bcolt = "<<Bcolt;
 pB=&Bcolt.values()[1];
 pC=&Ccolt.values()[1];
 Arow.multLeftMatrixCol(pB, pC, q);
 thePrintStream<<"pBcolt*Arow = "<<eol; 
 nbErrors+=checkValues(Ccolt ,rootname+"/Ccolt_1.in", tol, errors, "Ccolt_1",check);
 Acol.multLeftMatrixCol(pB, pC, q);
 thePrintStream<<"pBcolt*Acol = "<<eol;   
 nbErrors+=checkValues(Ccolt ,rootname+"/Ccolt_2.in", tol, errors, "Ccolt_2",check);
 Adual.multLeftMatrixCol(pB, pC, q);
 thePrintStream<<"pBcolt*Adual = "<< eol; 
 nbErrors+=checkValues(Ccolt ,rootname+"/Ccolt_3.in", tol, errors, "Ccolt_3",check);
 Asym.multLeftMatrixCol(pB, pC, q);
 thePrintStream<<"pBcolt*Asym = "<<eol;   
 nbErrors+=checkValues(Ccolt ,rootname+"/Ccolt_4.in", tol, errors, "Ccolt_4",check);

//!---------------------------------------------------------------------------
//! Test factorization of row Dense Storage
//!---------------------------------------------------------------------------
  thePrintStream <<"\n-------------------------------- test Gauss solver -----------------------------------\n";
  n=5;
  verboseLevel(10);
  LargeMatrix<Real> Hn(n, n, _dense, _row, 0.);
  std::vector<Real> rhs(n); x.resize(n);
  for(Number i=1;i<=n;++i)
  {
      x[i-1]=i;
      Hn(i,i)= 3.;
      if(i<n) Hn(i,i+1)= -1;
      if(i>1) Hn(i,i-1)= -1;
  }
  rhs=Hn*x;
  thePrintStream<<"Hn="<<Hn<<eol<<"rhs="<<rhs<<eol;
  std::vector<Real> xg=rhs;
  LargeMatrix<Real> Hnc=Hn;
  static_cast<DenseStorage*>(Hnc.storagep())->gaussSolverG(Hnc.values(),xg);
  thePrintStream<<"row dense : gaussSolveG(Hn,rhs) x="<<xg<<eol;
  Hnc = Hn; xg=rhs;
  gaussSolve(Hnc,xg);
  thePrintStream<<"row dense : gaussSolve(Hn,rhs) x=" << eol;  
  nbErrors+=checkValues(xg ,rootname+"/xg_rowGS.in", tol, errors, "xg_rowGS",check);
  Hnc = Hn; xg=rhs;
  Hnc.luFactorize();
  Hnc.luSolve(rhs,xg);
  thePrintStream << "LU factorization with row permutation of Hn (row dense) " <<eol;   
  thePrintStream << "row dense : LU factorization with row permutation of Hn, solve Hn*x = rhs, x= "  << xg << eol;
  thePrintStream<<"row dense : product of factorized matrix with permutation and vector, H*x-Hf*x = "<<Reals(Hn*x)-Reals(Hnc*x)<<eol;
  nbErrors+=checkValues(Hnc ,rootname+"/Hnc_rowperm.in", tol, errors, "Hnc_rowperm",check);
  nbErrors+=checkValues(xg ,rootname+"/xg_rowperm.in", tol, errors, "xg_rowperm",check);
  nbErrors+=checkValues(Reals(Hn*x)-Reals(Hnc*x),rootname+"/HnxmHncx_rowperm.in", tol, errors, "Row Perm. Reals(Hn*x)-Reals(Hnc*x)",check);
  Hnc = Hn; xg=rhs;
  Hnc.luFactorize(false);  //no permutation
  Hnc.luSolve(rhs,xg);
  thePrintStream << "row dense : LU factorization with no permutation of Hn, solve Hn*x = rhs, x= "  << xg << eol;
  thePrintStream<<"row dense : product of factorized matrix and vector, H*x-Hf*x = "<<Reals(Hn*x)-Reals(Hnc*x)<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_rownoperm.in", tol, errors, "xg_rownoperm",check);
  nbErrors+=checkValues(Reals(Hn*x)-Reals(Hnc*x),rootname+"/HnxmHncx_rownoperm.in", tol, errors, "Row No Perm. Reals(Hn*x)-Reals(Hnc*x)",check);

//  //test generic function of DenseStorage class
//  Hnc = Hn; xg=rhs;
//  DenseStorage* dsto =static_cast<DenseStorage*>(Hnc.storagep());
//  dsto->luG(Hnc.values(),Hnc.values());
//  dsto->lowerD1SolverG(Hnc.values(),rhs, xg);
//  dsto->upperSolverG(Hnc.values(),xg, xg);
//  //out << "generic LU factorization with no permutation of Hn (row dense) " << Hnc<<eol;
//  out << "row dense : generic LU factorization with no permutation of Hn, solve Hn*x = rhs, x= "  << xg << eol;
//  Hnc = Hn; xg=rhs;
//  std::vector<Number> P;
//  dsto=static_cast<DenseStorage*>(Hnc.storagep());
//  dsto->luG(Hnc.values(),Hnc.values(),P);
//  dsto->lowerD1SolverG(Hnc.values(),permute(rhs,rhs,P), xg);
//  dsto->upperSolverG(Hnc.values(),xg, xg);
//  //out << "generic LU factorization with row permuation of Hn (row dense) " << Hnc<<eol;
//  //out << "row permutation vector :  " << P<<eol;
//  out << "row dense : generic LU factorization with row permutation of Hn, solve Hn*x = rhs, x= "  << xg << eol;

  //!---------------------------------------------------------------------------
  //! Test factorization of col Dense Storage
  //!---------------------------------------------------------------------------
  LargeMatrix<Real> Hn2(n, n, _dense, _col, 0.);
  for(Number i=1;i<=n;++i)
  {
      x[i-1]=i;
      Hn2(i,i)= 3.;
      if(i<n) Hn2(i,i+1)= -1;
      if(i>1) Hn2(i,i-1)= -1;
  }
  rhs=Hn2*x;
  xg=rhs;
  LargeMatrix<Real> Hn2c=Hn2;
  gaussSolve(Hn2c,xg);
  thePrintStream<<"col dense : gaussSolve(Hn2,rhs) x="<<xg<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_colGS.in", tol, errors, "xg_colGS",check);
  Hn2c = Hn2; xg=rhs;
  Hn2c.luFactorize();
  Hn2c.luSolve(rhs,xg);
  thePrintStream << "col dense : LU factorization with row permutation of Hn2 (col dense), solve Hn2*x = rhs, x= "  << xg << eol;
  thePrintStream << "col dense : product of factorized matrix with permutation and vector, H*x-Hf*x = "<<Reals(Hn2*x)-Reals(Hn2c*x)<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_colperm.in", tol, errors, "xg_colperm",check);
  nbErrors+=checkValues(Reals(Hn2*x)-Reals(Hn2c*x),rootname+"/H2nxmHn2cx_colperm.in", tol, errors, "Col Perm. Reals(Hn2*x)-Reals(Hn2c*x)",check);
  Hn2c = Hn2; xg=rhs;
  Hn2c.luFactorize(false);
  Hn2c.luSolve(rhs,xg);
  thePrintStream << "col dense : LU factorization with no permutation of Hn2 (col dense), solve Hn2*x = rhs, x= "  << xg << eol;
  thePrintStream << "col dense : product of factorized matrix and vector, H*x-Hf*x = "<<Reals(Hn2*x)-Reals(Hn2c*x)<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_colnoperm.in", tol, errors, "xg_colnoperm",check);
  nbErrors+=checkValues(Reals(Hn2*x)-Reals(Hn2c*x),rootname+"/Hn2xmHn2cx_colnoperm.in", tol, errors, "Col No Perm. Reals(Hn2*x)-Reals(Hn2c*x)",check);

  //!---------------------------------------------------------------------------
  //! Test factorization of dual dense Storage
  //!---------------------------------------------------------------------------
  LargeMatrix<Real> Hn3(n, n, _dense, _dual, 0.);
  for(Number i=1;i<=n;++i)
  {
      x[i-1]=i;
      Hn3(i,i)= 3.;
      if(i<n) Hn3(i,i+1)= -1;
      if(i>1) Hn3(i,i-1)= -1;
  }
  rhs=Hn3*x;
  xg=rhs;
  LargeMatrix<Real> Hn3c=Hn3;
  gaussSolve(Hn3c,xg);
  thePrintStream<<"dual dense : gaussSolve(Hn3,rhs) x="<<xg<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_dualGS.in", tol, errors, "xg_dualGS",check);
  Hn3c = Hn3; xg=rhs;
  Hn3c.luFactorize();
  Hn3c.luSolve(rhs,xg);
  thePrintStream << "dual dense : LU factorization with row permutation of Hn3 (dual dense), solve Hn3*x = rhs, x= "  << xg << eol;
  thePrintStream << "dual dense : product of factorized matrix with permutation and vector, H*x-Hf*x = "<<Reals(Hn3*x)-Reals(Hn3c*x)<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_dualperm.in", tol, errors, "xg_dualperm",check);
  nbErrors+=checkValues(Reals(Hn3*x)-Reals(Hn3c*x),rootname+"/H3nxmHn3cx_dualperm.in", tol, errors, "Dual Perm. Reals(Hn3*x)-Reals(Hn3c*x)",check);
  Hn3c = Hn3; xg=rhs;
  Hn3c.luFactorize(false);
  Hn3c.luSolve(rhs,xg);
  thePrintStream << "dual dense : LU factorization with no permutation of Hn3 (dual dense), solve Hn3*x = rhs, x= "  << xg << eol;
  thePrintStream << "dual dense : product of factorized matrix and vector, H*x-Hf*x = "<<Reals(Hn3*x)-Reals(Hn3c*x)<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_dualnoperm.in", tol, errors, "xg_dualnoperm",check);
  nbErrors+=checkValues(Reals(Hn3*x)-Reals(Hn3c*x),rootname+"/Hn3xmHn3cx_dualnoperm.in", tol, errors, "Dual No Perm. Reals(Hn3*x)-Reals(Hn3c*x)",check);


  //!-----------------------------x*-----------x*-------------------------------
  //! Test factorization of sym dense Storage
  //!---------------------------------------------------------------------------
  LargeMatrix<Real> Hn4(n, n, _dense, _symmetric, 0.);
  for(Number i=1;i<=n;++i)
  {
      x[i-1]=i;
      Hn4(i,i)= 3.;
      if(i>1) Hn4(i,i-1)= -1;
  }
  rhs=Hn4*x;
  xg=rhs;
  LargeMatrix<Real> Hn4c=Hn4;
  Hn4c.ldltFactorize();
  Hn4c.ldltSolve(rhs,xg);
  thePrintStream << "sym dense : LDLt factorization with no permutation of Hn4, solve Hn4*x = rhs, x= "  << xg << eol;
  thePrintStream << "sym dense : product of factorized matrix and vector, H*x-Hf*x = "<<Reals(Hn4*x)-Reals(Hn4c*x)<<eol;
  nbErrors+=checkValues(xg ,rootname+"/xg_symperm.in", tol, errors, "xg_symperm",check);
  nbErrors+=checkValues(Reals(Hn4*x)-Reals(Hn4c*x),rootname+"/H4nxmHn4cx_symperm.in", tol, errors, "Sym Perm. Reals(Hn4*x)-Reals(Hn4c*x)",check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
