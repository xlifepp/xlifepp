/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_ClusterNode.cpp
	\author E. Lunéville
	\since 17 may 2016
	\date 17 may 2016

	Low level tests of ClusterNode class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "hierarchicalMatrix.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_ClusterNode {

void unit_ClusterNode(int argc, char* argv[], bool check)
{
  String rootname = "unit_ClusterNode";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec); 
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  Number nbox=10;
   //!test on a disk
   Mesh meshd(inputsPathTo(rootname+"/meshDisk.msh"), _name="P1 mesh of a disk");

   ClusterTree<Point> dn_regular(meshd.nodes,_regularBisection,nbox);
   dn_regular.saveToFile("cluster_disk_regular.dat");
   ClusterTree<Point> dn_bounding(meshd.nodes,_boundingBoxBisection,nbox);
   dn_bounding.saveToFile("cluster_disk_bounding.dat");
   ClusterTree<Point> dn_cardinal(meshd.nodes,_cardinalityBisection,nbox);
   dn_cardinal.saveToFile("cluster_disk_cardinal.dat");
   ClusterTree<Point> dn_kdtree(meshd.nodes,_uniformKdtree,10, 0, true, false,_fmmCloseNodeRule,0.01);
   dn_kdtree.saveToFile("cluster_disk_kdtree.dat");
   ClusterTree<Point> dn_nukdtree(meshd.nodes,_nonuniformKdtree,10, 0,true,false);
   dn_nukdtree.saveToFile("cluster_disk_nukdtree.dat");
   nbErrors+= checkValues("cluster_disk_regular.dat", rootname+"/cluster_disk_regular.in", tol, errors, "cluster_disk_regular.in", check);
   nbErrors+= checkValues("cluster_disk_bounding.dat", (rootname+"/cluster_disk_bounding.in"), tol, errors, "cluster_disk_bounding.in", check);
   nbErrors+= checkValues("cluster_disk_cardinal.dat", rootname+"/cluster_disk_cardinal.in", tol, errors, "cluster_disk_cardinal.in", check);
   nbErrors+= checkValues("cluster_disk_kdtree.dat", (rootname+"/cluster_disk_kdtree.in"), tol, errors, "cluster_disk_kdtree.in", check);
   nbErrors+= checkValues("cluster_disk_nukdtree.dat", (rootname+"/cluster_disk_nukdtree.in"), tol, errors, "cluster_disk_nukdtree.in", check);
 

   //!test on a circle
   Mesh meshc(inputsPathTo(rootname+"/meshCircle.msh"), _name="P1 mesh of a circle");
   nbox=10;
   ClusterTree<Point> cn_regular(meshc.nodes,_regularBisection,nbox);
   cn_regular.saveToFile("cluster_circle_regular.dat");
   ClusterTree<Point> cn_bounding(meshc.nodes,_boundingBoxBisection,nbox);
   cn_bounding.saveToFile("cluster_circle_bounding.dat");
   ClusterTree<Point> cn_cardinal(meshc.nodes,_cardinalityBisection,nbox);
   cn_cardinal.saveToFile("cluster_circle_cardinal.dat");
   ClusterTree<Point> cn_kdtree(meshc.nodes,_uniformKdtree,5);
   cn_kdtree.saveToFile("cluster_circle_kdtree.dat");
   ClusterTree<Point> cn_nukdtree(meshc.nodes,_nonuniformKdtree,5);
   cn_nukdtree.saveToFile("cluster_circle_nukdtree.dat");
   nbErrors+= checkValues("cluster_circle_regular.dat", (rootname+"/cluster_circle_regular.in"), tol, errors, "cluster_circle_regular.in", check);
   nbErrors+= checkValues("cluster_circle_bounding.dat", (rootname+"/cluster_circle_bounding.in"), tol, errors, "cluster_circle_bounding.in", check);
   nbErrors+= checkValues("cluster_circle_cardinal.dat", (rootname+"/cluster_circle_cardinal.in"), tol, errors, "cluster_circle_cardinal.in", check);
   nbErrors+= checkValues("cluster_circle_kdtree.dat", (rootname+"/cluster_circle_kdtree.in"), tol, errors, "cluster_circle_kdtree.in", check);
   nbErrors+= checkValues("cluster_circle_nukdtree.dat", (rootname+"/cluster_circle_nukdtree.in"), tol, errors, "cluster_circle_nukdtree.in", check);
 

   //!test on a half sphere
   Mesh meshs(inputsPathTo(rootname+"/meshSphere.msh"), _name="P1 mesh of a sphere");
   nbox=3;
   ClusterTree<Point> sn_regular(meshs.nodes,_regularBisection,nbox);
   sn_regular.saveToFile("cluster_sphere_regular.dat");
   ClusterTree<Point> sn_bounding(meshs.nodes,_boundingBoxBisection,nbox);
   sn_bounding.saveToFile("cluster_sphere_bounding.dat");
   ClusterTree<Point> sn_cardinal(meshs.nodes,_cardinalityBisection,nbox);
   sn_cardinal.saveToFile("cluster_sphere_cardinal.dat");
   ClusterTree<Point> sn_kdtree(meshs.nodes,_uniformKdtree,5);
   sn_kdtree.saveToFile("cluster_sphere_kdtree.dat");
   ClusterTree<Point> sn_nukdtree(meshs.nodes,_nonuniformKdtree,5);
   sn_nukdtree.saveToFile("cluster_sphere_nukdtree.dat");
   nbErrors+= checkValues("cluster_sphere_regular.dat", (rootname+"/cluster_sphere_regular.in"), tol, errors, "cluster_sphere_regular.in", check);
   nbErrors+= checkValues("cluster_sphere_bounding.dat", (rootname+"/cluster_sphere_bounding.in"), tol, errors, "cluster_sphere_bounding.in", check);
   nbErrors+= checkValues("cluster_sphere_cardinal.dat", (rootname+"/cluster_sphere_cardinal.in"), tol, errors, "cluster_sphere_cardinal.in", check);
   nbErrors+= checkValues("cluster_sphere_kdtree.dat", (rootname+"/cluster_sphere_kdtree.in"), tol, errors, "cluster_sphere_kdtree.in", check);
   nbErrors+= checkValues("cluster_sphere_nukdtree.dat", (rootname+"/cluster_sphere_nukdtree.in"), tol, errors, "cluster_sphere_nukdtree.in", check);
 

   //!test on a half ball
   Mesh meshb(inputsPathTo(rootname+"/meshBall.msh"), _name="P1 mesh of a ball");
   ClusterTree<Point> bn_regular(meshb.nodes,_regularBisection,nbox);
   bn_regular.saveToFile("cluster_ball_regular.dat");
   ClusterTree<Point> bn_bounding(meshb.nodes,_boundingBoxBisection,nbox);
   bn_bounding.saveToFile("cluster_ball_bounding.dat");
   ClusterTree<Point> bn_cardinal(meshb.nodes,_cardinalityBisection,nbox);
   bn_cardinal.saveToFile("cluster_ball_cardinal.dat");
   ClusterTree<Point> bn_kdtree(meshb.nodes,_uniformKdtree,5);
   bn_kdtree.saveToFile("cluster_ball_kdtree.dat");
   ClusterTree<Point> bn_nukdtree(meshb.nodes,_nonuniformKdtree,5);
   bn_nukdtree.saveToFile("cluster_ball_nukdtree.dat");
   nbErrors+= checkValues("cluster_ball_regular.dat", (rootname+"/cluster_ball_regular.in"), tol, errors, "cluster_ball_regular.in", check);
   nbErrors+= checkValues("cluster_ball_bounding.dat", (rootname+"/cluster_ball_bounding.in"), tol, errors, "cluster_ball_bounding.in", check);
   nbErrors+= checkValues("cluster_ball_cardinal.dat", (rootname+"/cluster_ball_cardinal.in"), tol, errors, "cluster_ball_cardinal.in", check);
   nbErrors+= checkValues("cluster_ball_kdtree.dat", (rootname+"/cluster_ball_kdtree.in"), tol, errors, "cluster_ball_kdtree.in", check);
   nbErrors+= checkValues("cluster_ball_nukdtree.dat", (rootname+"/cluster_ball_nukdtree.in"), tol, errors, "cluster_ball_nukdtree.in", check);
 

   //!test update of real bounding boxes
   dn_regular.updateRealBoundingBoxes();
   dn_regular.saveToFile("cluster_disk_regular_realbox.dat",true);
   nbErrors+= checkValues("cluster_disk_regular_realbox.dat", (rootname+"/cluster_disk_regular_realbox.in"), tol, errors, "cluster_disk_regular_realbox.in", check);

   //!test update of close nodes vectors
   dn_kdtree.updateCloseNodes();
   verboseLevel(10);
   dn_kdtree.print(thePrintStream);

   //!test from FeDof vector
   Domain omega = meshd.domain("Omega");
   Space V(_domain=omega, _interpolation=_P1, _name="V");
   ClusterTree<FeDof> fn_regular(V.feSpace()->dofs,_regularBisection,nbox);
   fn_regular.updateRealBoundingBoxes();
   fn_regular.updateDofNumbers();
   fn_regular.updateElements();
   fn_regular.saveToFile("cluster_fedof_regular.dat");
   nbErrors+= checkValues("cluster_fedof_regular.dat", (rootname+"/cluster_fedof_regular.in"), tol, errors, "cluster_fedof_regular.in", check);
   verboseLevel(10);
   fn_regular.print(thePrintStream);

  //! test from Element vector
   ClusterTree<Element> en_regular(V.feSpace()->elements,_regularBisection,nbox);
   en_regular.saveToFile("cluster_element_regular.dat");
   nbErrors+= checkValues("cluster_element_regular.in", (rootname+"/cluster_element_regular.in"), tol, errors, "cluster_element_regular.in", check);
   en_regular.updateRealBoundingBoxes();
   en_regular.saveToFile("cluster_element_regular_realbox.dat",true);
   nbErrors+= checkValues("cluster_element_regular_realbox.dat", (rootname+"/cluster_element_regular_realbox.in"), tol, errors, "cluster_element_regular_realbox.in", check);
   en_regular.updateDofNumbers();
   en_regular.updateElements();
   verboseLevel(10);
   en_regular.print(thePrintStream);

    //performance test
//  for(Number p=101;p<=1001;p+=100)
//  {
//      Mesh mesh2d(Rectangle(_xmin=0.,_xmax=1.,_ymin=0.,_ymax=1.,_nnodes=Numbers(p,p),
//                        _domain_name="Omega",_side_names="Gamma"), _shape=_triangle, _order=1, _generator=_structured);
//      elapsedTime();
//      ClusterTree cn(&meshd.nodes,_cardinalBisection, 20, true);
//      elapsedTime("cardinal box  cluster of 20 for a "+tostring(mesh2d.nbOfNodes())+" nodes mesh",thePrintStream);
//  }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  

}

}
