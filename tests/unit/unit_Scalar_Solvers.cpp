/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Scalar_Solvers.cpp
  \author E. Lunéville
	\since 25 mar 2016
	\date  25 mar 2016

	Test all solvers on a scalar problem (Neumann 2D)
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Scalar_Solvers
{

//data on sigma-

Real k=1.;

Real uex(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  return sin(k*pi_*x) * sin(k*pi_*y);
}

Real f(const Point& P, Parameters& pa = defaultParameters)
{
  return (2*k*k*pi_*pi_+1)*uex(P);
}

class myVector: public std::vector<Real>
{
  public:
    myVector(Number n=0) {if(n>0) resize(n);}
    ValueType valueType() const {return _real;} //MANDATORY
    myVector& operator +=(const myVector& V) //MANDATORY
    {for(Number i=0;i<size();i++) at(i)+=V.at(i);
    return *this;}
    myVector& operator -=(const myVector& V) //MANDATORY
    {for(Number i=0;i<size();i++) at(i)-=V.at(i);
    return *this;}
    myVector& operator *=(Real a) //MANDATORY
    {for(Number i=0;i<size();i++) at(i)*=a;
    return *this;}
      myVector& operator /=(Real a) //MANDATORY
    {for(Number i=0;i<size();i++) at(i)/=a;
    return *this;}
    Real norm2() const  //MANDATORY
    {
        Real n=0.;
        for(Number i=0;i<size();i++) n+=at(i)*at(i);
        return std::sqrt(n);
    }
    myVector& operator *=(Complex a) {return *this;} // MANDATORY fake complex operation
    myVector& operator /=(Complex a) {return *this;} // MANDATORY fake complex operation
};

Complex dotRC(const myVector& X,const myVector& Y)  //MANDATORY
{
  Real d=0.;
  for (Number i=0;i<X.size();i++) d+=X.at(i)*Y.at(i);
  return d;
}

Complex hermitianProduct(const myVector& X,const myVector& Y) //MANDATORY
{
  return dotRC(X,Y);
}
class myOperator
{
  public:
    Number mn;
    std::vector<Real> val;
    ValueType valueType() const {return _real;}   //MANDATORY
    myOperator(Number n) {mn=n; val.resize(n*n);}
    Real operator()(Number i, Number j) const {return val[j*mn+i];}
    Real& operator()(Number i, Number j) {return val[j*mn+i];}
};

void multMatrixVector(const myOperator& A, const myVector& X, myVector& Y)  //MANDATORY
{
    Number n=X.size();
    Y.resize(n,0.);
    for(Number i=0;i<n; ++i)
      {
          Y[i]=0;
          for(Number j=0;j<n;++j) Y[i]+=A(i,j)*X[j];
      }
}
void multVectorMatrix(const myVector& X, const myOperator& A, myVector& Y)  //MANDATORY BICG
{
    Number n=X.size();
    Y.resize(n,0.);
    for(Number i=0;i<n; ++i)
      {
          Y[i]=0;
          for(Number j=0;j<n;++j) Y[i]+=A(j,i)*X[j];
      }
}

void unit_Scalar_Solvers(int argc, char* argv[], bool check)
{
  String rootname = "unit_Scalar_Solvers";
  trace_p->push(rootname);

  verboseLevel(10);

  bool theCoutDetails = false;
  bool ok = true;
  Real eps = 0.005;  //waited error (below to)
  Real e,t;

  //!create problem
  Mesh mesh2d(Rectangle(_xmin=-0.5,_xmax=0.5,_ymin=-0.5,_ymax=0.5,_nnodes=15,_domain_name="Omega"), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Space V(_domain=omega, _interpolation=_P1, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");
  TermMatrix M(intg(omega,u*v));
  TermVector Uex(u,omega,uex);
  BilinearForm auv=intg(omega, grad(u) | grad(v)) + intg(omega, u * v);
  LinearForm fv=intg(omega,f*v);
  TermMatrix A(auv, _name="A");
  TermVector B(fv, _name="B");
//!   B=A*Uex;

  //!test solver
  TermVector U, E;
  TermMatrix ALU;
  Number nbErrors = 0;
  String errors;

#ifdef XLIFEPP_WITH_UMFPACK
  elapsedTime(); U=umfpackSolve(A,B,true); t=elapsedTime();
  nbErrors += checkValuesL2( U, Uex, M, eps, errors, "UMFPACK Solver TermVector/TermMatrix, time:"+tostring(t)+"s");
#endif // XLIFEPP_WITH_UMFPACK

  elapsedTime(); U=ldltSolve(A,B,ALU); t=elapsedTime();
  nbErrors += checkValuesL2(U, Uex, M, eps, errors, "LDLt Solver TermVector/TermMatrix, time:"+tostring(t)+"s");

  elapsedTime(); U=gaussSolve(A,B,true); t=elapsedTime();
  nbErrors += checkValuesL2(U, Uex, M, eps, errors, "Gauss Solver TermVector/TermMatrix, time:"+tostring(t)+"s");

  elapsedTime(); U=cgSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2(U, Uex, M, eps, errors, "CG Solver  TermVector/TermMatrix, time:"+tostring(t)+"s");

  elapsedTime(); U=bicgSolve(A,B); t=elapsedTime(); 
  nbErrors += checkValuesL2(U, Uex, M , eps, errors, "BICG Solver TermVector/TermMatrix, time:"+tostring(t)+"s");

  elapsedTime(); U=bicgStabSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2(U, Uex, M  , eps, errors, "BICGStab Solver  TermVector/TermMatrix, time:"+tostring(t)+"s");

  elapsedTime(); U=gmresSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2(U, Uex, M, eps, errors, "GMRes Solver TermVector/TermMatrix, time:"+tostring(t)+"s");

  elapsedTime(); U=qmrSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2(U, Uex, M, eps, errors, "QMR Solver TermVector/TermMatrix, time:"+tostring(t)+"s");

  elapsedTime(); U=cgsSolve(A,B); t=elapsedTime();
  nbErrors += checkValuesL2(U, Uex, M, eps, errors, "CGS Solver TermVector/TermMatrix, time:"+tostring(t)+"s");

  //! test with XLiFE++ matrix and vector
  Number mn=20;
  Matrix<Real> mA(mn,mn);
  Vector<Real> mX(mn), mB(mn), mZ(mn);
  for (Number i = 0; i < mn; i++)
  {
    mX[i]=i+1;
    for (Number j = 0; j < mn; j++)
      if (i==j) mA(i+1,j+1)= 2;
      else if (i==j+1 || j==i+1) mA(i+1,j+1)= -1;
  }
  multMatrixVector(mA,mX,mB);
  
  Vector<Real> mXs=CgSolver()(mA,mB,mZ);
  nbErrors += checkValuesN2(mXs, mX, eps, errors, "With Matrix/Vector: CG Solver");

  mXs=CgsSolver()(mA,mB,mZ);
  nbErrors += checkValuesN2(mXs, mX, eps, errors, "With Matrix/Vector: CGS Solver");
  mXs=BicgSolver()(mA,mB,mZ);
  nbErrors += checkValuesN2(mXs, mX, eps, errors, "With Matrix/Vector: CGS Solver");
  mXs=BicgStabSolver()(mA,mB,mZ);
  nbErrors += checkValuesN2(mXs, mX, eps, errors, "With Matrix/Vector: BiCGStab Solver");
  mXs=QmrSolver()(mA,mB,mZ);
  nbErrors += checkValuesN2(mXs, mX, eps, errors, "With Matrix/Vector: QMR Solver");
  mXs=GmresSolver()(mA,mB,mZ);
  nbErrors += checkValuesN2(mXs, mX, eps, errors, "With Matrix/Vector: GMRES Solver");

  // test with your own  matrix and vector stuff

  myOperator sA(mn);
  myVector sX(mn), sB(mn), sZ(mn);
  for (Number i = 0; i < mn; i++)
  {
    sX[i]=i+1;
    for (Number j = 0; j < mn; j++)
      if (i==j) sA(i,j)= 2; else if(i==j+1 || j==i+1) sA(i,j)= -1;
  }
  multMatrixVector(sA,sX,sB);

  myVector sXs=CgSolver()(sA,sB,sZ);
  nbErrors += checkValuesN2(sXs, sX, eps, errors, "Own Matrix/Vector: CG Solver");
  sXs=CgsSolver()(sA,sB,sZ);
  nbErrors += checkValuesN2(sXs, sX, eps, errors, "Own Matrix/Vector: CGS Solver");
  sXs=BicgSolver()(sA,sB,sZ);
  nbErrors += checkValuesN2(sXs, sX, eps, errors, "Own Matrix/Vector: BiCG Solver");
  sXs=BicgStabSolver()(sA,sB,sZ);
  nbErrors += checkValuesN2(sXs, sX, eps, errors, "Own Matrix/Vector: BiCGStab Solver");
  sXs=QmrSolver()(sA,sB,sZ);
  nbErrors += checkValuesN2(sXs, sX, eps, errors, "Own Matrix/Vector: QMR Solver");
  sXs=GmresSolver()(sA,sB,sZ);
  nbErrors += checkValuesN2(sXs, sX, eps, errors, "Own Matrix/Vector: GMRES Solver");

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
 
 trace_p->pop();
 
}

}
