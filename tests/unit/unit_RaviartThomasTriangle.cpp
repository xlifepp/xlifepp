/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_RaviartThomasTriangle.cpp
	\author E. Lunéville
	\since 31 jan 2015
	\date 31 jan 2015

	Low level tests of Raviart Thomas element on triangle
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_RaviartThomasTriangle
{

typedef GeomDomain& Domain;

Real f(const Point& P, Parameters& pa = defaultParameters)
{
   Real x=P(1), y=P(2);
   return 32*(x*(1-x)+y*(1-y));
}

Real solex(const Point& P, Parameters& pa = defaultParameters)
{
   Real x=P(1), y=P(2);
   return 16*x*(1-x)*y*(1-y);
}

const Element* elt;
Number index=1;
Vector<Real> w(const Point& P, Parameters& pa = defaultParameters)
{
  Vector<Real> r(2);
  ShapeValues shv = elt->computeShapeValues(P, false, false, 0);
  Number i=index-1;
  r[0]=shv.w[2*i];r[1]=shv.w[2*i+1];
  return r;
}

//compute dof_i(w_j) in physical space, check computeShapeValue and dof::operator()
void checkPhysicalShapeValue(const Space& V)
{
  theCout<<V<<eol;
  elt=V.element_p(Number(0));
  Function fw(w,2);
  theCout<<" -------- checkPhysicalShapeValue -------------"<<eol;
  for(Number d=1;d<=V.nbDofs();d++)
  {
    theCout<<"dof "<<d<<": ";
    for(Number j=1;j<=V.nbDofs();j++)
    {
      index=j;
      theCout<<roundToZero(V.dof(d)(fw).asReal(),1.E-10)<<" ";
     }
   theCout<<eol;
  }
}


void unit_RaviartThomasTriangle(int argc, char* argv[], bool check)
{
  String rootname = "unit_RaviartThomasTriangle";
  trace_p->push(rootname);
  verboseLevel(5);
  Point x(0.5,0.5);

  Number nbErrors = 0;
  String errors;
  Real tol=0.036;

  Mesh me(_triangle); Domain omg=me.domain("Omega");

  ShapeValues shv;
  Interpolation* interp;
  theCout<<"\n\n================  Raviart-Thomas, triangle, order 1 (RT1 base) ======================"<<eol;
  interp=findInterpolation(RaviartThomas, standard, 1, Hdiv);
  RaviartThomasStdTriangleP1 RT1b(interp);
  RT1b.print(theCout);
  shv = ShapeValues(RT1b);
  RT1b.computeShapeValues(x.begin(),shv,true);
  RT1b.printShapeValues(theCout,x.begin(),shv);
  theCout<<"\n\n================  Raviart-Thomas, triangle, ordre 1 (RT1) ======================"<<eol;
  interp=findInterpolation(RaviartThomas, standard, 1, Hdiv);
  RaviartThomasStdTrianglePk RT1(interp);
  RT1.print(theCout);
  shv = ShapeValues(RT1);
  RT1.computeShapeValues(x.begin(),shv,true);
  RT1.printShapeValues(theCout,x.begin(),shv);
  Space V1(omg,*interp,"V1",false);
  checkPhysicalShapeValue(V1);
  theCout<<"\n\n ================  Raviart-Thomas, triangle, ordre 2 (RT2) ======================"<<eol;
  interp=findInterpolation(RaviartThomas, standard, 2, Hdiv);
  RaviartThomasStdTrianglePk RT2(interp);
  RT2.print(theCout);
  shv = ShapeValues(RT2);
  RT2.computeShapeValues(x.begin(),shv,true);
  RT2.printShapeValues(theCout,x.begin(),shv);
  Space V2(omg,*interp,"V2",false);
  checkPhysicalShapeValue(V2);
  theCout<<"\n\n================  Raviart-Thomas, triangle, ordre 3 (RT3) ======================"<<eol;
  interp=findInterpolation(RaviartThomas, standard, 3, Hdiv);
  RaviartThomasStdTrianglePk RT3(interp);
  RT3.print(theCout);
  shv = ShapeValues(RT3);
  RT3.computeShapeValues(x.begin(),shv,true);
  RT3.printShapeValues(theCout,x.begin(),shv);
  Space V3(omg,*interp,"V3",false);
  checkPhysicalShapeValue(V3);
  theCout<<"\n\n================  Raviart-Thomas, triangle, ordre 4 (RT4) ======================"<<eol;
  interp=findInterpolation(RaviartThomas, standard, 4, Hdiv);
  RaviartThomasStdTrianglePk RT4(interp);
  RT4.print(theCout);
  shv = ShapeValues(RT4);
  RT4.computeShapeValues(x.begin(),shv,true);
  RT4.printShapeValues(theCout,x.begin(),shv);
  Space V4(omg,*interp,"V4",false);
  checkPhysicalShapeValue(V4);
  theCout<<eol;

  //check convergence
  Number nmax=16;
  for(Number k=1; k<=1; ++k)
  {
    std::vector<Real> hs, erL2, times;
    std::vector<Number> nbds;
    theCout<<"convergence of approximation P"<<tostring(k-1)<<"_RT"<<tostring(k)<<eol;
    for(Number n=6; n<=nmax; n+=5)
    {
      cpuTime();
      Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=n), _shape=_triangle, _order=1, _generator=_structured);
      Domain omega=mesh2d.domain("Omega");

      //!create interpolation Pk-1 and RTk
      Space H(omega,interpolation(_Lagrange,_standard,k-1,H1),"H",true);
      Space V(omega,interpolation(_RaviartThomas,_standard,k,Hdiv),"V",true);
      Unknown p(V, _name="p");  TestFunction q(p, _name="q");  //p=grad(u)
      Unknown u(H, _name="u");  TestFunction v(u, _name="v");

      TermMatrix D =(intg(omega,u*div(q)));
      TermVector un(u,omega,1.);

      //!create problem (Poisson problem)
      BilinearForm a=intg(omega,p|q)+intg(omega,u*div(q))-intg(omega,div(p)*v);
      LinearForm l=intg(omega,f*v);
      TermMatrix A(a, _name="A");
      TermVector b(l, _name="B");

      //!solve and save solution
      TermVector X=directSolve(A,b);
      saveToFile("u_Omega.vtk", X(u),_format=_vtk,_aUniqueFile);
      nbErrors += checkValues(X(u), rootname+"/Xu_"+tostring(n)+".in", tol/n, errors, "/Xu_"+tostring(n), check);

      //!compute error
      TermVector u_ex(u,omega,solex);
      TermVector u_er=u_ex-X(u);
      TermMatrix M(intg(omega,u*v), _name="M");
      Real h=1./(n-1);
      Real e=std::sqrt(real((M*u_er|u_er)));
      theCout<<"    h : "<<h<<", nb dofs : "<<X.nbDofs()<<", L2 error : "<<e<<eol;
      hs.push_back(h);
      erL2.push_back(e);
      times.push_back(cpuTime());
      nbds.push_back(X.nbDofs());
      nbErrors += checkValue(e, tol, tol, errors, "Err_"+tostring(n));
    }

    String fn=rootname+"/erreur_P"+tostring(k-1)+"_RT"+tostring(k)+".dat";
    std::ofstream erout(fn.c_str());
    erout<<hs<<eol<<erL2<<eol<<times<<eol<<nbds<<eol;
    erout.close();
  }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
