/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file unit_BicgStabSolver.cpp
  \author ManhHa NGUYEN
  \since 13 Sep 2012
  \date 27 Sep 2012

  Low level tests of BicgSolver.
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
  Test order:
  1. Real matrix and real vector
      1.1. WiththeCout a preconditioner
          a. DENSE STORAGE
          b. CS STORAGE
          c. SKYLINE STORAGE
      1.2 With a preconditioner
          a. DENSE STORAGE
          b. CS STORAGE
          c. SKYLINE STORAGE
  2. Complex matrix and complex vector
      The same as Real
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_BicgStabSolver {

void unit_BicgStabSolver(int argc, char* argv[], bool check)
{
  String rootname = "unit_BicgStabSolver";
  trace_p->push(rootname);
  verboseLevel(3);

  std::stringstream ssout;                  // string stream receiving results
  Number nbErrors = 0;
  String errors;
  ssout.precision(testPrec);

  Real eps=0.0001;
  Number maxit=100;

  const int rowNum = 3;
  const int colNum = 3;

  const std::string rMatrixDataSym(rootname+"/matrix3x3Sym.data");
  const std::string rMatrixDataSkewSym(rootname+"/matrix3x3SkewSym.data");
  const std::string rMatrixDataNoSym(rootname+"/matrix3x3NoSym.data");
  const std::string rMatrixDataSymPosDef(rootname+"/matrix3x3SymPosDef.data");

  const std::string cMatrixDataSym(rootname+"/cmatrix3x3Sym.data");
  const std::string cMatrixDataNoSym(rootname+"/cmatrix3x3NoSym.data");
  const std::string cMatrixDataSymSelfAjoint(rootname+"/cmatrix3x3SymSelfAjoint.data");
  const std::string cMatrixDataSymSkewAjoint(rootname+"/cmatrix3x3SymSkewAjoint.data");
  const std::string cMatrixDataSymPosDef(rootname+"/cmatrix3x3SymPosDef.data");

  BicgStabSolver bicgstab(eps, maxit), bicgstab2, bicgstabKV(_tolerance=eps, _maxIt=maxit);
  
  //------------ solvers ----------------
  theCout << bicgstab << eol;
  theCout << bicgstab2 << eol;
  theCout << bicgstabKV << eol;
  nbErrors += checkValue(theCout, rootname+"/solvers.in", errors, "Defining solvers", check);
  
  /*!
    Vectors
  */
  //! Real Vector B
  Vector<Real> rvB(rowNum, 1.);

  //! Real initial value
  Vector<Real> rvX0(rowNum, 0.);

  //! Vector real unknown
  Vector<Real> rvX(rowNum);
  Vector<Real> rvXe(rowNum);
  for (Number i=0;i<rowNum; i++) rvXe(i+1)=Real(i+1);
  theCout << "Real vector Xe is: " << rvXe <<  std::endl;

  //!-------------------------
  //! Vector complex B
  Vector<Complex> cvB(rowNum, 1.);

  //!  Vector initial value
  Vector<Complex> cvX0(rowNum, 0.);

  //! Vector complex unknown
  Vector<Complex> cvX(rowNum);
  Vector<Complex> cvXe(rowNum);
  for (Number i=0;i<rowNum; i++) cvXe(i+1)=Complex(1.,1.)*Real(i+1);
  theCout << "Complex vector Xe is: " << cvXe <<  std::endl;

  /*!
    Real Large Matrix (Dense Type)
  */
  //! No symmetric
  LargeMatrix<Real> rMatDenseRow(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Real> rMatDenseCol(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Real> rMatDenseDual(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Real> rMatDenseSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _dense, _sym);

  //! Symmetric
  LargeMatrix<Real> rMatDenseRowSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Real> rMatDenseColSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Real> rMatDenseDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Real> rMatDenseSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _dense, _symmetric);

  //! Skew Symmetric
  LargeMatrix<Real> rMatDenseRowSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Real> rMatDenseColSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Real> rMatDenseDualSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Real> rMatDenseSymSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, _dense, _skewSymmetric);

  /*!
   Complex Large Matrix (Dense Type)
  */
  //! No symmetric
  LargeMatrix<Complex> cMatDenseRow(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseCol(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDual(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSym(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _dense, _sym);

  //! Symmetric
  LargeMatrix<Complex> cMatDenseRowSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseColSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _dense, _symmetric);

  //! Self Ajoint Symmetric
  LargeMatrix<Complex> cMatDenseRowSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseColSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDualSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSymSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, _dense, _selfAdjoint);

  //! Skew Ajoint Symmetric
  LargeMatrix<Complex> cMatDenseRowSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _dense, _row);
  LargeMatrix<Complex> cMatDenseColSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _dense, _col);
  LargeMatrix<Complex> cMatDenseDualSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _dense, _dual);
  LargeMatrix<Complex> cMatDenseSymSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, _dense, _skewAdjoint);

  /*!
    Real Large Matrix (CS Type)
  */
  //! No symmetric
  LargeMatrix<Real> rMatCsRow(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Real> rMatCsCol(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Real> rMatCsDual(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _cs, _sym);

  //! Symmetric
  LargeMatrix<Real> rMatCsRowSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Real> rMatCsColSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Real> rMatCsDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _cs, _symmetric);

  //! Skew Symmetric
  LargeMatrix<Real> rMatCsRowSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Real> rMatCsColSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Real> rMatCsDualSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Real> rMatCsSymSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, _cs, _skewSymmetric);

  /*!
    Complex Large Matrix (Cs Type)
  */
  //! No symmetric
  LargeMatrix<Complex> cMatCsRow(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsCol(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDual(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSym(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _cs, _sym);

  //! Symmetric
  LargeMatrix<Complex> cMatCsRowSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsColSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _cs, _symmetric);

  //! Self Ajoint Symmetric
  LargeMatrix<Complex> cMatCsRowSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsColSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDualSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSymSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, _cs, _selfAdjoint);

  //! Skew Ajoint Symmetric
  LargeMatrix<Complex> cMatCsRowSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _cs, _row);
  LargeMatrix<Complex> cMatCsColSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _cs, _col);
  LargeMatrix<Complex> cMatCsDualSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _cs, _dual);
  LargeMatrix<Complex> cMatCsSymSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, _cs, _skewAdjoint);

  /*!
    Real Large Matrix (Skyline Type)
  */
  //! No symmetric
  LargeMatrix<Real> rMatSkylineDual(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSym(inputsPathTo(rMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _sym);

  //! Symmetric
  LargeMatrix<Real> rMatSkylineDualSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSymSym(inputsPathTo(rMatrixDataSym), _dense, rowNum, _skyline, _symmetric);

  //! Skew Symmetric
  LargeMatrix<Real> rMatSkylineDualSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Real> rMatSkylineSymSkewSym(inputsPathTo(rMatrixDataSkewSym), _dense, rowNum, _skyline, _skewSymmetric);

  /*!
    Complex Large Matrix (Skyline Type)
  */
  //! No symmetric
  LargeMatrix<Complex> cMatSkylineDual(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSym(inputsPathTo(cMatrixDataNoSym), _dense, rowNum, colNum, _skyline, _sym);

  //! Symmetric
  LargeMatrix<Complex> cMatSkylineDualSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSymSym(inputsPathTo(cMatrixDataSym), _dense, rowNum, _skyline, _symmetric);

  //! Self Ajoint Symmetric
  LargeMatrix<Complex> cMatSkylineDualSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSymSymSelfAjoint(inputsPathTo(cMatrixDataSymSelfAjoint), _dense, rowNum, _skyline, _selfAdjoint);

  //! Skew Ajoint Symmetric
  LargeMatrix<Complex> cMatSkylineDualSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, colNum, _skyline, _dual);
  LargeMatrix<Complex> cMatSkylineSymSymSkewAjoint(inputsPathTo(cMatrixDataSymSkewAjoint), _dense, rowNum, _skyline, _skewAdjoint);

  /*!------------------------------------------------------------------------------
     Test with Real value
    -----------------------------------------------------------------------------
  */
  // I. Solver without a preconditioner
  //! Test with DENSE STORAGE
  //! Test with symmetric matrices
  rvB = rMatDenseRowSym *rvXe;
  rvX = bicgstab(rMatDenseRowSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense col and sym matrix with real RHS");
  rvX = bicgstab(rMatDenseColSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense col and sym matrix with real RHS");
  rvX = bicgstab(rMatDenseDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense dual and sym matrix with real RHS");
  rvX = bicgstab(rMatDenseSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense sym and sym matrix with real RHS");

  //! Test with skew symmetric matrices
  rvB = rMatDenseRowSkewSym *rvXe;
  rvX = bicgstab(rMatDenseRowSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense row and skew sym matrix with real RHS");
  rvX = bicgstab(rMatDenseColSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense col and skew sym matrix with real RHS");
  rvX = bicgstab(rMatDenseDualSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense dual and skew sym matrix with real RHS");
  rvX = bicgstab(rMatDenseSymSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense sym and skew sym matrix with real RHS");

  //! Test with non symmetric matrices
  rvB = rMatDenseRow *rvXe;
  rvX = bicgstab(rMatDenseRow, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense row and non-sym matrix with real RHS");
  rvX = bicgstab(rMatDenseCol, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense col and non-sym matrix with real RHS");
  rvX = bicgstab(rMatDenseDual, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense dual and non-sym matrix with real RHS");
  rvX = bicgstab(rMatDenseSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real dense sym and non-sym matrix with real RHS");

  //!---------------------------------
  //! Test with CS STORAGE
  //! Test with symmetric matrices
  rvB = rMatCsRowSym *rvXe;
  rvX = bicgstab(rMatCsRowSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs row and sym matrix with real RHS");
  rvX = bicgstab(rMatCsColSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs col and sym matrix with real RHS");
  rvX = bicgstab(rMatCsDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs dual and sym matrix with real RHS");
  rvX = bicgstab(rMatCsSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs sym and sym matrix with real RHS");

  //! Test with skew symmetric matrices
   rvB = rMatCsRowSkewSym *rvXe;
  rvX = bicgstab(rMatCsRowSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs sym and skew sym matrix with real RHS");
  rvX = bicgstab(rMatCsColSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs col and skew sym matrix with real RHS");
  rvX = bicgstab(rMatCsDualSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs dual and skew sym matrix with real RHS");
  rvX = bicgstab(rMatCsSymSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs sym and skew sym matrix with real RHS");

  //! Test with non symmetric matrices
  rvB = rMatCsRow *rvXe;
  rvX = bicgstab(rMatCsRow, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs row and non-sym matrix with real RHS");
  rvX = bicgstab(rMatCsCol, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs col and non-sym matrix with real RHS");
  rvX = bicgstab(rMatCsDual, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs dual and non-sym matrix with real RHS");
  rvX = bicgstab(rMatCsSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Cs sym and non-sym matrix with real RHS");

  //!------------------------------
  //! Test with SKYLINE STORAGE
  //! Test with symmetric matrices
  rvB = rMatSkylineDualSym *rvXe;
  rvX = bicgstab(rMatSkylineDualSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Skyline dual and sym matrix with real RHS");
  rvX = bicgstab(rMatSkylineSymSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Skyline sym and sym matrix with real RHS");

  //! Test with skew symmetric matrices
  rvB = rMatSkylineDualSkewSym *rvXe;
  rvX = bicgstab(rMatSkylineDualSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Skyline dual and skew sym matrix with real RHS");
  rvX = bicgstab(rMatSkylineSymSkewSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Skyline sym and skew sym matrix with real RHS");

  //! Test with non symmetric matrices
  rvB = rMatSkylineDual *rvXe;
  rvX = bicgstab(rMatSkylineDual, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Skyline dual and non-sym matrix with real RHS");
  rvX = bicgstab(rMatSkylineSym, rvB, rvX0, _real);
  nbErrors += checkValuesN2(rvX, rvXe, eps,  errors, "real Skyline sym and non-sym matrix with real RHS");

  /*! -----------------------------------------------------------------------------
    Test with Complex values (Complex x Complex = Complex)
    -----------------------------------------------------------------------------
  */
  // I. Solver without a preconditioner
  //! Test with DENSE STORAGE
  //! Test with symmetric matrices
  cvB = cMatDenseRowSym *cvXe;
  cvX = bicgstab(cMatDenseRowSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense row and sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseColSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense col and sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense dual and sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense sym and sym matrix with complex RHS");

  //! Test with Sym SelfAjoint symmetric matrices
  cvB = cMatDenseRowSymSelfAjoint *cvXe;
  cvX = bicgstab(cMatDenseRowSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense row and selfadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseColSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense col and selfadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseDualSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense dual and selfadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseSymSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense sym and selfadjoint sym matrix with complex RHS");

  //! Test with Skew Ajoint symmetric matrices
  cvB = cMatDenseRowSymSkewAjoint *cvXe;
  cvX = bicgstab(cMatDenseRowSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense row and skewadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseColSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense col and skewadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseDualSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense dual and skewadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseSymSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense sym and skewadjoint sym matrix with complex RHS");

  //! Test with no symmetric matrices
  cvB = cMatDenseRow *cvXe;
  cvX = bicgstab(cMatDenseRow, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense row and non-sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseCol, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense col and non-sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseDual, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense dual and non-sym matrix with complex RHS");
  cvX = bicgstab(cMatDenseSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense sym and non-sym matrix with complex RHS");

  //!---------------------------------
  //! Test with CS STORAGE
  //! Test with symmetric matrices
  cvB = cMatCsRowSym *cvXe;
  cvX = bicgstab(cMatCsRowSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs row and sym matrix with complex RHS");
  cvX = bicgstab(cMatCsColSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs col and sym matrix with complex RHS");
  cvX = bicgstab(cMatCsDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs dual and sym matrix with complex RHS");
  cvX = bicgstab(cMatCsSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs sym and sym matrix with complex RHS");

  //! Test with selfAjoint symmetric matrices
  cvB = cMatCsRowSymSelfAjoint *cvXe;
  cvX = bicgstab(cMatCsRowSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs row and selfadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatCsColSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs col and selfadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatCsDualSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs dual and selfadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatCsSymSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs sym and selfadjoint sym matrix with complex RHS");

  //! Test with skewAjoint symmetric matrices
  cvB = cMatCsRowSymSkewAjoint*cvXe;
  cvX = bicgstab(cMatCsRowSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs row and skewadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatCsColSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs col and skewadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatCsDualSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs dual and skewadjoint sym matrix with complex RHS");
  cvX = bicgstab(cMatCsSymSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Cs sym and skewadjoint sym matrix with complex RHS");

  //! Test with no symmetric matrices
  cvB = cMatCsRow*cvXe;
  cvX = bicgstab(cMatCsRow, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense row and non-sym matrix with complex RHS");
  cvX = bicgstab(cMatCsCol, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense col and non-sym matrix with complex RHS");
  cvX = bicgstab(cMatCsDual, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense dual and non-sym matrix with complex RHS");
  cvX = bicgstab(cMatCsSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex dense sym and non-sym matrix with complex RHS");

  //!---------------------------------
  //! Test with SKYLINE STORAGE
  //! Test with symmetric matrices
  cvB = cMatSkylineDualSym*cvXe;
  cvX = bicgstab(cMatSkylineDualSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline dual and sym matrix with complex RHS");
  cvX = bicgstab(cMatSkylineSymSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline sym and sym matrix with complex RHS");

  //! Test with self ajoint sym matrices
  cvB = cMatSkylineDualSymSelfAjoint*cvXe;
  cvX = bicgstab(cMatSkylineDualSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline dual and selfajoint sym matrix with complex RHS");
  cvX = bicgstab(cMatSkylineSymSymSelfAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline sym and selfajoint  matrix with complex RHS");

  //! Test with skew ajoint matrices
  cvB = cMatSkylineDualSymSkewAjoint*cvXe;
  cvX = bicgstab(cMatSkylineDualSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline dual and skewajoint  matrix with complex RHS");
  cvX = bicgstab(cMatSkylineSymSymSkewAjoint, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline sym and skewajoint  matrix with complex RHS");

  //! Test with no symmetric matrices
  cvB = cMatSkylineDual*cvXe;
  cvX = bicgstab(cMatSkylineDual, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline dual and non-sym matrix with complex RHS");
  cvX = bicgstab(cMatSkylineSym, cvB, cvX0, xlifepp::_complex);
  nbErrors += checkValuesN2(cvX, cvXe, eps,  errors, "Complex Skyline sym and non-sym matrix with complex RHS");
  
  if (check)
  {
   if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
   else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else
  { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
