/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Projector.cpp
    \authors E. Lunéville
    \since 18 nov 2016
    \date  22 nov 2016

	Test of Projector class
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Projector {

Real fx(const Point&P, Parameters& pa = defaultParameters)
{return P(1)+P(2);}

Real fx2(const Point&P, Parameters& pa = defaultParameters)
{return P(1)*P(1)+P(2)*P(2);}

void unit_Projector(int argc, char* argv[], bool check)
{
  String rootname = "unit_Projector";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);
  String errors;
  Number nbErrors = 0;
  Real tol=0.00001;
  verboseLevel(1);
  //numberOfThreads(1);

  //!create a mesh and Domains
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=6,_side_names="Gamma"), _shape=_triangle, _order=1, _generator=_structured);
  Domain omega=mesh2d.domain("Omega"), gamma = mesh2d.domain("Gamma");

  //! create some spaces
  Space V0(_domain=omega, _interpolation=_P0, _name="V0", _notOptimizeNumbering);
  Unknown u0(V0, _name="u0");
  Space V1(_domain=omega, _interpolation=_P1, _name="V1", _notOptimizeNumbering);
  Unknown u1(V1, _name="u1");  TestFunction v1(u1,"v1");
  Space V2(_domain=omega, _interpolation=_P2, _name="V2", _notOptimizeNumbering);
  Unknown u2(V2, _name="u2");
  Space N1(omega, interpolation(_Nedelec,_firstFamily,1,Hrot), "N1", false);
  Unknown n1(N1, _name="n1");

  //!create some projectors from spaces and projector type
  Projector P0toP1(V0,V1,_L2Projector,"P0toP1");
  Projector P1toP0(V1,V0,_L2Projector,"P1toP0");
  Projector P2toP1(V2,V1,_L2Projector,"P2toP1");
  Projector N1toP1(N1,1,V1,2,_L2Projector,"N1toP1");
  theCout<<P0toP1<<P1toP0<<N1toP1;
  Projector P2toP1_H1(V2,V1,_H1Projector,"P2toP1_H1");
  theCout<<P2toP1_H1;
  //!create some projectors from spaces, domain and projector type
  Projector P1toP0_Gamma(V1,V0,gamma,_L2Projector,"P1toP0_Gamma");
  Projector P0toP1_Gamma(V0,V1,gamma,_L2Projector,"P0toP1_Gamma");
  theCout<<P1toP0_Gamma<<P0toP1_Gamma;

  //!create some projectors from spaces and bilinear form
  BilinearForm a=2*intg(omega,u1*v1);
  Projector P0toP1_a(V0,V1,a,"P0toP1_a");
  theCout<<P0toP1_a;
  BilinearForm a_gamma=intg(gamma,u1*v1)+intg(omega,u1*v1);
  Projector P0toP1_a_gamma(V0,V1,a_gamma,"P0toP1_a_gamma");
  theCout<<P0toP1_a_gamma;

  //!some projections
  verboseLevel(90);
  TermVector B0(u0,omega,1.,"B0");
  theCout<<"B0 = "<<B0;
  TermVector B01=P0toP1(B0,u1);
  theCout<<"B01=P0toP1(B0) = "<< B01;
  TermVector B010=P1toP0(B01,u0);
  theCout<<"B010=P1toP0(B01) = "<<B010;
  TermVector B2(u2,omega,fx2,"B2");
  theCout<<"B2 = "<<B2;
  TermVector B21=P2toP1(B2,u1);
  theCout<<"B21=P2toP1(B2) = "<< B21;
  TermVector B21b=P2toP1*B2;
  theCout<<"B21b=P2toP1*B2 = "<< B21b;
  Unknown e1(V1, _name="e1", _dim=2);
  TermVector E1(n1,omega,1.,"E1");
  TermVector E1P1=N1toP1(E1,e1);
  theCout<<"E1P1=N1toP1*E1 = "<< E1P1;

  nbErrors += checkValues(B0, rootname+"/B0.in", tol, errors, "Vec B0", check);
  nbErrors += checkValues(B01, rootname+"/B01.in", tol, errors, "Vec B01", check);
  nbErrors += checkValues(B010, rootname+"/B010.in", tol, errors, "Vec B010", check);
  nbErrors += checkValues(B2, rootname+"/B2.in", tol, errors, "Vec B2", check);
  nbErrors += checkValues(B21, rootname+"/B21.in", tol, errors, "Vec B21", check);
  nbErrors += checkValues(B21b, rootname+"/B21b.in", tol, errors, "Vec B21b", check);
  nbErrors += checkValues(E1P1, rootname+"/E1P1.in", tol, errors, "Vec E1P1", check);

  //!reinterpret as TermMatrix
  TermMatrix& P21 = P2toP1.asTermMatrix(u2,u1,"P21");
  nbErrors += checkValues( P21, rootname+"/P21.in",  tol, errors, "Mat P21", check);
  TermVector B21c=P21*B2;
  verboseLevel(10);
  ssout<<"B21c=P21*B2 = "<< B21c;
  nbErrors += checkValues(B21c, rootname+"/B21c.in", tol, errors, "Vec B21c", check);

  TermMatrix& N1P1 = N1toP1.asTermMatrix(n1,e1,"N1P1");
  ssout<<N1P1;
  TermVector E1P1c=N1P1*E1;
  verboseLevel(90);
  ssout<<"E1P1c=N1P1*E1 = "<< E1P1c;
  nbErrors += checkValues(E1P1c, rootname+"/E1P1c.in", tol, errors, "Vec E1P1c", check);

  //!call projection function
  TermVector B21d=projection(B2,V1);  //implicit L2 Projection, use existing P2toP1 projector
  ssout<<"B21d = projection(B2,V1) = "<< B21d;
  nbErrors += checkValues(B21d, rootname+"/B21d.in", tol, errors, "Vec B21d", check);
  TermVector B1(u1,omega,fx2,"B1");
  TermVector B12=projection(B1,V2);  //implicit L2 Projection, create projector P1->P2 on the fly
  ssout<<"B12 = projection(B1,V2) = "<< B12.roundToZero();
  nbErrors += checkValues(B12, rootname+"/B12.in", tol, errors, "Vec B12", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
