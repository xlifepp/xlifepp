/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*!
	\file unit_ILU_LargeMatrixCsStorage.cpp
	\authors C.Chambeyron
	\since 24 Mars 2017

	Low level tests of LargeMatrix  CsStorage ILU factorization.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <vector>

using namespace xlifepp;

namespace unit_ILU_LargeMatrixCsStorage
{

void unit_ILU_LargeMatrixCsStorage(int argc, char* argv[], bool check)
{
  String rootname = "unit_ILU_LargeMatrixCsStorage";
  trace_p->push(rootname);
  verboseLevel(9);
  
  String errors;
  Number nbErrors = 0;
  Real eps=0.001;
  bool isTM=true;

  Number nbLignes = 4;
  bool  pmat=true; // print matrix
  bool  psto=true;  // print storage
  
  theCout << "\n\n----------------------------------- test Col cs storage -----------------------------------\n";
  std::vector< std::vector<Number> > RI;
  std::vector<Number> u(nbLignes,0.);
  std::vector<Number> w(3,0.);
  w[0]=1; w[1]=3; w[2]=4;
  std::vector<Number> x(3,0.);
  x[0]=1; x[1]=2; x[2]=3;
  for(int k=0; k<nbLignes; k++) u[k]=k+1;
  RI.push_back(w);
  RI.push_back(x);
  RI.push_back(u);
  RI.push_back(w);

  ColCsStorage* MyColCs = new ColCsStorage(nbLignes, nbLignes,RI,"mycolCS");
  LargeMatrix<Real> MatC(MyColCs,0.);

  MatC(1,1)= 2;      MatC(1,2)= 1;     MatC(1,3)= 1;   MatC(1,4)= 2;
  /* MatC(2,1)= 4; */ MatC(2,2)= 5;     MatC(2,3)= 4; /* MatC(2,4)= 5; */
  MatC(3,1)= 6;      MatC(3,2)= 15;   MatC(3,3)= 19;  MatC(3,4)= 11;
  MatC(4,1)= 4;   /* MatC(4,2)= 1; */ MatC(4,3)= -2;   MatC(4,4)= 16 ;

  LargeMatrix<Real> MatC0=MatC;
  if (pmat)  MatC.saveToFile("MatCdense.mat", _dense);

  if (!isTM) {theCout<<"Mat Column CS Storage:"<<std::endl<<MatC<<std::endl;}
  iluFactorize(MatC);
  if (pmat)    MatC.saveToFile("ILUColdense.mat", _dense);
  if (!isTM) {theCout<<"ILU factorised Column CS Storage:"<<std::endl<<MatC<<std::endl;}
  nbErrors += checkIncompleteFacto( _ilu, MatC0, MatC, eps, errors, "-> Test incomplete facto  iLU ColCS \n");

  theCout << "\n\n----------------------------------- test row cs storage ----------------------------------\n";
  std::vector< std::vector<Number> > CI;
  std::vector<Number> a(nbLignes,0.);
  std::vector<Number> v(3,0.);
  v[1]=2; v[2]=3;
  std::vector<Number> c(3,0.);
  c[0]=1; c[1]=3; c[2]=4;
  for(int k=0; k<nbLignes; k++)  a[k]=k+1;
  CI.push_back(a);
  CI.push_back(v);
  CI.push_back(a);
  CI.push_back(c);

  RowCsStorage* MyRowCs = new RowCsStorage(nbLignes, nbLignes,CI,"myrowCS");
  LargeMatrix<Real> MatR(MyRowCs,0.);
  if (psto)  MatR.viewStorage(thePrintStream);

  giveValue(MatC0, MatR);
  LargeMatrix<Real> MatR0=MatR;

  if (pmat)   MatR.saveToFile("MatRdense.mat", _dense);
  if (!isTM) {theCout<<"Row CS Storage:"<<std::endl<<MatR<<std::endl;}
  iluFactorize(MatR);
  nbErrors += checkIncompleteFacto( _ilu, MatR0, MatR, eps, errors, "-> Test incomplete facto  iLU RowCS \n");
  if (pmat)    MatR.saveToFile("ILURowdense.mat", _dense);

  theCout << "\n\n----------------------------------- test dual cs storage ----------------------------------\n";
  std::vector<Number> CID(4,0);
  std::vector< Number > RPD(5,0.);
  std::vector< Number>  RID(5,0.);
  std::vector< Number > CPD(5,0);

  CID[0]=0; CID[1]=1; CID[2]=0; CID[3]=2;
  RPD[0]=0; RPD[1]=0; RPD[2]=0; RPD[3]=2; RPD[4]=4;
  RID[0]=0; RID[1]=0; RID[2]=1; RID[3]=0; RID[4]=2;
  CPD[0]=0; CPD[1]=0; CPD[2]=1; CPD[3]=3; CPD[4]=5;

  DualCsStorage* MyDualCs = new DualCsStorage(nbLignes, nbLignes,CID,RPD,RID,CPD,"DCS");
  LargeMatrix<Real> MatD(MyDualCs,0.);
  if (psto)   MatD.viewStorage(thePrintStream);

  giveValue( MatC0, MatD);
  LargeMatrix<Real> MatD0=MatD;

  if (!isTM) {theCout<<"Dual CS Storage:"<<std::endl<<MatD<<std::endl;}
  if (pmat)    MatD.saveToFile("MatDdense.mat", _dense);
  iluFactorize(MatD);
  nbErrors += checkIncompleteFacto( _ilu, MatD0, MatD, eps, errors, "-> Test incomplete facto  iLU DualCS \n");
  if (pmat)  MatD.saveToFile("ILUDualdense.mat", _dense);

  theCout << "\n\n------------------------- test dual cs storage, matrice EF --------------------------\n";
  Number n=3;
  std::vector< std::vector<Number> > elts((n - 1) * (n - 1), std::vector<Number>(4));
  for(Number j = 1; j <= n - 1; j++)
    for(Number i = 1; i <= n - 1; i++)
      {
        Number e = (j - 1) * (n - 1) + i - 1, p = (j - 1) * n + i;
        elts[e][0] = p; elts[e][1] = p + 1; elts[e][2] = p + n; elts[e][3] = p + n + 1;
      }
  DualCsStorage* csdual = new DualCsStorage(n * n, n * n, elts, elts);
  LargeMatrix<Real> Mat_0(csdual, 1.);
  //!D
  Mat_0(1,1)=1.;
  Mat_0(2,2)=2.;
  Mat_0(3,3)=3.;
  Mat_0(4,4)=4;
  Mat_0(5,5)=5;
  Mat_0(6,6)=6;
  Mat_0(7,7)=7;
  Mat_0(8,8)=8;
  Mat_0(9,9)=9;

  //!L
  Mat_0(2,1)= 3.;
  Mat_0(3,2)= 2.;
  Mat_0(4,1)=-1.;   Mat_0(4,2)=-2;
  Mat_0(5,1)= 2.;   Mat_0(5,2)= 2.;   Mat_0(5,3)=2.;  Mat_0(5,4)=-2.;
  Mat_0(6,2)= 3.;   Mat_0(6,3)= 2.;   Mat_0(6,5)=1.;
  Mat_0(7,4)= 4.;   Mat_0(7,5)=-3.;
  Mat_0(8,4)=-1.;   Mat_0(8,5)=-2.;   Mat_0(8,6)=1.;  Mat_0(8,7)=2.;
  Mat_0(9,5)= 1.;   Mat_0(9,6)= 2.;   Mat_0(9,8)=1.;
  /*U*/
  Mat_0(1,2)= 1.;  Mat_0(1,4)=-1.;  Mat_0(1,5)= 2.;
  Mat_0(2,3)=-2.;  Mat_0(2,4)= 1.;  Mat_0(2,5)= 2.; Mat_0(2,6)=-1.;
  Mat_0(3,5)= 1.;  Mat_0(3,6)= 1.;
  Mat_0(4,5)=-2.;  Mat_0(4,7)= 1.;  Mat_0(4,8)= 1.;
  Mat_0(5,6)= 1.;  Mat_0(5,7)=-1.;  Mat_0(5,8)=-1.; Mat_0(5,9)= 1.;
  Mat_0(6,8)=-2.;  Mat_0(6,9)= 2.;
  Mat_0(7,8)= 1.;
  Mat_0(8,9)= 1.;

  if (!isTM) {theCout << "Mat_0="<<std::endl <<Mat_0<< std::endl;}
  if (pmat)  Mat_0.saveToFile("Mat_0dense.mat", _dense);

  LargeMatrix<Real> ILU= Mat_0;
  iluFactorize(ILU);
  nbErrors += checkIncompleteFacto( _ilu, Mat_0, ILU, eps, errors, "-> Test incomplete facto  iLU EF  DualCS \n");
  if (pmat)  ILU.saveToFile("ILUdense.mat", _dense);
  if (psto)   ILU.viewStorage(thePrintStream);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// void giveValue( const LargeMatrix<Real>& M0, LargeMatrix<Real>& M1)
// {
//   // M1(i,j)=M0(i,j)
//   for(Number j = 1; j <= M0.nbRows ; j++)
//     for(Number i = 1; i <= M0.nbRows; i++)
//       if ( M0.pos(i,j) != 0 ) M1(i,j)=M0(i,j);
// }
// 
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// void validILU(std::stringstream& theCout, CsStorage* sto,String Name, LargeMatrix<Real>& M0, LargeMatrix<Real>& MILU)
// {
//   Number OK =0;
//   Number nbLignes = M0.nbRows;
//   LargeMatrix<Real> Mat_L(sto,0.);
//   LargeMatrix<Real> Mat_U(sto,0.);
//   for (Number i=1; i <=9; i++)
//     for (Number j=1; j <= i; j++)
//       if (MILU.pos(i,j) != 0)
//         {
//           if (i > j )  Mat_L(i,j)= MILU(i,j);
//           else if (i==j) Mat_L(i,j)=1.;
//         }
//   for (Number i=1; i <=9; i++)
//     for (Number j=i; j <=9; j++)
//       if (MILU.pos(i,j) != 0)
//         if (i <= j )  Mat_U(i,j)= MILU(i,j);
// //   Mat_U.saveToFile("Mat_U.mat", _dense);
// //   Mat_L.saveToFile("Mat_L.mat", _dense);
//   LargeMatrix<Real> MVerif = Mat_L*Mat_U;
// //   MVerif.saveToFile("MVerif.mat", _dense);
// 
// 
//   theCout << "test de validation:"<<std::endl;
//   theCout<<"MVerif = "<<eol<<MVerif<<eol;
//   theCout << " Le produit des matrices L et U de la factorisation ILU d'une matrice stockee Cs: "<< Name<<" a les memes coef non nuls que la matrices de depart a 0.0001 pres" << std::endl;
// 
//   for(Number j = 1; j <= nbLignes ; j++)
//     for(Number i = 1; i <= nbLignes; i++)
//       if ( M0.pos(i,j) != 0 )
//         if   ( std::abs(M0(i,j) - MVerif(i,j)) > 0.0001 )
//           {
//             theCout << "non valide"<<std::endl;
//             theCout << "car "<<M0(i,j)<<" \neq "<<MVerif(i,j)<<std::endl;
//             OK=1; break;
//           }
//   if (OK==0) theCout<<"-> Test ILU "<<Name<< " valide"<<std::endl;
// }
// 
}
