/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_TermIE.cpp
	\author E. Lunéville
	\since 13 sept 2013
	\date 18 sept 2013

	Tests of IE tem computation

	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting informations in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_TermIE {

//! scalar kernel function
Real G1(const Point& M, const Point& P, Parameters& pa = defaultParameters)
{
  return M.distance(P);
}

Real alpha(const Point& M,  Parameters& pa = defaultParameters)
{
  return 2.;
}


void unit_TermIE(int argc, char* argv[], bool check)
{
  String rootname = "unit_TermIE";
  trace_p->push(rootname);
  std::stringstream ssout;                  // string stream receiving results
  ssout.precision(testPrec);
  numberOfThreads(1);

  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  verboseLevel(9);
	//! create a 2D mesh and Domains
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=1"; sidenames[2] = "y=1"; sidenames[3] = "x=0";
  verboseLevel(10);
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=5,_side_names=sidenames),_shape=_triangle, _order=1, _generator=_structured, _name="P1 mesh of [0,1]x[0,1]");
  Domain omega=mesh2d.domain("Omega");
  Domain gamma1=mesh2d.domain("y=0");
  Domain gamma2=mesh2d.domain("y=1");

  //!create Lagrange P1 space and unknown
  Interpolation *LagrangeP1=findInterpolation(Lagrange,standard,1,H1);
  Space V1(omega,*LagrangeP1,"V1",false);
  Unknown u(V1, _name="u");
  TestFunction v(u, _name="v");

  Parameters pars(1.,"k");

  //!test regular Kernel (default quadrature)
  Kernel G=Helmholtz2dKernel(pars);
  BilinearForm bf = intg(gamma1, gamma2, u*G*v);
  ssout<<bf;
  TermMatrix MG12(bf, _name="u*G*v");
  ssout<<MG12<<eol;
  theCout<<MG12<<eol;
  TermVector un_g1(u,gamma1,1., _name="un_gamma1");
  TermVector un_g2(v,gamma2,1., _name="un_gamma2");
  ssout<<"\n (MG12*1_g1,1_g2)="<<realPart(MG12*un_g2|un_g1)<<"\n\n";
  theCout<<TermMatrix(intg(gamma1, gamma2, u*G*(alpha*v)))<<eol; //check function in opv

  //!test Kernel matrix and integral representation
  TermMatrix K12(v,gamma1,u, gamma2, G, _name="K12");
  ssout<<K12;
  TermMatrix M2(intg(gamma2,v*u), _name="M2");
  ssout<<K12*(M2*un_g2);
  nbErrors+=checkValues(K12, rootname+"/K12.in", tol, errors, "K12 ", check);
  nbErrors+=checkValues(K12*(M2*un_g2), rootname+"/K12M2un_g2.in", tol, errors, "K12*(M2*un_g2)", check);

  //!test omega-gamma integral
  BilinearForm bf2 = intg(omega, gamma1, u*G1*v);
  theCout<<bf2<<std::flush;
  TermMatrix MG2(bf2, _name="u*G1*v");
  ssout<<MG2<<eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  
}

}
