/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Quadrature.cpp
	\author E. Lunéville
	\since  11 sep 2012
	\date 11 sep 2012

	Low level tests of Quadrature and QuadratureRule classes.
 */

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace std;
using namespace xlifepp;

namespace unit_Quadrature
{

//!evaluate monomial integral with quadrature rule in 1,2 or 3 dimensions
Real approximateIntegral(const QuadratureRule& qr, Number n1, Number n2 = 0, Number n3 = 0)
{
  Real r = 0.;
  std::vector<Real>::const_iterator c_i = qr.coords().begin();
  for (std::vector<Real>::const_iterator w_i = qr.weights().begin(); w_i != qr.weights().end(); w_i++)
  {
    Real monomial = 1.;
    if(n1 > 0) { monomial *= std::pow(*(c_i), n1); } c_i++;
    if(qr.dim() > 1) { if(n2 > 0) { monomial *= std::pow(*(c_i), n2); } c_i++; }
    if(qr.dim() > 2) { if(n3 > 0) { monomial *= std::pow(*(c_i), n3); } c_i++; }
    r += (*w_i) * monomial;
  }
  return r;
}

//!exact evaluation of monomial integral in 1,2 or 3 dimensions
Real exactIntegral(ShapeType sh, Number n1, Number n2 = 0, Number n3 = 0)
{
  Real r = 1.;
  switch (sh)
  {
    case _segment     : return 1. / (n1 + 1.);
    case _quadrangle  : return 1. / ((n1 + 1.) * (n2 + 1.));
    case _hexahedron  : return 1. / ((n1 + 1.) * (n2 + 1.) * (n3 + 1.));
    case _triangle    :   // n1! n2! / (2+n1+n2)!
      for (Number d = 2; d <= n1; d++) { r *= d; }
      for (Number d = 2; d <= n2; d++) { r *= d; }
      for (Number d = 2; d <= 2 + n1 + n2 ; d++) { r *= 1. / d; }
      return r;
    case _tetrahedron :  // n1! n2! n3! / (3+n1+n2+n3)!
      for (Number d = 2; d <= n1; d++) { r *= d; }
      for (Number d = 2; d <= n2; d++) { r *= d; }
      for (Number d = 2; d <= n3; d++) { r *= d; }
      for (Number d = 2; d <= 3 + n1 + n2 + n3; d++) { r *= 1. / d; }
      return r;
    case _prism       : // n1! n2! / [(2+n1+n2)! (n3+1)]
      for (Number d = 2; d <= 2 + n1 + n2; d++) { r *= 1. / d; }
      for (Number d = 2; d <= n1; d++) { r *= d; }
      for (Number d = 2; d <= n2; d++) { r *= d; }
      return r / (n3 + 1);
    case _pyramid     : // n3! (n1+n2+2)! / [(3+n1+n2+n3)! (n1+1)(n2+1)]
      for (Number d = 2; d <= n3; d++) { r *= d; }
      for (Number d = 2; d <= 2 + n1 + n2; d++) { r *= d; }
      for (Number d = 2; d <= 3 + n1 + n2 +n3; d++) { r /= d; }
      return r / ((n1 + 1)*(n2+1));
    default:
      break;
  }
  return r;
}


//!check quadrature rule for monomial of max degree +1 of quadrature
void checkRule(const Quadrature& quad, std::ostream& os, Real relTol=1.0e-4)
{
  os << " Check rule " << quad.name << ", degree " << quad.degree
     << " on " << words("shape", quad.shapeType());
  for (Number dg = 0; dg <= quad.degree + 1; dg++)
  {
    Real err, relerr, exact, approx;
    switch (quad.shapeType())
    {
      case _segment:
        exact = exactIntegral(_segment, dg);
        approx = approximateIntegral(quad.quadratureRule, dg);
        err = std::abs(exact - approx);
        relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
        err=relerr*std::abs(exact);
        os << std::endl << "  x^" << dg << " | " << std::setw(18) << exact << " | "
           << std::setw(18) << approx << " | " << std::setw(18) << err;
        if (relerr > relTol) { os << " **** Error ****"; }
        break;
      case _quadrangle:
        os << std::endl << std::endl << " Monomial | deg |        exact       |       approx       |      error";
        os << std::endl << " -----------------------------------------------------------------------------";
        for (Number n1 = 0; n1 <= dg; n1++)
        {
          exact = exactIntegral(_quadrangle, n1, dg);
          approx = approximateIntegral(quad.quadratureRule, n1, dg);
          err = std::abs(exact - approx);
          relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
          err=relerr*std::abs(exact);
          os << std::endl << "   x^" << n1 << "y^" << dg << " | " << std::setw(3) << n1 + dg << " | "
             << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
          if (relerr > relTol) { os << " **** Error ****"; }
        }
        for (Number n2 = 0; n2 < dg; n2++)
        {
          Number n2bis=dg-n2;
          exact = exactIntegral(_quadrangle, dg, n2bis - 1);
          approx = approximateIntegral(quad.quadratureRule, dg, n2bis - 1);
          err = std::abs(exact - approx);
          relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
          err=relerr*std::abs(exact);
          os << std::endl << "   x^" << dg << "y^" << n2bis - 1 << " | " << std::setw(3) << n2bis - 1 + dg << " | "
              << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
          if (relerr > relTol) { os << " **** Error ****"; }
        }
        break;
      case _hexahedron:
        os << std::endl << std::endl << " Monomial | deg |        exact       |       approx       |      error";
        os << std::endl << " -----------------------------------------------------------------------------";
        for (Number n3 = 0; n3 <= dg; n3++)
        {
          for (Number n1 = 0; n1 <= dg; n1++)
          {
            exact = exactIntegral(_hexahedron, n1, dg, n3);
            approx = approximateIntegral(quad.quadratureRule, n1, dg, n3);
            err = std::abs(exact - approx);
            relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
            err=relerr*std::abs(exact);
            os << std::endl << "   x^" << n1 << "y^" << dg << "z^" << n3 << " | " << std::setw(3) << n1 + dg + n3 << " | "
               << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
            if (relerr > relTol) { os << " **** Error ****"; }
          }
          for (Number n2 = 0; n2 < dg; n2++)
          {
            Number n2bis=dg-n2;
            exact = exactIntegral(_hexahedron, dg, n2bis - 1, n3);
            approx = approximateIntegral(quad.quadratureRule, dg, n2bis - 1, n3);
            err = std::abs(exact - approx);
            relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
            err=relerr*std::abs(exact);
            os << std::endl << "   x^" << dg << "y^" << n2bis - 1 << "z^" << n3 << " | " << std::setw(3) << dg + n2bis - 1 + n3 << " | "
               << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
            if (relerr > relTol) { os << " **** Error ****"; }
          }
        }
        break;
      case _triangle:
        os << std::endl << std::endl << " Monomial | deg |        exact       |       approx       |      error";
        os << std::endl << " -----------------------------------------------------------------------------";
        for (Number n1 = 0; n1 <= dg; n1++)
        {
          Number n2 = dg - n1;
          exact = exactIntegral(_triangle, n1, n2);
          approx = approximateIntegral(quad.quadratureRule, n1, n2);
          err = std::abs(exact - approx);
          relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
          err=relerr*std::abs(exact);
          os << std::endl << "   x^" << n1 << "y^" << n2 << " | " << std::setw(3) << dg << " | "
             << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
          if (relerr > relTol) { os << " **** Error ****"; }
        }
        break;
      case _tetrahedron:
        os << std::endl << std::endl << " Monomial | deg |        exact       |       approx       |      error";
        os << std::endl << " -----------------------------------------------------------------------------";
        for (Number n1 = 0; n1 <= dg; n1++)
        {
          for (Number n2 = 0; n2 <= dg - n1; n2++)
          {
            Number n3 = dg - n1 - n2;
            exact = exactIntegral(_tetrahedron, n1, n2, n3);
            approx = approximateIntegral(quad.quadratureRule, n1, n2, n3);
            err = std::abs(exact - approx);
            relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
            err=relerr*std::abs(exact);
            os << std::endl << "   x^" << n1 << "y^" << n2 << "z^" << n3 << " | " << std::setw(3) << dg << " | "
               << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
            if (relerr > relTol) { os << " **** Error ****"; }
          }
        }
        break;
      case _prism:
        os << std::endl << std::endl << " Monomial | deg |        exact       |       approx       |      error";
        os << std::endl << " -----------------------------------------------------------------------------";
        for (Number n3 = 0; n3 <= dg; n3++)
        {
          for (Number n1 = 0; n1 <= dg - n3; n1++)
          {
            Number n2 = dg - n1 - n3;
            exact = exactIntegral(_prism, n1, n2, n3);
            approx = approximateIntegral(quad.quadratureRule, n1, n2, n3);    
            err = std::abs(exact - approx);
            relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
            err=relerr*std::abs(exact);
            os << std::endl << "   x^" << n1 << "y^" << n2 << "z^" << n3 << " | " << std::setw(3) << n1+n2+n3 << " | "
               << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
            if (relerr > relTol) { os << " **** Error ****"; }
          }
        }
        break;
      case _pyramid:
        os << std::endl << std::endl << " Monomial | deg |        exact       |       approx       |      error";
        os << std::endl << " -----------------------------------------------------------------------------";
        for (Number n3 = 0; n3 <= dg; n3++)
        {
          for (Number n1 = 0; n1 <= dg-n3; n1++)
          {
            Number n2 = dg - n1 -n3;
            exact = exactIntegral(_pyramid, n1, n2, n3);
            approx = approximateIntegral(quad.quadratureRule, n1, n2, n3);
            err = std::abs(exact - approx);
            relerr = roundToZero(std::abs(exact - approx)/std::abs(exact), relTol);
            err=relerr*std::abs(exact);
            os << std::endl << "   x^" << n1 << "y^" << n2 << "z^" << n3 << " | " << std::setw(3) << n1+n2+n3 << " | "
               << std::setw(18) << exact << " | " << std::setw(18) << approx << " | " << std::setw(18) << err;
            if (relerr > relTol) { os << " **** Error ****"; }
          } 
        }
      default:
        break;
    }
  }
  os << std::endl;
}

void checkRule(const Quadrature& quad, String fname, Real relTol=0.01)
{
  std::ofstream fout(inputsPathTo(fname).c_str());
  fout.precision(testPrec);
  checkRule( quad, fout, relTol);
}

Real a=1;
Number nbf=0;

Complex f(Real x)
{
  return Complex(std::exp(-a*x),0.);
}

Complex df(Real x)
{
  return Complex(-a*std::exp(-a*x),0.);
}

Complex ex(Real x, Real T)
{
  return (1.-std::exp(-(a+i_*x)*T))/(a+i_*x);
}

Real fp(Real x, Parameters& pa=defaultParameters)
{
  Number p = pa(1);
  nbf+=1;
  return std::pow(x,p);
}

Real gp(Real x)
{
  nbf+=1;
  return exp(-x)*sin(x);
}

void Error(Number ord, Real T)
{
  Real esup=0., el2=0., e;
  String file="erreur"+tostring(ord)+".dat";
  std::ofstream fout(file.c_str());
  fout.precision(testPrec);
  for (Number n=10; n<2000; n+=10)
  {
    FilonIM fimn(ord, n, T, f, df);
    esup=0.; el2=0.;
    for (Real x=0; x<=1; x+=0.01)
    {
      Complex y=fimn(x), ye=ex(x,T);
      e=std::abs(y-ye);
      esup=std::max(esup,e);
      el2+=e*e;
    }
    fout << T/n << " " << esup << " " << std::sqrt(T*el2/n) << eol;
  }
  fout.close();
}

// Vector<Complex> roundToZeroC(const Vector<Complex>& v, Real asZero = 10*theEpsilon)
// {
//    // Number N=v.size();
//    Vector<Complex> V(v);
//    Real eps=1e-10;
//    for (Number i=0; i!=v.size(); i++) 
//    {
// //    V[i]=Complex(roundToZero(real(v[i]),eps), roundToZero(imag(v[i]),eps));//       if (std::abs(realPart(v[i])) < asZero)
//       {
//         V[i]=Complex(0., imag(v[i]) );
//       }
//       if (std::abs(imag(v[i])) < asZero)
//       {
//         V[i]=Complex(real(V[i]),  0.);
//       }
//    }
//    return V;
// }

//! main quadrature test
void unit_Quadrature(int argc, char* argv[], bool check)
{
  String rootname = "unit_Quadrature";
  trace_p->push(rootname);
  std::stringstream os;
  os.precision(testPrec);
  
  Number nbErrors = 0;
  String errors;
  Real tol=0.0001;

  bool pr = true;
  Quadrature* quad = 0;

  os << "\n ============================= Segment ===============================\n";
  
  os << "\ntest Gauss-Legendre on segment :";
  for (Number i = 1; i < 21; i += 2)
  {
    quad = findQuadrature(_segment, _GaussLegendreRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLegendreSegment.in" , errors, "\ntest Gauss-Legendre on segment :", check);

  os << "\ntest Gauss-Lobatto on segment :";
  for (Number i = 1; i < 21; i += 2)
  {
    quad = findQuadrature(_segment, _GaussLobattoRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLobattoSegment.in" , errors, "\ntest Gauss-Lobatto on segment :", check);

  os << "\ntest nodal rule on segment :" ;
  for (Number i = 1; i < 5; i++)
  {
    quad = findQuadrature(_segment, _nodalRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/NodalRuleSegment.in" , errors, "\ntest Nodal rule on segment :", check);

  os << "\n ============================= Triangle ===============================\n";

  os << "\ntest miscellaneous rules on triangle :";
  for (Number i = 1; i < 7; i++)
  {
    quad = findQuadrature(_triangle, _miscRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/MiscellaneousRulesTriangle.in" , errors, "\ntest miscellaneous rules on triangle :", check);

  os << "\ntest nodal rules on triangle :" ;
  for (Number i = 1; i < 4; i++)
  {
    quad = findQuadrature(_triangle, _nodalRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/NodalRuleTriangle.in" , errors, "\ntest Nodal rule on triangle :", check);

  os << "\ntest Grundmann-Moller rules on triangle :" ;
  for (Number i = 1; i < 13; i += 2)
  {
    quad = findQuadrature(_triangle, _GrundmannMollerRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GrundmannMollerRulesTriangle.in" , errors, "\ntest Grundmann-Moller rules on triangle :", check);

  os << "\ntest Gauss-Legendre rules on triangle :" ;
  for (Number i = 1; i < 13; i += 2)
  {
    quad = findQuadrature(_triangle, _GaussLegendreRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLegendreTriangle.in" , errors, "\ntest Gauss-Legendre rule on triangle :", check);

  os << "\ntest symmetrical Gauss rules on triangle :" ;
  for (Number i = 1; i < 21; i ++)
  {
    quad = findQuadrature(_triangle, _symmetricalGaussRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/SymmetricalGaussTriangle.in" , errors, "\ntest symmetrical Gauss rule on triangle :", check);

  os << "\n ============================= Quadrangle ===============================\n";

  os << "\ntest nodal rules on quadrangle :" ;
  for (Number i = 1; i < 5; i++)
  {
    quad = findQuadrature(_quadrangle, _nodalRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/NodalRulesQuadrangle.in" , errors, "\ntest Nodal rules quadrangle :", check);

  os << "\ntest Gauss-Legendre rules on quadrangle :" ;
  for (Number i = 1; i < 9; i += 2)
  {
    quad = findQuadrature(_quadrangle, _GaussLegendreRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLegendreQuadrangle.in" , errors, "\ntest Gauss Legendre quadrangle :", check);

  os << "test Gauss-Lobatto rules on quadrangle :" ;
  for (Number i = 1; i < 9; i += 2)
  {
    quad = findQuadrature(_quadrangle, _GaussLobattoRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLobattoQuadrangle.in" , errors, "\ntest Gauss Lobatto quadrangle :", check);

  os << "\ntest symmetrical Gauss rules on quadrangle :" ;
  for (Number i = 1; i < 21; i +=2)
  {
    quad = findQuadrature(_quadrangle, _symmetricalGaussRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/SymmetricalGaussQuadrangle.in" , errors, "\ntest Symmetrical Gauss quadrangle :", check);

  //!test fft
  os << "\n ============================= Tetrahedron ===============================\n";
  
  os << "\ntest nodal rules on tetrahedron :" ;
  for (Number i = 1; i < 4; i += 2)
  {
    quad = findQuadrature(_tetrahedron, _nodalRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/NodalRulesTetrahedron.in", errors, "\ntest Nodal rules tetrahedron :", check);

  os << "\ntest miscellaneous rules on tetrahedron :" ;
  for (Number i = 1; i < 6; i++)
  {
    quad = findQuadrature(_tetrahedron, _miscRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/MiscelleanousRulesTetrahedron.in", errors, "\ntest Miscelleanous rules tetrahedron :", check);

  os << "\ntest Grundmann-Moller rules on tetrahedron :" ;
  for (Number i = 1; i < 13; i += 2)
  {
    quad = findQuadrature(_tetrahedron, _GrundmannMollerRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GrundmannMollerRulesTetrahedron.in", errors, "\ntest Grundmann Moller rules tetrahedron :", check);

  os << "\ntest Gauss-Legendre rules on tetrahedron :" ;
  for (Number i = 1; i < 13; i += 2)
  {
    quad = findQuadrature(_tetrahedron, _GaussLegendreRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLegendreTetrahedron.in", errors, "\ntest Gauss Legendre tetrahedron :", check);

  os << "\ntest symmetrical Gauss rules on tetrahedron :" ;
  for (Number i = 1; i < 11; i ++)
  {
    quad = findQuadrature(_tetrahedron, _symmetricalGaussRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/SymmetricalGaussTetrahedron.in", errors, "\ntest Symmetrical Gauss  tetrahedron :", check);

  os << "\n ============================= Hexahedron ===============================\n";

  os << "\ntest nodal rules on hexahedron :" ;
  for (Number i = 1; i < 5; i++)
  {
    quad = findQuadrature(_hexahedron, _nodalRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/NodalRulesHexahedron.in", errors, "\ntest Nodal rules hexahedron :", check);

  os << "\ntest Gauss-Legendre rules on hexahedron :" ;
  for (Number i = 1; i < 7; i += 2)
  {
    quad = findQuadrature(_hexahedron, _GaussLegendreRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLegendreHexahedron.in", errors, "\ntest Gauss Legendre hexahedron :", check);

  os << "\ntest Gauss-Lobatto rules on hexahedron :" ;
  for (Number i = 1; i < 7; i += 2)
  {
    quad = findQuadrature(_hexahedron, _GaussLobattoRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/GaussLobattoHexahedron.in", errors, "\ntest Gauss Lobatto hexahedron :", check);

  os << "\ntest symmetrical Gauss rules on hexahedron :" ;
  for (Number i = 1; i < 12; i +=2)
  {
    quad = findQuadrature(_hexahedron, _symmetricalGaussRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/SymmetricalGaussexahedron.in", errors, "\ntest Symmetrical Gauss hexahedron :", check);


  os << "\n ============================= Prism ===============================\n";
  os << "\ntest nodal rules on prism :" ;
  for (Number i = 1; i < 2; i += 2)
  {
    quad = findQuadrature(_prism, _nodalRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/NodalRulesPrism.in", errors, "\ntest Nodal rules prism :", check);

  os << "\ntest miscellaneous rules on prism :" ;
  for (Number i = 1; i < 6; i += 2)
  {
    quad = findQuadrature(_prism, _miscRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/MiscellaneousRulesPrism.in", errors, "\ntest Miscellaneous rules prism :", check);

  os << "\ntest symmetrical Gauss rules on prism :" ;
  for (Number i = 1; i < 11; i ++)
  {
    quad = findQuadrature(_prism, _symmetricalGaussRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/SymmetricalGaussPrism.in", errors, "\ntest Symmetrical Gauss rules prism :", check);

  os << "\n ============================= Pyramid ===============================\n";
  os << "\ntest nodal rules on pyramid :" ;
  quad = findQuadrature(_pyramid, _nodalRule, 1, pr);
  os << *quad;
  checkRule(*quad, os);
  nbErrors += checkValue(os, rootname+"/NodalRulesPyramid.in", errors, "\ntest Nodal rules pyramid :", check);

  os << "\ntest miscellaneous rules on pyramid :" ;
  quad = findQuadrature(_pyramid, _miscRule, 1, pr);
  os << *quad;
  checkRule(*quad, os);
  nbErrors += checkValue(os, rootname+"/MiscellaneousRulesPyramid1.in", errors, "\ntest Miscellaneous 1 rules pyramid :", check);
  quad = findQuadrature(_pyramid, _miscRule, 7, pr);
  os << *quad;
  checkRule(*quad, os);
  nbErrors += checkValue(os, rootname+"/MiscellaneousRulesPyramid7.in", errors, "\ntest Miscellaneous 7 rules pyramid :", check);

  os << "\ntest conical Legendre-Gauss rules on pyramid :" ;
  for (Number i = 1; i < 12; i +=2)
  {
    quad = findQuadrature(_pyramid, _GaussLegendreRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/ConicalLegendreGaussPyramid.in", errors, "\ntest conical Legendre-Gauss rules pyramid :", check);

  os << "\ntest symmetrical Gauss rules on pyramid :" ;
  for (Number i = 1; i < 11; i ++)
  {
    quad = findQuadrature(_pyramid, _symmetricalGaussRule, i, pr);
    os << *quad;
    checkRule(*quad, os);
  }
  nbErrors += checkValue(os, rootname+"/SymmetricalGaussRulesPyramid.in", errors, "\ntest Symmetrical Gauss rules pyramid :", check);

  //! test other quadrature Filon-FFT-Trapeze
  //! ---------------------------------------
  Real T=10;
  for (Number o=0; o<3; o++)
  {
    FilonIM fim(o,1000,T,f,df);
    fim.print(os);
    for (Real x=0; x<=0.1; x+=0.01)
    {
      os << "\n---------- Filon test ------------- " << eol;
      Complex y=fim(x), ye=ex(x,T);
      os << "- " << roundToZero(x) << " "
         << roundToZero(y.real()) << " " << roundToZero(y.imag()) << " "
         << roundToZero(ye.real()) << " " << roundToZero(ye.imag()) << " error = " << std::abs(y-ye) << eol;
    }
    nbErrors += checkValue(os, rootname+"/Filon_"+tostring(o)+".in", errors, "\ntest Filon "+tostring(o), check);
  }

  //! test fft
  os << "---------- FFT test ----------" << eol;

  Number ln=6, n=64;
  Vector<Complex> f(n),g(n),f2(n);
  Real tolfft=1.0e-09;
  for (Number i=0; i<n; i++) f[i]=roundToZero(std::sin(2*i*pi_/(n-1)),tolfft);
  fft(f.begin(),g.begin(),ln);
  os << "  fft(f)=" << f << eol << "  g=" << roundToZero(g, tolfft) << eol;
  ifft(g.begin(),f2.begin(),ln);
  os << "  ifft(g)=" << roundToZero(f2, tolfft) << eol;
  os << "  ||f - f2)||_2 = " << roundToZero(norm2(f-f2),tolfft) << eol;
  fft(f,g);
  os << "  fft(f,g)=" << roundToZero(g, tolfft) << eol;
  ifft(g,f2);
  os << "  ifft(g,f2)=" << roundToZero(f2,tolfft) << eol;
  ifft(fft(f,g),f2);
  os << "  ||f - ifft(fft(f,g),f2))||_2 = " << roundToZero(norm2(f-f2),tolfft) << eol;
  os << "  ||f - ifft(fft(f))||_2 = " << roundToZero(norm2(ifft(fft(f))),tolfft);
  nbErrors += checkValue(os, rootname+"/FFT.in", errors, "\ntest FFT test :", check);

  //! test rectangle method
  os << eol << "---------- Rectangle test ----------" << eol;

  Number np=20;
  Parameters pars(1,"p");
  os << "  rectangle intg_[0,1] x   -> error = " << std::abs(0.5-rectangle(fp, pars, 0., 1., np)) << eol;
  pars("p")=2;
  os << "  rectangle intg_[0,1] x^2 -> error = " << std::abs(1./3-rectangle(fp, pars, 0., 1., np)) << eol;
  pars("p")=3;
  os << "  rectangle intg_[0,1] x^3 -> error = " << std::abs(0.25-rectangle(fp, pars, 0., 1., np)) << eol;
  pars("p")=4;
  os << "  rectangle intg_[0,1] x^4 -> error = " << std::abs(0.20-rectangle(fp, pars, 0., 1., np)) << eol;

  Vector<Real> vf(np+1); Real h=1./np, intg=0.;
  pars("p")=1;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  rectangle intg_[0,1] x -> " << rectangle(np, h, vf.begin(), intg) << eol;
  pars("p")=2; intg=0.;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  rectangle intg_[0,1] x^2 -> " << rectangle(np, h, vf.begin(), intg) << eol;
  pars("p")=3; intg=0.;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  rectangle intg_[0,1] x^3 -> " << rectangle(np, h, vf.begin(), intg) << eol;
  os << "  rectangle intg_[0,1] x^3 -> " << rectangle(vf,h) << eol;
  nbErrors += checkValue(os, rootname+"/Rectangle.in", errors, "\ntest Rectangle test :", check);

  //! test trapeze method
  os << "---------- Trapeze test ----------" << eol;

  pars("p")=1;
  os << "  trapeze intg_[0,1] x   -> error = " << std::abs(0.5-trapz(fp, pars, 0., 1., np)) << eol;
  pars("p")=2;
  os << "  trapeze intg_[0,1] x^2 -> error = " << std::abs(1./3-trapz(fp, pars, 0., 1., np)) << eol;
  pars("p")=3;
  os << "  trapeze intg_[0,1] x^3 -> error = " << std::abs(0.25-trapz(fp, pars, 0., 1., np)) << eol;
  pars("p")=4;
  os << "  trapeze intg_[0,1] x^4 -> error = " << std::abs(0.20-trapz(fp, pars, 0., 1., np)) << eol;

  vf.resize(np+1); intg=0.;
  pars("p")=1;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  trapeze intg_[0,1] x -> " << trapz(np+1, h, vf.begin(), intg) << eol;
  pars("p")=2; intg=0.;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  trapeze intg_[0,1] x^2 -> " << trapz(np+1, h, vf.begin(), intg) << eol;
  pars("p")=3; intg=0.;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  trapeze intg_[0,1] x^3 -> " << trapz(np+1, h, vf.begin(), intg) << eol;
  os << "  trapeze intg_[0,1] x^3 -> " << trapz(vf,h) << eol;
  nbErrors += checkValue(os, rootname+"/Trapeze.in", errors, "\ntest Trapeze test :", check);

  //! test simpson method
  os << "---------- Simpson test ----------" << eol;

  pars("p")=1;
  os << "  Simpson intg_[0,1] x   -> error = " << std::abs(0.5-simpson(fp, pars, 0., 1., np)) << eol;
  pars("p")=2;
  os << "  Simpson intg_[0,1] x^2 -> error = " << std::abs(1./3-simpson(fp, pars, 0., 1., np)) << eol;
  pars("p")=3;
  os << "  Simpson intg_[0,1] x^3 -> error = " << std::abs(0.25-simpson(fp, pars, 0., 1., np)) << eol;
  pars("p")=4;
  os << "  Simpson intg_[0,1] x^4 -> error = " << std::abs(0.20-simpson(fp, pars, 0., 1., np)) << eol;

  intg=0.;
  pars("p")=1;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  Simpson intg_[0,1] x -> " << simpson(np+1, h, vf.begin(), intg) << eol;
  pars("p")=2; intg=0.;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  Simpson intg_[0,1] x^2 -> " << simpson(np+1, h, vf.begin(), intg) << eol;
  pars("p")=3; intg=0.;
  for (Number k=0; k<=np; k++) vf[k]=fp(k*h,pars);
  os << "  Simpson intg_[0,1] x^3 -> " << simpson(np+1, h, vf.begin(), intg) << eol;
  os << "  Simpson intg_[0,1] x^3 -> " << simpson(vf,h) << eol;
  nbErrors += checkValue(os, rootname+"/Simpson.in", errors, "\ntest Simpson test :", check);

  //! test adaptive trapeze method
  os << "---------- adaptive trapeze test ----------" << eol;

  Real era;
  pars("p")=1;nbf=0; era=std::abs(0.5 -adaptiveTrapz(fp, pars, 0., 1., 1E-4));
  os << "  adaptive trapeze intg_[0,1] x   -> error = " << era << ", number of f values = " << nbf << eol;
  pars("p")=2;nbf=0;era=std::abs(1./3-adaptiveTrapz(fp, pars, 0., 1., 1E-4));
  os << "  adaptive trapeze intg_[0,1] x^2 -> error = " << era << ", number of f values = " << nbf << eol;
  pars("p")=3;nbf=0;era=std::abs(0.25-adaptiveTrapz(fp, pars, 0., 1., 1E-4));
  os << "  adaptive trapeze intg_[0,1] x^3 -> error = " << era << ", number of f values = " << nbf << eol;
  pars("p")=4;nbf=0;era=std::abs(0.20-adaptiveTrapz(fp, pars, 0., 1., 1E-4)),
  os << "  adaptive trapeze intg_[0,1] x^4 -> error = " << era << ", number of f values = " << nbf << eol;
  nbf=0;era=std::abs(0.5*(1-exp(-2*pi_))-adaptiveTrapz(gp, 0., 2*pi_,1E-4));
  os << "  adaptive trapeze intg_[0,2pi] exp(-x)sin(x) -> error = " << era << ", number of f values = " << nbf << eol;
  nbErrors += checkValue(os, rootname+"/AdaptativeTrapeze.in", errors, "\ntest adaptative Trapeze Method :", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
