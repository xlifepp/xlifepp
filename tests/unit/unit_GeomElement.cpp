/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
\file unit_GeomElement.cpp
\author E. Lunéville
\since 05 apr 2012
\date  05 may 2012

Low level tests of GeomElement class and related classes.
Almost functionalities are checked.
This function may either creates a reference file storing the results (check=true)
or compares results to those stored in the reference file (check=true)
It returns reporting informations in a string
*/

//===============================================================================
//library dependances
#include "xlife++-libs.h"
#include "testUtils.hpp"

//===============================================================================
//stl dependances
#include <iostream>
#include <fstream>
#include <vector>

//===============================================================================

using namespace xlifepp;

namespace unit_GeomElement {

void unit_GeomElement(int argc, char* argv[], bool check)
{
  String rootname = "unit_GeomElement";
  trace_p->push(rootname);
  verboseLevel(3);
  
  String errors;
  Number nbErrors = 0;

  //!get interpolation structure
  Interpolation* interp_p = findInterpolation(Lagrange, standard, 1, H1);
  Interpolation* interp2_p = findInterpolation(Lagrange, standard, 2, H1);
  Number numelt = 1;
  
  //!create a segment with node 2 1
  theCout << "------------------ create plain elements -------------------------------\n";
  RefElement* ref_p = findRefElement(_segment, interp_p); //find reference element Lagrange P1
  GeomElement S1(0, ref_p, 1, numelt);
  S1.meshElement()->nodes[0] = new Point(0.);
  S1.meshElement()->nodes[1] = new Point(1.);
  S1.meshElement()->nodeNumbers[0] = 2; S1.meshElement()->nodeNumbers[1] = 1;
  theCout << "Element S1 : " << S1 << std::endl;
  numelt++;
  
  //!create a triangle with node 3 2 1
  ref_p = findRefElement(_triangle, interp_p); //find reference element Lagrange P1
  GeomElement T1(0, ref_p, 2, numelt);
  T1.meshElement()->nodes[0] = new Point(0., 0.);
  T1.meshElement()->nodes[1] = new Point(2., 0.);
  T1.meshElement()->nodes[2] = new Point(0., 2.);
  T1.meshElement()->nodeNumbers[0] = 3; T1.meshElement()->nodeNumbers[1] = 2; T1.meshElement()->nodeNumbers[2] = 1;
  T1.meshElement()->vertexNumbers = T1.meshElement()->nodeNumbers;
  theCout << "Element T1 : " << T1 << std::endl;
  numelt++;
  
  //!create a P2 triangle with node 1 2 3 4 5 6
  ref_p = findRefElement(_triangle, interp2_p); //find reference element Lagrange P1
  GeomElement T2(0, ref_p, 2, numelt);
  T2.meshElement()->nodes[0] = new Point(0., 0.);
  T2.meshElement()->nodes[1] = new Point(2., 0.);
  T2.meshElement()->nodes[2] = new Point(0., 2.);
  T2.meshElement()->nodes[3] = new Point(1., 0.);
  T2.meshElement()->nodes[4] = new Point(1., 1.);
  T2.meshElement()->nodes[5] = new Point(0., 1.);
  T2.meshElement()->nodeNumbers[0] = 1; T2.meshElement()->nodeNumbers[1] = 2; T2.meshElement()->nodeNumbers[2] = 3;
  T2.meshElement()->nodeNumbers[3] = 4; T2.meshElement()->nodeNumbers[4] = 5; T2.meshElement()->nodeNumbers[5] = 6;
  for (Number i = 0; i < T2.numberOfVertices(); i++)
  { T2.meshElement()->vertexNumbers[i] = T2.meshElement()->nodeNumbers[i]; }
  theCout << "Element T2 : " << T2 << std::endl;
  numelt++;

  nbErrors+=checkValue(theCout, rootname+"/PlainElements.in", errors, " Test PlainElements", check);

  //!create side elements
  theCout << "------------------- create side elements -------------------------------\n";
  GeomElement S1_1(&S1, 1, numelt);
  theCout << "Side element S1_1 : " << S1_1 << std::endl;
  numelt++;
  GeomElement T1_2(&T1, 2, numelt);
  theCout << "Side element T1_2 : " << T1_2 << std::endl;
  numelt++;
  GeomElement T2_3(&T2, 3, numelt);
  theCout << "Side element T2_3 : " << T2_3 << std::endl;
  numelt++;

  nbErrors+=checkValue(theCout, rootname+"/SideElements.in", errors, " Test SideElements", check);

  //!test GeomElement accessors for a plain element
  theCout << "------------ test accessors of GeomElement for plain elements -----------\n";
  theCout << "Element T1->meshElement() : " << (*T1.meshElement()) << "\n";
  theCout << "Element T1->elementDim() : " << T1.elementDim() << "\n";
  theCout << "Element T1->refElement() : " << (*T1.refElement()) << "\n";
  theCout << "Element T1->geomRefElement() : " << (*T1.geomRefElement()) << "\n";
  theCout << "Element T1->shapeType() : " << words("shape", T1.shapeType()) << "\n";
  theCout << "Element T1->numberOfVertices(), numberOfSides(), numberOfSideofSides() : "
          << T1.numberOfVertices() << ", " << T1.numberOfSides() << ", " << T1.numberOfSideOfSides();
  theCout << "\nList of vertex numbers : ";
  for (Number i = 1; i <= T1.numberOfVertices(); i++) { theCout << T1.vertexNumber(i) << " "; }
  theCout << "\nList of side shapes : ";
  for (Number i = 1; i <= T1.numberOfSides(); i++) { theCout << i << "->" << words("shape", T1.shapeType(i)) << "  "; }
  //!test specific MeshElement accessors for a plain element
  theCout << "\nList of node numbers : ";
  for (Number i = 0; i < T1.meshElement()->numberOfNodes(); i++) { theCout << T1.meshElement()->nodeNumbers[i] << " "; }
  theCout << "\n";
  nbErrors+=checkValue(theCout, rootname+"/AccessGEPlainElements.in", errors, " Test AccessGEPlainElements", check);

  //!test GeomElement accessors for a side element
  theCout << "------------ test accessors of GeomElement for side elements -----------\n";
  theCout << "Element T1_2->parent() : " << (*T1_2.parent()) << "\n";
  GeoNumPair gnp = T1_2.parentSide(0);
  theCout << "Element T1_2->parentSide(0).second : " << gnp.second;
  theCout << " Element T1_2->elementDim() : " << T1_2.elementDim() << "\n";
  theCout << "Element T1_2->refElement() : " << (*T1_2.refElement()) << "\n";
  theCout << "Element T1_2->geomRefElement() : " << (*T1_2.geomRefElement()) << "\n";
  theCout << "Element T1_2->shapeType() : " << words("shape", T1_2.shapeType()) << "\n";
  theCout << "Element T1_2->numberOfVertices(), numberOfSides(), numberOfSideofSides() : "
      << T1_2.numberOfVertices() << ", " << T1_2.numberOfSides() << ", " << T1_2.numberOfSideOfSides();
  theCout << "\nList of vertex numbers : ";
  for (Number i = 1; i <= T1_2.numberOfVertices(); i++) { theCout << T1_2.vertexNumber(i) << " "; }
  theCout << "\n";
  
  theCout << "Element T2_3->parent() : " << (*T2_3.parent()) << "\n";
  gnp = T2_3.parentSide(0);
  theCout << "Element T2_3->parentSide(0).second : " << gnp.second;
  theCout << " Element T2_3->elementDim() : " << T2_3.elementDim() << "\n";
  theCout << "Element T2_3->refElement() : " << (*T2_3.refElement()) << "\n";
  theCout << "Element T2_3->geomRefElement() : " << (*T2_3.geomRefElement()) << "\n";
  theCout << "Element T2_3->shapeType() : " << words("shape", T2_3.shapeType()) << "\n";
  theCout << "Element T2_3->numberOfVertices(), numberOfSides(), numberOfSideofSides() : "
      << T2_3.numberOfVertices() << ", " << T2_3.numberOfSides() << ", " << T2_3.numberOfSideOfSides() << "\n";
  theCout << "List of vertex numbers : ";
  for(Number i = 1; i <= T2_3.numberOfVertices(); i++) { theCout << T2_3.vertexNumber(i) << " "; }
  theCout << "\n";
  nbErrors+=checkValue(theCout, rootname+"/AccessGESideElements.in", errors, " Test AccessGESideElements", check);

  //!test construction of MeshElement for a side element
  theCout << "------------ test buildSideElement for side elements P1 and P2 -----------\n";
  T1_2.buildSideMeshElement();
  theCout << "Element T1_2 : " << T1_2 << "\n";
  T2_3.buildSideMeshElement();
  theCout << "Element T2_3 : " << T2_3 << "\n";
  
  nbErrors+=checkValue(theCout, rootname+"/BuildSideElementForSideElementsP1andP2.in", errors, " Test ABuildSideElementForSideElementsP1andP2", check);

  //!test of MeshElement functions
  MeshElement* melt = T1.meshElement();
  theCout << "\n------------ test MeshElement functions P1 -----------\n";
  theCout << "melt->index()=" << melt->index() << " melt->spaceDim()=" << melt->spaceDim();
  theCout << " melt->shapeType()= " << words("shape", melt->shapeType()) << " melt->numberOfNodes()= " << melt->numberOfNodes();
  theCout << " melt->numberOfSides()= " << melt->numberOfSides() << " melt->numberOfSideOfSides() : " << melt->numberOfSideOfSides() << "\n";
  theCout << "elt.refElement()=" << (*melt->refElement()) << "\n";
  theCout << "melt->geomRefElement() : " << (*melt->geomRefElement()) << "\n";
  theCout << "List of vertex numbers : ";
  for(Number i = 1; i <= melt->numberOfVertices(); i++) { theCout << melt->vertexNumber(i) << " "; }
  for(Number s = 1; s <= melt->numberOfSides(); s++)
  {
    theCout << "\nside " << s << " : melt->shapeType(s) = " << words("shape", melt->shapeType(s));
    theCout << " vertex numbers : ";
    for(Number i = 0; i < melt->verticesNumbers(s).size(); i++) { theCout << melt->verticesNumbers(s)[i] << " "; }
    theCout << "\nmelt->refElement(s) : " << (*melt->refElement(s)) << "\n";
    theCout << "melt->geomRefElement()(s) : " << (*melt->geomRefElement(s));
  }
  melt->computeMeasures();
  theCout << "\nmelt->computeMeasures() =";
  for(Number i = 0; i <= melt->numberOfSides(); i++) { theCout << " " << melt->measure(i); }
  melt->computeOrientation();
  theCout << "\nmelt->computeOrientation() = " << melt->orientation;
   
  nbErrors+=checkValue(theCout, rootname+"/MeshElementFunctionsP1.in", errors, " Test MeshElementFunctionsP1", check);

  melt = T2.meshElement();
  theCout << "\n------------ test MeshElement functions P2 -----------\n";
  theCout << "melt->index()=" << melt->index() << " melt->spaceDim()=" << melt->spaceDim();
  theCout << " melt->shapeType()= " << words("shape", melt->shapeType()) << " melt->numberOfNodes()= " << melt->numberOfNodes();
  theCout << " melt->numberOfSides()= " << melt->numberOfSides() << " melt->numberOfSideOfSides() : " << melt->numberOfSideOfSides() << "\n";
  theCout << "elt.refElement()=" << (*melt->refElement()) << "\n";
  theCout << "melt->geomRefElement() : " << (*melt->geomRefElement()) << "\n";
  theCout << "List of vertex numbers : ";
  for(Number i = 1; i <= melt->numberOfVertices(); i++) { theCout << melt->vertexNumber(i) << " "; }
  for(Number s = 1; s <= melt->numberOfSides(); s++)
  {
    theCout << "\nside " << s << " : melt->shapeType(s) = " << words("shape", melt->shapeType(s));
    theCout << " vertex numbers : ";
    for(Number i = 0; i < melt->verticesNumbers(s).size(); i++) { theCout << melt->verticesNumbers(s)[i] << " "; }
    theCout << "\nmelt->refElement(s) : " << (*melt->refElement(s)) << "\n";
    theCout << "melt->geomRefElement()(s) : " << (*melt->geomRefElement(s));
  }
  melt->computeMeasures();
  theCout << "\nmelt->computeMeasures() =";
  for(Number i = 0; i <= melt->numberOfSides(); i++) { theCout << " " << melt->measure(i); }
  melt->computeOrientation();
  theCout << "\nmelt->computeOrientation() = " << melt->orientation;
  nbErrors+=checkValue(theCout, rootname+"/MeshElementFunctionsP2.in", errors, " Test MeshElementFunctionsP2", check);

  
  theCout << "\n------------ test GeomMapData functions P1-----------\n";
  GeomMapData mapdata(T1.meshElement());
  theCout << "mapdata.geomMap(Point(0.,0.)) = " << mapdata.geomMap(Point(0., 0.));
  theCout << "\nmapdata.geomMap(Point(1.,0.)) = " << mapdata.geomMap(Point(1., 0.));
  theCout << "\nmapdata.geomMap(Point(0.,1.)) = " << mapdata.geomMap(Point(0., 1.));
  theCout << "\nmapdata.geomMap(Point(1./3,1./3)) = " << mapdata.geomMap(Point(1. / 3, 1. / 3));
  mapdata.computeJacobianMatrix(Point(0., 0.));
  theCout << "\nmapdata.computeJacobianMatrix(Point(0,0)) = " << mapdata.jacobianMatrix;
  theCout << " determinant = " << mapdata.computeJacobianDeterminant();
  mapdata.invertJacobianMatrix();
  theCout << "\nmapdata.invertJacobianMatrix() = " << mapdata.inverseJacobianMatrix;
  mapdata.computeJacobianMatrix(Point(0., 0.), 1);
  theCout << "\nmapdata.computeJacobianMatrix(Point(0,0),1) = " << mapdata.jacobianMatrix;
  
  nbErrors+=checkValue(theCout, rootname+"/GeomMapDataFunctionsP1.in", errors, " Test GeomMapDataFunctionsP1", check);


  theCout << "\n------------ test GeomMapData functions P2-----------\n";
  mapdata = GeomMapData(T2.meshElement());
  theCout << "mapdata.geomMap(Point(0.,0.)) = " << mapdata.geomMap(Point(0., 0.));
  theCout << "\nmapdata.geomMap(Point(1.,0.)) = " << mapdata.geomMap(Point(1., 0.));
  theCout << "\nmapdata.geomMap(Point(0.,1.)) = " << mapdata.geomMap(Point(0., 1.));
  theCout << "\nmapdata.geomMap(Point(1./3,1./3)) = " << mapdata.geomMap(Point(1. / 3, 1. / 3));
  mapdata.computeJacobianMatrix(Point(0., 0.));
  theCout << "\nmapdata.computeJacobianMatrix(Point(0,0)) = " << mapdata.jacobianMatrix;
  theCout << " determinant = " << mapdata.computeJacobianDeterminant();
  mapdata.invertJacobianMatrix();
  theCout << "\nmapdata.invertJacobianMatrix() = " << mapdata.inverseJacobianMatrix;
  mapdata.computeJacobianMatrix(Point(0., 0.), 1);
  theCout << "\nmapdata.computeJacobianMatrix(Point(0,0),1) = " << mapdata.jacobianMatrix;
  
  nbErrors+=checkValue(theCout, rootname+"/GeomMapDataFunctionsP2.in", errors, " Test GeomMapDataFunctionsP2", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
