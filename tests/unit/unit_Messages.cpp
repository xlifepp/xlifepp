/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
	\file unit_Messages.cpp
	\author N. Kielbasiewicz
	\since  8 dec 2011
	\date 11 may 2012

	Low level tests of Messages class.
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <sstream>

using namespace xlifepp;

namespace unit_Messages {

void unit_Messages(int argc, char* argv[], bool check)
{
  String rootname="unit_Messages";
  trace_p->push(rootname);
  std::stringstream oss;
  oss.precision(testPrec);
  
  int nbErrors = 0;
  String errors("Messages ");
   
  
  //!-------------------------------------------------------------
  //! test of MsgData
  //!-------------------------------------------------------------
  
  std::stringstream ossMsgData;
  
  MsgData msgd;
  msgd << 1 << 2.5 << "toto" << Complex(1.5, 2.5) << true;
  msgd.push(4);
  msgd.push("tata");
  msgd.push(3);
  msgd.push(Complex(-1.0, 1.0));
  msgd.push(false);
  msgd.push(-2.6);
  
  bool isInt0 = false;
  bool isInt1 = false;
  bool isInt2 = false;
  bool isReal0 = false;
  bool isReal1 = false;
  bool isComplex0 = false;
  bool isComplex1 = false;
  bool isBoolean0 = false;
  bool isBoolean1 = false;
  bool isString0 = false;
  bool isString1 = false;
  bool isPrintInt = true;
  bool isPrintReal = true;
  bool isPrintComplex = true;
  bool isPrintString = true;
  bool isPrintBoolean = true;
  bool isPushInt = true;
  bool isPushReal = true;
  bool isPushComplex = true;
  bool isPushString = true;
  bool isPushBoolean = true;
  int  nbMsgDataErrors = 0;
  
  if(msgd.intParameter(0) == 1) {isInt0 = true;}
  else {nbMsgDataErrors++;}
  if(msgd.intParameter(1) == 4) {isInt1 = true;}
  else {nbMsgDataErrors++;}
  if(msgd.intParameter(2) == 3) {isInt2 = true;}
  else {nbMsgDataErrors++;}
  isPrintInt = isPrintInt && isInt0;
  isPushInt = isPushInt && isInt1 && isInt2;
  
  if(msgd.realParameter(0) == 2.5) {isReal0 = true;}
  else {nbMsgDataErrors++;}
  if(msgd.realParameter(1) == -2.6) {isReal1 = true;}
  else {nbMsgDataErrors++;}
  isPrintReal = isPrintReal && isReal0;
  isPushReal = isPushReal && isReal1;
  
  if(msgd.complexParameter(0) == Complex(1.5, 2.5)) {isComplex0 = true;}
  else {nbMsgDataErrors++;}
  if(msgd.complexParameter(1) == Complex(-1.0, 1.0)) {isComplex1 = true;}
  else {nbMsgDataErrors++;}
  isPrintComplex = isPrintComplex && isComplex0;
  isPushComplex = isPushComplex && isComplex1;
  
  if(msgd.stringParameter(0) == "toto") {isString0 = true;}
  else {nbMsgDataErrors++;}
  if(msgd.stringParameter(1) == "tata") {isString1 = true;}
  else {nbMsgDataErrors++;}
  isPrintString = isPrintString && isString0;
  isPushString = isPushString && isString1;
  
  if(msgd.booleanParameter(0) == true) {isBoolean0 = true;}
  else {nbMsgDataErrors++;}
  if(msgd.booleanParameter(1) == false) {isBoolean1 = true;}
  else {nbMsgDataErrors++;}
  isPrintBoolean = isPrintBoolean && isBoolean0;
  isPushBoolean = isPushBoolean && isBoolean1;
  
  if(!isInt0 || !isInt1 || !isInt2)
  {
    ossMsgData << "  - MsgData::intParameter ERROR -> (0,1=" << isInt0 << ") (1,4=" << isInt1 << ") (2,3=" << isInt2 << ")" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isReal0 || !isReal1)
  {
    ossMsgData << "  - MsgData::realParameter ERROR -> (0,2.5=" << isReal0 << ") (1,-2.6=" << isReal1 << ")" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isComplex0 || !isComplex1)
  {
    ossMsgData << "  - MsgData::complexParameter ERROR -> (0,1.5+2.5i=" << isComplex0 << ") (1,-1+1i=" << isComplex1 << ")" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isString0 || !isString1)
  {
    ossMsgData << "  - MsgData::stringParameter ERROR -> (0,toto=" << isString0 << ") (1,tata=" << isString1 << ")" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isBoolean0 || !isBoolean1)
  {
    ossMsgData << "  - MsgData::booleanParameter ERROR -> (0,true=" << isString0 << ") (1,false=" << isString1 << ")" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPrintInt)
  {
    ossMsgData << "  - MsgData friend operator<<(int) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPrintReal)
  {
    ossMsgData << "  - MsgData friend operator<<(real) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPrintComplex)
  {
    ossMsgData << "  - MsgData friend operator<<(complex) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPrintString)
  {
    ossMsgData << "  - MsgData friend operator<<(string) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPrintBoolean)
  {
    ossMsgData << "  - MsgData friend operator<<(bool) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPushInt)
  {
    ossMsgData << "  - MsgData::push(int) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPushReal)
  {
    ossMsgData << "  - MsgData::push(real) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPushComplex)
  {
    ossMsgData << "  - MsgData::push(complex) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPushString)
  {
    ossMsgData << "  - MsgData::push(string) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  if(!isPushBoolean)
  {
    ossMsgData << "  - MsgData::push(bool) ERROR" << std::endl;
    nbMsgDataErrors++;
  }
  
  oss << "---------- test of MsgData class ----------" << std::endl
      << "Errors : " << nbMsgDataErrors << std::endl << ossMsgData.str() << std::endl;
  nbErrors += nbMsgDataErrors;
  
  //!-------------------------------------------------------------
  //! test of MsgFormat
  //!-------------------------------------------------------------
  
  std::stringstream ossMsgFormat;
  
  MsgFormat msgf("toto", "msg_undef", _warning, false, true);
  
  bool isFormat = false;
  bool isStringId = false;
  bool isType = false;
  bool isStop = false;
  bool isConsole = false;
  int nbMsgFormatErrors = 0;
  
  if(msgf.format() == "toto") {isFormat = true;}
  else {nbMsgFormatErrors++;}
  if(msgf.stringId() == "msg_undef") {isStringId = true;}
  else {nbMsgFormatErrors++;}
  if(msgf.type() == _warning) {isType = true;}
  else {nbMsgFormatErrors++;}
  if(msgf.stop() == false) {isStop = true;}
  else {nbMsgFormatErrors++;}
  if(msgf.console() == true) {isConsole = true;}
  else {nbMsgFormatErrors++;}
  
  if(!isFormat)
  {
    ossMsgFormat << "MsgFormat::format() ERROR -> toto=" << msgf.format() << std::endl;
    nbMsgFormatErrors++;
  }
  
  if(!isStringId)
  {
    ossMsgFormat << "MsgFormat::stringId() ERROR -> msg_undef=" << msgf.stringId() << std::endl;
    nbMsgFormatErrors++;
  }
  
  if(!isType)
  {
    ossMsgFormat << "MsgFormat::type() ERROR -> 2=" << msgf.type() << std::endl;
    nbMsgFormatErrors++;
  }
  
  if(!isStop)
  {
    ossMsgFormat << "MsgFormat::stop() ERROR -> false=" << msgf.stop() << std::endl;
    nbMsgFormatErrors++;
  }
  
  if(!isConsole)
  {
    ossMsgFormat << "MsgFormat::console() ERROR -> true=" << msgf.console() << std::endl;
    nbMsgFormatErrors++;
  }
  
  if(!isFormat || !isStringId || !isType || !isStop || !isConsole)
  {
    ossMsgFormat << "MsgFormat::MsgFormat(...) ERROR" << std::endl;
    nbMsgFormatErrors++;
  }
  
  oss << "---------- test of MsgFormat class ----------" << std::endl
      << "Errors : " << nbMsgFormatErrors << std::endl << ossMsgFormat.str() << std::endl;
  nbErrors += nbMsgFormatErrors;
  //!-------------------------------------------------------------
  //! test of Messages
  //!-------------------------------------------------------------
  
  std::stringstream ossMessages;
  bool isPrintList = true;
  bool isFind = true;
  bool isAppend = true;
  int nbMessagesErrors = 0;
  
  const String msgType = "warning";
  const String msgFile = inputsPathTo(rootname+"/userInfo_en.in");
  String MU;
  if (check)
  {
    MU="msgUser.txt";
  }
  else
  {
    MU=inputsPathTo(rootname+"/msgUser.in");
  }
  std::ofstream ofs(MU.c_str());
  
  Messages* myMsg = new Messages(msgFile, ofs, msgType);
  
  myMsg->printList(ofs);
  
  std::ifstream ifs("msgUser.txt");
  std::ifstream ifsref(msgFile.c_str());
  String line, lineref;
  
  while(!ifsref.eof())
  {
    getline(ifsref, lineref);
    getline(ifs, line);
    if(line == lineref) {isPrintList = isPrintList && true;}
  }
  
  if (!isPrintList)
  {
    ossMessages << "Messages::printList(ofstream) ERROR" << std::endl;
    nbMessagesErrors++;
  }
  
  ofs.close();
    
  MsgFormat* msgf2 = myMsg->find("logon");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "logon")
    {
      isFind = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != "Log file \"%s\" activated.")
    {
      isFind = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 2)
    {
      isFind = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 0)
    {
      isFind = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isFind = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isFind = false;
    nbMessagesErrors++;
  }
  
  if (!isFind)
  {
    ossMessages << "Messages::find(string) ERROR" << std::endl;
  }

  myMsg->appendFromFile(inputsPathTo(rootname+"/interface_en.in"));
  myMsg->appendFromFile(inputsPathTo(rootname+"/internal_en.in"));
  myMsg->appendFromFile(inputsPathTo(rootname+"/fem_en.in"));
  myMsg->appendFromFile(inputsPathTo(rootname+"/term_en.in"));
  myMsg->appendFromFile(inputsPathTo(rootname+"/mesh_en.in"));
  myMsg->appendFromFile(inputsPathTo(rootname+"/solver_en.in"));
  
  msgf2 = myMsg->find("on_error");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "on_error")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != ":-( %s\n:-( Error, type %s \"%s\" (%i) (see file %s for more)\n:-( in %s.\n:-( %s")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 2)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isAppend = false;
    nbMessagesErrors++;
  }
  
  msgf2 = myMsg->find("invalidop");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "invalidop")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != "Invalid operation : %s")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isAppend = false;
    nbMessagesErrors++;
  }
  
  msgf2 = myMsg->find("failure+");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "failure+")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != "%sSolver fails to converge in less than %i iterations (residue = %r, epsilon = %r)\n Previous values of residue %r, %r, %r.")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isAppend = false;
    nbMessagesErrors++;
  }
  
  msgf2 = myMsg->find("bad_order");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "bad_order")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != "Bad mesh element order %i in %s constructor.")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isAppend = false;
    nbMessagesErrors++;
  }
  
  msgf2 = myMsg->find("msg_undef");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "msg_undef")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != "Message of type \"%s\" undefined!")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isAppend = false;
    nbMessagesErrors++;
  }
  
  msgf2 = myMsg->find("mat/0");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "mat/0")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != "divide by 0 in matrix operation %s matrix dimension : (%i,%i)")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isAppend = false;
    nbMessagesErrors++;
  }
  
  msgf2 = myMsg->find("quad_altrule");
  if (msgf2 != 0)
  {
    if (msgf2->stringId() != "quad_altrule")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->format() != "No quadrature rule number %i for %s, using %s rule instead.")
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->type() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->stop() != 0)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
    if (msgf2->console() != 1)
    {
      isAppend = false;
      nbMessagesErrors++;
    }
  }
  else
  {
    isAppend = false;
    nbMessagesErrors++;
  }
  
  if(!isAppend)
  {
    ossMessages << "Messages::appendFromFile(string) ERROR" << std::endl;
  }
  oss << "---------- test of Messages class ----------" << std::endl
      << "Errors : " << nbMessagesErrors << std::endl << ossMessages.str() << std::endl;
  nbErrors += nbMessagesErrors;
    
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  
}

}
