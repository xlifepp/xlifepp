/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Vector.cpp
	\author E. Lunéville
	\since 14 dec 2011
	\date 11 may 2012

	Low level tests of Vector class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results (check=true)
	or compares results to those stored in the reference file (check=true)
	It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace xlifepp;

namespace unit_Vector {

void unit_Vector(int argc, char* argv[], bool check)
{
  String rootname = "unit_Vector";
  trace_p->push(rootname);
  String errors;
  Number nbErrors = 0;
  
  //! testing constructors
  theCout << words("constructors") << std::endl;
  Vector<Real> ur(4);
  Vector<Complex> uc(4);
  theCout << "ur=" << ur << " uc=" << uc << std::endl;
  Vector<Real> vr(4, 1);
  Vector<Complex> vc(4, i_);
  theCout << "vr=" << vr << " vc=" << vc << std::endl;
  uc = cmplx(vr);
  theCout << "uc=vr, uc=" << uc << std::endl;
  theCout<< "Vector<Real>(1.) = "<< Vector<Real>(1.) <<std::endl;
  theCout<< "Vector<Real>(1.,2.) = "<< Vector<Real>(1.,2.) <<std::endl;
  theCout<< "Vector<Real>(1.,2.,3.) = "<< Vector<Real>(1.,2.,3.) <<std::endl;
  theCout<< "Vector<Real>(1.,2.,3.,4.) = "<< Vector<Real>(1.,2.,3.,4.) <<std::endl;
  theCout<< "Vector<Real>{1.,2.,3.,4.} = "<< Vector<Real>{1.,2.,3.,4.} <<std::endl;
  //theCout<< "Vector<Complex>(0.) = "<< Vector<Complex>(0.) <<std::endl;               // does not work
  theCout<< "Vector<Complex>(3,1.) = "<< Vector<Complex>(3,1.) <<std::endl;
  theCout<< "Vector<Complex>{0.} = "<< Vector<Complex>{0.} <<std::endl;
  theCout<< "Vector<Complex>(Complex(0.), Complex(1.)) = "<< Vector<Complex>(Complex(0.), Complex(1.)) <<std::endl;
  theCout<< "Vector<Complex>{Complex(0.), Complex(1.)} = "<< Vector<Complex>{Complex(0.), Complex(1.)} <<std::endl;
  theCout<< "Vector<Complex>{0.,1.} = "<< Vector<Complex>{0.,1.} <<std::endl;
  theCout<< "Vector<Real>{0,1,1.5} = "<< Vector<Real>{0,1,1.5} <<std::endl;
  theCout<< "Vector<Complex>{0,1} = "<< Vector<Complex>{0,1} <<std::endl;
  Vector<Real> wr;
  wr = {1.,2.,3.,5.}; theCout<<" wr = {1.,2.,3.,5.} = "<< wr <<std::endl;
  Vector<Complex> wc;
  wc = {Complex(1.),Complex(2.),Complex(3.),Complex(5.)}; theCout<<" wc = {Complex(1.),Complex(2.),Complex(3.),Complex(5.)} = "<< wc <<std::endl;
  wc = {1.,2.,3.,5.}; theCout<<" wc = {1.,2.,3.,5.} = "<< wc <<std::endl;

//!   size and accessors
  theCout << words("accessors") << std::endl;
  theCout << "uc.size=" << uc.size() << " uc.begin()->" << (*uc.begin()) << " uc.end()-1 ->" << (*(--uc.end())) << std::endl;
  theCout << "vc(2)=" << vc(2) << " vc[1]=" << vc[1] << std::endl;
  vc(2) *= 2; vc(3) *= 3; vc(4) *= 4;
  theCout << "vc(2)*=2, vc(2)=" << vc(2) << "  vc(3)*=3, vc(3)=" << vc(3) << "  vc[3]*=4, vc(4)=" << vc(4) << std::endl;
  ur = 7.; uc = i_;
  theCout << "ur=7.; uc=i; ur=" << ur << " uc=" << uc << std::endl;
  uc = Complex(8.);
  theCout << "uc=8.; uc=" << uc << std::endl;

//!   algebraic operations
  theCout << words("algebraic operations") << std::endl;
  theCout << "+ur=" << +ur << " -ur=" << -ur << " +vc=" << +vc << " -vc=" << -vc  << std::endl;
  ur += 1; uc += i_; vr += ur; vc += uc;
  theCout << "ur+=1;uc+=i;vr+=ur;vc+=uc;  ur=" << ur << " uc=" << uc << " vr=" << vr << " vc=" << vc << std::endl;
  ur -= 1; uc -= i_; vr -= ur; vc -= uc;
  theCout << "ur-=1;uc-=i;vr-=ur;vc-=uc;  ur=" << ur << " uc=" << uc << " vr=" << vr << " vc=" << vc << std::endl;
  uc += cmplx(3); vc -= cmplx(2);
  theCout << "uc+=3;vc-=2;  uc=" << uc << " vc=" << vc << std::endl;
  ur *= 2; uc *= i_; vc *= -4;
  theCout << "ur*=2;uc*=i;vc*=-4;  ur=" << ur << " uc=" << uc << " vc=" << vc << std::endl;
  ur /= 2; uc /= i_; vc /= -4;
  theCout << "ur*=2;uc*=i;vc*=-4;  ur=" << ur << " uc=" << uc << " vc=" << vc << " vr=" << vr  << std::endl;
  theCout << "ur+vr=" << ur + vr << " uc+vc=" << uc + vc << " ur+uc=" << ur + uc << " uc+ur=" << uc + ur  << std::endl;
  theCout << "ur+1.=" << ur + 1. << " uc+i=" << uc + i_ << " ur+i=" << ur + i_ << " uc+1=" << uc + 1  << std::endl;
  theCout << "1+ur=" << 1. + ur << " i+uc=" << i_ + uc << " i+ur=" << i_ + ur << " 1+uc=" << 1 + uc << std::endl;
  theCout << "ur-vr=" << ur - vr << " uc-vc=" << uc - vc << " ur-uc=" << ur - uc << " uc-ur=" << uc - ur;
  theCout << "ur-1=" << ur - 1. << " uc-i=" << uc - i_ << " ur-i=" << ur - i_ << " uc-1=" << uc - 1 << std::endl;
  theCout << "1-ur=" << 1. - ur << " i-uc=" << i_ - uc << " i-ur=" << i_ - ur << " 1-uc=" << 1 - uc << std::endl;
  theCout << "ur*2=" << ur * 2. << " uc*i=" << uc* i_ << " ur*i=" << ur* i_ << " uc*2=" << uc * 2 << std::endl;
  theCout << "2*ur=" << 2.*ur << " i*uc=" << i_* uc << " i*ur=" << i_* ur << " 2*uc=" << 2 * uc << std::endl;
  theCout << "ur/2=" << ur / 2. << " uc/i=" << uc / i_ << " ur/i=" << ur / i_ << " uc/2=" << uc / 2. << std::endl;

//!   other operations
  theCout << words("other operations") << std::endl;
  theCout << "real(uc)=" << real(uc) << " imag(uc)=" << imag(uc) << std::endl;
  theCout << "conj(ur)=" << conj(ur) << " conj(uc)=" << conj(uc) << " cmplx(ur)=" << cmplx(ur) << std::endl;
  theCout << "norminfty(ur)=" << ur.norminfty() << " norminfty(uc)=" << uc.norminfty();
  theCout << " norm2squared(ur)=" << ur.norm2squared() << " norm2squared(uc)=" << uc.norm2squared();
  theCout << " norm2(ur)=" << ur.norm2() << " norm2(uc)=" << uc.norm2() << std::endl;
  theCout << "norminfty(vr)=" << vr.norminfty() << " norminfty(vc)=" << vc.norminfty();
  theCout << " norm2squared(vr)=" << vr.norm2squared() << " norm2squared(vc)=" << vc.norm2squared();
  theCout << " norm2(vr)=" << vr.norm2() << " norm2(vc)=" << vc.norm2() << std::endl;

//!   tests of Vector<Vector<Real> > and  Vector<Vector<Complex_t> >
  theCout << words("tests of") << " Vector<Vector<Real> > " << words("and") << " Vector<Vector<Complex_t> >" << std::endl;
  theCout << words("constructors") << std::endl;
  Vector<Real> rv1(3, 1.);
  Vector<Real> rv2(3, 1.5);
  Vector<Complex> cv1(3, 2 * i_);
  Vector<Vector<Real> > rvu(4);
  Vector<Vector<Complex> > cvu(4);
  theCout << "rvu=" << rvu << " cvu=" << cvu << std::endl;
  Vector<Vector<Real> > rvv(4, rv1);
  Vector<Vector<Complex> > cvv(4, cv1);
  theCout << "rvv=" << rvv << " cvv=" << cvv << std::endl;
  cvu = cmplx(rvv);
  theCout << "cvu=rvu; cvu=" << cvu << std::endl;

  theCout << words("accessors") << std::endl;
  theCout << "cvv.size=" << cvv.size() << " cvv.begin()->" << (*cvv.begin()) << " cvv.end()-1 ->" << (*(--cvv.end())) << std::endl;
  theCout << "cvv(2)=" << cvv(2) << " cvv[1]=" << cvv[1] << std::endl;
  cvv(2) *= 2; cvv(3) *= 3; cvv[3] *= 4;
  theCout << "cvv(2)*=2;cvv(3)*=3;cvv[3]*=4; cvv(2)=" << cvv(2) << " cvv(3)=" << cvv(3) << " cvv(4)=" << cvv(4) << std::endl;
  rvu = rv1; cvu = cv1;
  theCout << "rvu=rv1;cvu=cv1;  rvu=" << rvu << " cvu=" << cvu << std::endl;
  cvv = cmplx(rv2);
  theCout << "cvv=cmplx(rv2); cvv=" << cvv << std::endl;

  theCout << words("algebraic operations") << std::endl;
  theCout << "+rvv=" << +rvv << " -rvv=" << -rvv << std::endl;
  theCout << "+cvv=" << +cvv << " -cvv=" << -cvv << std::endl;
  rvu += rv1; cvu += cv1; rvv += rvv; cvv += cvv;
  theCout << "rvu+=rv1;cvu+=cv1;rvv+=rvv;cvv+=cvv;  rvu=" << rvu << " cvu=" << cvu << std::endl;
  theCout << "                                      rvv=" << rvv << " cvv=" << cvv << std::endl;
  rvu -= rv1; cvu -= cv1; rvv -= rvv; cvv -= cvv;
  theCout << "rvu-=rv1;cvu-=cv1;rvv-=rvv;cvv-=cvv;  rvu=" << rvu << " cvu=" << cvu << std::endl;
  theCout << "                                      rvv=" << rvv << " cvv=" << cvv << std::endl;
  cvu += cmplx(rv1); cvv -= cmplx(rv1);
  theCout << "cvu+=cmplx(rv1);cvv-=cmplx(rv1);      cvu=" << cvu << " cvv=" << cvv << std::endl;
  theCout << "rvu+rvv=" << rvu + rvv << "cvu+cvv=" << cvu + cvv << std::endl;
  theCout << "cmplx(rvu)+cvu=" << cmplx(rvu) + cvu << " cvu+cmplx(rvu)=" << cvu + cmplx(rvu) << std::endl;
  theCout << "rvu+rv1=" << rvu + rv1 << "cvu+cv1=" << cvu + cv1 << std::endl;
  theCout << "cmplx(rv1)+cvu=" << cmplx(rv1) + cvu << " cvu+cmplx(rv1)=" << cvu + cmplx(rv1) << std::endl;
  theCout << "rvu-rvv=" << rvu - rvv << "cvu-cvv=" << cvu - cvv << std::endl;
  theCout << "cmplx(rvu)-cvu=" << cmplx(rvu) - cvu << " cvu-cmplx(rvu)=" << cvu - cmplx(rvu) << std::endl;
  theCout << "rvu-rv1=" << rvu - rv1 << "cvu-cv1=" << cvu - cv1 << std::endl;
  theCout << "cmplx(rv1)-cvu=" << cmplx(rv1) - cvu << " cvu-cmplx(rv1)=" << cvu - cmplx(rv1) << std::endl;

//!   Tests of some new functions used for Solvers
  theCout << "Some extra functions serve for Solver" << std::endl;
  Vector<Real> rVec1(3, 1.);
  Vector<Complex> cVec1(3, i_);
  Vector<Real> rVecRes(3, 0.);
  Vector<Complex> cVecRes(3, Complex(0.));
  Real rScalar(3.0);
  Complex cScalar(3.0, 1.0);

  theCout << "Real vector: " << rVec1 << std::endl;
  theCout << "Complex vector: " << cVec1 << std::endl;
  theCout << "Real scalar: " << rScalar << std::endl;
  theCout << "Complex scalar: " << cScalar << std::endl;

  cVecRes = cVec1;
  theCout << "Complex vector before operation" << cVecRes << std::endl;
  addVectorThenAssign(cVecRes, rVec1);
  theCout << "Complex vector after adding and assigning a real vector" << cVecRes << std::endl;
  addVectorThenAssign(cVecRes, cVec1);
  theCout << "Complex vector after adding and assigning a Complex vector" << cVecRes << std::endl;

  rVecRes = rVec1;
  theCout << "Real vector before operation" << rVecRes << std::endl;
  addVectorThenAssign(rVecRes, rVec1);
  theCout << "Real vector after adding and assigning a real vector" << rVecRes << std::endl;

  cVecRes = cVec1;
  theCout << "Complex vector before operation" << cVecRes << std::endl;
  multScalarThenAssign(cVecRes, rScalar);
  theCout << "Complex vector after multipling and assigning a real scalar" << cVecRes << std::endl;
  multScalarThenAssign(cVecRes, cScalar);
  theCout << "Complex vector after multipling and assigning a complex scalar" << cVecRes << std::endl;

  rVecRes = rVec1;
  theCout << "Real vector before operation" << rVecRes << std::endl;
  multScalarThenAssign(rVecRes, rScalar);
  theCout << "Real vector after multipling and assigning a real scalar" << rVecRes << std::endl;

  scaleVector(rScalar, rVec1, cVecRes);
  theCout << "Complex vector after scaling a real scalar and real vector" << cVecRes << std::endl;
  scaleVector(cScalar, rVec1, cVecRes);
  theCout << "Complex vector after scaling a complex scalar and a real vector" << cVecRes << std::endl;
  scaleVector(rScalar, cVec1, cVecRes);
  theCout << "Complex vector after scaling a real scalar and a complex vector" << cVecRes << std::endl;
  scaleVector(cScalar, cVec1, cVecRes);
  theCout << "Complex vector after scaling a complex scalar and a complex vector" << cVecRes << std::endl;
  scaleVector(rScalar, rVec1, rVecRes);
  theCout << "Real vector after scaling a real scalar and real vector" << cVecRes << std::endl;

  nbErrors += checkValue(theCout, rootname+"/Vector.in" , errors, "Test Vector",check);

  if (check)
  {
   if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
   else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else
  { theCout << "Data updated " << eol; }

 trace_p->pop();

}

}
