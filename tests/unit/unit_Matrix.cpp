/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file unit_Matrix.cpp
	\author E. Lunéville
	\since 14 dec 2011
	\date 11 may 2012

	Low level tests of Matrix class.
	Almost functionalities are checked.
	This function may either creates a reference file storing the results 
	or compares results to those stored in the reference file 
	It returns reporting information in a string
*/

//#include "xlife++-libs.h"
// #include "init.h"
// #include "finalize.h"
// #include "utils.h"
// #include "testUtils.hpp"

// #include <iostream>
// #include <fstream>
// #include <vector>

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace unit_Matrix {

void unit_Matrix(int argc, char* argv[], bool check)
{
  String rootname = "unit_Matrix";
  trace_p->push(rootname);
  String errors;
  Number nbErrors = 0;
  Real tol=0.001;
  Real tolJ= 0.01;
  Number nbTry;
  Number maxTry=50;
  verboseLevel(1);
  numberOfThreads(1);


  //! testing constructors real and complex matrix
  theCout << "------------------ matrix constructors -----------------------" << std::endl;
  theCout << words("constructors for real matrix") << std::endl;
  Matrix<Real> rA;
  theCout << "default rA=" << rA << std::endl;
  Matrix<Real> rB(3, 2);
  theCout << "rB(3,2)=" << rB << std::endl;
  Matrix<Real> rC(rB);
  theCout << "copy rC(rB)=" << rC << std::endl;
  Matrix<Real> rD(3, 2, 1);
  theCout << "with different dims rD(3,2,1)=" << rD << std::endl;
  Matrix<Real> rE(3, 2, 2.);
  theCout << "with different dims rE(3,2,2.)=" << rE << std::endl;
  Vector<Real> ru(3, 2.);
  Matrix<Real> rF(ru);
  theCout << "diagonal from Vector rF(u)=" << rF << std::endl;
  Vector<std::vector<Real> > rU(3, ru);
  Matrix<Real> rG(rU);
  theCout << "from Vector of std::vectors rG(rU)=" << rG << std::endl;
  Vector<Vector<Real> > rV(3, ru);
  theCout << "rV=" << rV << std::endl;
  Matrix<Real> rH(rV);
  theCout << "from Vector of Vectors rH(rV)=" << rH << std::endl;
  Matrix<Real> rZero(3, _zeroMatrix);
  theCout << "construct Zero Matrix rZero(3,_zeroMatrix)=" << rZero << std::endl;
  Matrix<Real> rId(3, _idMatrix);
  theCout << "construct Id Matrix rId(3,_idMatrix)=" << rId << std::endl;
  Matrix<Real> rOnes(3, _onesMatrix);
  theCout << "construct ones Matrix rOnes(3,_onesMatrix)=" << rOnes << std::endl;
  Matrix<Real> rHil(3, _hilbertMatrix);
  theCout << "construct Hilbert Matrix rOnes(3,_hilbertMatrix)=" << rHil << std::endl;
  Matrix<Real> rL(inputsPathTo(rootname+"/matrix.data").c_str());
  theCout << words("construction from file") << " rL(\"matrix.data\")=" << rL << std::endl;
  Matrix<Real> rLb(inputsPathTo(rootname+"/matrix.data"));
  theCout << words("construction from file") << " rLb(String(\"matrix.data\"))=" << rLb << std::endl;
  theCout << words("constructors for complex matrix") << std::endl;
  Matrix<Complex> cA;
  theCout << "default cA=" << cA << std::endl;
  Matrix<Complex> cB(3, 2);
  theCout << "cB(3,2)=" << cB << std::endl;
  Matrix<Complex> cC(cB);
  theCout << "copy cC(rB)=" << cC << std::endl;
  Matrix<Complex> cD(3, 2, 1);
  theCout << "with different dims and int cD(3,2,1)=" << cD << std::endl;
  Matrix<Complex> cE(3, 2, 2.);
  theCout << "with different dims and real cE(3,2,2.)=" << cE << std::endl;
  Matrix<Complex> cEi(3, 2, i_);
  theCout << "with different dims and real cEi(3,2,i)=" << cEi << std::endl;
  Vector<Complex> cu(3, 2.*i_);
  Matrix<Complex> cF(cu);
  theCout << "diagonal from Vector cF(u)=" << cF << std::endl;
  Vector<std::vector<Complex> > cU(3, cu);
  Matrix<Complex> cG(cU);
  theCout << "from Vector of std::vectors cG(cU)=" << cG << std::endl;
  Vector<Vector<Complex> > cV(3, cu);
  theCout << "cV=" << cV << std::endl;
  Matrix<Complex> cH(cV);
  theCout << "from Vector of Vectors rH(cV)=" << cH << std::endl;
  Matrix<Complex> cZero(3, _zeroMatrix);
  theCout << "construct Zero Matrix cZero(3,_zeroMatrix)=" << cZero << std::endl;
  Matrix<Complex> cId(3, _idMatrix);
  theCout << "construct Id Matrix cId(3,_idMatrix)=" << cId << std::endl;
  Matrix<Complex> cOnes(3, _onesMatrix);
  theCout << "construct ones Matrix cOnes(3,_onesMatrix)=" << cOnes << std::endl;
  Matrix<Complex> cHil(3, _hilbertMatrix);
  theCout << "construct Hilbert Matrix cOnes(3,_hilbertMatrix)=" << cHil << std::endl;
  Matrix<Complex> cL(inputsPathTo(rootname+"/cmatrix.data").c_str());
  theCout << "construct from file cL(\"cmatrix.data\")=" << cL << std::endl;
  Matrix<Complex> cLb(inputsPathTo(rootname+"/cmatrix.data"));
  theCout << "construct from file cLb(String(\"cmatrix.data\"))=" << cLb << std::endl;

//! Check Matrices:
  nbErrors += checkValues(rA, rootname+"/rA.in", tol, errors, "Matrix rA", check);
  nbErrors+= checkValues(rB, rootname+"/rB.in", tol, errors, "Matrix rB", check);
  nbErrors+= checkValues(rC, rootname+"/rC.in", tol, errors, "Matrix rC", check);
  nbErrors+= checkValues(rD, rootname+"/rD.in", tol, errors, "Matrix rD", check);
  nbErrors+= checkValues(rE, rootname+"/rE.in", tol, errors, "Matrix rE", check);
  nbErrors+= checkValues(rF, rootname+"/rF.in", tol, errors, "Matrix rF", check);
  nbErrors += checkValues(rG, rootname+"/rG.in", tol, errors, "Matrix rG", check);
  nbErrors += checkValues(rV, rootname+"/rV.in", tol, errors, "Vector R rV", check);
  nbErrors+= checkValues(rH, rootname+"/rH.in", tol, errors, "Matrix rH", check);
  nbErrors+= checkValues(rZero, rootname+"/rZero.in", tol, errors, "Matrix rZero", check);
  nbErrors+= checkValues(rId, rootname+"/rId.in", tol, errors, "Matrix rId", check);
  nbErrors += checkValues(rOnes, rootname+"/rOnes.in", tol, errors, "Matrix rOnes", check);
  nbErrors+= checkValues(rHil, rootname+"/rHil.in", tol, errors, "Matrix rHil", check);
  nbErrors+= checkValues(rL, rootname+"/rL.in", tol, errors, "Matrix rL", check);
  nbErrors+= checkValues(rLb, rootname+"/rLb.in", tol, errors, "Matrix rLb", check);

  nbErrors+= checkValues(cA, rootname+"/cA.in", tol, errors, "Matrix cA", check);
  nbErrors+= checkValues(cB, rootname+"/cB.in", tol, errors, "Matrix cB", check);
  nbErrors+= checkValues(cC, rootname+"/cC.in", tol, errors, "Matrix cC", check);
  nbErrors+= checkValues(cD, rootname+"/cD.in", tol, errors, "Matrix cD", check);
  nbErrors+= checkValues(cE, rootname+"/cE.in", tol, errors, "Matrix cE", check);
  nbErrors+= checkValues(cF, rootname+"/cF.in", tol, errors, "Matrix cF", check);
  nbErrors += checkValues(cG, rootname+"/cG_0.in", tol, errors, "Matrix cG", check);
  nbErrors += checkValues(cV, rootname+"/cV.in", tol, errors, "Vector C cV", check);
  nbErrors+= checkValues(cH, rootname+"/cH.in", tol, errors, "Matrix cH", check);
  nbErrors+= checkValues(cZero, rootname+"/cZero.in", tol, errors, "Matrix cZero", check);
  nbErrors+= checkValues(cId, rootname+"/cId.in", tol, errors, "Matrix cId", check);
  nbErrors+= checkValues(cOnes, rootname+"/cOnes.in", tol, errors, "Matrix cOnes", check);
  nbErrors+= checkValues(cHil, rootname+"/cHil.in", tol, errors, "Matrix cHil", check);
  nbErrors+= checkValues(cL, rootname+"/cL.in", tol, errors, "Matrix cL", check);
  nbErrors+= checkValues(cLb, rootname+"/cLb.in", tol, errors, "Matrix cLb", check);




  //! assignment operators
  theCout << "------------------ matrix operator = -----------------------" << std::endl;
  rA = rE;
  theCout << "rE=" << rE << std::endl << " from Matrix rA=rE; rA=" << rA << std::endl;
  rB = rV;
  theCout << "rV=" << rV << std::endl << " from Vector<Vector> rB=rV; rB=" << rB << std::endl;
  nbErrors+= checkValues(rV, rB, tol, errors, "rB=rV");
  rB = rU;
  theCout << "rU=" << rU << std::endl << " from Vector<vector> rB=rU; rB=" << rB << std::endl;
  cA = cE;
  theCout << "cE=" << cE << std::endl << " from Matrix cA=cE; cA=" << cA << std::endl;
  nbErrors+= checkValues(cA, cE, tol, errors, "cA=cE");
  cB = cV;
  theCout << "cV=" << cV << std::endl << " from Vector<Vector> cB=cV; cB=" << cB << std::endl;
  nbErrors+= checkValues(cV, cB, tol, errors, "cB=cV");
  cB = cU;
  theCout << "cU=" << cU << std::endl << " from Vector<vector> cB=cU; cB=" << cB << std::endl;
  cA = rB;
  theCout << "rB=" << rB << std::endl << " from Matrix, autocast real->complex cA=rB; cA=" << cA << std::endl;

  nbErrors+= checkValues(rA, rE, tol, errors, "rA=rE");
  nbErrors+= checkValues(rB, rU, tol, errors, "rB=rU");
  nbErrors+= checkValues(cB, cU, tol, errors, "cB=cU");
  nbErrors+= checkValues(cA, rB, tol, errors, "cA=rB");

  //! access operators
  theCout << "------------------ matrix accessors --------------------------" << std::endl;
  Matrix<Real> rLL = rL;
  theCout << "rL=" << rL << "\nsize()=" << rL.size() << " vsize()=" << rL.vsize() << " hsize()=" << rL.hsize() << std::endl;
  theCout << " numberOfRows()=" << rL.numberOfRows() << " numberOfColumns()=" << rL.numberOfColumns() << std::endl;
  theCout << "rL(1,1)=" << rL(1, 1) << " rL(1,2)=" << rL(1, 2) << " rL(1,3)=" << rL(1, 3) << " rL(2,1)=" << rL(2, 1) << " rL(2,2)=" << rL(2, 2) << " rL(2,3)=" << rL(2, 3) << std::endl;
  theCout << "*rL.begin()=" << *rL.begin() << " *(rL.end()-1)=" << *(rL.end() - 1) << std::endl;
  theCout << "rL.row(1)=" << rL.row(1) << " rL.row(2)=" << rL.row(2) << std::endl;
  theCout << "rL.column(1)=" << rL.column(1) << " rL.column(2)=" << rL.column(2) << " rL.column(3)=" << rL.column(3) << std::endl;
  nbErrors+=checkValue(rL.size(), 6, tol, errors, "size:rL="+tostring(6));
  nbErrors+=checkValue(rL.hsize(), 3, tol, errors, "hsize:rL="+tostring(3));
  nbErrors+=checkValue(rL.vsize(), 2, tol, errors, "vsize:rL="+tostring(2));
  nbErrors+=checkValue(rL.numberOfRows(), 2, tol, errors, "nb of rows:rL="+tostring(2));
  nbErrors+=checkValue(rL.numberOfColumns(), 3, tol, errors, "number of Columns:rL="+tostring(3));
  nbErrors+=checkValue(*rL.begin(), 11, tol, errors, "*rL.begin()="+tostring(11));
  nbErrors+=checkValue(*(rL.end()-1), 23, tol, errors, "*(rL.end()-1)="+tostring(23));
  nbErrors+=checkValue(rL(1,1), 11, tol, errors, "rL(1,1)="+tostring(11));
  nbErrors+=checkValue(rL(1,2), 12, tol, errors, "rL(1,2)="+tostring(12));
  nbErrors+=checkValue(rL(1,3), 13, tol, errors, "rL(1,3)="+tostring(13));
  nbErrors+=checkValue(rL(2,1), 21, tol, errors, "rL(2,1)="+tostring(21));
  nbErrors+=checkValue(rL(2,2), 22, tol, errors, "rL(2,2)="+tostring(22));
  nbErrors+=checkValue(rL(2,3), 23, tol, errors, "rL(2,3)="+tostring(23));
  vector<Real> rLrR, rLcR;
  for (int k = 1; k <= 2; k++)
  {
    for (int i = 1; i <= 3; i++)
        rLrR.push_back(k*10+i);
    Vector<Real> rLrRef(rLrR);
    nbErrors+= checkValues( rL.row(k), rLrRef, tol, errors, "rL.row("+tostring(k)+")");
    rLrR.clear();
  }

  for (int i = 1; i <= 3; i++)
  {
    for (int k = 1; k <= 2; k++)
        rLcR.push_back(k*10+i);
    Vector<Real> rLcRef(rLcR);
    nbErrors+= checkValues( rL.column(i), rLcRef, tol, errors, "rL.column("+tostring(i)+")");
    rLcR.clear();
  }

  rL(1, 1) = 110; rL(2, 3) = 230;
  theCout << "rL(1,1)=110;rL(2,3)=230; -> rL=" << rL << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rL1.in", tol, errors, "Matrix rL ", check);
  rL.row(1, Vector<Real>(3, -1));
  theCout << "rL.row(1,Vector<Real>(3,-1)); -> rL=" << rL << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rL2.in", tol, errors, "Matrix rL ", check);
  rL.column(3, Vector<Real>(2, -3));
  theCout << "rL.column(3,Vector<Real>(2,-3)); -> rL=" << rL << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rL3.in", tol, errors, "Matrix rL ", check);

  rL.diag(rLL);
  Vector<Real> rd = rLL.diag();
  theCout << "extract diagonal rLL.diag()=" << rd << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLdiag.in", tol, errors, "rLdiag rL ", check);
  theCout << "diagonal assignment rL.diag(rLL)=" << rL << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLdiag2.in", tol, errors, "rLdiag2 rL ", check);

  rL.diag(5);
  theCout << "constant diagonal assignment rL.diag(5)=" << rL << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLdiag5.in", tol, errors, "Matrix rL diag5", check);
  rL.diag(rd);
  theCout << "Vector diagonal assignment rL.diag(rd)=" << rL << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLdiagrd.in", tol, errors, "Matrix rL diagrd", check);
  rL.adddiag(100);
  theCout << "add constant on diagonal rL.addiag(100)=" << rL << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLdiag100.in", tol, errors, "Matrix rL diag100", check);


  Matrix<Complex> cLL = cL;
  theCout << "cL=" << cL << "\nsize()=" << cL.size() << " vsize()=" << cL.vsize() << " hsize()=" << cL.hsize();
  theCout << " numberOfRows()=" << cL.numberOfRows() << " numberOfColumns()=" << cL.numberOfColumns() << std::endl;
  theCout << "cL(1,1)=" << cL(1, 1) << " cL(1,2)=" << cL(1, 2) << " cL(1,3)=" << cL(1, 3) << " cL(2,1)=" << cL(2, 1) << " cL(2,2)=" << cL(2, 2) << " cL(2,3)=" << cL(2, 3) << std::endl;
  theCout << "*cL.begin()=" << *cL.begin() << " *(cL.end()-1)=" << *(cL.end() - 1) << std::endl;
  theCout << "cL.row(1)=" << cL.row(1) << " cL.row(2)=" << cL.row(2) << std::endl;
  theCout << "cL.column(1)=" << cL.column(1) << " cL.column(2)=" << cL.column(2) << " cL.column(3)=" << cL.column(3) << std::endl;
  nbErrors+=checkValue(cL.size(), 6, tol, errors, "size:cL="+tostring(6));
  nbErrors+=checkValue(cL.hsize(), 3, tol, errors, "hsize:cL="+tostring(3));
  nbErrors+=checkValue(cL.vsize(), 2, tol, errors, "vsize:cL="+tostring(2));
  nbErrors+=checkValue(cL.numberOfRows(), 2, tol, errors, "nb of rows:cL="+tostring(2));
  nbErrors+=checkValue(cL.numberOfColumns(), 3, tol, errors, "number of Columns:cL="+tostring(3));
  nbErrors+=checkValue(*cL.begin(), Complex(1,1), tol, errors, "*cL.begin()="+tostring(1)+"+i"+tostring(3));
  nbErrors+=checkValue(*(cL.end()-1), Complex (2,3), tol, errors, "*(cL.end()-1)="+tostring(2)+"+i"+tostring(3));
  nbErrors+=checkValue(cL(1,1), Complex(1,1), tol, errors, "cL(1,1)="+tostring(1)+"+i"+tostring(1));
  nbErrors+=checkValue(cL(1,2), Complex(1,2), tol, errors, "cL(1,2)="+tostring(1)+"+i"+tostring(2));
  nbErrors+=checkValue(cL(1,3), Complex(1,3), tol, errors, "cL(1,3)="+tostring(1)+"+i"+tostring(3));
  nbErrors+=checkValue(cL(2,1), Complex(2,1), tol, errors, "cL(2,1)="+tostring(2)+"+i"+tostring(1));
  nbErrors+=checkValue(cL(2,2), Complex(2,2), tol, errors, "cL(2,2)="+tostring(2)+"+i"+tostring(2));
  nbErrors+=checkValue(cL(2,3), Complex(2,3), tol, errors, "cL(2,3)="+tostring(2)+"+i"+tostring(3));
  for (int k = 1; k <= 2; k++)
  {
    nbErrors+= checkValues( cL.row(k), rootname+"/cLrow"+tostring(k)+".in", tol, errors, "cL.row("+tostring(k)+")",check);
  }

  for (int i = 1; i <= 3; i++)
  {
      nbErrors += checkValues( cL.column(i), rootname+"/cLcol"+tostring(i)+".in", tol, errors, "cL.column("+tostring(i)+")", check);
  }


  cL(1, 1) = 11 + i_; cL(2, 3) = 23 + i_;

  theCout << "cL(1,1)=11+i;cL(2,3)=23+i; -> cL=" << cL << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cL1.in", tol, errors, "Matrix cL ", check);
  cL.row(1, Vector<Complex>(3, -i_));
  theCout << "cL.row(1,Vector<Complex>(3,-i)); -> cL=" << cL << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cL2.in", tol, errors, "Matrix cL ", check);
  cL.column(3, Vector<Complex>(2, -3 * i_));
  theCout << "cL.column(3,Vector<Complex>(2,-3*i)); -> cL=" << cL << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cL3.in", tol, errors, "Matrix cL ", check);
  cL.diag(cLL);
  Vector<Complex> cd = cLL.diag();
  theCout << "extract diagonal cLL.diag()=" << cd << std::endl;
  nbErrors+= checkValues(cd, rootname+"/cddiag.in", tol, errors, "cLdiag cd", check);
  theCout << "diagonal assignment cL.diag(cLL)=" << cL << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLdiag.in", tol, errors, "cLdiag cL", check);
  cL.diag(5);
  theCout << "constant diagonal assignment cL.diag(i)=" << cL << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLdiag5.in", tol, errors, "Matrix cL diag5", check);
  cL.diag(cd);
  theCout << "Vector diagonal assignment cL.diag(cd)=" << cL << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLdiagcd.in", tol, errors, "Matrix cL diagcd", check);
  cL.adddiag(100 * i_);
  theCout << "add constant on diagonal cL.addiag(100*i)=" << cL << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLdiag100.in", tol, errors, "Matrix cL diag100", check);

  //!matrix structure functions
  theCout << "------------------ matrix structure functions ----------------" << std::endl;
  Vector<Number> Vbool(31);
  theCout << "dim test : rL.shareDims(rLL)=" << booltoWord(rL.shareDims(rLL)) << " rL.shareDims(rF)=" << booltoWord(rL.shareDims(rF));
  theCout << " rL.shareDims(cL)=" << booltoWord(rL.shareDims(cL)) << std::endl;
  Vbool(1)=rL.shareDims(rLL);
  Vbool(2)=rL.shareDims(rF);
  Vbool(3)=rL.shareDims(cL);
  cA = cId; cA.adddiag(i_);

  theCout << "cA=" << cA << std::endl;
  nbErrors+= checkValues(cA, rootname+"/cA_2.in", tol, errors, "Matrix cA 2eme:", check);
  theCout << "symmetry test : rL.isSymmetric()=" << booltoWord(rL.isSymmetric()) << " rOnes.isSymmetric()=" << booltoWord(rOnes.isSymmetric());

  Vbool(4)=rL.isSymmetric();
  Vbool(5)=rOnes.isSymmetric();
  theCout << " rHil.isSymmetric()=" << booltoWord(rHil.isSymmetric()); Vbool(6)=rHil.isSymmetric();
  theCout << " cL.isSymmetric()=" << booltoWord(cL.isSymmetric()) << " cOnes.isSymmetric()=" << booltoWord(cOnes.isSymmetric());
  Vbool(7)=cL.isSymmetric();
  Vbool(8)=cOnes.isSymmetric();
  theCout << " cHil.isSymmetric()=" << booltoWord(cHil.isSymmetric()) << " cA.isSymmetric()=" << booltoWord(cA.isSymmetric()) << std::endl;
  Vbool(9)=cHil.isSymmetric();
  Vbool(10)=cA.isSymmetric();
  theCout << "skew-symmetry test : rL.isSkewSymmetric()=" << booltoWord(rL.isSkewSymmetric()) << " rOnes.isSkewSymmetric()=" << booltoWord(rOnes.isSkewSymmetric());
  Vbool(11)=rL.isSymmetric();
  Vbool(12)=rOnes.isSymmetric();
  theCout << " rHil.isSkewSymmetric()=" << booltoWord(rHil.isSkewSymmetric());
  Vbool(13)=rHil.isSkewSymmetric();
  theCout << " cL.isSkewSymmetric()=" << booltoWord(cL.isSkewSymmetric()) << " cOnes.isSkewSymmetric()=" << booltoWord(cOnes.isSkewSymmetric());
  Vbool(14)=cL.isSkewSymmetric();
  Vbool(15)=cOnes.isSkewSymmetric();
  theCout << " cHil.isSkewSymmetric()=" << booltoWord(cHil.isSkewSymmetric()) << " cA.isSkewSymmetric()=" << booltoWord(cA.isSkewSymmetric()) << std::endl;
  Vbool(16)=cHil.isSkewSymmetric();
  Vbool(17)=cA.isSkewSymmetric();
  theCout << "self-adjointness test : rL.isSelfAdjoint()=" << booltoWord(rL.isSelfAdjoint()) << " rOnes.isSelfAdjoint()=" << booltoWord(rOnes.isSelfAdjoint());
  Vbool(18)=rL.isSelfAdjoint();
  Vbool(19)=rOnes.isSelfAdjoint();
  theCout << " rHil.isSelfAdjoint()=" << booltoWord(rHil.isSelfAdjoint());
  Vbool(20)=rHil.isSelfAdjoint();
  theCout << " cL.isSelfAdjoint()=" << booltoWord(cL.isSelfAdjoint()) << " cOnes.isSelfAdjoint()=" << booltoWord(cOnes.isSelfAdjoint());
  Vbool(21)=cL.isSelfAdjoint();
  Vbool(22)=cOnes.isSelfAdjoint();
  theCout << " cHil.isSelfAdjoint()=" << booltoWord(cHil.isSelfAdjoint()) << " cA.isSelfAdjoint()=" << booltoWord(cA.isSelfAdjoint()) << std::endl;
  Vbool(23)=cHil.isSelfAdjoint();
  Vbool(24)=cA.isSelfAdjoint();
  theCout << "skew-adjointness test : rL.isSkewAdjoint()=" << booltoWord(rL.isSkewAdjoint()) << " rOnes.isSkewAdjoint()=" << booltoWord(rOnes.isSkewAdjoint());
  Vbool(25)=rL.isSkewAdjoint();
  Vbool(26)=rOnes.isSkewAdjoint();
  theCout << " rHil.isSkewAdjoint()=" << booltoWord(rHil.isSkewAdjoint());
  Vbool(27)=rHil.isSkewAdjoint();
  theCout << " cL.isSkewAdjoint()=" << booltoWord(cL.isSkewAdjoint()) << " cOnes.isSkewAdjoint()=" << booltoWord(cOnes.isSkewAdjoint());
  Vbool(28)=cL.isSkewAdjoint();
  Vbool(29)=cOnes.isSkewAdjoint();
  theCout << " cHil.isSkewAdjoint()=" << booltoWord(cHil.isSkewAdjoint()) << " cA.isSkewAdjoint()=" << booltoWord(cA.isSkewAdjoint()) << std::endl;
  Vbool(30)=cHil.isSkewAdjoint();
  Vbool(31)=cA.isSkewAdjoint();

  nbErrors+= checkValues(Vbool, rootname+"/MatStruct.in", 0, errors, "Matstruct", check);

  rB = rHil; cB = cHil;
  Matrix<Real> rS(3, 3);
  Matrix<Complex> cS(3, 3);
  for(int k = 1; k <= 3; k++)
    for(int j = 1; j <= 3; j++)
    {
      rS(k, j) = 10 * k + j;
      cS(k, j) = k + j * i_;
    }
  theCout << "rS=" << rS << std::endl;
  nbErrors+= checkValues(rS, rootname+"/rS.in", tol, errors, "rS=", check);
  theCout << "tranposition rHil.transpose()=" << rB.transpose() << "\nrS.transpose()=" << rS.transpose() << std::endl;
  nbErrors+= checkValues(rB.transpose(), rootname+"/rBtranspose.in", tol, errors, "rB Transpose=", check);
  nbErrors+= checkValues(rS.transpose(), rootname+"/rStranspose.in", tol, errors, "rS Transpose=", check);
  theCout << "adjoint rHil.adjoint()=" << rB.adjoint() << "\nrS.adjoint()=" << rS.adjoint() << std::endl;
  nbErrors+= checkValues(rB.adjoint(), rootname+"/rBadjoint.in", tol, errors, "rB adjoint", check);
  nbErrors+= checkValues(rS.adjoint(), rootname+"/rSadjoint.in", tol, errors, "rS adjoint", check);

  theCout << "cS=" << cS << std::endl;
  nbErrors+= checkValues(cS, rootname+"/cS.in", tol, errors, "cS=", check);
  theCout << "tranposition cHil.transpose()=" << cB.transpose() << "\ncS.transpose()=" << cS.transpose() << std::endl;
  nbErrors+= checkValues(cB.transpose(), rootname+"/cBtranspose.in", tol, errors, "cB Transpose=", check);
  nbErrors+= checkValues(cS.transpose(), rootname+"/cStranspose.in", tol, errors, "cS Transpose=", check);
  theCout << "adjoint cHil.adjoint()=" << cB.adjoint() << "\ncS.adjoint()=" << cS.adjoint() << std::endl;
  nbErrors+= checkValues(cB.adjoint(), rootname+"/cBadjoint.in", tol, errors, "cB adjoint", check);
  nbErrors+= checkValues(cS.adjoint(), rootname+"/cSadjoint.in", tol, errors, "cS adjoint", check);
  theCout << "real part of the complex matrix" << cS << " cS.real()=" << cS.real() << std::endl;
  nbErrors+= checkValues(cS.real(), rootname+"/cS_real.in", tol, errors, "cS real", check);
  theCout << "imaginary part of the complex matrix" << cS << " cS.imag()=" << cS.imag() << std::endl;
  nbErrors+= checkValues(cS.imag(), rootname+"/cS_imag.in", tol, errors, "cS imag", check);

  theCout << "real part of the real matrix" << rS << " rS.real()=" << rS.real() << std::endl;
  nbErrors+= checkValues(rS.real(), rootname+"/rS_real.in", tol, errors, "rS real", check);
  theCout << "imaginary part of the real matrix" << rS << " rS.imag()=" << rS.imag() << std::endl;
  nbErrors+= checkValues(rS.imag(), rootname+"/rS_imag.in", tol, errors, "rS imag", check);

  //! internal algebraic operations (+=,-=,*=,/=)
  rL = rLL; cL = cLL;
  theCout << "------------------ internal algebraic operations -----------------------" << std::endl;
  theCout << "rL+=10" << (rL += 10) << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLpe10.in", tol, errors, "rL+=10", check);
  theCout << "rL-=10" << (rL -= 10) << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLme10.in", tol, errors, "rL-=10", check);
  theCout << "rL*=10" << (rL *= 10) << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLfe10.in", tol, errors, "rL*=10", check);
  theCout << "rL/=10" << (rL /= 10) << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLde10.in", tol, errors, "rL/=10", check);
  theCout << "rL+=rLL" << (rL += rLL) << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLperLL.in", tol, errors, "rL+=rLL", check);
  theCout << "rL-=rLL" << (rL -= rLL) << std::endl;
  nbErrors+= checkValues(rL, rootname+"/rLmerLL.in", tol, errors, "rL-=rLL", check);
  theCout << "cL+=10" << (cL += 10) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLpe10.in", tol, errors, "cL+=10", check);
  theCout << "cL-=10" << (cL -= 10) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLme10.in", tol, errors, "cL-=10", check);
  theCout << "cL*=10" << (cL *= 10) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLfe10.in", tol, errors, "cL*=10", check);
  theCout << "cL/=10" << (cL /= 10) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLde10.in", tol, errors, "cL/=10", check);
  theCout << "cL+=10*i" << (cL += 10 * i_) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLpe10i.in", tol, errors, "cL+=10i", check);
  theCout << "cL-=10*i" << (cL -= 10 * i_) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLme10i.in", tol, errors, "cL-=10i", check);
  theCout << "cL*=10*i" << (cL *= 10 * i_) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLfe10i.in", tol, errors, "cL*=10i", check);
  theCout << "cL/=10*i" << (cL /= 10 * i_) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLde10i.in", tol, errors, "cL/=10i", check);
  theCout << "cL+=cLL" << (cL += cLL) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLpecLL.in", tol, errors, "cL+=cLL", check);
  theCout << "cL-=cLL" << (cL -= cLL) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLmecLL.in", tol, errors, "cL-=cLL", check);
  theCout << "cL+=rLL" << (cL += rLL) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLperLL.in", tol, errors, "cL+=rLL", check);
  theCout << "cL-=rLL" << (cL -= rLL) << std::endl;
  nbErrors+= checkValues(cL, rootname+"/cLmerLL.in", tol, errors, "cL-=rLL", check);
  theCout << "rL.real()" << rL.real() << std::endl;
  nbErrors+= checkValues(rL.real(), rootname+"/rL_real.in", tol, errors, "rL.real()", check);
  theCout << "rL.imag()" << rL.imag() << std::endl;
  nbErrors+= checkValues(rL.imag(), rootname+"/rL_imag.in", tol, errors, "rL.imag()", check);
  theCout << "cL.real()" << cL.real() << std::endl;
  nbErrors+= checkValues(cL.real(), rootname+"/cL_real.in", tol, errors, "cL.real()", check);
  theCout << "cL.imag()" << cL.imag() << std::endl;
  nbErrors+= checkValues(cL.imag(), rootname+"/cL_imag.in", tol, errors, "cL.imag()", check);

  theCout << "-------------Matrix*Vector & Vector*Matrix----------------------" << std::endl;
  Matrix<Real> A(3,2,0.); Vector<Real> X(2),Y(3);
  Matrix<Complex> Ac(3,2,0.); Vector<Complex> Xc(2),Yc(3);
  Number k=1;
  for(Number j=1;j<=2;j++)
  {
    X(j)=j;
    Xc(j)=j+i_;
    for(Number i=1;i<=3;i++,k++)
    {
      A(i,j)= k;
      Ac(j,i)=i_*(1+k);
      Y(i)=i;
      Yc(i)=i+i_;
    }
  }
  theCout << "A*X = " << A*X << ", A*Xc = " << A*Xc << ", Ac*X = " << Ac*X << ", Ac*Xc = " << Ac*Xc << std::endl;
  theCout << "Y*A = " << Y*A << ", Yc*A = " << Yc*A << ", Y*Ac = " << Y*Ac << ", Yc*Ac = " << Yc*Ac << std::endl;
  nbErrors += checkValue(theCout, rootname+"/MatrixVector.in" , errors, "Matrix*Vector ",check);

  thePrintStream << "------------------ Matrix comparison -----------------------" << std::endl;
  thePrintStream << "comparison rL==rLL is " << booltoWord(rL == rLL) << ", rL==2*rLL is " << booltoWord(rL == 2 * rLL);
  thePrintStream << ", cL==cLL is " << booltoWord(cL == cLL) << ", cL==2*cLL is " << booltoWord(cL == 2 * cLL) << std::endl;
  thePrintStream << "comparison rL!=rLL is " << booltoWord(rL != rLL) << ", rL!=2*rLL is " << booltoWord(rL != 2 * rLL);
  thePrintStream << ", cL!=cLL is " << booltoWord(cL != cLL) << ", cL!=2*cLL is " << booltoWord(cL != 2 * cLL) << std::endl;
  Vector<Number> VboolC(8);
  VboolC(1)=(rL == rLL); VboolC(2)=(rL == 2*rLL); VboolC(3)=(rL != rLL);VboolC(4)=(rL != 2*rLL);
  VboolC(5)=(cL == cLL); VboolC(6)=(cL == 2*cLL); VboolC(7)=(cL != cLL);VboolC(8)=(cL != 2*cLL);
  nbErrors+= checkValues(VboolC, rootname+"/MatComparison.in", 0, errors, "Mat compar", check);

  theCout << "------------------ multMatrixRow -----------------------------" << std::endl;
  Matrix<Real> M(3,2,0.), Mt(2,3,0.), N(3,2,0.), Nt(2,3,0.);
  k=1;
  for(Number i=1;i<=3;i++)
    for(Number j=1;j<=2;j++, k++)
    {
        M(i,j)= k;
        Mt(j,i)=k;
    }
  rS.multMatrixRow(&M[0],&N[0],2);
  thePrintStream << "rS =" << rS << std::endl << "M =" << M << std::endl << "rS*(row)M =" << N << std::endl;
  nbErrors+= checkValues(rS, rootname+"/rSrow.in", tol, errors, "rSrow", check);
  nbErrors+= checkValues(M, rootname+"/Mrow.in", tol, errors, "Mrow", check);
  nbErrors+= checkValues(N, rootname+"/Nrow.in", tol, errors, "Nrow", check);
  rS.multLeftMatrixRow(&Mt[0],&Nt[0],2);
  thePrintStream << "rS =" << rS << std::endl << "Mt =" << Mt << std::endl << "Mt*rS =" << Nt << std::endl;
  nbErrors+= checkValues(rS, rootname+"/rSrowt.in", tol, errors, "rSrowt", check);
  nbErrors+= checkValues(Mt, rootname+"/Mtrowt.in", tol, errors, "Mtrowt", check);
  nbErrors+= checkValues(N, rootname+"/Ntrowt.in", tol, errors, "Ntrowt", check);

  theCout << "------------------ multMatrixCol -----------------------------" << std::endl;
  rS.multMatrixCol(&M[0],&N[0],2);
  thePrintStream << "rS =" << rS << std::endl << "M =" << M << std::endl << "rS*M =" << N << std::endl;
  nbErrors+= checkValues(rS, rootname+"/rScol.in", tol, errors, "rScol", check);
  nbErrors+= checkValues(M, rootname+"/Mcol.in", tol, errors, "Mcol", check);
  nbErrors+= checkValues(N, rootname+"/Ncol.in", tol, errors, "Ncol", check);
  rS.multLeftMatrixCol(&Mt[0],&Nt[0],2);
  thePrintStream << "rS =" << rS << std::endl << "Mt =" << Mt << std::endl << "Mt*rS =" << Nt << std::endl;
  nbErrors+= checkValues(rS, rootname+"/rScolt.in", tol, errors, "rScolt", check);
  nbErrors+= checkValues(Mt, rootname+"/Mcolt.in", tol, errors, "Mcolt", check);
  nbErrors+= checkValues(Nt, rootname+"/Ncolt.in", tol, errors, "Ncolt", check);

  theCout << "------------------ GaussSolver -------------------------------" << std::endl;
  //!test gauss solver, LU factorization and inverse
  Dimen n=5;
  Matrix<Real> Hn(n, n);
  Vector<Real> rhs(n), x(n);
  for(Number i=1;i<=n;++i)
  {
      x[i-1]=i;
      Hn(i,i)= 3.;
      if(i<n) Hn(i,i+1)= -1;
      if(i>1) Hn(i,i-1)= -1;
  }
  thePrintStream << "Hn=" << Hn << eol;
  rhs=Hn*x;
  Number r=0.;
  Matrix<Real> Hnc=Hn;
  Real piv;
  gaussSolver(Hnc, rhs, piv, r);
  thePrintStream << "gaussSolver x=" << rhs << eol;
  nbErrors+= checkValues(rhs, x, tol, errors, "gaussSolver");
  Hnc=Hn;
  Matrix<Real> invHn=inverse(Hnc);
  thePrintStream << "inverse(Hn)=" << invHn << eol;
  thePrintStream << "invHn*Hn=" << invHn*Hn << eol;
  nbErrors+= checkValues(invHn, rootname+"/invHn.in", tol, errors, "inv(Hn)", check);
  nbErrors+= checkValues(invHn*Hn, rootname+"/IdHn.in", tol, errors, "inv(Hn)*Hn", check);
  Hnc=Hn;
  lu(Hnc);
  thePrintStream << "lu(Hn)=" << Hnc << eol;
  nbErrors+= checkValues(Hnc, rootname+"/luHnc.in", tol, errors, "lu(Hnc)", check);
  Hnc=Hn;
  Matrix<Real> L,U;
  lu(Hnc,L,U);
  thePrintStream << "lu(Hn,L,U), L=" << L << eol << "U=" << U << eol;
  thePrintStream << "L*U=" << L*U << eol;
  nbErrors+= checkValues(L*U, Hn, tol, errors, "L*U/Hn");
  std::vector<Dimen> p;
  lu(Hnc,p);
  thePrintStream << "lu(Hn)=" << Hnc << eol;
  nbErrors+= checkValues(Hnc, rootname+"/Hnc_ref.in", tol, errors, "lu(Hnc,p)", check);
  Hnc=Hn;

  //!test SVD and random SDV for Matrix (row dense)
  n=20;
  Number rk=5;
  Real eps=1.E-08;
  Matrix<Real> U1,V1;
  Vector<Real> D1;
  Matrix<Real> Hilbn(n,_hilbertMatrix);
  thePrintStream << "Hilbert matrix Hilb" << n << " = " << eol << Hilbn << eol;
  nbErrors+= checkValues(Hilbn, rootname+"/Hilbn.in", tol, errors, " Hilbert matrix Hilb", check);
  thePrintStream << "norm2(Hilbn)=" << norm2(Hilbn) << eol;

  nbErrors+=checkValue( norm2(Hilbn), 1.9698134529, tol,errors, " norm2(Hilbn)");

#ifdef XLIFEPP_WITH_EIGEN
  theCout << "------------------ SVD on Matrix ----------------------------- " << eol;
  svd(Hilbn,U1,D1,V1);
  Matrix<Real> matD1(D1);

  Real nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
  thePrintStream << "svd 'exact' : D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 1.4681632066e-14, tol,errors, " norm2(U1*D1*V1'-Hilbn)");
  nbErrors+= checkValues(D1, rootname+"/Dsvdexacte.in", tol, errors, " D1 exacte", check);

  svd(Hilbn,U1,D1,V1,0,eps);
  matD1=Matrix<Real>(D1);
  nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
  thePrintStream << "svd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 6.0400810905e-10, tol,errors, " norm2(U1*matDA*transpose(V1)'-Hilbn)");
  nbErrors+= checkValues(matD1, rootname+"/DA.in", tol, errors, "DA", check);

  svd(Hilbn,U1,D1,V1,rk);
  matD1=Matrix<Real>(D1);
  nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
  thePrintStream << "svd truncation r=" << rk << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., 0.06,errors, " norm2(U1*matDB*transpose(V1)-Hilbn)");
  nbErrors+= checkValue( rk, 5, tol,errors, " svd truncation r");
  nbErrors+= checkValues(matD1, rootname+"/DB.in", tol, errors, " DB", check);

  theCout << "------------------ RSVD on Matrix ----------------------------" << eol;
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    rsvd(Hilbn,U1,D1,V1,0,theTolerance);
    matD1=Matrix<Real>(D1);
    nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "try " << nbTry << ": rsvd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 4.4192208766e-12, tolJ,errors, " norm2(U1*matDC*transpose(V1)'-Hilbn), try number:"+tostring(nbTry));

  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    rsvd(Hilbn,U1,D1,V1,0,eps);
    matD1=Matrix<Real>(D1);
    nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "try " << nbTry << ": rsvd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 2.2475939367e-09, tolJ,errors, " norm2(U1*DD*V1'-Hilbn)");

  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    rsvd(Hilbn,U1,D1,V1,rk);
    matD1=Matrix<Real>(D1);
    nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "try " << nbTry << ": rsvd truncation r=" << rk << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << norm2(U1*matD1*transpose(V1)-Hilbn) << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DE*V1'-Hilbn)");

  theCout << "------------------ R3SVD on Matrix --------------------------- " << eol;
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    r3svd(Hilbn,U1,D1,V1, theTolerance);
    matD1=Matrix<Real>(D1);
    nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "try " << nbTry << ": r3svd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DF*V1'-Hilbn)");


  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
   r3svd(Hilbn,U1,D1,V1, eps);
   matD1=Matrix<Real>(D1);
   nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
   nbTry++;
  }
  thePrintStream << "try " << nbTry << ": r3svd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DG*V1'-Hilbn)");

  //!test SVD and random SDV for col dense matrix
  std::vector<Real> cHilbn(n*n);
  std::vector<Real>::iterator itc=cHilbn.begin();
  for(Number j=1;j<=n;++j)
    for(Number i=1;i<=n;++i, ++itc) *itc=Hilbn(i,j);

  theCout << "------------------ SVD on col dense matrix as vector ---------" << eol;
  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  r=0;
  svd(&cHilbn[0], n, n, &V1[0],&D1[0],&U1[0],r);
  U1.resize(n*r);V1.resize(n*r);D1.resize(r);
  matD1=Matrix<Real>(D1);
  nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
  thePrintStream << "svd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*DH*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValues(cHilbn, rootname+"/cHilbnH.in", tol, errors, " cHILBNH", check);

  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  r=0;
  svd(&cHilbn[0], n, n, &V1[0],&D1[0],&U1[0],r, eps);
  U1.resize(n*r);V1.resize(n*r);D1.resize(r);
  matD1=Matrix<Real>(D1);
  nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
  thePrintStream << "svd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DI*V1'-Hilbn)");

  U1.resize(n*rk);V1.resize(n*rk);D1.resize(rk);
  svd(&cHilbn[0], n, n, &V1[0],&D1[0],&U1[0],rk, eps);
  matD1=Matrix<Real>(D1);
  nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
  thePrintStream << "svd truncation r=" << rk << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DJ*V1'-cHilbn)");

  theCout << "------------------ RSVD on col dense matrix as vector -------- " << eol;
  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  r=0;
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    rsvd(&cHilbn[0], n, n, theTolerance, r, &V1[0],&D1[0],&U1[0]);
    U1.resize(n*r);V1.resize(n*r);D1.resize(r);
    matD1=Matrix<Real>(D1);
    nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": rsvd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DK*V1'-Hilbn)");


  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  r=0;
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
   rsvd(&cHilbn[0], n, n, eps, r, &V1[0],&D1[0],&U1[0]);
   U1.resize(n*r);V1.resize(n*r);D1.resize(r);
   matD1=Matrix<Real>(D1);
   nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
   nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": rsvd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*D1*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DL*V1'-Hilbn)");

  U1.resize(n*rk);V1.resize(n*rk);D1.resize(rk);
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    rsvd(&cHilbn[0], n, n, rk, &V1[0],&D1[0],&U1[0]);
    U1.resize(n*rk);V1.resize(n*rk);D1.resize(rk);
    matD1=Matrix<Real>(D1);
    nApprox = norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << " rsvd truncation r=" << rk << ": D1=" << D1 << " norm2(U1*DM*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0.,tolJ,errors, "  norm2(U1*DM*V1'-cHilbn)");


  theCout << "------------------ R3SVD on col dense matrix as vector ------- " << eol;
  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    r3svd(&cHilbn[0], n, n, theTolerance, r, &V1[0],&D1[0],&U1[0]);
    U1.resize(n*r);V1.resize(n*r);D1.resize(r);
    matD1=Matrix<Real>(D1);
    nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": r3svd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*DN*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DN*V1'-cHilbn)");


  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    r3svd(&cHilbn[0], n, n, eps, r, &V1[0],&D1[0],&U1[0]);
    U1.resize(n*r);V1.resize(n*r);D1.resize(r);
    matD1=Matrix<Real>(D1);
    nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": r3svd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*DO*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DO*V1'-Hilbn)");


  theCout << "------------------ RSVD on row dense matrix as vector -------- " << eol;
  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  r=0;
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    rsvd(&Hilbn[0], n, n, theTolerance, r, &U1[0],&D1[0],&V1[0]);
    U1.resize(n*r);V1.resize(n*r);D1.resize(r);
    matD1=Matrix<Real>(D1);
    nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": rsvd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*DP*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DP*V1'-Hilbn)");

  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  r=0;
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    rsvd(&Hilbn[0], n, n, eps, r, &V1[0],&D1[0],&U1[0]);
    U1.resize(n*r);V1.resize(n*r);D1.resize(r);
    matD1=Matrix<Real>(D1);
    nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": rsvd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*DQ*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DQ*V1'-Hilbn)");


  U1.resize(n*rk);V1.resize(n*rk);D1.resize(rk);
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
   rsvd(&Hilbn[0], n, n, rk, &U1[0],&D1[0],&V1[0]);
   U1.resize(n*rk);V1.resize(n*rk);D1.resize(rk);
   matD1=Matrix<Real>(D1);
   nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
   nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": rsvd truncation r=" << rk << ": D1=" << D1 << " norm2(U1*DR*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DR*V1'-Hilbn)");


  theCout << "------------------ R3SVD on row dense matrix as vector ------- " << eol;
  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    r3svd(&Hilbn[0], n, n, theTolerance, r, &U1[0],&D1[0],&V1[0]);
    U1.resize(n*r);V1.resize(n*r);D1.resize(r);
    matD1=Matrix<Real>(D1);
    nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": r3svd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*DS*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DS*V1'-Hilbn) ");

  U1.resize(n*n);V1.resize(n*n);D1.resize(n);
  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    r3svd(&Hilbn[0], n, n, eps, r, &U1[0],&D1[0],&V1[0]);
    U1.resize(n*r);V1.resize(n*r);D1.resize(r);
    matD1=Matrix<Real>(D1);
    nApprox=norm2(U1*matD1*transpose(V1)-Hilbn);
    nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": r3svd truncation eps=" << eps << ": D1=" << D1 << " norm2(U1*DT*V1'-Hilbn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ, errors, "  norm2(U1*DT*V1'-Hilbn)");

  //!svd on a rectangular matrix
  Number m=10;
  n=20;
  Matrix<Real> Hilbmn(m,n,0.);
  for(Number i=1;i<=m;i++)
    for(Number j=1;j<=n;j++) Hilbmn(i,j)=1./(i+j);
  rk=5;
  eps=1.E-08;
  thePrintStream << "Hilbert matrix Hilb" << n << "_" << m << " = " << eol << Hilbmn << eol;
  thePrintStream << "norm2(Hilbmn)=" << norm2(Hilbmn) << eol;
  theCout << "------------------ SVD, RSVD, R3SVD on a rectangular Matrix -- " << eol;
  svd(Hilbmn,U1,D1,V1);
  matD1=Matrix<Real>(D1);
  nApprox=norm2(U1*matD1*transpose(V1)-Hilbmn);
  thePrintStream << "svd 'exact' : D1=" << D1 << " norm2(U1*DU*V1'-Hilbmn)=" << nApprox << eol;
  nbErrors+= checkValues(Hilbmn, rootname+"/Hilbmn1.in", tol, errors, " Hilbmn svd.", check);
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DU*V1'-Hilbmn)");

  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
   rsvd(Hilbmn,U1,D1,V1,0,theTolerance);
   matD1=Matrix<Real>(D1);
   nApprox=norm2(U1*matD1*transpose(V1)-Hilbmn);
   nbTry++;
  }
  thePrintStream << "Try " << nbTry << ": rsvd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*DV*V1'-Hilbmn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DV*V1'-Hilbmn rsvd)");

  nbTry=0; nApprox=10.;
  while ( ( nApprox > tolJ ) & ( nbTry < maxTry ) )
  {
    r3svd(Hilbmn,U1,D1,V1, theTolerance);
    matD1=Matrix<Real>(D1);
    nApprox=norm2(U1*matD1*transpose(V1)-Hilbmn);
    nbTry++;
  }
  thePrintStream << "Try" << nbTry << ": r3svd truncation eps=" << theTolerance << ": D1=" << D1 << " norm2(U1*DW*V1'-Hilbmn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 0., tolJ,errors, "  norm2(U1*DW*V1'-Hilbmn)");

  //! test QR
  Matrix<Real> Q,R;
  qr(Hn,Q,R);
  theCout << "------------------ QR ---------------------------------------- " << eol;
  thePrintStream << "Q=" << Q << eol << "R=" << R << eol;
  nApprox=norm2(Q*R-Hn);
  thePrintStream << "qr(Hn,Q,R) : norm2(Q*R-Hn)=" << nApprox << eol;
  nbErrors+= checkValue( nApprox, 1.4318353347e-15, tol,errors, " norm2(Q*R-Hn)");
  nbErrors+= checkValues(Q, rootname+"/Q.in", tol, errors, " Q.", check);
  nbErrors+= checkValues(R, rootname+"/R.in", tol, errors, " R.", check);
  nbErrors+= checkValues(Hn, rootname+"/Hn.in", tol, errors, " Hn.", check);

  //! test eigen computation
  Matrix<Real> H10(10,_hilbertMatrix);
  Matrix<Complex> Xs;  //eigen vectors stored by rows !
  Vector<Complex> ls;  //eigen values
  eigs(Matrix<Real>(10,_hilbertMatrix),ls, Xs);
  thePrintStream << "----------------------- eigen ----------------------- " << eol;
  thePrintStream << "H10=" << H10 << eol;
  thePrintStream << "eigen values : " << ls << eol;
  thePrintStream << "eigen vectors : " << eol;
  thePrintStream << tran(Xs) << eol;
  nbErrors+= checkValues(H10, rootname+"/H10.in", tol, errors, " H10.", check);
  nbErrors+= checkValues(ls, rootname+"/ls.in", tol, errors, " ls.", check);
  nbErrors+= checkValues(tran(Xs), rootname+"/trXs.in", tol, errors, " tran(Xs)", check);

#endif

  //test SparseMatrix (real)
  SparseMatrix<Real> s0;         // null matrix
  theCout << "SparseMatrix<Real> s0: " << s0 << endl;
  SparseMatrix<Real> s5(5,5);    // void 5x5 matrix
  theCout << "SparseMatrix<Real> s5(5,5): " << s5 << endl;
  SparseMatrix<Real> s42(4,2);   // void 4x2 matrix
  theCout << "SparseMatrix<Real> s42(4,2): " << s42 << endl;
  std::vector<Real> d3(3,1);d3[0]=1;d3[1]=2;d3[2]=3;
  SparseMatrix<Real> s3(d3);   //diag constructor
  theCout << "SparseMatrix<Real> s3(d3): " << s3 << endl;
  SparseMatrix<Real> sd5(5,_idMatrix); //Id constructor
  theCout << "SparseMatrix<Real> sd5(5,_idMatrix): " << sd5 << endl;
  SparseMatrix<Real> s3b(s3);  //copy constructor
  theCout << "copy SparseMatrix<Real> s3b(s3): " << s3b << endl;
  SparseMatrix<Real> s3c;       // null matrix
  s3c=s3;                       // = operator
  theCout << "s3c=S3: " << s3b << endl;
  theCout << "dims of s42: " << s42.dims() << " type: " << s42.typeStr() << eol;
  s42(3,2)=32;
  s42(2,1)=21;
  theCout << "s42(3,2)=32; s42(2,1)=21 -> " << s42 << endl;
  s42(5,1)=51;  //auto extension
  theCout << "auto extension s42(5,1)=51 -> " << s42 << eol;
  String is=(s3.isSymmetric()?"true":"false"); theCout << "s3.isSymmetric(): " << is;
  is=(s3.isSkewSymmetric()?"true":"false"); theCout << " s3.isSkewSymmetric(): " << is;
  is=(s3.isSelfAdjoint()?"true":"false"); theCout << " s3.isSelfAdjoint(): " << is;
  is=(s3.isSkewAdjoint()?"true":"false"); theCout << " s3.isSkewAdjoint(): " << is << eol;
  theCout << "s42.transpose(): " << s42.transpose() << eol;
  theCout << "s42.adjoint(): " << s42.adjoint() << eol;
  theCout << "s42.conjugate(): " << s42.conjugate() << eol;
  s42.add(5,1,0.5);s42.add(2,2,0.5);
  theCout << "s42.add(5,1,0.5); s42.add(2,2,0.5), s42: " << s42 << eol;
  s42.sub(5,1,0.5);s42.sub(2,2,0.5);
  theCout << "s42.sub(5,1,0.5); s42.sub(2,2,0.5), s42: " << s42 << eol;
  s42+=sd5;
  theCout << "s42+=sd5: " << s42 << eol;
  s42+=5;
  theCout << "s42+=5: " << s42 << eol;
  s42-=sd5;
  theCout << "s42-=s5: " << s42 << eol;
  s42-=5;
  theCout << "s42-=5: " << s42 << eol;
  theCout << "s42.clean: " << s42.clean() << eol;
  s42*=10;
  theCout << "s42*=10: " << s42 << eol;
  s42/=10;
  theCout << "s42/=10: " << s42 << eol;
  theCout << "s42.real(): " << s42.real() << eol;
  theCout << "s42.imag(): " << s42.imag() << eol;
  std::vector<Real> v2(2,2);v2[1]=3;
  std::vector<Real> v5(5,1);v5[1]=2;v5[2]=3;v5[3]=4;v5[4]=5;
  theCout << "s42.diagProduct(v5): " << s42.diagProduct(v5) << eol;
  theCout << "s42.productDiag(v5): " << s42.productDiag(v5) << eol;
  theCout << "s42.squaredNorm(): " << s42.squaredNorm() << eol;
  theCout << "s42.resize(5,2): " << s42.resize(5,2) << eol;
  s42.saveToFile("s42.dat");
  SparseMatrix<Real> s42f; s42f.loadFromFile("s42.dat");
  theCout << "s42f.loadFromFile(\"s42.dat\"): " << s42f << eol;
  theCout << "abs(s42): " << abs(s42) << eol;
  is=((s42==s42f)?"true":"false");  theCout << "s42==s42f: " << is;
  is=((s42!=s42f)?"true":"false");  theCout << " s42!=s42f: " << is << eol;
  theCout << "+s42: " << (+s42) << eol;
  theCout << "-s42: " << (-s42) << eol;
  std::vector<Real> r5(5), r2(2);
  matvec(s42,v2.begin(),r5.begin());
  theCout << "matvec(s42,v2.begin(),r5.begin()): " << r5 << eol;
  vecmat(s42,v5.begin(),r2.begin());
  theCout << "vecmat(s42,v5.begin(),r2.begin()): " << r2 << eol;
  theCout << "(s42*4-2*s42)/2: " << (s42*4.-2.*s42)/2. << eol;
  theCout << "s42*v2: " << s42*v2 << eol;
  theCout << "v5*s42: " << v5*s42 << eol;
  theCout << "transpose(s42): " << transpose(s42) << eol;
  theCout << "adjoint(s42): " << adjoint(s42) << eol;
  theCout << "dimsOf(s42): " << dimsOf(s42) << eol;
  Matrix<Real> m42=s42.toMatrix();
  theCout << "s42.toMatrix(): " << eol << m42 << eol;
  SparseMatrix<Real> sm42(m42);
  theCout << "SparseMatrix(m42): " << sm42 << eol;

  //test SparseMatrix (complex)
  SparseMatrix<Complex> cs0;         // null matrix
  theCout << "SparseMatrix<Complex> cs0: " << cs0 << endl;
  SparseMatrix<Complex> cs5(5,5);    // void 5x5 matrix
  theCout << "SparseMatrix<Complex> cs5(5,5): " << cs5 << endl;
  SparseMatrix<Complex> cs42(4,2);   // void 4x2 matrix
  theCout << "SparseMatrix<Complex> cs42(4,2): " << cs42 << endl;
  std::vector<Complex> cd3(3,1);cd3[0]=1+i_;cd3[1]=2+i_;cd3[2]=1+3*i_;
  SparseMatrix<Complex> cs3(cd3);   //diag constructor
  theCout << "SparseMatrix<Complex> cs3(d3): " << cs3 << endl;
  SparseMatrix<Complex> csd5(5,_idMatrix); //Id constructor
  theCout << "SparseMatrix<Complex> csd5(5,_idMatrix): " << csd5 << endl;
  SparseMatrix<Complex> cs3b(cs3);  //copy constructor
  theCout << "copy SparseMatrix<Complex> cs3b(cs3): " << cs3b << endl;
  SparseMatrix<Complex> cs3c;       // null matrix
  cs3c=cs3;                       // = operator
  theCout << "cs3c=S3: " << cs3b << endl;
  theCout << "dims of cs42: " << cs42.dims() << " type: " << cs42.typeStr() << eol;
  cs42(3,2)=32*i_;
  cs42(2,1)=21*i_;
  theCout << "cs42(3,2)=32; cs42(2,1)=21 -> " << cs42 << endl;
  cs42(5,1)=51*i_;  //auto extension
  theCout << "auto extension cs42(5,1)=51 -> " << cs42 << eol;
  is=(cs3.isSymmetric()?"true":"false"); theCout << "cs3.isSymmetric(): " << is;
  is=(cs3.isSkewSymmetric()?"true":"false"); theCout << " cs3.isSkewSymmetric(): " << is;
  is=(cs3.isSelfAdjoint()?"true":"false"); theCout << " cs3.isSelfAdjoint(): " << is;
  is=(cs3.isSkewAdjoint()?"true":"false"); theCout << " cs3.isSkewAdjoint(): " << is << eol;
  theCout << "cs42.transpose(): " << cs42.transpose() << eol;
  theCout << "cs42.adjoint(): " << cs42.adjoint() << eol;
  theCout << "cs42.conjugate(): " << cs42.conjugate() << eol;
  cs42.add(5,1,0.5);cs42.add(2,2,0.5);
  theCout << "cs42.add(5,1,1); cs42.add(2,2,1), cs42: " << cs42 << eol;
  cs42.sub(5,1,0.5);cs42.sub(2,2,0.5);
  theCout << "cs42.sub(5,1,1); cs42.sub(2,2,1), cs42: " << cs42 << eol;
  cs42+=csd5;
  theCout << "cs42+=csd5: " << cs42 << eol;
  cs42+=5;
  theCout << "cs42+=5: " << cs42 << eol;
  cs42-=csd5;
  theCout << "cs42-=cs5: " << cs42 << eol;
  cs42-=5;
  theCout << "cs42-=5: " << cs42 << eol;
  theCout << "cs42.clean: " << cs42.clean() << eol;
  cs42*=10;
  theCout << "cs42*=10: " << cs42 << eol;
  cs42/=10;
  theCout << "cs42/=10: " << cs42 << eol;
  theCout << "cs42.real(): " << cs42.real() << eol;
  theCout << "cs42.imag(): " << cs42.imag() << eol;
  std::vector<Complex> cv2(2,2);cv2[1]=3*i_;
  std::vector<Complex> cv5(5,1);cv5[1]=2*i_;cv5[2]=3*i_;cv5[3]=4*i_;cv5[4]=5*i_;
  theCout << "cs42.diagProduct(cv5): " << cs42.diagProduct(cv5) << eol;
  theCout << "cs42.productDiag(cv5): " << cs42.productDiag(cv5) << eol;
  theCout << "cs42.squaredNorm(): " << cs42.squaredNorm() << eol;
  theCout << "cs42.resize(5,2): " << cs42.resize(5,2) << eol;
  cs42.saveToFile("cs42.dat");
  SparseMatrix<Complex> cs42f; cs42f.loadFromFile("cs42.dat");
  theCout << "s42f.loadFromFile(\"s42.dat\"): " << cs42f << eol;
  theCout << "abs(cs42): " << abs(cs42) << eol;
  is=((cs42==cs42f)?"true":"false");  theCout << "cs42==cs42f: " << is;
  is=((cs42!=cs42f)?"true":"false");  theCout << " cs42!=cs42f: " << is << eol;
  theCout << "+cs42: " << (+cs42) << eol;
  theCout << "-cs42: " << (-cs42) << eol;
  std::vector<Complex> cr5(5), cr2(2);
  matvec(cs42,cv2.begin(),cr5.begin());
  theCout << "matvec(cs42,cv2.begin(),cr5.begin()): " << cr5 << eol;
  vecmat(cs42,cv5.begin(),cr2.begin());
  theCout << "vecmat(cs42,cv5.begin(),cr2.begin()): " << cr2 << eol;
  theCout << "(cs42*4-2*s42)/2: " << (cs42*4.-2.*cs42)/2. << eol;
  theCout << "cs42*cv2: " << cs42*cv2 << eol;
  theCout << "cv5*cs42: " << cv5*cs42 << eol;
  theCout << "transpose(cs42): " << transpose(cs42) << eol;
  theCout << "adjoint(cs42): " << adjoint(cs42) << eol;
  theCout << "dimsOf(cs42): " << dimsOf(cs42) << eol;
  Matrix<Complex> cm42=cs42.toMatrix();
  theCout << "cs42.toMatrix(): " << eol << cm42 << eol;
  SparseMatrix<Complex> csm42(cm42);
  theCout << "SparseMatrix(cm42): " << csm42 << eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
  

}

}
