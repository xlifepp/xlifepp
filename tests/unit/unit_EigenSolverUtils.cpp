/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; withssout even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/*!
  \file unit_EigenSolverUtils.cpp
  \author ManhHa NGUYEN
  \since 23 jan 2013
  \date 4 Mars 2013

  Low level tests of functionalities for (dense) eigen problem (eigenCore).
  Almost functionalities are checked.
  This function may either creates a reference file storing the results (check=true)
  or compares results to those stored in the reference file (check=true)
  It returns reporting information in a string
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace unit_EigenSolverUtils {

void unit_EigenSolverUtils(int argc, char* argv[], bool check)
{
  String rootname = "unit_EigenSolverUtils";
  trace_p->push(rootname);
  std::stringstream ssout; // string stream receiving results
  ssout.precision(testPrec);
  verboseLevel(10);

  Number nbErrors = 0;
  String errors;
  Real tol=0.001;
  
  const Number rowNum = 5;
  const Number colNum = 5;
  
  const std::string rMatrixDataSym(inputsPathTo(rootname+"/rSym5.data"));
  const std::string rMatrixSymPos(inputsPathTo(rootname+"/rPosDef5.data"));
  const std::string cMatrixDataSym(inputsPathTo(rootname+"/cSelfAdjoint5.data"));
  const std::string cMatrixSymPos(inputsPathTo(rootname+"/cPosDef5.data"));
  
  VectorEigenDense<Real> rVec(rowNum), rEss(rowNum - 1), rTail, rRowTail(colNum - 1, 0.0, _row), rRow(colNum, 0.0, _row);
  VectorEigenDense<Complex> cVec(rowNum), cEss(rowNum - 1), cTail, cRowTail(colNum - 1, 0.0, _row), cRow(colNum, 0.0, _row);
  MatrixEigenDense<Real> rMat(rowNum, colNum), rMatPos(rowNum, colNum), rMatRes(rowNum, colNum), rMatChol(rowNum, colNum);
  MatrixEigenDense<Complex> cMat(rowNum, colNum), cMatPos(rowNum, colNum), cMatRes(rowNum, colNum), cMatChol(rowNum, colNum);
  
  Real rTau, beta;
  Complex cTau;
  
  Indexing idx(4);
  idx[0] = idx[1] = 1;
  idx[2] = idx[3] = rowNum - 1;
  
  rVec[0] = 8.40188; rVec[1] = 7.9844; rVec[2] = 3.35223; rVec[3] = 5.5397; rVec[4] = 3.64784;
  cVec[0] = Complex(7.83099,3.94383); cVec[1] = Complex(1.97551,9.11647); cVec[2] = Complex(2.77775,7.6823); cVec[3] = Complex(6.28871,4.77397); cVec[4] = Complex(9.5223,5.13401);

  rMat.loadFromFile(rMatrixDataSym.c_str()); 
  rMatPos.loadFromFile(rMatrixSymPos.c_str());
  cMat.loadFromFile(cMatrixDataSym.c_str());
  cMatPos.loadFromFile(cMatrixSymPos.c_str());


  std::vector<VectorEigenDense<Real> > VrVec(9);
  std::vector<Real > VrVal(9);
  
  ssout << "----------------------------Test VectorEigenDense--------------------" << "\n";
  ssout << "----------------------------Real vector--------------------" << "\n";
  ssout << "Real vector rVec = " << rVec << "\n"; VrVec[0]=rVec;
  rVec.makeHouseHolder(rEss, rTau, beta);
  ssout << "Real vector Essential cEss = " << rEss << "\n"; VrVec[1]=rEss; 
  ssout << "Real value tau rTau " << rTau << "\n"; VrVal[0]=rTau;
  ssout << "Real value beta " << beta << "\n"; VrVal[1]=beta;
  rVec.makeHouseHolderInPlace(rTau, beta);
  ssout << "Make houseHolder in place \n";
  ssout << "Real vector rVec = "  << rVec << "\n"; VrVec[2]=rVec;
  ssout << "Real value tau rTau " << rTau << "\n"; VrVal[2]=rTau;
  ssout << "Real value beta " << beta << "\n";  VrVal[3]=beta;
  rVec.makeHouseHolderInPlace(rTau, beta, rowNum - 1);
  ssout << "Make houseHolder in place in tail \n";
  ssout << "Real vector rVec = "  << rVec << "\n";VrVec[3]=rVec;
  ssout << "Real value tau rTau " << rTau << "\n";  VrVal[4]=rTau;
  ssout << "Real value beta " << beta << "\n";  VrVal[5]=beta;
  rTail = rVec.tail(rowNum - 1);
  ssout << "Real tail vector of rVec " << rTail << "\n"; VrVec[4]=rTail;
  rTail.makeHouseHolderInPlace(rTau, beta);
  ssout << "Real (modified) tail vector of rVec " << rTail << "\n";VrVec[5]=rTail;
  ssout << "Real vector rVec before being replaced by tail" << rVec << "\n"; VrVec[6]=rVec;
  rVec.tail(rowNum - 1, rTail);
  ssout << "Real vector rVec after being replaced by tail" << rVec << "\n";VrVec[7]=rVec;
  rVec = adjointVec(rVec);
  ssout << "Real vector rVec after being adjointed " << rVec << "\n"; VrVec[8]=rVec;
  ssout << "Min element of vector = " << rVec.minElement(0, rowNum - 1) <<  "\n";  VrVal[6]=rVec.minElement(0, rowNum - 1);
  ssout << "Dot product rVec*rVec = " << rVec.dotProduct(rVec) << "\n"; VrVal[7]=rVec.dotProduct(rVec);

  nbErrors += checkValues(VrVal,rootname+"/RealValueVectorEigenDense.in" ,  tol, errors, "Real values of VectorEigenDense", check);
  nbErrors += checkValues(VrVec,rootname+"/RealVectorVectorEigenDense.in" ,  tol, errors, "Real vectors of VectorEigenDense", check);

  ssout << "----------------------------Complex vector--------------------" << "\n";
  std::vector<VectorEigenDense<Complex> > VcVec(7);
  std::vector<Complex > VcVal(7);

  ssout << "Complex vector cVec = " << cVec << "\n"; VcVec[0]=cVec;
  cVec.makeHouseHolder(cEss, cTau, beta);
  ssout << "Complex vector Essential cEss = " << cEss << "\n";  VcVec[1]=cVec;
  ssout << "Complex value tau cTau " << cTau << "\n";VcVal[0]=cTau;
  ssout << "Complex value beta " << beta << "\n";VcVal[1]=beta;
  cVec.makeHouseHolderInPlace(cTau, beta);
  ssout << "Make houseHolder in place \n";
  ssout << "Complex value tau cTau " << cTau << "\n";VcVal[2]=cTau;
  ssout << "Complex value beta " << beta << "\n";VcVal[3]=beta;
  cVec.makeHouseHolderInPlace(cTau, beta, rowNum - 1);
  ssout << "Make houseHolder in place in tail \n";
  ssout << "Complex value tau cTau " << cTau << "\n";VcVal[4]=cTau;
  ssout << "Complex value beta " << beta << "\n";VcVal[5]=beta;
  cTail = cVec.tail(rowNum - 1);
  ssout << "Complex tail vector of cVec " << cTail << "\n"; VcVec[2]=cTail;
  cTail.makeHouseHolderInPlace(cTau, beta);
  ssout << "Complex (modified) tail vector of cVec " << cTail << "\n"; VcVec[3]=cTail;
  ssout << "Complex vector cVec before being replaced by tail" << cVec << "\n"; VcVec[4]=cVec;
  cVec.tail(rowNum - 1, cTail);
  ssout << "Complex vector cVec after being replaced by tail" << cVec << "\n"; VcVec[5]=cVec;
  cVec = adjointVec(cVec);
  ssout << "Complex vector cVec after being adjointed " << cVec << "\n"; VcVec[6]=cVec;
  ssout << "Dot product cVec*cVec = " << roundToZero(cVec.dotProduct(cVec)) << "\n";VcVal[6]=roundToZero(cVec.dotProduct(cVec));
  nbErrors += checkValues(VcVal, rootname+"/ComplexValueVectorEigenDense.in" ,  tol, errors, "Complex values of VectorEigenDense ", check);
  nbErrors += checkValues(VcVec, rootname+"/ComplexVectorVectorEigenDense.in" ,  tol, errors, "Complex vectors of VectorEigenDense ", check);

  
  ssout << "----------------------------Test MatrixEigenDense--------------------" << "\n";
  ssout << "----------------------------Real matrix--------------------" << "\n";
  std::vector<MatrixEigenDense<Real> > VrMat(21);
  std::vector<VectorEigenDense<Real> > VrVec_2(8);

  ssout << "Real matrix rMat =" << rMat << "\n"; VrMat[0]=rMat;
  rMat.applyHouseholderOnTheLeft(rEss, rTau);
  ssout << "Applying house holder on the left of rMat " << rMat << "\n"; VrMat[1]=rMat;
  rMat.applyHouseholderOnTheRight(rEss, rTau);
  ssout << "Applying house holder on the right of rMat " << rMat << "\n"; VrMat[2]=rMat;
  rMat.multSubMatVecVec(idx, rTail, rTail);
  ssout << "sub-rMat * vec = " << rTail << "\n"; VrVec_2[0]=rTail;
  rMat.multVecSubMatVec(idx, transposeVec(rTail), rRowTail);
  ssout << "vec * sub-rMat = " << rTail << "\n";  VrVec_2[1]=rTail;
  rMat.multVecVecSubMat(idx, rTail, transposeVec(rTail));
  ssout << "vec * vec = " << rMat << "\n"; VrMat[3]=rMat;
  rMat.rankUpdate(idx, rTail, rTail, -1);
  ssout << "Rank update " << rMat << "\n"; VrMat[4]=rMat;
  ssout << "Row 0 of matrix " << rMat.rowVector(0) << "\n"; VrVec_2[2]=rMat.rowVector(0);
  ssout << "Column 0 of matrix " << rMat.columnVector(0) << "\n"; VrVec_2[3]=rMat.columnVector(0);
  rMat.rowVector(0, rRow);
  ssout << "Replace row 0 of matrix " << rMat << "\n";  VrMat[5]=rMat;
  rMat.columnVector(0, rVec);
  ssout << "Replace column 0 of matrix " << rMat << "\n";  VrMat[6]=rMat;
  rMat.addAssignRow(0, rRow, 1);
  ssout << "Add assigment row 0 of matrix " << rMat << "\n"; VrMat[7]=rMat;
  rMat.addAssignCol(0, rVec, 1);
  ssout << "Add assigment column 0 of matrix " << rMat << "\n";  VrMat[8]=rMat;
  rMat.swapRows(0, 1);
  ssout << "Swap row 0 and 1 " << rMat << "\n";  VrMat[9]=rMat;
  rMat.swapCols(0, 1);
  ssout << "Swap column 0 and 1 " << rMat << "\n";  VrMat[10]=rMat;
  ssout << "Diagonal " << rMat.diagonal().real() << "\n"; VrVec_2[4]=rMat.diagonal().real();
  ssout << "Sub-Diagonal " << rMat.subDiagonal().real() << "\n"; VrVec_2[5]=rMat.subDiagonal().real();
  
  rMat.coeffRef(2, 2) += 2.0;
  ssout << "Modified coefficient (2,2) " << rMat.coeff(2, 2) << "\n";
  nbErrors += checkValue(rMat.coeff(2, 2), 1.9778243877, tol, errors, "Modified coefficient (2,2)");
  ssout << "Sub - matrix " << rMat << "\n"; VrMat[11]=rMat;
  ssout << "Matrix " << rMat << "\n";  VrMat[12]=rMat;
  rMat.replace(rMat, 0, 0, rowNum, rowNum);
  ssout << "Matrix after substituted " << rMat << "\n"; VrMat[13]=rMat;
  ssout << "Bottom right corner matrix " << rMat.bottomRightCorner(rowNum - 1, colNum - 1) << "\n"; VrMat[13]=rMat;
  ssout << "Block row 0  " << rMat.blockRow(0, 0, colNum - 1) << "\n"; VrVec_2[6]=rMat.blockRow(0, 0, colNum - 1);
  ssout << "Block column 0  " << rMat.blockCol(0, 0, rowNum - 1) << "\n"; VrVec_2[7]=rMat.blockCol(0, 0, rowNum - 1);
  
  multMatMat(rMat, rMat, rMatRes);
  ssout << "Matrix multiplication mat*mat= " << rMatRes << "\n"; VrMat[14]=rMatRes;
  rMatRes.setZeroStrictUpper();
  ssout << "Set Zero Strict Upper " << rMatRes << "\n"; VrMat[15]=rMatRes;
  
  ssout << "Kronecker product " << kroneckerProduct(rMat, rMat) << "\n";

  ssout << "Positive definite matrix " << rMatPos << "\n"; VrMat[16]=rMatPos;
  rMatChol = rMatPos.cholesky();
  ssout << "Cholesky matrix " << rMatChol << "\n"; VrMat[17]=rMatPos;
  rMatChol.solveCholeskyInplaceLower(rMat, rMatRes);
  ssout << "Solve cholesky Inplace Lower" << rMatRes << "\n"; VrMat[17]=rMatRes;
  rMatChol.solveCholeskyInplaceUpper(rMatRes, rMatRes);
  ssout << "Solve cholesky Inplace Upper" << rMatRes << "\n"; VrMat[18]=rMatRes;
  ssout << "Transpose matrix " << transpose(rMatRes) << "\n"; VrMat[19]=rMatRes;
  rMatRes.setZeroLowerTriangle();
  ssout << "Zero ssout lower part " << rMatRes << "\n"; VrMat[20]=rMatRes;

  nbErrors += checkValues(VrVec_2,rootname+"/RealVectorVectorEigenDense_2.in" ,  tol, errors, "2/  vector of Real VectorEigenDense", check);
  nbErrors += checkValues(VrMat,rootname+"/RealVectorMatrixEigenDense.in" ,  tol, errors, "  vector of Real MatrixEigenDense", check);

  
  ssout << "----------------------------Complex matrix--------------------" << "\n";
  std::vector<MatrixEigenDense<Complex> > VcMat(23);
  std::vector<VectorEigenDense<Complex> > VcVec_3(4);
  ssout << "Complex matrix cMat =" << cMat << "\n"; VcMat[0]=cMat;
  cMat.applyHouseholderOnTheLeft(cEss, cTau);
  ssout << "Applying house holder on the left of cMat " << cMat << "\n"; VcMat[1]=cMat;
  cMat.applyHouseholderOnTheRight(cEss, cTau);
  ssout << "Applying house holder on the right of cMat " << cMat << "\n"; VcMat[2]=cMat;
  cMat.multSubMatVecVec(idx, cTail, cTail);
  ssout << "sub-cMat * vec = " << cTail << "\n";  VcVec_3[0]=cTail;
  cMat.multVecSubMatVec(idx, transposeVec(cTail), cRowTail);
  ssout << "vec * sub-cMat = " << cTail << "\n"; VcVec_3[1]=cTail;
  cMat.multVecVecSubMat(idx, cTail, transposeVec(cTail));
  ssout << "vec * vec = " << cMat << "\n"; VcMat[3]=cMat;
  cMat.rankUpdate(idx, cTail, cTail, -1);
  ssout << "Rank update " << cMat << "\n"; VcMat[4]=cMat;
  ssout << "Row 0 of matrix " << cMat.rowVector(0) << "\n";
  ssout << "Column 0 of matrix " << cMat.columnVector(0) << "\n";
  cMat.rowVector(0, cRow);
  ssout << "Replace row 0 of matrix " << cMat << "\n"; VcMat[5]=cMat;
  cMat.columnVector(0, cVec);
  ssout << "Replace column 0 of matrix " << cMat << "\n";VcMat[6]=cMat;
  cMat.addAssignRow(0, cRow, 1);
  ssout << "Add assigment row 0 of matrix " << cMat << "\n";VcMat[7]=cMat;
  cMat.addAssignCol(0, cVec, 1);
  ssout << "Add assigment column 0 of matrix " << cMat << "\n";VcMat[8]=cMat;
  cMat.swapRows(0, 1);
  ssout << "Swap row 0 and 1 " << cMat << "\n";VcMat[9]=cMat;
  cMat.swapCols(0, 1);
  ssout << "Swap column 0 and 1 " << cMat << "\n"; VcMat[9]=cMat;
  ssout << "Diagonal " << cMat.diagonal().real() << "\n";
  ssout << "Sub-Diagonal " << cMat.subDiagonal().real() << "\n";
  
  cMat.coeffRef(2, 2) += 2.0;
  ssout << "Modified coefficient (2,2) " << cMat.coeff(2, 2) << "\n";
  nbErrors += checkValue(cMat.coeff(2, 2), Complex (1.9681863057,-0.017677340271), tol, errors, "Modified complex coefficient (2,2)");
  ssout << "Sub - matrix " << cMat << "\n"; VcMat[10]=cMat;
  ssout << "Matrix " << cMat << "\n";VcMat[11]=cMat;
  cMat.replace(cMat, 0, 0, rowNum, rowNum);
  ssout << "Matrix after substituted " << cMat << "\n"; VcMat[12]=cMat;
  ssout << "Bottom right corner matrix " << cMat.bottomRightCorner(rowNum - 1, colNum - 1) << "\n"; VcMat[13]= cMat.bottomRightCorner(rowNum - 1, colNum - 1);
  ssout << "Block row 0  " << cMat.blockRow(0, 0, colNum - 1) << "\n"; VcVec_3[2]=cMat.blockRow(0, 0, colNum - 1);
  ssout << "Block column 0  " << cMat.blockCol(0, 0, rowNum - 1) << "\n"; VcVec_3[3]=cMat.blockCol(0, 0, rowNum - 1);
  
  multMatMat(cMat, cMat, cMatRes);
  ssout << "Matrix multiplication mat*mat= " << cMatRes << "\n"; VcMat[14]=cMatRes;
  cMatRes.setZeroStrictUpper();
  ssout << "Set Zero Strict Upper " << cMatRes << "\n"; VcMat[15]=cMatRes;
  
  ssout << "Kronecker product " << kroneckerProduct(cMat, cMat) << "\n";

  ssout << "Positive definite matrix " << cMatPos << "\n"; VcMat[16]=cMatPos;
  cMatChol = cMatPos.cholesky();
  ssout << "Cholesky matrix " << cMatChol << "\n";VcMat[17]=cMatChol;
  cMatChol.solveCholeskyInplaceLower(cMat, cMatRes);
  ssout << "Solve cholesky Inplace Lower" << cMatRes << "\n";VcMat[18]=cMatRes;
  cMatChol.solveCholeskyInplaceUpper(cMatRes, cMatRes);
  ssout << "Solve cholesky Inplace Upper" << cMatRes << "\n"; VcMat[19]=cMatRes;
  ssout << "Transpose matrix " << transpose(cMatRes) << "\n";VcMat[20]=transpose(cMatRes);
  ssout << "Conjugate matrix " << conj(transpose(cMatRes)) << "\n"; VcMat[21]=conj(transpose(cMatRes));
  cMatRes.setZeroLowerTriangle();
  ssout << "Zero ssout lower part " << cMatRes << "\n"; VcMat[22]=cMatRes;

  nbErrors += checkValues(VcVec_3, rootname+"/ComplexVectorVectorEigenDense_3.in" ,  tol, errors, "3/  vector of Real VectorEigenDense", check);
  nbErrors += checkValues(VcMat, rootname+"/ComplexVectorMatrixEigenDense.in",  tol, errors, "  vector of Cmplx MatrixEigenDense", check);


  ssout << "----------------------------Test Jacobi Rotation--------------------" << "\n";
  std::vector<MatrixEigenDense<Real> > VrMatJR(2);
  JacobiRotation<Real> rot;
  Real x = 10 * (Real)(std::rand() / RAND_MAX);
  Real z = 10 * (Real)(std::rand() / RAND_MAX);
  rot.makeGivens(x, z);
  Real cxmsz=rot.c() * x + rot.s() * z;
  Real sxpcz=rot.s() * x + rot.c() * z;
  ssout << "Jacobi coefficient c = " << rot.c() << "\n";
  ssout << "Jacobi coefficient s = " << rot.s() << "\n";
  ssout << " c*x - s*z =" << cxmsz << "\n";
  ssout << " s*x + c*z =" << sxpcz << "\n";
  ssout << "Matrix before rotation " << rMat << "\n"; VrMatJR[0]=rMat;
  rMat.applyOnTheRight(1, 2, rot);
  ssout << "Matrix after rotation " << rMat << "\n";  VrMatJR[1]=rMat;
  nbErrors += checkValue(rot.c(), 1., tol, errors, "rot.c()");
  nbErrors += checkValue(rot.s(), 0., tol, errors, "rot.s()");
  nbErrors += checkValue(cxmsz, 0., tol, errors, "c*x - s*z ");
  nbErrors += checkValue(sxpcz, 0., tol, errors, "s*x + c*z ");
  nbErrors += checkValues(VrMatJR,rootname+"/RealVectorMatrixJR.in" ,  tol, errors, "  vector of Real MatrixJR", check);

  
  std::vector<MatrixEigenDense<Complex> > VcMatJR(2);
  JacobiRotation<Complex> cplRot;
  Complex cx = 10 * (Complex)(std::rand() / RAND_MAX);
  Complex cz = 10 * (Complex)(std::rand() / RAND_MAX);
  Complex Ccxmsz=cplRot.c() * cx + cplRot.s() * cz;
  Complex Csxpcz=rot.s() * x + rot.c() * z;
  cplRot.makeGivens(cx, cz);
  ssout << "Jacobi coefficient c = " << cplRot.c() << "\n";
  ssout << "Jacobi coefficient s = " << cplRot.s() << "\n";
  ssout << " c*x - s*z =" << Ccxmsz << "\n";
  ssout << " s*x + c*z =" << Csxpcz << "\n";
  ssout << "Matrix before rotation " << cMat << "\n";VcMatJR[0]=cMat;
  cMat.applyOnTheRight(1, 2, cplRot);
  ssout << "Matrix after rotation " << cMat << "\n";VcMatJR[1]=cMat;
  nbErrors += checkValue(cplRot.c(), Complex (1.,0.), tol, errors, "cplRot.c()");
  nbErrors += checkValue(cplRot.s(), Complex(0,0), tol, errors, "cplRot.s()");
  nbErrors += checkValue(Ccxmsz, Complex(0,0), tol, errors, "c*x - s*z ");
  nbErrors += checkValue(Csxpcz, Complex(0,0), tol, errors, "s*x + c*z ");
  nbErrors += checkValues(VrMatJR, rootname+"/ComplexVectorMatrixJR.in" ,  tol, errors, "  vector of Complex MatrixJR", check);

  
  ssout << "----------------------------Test Smart Pointer--------------------" << "\n";

  Number n = 3;
  std::vector< std::vector<Number> > elts((n - 1) * (n - 1), std::vector<Number>(4));
  for(Number j = 1; j <= n - 1; j++)
  for(Number i = 1; i <= n - 1; i++)
  {
    Number e = (j - 1) * (n - 1) + i - 1, p = (j - 1) * n + i;
    elts[e][0] = p; elts[e][1] = p + 1; elts[e][2] = p + n; elts[e][3] = p + n + 1;
  }

  SmartPtr<MatrixStorage> csr(_smPtrNull);
  csr = SmartPtr<RowCsStorage>(new RowCsStorage(n * n, n * n, elts, elts));
  if (_smPtrNull == csr) ssout << "Null smart pointer " << "\n";
  else csr->visual(ssout);
  nbErrors += checkValue(ssout, rootname+"/csrSP.in", errors, "Test smart pointer", check);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }
  
  trace_p->pop();
  

}

}
