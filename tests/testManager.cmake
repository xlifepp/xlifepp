# function checkExternalDependencies
function(checkExternalDependencies filename OK_FOR_GLOBAL_TEST)
  file(READ ${filename} fileData)
  string(REGEX MATCH arpackSolve arpackSolve_found ${fileData})
  string(REGEX MATCH umfpackSolve umfpackSolve_found ${fileData})
  set(OK_FOR_GLOBAL_TEST TRUE PARENT_SCOPE)
  if (NOT XLIFEPP_ENABLE_ARPACKPP AND NOT arpackSolve_found STREQUAL "")
    set(OK_FOR_GLOBAL_TEST FALSE PARENT_SCOPE)
  endif()
  if ((NOT XLIFEPP_ENABLE_UMFPACK AND NOT XLIFEPP_ENABLE_SUITESPARSE) AND NOT umfpackSolve_found STREQUAL "")
    set(OK_FOR_GLOBAL_TEST FALSE PARENT_SCOPE)
  endif()

endfunction(checkExternalDependencies)

# function testManager
# this function is devoted to define targets about a family of tests
# Arguments
#   testfamily : name of the family of tests : unit, sys, dev or all
#   testsrcs : list of source files for the family of tests
function(testManager testfamily testsrcs)
  set(ALLTEST_CODE "")
  string(TOLOWER ${CMAKE_SYSTEM_NAME} system_name)
  foreach (testsrc ${${testsrcs}})
    checkExternalDependencies(${testsrc} OK_FOR_GLOBAL_TEST)
    get_filename_component(testfilename ${testsrc} NAME_WE)
    set(PRINT_FILE "thePrintFile = resPathTo(\"${testfilename}\")+\".res\";")
    set(TEST_CODE "")
    set(TEST_CODE "${TEST_CODE}${testfilename}::${testfilename}(argc, argv, check);\n  ")
    if (${OK_FOR_GLOBAL_TEST})
      set(ALLTEST_CODE "${ALLTEST_CODE}${testfilename}::${testfilename}(argc, argv, check);\n  ")
      set(ALLTEST_CODE "${ALLTEST_CODE}testClean();\n  ")
      list(APPEND srcDependencies ${testsrc})
    endif()
    set(TESTLIST_CODE "${TESTLIST_CODE}void ${testfilename}(int argc, char* argv[], bool check);\n")
        
    # if the family is not all, we define targets for each test file
    if (NOT ${testfamily} STREQUAL "all")
      # create the main source file for the test
      # variables to be set :
      # TEST_CODE
      # PRINT_FILE
      configure_file(${CMAKE_SOURCE_DIR}/etc/templates/main_tests.cmake.cpp ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}_tmp.cpp)
      file(READ ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}_tmp.cpp mainTestData)
      if (EXISTS ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}.cpp)
        file(READ ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}.cpp existingMainTestData)
        if (NOT mainTestData STREQUAL existingMainTestData)
          configure_file(${CMAKE_SOURCE_DIR}/etc/templates/main_tests.cmake.cpp ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}.cpp)
        endif()
      else()
        configure_file(${CMAKE_SOURCE_DIR}/etc/templates/main_tests.cmake.cpp ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}.cpp)
      endif()
      file(REMOVE ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}_tmp.cpp)

      set(TEST_EXE ${testfilename}-${CMAKE_SYSTEM_PROCESSOR}-${system_name}-${XLIFEPP_CXX_COMPILER}-${CMAKE_BUILD_TYPE})
      # test compilation
      file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin/${testfilename})
      add_executable(${TEST_EXE} EXCLUDE_FROM_ALL ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfilename}.cpp ${CMAKE_SOURCE_DIR}/tests/testUtils.cpp ${testsrc})
      set_target_properties(${TEST_EXE} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin/${testfilename})
      if (XLIFEPP_ENABLE_OMP)
        if (NOT ${COMPILER_IS_CLANGPP} STREQUAL "-1")
          target_link_libraries(${TEST_EXE} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${libexts} "-L/usr/local/opt/libomp/lib" "-lomp")
        else()
          target_link_libraries(${TEST_EXE} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${libexts})
        endif()
      else()
        target_link_libraries(${TEST_EXE} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${libexts})
      endif()
      if (XLIFEPP_ENABLE_AMOS)
        if (XLIFEPP_FROM_SRCS)
          if(NOT EXISTS ${XLIFEPP_AMOS_LIB})
            add_dependencies(${TEST_EXE} ${XLIFEPP_LIBS} amos)
          endif()
        endif()
      endif()
      add_dependencies(tests ${TEST_EXE})
      if (testfamily MATCHES "unit")
        add_test(NAME ${testfilename} COMMAND ${CMAKE_CURRENT_BINARY_DIR}/bin/${testfilename}/${TEST_EXE} -c true WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin/${testfilename})
        set_tests_properties(${testfilename} PROPERTIES FAIL_REGULAR_EXPRESSION "Error XLiFE")
      endif()
    endif()
  endforeach()
  
  set(TARGET_SUFFIX "_${testfamily}")
  if (${testfamily} STREQUAL "all")
    set(TARGET_SUFFIX "")
  endif()
  
  ######## we define a test executing all tests of the family #######
  if (${BUILD_FAMILY})
    set(TEST_CODE ${ALLTEST_CODE})
    # create the main source file for the test
    # variables to be set :
    # TEST_CODE
    # PRINT_FILE
    set(PRINT_FILE "thePrintFile = resPathTo(\"${testfamily}\")+\".res\";")
    configure_file(${CMAKE_SOURCE_DIR}/etc/templates/main_tests.cmake.cpp ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}_tmp.cpp)
    file(READ ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}_tmp.cpp mainTestData)
    if (EXISTS ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}.cpp)
      file(READ ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}.cpp existingMainTestData)
      if (NOT mainTestData STREQUAL existingMainTestData)
        configure_file(${CMAKE_SOURCE_DIR}/etc/templates/main_tests.cmake.cpp ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}.cpp)
      endif()
    else()
      configure_file(${CMAKE_SOURCE_DIR}/etc/templates/main_tests.cmake.cpp ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}.cpp)
    endif()
    file(REMOVE ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}_tmp.cpp)
  
    set(TEST_EXE ${testfamily}-${CMAKE_SYSTEM_PROCESSOR}-${system_name}-${XLIFEPP_CXX_COMPILER}-${CMAKE_BUILD_TYPE})
    # test compilation
    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin/${testfamily})
    add_executable(${TEST_EXE} EXCLUDE_FROM_ALL ${CMAKE_CURRENT_BINARY_DIR}/builds/main_${testfamily}.cpp ${CMAKE_SOURCE_DIR}/tests/testUtils.cpp ${srcDependencies})
    set_target_properties(${TEST_EXE} PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin/${testfamily})

    if (XLIFEPP_ENABLE_OMP)
      if (NOT ${COMPILER_IS_CLANGPP} STREQUAL "-1")
        target_link_libraries(${TEST_EXE} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${libexts} "-L/usr/local/opt/libomp/lib" "-lomp")
      else()
          target_link_libraries(${TEST_EXE} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${libexts})
      endif()
    else()
      target_link_libraries(${TEST_EXE} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${XLIFEPP_LIBS} ${libexts})
    endif()
    if (XLIFEPP_ENABLE_AMOS)
      if (XLIFEPP_FROM_SRCS)
        if(NOT EXISTS ${XLIFEPP_AMOS_LIB})
          add_dependencies(${TEST_EXE} ${XLIFEPP_LIBS} amos)
        endif()
      endif()
    endif()
    add_dependencies(tests ${TEST_EXE})
  endif()
endfunction(testManager)
