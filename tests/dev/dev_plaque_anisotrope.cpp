/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file dev_plaque_anisotrope.cpp
	\since 5 mai 2014
	\date 5 mai 2014
	\author Eric Lunéville

	anisotropic plate with hybrid DtN - half waveguide
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace dev_plaque_anisotrope {

//map  sigmaP (2D) -> [0,1] (1D)  return a vector of size 1 !
Vector<Real> mapS(const Point& P, Parameters& pa = defaultParameters)
{
  return Vector<Real>(1,P(2));
}

void dev_plaque_anisotrope(int argc, char* argv[], bool check)
{
  String rootname = "dev_plaque_anisotrope";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(50);
  String errors;
  Number nbErrors = 0;

  //elaticity data
  Real lambda=112.134, mu=83.53, omg2=4*pi_*pi_/25, rho=7.86, h=25.;
  Matrix<Real> C(3,3);  //Voigt form of C in sigma=C*epsilon
  C(1,1)=2*mu+lambda; C(2,2)=2*mu+lambda; C(3,3)=2*mu; C(1,2)=lambda; C(2,1)=lambda;
  //C(1,1)=279.2; C(2,2)=279.2; C(3,3)=2*83.53; C(1,2)=112.134; C(2,1)=112.134;

  //compute eigen vectors in cross section S (SAFE method)
  Mesh mesh1d(Segment(_xmin=0,_xmax=h,_nnodes=50), _order=1);
  Domain S=mesh1d.domain("Omega"); S.rename("S");
  Space V1(_domain=S, _interpolation=P1, _name="V1");       //P1 Lagrange interpolation
  Unknown u(V1, _name="u", _dim=2);       //2 vector unknown u
  Unknown bu(V1, _name="bu", _dim=2);     //2 vector unknown beta.u
  TestFunction v(u, _name="v");
  TestFunction bv(bu, _name="bv");
  //matrices of eigen devtem
  BilinearForm a = omg2*intg(S,(rho*u)|bv) - intg(S,(C%epsilonG(u,1,1,0))%epsilonG(bv,1,1,0))
                   - i_*intg(S,(C%epsilonG(bu,0,0,1))%epsilonG(bv,1,1,0)) + i_*intg(S,(C%epsilonG(bu,1,1,0))%epsilonG(bv,0,0,1))
                   + intg(S,(rho*bu)|v);
  BilinearForm b = intg(S,(rho*u)|v,_symmetric)
                   + intg(S,(C%epsilonG(bu,0,0,1))%epsilonG(bv,0,0,1),_symmetry=_symmetric);
  TermMatrix A(a, _name="A"), B(b, _name="B");
//  A.toGlobal(_skyline,_dual);
//  B.toGlobal(_skyline,_dual);

  //compute eigen values and eigen vectors, using arpack
  Number n=50;
  //EigenElements eigs = arpackSolve(A, B, _nev=n, _which="SM", _tolerance=1.0e-12);
  EigenElements eigs = eigenSolve(A, B, _nev=n, _which="SM", _tolerance=1.0e-12);

  //keep only eigenvalues with positive real part or positive imaginary part and sort them
  // first real eigenvalues (descending), then other eigenvalues sorted by ascending imaginary part
  std::map<Real,Number> sortedEigsR, sortedEigsC;
  for(Number k=1; k<=n; k++)
    {
      Complex lk=eigs.value(k);
      if(abs(lk.imag())<=theTolerance)  //real eigenvalue
        {if(lk.real()>= 0) sortedEigsR[lk.real()]=k;}
      else
        {if(lk.imag()>= 0) sortedEigsC[lk.imag()]=k;}
    }
  n=sortedEigsR.size()+sortedEigsC.size();
  TermVectors evs(n);
  Vector<Complex> betas(n);
  std::map<Real,Number>::reverse_iterator rit=sortedEigsR.rbegin();
  Number m=1;
  for(; rit!=sortedEigsR.rend(); m++, rit++)
  {
    betas(m) = rit->first;
    evs(m)=eigs.vector(rit->second)(u);
    thePrintStream<<"beta_"<<tostring(m)<<" = "<<betas(m)<<eol;
  }
  std::map<Real,Number>::iterator it=sortedEigsC.begin();
  for(; it!=sortedEigsC.end(); m++, it++)
  {
    betas(m) = eigs.value(it->second);
    if(abs(betas(m).real())<=theTolerance) betas(m)=Complex(0.,betas(m).imag());
    evs(m)=eigs.vector(it->second)(u);
    thePrintStream<<"beta_"<<tostring(m)<<" = "<<betas(m)<<eol;
  }

  //compute sigma=(s_11, s_22, s_12) from u, using Voigt to matrix operator
  Unknown sigma(V1, _name="sigma", _dim=3);       //3_vector unknown sigma
  TestFunction tau(sigma, _name="tau");     //3_vector test function tau
  TermMatrix Ms(intg(S,voigtToM(sigma)%voigtToM(tau)), _name="Ms");
  TermMatrix Ds(intg(S,(C%epsilonG(u,0,0,1))%voigtToM(tau)), _name="Ds");
  TermMatrix Es(intg(S,(C%epsilonG(u,1,1,0))%voigtToM(tau)), _name="Es");
  TermMatrix Msf;
  factorize(Ms, Msf);
  TermVectors svs(n);
  TermMatrix M1(intg(S,u[1]*sigma[3]), _name="M1");
  TermMatrix M2(intg(S,sigma[2]*u[2]), _name="M2");
  for(Number k=1; k<=n; k++)
  {
    TermVector fk=(i_*betas(k))*(Ds*evs(k))+(Es*evs(k));
    svs(k)=directSolve(Msf,fk);
    //normalisation
    TermVector v1=(M1*(evs(k)(u[1])))(sigma[3]), v2=(M2*(svs(k)(sigma[2])))(u[2]);
    Complex jk = dotRC(svs(k)(sigma[3]), v1) - dotRC(evs(k)(u[2]), v2);
    Complex sjk=sqrt(jk);
    evs(k)/=sjk;
    svs(k)/=sjk;
    //thePrintStream<<"sqrt(j"<<tostring(k)<<") = "<<sjk<<eol;
  }

//// Validation : save to files and orthogonality of modes
//  for(Number k=1; k<=n; k++)
//    {
//      evs(k).saveToFile("evs_"+tostring(k)+".dat");
//      svs(k).saveToFile("svs_"+tostring(k)+".dat");
//      TermVector v1=(M1*(evs(k)(u[1])))(sigma[3]), v2=(M2*(svs(k)(sigma[2])))(u[2]);
//      for(Number q=1; q<=n; q++)
//        {
//          Complex jkq = dotRC(svs(q)(sigma[3]), v1) - dotRC(evs(q)(u[2]), v2);
//          thePrintStream<<"j_"<<tostring(k)<<"_"<<tostring(q)<<" = "<<jkq<<eol;
//        }
//    }
  clear(A,B);
  clear(Ds,Es,Ms,M1,M2);

  //solve 2D problem using hybrid DtN
  std::vector<String> sidenames(4, "");
  sidenames[0] = "y=0"; sidenames[1] = "x=1"; sidenames[2] = "y=h"; sidenames[3] = "x=0";
  Mesh mesh2d(Rectangle(_xmin=0,_xmax=h,_ymin=0,_ymax=h,_nnodes=30,_side_names=sidenames), _shape=_triangle, _order=1,_generator=_structured);
  Domain omega=mesh2d.domain("Omega");
  Domain sigmaP=mesh2d.domain("x=1");
  Domain sigmaM=mesh2d.domain("x=0");
  Space V2(_domain=omega, _interpolation=P2, _name="V2");           //P1 Lagrange interpolation
  Unknown U(V2, _name="U", _dim=2);               //2_vector displacement unknown u
  TestFunction V(U, _name="V");
  Space Vt(_domain=sigmaP, _interpolation=P2, _name="Vt");          //P1 Lagrange interpolation
  Unknown t(Vt, _name="t", _dim=1);               //vector strain unknown t = tau_3
  TestFunction q(t, _name="q");

  //construct tensor kernels
  n=15;
  TermVectors tsn(n), t3n(n), usn(n), u3n(n);
  Vector<Complex> mat(n);
  for(Number k=1; k<=n; k++) //extract transverse component of displacement and longitudinal component of Sigma
    {
      usn(k)=evs(k)(u[1]); usn(k).name()="us_"+tostring(k);
      u3n(k)=evs(k)(u[2]); u3n(k).name()="u3_"+tostring(k);
      t3n(k)=svs(k)(sigma[2]); t3n(k).name()="t3_"+tostring(k);
      tsn(k)=svs(k)(sigma[3]); tsn(k).name()="ts_"+tostring(k);
      mat[k-1]=1.;
    }
  Function fmaps(mapS);
  TensorKernel ts_ts(tsn,mat); ts_ts.xmap=fmaps; ts_ts.ymap=fmaps;
  TensorKernel u3_u3(u3n,mat); u3_u3.xmap=fmaps; u3_u3.ymap=fmaps;
  TensorKernel u3_ts(u3n,mat,tsn); u3_ts.xmap=fmaps; u3_ts.ymap=fmaps;
  TensorKernel ts_u3(tsn,mat,u3n); ts_u3.xmap=fmaps; ts_u3.ymap=fmaps;

  //construct bilinear forms
   BilinearForm aut = intg(omega,(C%epsilon(U))%epsilon(V))-omg2*intg(omega,(rho*U)|V)
                    - intg(sigmaP,sigmaP,U[2]*ts_ts*V[2]) - intg(sigmaP,sigmaP,t*u3_ts*V[2]) + intg(sigmaP,t*V[1])
                    - intg(sigmaP,sigmaP,U[2]*ts_u3*q) - intg(sigmaP,sigmaP,t*u3_u3*q) + intg(sigmaP,U[1]*q);

   TermMatrix Aut(aut, _name="Aut");
   //thePrintStream<<Aut<<eol;

   //construct right hand side related to mode diffraction from mode computation
   DomainMap dmap(sigmaM,S,mapS);
   TermVector s11=svs(1)(sigma[2]).mapTo(sigmaM,V[1]);s11.name()="s11";
   TermVector s12=svs(1)(sigma[3]).mapTo(sigmaM,V[2]);s12.name()="s12";
   TermMatrix Msig(intg(sigmaM,U|V));
   TermVector rhs=Msig*s12+Msig*s11;rhs.name()="rhs";
   //thePrintStream<<Msig<<eol<<svs(1)<<eol<<s11<<eol<<s12<<eol<<rhs<<eol;

   //solve 2D problem
   TermVector sol=directSolve(Aut, rhs);
   thePrintStream<<sol<<eol;
   saveToFile("U.vtk",sol(U));

   //construct mode 1
   TermVector U1(U,omega,Vector<Complex>(2,0.),"U1");
   TermVector uinc=evs(1).mapTo(sigmaM,U);uinc.name()="uinc";
   for(Number n=1;n<=U1.nbDofs(U); n++)
   {
       Point p=U1.dof(U,n).coords();
       Vector<Complex> vc=uinc.evaluate(U,Point(0.,p(2))).value<Vector<Complex> >();
       U1.setValue(U,n,-vc*exp(i_*betas(1)*p(1)));
   }
   thePrintStream<<U1<<eol; //thePrintStream.flush();
   saveToFile("U1.vtk",U1);

  std::cout<<"U saved, that's all folks"<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
