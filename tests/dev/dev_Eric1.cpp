/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Eric1; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_Eric1.cpp
\author E. Lunéville
\since 1 March 2014
\date 1 March 2014
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace dev_Eric1
{
    // global data
Real R = 2*pi_;
Point C(0.,0.);
Real ae = 1, be=1, ce=1; // ellipse lengths
Real nue=0;              // rotation angle of the ellipse
Real kx=1., ky=0., k=1.;


/* temporary tools related to the parametrization of an ellipse
    nu=0 : x(t) = x0 + ae*R*cos(t), y(t) = y0 + be*R*sin(t) with t in [-pi,pi]

   |cos(v) -sin(v)| |x0 + ae*R*cos(t)|   x(t)=cos(v)(x0 + ae*R*cos(t))-sin(v)(y0 + be*R*sin(t))
   |sin(v)  cos(v)| |y0 + be*R*sin(t)|   y(t)=sin(v)(x0 + ae*R*cos(t))+cos(v)(y0 + be*R*sin(t))
*/
Point ellipse_parametrization(Real t)
{
  Real s=sin(t), c=cos(t);
  Real x=ae*R*c, y = ae*R*s;
  if(nue==0.) return Point(x,y);
  return Point(cos(nue)*x-sin(nue)*y,sin(nue)*x+cos(nue)*y);
}

Real ellipse_toParameter(const Point& P)
{
  if(nue==0.) return std::atan2((P(2)-C(2))/be,(P(1)-C(1))/ae);
  Real x=cos(nue)*P(1)+sin(nue)*P(2), y=-sin(nue)*P(1)+cos(nue)*P(2);
  return std::atan2((y-C(2))/be,(x-C(1))/ae);
}

Real ellipse_flength(Real t)
{
  return R*std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
}

Real ellipse_fcurvature(Real t)
{
  return ae*be*std::pow(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t),-1.5)/R;
}

Vector<Real> ellipse_ftangent(Real t)
{
  Vector<Real> n(2);
  Real l=std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
  n[0]=-ae*sin(t)/l;
  n[1]= be*cos(t)/l;
  if(nue==0.) return n;
  Real nx=n[0],ny=n[1];
  n[0]=cos(nue)*nx-sin(nue)*ny;
  n[1]=sin(nue)*nx+cos(nue)*ny;
  return n;
}

Vector<Real> ellipse_fnormal(Real t)
{
  Vector<Real> n(2);
  Real l=std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
  n[0]=be*cos(t)/l;
  n[1]=ae*sin(t)/l;
  if(nue==0.)
    return n;
  Real nx=n[0],ny=n[1];
  n[0]=cos(nue)*nx-sin(nue)*ny;
  n[1]=sin(nue)*nx+cos(nue)*ny;
  return n;
}

/* temporary tools related to the parametrization of a Rosello's curve
    nu=0 : x(t) = ae*R*cos(t), y(t) = ae*R*sin(t)*(be-ae*cos(t) )/( ce-ae*cos(t)) with t in [-pi,pi]

   |cos(v) -sin(v)| |x(t)|   =cos(v)x(t)-sin(v)y(t)
   |sin(v)  cos(v)| |y(t)|   =sin(v)x(t)+cos(v)y(t)

*/
Point egg_parametrization(Real t)
{
  Real s=sin(t), c=cos(t);
  Real x=ae*R*c, y = ae*R*s*(be-ae*c)/(ce-ae*c);
  if(nue==0.)
    return Point(x,y);
  return Point(cos(nue)*x-sin(nue)*y,sin(nue)*x+cos(nue)*y);
}
Real egg_toParameter(const Point& P)
{
  Real x=P(1), y=P(2);
  if(nue!=0.)
    {
      x=cos(nue)*P(1)+sin(nue)*P(2);
      y=-sin(nue)*P(1)+cos(nue)*P(2);
    }
  Real t=std::acos(x/(ae*R));
  if(y>=0)
    return t;
  return -t;
}

Real egg_flength(Real t)
{
  Real s=sin(t), c=cos(t);
  Real xp=-ae*R*s;
  Real yp= ae*R*(c*(be-ae*c)/(ce-ae*c)+ae*s*s*(ce-be)/((ce-ae*c)*(ce-ae*c)));
  return sqrt(xp*xp+yp*yp);
}

Real egg_fcurvature(Real t)
{
  Real s=sin(t), c=cos(t);
  Real xp=-ae*R*s, xpp=-ae*R*c;
  Real v=(ce-ae*c);
  Real yp= ae*R*(c*(be-ae*c)/v + ae*s*s*(ce-be)/(v*v)),
         ypp=ae*R*((ae*c-be)*s/v + 3*ae*s*c*(ce-be)/(v*v) - 2*ae*ae*s*s*s*(ce-be)/(v*v*v));
  return (xp*ypp-yp*xpp)/std::pow(xp*xp+yp*yp,1.5);
}

Vector<Real> egg_ftangent(Real t)
{
  Vector<Real> n(2);
  Real s=sin(t), c=cos(t), v=(ce-ae*c);
  Real xp=-ae*R*s, yp= ae*R*(c*(be-ae*c)/v + ae*s*s*(ce-be)/(v*v));
  Real l=sqrt(xp*xp+yp*yp);
  n[0]= xp/l;
  n[1]= yp/l;
  if(nue==0.)
    return n;
  Real nx=n[0],ny=n[1];
  n[0]=cos(nue)*nx-sin(nue)*ny;
  n[1]=sin(nue)*nx+cos(nue)*ny;
  return n;
}

Vector<Real> egg_fnormal(Real t)
{
  Vector<Real> n(2);
  Real s=sin(t), c=cos(t), v=(ce-ae*c);
  Real xp=-ae*R*s, yp= ae*R*(c*(be-ae*c)/v + ae*s*s*(ce-be)/(v*v));
  Real l=sqrt(xp*xp+yp*yp);
  n[0]= yp/l;
  n[1]=-xp/l;
  if(nue==0.)
    return n;
  Real nx=n[0],ny=n[1];
  n[0]=cos(nue)*nx-sin(nue)*ny;
  n[1]=sin(nue)*nx+cos(nue)*ny;
  return n;
}

class CurvatureData //! additional data at a point of a parametrized boundary
{
  public:
    Real t;               //!< parameter
    Real abcissa;         //!< curvilinear abcissa
    Real curvature;       //!< curvature
    Real dscurvature;     //!< derivative/s of curvature
    Real length;          //!< local length
    Vector<Complex> psi;  //!< 2^(-1/3)int_0^s c(t)^(2/3)dt,
    Vector<Real> normal;  //!< normal vector
    Vector<Real> tangent; //!< tangent vector
    Number ndof;          //!< dof number
    CurvatureData(Real tt=0., Real a=0., Real c=0., Real dc=0., Real l=0., const Vector<Complex>& vp=Vector<Complex>(),
                  const Vector<Real>& vn =Vector<Real>(), const Vector<Real>& vt =Vector<Real>(),Number nd=0)
      : t(tt), abcissa(a), curvature(c), dscurvature(dc), length(l), psi(vp), normal(vn), tangent(vt), ndof(nd) {}
};

Point(*parametrization)(Real);
Real (*toParameter)(const Point&);
Real (*flength)(Real);
Real (*fcurvature)(Real);
Vector<Real> (*fnormal)(Real);
Vector<Real> (*ftangent)(Real);

void saveFarField(const TermVector& ud, const Complex& amp, const String& fn="farfield.data")
{
  const Unknown& u = *ud.unknown();
  std::map<Real,Real> mfar;
  for(Number i=1; i<=ud.nbDofs(); i++)
    {
      Point p=ud.dof(u,i).coords();
      Complex val=ud.getValue(i).asComplex();
      Real t=std::atan2(p(2),p(1));
      mfar[t]=std::abs(amp*val);
    }
  std::map<Real,Real>::iterator itm=mfar.begin();
  std::ofstream fout(fn.c_str());
  for(; itm!=mfar.end(); ++itm)
    fout<<itm->first<<" "<<itm->second<<eol;
  fout.close();
}

enum ComputeBoxType {_BEM1=0, _BEM2, _BEM_CFIE, _BEM_F, _BEM_ND, _BEM_NF, _BEM_sigma, _Kirchoff, _ray, _UTD};
enum DataRequired {_noData, _uRequired, _dnuRequired, _dfuRequired, _graduRequired, _ugraduRequired};
typedef std::pair<GeomDomain*,Unknown*> DomUnkPair;

//=====================================================================================================================
//  class to manage data required by a box
//=====================================================================================================================
typedef std::list<std::pair<DiffOpType,Complex> > LcDiffOperator;   //linear combination of diff operator

std::set<DiffOpType> diffOperators(const LcDiffOperator& lcdo)
{
  std::set<DiffOpType> ops;
  std::list<std::pair<DiffOpType,Complex> >::const_iterator itl=lcdo.begin();
  for(; itl!=lcdo.end(); ++itl)
    ops.insert(itl->first);
  return ops;
}

std::ostream& operator<<(std::ostream& out, const LcDiffOperator& lcdo)
{
  std::list<std::pair<DiffOpType,Complex> >::const_iterator itl=lcdo.begin();
  String plus="";
  for(; itl!=lcdo.end(); ++itl)
    {
      out<<plus;
      if(itl->second!=1.)
        out<<itl->second<<"*";
      out<<words("diffop",itl->first);
      plus="+";
    }
  return out;
}

class DataBox
{
  public:
    std::list<std::pair<LcDiffOperator, TermVector> > data;
    typedef  std::list<std::pair<LcDiffOperator, TermVector> >::iterator ItDb;
    typedef  std::list<std::pair<LcDiffOperator, TermVector> >::const_iterator CitDb;
    ItDb begin() {return data.begin();}
    CitDb begin() const {return data.begin();}
    ItDb end() {return data.end();}
    CitDb end() const {return data.end();}

    DataBox() {}
    DataBox(DiffOpType df, const Complex& a=1.) {add(df,a);}

    void add(DiffOpType df, const Complex& a=1.)
    {
      data.push_back(std::make_pair(LcDiffOperator(1,std::make_pair(df,a)),TermVector()));
    }

    void add(DiffOpType df1, const Complex& a1, DiffOpType df2, const Complex& a2) //linear combination a1.df1+a2.df2
    {
      LcDiffOperator lcdo;
      lcdo.push_back(std::make_pair(df1,a1));
      lcdo.push_back(std::make_pair(df2,a2));
      data.push_back(std::make_pair(lcdo,TermVector()));
    }

    std::set<DiffOpType> operatorsRequired() const
    {
      std::set<DiffOpType> ops;
      for(CitDb itdb=begin(); itdb!=end(); ++itdb)
        {
          std::set<DiffOpType> opsi=diffOperators(itdb->first);
          ops.insert(opsi.begin(),opsi.end());
        }
      return ops;
    }

    DataBox& operator+=(const DataBox& cb)  //!< += assuming same structure (not checked)
    {
      CitDb it2=cb.begin();
      for(ItDb it=begin(); it!=end(); ++it, ++it2)
        it->second+=it2->second;
      return *this;
    }

    DataBox& operator-=(const DataBox& cb)  //!< -= assuming same structure (not checked)
    {
      CitDb it2=cb.begin();
      for(ItDb it=begin(); it!=end(); ++it,++it2)
        it->second-=it2->second;
      return *this;
    }

    void print(std::ostream& out) const
    {
      out<<"DataBox"<<eol;
      for(CitDb it=begin(); it!=end(); ++it)
        out<<it->first<<":"<<it->second;
    }
}; //end of DataBox class definition

std::ostream& operator<<(std::ostream& out, const DataBox& db)
{db.print(out); return out;}

//=====================================================================================================================
// abstract class for all ComputeBox classes
//=====================================================================================================================
class ComputeBox
{
  public:
    String name;                            //!< name of the ComputeBox
    mutable Number iter;                    //!< current iteration number
    bool preComputed;                         //!< flag useful if precomputation
    DataBox dataBox;                          //!< data used by box
    bool update_obs;                          //!< intern flag to manage the computation on the observation domain
    Real index;                             //!< free index

    virtual ComputeBoxType type() const = 0;  //!< type of the ComputeBox,  see ComputeBoxType enum
    virtual String typeName() const = 0;    //!< type of the ComputeBox
    virtual GeomDomain& domain() const = 0;   //!< return domain if one
    virtual Unknown& unknown() const = 0;     //!< return unknown if one
    virtual Unknown& vec_unknown() const      //! return vector unknown if one (use for grad computation)
    {
      error("free_error","vector unknown not managed by ComputeBox "+name);
      return *new Unknown();  //fake return
    }

    virtual bool hasComputedField() const {return false;}
    virtual const TermVector& computedField() const
    {
      error("free_error","no computed field in ComputeBox "+name);
      return * new TermVector(); //fake return
    }

    void setData(const TermVector& dt1, const TermVector& dt2=TermVector()) //! set computeBox data according to DataBox structure (not checked)
    {
      DataBox::ItDb itd=dataBox.begin();
      *itd=std::make_pair(itd->first,dt1);
      itd++;
      if(dt2.computed())
        *itd=std::make_pair(itd->first,dt2);
    }

    virtual void preCompute() {} ;                      //!< virtual precomputation function
    virtual void solve() = 0;                           //!< virtual solver

    virtual TermVector computeOn(Domain repdom, Unknown& ur)=0;     //!< virtual computation of representation on other domain using given data
    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)=0;   //!< virtual computation of representation on other domain using given data
    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)=0; //!< virtual computation of representation on other domain using given data

    //! evaluate all required data (dataBox) by the ComputeBox cb using p,q data
    virtual void computeOn(ComputeBox& cb)
    {
      std::set<DiffOpType> ops =cb.dataBox.operatorsRequired();
      std::map<DiffOpType, TermVector> tvs;
      for(std::set<DiffOpType>::iterator its=ops.begin(); its!=ops.end(); ++its)
        {
          switch(*its)
            {
              case _id :
                tvs[_id]= computeOn(cb.domain(),cb.unknown());
                break;
              case _ndotgrad:
              case _ndotgrad_x:
                tvs[_ndotgrad]= computeDnOn(cb.domain(),cb.unknown());
                break;
              case _grad:
              case _grad_x:
                tvs[_grad]= computeGradOn(cb.domain(),cb.vec_unknown());
                break;
              default:
                error("free_error","unavailable diff operator in ComputeBox::computeOnBox");
            }
        }
      for(DataBox::ItDb itdb=cb.dataBox.begin(); itdb!=cb.dataBox.end(); ++itdb)
        {
          LcDiffOperator lcdo=itdb->first;
          TermVector & tv=itdb->second;
          tv*=0.;
          std::list<std::pair<DiffOpType,Complex> >::const_iterator itl=lcdo.begin();
          for(; itl!=lcdo.end(); ++itl)
            {
              if(itl->second!=0)
                {
                  if(itl->second==1.)
                    tv+= tvs[itl->first];
                  else
                    tv+=itl->second * tvs[itl->first];
                }
            }
        }
    }

};

//=====================================================================================================================
// computeBoxBEM abstract class collecting all BEM methods
//=====================================================================================================================
class ComputeBoxBEM: public ComputeBox
{
  public:
    Kernel *Gp;                               //!< pointer to kernel
    const IntegrationMethods *IMie;           //!< integration methods for BEM
    const IntegrationMethods *IMir;           //!< integration methods for integral representations
    TermVector X;                             //!< BEM solution

    ComputeBoxBEM() : Gp(0), IMie(0), IMir(0) {index=0;}
    ComputeBoxBEM(Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir, bool precompute, const String& na="")
      :  Gp(&ker), IMie(&ie), IMir(&ir)
    {
      name=na;
      preComputed=precompute;
    }
    virtual ComputeBoxType type() const =0;
    virtual String typeName() const =0;
    virtual GeomDomain& domain() const = 0;
    virtual Unknown& unknown() const = 0;
    virtual Unknown& vec_unknown() const
    {
      error("free_error","vector unknown not managed by ComputeBoxBEM "+name);
      return *new Unknown();  //fake return
    }
};

//=====================================================================================================================
// CFIE BEM method (Neumann dn_u=-dn_ui) :
//       (I/2-D)p - eta Sp = ui + eta.dn.ui on domp
//       u = Dp   on dom_ext
// with  eta = - a/(1-a) * i/k  (common value a=0.2)
//=====================================================================================================================
class ComputeBoxBEMCFIE: public ComputeBoxBEM
{
  public:
    GeomDomain *domp;                         //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                         //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                   //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermVector Ui;                            //!< Dirichlet trace data
    TermMatrix K, Krhs;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> URep;     //!< integral representation matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> DnURep;   //!< Dn integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> gradURep; //!< grad integral representation matrix list indexed by (dom, unknown)
    Complex eta;
    Real k;

  private:
    ComputeBoxBEMCFIE() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr;}  //forbidden default constructor
  public:
    ComputeBoxBEMCFIE(GeomDomain& d, Unknown& unk, Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir,
                      Real ki, Real a=0.2, bool precompute=true, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&d;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      dataBox.add(_id);       //say require u
      dataBox.add(_ndotgrad); //say require dn(u)
      k=ki;
      eta=Complex(0.,-a/(k*(1-a)));
      if(precompute)
        preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM_CFIE;}
    virtual String typeName() const {return "BEM_CFIE";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      GeomDomain& dom=*domp;
      BilinearForm bf=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie)
                       +eta*intg(dom,dom,(ncrossgrad(u)*G)|ncrossgrad(v),_method=*IMie)-(eta*k*k)*intg(dom,dom,u*(G*(_nx|_ny))*v,_method=*IMie);
      BilinearForm bfrhs=intg(dom,u*v);
      K=TermMatrix(bf,_storage=denseRow, _name="K");
      Krhs=TermMatrix(bfrhs, _name="Krhs");
      factorize(K,K);
      preComputed=true;
    }

    virtual void solve()
    {
      TermVector& ui=dataBox.begin()->second;
      TermVector& dui=(++dataBox.begin())->second;
      TermVector ci=eta*dui+ui;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          GeomDomain& dom=*domp;
          BilinearForm bf=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie)
                           +eta*intg(dom,dom,(ncrossgrad(u)*G)|ncrossgrad(v),_method=*IMie)-(eta*k*k)*intg(dom,dom,u*(G*(_nx|_ny))*v,_method=*IMie);
          BilinearForm bfrhs=intg(dom,u*v);
          K=TermMatrix(bf, _name="K");
          Krhs=TermMatrix(bfrhs, _name="Krhs");
          X=directSolve(K,Krhs*ci);
        }
      else
        X=factSolve(K,Krhs*ci);
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(URep.find(dup)==URep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          URep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_y(G)*u,_method=*IMir));
        }
      return (URep[dup]*X);
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DnURep.find(dup)==DnURep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          DnURep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir));
        }
      return (DnURep[dup]*X);
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      GeomDomain& dom=*domp;
      return integralRepresentation(ur,repdom,intg(dom,grad_x(ndotgrad_y(G))*u,_method=*IMir),X);
    }

}; //end of ComputeBoxBEMCFIE definition

//=====================================================================================================================
// BEM method for Robin dn_u+ alpha u=-(dn_ui+alpha ui) :
//       (N + alpha^2 S)p = (dn_ui+alpha ui) on domp
//       u = -(D + alpha S)p   on dom_ext
// no singular frequence if imag(alpha)!=0
//=====================================================================================================================
class ComputeBoxBEMF: public ComputeBoxBEM
{
  public:
    GeomDomain *domp;                         //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                         //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                   //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermMatrix K, Krhs;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> URep;     //!< integral representation matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> DnURep;   //!< Dn integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> gradURep; //!< grad integral representation matrix list indexed by (dom, unknown)
    Complex alpha;
    Real k;

  private:
    ComputeBoxBEMF() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr;}  //forbidden default constructor
  public:
    ComputeBoxBEMF(GeomDomain& d, Unknown& unk, Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir,
                      Real ki, Complex ai, bool precompute=true, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&d;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      dataBox.add(_id);       //say require u
      dataBox.add(_ndotgrad); //say require dn(u)
      k=ki;
      alpha=ai;
      if(precompute) preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM_CFIE;}
    virtual String typeName() const {return "BEM_CFIE";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      GeomDomain& dom=*domp;
      BilinearForm bf=(k*k)*intg(dom,dom,u*(G*(_nx|_ny))*v,_method=*IMie)-intg(dom,dom,(ncrossgrad(u)*G)|ncrossgrad(v),_method=*IMie)
                       +(alpha*alpha)*intg(dom,dom,u*G*v,_method=*IMie);
      BilinearForm bfrhs=intg(dom,u*v);
      K=TermMatrix(bf,_storage=denseRow, _name="K");
      Krhs=TermMatrix(bfrhs, _name="Krhs");
      factorize(K,K);
      preComputed=true;
    }

    virtual void solve()
    {
      TermVector& ui=dataBox.begin()->second;
      TermVector& dui=(++dataBox.begin())->second;
      TermVector ci=dui+alpha*ui;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          GeomDomain& dom=*domp;
          BilinearForm bf=(k*k)*intg(dom,dom,u*(G*(_nx|_ny))*v,_method=*IMie)-intg(dom,dom,(ncrossgrad(u)*G)|ncrossgrad(v),_method=*IMie)
                          +(alpha*alpha)*intg(dom,dom,u*G*v,_method=*IMie);
          BilinearForm bfrhs=intg(dom,u*v);
          K=TermMatrix(bf, _name="K");
          Krhs=TermMatrix(bfrhs, _name="Krhs");
          X=directSolve(K,Krhs*ci);
        }
      else
        X=factSolve(K,Krhs*ci);
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(URep.find(dup)==URep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          URep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_y(G)*u,_method=*IMir));
          if(alpha!=Complex(0.)) URep[dup]+=alpha*integralRepresentation(ur,repdom,intg(dom,G*u,_method=*IMir));
        }
      return (URep[dup]*X)*=-1.;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DnURep.find(dup)==DnURep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          DnURep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir)+alpha*intg(dom,ndotgrad_x(G)*u,_method=*IMir));
        }
      return (DnURep[dup]*X);
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur) // ####### TO BE CHANGED
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      GeomDomain& dom=*domp;
      return integralRepresentation(ur,repdom,intg(dom,grad_x(ndotgrad_y(G))*u,_method=*IMir),X);
    }

}; //end of ComputeBoxBEMF definition


//=====================================================================================================================
// BEM method for Robin dn_u+ alpha u=-(dn_ui+alpha ui) : p = u|Gamma
//       (0.5I-D - alpha S)p = S(dn_ui+alpha ui) on domp
//        u = S(dn_ui+alpha ui) - (D+alpha S)p = -S(dn_ui+alpha(ui+p)) - Dp  on dom_ext
// no singular frequence if imag(alpha)!=0
//=====================================================================================================================
class ComputeBoxBEMF2: public ComputeBoxBEM
{
  public:
    GeomDomain *domp;                         //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                         //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                   //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermMatrix K, Krhs;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> DRep;     //!< integral representation matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> SRep;     //!< single layer matrix list indexed by (domain, unknown)
    Complex alpha;
    Real k;

  private:
    ComputeBoxBEMF2() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr;}  //forbidden default constructor
  public:
    ComputeBoxBEMF2(GeomDomain& d, Unknown& unk, Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir,
                      Real ki, Complex ai, bool precompute=true, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&d;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      dataBox.add(_id);       //say require u
      dataBox.add(_ndotgrad); //say require dn(u)
      k=ki;
      alpha=ai;
      if(precompute) preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM_CFIE;}
    virtual String typeName() const {return "BEM_CFIE";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      GeomDomain& dom=*domp;
      BilinearForm bf=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie);
      BilinearForm bfrhs=intg(dom,dom,u*G*v,_method=*IMie);
      K=TermMatrix(bf, _name="K");
      Krhs=TermMatrix(bfrhs, _name="Krhs");
      if(alpha!=Complex(0.)) K-=alpha*Krhs;
      factorize(K,K);
      preComputed=true;
    }

    virtual void solve()
    {
      TermVector ci=(++dataBox.begin())->second;
      if(alpha!=Complex(0.)) ci+=alpha*dataBox.begin()->second;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          GeomDomain& dom=*domp;
          BilinearForm bf=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie);
          BilinearForm bfrhs=intg(dom,dom,u*G*v,_method=*IMie);
          K=TermMatrix(bf, _name="K");
          Krhs=TermMatrix(bfrhs, _name="Krhs");
          if(alpha!=Complex(0.)) K-=alpha*Krhs;
          X=directSolve(K,Krhs*ci);
        }
      else X=factSolve(K,Krhs*ci);

      String na="current_BEM_"+tostring(index)+".dat";
      std::ofstream out(na.c_str());
      for(Number i=0; i<up->space()->dimSpace(); i++)
        {
          const Point& P=up->space()->feDofCoords(i);
          Complex v=dataBox.begin()->second.getValue(i+1).asComplex()+X.getValue(i+1).asComplex();
          out<<toParameter(P)<<" "<<std::real(v)<< " "<<std::imag(v)<<eol;
        }
      out.close();
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          DRep[dup]= integralRepresentation(ur,repdom,intg(dom,ndotgrad_y(G)*u,_method=*IMir));
          SRep[dup]= integralRepresentation(ur,repdom,intg(dom,G*u,_method=*IMir));
        }
      TermVector ci=(++dataBox.begin())->second;
      if(alpha!=Complex(0.)) ci+=alpha*(dataBox.begin()->second +X);
      return (SRep[dup]*ci+DRep[dup]*X)*=-1.;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur) // ####### TO BE CHANGED
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      return (DRep[dup]*X);
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur) // ####### TO BE CHANGED
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      GeomDomain& dom=*domp;
      return integralRepresentation(ur,repdom,intg(dom,grad_x(ndotgrad_y(G))*u,_method=*IMir),X);
    }

}; //end of ComputeBoxBEMF2 definition


//=====================================================================================================================
// Kirchoff approximation stuff
//=====================================================================================================================
// shadow coloring rule of an element, one of the dof belongs to the shadow zone (imag(grad_u/u) | n) > 0 <=> im(dn_u/u)>0
Real shadowColoringRule(const GeomElement& gelt, const std::vector<Vector<Real> >& val)
{
  GeomMapData *gmap=gelt.meshElement()->geomMapData_p;
  if(gmap==nullptr)
    gmap = new GeomMapData(gelt.meshElement());
  if(gmap->normalVector.size()==0)
    gmap->computeOrientedNormal();
  Real res = 0.;
  for(Number i=0; i<val.size() && res==0; i++)
    {
      if(dot(val[i],gmap->normalVector)>-theTolerance)
        res+=-1.;
    }
  return res;
}

// find shadow/light dofs (terminators) from u and grad u on a side domain
//   dofs where the color is -1, dof colors are built from elt colors (see shadowColoringRule function)
//                              shadow/light
//   dof colors            -2        -1          0
//                 ---------|---------|----------|----------
//   elt colors        -1       -1         0         0
//
// update shadow/light dof, normals and phaseGradient (ray direction) at these dofs
void buildShadowLightDofs(const TermVector& U, const TermVector& gradU, Real k, std::vector<const Dof*>& shadowLightDofs, std::vector<Int>& colors,
                          std::vector<Vector<Real> >& normals, std::vector<Real>& phase, std::vector<Vector<Real> >& phaseGrad)
{
  //update shadow/light dofs
  Space& V = *U.unknown()->space();
  colors.resize(V.nbDofs());
  std::fill(colors.begin(),colors.end(),0);
  for(Number j=0; j<V.nbOfElements(); j++) // built dof colors
    {
      const Element* elt=V.element_p(j);
      Real color=elt->geomElt_p->color;
      const std::vector<Number>& dofNum=elt->dofNumbers;
      std::vector<Number>::const_iterator itn = dofNum.begin();
      for(; itn!=dofNum.end(); ++itn)
        colors[*itn-1]+=color;
    }
  shadowLightDofs.clear();
  normals.clear();
  phaseGrad.clear();
  std::vector<Point> points;
  for(Number j=0; j<colors.size(); j++) // locate shadow/light dofs
    {
      if(colors[j]==-1)
        {
          const Dof& dof=V.dof(j+1);
          shadowLightDofs.push_back(&dof);
          points.push_back(dof.coords());
          phaseGrad.push_back(imag(gradU.getValue(dof).asComplexVector()/(k*U.getValue(dof).asComplex())));
          phase.push_back(arg(U.getValue(dof).asComplex())/k);
        }
    }
  normals = computeNormalsAt(*V.domain(), points);
}

Complex shadowLightCorrection(const Point& p, Parameters& pa = defaultParameters)
{
  const std::vector<const Dof*>& shadowLightDofs =*static_cast<const std::vector<const Dof* >* >(pa("shadowLightDofs").p_);
  const std::vector<Vector<Real> >& normals=*static_cast<const std::vector<Vector<Real> >* >(pa("normals").p_);
  const std::vector<Real>& phase =*static_cast<const std::vector<Real>* >(pa("phases").p_);
  const std::vector<Vector<Real> >& phaseGrad=*static_cast<const std::vector< Vector<Real> >* >(pa("phaseGrads").p_);
  Real k=real(pa("k"));
  Complex res(0.,0.);
  for(int j=0; j<shadowLightDofs.size(); j++)
    {
      Point y=shadowLightDofs[j]->coords();           // shadow/light point
      const Vector<Real>& grad=phaseGrad[j];        // incident direction
      Vector<Real> i=grad/norm(grad);               // normalized incident direction
      const Vector<Real>& n= normals[j];            // outward normal vector
      Real lx=y.distance(p);                        // lx =|y-p|
      const Vector<Real> t = (p-y)/lx;              // observation direction
      Vector<Real> s(2);
      s[0]=-n[1];
      s[1]=n[0];     // tangential vector
      Real psix = phase[j] + lx;
      Real tmp=dot(i-t,s);
      if(std::abs(tmp)>theEpsilon)
        res+=dot(s,i)*dot(t,n)*std::exp(k*i_*psix)/(tmp*std::sqrt(lx));
    }
  res*=std::exp(0.25*pi_*i_)/std::sqrt(2.*pi_*k);
  return res;
}

TermVector colors(const GeomDomain& dom)  //produce the colors of domain element as a P0 TermVector
{
  Space* V=Space::findSpace("P0@");
  Unknown* u=Unknown::findUnknown("uP0@");
  if(V==nullptr)
    V=new Space(_domain=dom,_interpolation=P0, _name="P0@");
  if(u==nullptr)
    u=new Unknown(*V, _name="uP0@");
  TermVector cols(*u,dom,0.);
  for(Number k = 0; k < V->nbOfElements(); k++) // data related to u
    {
      const Element* elt = V->element_p(k);
      GeomElement* gelt = elt->geomElt_p;
      const std::vector<Number>& dofNum = V->elementDofs(k);
      cols.setValue(dofNum[0],gelt->color);
    }
  return cols;
}

//=====================================================================================================================
// computeBoxUTD class implementing Kirchoff method with Uniform Theory of Diffraction for 2D acoustic Neumann problem
//   light zone  (s<s1) : 2.*intg_dom ndotgrad_y(G)*ui
//   shadow zone (s>s2) : creeping ray from UTD
//   light/shadow zone (s1<s<s2): Fock function
//=====================================================================================================================
class ComputeBoxUTD: public ComputeBox
{
  public:
    GeomDomain *domp;                          //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                          //!< unknown related to the ComputeBox (should not be 0!)
    Kernel *Gp;                                //!< pointer to kernel
    const IntegrationMethods *IMir;            //!< integration methods for integral representations
    Real k;                                  //!< wave number
    Complex z;                               //!< impedance factor in boundary condition dn.u + iz.u=0;
    Real s1,s2;                              //!< fock zone bounds (Fock coordinate)
    Real sc;                                 //!< limit of creeping waves (>s2) (curvilinear coordinate)
    Real ds;                                 //!< width of transition area Fock-Kirchoff : s1-ds < s < s1+ds (Fock coordinate)
    bool useIN;                                //!< use (I|n) in Fock scaling
    Fock fock;                                 //!< Fock function object
    std::map<DomUnkPair,TermMatrix> DRep;      //!< double layer matrix list indexed by (domain, unknown)
    Real perimeter;                          //!< mesure of the boundary (computed by computeCurData)
    Vector<Complex> psit;                    //!< last values of psi (computed by computeCurData)
    std::map<const FeDof*,CurvatureData> curdatas; //!< additional data at points of the parametrized boundary (computed by computeCurData)
    std::vector<const Dof*> shadowLightDofs;   //!< list of shadow/light dofs
    std::vector<Vector<Real> > normals;      //!< computed normals at shadow/light dofs
    std::vector<Real> phase;                 //!< phase at shadow/light dofs
    std::vector<Vector<Real> > phaseGrad;    //!< phase gradient (ray direction) at shadow/light dofs
    GeomDomain* obsp;                          //!< if required, the observation domain
    Unknown* uobsp;                            //!< if required, the unknown related to the observation domain
    TermVector uobs;                           //!< if required, the diffracted field on the observation domain (additive)

    ComputeBoxUTD() {domp=nullptr; up=nullptr; vp=nullptr; Gp=nullptr; IMir=nullptr; obsp=nullptr; uobsp=nullptr; s1=0; s2=0.; sc=0.; ds=0.; perimeter=0, useIN=false; index=0;}
    ComputeBoxUTD(GeomDomain& dom, Unknown& unk, Kernel& ker, const IntegrationMethods& ir, Real kin, Number nbr, Real si1, Real si2, Real sic, Real dsi, bool ini, const String& na="")
    {
      name=na;
      domp=&dom;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      Gp=&ker;
      IMir=&ir;
      k=kin;
      s1=si1;
      s2=si2;
      sc=sic;
      ds=dsi;
      useIN=ini;
      fock = Fock(0,nbr,200);
      obsp=nullptr;
      uobsp=nullptr;
      dataBox.add(_id);
      dataBox.add(_grad);
      index=0;
      computeCurData();
    }
    virtual ComputeBoxType type() const {return _UTD;}
    virtual String typeName() const {return "UTD";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *const_cast<Unknown*>((++dataBox.begin())->second.unknown());}
    virtual bool hasComputedField() const {return obsp!=nullptr;}
    virtual const TermVector& computedField() const
    {
      if(obsp!=nullptr)
        return uobs;
      error("free_error","no computed field in ComputeBox "+name);
      return *new TermVector();
    }

    void computeCurData() //!< compute additional data at points of a parametrized boundary
    {
      Space* V = up->space();
      const FeDof* d0 = static_cast<const FeDof*>(&V->dof(1)), * dp=d0, *dc=nullptr;
      std::vector<std::pair<Element*, Number> >::const_iterator iti = d0->elements().begin();
      Real tp=toParameter(dp->coords()), tc, cp=fcurvature(tp), cc, lp=flength(tp), lc;
      Real s=0, h;
      Number nbr=fock.nbr;
      Vector<Complex> psi(nbr,0.);
      Real a23= std::pow(2,-1./3.);
      curdatas[dp] = CurvatureData(tp,s,cp,0.,lp,psi,fnormal(tp), ftangent(tp),1);
      Element * elt;
      Number nd, ndof;
      while(dc!=d0)
        {
          elt = iti->first;;
          nd = iti->second;
          ndof = elt->dofNumbers[nd%2];
          dc = &elt->feSpaceP()->fedof(ndof);
          tc = toParameter(dc->coords());
          cc = fcurvature(tc);
          lc=flength(tc);
          Real dt=abs(tc-tp);
          if(dt > pi_) dt=abs(dt-2*pi_);
          h=0.5*dt*(lp+lc);   //approximation of the arc length
          s+=h;
          Real dsc=(cc-cp)/h;       //approx. of the s derivative of the curvature
          for(Number i=0; i<nbr; i++)  psi[i]+=h*0.5*a23*(std::pow(cp,2./3.)+std::pow(cc,2./3.));
          iti = dc->elements().begin();
          if(iti->first==elt) iti++;
          if(dc!=d0) curdatas[dc] = CurvatureData(tc,s,cc,dsc,lc,psi,fnormal(tc), ftangent(tc), ndof);
          else curdatas[dc].dscurvature=dsc; //update derivative of the curvature
          dp=dc; cp=cc; tp=tc; lp=lc;
          //thePrintStream<<"tc="<<tc<<" h="<<h<<" s="<<s<<" c="<<cc<<" psi[0]="<<psi[0]<<eol;
        }
      perimeter=s;
      psit=psi;
      thePrintStream<<std::floor(s*k/(2*pi_))<<" wavelenths on perimeter ("<<s<<")"<<eol;
    }

    virtual void solve()
    {
      //update shadow element colors
      TermVector& ui = dataBox.begin()->second;
      TermVector& gradui = (++dataBox.begin())->second;
      TermVector gradsU =((gradui/ui).toImag())/k;
      setColor(*domp, gradsU, shadowColoringRule);
      std::vector<Int> color;
      buildShadowLightDofs(ui, gradui, k, shadowLightDofs, color, normals, phase, phaseGrad);
      Number ns=shadowLightDofs.size();
      Number nbr=fock.nbr;
      Real st = perimeter, s3 = sc*perimeter;
      std::map<const FeDof*,CurvatureData>::iterator itm, itms;
      std::vector<Complex> vis(ns);
      std::vector<Real> shadowOrientation(ns);   // =(I|t) : if <0 shadow oriented as s decreasing, if >0 as s increasing TO BE IMPROVED
      for(Number j=0; j<ns; j++)
      {
          itms = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
          shadowOrientation[j]=dot(phaseGrad[j],itms->second.tangent);
          vis[j]= ui.getValue(*shadowLightDofs[j]).asComplex();
      }
      TermVector ui2 = TermVector(ui);
      Space* V = up->space();

      std::vector<Number> dofStatus(V->dimSpace(),0); // 0 : not computed, 1: Kirchoff, 2: Shadow, 3: Fock

      // Limit of shadow Fock zone sf[j] = min{s>0 such that sb>s1}, s related to shadow/light point j
      // precomputation of c, psi at transition Fock shadow - creeping
      Vector<Real> s2f(ns,st),s2fb(ns,s2),c2(ns,-1.), s1f(ns,-st),s1fb(ns,s1),spec(ns), specb(ns), s1fm(ns,-st),s1fbm(ns,0);
      Vector<Vector<Complex> > psi2(ns,Vector<Complex>(nbr));
      for(Number j=0; j<ns; j++)
        {
          Real isnmin=1;
          itms = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
          Real ori = shadowOrientation[j];
          thePrintStream<<" Shadow/Light P_"<<j<<"="<<shadowLightDofs[j]->coords()<<" (t="<< toParameter(shadowLightDofs[j]->coords())
                        <<")  orient="<<ori<<eol;
          String fn="s1b_"+tostring(j+1)+".dat";
          std::ofstream fout1(fn.c_str());
          fn="s2b_"+tostring(j+1)+".dat";
          std::ofstream fout2(fn.c_str());
          for(itm=curdatas.begin(); itm!=curdatas.end(); ++itm)
            {
              Int col=color[itm->second.ndof-1];
              if(col==-2) //in shadow
                {
                  Real s = itm->second.abcissa-itms->second.abcissa, c = itm->second.curvature;
                  if ((s>0 && ori<0) || (s<0 && ori>0)) s=st-abs(s); else s=abs(s);
                  Real sb = std::pow(0.5*k*c*c,1./3.)*s;
                  if(sb>=s2 && s<s2f[j]) //Fock limiter : max{s; sb>=s2}
                    {
                      s2f[j]=s;
                      s2fb[j]=sb;
                      c2[j]=c;
                      for(Number i=0; i<nbr; i++) psi2[j][i]=itm->second.psi[i];
//                      thePrintStream<<"shadow t="<<itm->second.t<<" s="<<s<<" sb="<<sb<<" s2f[j]="<<s2f[j]<<eol;
                    }
                    fout2<<s<<" "<<sb<<" "<<c<<" "<<eol;
                }
                else if(col==0) //in light
                {
                  Real s = itm->second.abcissa-itms->second.abcissa, c = itm->second.curvature, dc=itm->second.dscurvature;
                  if ((s>0 && ori>0) || (s<0 && ori<0)) s=abs(s)-st; else s=-abs(s);
                  Real sb;
                  Number n = itm->second.ndof;
                  Vector<Complex> gv=(++dataBox.begin())->second.getValue(n).asComplexVector()/(k*(dataBox.begin())->second.getValue(n).asComplex());
                  Real isn=dot(imag(gv),itm->second.normal);
                  if(useIN) sb = std::pow(0.5*k/c,1./3.)*isn;
                  else sb = std::pow(0.5*k*c*c,1./3.)*s;
                  if(isn<isnmin) {isnmin=isn;spec[j]=s;specb[j]=sb;}  // detect specular point
                  if(sb<=s1 && s>s1f[j]) //first Fock limiter :  min{s; sb> s1}
                    {
//                      thePrintStream<<"light  t="<<itm->second.t<<" s="<<s<<" sb="<<sb<<" s1f[j]="<<s1f[j]<<eol;
                      s1f[j]=s;
                      s1fb[j]=sb;
                    }
                  // second Fock limiter : max{s; (c^(2/3)s)'=0 <=> c + 2*c'*s/3=0}, use  max{s; c + 2*c'*s/3<0}
                  Real der;
                  if (ori>0) der=c+2*dc*s/3; else der=c-2*dc*s/3;
                  if(der <= 0 && s>s1fm[j]) {s1fm[j]=s;s1fbm[j]=sb;}
                  fout1<<s<<" "<<sb<<" "<<c<<" "<<dc<<" "<<der<<eol;
                }
            }
            fout1.close();fout2.close();
            thePrintStream<<" s1f[j]="<<s1f[j]<<" s1fm[j]="<<s1fm[j]<<" spec[j]="<<spec[j]<<eol;
            if(s1f[j]==-st) {s1f[j]=s1fm[j];s1fb[j]=s1fbm[j];} //sb(s) does not cross s1, take s min(sb)
            if(s1f[j]<0.98*spec[j]) // restrict to exclude specular point from the light Fock region
                {
                    s1f[j]=0.98*spec[j];
                    s1fb[j]=0.98*s1fbm[j];  // if using isn, assume new isn is close to 0.95*isn, correct if isn is not used
                }
        }
      thePrintStream<<" Fock limiter : s1f="<<s1f<<"->"<<s1fb<<" s2f="<<s2f<<"->"<<s2fb<<eol;

      Vector<Complex> cte(nbr),xi(nbr);
      for(Number i=0; i<nbr; i++)
      {
        xi[i]=std::conj(fock.poles[i]);
        cte[i]=2*i_*sqrtOfpi_/std::conj(fock.vpoles[i]);
      }

      for(Number e=0; e<V->nbOfElements(); e++) //loop on elements
        {
          const Element* elt=V->element_p(e);
          bool shadow =  elt->geomElt_p->color!=0;
          const std::vector<Number>& dofNum=elt->dofNumbers;
          std::vector<Number>::const_iterator itn=dofNum.begin();
          for(itn = dofNum.begin(); itn!=dofNum.end(); ++itn)
            {
              Number n=*itn;
              if(dofStatus[n-1]==0)
                {
                  Complex vi=ui2.getValue(n).asComplex();
                  itm = curdatas.find(static_cast<const FeDof*>(&elt->feSpaceP()->dof(n)));
                  if(itm==curdatas.end()) error("free_error","abnormal failure in ComputeBoxUTD::solve");
                  Real s = itm->second.abcissa, c = itm->second.curvature, l=itm->second.length;
                  Vector<Complex>& psi = itm->second.psi;
                  Vector<Real>& normal = itm->second.normal;
//                  thePrintStream<<"n="<<n<<" p="<<itm->first->coords()<<" shadow="<<shadow<<" t="<<toParameter(itm->first->coords())<<eol;
                  for(Number j=0; j<ns; j++) // analyze distance to shadow/light point
                    {
                      itm = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
                      if(itm==curdatas.end()) error("free_error","abnormal failure in ComputeBoxUTD::solve");
                      Real ori = shadowOrientation[j];
                      Real c0 = itm->second.curvature;
                      Real ss0 = s-itm->second.abcissa, ss=ss0;
                      ss=abs(ss0);
                      bool jump= (ss0>0 && ((shadow && ori<0) || (!shadow && ori>0))) || (ss0<0 && ((shadow && ori>0) || (!shadow && ori<0)));
                      if(jump) ss=st-ss;
                      if(!shadow) ss*=-1;
                      Complex v=0.;
                      Real isn;
                      Real sb=std::pow(0.5*k*c*c,1./3.)*ss;
                      if(useIN && ss<=0 && ss>-0.25*st)  //in light region, compute sb using I|n
                        {
                          Vector<Complex> gv=((++dataBox.begin())->second.getValue(n).asComplexVector()/(k*vi));
                          isn=dot(imag(gv),normal);
                          sb = std::pow(0.5*k/c,1./3.)*isn;
                        }
//                      thePrintStream<<"   j="<<j<<" s="<<s<<" sld="<<itm->second.abcissa<<" ss0="<<ss0<<" ss="<<ss<<" sb="<<sb;

                      if(ss>s1f[j] && sb>s1 && sb<0) // Fock light : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          Real x=static_cast<const FeDof&>(elt->feSpaceP()->dof(n)).coords()(1);
                          if(useIN) v=std::conj(fock(sb))*vi*std::exp(i_*k*isn*isn*isn/(6*c)); // explicit ui-rescaling
                          else      v=std::conj(fock(sb))*vi*std::exp(i_*k*c*c*ss*ss*ss/6);
                          ui.setValue(n,v);
                          dofStatus[n-1]=3;
//                          thePrintStream<<" light Fock v="<<v;
                        }
                      if((ss<=s1f[j] || sb<=s1) && dofStatus[n-1]==0) // light : use Kirchoff
                        {
                          ui.setValue(n,2*vi);
                          dofStatus[n-1]=1;
//                          thePrintStream<<" light Kirchoff v="<<2*vi;
                        }
                      if((ss>=s2f[j] || sb>=s2) && dofStatus[n-1]==0) // shadow : set to 0 the first time
                        {
                          ui.setValue(n,0.);
                          dofStatus[n-1]=2;
//                          thePrintStream<<" shadow v=0";
                        }
                      if(ss>=0 && ss<s2f[j] && sb<s2) //  Fock shadow : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          v=std::conj(fock(sb))*std::exp(i_*k*ss)*vis[j]; //take into account ui at transition
                          ui.setValue(n,v);
                          dofStatus[n-1]=3;
//                          thePrintStream<<" shadow Fock v="<<v;
                        }
//                      thePrintStream<<eol;
                      // add creeping waves
                      ss=std::abs(ss0);jump=false;
                      if ((ss0>0 && ori<0) || (ss0<0 && ori>0)) {ss=st-ss;jump=true;}
                      if((ss>=s2f[j] || sb>=s2) && ss<=s3)
                        {
                          v=0;
                          for(Number i=0; i<nbr; i++)
                            {
                              Complex ps = xi[i]*(psi[i]-psi2[j][i]);
                              if(jump) {if(ss0<0) ps=xi[i]*psit[i]+ps; else ps=xi[i]*psit[i]-ps;}
                              if(std::imag(ps)<0) ps=-ps;
                              v+=std::exp(i_*(k*ss+std::pow(k,1./3.)*ps+s2fb[j]*conj(fock.poles[i])))*std::pow(c/c2[j],1./6.)*cte[i]*vis[j]; //take into account ui at transition
//                              thePrintStream<<"   creeping "<<i<<" : psi[i]="<<psi[i]<<" psi2[j][i]="<<psi2[j][i]<<" psit[i]="<<psit[i]<<" xi[i]="<<xi[i]
//                                            <<" c="<<c<<" c2="<<c2[j]<<" cte="<<cte[i]<<" vis="<<vis[j]<<" ps="<<ps<<" v="<<v<<eol;
                            }
                          v+=ui.getValue(n).asComplex();
                          ui.setValue(n,v);
                        }
                    }
                }
            }
        }
      String na="current_UTD_"+tostring(index)+".dat";
      std::ofstream out(na.c_str());
      for(Number i=0; i<V->dimSpace(); i++)
        {
          const Point& P=V->feDofCoords(i);
          Complex v=ui.getValue(i+1).asComplex();
          out<<toParameter(P)<<" "<<std::real(v)<< " "<<std::imag(v)<<eol;
        }
      out.close();
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      if(&repdom==obsp)
        return uobs; //special case
      TermVector& ui = dataBox.begin()->second;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
        DRep[dup]=integralRepresentation(ur,repdom,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
      TermVector res=DRep[dup]*ui;
      if(obsp!=nullptr) //update field on observation domain
        {
          dup=std::make_pair(obsp,uobsp);
          if(DRep.find(dup)==DRep.end())
            DRep[dup]=integralRepresentation(*uobsp,*obsp,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
          uobs+=DRep[dup]*ui;
        }
      return res;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxKirchoff does not yet propose integral representation of Dn");
      return TermVector(); //fake return
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxComputeBoxKirchoff does not yet propose integral representation of gradient");
      return TermVector(); //fake return
    }
};


//=====================================================================================================================
// computeBoxUTDF class implementing Kirchoff method with Uniform Theory of Diffraction for 2D acoustic Fourier problem
//   (delta+k2)u=0 and (dn+ikZ)u= -(dn+ikZ)ui on Gamma
//
//   light zone  (s<s1) : 2.*intg_dom ndotgrad_y(G)*ui (Kirchoff)
//   shadow zone (s>s2) : creeping ray from UTD
//   light/shadow zone (s1<s<s2): Fock function
//=====================================================================================================================
class ComputeBoxUTDF: public ComputeBox
{
  public:
    GeomDomain *domp;                          //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                          //!< unknown related to the ComputeBox (should not be 0!)
    Kernel *Gp;                                //!< pointer to kernel
    const IntegrationMethods *IMir;            //!< integration methods for integral representations
    Real k;                                  //!< wave number
    Complex z;                               //!< impedance factor in boundary condition (dn + ikz)u=0;
    Real s1,s2;                              //!< fock zone bounds (Fock coordinate)
    Real sc;                                 //!< limit of creeping waves (>s2) (curvilinear coordinate)
    bool useIN;                                //!< use (I|n) in Fock scaling
    Number nbr;                              //!< number of residues used by Fock and creeping rays approximation
    std::map<DomUnkPair,TermMatrix> DRep;      //!< double layer matrix list indexed by (domain, unknown)
    Real perimeter;                          //!< mesure of the boundary (computed by computeCurData)
    Vector<Complex> psit;                    //!< last values of psi (computed by computeCurData)
    std::vector<Fock*> focks;                  //!< Fock function objects, one by shadow/light point if Z!=0
    std::map<const FeDof*,CurvatureData> curdatas; //!< additional data at points of the parametrized boundary (computed by computeCurData)
    std::vector<const Dof*> shadowLightDofs;   //!< list of shadow/light dofs
    std::vector<Vector<Real> > normals;      //!< computed normals at shadow/light dofs
    std::vector<Real> phase;                 //!< phase at shadow/light dofs
    std::vector<Vector<Real> > phaseGrad;    //!< phase gradient (ray direction) at shadow/light dofs
    GeomDomain* obsp;                          //!< if required, the observation domain
    Unknown* uobsp;                            //!< if required, the unknown related to the observation domain
    TermVector uobs;                           //!< if required, the diffracted field on the observation domain (additive)

    ComputeBoxUTDF() {domp=nullptr; up=nullptr; vp=nullptr; Gp=nullptr; IMir=nullptr; obsp=nullptr; uobsp=nullptr; s1=0; s2=0.; sc=0.; perimeter=0, useIN=false; index=0;}
    ComputeBoxUTDF(GeomDomain& dom, Unknown& unk, Kernel& ker, const IntegrationMethods& ir, Real kin, const Complex& zin, Number nbrin,
                  Real si1, Real si2, Real sic, bool ini, const String& na="")
    {
      name=na;
      domp=&dom;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      Gp=&ker;
      IMir=&ir;
      k=kin;
      z=zin;
      nbr=nbrin;
      s1=si1;
      s2=si2;
      sc=sic;
      useIN=ini;
      obsp=nullptr;
      uobsp=nullptr;
      dataBox.add(_id);
      dataBox.add(_grad);
      index=0;
      computeCurData();
      if(z==0) {focks.push_back(new Fock(0,nbr,200)); nbr=focks[0]->nbr;}
    }

    ~ComputeBoxUTDF() {clear();}
    void clear()
    {
        if(focks.size()==0) return;
        for(std::vector<Fock*>::iterator it=focks.begin(); it!=focks.end();++it) delete *it;
        focks.clear();
    }
    virtual ComputeBoxType type() const {return _UTD;}
    virtual String typeName() const {return "UTD";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *const_cast<Unknown*>((++dataBox.begin())->second.unknown());}
    virtual bool hasComputedField() const {return obsp!=nullptr;}
    virtual const TermVector& computedField() const
    {
      if(obsp!=nullptr)
        return uobs;
      error("free_error","no computed field in ComputeBox "+name);
      return *new TermVector();
    }

    void computeCurData() //!< compute additional data at points of a parametrized boundary
    {
      Space* V = up->space();
      const FeDof* d0 = static_cast<const FeDof*>(&V->dof(1)), * dp=d0, *dc=nullptr;
      std::vector<std::pair<Element*, Number> >::const_iterator iti = d0->elements().begin();
      Real tp=toParameter(dp->coords()), tc, cp=fcurvature(tp), cc, lp=flength(tp), lc;
      Real s=0, h;
      Vector<Complex> psi(nbr,0.);
      Real a23= std::pow(2,-1./3.);
      curdatas[dp] = CurvatureData(tp,s,cp,0.,lp,psi,fnormal(tp), ftangent(tp),1);
      Element * elt;
      Number nd, ndof;
      while(dc!=d0)
        {
          elt = iti->first;;
          nd = iti->second;
          ndof = elt->dofNumbers[nd%2];
          dc = &elt->feSpaceP()->fedof(ndof);
          tc = toParameter(dc->coords());
          cc = fcurvature(tc);
          lc=flength(tc);
          Real dt=abs(tc-tp);
          if(dt > pi_) dt=abs(dt-2*pi_);
          h=0.5*dt*(lp+lc);   //approximation of the arc length
          s+=h;
          Real dsc=(cc-cp)/h;       //approx. of the s derivative of the curvature
          for(Number i=0; i<nbr; i++)  psi[i]+=h*0.5*a23*(std::pow(cp,2./3.)+std::pow(cc,2./3.));
          iti = dc->elements().begin();
          if(iti->first==elt) iti++;
          if(dc!=d0) curdatas[dc] = CurvatureData(tc,s,cc,dsc,lc,psi,fnormal(tc), ftangent(tc), ndof);
          else curdatas[dc].dscurvature=dsc; //update derivative of the curvature
          dp=dc; cp=cc; tp=tc; lp=lc;
          //thePrintStream<<"tc="<<tc<<" h="<<h<<" s="<<s<<" c="<<cc<<" psi[0]="<<psi[0]<<eol;
        }
      perimeter=s;
      psit=psi;
      thePrintStream<<std::floor(s*k/(2*pi_))<<" wavelengths on perimeter ("<<s<<")"<<eol;
    }

    virtual void solve()
    {
      //update shadow element colors
      TermVector& ui = dataBox.begin()->second;
      TermVector& gradui = (++dataBox.begin())->second;
      TermVector gradsU =((gradui/ui).toImag())/k;
      setColor(*domp, gradsU, shadowColoringRule);
      std::vector<Int> color;
      buildShadowLightDofs(ui, gradui, k, shadowLightDofs, color, normals, phase, phaseGrad);
      Number ns=shadowLightDofs.size();
      if(z!=0) {focks.clear();focks.resize(ns);}
      Real st = perimeter, s3 = sc*perimeter;
      Complex z2=z*z;
      std::map<const FeDof*,CurvatureData>::iterator itm, itms;
      std::vector<Complex> vis(ns);
      std::vector<Real> shadowOrientation(ns);   // =(I|t) : if <0 shadow oriented as s decreasing, if >0 as s increasing TO BE IMPROVED
      for(Number j=0; j<ns; j++)
      {
          itms = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
          shadowOrientation[j]=dot(phaseGrad[j],itms->second.tangent);
          vis[j]= ui.getValue(*shadowLightDofs[j]).asComplex();
          if(z!=0)
          {
              focks[j]=new Fock(0,nbr,200,std::conj(i_*z)*std::pow(0.5*k/itms->second.curvature,1./3.));//Fock initialization
              thePrintStream<<"shadow-light transition at t="<<itms->second.t<<" c="<<itms->second.curvature<<" mu="<<std::conj(i_*z)*std::pow(0.5*k/itms->second.curvature,1./3.)<<eol;
          }
      }
      TermVector ui2 = TermVector(ui);
      Space* V = up->space();

      std::vector<Number> dofStatus(V->dimSpace(),0); // 0 : not computed, 1: Kirchoff, 2: Shadow, 3: Fock

      // Limit of shadow Fock zone sf[j] = min{s>0 such that sb>s1}, s related to shadow/light point j
      // precomputation of c, psi at transition Fock shadow - creeping
      Vector<Real> s2f(ns,st),s2fb(ns,s2),c2(ns,-1.),s1f(ns,-st),s1fb(ns,s1),spec(ns), specb(ns), s1fm(ns,-st),s1fbm(ns,0);
      Vector<Vector<Complex> > psi2(ns,Vector<Complex>(nbr));
      for(Number j=0; j<ns; j++)
        {
          Real isnmin=1;
          itms = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
          Real ori = shadowOrientation[j];
//          thePrintStream<<" Shadow/Light P_"<<j<<"="<<shadowLightDofs[j]->coords()<<" (t="<< toParameter(shadowLightDofs[j]->coords())
//                        <<")  orient="<<ori<<eol;
//          String fn="s1b_"+tostring(j+1)+".dat";
//          std::ofstream fout1(fn.c_str());
//          fn="s2b_"+tostring(j+1)+".dat";
//          std::ofstream fout2(fn.c_str());
          for(itm=curdatas.begin(); itm!=curdatas.end(); ++itm)
            {
              Int col=color[itm->second.ndof-1];
              if(col==-2) //in shadow
                {
                  Real s = itm->second.abcissa-itms->second.abcissa, c = itm->second.curvature;
                  if ((s>0 && ori<0) || (s<0 && ori>0)) s=st-abs(s); else s=abs(s);
                  Real sb = std::pow(0.5*k*c*c,1./3.)*s;
                  if(sb>=s2 && s<s2f[j]) //Fock limiter : max{s; sb>=s2}
                    {
                      s2f[j]=s;
                      s2fb[j]=sb;
                      c2[j]=c;
                      for(Number i=0; i<nbr; i++) psi2[j][i]=itm->second.psi[i];
//                      thePrintStream<<"shadow t="<<itm->second.t<<" s="<<s<<" sb="<<sb<<" s2f[j]="<<s2f[j]<<eol;
                    }
//                    fout2<<s<<" "<<sb<<" "<<c<<" "<<eol;
                }
                else if(col==0) //in light
                {
                  Real s = itm->second.abcissa-itms->second.abcissa, c = itm->second.curvature, dc=itm->second.dscurvature;
                  if ((s>0 && ori>0) || (s<0 && ori<0)) s=abs(s)-st; else s=-abs(s);
                  Real sb;
                  Number n = itm->second.ndof;
                  Vector<Complex> gv=(++dataBox.begin())->second.getValue(n).asComplexVector()/(k*(dataBox.begin())->second.getValue(n).asComplex());
                  Real isn=dot(imag(gv),itm->second.normal);
                  if(useIN) sb = std::pow(0.5*k/c,1./3.)*isn;
                  else sb = std::pow(0.5*k*c*c,1./3.)*s;
                  if(isn<isnmin) {isnmin=isn;spec[j]=s;specb[j]=sb;}  // detect specular point
                  if(sb<=s1 && s>s1f[j]) //first Fock limiter :  min{s; sb> s1}
                    {
//                      thePrintStream<<"light  t="<<itm->second.t<<" s="<<s<<" sb="<<sb<<" s1f[j]="<<s1f[j]<<eol;
                      s1f[j]=s;
                      s1fb[j]=sb;
                    }
                  // second Fock limiter : max{s; (c^(2/3)s)'=0 <=> c + 2*c'*s/3=0}, use  max{s; c + 2*c'*s/3<0}
                  Real der;
                  if (ori>0) der=c+2*dc*s/3; else der=c-2*dc*s/3;
                  if(der <= 0 && s>s1fm[j]) {s1fm[j]=s;s1fbm[j]=sb;}
                  //fout1<<s<<" "<<sb<<" "<<c<<" "<<dc<<" "<<der<<eol;
                }
            }
//            fout1.close();fout2.close();
//            thePrintStream<<" s1f[j]="<<s1f[j]<<" s1fm[j]="<<s1fm[j]<<" spec[j]="<<spec[j]<<eol;
            if(s1f[j]==-st) {s1f[j]=s1fm[j];s1fb[j]=s1fbm[j];} //sb(s) does not cross s1, take s min(sb)
            if(s1f[j]<0.98*spec[j]) // restrict to exclude specular point from the light Fock region
                {
                    s1f[j]=0.98*spec[j];
                    s1fb[j]=0.98*s1fbm[j];  // if using isn, assume new isn is close to 0.95*isn, correct if isn is not used
                }
        }
      //thePrintStream<<" Fock limiter : s1f="<<s1f<<"->"<<s1fb<<" s2f="<<s2f<<"->"<<s2fb<<eol;

      for(Number e=0; e<V->nbOfElements(); e++) //loop on elements
        {
          const Element* elt=V->element_p(e);
          bool shadow =  elt->geomElt_p->color!=0;
          const std::vector<Number>& dofNum=elt->dofNumbers;
          std::vector<Number>::const_iterator itn=dofNum.begin();
          for(itn = dofNum.begin(); itn!=dofNum.end(); ++itn)
            {
              Number n=*itn;
              if(dofStatus[n-1]==0)
                {
                  Complex vi=ui2.getValue(n).asComplex();
                  itm = curdatas.find(static_cast<const FeDof*>(&elt->feSpaceP()->dof(n)));
                  if(itm==curdatas.end()) error("free_error","abnormal failure in ComputeBoxUTD::solve");
                  Real s = itm->second.abcissa, c = itm->second.curvature, l=itm->second.length;
                  Vector<Complex>& psi = itm->second.psi;
                  Vector<Real>& normal = itm->second.normal;
//                  thePrintStream<<"n="<<n<<" p="<<itm->first->coords()<<" shadow="<<shadow<<" t="<<toParameter(itm->first->coords())<<eol;
                  for(Number j=0; j<ns; j++) // analyze distance to shadow/light point
                    {
                      Number fj=0;
                      if(z!=0) fj=j;
                      Fock& fock = *focks[fj];
                      Vector<Complex> cte(fock.nbr),xi(fock.nbr);
                      for(Number i=0; i<fock.nbr; i++)
                        {
                          xi[i]=std::conj(fock.poles[i]);
                          cte[i]=2*i_*sqrtOfpi_/std::conj(fock.vpoles[i]);
                        }
                      itm = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
                      if(itm==curdatas.end()) error("free_error","abnormal failure in ComputeBoxUTD::solve");
                      Real ori = shadowOrientation[j];
                      Real c0 = itm->second.curvature;
                      Real ss0 = s-itm->second.abcissa, ss=ss0;
                      ss=abs(ss0);
                      bool jump= (ss0>0 && ((shadow && ori<0) || (!shadow && ori>0))) || (ss0<0 && ((shadow && ori>0) || (!shadow && ori<0)));
                      if(jump) ss=st-ss;
                      if(!shadow) ss*=-1;
                      Complex v=0.;
                      Vector<Complex> gv=((++dataBox.begin())->second.getValue(n).asComplexVector()/(k*vi));
                      Real isn=dot(imag(gv),normal);
                      Real sb=std::pow(0.5*k*c*c,1./3.)*ss;
//                      thePrintStream<<"   j="<<j<<" s="<<s<<" sld="<<itm->second.abcissa<<" ss0="<<ss0<<" ss="<<ss<<" sb="<<sb;
                      if(useIN && ss<=0 && ss>-0.25*st)
                      {
                          sb = std::pow(0.5*k/c,1./3.)*isn; //in light region, compute sb using I|n
//                          thePrintStream<<" sb (isn)="<<sb;
                      }
                      Complex am=isn/(isn-z);
                      if(ss>s1f[j] && sb>s1 && sb<0) // Fock light : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          Real x=static_cast<const FeDof&>(elt->feSpaceP()->dof(n)).coords()(1);
                          if(useIN)
                          {
                              v=std::conj(fock(sb))*vi*std::exp(i_*k*isn*isn*isn/(6*c));    // explicit ui-rescaling
                              if(z!=Complex(0.)) v*=(isn-std::pow(c/c0,1./3.)*z)/(isn-z); // additional correction when z!=0 (do not modify the value at s=0!)
                          }
                          else
                          {
                              v=std::conj(fock(sb))*vi*std::exp(i_*k*c*c*ss*ss*ss/6);
                              if(z!=Complex(0.)) v*=(c*ss-std::pow(c/c0,1./3.)*z)/(c*ss-z); // additional correction when z!=0 (do not modify the value at s=0!)
                          }
                          ui.setValue(n,v);
                          dofStatus[n-1]=3;
//                          thePrintStream<<" light Fock v="<<v;
                        }
                      if((ss<=s1f[j] || sb<=s1) && dofStatus[n-1]==0) // light : use Kirchoff
                        {
                          ui.setValue(n,2*vi*am);
                          dofStatus[n-1]=1;
//                          thePrintStream<<" light Kirchoff v="<<2*vi;
                        }
                      if((ss>=s2f[j] || sb>=s2) && dofStatus[n-1]==0) // shadow : set to 0 the first time
                        {
                          ui.setValue(n,0.);
                          dofStatus[n-1]=2;
//                          thePrintStream<<" shadow v=0";
                        }
                      if(ss>=0 && ss<s2f[j] && sb<s2) //  Fock shadow : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          v=std::conj(fock(sb))*std::exp(i_*k*ss)*vis[j]; //take into account ui at transition
                          ui.setValue(n,v);
                          dofStatus[n-1]=3;
//                          thePrintStream<<" shadow Fock v="<<v;
                        }
//                      thePrintStream<<eol;
                      // add creeping waves
                      ss=std::abs(ss0);jump=false;
                      if ((ss0>0 && ori<0) || (ss0<0 && ori>0)) {ss=st-ss;jump=true;}
                      if((ss>=s2f[j] || sb>=s2) && ss<=s3)
                        {
                          v=0;
                          Complex mus =std::pow(0.5*k/c,1./3.), mu0= std::pow(0.5*k/c0,1./3.), mu2 = std::pow(0.5*k/c2[j],1./3.);
                          for(Number i=0; i<fock.nbr; i++)
                            {
                              Complex ps = xi[i]*(psi[i]-psi2[j][i]);
                              if(jump) {if(ss0<0) ps=xi[i]*psit[i]+ps; else ps=xi[i]*psit[i]-ps;}
                              if(std::imag(ps)<0) ps=-ps;
                              Complex kai=std::sqrt((xi[i]+mu2*mu2*z2)/(xi[i]+mus*mus*z2));  // 1 if z=0
                              v+=std::exp(i_*(k*ss+std::pow(k,1./3.)*ps+s2fb[j]*xi[i]))*std::pow(c/c2[j],1./6.)*kai*cte[i]*vis[j]; //take into account ui at transition
                            }
                          v+=ui.getValue(n).asComplex();
//                          thePrintStream<<"  after creeping vf="<<v<<eol;
                          ui.setValue(n,v);
                        }
                    }
                }
            }
        }

      String na="current_UTD_"+tostring(index)+".dat";
      std::ofstream out(na.c_str());
      for(Number i=0; i<V->dimSpace(); i++)
        {
          const Point& P=V->feDofCoords(i);
          Complex v=ui.getValue(i+1).asComplex();
          out<<toParameter(P)<<" "<<std::real(v)<< " "<<std::imag(v)<<eol;
        }
      out.close();
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      if(&repdom==obsp)
        return uobs; //special case
      TermVector& ui = dataBox.begin()->second;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
      {
          DRep[dup]=integralRepresentation(ur,repdom,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
          if(z!=Complex(0.)) DRep[dup]+=(i_*k*z)*integralRepresentation(ur,repdom,intg(*domp,(*Gp)**up,_method=*IMir));
      }
      TermVector res=DRep[dup]*ui;
      if(obsp!=nullptr) //update field on observation domain
        {
          dup=std::make_pair(obsp,uobsp);
          if(DRep.find(dup)==DRep.end())
            {
                DRep[dup]=integralRepresentation(*uobsp,*obsp,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
                if(z!=Complex(0.)) DRep[dup]+=(i_*k*z)*integralRepresentation(ur,repdom,intg(*domp,(*Gp)**up,_method=*IMir));
            }
          uobs+=DRep[dup]*ui;
        }
      return res;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxKirchoff does not yet propose integral representation of Dn");
      return TermVector(); //fake return
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxComputeBoxKirchoff does not yet propose integral representation of gradient");
      return TermVector(); //fake return
    }
};

//=====================================================================================================================
//  General iterative scheme
//=====================================================================================================================
//    cbL : compute box related to large obstacle
//    cbS : compute box related to small obstacle
//    niter : number of iterations
//    viewStep : step for output
//    uv : unknown pointer used for saving diffracted fields to file at each step, if 0 nothing is saved
//
//    NOTE: cbL.dataBox and cbS.dataBox have to be initialized with the incident field (see ComputeBox::setData functions)
//          at the end of process, cbS.dataBox and cbL.dataBox contains the cumulative data allowing to reconstruct the total field
void iterativeScheme(ComputeBox& cbS, ComputeBox& cbL, Number niter, Number viewStep=0, Unknown* uv=nullptr)
{
  GeomDomain* obs =nullptr;
  if(uv!=nullptr)
    obs = const_cast<GeomDomain*>(uv->space()->domain());
  String na=cbL.typeName()+"_"+cbS.typeName();

  DataBox dbL = cbL.dataBox, dbS = cbS.dataBox;
  for(Number n=1; n<=niter; n++)
    {
      bool view= viewStep!=0 && ((n-1)% viewStep ==0 || n==niter);
      theCout<<" iteration "<<n<<eol<<" ---------"<<eol;

      // solve L obstacle
      cbL.iter=n;
      cbL.update_obs=true;
      cbL.solve();
      cbL.computeOn(cbS);          // compute cbS.dataBox using cbL.dataBox
      dbS += cbS.dataBox;
      if(n==1)
        cbS.dataBox = dbS;  // at first iteration take for cbS the incident field + the diffracted field by cbL
      if(view)
        {
          thePrintStream<<" cbL : "<<cbL.dataBox<<eol;
          saveToFile("cbL_"+na+"_"+tostring(n),cbL.dataBox.begin()->second,_format=_vtu);
          if(uv!=nullptr)
            saveToFile("UL_"+na+"_"+tostring(n),cbL.computeOn(*obs,*uv),_format=_vtu);
        }

      //solve S obstacle
      cbS.iter=n;
      cbS.solve();
      cbS.computeOn(cbL);          // compute cbL.dataBox using cbS.dataBox
      dbL+=cbL.dataBox;
      if(view)
        {
          thePrintStream<<" cbS : "<<cbS.dataBox<<eol;
          saveToFile("cbS_"+na+"_"+tostring(n),cbS.dataBox.begin()->second,_format=_vtu);
          if(uv!=nullptr)
            saveToFile("US_"+na+"_"+tostring(n),cbS.computeOn(*obs,*uv),_format=_vtu);
        }
    } //end of iterative loop

  //update cumulative data and re-solve if field has not been computed
  cbL.dataBox=dbL;
  cbS.dataBox=dbS;
  if(!cbL.hasComputedField())
    cbL.solve();
  if(!cbS.hasComputedField())
    cbS.solve();
}

//    cbs : list of compute boxes
//    niter : number of iterations
//    viewStep : step for output
//    uv : unknown pointer used for saving diffracted fields to file at each step, if 0 nothing is saved
//
//    NOTE: cbs[i].dataBox have to be initialized with the incident field (see ComputeBox::setData functions)
//          at the end of process, cbs[i].dataBox contains the cumulative data allowing to reconstruct the total field
void iterativeScheme(std::vector<ComputeBox*>& cbs, Number niter, Number viewStep=0, Unknown* uv=nullptr)
{
  GeomDomain* obs =nullptr;
  if(uv!=nullptr)
    obs = const_cast<GeomDomain*>(uv->space()->domain());
  Number nbox = cbs.size();
  std::vector<DataBox> dbs(nbox);
  for(Number i=0; i<nbox; i++)
    {
      dbs[i]=cbs[i]->dataBox;
      thePrintStream<<"databox "<<tostring(i)<<": "<<cbs[i]->dataBox<<eol;
    }

  for(Number n=1; n<=niter; n++)
    {
      bool view= viewStep!=0 && ((n-1)% viewStep ==0 || n==niter);
      if(view)
        thePrintStream<<" iteration "<<n<<eol<<" ---------"<<eol;
      for(Number i=0; i<nbox; i++)
        {
          // solve obstacle i
          ComputeBox& cbi=*cbs[i];
          cbi.iter=n;
          cbi.update_obs=true;
          cbi.solve();
          for(Number j=0; j<nbox; j++) // contribution from cbi to cbj
            if(j!=i)
              {
                ComputeBox& cbj=*cbs[j];
                cbi.computeOn(cbj);
                dbs[j] += cbj.dataBox;
                if(n==1 && i==0)
                  cbj.dataBox = dbs[j];  // at first iteration take for cbS the incident field + the diffracted field by cbL
                thePrintStream<<"databox "<<tostring(j)<<": "<<cbj.dataBox<<eol;
              }
        }

    } //end of iterative loop

  //update cumulative data only if field has not been computed
  for(Number i=0; i<nbox; i++)
    {
      cbs[i]->dataBox=dbs[i];
      if(!cbs[i]->hasComputedField())
        cbs[i]->solve();
    }

}


//=====================================================================================================================
// mesh stuff
//=====================================================================================================================

Geometry circleByArcs(const Point& c, Real radius, Real hs, const String& name)
{
  Point S1=c+Point(radius,0.), S2=c+Point(0.,radius), S3=c+Point(-radius,0.), S4=c+Point(0.,-radius);
  CircArc c1(_center=c,_v1=S1,_v2=S2,_hsteps=hs,_domain_name=name);
  CircArc c2(_center=c,_v1=S2,_v2=S3,_hsteps=hs,_domain_name=name);
  CircArc c3(_center=c,_v1=S3,_v2=S4,_hsteps=hs,_domain_name=name);
  CircArc c4(_center=c,_v1=S4,_v2=S1,_hsteps=hs,_domain_name=name);
  return c1+c2+c3+c4;
}

// Function generating mesh of the only large obstacle: ellipse (x0+aRcos(t),y0+bRsin(t)) rotated with theta angle
void doMeshEllipse(const Point& center, Real radius, Real a, Real b, Real theta, Real hsize, Mesh& meshObs, Real xr, bool view2d, Mesh& meshR)
{
  //computation mesh
  Ellipse obs(_center=center, _xlength=a*radius, _ylength=b*radius, _hsteps=hsize, _domain_name="LargeObs",_side_names="obstacle");
  if(theta!=0)
    obs.rotate2d(center,theta);
  std::set<MeshOption> mos;
  meshObs.buildMesh(obs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=center, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      meshR.buildMesh(obsR-obs, _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(center, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR,_segment,1,_gmsh, _defaultPattern, _noSplitRule);
    }
}

void saveFields(const TermVector& Ut, const TermVector& Ud, const TermVector& Ui,
                Domain sideExt, const Complex& amp, const String& name, bool view2d=true)
{
  if(view2d)
    {
      saveToFile("Ut"+name,Ut,_format=_vtu);
      saveToFile("Ud"+name,Ud,_format=_vtu);
      saveToFile("Ui",Ui,_format=_vtu);
      saveFarField(Ud|sideExt, amp, "Ufar"+name+".dat");
    }
  else
    {
      saveFarField(Ud, amp, "Ufar"+name+".dat");
      //saveToFile("Ufar"+meth+obstacleName(obst),amp*UdBEM,_format=_vtu);
    }
}
//=====================================================================================================================

Kernel G;

// incident field functions
// Function evaluating incident plane wave for Dirichlet Boundary Condition
Complex uinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kr=kx*p(1)+ky*p(2);
  return exp(i_*kr);
  //return Helmholtz2d(0.*p,p,pa);
}

Complex muinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kr=kx*p(1)+ky*p(2);
  return -exp(i_*kr);
  //return -Helmholtz2d(0.*p,p,pa);
}

/* Function evaluating normal derivative of incident plane wave: for Neumann Boundary Condition */
Complex duinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kr = kx*p(1)+ky*p(2);
  const Vector<Real>& n = getN();
  return i_*(kx*n(1)+ky*n(2))*exp(i_*kr);
}

// Function evaluating a linear combination of the plane wave and its normal derivative for Fourier Boundary condition
Complex fuinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kr = kx*p(1)+ky*p(2);
  Complex mu = pa("mu"), nu=pa("nu");
  const Vector<Real>& n = getN();
  return (nu*i_*(kx*n(1)+ky*n(2))+mu)*exp(i_*kr);
}

// Function evaluating the gradient of incident plane wave
Vector<Complex> guinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kr = kx*p(1)+ky*p(2);
  Complex a=i_*exp(i_*kr);
  Vector<Complex> g(2);
  g(1)=a*kx;
  g(2)=a*ky;
  return g;
}

//=====================================================================================================================
// main program
//=====================================================================================================================
void dev_Eric1(int argc, char* argv[], bool check)
{
  String rootname = "dev_Eric1";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  //numberOfThreads(1);
  verboseLevel(10);
  String errors;
  Number nbErrors = 0;

  Options opts;
  opts.addOption("k",25.);
  opts.addOption("z",Complex(0.));
  opts.addOption("ellipse_a","ae",1.);
  opts.addOption("ellipse_b","be",1.);
  opts.addOption("egg_c","ce",1.);
  opts.addOption("mesh_refinement","mr",10);
  opts.addOption("ellipse_rotation_angle_(deg)","nue",0.);
  opts.addOption("use_(i|n)","isn",true);
  opts.addOption("compute_BEM","bem",true);
  opts.addOption("view_2D",false);
  opts.addOption("shape","sh","ellipse");
  opts.addOption("s1",-2.5);
  opts.addOption("s2",1.);
  opts.addOption("s3",0.5);
  opts.addOption("number_of_residue","nbr",1);
  opts.addOption("incidence_angles_(deg)","ias",Reals(0,0,30));  //(amin,amax,da) : exp(ik(cos(a)x+sin(a)y))  amin + i*da <= amax,

  opts.parse("opt.txt");

  k=opts("k");
  Complex z=opts("z");
  Number mr=opts("mesh_refinement");
  ae=opts("ellipse_a");
  be=opts("ellipse_b");
  ce=opts("egg_c");
  nue =opts("ellipse_rotation_angle_(deg)");
  nue*=pi_/180;
  bool isn=opts("use_(i|n)").get_b();
  bool withBEM=opts("compute_BEM").get_b();
  String shape=opts("shape");
  Real s1=opts("s1"), s2=opts("s2"), s3=opts("s3");
  Number nbr=opts("number_of_residue");
  Reals ias=opts("incidence_angles_(deg)");
  bool view2d=opts("view_2D").get_b();
  theCout<<" compute on "<<shape<<" with ae="<<ae<<", be="<<be<<", ce="<<ce<<", nue="<<nue<< "\xb0, mr="<<mr<<", k="<<k<<", z="<<z<<", isn="<<isn<<", BEM="<<withBEM
         <<", s1="<<s1<<", s2="<<s2<<", s3="<<s3<<", nbr="<<nbr;
  theCout<<", incidence angles (deg) : ";
  for(Real i=ias[0]; i<=ias[1]; i+=ias[2]) theCout<<i<<"\xb0 ";
  theCout<<eol;

  //define parameters
  Real k2=k*k;
  Real wavelength = 2*pi_/k;
  kx=k*cos(ias[0]);
  ky=k*sin(ias[0]);
  Parameters pars;
  pars << Parameter(k,"k") << Parameter(kx, "kx") << Parameter(ky, "ky"); // kx, ky


  //define Kernel
  G=Helmholtz2dKernel(pars);
  Kernel Gsym;
  Kernel* GL=&G, *GS=&G;

  // Mesh characteristics
  Real hsize = wavelength/mr;
  Point center(0.,0.);
  Real radius=k*wavelength;
  Mesh mR, meshObs;
  Real xr = 2*std::max(ae,1.)*radius;
  Complex amp=std::sqrt(k*pi_*xr/2);

  if(shape=="egg")
    {
      doMeshEllipse(center, 2*radius, 1, 1, 0, hsize, meshObs, xr, view2d, mR);
      for(Number i=0; i < meshObs.nodes.size(); ++i)
        {
          Point& p=meshObs.nodes[i];
          Real t = atan2(p(2),p(1));
          meshObs.nodes[i]=egg_parametrization(t);
        }
      parametrization=egg_parametrization;
      toParameter=egg_toParameter;
      flength=egg_flength;
      fcurvature=egg_fcurvature;
      fnormal=egg_fnormal;
      ftangent=egg_ftangent;
    }
  else
    {
      doMeshEllipse(center, 2*radius, ae, be, nue, hsize, meshObs, xr, view2d, mR);
      parametrization=ellipse_parametrization;
      toParameter=ellipse_toParameter;
      flength=ellipse_flength;
      fcurvature=ellipse_fcurvature;
      fnormal=ellipse_fnormal;
      ftangent=ellipse_ftangent;
    }

  meshObs.saveToFile("mObs.msh",_msh);
  mR.saveToFile("mR.msh",_msh);
  GeomDomain* obsp=nullptr;
  if(view2d)
    obsp = mR.domainP("Omega_ext");
  else
    obsp = mR.domainP("side_ext");
  Domain omegaobs = *obsp;
  Domain obstacle=meshObs.domain("obstacle"); // large obstacle

  // Define observation space
  Space Vobs(_domain=omegaobs, _interpolation=P1, _name="V1", _notOptimizeNumbering);
  Unknown uobs(Vobs, _name="uobs");
  TestFunction vobs(uobs, _name="vobs");

  // Integration Methods
  IntegrationMethods IMieDuf(_method=Duffy,_order=20,_quad=_defaultRule,_order=15,_bound=1.,_quad=_defaultRule,_order=10,_bound=2.,_quad=_defaultRule,_order=8); // Duffy integration method
  IntegrationMethods IMir(_quad=_defaultRule,_order=20,_bound=1.,_quad=_defaultRule,_order=15,_bound=3,_quad=_defaultRule,_order=10);
  IntegrationMethods IMir2(_method=LenoirSalles2dIR(),_functionPart=_singularPart,_bound=theRealMax,_quad=QuadratureIM(_GaussLegendreRule,10),_functionPart=_regularPart,_bound=theRealMax);

  // Define functions
  Function finc(uinc,pars);        // incident wave
  Function dfinc(duinc,pars);      // normal derivative of incident wave
  Function ffinc(fuinc,pars);      // Fourier derivative of incident wave
  Function ginc(guinc,pars);       // gradient of incident wave
  dfinc.require(_n);
  ffinc.require(_n);

  InterpolationType pt=P1;
  String meth="?";
  TermVector Ui(uobs,omegaobs,finc);
  obstacle.setNormalOrientation(_towardsInfinite);
  obstacle.saveNormalsToFile("SL_normals",_vtk);
  Space V(_domain=obstacle, _interpolation=pt, _name="V", _notOptimizeNumbering);
  Unknown u(V, _name="u");
  TestFunction v(u, _name="v");

  TermVector UdBEM,UtBEM;
  if(withBEM) //BEM  CFIE
    {
      meth="_BEM_CFIE_";
      theCout<<"========================================================"<<eol;
      theCout<<"              "<<meth<<eol;
      theCout<<"========================================================"<<eol;
      TermMatrix M(intg(obstacle, u*v), _name="M");
      //ComputeBoxBEMCFIE cpBox(obstacle,u,G,IMieDuf,IMir2,k,0.2,true,"cpBox");
      ComputeBoxBEMF2 cpBox(obstacle,u,G,IMieDuf,IMir2,k,i_*k*z,true,"cpBox");
      for(Real i=ias[0]; i<=ias[1]; i+=ias[2])
        {
          theCout<<"===== compute CFIE for the incidence "<<i<<"\xb0 ======"<<eol;
          Real a=i*pi_/180;
          cpBox.index=i;
          kx=k*cos(a);
          ky=k*sin(a);
          TermVector Uio(u,obstacle,finc);
          TermVector DnUio= directSolve(M,TermVector(intg(obstacle, dfinc*v)));
          cpBox.setData(Uio,DnUio);
          cpBox.solve();
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          UtBEM = UdBEM+Ui;
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, meth+tostring(i),view2d);
        }

    }

  meth="_UTD_";
  theCout<<"========================================================"<<eol;
  theCout<<"              "<<meth<<eol;
  theCout<<"========================================================"<<eol;
  Unknown u2(V, _name="u2", _dim=2); //vector unknown
  obstacle.setColorFlag(false);  //use or not use the restriction to 0 color elements in integral representation computations
  //ComputeBoxUTD cpBox(obstacle,u,G,IMir2,k,nbr,s1,s2,s3,0., isn, "box L");
  ComputeBoxUTDF cpBox(obstacle,u,G,IMir2,k,z,nbr,s1,s2,s3,isn,"box L");
  for(Real i=ias[0]; i<=ias[1]; i+=ias[2])
    {
      theCout<<"===== compute UTD for the incidence "<<i<<"\xb0 ======"<<eol;
      Real a=i*pi_/180;
      kx=k*cos(a);
      ky=k*sin(a);
      cpBox.index=i;
      cpBox.setData(TermVector(u,obstacle,finc),TermVector(u2,obstacle,ginc)); //ui and grad(ui) on S
      cpBox.solve();
      TermVector UdUTD = cpBox.computeOn(omegaobs,uobs);
      TermVector UtUTD = UdUTD+Ui;
      saveFields(UtUTD, UdUTD, Ui, mR.domain("side_ext"),amp, meth+tostring(i),view2d);
    }

//------------------------------------------------------------------------------------
// save results in a file or compare results with some references value in a file
//------------------------------------------------------------------------------------
if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
