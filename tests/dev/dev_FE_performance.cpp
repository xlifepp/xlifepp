/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_FE_performance.cpp
\author E. Lunéville
\since 21 oct 2015
\date 1 March 2015
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace dev_FE_performance
{

void dev_FE_performance(int argc, char* argv[], bool check)
{
   String rootname = "dev_FE_performance";
   trace_p->push(rootname);
   std::stringstream out;
   out.precision(testPrec);
   verboseLevel(30);
  String errors;
  Number nbErrors = 0;

   Strings sidenames("y=ymin", "x=xmax", "y=ymax", "x=xmin");
   Number dkmin=1, dkmax=1;
   MeshGenerator mg=_gmsh;

   for (Number dk=dkmin; dk<=dkmax; dk++)  //loop on degree
   {
      thePrintStream << "\n=======================  test Degree " << dk << " ==========================" << eol;

        Number nmin=200, nmax=200/dk, sn=50;
        Number si=(nmax-nmin)/sn;si+=1;
        std::vector<Number> nbdofs(si);
        Vector<Real> tMsh(si), tVk(si), tM0(si), tM(si), tK(si), tKPM(si), tKPM2(si), tDXUV(si), tUDXV(si);
        Number s=0;
        for (Number n=nmin; n<=nmax; n+=sn, s++)  //loop on mesh refinement
        {
           Mesh mesh2d(Rectangle(_xmin=0,_xmax=1,_ymin=0,_ymax=1,_nnodes=n,_domain_name="Omega"), _shape=_triangle , _order=1, _generator=mg);
           Domain omega = mesh2d.domain("Omega");
           tMsh[s]=elapsedTime("=== time to mesh :", thePrintStream);

           Space Vk(_domain=omega, _FE_type=Lagrange, _order=dk, _name="Vk", _notOptimizeNumbering);
           Unknown u(Vk, _name="u"); TestFunction v(u, _name="v");
           thePrintStream << "number of nodes by side : " << n << " ===> ";
           thePrintStream << Vk.nbOfElements() << " elements , "<<Vk.nbDofs()<<" dofs ";
           nbdofs[s]=Vk.nbDofs();
           TermVector un(u,omega,1.);
           tVk[s]=elapsedTime("to construct space Vk :", thePrintStream);

           TermMatrix M0(intg(omega, u *v));
           //thePrintStream<<"(M*un|un)= "<<real(M0*un|un)<<" ";
           tM0[s]=elapsedTime("to construct matrix M", thePrintStream);
           TermMatrix M(intg(omega, u *v));
           //thePrintStream<<"(M*un|un)= "<<real(M*un|un)<<" ";
           tM[s]=elapsedTime("to reconstruct matrix M", thePrintStream);
           TermMatrix K(intg(omega, grad(u) | grad(v)));
           //thePrintStream<<"(K*un|un)= "<<real(K*un|un)<<" ";
           tK[s]=elapsedTime("to construct matrix K", thePrintStream);
           TermMatrix KPM(intg(omega, grad(u) | grad(v))+intg(omega, u *v));
           //thePrintStream<<"(KPM*un|un)= "<<real(KPM*un|un)<<" ";
           tKPM[s]=elapsedTime("to construct matrix KPM", thePrintStream);
           TermMatrix KPM2=K+M;
           //thePrintStream<<"((K+M)*un|un)= "<<real(KPM2*un|un)<<" ";
           tKPM2[s]=elapsedTime("to construct matrix K+M", thePrintStream);
           TermMatrix DXUV=(intg(omega, dx(u) *v));
           //thePrintStream<<"((DXUV)*un|un)= "<<real(DXUV*un|un)<<" ";
           tDXUV[s]=elapsedTime("to construct matrix DXUV", thePrintStream);
           TermMatrix UDXV=(intg(omega, u*dx(v)));
           //thePrintStream<<"((UDXV)*un|un)= "<<real(UDXV*un|un)<<" ";
           tUDXV[s]=elapsedTime("to construct matrix UDXV", thePrintStream);
        }

       thePrintStream << " computation tables (nbdofs, mesh time, space time, M0 time, M time, K time, KPM time, K+M time, DXUV time, UDXV time) "<<eol;
       for(s=0;s<si; s++)
         thePrintStream<<nbdofs[s]<<" "<<tMsh[s]<<" "<<tVk[s]<<" "<<tM0[s]<<" "<<tM[s]<<" "
                        <<tK[s]<<" "<<tKPM[s]<<" "<<tKPM2[s]<<" "<<tDXUV[s]<<" "<<tUDXV[s]<<eol;
   }

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
