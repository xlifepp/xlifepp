/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_isogeometric.cpp
\author E. Lunéville
\since  january 2022
\date  january 2022
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"


using namespace std;
using namespace xlifepp;

namespace dev_isogeometric
{
Number k=2;
Real eps=1.E-8;

Real uex(const Point& p, Parameters& pars=defaultParameters)
{
  Real x=p(1), y=p(2), r=std::sqrt(x*x+y*y), rk=r, t=std::atan2(y,x);
  Number i=1;
  while (i<k) {rk*=r; i++;}
  return rk*std::cos(k*t);

}
Real g(const Point& p, Parameters& pars=defaultParameters)
{
  Real t=std::atan2(p(2),p(1));
  return std::cos(k*t);
}

//Squelched Grid Open Mapping square<->disk
Vector<Real> sgomPar(const Point& uv, Parameters& pars, DiffOpType d=_id)
{
  Number dim=2;
  Vector<Real> xy(dim);
  Real u=2*uv[0]-1, v=2*uv[1]-1, u2=u*u, v2=v*v, n2=1-u2*v2;
  switch (d)
  {
    case _id:
      xy[0]=u*sqrt((1-v2)/n2);
      xy[1]=v*sqrt((1-u2)/n2);
      break;
    case _d1:

      break;
    case _d2:

      break;
    case _d12:
    case _d21:

      break;
    case _d11:
    case _d22:

      break;
    default:
      parfun_error("Squelched Grid Open Mapping", d);
  }
  return xy;
}

Vector<Real> invSgomPar(const Point& xy, Parameters& pars, DiffOpType d=_id)
{
  Number dim=2;
  Vector<Real> uv(dim);
  Real x=xy[0], y=xy[1];
  switch (d)
  {
    case _id:
      uv[0]=0.5*(1+x*sqrt(1-y*y));
      uv[1]=0.5*(1+y*sqrt(1-x*x));
      break;
    case _d1:

      break;
    case _d2:

      break;
    case _d12:
    case _d21:

      break;
    case _d11:
    case _d22:

      break;
    default:
      parfun_error("inv Squelched Grid Open Mapping", d);
  }
  return uv;
}

//2-Squircular square<->disk
Vector<Real> fgsqc2Par(const Point& uv, Parameters& pars, DiffOpType d=_id)
{
  Number dim=2;
  Vector<Real> xy(dim);
  Real u=2*uv[0]-1, v=2*uv[1]-1, u2=u*u, v2=v*v, n=sqrt(1+u2*v2);
  switch (d)
  {
    case _id:
      xy[0]=u/n;
      xy[1]=v/n;
      break;
    case _d1:

      break;
    case _d2:

      break;
    case _d12:
    case _d21:

      break;
    case _d11:
    case _d22:

      break;
    default:
      parfun_error("Squircular", d);
  }
  return xy;
}

Vector<Real> invFgsqc2Par(const Point& xy, Parameters& pars, DiffOpType d=_id)
{
  Number dim=2;
  Vector<Real> uv(dim);
  Real x=xy[0], y=xy[1], r=sqrt(1-sqrt(1-4*x*x*y*y)), s=sign(x*y)/sqrt(2);
  switch (d)
  {
    case _id:
      uv[0]=0.5*(1+r*s/y);
      uv[1]=0.5*(1+r*s/x);
      break;
    case _d1:

      break;
    case _d2:

      break;
    case _d12:
    case _d21:

      break;
    case _d11:
    case _d22:

      break;
    default:
      parfun_error("inv Squircular", d);
  }
  return uv;
}

//Elliptical square<->disk
Vector<Real> ellPar(const Point& uv, Parameters& pars, DiffOpType d=_id)
{
  Number dim=2;
  Real R=1+eps;
  Vector<Real> xy(dim);
  Real u=2*uv[0]-1, v=2*uv[1]-1, u2=u*u, v2=v*v, ru=sqrt(1-0.5*u2), rv=sqrt(1-0.5*v2);
  switch (d)
  {
    case _id:
      xy[0]=u*rv*R;
      xy[1]=v*ru*R;
      break;
    case _d1:
      xy[0]=2*rv*R;
      xy[1]=-u*v*R/ru;
      break;
    case _d2:
      xy[0]=-u*v*R/rv;
      xy[1]=2*ru*R;
      break;
    case _d12:
    case _d21:

      break;
    case _d11:
    case _d22:

      break;
    default:
      parfun_error("Elliptical", d);
  }
  return xy;
}

Vector<Real> invEllPar(const Point& xy, Parameters& pars, DiffOpType d=_id)
{
  Number dim=2;
  Real R=1+eps, R2=R*R;
  Vector<Real> uv(dim);
  Real x=xy[0], y=xy[1], x2=x*x, y2=y*y, r=2*sqrt(2);
  switch (d)
  {
    case _id:
      uv[0]=0.5*(1+0.5*sqrt(2+(x2-y2)/R2+r*x/R)-0.5*sqrt(2+(x2-y2)/R2-r*x/R));
      uv[1]=0.5*(1+0.5*sqrt(2+(y2-x2)/R2+r*y/R)-0.5*sqrt(2+(y2-x2)/R2-r*y/R));
      break;
    case _d1:

      break;
    case _d2:

      break;
    case _d12:
    case _d21:

      break;
    case _d11:
    case _d22:

      break;
    default:
      parfun_error("inv Elliptical", d);
  }
  return uv;
}

void dev_isogeometric(int argc, char* argv[], bool check)
{
  String rootname = "dev_isogeometric";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  numberOfThreads(1);
  verboseLevel(0);

  Real l1, li, l;
  // check mass matrix for a rectangle
  Rectangle R(_origin=Point(0.,0.),_xlength=1, _ylength=1,_nnodes=5,_domain_name="Omega");
  Mesh MeR(R,_shape=_triangle, _order=1, _generator=gmsh);
  Domain omegaR=MeR.domain("Omega");
  Space VR(_domain=omegaR,_interpolation=P1,_name="VR");
  Unknown ur(VR,_name="ur");TestFunction vr(ur,_name="vr");
  TermMatrix MR(intg(omegaR,ur*vr,_quad=GaussLegendre,_order=5),_name="MR");
  TermMatrix MRi(intg(omegaR,ur*vr,_isogeo,_quad=GaussLegendre,_order=5),_name="MRi");
  TermVector unR(ur,omegaR,1.);
  l1=real(MR*unR|unR), li= real(MRi*unR|unR), l=1;
  thePrintStream<<"check mass matrix for a rectangle "<<eol;
  thePrintStream<<"      P1 : MR*un|un  = "<<l1<<" error = "<<abs(l1-l)<<eol;
  thePrintStream<<"  iso-P1 : MRi*un|un = "<<li<<" error = "<<abs(li-l)<<eol;

  //check mass matrix for a quarter of circle
  CircArc CA(_center=Point(0.,0.,0.),_v1=Point(1.,0.,0.),_v2=Point(0.,1.,0.),_nnodes=5,_domain_name="Omega");
  Mesh MeC(CA, _shape=_segment, _order=1, _generator=_gmsh);
  Domain omegaC=MeC.domain("Omega");
  Space VC(_domain=omegaC,_interpolation=P1,_name="VC");
  Unknown uc(VC,_name="uc");TestFunction vc(uc,_name="vc");
  TermMatrix MC(intg(omegaC,uc*vc,_quad=GaussLegendre,_order=5),_name="MC");
  TermMatrix MCi(intg(omegaC,uc*vc,_isogeo,_quad=GaussLegendre,_order=5),_name="MCi");
  TermVector unC(uc,omegaC,1.);
  l1=real(MC*unC|unC), li= real(MCi*unC|unC), l=pi_/2;
  thePrintStream<<"check mass matrix for a quarter of circle "<<eol;
  thePrintStream<<"      P1 :  MC*un|un = "<<l1<<" error = "<<std::abs(l1-l)<<eol;
  thePrintStream<<"  iso-P1 : MCi*un|un = "<<li<<" error = "<<std::abs(li-l)<<eol;
  omegaC.meshDomain()->exportIsoNodes("isoC.dat",10);

  //check mass matrix for a quarter of disk, moving slightly center
  Disk HD(_center=Point(0.,0.),_radius=1,_angle1=0, _angle2=90*deg_,_nnodes=9,_domain_name="Omega");
  Mesh MeH(HD,_shape=_triangle, _order=1, _generator=gmsh);
  for(auto itn=MeH.nodes.begin();itn!=MeH.nodes.end();itn++)
  {
      if(norm(*itn)<theEpsilon) {(*itn)[0]=0.001;(*itn)[1]=0.001;break;}
  }
  saveToFile("meshHD.msh",MeH);
  Domain omegaH=MeH.domain("Omega");
  Space VH(_domain=omegaH,_interpolation=P1,_name="VH");
  Unknown uh(VH,_name="uh");TestFunction vh(uh,_name="vh");
  TermMatrix MH(intg(omegaH,uh*vh,_quad=GaussLegendre,_order=9),_name="MH");
  TermMatrix MHi(intg(omegaH,uh*vh,_isogeo,_quad=GaussLegendre,_order=9),_name="MHi");
  TermVector unH(uh,omegaH,1.);
  l1=real(MH*unH|unH), li= real(MHi*unH|unH), l=pi_/4;
  thePrintStream<<"check mass matrix for a quarter of disk, moving slightly the center "<<eol;
  thePrintStream<<"     P1 :  MH*un|un = "<<l1<<" error="<<std::abs(l1-l)<<eol;
  thePrintStream<<" iso-P1 : MHi*un|un = "<<li<<" error="<<std::abs(li-l)<<eol;
  omegaH.meshDomain()->exportIsoNodes("isoH.dat",10);

  //check mass matrix for a quarter of disk with small hole (radius = eps)
  Real h=0.1; Number p=8;
  Real eps=std::sqrt(4./pi_)*std::pow(10,(-0.5*p));
  CircArc Ce(_center=Point(0.,0.),_v1=Point(1.,0.),_v2=Point(0.,1.),_hsteps=h);
  CircArc Ci(_center=Point(0.,0.),_v1=Point(0.,eps),_v2=Point(eps,0.),_hsteps=h);
  Segment S1(_v1=Point(0.,1.),_v2=Point(0.,eps),_hsteps=h);
  Segment S2(_v1=Point(eps,0.),_v2=Point(1,0.),_hsteps=h);
  Geometry Cour=surfaceFrom(Ce+S1+Ci+S2,"Omega",true);
  Mesh MeCour(Cour,_shape=_triangle, _order=1, _generator=gmsh);
  MeCour.geometry_p=new Disk(_center=Point(0.,0.),_radius=1,_angle1=0, _angle2=90*deg_,_nnodes=10,_domain_name="cour");
  Domain omegaCour=MeCour.domain("Omega");
  omegaCour.setGeometry(MeCour.geometry_p);
  Space VCour(_domain=omegaCour,_interpolation=P1,_name="VCour");
  Unknown uo(VCour,_name="uo");TestFunction vo(uo,_name="vo");
  TermMatrix MO(intg(omegaCour,uo*vo,_quad=GaussLegendre,_order=5),_name="MO");
  TermMatrix MOi(intg(omegaCour,uo*vo,_isogeo,_quad=GaussLegendre,_order=5),_name="MOi");
  TermVector unO(uo,omegaCour,1.);
  l1=real(MO*unO|unO), li= real(MOi*unO|unO), l=pi_/4;
  thePrintStream<<"check mass matrix for a quarter of disk, with small hole (eps = "<<eps<<")"<<eol;
  thePrintStream<<"     P1 :  MO*un|un = "<<l1<<" error = "<<std::abs(l1-l)<<eol;
  thePrintStream<<" iso-P1 : MOi*un|un = "<<li<<" error = "<<std::abs(li-l)<<eol;
  omegaCour.meshDomain()->exportIsoNodes("isoO.dat",10);

  //check mass matrix for a disk with small hole (radius = eps)
  Disk De(_center=Point(0.,0.),_radius=1,_hsteps=h,_domain_name="Omega",_side_names="Gamma");
  Disk Di(_center=Point(0.,0.),_radius=eps,_hsteps=h,_domain_name="Omega");
  Geometry Dime=De-Di;
  Mesh MeD(Dime,_shape=_triangle, _order=1, _generator=gmsh);
  Domain omegaD=MeD.domain("Omega");
  omegaD.setGeometry(&De);
  Space VD(_domain=omegaD,_interpolation=P1,_name="VD");
  Unknown ud(VD,_name="ud");TestFunction vd(ud,_name="vd");
  TermMatrix MD(intg(omegaD,ud*vd,_quad=GaussLegendre,_order=5),_name="MD");
  TermMatrix MDi(intg(omegaD,ud*vd,_isogeo,_quad=GaussLegendre,_order=5),_name="MDi");
  TermVector und(ud,omegaD,1.);
  l1=real(MD*und|und), li= real(MDi*und|und), l=pi_;
  thePrintStream<<"check mass matrix for a disk, with small hole (eps = "<<eps<<")"<<eol;
  thePrintStream<<"     P1 :  M*un|un = "<<l1<<" error = "<<std::abs(l1-l)<<eol;
  thePrintStream<<" iso-P1 : Mi*un|un = "<<li<<" error = "<<std::abs(li-l)<<eol;
  omegaD.meshDomain()->exportIsoNodes("isoD.dat",10);

  Parameters pars;
  // solving Dirichlet pb in a disk
  Mesh Me(De,_shape=_triangle, _order=1, _generator=gmsh);
  Domain omega=Me.domain("Omega"), gamma=Me.domain("Gamma");
  Space V(_domain=omega,_interpolation=P1, _name="V");
  Unknown u(V,_name="u");TestFunction v(u,_name="v");
  TermVector Uex(u,omega,uex);
  EssentialConditions ec=(u|gamma=g);
  TermVector F(u,omega,0.);
  //standard case
  BilinearForm a=intg(omega,grad(u)|grad(v),_quad=GaussLegendre,_order=3);
  TermMatrix K(a,ec,_name="K");
  TermVector U=directSolve(K,F);
  TermVector E=U-Uex;
  TermMatrix M(intg(omega,u*v),_name="M");
  thePrintStream<<"solving Dirichlet problem in a disk"<<eol;
  thePrintStream<<"      P1 : L2 error = "<<real(std::sqrt(M*E|E))<<eol;
  thePrintStream<<"      P1 : C0 error = "<<norminfty(E)<<eol;
  saveToFile("Uex",Uex,_format=_vtu); saveToFile("U",U,_format=_vtu);
  //isogeometric case
  BilinearForm ai=intg(omega,grad(u)|grad(v),_isogeo,_quad=GaussLegendre,_order=3);
  TermMatrix Ki(ai,ec,_name="Ki");
  TermVector Ui=directSolve(Ki,F);
  E=Ui-Uex;
  TermMatrix Mi(intg(omega,u*v,_isogeo,_quad=GaussLegendre,_order=5),_name="Mi");
  thePrintStream<<"  iso-P1 : L2 error = "<<real(std::sqrt(Mi*E|E))<<eol;
  thePrintStream<<"  iso-P1 : C0 error = "<<norminfty(E)<<eol;
  saveToFile("Ui",Ui,_format=_vtu);

  // solving Dirichlet pb in a disk, using the elliptical parametrisation
  thePrintStream<<eol<<"using ellptical parametrization of the disk";
  Mesh MeDe(De,_shape=_triangle, _order=1, _generator=gmsh);
  Domain omegaDe=MeDe.domain("Omega"), gammaDe=MeDe.domain("Gamma");
  Parametrization parD(0,1,0,1,ellPar,pars);
  parD.invParametrization_p=invEllPar;
  omegaDe.setParametrization(parD);
  Space VDe(_domain=omegaDe,_interpolation=P1,_name="VDe");
  Unknown ude(VDe,_name="ude");TestFunction vde(ude,_name="vde");
  TermMatrix MDE(intg(omegaDe,ude*vde,_quad=GaussLegendre,_order=5),_name="MDE");
  TermMatrix MDEi(intg(omegaDe,ude*vde,_isogeo,_quad=GaussLegendre,_order=5),_name="MDEi");
  TermVector unde(ude,omegaDe,1.);
  l1=real(MDE*unde|unde), li= real(MDEi*unde|unde), l=pi_;
  thePrintStream<<"checking mass matrix"<<eol;
  thePrintStream<<"     P1 :  MDE*un|un = "<<l1<<" error = "<<std::abs(l1-l)<<eol;
  thePrintStream<<" iso-P1 : MDEi*un|un = "<<li<<" error = "<<std::abs(li-l)<<eol;

  EssentialConditions ece=(ude|gammaDe=g);
  TermVector Fe(ude,omegaDe,0.);
  BilinearForm ae=intg(omegaDe,grad(ude)|grad(vde),_quad=GaussLegendre,_order=5);
  TermMatrix Ke(ae,ece,_name="Ke");
  TermVector Ue=directSolve(Ke,Fe);
  TermVector Ueex(ude,omegaDe,uex);
  TermVector Ee=Ue-Ueex;
  BilinearForm muve=intg(omegaDe,ude*vde);
  thePrintStream<<"solving Dirichlet problem "<<eol;
  thePrintStream<<"     P1 : L2 error = "<<real(std::sqrt(MDE*Ee|Ee))<<eol;
  thePrintStream<<"     P1 : C0 error = "<<norminfty(Ee)<<eol;
  saveToFile("Uex",Ueex,_format=_vtu);
  saveToFile("U",Ue,_format=_vtu);
  BilinearForm aei=intg(omegaDe,grad(ude)|grad(vde),_isogeo,_quad=GaussLegendre,_order=5);
  TermMatrix Kei(aei,ece,_name="Kei");
  TermVector Uei=directSolve(Kei,Fe);
  Ee=Uei-Ueex;
  TermMatrix Ke0(ae,_name="Ke0"), Kei0(aei,_name="Kei0");
  TermMatrix DK=Ke0-Kei0;
  verboseLevel(10);
  thePrintStream<<"checking rigidity matrix"<<eol;
  thePrintStream<<"     norm2(Ke0-Kei0) = "<<norm2(DK)<<eol;
  thePrintStream<<" norminfty(Ke0-Kei0) = "<<norminfty(DK)<<eol;
  omegaDe.meshDomain()->exportIsoNodes("isoD.dat",10);
  thePrintStream<<" iso-P1 : L2 error = "<<real(std::sqrt(MDEi*Ee|Ee))<<"  using iso-P1 mass matrix"<<eol;
  thePrintStream<<" iso-P1 : L2 error = "<<real(std::sqrt(MDE*Ee|Ee))<<"  using P1 mass matrix"<<eol;
  thePrintStream<<" iso-P1 : C0 error = "<<norminfty(Ee)<<eol;
  saveToFile("Ui",Uei,_format=_vtu);

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  trace_p->pop();
}

} // end of namespace dev_other
