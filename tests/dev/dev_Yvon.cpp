/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_Yvon.cpp
\author E. Lunéville
\since 1 March 2014
\date 1 March 2014
*/
#include "xlife++-libs.h"
#include "testUtils.hpp"


using namespace xlifepp;


namespace dev_Yvon
{

void dev_Yvon(int argc, char* argv[], bool check)
{
  String rootname = "dev_Yvon";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  String errors;
  Number nbErrors = 0;
  Real eps=0.0001;
  verboseLevel(11);
  numberOfThreads(1);

  Real k=pi_, k2=k*k;
  //Create parameterList
  Parameters pars;
  pars << Parameter(k,"k") ;
  Kernel H=Helmholtz3dKernel(pars);
  //Mesh and domains definition
  Real hsize=2*pi_/(k*10);
  const Real R1=1, R2=1.4;
  Number npR1=Number(2*pi_/(4*hsize)), npR2=Number(2*pi_*R2/(4*hsize));
  Ball b1(_center=Point(0.,0.,0.), _radius=R1, _nnodes=npR1/4, _side_names="Gamma");
  Ball b2(_center=Point(0.,0.,0.), _radius=R2, _nnodes=npR2/4, _domain_name="Omega", _side_names="Sigma");
  Mesh msh(b2-b1,_shape=_tetrahedron,_order=1,_generator=_gmsh);
  Domain Omega=msh.domain("Omega");
  Domain Gamma=msh.domain("Gamma");
  Domain Sigma=msh.domain("Sigma");
  //NormalOrientation
  Sigma.setNormalOrientation(_outwardsDomain,Omega); //outwards normals
  Gamma.setNormalOrientation(_outwardsDomain,Omega);
  //create P1 Lagrange interpolation
  Space V(_domain=Omega, _interpolation=P1, _name="V");
  Unknown u(V, _name="u" );  TestFunction v(u,_name="v");
  //Extension
  Extension Exty(Gamma, Omega, _y);
  theCout<<Exty<<eol;
  // create bilinear form
  Complex lambda=-i_*k;
  BilinearForm cuv0 = intg(Sigma,Omega,u*((grad_x(Exty(H)))|_nx)*v);
  BilinearForm cuv1 = intg(Sigma,Omega,u*Exty(H)*v);
  BilinearForm cuv2 = cuv0 +cuv1;
  TermMatrix C0(cuv0);
  TermMatrix C1(cuv1);
  TermMatrix C2(cuv2);
  BilinearForm cuv3 = intg(Sigma,Omega,grad(u)|grad_y(Exty(H))*v);
  TermMatrix C3(cuv3);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();

}

}
