/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
  \file dev_colin.cpp
  \since 14 nov 2013
  \date 14 nov 2013

*/
#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

Complex uinc(const Point& P, Parameters& pars = defaultParameters)
{
  Real k = pars("k");
  return exp(i_*k*P(1));
}

Complex dnuinc(const Point& P, Parameters& pars = defaultParameters)
{
  Real k = pars("k"), x=P(1);
  const RealVector& n = pars.getVector(_n);
  return -i_*k*exp(i_*k*x)*n(1);
  //return i_*k*exp(i_*k*x)*P(1)/norm2(P);
}

ComplexVector Ei(const Point& P, Parameters& pars)
{
  Vector<Real> incPol(3,0.); incPol(1)=1.;
  Point incDir(0.,0.,1.) ;
  Real k = pars("k");
  return  -incPol*exp(i_*k * dot(P,incDir));
}

Real k2(const Point &P, Parameters &pars = defaultParameters)
{
  Real k = pars("k"), x=P(1);
  return 4*k*k;
}

namespace dev_colin
{
void dev_colin(int argc, char* argv[], bool check)
{
  String rootname = "dev_colin";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(10);
  //numberOfThreads(1);
  String errors;
  Number nbErrors = 0;


//==========================================================================================================
//                                      //Helmholtz3D - BEM EFIE
//==========================================================================================================
//  Mesh MS(Sphere(_center=Point(0.,0.,0.), _radius=1., _hsteps=0.2), _shape=_triangle, _order=1, _generator=_gmsh);
//  Domain Gamma = MS.domain(0);
//  Space V(Gamma, P1,"V");
//  Unknown phi(V,"phi"); TestFunction psi(phi,"psi");
//  Real k=5; Parameters pars(k,"k");
//  Function F(f,pars);
//  Kernel G=Helmholtz3dKernel(k);

//  IntegrationMethods ims(_SauterSchwabIM, 5, 0.,_defaultRule, 4, 2.,_defaultRule, 3);
//  IntegrationMethods imsh(LenoirSalles3dIR(),_singularPart,theRealMax,
//                          QuadratureIM(GaussLegendre,4),_regularPart,theRealMax);
//  BilinearForm a = intg(Gamma, Gamma, phi*G*psi, _method=ims);
//  LinearForm   b = intg(Gamma, F*psi);
//  TermMatrix A(a,_name="A");
//  TermVector B(b,_name="B");
//  TermVector X=gmresSolve(A,B);
//
//  TermVector U = integralRepresentation(phi, Gamma, intg(Gamma,G*phi,_method=imsh), X, "SL0");
//  saveToFile("U",U, _format=_vtu);
//  TermVector Ut=U-TermVector(phi,Gamma,F);
//  saveToFile("Ut",Ut, _format=_vtu);
//
//  Real c=0;
//  Rectangle Rx(_v1=Point(c,-3.,-3.), _v2=Point(c,3.,-3.),_v4=Point(c,-3.,3.), _hsteps=0.1);
//  Mesh Px(Rx,_shape=_triangle, _order=1, _generator=gmsh);
//  Domain dompx=Px.domain(0);
//  Space Vx(dompx,P1,"Vx"); Unknown ux(Vx,"ux");
//  TermVector Upx = integralRepresentation(ux, dompx, intg(Gamma,G*phi,_method=imsh), X, "SLpx");
//  saveToFile("Upx",Upx-TermVector(ux,dompx,F), _format=_vtu);
//  Rectangle Ry(_v1=Point(-3.,c,-3.), _v2=Point(3.,c,-3.),_v4=Point(-3.,c,3.), _hsteps=0.1);
//  Mesh Py(Ry,_shape=_triangle, _order=1, _generator=gmsh);
//  Domain dompy=Py.domain(0);
//  Space Vy(dompy,P1,"Vy"); Unknown uy(Vy,"uy");
//  TermVector Upy = integralRepresentation(uy, dompy, intg(Gamma,G*phi,_method=imsh), X, "SLpy");
//  saveToFile("Upy",Upy-TermVector(uy,dompy,F), _format=_vtu);
//  Rectangle Rz(_v1=Point(-3.,-3.,c), _v2=Point(3.,-3.,c),_v4=Point(-3.,3.,c), _hsteps=0.1);
//  Mesh Pz(Rz,_shape=_triangle, _order=1, _generator=gmsh);
//  Domain dompz=Pz.domain(0);
//  Space Vz(dompz,P1,"Vz"); Unknown uz(Vz,"uz");
//  TermVector Upz = integralRepresentation(uz, dompz, intg(Gamma,G*phi,_method=imsh), X, "SLpz");
//  saveToFile("Upz",Upz-TermVector(uz,dompz,F), _format=_vtu);

//==========================================================================================================
//                                      //Maxwell3D - BEM EFIE
//==========================================================================================================
//  IntegrationMethods imshM(_defaultRule,10,1.,_defaultRule, 5);
//  Function fi(Ei, pars);                   // define right hand side function
//  Space RT(Gamma, RT_1, "RT");
//  Unknown pM(RT,"pM"); TestFunction qM(pM,"V");
//  BilinearForm aM=((1./k)*intg(Gamma,Gamma, div(pM)*G*div(qM),_method=ims)-k*intg(Gamma,Gamma, (pM*G)|qM,_method=ims));
//  TermMatrix AM(aM, _name="aM");
//  TermVector BM(intg(Gamma,fi|qM), _name="BM");
//  TermVector XM = directSolve(AM,BM);
//  Unknown uxM(Vx,"uxM",3);
//  TermVector EMpx=-(1./k)*integralRepresentation(uxM, dompx, intg(Gamma,grad_x(G)*div(pM),_method=imshM), XM)
//                  -k*integralRepresentation(uxM, dompx, intg(Gamma,G*pM,_method=imshM), XM);
//  saveToFile("EMpx", EMpx, _format=_vtu);
//  Unknown uyM(Vy,"uyM",3);
//  TermVector EMpy=-(1./k)*integralRepresentation(uyM, dompy, intg(Gamma,grad_x(G)*div(pM),_method=imshM), XM)
//                  -k*integralRepresentation(uyM, dompy, intg(Gamma,G*pM,_method=imshM), XM);
//  saveToFile("EMpy", EMpy, _format=_vtu);
//  Unknown uzM(Vz,"uzM",3);
//  TermVector EMpz=-(1./k)*integralRepresentation(uzM, dompz, intg(Gamma,grad_x(G)*div(pM),_method=imshM), XM)
//                  -k*integralRepresentation(uzM, dompz, intg(Gamma,G*pM,_method=imshM), XM);
//  saveToFile("EMpz", EMpz, _format=_vtu);

//==========================================================================================================
//                                      FEM-IR 3D
//==========================================================================================================
//   Real R = 1.;
//  Sphere S1(_center=Point(0.,0.,0.), _radius=1., _hsteps=0.2,_side_names="Gamma");
//  Sphere S2(_center=Point(0.,0.,0.), _radius=1.2, _hsteps=0.24,_domain_name="Omega",_side_names="Sigma");
//  Mesh mC(S2-S1, _shape=_tetrahedron, _order=1, _generator=_gmsh);
//    //mC.saveToFile("crown",_vtk,true);
//  Domain Omega = mC.domain("Omega");
//  Domain Gamma =mC.domain("Gamma"), Sigma =mC.domain("Sigma");
//  Gamma.setNormalOrientation(outwardsDomain ,Omega);
//  Sigma.setNormalOrientation(outwardsDomain ,Omega);
//
//  Real k=5; Complex eta=-i_*k;
//  Parameters pars(k,"k");
//  Function dnF(dnf,pars);
//  dnF.associateVector(_n);
//  Kernel G=Helmholtz3dKernel(k);
//  Space V(Omega, P2, "V");
//  Unknown  u(V,"u"); TestFunction v(u,"v");
//  BilinearForm a = intg(Omega,grad(u)|grad(v))-(k*k)*intg(Omega,u*v)+eta*intg(Sigma, u*v)
//                 + intg(Sigma, Gamma, u*ndotgrad_x(ndotgrad_y(G))*v)
//                 + eta*intg(Sigma, Gamma, u* ndotgrad_y(G)*v);
//  BilinearForm af = intg(Gamma, u*v)
//                  + intg(Sigma, Gamma, u*ndotgrad_x(G)*v)
//                  + eta*intg(Sigma, Gamma, u*G*v);
//  TermMatrix A(a, _name="A"), Af(af);
//  TermVector D(u,Gamma,dnF);
//  TermVector B = Af*D;
//  TermVector X=directSolve(A,B);
//  saveToFile("u_fem_ir",X, _format=_vtu);
//
//  Function ui(f,pars);
//  Real c=0;
////  IntegrationMethods imr(LenoirSalles3dIR(),_singularPart,theRealMax,
////                          QuadratureIM(GaussLegendre,4),_regularPart,theRealMax);
//  IntegrationMethods imr(_defaultRule,10,1.,_defaultRule, 5);
//  Rectangle Ry(_v1=Point(-3.,c,-3.), _v2=Point(3.,c,-3.),_v4=Point(-3.,c,3.), _hsteps=0.1);
//  Mesh Py(Ry,_shape=_triangle, _order=1, _generator=gmsh);
//  Rectangle Rz(_v1=Point(-3.,-3.,c), _v2=Point(3.,-3.,c),_v4=Point(-3.,3.,c), _hsteps=0.1);
//  Mesh Pyz(Rz,_shape=_triangle, _order=1, _generator=gmsh);
//  Pyz.merge(Py);
//  Domain dompyz=Pyz.domain("main domain");
//  Space Vyz(dompyz,P1,"Vyz"); Unknown uyz(Vyz,"uyz");
//  TermVector Upyz = integralRepresentation(uyz, dompyz, intg(Gamma,G*D,_method=imr))
//                  - integralRepresentation(uyz, dompyz, intg(Gamma,ndotgrad_y(G)*X,_method=imr));
//  saveToFile("Upyz",Upyz+TermVector(uyz,dompyz,ui), _format=_vtu);

//==========================================================================================================
//                                      FEM-BEM 2D
//==========================================================================================================
  Disk D1(_center=Point(0.,0.), _radius=1., _hsteps=0.1,_domain_name="Omega",_side_names="Gamma");
  SquareGeo C(_origin=Point(-4,-4),_length=8,_hsteps=0.1, _domain_name="S");
  Mesh mC(C+D1,_shape=_triangle, _order=1, _generator=gmsh);
  Domain Omega = mC.domain("Omega");
  Domain S = mC.domain("S");
  Domain Gamma =mC.domain("Gamma");
  mC.saveToFile("FEM_BEM2D",_vtk, true);

  Space V(_domain=Omega, _interpolation=P1, _name="V"); Unknown  u(V, _name="u"); TestFunction v(u, _name="v");
  Space L(_domain=Gamma, _interpolation=P0, _name="L"); Unknown  l(L, _name="l"); TestFunction t(l, _name="t");

  Real k=5;
  Kernel G=Helmholtz2dKernel(k);
  Parameters pars(k,"k");
  Function fk2(k2,pars);
  Function finc(uinc,pars);
  IntegrationMethods ims(Duffy,10,0.,defaultQuadrature,8,1.,defaultQuadrature,6,2.,defaultQuadrature,4);
  BilinearForm a = intg(Omega, grad(u)|grad(v)) - intg(Omega, fk2*u*v) - intg(Gamma, l*v)
                   + 0.5*intg(Gamma, u*t) - intg(Gamma, Gamma, u*ndotgrad_y(G)*t, _method=ims)
                   + intg(Gamma, Gamma, l*G*t, ims);
  LinearForm b=intg(Gamma, finc *t) ;
  TermMatrix A(a, _name="A");
  TermVector B(b, _name="B");
  TermVector X=directSolve(A,B);
  saveToFile("Uint",X(u), _format=_vtu);

  IntegrationMethods imr(_method=LenoirSalles2dIR(), _functionPart=_singularPart, _bound=theRealMax,
                         _quad=QuadratureIM(defaultQuadrature,4),_functionPart=_regularPart, _bound=theRealMax);
  Space VS(_domain=S, _interpolation=P1, _name="VS"); Unknown uS(VS, _name="uS");
  TermVector US = integralRepresentation(uS,S,intg(Gamma,ndotgrad_y(G)*X(u),_method=imr))
                - integralRepresentation(uS,S,intg(Gamma,G*X(l),_method=imr));
  saveToFile("US",US+TermVector(uS,S,finc), _format=_vtu);

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
