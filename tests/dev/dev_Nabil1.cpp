/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_nabil1.cpp
\author E. Lunéville
\since 1 March 2014
\date 1 March 2014
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;

namespace dev_Nabil1
{

// Modes de Neumann triés par ordre croissant de valeurs propres :

xlifepp::Matrix<Real> vpcroiss_Neumann(Real Nb)
{
    xlifepp::Matrix<Real> maliste(Nb,2);

    maliste(1,1) = 1.; maliste(1,2) = 0.;
    maliste(2,1) = 0.; maliste(2,2) = 1.;
    maliste(3,1) = 1.; maliste(3,2) = 1.;
    maliste(4,1) = 2.; maliste(4,2) = 0.;
    maliste(5,1) = 2.; maliste(5,2) = 1.;
    maliste(6,1) = 0.; maliste(6,2) = 2.;
    maliste(7,1) = 1.; maliste(7,2) = 2.;
    maliste(8,1) = 3.; maliste(8,2) = 0.;
    maliste(9,1) = 2.; maliste(9,2) = 2.;
    maliste(10,1) = 3.; maliste(10,2) = 1.;
    maliste(11,1) = 0.; maliste(11,2) = 3.;
    maliste(12,1) = 1.; maliste(12,2) = 3.;
    maliste(13,1) = 3.; maliste(13,2) = 2.;
    maliste(14,1) = 4.; maliste(14,2) = 0.;
    maliste(15,1) = 4.; maliste(15,2) = 1.;
    maliste(16,1) = 2.; maliste(16,2) = 3.;
    maliste(17,1) = 4.; maliste(17,2) = 2.;
    maliste(18,1) = 3.; maliste(18,2) = 3.;
    maliste(19,1) = 0.; maliste(19,2) = 4.;
    maliste(20,1) = 5.; maliste(20,2) = 0.;
    maliste(21,1) = 1.; maliste(21,2) = 4.;
    maliste(22,1) = 5.; maliste(22,2) = 1.;
    maliste(23,1) = 2.; maliste(23,2) = 4.;
    maliste(24,1) = 4.; maliste(24,2) = 3.;
    maliste(25,1) = 5.; maliste(25,2) = 2.;
    maliste(26,1) = 3.; maliste(26,2) = 4.;
    maliste(27,1) = 0.; maliste(27,2) = 5.;
    maliste(28,1) = 5.; maliste(28,2) = 3.;
    maliste(29,1) = 1.; maliste(29,2) = 5.;
    maliste(30,1) = 4.; maliste(30,2) = 4.;
    maliste(31,1) = 2.; maliste(31,2) = 5.;
    maliste(32,1) = 3.; maliste(32,2) = 5.;
    maliste(33,1) = 5.; maliste(33,2) = 4.;
    maliste(34,1) = 4.; maliste(34,2) = 5.;
    maliste(35,1) = 5.; maliste(35,2) = 5.;

    return maliste;
}

xlifepp::Matrix<Real> vpcroiss_Dirichlet(Real Nb)
{
    xlifepp::Matrix<Real> maliste(Nb,2);

    maliste(1,1) = 1.; maliste(1,2) = 1.;
    maliste(2,1) = 2.; maliste(2,2) = 1.;
    maliste(3,1) = 1.; maliste(3,2) = 2.;
    maliste(4,1) = 2.; maliste(4,2) = 2.;
    maliste(5,1) = 3.; maliste(5,2) = 1.;
    maliste(6,1) = 1.; maliste(6,2) = 3.;
    maliste(7,1) = 3.; maliste(7,2) = 2.;
    maliste(8,1) = 4.; maliste(8,2) = 1.;
    maliste(9,1) = 2.; maliste(9,2) = 3.;
    maliste(10,1) = 4.; maliste(10,2) = 2.;
    maliste(11,1) = 3.; maliste(11,2) = 3.;
    maliste(12,1) = 1.; maliste(12,2) = 4.;
    maliste(13,1) = 5.; maliste(13,2) = 1.;
    maliste(14,1) = 2.; maliste(14,2) = 4.;
    maliste(15,1) = 4.; maliste(15,2) = 3.;
    maliste(16,1) = 5.; maliste(16,2) = 2.;
    maliste(17,1) = 3.; maliste(17,2) = 4.;
    maliste(18,1) = 5.; maliste(18,2) = 3.;
    maliste(19,1) = 1.; maliste(19,2) = 5.;
    maliste(20,1) = 4.; maliste(20,2) = 4.;
    maliste(21,1) = 2.; maliste(21,2) = 5.;
    maliste(22,1) = 3.; maliste(22,2) = 5.;
    maliste(23,1) = 5.; maliste(23,2) = 4.;
    maliste(24,1) = 4.; maliste(24,2) = 5.;
    maliste(25,1) = 5.; maliste(25,2) = 5.;

    return maliste;
}

// Modes de Neumann et nu vect rot

Vector<Complex> mode_Neumann(const Point&P, Real n, Real m, Parameters& pa )
{
    Real x = P(1), y = P(2), z = P(3);
    Real a = pa("a"), b = pa("b");
    Complex w = pa("w");
    Vector<Complex> u(3);

    if ( (n == 0) && (m == 0)) return u;
    else {
    Real cst;
    Real lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    if ( (n == 0) || (m == 0) ) cst = sqrt(2.)/sqrt(lambda * a * b);
    else cst = 2./sqrt(lambda * a * b);

    Complex beta = sqrt(Complex(w*w-lambda));

    u(1) = - m*pi_/b * cst * exp(i_*beta*z) * cos(n*pi_*x/a) * sin(m*pi_*y/b) ;
    u(2) =   n*pi_/a * cst * exp(i_*beta*z) * sin(n*pi_*x/a) * cos(m*pi_*y/b) ;

    return u;}
}

Vector<Complex> nu_rot_mode_Neumann(const Point&P, Real n, Real m, Parameters& pa )                  // Calcul de nu ^ rot u sur la frontière gauche
{                                                                                                                       // nu = (0, 0, -1) et u est mode de Neumann
    Real x = P(1), y = P(2), z = P(3);
    Real a = pa("a"), b = pa("b");
    Complex w = pa("w");
    Vector<Complex> u(3);

    if ( (n == 0) && (m == 0)) return u;
    else {
    Real cst;
    Real lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    if ( (n == 0) || (m == 0) ) cst = sqrt(2.)/sqrt(lambda * a * b);
    else cst = 2./sqrt(lambda * a * b);

    Complex beta = sqrt(Complex(w*w-lambda));

    u(1) = - m*pi_/b * cst * exp(i_*beta*z) * cos(n*pi_*x/a) * sin(m*pi_*y/b) ;
    u(2) =   n*pi_/a * cst * exp(i_*beta*z) * sin(n*pi_*x/a) * cos(m*pi_*y/b) ;

    return i_ * beta * u;}
}

// Modes de Dirichlet trace trangentielle à gauche

Vector<Complex> mode_Dirichlet(const Point&P, Real n, Real m, Parameters& pa )
{
    Real x = P(1), y = P(2), z = P(3);
    Real a = pa("a"), b = pa("b");
    Complex w = pa("w");
    Vector<Complex> u(3);

    Real lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    Real cst = 2./sqrt(lambda * a * b);
    Complex beta = sqrt(Complex(w*w-lambda));

    u(1) = n*pi_/a * cst * exp(i_*beta*z) * cos(n*pi_*x/a) * sin(m*pi_*y/b) ;
    u(2) = m*pi_/b * cst * exp(i_*beta*z) * sin(n*pi_*x/a) * cos(m*pi_*y/b) ;
    u(3) = - i_* lambda / beta * cst * exp(i_*beta*z) * sin(n*pi_*x/a) * sin(m*pi_*y/b) ;

    return u;
}

// Construction de la solution exacte testée et de nu vect rot

Vector<Complex> u_exa(const Point&P, Parameters& pa )
{
    Real NbModes = 1;
    Real n,m;
    Vector<Complex> u_Neu(3);

    xlifepp::Matrix<Real> maliste;
    maliste = vpcroiss_Neumann(35);

    for (Real N = 1; N <= NbModes; N++){
    n = maliste(N,1);
    m = maliste(N,2);
    u_Neu = u_Neu + (1./N) * mode_Neumann(P, n, m, pa); }

    return u_Neu;
}

Vector<Complex> nug_rot_u_exa(const Point&P, Parameters& pa )
{
    Real NbModes = 1;
    Real n,m;
    Vector<Complex> nug_rot_u_Neu(3);

    xlifepp::Matrix<Real> maliste;
    maliste = vpcroiss_Neumann(35);

    for (Real N = 1; N <= NbModes; N++){
    n = maliste(N,1);
    m = maliste(N,2);
    nug_rot_u_Neu = nug_rot_u_Neu + (1./N) * nu_rot_mode_Neumann(P, n, m, pa);}

    return nug_rot_u_Neu;
}

Vector<Complex> modepropagatif( const Point& P, Parameters& pa  )
{
    Real n = 1;
    Real m = 0;

    Real  x = P(1), y = P(2);
    Real a = pa("a"), b = pa("b");
    Complex w = pa("w");
    Vector<Complex> ynm(3);

    Real cst;
    Real lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    if ( (n == 0) || (m == 0) ) cst = sqrt(2.)/sqrt(lambda * a * b);
    else cst = 2./sqrt(lambda * a * b);

    Complex beta = sqrt(Complex(w*w-lambda));

    ynm(1) = - m*pi_/b * cst * cos(n*pi_*x/a) * sin(m*pi_*y/b) ;
    ynm(2) =   n*pi_/a * cst * sin(n*pi_*x/a) * cos(m*pi_*y/b) ;


    return ynm;
}

// Gradients des fonctions propres du problème de Laplace pour la création du noyau matriciel

xlifepp::Matrix<Complex> gradT_Neumann ( const Point& P, Real N, Parameters& pa  )
{
    xlifepp::Matrix<Real> maliste;
    maliste = vpcroiss_Neumann(35);
    Real n, m;
    n = maliste(N,1);
    m = maliste(N,2);

    Real  x = P(1), y = P(2);
    Real a = pa("a"), b = pa("b");
    Complex w = pa("w");
    xlifepp::Matrix<Complex> ynm(3,1);

    Real cst;
    Real lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    if ( (n == 0) || (m == 0) ) cst = sqrt(2.)/sqrt(lambda * a * b);
    else cst = 2./sqrt(lambda * a * b);

    Complex beta = sqrt(Complex(w*w-lambda));

    ynm(1,1) = - m*pi_/b * cst * cos(n*pi_*x/a) * sin(m*pi_*y/b) ;
    ynm(2,1) =   n*pi_/a * cst * sin(n*pi_*x/a) * cos(m*pi_*y/b) ;


    return ynm;
}

xlifepp::Matrix<Complex> grad_Dirichlet ( const Point& P, Real N, Parameters& pa  )
{
    xlifepp::Matrix<Real> maliste;
    maliste = vpcroiss_Dirichlet(25);
    Real n, m;
    n = maliste(N,1);
    m = maliste(N,2);

    Real  x = P(1), y = P(2);
    Real a = pa("a"), b = pa("b");
    Complex w = pa("w");
    xlifepp::Matrix<Complex> ynm(3,1);

    if (n == 0 || m == 0) return ynm;
    else{

    Real lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    Real cst = 2./sqrt(lambda * a * b);
    Complex beta = sqrt(Complex(w*w-lambda));

    ynm(1,1) = n*pi_/a * cst * cos(n*pi_*x/a) * sin(m*pi_*y/b) ;
    ynm(2,1) = m*pi_/b * cst * sin(n*pi_*x/a) * cos(m*pi_*y/b) ;

    return ynm;}
}

// Création du noyau matricielle (opérateur Electric-to-Magnetic tronqué)

xlifepp::Matrix<Complex> KMatrix_recouv ( const Point& P, const Point& Q, Parameters& pa  )
{
    xlifepp::Matrix<Complex> GTN1(3,1), GTN2(3,1), GD1(3,1), GD2(3,1);
    xlifepp::Matrix<Complex> S(3,3);
    Real a = pa("a"), b = pa("b"), lambda, betaRobin = pa("betaRobin");
    Real NbModes_Neumann = pa("NbModes_Neumann"), NbModes_Dirichlet = pa("NbModes_Dirichlet");
    Real zp = P(3), zq = Q(3), z = abs(zq - zp);
    Complex w = pa("w");
    Complex beta;
    Real i, n, m;

    xlifepp::Matrix<Real> maliste_Neu, maliste_Dir;
    maliste_Neu = vpcroiss_Neumann(35);
    maliste_Dir = vpcroiss_Dirichlet(25);

    for (Real N = 1; N <= NbModes_Neumann; N++){
    n = maliste_Neu(N,1);
    m = maliste_Neu(N,2);
    lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    beta = sqrt(Complex(w*w-lambda));
    GTN1 = gradT_Neumann(Q, N, pa);
    GTN2 = gradT_Neumann(P, N, pa);
    S = S - i_ * (beta - betaRobin) * exp(i_*beta*z) * GTN1 * adj(GTN2);
    }

    for (Real N = 1; N <= NbModes_Dirichlet; N++){
    n = maliste_Dir(N,1);
    m = maliste_Dir(N,2);
    lambda = pi_*pi_ * ( n*n / (a*a) + m*m / (b*b));
    beta = sqrt(Complex(w*w-lambda));
    GD1 = grad_Dirichlet(Q, N, pa);
    GD2 = grad_Dirichlet(P, N, pa);
    S = S - i_ * (w*w / beta - betaRobin) * exp(i_*beta*z) * GD1 * adj(GD2);
    }

    return S;
}

xlifepp::Matrix<Complex> Noyau_nul ( const Point& P, const Point& Q, Parameters& pa )
{
    xlifepp::Matrix<Complex> S(3,3);
    return S;
}

//Partie liée aux PMLs

Vector<Complex> nu_cross_uexa(const Point&P, Parameters& pa )
{
    Real x = P(1), y = P(2), z = P(3);
    Real a = pa("a"), b = pa("b");
    Vector<Real> PC(3);

    Vector<Real> norm = getN();

    Vector<Complex> u_Neu_10 = -1. * u_exa(P, pa);
    return crossProduct(norm, u_Neu_10);
}

xlifepp::Matrix<Complex>  A_alpha(const Point&P, Parameters& pa )
{
    Complex alpha = pa("alpha");
    xlifepp::Matrix<Complex> A(3,3);
    A(1,1) = 1./alpha;
    A(2,2) = 1./alpha;
    A(3,3) = alpha;
    return A;
}

xlifepp::Matrix<Complex>  A_alpha_inv(const Point&P, Parameters& pa )
{
    Complex alpha = pa("alpha");
    xlifepp::Matrix<Complex> A(3,3);
    A(1,1) = alpha;
    A(2,2) = alpha;
    A(3,3) = 1./alpha;
    return A;
}


void dev_Nabil1(int argc, char* argv[], bool check)
{
  String rootname = "dev_Nabil1";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(1);
  numberOfThreads(4);
  String errors;
  Number nbErrors = 0;

 theCout << "\n-!-!-!-!-!-!-!-! Approximation numérique : Electric-to-Robin avec recouvrement !-!-!-!-!-!-!-!- " << eol;
  theCout << "................................. Guide d'ondes rectangulaire ................................." << eol;
  theCout << ".................................. Comparaison avec les PMLs .................................." << eol;

  // Définiton des paramètres du problème :
  Real a = 1, b = 0.8;                        // la section du guide est un rectangle de coté a (selon x) * b (selon y), w est la pulsation de l'onde
  Real w = sqrt(1.55)*pi_;
  Real hz = 1.;                                                   // hz est la longueur du guide (propagation selon z)
  Real dx = 1./2.;                                               // dx est le pas du maillage
  Real hPML = 2.;                                                 // hPML est la longueur de la couche PML

  Complex alpha = exp(- i_*pi_/3);                                // coefficient PML

  Real lambdaR = pi_*pi_ / (a*a);
  Complex betaRobin = sqrt(Complex(w*w-lambdaR));
  theCout << "\nbetaRobin = " << betaRobin << eol;
  theCout << "longueur PML : " << hPML << eol;
  theCout << "amortissement théorique : " << abs(exp(-i_*betaRobin*alpha*hPML)) << eol;

  Real NbModes_Neumann = 2.;
  Real NbModes_Dirichlet = 0.;
  theCout << "Nombre de modes de Neumann : " << NbModes_Neumann << eol;
  theCout << "Nombre de modes de Dirichlet : " << NbModes_Dirichlet << eol;

  Parameters pars;
  pars << Parameter (a, "a") << Parameter (b, "b") << Parameter (w, "w") << Parameter (betaRobin, "betaRobin") << Parameter (alpha, "alpha")
  << Parameter (NbModes_Neumann, "NbModes_Neumann") << Parameter (NbModes_Dirichlet, "NbModes_Dirichlet");

  Real r = b/3;                                                   // r est le rayon de la boule obstacle
  Number nd(2*r/dx);                                              // défini le nombre de noeuds sur la boule pour avoir le même pas de maillage

  // Définitions des fonctions utilisées :
  Function Aalpha (A_alpha, pars);
  Function Aalphainv (A_alpha_inv, pars);
  Function nucrossuex (nu_cross_uexa, pars);
  nucrossuex.require(_n);
  Kernel Kr(KMatrix_recouv, pars);
  Kernel Kzero(Noyau_nul, pars);
  Function uex (u_exa, pars);
  Function nugrotuex (nug_rot_u_exa, pars);

  // Éléments de géométrie invariants :
  Cuboid C_milieu(_xmin = 0., _xmax = a, _ymin = 0., _ymax = b, _zmin = -hz, _zmax = 0., _hsteps = dx, _domain_name = "Omega_milieu",
  _side_names = Strings("Gamma_g","Gamma_d","Gamma_ext_EtR","Gamma_ext_EtR","Gamma_ext_EtR","Gamma_ext_EtR") );
  Ball Ba(_center = Point(a/2-0.1, b/2+0.1, -hz/2), _radius = r, _nnodes = Numbers(nd, nd, nd), _side_names = "Gamma_ball");

  // Boucle sur la taille du recouvrement :
  Vector<Real> TailleCouche(4), ListeErreurs(4);

  theCout << "\n -!-!-!-!- Début de la boucle -!-!-!-!- " << '\n' << eol;

  for (Real NbCouche = 1; NbCouche <= 3; NbCouche++)
  {
    //printAllInMemory(thePrintStream);
    Real hL = NbCouche * dx;                                              // hL est la longueur du recouvrement (ATTENTION, ON DOIT TOUJOURS AVOIR hL < hPML !)
    TailleCouche(NbCouche) = hL;
    theCout << "\nÉpaisseur du recouvrement = " << hL << '\n' << eol;

    // Construction des éléments géométriques et du maillage :
    Cuboid C_gauche_pml(_xmin = 0., _xmax = a, _ymin = 0., _ymax = b, _zmin = -hz-hPML, _zmax = -hz-hL, _hsteps = dx, _domain_name = "Omega_gauche_pml",
    _side_names = Strings("","Gamma_gL","Gamma_ext_pml","Gamma_ext_pml","Gamma_ext_pml","Gamma_ext_pml") );
    Cuboid C_gauche(_xmin = 0., _xmax = a, _ymin = 0., _ymax = b, _zmin = -hz-hL, _zmax = -hz, _hsteps = dx, _domain_name = "Omega_gauche_EtR",
    _side_names = Strings("Gamma_gL","Gamma_g","Gamma_ext_EtR","Gamma_ext_EtR","Gamma_ext_EtR","Gamma_ext_EtR") );
    Cuboid C_droite(_xmin = 0., _xmax = a, _ymin = 0., _ymax = b, _zmin = 0, _zmax = hL, _hsteps = dx, _domain_name = "Omega_droite_EtR",
    _side_names = Strings("Gamma_d","Gamma_dL","Gamma_ext_EtR","Gamma_ext_EtR","Gamma_ext_EtR","Gamma_ext_EtR") );
    Cuboid C_droite_pml(_xmin = 0., _xmax = a, _ymin = 0., _ymax = b, _zmin = hL, _zmax = hPML, _hsteps = dx, _domain_name = "Omega_droite_pml",
    _side_names = Strings("Gamma_dL","","Gamma_ext_pml","Gamma_ext_pml","Gamma_ext_pml","Gamma_ext_pml") );

    Mesh mail3d(C_gauche_pml + C_gauche + (C_milieu-Ba) + C_droite + C_droite_pml, _shape=_tetrahedron, _order=1, _generator=_gmsh);

    Domain Omega_milieu = mail3d.domain("Omega_milieu");
    Domain Omega_gauche_pml = mail3d.domain("Omega_gauche_pml");
    Domain Omega_gauche_EtR = mail3d.domain("Omega_gauche_EtR");
    Domain Omega_droite_EtR = mail3d.domain("Omega_droite_EtR");
    Domain Omega_droite_pml = mail3d.domain("Omega_droite_pml");
    Domain Omega_EtR = merge(Omega_gauche_EtR, Omega_milieu, Omega_droite_EtR, "Omega_EtR");
    Domain Omega = mail3d.domain("Omega");
    Domain Gamma_g = mail3d.domain("Gamma_g");
    Domain Gamma_gL = mail3d.domain("Gamma_gL");
    Domain Gamma_d = mail3d.domain("Gamma_d");
    Domain Gamma_dL = mail3d.domain("Gamma_dL");
    Domain Gamma_e_pml = mail3d.domain("Gamma_ext_pml");
    Domain Gamma_e_EtR = mail3d.domain("Gamma_ext_EtR");
    Domain Gamma_ball = mail3d.domain("Gamma_ball");

    Gamma_gL.setNormalOrientation (_outwardsDomain, Omega_gauche_EtR);
    Gamma_e_EtR.setNormalOrientation (_towardsInfinite);
    Gamma_e_pml.setNormalOrientation (_towardsInfinite);
    Gamma_dL.setNormalOrientation (_outwardsDomain, Omega_droite_EtR);
    Gamma_ball.setNormalOrientation (_outwardsDomain, Omega_milieu);

    Gamma_gL.updateParentOfSideElements();
    Gamma_dL.updateParentOfSideElements();

    elapsedTime("Construction du maillage "); theCout << "\n" << eol;

    // Construction de l'espace éléments finis :
    Space V(_domain = Omega, _interpolation = NE1_2, _name = "Hrot_Ned2", _optimizeNumbering);              // Interpolation de Nédélec d'ordre 1
    Unknown u(V, _name = "u");  TestFunction v(u, _name = "v");

    Space Vs(_domain = Omega_EtR, _interpolation = NE1_2, _name = "Hrot_Ned2_small", _optimizeNumbering);              // Interpolation de Nédélec d'ordre 1
    Unknown us(Vs, _name = "us");  TestFunction vs(us, _name = "vs");

    // Résolution du probleme PML :

    BilinearForm a_pml = intg(Omega_milieu, rot(u) | rot(v) ) - w * w * intg(Omega_milieu, u | v )
    + intg(Omega_droite_EtR, Aalphainv * rot(u) | rot(v) ) - w * w * intg(Omega_droite_EtR, Aalpha * u | v )
    + intg(Omega_droite_pml, Aalphainv * rot(u) | rot(v) ) - w * w * intg(Omega_droite_pml, Aalpha * u | v )
    + intg(Omega_gauche_EtR, Aalphainv * rot(u) | rot(v) ) - w * w * intg(Omega_gauche_EtR, Aalpha * u | v )
    + intg(Omega_gauche_pml, Aalphainv * rot(u) | rot(v) ) - w * w * intg(Omega_gauche_pml, Aalpha * u | v );

    EssentialConditions ecs_pml = ( ncross(u) | Gamma_e_EtR = 0 ) & ( ncross(u) | Gamma_e_pml = 0 ) & ( ncross(u) | Gamma_ball = nucrossuex );          // Condition conducteur parfait sur la frontière réelle

    TermMatrix A_pml(a_pml, ecs_pml, _name = "apml(u,v)");
    TermVector B_pml(u, Omega, 0., _name = "bpml(v)");
    elapsedTime("Creation matrice A_pml et vecteur B_pml "); theCout << "\n" << eol;

    TermVector U_pml = directSolve(A_pml, B_pml);                             // Résolution du système
    theCout << "\n" << eol; elapsedTime("Resolution directe du systeme PML"); theCout << "\n" << eol;

    // Résolution du probleme EtR avec recouvrement :
    BilinearForm ad = intg(Gamma_d, Gamma_d, ( us | Kzero * vs ) ) + intg(Gamma_dL, Gamma_dL, ( us | Kzero * vs ) )
    + intg(Gamma_d, Gamma_dL, ( us | Kzero * vs ) ) + intg(Gamma_dL, Gamma_d, ( us | Kr * vs ) )
    + intg(Gamma_g, Gamma_g, ( us | Kzero * vs ) ) + intg(Gamma_gL, Gamma_gL, ( us | Kzero * vs ) )
    + intg(Gamma_g, Gamma_gL, ( us | Kzero * vs ) ) + intg(Gamma_gL, Gamma_g, ( us | Kr * vs ) );             // Correspond à l'action de l'opéraeur EtR

    BilinearForm a_etr = intg(Omega_milieu, rot(us) | rot(vs) ) - w * w * intg(Omega_milieu, us | vs )
    + intg(Omega_gauche_EtR, rot(us) | rot(vs) ) - w * w * intg(Omega_gauche_EtR, us | vs )
    + intg(Omega_droite_EtR, rot(us) | rot(vs) ) - w * w * intg(Omega_droite_EtR, us | vs )
    - i_ * betaRobin * intg(Gamma_gL, ncross(us) | ncross(vs)) - i_ * betaRobin * intg(Gamma_dL, ncross(us) | ncross(vs)) + ad;

    LinearForm b_int = - 2 * intg(Gamma_gL, vs | nugrotuex);

    EssentialConditions ecs_EtR = ( ncross(us) | Gamma_e_EtR = 0 ) & ( ncross(us) | Gamma_ball = 0 );                                                  // Condition conducteur parfait sur la frontière réelle
    TermVector B_EtR(b_int, _name = "bv(v)");                                                                           // Ici la densité de courant J est supposée nulle
    TermMatrix A_EtR(a_etr, ecs_EtR, _name = "a(u,v)");
    elapsedTime("Creation matrice A_EtR et vecteur B_EtR "); theCout << "\n" << eol;

    BilinearForm a_rob = intg(Omega_milieu, rot(us) | rot(vs) ) - w*w * intg(Omega_milieu, us | vs )
    + intg(Omega_gauche_EtR, rot(us) | rot(vs) ) - w*w * intg(Omega_gauche_EtR, us | vs )
    + intg(Omega_droite_EtR, rot(us) | rot(vs) ) - w*w * intg(Omega_droite_EtR, us | vs )
    - i_ * betaRobin * intg(Gamma_dL, ncross(us) | ncross(vs) )
    - i_ * betaRobin * intg(Gamma_gL, ncross(us) | ncross(vs) );
    TermMatrix Arob(a_rob, ecs_EtR, _name = "arob(u,v)");
    PreconditionerTerm precondRobin(Arob, _ldltPrec);
    elapsedTime("Creation préconditionneur "); theCout << "\n" << eol;
    TermVector U_EtR = iterativeSolve(A_EtR, B_EtR, precondRobin, _solver=_bicg, _verbose = 10);                             // Résolution du système
    theCout << "\n" << eol; elapsedTime("Resolution itérative du systeme EtR avec recouvrement"); theCout << "\n" << eol;;

    // Interpolation sur l'espace de Lagrange pour calcul de l'erreur et affichage de la solution
    Space W(_domain = Omega, _interpolation = P2, _name = "W");                                             // Espace éléments finis de Lagrange d'ordre 1
    Unknown up1(W, _name = "up1", _dim = 3);
    TestFunction vp1(up1, _name = "vp1");
    theCout << "\n" << eol; elapsedTime("Création de l'espace de Lagrange "); theCout << "\n" << eol;

    TermVector Uexa(up1, Omega_EtR, uex, _name = "U_exa");                                                                       // On ne peut définir la solution exacte point par point que sur Lagrange
    elapsedTime("Creation solution exacte "); theCout << "\n" << eol;

    BilinearForm m = intg(Omega_milieu, us | vs );
    TermMatrix M(m, _name = "M");                                                                                   // Permets de calculer le produit scalaire L^2
    TermVector U_pml_restreint =  U_pml|Omega_EtR;
    TermVector U_PML = projection(U_pml_restreint, Vs, 1);
    TermVector Uref = projection(Uexa, Vs, 1) + U_PML;                                                               // On projette la solution exacte dans Nédélec
    TermVector U_err = Uref - U_EtR;
    Real er = sqrt( abs( (M*U_err|U_err) ) );                                                                     // On calcul la norme 2 de l'erreur
    Real norm = sqrt( abs( (M*Uref|Uref) ) );                                                               // Norme 2 de la solution exacte
    theCout << "L2 error (projection) = " << er << eol;
    theCout << "erreur relative en norme L2 (projection) : " << 100*er/norm << "% \n" << eol;
    elapsedTime("Calcul de l'erreur "); theCout << "\n" << eol;

    ListeErreurs(NbCouche) = er/norm;
  }

  theCout << "\nListe des épaisseur de recouvrement considéré : " << TailleCouche << eol;
  theCout << "\nListe des erreurs relatives : " << ListeErreurs << eol;

  totalElapsedTime("Temps total ");

  theCout << "\n -!-!-!-!-!-!-!-!-!- Program finished -!-!-!-!-!-!-!-!-!- \n" << eol;

  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
