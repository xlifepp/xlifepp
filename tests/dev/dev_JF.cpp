/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_JF.cpp
\author E. Lunéville
\since 1 March 2014
\date 1 March 2014
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"
#ifdef XLIFEPP_WITH_EIGEN
#include <Eigen>
#endif
using namespace xlifepp;
using namespace std;

namespace dev_JF
{
Complex uinc(const Point& M, Parameters& pa = defaultParameters)
{
  Real k=real(pa("k"));
  Point d(1., 0.);
  return exp(i_*(k*dot(M, d)));
}

//================================================================================================================
void dev_JF(int argc, char* argv[], bool check)
//================================================================================================================
{
  String rootname = "dev_JF";
  trace_p->push(rootname);
  verboseLevel(1);
  // numberOfThreads(1); // trackingObjects = false;
  String errors;
  Number nbErrors = 0;
  Real k=5.;
  Real eps=1E-4;
  Parameters pars;
  pars << Parameter(k, "k");
  Kernel G=Helmholtz2dKernel(pars);
  IntegrationMethods ims(_method=Duffy, _order=10, _bound=0., _method=_defaultRule, _order=8, _bound=1.,
                         _method=_defaultRule,_order= 6, _bound=2., _method=_defaultRule, _order=4);

  for (Number n=50; n<501; n+=50)
  {
    //test on a disk
    Mesh meshd(Disk(_center=Point(0.,0.),_radius=1.,_nnodes=n,_domain_name="Gamma"), _shape=_segment, _order=1, _generator=gmsh);
    Function finc(uinc, pars);
    Domain gamma=meshd.domain("Gamma");
    Space V(_domain=gamma, _interpolation=_P0, _name="V", _notOptimizeNumbering);
    Unknown u(V, _name="u"); TestFunction v(u, _name="v");
    Number maxrc = std::max(Number(10),V.dimSpace()/100); // minimum size of block matrix -> not split if nbrow < sb and nbcol < sb
    BilinearForm bf=intg(gamma,gamma,u*G*v,_method=ims);
    LinearForm lf=-intg(gamma, finc*v);
    TermMatrix A(bf, _name="A");
    TermVector b(lf, _name="b");
    thePrintStream<<"Full BEM : A.nnz = "<<A.getLargeMatrix<Complex>().nbNonZero()<<eol;
    TermVector U=directSolve(A,b);
    saveToFile("U", U, _format=vtu);

    HMatrixIM him(_clustering_method=cardinalityBisection, _min_block_size=maxrc, _approximation_method=_acaPlus, _threshold=eps, _method=ims); // HMatrix parameters
    BilinearForm bh=intg(gamma,gamma,u*G*v,_method=him);
    TermMatrix H(bh, _name="H");
    HMatrix<Complex,FeDof>& Hm=H.getHMatrix<Complex>();
    thePrintStream<<"ACA plus (eps="<<eps<<") : "<<" average rank = "<<Hm.averageRank()<<" nnz = "<<Hm.nbNonZero()<<eol;
    TermVector Uh=gmresSolve(H,b);

    TermMatrix M(intg(gamma,u*v,_quad=_defaultRule,_order=4));
    TermVector E=U-Uh;
    thePrintStream<<"|U-Uh| = "<<std::sqrt(std::abs(M*E|E))<<eol<<std::flush;
  }

  trace_p->pop();
}

}
