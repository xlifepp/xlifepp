/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_other.cpp
\author E. Lunéville
\since  december 2019
\date  january 2020
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace dev_other
{

Real epsilon( const Point& P, Parameters& pa = defaultParameters )
{
    Real x = P(1), y = P(2);
    Real eps = 1. + 0. * x + 0. * y;

    return eps;
}

Real mu( const Point& P, Parameters& pa = defaultParameters )
{
    Real x = P(1), y = P(2);
    Real mu = 1. + 0. * x + 0. * y;

    return mu;
}

Real epsilon_inv( const Point& P, Parameters& pa = defaultParameters )
{
    return 1/epsilon(P, pa);
}

Real mu_inv( const Point& P, Parameters& pa = defaultParameters )
{
    return 1/mu(P, pa);
}



void dev_other(int argc, char* argv[], bool check)
{
  String rootname = "dev_other";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  //numberOfThreads(1);
  verboseLevel(22);
  String errors;
  Number nbErrors = 0;
  Trace::traceMemory=false;

  theCout << "\n-!-!-!-!-!-!-!-!-!- Calcul des modes d'un guide d'ondes électromagnétiques -!-!-!-!-!-!-!-!-!- " << eol;
  theCout << "......................................... Cas général ........................................." << eol;

    // ******************** Définiton des paramètres du problème : ********************

    Real a = 1, b = 0.8;                            // Si la section du guide est un rectangle de coté a (selon x) * b (selon y)
    Real R_g = 1;                                   // Si la section du guide est un disque de rayon R_g
    Complex w = sqrt(11)*pi_;                       // w est la pulsation de l'onde
    Real hz = 0.6;                                  // hz est la longueur du guide (propagation selon z)
    Real dx = 1./15.;                               // dx est le pas du maillage
    Parameters pars;
    pars << Parameter (w, "w");

    Real r = a/3;                                   // r est le rayon de la boule obstacle
    Number nd(2*r/dx);                              // défini le nombre de noeuds sur la boule pour avoir le même pas de maillage
    //String section = "rectangle";
    String section = "Disk";
    // ******************** Construction des éléments géométriques et du maillage : ********************
    Geometry Guide;
    if (section == "rectangle"){
        Rectangle S1(_v1 = Point(0, 0, 0), _v2 = Point(a, 0, 0), _v4 = Point(0, 0, hz), _hsteps = dx, _domain_name = "Sigma_ext",
        _side_names = Strings("Gamma_gauche","","Gamma_droite",""));
        Rectangle S2(_v1 = Point(0, b, 0), _v2 = Point(a, b, 0), _v4 = Point(0, b, hz), _hsteps = dx, _domain_name = "Sigma_ext",
        _side_names = Strings("Gamma_gauche","","Gamma_droite",""));
        Rectangle S3(_v1 = Point(0, 0, 0), _v2 = Point(0, b, 0), _v4 = Point(0, 0, hz), _hsteps = dx, _domain_name = "Sigma_ext",
        _side_names = Strings("Gamma_gauche","","Gamma_droite",""));
        Rectangle S4(_v1 = Point(a, 0, 0), _v2 = Point(a, b, 0), _v4 = Point(a, 0, hz), _hsteps = dx, _domain_name = "Sigma_ext",
        _side_names = Strings("Gamma_gauche","","Gamma_droite",""));
        Rectangle S5(_v1 = Point(0, 0, 0), _v2 = Point(a, 0, 0), _v4 = Point(0, b, 0), _hsteps = dx, _domain_name = "Sigma_gauche",
        _side_names=Strings("Gamma_gauche", "Gamma_gauche", "Gamma_gauche", "Gamma_gauche"));
        Rectangle S6(_v1 = Point(0, 0, hz), _v2 = Point(a, 0, hz), _v4 = Point(0, b, hz), _hsteps = dx, _domain_name = "Sigma_droite",
        _side_names=Strings("Gamma_droite", "Gamma_droite", "Gamma_droite", "Gamma_droite"));
        Guide = volumeFrom(S1+S2+S3+S4+S5+S6, _domain_name = "Omega");
    }
    else{
        Point cg(0., 0., 0.), pg1(R_g, 0., 0.), pg2(0., R_g, 0.), pg3(-R_g, 0., 0.), pg4(0., -R_g, 0.);
        Point cd(0., 0., hz), pd1(R_g, 0., hz), pd2(0., R_g, hz), pd3(-R_g, 0., hz), pd4(0., -R_g, hz);
        CircArc L1(_center = cg, _v1 = pg1, _v2 = pg2, _hsteps = dx, _domain_name = "Gamma_gauche");
        CircArc L2(_center = cg, _v1 = pg2, _v2 = pg3, _hsteps = dx, _domain_name = "Gamma_gauche");
        CircArc L3(_center = cg, _v1 = pg3, _v2 = pg4, _hsteps = dx, _domain_name = "Gamma_gauche");
        CircArc L4(_center = cg, _v1 = pg4, _v2 = pg1, _hsteps = dx, _domain_name = "Gamma_gauche");
        CircArc L5(_center = cd, _v1 = pd2, _v2 = pd1, _hsteps = dx, _domain_name = "Gamma_droite");
        CircArc L6(_center = cd, _v1 = pd3, _v2 = pd2, _hsteps = dx, _domain_name = "Gamma_droite");
        CircArc L7(_center = cd, _v1 = pd4, _v2 = pd3, _hsteps = dx, _domain_name = "Gamma_droite");
        CircArc L8(_center = cd, _v1 = pd1, _v2 = pd4, _hsteps = dx, _domain_name = "Gamma_droite");
        Segment L9 (_v1 = pg1, _v2 = pd1, _hsteps = dx);
        Segment L10(_v1 = pg2, _v2 = pd2, _hsteps = dx);
        Segment L11(_v1 = pg3, _v2 = pd3, _hsteps = dx);
        Segment L12(_v1 = pg4, _v2 = pd4, _hsteps = dx);
        Geometry S1 = ruledSurfaceFrom(L1 + L10 + L5 + ~L9,  _domain_name = "Sigma_ext");
        Geometry S2 = ruledSurfaceFrom(L2 + L11 + L6 + ~L10, _domain_name = "Sigma_ext");
        Geometry S3 = ruledSurfaceFrom(L3 + L12 + L7 + ~L11, _domain_name = "Sigma_ext");
        Geometry S4 = ruledSurfaceFrom(L4 + L9  + L8 + ~L12, _domain_name = "Sigma_ext");
        Geometry S5 = surfaceFrom(L1 + L2 + L3 + L4, _domain_name = "Sigma_gauche");
        Geometry S6 = surfaceFrom(L8 + L7 + L6 + L5, _domain_name = "Sigma_droite");
        Guide = volumeFrom(S1 + S2 + S3 + S4 + S5 + S6, _domain_name = "Omega");
    }
    Mesh mail3d(Guide, _tetrahedron, 1, _gmsh);
    Domain Omega = mail3d.domain("Omega");
    Domain Sigma_g = mail3d.domain("Sigma_gauche");
    Domain Sigma_d = mail3d.domain("Sigma_droite");
    Domain Sigma_e = mail3d.domain("Sigma_ext");
    Domain Gamma_bg = mail3d.domain("Gamma_gauche");
    Domain Gamma_bd = mail3d.domain("Gamma_droite");
    Sigma_g.setNormalOrientation (_towardsInfinite);
    Sigma_e.setNormalOrientation (_towardsInfinite);
    Sigma_d.setNormalOrientation (_towardsInfinite);
    elapsedTime("Construction du maillage "); theCout << "\n" << eol;

    // ******************** Calcul des éléments propres ********************
    Number NbModes = 50;                                                           // Nombres de modes calculés
    Space W_ned(_domain = Sigma_d, _interpolation = NE1_1, _name = "Wned");        // Espace éléments finis de Nédélec d'ordre 1
    Unknown ue(W_ned, _name = "ue"); TestFunction ve(ue, _name = "ve");            // inconnu e (vecteur dim 2 tangentiel)
    Unknown uh(W_ned, _name = "uh"); TestFunction vh(uh, _name = "vh");            // inconnu h (vecteur dim 2 tangentiel)
    theCout << "\n" << eol; elapsedTime("Création des espaces éléments finis "); theCout << "\n" << eol;


    Function feps(epsilon, pars);
    Function fmu(mu, pars);
    Function fepsinv(epsilon_inv, pars);
    Function fmuinv(mu_inv, pars);

    BilinearForm a1 = intg(Sigma_d, fepsinv * curl(uh) | curl(vh));
    BilinearForm a2 = intg(Sigma_d, fmuinv * curl(ue) | curl(ve));
    BilinearForm a3 = - w*w * intg(Sigma_d, fmu * uh | vh);
    BilinearForm a4 = - w*w * intg(Sigma_d, feps * ue | ve);

    BilinearForm b1 = w * intg(Sigma_d, ncross(ue) | vh);
    BilinearForm b2 = - w * intg(Sigma_d, ncross(uh) | ve);


    EssentialConditions ecs = ( ncross(ue) | Gamma_bd = 0 );

    TermMatrix A(a1 + a2 + a3 + a4, ecs, _reduction=ReductionMethod(_pseudoReduction, 1000.), _name = "A");
    TermMatrix B(b1 + b2, ecs, _reduction=ReductionMethod(_pseudoReduction, 1.), _name = "B");

    EigenElements ees = eigenSolve(B, A, _nev = NbModes, _which = "LM");
    TermVectors& VecProp = ees.vectors;
    elapsedTime("Calcul des éléments propres "); theCout << "\n" << eol;


    Space Wlag(_domain = Sigma_d, _interpolation = P1, _name = "Wlag");            // Espace éléments finis de Lagrange d'ordre 1
    Unknown ulag(Wlag, _name = "ulag", _dim = 3);
    TestFunction vlag(ulag, _name = "vlag");
    theCout << "\n" << eol; elapsedTime("Création de l'espace de Lagrange "); theCout << "\n" << eol;

    TermVectors VecPropLag(NbModes);

    for (int i=1; i <= NbModes; i++){
        TermVector& vpropi=VecProp(i);
        VecPropLag(i) = projection(vpropi(ue), Wlag, 3, ulag);
    }
    saveToFile("modes_propres", VecPropLag, _format = _vtu, _data_name="u_lag");

    totalElapsedTime("Temps total ");

    theCout << "\n -!-!-!-!-!-!-!-!-!- Program finished -!-!-!-!-!-!-!-!-!- \n" << eol;
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

} // end of namespace dev_other
