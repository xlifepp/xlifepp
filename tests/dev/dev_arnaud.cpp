/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_arnaud.cpp
\author E. Lunéville
\since 1 March 2015
\date 1 March 2015
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace xlifepp;
using namespace std;

namespace dev_arnaud
{
Complex uinc(const Point& M, Parameters& pa = defaultParameters)
{
  Real k=pa("k");Point d=pa("dinc");
  return exp(i_*(k*dot(M,d)));
}

Vector<Complex> grad_uinc(const Point& M, Parameters& pa = defaultParameters)
{
  Real k=pa("k");Point d=pa("dinc");
  return i_*k*exp(i_*(k*dot(M,d)))*Vector<Complex>(d);
}

Real shadowColoringRule(const GeomElement& gelt,
                          const vector<Vector<Real> >& val)
{ GeomMapData *gmap=gelt.meshElement()->geomMapData_p;
  if(gmap==nullptr)  gmap = new GeomMapData(gelt.meshElement());
  if(gmap->normalVector.size()==0) gmap->computeOrientedNormal();
  Real res = 0.;
  for(Number i=0; i<val.size() && res==0; i++)
    {if(dot(val[i],gmap->normalVector)>-theTolerance)
       res+=-1.;
    }
  return res;}

void restrictToLightZone(TermVector& ui)
{
  Space& V = *ui.unknown()->space();
  for(Number j=0; j<V.nbOfElements(); j++) // built dof colors
  {
    const Element* elt=V.element_p(j);
    if(elt->geomElt_p->color!=0)
    {
      const std::vector<Number>& dofNum=elt->dofNumbers;
      std::vector<Number>::const_iterator itn = dofNum.begin();
      for(; itn!=dofNum.end(); ++itn)
      ui.setValue(*itn,0.);
    }
  }
}

void dev_arnaud(int argc, char* argv[], bool check)
{
  String rootname = "dev_arnaud";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(40);
  // Trace::trackingMode=true;
  // Trace::logOn();
  String errors;
  Number nbErrors = 0;

  Real k=60.;

  //meshing
  Real r1=1., r2=0.25, lext=max(r1,r2)/2;
  Point c1(0.,0.), c2(-1.5,0.5);
  Real a=min(c1[0]-r1,c2[0]-r2)-lext, b=max(c1[0]+r1,c2[0]+r2)+lext;
  Real c=min(c1[1]-r1,c2[1]-r2)-lext, d=max(c1[1]+r1,c2[1]+r2)+lext;
  Point v1(a,c), v2(b,c), v4(a,d);
  Real hsize=2*pi_/k/10.;  // 10 pts by wavelenght
  Disk d1(_center=c1, _radius=r1, _hsteps=hsize, _domain_name="Obs1", _side_names="Gamma1");
  Disk d2(_center=c2, _radius=r2, _hsteps=hsize, _domain_name="Obs2", _side_names="Gamma2");
  Rectangle rect(_v1=v1,_v2=v2,_v4=v4,_hsteps=2*hsize,_domain_name="Omega");
  Mesh m(rect-d1-d2, _shape=triangle);
  Domain omega = m.domain("Omega");
  Domain gamma1 = m.domain("Gamma1"),gamma2 = m.domain("Gamma2");
  theCout << "Mesh size = " << hsize <<" Number of Triangles = " << m.nbOfElements() << eol;

  gamma1.setNormalOrientation(_towardsInfinite);
  gamma2.setNormalOrientation(_towardsInfinite);

  Domain gamma=merge(gamma1,gamma2,"Gamma");
  gamma.setNormalOrientation(_towardsInfinite);
  Number ord=1;


  //defining parameter and kernel
  Parameters pars;
  Real theta=0;
  pars << Parameter(k,"k")<<Parameter(Point(cos(theta),sin(theta)),"dinc");
  Kernel G=Helmholtz2dKernel(pars);
  Function fuinc(uinc,pars);
  IntegrationMethods IMie(Duffy,15,0.,_defaultRule,12,1.,_defaultRule,10,2.,_defaultRule,8); // Duffy integration method
  IntegrationMethods IMir(LenoirSalles2dIR(),_singularPart,theRealMax, QuadratureIM(_GaussLegendreRule,10),_regularPart,theRealMax);
  IntegrationMethods IMir2(_GaussLegendreRule,20);
  //space for integral representation
  Space V(_domain=omega, _interpolation=P1, _name="V", _notOptimizeNumbering);
  Unknown u(V,"u");
  theCout<< "Nb dofs V= " << V.nbDofs() << eol;
  elapsedTime();

  // =================================
  //             full BEM
  // =================================
  //BEM space and unknown
  Space H(_domain=gamma, _FE_type=Lagrange, _order=ord, _name="H", _notOptimizeNumbering);
  Unknown p(H, _name="p"); TestFunction q(p, _name="q");
  theCout<<"Nb dofs full BEM= " << H.nbDofs()<< eol;
  //defining Integral Equation (indirect MFIE)
  BilinearForm mlf=intg(gamma,p*q);
  BilinearForm apq=0.5*intg(gamma,p*q)-intg(gamma,gamma,p*ndotgrad_y(G)*q,_method=IMie);
  TermMatrix K(apq, _name="K"), M(mlf, _name="M");
  TermVector Uinc(p,gamma,fuinc,_name="F");
  TermVector F=M*Uinc; //F*=-1;
  //solving linear system using direct method
  TermVector P=directSolve(K,F);
  elapsedTime("BEM",theCout);
  //representing the solution on Omega
  TermVector Uext = integralRepresentation(u,omega,intg(gamma,ndotgrad_y(G)*P,_method=IMir));
  TermVector Ui(u,omega,fuinc);
  TermVector Uext_t = Uext + Ui;
  elapsedTime("repint",theCout);
  saveToFile("Uinc", Ui, _format=_vtu);       // incident field on omega
  saveToFile("U_BEM", Uext, _format=_vtu);     // diffracted field on omega
  saveToFile("U_BEM_t", Uext_t, _format=_vtu); // total field on omega
  elapsedTime();

  // =================================
  //        itératif BEM-BEM
  // =================================
  //BEM sur Gamma1
  Space H1(_domain=gamma1, _FE_type=Lagrange, _order=ord, _name="H1", _notOptimizeNumbering);
  Unknown p1(H1, _name="p1");TestFunction q1(p1, _name="q1");
  theCout<<"Nb dofs BEM1= " << H1.nbDofs()<< eol;
  BilinearForm mlf1=intg(gamma1,p1*q1);
  BilinearForm ap1q1=0.5*intg(gamma1,p1*q1)-intg(gamma1,gamma1,p1*ndotgrad_y(G)*q1,_method=IMie);
  TermMatrix K1(ap1q1, _name="K"), M1(mlf1, _name="M");
  factorize(K1);
  elapsedTime("construction BEM1",theCout);
  //BEM sur Gamma2
  Space H2(_domain=gamma2, _FE_type=Lagrange, _order=ord, _name="H2", _notOptimizeNumbering);
  Unknown p2(H2, _name="p2");TestFunction q2(p2, _name="q2");
  theCout<<"Nb dofs BEM2= " << H2.nbDofs()<< eol;
  BilinearForm mlf2=intg(gamma2,p2*q2);
  BilinearForm ap2q2=0.5*intg(gamma2,p2*q2)-intg(gamma2,gamma2,p2*ndotgrad_y(G)*q2,_method=IMie);
  TermMatrix K2(ap2q2, _name="K"), M2(mlf2, _name="M");
  factorize(K2);
  elapsedTime("construction BEM2",theCout);

  TermMatrix R12=integralRepresentation(p2,gamma2,intg(gamma1,ndotgrad_y(G)*p1,_method=IMir),"R12");
  TermMatrix R21=integralRepresentation(p1,gamma1,intg(gamma2,ndotgrad_y(G)*p2,_method=IMir),"R21");
  elapsedTime("construction R12, R21",theCout);
  TermMatrix R1=integralRepresentation(u,omega,intg(gamma1,ndotgrad_y(G)*p1,_method=IMir),"R1");
  TermMatrix R2=integralRepresentation(u,omega,intg(gamma2,ndotgrad_y(G)*p2,_method=IMir),"R2");

  TermVector PBEM1= projection(P,H1);
  TermVector PBEM2= projection(P,H2);
  Real nl2;
  nl2 =sqrt(abs(hermitianProduct(M1*PBEM1,PBEM1))+abs(hermitianProduct(M2*PBEM2,PBEM2)));
  elapsedTime();
  //initialisation de la boucle iterative BEM1-BEM2 (Jacobi)
  TermVector U1inc(p1,gamma1,fuinc,_name="U1inc"), U1=U1inc, P1, E1, P1p(p1,gamma1,Complex(0));
  TermVector U2inc(p2,gamma2,fuinc,_name="U2inc"), U2=U2inc, P2, E2, P2p(p2,gamma2,Complex(0));
  Number N=10;
  for (Number n=0;n<N;n++)
  {
    P1=factSolve(K1,M1*U1); P2=factSolve(K2,M2*U2);
    U1 = R21*P2; U2 = R12*P1;
    P1p+=P1; P2p+=P2;  //accumulate P's
    //écart
    E1=P1p-PBEM1, E2=P2p-PBEM2;
    theCout << "iteration "<<n+1<<" ecart (%) with full BEM : "
            << 100*sqrt(abs(hermitianProduct(M1*E1,E1))+abs(hermitianProduct(M2*E2,E2)))/nl2<<eol;
  }
  elapsedTime("schéma itératif",theCout);
  TermVector U12ext=R1*P1p+R2*P2p;
  TermVector U12ext_t=U12ext+Ui;
  saveToFile("U_BEM_BEM", U12ext, _format=_vtu);     // diffracted field on omega
  saveToFile("U_BEM_BEM_t", U12ext_t, _format=_vtu); // total field on omega
  saveToFile("ecart_UBEM_BEM", U12ext-Uext, _format=_vtu);
  E1=P1p-PBEM1, E2=P2p-PBEM2;
  Real erl2=sqrt(abs(hermitianProduct(M1*E1,E1))+abs(hermitianProduct(M2*E2,E2)));
  theCout<<"ecart (%) with full BEM : "<<100*erl2/nl2<<eol;

  // =================================
  //      itératif Kirchoff-BEM
  // =================================
  N=10;
  Unknown p1_2(H1,"p1_2",2);
  Function ginc(grad_uinc,pars);
  U1=U1inc; U2=U2inc; P2p = TermVector(p2,gamma2,Complex(0)) ;
  TermVector U1p(p1,gamma1,Complex(0));
  TermVector gradU1(p1_2,gamma1,ginc, _name="gradU1");
  nl2 =sqrt(abs(hermitianProduct(M2*PBEM2,PBEM2)));
  TermMatrix RK12=2*integralRepresentation(p2,gamma2,intg(gamma1,ndotgrad_y(G)*p1,_method=IMir2),"RK12");
  TermMatrix RK1=2*integralRepresentation(u,omega,intg(gamma1,ndotgrad_y(G)*p1,_method=IMir),"RK1");
  for (Number n=0;n<N;n++)
  {
    // solve gamma1 pb using Kirchoff
    TermVector D1s =(gradU1/U1).toImag()/k;
    setColor(gamma1, D1s, shadowColoringRule);
    restrictToLightZone(U1);
    saveToFile("D1_"+tostring(n), D1s);
    saveToFile("U1_light_"+tostring(n), U1);
    gamma1.saveColorsToFile("colors_"+tostring(n));
    U1p+=U1;
    if (n==0) U2 += RK12*U1;  else U2 = RK12*U1;  //at first step we add first contribution from gamma1
    // solve gamma2 pb using BEM and update U1 and gradU1
    P2=factSolve(K2,M2*U2);
    saveToFile("U2omega_"+tostring(n), R2*P2);
    P2p+=P2;   // accumulate P2 to do intg rep outside the loop
    U1 = R21*P2;
    gradU1 = integralRepresentation(p1_2,gamma1,intg(gamma2,grad_x(ndotgrad_y(G))*p2,_method=IMir2),P2);
    // gamma2 current difference
    E2=P2p-PBEM2;
    Real erl2=sqrt(abs(hermitianProduct(M2*E2,E2)));
    theCout<<"ecart P2 (%) with full BEM : "<<100*erl2/nl2<<eol;
  }
  //TermVector UKBEM=U1p+R2*P2p;
  TermVector UKBEM=RK1*U1p+R2*P2p;
  saveToFile("U_KIRCHOFF_BEM", UKBEM, _format=_vtu);      // diffracted field on omega
  saveToFile("U_KIRCHOFF_BEM_t", UKBEM+Ui, _format=_vtu); // total field on omega

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
