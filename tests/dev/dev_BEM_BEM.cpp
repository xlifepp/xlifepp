/*
XLiFE++ is an extended library of finite elements written in C++
    Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*!

\file dev_BEM_BEM.cpp
\author E. Lunéville
\since 12 Sep 2018
\date 12 Sep 2018
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"


using namespace std;
using namespace xlifepp;


namespace dev_BEM_BEM
{

Real ia=0.,wa=135;          // incidence angle
Real k=1., kx=1., ky=0.;

//=====================================================================================================================
// set value to 0 on an interface except for some dofs
void correctInterface(TermVector& q, Domain interface, const std::list<Dof*>& dofs = std::list<Dof*>())
{
  std::list<Value> vs;
  std::list<Dof*>::const_iterator itd=dofs.begin();
  for(; itd!=dofs.end(); ++itd)
    vs.push_back(q.getValue(**itd));
  if(q.valueType()==xlifepp::_complex)
    q.setValue(interface,Complex(0));  // cancel values on interface
  else
    q.setValue(interface,0.);
  std::list<Value>::iterator itv=vs.begin();
  for(itd=dofs.begin(); itd!=dofs.end(); ++itd,++itv)
    q.setValue(**itd,*itv);
}

void correctInterface(TermVector& q, Domain interface, Dof* d1, Dof* d2=nullptr)
{
  std::list<Dof*> dofs;
  if(d1!=nullptr)
    dofs.push_back(d1);
  if(d2!=nullptr)
    dofs.push_back(d2);
  correctInterface(q,interface,dofs);
}

void saveFarField(const TermVector& ud, const Complex& amp, const Point& Cr, const String& fn="farfield.data")
{
  const Unknown& u = *ud.unknown();
  std::map<Real,Real> mfar;
  for(Number i=1; i<=ud.nbDofs(); i++)
    {
      Point p=ud.dof(u,i).coords();
      Complex val=ud.getValue(i).asComplex();
      Real t=std::atan2(p(2)-Cr(2),p(1)-Cr(1));
      mfar[t]=std::abs(amp*val);
    }
  std::map<Real,Real>::iterator itm=mfar.begin();
  std::ofstream fout(fn.c_str());
  for(; itm!=mfar.end(); ++itm)
    fout<<itm->first<<" "<<itm->second<<eol;
  fout.close();
}

enum ComputeBoxType {_BEM1=0, _BEM2, _BEM_CFIE, _BEM_ND, _BEM_NF, _BEM_sigma, _Kirchoff, _ray, _UTD};
enum DataRequired {_noData, _uRequired, _dnuRequired, _dfuRequired, _graduRequired, _ugraduRequired};
typedef std::pair<GeomDomain*,Unknown*> DomUnkPair;

//=====================================================================================================================
//  class to manage data required by a box
//=====================================================================================================================
typedef std::list<std::pair<DiffOpType,Complex> > LcDiffOperator;   //linear combination of diff operator

std::set<DiffOpType> diffOperators(const LcDiffOperator& lcdo)
{
  std::set<DiffOpType> ops;
  std::list<std::pair<DiffOpType,Complex> >::const_iterator itl=lcdo.begin();
  for(; itl!=lcdo.end(); ++itl)
    ops.insert(itl->first);
  return ops;
}

std::ostream& operator<<(std::ostream& out, const LcDiffOperator& lcdo)
{
  std::list<std::pair<DiffOpType,Complex> >::const_iterator itl=lcdo.begin();
  String plus="";
  for(; itl!=lcdo.end(); ++itl)
    {
      out<<plus;
      if(itl->second!=1.)
        out<<itl->second<<"*";
      out<<words("diffop",itl->first);
      plus="+";
    }
  return out;
}

class DataBox
{
  public:
    std::list<std::pair<LcDiffOperator, TermVector> > data;
    typedef  std::list<std::pair<LcDiffOperator, TermVector> >::iterator ItDb;
    typedef  std::list<std::pair<LcDiffOperator, TermVector> >::const_iterator CitDb;
    ItDb begin() {return data.begin();}
    CitDb begin() const {return data.begin();}
    ItDb end() {return data.end();}
    CitDb end() const {return data.end();}

    DataBox() {}
    DataBox(DiffOpType df, const Complex& a=1.) {add(df,a);}

    void add(DiffOpType df, const Complex& a=1.)
    {
      data.push_back(std::make_pair(LcDiffOperator(1,std::make_pair(df,a)),TermVector()));
    }

    void add(DiffOpType df1, const Complex& a1, DiffOpType df2, const Complex& a2) //linear combination a1.df1+a2.df2
    {
      LcDiffOperator lcdo;
      lcdo.push_back(std::make_pair(df1,a1));
      lcdo.push_back(std::make_pair(df2,a2));
      data.push_back(std::make_pair(lcdo,TermVector()));
    }

    std::set<DiffOpType> operatorsRequired() const
    {
      std::set<DiffOpType> ops;
      for(CitDb itdb=begin(); itdb!=end(); ++itdb)
        {
          std::set<DiffOpType> opsi=diffOperators(itdb->first);
          ops.insert(opsi.begin(),opsi.end());
        }
      return ops;
    }

    DataBox& operator+=(const DataBox& cb)  //!< += assuming same structure (not checked)
    {
      CitDb it2=cb.begin();
      for(ItDb it=begin(); it!=end(); ++it, ++it2)
        it->second+=it2->second;
      return *this;
    }

    DataBox& operator-=(const DataBox& cb)  //!< -= assuming same structure (not checked)
    {
      CitDb it2=cb.begin();
      for(ItDb it=begin(); it!=end(); ++it,++it2)
        it->second-=it2->second;
      return *this;
    }

    void print(std::ostream& out) const
    {
      out<<"DataBox"<<eol;
      for(CitDb it=begin(); it!=end(); ++it)
        out<<it->first<<":"<<it->second;
    }
}; //end of DataBox class definition

std::ostream& operator<<(std::ostream& out, const DataBox& db)
{db.print(out); return out;}

//=====================================================================================================================
// abstract class for all ComputeBox classes
//=====================================================================================================================
class ComputeBox
{
  public:
    String name;                            //!< name of the ComputeBox
    mutable Number iter;                    //!< current iteration number
    bool preComputed;                         //!< flag useful if precomputation
    DataBox dataBox;                          //!< data used by box
    bool update_obs;                          //!< intern flag to manage the computation on the observation domain
    Real index;                             //!< free index

    virtual ComputeBoxType type() const = 0;  //!< type of the ComputeBox,  see ComputeBoxType enum
    virtual String typeName() const = 0;    //!< type of the ComputeBox
    virtual GeomDomain& domain() const = 0;   //!< return domain if one
    virtual Unknown& unknown() const = 0;     //!< return unknown if one
    virtual Unknown& vec_unknown() const      //! return vector unknown if one (use for grad computation)
    {
      error("free_error","vector unknown not managed by ComputeBox "+name);
      return *new Unknown();  //fake return
    }

    virtual bool hasComputedField() const {return false;}
    virtual const TermVector& computedField() const
    {
      error("free_error","no computed field in ComputeBox "+name);
      return * new TermVector(); //fake return
    }

    void setData(const TermVector& dt1, const TermVector& dt2=TermVector()) //! set computeBox data according to DataBox structure (not checked)
    {
      DataBox::ItDb itd=dataBox.begin();
      *itd=std::make_pair(itd->first,dt1);
      itd++;
      if(dt2.computed())
        *itd=std::make_pair(itd->first,dt2);
    }

    virtual void preCompute() {} ;                      //!< virtual precomputation function
    virtual void solve() = 0;                           //!< virtual solver

    virtual TermVector computeOn(Domain repdom, Unknown& ur)=0;     //!< virtual computation of representation on other domain using given data
    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)=0;   //!< virtual computation of representation on other domain using given data
    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)=0; //!< virtual computation of representation on other domain using given data

    //! evaluate all required data (dataBox) by the ComputeBox cb using p,q data
    virtual void computeOn(ComputeBox& cb)
    {
      std::set<DiffOpType> ops =cb.dataBox.operatorsRequired();
      std::map<DiffOpType, TermVector> tvs;
      for(std::set<DiffOpType>::iterator its=ops.begin(); its!=ops.end(); ++its)
        {
          switch(*its)
            {
              case _id :
                tvs[_id]= computeOn(cb.domain(),cb.unknown());
                break;
              case _ndotgrad:
              case _ndotgrad_x:
                tvs[_ndotgrad]= computeDnOn(cb.domain(),cb.unknown());
                break;
              case _grad:
              case _grad_x:
                tvs[_grad]= computeGradOn(cb.domain(),cb.vec_unknown());
                break;
              default:
                error("free_error","unavailable diff operator in ComputeBox::computeOnBox");
            }
        }
      for(DataBox::ItDb itdb=cb.dataBox.begin(); itdb!=cb.dataBox.end(); ++itdb)
        {
          LcDiffOperator lcdo=itdb->first;
          TermVector & tv=itdb->second;
          tv*=0.;
          std::list<std::pair<DiffOpType,Complex> >::const_iterator itl=lcdo.begin();
          for(; itl!=lcdo.end(); ++itl)
            {
              if(itl->second!=0)
                {
                  if(itl->second==1.)
                    tv+= tvs[itl->first];
                  else
                    tv+=itl->second * tvs[itl->first];
                }
            }
        }
    }

};

//=====================================================================================================================
// computeBoxBEM abstract class collecting all BEM methods
//=====================================================================================================================
class ComputeBoxBEM: public ComputeBox
{
  public:
    Kernel *Gp;                               //!< pointer to kernel
    const IntegrationMethods *IMie;           //!< integration methods for BEM
    const IntegrationMethods *IMir;           //!< integration methods for integral representations
    TermVector X;                             //!< BEM solution

    ComputeBoxBEM() : Gp(0), IMie(0), IMir(0) {}
    ComputeBoxBEM(Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir, bool precompute, const String& na="")
      :  Gp(&ker), IMie(&ie), IMir(&ir)
    {
      name=na;
      preComputed=precompute;
    }
    virtual ComputeBoxType type() const =0;
    virtual String typeName() const =0;
    virtual GeomDomain& domain() const = 0;
    virtual Unknown& unknown() const = 0;
    virtual Unknown& vec_unknown() const
    {
      error("free_error","vector unknown not managed by ComputeBoxBEM "+name);
      return *new Unknown();  //fake return
    }
};

//=====================================================================================================================
// BEM method 1 (Neumann dn_u=-dn_ui) : (I/2-D)p = - ui  on dom and u= -Dp on dom_ext
//=====================================================================================================================
class ComputeBoxBEM1: public ComputeBoxBEM
{
  public:
    GeomDomain *domp;                         //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                         //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                   //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermVector Ui;                            //!< Dirichlet trace data
    TermMatrix K, Krhs;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> URep;     //!< integral representation matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> DnURep;   //!< Dn integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> gradURep; //!< grad integral representation matrix list indexed by (dom, unknown)
    GeomDomain* interfacep;                   //!< dom where solution is cancelled (unused when 0)
    std::list<Dof*> exceptDofs;               //!< list of dofs on interface that are not cancelled (generally dofs at endpoints)
    bool dofCorrectionAllTime;                //!< if true dof correction is applied to all iterations else only to the first iteration (default)

  private:
    ComputeBoxBEM1() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr;}  //forbidden default constructor
  public:
    ComputeBoxBEM1(GeomDomain& d, Unknown& unk, Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir, bool precompute, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&d;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      interfacep=nullptr;
      dataBox.add(_id);  //say require u
      if(precompute)
        preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM1;}
    virtual String typeName() const {return "BEM1";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    void setInterface(GeomDomain& interf, Dof* d1=nullptr, Dof* d2=nullptr, bool dcall = false)
    {
      interfacep=&interf;
      if(d1!=nullptr)
        exceptDofs.push_back(d1);
      if(d2!=nullptr)
        exceptDofs.push_back(d2);
      dofCorrectionAllTime = dcall;
    }

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      GeomDomain& dom=*domp;
      BilinearForm auv=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie);
      BilinearForm rhs=intg(dom,u*v);
      K=TermMatrix(auv, _name="K");
      Krhs=TermMatrix(rhs, _name="Krhs");
      factorize(K,K);
      preComputed=true;
    }

    virtual void solve()
    {
      TermVector& ui=dataBox.begin()->second;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          GeomDomain& dom=*domp;
          BilinearForm auv=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie);
          BilinearForm rhs=intg(dom,u*v);
          K=TermMatrix(auv, _name="K");
          Krhs=TermMatrix(rhs, _name="Krhs");
          X=directSolve(K,Krhs*ui);
        }
      else
        X=factSolve(K,Krhs*ui);
      //thePrintStream<<"X="<<X<<eol;
      if(interfacep!=nullptr)
        {
          if(iter==1 || dofCorrectionAllTime)
            correctInterface(X,*interfacep,exceptDofs);
          else
            correctInterface(X,*interfacep);
        }
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      if(repdom.numberOfElements()*ur.space()->dimSpace() > 5.E08) // IR matrix too big, direct computation
        return integralRepresentation(ur,repdom,intg(*domp,ndotgrad_y(*Gp)*(*up),_method=*IMir),X);
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(URep.find(dup)==URep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          URep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_y(G)*u,_method=*IMir));
        }
      return (URep[dup]*X);
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      if(repdom.numberOfElements()*ur.space()->dimSpace() > 5.E08) // IR matrix too big, direct computation
        return integralRepresentation(ur,repdom,intg(*domp,ndotgrad_x(ndotgrad_y(*Gp))*(*up),_method=*IMir),X);
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DnURep.find(dup)==DnURep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          DnURep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir));
        }
      return (DnURep[dup]*X);
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      GeomDomain& dom=*domp;
      return integralRepresentation(ur,repdom,intg(dom,grad_x(ndotgrad_y(G))*u,_method=*IMir),X);
    }

}; //end of ComputeBoxBEM1 definition


//=====================================================================================================================
//BEM method 2 (I/2-D)p = S dn_ui  (Neumann dn_u=-dn_ui)
//=====================================================================================================================
class ComputeBoxBEM2: public ComputeBoxBEM
{
  public:
    GeomDomain *domp;                          //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                          //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                    //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermMatrix K, Krhs;                        //!< BEM matrices
    TermVector DnUi;                           //!< Neumann trace data
    std::map<DomUnkPair,TermMatrix> SRep;      //!< single layer matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> DRep;      //!< double layer matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> D2Rep;     //!< double double layer matrix list indexed by (domain, unknown)
    GeomDomain* interfacep;                    //!< domain where solution is cancelled (unused when 0)
    std::list<Dof*> exceptDofs;                //!< list of dofs on interface that are not cancelled (generally dofs at endpoints)
    bool dofCorrectionAllTime;                 //!< if true dof domcorrection is applied to all iterations else only to the first iteration (default)

    ComputeBoxBEM2() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr; interfacep=nullptr;}
    ComputeBoxBEM2(GeomDomain& d, Unknown& unk, Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir, bool precompute, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&d;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      interfacep=nullptr;
      dataBox.add(_ndotgrad);
      if(precompute)
        preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM2;}
    virtual String typeName() const {return "BEM2";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    void setInterface(GeomDomain& interf, Dof* d1=nullptr, Dof* d2=nullptr, bool dcall = false)
    {
      interfacep=&interf;
      if(d1!=nullptr)
        exceptDofs.push_back(d1);
      if(d2!=nullptr)
        exceptDofs.push_back(d2);
      dofCorrectionAllTime = dcall;
    }

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      GeomDomain& dom=*domp;
      BilinearForm auv=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie); // I/2-D
      BilinearForm rhs=intg(dom,dom, u*G*v,_method=*IMie);  // S
      K=TermMatrix(auv, _name="K");
      Krhs=TermMatrix(rhs, _name="Krhs");
      factorize(K,K);
      preComputed=true;
    }
    virtual void solve()
    {
      TermVector& dnui=dataBox.begin()->second;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          GeomDomain& dom=*domp;
          BilinearForm auv=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie); // I/2-D
          BilinearForm rhs=intg(dom,dom, u*G*v,_method=*IMie);  // S
          K=TermMatrix(auv, _name="K");
          Krhs=TermMatrix(rhs, _name="Krhs");
          X=directSolve(K,Krhs*dnui);
        }
      else
        X=factSolve(K,Krhs*dnui);
      if(interfacep!=nullptr)
        {
          if(iter==1 || dofCorrectionAllTime)
            correctInterface(X,*interfacep,exceptDofs);
          else
            correctInterface(X,*interfacep);
        }
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      GeomDomain& dom=*domp;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
        DRep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_y(G)*u,_method=*IMir));
      if(SRep.find(dup)==SRep.end())
        SRep[dup]=integralRepresentation(ur,repdom,intg(dom,G*u,_method=*IMir));
      Number nbcol=DRep[dup].numberOfCols();
      return DRep[dup]*X+SRep[dup]*dataBox.begin()->second;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      GeomDomain& dom=*domp;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
        DRep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_y(G)*u,_method=*IMir));
      if(D2Rep.find(dup)==D2Rep.end())
        D2Rep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir));
      return D2Rep[dup]*X-DRep[dup]*dataBox.begin()->second;
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxBEM2 does not yet propose integral representation of gradient");
      return TermVector(); //fake return
    }
}; //end of ComputeBoxBEM2 definition

//=====================================================================================================================
// CFIE BEM method (Neumann dn_u=-dn_ui) :
//       (I/2-D)p - eta Sp = ui + eta.dn.ui on domp
//       u = Dp   on dom_ext
// with  eta = - a/(1-a) * i/k  (common value a=0.2)
//=====================================================================================================================
class ComputeBoxBEMCFIE: public ComputeBoxBEM
{
  public:
    GeomDomain *domp;                         //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                         //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                   //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermVector Ui;                            //!< Dirichlet trace data
    TermMatrix K, Krhs;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> URep;     //!< integral representation matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> DnURep;   //!< Dn integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> gradURep; //!< grad integral representation matrix list indexed by (dom, unknown)
    Complex eta;
    Real k;

  private:
    ComputeBoxBEMCFIE() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr;}  //forbidden default constructor
  public:
    ComputeBoxBEMCFIE(GeomDomain& d, Unknown& unk, Kernel& ker,const IntegrationMethods& ie, const IntegrationMethods& ir,
                      Real ki, Real a=0.2, bool precompute=true, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&d;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      dataBox.add(_id);       //say require u
      dataBox.add(_ndotgrad); //say require dn(u)
      k=ki;
      eta=Complex(0.,-a/(k*(1-a)));
      if(precompute)
        preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM_CFIE;}
    virtual String typeName() const {return "BEM_CFIE";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      GeomDomain& dom=*domp;
      BilinearForm auv=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie)
                       +eta*intg(dom,dom,(ncrossgrad(u)*G)|ncrossgrad(v),_method=*IMie)-(eta*k*k)*intg(dom,dom,u*(G*(_nx|_ny))*v,_method=*IMie);
      BilinearForm rhs=intg(dom,u*v);
      K=TermMatrix(auv, _name="K");
      Krhs=TermMatrix(rhs, _name="Krhs");
      factorize(K,K);
      preComputed=true;
    }

    virtual void solve()
    {
      TermVector& ui=dataBox.begin()->second;
      TermVector& dui=(++dataBox.begin())->second;
      TermVector ci=eta*dui+ui;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          GeomDomain& dom=*domp;
          BilinearForm auv=0.5*intg(dom,u*v)-intg(dom,dom,u*ndotgrad_y(G)*v,_method=*IMie)
                           +eta*intg(dom,dom,(ncrossgrad(u)*G)|ncrossgrad(v),_method=*IMie)-(eta*k*k)*intg(dom,dom,u*(G*(_nx|_ny))*v,_method=*IMie);
          BilinearForm rhs=intg(dom,u*v);
          K=TermMatrix(auv, _name="K");
          Krhs=TermMatrix(rhs, _name="Krhs");
          X=directSolve(K,Krhs*ci);
        }
      else
        X=factSolve(K,Krhs*ci);
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(URep.find(dup)==URep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          URep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_y(G)*u,_method=*IMir));
        }
      return (URep[dup]*X);
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DnURep.find(dup)==DnURep.end())
        {
          Kernel& G=*Gp;
          Unknown& u=*up;
          GeomDomain& dom=*domp;
          DnURep[dup]=integralRepresentation(ur,repdom,intg(dom,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir));
        }
      return (DnURep[dup]*X);
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      GeomDomain& dom=*domp;
      return integralRepresentation(ur,repdom,intg(dom,grad_x(ndotgrad_y(G))*u,_method=*IMir),X);
    }

}; //end of ComputeBoxBEMCFIE definition

//=====================================================================================================================
// BEM method ND :  (I/2-D).p|gamma_N + S.p|gamma_D =  -ui  (dn_u=-dn_ui on gamma_N and u=-ui on gamma_D)
//                   u = Sp|gamma_D - Dp|gamma_N
//=====================================================================================================================
class ComputeBoxBEM_ND: public ComputeBoxBEM
{
  public:
    GeomDomain * domp;                        //!< Domain supporting the ComputeBox (gamma_N + gamma_D)
    GeomDomain * gamma_N;                     //!< Domain supporting the Neumann condition
    GeomDomain * gamma_D;                     //!< Domain supporting the Dirichlet condition
    Unknown *up, *vp;                         //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                   //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermVector Ui;                            //!< Dirichlet trace data
    TermMatrix K, Krhs;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> URep;     //!< integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> DnURep;   //!< Dn integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> gradURep; //!< grad integral representation matrix list indexed by (dom, unknown)

    std::list<Dof*> exceptDofs;               //!< list of dofs on interface that are not cancelled (generally dofs at endpoints)
    bool dofCorrectionAllTime;                //!< if true dof correction is applied to all iterations else only to the first iteration (default)

  private:
    ComputeBoxBEM_ND() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr;}  //forbidden default constructor
  public:
    ComputeBoxBEM_ND(GeomDomain& dom, GeomDomain& domN, GeomDomain& domD, Unknown& unk, Kernel& ker,const IntegrationMethods& ie,
                     const IntegrationMethods& ir, bool precompute, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&dom;
      gamma_N=&domN;
      gamma_D=&domD;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      dataBox.add(_id);       //say require u
      if(precompute)
        preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM_ND;}
    virtual String typeName() const {return "BEM_ND";}
    virtual GeomDomain& domain() const {return *domp;}
    GeomDomain& domainN() const {return *gamma_N;}
    GeomDomain& domainD() const {return *gamma_D;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      BilinearForm auv= 0.5*intg(*gamma_N,u*v)-intg(*domp,*gamma_N,u*ndotgrad_y(G)*v,_method=*IMie)
                        + intg(*domp,*gamma_D,u*G*v,_method=*IMie);
      BilinearForm rhs=intg(*domp,u*v);
      K=TermMatrix(auv, _name="K");
      Krhs=TermMatrix(rhs, _name="Krhs");
      factorize(K,K);
      preComputed=true;
    }

    virtual void solve()
    {
      TermVector& ui=dataBox.begin()->second;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          BilinearForm auv= 0.5*intg(*gamma_N,u*v)-intg(*domp,*gamma_N,u*ndotgrad_y(G)*v,_method=*IMie)
                            + intg(*domp,*gamma_D,u*G*v,_method=*IMie);
          BilinearForm rhs=intg(*domp,u*v);
          K=TermMatrix(auv, _name="K");
          Krhs=TermMatrix(rhs, _name="Krhs");
          X=directSolve(K,-(Krhs*ui));
        }
      else
        X=factSolve(K,-(Krhs*ui));
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
//    DomUnkPair dup=std::make_pair(&repdom,&ur);
//    if(URep.find(dup)==URep.end())
//      {
//        Kernel& G=*Gp; Unknown& u=*up;
//        URep[dup]= integralRepresentation(ur,repdom,intg(*gamma_D,G*u,_method=*IMir))
//                   - integralRepresentation(ur,repdom,intg(*gamma_N,ndotgrad_y(G)*u,_method=*IMir));
//      }
//    return URep[dup]*X;
      Kernel& G=*Gp;
      Unknown& u=*up;
      return integralRepresentation(ur,repdom,intg(*gamma_D,G*u,_method=*IMir),X|*gamma_D)
             - integralRepresentation(ur,repdom,intg(*gamma_N,ndotgrad_y(G)*u,_method=*IMir),X|*gamma_N);
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
//    DomUnkPair dup=std::make_pair(&repdom,&ur);
//    if(DnURep.find(dup)==DnURep.end())
//      {
//        Kernel& G=*Gp; Unknown& u=*up;
//        DnURep[dup]= integralRepresentation(ur,repdom,intg(*gamma_D,ndotgrad_x(G)*u,_method=*IMir))
//                     - integralRepresentation(ur,repdom,intg(*gamma_N,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir));
//      }
//    return (DnURep[dup]*X);
      Kernel& G=*Gp;
      Unknown& u=*up;
      return integralRepresentation(ur,repdom,intg(*gamma_D,ndotgrad_x(G)*u,_method=*IMir),X|*gamma_D)
             - integralRepresentation(ur,repdom,intg(*gamma_N,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir),X|*gamma_N);
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      return   integralRepresentation(ur,repdom,intg(*gamma_D,grad_x(G)*u,_method=*IMir),X|*gamma_D)
               - integralRepresentation(ur,repdom,intg(*gamma_N,grad_x(ndotgrad_y(G))*u,_method=*IMir),X|*gamma_N);
    }

}; //end of ComputeBoxBEM_ND definition

//=====================================================================================================================
// BEM method NF :  (I/2-D).p -alpha S.p|gamma_F =  -ui  -> dn u=-dn ui on gamma_N and (dn+alpha) u=-(dn+alpha)ui on gamma_F)
//                   u = -alpha S.p|gamma_F - Dp
//=====================================================================================================================
class ComputeBoxBEM_NF: public ComputeBoxBEM
{
  public:
    GeomDomain * domp;                        //!< Domain supporting the ComputeBox (gamma_N + gamma_D)
    GeomDomain * gamma_N;                     //!< Domain supporting the Neumann condition
    GeomDomain * gamma_F;                     //!< Domain supporting the Fourier condition
    Unknown *up, *vp;                         //!< unknown related to the ComputeBox (should not be 0!)
    Unknown *vecup, *vecvp;                   //!< vector unknown related to the ComputeBox (useful when computing gradient of IR)
    TermVector Ui;                            //!< Dirichlet trace data
    TermMatrix K, Krhs;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> URep;     //!< integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> DnURep;   //!< Dn integral representation matrix list indexed by (dom, unknown)
    std::map<DomUnkPair,TermMatrix> gradURep; //!< grad integral representation matrix list indexed by (dom, unknown)
    Complex alpha;                          //!< Fourier coefficient

    std::list<Dof*> exceptDofs;               //!< list of dofs on interface that are not cancelled (generally dofs at endpoints)
    bool dofCorrectionAllTime;                //!< if true dof correction is applied to all iterations else only to the first iteration (default)

  private:
    ComputeBoxBEM_NF() : ComputeBoxBEM() {domp=nullptr; up=nullptr; vp=nullptr;}  //forbidden default constructor
  public:
    ComputeBoxBEM_NF(GeomDomain& dom, GeomDomain& domN, GeomDomain& domF, Unknown& unk, Kernel& ker,const IntegrationMethods& ie,
                     const IntegrationMethods& ir, const Complex& a, bool precompute, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domp=&dom;
      gamma_N=&domN;
      gamma_F=&domF;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      dataBox.add(_id);       //say require u
      alpha=a;
      if(precompute)
        preCompute();
    }
    virtual ComputeBoxType type() const {return _BEM_NF;}
    virtual String typeName() const {return "BEM_NF";}
    virtual GeomDomain& domain() const {return *domp;}
    GeomDomain& domainN() const {return *gamma_N;}
    GeomDomain& domainF() const {return *gamma_F;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *vecup;}

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& u=*up, &v=*vp;
      BilinearForm auv= 0.5*intg(*domp,u*v)-intg(*domp,*domp,u*ndotgrad_y(G)*v,_method=*IMie)
                        - alpha*intg(*domp,*gamma_F,u*G*v,_method=*IMie);
      BilinearForm rhs=intg(*domp,u*v);
      K=TermMatrix(auv, _name="K");
      Krhs=TermMatrix(rhs, _name="Krhs");
      factorize(K,K);
      preComputed=true;
    }

    virtual void solve()
    {
      TermVector& ui=dataBox.begin()->second;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& u=*up, &v=*vp;
          BilinearForm auv= 0.5*intg(*domp,u*v)-intg(*domp,*domp,u*ndotgrad_y(G)*v,_method=*IMie)
                            - alpha*intg(*domp,*gamma_F,u*G*v,_method=*IMie);
          BilinearForm rhs=intg(*domp,u*v);
          K=TermMatrix(auv, _name="K");
          Krhs=TermMatrix(rhs, _name="Krhs");
          X=directSolve(K,-(Krhs*ui));
        }
      else
        X=factSolve(K,-(Krhs*ui));
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
//    DomUnkPair dup=std::make_pair(&repdom,&ur);
//    if(URep.find(dup)==URep.end())
//      {
//        Kernel& G=*Gp; Unknown& u=*up;
//        URep[dup]= integralRepresentation(ur,repdom,intg(*gamma_D,G*u,_method=*IMir))
//                   - integralRepresentation(ur,repdom,intg(*gamma_N,ndotgrad_y(G)*u,_method=*IMir));
//      }
//    return URep[dup]*X;
      Kernel& G=*Gp;
      Unknown& u=*up;
      return - alpha*integralRepresentation(ur,repdom,intg(*gamma_F,G*u,_method=*IMir),X|*gamma_F)
             - integralRepresentation(ur,repdom,intg(*domp,ndotgrad_y(G)*u,_method=*IMir),X);
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
//    DomUnkPair dup=std::make_pair(&repdom,&ur);
//    if(DnURep.find(dup)==DnURep.end())
//      {
//        Kernel& G=*Gp; Unknown& u=*up;
//        DnURep[dup]= integralRepresentation(ur,repdom,intg(*gamma_D,ndotgrad_x(G)*u,_method=*IMir))
//                     - integralRepresentation(ur,repdom,intg(*gamma_N,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir));
//      }
//    return (DnURep[dup]*X);
      Kernel& G=*Gp;
      Unknown& u=*up;
      return - alpha*integralRepresentation(ur,repdom,intg(*gamma_F,ndotgrad_x(G)*u,_method=*IMir),X|*gamma_F)
             - integralRepresentation(ur,repdom,intg(*domp,ndotgrad_x(ndotgrad_y(G))*u,_method=*IMir),X);
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& u=*up;
      return   - alpha*integralRepresentation(ur,repdom,intg(*gamma_F,grad_x(G)*u,_method=*IMir),X|*gamma_F)
               - integralRepresentation(ur,repdom,intg(*domp,grad_x(ndotgrad_y(G))*u,_method=*IMir),X);
    }
}; //end of ComputeBoxBEM_NF definition

//=====================================================================================================================
//  BEM with additional S around G, solve coupled integral equation
//   (nu.dn+mu)SL_S(uS) = (nu.dn+mu)ui sur S
//   dn( DL_G(uG)+lambda.SL_G(uG)+SL_S(uS) ) = 0 sur G
//=====================================================================================================================
class ComputeBoxBEMSigma: public ComputeBoxBEM
{
  public:
    Unknown *uGp, *vGp, *uSp, *vSp;
    GeomDomain *domGp, *domSp;                 //!< computation domains
    TermMatrix KSG, MSG;                       //!< BEM matrices
    std::map<DomUnkPair,TermMatrix> SRep;      //!< single layer matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> DRep;      //!< double layer matrix list indexed by (domain, unknown)
    std::map<DomUnkPair,TermMatrix> D2Rep;     //!< double double layer matrix list indexed by (domain, unknown)
    GeomDomain* interfacep;                    //!< domain where solution is cancelled (unused when 0)
    std::list<Dof*> exceptDofs;                //!< list of dofs on interface that are not cancelled (generally dofs at endpoints)
    bool dofCorrectionAllTime;                 //!< if true dof correction is applied to all iterations else only to the first iteration (default)
    Complex lambda_, mu_, nu_;               //!< complex coefficients involved in coupled BEM
    Real k;                                  //!< wave number
    ComputeBoxBEMSigma() : ComputeBoxBEM() {uGp=nullptr; vGp=nullptr; uSp=nullptr; vSp=nullptr; domGp=nullptr; domSp=nullptr; interfacep=nullptr; mu_=0., nu_=0., lambda_=0.;}
    ComputeBoxBEMSigma(GeomDomain& d, Unknown& ug, GeomDomain& ds, Unknown& us, Kernel& ker, const IntegrationMethods& ie, const IntegrationMethods& ir,
                       const Complex & l, const Complex& m, const Complex& n, const Real kin, bool precompute, const String& na="")
      : ComputeBoxBEM(ker,ie,ir,precompute,na)
    {
      domGp=&d;
      uGp=&ug;
      vGp=const_cast<Unknown*>(&uGp->dual());
      domSp=&ds;
      uSp=&us;
      vSp=const_cast<Unknown*>(&uSp->dual());
      interfacep=nullptr;
      lambda_=l;
      mu_=m;
      nu_=n;
      k=kin;
      if(mu_==Complex(0.))
        {
          if(nu_==Complex(0.))
            error("free_error"," in ComputeBoxBEMSigma construction, mu and nu cannot be both zero");
          dataBox.add(_ndotgrad);
        }
      else // mu !=0
        {
          if(nu_==Complex(0.))
            dataBox.add(_id);
          else
            dataBox.add(_ndotgrad,nu_,_id,mu_);
        }
      if(precompute)
        preCompute();
    }

    virtual ComputeBoxType type() const {return _BEM_sigma;}
    virtual String typeName() const {return "BEMSigma";}
    virtual GeomDomain& domain() const {return *domSp;}
    virtual Unknown& unknown() const {return *uSp;}
    virtual Complex mu() const {return mu_;}
    virtual Complex nu() const {return nu_;}
    virtual Complex lambda() const {return lambda_;}

    void setInterface(GeomDomain& interf, Dof* d1=nullptr, Dof* d2=nullptr, bool dcall = false)
    {
      interfacep=&interf;
      if(d1!=nullptr)
        exceptDofs.push_back(d1);
      if(d2!=nullptr)
        exceptDofs.push_back(d2);
      dofCorrectionAllTime = dcall;
    }

    virtual void preCompute()
    {
      Kernel& G=*Gp;
      Unknown& uG=*uGp, &vG=*vGp, &uS=*uSp, &vS=*vSp;
      GeomDomain& domG=*domGp, &domS=*domSp;
      Complex cz = Complex(0.);
      BilinearForm  bSG = -intg(domG,domG,ncrossgrad(uG)*G|ncrossgrad(vG),_method=*IMie)+(k*k)*intg(domG,domG,uG*(G*(_nx|_ny))*vG,_method=*IMie)-intg(domG,domS,uS*ndotgrad_x(G)*vG,_method=*IMie);
      if(lambda_!=cz)
        bSG+= -lambda_*intg(domG,domG,uG*ndotgrad_x(G)*vG,_method=*IMie)+0.5*lambda_*intg(domG,uG*vG);
      if(nu_!=cz)
        bSG-= 0.5*nu_*intg(domS, uS*vS)+nu_*intg(domS,domS,uS*ndotgrad_x(G)*vS,_method=*IMie);
      if(mu_!=cz)
        bSG-=  mu_*intg(domS,domS,uS*G*vS,_method=*IMie);
      BilinearForm brhsSG = intg(domS,uS*vS);
      KSG=TermMatrix(bSG, _name="KSG");
      MSG=TermMatrix(brhsSG, _name="MSG");
      factorize(KSG,KSG);
      preComputed=true;
    }
    virtual void solve()
    {
      TermVector& dfui=dataBox.begin()->second;
      if(!preComputed)
        {
          Kernel& G=*Gp;
          Unknown& uG=*uGp, &vG=*vGp, &uS=*uSp, &vS=*vSp;
          GeomDomain& domG=*domGp, &domS=*domSp;
          BilinearForm  bSG = -intg(domG,domG,ncrossgrad(uG)*G|ncrossgrad(vG),_method=*IMie)+(k*k)*intg(domG,domG,uG*(G*(_nx|_ny))*vG,_method=*IMie)-intg(domG,domS,uS*ndotgrad_x(G)*vG,_method=*IMie);
          if(lambda_!=0)
            bSG+= -lambda_*intg(domG,domG,uG*ndotgrad_x(G)*vG,_method=*IMie)+0.5*lambda_*intg(domG,uG*vG);
          if(nu_!=0)
            bSG-= 0.5*nu_*intg(domS, uS*vS)+nu_*intg(domS,domS,uS*ndotgrad_x(G)*vS,_method=*IMie);
          if(mu_!=0)
            bSG-=  mu_*intg(domS,domS,uS*G*vS,_method=*IMie);
          BilinearForm brhsSG = intg(domS,uS*vS);
          KSG=TermMatrix(bSG, _name="KSG");
          MSG=TermMatrix(brhsSG, _name="MSG");
          X=directSolve(KSG,MSG*dfui);
        }
      else
        X=factSolve(KSG,MSG*dfui);
      if(interfacep!=nullptr)
        {
          if(iter==1 || dofCorrectionAllTime)
            correctInterface(X,*interfacep,exceptDofs);
          else
            correctInterface(X,*interfacep);
        }
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& uG=*uGp;
      GeomDomain& domG=*domGp;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
        DRep[dup]=integralRepresentation(ur,repdom,intg(domG,ndotgrad_y(G)*uG,_method=*IMir));
      if(lambda_!=0 && SRep.find(dup)==SRep.end())
        SRep[dup]=integralRepresentation(ur,repdom,intg(domG,G*uG,_method=*IMir));
      if(lambda_==0.)
        return DRep[dup]*X(uG);
      return (DRep[dup]*X(uG)-lambda_*SRep[dup]*X(uG));
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& uG=*uGp;
      GeomDomain& domG=*domGp;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(lambda_!=0 && DRep.find(dup)==DRep.end())
        DRep[dup]=integralRepresentation(ur,repdom,intg(domG,ndotgrad_y(G)*uG,_method=*IMir));
      if(D2Rep.find(dup)==D2Rep.end())
        D2Rep[dup]=integralRepresentation(ur,repdom,intg(domG,ndotgrad_x(ndotgrad_y(G))*uG,_method=*IMir));
      if(lambda_==0)
        return D2Rep[dup]*X(uG);
      return (D2Rep[dup]*X(uG)-lambda_*DRep[dup]*X(uG));
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      Kernel& G=*Gp;
      Unknown& uG=*uGp;
      GeomDomain& domG=*domGp;
      TermVector res = integralRepresentation(ur,repdom,intg(domG,grad_x(ndotgrad_y(G))*uG,_method=*IMir),X(uG));
      if(lambda_!=Complex(0.))
        res -= lambda_*integralRepresentation(ur,repdom,intg(domG,grad_x(G)*uG,_method=*IMir),X(uG));
      return res;
    }

}; //end of ComputeBoxBEMSigma definition

//=====================================================================================================================
// Kirchoff approximation stuff
//=====================================================================================================================
// shadow coloring rule of an element, one of the dof belongs to the shadow zone (imag(grad_u/u) | n) > 0 <=> im(dn_u/u)>0
Real shadowColoringRule(const GeomElement& gelt, const std::vector<Vector<Real> >& val)
{
  GeomMapData *gmap=gelt.meshElement()->geomMapData_p;
  if(gmap==nullptr)
    gmap = new GeomMapData(gelt.meshElement());
  if(gmap->normalVector.size()==0)
    gmap->computeOrientedNormal();
  Real res = 0.;
  for(Number i=0; i<val.size() && res==0; i++)
    {
      if(dot(val[i],gmap->normalVector)>-theTolerance)
        res+=-1.;
    }
  return res;
}

// find shadow/light dofs (terminators) from u and grad u on a side domain
//   dofs where the color is -1, dof colors are built from elt colors (see shadowColoringRule function)
//                              shadow/light
//   dof colors            -2        -1          0
//                 ---------|---------|----------|----------
//   elt colors        -1       -1         0         0
//
// update shadow/light dof, normals and phaseGradient (ray direction) at these dofs
void buildShadowLightDofs(const TermVector& U, const TermVector& gradU, Real k, std::vector<const Dof*>& shadowLightDofs, std::vector<Int>& colors,
                          std::vector<Vector<Real> >& normals, std::vector<Real>& phase, std::vector<Vector<Real> >& phaseGrad)
{
  //update shadow/light dofs
  Space& V = *U.unknown()->space();
  colors.resize(V.nbDofs());
  std::fill(colors.begin(),colors.end(),0);
  for(Number j=0; j<V.nbOfElements(); j++) // built dof colors
    {
      const Element* elt=V.element_p(j);
      Real color=elt->geomElt_p->color;
      const std::vector<Number>& dofNum=elt->dofNumbers;
      std::vector<Number>::const_iterator itn = dofNum.begin();
      for(; itn!=dofNum.end(); ++itn)
        colors[*itn-1]+=color;
    }
  shadowLightDofs.clear();
  normals.clear();
  phaseGrad.clear();
  std::vector<Point> points;
  for(Number j=0; j<colors.size(); j++) // locate shadow/light dofs
    {
      if(colors[j]==-1)
        {
          const Dof& dof=V.dof(j+1);
          shadowLightDofs.push_back(&dof);
          points.push_back(dof.coords());
          phaseGrad.push_back(imag(gradU.getValue(dof).asComplexVector()/(k*U.getValue(dof).asComplex())));
          phase.push_back(arg(U.getValue(dof).asComplex())/k);
        }
    }
  normals = computeNormalsAt(*V.domain(), points);
}

//!< structure to store data related to a wedge
class WedgeData
{
public:
    Point wedge;             // wedge vertex
    Real wedgeAngle;       // wedge angle
    Vector<Real> wedgeDir; // wedge direction (opposite to the wedge)
    const Dof* wedgeDof;     // pointer to wedge vertex dof
    WedgeData(const Dof* dof, Real angle, const Vector<Real>& dir)
        : wedgeDof(dof), wedgeAngle(angle), wedgeDir(dir) {wedge=dof->coords();}
};

// find shadow/light dofs (terminators) from u and grad u on a side domain
//   dofs where the color is -1, dof colors are built from elt colors (see shadowColoringRule function)
//                              shadow/light
//   dof colors            -2        -1          0
//                 ---------|---------|----------|----------
//   elt colors        -1       -1         0         0
//
// and wedge dofs characterized by n1.n2 >= 1-a (n1 and n2 normals to elements sharing dofs)
// choosing a=0 inhibit the wedge detection !
// note that a wedge dof cannot be a shadow/light dof
// update shadow/light dof, normals and phaseGradient (ray direction) at these dofs
// DESIGNED FOR CURVE
void buildSingularDofs(const TermVector& U, const TermVector& gradU, Real k, std::vector<const Dof*>& shadowLightDofs, std::vector<Int>& colors,
                       std::vector<Vector<Real> >& normals, std::vector<Real>& phase, std::vector<Vector<Real> >& phaseGrad,
                       Real a, std::vector<WedgeData>& wedges)
{
  //update shadow/light dofs
  Space& V = *U.unknown()->space();
  Number nbd=V.nbDofs();
  std::vector<std::pair< Vector<Real>,Vector<Real> > > nsd(nbd, std::make_pair(Vector<Real>(0),Vector<Real>(0)));
  colors.resize(nbd);
  std::fill(colors.begin(),colors.end(),0);
  for(Number j=0; j<V.nbOfElements(); j++) // built dof colors
    {
      const Element* elt=V.element_p(j);
      Real color=elt->geomElt_p->color;
      const std::vector<Number>& dofNum=elt->dofNumbers;
      std::vector<Number>::const_iterator itn = dofNum.begin();
      for(; itn!=dofNum.end(); ++itn)
        {
            Number n=*itn-1;
            colors[n]+=color;
            if(nsd[n].first.size()==0) nsd[n].first= elt->geomElt_p->meshElement()->geomMapData_p->normalVector;
            else nsd[n].second= elt->geomElt_p->meshElement()->geomMapData_p->normalVector;
        }
    }
  //locate wedge dofs
  wedges.clear();
  std::vector<std::pair< Vector<Real>,Vector<Real> > >::iterator itn=nsd.begin();
  for(Number j=0; j<nsd.size(); j++)
  {
      Vector<Real>& n1=nsd[j].first, n2=nsd[j].second;
      if(n1.size()>0 && n2.size()>0)
      {
          Real ps=xlifepp::dot(n1,n2);
          if(ps < 1- a) wedges.push_back(WedgeData(&V.dof(j+1),(pi_+std::acos(ps))/2,(n1+n2)/std::sqrt(2*(1+ps))));
      }
  }
  // locate shadow/light dofs
  shadowLightDofs.clear();
  normals.clear();
  phaseGrad.clear();
  std::vector<Point> points;
  for(Number j=0; j<colors.size(); j++)
    {
      if(colors[j]==-1)
        {
          const Dof& dof=V.dof(j+1);
          bool now=true;
          std::vector<WedgeData>::iterator itw=wedges.begin();
          for(;itw!=wedges.end() && now; ++itw) now=(&dof!=itw->wedgeDof);
          if(now)
          {
              shadowLightDofs.push_back(&dof);
              points.push_back(dof.coords());
              phaseGrad.push_back(imag(gradU.getValue(dof).asComplexVector()/(k*U.getValue(dof).asComplex())));
              phase.push_back(arg(U.getValue(dof).asComplex())/k);
          }
        }
    }
  normals = computeNormalsAt(*V.domain(), points);
}

Complex shadowLightCorrection(const Point& p, Parameters& pa = defaultParameters)
{
  const std::vector<const Dof*>& shadowLightDofs =*static_cast<const std::vector<const Dof* >* >(pa("shadowLightDofs").p_);
  const std::vector<Vector<Real> >& normals=*static_cast<const std::vector<Vector<Real> >* >(pa("normals").p_);
  const std::vector<Real>& phase =*static_cast<const std::vector<Real>* >(pa("phases").p_);
  const std::vector<Vector<Real> >& phaseGrad=*static_cast<const std::vector< Vector<Real> >* >(pa("phaseGrads").p_);
  Real k=real(pa("k"));
  Complex res(0.,0.);
  for(int j=0; j<shadowLightDofs.size(); j++)
    {
      Point y=shadowLightDofs[j]->coords();           // shadow/light point
      const Vector<Real>& grad=phaseGrad[j];        // incident direction
      Vector<Real> i=grad/norm(grad);               // normalized incident direction
      const Vector<Real>& n= normals[j];            // outward normal vector
      Real lx=y.distance(p);                        // lx =|y-p|
      const Vector<Real> t = (p-y)/lx;              // observation direction
      Vector<Real> s(2);
      s[0]=-n[1];
      s[1]=n[0];     // tangential vector
      Real psix = phase[j] + lx;
      Real tmp=dot(i-t,s);
      if(std::abs(tmp)>theEpsilon)
        res+=dot(s,i)*dot(t,n)*std::exp(k*i_*psix)/(tmp*std::sqrt(lx));
    }
  res*=std::exp(0.25*pi_*i_)/std::sqrt(2.*pi_*k);
  return res;
}

TermVector colors(const GeomDomain& dom)  //produce the colors of domain element as a P0 TermVector
{
  Space* V=Space::findSpace("P0@");
  Unknown* u=Unknown::findUnknown("uP0@");
  if(V==nullptr)
    V=new Space(_domain=dom, _interpolation=P0, _name="P0@");
  if(u==nullptr)
    u=new Unknown(*V, _name="uP0@");
  TermVector cols(*u,dom,0.);
  for(Number k = 0; k < V->nbOfElements(); k++) // data related to u
    {
      const Element* elt = V->element_p(k);
      GeomElement* gelt = elt->geomElt_p;
      const std::vector<Number>& dofNum = V->elementDofs(k);
      cols.setValue(dofNum[0],gelt->color);
    }
  return cols;
}


//=====================================================================================================================
// computeBoxKirchoff class implementing Kirchoff method
// 2.*intg_dom ndotgrad_y(G)*ui + correction on shadow-light points
//=====================================================================================================================
class ComputeBoxKirchoff: public ComputeBox
{
  public:
    GeomDomain *domp;                          //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                          //!< unknown related to the ComputeBox (should not be 0!)
    Kernel *Gp;                                //!< pointer to kernel
    const IntegrationMethods *IMir;            //!< integration methods for integral representations
    Real k;                                  //!< wave number
    std::map<DomUnkPair,TermMatrix> DRep;      //!< double layer matrix list indexed by (domain, unknown)
    Number nbOfCorrection;                   //!< number of correction at shadow/light points (0:no correction, 1:at first iteration, ...)
    std::vector<const Dof*> shadowLightDofs;   //!< list of shadow/light dofs
    std::vector<Vector<Real> > normals;      //!< normals at shadow/light dofs
    std::vector<Real> phase;                 //!< phase at shadow/light dofs
    std::vector<Vector<Real> > phaseGrad;    //!< phase gradient (ray direction) at shadow/light dofs
    Function shadowLightCorrection_;           //!< link to real function shadowLightCorrection
    Parameters pars_;                          //!< parameters associated to real function shadowLightCorrection
    GeomDomain* obsp;                          //!< if required, the observation domain
    Unknown* uobsp;                            //!< if required, the unknown related to the observation domain
    TermVector uobs;                           //!< if required, the diffracted field on the observation domain (additive)

    ComputeBoxKirchoff()  {domp=nullptr; up=nullptr; vp=nullptr; Gp=nullptr; IMir=nullptr; nbOfCorrection=0; obsp=nullptr; uobsp=nullptr;}
    ComputeBoxKirchoff(GeomDomain& dom, Unknown& unk, Kernel& ker, const IntegrationMethods& ir, Real kin, Number nc, const String& na="")
    {
      name=na;
      domp=&dom;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      Gp=&ker;
      IMir=&ir;
      k=kin;
      nbOfCorrection=nc;
      obsp=nullptr;
      uobsp=nullptr;
      dataBox.add(_id);
      dataBox.add(_grad);
      pars_<<Parameter(k,"k");
      pars_<<Parameter(static_cast<const void*>(&shadowLightDofs), "shadowLightDofs");
      pars_<<Parameter(static_cast<const void*>(&normals), "normals");
      pars_<<Parameter(static_cast<const void*>(&phase), "phases");
      pars_<<Parameter(static_cast<const void*>(&phaseGrad), "phaseGrads");
      shadowLightCorrection_=Function(shadowLightCorrection,pars_);
    }
    virtual ComputeBoxType type() const {return _Kirchoff;}
    virtual String typeName() const {return "Kirchoff";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *const_cast<Unknown*>((++dataBox.begin())->second.unknown());}
    virtual bool hasComputedField() const {return obsp!=nullptr;}
    virtual const TermVector& computedField() const
    {
      if(obsp!=nullptr)
        return uobs;
      error("free_error","no computed field in ComputeBox "+name);
      return *new TermVector();
    }
    void restrictUiToLightZone()
    {
      TermVector& ui=dataBox.begin()->second;
      Space* V = up->space();
      for(Number j=0; j<V->nbOfElements(); j++) // built dof colors
        {
          const Element* elt=V->element_p(j);
          if(elt->geomElt_p->color!=0)
            {
              const std::vector<Number>& dofNum=elt->dofNumbers;
              std::vector<Number>::const_iterator itn = dofNum.begin();
              for(; itn!=dofNum.end(); ++itn)
                ui.setValue(*itn,0.);
            }
        }
    }

    virtual void solve()
    {
      //update shadow element colors
      TermVector& ui = dataBox.begin()->second;
      TermVector& gradui = (++dataBox.begin())->second;
      TermVector gradsU =((gradui/ui).toImag())/k;
      //saveToFile("gradsU_"+tostring(iter), gradsU, _format=_vtk);
      setColor(*domp, gradsU, shadowColoringRule);
      //saveToFile("colors_"+tostring(iter), colors(*domp), _format=_vtu);
      std::vector<Int> color;
      if (iter<=nbOfCorrection) buildShadowLightDofs(ui, gradui, k, shadowLightDofs, color, normals, phase, phaseGrad);
      restrictUiToLightZone();
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      TermVector cor(ur, repdom, shadowLightCorrection_);
      if(&repdom==obsp)
        return uobs; //special case
      TermVector& ui = dataBox.begin()->second;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
        DRep[dup]=2.*integralRepresentation(ur,repdom,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
      TermVector res=DRep[dup]*ui;
      //Real amp=std::sqrt(k*pi_*norm(cor.subVector().dof(1).coords())/2);
      //saveFarField(res,amp,"far_nocorrection.dat");
      if(iter<=nbOfCorrection)
        {
          TermVector cor(ur, repdom, shadowLightCorrection_);
          //saveFarField(cor,amp,"far_correction.dat");
          res+=cor;
        }
      if(obsp!=nullptr) //update field on observation domain
        {
          dup=std::make_pair(obsp,uobsp);
          if(DRep.find(dup)==DRep.end())
            DRep[dup]=2.*integralRepresentation(*uobsp,*obsp,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
          uobs+=DRep[dup]*ui;
          if(iter<=nbOfCorrection)
            uobs+= TermVector(*uobsp, *obsp, shadowLightCorrection_);
        }
      return res;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxKirchoff does not yet propose integral representation of Dn");
      return TermVector(); //fake return
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxComputeBoxKirchoff does not yet propose integral representation of gradient");
      return TermVector(); //fake return
    }
};

/* temporary tools related to the parametrization of an ellipse
    nu=0 : x(t) = x0 + ae*R*cos(t), y(t) = y0 + be*R*sin(t) with t in [-pi,pi]

   |cos(v) -sin(v)| |x0 + ae*R*cos(t)|   x(t)=cos(v)(x0 + ae*R*cos(t))-sin(v)(y0 + be*R*sin(t))
   |sin(v)  cos(v)| |y0 + be*R*sin(t)|   y(t)=sin(v)(x0 + ae*R*cos(t))+cos(v)(y0 + be*R*sin(t))

*/
//Real R = 2*pi_;
//Point C(0.,0.);
//Real ae = 1, be=1; // ellipse lengths
//Real nue=0;      // rotation angle of the ellipse
//
//Real toParameter(const Point& P)
//{
//  if(nue==0.) return std::atan2((P(2)-C(2))/be,(P(1)-C(1))/ae);
//  Real x=cos(nue)*P(1)+sin(nue)*P(2), y=-sin(nue)*P(1)+cos(nue)*P(2);
//  return std::atan2((y-C(2))/be,(x-C(1))/ae);
//}
//
//Real flength(Real t)
//{
//  return R*std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
//}
//
//Real fcurvature(Real t)
//{
//  return ae*be*std::pow(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t),-1.5)/R;
//}
//
//Vector<Real> f_normal(Real t)
//{
//    Vector<Real> n(2);
//    Real l=std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
//    n[0]=be*cos(t)/l;
//    n[1]=ae*sin(t)/l;
//    if(nue==0.) return n;
//    Real nx=n[0],ny=n[1];
//    n[0]=cos(nue)*nx-sin(nue)*ny;
//    n[1]=sin(nue)*nx+cos(nue)*ny;
//    return n;
//}
//
//class curvatureData
//{
//  public:
//    Real abcissa;
//    Real curvature;
//    Real length;
//    Complex psi;
//    Vector<Real> normal;
//    curvatureData(Real a=0., Real c=0., Real l=0., const Complex& p=0., const Vector<Real>& n =Vector<Real>()) : abcissa(a), curvature(c), length(l), psi(p), normal(n) {}
//};


class CurvatureData //! additional data at a point of a parametrized boundary
{
  public:
    Real t;               //!< parameter
    Real abcissa;         //!< curvilinear abcissa
    Real curvature;       //!< curvature
    Real dscurvature;     //!< derivative/s of curvature
    Real length;          //!< local length
    Vector<Complex> psi;  //!< 2^(-1/3)int_0^s c(t)^(2/3)dt,
    Vector<Real> normal;  //!< normal vector
    Vector<Real> tangent; //!< tangent vector
    Number ndof;          //!< dof number
    CurvatureData(Real tt=0., Real a=0., Real c=0., Real dc=0., Real l=0., const Vector<Complex>& vp=Vector<Complex>(),
                  const Vector<Real>& vn =Vector<Real>(), const Vector<Real>& vt =Vector<Real>(),Number nd=0)
      : t(tt), abcissa(a), curvature(c), dscurvature(dc), length(l), psi(vp), normal(vn), tangent(vt), ndof(nd) {}
    friend ostream& operator<<(ostream& out,const CurvatureData& cd)
    {
        out<<"t="<<cd.t<<" curv. abc="<<cd.abcissa<<" curvature="<<cd.curvature<< " dscurvature="<<cd.dscurvature<<" length="<<cd.length
           <<" psi="<<cd.psi<<" normal="<<cd.normal<<" tangent="<<cd.tangent<<" dof "<<cd.ndof;
        return out;
    }
};

/* tools related to the parametrization of a Rosillo's curve
    nu=0 : x(t) = ae*cos(t)+C(0), y(t) = C(1)+ae*sin(t)*(be-ae*cos(t) )/( ce-ae*cos(t)) with t=(2s-1)pi, s in [0,1]

   |cos(v) -sin(v)| |x(t)|   =cos(v)x(t)-sin(v)y(t)
   |sin(v)  cos(v)| |y(t)|   =sin(v)x(t)+cos(v)y(t)

*/
Real R = 1;                 // rosillo radius
Point C(0.,0.);               // rosillo center
Real ae = 1, be=1.5, ce=5;  // rosillo parameters
Real nue=0;                 // rotation angle

Vector<Real> rosilloParametrization(const Point& P, Parameters& pars, DiffOpType d=_id)
{
    Real t=(2*P(1)-1)*pi_, s=sin(t), c=cos(t), v=ce-ae*c, dpi=2*pi_;
    Real x, y;
    switch(d)
    {
      case  _id : x=ae*c+C(1); y = ae*s*(be-ae*c)/(ce-ae*c)+C(2); break;
      case  _d1 : x=-dpi*ae*s; y= dpi*ae*(c*(be-ae*c)/v + ae*s*s*(ce-be)/(v*v)); break;
      case _d11 : x=-dpi*dpi*ae*c; y=dpi*dpi*ae*((ae*c-be)*s/v + 3*ae*s*c*(ce-be)/(v*v) -2*dpi*dpi*ae*ae*s*s*s*(ce-be)/(v*v*v)); break;
      default: error("free_error", "derivatives not handled in rosilloParametrization");
    }
    if(nue==0) return Point(x,y);
    return Point(cos(nue)*x-sin(nue)*y,sin(nue)*x+cos(nue)*y);
}
Vector<Real> rosilloToParameter(const Point& P, Parameters& pars, DiffOpType d=_id)
{
  Real x=P(1)-C(1), y=P(2)-C(2);
  if(nue!=0.) {x=cos(nue)*P(1)+sin(nue)*P(2); y=-sin(nue)*P(1)+cos(nue)*P(2);}
  Real t=std::acos(x/ae);
  if(y>=0) return Vector<Real>(1,(t+pi_)/(2*pi_));
  return Vector<Real>(1,(pi_-t)/(2*pi_));
}

Vector<Real>  rosilloLength(const Point& P, Parameters& pars, DiffOpType d=_id)
{
  Real t=(2*P(1)-1)*pi_, s=sin(t), c=cos(t), dpi=2*pi_;
  Real xp=-dpi*ae*s;
  Real yp= ae*dpi*(c*(be-ae*c)/(ce-ae*c)+ae*s*s*(ce-be)/((ce-ae*c)*(ce-ae*c)));
  return Vector<Real>(1,sqrt(xp*xp+yp*yp));
}

Vector<Real> rosilloCurvature(const Point& P, Parameters& pars, DiffOpType d=_id)
{
  Real t=(2*P(1)-1)*pi_, s=sin(t), c=cos(t), dpi=2*pi_;
  Real xp=-dpi*ae*s, xpp=-dpi*dpi*ae*c;
  Real v=(ce-ae*c);
  Real yp= dpi*ae*(c*(be-ae*c)/v + ae*s*s*(ce-be)/(v*v)),
         ypp=dpi*dpi*ae*((ae*c-be)*s/v + 3*ae*s*c*(ce-be)/(v*v) -2*dpi*dpi*ae*ae*s*s*s*(ce-be)/(v*v*v));
  return Vector<Real>(1,(xp*ypp-yp*xpp)/std::pow(xp*xp+yp*yp,1.5));
}

Vector<Real> rosilloNormal(const Point& P, Parameters& pars, DiffOpType d=_id)
{
    Vector<Real> n(2);
    Real t=(2*P(1)-1)*pi_, s=sin(t), c=cos(t), v=(ce-ae*c), dpi=2*pi_;
    Real xp=-dpi*ae*s, yp= dpi*ae*(c*(be-ae*c)/v + ae*s*s*(ce-be)/(v*v));
    Real l=sqrt(xp*xp+yp*yp);
    n[0]= yp/l;
    n[1]=-xp/l;
    if(nue==0.) return n;
    Real nx=n[0],ny=n[1];
    n[0]=cos(nue)*nx-sin(nue)*ny;
    n[1]=sin(nue)*nx+cos(nue)*ny;
    return n;
}

Point(*parametrization)(Real);
Real (*toParameter)(const Point&);
Real (*flength)(Real);
Real (*fcurvature)(Real);
Vector<Real> (*fnormal)(Real);
Vector<Real> (*ftangent)(Real);

Point ellipse_parametrization(Real t)
{
  Real s=sin(t), c=cos(t);
  Real x=ae*R*c, y = ae*R*s;
  if(nue==0.) return Point(x,y);
  return Point(cos(nue)*x-sin(nue)*y,sin(nue)*x+cos(nue)*y);
}

Real ellipse_toParameter(const Point& P)
{
  if(nue==0.) return std::atan2((P(2)-C(2))/be,(P(1)-C(1))/ae);
  Real x=cos(nue)*P(1)+sin(nue)*P(2), y=-sin(nue)*P(1)+cos(nue)*P(2);
  return std::atan2((y-C(2))/be,(x-C(1))/ae);
}

Real ellipse_flength(Real t)
{
  return R*std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
}

Real ellipse_fcurvature(Real t)
{
  return ae*be*std::pow(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t),-1.5)/R;
}

Vector<Real> ellipse_ftangent(Real t)
{
  Vector<Real> n(2);
  Real l=std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
  n[0]=-ae*sin(t)/l;
  n[1]= be*cos(t)/l;
  if(nue==0.) return n;
  Real nx=n[0],ny=n[1];
  n[0]=cos(nue)*nx-sin(nue)*ny;
  n[1]=sin(nue)*nx+cos(nue)*ny;
  return n;
}

Vector<Real> ellipse_fnormal(Real t)
{
  Vector<Real> n(2);
  Real l=std::sqrt(ae*ae*sin(t)*sin(t)+be*be*cos(t)*cos(t));
  n[0]=be*cos(t)/l;
  n[1]=ae*sin(t)/l;
  if(nue==0.)
    return n;
  Real nx=n[0],ny=n[1];
  n[0]=cos(nue)*nx-sin(nue)*ny;
  n[1]=sin(nue)*nx+cos(nue)*ny;
  return n;
}

//=====================================================================================================================
// computeBoxUTD class implementing Kirchoff method with Uniform Theory of Diffraction for 2D acoustic Neumann problem
//   light zone  (s<s1) : 2.*intg_dom ndotgrad_y(G)*ui
//   shadow zone (s>s2) : creeping ray from UTD
//   light/shadow zone (s1<s<s2): Fock function
//=====================================================================================================================
class ComputeBoxUTD: public ComputeBox
{
  public:
    GeomDomain *domp;                          //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                          //!< unknown related to the ComputeBox (should not be 0!)
    Kernel *Gp;                                //!< pointer to kernel
    const IntegrationMethods *IMir;            //!< integration methods for integral representations
    Real k;                                  //!< wave number
    Real s1,s2;                              //!< fock zone bounds (Fock coordinate)
    Real sc;                                 //!< limit of creeping waves (>s2) (curvilinear coordinate)
    Real ds;                                 //!< width of transition area Fock-Kirchoff : s1-ds < s < s1+ds (Fock coordinate)
    bool useIN;                                //!< use (I|n) in Fock scaling
    Fock fock;                                 //!< Fock function object
    std::map<DomUnkPair,TermMatrix> DRep;      //!< double layer matrix list indexed by (domain, unknown)
    Number nbOfCorrection;                   //!< number of correction at shadow/light points (0:no correction, 1:at first iteration, ...)
    std::vector<const Dof*> shadowLightDofs;   //!< list of shadow/light dofs
    std::vector<Vector<Real> > normals;      //!< normals at shadow/light dofs
    std::vector<Real> phase;                 //!< phase at shadow/light dofs
    std::vector<Vector<Real> > phaseGrad;    //!< phase gradient (ray direction) at shadow/light dofs
    GeomDomain* obsp;                          //!< if required, the observation domain
    Unknown* uobsp;                            //!< if required, the unknown related to the observation domain
    TermVector uobs;                           //!< if required, the diffracted field on the observation domain (additive)

    ComputeBoxUTD()  {domp=nullptr; up=nullptr; vp=nullptr; Gp=nullptr; IMir=nullptr, nbOfCorrection=0; obsp=nullptr; uobsp=nullptr; s1=0; s2=0.; sc=0.; ds=0.;useIN=false;}
    ComputeBoxUTD(GeomDomain& dom, Unknown& unk, Kernel& ker, const IntegrationMethods& ir, Real kin, Real si1, Real si2, Real sic, Real dsi, bool ini, const String& na="")
    {
      name=na;
      domp=&dom;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      Gp=&ker;
      IMir=&ir;
      k=kin;
      s1=si1;
      s2=si2;
      sc=sic;
      ds=dsi;
      useIN=ini;
      fock = Fock(0,1,200);
      obsp=nullptr;
      uobsp=nullptr;
      dataBox.add(_id);
      dataBox.add(_grad);
    }
    virtual ComputeBoxType type() const {return _UTD;}
    virtual String typeName() const {return "UTD";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *const_cast<Unknown*>((++dataBox.begin())->second.unknown());}
    virtual bool hasComputedField() const {return obsp!=nullptr;}
    virtual const TermVector& computedField() const
    {
      if(obsp!=nullptr)
        return uobs;
      error("free_error","no computed field in ComputeBox "+name);
      return *new TermVector();
    }

    virtual void solve()
    {
      //update shadow element colors
      TermVector& ui = dataBox.begin()->second;
      TermVector& gradui = (++dataBox.begin())->second;
      TermVector gradsU =((gradui/ui).toImag())/k;
      setColor(*domp, gradsU, shadowColoringRule);
      std::vector<Int> color;
      buildShadowLightDofs(ui, gradui, k, shadowLightDofs, color, normals, phase, phaseGrad);
      Number ns=shadowLightDofs.size();

      //compute curvilinear coordinates (only P1 Finite Element)
      std::vector<Int> orient(ns);   // for each shadowLightDof, orientation of the light zone (s>s0 :1, s<s0 :-1, unset:0)
      Space* V = up->space();
      std::map<const FeDof*,CurvatureData> curvabc;
      const FeDof* d0 = static_cast<const FeDof*>(*shadowLightDofs.begin()), * dp=d0, *dc=nullptr;
      std::vector<std::pair<Element*, Number> >::const_iterator iti = d0->elements().begin();
      //Real tp=toParameter(dp->coords()), tc, cp=fcurvature(tp), cc, lp=flength(tp), lc;
      const Parametrization& par=domp->parametrization();
      Real tp=par.toParameter(dp->coords())[0], tc, cp=par.curvature(tp), cc, lp=par.length(tp), lc;
      Real s=0, h;
      Complex psi=0.;
      Complex xi=std::conj(fock.poles[0])*std::pow(2,-1./3.);
      curvabc[dp] = CurvatureData(tp,s,cp,0.,lp,Vector<Complex>(1,psi),fnormal(tp), ftangent(tp), 0);
      Element * elt;
      Number nd;
      while(dc!=d0)
        {
          elt = iti->first;;
          nd = iti->second;
          dc = &elt->feSpaceP()->fedof(elt->dofNumbers[nd%2]);
//          tc = toParameter(dc->coords());
//          cc = fcurvature(tc);
//          lc=flength(tc);
          tc = par.toParameter(dc->coords())[0];
          cc = par.curvature(tc);
          lc = par.length(tc);
          if(std::abs(tc-tp)>pi_)  tc-=2*pi_;
          h=0.5*std::abs(tc-tp)*(lp+lc);   //approximation of the arc length
          s+=h;
          Real dsc=(cc-cp)/h;       //approx. of the s derivative of the curvature
          psi+=xi*h*0.5*(std::pow(cp,2./3.)+std::pow(cc,2./3.));
          for(Number j=0; j<ns; j++)
            if(orient[j]==0 && dp==static_cast<const FeDof*>(shadowLightDofs[j]))
              {
                if(elt->geomElt_p->color!=0) orient[j]=-1; else orient[j]=1;
              }
          iti = dc->elements().begin();
          if(iti->first==elt) iti++;
          dp=dc; cp=cc; tp=tc;lp=lc;
          if(dc!=d0) curvabc[dc] = CurvatureData(tp,s,cp,dsc,lp,Vector<Complex>(1,psi),fnormal(tp),ftangent(tp), 0); //curvatureData(s,cc,lc,psi,f_normal(tc));
        }

//      std::map<const FeDof*,CurvatureData>::iterator itc=curvabc.begin();
//      ofstream fout("curvdata.dat");
//      for(;itc!=curvabc.end();++itc)
//        {
//            Point pd=itc->first->coords();
//            CurvatureData& cd=itc->second;
//            theCout<<" dof "<<pd<<" curdata : "<<cd<<eol;
//            fout<<pd(1)<<" "<<pd(2)<<" "<<" "<<cd.t<<" "<<cd.abcissa<<" "<<cd.curvature<<" "<<cd.length<<" "<<cd.normal[0]<<" "<<cd.normal[1]<<eol;
//        }
//      theCout<<std::flush;
//      fout.close();

      Real st = s;
      Complex pt = psi;
      std::vector<Complex> vis(ns);
      for(Number j=0; j<ns; j++) vis[j]= ui.getValue(*shadowLightDofs[j]).asComplex();

      //compute field using Kirchoff-Fock and creaping waves
      std::map<const FeDof*,CurvatureData>::iterator itm;
      TermVector ui2 = TermVector(ui);
      std::vector<Number> dofStatus(V->dimSpace(),0); // 0 : not computed, 1: Kirchoff, 2: Shadow, 3: Fock
      Complex cte =2*i_*sqrtOfpi_/std::conj(fock.vpoles[0]);
      for(Number j=0; j<V->nbOfElements(); j++) //loop on elements
        {
          const Element* elt=V->element_p(j);
          bool shadow =  elt->geomElt_p->color!=0;
          const std::vector<Number>& dofNum=elt->dofNumbers;
          std::vector<Number>::const_iterator itn=dofNum.begin();
          for(itn = dofNum.begin(); itn!=dofNum.end(); ++itn)
            {
              Number n=*itn;
              if(dofStatus[n-1]==0)
                {
                  Complex vi=ui2.getValue(n).asComplex();
                  itm = curvabc.find(static_cast<const FeDof*>(&elt->feSpaceP()->dof(n)));
                  if(itm==curvabc.end()) error("free_error","abnormal failure in ComputeBoxUTD::solve");
                  Real s = itm->second.abcissa, c = itm->second.curvature, l=itm->second.length;
                  if(std::abs(c)<theTolerance) c=theTolerance;  // to avoid 0 curvature
                  Complex psi = itm->second.psi[0];
                  Vector<Real>& normal = itm->second.normal;
                  for(Number j=0; j<ns; j++) // analyze distance to shadow/light point
                    {
                      itm = curvabc.find(static_cast<const FeDof*>(shadowLightDofs[j]));
                      if(itm==curvabc.end()) error("free_error","abnormal failure in ComputeBoxUTD::solve");
                      Real c0 = itm->second.curvature;
                      if(std::abs(c0)<theTolerance) c0=theTolerance;  // to avoid 0 curvature
                      Real ss = s-itm->second.abcissa;
                      Complex ps = psi-itm->second.psi[0];
                      if(ss >  0.5*st) {ss-=st; ps-=pt;}
                      if(ss < -0.5*st) {ss+=st; ps+=pt;}
                      if((shadow && ss<0)|| (!shadow && ss>0)) {ss=-ss; ps=-ps;}
                      Complex v=0.;
                      Real isn;
                      Real sb=std::pow(0.5*k*c*c,1./3.)*ss;
                      if(useIN && ss<=0 && ss>-0.25*st)  //in light region, compute sb using I|n
                      {
                         Vector<Complex> gv=((++dataBox.begin())->second.getValue(n).asComplexVector()/(k*vi));
                         isn=dot(imag(gv),normal);
                         sb = std::pow(0.5*k/c,1./3.)*isn;
                      }
                      if(sb>=0 && sb<s2) //  Fock shadow : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          v=std::conj(fock(sb))*std::exp(i_*k*ss)*vis[j]; //take into account ui at transition
                          ui.setValue(n,v);
                          dofStatus[n-1]=3;
                        }
                      if(sb>s1 && sb<0) // Fock light : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          Real x=static_cast<const FeDof&>(elt->feSpaceP()->dof(n)).coords()(1);
                          if(useIN) v=std::conj(fock(sb))*vi*std::exp(i_*k*isn*isn*isn/(6*c)); // explicit ui-rescaling
                          else v=std::conj(fock(sb))*vi*std::exp(i_*k*c*c*ss*ss*ss/6);
                          ui.setValue(n,v);
                          dofStatus[n-1]=3;
                        }
                      if(sb<=s1 && dofStatus[n-1]==0) // light : use Kirchoff
                        {
                          ui.setValue(n,2*vi);
                          dofStatus[n-1]=1;
                        }
                      if(sb>=s2 && dofStatus[n-1]==0) // shadow : set to 0 the first time
                        {
                          ui.setValue(n,0.);
                          dofStatus[n-1]=2;
                        }
                      if(sb>=s2 && ss<=sc) // shadow : add creeping waves
                        {
                          v=ui.getValue(n).asComplex() + std::exp(i_*(k*ss+std::pow(k,1./3.)*ps))*std::pow(c/c0,1./6.)*cte*vis[j]; //take into account ui at transition
                          ui.setValue(n,v);
                        }
                    }
                }
            }
        }
      std::ofstream out("current_UTD.dat");
      for(Number i=0; i<V->dimSpace(); i++)
        {
          const Point& P=V->feDofCoords(i);
          Complex v=ui.getValue(i+1).asComplex();
          out<<domp->parametrization().toParameter(P)<<" "<<std::real(v)<< " "<<std::imag(v)<<eol;
        }
      out.close();
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      if(&repdom==obsp)
        return uobs; //special case
      TermVector& ui = dataBox.begin()->second;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
        DRep[dup]=integralRepresentation(ur,repdom,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
      TermVector res=DRep[dup]*ui;
      if(obsp!=nullptr) //update field on observation domain
        {
          dup=std::make_pair(obsp,uobsp);
          if(DRep.find(dup)==DRep.end())
            DRep[dup]=integralRepresentation(*uobsp,*obsp,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
          uobs+=DRep[dup]*ui;
        }
      return res;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxKirchoff does not yet propose integral representation of Dn");
      return TermVector(); //fake return
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxComputeBoxKirchoff does not yet propose integral representation of gradient");
      return TermVector(); //fake return
    }
};

//=====================================================================================================================
// computeBoxUTDF class implementing Kirchoff method with Uniform Theory of Diffraction for 2D acoustic Fourier problem
//   (delta+k2)u=0 and (dn+ikZ)u= -(dn+ikZ)ui on Gamma
//
//   light zone  (s<s1) : 2.*intg_dom ndotgrad_y(G)*ui (Kirchoff)
//   shadow zone (s>s2) : creeping ray from UTD
//   light/shadow zone (s1<s<s2): Fock function
//=====================================================================================================================
class ComputeBoxUTDF: public ComputeBox
{
  public:
    GeomDomain *domp;                          //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                          //!< unknown related to the ComputeBox (should not be 0!)
    Kernel *Gp;                                //!< pointer to kernel
    const IntegrationMethods *IMir;            //!< integration methods for integral representations
    Real k;                                  //!< wave number
    Complex z;                               //!< impedance factor in boundary condition (dn + ikz)u=0;
    Real s1,s2;                              //!< fock zone bounds (Fock coordinate)
    Real sc;                                 //!< limit of creeping waves (>s2) (curvilinear coordinate)
    bool useIN;                                //!< use (I|n) in Fock scaling
    bool wedgeContribution;                    //!< if true add wedge contributions (default)
    Number nbr;                              //!< number of residues used by Fock and creeping rays approximation
    std::map<DomUnkPair,TermMatrix> DRep;      //!< double layer matrix list indexed by (domain, unknown)
    Real perimeter;                          //!< mesure of the boundary (computed by computeCurData)
    Vector<Complex> psit;                    //!< last values of psi (computed by computeCurData)
    std::vector<Fock*> focks;                  //!< Fock function objects, one by shadow/light point if Z!=0
    std::map<const FeDof*,CurvatureData> curdatas; //!< additional data at points of the parametrized boundary (computed by computeCurData)
    std::vector<const Dof*> shadowLightDofs;   //!< list of shadow/light dofs
    std::vector<WedgeData> wedges;             //!< list of wedges
    std::vector<Vector<Real> > normals;      //!< computed normals at shadow/light dofs
    std::vector<Real> phase;                 //!< phase at shadow/light dofs
    std::vector<Vector<Real> > phaseGrad;    //!< phase gradient (ray direction) at shadow/light dofs
    GeomDomain* obsp;                          //!< if required, the observation domain
    Unknown* uobsp;                            //!< if required, the unknown related to the observation domain
    TermVector uobs;                           //!< if required, the diffracted field on the observation domain (additive)

    ComputeBoxUTDF() {domp=nullptr; up=nullptr; vp=nullptr; Gp=nullptr; IMir=nullptr; obsp=nullptr; uobsp=nullptr; s1=0; s2=0.; sc=0.; perimeter=0, useIN=false; index=0;wedgeContribution=true;}
    ComputeBoxUTDF(GeomDomain& dom, Unknown& unk, Kernel& ker, const IntegrationMethods& ir, Real kin, const Complex& zin, Number nbrin,
                  Real si1, Real si2, Real sic, bool ini, bool wc, const String& na="")
    {
      name=na;
      domp=&dom;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      Gp=&ker;
      IMir=&ir;
      k=kin;
      z=zin;
      nbr=nbrin;
      s1=si1;
      s2=si2;
      sc=sic;
      useIN=ini;
      wedgeContribution=wc;
      obsp=nullptr;
      uobsp=nullptr;
      dataBox.add(_id);
      dataBox.add(_grad);
      index=0;
      computeCurData();
      if(z==Complex(0.)) {focks.push_back(new Fock(0,nbr,200)); nbr=focks[0]->nbr;}
    }

    ~ComputeBoxUTDF() {clear();}
    void clear()
    {
        if(focks.size()==0) return;
        for(std::vector<Fock*>::iterator it=focks.begin(); it!=focks.end();++it) delete *it;
        focks.clear();
    }
    virtual ComputeBoxType type() const {return _UTD;}
    virtual String typeName() const {return "UTD";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *const_cast<Unknown*>((++dataBox.begin())->second.unknown());}
    virtual bool hasComputedField() const {return obsp!=nullptr;}
    virtual const TermVector& computedField() const
    {
      if(obsp!=nullptr)
        return uobs;
      error("free_error","no computed field in ComputeBox "+name);
      return *new TermVector();
    }

    void computeCurData() //!< compute additional data at points of a parametrized boundary
    {
      Space* V = up->space();
      const FeDof* d0 = static_cast<const FeDof*>(&V->dof(1)), * dp=d0, *dc=nullptr;
      std::vector<std::pair<Element*, Number> >::const_iterator iti = d0->elements().begin();
      //Real tp=toParameter(dp->coords()), tc, cp=fcurvature(tp), cc, lp=flength(tp), lc;
      const Parametrization& par=domp->parametrization();
      Real tp=par.toParameter(dp->coords())[0], tc, cp=par.curvature(tp), cc, lp=par.length(tp), lc;
      Real s=0, h;
      Vector<Complex> psi(nbr,0.);
      Real a23= std::pow(2,-1./3.);
      curdatas[dp] = CurvatureData(tp,s,cp,0.,lp,psi,par.normal(tp), par.tangent(tp),1);
      Element * elt;
      Number nd, ndof;
      while(dc!=d0)
        {
          elt = iti->first;;
          nd = iti->second;
          ndof = elt->dofNumbers[nd%2];
          dc = &elt->feSpaceP()->fedof(ndof);
//          tc = toParameter(dc->coords());
//          cc = fcurvature(tc);
//          lc = flength(tc);
          tc = par.toParameter(dc->coords())[0];
          cc = par.curvature(tc);
          lc = par.length(tc);
          Real dt=abs(tc-tp);
          //if(dt > pi_) dt=abs(dt-2*pi_);
          if(dt > 0.5) dt=abs(dt-1);  //TO BE IMPROVED USING TMIN, TMAX OF PARAMETRIZATION
          h=0.5*dt*(lp+lc);   //approximation of the arc length
          s+=h;

          Real dsc=(cc-cp)/h;       //approx. of the s derivative of the curvature
          for(Number i=0; i<nbr; i++)  psi[i]+=h*0.5*a23*(std::pow(cp,2./3.)+std::pow(cc,2./3.));
          iti = dc->elements().begin();
          if(iti->first==elt) iti++;
          if(dc!=d0) curdatas[dc] = CurvatureData(tc,s,cc,dsc,lc,psi,par.normal(tc), par.tangent(tc), ndof);
          else curdatas[dc].dscurvature=dsc; //update derivative of the curvature
          dp=dc; cp=cc; tp=tc; lp=lc;
          //theLogStream<<"tc="<<tc<<" h="<<h<<" s="<<s<<" par.s="<<par.curabc(tc)<<" c="<<cc<<" psi[0]="<<psi[0]<<eol;
        }
      perimeter=s;
      psit=psi;
      theLogStream<<std::floor(s*k/(2*pi_))<<" wavelengths on perimeter ("<<s<<")"<<eol;
    }

    virtual void solve()
    {
      const Parametrization& par=domp->parametrization();
      //update shadow element colors
      TermVector& ui = dataBox.begin()->second;
      TermVector& gradui = (++dataBox.begin())->second;
      TermVector gradsU =((gradui/ui).toImag())/k;
      setColor(*domp, gradsU, shadowColoringRule);
      std::vector<Int> color;
      Real a=0.1;
      buildSingularDofs(ui, gradui, k, shadowLightDofs, color, normals, phase, phaseGrad,a,wedges);
      Number ns=shadowLightDofs.size();
      if(z!=Complex(0.)) {focks.clear();focks.resize(ns);}
      Real st = perimeter, s3 = sc*perimeter;
      Complex z2=z*z;
      std::map<const FeDof*,CurvatureData>::iterator itm, itms;
      std::vector<Complex> vis(ns);
      std::vector<Real> shadowOrientation(ns);   // =(I|t) : if <0 shadow oriented as s decreasing, if >0 as s increasing TO BE IMPROVED
      for(Number j=0; j<ns; j++)
      {
          itms = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
          shadowOrientation[j]=dot(phaseGrad[j],itms->second.tangent);
          vis[j]= ui.getValue(*shadowLightDofs[j]).asComplex();
          //theLogStream<<"shadow-light transition at t="<<itms->second.t<<" c="<<itms->second.curvature<<" ori="<<shadowOrientation[j];
          if(z!=Complex(0.))
          {
              Real c=itms->second.curvature;
              if(abs(c)<theTolerance) c=theTolerance;
              focks[j]=new Fock(0,nbr,200,std::conj(i_*z)*std::pow(0.5*k/c,1./3.));//Fock initialization
              //theLogStream<<" mu="<<std::conj(i_*z)*std::pow(0.5*k/c,1./3.);
          }
          //theLogStream<<eol;
      }
      TermVector ui2 = TermVector(ui);
      Space* V = up->space();

      std::vector<Number> dofStatus(V->dimSpace(),0); // 0 : not computed, 1: Kirchoff, 2: light Fock, 3: shadow Fock, 4: shadow
      std::vector<Number> creepingStatus(V->dimSpace(),0); // 0 : no creeping, 1: creeping

      // Limit of shadow Fock zone sf[j] = min{s>0 such that sb>s1}, s related to shadow/light point j
      // precomputation of c, psi at transition Fock shadow - creeping
      Vector<Real> s2f(ns,st),s2fb(ns,s2),c2(ns,-1.),s1f(ns,-st),s1fb(ns,s1),spec(ns), specb(ns), s1fm(ns,-st),s1fbm(ns,0);
      Vector<Vector<Complex> > psi2(ns,Vector<Complex>(nbr));
      for(Number j=0; j<ns; j++)
        {
          Real isnmin=1;
          itms = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
          Real ori = shadowOrientation[j];
//          theLogStream<<" Shadow/Light P_"<<j<<"="<<shadowLightDofs[j]->coords()<<" (t="<< toParameter(shadowLightDofs[j]->coords())
//                        <<")  orient="<<ori<<eol;
//          String fn="s1b_"+tostring(j+1)+".dat";
//          std::ofstream fout1(fn.c_str());
//          fn="s2b_"+tostring(j+1)+".dat";
//          std::ofstream fout2(fn.c_str());
          for(itm=curdatas.begin(); itm!=curdatas.end(); ++itm)
            {
              Int col=color[itm->second.ndof-1];
              if(col==-2) //in shadow
                {
                  Real s = itm->second.abcissa-itms->second.abcissa, c = itm->second.curvature;
                  if ((s>0 && ori<0) || (s<0 && ori>0)) s=st-abs(s); else s=abs(s);
                  Real sb = std::pow(0.5*k*c*c,1./3.)*s;
                  if(sb>=s2 && s<s2f[j]) //Fock limiter : max{s; sb>=s2}
                    {
                      s2f[j]=s;
                      s2fb[j]=sb;
                      c2[j]=c;
                      for(Number i=0; i<nbr; i++) psi2[j][i]=itm->second.psi[i];
//                      theLogStream<<"shadow t="<<itm->second.t<<" s="<<s<<" sb="<<sb<<" s2f[j]="<<s2f[j]<<eol;
                    }
//                    fout2<<s<<" "<<sb<<" "<<c<<" "<<eol;
                }
                else if(col==0) //in light
                {
                  Real s = itm->second.abcissa-itms->second.abcissa, c = itm->second.curvature, dc=itm->second.dscurvature;
                  if ((s>0 && ori>0) || (s<0 && ori<0)) s=abs(s)-st; else s=-abs(s);
                  Real sb;
                  Number n = itm->second.ndof;
                  Vector<Complex> gv=(++dataBox.begin())->second.getValue(n).asComplexVector()/(k*(dataBox.begin())->second.getValue(n).asComplex());
                  Real isn=dot(imag(gv),itm->second.normal);
                  if(useIN) sb = std::pow(0.5*k/c,1./3.)*isn;
                  else sb = std::pow(0.5*k*c*c,1./3.)*s;
                  if(isn<isnmin) {isnmin=isn;spec[j]=s;specb[j]=sb;}  // detect specular point
                  if(sb<=s1 && s>s1f[j]) //first Fock limiter :  min{s; sb> s1}
                    {
//                      theLogStream<<"light  t="<<itm->second.t<<" s="<<s<<" sb="<<sb<<" s1f[j]="<<s1f[j]<<eol;
                      s1f[j]=s;
                      s1fb[j]=sb;
                    }
                  // second Fock limiter : max{s; (c^(2/3)s)'=0 <=> c + 2*c'*s/3=0}, use  max{s; c + 2*c'*s/3<0}
                  Real der;
                  if (ori>0) der=c+2*dc*s/3; else der=c-2*dc*s/3;
                  if(der <= 0 && s>s1fm[j]) {s1fm[j]=s;s1fbm[j]=sb;}
                  //fout1<<s<<" "<<sb<<" "<<c<<" "<<dc<<" "<<der<<eol;
                }
            }
//            fout1.close();fout2.close();
//            theLogStream<<" s1f[j]="<<s1f[j]<<" s1fm[j]="<<s1fm[j]<<" spec[j]="<<spec[j]<<eol;
            if(s1f[j]==-st) {s1f[j]=s1fm[j];s1fb[j]=s1fbm[j];} //sb(s) does not cross s1, take s min(sb)
            if(s1f[j]<0.98*spec[j]) // restrict to exclude specular point from the light Fock region
                {
                    s1f[j]=0.98*spec[j];
                    s1fb[j]=0.98*s1fbm[j];  // if using isn, assume new isn is close to 0.95*isn, correct if isn is not used
                }
        }
      //theLogStream<<" Fock limiter : s1f="<<s1f<<"->"<<s1fb<<" s2f="<<s2f<<"->"<<s2fb<<eol;
      std::ofstream dout("dofStatus.dat");

      for(Number e=0; e<V->nbOfElements(); e++) //loop on elements
        {
          const Element* elt=V->element_p(e);
          bool shadow =  elt->geomElt_p->color!=0;
          const std::vector<Number>& dofNum=elt->dofNumbers;
          std::vector<Number>::const_iterator itn=dofNum.begin();
          for(itn = dofNum.begin(); itn!=dofNum.end(); ++itn)
            {
              Number n=*itn;
              if(dofStatus[n-1]==0)
                {
                  Complex vi=ui2.getValue(n).asComplex();
                  itm = curdatas.find(static_cast<const FeDof*>(&elt->feSpaceP()->dof(n)));
                  if(itm==curdatas.end()) error("free_error","abnormal failure in ComputeBoxUTDF::solve");
                  Real s = itm->second.abcissa, c = itm->second.curvature, l=itm->second.length;
                  if(std::abs(c)<theTolerance) c=theTolerance; //avoid 0 curvature
                  Vector<Complex>& psi = itm->second.psi;
                  Vector<Real>& normal = itm->second.normal;
                  Point p = itm->first->coords();
                  //theLogStream<<"n="<<n<<" p="<<p<<" c="<<c<<" s="<<s<<" shadow="<<shadow<<" t="<<par.toParameter(itm->first->coords())<<eol;
                  for(Number j=0; j<ns; j++) // analyze distance to shadow/light point
                    {
                      Number fj=0;
                      if(z!=Complex(0.)) fj=j;
                      Fock& fock = *focks[fj];
                      Vector<Complex> cte(fock.nbr),xi(fock.nbr);
                      for(Number i=0; i<fock.nbr; i++)
                        {
                          xi[i]=std::conj(fock.poles[i]);
                          cte[i]=2*i_*sqrtOfpi_/std::conj(fock.vpoles[i]);
                        }
                      itm = curdatas.find(static_cast<const FeDof*>(shadowLightDofs[j]));
                      if(itm==curdatas.end()) error("free_error","abnormal failure in ComputeBoxUTDF::solve");
                      Real ori = shadowOrientation[j];
                      Real c0 = itm->second.curvature;
                      Real ss0 = s-itm->second.abcissa, ss=ss0;
                      ss=abs(ss0);
                      bool jump= (ss0>0 && ((shadow && ori<0) || (!shadow && ori>0))) || (ss0<0 && ((shadow && ori>0) || (!shadow && ori<0)));
                      if(jump) ss=st-ss;
                      if(!shadow) ss*=-1;
                      Complex v=0.;
                      Vector<Complex> gv=((++dataBox.begin())->second.getValue(n).asComplexVector()/(k*vi));
                      Real isn=dot(imag(gv),normal);
                      Real sb=std::pow(0.5*k*c*c,1./3.)*ss;
                      //theLogStream<<"   j="<<j<<" s="<<s<<" sld="<<itm->second.abcissa<<" ss0="<<ss0<<" ss="<<ss<<" sb="<<sb;
                      if(useIN && ss<=0 && ss>-0.25*st)
                      {
                          sb = std::pow(0.5*k/c,1./3.)*isn; //in light region, compute sb using I|n
                          //theLogStream<<" sb (isn)="<<sb;
                      }
                      Complex am=isn/(isn-z);
                      if(ss>s1f[j] && sb>s1 && sb<0) // Fock light : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          Real x=static_cast<const FeDof&>(elt->feSpaceP()->dof(n)).coords()(1);
                          if(useIN)
                          {
                              v=std::conj(fock(sb))*vi*std::exp(i_*k*isn*isn*isn/(6*c));    // explicit ui-rescaling
                              if(z!=Complex(0.)) v*=(isn-std::pow(c/c0,1./3.)*z)/(isn-z); // additional correction when z!=0 (do not modify the value at s=0!)
                          }
                          else
                          {
                              v=std::conj(fock(sb))*vi*std::exp(i_*k*c*c*ss*ss*ss/6);
                              if(z!=Complex(0.)) v*=(c*ss-std::pow(c/c0,1./3.)*z)/(c*ss-z); // additional correction when z!=0 (do not modify the value at s=0!)
                          }
                          ui.setValue(n,v);
                          dofStatus[n-1]=2;
                          //theLogStream<<" light Fock v="<<v;
                        }
                      if((ss<=s1f[j] || sb<=s1) && dofStatus[n-1]==0) // light : use Kirchoff
                        {
                          ui.setValue(n,2*vi*am);
                          dofStatus[n-1]=1;
                          //theLogStream<<" light Kirchoff v="<<2*vi;
                        }
                      if((ss>=s2f[j] || sb>=s2) && dofStatus[n-1]==0) // shadow : set to 0 the first time
                        {
                          ui.setValue(n,0.);
                          dofStatus[n-1]=4;
                          //theLogStream<<" shadow v=0";
                        }
                      if(ss>=0 && ss<s2f[j] && sb<s2) //  Fock shadow : set Fock function to ui field (ui=1 at shadow-light point)
                        {
                          v=std::conj(fock(sb))*std::exp(i_*k*ss)*vis[j]; //take into account ui at transition
                          ui.setValue(n,v);
                          dofStatus[n-1]=3;
                          //theLogStream<<" shadow Fock v="<<v;
                        }
                      //theLogStream<<eol;
                      // add creeping waves
                      ss=std::abs(ss0);jump=false;
                      if ((ss0>0 && ori<0) || (ss0<0 && ori>0)) {ss=st-ss;jump=true;}
                      if((ss>=s2f[j] || sb>=s2) && ss<=s3)
                        {
                          v=0;
                          Complex mus =std::pow(0.5*k/c,1./3.), mu0= std::pow(0.5*k/c0,1./3.), mu2 = std::pow(0.5*k/c2[j],1./3.);
                          for(Number i=0; i<fock.nbr; i++)
                            {
                              Complex ps = xi[i]*(psi[i]-psi2[j][i]);  //TO BE IMPROVED
                              //theLogStream<<"     residu "<<i<<" ss0="<<ss0<<" ss="<<ss<<" jump="<<jump<<" psi="<<psi[i]<<" psi2="<<psi2[j][i]<<" ps0="<<ps;
//                            if(jump) {if(ss0<0) ps=xi[i]*psit[i]+ps; else ps=xi[i]*psit[i]-ps;}
                              //theLogStream<<" ps1="<<ps;
                              if(std::imag(ps)<0) ps=-ps;
                              Complex kai=1.;
                              if(z!=Complex(0.)) kai=std::sqrt((xi[i]+mu2*mu2*z2)/(xi[i]+mus*mus*z2));
                              Real ampj=std::pow(c/c2[j],1./6.);
                              v+=std::exp(i_*(k*ss+std::pow(k,1./3.)*ps+s2fb[j]*xi[i]))*ampj*kai*cte[i]*vis[j]; //take into account ui at transition
                              //theLogStream<<" ps="<<ps<<" -> vf="<<v<<eol;
                            }
                          v+=ui.getValue(n).asComplex();
                          //theLogStream<<"  after creeping  vf="<<v<<eol;
                          creepingStatus[n-1]+=1;
                          ui.setValue(n,v);
                        }
                    }
                    dout<<p<<" "<<par.toParameter(p)<<" "<<dofStatus[n-1]<<" "<<creepingStatus[n-1]<<eol;
                }
            }
        }
      dout.close();

      String na="current_UTD_"+tostring(index)+".dat";
      std::ofstream out(na.c_str());
      for(Number i=0; i<V->dimSpace(); i++)
        {
          const Point& P=V->feDofCoords(i);
          Complex v=ui.getValue(i+1).asComplex();
          out<<par.toParameter(P)<<" "<<std::real(v)<< " "<<std::imag(v)<<eol;
        }
      out.close();

      if(wedgeContribution) //add wedge contributions
      {
          const std::vector<FeDof>& dofs= V->feDofs();
          Number nd=dofs.size();
          std::vector<WedgeData>::iterator itw=wedges.begin();
          for(;itw!=wedges.end();++itw)
          {
           Point W = itw->wedge;
           Point dphip=rotate2d(Point(itw->wedgeDir),Point(0.,0.),itw->wedgeAngle);
           Point dphim=rotate2d(Point(itw->wedgeDir),Point(0.,0.),-itw->wedgeAngle);
           Number nw=itw->wedgeDof->id();
           Vector<Real> incdir=gradsU.getValue(nw).asRealVector();
           Real incAngle=std::acos(dot(itw->wedgeDir,incdir)/(norm(itw->wedgeDir)*norm(incdir)));
           theCout <<" compute wedge contribution at "<<W<<" angle = "<<itw->wedgeAngle<<" direction = "<<itw->wedgeDir
                   <<" phip = "<<dphip<<" phim = "<<dphim<<" inc. angle = "<<incAngle<<eol;
           std::vector<FeDof>::const_iterator itd=dofs.begin();
           std::list<std::pair<Real,Number> > krp, krm;
           for(Number i=0; i<V->dimSpace(); i++)
           {
               Point pw = V->feDofCoords(i)-W;
               if(norm(crossProduct(pw,dphip))<theTolerance  && dot(pw,dphip)>theTolerance) krp.push_back(std::make_pair(k*norm(pw),i+1));
               else if(norm(crossProduct(pw,dphim))<theTolerance  && dot(pw,dphim)>theTolerance) krm.push_back(std::make_pair(k*norm(pw),i+1));
           }
           if(krp.size()>0)
           {
               std::vector<Real> krs(krp.size(),0.);
               std::vector<Real>::iterator itkr=krs.begin();
               std::list<std::pair<Real,Number> >::iterator it=krp.begin();
               for(;it!=krp.end(); ++it, ++itkr) *itkr=it->first;
               std::vector<Complex> curp = wedgeCurrentSD(krs,itw->wedgeAngle,1,incAngle,_wedgeField,1000,_NeumannEc);
               out.open("currentwedgeP.dat");
               std::vector<Complex>::iterator itc=curp.begin();
               for(it=krp.begin();it!=krp.end(); ++it, ++itc)
               {
                   Number n=it->second;
                   ui.setValue(n,ui.getValue(n).asComplex()+*itc);
                   out<<it->first<<" "<<itc->real()<<" "<<itc->imag()<<eol;
               }
               out.close();
           }
           if(krm.size()>0)
           {
               std::vector<Real> krs(krm.size(),0.);
               std::vector<Real>::iterator itkr=krs.begin();
               std::list<std::pair<Real,Number> >::iterator it=krm.begin();
               for(;it!=krm.end(); ++it, ++itkr) *itkr=it->first;
               std::vector<Complex> curp = wedgeCurrentSD(krs,itw->wedgeAngle,-1,incAngle,_wedgeField,1000,_NeumannEc);
               //theLogStream<<"current on -phi : "<<curp<<eol;
               std::vector<Complex>::iterator itc=curp.begin();
               out.open("currentwedgeM.dat");
               for(it=krm.begin();it!=krm.end(); ++it, ++itc)
               {
                   Number n=it->second;
                   ui.setValue(n,ui.getValue(n).asComplex()+*itc);
                   out<<it->first<<" "<<itc->real()<<" "<<itc->imag()<<eol;
               }
               out.close();
           }
         }
         na="current_UTD_WEDGE"+tostring(index)+".dat";
         out.open(na.c_str());
         for(Number i=0; i<V->dimSpace(); i++)
        {
          const Point& P=V->feDofCoords(i);
          Complex v=ui.getValue(i+1).asComplex();
          out<<par.toParameter(P)<<" "<<std::real(v)<< " "<<std::imag(v)<<eol;
        }
        out.close();
      }
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      if(&repdom==obsp)
        return uobs; //special case
      TermVector& ui = dataBox.begin()->second;
      DomUnkPair dup=std::make_pair(&repdom,&ur);
      if(DRep.find(dup)==DRep.end())
      {
          DRep[dup]=integralRepresentation(ur,repdom,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
          if(z!=Complex(0.)) DRep[dup]+=(i_*k*z)*integralRepresentation(ur,repdom,intg(*domp,(*Gp)**up,_method=*IMir));
      }
      TermVector res=DRep[dup]*ui;
      if(obsp!=nullptr) //update field on observation domain
        {
          dup=std::make_pair(obsp,uobsp);
          if(DRep.find(dup)==DRep.end())
            {
                DRep[dup]=integralRepresentation(*uobsp,*obsp,intg(*domp,ndotgrad_y(*Gp)**up,_method=*IMir));
                if(z!=Complex(0.)) DRep[dup]+=(i_*k*z)*integralRepresentation(ur,repdom,intg(*domp,(*Gp)**up,_method=*IMir));
            }
          uobs+=DRep[dup]*ui;
        }
      return res;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeUTDF does not yet propose integral representation of Dn");
      return TermVector(); //fake return
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxComputeUTDF does not yet propose integral representation of gradient");
      return TermVector(); //fake return
    }
};

//=====================================================================================================================
// computeBoxRay class implementing the ray method
/* algorithm to compute diffraction from current domain dom1 on a given domain dom2
   - compute the directions of reflecting ray from incident wave for each dofs of dom1
   - compute the set of illuminating elements of dom1(say E1) and the set of illuminated elements of dom2 (say E2)
   - for each dof of E2,
        loop on elements of E1
          find if one interpolated reflecting ray exist
          compute the amplitude on ray at dof
*/
//=====================================================================================================================
class ComputeBoxRay: public ComputeBox
{
  public:
    GeomDomain *domp;                                  //!< Domain supporting the ComputeBox
    Unknown *up, *vp;                                  //!< unknown related to the ComputeBox (should not be 0!)
    Real k;                                          //!< wave number
    set<const Element*> E1;                            //! subset of illuminating elements (on current dom)
    std::map<GeomDomain*,set<const GeomElement*> > E2; //!< map dom -> subset of illuminated elements (on dom)
    TermVector normals;                                //!< normals at dof coordinates
    TermVector rays;                                   //!< rays at dof coordinates
    TermVector phases;                                 //!< phases at dof coordinates
    std::vector<std::vector<Vector<Real> > > dofrays;//!< vector of rays arriving at observation points (visualization purpose)
    bool save_rays;                                    //!< true for saving rays in dofrays vector
    const Unknown* uobsp;                              //!< if required, the unknown related to the observation domain
    TermVector uobs;                                   //!< if required, the total field on the observation domain (additive)
    const TermVector* uinc;                            //!< pointer to the incident field on the observation domain, given by user
    std::vector<bool> lightDof;                        //!< vector used to mark illuminated dofs of observation domai

    ComputeBoxRay()  {domp=nullptr; up=nullptr; vp=nullptr; k=1.; save_rays=false; uobsp=nullptr; uinc=nullptr; update_obs=true;}
    ComputeBoxRay(GeomDomain& dom, Unknown& unk,Real kin, const String& na="")
    {
      name=na;
      domp=&dom;
      up=&unk;
      vp=const_cast<Unknown*>(&up->dual());
      k=kin;
      dataBox.add(_id);
      dataBox.add(_grad);
      save_rays=false;
      uinc=nullptr;
      uobsp=nullptr;
      iter=0;
      update_obs=true;
    }

    virtual ComputeBoxType type() const {return _ray;}
    virtual String typeName() const {return "Ray";}
    virtual GeomDomain& domain() const {return *domp;}
    virtual Unknown& unknown() const {return *up;}
    virtual Unknown& vec_unknown() const {return *const_cast<Unknown*>((++dataBox.begin())->second.unknown());}
    void setUinc(const TermVector& ui) {uinc=&ui;}
    void setObs(const Unknown& uobs) {uobsp=&uobs;}
    virtual bool hasComputedField() const {return uobsp!=nullptr;}
    virtual const TermVector& computedField() const {return uobs;}
    bool admissible(const Vector<Real>& xray) {return true;}

    virtual void solve()
    {
      // compute reflecting rays and update illuminating set
      TermVector& ui = dataBox.begin()->second;
      TermVector& gradui = (++dataBox.begin())->second;
      TermVector grads =((gradui/ui).toImag())/k;
      phases = TermVector(*up,*domp,0.);
      if(iter<=1)
        normals=normalsOn(*domp,*grads.unknown());
      setColor(*domp, grads, shadowColoringRule);
      const Space& V = *grads.unknown()->space();
      rays=grads*0.;
      std::set<Number> dofnums;
      for(Number j=0; j<V.nbOfElements(); j++)
        {
          const Element* elt=V.element_p(j);
          if(elt->geomElt_p->color==0)
            {
              E1.insert(elt);
              const std::vector<Number>& dofNum=elt->dofNumbers;
              std::vector<Number>::const_iterator itn = dofNum.begin();
              for(; itn!=dofNum.end(); ++itn)

                if(dofnums.find(*itn)==dofnums.end())
                  {
                    dofnums.insert(*itn);
                    Vector<Real> r=-grads.getValue(*itn).asRealVector();
                    Vector<Real> n=normals.getValue(*itn).asRealVector();
                    rays.setValue(*itn,2*dot(r,n)*n-r);
                    phases.setValue(*itn,std::arg(ui.getValue(*itn).asComplex()));
                  }
            }
        }
      saveToFile("rays", rays, _format=_vtu);
      saveToFile("normals", normals, _format=_vtu);
      saveToFile("phases", phases, _format=_vtu);
    }

    void illuminatedSet(Domain repdom)
    {
      E2[&repdom] = std::set<const GeomElement*>();
      std::set<const GeomElement*>& E2r = E2[&repdom];
      for(Number j=1; j<=repdom.numberOfElements(); j++)
        E2r.insert(repdom.element(j));
    }

    void amplitudePhase(const Point& x, const Point& y1,const Point& y2, const Vector<Real>& t1, const Vector<Real>& t2, const Vector<Real>& n1,const Vector<Real>& n2,
                        Real p1, Real p2, const Complex& u1, const Complex& u2,const Vector<Complex>& gu1, const Vector<Complex>& gu2, Real s,
                        Real& amplitude, Real& phase)
    {
      Real ms=1-s;
      Point z = ms*y1+s*y2;
      Real l= x.distance(z), h=y1.distance(y2);
      Vector<Real> tz = ms*t1+s*t2;
      tz/=norm(tz);
      Vector<Real> nz = ms*n1+s*n2;
      nz/=norm(nz);
      Vector<Real> sz(2);
      sz[0] = -nz[1];
      sz[1] = nz[0];
      Complex uz=ms*u1+s*u2;
      Vector<Complex> guz=ms*gu1+s*gu2;
      Vector<Real> iz =  imag(guz/(k*uz));
      iz/=norm(iz);
      Vector<Real> di =  imag((gu2-gu1)/(h*k*uz) -(dot(guz,sz)/(k*uz*uz))*guz);
      Vector<Real> dn = (n2-n1)/h;
      Vector<Real> dt = sz+l*(di-2*(dot(di,nz)+dot(iz,dn))*nz-2*dot(iz,nz)*dn);
      Real j0 = sz[0]*tz[1] - sz[1]*tz[0], jx = dt[0]*tz[1] - dt[1]*tz[0];
      amplitude = std::abs(uz)*std::sqrt(std::abs(j0/jx));
      phase = std::arg(uz)/k+l;
    }

    // find if x can be reached by an interpolated ray (1-s)*r1+s*r2 solving the second degree equation
    //        ((1-s)*y1+s*y2-x) ^ ((1-s)*r1+s*r2) = 0
    // and add to x value the contributions amp*exp(i*k*p) where amp is the amplitude at x and p the phase at x
    // set also the collection of rays returning to x (xrays)
    bool interpolatedRayContribution(const Point& x, const Element* elt, Complex& val1, Complex& val2, std::vector<Vector<Real> >& xrays)
    {
      val1=0.;
      val2=0.;
      const std::vector<Number>& dofNum=elt->dofNumbers;
      Number k1=dofNum[0], k2=dofNum[1];
      Point& y1=elt->feSpaceP()->dof(k1).coords();
      Point& y2=elt->feSpaceP()->dof(k2).coords();
      TermVector& ui = dataBox.begin()->second;
      Complex u1=ui.getValue(k1).asComplex();
      Complex u2=ui.getValue(k2).asComplex();
      if(x.distance(y1)<theEpsilon)
        {
          val1=u1;  // do not use ray method
          return true;
        }
      if(x.distance(y2)<theEpsilon)
        {
          val1=u2;  // do not use ray method
          return true;
        }

      const Vector<Real>& t1 = rays.getValue(k1).asRealVector();
      const Vector<Real>& t2 = rays.getValue(k2).asRealVector();
      if(norm(t1)<theEpsilon || norm(t2)<theEpsilon)
        return false;   //no ray at one point !!!

      Point t2mt1 = t2-t1, y2my1 = y2-y1, xmy1=x-y1, pt1(t1);
      Real a = y2my1[0]*t2mt1[1] - y2my1[1]*t2mt1[0],
             b = y2my1[0]*pt1[1] - y2my1[1]*pt1[0] - xmy1[0]*t2mt1[1] + xmy1[1]*t2mt1[0],
             c = xmy1[1]*pt1[0]-xmy1[0]*pt1[1];
      Real d=b*b-4*a*c;
      if(d<0)
        return false;
      //thePrintStream<<"y1="<<y1<<" y2="<<y2<<" t1="<<t1<<" t2="<<t2<<" u1="<<u1<<" u2="<<u2<<" a="<<a<<" b="<<b<<" c="<<c<<" d="<<d<<eol<<std::flush;

      Real sp=theRealMax, sm=theRealMax;
      bool res=false;
      if(std::abs(a)<theEpsilon)
        {
          if(std::abs(b)>theEpsilon)
            sm=-c/b;
          else
            return false;  //a=b=0 !!!
        }
      else if(d<theEpsilon*theEpsilon)
        sp=-b/(2*a);
      else
        {
          d=std::sqrt(d);
          sp=(-b+d)/(2*a);
          sm=(-b-d)/(2*a);
        }
      bool computesp = (sp>-theEpsilon && sp<1+theEpsilon && dot(xmy1-sp*y2my1,pt1+sp*t2mt1)>0),
           computesm = (sm>-theEpsilon && sm<1+theEpsilon && dot(xmy1-sm*y2my1,pt1+sm*t2mt1)>0);
      if(!computesm && !computesp)
        return false;
      const Vector<Real>& n1 = normals.getValue(k1).asRealVector();
      const Vector<Real>& n2 = normals.getValue(k2).asRealVector();
      Real p1=phases.getValue(k1).asReal();
      Real p2=phases.getValue(k2).asReal();
      TermVector& gradui = (++dataBox.begin())->second;
      const Vector<Complex>& gu1=gradui.getValue(k1).asComplexVector();
      const Vector<Complex>& gu2=gradui.getValue(k2).asComplexVector();
      Real amp, ph;
      if(computesm && computesp)
        thePrintStream<<" two rays "<<eol;
      if(computesp)
        {
          amplitudePhase(x,y1,y2,t1,t2,n1,n2,p1,p2,u1,u2,gu1,gu2,sp,amp,ph);
          val1=amp*std::exp(i_*k*ph);
          xrays.push_back(xmy1-sp*y2my1);
          //thePrintStream<<"x="<<x<<" y1="<<y1<<" y2="<<y2<<" t1="<<t1<<" t2="<<t2<<" u1="<<u1<<" u2="<<u2<<" sp="<<sp<<" amp="<<amp<<" ph="<<ph<<eol<<std::flush;
        }
      if(computesm)
        {
          amplitudePhase(x,y1,y2,t1,t2,n1,n2,p1,p2,u1,u2,gu1,gu2,sm,amp,ph);
          if(computesp)
            val2=amp*std::exp(i_*k*ph);
          else
            val1=amp*std::exp(i_*k*ph);
          //thePrintStream<<"x="<<x<<" y1="<<y1<<" y2="<<y2<<" t1="<<t1<<" t2="<<t2<<" u1="<<u1<<" u2="<<u2<<" sm="<<sm<<" amp="<<amp<<" ph="<<ph<<eol<<std::flush;
          xrays.push_back(xmy1-sm*y2my1);
        }

      return true;
    }

    virtual TermVector computeOn(Domain repdom, Unknown& ur)
    {
      TermVector res(ur,repdom,Complex(0.),"ures");
      computeRayDiffraction(res,repdom,ur,save_rays);
      if(iter==1 && uinc!=0)
        addIncidentField(res,*uinc);//add to res the total field in the light zone
      if(update_obs && uobsp!=0 && uobsp!=&ur) //update field on observation domain (iterative case)
        {
          GeomDomain& obs= *const_cast<GeomDomain*>(uobsp->space()->domain());
          if(uobs.size()==0)
            uobs = TermVector(*uobsp,obs,Complex(0.),"uobs");
          computeRayDiffraction(uobs,obs,*uobsp,false);
          saveToFile("uLobs_"+tostring(iter), uobs, _format=_vtu);
          update_obs=false;
        }
      return res;
    }

    void computeRayDiffraction(TermVector& res, Domain repdom, const Unknown& ur, bool sray)
    {
      theCout<<"compute ray diffraction on "<<repdom.name()<<eol;
      Space* V=ur.space();
      illuminatedSet(repdom);
      Vector<Complex>& vec = *res.subVector().entries()->cEntries_p;
      if(sray)
        dofrays.resize(res.size());
      lightDof.resize(res.size(),false);
      #ifdef XLIFEPP_WITH_OMP
      #pragma omp parallel for schedule(dynamic)
      #endif // XLIFEPP_WITH_OMP
      for(Number j=0; j<vec.size(); j++)
        {
          Point x=V->feDofCoords(j);
          std::vector<Vector<Real> > xrays;
          for(std::set<const Element*>::iterator ite=E1.begin(); ite!=E1.end(); ++ite)
            {
              Complex val1, val2;
              bool add = interpolatedRayContribution(x,*ite, val1, val2, xrays);
              if(add)
                {
                  add=false;
                  if(val1!=0 && admissible(xrays[0]))
                    {
                      vec[j]+=val1;
                      add=true;
                    }
                  if(val2!=0 && admissible(xrays[1]))
                    {
                      vec[j]+=val2;
                      add=true;
                    }
                  if(!lightDof[j] && add)
                    lightDof[j]=true;
                }
              if(sray)
                dofrays[j]=xrays;
            }
        }
      if(sray)
        saveRays(repdom,ur);
    }

    void addIncidentField(TermVector& res, const TermVector& ui)
    {
      Vector<Complex>& vec = *res.subVector().entries()->cEntries_p;
      const Vector<Complex>& inc = *ui.subVector().entries()->cEntries_p;
      for(Number j=0; j<vec.size(); j++)
        if(lightDof[j])
          vec[j]+=inc[j];
    }

    Complex computeAt(const Point& x)
    {
      Complex val;
      std::vector<Vector<Real> > xrays;
      Complex val1, val2;
      for(std::set<const Element*>::iterator ite=E1.begin(); ite!=E1.end(); ++ite)
        {
          interpolatedRayContribution(x,*ite,val1, val2, xrays);
          if(val1!=0 && admissible(xrays[0]))
            val+=val1;
          if(val2!=0 && admissible(xrays[1]))
            val+=val2;
        }
      return val;
    }

    virtual TermVector computeDnOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxRay does not yet propose integral representation of Dn");
      return TermVector(); //fake return
    }

    virtual TermVector computeGradOn(Domain repdom, Unknown& ur)
    {
      error("free_error","ComputeBoxray does not yet propose integral representation of gradient");
      return TermVector(); //fake return
    }

    void saveRays(Domain repdom, const Unknown& ur)
    {
      if(dofrays.size()==0)
        warning("free_warning", "ComputeBoxRay::saveRays(), rays have not been saved, switch on saving: ComputeBoxRay::save_rays=true");
      //paraview file
      String nu=ur.name()+"#2";
      Unknown* ur2 = Unknown::findUnknown(nu);
      if(ur2==nullptr)
        ur2=new Unknown(*ur.space(),nu,2);
      Number n=ur.space()->nbDofs(), maxr=0;
      for(Number j=0; j<n; j++)
        maxr=std::max(maxr,dofrays[j].size());
      for(Number k=0; k<maxr; k++)
        {
          TermVector R(*ur2,repdom,Vector<Real>(2));
          for(Number j=0; j<n; j++)
            {
              if(dofrays[j].size()>k)
                R.setValue(j+1,dofrays[j][k]);
            }
          saveToFile("rays_"+nu+"_"+tostring(k)+"_"+tostring(iter), R, _format=_vtu);
        }

      //raw file (matlab)
      std::ofstream out(String("xrays_"+nu+"_"+tostring(iter)+".dat").c_str());
      for(Number j=0; j<n; j++)
        {
          Point x=ur.space()->feDofCoords(j);
          for(Number k=0; k<dofrays[j].size(); k++)
            {
              Vector<Real>& t = dofrays[j][k];
              out<<x[0]<<" "<<x[1]<<" "<<t[0]<<" "<<t[1]<<eol;
            }
        }
      out.close();
      out.open(String("rays_"+nu+"_"+tostring(iter)+".dat").c_str());
      for(Number j=0; j<up->space()->nbDofs(); j++)
        {
          Point x=up->space()->feDofCoords(j);
          Vector<Real> t = rays.getValue(j+1).asRealVector();
          out<<x[0]<<" "<<x[1]<<" "<<t[0]<<" "<<t[1]<<eol;
        }
      out.close();
    }
};

void saveFields(const TermVector& Ut, const TermVector& Ud, const TermVector& Ui,
                Domain sideExt, const Complex& amp, const Point& Cr, const String& name, bool view2d=true)
{
  if(view2d)
    {
      saveToFile("Ut"+name, Ut, _format=_vtu);
      saveToFile("Ud"+name, Ud, _format=_vtu);
      saveToFile("Ui", Ui, _format=_vtu);
      saveFarField(Ud|sideExt, amp, Cr, "Ufar"+name+".dat");
    }
  else
    {
      saveFarField(Ud, amp, Cr, "Ufar"+name+".dat");
      //saveToFile("Ufar"+meth+obstacleName(obst), amp*UdBEM, _format=_vtu);
    }
}

//=====================================================================================================================
//  General iterative scheme
//=====================================================================================================================
//    cbL : compute box related to large obstacle
//    cbS : compute box related to small obstacle
//    niter : number of iterations
//    viewStep : step for output
//    uv : unknown pointer used for saving diffracted fields to file at each step, if 0 nothing is saved
//
//    NOTE: cbL.dataBox and cbS.dataBox have to be initialized with the incident field (see ComputeBox::setData functions)
//          at the end of process, cbS.dataBox and cbL.dataBox contains the cumulative data allowing to reconstruct the total field
void iterativeScheme(ComputeBox& cbS, ComputeBox& cbL, Number niter, Number viewStep=0, Unknown* uv=nullptr)
{
  GeomDomain* obs =nullptr;
  if(uv!=nullptr)
    obs = const_cast<GeomDomain*>(uv->space()->domain());
  String na=cbL.typeName()+"_"+cbS.typeName();

  DataBox dbL = cbL.dataBox, dbS = cbS.dataBox;
  for(Number n=1; n<=niter; n++)
    {
      bool view= viewStep!=0 && ((n-1)% viewStep ==0 || n==niter);
      theCout<<" iteration "<<n<<eol<<" ---------"<<eol;

      // solve L obstacle
      cbL.iter=n;
      cbL.update_obs=true;
      cbL.solve();
      cbL.computeOn(cbS);          // compute cbS.dataBox using cbL.dataBox
      dbS += cbS.dataBox;
      if(n==1)
        cbS.dataBox = dbS;  // at first iteration take for cbS the incident field + the diffracted field by cbL
      if(view)
        {
          thePrintStream<<" cbL : "<<cbL.dataBox<<eol;
          saveToFile("cbL_"+na+"_"+tostring(n), cbL.dataBox.begin()->second, _format=_vtu);
          if(uv!=nullptr)
            saveToFile("UL_"+na+"_"+tostring(n), cbL.computeOn(*obs,*uv),_format= _vtu);
        }

      //solve S obstacle
      cbS.iter=n;
      cbS.solve();
      cbS.computeOn(cbL);          // compute cbL.dataBox using cbS.dataBox
      dbL+=cbL.dataBox;
      if(view)
        {
          thePrintStream<<" cbS : "<<cbS.dataBox<<eol;
          saveToFile("cbS_"+na+"_"+tostring(n), cbS.dataBox.begin()->second, _format=_vtu);
          if(uv!=nullptr)
            saveToFile("US_"+na+"_"+tostring(n), cbS.computeOn(*obs,*uv), _format=_vtu);
        }
    } //end of iterative loop

  //update cumulative data and re-solve if field has not been computed
  cbL.dataBox=dbL;
  cbS.dataBox=dbS;
  if(!cbL.hasComputedField())
    cbL.solve();
  if(!cbS.hasComputedField())
    cbS.solve();
}

//    cbs : list of compute boxes
//    niter : number of iterations
//    viewStep : step for output
//    uv : unknown pointer used for saving diffracted fields to file at each step, if 0 nothing is saved
//
//    NOTE: cbs[i].dataBox have to be initialized with the incident field (see ComputeBox::setData functions)
//          at the end of process, cbs[i].dataBox contains the cumulative data allowing to reconstruct the total field
void iterativeScheme(std::vector<ComputeBox*>& cbs, Number niter, Number viewStep=0, Unknown* uv=nullptr)
{
  GeomDomain* obs =nullptr;
  if(uv!=nullptr)
    obs = const_cast<GeomDomain*>(uv->space()->domain());
  Number nbox = cbs.size();
  std::vector<DataBox> dbs(nbox);
  for(Number i=0; i<nbox; i++)
    {
      dbs[i]=cbs[i]->dataBox;
      thePrintStream<<"databox "<<tostring(i)<<": "<<cbs[i]->dataBox<<eol;
    }

  for(Number n=1; n<=niter; n++)
    {
      bool view= viewStep!=0 && ((n-1)% viewStep ==0 || n==niter);
      if(view)
        thePrintStream<<" iteration "<<n<<eol<<" ---------"<<eol;
      for(Number i=0; i<nbox; i++)
        {
          // solve obstacle i
          ComputeBox& cbi=*cbs[i];
          cbi.iter=n;
          cbi.update_obs=true;
          cbi.solve();
          for(Number j=0; j<nbox; j++) // contribution from cbi to cbj
            if(j!=i)
              {
                ComputeBox& cbj=*cbs[j];
                cbi.computeOn(cbj);
                dbs[j] += cbj.dataBox;
                if(n==1 && i==0)
                  cbj.dataBox = dbs[j];  // at first iteration take for cbS the incident field + the diffracted field by cbL
                thePrintStream<<"databox "<<tostring(j)<<": "<<cbj.dataBox<<eol;
              }
        }

    } //end of iterative loop

  //update cumulative data only if field has not been computed
  for(Number i=0; i<nbox; i++)
    {
      cbs[i]->dataBox=dbs[i];
      if(!cbs[i]->hasComputedField())
        cbs[i]->solve();
    }

}


//=====================================================================================================================
// mesh stuff
//=====================================================================================================================
enum ObstacleType {_noScatterer,_distantDisk,_distantTriangle,_distantSquare,_distantCrescent,_stickCrescent,_stickTriangle,
                   _stickSquare,_stickCrescentSigma,_stickTriangleSigma, _distant2Disks, _distantCrescentND, _distantCrescentND2, _diskND,
                   _crossDisk,_egg,_conesphere
                  };

String obstacleName(ObstacleType obst)
{
  switch(obst)
    {
      case _noScatterer :
        return "noScatterer" ;
      case _distantDisk :
        return "disk" ;
      case _distant2Disks :
        return "two disks" ;
      case _distantTriangle :
        return "triangle" ;
      case _distantSquare :
        return "square" ;
      case _distantCrescent :
        return "crescent" ;
      case _stickCrescent :
        return "stick_crescent" ;
      case _stickTriangle :
        return "stick_triangle" ;
      case _stickSquare :
        return "stick_square" ;
      case _stickCrescentSigma :
        return "stick_crescent_sigma" ;
      case _stickTriangleSigma :
        return "stick_triangle_sigma" ;
      case _distantCrescentND :
        return "distant_Crescent_ND";
      case _distantCrescentND2 :
        return "distant_Crescent_ND2";
      case _diskND :
        return "disk_ND";
      case _crossDisk :
        return "cross_disk";
      case _egg:
         return "egg";
      case _conesphere:
         return "conesphere";
      default:
        break;
    }
  return "";
}

Geometry circleByArcs(const Point& c, Real radius, Real hs, const String& name)
{
  Point S1=c+Point(radius,0.), S2=c+Point(0.,radius), S3=c+Point(-radius,0.), S4=c+Point(0.,-radius);
  CircArc c1(_center=c,_v1=S1,_v2=S2,_hsteps=hs,_domain_name=name);
  CircArc c2(_center=c,_v1=S2,_v2=S3,_hsteps=hs,_domain_name=name);
  CircArc c3(_center=c,_v1=S3,_v2=S4,_hsteps=hs,_domain_name=name);
  CircArc c4(_center=c,_v1=S4,_v2=S1,_hsteps=hs,_domain_name=name);
  return c1+c2+c3+c4;
}

// Function generating mesh of the only large obstacle
void doMeshLargeDisk(const Point& centerL, Real radiusL, Real hsize, Mesh& meshLargeObs, Mesh& meshSmallObs, Real xr, bool view2d, Mesh& meshR)
{
  //computation mesh
  Disk largeObs(_center=centerL, _radius=radiusL, _hsteps=hsize, _domain_name="LargeObs",_side_names="obstacle");
  Disk virtObs(_center=centerL, _radius=1.1*radiusL, _hsteps=hsize, _domain_name="VirtualObs",_side_names="sigma");
  meshLargeObs.buildMesh(largeObs+virtObs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  Disk smallObs(_center=centerL, _radius=radiusL, _hsteps=hsize, _domain_name="SmallObs", _side_names="gamma");
  meshSmallObs.buildMesh(smallObs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule); //same as LargeObs
  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      meshR.buildMesh(obsR-largeObs, _triangle, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
}

// Function generating mesh of the only large obstacle: ellipse (x0+aRcos(t),y0+bRsin(t)) rotated with theta angle
void doMeshLargeEllipse(const Point& centerL, Real radiusL, Real a, Real b, Real theta, Real hsize, Mesh& meshLargeObs, Mesh& meshSmallObs, Real xr, bool view2d, Mesh& meshR)
{
  //computation mesh
  Ellipse largeObs(_center=centerL, _xlength=a*radiusL, _ylength=b*radiusL, _hsteps=hsize, _domain_name="LargeObs",_side_names="obstacle");
  Ellipse virtObs(_center=centerL,  _xlength=1.1*a*radiusL, _ylength=1.1*b*radiusL, _hsteps=hsize, _domain_name="VirtualObs",_side_names="sigma");
  if(theta!=0)
    {
      largeObs.rotate2d(centerL,theta);
      virtObs.rotate2d(centerL,theta);
    }
  meshLargeObs.buildMesh(largeObs+virtObs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  Disk smallObs(_center=centerL, _radius=radiusL, _hsteps=hsize, _domain_name="SmallObs", _side_names="gamma");
  meshSmallObs.buildMesh(smallObs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule); //same as LargeObs
  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      meshR.buildMesh(obsR-largeObs, _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
}

// Function generating mesh of the only large obstacle: circle
void doMeshCircle(const Point& c, Real r, Real hsize, Mesh& meshLargeObs, Mesh& meshSmallObs, Real xr, bool view2d, Mesh& meshR)
{
  Geometry obs = circleByArcs(c, r, hsize, "obstacle");
  meshLargeObs.buildMesh(obs, _segment, 1, gmsh, _defaultPattern, _noSplitRule);
  if(view2d)
    {
      Disk obsR(_center=c, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      meshR.buildMesh(obsR-surfaceFrom(obs,"Omega_int",true), _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(c, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
  }

// Function generating mesh of a 2D cone-sphere
/* mesh wedge with angle angle merged with a disk sector

             -----\
             ------\   pi/2 < angle <pi
             wedge  \_____________________
             -------/(0,0)
             ------/
             -----/
    may be translated and rotated around apex with angle theta (not implemented)
  */
  void doMeshConeSphere(Real angle, Real dist, Real hsize, Mesh& meshLargeObs, Mesh& meshSmallObs, Real xr, Point& Cr, bool view2d, Mesh& meshR, const Point& apex=Point(0.,0.), Real theta=0)
  {
      Real R=dist*std::sin(angle);
      Point O(0.,0.), C(-dist,0.), A(-dist-R,0.);
      Point P(-dist+R*std::sin(angle),-R*std::cos(angle)), Q(-dist+R*std::sin(angle),R*std::cos(angle));
      Segment Sp(_v1=O,_v2=P,_hsteps=Reals(hsize/4,hsize),_domain_name="GammaP");
      Segment Sm(_v1=Q,_v2=O,_hsteps=Reals(hsize,hsize/4),_domain_name="GammaM");
      CircArc Cap(_center=C,_v1=P,_v2=A,_hsteps=Reals(hsize,hsize),_domain_name="GammaC");
      CircArc Cam(_center=C,_v1=A,_v2=Q,_hsteps=Reals(hsize,hsize),_domain_name="GammaC");
      Geometry obs=Sp+Cap+Cam+Sm; obs.domName()="obstacle";
//      Segment Sa(_v1=P,_v2=Q,_hsteps=Reals(hsize,hsize),_domain_name="GammaC");
//      Geometry obs=Sp+Sa+Sm;
      meshLargeObs.buildMesh(obs, _segment, 1, gmsh, _defaultPattern, _noSplitRule);
      //meshLargeObs.domain("#Omega").rename("obstacle");
      Cr=Point((-dist-R)/2,0.);  // exterior disk center
      //representation mesh
      if(view2d)
      {
        Disk obsR(_center=Cr, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
        meshR.buildMesh(obsR-surfaceFrom(obs,"Omega_int",true), _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
      }
      else
      {
        Geometry obsR = circleByArcs(Cr, xr, hsize*1.5, "side_ext");
        meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
      }
  }


/* Function generating the meshes of the small object, the large obstacle and the fictious boundary (sigma) embedding the small object
   centerL, radiusL : geometric characteristic of the large obstacle (disk)
   objectS : geometry related to the small sticked object defined relatively to the origin
   centerS : center of objectS (used for construction of sigma)
   radiusS : radius of a disk strictly containing the objectS (used for construction of sigma)
   hsize : characteristic size of the large obstacle
   xr : radius of observation domain
   meshLargeObs : 1D mesh of the large obstacle
   meshSmallObs : 1D mesh of the small obstacle
   meshR : 2D mesh of the observation area
*/
void doMeshDistantObject(Point& centerL, Real radiusL, Geometry& objectS, const Point& centerS, Real radiusS, Real hsize, Real xr, bool view2d, Mesh& meshLargeObs, Mesh& meshSmallObs, Mesh& meshR)
{
  Disk largeObs(_center=centerL, _radius=radiusL, _hsteps=hsize, _domain_name="LargeObs",_side_names="obstacle");
  Disk virtObs(_center=centerS, _radius=radiusS, _hsteps=hsize/4, _domain_name="VirtualObs",_side_names="sigma");
  meshLargeObs.buildMesh(largeObs+objectS, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  meshSmallObs.buildMesh(objectS+virtObs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      meshR.buildMesh(obsR-(largeObs+surfaceFrom(objectS,"",true)), _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
}

/* Function generating the meshes of the sticked object, the large obstacle and the fictious boundary (sigma) embedding the small object
   centerL, radiusL : geometric characteristic of the large obstacle (disk)
   theta : angle defining the size of the interface
   objectS : geometry related to the small sticked object defined relatively to the origin
   alpha : angle of global rotation of the small object
   hsize : characteristic size of the large obstacle
   xr: radius of observation domain
   meshLargeObs : 1D mesh of the large obstacle
   meshSmallObs : 1D mesh of the small obstacle
   meshR : 2D mesh of the observation area
*/
void doMeshStickObject(Point& centerL, Real radiusL, Geometry& objectS, Real theta, Real alpha, Real hsize, Real xr, bool view2d, Mesh& meshLargeObs, Mesh& meshSmallObs, Mesh& meshR)
{
  // Characteristic translation and rotation of crescent w.r.t large disc
  Rotation2d r(centerL,alpha);
  Point M1=r.apply(Point(radiusL*cos(theta), -radiusL*sin(theta))),  M2=r.apply(Point(radiusL*cos(theta), radiusL*sin(theta))),
        M3=r.apply(Point(-radiusL,0.)), M4=r.apply(Point(0.,-radiusL));
  CircArc c1(_center=centerL,_v1=M1,_v2=M2,_hsteps=hsize/4.,_domain_name="interface");
  CircArc c2(_center=centerL,_v1=M2,_v2=M3,_hsteps=hsize,_domain_name="obstacle");
  CircArc c3(_center=centerL,_v1=M3,_v2=M4,_hsteps=hsize,_domain_name="obstacle");
  CircArc c4(_center=centerL,_v1=M4,_v2=M1,_hsteps=hsize,_domain_name="obstacle");
  Geometry cs = c1+c2+c3+c4+objectS;
  Geometry cs_ext = c1+objectS;
  meshLargeObs.buildMesh(cs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  meshSmallObs.buildMesh(cs_ext, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      Geometry cs2 = objectS+c2+c3+c4;
      meshR.buildMesh(obsR-surfaceFrom(cs2,"Omega_int",true), _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {std::set<MeshOption> mos; mos.insert(_defaultMeshOption);
      Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
}

/* Function generating the meshes of a small disk crossing a large disk
   centerL, radiusL : geometric characteristic of the large obstacle (disk)
   radiusS : radius of the small disk
   d : distance from two disks -> centerS = centerL + radiusL + d (for crossing d  < radiusS)
   alpha : angle of global rotation of the small object
   hsize : characteristic size of the large obstacle
   xr: radius of observation domain
   meshLargeObs : 1D mesh of the large obstacle
   meshSmallObs : 1D mesh of the small obstacle
   meshR : 2D mesh of the observation area
*/
void doMeshCrossDisk(Point& centerL, Real radiusL, Real radiusS, Real d, Real alpha, Real hsize, Real xr, bool view2d, Mesh& meshLargeObs, Mesh& meshSmallObs, Mesh& meshR)
{
  // Characteristic translation and rotation of crescent w.r.t large disc
  Real x=0.5*(radiusL+d+(radiusL*radiusL-radiusS*radiusS)/(radiusL+d)), y=std::sqrt(radiusL*radiusL-x*x);
  Rotation2d r(centerL,alpha);
  Point M1=r.apply(Point(x,-y)),  M2=r.apply(Point(x,y)), M3=r.apply(Point(-radiusL,0.)), M4=r.apply(Point(0.,-radiusL)), M5=r.apply(Point(radiusL+radiusS+d,0.));
  Point centerS=r.apply(Point(radiusL+d,0.));
  CircArc c1(_center=centerL,_v1=M1,_v2=M2,_hsteps=hsize/2.,_domain_name="interfaceL");
  CircArc c2(_center=centerL,_v1=M2,_v2=M3,_hsteps=Reals(hsize/2.,hsize),_domain_name="obstacle");
  CircArc c3(_center=centerL,_v1=M3,_v2=M4,_hsteps=hsize,_domain_name="obstacle");
  CircArc c4(_center=centerL,_v1=M4,_v2=M1,_hsteps=Reals(hsize, hsize/2.),_domain_name="obstacle");
  CircArc c5(_center=centerS,_v1=M1,_v2=M5,_hsteps=hsize/2.,_domain_name="gamma");
  CircArc c6(_center=centerS,_v1=M5,_v2=M2,_hsteps=hsize/2,_domain_name="gamma");
  CircArc c7(_center=centerS,_v1=M2,_v2=M1,_hsteps=hsize/2,_domain_name="interfaceS");
  Geometry cL = c1+c2+c3+c4+c5+c6;
  meshLargeObs.buildMesh(cL, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  Geometry cS = c5+c6+c7;
  meshSmallObs.buildMesh(cS, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      Geometry cs2 = c2+c3+c4+c5+c6;
      meshR.buildMesh(obsR-surfaceFrom(cs2,"Omega_int",true), _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
}

// Function generating the meshes of the sticked crescent, the large obstacle and the fictitious boundary (sigma) embedding the crescent
void doMeshStickCrescentSigma(Point& centerL, Point& centerS, Real radiusL, Real radiusS, Real theta, Real alpha, Real hsize, Real k, Real xr, bool view2d,
                              Mesh& meshLargeObs, Mesh& meshSmallObs, Mesh& meshR, Point& M1, Point& M2)
{
  // Characteristic translation and rotation of crescent w.r.t large disc
  Real dtheta=theta/2;
  Rotation2d r(centerL,alpha);
  M1=r.apply(Point(radiusL*cos(theta), -radiusL*sin(theta)));
  M2=r.apply(Point(radiusL*cos(theta), radiusL*sin(theta)));
  Point M3=r.apply(Point(-radiusL,0.)), M4=r.apply(Point(0.,-radiusL)), M12=r.apply(Point(radiusL+radiusS,0.));
  Point M1e=r.apply(Point(radiusL*cos(theta+dtheta), -radiusL*sin(theta+dtheta))),  M2e=r.apply(Point(radiusL*cos(theta+dtheta), radiusL*sin(theta+dtheta)));
  Point centerSr=r.apply(centerS);
  CircArc c1(_center=centerL,_v1=M1,_v2=M2,_hsteps=hsize/3.,_domain_name="interface");
  CircArc c1e(_center=centerL,_v1=M1e,_v2=M1,_hsteps=Reals(hsize/3,hsize/3.),_domain_name="interface1");
  CircArc c2e(_center=centerL,_v1=M2,_v2=M2e,_hsteps=Reals(hsize/3.,hsize/3),_domain_name="interface2");
  CircArc c2(_center=centerL,_v1=M2e,_v2=M3,_hsteps=Reals(hsize/4.,hsize),_domain_name="obstacle");
  CircArc c3(_center=centerL,_v1=M3,_v2=M4,_hsteps=hsize,_domain_name="obstacle");
  CircArc c4(_center=centerL,_v1=M4,_v2=M1e,_hsteps=Reals(hsize,hsize/4),_domain_name="obstacle");
  CircArc c51(_center=centerSr,_v1=M1,_v2=M12,_hsteps=hsize/4.,_domain_name="gamma");
  CircArc c52(_center=centerSr,_v1=M12,_v2=M2,_hsteps=hsize/4.,_domain_name="gamma");

  // sigma circle
  //Point M12e=r.apply(Point(radiusL+dist(M1e,centerSr),0.));
  //CircArc c12a(_center=centerSr,_v1=M2e,_v2=M12e,_hsteps=hsize/4.,_domain_name="sigma");
  //CircArc c12b(_center=centerSr,_v1=M12e,_v2=M1e,_hsteps=hsize/4.,_domain_name="sigma");
  //Geometry cs_ext = c1e+c51+c52+c2e+c1+c12a+c12b;
  //Geometry cs_int = c1e+c1+c2e+c2+c3+c4;

  // sigma triangle
  //Point M12e=r.apply(Point(radiusL+2*dist(M1e,centerSr),0.));
  //Segment c12a(_v1=M2e,_v2=M12e,_hsteps=hsize/4.,_domain_name="sigma");
  //Segment c12b(_v1=M12e,_v2=M1e,_hsteps=hsize/4.,_domain_name="sigma");
  //Geometry cs = c1e+c1+c2e+c2+c3+c4+c51+c52+c12a+c12b;
  //Geometry cs_ext = c1e+c51+c52+c2e+c1+c12a+c12b;
  //Geometry cs_int = c1e+c1+c2e+c2+c3+c4;

  // sigma square
  Point Q=r.apply(Point(dist(M1e,centerSr),0.));
  Point M1eq=M1e+Q, M2eq=M2e+Q;
  Segment c12a(_v1=M2e,_v2=M2eq,_hsteps=hsize/4.,_domain_name="sigma");
  Segment c12b(_v1=M2eq,_v2=M1eq,_hsteps=hsize/4.,_domain_name="sigma");
  Segment c12c(_v1=M1eq,_v2=M1e,_hsteps=hsize/4.,_domain_name="sigma");
  Geometry cs_ext = c1e+c51+c52+c2e+c1+c12a+c12b+c12c;
  Geometry cs_int = c1e+c1+c2e+c2+c3+c4+c51+c52;

  meshLargeObs.buildMesh(cs_int, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  meshSmallObs.buildMesh(cs_ext, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  meshLargeObs.printInfo();
  meshSmallObs.printInfo();

  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      Geometry cs2 = c51+c52+c2e+c2+c3+c4+c1e;
      meshR.buildMesh(obsR-surfaceFrom(cs2,"Omega_int",true), _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
}

// Function generating the meshes of the sticked crescent, the large obstacle and the fictitious boundary (sigma) embedding the crescent
void doMeshStickTriangleSigma(Point& centerL, Real radiusL, Real ht, Real theta, Real alpha, Real hsize, Real k, Real xr, bool view2d,
                              Mesh& meshLargeObs, Mesh& meshSmallObs, Mesh& meshR)
{
  // Characteristic translation and rotation of crescent w.r.t large disc
  Real dtheta=theta/5;
  Rotation2d r(centerL,alpha);
  Point M1=r.apply(Point(radiusL*cos(theta), -radiusL*sin(theta))),  M2=r.apply(Point(radiusL*cos(theta), radiusL*sin(theta))),
        M3=r.apply(Point(-radiusL,0.)), M4=r.apply(Point(0.,-radiusL)), M12=r.apply(Point(radiusL+ht,0.));
  Point M1e=r.apply(Point(radiusL*cos(theta+dtheta), -radiusL*sin(theta+dtheta))),  M2e=r.apply(Point(radiusL*cos(theta+dtheta), radiusL*sin(theta+dtheta)));
  Point M12e=r.apply(Point(((radiusL+ht)*sin(theta+dtheta)-radiusL*sin(dtheta))/sin(theta),0.));
  CircArc c1(_center=centerL,_v1=M1,_v2=M2,_hsteps=hsize/4.,_domain_name="interface");
  CircArc c1e(_center=centerL,_v1=M1e,_v2=M1,_hsteps=Reals(hsize/4,hsize/4.),_domain_name="interface1");
  CircArc c2e(_center=centerL,_v1=M2,_v2=M2e,_hsteps=Reals(hsize/4.,hsize/4),_domain_name="interface2");
  CircArc c1L(_center=centerL,_v1=M1,_v2=M2,_hsteps=hsize/4.,_domain_name="interface");
  CircArc c1eL(_center=centerL,_v1=M1e,_v2=M1,_hsteps=Reals(hsize/4,hsize/4.),_domain_name="interface1");
  CircArc c2eL(_center=centerL,_v1=M2,_v2=M2e,_hsteps=Reals(hsize/4.,hsize/4),_domain_name="interface2");
  CircArc c2(_center=centerL,_v1=M2e,_v2=M3,_hsteps=Reals(hsize/4.,hsize),_domain_name="obstacle");
  CircArc c3(_center=centerL,_v1=M3,_v2=M4,_hsteps=hsize,_domain_name="obstacle");
  CircArc c4(_center=centerL,_v1=M4,_v2=M1e,_hsteps=Reals(hsize,hsize/4),_domain_name="obstacle");
  Segment s51(_v1=M1,_v2=M12,_hsteps=hsize/4.,_domain_name="gamma");
  Segment s52(_v1=M12,_v2=M2,_hsteps=hsize/4.,_domain_name="gamma");
  Segment s12a(_v1=M2e,_v2=M12e,_hsteps=hsize/4.,_domain_name="sigma");
  Segment s12b(_v1=M12e,_v2=M1e,_hsteps=hsize/4.,_domain_name="sigma");
  Geometry cs = c1eL+c1L+c2eL+c2+c3+c4+s51+s52;
  Geometry cs_ext = c1e+c1+c2e+s51+s52+s12a+s12b;

  meshLargeObs.buildMesh(cs, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  meshSmallObs.buildMesh(cs_ext, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
  meshLargeObs.printInfo();
  meshSmallObs.printInfo();
  //representation mesh
  if(view2d)
    {
      Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
      Geometry cs2 = s51+s52+c2e+c2+c3+c4+c1e;
      meshR.buildMesh(obsR-surfaceFrom(cs2,"Omega_int",true), _triangle, 1, gmsh, _defaultPattern, _noSplitRule);
    }
  else
    {
      Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
      meshR.buildMesh(obsR, _segment, 1, _gmsh, _defaultPattern, _noSplitRule);
    }
}

//=====================================================================================================================
Kernel G;

// incident field functions
// Function evaluating incident plane wave for Dirichlet Boundary Condition
Complex uinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kr=kx*p(1)+ky*p(2);
  return exp(i_*kr);
  //return Helmholtz2d(0.*p,p,pa);
}

Complex muinc(const Point& p, Parameters& pa = defaultParameters)
{
  Real kr=kx*p(1)+ky*p(2);
  return -exp(i_*kr);
  //return -Helmholtz2d(0.*p,p,pa);
}

/* Function evaluating normal derivative of incident plane wave: for Neumann Boundary Condition */
Complex duinc(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real kr = kx*x+ky*y;
  const Vector<Real>& n = getN();
  return i_*kx*exp(i_*kr)*n(1);
}

// Function evaluating a linear combination of the plane wave and its normal derivative for Fourier Boundary condition
Complex fuinc(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real kr = kx*x+ky*y;
  Complex mu = pa("mu"), nu=pa("nu");
  const Vector<Real>& n = getN();
  return (nu*i_*kx*n(1)+mu)*exp(i_*kr);
}

// Function evaluating the gradient of incident plane wave
Vector<Complex> guinc(const Point& P, Parameters& pa = defaultParameters)
{
  Real x=P(1), y=P(2);
  Real kr = kx*x+ky*y;
  Complex a=i_*exp(i_*kr);
  Vector<Complex> g(2);
  g(1)=a*kx;
  g(2)=a*ky;
  return g;
}

Real kra;

// Function evaluating the wedge diffraction
Complex wedgeSOM(const Point& P, Parameters& pa = defaultParameters)
{
  Point Q=toPolar(P);
  return wedgeDiffractionSOM(k*Q(1),Q(2),wa,ia+pi_,_wedgeField,_NeumannEc);
}

Complex wedgeSD(const Point& P, Parameters& pa = defaultParameters)
{
  Point Q=toPolar(P);
  return wedgeDiffractionSD(k*Q(1),Q(2),wa,ia+pi_,_wedgeField,kra,_NeumannEc);
}

//=====================================================================================================================
// main program
//=====================================================================================================================
void dev_BEM_BEM(int argc, char* argv[], bool check)
{
  String rootname = "dev_BEM_BEM";
  trace_p->push(rootname);
  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(70);
  String errors;
  Number nbErrors = 0;

  Options opts;
  opts.addOption("k",50.);
  opts.addOption("ellipse_a","ae",1.);
  opts.addOption("ellipse_b","be",1.);
  opts.addOption("ellipse_c","ce",1.);
  opts.addOption("mesh_refinement","mr",10);
  opts.addOption("ellipse_rotation_angle_(deg)","nue",0.);
  opts.addOption("use_(i|n)","isn",true);
  opts.addOption("wedge_contribution","wc",true);
  opts.addOption("compute_BEM","BEM",true);
  opts.addOption("incidence_angle","inc",0.);  //exp(ik(cos(a)x+sin(a)y))
  opts.addOption("shape","sh","");
  opts.addOption("wedge_angle","wa",135);  //in degree
  opts.addOption("wedge_length","wl",1.);
  opts.addOption("kr_asymptotic","kra",0.);
  opts.addOption("nb_of_threads","nbt",0);
  opts.addOption("view_2d","v2d",false);

  opts.parse("opts.txt");

  k=opts("k");
  Number mr=opts("mesh_refinement");
  ae=opts("ellipse_a");
  be=opts("ellipse_b");
  ce=opts("ellipse_c");
  nue =opts("ellipse_rotation_angle_(deg)")*pi_/180;
  bool isn=opts("use_(i|n)").get_b();
  bool withBEM=opts("compute_BEM").get_b();
  ia=opts("incidence_angle")*pi_/180;
  String shape=opts("shape");
  wa=opts("wedge_angle")*pi_/180;
  Real wl=opts("wedge_length");
  bool wc=opts("wedge_contribution").get_b();
  kra=opts("kr_asymptotic");
  int nbt = opts("nb_of_threads");
  bool view2d=opts("view_2d").get_b();
  numberOfThreads(nbt);


  //theCout<<" compute with k="<<k<<", mr="<<mr<<", ae="<<ae<<", be="<<be<<", nue="<<nue<< "("<<nue*180/pi_<<"°), isn="<<isn<<", BEM="<<withBEM<<eol;

  //associate parametrization functions
//  parametrization=ellipse_parametrization;
//  toParameter=ellipse_toParameter;
//  flength=ellipse_flength;
//  fcurvature=ellipse_fcurvature;
//  fnormal=ellipse_fnormal;
//  ftangent=ellipse_ftangent;

  //define parameters

  Real k2=k*k;
  Real wavelength = 2*pi_/k;
  kx=k*std::cos(ia), ky=k*std::sin(ia);
  Parameters pars;
  pars << Parameter(k,"k") << Parameter(k, "kx") << Parameter(0., "ky"); // kx, ky
  Complex mu = i_, lambda = 0.5*i_*k, nu=1.;
  mu=1.;  nu=0.;
  pars << Parameter(mu,"mu");
  pars << Parameter(nu,"nu");
  pars << Parameter(lambda,"lambda");
  //define Kernel
  G=Helmholtz2dKernel(pars);
  Kernel Gsym;
  Kernel* GL=&G, *GS=&G;

  // Mesh characteristics
  Real hsize = wavelength/mr;
  Point centerL(0.,0.);
  Real radiusL=k*wavelength;
  Point centerS=Point(radiusL,0.);
  Real dapp_gam = wavelength;
  Real theta = dapp_gam/(2*radiusL);
  Real ct=cos(theta), st=sin(theta);
  Real radiusS=radiusL*std::sqrt(2-2*ct);
  Real alpha = 5*pi_/4;
  Real hT=radiusL*st;
  Rotation2d r(centerL,alpha);
  Point M1, M2;
  Mesh mR, meshLargeObs, meshSmallObs, mext;

//  radiusL=0.347;
//  centerL=Point(-2.,0.);
//  shape="circle";
//  ae=1; be=1.;

  Real xr = 1.5*std::max(ae,1.)*radiusL;
  xr=2;
  Complex amp=std::sqrt(k*pi_*xr/2);
  Point Cr(ae,0.); C(1)=ae;

  ObstacleType obst=_noScatterer;
  switch(obst)
    {
      case _noScatterer :
          if(shape=="egg")  //rosillo
          {
//           doMeshLargeEllipse(centerL, 2*radiusL, 1, 1, 0, hsize, meshLargeObs, meshSmallObs, xr, view2d, mR);
//           for (Number i=0; i < meshLargeObs.nodes.size(); ++i)
//             {
//                Point& p=meshLargeObs.nodes[i];
//                Real t = atan2(p(2),p(1));
//                meshLargeObs.nodes[i]=parametrization(t);
//             }
            Parametrization rosilloPar(0,1,rosilloParametrization,pars);
            rosilloPar.curvature_p = rosilloCurvature;
            rosilloPar.length_p = rosilloLength;
            rosilloPar.invParametrization_p=rosilloToParameter;
            rosilloPar.normal_p=rosilloNormal;
            ParametrizedArc parc(_parametrization = rosilloPar, _partitioning=_linearPartition, _nbParts=60,_hsteps=0.1,_domain_name="obstacle");
            meshLargeObs.buildMesh(parc,_segment,1,gmsh, _defaultPattern, _noSplitRule);
            Disk obsR(_center=Cr, _radius=xr, _hsteps=hsize*1.5, _domain_name="side_ext");
            if(view2d)
            {
                Disk obsR2(_center=Cr, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
                theLogStream<<surfaceFrom(parc,"Omega_int",true)<<eol<<obsR2-surfaceFrom(parc,"Omega_int",true)<<eol<<std::flush;
                mR.buildMesh(obsR2-surfaceFrom(parc,"Omega_int",true),_triangle, 1, _gmsh, _defaultPattern, _noSplitRule);
            }
            else mR.buildMesh(obsR,_segment,1,_gmsh, _defaultPattern,_noSplitRule);
         }
         else
         {
             if (shape=="conesphere") doMeshConeSphere(wa,wl,hsize,meshLargeObs, meshSmallObs, xr, Cr,view2d, mR);
             else if(shape=="circle") doMeshCircle(centerL,radiusL,hsize,meshLargeObs, meshSmallObs, xr,view2d, mR);
             else doMeshLargeEllipse(centerL, 2*radiusL, ae, be, nue, hsize, meshLargeObs, meshSmallObs, xr, view2d, mR);
         }
        break;

      case _distantDisk :
      case _distant2Disks :
      case _distantCrescent :
      case _distantCrescentND :
      case _distantTriangle :
      case _distantSquare :
      case _diskND :
        {
          Real trans = wavelength;
          M1=r.apply(Point(radiusL*ct+trans, -radiusL*st));
          M2=r.apply(Point(radiusL*ct+trans,  radiusL*st));
          if(obst==_distantDisk)
            {
              Point centerSr = r.apply(Point(radiusL+radiusS+trans,0.));
              Geometry obj = circleByArcs(centerSr,radiusS,hsize/4,"gamma");
              doMeshDistantObject(centerL, radiusL, obj, centerSr, std::min(1.2*radiusS, radiusS+trans/2), hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }
          if(obst==_distantCrescent)
            {
              Point M12 = r.apply(Point(radiusL+radiusS+trans,0.)), centerSr = r.apply(Point(radiusL+trans,0.));
              CircArc c1(_center=centerSr,_v1=M1,_v2=M12,_hsteps=hsize/4,_domain_name="gamma");
              CircArc c2(_center=centerSr,_v1=M12,_v2=M2,_hsteps=hsize/4,_domain_name="gamma");
              CircArc c3(_center=r.apply(Point(trans,0.)),_v1=M2,_v2=M1,_hsteps=hsize/4,_domain_name="gamma");
              Geometry obj = c1+c2+c3;
              doMeshDistantObject(centerL, radiusL, obj, centerSr, 1.2*dist(centerSr,M1), hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }
          if(obst==_distantTriangle) //equilateral
            {
              Point M12=r.apply(Point(radiusL+trans+sqrt(3)*hT,0.));
              Segment s1(_v1=M1,_v2=M12,_hsteps=hsize/4,_domain_name="gamma");
              Segment s2(_v1=M12,_v2=M2,_hsteps=hsize/4,_domain_name="gamma");
              Segment s3(_v1=M2,_v2=M1,_hsteps=hsize/4, _domain_name="gamma");
              Geometry obj = s1+s2+s3;
              Point centerO=(M1+M2+M12)/3;
              doMeshDistantObject(centerL, radiusL, obj, centerO, 1.2*dist(centerO,M1), hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }
          if(obst==_distantSquare)
            {
              Point M11=M1+r.apply(Point(2*hT,0.)), M22=M2+r.apply(Point(2*hT,0.));
              Segment s1(_v1=M1,_v2=M11,_hsteps=hsize/4,_domain_name="gamma");
              Segment s2(_v1=M11,_v2=M22,_hsteps=hsize/4,_domain_name="gamma");
              Segment s3(_v1=M22,_v2=M2,_hsteps=hsize/4,_domain_name="gamma");
              Segment s4(_v1=M2,_v2=M1,_hsteps=hsize/4,_domain_name="gamma");
              Geometry obj = s1+s2+s3+s4;
              Point centerO=(M1+M22)/2;
              doMeshDistantObject(centerL, radiusL, obj, centerO, 1.2*dist(centerO,M1), hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }
          if(obst==_distant2Disks)
            {
              Point centerSr1 = r.apply(Point(radiusL+radiusS+trans,0.));
              Geometry obj1 = circleByArcs(centerSr1,radiusS,hsize/4,"gamma1");
              Real alpha2 = 9*pi_/8;
              Rotation2d r2(centerL,alpha2);
              Point centerSr2 = r2.apply(Point(radiusL+radiusS+trans,0.));
              Geometry obj2 = circleByArcs(centerSr2,radiusS,hsize/4,"gamma2");
              Geometry objs=obj1+obj2;
              doMeshDistantObject(centerL, radiusL, objs, (centerSr1+centerSr2)/2., norm((centerSr1-centerSr2)/2.)+2.5*radiusS, hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }
          if(obst==_diskND)
            {
              Disk largeObs(_center=centerL, _radius=radiusL, _hsteps=hsize, _domain_name="disk",_side_names=Strings("obstacle","obstacle","gamma_D","obstacle"));
              Disk virtObs(_center=centerL, _radius=1.1*radiusL, _hsteps=hsize, _domain_name="omega",_side_names="sigma");
              Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
              std::set<MeshOption> mos;
              mos.insert(_defaultMeshOption);
              meshLargeObs.buildMesh(virtObs-largeObs,_triangle, 1, _gmsh, _defaultPattern, _noSplitRule);
              //meshLargeObs.buildMesh(largeObs,_segment,1,_gmsh, _defaultPattern, _noSplitRule);
              mR.buildMesh(obsR+virtObs-largeObs,_triangle, 1, _gmsh, _defaultPattern, _noSplitRule);
            }
          if(obst==_distantCrescentND)
            {
              Point M12 = r.apply(Point(radiusL+radiusS+trans,0.)), centerSr = r.apply(Point(radiusL+trans,0.));
              CircArc c1(_center=centerSr,_v1=M1,_v2=M12,_hsteps=hsize/4,_domain_name="gamma_N");
              CircArc c2(_center=centerSr,_v1=M12,_v2=M2,_hsteps=hsize/4,_domain_name="gamma_N");
              CircArc c3(_center=r.apply(Point(trans,0.)),_v1=M2,_v2=M1,_hsteps=hsize/4,_domain_name="gamma_D");
              Geometry obj = c1+c2+c3;
              doMeshDistantObject(centerL, radiusL, obj, centerSr, 1.2*dist(centerSr,M1), hsize/2, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }

          if(obst==_distantCrescentND2)
            {
              double x1=radiusL+trans+(trans*trans-radiusS*radiusS)/(2*radiusL), y1=std::sqrt(radiusS*radiusS-(x1-radiusL)*(x1-radiusL));
              Point M1t=r.apply(Point(x1,-y1));
              Point M2t=r.apply(Point(x1, y1));
              Point M12 = r.apply(Point(radiusL+radiusS,0.)), centerSr=r.apply(centerS);
              CircArc c1(_center=centerSr,_v1=M1t,_v2=M12,_hsteps=hsize/4,_domain_name="gamma_N");
              CircArc c2(_center=centerSr,_v1=M12,_v2=M2t,_hsteps=hsize/4,_domain_name="gamma_N");
              CircArc c3(_center=centerL,_v1=M2t,_v2=M1t,_hsteps=hsize/4,_domain_name="gamma_D");
              Geometry obj = c1+c2+c3;
              Point M1=r.apply(Point(radiusL*cos(theta), -radiusL*sin(theta))),  M2=r.apply(Point(radiusL*cos(theta), radiusL*sin(theta))),
                    M3=r.apply(Point(-radiusL,0.)), M4=r.apply(Point(0.,-radiusL));
              CircArc l1(_center=centerL,_v1=M1,_v2=M2,_hsteps=hsize/3.,_domain_name="interface");
              CircArc l2(_center=centerL,_v1=M2,_v2=M3,_hsteps=hsize,_domain_name="obstacle");
              CircArc l3(_center=centerL,_v1=M3,_v2=M4,_hsteps=hsize,_domain_name="obstacle");
              CircArc l4(_center=centerL,_v1=M4,_v2=M1,_hsteps=hsize,_domain_name="obstacle");
              Geometry cs = l1+l2+l3+l4;
              meshLargeObs.buildMesh(cs+obj,_segment,1,_gmsh, _defaultPattern, _noSplitRule);
              meshSmallObs.buildMesh(obj,_segment,1,_gmsh, _defaultPattern, _noSplitRule);
              if(view2d)
                {
                  Disk obsR(_center=centerL, _radius=xr, _hsteps=hsize*1.5, _domain_name="Omega_ext",_side_names="side_ext");
                  Geometry cs2 = obj+cs;
                  mR.buildMesh(obsR-surfaceFrom(cs2,"Omega_int",true),_triangle,1,gmsh, _defaultPattern, _noSplitRule);
                }
              else
                {
                  Geometry obsR = circleByArcs(centerL, xr, hsize*1.5, "side_ext");
                  mR.buildMesh(obsR,_segment,1,_gmsh, _defaultPattern, _noSplitRule);
                }
            }
        }
        break;
      case _stickCrescent  :
      case _stickTriangle  :
      case _stickSquare    :
      case _stickCrescentSigma :
      case _stickTriangleSigma :
        {
          M1=r.apply(Point(radiusL*ct, -radiusL*st));
          M2=r.apply(Point(radiusL*ct,  radiusL*st));
          if(obst==_stickCrescent)
            {
              Point M12=r.apply(Point(radiusL+radiusS,0.)), centerSr=r.apply(centerS);
              CircArc c51(_center=centerSr,_v1=M1,_v2=M12,_hsteps=hsize/4,_domain_name="gamma");
              CircArc c52(_center=centerSr,_v1=M12,_v2=M2,_hsteps=hsize/4,_domain_name="gamma");
              Geometry obj = c51+c52;
              doMeshStickObject(centerL, radiusL, obj, theta, alpha, hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);

            }
          if(obst==_stickTriangle)
            {
              Point M12=r.apply(Point(radiusL+hT,0.));
              Segment s51(_v1=M1,_v2=M12,_hsteps=hsize/4,_domain_name="gamma");
              Segment s52(_v1=M12,_v2=M2,_hsteps=hsize/4,_domain_name="gamma");
              Geometry obj = s51+s52;
              doMeshStickObject(centerL, radiusL, obj, theta, alpha, hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }
          if(obst==_stickSquare)
            {
              Point M11=M1+r.apply(Point(hT,0.)), M22=M2+r.apply(Point(hT,0.));
              Segment s51(_v1=M1,_v2=M11,_hsteps=hsize/4,_domain_name="gamma");
              Segment s50(_v1=M11,_v2=M22,_hsteps=hsize/4,_domain_name="gamma");
              Segment s52(_v1=M22,_v2=M2,_hsteps=hsize/4,_domain_name="gamma");
              Geometry obj = s51+s50+s52;
              doMeshStickObject(centerL, radiusL, obj, theta, alpha, hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
            }
          if(obst==_stickCrescentSigma)
            doMeshStickCrescentSigma(centerL,centerS,radiusL,radiusS,theta,alpha,hsize,k,xr,view2d, meshLargeObs, meshSmallObs,mR, M1, M2);
          if(obst==_stickTriangleSigma)
            doMeshStickTriangleSigma(centerL,radiusL,hT,theta,alpha,hsize,k,xr,view2d, meshLargeObs, meshSmallObs,mR);
          Gsym = Helmholtz2dHalfPlaneKernel(k, M2-M1, (M1+M2)/2,_Neumann);
          GS = &Gsym;
        }
        break;
      case _crossDisk :
        {
          Real d = 0;
          doMeshCrossDisk(centerL, radiusL, radiusS, d, alpha, hsize, xr, view2d, meshLargeObs, meshSmallObs, mR);
        }
      default:
        break;
    }

  meshLargeObs.saveToFile("mLargeObs.msh",_msh);
  meshSmallObs.saveToFile("mSmallObs.msh",_msh);
  mR.saveToFile("mR.msh",_msh);

  GeomDomain* obsp=nullptr;
  if(view2d)
    {
      if(obst==_diskND)  obsp=mR.domainP("#Omega");
      else               obsp = mR.domainP("Omega_ext");
    }
  else obsp = mR.domainP("side_ext");
  Domain omegaobs = *obsp;
  Domain obstacle=meshLargeObs.domain("obstacle");     // large obstacle
  GeomDomain* gamma = meshSmallObs.domainP("gamma");   // small obstacle, may be not exist
  theCout<<obstacle.geometry()->connectedParts()<<eol<<std::flush;
  theCout<<obstacle.parametrization()<<eol;
  std::map<Number, Geometry*>::iterator itm=obstacle.geometry()->components().begin();
  for(;itm!=obstacle.geometry()->components().end();++itm)
    theCout<<itm->first<<" "<<itm->second->domName()<<": "<<itm->second->asString()<<eol;
  std::map<Number, std::vector<Number> >::const_iterator itmg= obstacle.geometry()->geometries().begin();
  for(;itmg!=obstacle.geometry()->geometries().end();++itmg)
    theCout<<"geometry "<<itmg->first<<": "<<itmg->second<<eol;

  meshLargeObs.printInfo(cout);


  // Define observation space
  Space Vobs(_domain=omegaobs, _interpolation=P1, _name="V1", _notOptimizeNumbering);
  Unknown uobs(Vobs, _name="uobs");
  TestFunction vobs(uobs, _name="vobs");

  // Integration Methods
  IntegrationMethods IMieDuf(_method=Duffy,_order=20,_quad=_defaultRule,_order=15,_bound=1.,_quad=_defaultRule,_order=10,_bound=2.,_quad=_defaultRule,_order=8); // Duffy integration method
  IntegrationMethods IMir(_quad=_defaultRule,_order=20,_bound=1.,_quad=_defaultRule,_order=15,_bound=3.,_quad=_defaultRule,_order=10);
  IntegrationMethods IMir2(_method=LenoirSalles2dIR(),_functionPart=_singularPart,_bound=theRealMax,_quad=QuadratureIM(_GaussLegendreRule,10),_functionPart=_regularPart,_bound=theRealMax);

  // Define functions
  Function finc(uinc,pars);        // incident wave
  Function dfinc(duinc,pars);      // normal derivative of incident wave
  Function ffinc(fuinc,pars);      // Fourier derivative of incident wave
  Function ginc(guinc,pars);       // gradient of incident wave
  dfinc.require(_n);
  ffinc.require(_n);

  InterpolationType pt=P1;
  String meth="?";
  GeomDomain *SL, *gammaL, *sigma, *gammaN=nullptr, *gammaD=nullptr;

//  elapsedTime();
//  TermVector UdUTD_SOM(uobs,omegaobs,wedgeSOM);
//  elapsedTime("Usd sommerfeld");
//  saveToFile("udUTD_SOM", UdUTD_SOM, _format=_vtu);
//  elapsedTime();
//  TermVector UdUTD_SD(uobs,omegaobs,wedgeSD);
//  elapsedTime("Usd steepest");
//  saveToFile("udUTD_SD", UdUTD_SD, _format=_vtu);


//  Domain gammaP=mR.domain("GammaP");
//  Space VP(gammaP, Vobs);
//  const std::vector<FeDof>& dofs= VP.feDofs();
//  elapsedTime();
//  Number nd=dofs.size();
//  Reals krs(nd);
//  for(Number i=0;i<nd;i++) krs[i]=k*norm(dofs[i].coords());
//  elapsedTime();
//  Vector<Complex> curp=wedgeCurrentSD(krs,wa,1,ia,_wedgeField,kra,_NeumannEc);
//  elapsedTime("curp");
//  elapsedTime();
//  curp=wedgeCurrentSD_par(krs,wa,1,ia,_wedgeField,kra,_NeumannEc);
//  elapsedTime("curp par");
//  TermVector curpt(uobs,gammaP,curp);
//  saveToFile("curpt", curpt, _format=_vtu);

  // direct computations
  // =======================
  TermVector Ui(uobs,omegaobs,finc);
  TermVector cur_BEM;
  TermVector UdBEM,UtBEM;
  bool BEM1=withBEM, BEM2=false, BEM_CFIE=false, BEM_ND=false, BEM_ND_sigma=false, BEM_sigma=false, Kirchoff=false, Ray=false, UTD=true;
  if(BEM1 || BEM2 || BEM_CFIE || BEM_ND || BEM_ND_sigma|| BEM_sigma || Kirchoff || Ray || UTD) // direct resolution
    {
      gammaL = meshLargeObs.domainP("gamma");
      sigma=meshLargeObs.domainP("sigma");
      if(obst==_distant2Disks)
        gammaL=&merge(meshLargeObs.domain("gamma1"),meshLargeObs.domain("gamma2"),"gamma");
      if(obst==_stickCrescentSigma ||obst==_stickTriangleSigma)
        {
          Domain interface1=meshLargeObs.domain("interface1");
          Domain interface2=meshLargeObs.domain("interface2");
          SL = &merge(obstacle, interface1, interface2, *gammaL, "SL");
        }
      else if(obst==_distantCrescentND)
        {
          gammaN=&merge(obstacle, meshLargeObs.domain("gamma_N"), "SL");
          gammaD=meshLargeObs.domainP("gamma_D");
          SL = &merge(*gammaN, *gammaD, "SL");
        }
      else if(obst==_diskND)
        {
          gammaN=meshLargeObs.domainP("obstacle");
          gammaD=meshLargeObs.domainP("gamma_D");
          SL = &merge(*gammaN, *gammaD, "SL");
        }
      else if(obst==_noScatterer || gammaL==0)
        SL = &obstacle;
      else
        SL = &merge(obstacle,*gammaL,"SL");
      SL->setNormalOrientation(_towardsInfinite);
      SL->saveNormalsToFile("SL_normals",_vtk);
      Space V(_domain=*SL, _interpolation=pt, _name="V",_notOptimizeNumbering);
      Unknown u(V, _name="u");
      TestFunction v(u, _name="v");
      if(BEM1)// BEM 1 (MFIE)
        {
          meth="_BEM1_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          ComputeBoxBEM1 cpBox(*SL,u,G,IMieDuf,IMir2,false,"cpBox");
          cpBox.setData(TermVector(u,*SL,finc));
          elapsedTime();
          cpBox.solve();
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          elapsedTime("BEM");
          UtBEM = UdBEM+Ui;
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
          cur_BEM = cpBox.X;
          saveToFile("current_BEM", cur_BEM, _format=_vtu);
//          saveToFile("ud_BEM-MAL", UdBEM-UdUTD_SOM, _format=_vtu);

        }
      if(BEM2) //BEM 2 (EFIE)
        {
          meth="_BEM2_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          TermMatrix M(intg(*SL, u*v), _name="M");
          TermVector DnUi= directSolve(M,TermVector(intg(*SL, dfinc*v)));
          ComputeBoxBEM2 cpBox(*SL,u,G,IMieDuf,IMir2,false,"cpBox");
          cpBox.setData(DnUi);
          cpBox.solve();
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          UtBEM = UdBEM+Ui;
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
          cur_BEM = cpBox.X;
        }

      if(BEM_CFIE) //BEM  CFIE
        {
          meth="_BEM_CFIE_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          TermMatrix M(intg(*SL, u*v), _name="M");
          TermVector DnUi= directSolve(M,TermVector(intg(*SL, dfinc*v)));
          ComputeBoxBEMCFIE cpBox(*SL,u,G,IMieDuf,IMir2,k,0.2,false,"cpBox");
          cpBox.setData(TermVector(u,*SL,finc),DnUi);
          cpBox.solve();
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          UtBEM = UdBEM+Ui;
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
          cur_BEM = cpBox.X;
        }

      if(BEM_ND && (obst==_distantCrescentND || obst==_diskND)) //BEM ND
        {
          meth="_BEM_ND_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          gammaN->setNormalOrientation(_towardsInfinite,centerL);
          gammaD->setNormalOrientation(_outwardsInfinite);
          gammaN->saveNormalsToFile("normal_gammaN",_vtk);
          gammaD->saveNormalsToFile("normal_gammaD",_vtk);
          ComputeBoxBEM_ND cpBox(*SL,*gammaN, *gammaD, u, G, IMieDuf, IMir2, true,"cpBox");
          cpBox.setData(TermVector(u,*SL,finc));
          cpBox.solve();
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          UtBEM = UdBEM+Ui;
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
        }
      if(BEM_ND_sigma && obst==_diskND) //BEM ND sigma
        {
          meth="_BEM_ND_sigma_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Domain omega=meshLargeObs.domain("omega"), sigma=meshLargeObs.domain("sigma");
          sigma.updateParentOfSideElements();
          Space Vs(_domain=sigma, _interpolation=P1, _name="Vs");
          Unknown l(Vs, _name="l");
          TestFunction t(l, _name="t");
          Space Vo(_domain=omega, _interpolation=P1, _name="V");
          Unknown p(Vo, _name="p");
          TestFunction q(p, _name="q");
          BilinearForm apq = intg(omega,grad(p)|grad(q))-k*k*intg(omega,p*q)-intg(sigma,l*q)
                             + 0.5*intg(sigma,p*t)-intg(sigma,sigma,p*ndotgrad_y(G)*t,_method=IMieDuf)+intg(sigma,sigma,l*G*t,_method=IMieDuf);
          LinearForm lrhs = intg(sigma,finc*t);
          BoundaryConditions bcs = (p|*gammaD = 0);
          TermMatrix A(apq, bcs, _name="K");
          TermVector rhs(lrhs, _name="rhs");
          TermVector X=directSolve(A,rhs);
          Domain omg_ext =mR.domain("Omega_ext"), omg_int=mR.domain("omega");
          TermVector UFEM = interpolate(uobs,omg_int,X(p)); // FEM solution (total field)
          TermVector UBEM = integralRepresentation(uobs,omg_ext,intg(sigma,ndotgrad_y(G)*X(p),_method=IMir2))
                            -integralRepresentation(uobs,omg_ext,intg(sigma,G*X(l),_method=IMir2));
          TermVector ut = UBEM + TermVector(uobs,omg_ext,finc);
          UtBEM = merge(UFEM,ut);
          ut = UFEM - TermVector(uobs,omg_int,finc);
          UdBEM = merge(ut,UBEM);
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
        }
      if(BEM_sigma && sigma!=0) // BEM sigma
        {
          meth="_BEMSigma_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          sigma->setNormalOrientation(_towardsInfinite);
          sigma->saveNormalsToFile("sigma_normals",_vtk);
          Space VS(_domain=*sigma, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          ComputeBoxBEMSigma cpBox(*SL, u, *sigma, uS, G, IMieDuf, IMir2, lambda, mu, nu, k,false,"cpBox");
          cpBox.setData(TermVector(uS,*sigma,ffinc));
          cpBox.solve();
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          UtBEM = UdBEM+Ui;
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
        }
      if(Kirchoff && obst==_noScatterer)
        {
          meth="_Kirchoff_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Number nbcor=0;
          if(nbcor>0)
            meth+="cor_";
          Unknown u2(V, _name="u2", _dim=2); //vector unknown
          SL->setColorFlag(false);  //use or not use the restriction to 0 color elements in integral representation computations
          ComputeBoxKirchoff cpBox(*SL,u,G,IMir2,k,0,"box L");
          cpBox.setData(TermVector(u,*SL,finc),TermVector(u2,*SL,ginc)); //ui and grad(ui) on S
          //cpBox.obsp=&omegaobs; cpBox.uobsp=&uobs;
          cpBox.solve();
          cpBox.iter=1;
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          UtBEM = UdBEM+Ui;
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
        }
      if(UTD && obst==_noScatterer)
        {
          meth="_UTD_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Unknown u2(V, _name="u2", _dim=2); //vector unknown
          SL->setColorFlag(false);  //use or not use the restriction to 0 color elements in integral representation computations
          Real per=0.5*pi_*radiusL*(3*(ae+be)-std::sqrt((3*ae+be)*(ae+3*be))); //half perimeter of ellipse (Ramanujan estimate)
          Number nbr = 10; //number of residus
          Real s1=-1.5, s2=0.3, s3=0.5;   //asymptotic matching parameters
          //ComputeBoxUTD cpBox(*SL,u,G,IMir2,k,-2.5,0.53, per, 0., isn, "box L");
          ComputeBoxUTDF cpBox(*SL,u,G,IMir2,k,0.,nbr,s1,s2,s3,isn,wc,"box L");
          cpBox.setData(TermVector(u,*SL,finc),TermVector(u2,*SL,ginc)); //ui and grad(ui) on S
          //cpBox.obsp=&omegaobs; cpBox.uobsp=&uobs;
          elapsedTime();
          cpBox.solve();
          TermVector UdUTD = cpBox.computeOn(omegaobs,uobs);
          elapsedTime("UTD");
          TermVector UtUTD = UdUTD+Ui;
          saveFields(UtUTD, UdUTD, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
          if(BEM1)
            {
              saveToFile("Ecart_UTD_BEM", UtUTD-UtBEM, _format=_vtu);
              SuTermVector& Cu=cur_BEM.subVector();
              std::ofstream fout("current_BEM.dat");
              const Parametrization& par=SL->parametrization();
              for(Number i=1; i<=Cu.nbDofs(); i++)
                {
                  Point pd=Cu.dof(i).coords();
                  Real t=par.toRealParameter(pd);
                  fout<<pd(1)<<" "<<pd(2)<<" "<<t<<" "<<par.curabc(t)<<" "<<par.curvature(t)<<" ";
                  Complex v=Cu.getValue(i).asComplex();
                  fout<<std::real(v)<<" "<<std::imag(v)<<eol;
                }
              fout.close();
            }
            else remove("current_BEM.dat");
        }
      if(Ray && obst==_noScatterer)
        {
          meth="_Ray_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          elapsedTime();
          ComputeBoxRay cpBox(*SL,u,k,"box L");
          Unknown u2(V, _name="u2", _dim=2); //vector unknown
          cpBox.setData(TermVector(u,*SL,finc),TermVector(u2,*SL,ginc)); //ui and grad(ui) on S
          if(!view2d)
            cpBox.save_rays=true;
          cpBox.solve();
          UdBEM = cpBox.computeOn(omegaobs,uobs);
          UtBEM=UdBEM;
          cpBox.addIncidentField(UtBEM,Ui);
          saveFields(UtBEM, UdBEM, Ui, mR.domain("side_ext"),amp, Cr, meth+obstacleName(obst),view2d);
        }
    }

  // iterative BEM-BEM methods
  // =========================
  Number niter=10;
  TermVector UdIT, UtIT;
  bool SOR=false, BEM1_BEM1=false, BEM2_BEM1=false, BEMCFIE_BEMCFIE= false, Kirchoff_BEM1=false, Ray_BEM1=false, Ray_BEM1_n=false, Ray_BEM1_p=false,
       BEM1_BEM_ND = false, BEM_ND_BEM_ND=false, BEM_NF_BEM_NF=false, Kirchoff_BEM_ND=false, Ray_BEM_ND=false, UTD_BEM1=true;
  Number viewStep=1;
  if((SOR || BEM1_BEM1 ||  BEM2_BEM1 || BEMCFIE_BEMCFIE|| Kirchoff_BEM1|| Ray_BEM1 || Ray_BEM1_n || Ray_BEM1_p
          || BEM1_BEM_ND || BEM_ND_BEM_ND || BEM_NF_BEM_NF ||Kirchoff_BEM_ND || Ray_BEM_ND || UTD_BEM1) && obst!=_noScatterer)
    {
      GeomDomain *S, *L, *interfaceL, *interfaceS, *interface1L, *interface2L, *interface1S, *interface2S;
      switch(obst)
        {
          case _stickCrescent:
          case _stickTriangle:
          case _stickSquare:
            {
              interfaceL = &meshLargeObs.domain("interface");
              interfaceS = &meshSmallObs.domain("interface");
              S = &merge(*gamma,*interfaceS,"S");
              L = &merge(obstacle,*interfaceL,"L");
              gammaN=gamma;
              gammaD=interfaceS;
            }
            break;
          case _stickCrescentSigma:
          case _stickTriangleSigma:
            {
              interfaceL = &meshLargeObs.domain("interface");
              interfaceS = &meshSmallObs.domain("interface");
              interface1L= &meshLargeObs.domain("interface1");
              interface2L= &meshLargeObs.domain("interface2");
              S = &merge(*gamma,*interfaceS, "S");
              L = &merge(obstacle, *interfaceL,  *interface1L, *interface2L, "L");
            }
            break;
          case _distant2Disks:
            {
              L=&obstacle;
              S=&merge(meshLargeObs.domain("gamma1"),meshLargeObs.domain("gamma2"),"gamma");
            }
            break;
          case _distantCrescentND:
            {
              gammaN=meshLargeObs.domainP("gamma_N");
              gammaD=meshLargeObs.domainP("gamma_D");
              L=&obstacle;
              S=&merge(*gammaN,*gammaD,"gamma");
            }
            break;
          case _distantCrescentND2:
            {
              gammaN=meshLargeObs.domainP("gamma_N");
              gammaD=meshLargeObs.domainP("gamma_D");
              interfaceL = &meshLargeObs.domain("interface");
              //L=&obstacle;
              L=&merge(obstacle,*interfaceL,"L");
              S=&merge(*gammaN,*gammaD,"gamma");
            }
            break;
          case _crossDisk :
            {
              interfaceL = &meshLargeObs.domain("interfaceL");
              interfaceS = &meshSmallObs.domain("interfaceS");
              L=&merge(obstacle,*interfaceL,"L");
              S=&merge(*gamma,*interfaceS,"S");
              gammaN=gamma;
              gammaD=interfaceS;
            }
            break;
          default:
            {
              L=&obstacle;
              S=gamma;

            }
        }
      S->setNormalOrientation(_towardsInfinite);
      L->setNormalOrientation(_towardsInfinite);
      S->saveNormalsToFile("normal_S",_vtk);
      L->saveNormalsToFile("normal_L",_vtk);

      if(SOR)
        {
          Complex omega(1,0);
          niter=10;
          meth="_SOR_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          obstacle.saveNormalsToFile("normal_obstacle",_vtk);
          gamma->saveNormalsToFile("normal_gamma",_vtk);
          Space VL(_domain=obstacle, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Space VS(_domain=*gamma, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          BilinearForm bfL=0.5*intg(obstacle,uL*vL)-intg(obstacle,obstacle,uL*ndotgrad_y(G)*vL,_method=IMieDuf);
          TermMatrix KL=TermMatrix(bfL, _name="KL");
          factorize(KL,KL);
          BilinearForm bfrhsL=intg(obstacle,uL*vL);
          TermMatrix KrhsL=TermMatrix(bfrhsL, _name="KrhsL");
          TermMatrix CL(intg(*gamma,obstacle,uL*ndotgrad_y(G)*vS,_method=IMieDuf));
          TermVector uiL(uL,obstacle,finc);
          TermVector fL=KrhsL*uiL;
          BilinearForm bfS=0.5*intg(*gamma,uS*vS)-intg(*gamma,*gamma,uS*ndotgrad_y(G)*vS,_method=IMieDuf);
          TermMatrix KS=TermMatrix(bfS, _name="KS");
          factorize(KS,KS);
          BilinearForm bfrhsS=intg(*gamma,uS*vS);
          TermMatrix KrhsS=TermMatrix(bfrhsS, _name="KrhsS");
          TermMatrix CS(intg(obstacle,*gamma,uS*ndotgrad_y(G)*vL,_method=IMieDuf));
          TermVector uiS(uS,*gamma,finc);
          TermVector fS=KrhsS*uiS;
          TermMatrix RL =integralRepresentation(uobs,omegaobs,intg(obstacle,ndotgrad_y(G)*uL,_method=IMir2));
          TermMatrix RS =integralRepresentation(uobs,omegaobs,intg(*gamma,ndotgrad_y(G)*uS,_method=IMir2));
          TermVectors udL(niter),udS(niter);
          TermVector tS, tL, E;
          for(Number n=0; n<niter; n++)
            {
              theCout<<" Iteration "<<n+1<<eol<<" -----------"<<eol;
              if(n>0)
                tL=omega*fL+(1-omega)*tL+omega*(CS*udS[n-1]);
              else
                tL=omega*fL;
              udL[n]=factSolve(KL,tL);
              if(n>0)
                tS=omega*fS+(1-omega)*tS+omega*(CL*udL[n]);
              else
                tS=omega*fS+omega*(CL*udL[n]);
              udS[n]=factSolve(KS,tS);
              E=udL[n];
              if(n>0)
                E-=udL[n-1];
              thePrintStream<<"tL= "<<tL<<"udL= "<<udL[n]<<"E= "<<E<<eol;
              double el2=std::sqrt(real(KrhsL*E|E));
              theCout<<"L2 ecart on UdL = "<< el2;
              E=udS[n];
              if(n>0)
                E-=udS[n-1];
              thePrintStream<<"tS= "<<tS<<"udS= "<<udS[n]<<"E= "<<E<<eol;
              el2=std::sqrt(real(KrhsS*E|E));
              theCout<<" L2 ecart on UdS = "<< el2<<eol;
              saveToFile("UL"+meth+obstacleName(obst)+"_"+tostring(n), RL*udL[n], _format=_vtu);
              saveToFile("US"+meth+obstacleName(obst)+"_"+tostring(n), RS*udS[n], _format=_vtu);
            }
          TermVector UdITS = RS*udS[niter-1];
          TermVector UdITL = RL*udL[niter-1];
          UdIT = UdITS + UdITL;
//          saveToFile("UdITS"+meth+obstacleName(obst), UdITS, _format=_vtu);
//          saveToFile("UdITL"+meth+obstacleName(obst), UdITL, _format=_vtu);
          UtIT = Ui+UdIT;
        }

      if(BEM1_BEM1) //BEM1-BEM1
        {
          meth="_BEM1_BEM1_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          ComputeBoxBEM1 cbS(*S,uS,G,IMieDuf,IMir2,true,"box S"), cbL(*L,uL,G,IMieDuf,IMir2,true,"box L");
//          if(interfaceL!=0)
//            {
//              Dof* d1 = &VL.locateDof(M1), *d2=&VL.locateDof(M2);
//              cbL.setInterface(*interfaceL, d1, d2);
//            }
          cbL.setData(TermVector(uL,*L,finc));
          cbS.setData(TermVector(uS,*S,finc));
          iterativeScheme(cbS,cbL,10,0,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
          UtIT = Ui+UdIT;
        }

      if(BEMCFIE_BEMCFIE) //BEM CFIE-BEM CFIE
        {
          meth="_BEMCFIE_BEMCFIE_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          TermMatrix ML(intg(*L, uL*vL), _name="ML");
          TermVector DnUiL= directSolve(ML,TermVector(intg(*L, dfinc*vL)));
          TermMatrix MS(intg(*S, uS*vS), _name="MS");
          TermVector DnUiS= directSolve(MS,TermVector(intg(*S, dfinc*vS)));
          ComputeBoxBEMCFIE cbS(*S,uS,G,IMieDuf,IMir,k,0.2,true,"box S"), cbL(*L,uL,G,IMieDuf,IMir,k,0.2,true,"box L");
          cbL.setData(TermVector(uL,*L,finc), DnUiL);
          cbS.setData(TermVector(uS,*S,finc), DnUiS);
          iterativeScheme(cbS,cbL,10,viewStep,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
          UtIT = Ui+UdIT;
        }

      if(BEM2_BEM1) //BEM2-BEM1
        {
          meth="_BEM2_BEM1_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          ComputeBoxBEM1 cbS(*S,uS,G,IMieDuf,IMir,true,"box S");
          ComputeBoxBEM2 cbL(*L,uL,G,IMieDuf,IMir,true,"box L");
          if(interfaceL!=0)
            {
              Dof* d1 = &VL.locateDof(M1), *d2=&VL.locateDof(M2);
              cbL.setInterface(*interfaceL, d1, d2);
            }
          TermMatrix ML(intg(*L, uL*vL), _name="ML");
          TermVector DnUiL= directSolve(ML,TermVector(intg(*L, dfinc*vL)));
          cbL.setData(DnUiL);
          cbS.setData(TermVector(uS,*S,finc));
          iterativeScheme(cbS,cbL,10,viewStep,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
          UtIT = Ui+UdIT;
        }

      if(BEM1_BEM_ND) //BEM1-BEM_ND
        {
          meth="_BEM1_BEM_ND_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          gammaN->setNormalOrientation(_outwardsInfinite);
          gammaD->setNormalOrientation(_towardsInfinite);
          gammaN->saveNormalsToFile("normal_gammaN",_vtk);
          gammaD->saveNormalsToFile("normal_gammaD",_vtk);
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          ComputeBoxBEM_ND cbS(*S,*gammaN, *gammaD, uS, G, IMieDuf, IMir2, true,"box S");
          cbS.setData(TermVector(uS,*S,finc));
          ComputeBoxBEM1 cbL(*L,uL,G,IMieDuf,IMir2,true,"box L");
          cbL.setData(TermVector(uL,*L,finc));
          //if(interfaceL!=0) cbL.setInterface(*interfaceL, 0, 0);
          iterativeScheme(cbS,cbL,160,160,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
          UtIT = Ui+UdIT;
        }

      if(BEM_ND_BEM_ND) //BEM_ND-BEM_ND
        {
          meth="_BEM_ND_BEM_ND_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          gammaN->setNormalOrientation(_outwardsInfinite);
          gammaD->setNormalOrientation(_towardsInfinite);
          gammaN->saveNormalsToFile("normal_gammaN",_vtk);
          gammaD->saveNormalsToFile("normal_gammaD",_vtk);
          obstacle.setNormalOrientation(_towardsInfinite);
          interfaceL->setNormalOrientation(_outwardsInfinite);
          obstacle.saveNormalsToFile("normal_obstacle",_vtk);
          interfaceL->saveNormalsToFile("normal_interfaceL",_vtk);
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          ComputeBoxBEM_ND cbS(*S,*gammaN, *gammaD, uS, G, IMieDuf, IMir2, true,"box S");
          cbS.setData(TermVector(uS,*S,finc));
          ComputeBoxBEM_ND cbL(*L,obstacle, *interfaceL, uL, G, IMieDuf, IMir2, true,"box L");
          cbL.setData(TermVector(uL,*L,finc));
          //if(interfaceL!=0) cbL.setInterface(*interfaceL, 0, 0);
          iterativeScheme(cbS,cbL,20,20,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
          UtIT = Ui+UdIT;
        }

      if(BEM_NF_BEM_NF) //BEM_NF-BEM_NF
        {
          Complex alpha(0.,k);  //ik
          meth="_BEM_NF_BEM_NF_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          //gammaN->setNormalOrientation(_outwardsInfinite); gammaD->setNormalOrientation(_towardsInfinite);
          gammaN->saveNormalsToFile("normal_gammaN",_vtk);
          gammaD->saveNormalsToFile("normal_gammaD",_vtk);
          obstacle.setNormalOrientation(_towardsInfinite);
          interfaceL->setNormalOrientation(_outwardsInfinite);
          obstacle.saveNormalsToFile("normal_obstacle",_vtk);
          interfaceL->saveNormalsToFile("normal_interfaceL",_vtk);
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          ComputeBoxBEM_NF cbS(*S,*gammaN, *gammaD, uS, G, IMieDuf, IMir2, alpha, true,"box S");
          cbS.setData(TermVector(uS,*S,finc));
          ComputeBoxBEM_NF cbL(*L,obstacle, *interfaceL, uL, G, IMieDuf, IMir2, alpha,true,"box L");
          cbL.setData(TermVector(uL,*L,finc));
          //if(interfaceL!=0) cbL.setInterface(*interfaceL, 0, 0);
          iterativeScheme(cbS,cbL,30,0,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
          UtIT = Ui+UdIT;
        }

      if(Kirchoff_BEM1)
        {
          meth="_Kirchoff_BEM1_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Number nbcor=1;   // Kirchoff correction only at first iteration
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Unknown uL2(VL, _name="uL2", _dim=2); //vector unknown
          L->setColorFlag(false);  //use or not use the restriction to 0 color elements in integral representation computations
          ComputeBoxKirchoff cbL(*L,uL,G,IMir2,k,nbcor,"box L");
          cbL.setData(TermVector(uL,*L,finc),TermVector(uL2,*L,ginc)); //ui and grad(ui) on L
          cbL.obsp=&omegaobs;
          cbL.uobsp=&uobs;
          //cbL.uobs=Ui; //to see total field at each iteration on L
          ComputeBoxBEM1 cbS(*S,uS,G,IMieDuf,IMir,true,"box S");
          cbS.setData(TermVector(uS,*S,finc));                         //ui on S
          iterativeScheme(cbS,cbL,5,0,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.uobs;
          UtIT = Ui+UdIT;
        }

      if(Kirchoff_BEM_ND)
        {
          meth="_Kirchoff_BEM_ND";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
//          gammaN->setNormalOrientation(_outwardsInfinite); gammaD->setNormalOrientation(_towardsInfinite);
          gammaN->saveNormalsToFile("normal_gammaN",_vtk);
          gammaD->saveNormalsToFile("normal_gammaD",_vtk);
          Number nbcor=1;   // Kirchoff correction only at first iteration
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Unknown uL2(VL, _name="uL2", _dim=2); //vector unknown
          L->setColorFlag(false);  //use or not use the restriction to 0 color elements in integral representation computations
          ComputeBoxKirchoff cbL(*L,uL,G,IMir2,k,nbcor,"box L");
          cbL.setData(TermVector(uL,*L,finc),TermVector(uL2,*L,ginc)); //ui and grad(ui) on L
          cbL.obsp=&omegaobs;
          cbL.uobsp=&uobs;
          //cbL.uobs=Ui; //to see total field at each iteration on L
          ComputeBoxBEM_ND cbS(*S,*gammaN, *gammaD, uS, G, IMieDuf, IMir, true,"box S");
          cbS.setData(TermVector(uS,*S,finc));                     //ui on S
          iterativeScheme(cbS,cbL,10,0,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.uobs;
          UtIT = Ui+UdIT;
        }

        if(UTD_BEM1)
        {
          meth="_UTD_BEM1_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering); Unknown uS(VS,"uS"); TestFunction vS(uS,"vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering); Unknown uL(VL,"uL"); TestFunction vL(uL,"vL");
          Unknown uL2(VL, _name="uL2", _dim=2); //vector unknown
          L->setColorFlag(false);  //use or not use the restriction to 0 color elements in integral representation computations
          Number nbr = 10; //number of residus
          Real s1=-1.5, s2=0.3, s3=0.5;   //asymptotic matching parameters
          Number isn=0; // use_(i|n)
          ComputeBoxUTDF cbL(*L,uL,G,IMir2,k,0.,nbr,s1,s2,s3,isn,"box L");
          cbL.setData(TermVector(uL,*L,finc),TermVector(uL2,*L,ginc)); //ui and grad(ui) on L
          cbL.obsp=&omegaobs;
          cbL.uobsp=&uobs;
          //cbL.uobs=Ui; //to see total field at each iteration on L
          ComputeBoxBEM1 cbS(*S,uS,G,IMieDuf,IMir,true,"box S");
          cbS.setData(TermVector(uS,*S,finc));                     //ui on S
          iterativeScheme(cbS,cbL,10,0,&uobs);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.uobs;
          UtIT = Ui+UdIT;
        }

      if(Ray_BEM1)
        {
          meth="_Ray_BEM1_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Unknown uL2(VL, _name="uL2", _dim=2); //vector unknown required for grad computation
          ComputeBoxRay cbL(*L,uL,k,"box L");
          cbL.setData(TermVector(uL,*L,finc),TermVector(uL2,*L,ginc)); //ui and grad(ui) on L
          cbL.setObs(uobs);
          cbL.save_rays=true;
          ComputeBoxBEM1 cbS(*S,uS,G,IMieDuf,IMir,true,"box S");
          cbS.setData(TermVector(uS,*S,finc));
          iterativeScheme(cbS,cbL,5,0,0);
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.uobs;
          UtIT = Ui+UdIT;
        }
      if(Ray_BEM_ND)
        {
          meth="_Ray_BEM_ND";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;

//          gammaN->setNormalOrientation(_outwardsInfinite); gammaD->setNormalOrientation(_towardsInfinite);
          gammaN->saveNormalsToFile("normal_gammaN",_vtk);
          gammaD->saveNormalsToFile("normal_gammaD",_vtk);
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Unknown uL2(VL, _name="uL2", _dim=2); //vector unknown
          ComputeBoxRay cbL(*L,uL,k,"box L");
          cbL.setData(TermVector(uL,*L,finc),TermVector(uL2,*L,ginc)); //ui and grad(ui) on L
          cbL.setObs(uobs);
          cbL.save_rays=true;
          //cbL.uobs=Ui; //to see total field at each iteration on L
          ComputeBoxBEM_ND cbS(*S,*gammaN, *gammaD, uS, G, IMieDuf, IMir, true,"box S");
          cbS.setData(TermVector(uS,*S,finc));                     //ui on S
          iterativeScheme(cbS,cbL,10,0,0);
          TermVector UdITS = cbS.computeOn(omegaobs,uobs);
          UdIT=UdITS + cbL.uobs;
          UtIT = Ui + UdIT;
          ComputeBoxKirchoff cbK(*L,uL,G,IMir2,k,0,"box Kirchoff L");
          cbK.setData(cbL.dataBox.begin()->second, (++cbL.dataBox.begin())->second);
          cbK.solve();
          TermVector UdITK = UdITS + cbK.computeOn(omegaobs,uobs);
          saveFarField(UdITK, amp, Cr, "Ufar"+meth+"with_Kirchoff_"+obstacleName(obst)+".dat");
        }

      if(Ray_BEM1_n && obst==_distant2Disks)  //only for two distant disks
        {
          meth="_Ray_BEM1_n_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Domain gamma1=meshLargeObs.domain("gamma1"), gamma2=meshLargeObs.domain("gamma2");
          gamma1.setNormalOrientation(towardsInfinite);
          gamma2.setNormalOrientation(towardsInfinite);
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Space V1(_domain=gamma1, _interpolation=pt, _name="V1", _notOptimizeNumbering);
          Unknown u1(V1, _name="u1");
          TestFunction v1(u1, _name="v1");
          Space V2(_domain=gamma2, _interpolation=pt, _name="V2", _notOptimizeNumbering);
          Unknown u2(V2, _name="u2");
          TestFunction v2(u2, _name="v2");
          Unknown uL2(VL, _name="uL2", _dim=2); //vector unknown required for grad computation
          ComputeBoxRay cbL(*L,uL,k,"box L");
          cbL.setData(TermVector(uL,*L,finc),TermVector(uL2,*L,ginc)); //ui and grad(ui) on L
          cbL.setObs(uobs);
          cbL.save_rays=true;
          ComputeBoxBEM1 cb1(gamma1,u1,G,IMieDuf,IMir,true,"box 1");
          cb1.setData(TermVector(u1,gamma1,finc));
          ComputeBoxBEM1 cb2(gamma2,u2,G,IMieDuf,IMir,true,"box 2");
          cb2.setData(TermVector(u2,gamma2,finc));
          std::vector<ComputeBox*> cbs(3);
          cbs[0]=&cbL;
          cbs[1]=&cb1;
          cbs[2]=&cb2;
          iterativeScheme(cbs,5,0,0);
          UdIT=cb1.computeOn(omegaobs,uobs)+ cb2.computeOn(omegaobs,uobs)+cbL.uobs;
          UtIT = Ui+UdIT;
        }

      if(Ray_BEM1_p && obst==_distant2Disks)  //only for two distant disks
        {
          meth="_Ray_BEM1_p_";
          theCout<<"========================================================"<<eol;
          theCout<<"              "<<meth<<eol;
          theCout<<"========================================================"<<eol;
          Domain gamma1=meshLargeObs.domain("gamma1"), gamma2=meshLargeObs.domain("gamma2");
          gamma1.setNormalOrientation(towardsInfinite);
          gamma2.setNormalOrientation(towardsInfinite);
          Space VS(_domain=*S, _interpolation=pt, _name="VS", _notOptimizeNumbering);
          Unknown uS(VS, _name="uS");
          TestFunction vS(uS, _name="vS");
          Space VL(_domain=*L, _interpolation=pt, _name="VL", _notOptimizeNumbering);
          Unknown uL(VL, _name="uL");
          TestFunction vL(uL, _name="vL");
          Unknown uL2(VL, _name="uL2", _dim=2); //vector unknown required for grad computation
          ComputeBoxRay cbL(*L,uL,k,"box L");
          cbL.setData(TermVector(uL,*L,finc),TermVector(uL2,*L,ginc)); //ui and grad(ui) on L
          cbL.setObs(uobs);
          cbL.save_rays=true;
          ComputeBoxBEM1 cbS(*S,uS,G,IMieDuf,IMir,true,"box S");
          cbS.setData(TermVector(uS,*S,finc));
          DataBox dbL=cbL.dataBox, dbS=cbS.dataBox;
          cbL.iter=1;
          cbS.iter=1;
          cbL.solve();
          static_cast<ComputeBox&>(cbL).computeOn(static_cast<ComputeBox&>(cbS));
          dbS += cbS.dataBox;
          cbS.dataBox = dbS;  // at first iteration take for cbS the incident field + the diffracted field by cbL
          for(Number n=1; n<=3; n++)
            {
              cbS.solve();
              cbL.setData(integralRepresentation(uL,*L,intg(gamma1,ndotgrad_y(G)*uS,_method=IMir),cbS.X),
                          integralRepresentation(uL2,*L,intg(gamma1,grad_x(ndotgrad_y(G))*uS,_method=IMir),cbS.X));
              cbL.solve();
              cbL.update_obs=true;
              static_cast<ComputeBox&>(cbL).computeOn(static_cast<ComputeBox&>(cbS));
              dbS += cbS.dataBox;
              cbL.setData(integralRepresentation(uL,*L,intg(gamma2,ndotgrad_y(G)*uS,_method=IMir),cbS.X),
                          integralRepresentation(uL2,*L,intg(gamma2,grad_x(ndotgrad_y(G))*uS,_method=IMir),cbS.X));
              cbL.solve();
              cbL.update_obs=true;
              static_cast<ComputeBox&>(cbL).computeOn(static_cast<ComputeBox&>(cbS));
              dbS += cbS.dataBox;
            }
          cbS.dataBox=dbS;
          cbS.solve();
          UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.uobs;
          UtIT = Ui+UdIT;
        }
      //save results to file
      if(view2d)
        {
          saveToFile("Ut"+meth+obstacleName(obst),UtIT,_vtu);
          saveToFile("Ud"+meth+obstacleName(obst),UdIT,_vtu);
          Domain sideExt=mR.domain("side_ext");
          saveFarField(UdIT|sideExt, amp, Cr, "Ufar"+meth+obstacleName(obst)+".dat");
        }
      else
        {
          saveFarField(UdIT, amp, Cr, "Ufar"+meth+obstacleName(obst)+".dat");
          //saveToFile("Ufar"+meth+obstacleName(obst),amp*UdIT,_vtu);
        }
    }

  bool BEM1_BEMSigma=false, RAY_BEMSigma=false;
  viewStep=0;
  if(BEM1_BEMSigma || RAY_BEMSigma && obst!=_noScatterer) //BEM1-BEMSigma
    {
      switch(obst)
        {
          case _stickCrescentSigma:
          case _stickTriangleSigma:
            {
              Domain interfaceL = meshLargeObs.domain("interface"),interface1L= meshLargeObs.domain("interface1"), interface2L= meshLargeObs.domain("interface2");
              Domain interface1S= meshSmallObs.domain("interface1"), interface2S= meshSmallObs.domain("interface2");
              Domain gamma=meshSmallObs.domain("gamma"), sig=meshSmallObs.domain("sigma");
              Domain gam = merge(gamma,interface1S, interface2S, "gam");
              Domain sigam=merge(interface1S, gam, interface2S, sig,"sigma gamma");
              sigam.setNormalOrientation(_towardsInfinite);
              sigam.saveNormalsToFile("normal_sigma_gamma",vtk);
              Domain obs = merge(obstacle,interface1L, interface2L, interfaceL,"obs");
              obs.setNormalOrientation(_towardsInfinite);
              gam.saveNormalsToFile("normal_gamma",vtk);
              obs.saveNormalsToFile("normal_largeObs",vtk);
              sig.saveNormalsToFile("normal_sigma",vtk);
              Domain IL=merge(interfaceL,interface1L,interface2L,"IL");
              Space VL(_domain=obs, _interpolation=pt, _name="VL", _notOptimizeNumbering);
              Unknown uL(VL, _name="uL");
              TestFunction vL(uL, _name="vL");
              Space VS(_domain=sig, _interpolation=pt, _name="VS", _notOptimizeNumbering);
              Unknown uS(VS, _name="uS");
              TestFunction vS(uS, _name="vS");
              Space VG(_domain=gam, _interpolation=pt, _name="VG", _notOptimizeNumbering);
              Unknown uG(VG, _name="uG");
              TestFunction vG(uG, _name="vG");
              ComputeBoxBEMSigma cbS(gam, uG, sig, uS, G, IMieDuf, IMir2, lambda, mu, nu, k, true,"box sigma");
              if(BEM1_BEMSigma)
                {
                  meth="_BEM1_BEMSigma_";
                  theCout<<"========================================================"<<eol;
                  theCout<<"              "<<meth<<eol;
                  theCout<<"========================================================"<<eol;
                  ComputeBoxBEM1 cbL(obs,uL,G,IMieDuf,IMir,true,"box L");
                  Dof* d1 = &VL.locateDof(M1), *d2=&VL.locateDof(M2); // does not work very well !!!
                  cbL.setInterface(IL, 0, 0);
                  cbL.setData(TermVector(uL,obs,finc));
                  cbS.setData(TermVector(uS,sig,ffinc));
                  iterativeScheme(cbS,cbL,10,viewStep,&uobs);
                  UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
                  UtIT = Ui+UdIT;
                }
            }
            break;
          case _distantDisk : //not sticked
          case _distantCrescent :
          case _distantTriangle :
          case _distantSquare :
            {
              meth="_BEM1_BEMSigma_";
              theCout<<"========================================================"<<eol;
              theCout<<"              "<<meth<<eol;
              theCout<<"========================================================"<<eol;
              Domain sigma=meshSmallObs.domain("sigma");
              sigma.setNormalOrientation(_towardsInfinite);
              Domain gamma=meshSmallObs.domain("gamma");
              gamma.setNormalOrientation(_towardsInfinite);
              obstacle.setNormalOrientation(_towardsInfinite);
              Space VL(_domain=obstacle, _interpolation=pt, _name="VL", _notOptimizeNumbering);
              Unknown uL(VL, _name="uL");
              TestFunction vL(uL, _name="vL");
              Space VS(_domain=sigma, _interpolation=pt, _name="VS", _notOptimizeNumbering);
              Unknown uS(VS, _name="uS");
              TestFunction vS(uS, _name="vS");
              Space VG(_domain=gamma, _interpolation=pt, _name="VG", _notOptimizeNumbering);
              Unknown uG(VG, _name="uG");
              TestFunction vG(uG, _name="vG");
              if(BEM1_BEMSigma)
                {
                  ComputeBoxBEMSigma cbS(gamma, uG, sigma, uS, G, IMieDuf, IMir2, lambda, mu, nu, k,true,"box S");
                  ComputeBoxBEM1 cbL(obstacle,uL,G,IMieDuf,IMir,true,"box L");
                  cbL.setData(TermVector(uL,obstacle,finc));
                  cbS.setData(TermVector(uS,sigma,ffinc));
                  iterativeScheme(cbS,cbL,10,viewStep,&uobs);
                  UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.computeOn(omegaobs,uobs);
                  UtIT = Ui+UdIT;
                }
              if(RAY_BEMSigma)
                {
                  meth="_Ray_BEM1Sigma_";
                  theCout<<"========================================================"<<eol;
                  theCout<<"              "<<meth<<eol;
                  theCout<<"========================================================"<<eol;
                  ComputeBoxBEMSigma cbS(gamma, uG, sigma, uS, G, IMieDuf, IMir, lambda, mu, 0., k,true,"box S");
                  Unknown uL2(VL,_name="uL2", _dim=2); //vector unknown required for grad computation
                  ComputeBoxRay cbL(obstacle,uL,k,"box L");
                  cbL.setData(TermVector(uL,obstacle,finc),TermVector(uL2,obstacle,ginc)); //ui and grad(ui) on L
                  cbS.setData(TermVector(uS,sigma,ffinc));
                  cbL.setObs(uobs);
                  cbL.save_rays=true;
                  iterativeScheme(cbS,cbL,3,viewStep,0);
                  UdIT=cbS.computeOn(omegaobs,uobs)+ cbL.uobs;
                  UtIT = Ui+UdIT;
                }
            }
            break;
          default:
            error("free_error",obstacleName(obst)+" not allowed for BEM1-BEMSigma iterative method");
        }
      //save results to file
      if(view2d)
        {
          saveToFile("Ut"+meth+obstacleName(obst), UtIT, _format=_vtu);
          saveToFile("Ud"+meth+obstacleName(obst), UdIT, _format=_vtu);
          Domain sideExt=mR.domain("side_ext");
          saveFarField(UdIT|sideExt, amp, Cr, "Ufar"+meth+obstacleName(obst)+".dat");
        }
      else
        {
          saveFarField(UdIT, amp, Cr, "Ufar"+meth+obstacleName(obst)+".dat");
          //saveToFile("Ufar"+meth+obstacleName(obst), amp*UdIT, _format=_vtu);
        }
    }

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

} // end of namespace dev_operator
