/*
  XLiFE++ is an extended library of finite elements written in C++
  Copyright (C) 2014  Lunéville, Eric; Kielbasiewicz, Nicolas; Lafranche, Yvon; Nguyen, Manh-Ha; Chambeyron, Colin

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
	\file dev_dtn_3d.cpp
  \author E. Lunéville
	\since 14 nov 2013
	\date 14 nov 2013

	Scalar DtN test using Helmholtz operator in 3D waveguide, any section

              /                             /
             /            dn(u)=0          /
     y=1/2  --------------------------------
	        |                              |
            |                              |
  dn(u)=-ik |      delta(u)+k2.u=0         | dn(u)=Tu
	        |                              |
	        |                              |
	 y=-1/2 --------------------------------/
           x=0           dn(u)=0          x=pi
*/

#include "xlife++-libs.h"
#include "testUtils.hpp"

using namespace std;
using namespace xlifepp;

namespace dev_dtn_3d
{
Real k=20;


//map sigma+ (z=4) -> S (same x,y)
Vector<Real> mapS(const Point& P, Parameters& pa = defaultParameters)
{
  Point P2(P.x(),P.y());
  return P2;
}

//data on Sigma-
Complex g(const Point& P, Parameters& pa = defaultParameters)
{
  return -i_*k;
}

void dev_dtn_3d(int argc, char* argv[], bool check)
{
  String rootname = "dev_dtn_3d";
  trace_p->push(rootname);

  std::stringstream out;
  out.precision(testPrec);
  verboseLevel(50);
  String errors;
  Number nbErrors = 0;

  //mesh of the section
  Disk dext(_center=Point(0.,0.,0.),_v1=Point(1.,0.,0.),_v2=Point(0.,1.,0.),20,_domain_name="S");
  Disk dint(_center=Point(0.,0.,0.),_v1=Point(0.8,0.,0.),_v2=Point(0.,0.8,0.),16,_domain_name="sint");
  Mesh ms(dext-dint,_shape=_triangle, _order=1, _generator=gmsh,"mS");
  ms.saveToFile("ms",_vtk);
  Domain S=ms.domain("S");

  //compute mode on section S (eigen values of Delta)
  Space W(_domain=S, _interpolation=P1, _name="W");
  Unknown p(W, _name="p"); TestFunction q(p, _name="q");
  BilinearForm apq=intg(S,grad(p)|grad(q)), bpq=intg(S,p*q);
  TermMatrix As(apq, _name="As"), Bs(bpq, _name="Bs");
  Number n=20;
  EigenElements eigs = arpackSolve(As, Bs, _nev=n, _sigma=Complex(0.), _tolerance=1.0e-12);
  n=eigs.numberOfEigenValues();
  eigs.toReal();  //force real
  Vector<Complex> betas(n);
  for(Number j=1;j<=n;j++) //L2(S) normalization and building betas
  {
      betas(j)=sqrt(Complex(k*k-eigs.value(j)));
      Real ni= sqrt(realPart(Bs*eigs.vector(j)|eigs.vector(j)));
      eigs.vector(j)/=ni;
  }
  saveToFile("eigs",eigs,_format=_vtu);

  //load 3D mesh
  //Mesh mesh("pipeline.msh", _name="pipeline");
  Mesh mesh(inputsPathTo("pipeline_test.msh"), _name="pipeline");
  mesh.saveToFile("pipeline",_vtk);
  Domain omega=mesh.domain("Omega"),
         sigmaM=mesh.domain("Sigma-"),
         sigmaP=mesh.domain("Sigma+");

  //construct tensor kernel on Sigma+ and problem
  TensorKernel tkP(eigs.vectors, betas);
  tkP.xmap= mapS; tkP.ymap= mapS;
  Space V(_domain=omega, _interpolation=P1, _name="V");
  Unknown u(V, _name="u"); TestFunction v(u, _name="v");
  BilinearForm auv = intg(omega,grad(u)|grad(v))-k*k*intg(omega,u*v)
                   - i_*intg(sigmaP,sigmaP,u*tkP*v,_quad=GaussLegendre,_order=3,_symmetry=_symmetric);
  LinearForm fv=intg(sigmaM,g*v);
  TermMatrix A(auv, _name="A");
  TermVector F(fv, _name="F");
  std::cout<<"TermMatrix A computed cputime="<<cpuTime()<<eol;

  // solve linear system AX=B
  TermVector U=directSolve(A,F);
  std::cout<<"linear system solved cputime="<<cpuTime()<<eol;
  saveToFile("U",U,_format=_vtu);

  //create interpolation
//  Interpolation* LagrangePk=findInterpolation(Lagrange,standard,2,H1);
//  Space V(omega,*LagrangePk,"V",true);
//  Unknown u(V,"u");
//  TestFunction v(u,"v");
//
//  //parameters
//  Real h=1;
//  Parameters params(h,"h");
//  params<<Parameter(k,"k");
//
//  //create spectral space and dtn kernel
//  Number N=5;
//  Space Sp(sigmaP, Function(cosny,params), N, "cos(n*pi*y)");
//  Unknown phiP(Sp,"phiP");
//  Vector<Complex> lambda(N);
//  for(Number n=0; n<N; n++) lambda[n]=sqrt(Complex(k*k-n*n*pi*pi/(h*h)));
//  TensorKernel tkp(phiP,lambda);
//  std::cout<<"space created cputime="<<cpuTime()<<eol;
//
//  //================= analytic kernel and full bilinear form approach =========================
//  //create bilinear form and linear form
//  BilinearForm auv=intg(omega,grad(u)|grad(v)) - k*k*intg(omega,u*v)
//                  - i*intg(sigmaP,sigmaP,u*tkp*v,_quad=GaussLegendre,_order=5,_symmetry=_symmetric);
//  LinearForm fv=intg(sigmaM,gp*v);
//  TermMatrix A(auv, _name="A");
//  TermVector B(fv,_name="B");
//  std::cout<<"TermMatrix A computed cputime="<<cpuTime()<<eol;
//  // solve linear system AX=F using LU factorization
//  TermVector U=directSolve(A,B);
//  std::cout<<"linear system solved cputime="<<cpuTime()<<eol;
//  out<<"analytic kernel and bilinear form approach : u = "<<U<<eol;
//  saveToFile("U.vtk",U,_format=_vtk);
//  BilinearForm uv=intg(omega,u*v);
//  TermMatrix M(uv,_name="M");
//  TermVector Uex(u,omega,modeplan);
//  saveToFile("Uex.vtk",Uex,_format=_vtk);
//  TermVector Ec=U-Uex;
//  Real erL2=sqrt(real(M*Ec|Ec));
//  std::cout<<"L2 error ="<<erL2<<eol;
//  out<<"L2 error ="<<erL2<<eol;
//
//  clear(A);
//  //================= analytic kernel and matrix product approach =========================
//  BilinearForm auv2=intg(omega,grad(u)|grad(v)) - k*k*intg(omega,u*v);
//  TermMatrix H(auv2,_storage=csDual,_name="H");
//  BilinearForm euv=intg(sigmaP,phiP*v,_quad=GaussLegendre,_order=5);
//  BilinearForm fuv=intg(sigmaP,u*phiP,_quad=GaussLegendre,_order=5);
//  TermMatrix E(euv,_name="E");
//  TermMatrix F(fuv,_name="F");
//  std::cout<<"TermMatrix H, E, F computed cputime="<<cpuTime()<<eol;
//  TermMatrix L(phiP,sigmaP,lambda,_name="L");  //diagonal matrix
//  TermMatrix ELF=E*L*F;
//  std::cout<<"TermMatrix ELF=E*L*F computed cputime="<<cpuTime()<<eol;
//  TermMatrix A2=H-i*ELF;
//  std::cout<<"TermMatrix A2 computed cputime="<<cpuTime()<<eol;
//  //solve linear system AX=F using LU factorization
//  TermMatrix LU2;
//  luFactorize(A2,LU2);
//  std::cout<<"matrix factorized cputime="<<cpuTime()<<eol;
//  TermVector U2=directSolve(LU2,B);
//  std::cout<<"linear system solved cputime="<<cpuTime()<<eol;
//  out<<"analytic kernel and matrix product approach u = "<<U2<<eol;
//  saveToFile("U2.vtk",U2,_format=_vtk);
//  //compare U to U2
//  TermVector X=U-U2;
//  Real difL2=sqrt(real(M*X|X));
//  std::cout<<"L2 difference U-U2 ="<<difL2<<eol;
//  out<<"L2 difference U-U2 ="<<difL2<<eol;
//
//  clear(H,E,F,L);
//  clear(A2,LU2);
//  //================= interpolated kernel and full bilinear form approach =========================
//  Function cos(cosny, params);
//  TermVectors cos_int(N);
//  for(Number n=1; n<=N; n++)
//    {
//      cos.parameter("basis index")=n;
//      cos_int(n)=TermVector(u,sigmaP,cos,_name="c"+tostring(n-1));
//    }
//  Space Sp_int(cos_int, "interpolated cos(n*pi*y)");
//  Unknown phiP_int(Sp_int,"phiP_int");
//  TensorKernel tkp_int(phiP_int,lambda);
//  BilinearForm auv_int=intg(omega,grad(u)|grad(v)) - k*k*intg(omega,u*v) - i*intg(sigmaP,sigmaP,u*tkp_int*v);
//  LinearForm fv_int=intg(sigmaM,gp*v);
//  TermMatrix A_int(auv_int,_storage=csDual,_name="A_int");
//  TermVector B_int(fv_int,"B_int");
//  std::cout<<"TermMatrix A_int computed cputime="<<cpuTime()<<eol;
//  //solve linear system AX=F using LU factorization
//  TermMatrix LU;
//  luFactorize(A_int,LU);
//  std::cout<<"matrix factorized cputime="<<cpuTime()<<eol;
//  TermVector U_int=directSolve(LU,B_int);
//  std::cout<<"linear system solved cputime="<<cpuTime()<<eol;
//  out<<"interpolated kernel and bilinear form approach : u = "<<U_int<<eol;
//  saveToFile("U_int.vtk",U_int, _format=_vtk);
//
//  //----------------------------------------------------------------------------------
//  //   Test on an elbow waveguide using map
//  //----------------------------------------------------------------------------------
//   Mesh coude(inputsPathTo("coude60_court_moyen.msh"), _name="coude60deg");
//   Domain comega=coude.domain("omega");
//   Domain csigmaP=coude.domain("sigmaP");
//   Domain csigmaM=coude.domain("sigmaM");
//   //create interpolation
//   Space W(comega,P2,"W",true);
//   Unknown p(W,"p");
//   TestFunction q(p,"q");
//
//  //create spectral space and dtn kernel
//  Space cSp(csigmaP, Function(cosny,params), N, "cos(n*pi*y)");
//  Unknown cphiP(cSp,"cphiP");
//  TensorKernel ctkp(cphiP,lambda);
//  ctkp.xmap=Function(mapP);    //use map from sigma+ to sigma- where are defined spectral functions
//  ctkp.ymap=ctkp.xmap;
//
//  //================= map analytic kernel and full bilinear form approach =========================
//  BilinearForm apq=intg(comega,grad(p)|grad(q)) - k*k*intg(comega,p*q) - i*intg(csigmaP,csigmaP,p*ctkp*q);
//  LinearForm fq=intg(csigmaM,gp*q);
//  TermMatrix cA(apq,_storage=csDual,_name="cA");
//  TermVector cB(fq,_name="cB");

//  TermMatrix cLU;
//  luFactorize(cA,cLU);
//  TermVector P=directSolve(cLU,cB);
//  out<<"analytic kernel and bilinear form approach : p = "<<P<<eol;
//  saveToFile("P.vtk",P,_format=_vtk);
//  clear(cA,cLU);
//
//  //================= map interpolated kernel and full bilinear form approach =========================
//  TermVectors ccos_int(N);
//  for(Number n=1; n<=N; n++)
//    {
//      cos.parameter("basis index")=n;
//      ccos_int(n)=TermVector(p,csigmaM,cos,_name="c"+tostring(n-1));
//    }
//  Space cSp_int(ccos_int, "interpolated cos(n*pi*y)");
//  Unknown cphiP_int(cSp_int,"cphiP_int");
//  TensorKernel ctkp_int(cphiP_int,lambda);
//  ctkp_int.xmap=Function(mapP);
//  ctkp_int.ymap=ctkp_int.xmap;
//  BilinearForm apq_int=intg(comega,grad(p)|grad(q)) - k*k*intg(comega,p*q) - i*intg(csigmaP,csigmaP,p*ctkp_int*q);
//  LinearForm fq_int=intg(csigmaM,gp*q);
//  TermMatrix cA_int(apq_int,_storage=csDual,_name="A_int");
//  TermVector cB_int(fq_int,_name="B_int");
//  luFactorize(cA_int,cLU);
//  TermVector P_int=directSolve(cLU,cB_int);
//  out<<"interpolated kernel and bilinear form approach : p = "<<P_int<<eol;
//  saveToFile("P_int.vtk",P_int,_format=_vtk);
//
//  //================= kernel from computed eigen elements and full bilinear form approach =========================
//  //compute eigen elements on cross section
//  Mesh mesh1d(Segment(-0.5,0.5,21), _order=1);
//  Domain section=mesh1d.domain("Omega");
//  verboseLevel(2);
//  thePrintStream<<mesh1d;
//  Space S(section,P2,"S");
//  Unknown r(S,"r");
//  TestFunction s(r,"s");
//  BilinearForm as=intg(section,grad(r)|grad(s));
//  BilinearForm ms=intg(section,r*s);
//  TermMatrix As(as,_name="As");
//  TermMatrix Ms(ms,_name="Ms");
//  As+=Ms;
//  EigenElements eigs = eigenSolve(As, Ms, N, "SM");
//  lambda=sqrt(k*k-(eigs.values-1));
//  for(Number n=1;n<=N;n++)  //L2 normalisation
//  {
//      Real nl2=sqrt(real(Ms*eigs.vector(n)|eigs.vector(n)));
//      eigs.vector(n)/=nl2;
//      thePrintStream<<eigs.vector(n)<<eol;
//  }
//  Space sSp_int(eigs.vectors, "computed eigenvectors cos(n*pi*y)");
//  Unknown sphiP_int(sSp_int,"sphiP_int");
//  TensorKernel stkp_int(sphiP_int,lambda);
//  stkp_int.xmap=Function(mapS);
//  stkp_int.ymap=stkp_int.xmap;
//  BilinearForm sapq_int=intg(comega,grad(p)|grad(q)) - k*k*intg(comega,p*q) - i*intg(csigmaP,csigmaP,p*stkp_int*q);
//  LinearForm sfq_int=intg(csigmaM,gp*q);
//  TermMatrix scA_int(sapq_int,_storage=csDual,_name="A_int");
//  TermVector scB_int(sfq_int,_name="B_int");
//  thePrintStream<<scA_int<<scB_int;
//  scA_int.saveToFile("sCA_int.dat",_coo,false);
//  scB_int.saveToFile("sCB_int.dat",false);
//  TermMatrix scLU;
//  luFactorize(scA_int,scLU);
//  TermVector sP_int=directSolve(scLU,scB_int);
//  out<<"eigen interpolated kernel and bilinear form approach : p = "<<sP_int<<eol;
//  saveToFile("sP_int.vtk",sP_int,_format=_vtk);

  std::cout<<"U saved, that's all folks"<<eol;

  //------------------------------------------------------------------------------------
  // save results in a file or compare results with some references value in a file
  //------------------------------------------------------------------------------------
  if (check)
  {
    if (nbErrors == 0 ){ theCout << message("test_report", rootname, 0, ""); }
    else { error("test_report", rootname, nbErrors, ":\n"+errors); }
  }
  else { theCout << "Data updated " << eol; }

  trace_p->pop();
}

}
